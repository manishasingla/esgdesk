<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddProjectCR.aspx.cs" Inherits="AddProjectCR" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title><%= ConfigurationManager.AppSettings["CompanyName"].ToString()%></title>
     <link href="StyleSheet.css" rel="stylesheet" type="text/css" />
      <link href="DashStyle.css" rel="stylesheet" type="text/css" />
     
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <div id="inner">
            <div class="pagearea" style="width: 80%; float: left">
                <div id="TicketSummary">
                    <%--<h3 style="font-size: 16px; font-weight: bold; padding: 5px 0">--%>
                    <%--    <h3 style="font-size: 16px; font-weight: bold; padding: 5px 0px 5px 6px; background-color: rgb(62, 63, 61);
                        color: white; border-top: 4px solid rgb(0, 166, 81);">
                        New solution category</h3>--%>
                    <div id="errorExplanation" class="errorExplanation" runat="server" visible="false">
                        <p>
                            There were problems with the following fields:</p>
                        <ul id="ulalready" runat="server" visible="false">
                            <li>Category name has already been taken</li></ul>
                        <ul id="ulerror" runat="server" visible="false">
                            <li>Some error while adding category name </li>
                        </ul>
                    </div>
                    <table width="100%" style="background: #EBEBEB; padding: 6px">
                        <tr>
                            <td>
                                Title *
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtSolutionname" runat="server" Style="width: 99%;"> </asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This field is required."
                                    ControlToValidate="txtSolutionname" ValidationGroup="error"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Description
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <telerik:RadEditor ID="ftbComment" runat="server" Height="250px" Width="100%">
                                    <Modules>
                                        <telerik:EditorModule Name="RadEditorStatistics" ScriptFile="" Visible="False" />
                                        <telerik:EditorModule Name="RadEditorDomInspector" ScriptFile="" Visible="False" />
                                        <telerik:EditorModule Name="RadEditorNodeInspector" ScriptFile="" Visible="False" />
                                        <telerik:EditorModule Name="RadEditorHtmlInspector" ScriptFile="" Visible="False" />
                                    </Modules>
                                    <Links>
                                        <telerik:EditorLink Name="Telerik" Href="http://www.telerik.com">
                                            <telerik:EditorLink Name="Telerik Products" Href="http://www.telerik.com/products"
                                                Target="_blank" />
                                            <telerik:EditorLink Name="Telerik Support Center" Href="http://www.telerik.com/support/home.aspx"
                                                Target="_blank" />
                                            <telerik:EditorLink Name="Telerik Community" Href="http://www.telerik.com/community/home.aspx"
                                                Target="_blank" ToolTip="Telerik Community" />
                                        </telerik:EditorLink>
                                    </Links>
                                    <Snippets>
                                        <telerik:EditorSnippet Name="Product Template" Value="&lt;table&gt;&lt;tr&gt;&lt;td&gt;&lt;div style='width:300px;border:2px outset #D8D2BD;padding:34px;font-family:Arial, Verdana; font-size:11px;background- color:#F1EFE6;'&gt;Dear ____________________,&lt;br&gt;Thank you for inquiring about ____________. Your request will be processed in 48 hours and shipped at the address you have provided.&lt;b&gt;Please, contact us if you have any problems.&lt;/b&gt;&lt;/div&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;"></telerik:EditorSnippet>
                                        <telerik:EditorSnippet Name="Signature" Value="&lt;strong&gt;John Smith&lt;/strong&gt;&lt;br&gt;Sales Manager&lt;br&gt;"></telerik:EditorSnippet>
                                    </Snippets>
                                    <Content>
                                    </Content>
                                    <ImageManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                    <DocumentManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                    <FlashManager DeletePaths="~/Admin/Images,~/Admin/Images" UploadPaths="~/Admin/Images"
                                        ViewPaths="~/Admin/Images" />
                                    <MediaManager DeletePaths="~/Admin/Images,~/Admin/Images" UploadPaths="~/Admin/Images"
                                        ViewPaths="~/Admin/Images" />
                                    <TemplateManager DeletePaths="~/Admin/Images,~/Admin/Images" UploadPaths="~/Admin/Images"
                                        ViewPaths="~/Admin/Images" />
                                </telerik:RadEditor>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="This field is required."
                                    ControlToValidate="ftbComment" ValidationGroup="error"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td width="50%">
                                            <table>
                                                <tr>
                                                    <td>
                                                        Folder
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:DropDownList ID="ddlfolderlist" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <%-- <br />--%>
                    <table width="100%">
                        <tr>
                            <td style="text-align: right">
                                <asp:Button ID="bntdel" runat="server" Text="Delete" Visible="false" Style="float: left"
                                    OnClick="bntdel_Click" />
                                <asp:Button ID="btnupdate" runat="server" Text="Update" Visible="false" ValidationGroup="error"
                                    OnClick="btnupdate_Click" />
                                <asp:Button ID="btnsave" runat="server" Text="Save" OnClick="btnsave_Click" ValidationGroup="error" />
                                <asp:Button ID="btnSaveAnother" runat="server" Text="Save and Create Another" OnClick="btnSaveAnother_Click"
                                    ValidationGroup="error" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
    </form>
</body>
</html>
