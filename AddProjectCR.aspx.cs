using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class AddProjectCR : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
      if (Session["clientid"] == null || Session["username"] == null || Session["clientid"].ToString() == "" || Session["username"].ToString() == "")
        {
            Session["returnUrl"] = "ClientRequests.aspx";
            Response.Redirect("login.aspx", false);
            return;
        }
        bntdel.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete this solution?')");
        if (!IsPostBack)
        {
            try
            {
                if (Server.UrlDecode(Request.QueryString["projectname"]) != null && Request.QueryString["folderid"] != null && Request.QueryString["solutionid"] != null)
                {
                    try
                    {
                        getdropvalues(Server.UrlDecode(Request.QueryString["projectname"].ToString()));
                        getsolution(Server.UrlDecode(Request.QueryString["projectname"].ToString()), Request.QueryString["folderid"], Request.QueryString["solutionid"]);
                    }
                    catch (Exception ex)
                    {
                       // DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at AddProjectCR.aspx: " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Page Load Outer 1,Project Name:-" + Request.QueryString["projectname"] + " FolderId:-" + Request.QueryString["folderid"] + "Solution Id:-" + Request.QueryString["solutionid"]);
                    }

                }
                else if (Request.QueryString["projectname"] != null && Request.QueryString["folderid"] != null)
                {
                    try
                    {
                        getdropvalues(Server.UrlDecode(Request.QueryString["projectname"].ToString()));
                        ddlfolderlist.SelectedValue = Request.QueryString["folderid"].ToString();
                    }
                    catch (Exception ex)
                    {
                        //DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at AddProjectCR.aspx: " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Page Load Outer 2,Project Name:-" + Request.QueryString["projectname"] + " FolderId:-" + Request.QueryString["folderid"]);
                    }
                }
                else
                {
                    //=========================================
                    //Response.Redirect("solution.aspx");
                    //=========================================
                }
            }
            catch (Exception ex)
            {

                //DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at AddProjectCR.aspx: " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Page Load Outer ,Project Name:-" + Request.QueryString["projectname"] + " FolderId:-" + Request.QueryString["folderid"] + "Solution Id:-" + Request.QueryString["solutionid"]);
            }
        }

    }
    void getdropvalues(string cid)
    {
        string sql = @"select * from Project_Folder where IsProject='False' and Project_Name='" + cid + "'";
        DataSet ds = null;
        try
        {
            ds = DatabaseHelper.getallstatus(sql);
        }
        catch (Exception ex)
        {
            //DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at AddProjectCR.aspx: " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + " getdropvalues() " + sql);
        }
        if (ds != null)
        {
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {

                ddlfolderlist.DataSource = ds.Tables[0];
                ddlfolderlist.DataTextField = "foldername";
                ddlfolderlist.DataValueField = "folder_id";
                ddlfolderlist.DataBind();

            }
        }
    }
    void getsolution(string cid, string fid, string sid)
    {

        string sql = @"select * from ProjectNotes where IsProject='False' and Project_Name='" + cid + "' and folder_id= " + fid + " and Note_Id=" + sid;
        DataSet ds = null;
        try
        {
            ds = DatabaseHelper.getallstatus(sql);
        }
        catch (Exception ex)
        {
           // DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at AddProjectCR.aspx: " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "getsolution()" + sql);
        }

        if (ds != null)
        {
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                txtSolutionname.Text = ds.Tables[0].Rows[0]["NoteName"].ToString().Replace("|@", "'");
                ftbComment.Content = ds.Tables[0].Rows[0]["NoteDescription"].ToString().Replace("|@", "'");

                ddlfolderlist.SelectedValue = ds.Tables[0].Rows[0]["Folder_Id"].ToString();

                bntdel.Visible = true;
                btnupdate.Visible = true;
                btnsave.Visible = false;
                btnSaveAnother.Visible = false;
            }
        }
    }
    private void CategoryInsert(string next)
    {

        string sql = @"insert into ProjectNotes 
(Folder_Id,Project_Name,NoteName,NoteDescription,reported_date,IsProject)values
(" + ddlfolderlist.SelectedValue + ",'" + Server.UrlDecode(Request.QueryString["projectname"].ToString()) + "','" + txtSolutionname.Text.Replace("'", "|@").Trim() + "','" + ftbComment.Content.Replace("'", "|@") + "',getdate(),'False')";
        int intResult = 0;
        try
        {
            intResult = DatabaseHelper.executeSQLquery(sql);
        }
        catch (Exception ex)
        {
           // DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at AddProjectCR.aspx:1 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "CategoryInsert()" + sql);
        }

        if (intResult == 0)
        {
            errorExplanation.Visible = true;
            ulerror.Visible = true;
        }
        else
        {
            string SqlNotestatus = "";
            int insertvalue = 0;
            if (next == "")
            {
                if (Server.UrlDecode(Request.QueryString["projectname"]) != null && Request.QueryString["folderid"] != null)
                {
                    //=========================================================================
                    string selectNoteId = @"Select * from ProjectNotes where IsProject='False' and Folder_Id=" + ddlfolderlist.SelectedValue + " and Project_Name='" + Server.UrlDecode(Request.QueryString["projectname"].ToString()) + "' and NoteName='" + txtSolutionname.Text.Replace("'", "|@").Trim() + "' and NoteDescription='" + ftbComment.Content.Replace("'", "|@") + "'";
                    DataSet dsinsertStatus = null;
                    try
                    {
                        dsinsertStatus = DatabaseHelper.getDataset(selectNoteId);
                    }
                    catch (Exception ex)
                    {
                        //DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at AddProjectCR.aspx:2 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "CategoryInsert()" + selectNoteId);
                    }
                    if (dsinsertStatus != null && dsinsertStatus.Tables.Count > 0 && dsinsertStatus.Tables[0].Rows.Count > 0)
                    {
                        //======================================================================
                        string selectProject = "", SelectUser = "";
                        DataSet dsProject = null;
                        selectProject = "Select UserName from NonesCRMusers where ProjectName=(select ProjectName from NonesCRMusers where UserName='" + Session["username"] + "')";
                        try
                        {
                            dsProject = DatabaseHelper.getDataset(selectProject);
                        }
                        catch (Exception ex)
                        {

                           // DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at AddProjectCR.aspx:5 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "CategoryInsert()" + selectProject);
                        }

                        if (dsProject != null && dsProject.Tables.Count > 0 && dsProject.Tables[0].Rows.Count > 0)
                        {
                            for (int icount = 0; icount < dsProject.Tables[0].Rows.Count; icount++)
                            {

                               //======================================================================
                                //SqlNotestatus = @"insert into NotesStatus(Note_Id,Folder_Id,Project_Name,UserName,NoteStatus,IsProject)values('" + Convert.ToInt32(dsinsertStatus.Tables[0].Rows[0]["Note_Id"]) + " ','" + ddlfolderlist.SelectedValue + " ','" + Request.QueryString["projectname"].ToString() + "','" + Session["username"] + "','False','False')";
                                SqlNotestatus = @"insert into NotesStatus(Note_Id,Folder_Id,Project_Name,UserName,NoteStatus,IsProject)values('" + Convert.ToInt32(dsinsertStatus.Tables[0].Rows[0]["Note_Id"]) + " ','" + ddlfolderlist.SelectedValue + " ','" + Request.QueryString["projectname"].ToString() + "','" + dsProject.Tables[0].Rows[icount]["UserName"].ToString() + "','False','False')";
                                try
                                {
                                    insertvalue = DatabaseHelper.executeNonQuery(SqlNotestatus);
                                }
                                catch (Exception ex)
                                {
                                  //  DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at AddProjectCR.aspx:3 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "CategoryInsert()" + SqlNotestatus);
                                }
                            }
                        }
                    }
                    //==========================================================================

                    Response.Redirect("ProjectNoteListCR.aspx?projectname=" + Server.UrlEncode(Request.QueryString["projectname"].ToString()) + "&folderid=" + Request.QueryString["folderid"].ToString());
                }
                else
                {
                    Response.Redirect("ProjectNoteListCR.aspx?projectname=" + Server.UrlEncode(Request.QueryString["projectname"].ToString()));
                }
            }
            else
            {
                //=========================================================================
                string selectNoteId = @"Select * from ProjectNotes where IsProject='False' and Folder_Id=" + ddlfolderlist.SelectedValue + " and Project_Name='" + Server.UrlDecode(Request.QueryString["projectname"].ToString()) + "' and NoteName='" + txtSolutionname.Text.Replace("'", "|@").Trim() + "' and NoteDescription='" + ftbComment.Content.Replace("'", "|@") + "'";
                DataSet dsinsertStatus = null;
                try
                {
                    dsinsertStatus = DatabaseHelper.getDataset(selectNoteId);
                }
                catch (Exception ex)
                {
                  //  DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at AddProjectCR.aspx:4 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "CategoryInsert()" + selectNoteId);
                }
                if (dsinsertStatus != null && dsinsertStatus.Tables.Count > 0 && dsinsertStatus.Tables[0].Rows.Count > 0)
                {
                    string selectProject = "",SelectUser="";
                    DataSet dsProject = null;
                    selectProject = "Select UserName from NonesCRMusers where ProjectName=(select ProjectName from NonesCRMusers where UserName='" + Session["username"] + "')";
                    try
                    {
                    dsProject = DatabaseHelper.getDataset(selectProject);
                    }
                    catch(Exception ex)
                    {
                    
                  //  DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at AddProjectCR.aspx:5 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "CategoryInsert()" + selectProject);
                    }
                    
                    if (dsProject != null && dsProject.Tables.Count > 0 && dsProject.Tables[0].Rows.Count > 0)
                    {
                           for(int icount=0; icount < dsProject.Tables[0].Rows.Count; icount++)
                           {
                                //SqlNotestatus = @"insert into NotesStatus(Note_Id,Folder_Id,Project_Name,UserName,NoteStatus,IsProject)values('" + Convert.ToInt32(dsinsertStatus.Tables[0].Rows[0]["Note_Id"]) + " ','" + ddlfolderlist.SelectedValue + " ','" + Request.QueryString["projectname"].ToString() + "','" + Session["username"] + "','False','False')";
                               SqlNotestatus = @"insert into NotesStatus(Note_Id,Folder_Id,Project_Name,UserName,NoteStatus,IsProject)values('" + Convert.ToInt32(dsinsertStatus.Tables[0].Rows[0]["Note_Id"]) + " ','" + ddlfolderlist.SelectedValue + " ','" + Request.QueryString["projectname"].ToString() + "','" + dsProject.Tables[0].Rows[icount]["UserName"].ToString() + "','False','False')";
                                
                                
                                try
                                {
                                    insertvalue = DatabaseHelper.executeNonQuery(SqlNotestatus);
                                }
                                catch (Exception ex)
                                {
                                    //DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at AddProjectCR.aspx:5 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "CategoryInsert()" + SqlNotestatus);
                                }
                           }
                    }
                    
                }
                //==========================================================================

                clear();
            }
        }

    }

    private void Categoryupdate(string cid, string fid, string sid)
    {
        string sql = @"update ProjectNotes set Folder_Id=" + ddlfolderlist.SelectedValue + ",NoteName='" + txtSolutionname.Text.Replace("'", "|@").Trim() + "',NoteDescription='" + ftbComment.Content.Replace("'", "|@") + "' where IsProject='False' and Project_Name='" + cid + "' and Folder_Id=" + fid + " and Note_Id=" + sid;
        int intResult = 0;
        try
        {
            intResult = DatabaseHelper.executeSQLquery(sql);
        }
        catch (Exception ex)
        {
          //  DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at AddProjectCR.aspx:1 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Categoryupdate()" + sql);
        }

        if (intResult == 0)
        {
            errorExplanation.Visible = true;
            ulerror.Visible = true;
        }
        else
        {

            Response.Redirect("ProjectNoteListCR.aspx?projectname=" + Server.UrlEncode(Request.QueryString["projectname"].ToString()) + "&folderid=" + ddlfolderlist.SelectedValue + "&solutionid=" + sid);
        }

    }

    private void solutiondelete(string Categoryid, string folderid, string solutionid)
    {

        string sqlsub = @"delete from ProjectNotes where IsProject='False' and Project_Name='" + Categoryid + "' and Folder_Id=" + folderid + " and Note_Id=" + solutionid;
        int Result = 0;
        try
        {
            Result = DatabaseHelper.executeSQLquery(sqlsub);
        }
        catch (Exception ex)
        {
          //  DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at AddProjectCR.aspx:1 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "solutiondelete()" + sqlsub);
        }

        if (Result == 0)
        {
            errorExplanation.Visible = true;
            ulerror.Visible = true;
        }
        else
        {
            Response.Redirect("ProjectNoteListCR.aspx?projectname=" + Server.UrlEncode(Request.QueryString["projectname"].ToString()) + "&folderid=" + ddlfolderlist.SelectedValue);
        }

    }

    void clear()
    {
        try
        {
            txtSolutionname.Text = string.Empty;
            ftbComment.Content = string.Empty;
            errorExplanation.Visible = false;
            ulalready.Visible = false;
            ulerror.Visible = false;
        }
        catch (Exception ex)
        {
           // DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at AddProjectCR.aspx:1 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "clear()");
        }
    }
    protected void bntdel_Click(object sender, EventArgs e)
    {
        try
        {
            CategoryInsert("");
        }
        catch (Exception ex)
        {
          //  DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at AddProjectCR.aspx:1 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "btnsave_Click()");
        }
    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        try
        {
            Categoryupdate(Server.UrlDecode(Request.QueryString["projectname"]), Request.QueryString["folderid"], Request.QueryString["solutionid"]);
        }
        catch (Exception ex)
        {
          //  DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at AddProjectCR.aspx:1 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "btnupdate_Click()");
        }
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        try
        {
            CategoryInsert("");
        }
        catch (Exception ex)
        {
           // DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at AddProjectCR.aspx:1 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "btnsave_Click()");
        }
    }
    protected void btnSaveAnother_Click(object sender, EventArgs e)
    {
        try
        {
            CategoryInsert("Yes");
        }
        catch (Exception ex)
        {
           // DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at AddProjectCR.aspx:1 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "btnSaveAnother_Click()");
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {

            if (Server.UrlDecode(Request.QueryString["projectname"]) != null && Request.QueryString["folderid"] != null && Request.QueryString["solutionid"] != null)
            {
                Response.Redirect("ProjectdetailCR.aspx?projectname=" + Server.UrlEncode(Request.QueryString["projectname"].ToString()) + "&folderid=" + ddlfolderlist.SelectedValue + "&solutionid=" + Request.QueryString["solutionid"]);
            }
            else if (Request.QueryString["projectname"] != null && Request.QueryString["folderid"] != null)
            {
                Response.Redirect("ProjectNoteListCR.aspx?projectname=" + Server.UrlEncode(Request.QueryString["projectname"].ToString()) + "&folderid=" + ddlfolderlist.SelectedValue);
            }
            else
            {
                Response.Redirect("ProjectNoteListCR.aspx?projectname=" + Server.UrlEncode(Request.QueryString["projectname"].ToString()));
            }
        }
        catch (Exception ex)
        {
           // DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at AddProjectCR.aspx:1 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "btnCancel_Click()");
        }
    }
}
