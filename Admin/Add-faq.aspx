﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Add-faq.aspx.cs" Inherits="Admin_AddFaq" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="Notifications.ascx" TagName="Notifications" TagPrefix="uc4" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Add/Edit FAQ</title>
    <link rel="stylesheet" href="../class/style.css" type="text/css" media="screen, projection" />
    <link rel="stylesheet" href="../class/layout.css" type="text/css" media="screen, projection" />
<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script language="javascript" >
        function keypressHandler(evt) {

            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode == 13)
                var btn = document.getElementById('<%=btnAdd.ClientID%>');
            btn.focus();
            btn.click();
        }

    </script>
</telerik:RadCodeBlock>
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrap" class="container clearfix">
        <uc2:Admin_Header ID="Header1" runat="server" />
        <div class="borderdiv2">
        </div>
        <div class="divider4">
        </div>
        <div class="middivtest">
            <div>
                <h3 class="htitle">
                    Admin FAQ</h3>
            </div>
            <div class="txtabt">
                <div>
                    <table>
                        <tr>
                            <td>
                                Question :
                            </td>
                            <td>
                                <asp:TextBox ID="txtQuest" runat="server" Width="400px" onkeypress="javascript:keypressHandler(event)"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="ReqQ" runat="server" ControlToValidate="txtQuest"
                                    ErrorMessage="Please enter question." Display="None" ValidationGroup="g1">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" height="12">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Answer :
                            </td>
                            <td>
                                <%--==========================================================================================================--%>
                                <telerik:RadEditor ID="txtAnsw" runat="server" Height="300px" Width="100%">
                                    <Snippets>
                                        <telerik:EditorSnippet Name="Product Template" Value="<table><tr><td><div style='width:300px;border:2px outset #D8D2BD;padding:34px;font-family:Arial, Verdana; font-size:11px;background- color:#F1EFE6;'>Dear ____________________,<br>Thank you for inquiring about ____________. Your request will be processed in 48 hours and shipped at the address you have provided.<b>Please, contact us if you have any problems.</b></div></td></tr></table>">            
                                        </telerik:EditorSnippet>
                                        <telerik:EditorSnippet Name="Signature" Value="<strong>John Smith</strong><br>Sales Manager<br>">
                                        </telerik:EditorSnippet>
                                    </Snippets>
                                    <Links>
                                        <telerik:EditorLink Name="Telerik" Href="http://www.telerik.com">
                                            <telerik:EditorLink Name="Telerik Products" Href="http://www.telerik.com/products"
                                                Target="_blank" />
                                            <telerik:EditorLink Name="Telerik Support Center" Href="http://www.telerik.com/support/home.aspx"
                                                Target="_blank" />
                                            <telerik:EditorLink Name="Telerik Community" Href="http://www.telerik.com/community/home.aspx"
                                                Target="_blank" ToolTip="Telerik Community" />
                                        </telerik:EditorLink>
                                    </Links>
                                    <Tools>
                                    </Tools>
                                    <ImageManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                    <MediaManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                    <FlashManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                    <TemplateManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                    <DocumentManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                    <Modules>
                                        <telerik:EditorModule Name="RadEditorStatistics" Visible="False" Enabled="true" />
                                        <telerik:EditorModule Name="RadEditorDomInspector" Visible="False" Enabled="true" />
                                        <telerik:EditorModule Name="RadEditorNodeInspector" Visible="False" Enabled="true" />
                                        <telerik:EditorModule Name="RadEditorHtmlInspector" Visible="False" Enabled="true" />
                                    </Modules>
                                    <Content>
                                    </Content>
                                </telerik:RadEditor>
                                <%--==========================================================================================================--%>
                                <%--<asp:TextBox ID="txtAnsw" runat="server" Width="400px" TextMode="MultiLine" onkeypress="javascript:keypressHandler(event)"></asp:TextBox>--%>
                                <asp:RequiredFieldValidator ID="ReqA" runat="server" ControlToValidate="txtAnsw"
                                    ErrorMessage="Please enter answer." Display="None" ValidationGroup="g1">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" height="12">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="right">
                                <asp:Button ID="btnAdd" CssClass="button" runat="server" Text="Add" OnClick="btnAdd_Click"
                                    ValidationGroup="g1" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="left">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                                    ShowSummary="False" ValidationGroup="g1" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblmessageAD" runat="server" ForeColor="Red"></asp:Label>
                                <asp:SqlDataSource ID="SqlDataSource1" runat="server">
                                    <UpdateParameters>
                                        <asp:Parameter Name="ID" />
                                    </UpdateParameters>
                                    <DeleteParameters>
                                        <asp:Parameter Name="ID" />
                                    </DeleteParameters>
                                </asp:SqlDataSource>
                                <asp:GridView ID="GridView1" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                    AutoGenerateDeleteButton="True" BackColor="White" BorderColor="#39436f" AutoGenerateEditButton="True"
                                    BorderStyle="Solid" BorderWidth="1px" CellPadding="10" DataSourceID="SqlDataSource1"
                                    OnRowDeleting="GridView1_RowDeleting" DataKeyNames="ID" Font-Size="Small" Font-Strikeout="False"
                                    Font-Underline="False" OnRowDataBound="GridView1_RowDataBound" ShowFooter="True"
                                    Width="970px" AllowPaging="True" PageSize="20" OnRowUpdating="GridView1_RowUpdating"
                                    CellSpacing="15" HeaderStyle-BackColor="#39436e" HeaderStyle-ForeColor="#ffffff"
                                    HeaderStyle-CssClass="faqques" CssClass="faqtbl">
                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                    <Columns>
                                        <asp:BoundField DataField="ID" HeaderText="ID" ReadOnly="True" SortExpression="ID" />
                                        <asp:TemplateField HeaderText="Question" SortExpression="Question">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox1" Width="406px" runat="server" Text='<%# Bind("Question")%>'
                                                    CausesValidation="false"></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <%# ((string)Eval("Question")).Replace("|@", "'") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Answer" SortExpression="Answer">
                                         <EditItemTemplate>
                                         
                                         <%--==========================================================================================================--%>
                                <telerik:RadEditor ID="TextBox2" runat="server" Height="300px" Width="100%" Content='<%# Bind("Answer")%>'>
                                    <Snippets>
                                        <telerik:EditorSnippet Name="Product Template" Value="<table><tr><td><div style='width:300px;border:2px outset #D8D2BD;padding:34px;font-family:Arial, Verdana; font-size:11px;background- color:#F1EFE6;'>Dear ____________________,<br>Thank you for inquiring about ____________. Your request will be processed in 48 hours and shipped at the address you have provided.<b>Please, contact us if you have any problems.</b></div></td></tr></table>">            
                                        </telerik:EditorSnippet>
                                        <telerik:EditorSnippet Name="Signature" Value="<strong>John Smith</strong><br>Sales Manager<br>">
                                        </telerik:EditorSnippet>
                                    </Snippets>
                                    <Links>
                                        <telerik:EditorLink Name="Telerik" Href="http://www.telerik.com">
                                            <telerik:EditorLink Name="Telerik Products" Href="http://www.telerik.com/products"
                                                Target="_blank" />
                                            <telerik:EditorLink Name="Telerik Support Center" Href="http://www.telerik.com/support/home.aspx"
                                                Target="_blank" />
                                            <telerik:EditorLink Name="Telerik Community" Href="http://www.telerik.com/community/home.aspx"
                                                Target="_blank" ToolTip="Telerik Community" />
                                        </telerik:EditorLink>
                                    </Links>
                                    <Tools>
                                    </Tools>
                                    <ImageManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                    <MediaManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                    <FlashManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                    <TemplateManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                    <DocumentManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                    <Modules>
                                        <telerik:EditorModule Name="RadEditorStatistics" Visible="False" Enabled="true" />
                                        <telerik:EditorModule Name="RadEditorDomInspector" Visible="False" Enabled="true" />
                                        <telerik:EditorModule Name="RadEditorNodeInspector" Visible="False" Enabled="true" />
                                        <telerik:EditorModule Name="RadEditorHtmlInspector" Visible="False" Enabled="true" />
                                    </Modules>
                                    <Content>
                                    </Content>
                                </telerik:RadEditor>
                                <%--==========================================================================================================--%>
                                         <%--
                                                <asp:TextBox Width="423px" Height="100%" ID="TextBox2" MaxLength="250" runat="server"
                                                    Text='<%# Bind("Answer")%>' Wrap="true" CausesValidation="false" TextMode="MultiLine"></asp:TextBox>--%>
                                            <%--Content='<%# DataBinder.Eval( Container, "DataItem.Cntnt") %>'--%>
                                            
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <%# ((string)Eval("Answer")).Replace("|@", "'") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--  <asp:CheckBoxField DataField="ID" HeaderText="Select multiple"  SortExpression="ID" />--%>
                                    </Columns>
                                    <RowStyle ForeColor="Black" />
                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="White" ForeColor="#39436f" Font-Size="Medium" HorizontalAlign="Left" />
                                    <HeaderStyle Font-Bold="True" ForeColor="#39436f" />
                                    <EditRowStyle Wrap="true" />
                                    <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="footlogo">
            <div class="divider">
            </div>
            <div class="divider2">
            </div>
            <uc1:footer ID="Footer1" runat="server" />
        </div>
    </div>
    </form>
</body>
</html>
