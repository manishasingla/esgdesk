﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OleDb;
using System.Data;
using System.Configuration;
public partial class Admin_AddFaq : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] != null)
        {
            SqlDataSource1 = ConnSqlDataSource();
        }
        else
        {
            Session["ReturnUrl"] = "Add-faq.aspx";
            Response.Redirect("login.aspx");
        }
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        
        int intResult = DatabaseHelper.insertFaq(txtQuest.Text, txtAnsw.Content);
        if (intResult > 0)
        {
            lblMsg.Text = "Record inserted successfully.";
            SqlDataSource1 = ConnSqlDataSource();
            GridView1.DataBind();
            txtQuest.Text = "";
            txtAnsw.Content  = "";

        }
        else
        {
            lblMsg.Text = "Error occured while inserting record.";
        }
    }

    public SqlDataSource ConnSqlDataSource()
    {
        try
        {
            string strConn = ConfigurationManager.AppSettings["strConn"];
            //string CompName = ConfigurationManager.AppSettings["NewsCompanyName"];
            SqlDataSource1.ProviderName = "System.Data.SqlClient";
            SqlDataSource1.ConnectionString = strConn;
            SqlDataSource1.SelectCommand = "SELECT * from [FAQ]";
            SqlDataSource1.UpdateCommand = "UPDATE [FAQ] SET [Question]=@Question,[Answer]=@Answer WHERE [ID]=@ID";
            SqlDataSource1.DeleteCommand = "DELETE FROM [FAQ] WHERE [ID]=@ID";

        }
        catch
        {
            Response.Redirect("Admin_login.aspx", false);
        }
        return SqlDataSource1;

    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

        SqlDataSource1 = ConnSqlDataSource();
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton lb = (LinkButton)e.Row.Cells[0].Controls[2];
            if (lb.Text == "Delete")
            {
                lb.Attributes.Add("onclick", "return confirm('Are you sure you want to delete comment  " + ((DataRowView)e.Row.DataItem).Row["Question"].ToString() + " ? ')");
            }
        }

    }

    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        SqlDataSource1 = ConnSqlDataSource();
    }
}
