﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OleDb;
using System.Data;
using System.Configuration;
public partial class Admin_Glossary : System.Web.UI.Page
{
   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Admin"] != null)
        {
            SqlDataSource1 = ConnSqlDataSource();
        }
        else
        {
            Session["ReturnUrl"] = "Add-glossary.aspx";
            Response.Redirect("login.aspx");
        }
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
       
            int intResult = DatabaseHelper.insertGlossary(txtWord.Text, txtDesc.Text);
            //int intResult = 0;
            if (intResult > 0)
            {
                lblMsg.Text = "Record inserted successfully.";
                SqlDataSource1 = ConnSqlDataSource();
                GridView1.DataBind();
                txtWord.Text = "";
                txtDesc.Text = "";
            }
            else
            {
                lblMsg.Text = "Error occured while inserting record.";
            }
       
    }

    public SqlDataSource ConnSqlDataSource()
    {
        try
        {
            string strConn = ConfigurationManager.AppSettings["strConn"];

            //string CompName = ConfigurationManager.AppSettings["NewsCompanyName"];
            SqlDataSource1.ProviderName = "System.Data.SqlClient";
            SqlDataSource1.ConnectionString = strConn;

            SqlDataSource1.SelectCommand = "SELECT * from [Glossary] order by [Desc]";
            SqlDataSource1.UpdateCommand = "UPDATE [Glossary] SET [Word]=@Word,[Desc]=@Desc WHERE [ID]=@ID";
            SqlDataSource1.DeleteCommand = "DELETE FROM [Glossary] WHERE [ID]=@ID";

        }
        catch
        {
            Response.Redirect("Admin_login.aspx", false);
        }
        return SqlDataSource1;

    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

        SqlDataSource1 = ConnSqlDataSource();
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton lbDel = (LinkButton)e.Row.Cells[0].Controls[2];
            LinkButton lbEdit = (LinkButton)e.Row.Cells[0].Controls[0];
            if (lbDel.Text == "Delete" || lbDel.Text=="Update")
            {
                lbDel.Attributes.Add("onclick", "return confirm('Are you sure you want to delete comment  " + ((DataRowView)e.Row.DataItem).Row["Word"].ToString() + " ? ')");
               // lbDel.CausesValidation = false;
                lbDel.Attributes.Add("CausesValidation", "false");
            }
            if (lbEdit.Text == "Edit" || lbEdit.Text == "Cancel")
            {
                lbEdit.Attributes.Add("CausesValidation", "false");
               // lbEdit.CausesValidation = false;
            }
              
           
        }

    }

    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        SqlDataSource1 = ConnSqlDataSource();
       
    }
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        this.GridView1.EditIndex = e.NewEditIndex;
        //Bind GridView.
        GridViewRow currentRow = this.GridView1.Rows[e.NewEditIndex];
        LinkButton lb = (LinkButton)currentRow.Cells[0].Controls[0];
        lb.Attributes.Add("CausesValidation" ,"false");

    }
}
