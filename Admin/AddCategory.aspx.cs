﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Admin_AddCategory : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Response.Redirect("login.aspx");
            return;
        }
        else
        {
            if (!DatabaseHelper.isPrivateSolutions(Session["admin"].ToString()))
            {
                Response.Redirect("Solution.aspx");
            }
        }
        bntdel.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete this category?')");
        if (!IsPostBack)
        {
            if (Request.QueryString["categoryid"] != null)
            {

                GetCategory(Request.QueryString["categoryid"].ToString());
            }
        }
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        CategoryInsert("");
    }

    private void CategoryInsert(string next)
    {
        string sqlsub = @"select * from Category where CategoryName='" + txtCategoryname.Text.Replace("'", "|@").Trim() + "'";


        int Result = DatabaseHelper.checkuser(sqlsub);

        if (Result <= 0)
        {
            string sql = @"insert into Category (CategoryName,CategoryDescription,VisibleTo,IsDefaultCategory)values
                     ('" + txtCategoryname.Text.Replace("'", "|@").Trim() + "','" + txtDescription.Text.Replace("'", "|@") + "','" + ddlallow.SelectedItem.Value + "',0)";

            int intResult = DatabaseHelper.executeSQLquery(sql);

            if (intResult == 0)
            {
                errorExplanation.Visible = true;
                ulerror.Visible = true;
            }
            else
            {
                if (next == "")
                {
                    Response.Redirect("Solution.aspx");
                }
                else
                {
                    clear();
                }
            }
        }
        else
        {
            errorExplanation.Visible = true;
            ulalready.Visible = true;
        }

    }
    private void Categoryupdate(string cid)
    {

        string sqlsub = @"select * from Category where CategoryName='" + txtCategoryname.Text.Replace("'", "|@").Trim() + "' and category_id<>" + cid;


        int Result = DatabaseHelper.checkuser(sqlsub);

        if (Result <= 0)
        {

            string sql = @"update Category set CategoryName='" + txtCategoryname.Text.Replace("'", "|@").Trim() + "',CategoryDescription='" + txtDescription.Text.Replace("'", "|@") + "', VisibleTo='" + ddlallow.SelectedItem.Value + "' where Category_Id=" + cid;


            int intResult = DatabaseHelper.executeSQLquery(sql);

            if (intResult == 0)
            {
                errorExplanation.Visible = true;
                ulerror.Visible = true;
            }
            else
            {

                Response.Redirect("Solution.aspx");

            }
        }
        else
        {
            errorExplanation.Visible = true;
            ulalready.Visible = true;
        }
    }

    private void Categorydelete(string Categoryid)
    {
        string sqlsub = @"delete from Category where Category_id=" + Categoryid;

        int Result = DatabaseHelper.executeSQLquery(sqlsub);

        if (Result == 0)
        {
            errorExplanation.Visible = true;
            ulerror.Visible = true;
        }
        else
        {
            Response.Redirect("Solution.aspx");
        }

    }
    protected void btnSaveAnother_Click(object sender, EventArgs e)
    {
        CategoryInsert("Yes");
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Solution.aspx");
    }

    void clear()
    {
        txtCategoryname.Text = string.Empty;
        txtDescription.Text = string.Empty;
        errorExplanation.Visible = false;
        ulalready.Visible = false;
        ulerror.Visible = false;
    }
    private void GetCategory(string Categoryid)
    {
        string sql = @"select * from Category where Category_id=" + Categoryid;


        DataSet ds = DatabaseHelper.getallstatus(sql);

        if (ds != null)
        {
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                txtCategoryname.Text = ds.Tables[0].Rows[0]["CategoryName"].ToString().Replace("|@", "'");
                txtDescription.Text = ds.Tables[0].Rows[0]["CategoryDescription"].ToString().Replace("|@", "'");
                //ddlallow.SelectedItem.Value = ds.Tables[0].Rows[0]["VisibleTo"].ToString();
                ddlallow.SelectedValue = ds.Tables[0].Rows[0]["VisibleTo"].ToString();
                bntdel.Visible = true;
                btnupdate.Visible = true;
                btnsave.Visible = false;
                btnSaveAnother.Visible = false;
            }
        }
        else
        {
            errorExplanation.Visible = true;
            ulalready.Visible = true;
        }

    }
    protected void bntdel_Click(object sender, EventArgs e)
    {
        Categorydelete(Request.QueryString["categoryid"].ToString());
    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        Categoryupdate(Request.QueryString["categoryid"]);
    }
}