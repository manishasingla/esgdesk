<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddGoogleAc.aspx.cs" Inherits="Admin_AddGoogleAc" %>

<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Add google account</title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
    
  <script language="javascript" type="text/javascript">
    function checkProject()
    {
    var project =  document.getElementById("txtName").value;
    var drpprjct = document.getElementById("drpProjects").value;
    var rlevanUrl =  document.getElementById("txtRelevanUrl").value;
    if(drpprjct == "other")
    {
    
    if(project =="")
    {
    alert("Please enter project name");
    return false;
    }
       
    }
    
 /*else if(document.getElementById("chkRelevanturl").checked)
  
    {
     alert("start");
    if(rlevanUrl == "")
    {
    alert("start2");
    alert("Please enter relevant url");
    return false;
    }
    
    }*/
   else
    {
    var btnsend = document.getElementById("btnAddProject");
btnsend.click(); 
    }
    
    }
    
    </script> 
    
    
    <script language="javascript" type="text/javascript">
function showrelevanturl()
{


if(document.getElementById("chkRelevanturl").checked)
 {

 document.getElementById("TrRelevantUrl").style.display = "Block";
 }
  

  else
  {
   document.getElementById("TrRelevantUrl").style.display = "none";
   return false;
  }

}
  
  

</script> 

  <script language="javascript" type="text/javascript">
    function enableGoogleaccount()
    {

   
     var UsernameTab =  document.getElementById("TRGogUser");
   
     
    if( document.getElementById("ChkSEO").checked)
    {

   // UsernameTab.style.display = "block";

    
    document.getElementById("DivGogUser").style.display = "block";
 
     
    
    }
    else
    {

   // UsernameTab.style.display = "none";

    
    document.getElementById("DivGogUser").style.display = "none";
  
  
    }
    
    }
    </script>

    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
</head>
<body onload = "enableGoogleaccount();">
    <form id="form1" runat="server">
    <div id="wrapper">
        <uc2:Admin_Header ID="Admin_Header1" runat="server" />
        <div id="Content">
        <div width="100%" align="left" valign="middle" class="text_default" style="background:#ebebeb; padding:5px; margin-top:5px;">
            A<a href="AddGoogleAc.aspx">dd new google analytics</a><a href="GoogleAcList.aspx">&nbsp;|&nbsp;Back
            to google analytics</a><a href="projects.aspx" visible="false" id="FrmCompLnk" runat="server">&nbsp;|&nbsp;Back
            to project</a></div>
        <div id="DivEntry" runat="server" >
            <table>
      
              <tr>
                
                <td align="left" valign="top"><table style="width: 420px">
                    <tr>
                      <td align="left" class="titleText" valign="top" style="width: 100%x; display:none;"> Google Analytics&nbsp;|&nbsp;
                          
                            
                           
                          
                        </td>
                    </tr>

                    <tr>
                      <td><table width="420" cellpadding="0" cellspacing="0">
                          <tr>
                            <td align="left" valign="top"><img height="16" src="../images/loginBox_top.png" width="420" /></td>
                          </tr>
                          <tr>
                            <td align="left" style="background: url(../images/loginBox_mid.png) repeat-y left top;"><div style="text-align: center">
                                <table align="center" cellpadding="8" cellspacing="0" style="width: 95
                                %">
                                  <tr>
                                    <td align="left" class="whitetext1" style="width: 140px" valign="top">Project ID:</td>
                                    <td align="left" valign="top" class="whitetext2"><asp:Label ID="lblId" runat="server">New</asp:Label></td>
                                  </tr>
                                  <tr>
                                    <td align="left" class="whitetext1"   valign="top">Company name:</td>
                                    <td align="left" valign="top" class="whitetext2"><asp:DropDownList ID="drpCompany" runat="server" CssClass="filerDrpodown" AutoPostBack="True"  Width="220px" OnSelectedIndexChanged="drpCompany_SelectedIndexChanged"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="drpCompany"
                            ErrorMessage="Please select company name.">*</asp:RequiredFieldValidator>
                                        <cc1:ValidatorCalloutExtender
                            ID="ValidatorCalloutExtender2" runat="server" TargetControlID="RequiredFieldValidator2"> </cc1:ValidatorCalloutExtender>                                    </td>
                                  </tr>
                                  <tr>
                                    <td align="left" class="whitetext1"  valign="top">Project name:</td>
                                    <td align="left" valign="top" class="whitetext2"><asp:DropDownList ID="drpProjects" runat="server" CssClass="filerDrpodown" AutoPostBack="True" OnSelectedIndexChanged="drpProjects_SelectedIndexChanged" Width="220px" Height="24"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName"
                            ErrorMessage="Please enter project name.">*</asp:RequiredFieldValidator>
                                        <cc1:ValidatorCalloutExtender
                            ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator1"> </cc1:ValidatorCalloutExtender>
                                        <table id="TDAddNewProject" runat="server" style="border:solid 1px #999999; padding-top:10px">
                                          <tr style="padding-top:7px">
                                            <td><asp:TextBox ID="txtName" runat="server" MaxLength="50" Width="215px"></asp:TextBox></td>
                                          </tr>
                                          <tr align="right">
                                            <td align="right" style="height: 25px"><asp:Label ID="lblProjectmsg" Visible="false"  runat="server" ForeColor="Red" Text=""></asp:Label>
                                                <asp:Button
                                    ID="btnAddProject"  runat="server" Visible="false"  Text="Add" CausesValidation="false" OnClick="btnAddProject_Click" />                                        
                                              <asp:Button
                                    ID="btncloseProject" runat="server"  Text="Close" CausesValidation="False" OnClick="btncloseProject_Click"  />                                            </td>
                                          </tr>
                                      </table></td>
                                  </tr>
                                  <tr>
                                    <td align="left" class="whitetext1"  valign="top">Website url:</td>
                                    <td align="left" valign="top" class="whitetext2"><asp:TextBox ID="txtwebsite" runat="server" MaxLength="50" Width="215px"></asp:TextBox></td>
                                  </tr>
                                  <tr>
                                    <td align="left" class="whitetext1" style="width: 96px;"  valign="top"><span id="DataGrid1_ctl03_lblComment">SEO?</span>:</td>
                                    <td align="left" valign="top" class="whitetext2"><asp:CheckBox id="ChkSEO" onclick="enableGoogleaccount();"  runat="server" Text="SEO" ></asp:CheckBox></td>
                                  </tr>
                                </table>
                            </div></td>
                          </tr>
                          <tr id="TRGogUser">
                            <td align="left" style="background: url(../images/loginBox_mid.png) repeat-y left top;"><div id="DivGogUser" runat="server">
                              <table align="center" cellpadding="5" cellspacing="0" style="width: 90%">
                                <tr>
                                  <td align="left" valign="top" class="whitetext1" id="TRGogUser1" style="height: 26px"> Google username</td>
                                  <td id="TRGogUser2" align="left" style="padding-left:12px; height: 26px;" valign="top" ><asp:TextBox ID="txtGogUsername" runat="server" MaxLength="150" Width="220px"></asp:TextBox></td>
                                </tr>
                                <tr>
                                  <td align="left"  valign="top" class="whitetext1" style="height: 26px"> Google password</td>
                                  <td align="left"  style="padding-left:12px; height: 26px;"  valign="top"><asp:TextBox ID="txtGogPassword" runat="server" MaxLength="50" TextMode="Password" Width="220px"></asp:TextBox></td>
                                </tr>
                              </table>
                            </div></td>
                          </tr>
                          <tr>
                            <td align="left" style="width: 100px"><img height="16" src="../images/loginBox_btm.png" width="420" /></td>
                          </tr>
                      </table></td>
                    </tr>
                    <tr>
                      <td align="left" valign="top"><hr  style="width:420px;" align="left"/></td>
                    </tr>
                   
                    <tr>
                      <td style="width: 424px"><table border="0" cellpadding="0" cellspacing="0" width="420">
                            <tr>
                              <td style="height: 24px"></td>
                              <td align="right" style="height: 24px"><asp:Button ID="btnCreate" runat="server" Text="Create" OnClick="btnCreate_Click" CssClass="blueBtns"  /></td>
                            </tr>
                        </table></td>
                    </tr>
                    <tr>
                      <td><div id="divMessage2" runat="server" style="color: red" visible="true"> </div></td>
                    </tr>
                </table></td>
              </tr>
            </table>
      
          <asp:HiddenField ID="prevName" runat="server" />
        </div> 
        <div id="divMessage" runat="server" style="color:Red;font-weight:bold"></div>
        <br />
        <div>
        </div>
        </div>  </div>  
       
                <uc1:footer ID="Footer1" runat="server" />
    </form>
</body>
</html>

