using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Admin_AddGoogleAc : System.Web.UI.Page
{
    int id;
    string sql = "";
    protected void Page_Load(object sender, EventArgs e)
    {

        btnCreate.Attributes.Add("Onclick", "return checkProject();");
  ///chkRelevanturl.Attributes.Add("OnClick", "showrelevanturl();");
        ////Page.RegisterClientScriptBlock("Onload", "showrelevanturl();");

        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = "AddGoogleAc.aspx";
            Response.Redirect("login.aspx");
            return;
        }

        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            Response.Redirect("tasks.aspx");
            return;
        }
        //////try
        //////{
        //////    if (Request.QueryString["Comp"] == null || Request.QueryString["Comp"].ToString() == "")
        //////    {
        //////        FrmCompLnk.Visible = false;
        //////    }
        //////    else if (Request.QueryString["Comp"].ToString() == "True")
        //////    {
        //////        FrmCompLnk.Visible = true;
        //////    }
        //////}
        //////catch { }


        try
        {
            if (Request.QueryString["Project"] == null || Request.QueryString["Project"].ToString() == "")
            {
                FrmCompLnk.Visible = false;
            }
            else if (Request.QueryString["Project"].ToString() == "True")
            {
                FrmCompLnk.Visible = true;
            }
        }
        catch { }
        

        string var = Request.QueryString["id"];
        if (var == null)
        {
            id = 0;
        }
        else
        {
            try
            {
                id = Convert.ToInt32(var);
            }
            catch
            {
                divMessage.InnerHtml = "Project ID must be an integer.";
                DivEntry.Visible = false;
                return;
            }
        }

        if (!IsPostBack)
        {
            // add or edit?
            TDAddNewProject.Visible = false;
            loadCompany_dropdowns();
            Loadprogect_drp();
            if (id == 0)
            {
                clearControls();
            }
            else
            {
              

                string strSql = "select * from projects where project_id=" + id;
                DataSet ds = DatabaseHelper.getDataset(strSql);

                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        lblId.Text = ds.Tables[0].Rows[0]["project_id"].ToString();
                        drpCompany.SelectedValue = ds.Tables[0].Rows[0]["CompanyName"].ToString();
                        Loadprogect_drp();
                        txtName.Text = ds.Tables[0].Rows[0]["project_name"].ToString();
                        drpProjects.SelectedValue = ds.Tables[0].Rows[0]["project_name"].ToString();
                        prevName.Value = ds.Tables[0].Rows[0]["project_name"].ToString();


                        if (ds.Tables[0].Rows[0]["SEO"].ToString() != "")
                        {
                            if (ds.Tables[0].Rows[0]["SEO"].ToString() == "True")
                            {
                                ChkSEO.Checked = true;
                                DivGogUser.Style["display"] = "block";
                               // DivGogUser.Visible = true;
                            }
                            else
                            {
                                ChkSEO.Checked = false;
                                DivGogUser.Style["display"] = "none";
                              ///DivGogUser.Visible = false;
                            }

                        }
                        else
                        {
                            ChkSEO.Checked = false;
                            DivGogUser.Style["display"] = "none";
                            //DivGogUser.Visible = false;
                        }

                        txtwebsite.Text = ds.Tables[0].Rows[0]["WebsiteUrl"].ToString();
                        txtGogUsername.Text = ds.Tables[0].Rows[0]["GoogleUserId"].ToString();
                       /// txtGogPassword.Text = ds.Tables[0].Rows[0]["GooglePwd"].ToString();s
                        string Gpwd = ds.Tables[0].Rows[0]["GooglePwd"].ToString();
                        txtGogPassword.Attributes.Add("value", Gpwd);
                       
                        btnCreate.Text = "Update";
                        DivEntry.Visible = true;
                        DivEntry.Visible = true;
                        divMessage.InnerHtml = "";
                        TDAddNewProject.Visible = true;
                    }
                    else
                    {
                        divMessage.InnerHtml = "Project was not found.";
                        DivEntry.Visible = false;
                    }
                }
                else
                {
                    divMessage.InnerHtml = "Project was not found.";
                    DivEntry.Visible = false;
                }
            }


           
        }
        else
        {
           
        }
    }

    void Loadprogect_drp()
    {
        drpProjects.Items.Clear();
        sql = @"select *
		from projects
		where active = 'Y' and CompanyName='"+ drpCompany.SelectedValue.ToString() +"' order by project_name;";
        DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);
        if (ds_dropdowns.Tables[0].Rows.Count > 0)
        {
            try
            {
                drpProjects.DataSource = ds_dropdowns.Tables[0];
                drpProjects.DataTextField = "project_name";
                drpProjects.DataValueField = "project_id";
                drpProjects.DataBind();
               drpProjects.Items.Insert(0, new ListItem("[no project]", ""));
               /// drpProjects.Items.Insert(1, new ListItem("[Other]", "other"));
            }
            catch { }
        }
        else
        {
            drpProjects.Items.Insert(0, new ListItem("[no project]", ""));
            ///drpProjects.Items.Insert(1, new ListItem("[Other]", "other"));
        }

    }

    void loadCompany_dropdowns()
    {

        // projects
        ////////////        sql = @"select project_name
        ////////////		from projects
        ////////////		where active = 'Y' order by project_name;";

        sql = @"select distinct Company_Name
		from Company where Company_Name !='' order by Company_Name;";
       /// DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);
        // do a batch of sql statements
        
            DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);
            try
            {
            drpCompany.DataSource = ds_dropdowns.Tables[0];
            drpCompany.DataTextField = "Company_Name";
            drpCompany.DataValueField = "Company_Name";
            drpCompany.DataBind();
            drpCompany.Items.Insert(0, new ListItem("[no company]", ""));
        }
        catch { }
       // drpProjects.Items.Insert(1, new ListItem("[Other]", "other"));


    }

    protected void btnCreate_Click(object sender, EventArgs e)
    {
        if (txtName.Text.Trim() == "")
        {
            divMessage.InnerHtml = "Please enter project name.";
            return;
        }

       

        if (btnCreate.Text == "Create")
        {
            object objResult = DatabaseHelper.executeScalar("select count(*) from projects where [project_name]='" + txtName.Text.Trim() + "'");

            if (objResult.ToString() != "0")
            {
                divMessage.InnerHtml = "Project name alredy exist.";
                return;
            }


            string strSql = " update projects ";
           // strSql += " set [project_name]= '" + txtName.Text.Trim().Replace("'", "''") + "',";
            /// strSql += " [description] = '" + txtDescription.Text.Trim().Replace("'", "''")  + "',";
            //  strSql += " [default_selection] = '" + (chkDefault.Checked ? "Y" : "N") + "',";
            // strSql += " [active] = '" + (chkActive.Checked ? "Y" : "N") +"',";
            strSql += " set [CompanyName] = '" + drpCompany.SelectedValue.ToString().Replace("'", "''") + "',";
            strSql += " [WebsiteUrl] = '" + txtwebsite.Text.Replace("'", "''") + "',";
            strSql += " [SEO] = '" + (ChkSEO.Checked ? "True" : "False") + "',";
            strSql += " [GoogleUserId] = '" + txtGogUsername.Text.Replace("'", "''") + "',";
            strSql += " [GooglePwd] = '" + txtGogPassword.Text.Replace("'", "''") + "'";
            strSql += " where project_id = " + lblId.Text.Trim();

            int intResult = DatabaseHelper.executeNonQuery(strSql);


            if (intResult != 0)
            {
                divMessage.InnerHtml = "Project was created.";
                clearControls();
            }
            else
            {
                divMessage.InnerHtml = "Project was not created.";
            }
        }
        else if (btnCreate.Text == "Update")
        {
            object objResult = DatabaseHelper.executeScalar("select count(*) from projects where [project_name]='" + txtName.Text.Trim() + "' and [project_id]!=" + lblId.Text);

            if (objResult.ToString() != "0")
            {
                divMessage.InnerHtml = "Project name alredy exist.";
                return;
            }

            string strSql = " update projects ";
           // strSql += " set [project_name]= '" + txtName.Text.Trim().Replace("'", "''") + "',";
           /// strSql += " [description] = '" + txtDescription.Text.Trim().Replace("'", "''")  + "',";
          //  strSql += " [default_selection] = '" + (chkDefault.Checked ? "Y" : "N") + "',";
           // strSql += " [active] = '" + (chkActive.Checked ? "Y" : "N") +"',";
            strSql += " set [CompanyName] = '" + drpCompany.SelectedValue.ToString().Replace("'", "''") + "',";
            strSql += " [WebsiteUrl] = '" + txtwebsite.Text.Replace("'", "''") + "',";
            strSql += " [SEO] = '" + (ChkSEO.Checked ? "True" : "False") + "',";
            strSql += " [GoogleUserId] = '" + txtGogUsername.Text.Replace("'", "''") + "',";
            strSql += " [GooglePwd] = '" + txtGogPassword.Text.Replace("'", "''") + "'";
            strSql += " where project_id = " + lblId.Text.Trim();

            int intResult = DatabaseHelper.executeNonQuery(strSql);

            if (intResult != 0)
            {
                ////////strSql = "update tasks set project = '" + txtName.Text.Trim().Replace("'", "''") + "' ";
                ////////strSql += " where project ='" + prevName.Value.Replace("'", "''") + "'";
                ////////intResult = DatabaseHelper.executeNonQuery(strSql);
                ////////strSql = "update ClientRequest set ProjectName = '" + txtName.Text.Trim().Replace("'", "''") + "' ";
                ////////strSql += " where ProjectName ='" + prevName.Value.Replace("'", "''") + "'";
                ////////intResult = DatabaseHelper.executeNonQuery(strSql);

                divMessage.InnerHtml = "Project was updated.";
            }
            else
            {
                divMessage.InnerHtml = "Project was not updated.";
            }
        }
    }
    
    private void clearControls()
    {
        lblId.Text = "New";
        txtName.Text = "";
        ///chkActive.Checked = true;
        //txtDescription.Text = "";
        btnCreate.Text = "Create";
    }
    protected void drpCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        Loadprogect_drp();
        txtName.Text = "";
        TDAddNewProject.Visible = false;
        DivGogUser.Style["display"] = "none";
        ChkSEO.Checked = false;
        txtwebsite.Text = "";
        divMessage.InnerHtml = "";
        txtGogPassword.Text = "";
        txtGogUsername.Text = "";
        
        
    }

    protected void btnAddProject_Click(object sender, EventArgs e)
    {

        object objResult = DatabaseHelper.executeScalar("select count(*) from projects where [project_name]='" + txtName.Text.Trim() + "'");

        if (objResult.ToString() != "0")
        {
            lblProjectmsg.Text = "Project name alredy exist.";
            return;
        }

        string desc = "";
        string dfltselection = "N";
        string stractive = "Y";

        string strSql = " insert into projects ([project_name],[description],[default_selection],[active],[CompanyName]) ";
        strSql += " values('" + txtName.Text.Trim().Replace("'", "''") + "','" + desc.Trim().Replace("'", "''") + "','" + dfltselection + "','" + stractive + "', '" + drpCompany.SelectedValue.ToString().Replace("'", "''") + "')";

        int intResult = DatabaseHelper.executeNonQuery(strSql);

        if (intResult != 0)
        {
            Loadprogect_drp();
            lblProjectmsg.Text = "Project added successfully.";

            ///clearControls();
        }
        else
        {
            lblProjectmsg.Text = "Project was not created.";
        }

    }
    protected void btncloseProject_Click(object sender, EventArgs e)
    {

        txtName.Text = "";
        lblProjectmsg.Text = "";
        Loadprogect_drp();
        TDAddNewProject.Visible = false;
    }
    protected void drpProjects_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (drpProjects.SelectedValue == "other")
        {
            TDAddNewProject.Visible = false;
            txtName.Text = "";
            ///txtCompanyName.Visible = true;
        }
        else
        {
            TDAddNewProject.Visible = false;
            txtName.Text  = drpProjects.SelectedItem.ToString();
            // txtCompanyName.Visible = false;
        }
       lblId.Text = drpProjects.SelectedValue;
        try
        {
            if (lblId.Text.ToString() != "0")
            {

                string strSql = "select * from projects where project_id='" + lblId.Text.ToString() + "'";
                DataSet ds = DatabaseHelper.getDataset(strSql);

                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        lblId.Text = ds.Tables[0].Rows[0]["project_id"].ToString();
                        drpCompany.SelectedValue = ds.Tables[0].Rows[0]["CompanyName"].ToString();
                        ///Loadprogect_drp();
                        txtName.Text = ds.Tables[0].Rows[0]["project_name"].ToString();
                      //  drpProjects.SelectedValue = ds.Tables[0].Rows[0]["project_name"].ToString();
                       /// prevName.Value = ds.Tables[0].Rows[0]["project_name"].ToString();


                        if (ds.Tables[0].Rows[0]["SEO"].ToString() != "")
                        {
                            if (ds.Tables[0].Rows[0]["SEO"].ToString() == "True")
                            {
                                ChkSEO.Checked = true;
                               // DivGogUser.Visible = true;
                                DivGogUser.Style["display"] = "block";
                            }
                            else
                            {
                                ChkSEO.Checked = false;
                                DivGogUser.Style["display"] = "none";
                            }

                        }
                        else
                        {
                            ChkSEO.Checked = false;
                            DivGogUser.Style["display"] = "none";
                        }

                        txtwebsite.Text = ds.Tables[0].Rows[0]["WebsiteUrl"].ToString();
                        txtGogUsername.Text = ds.Tables[0].Rows[0]["GoogleUserId"].ToString();
                        /// txtGogPassword.Text = ds.Tables[0].Rows[0]["GooglePwd"].ToString();
                        string Gpwd = ds.Tables[0].Rows[0]["GooglePwd"].ToString();
                        txtGogPassword.Attributes.Add("value", Gpwd);

                        btnCreate.Text = "Update";
                        DivEntry.Visible = true;
                        DivEntry.Visible = true;
                        divMessage.InnerHtml = "";
                        TDAddNewProject.Visible = true;
                    }
                    else
                    {
                        divMessage.InnerHtml = "Project was not found.";
                        DivEntry.Visible = false;
                    }
                }
                else
                {
                    divMessage.InnerHtml = "Project was not found.";
                    DivEntry.Visible = false;
                }
            }
            else
            {
                divMessage.InnerHtml = "Please select Project name.";
                DivEntry.Visible = false;
            }
        }
        catch { }



    }
}
