﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Admin_AddNews : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Response.Redirect("login.aspx");
            return;
        }
        bntdel.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete this category?')");
        if (!IsPostBack)
        {
            if (Request.QueryString["id"] != null)
            {

                GetCategory(Request.QueryString["id"].ToString());
            }
        }
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        newsInsert("");
    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        newsupdate(Request.QueryString["id"]);
    }
    protected void btnSaveAnother_Click(object sender, EventArgs e)
    {
        newsInsert("Yes");
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if (Request.QueryString["id"] != null)
        {
            Response.Redirect("newsdetails.aspx?id=" + Request.QueryString["id"]);
        }
        else
        {
            Response.Redirect("newslist.aspx");
        }
    }
    protected void bntdel_Click(object sender, EventArgs e)
    {
        newsdelete(Request.QueryString["id"]);
    }

    private void newsInsert(string next)
    {

        string sql = @"insert into Newstbl(heading,comment,Addeddate,Visible,EmporCR)values
                     ('" + txtnewsname.Text.Replace("'", "|@") + "','" + txtDescription.Text.Replace("'", "|@") + "',GETDATE(),'" + ddlshownews.SelectedValue + "','" + ddladdto.SelectedValue + "')";

        int intResult = DatabaseHelper.executeSQLquery(sql);

        if (intResult == 0)
        {
            errorExplanation.Visible = true;
            ulerror.Visible = true;
        }
        else
        {
            if (next == "")
            {
                Response.Redirect("newslist.aspx");
            }
            else
            {
                clear();
            }
        }


    }
    private void newsupdate(string id)
    {


        string sql = @"update Newstbl set heading='" + txtnewsname.Text.Replace("'", "|@") + "',comment='" + txtDescription.Text.Replace("'", "|@") + "',Visible='" + ddlshownews.SelectedValue + "',EmporCR='" + ddladdto.SelectedValue + "'   where Id=" + id;


        int intResult = DatabaseHelper.executeSQLquery(sql);

        if (intResult == 0)
        {
            errorExplanation.Visible = true;
            ulerror.Visible = true;
        }
        else
        {

            Response.Redirect("newslist.aspx");

        }

    }
    private void newsdelete(string id)
    {
        string sqlsub = @"delete from Newstbl where id=" + id;

        int Result = DatabaseHelper.executeSQLquery(sqlsub);

        if (Result == 0)
        {
            errorExplanation.Visible = true;
            ulerror.Visible = true;
        }
        else
        {
            Response.Redirect("newslist.aspx");
        }

    }
    void clear()
    {
        txtnewsname.Text = string.Empty;
        txtDescription.Text = string.Empty;
        ddladdto.SelectedIndex = 0;
        ddlshownews.SelectedIndex = 0;
        errorExplanation.Visible = false;
        ulalready.Visible = false;
        ulerror.Visible = false;
    }
    private void GetCategory(string id)
    {
        string sql = @"select * from Newstbl where id=" + id;


        DataSet ds = DatabaseHelper.getallstatus(sql);

        if (ds != null)
        {
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                txtnewsname.Text = ds.Tables[0].Rows[0]["heading"].ToString().Replace("|@", "'");
                txtDescription.Text = ds.Tables[0].Rows[0]["comment"].ToString().Replace("|@", "'");
                ddladdto.SelectedValue = ds.Tables[0].Rows[0]["EmporCR"].ToString().Replace("|@", "'");
                ddlshownews.SelectedValue = ds.Tables[0].Rows[0]["Visible"].ToString().Replace("|@", "'");

                bntdel.Visible = true;
                btnupdate.Visible = true;
                btnsave.Visible = false;
                btnSaveAnother.Visible = false;
                this.Title = "Edit news";
            }
            
        }
        else
        {
            errorExplanation.Visible = true;
            ulalready.Visible = true;
        }

    }
}