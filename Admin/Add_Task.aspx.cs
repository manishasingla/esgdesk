using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.Drawing.Drawing2D;
using System.IO;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Net;
//using System.Windows.Forms;


public partial class Admin_Add_Task : System.Web.UI.Page
{
    int id;
    int reqid;
    int IRTID;
    string sql = "";
    string task_action = "";
    string emailSubject = "";
    bool deletedFlag = false;
    bool flagEmailNotification = false;
    protected static string taskCompany;
   
    protected static string strresource;
    int etcMins = 0;


    static ArrayList arr1aAssignedUsers = new ArrayList();

    protected void Page_Load(object sender, EventArgs e)
    {
       //// Page.ClientScript.RegisterOnSubmitStatement(typeof(string), "onSubmit", "return validateForm(Admin_edit_task.aspx)");
        btnOK.Attributes.Add("onClick", "return checkedOkConfirm();");
        btnCancel.Attributes.Add("onClick", "return checkedCancelConfirm();");
        drpStatuses.Attributes.Add("onchange", "return GetStatus();");
     //   LnkMakecommentRead.Attributes.Add("onclick", "return confirm('Sure you want to mark every comment in every CR as read by you?');");
        //LnkMakeTaskcommentRead.Attributes.Add("onclick", "javascript:return confirm('Sure you want to mark every comment in each of your tasks as read by you (this means you must have read them all and acted on all that require action already)?');");
       // LnkMakeAllTaskCmmntRead.Attributes.Add("onclick", "javascript:return confirm('Sure you want to mark every comment in each of tasks as read by you (this means you must have read them all and acted on all that require action already)?');");
        ChkIncludeRead.Attributes.Add("onclick", "return GotoIncludeRead();");
        CheckAllasRead.Attributes.Add("onclick", "return GotoChaekAllread();");
        CheckAllToDelete.Attributes.Add("onclick", "return GotoChaekAllDel();");
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = (Request.QueryString["id"] == null || Request.QueryString["id"].ToString() == "" ? "edit_task.aspx" : "edit_task.aspx?id=" + Request.QueryString["id"].ToString());
            //========================================
            //if (ftbComment.Text.Trim() != "")
            //{
                //Session["comment"] = ftbComment.Text;
            //}
            if (ftbComment.Content.Trim() != "")
            {
                Session["comment"] = ftbComment.Content;
            }
            //=========================================
            Response.Redirect("login.aspx");
            return;
        }

        divMessage1.InnerHtml = "";
        divMessage.InnerHtml = "";

        if (Session["Canned_msg"] == null || Session["Canned_msg"].ToString() == "")
        {

        }
        else
        {
            Session.Remove("Canned_msg");
        }
        
        string task_id = Request["id"];
        if (task_id == null || task_id == "0" || task_id == "")
        {
            id = 0;
        }
        else
        {
            if (DatabaseHelper.is_int(task_id))
            {
                id = Convert.ToInt32(task_id);
               LnkAskQuestion.HRef = "task_Question.aspx?id=" + id.ToString(); 
            }
            else
            {
                // Display an error because the bugid must be an integer
                divMessage.InnerHtml = "Task ID must be an integer.";
                divMessage.InnerHtml += "<p><a href='tasks.aspx'>View tasks</a></p>";
                DivEntry.Visible = false;
                return;
            }
        }

        string req_id = Request["reqid"];
        if (req_id == null || req_id == "0" || req_id == "")
        {
            reqid = 0;
            lnkBackToRequest.Visible = false;
     
        }
         

        else
        {
            if (DatabaseHelper.is_int(req_id))
            {
                reqid = Convert.ToInt32(req_id);
                lnkBackToRequest.HRef = "edit_request.aspx?reqid=" + reqid.ToString();
                lnkBackToRequest.Visible = true;
            }
            else
            {
                // Display an error because the bugid must be an integer
                divMessage.InnerHtml = "<br>Change request ID must be an integer.";
                divMessage.InnerHtml += "<p><a href='client_requests.aspx'>View change requests</a></p>";
                DivEntry.Visible = false;
                return;
            }
        }

        ///string IntRNo = Request["IrtID"];

        string IntRNo = Request["IrtID"];
        if (IntRNo == null || IntRNo == "0" || IntRNo == "")
        {
            ///id = Convert.ToInt32(id);
            IRTID = 0;
            LnkbacktoTask.Visible = false;
            LnkAskQuestion.Visible = false;
            lnkCreateNewCR.Visible = false;

        }
        else
        {
            if (DatabaseHelper.is_int(IntRNo))
            {
                IRTID = Convert.ToInt32(IntRNo);
                LnkbacktoTask.HRef = "edit_task.aspx?id=" + IRTID.ToString();
                LnkbacktoTask.Visible = true;
            }
            else
            {
                // Display an error because the bugid must be an integer

                divMessage.InnerHtml = "<br>Task ID must be an integer.";
                divMessage.InnerHtml += "<p><a href='tasks.aspx'>View tasks</a></p>";
                /// DivEntry.Visible = false;
                return;
            }
        }

        if (!Page.IsPostBack)
        {
            chkShowAllHistory.Attributes.Add("onclick", "checkChecked();");
            lnkNudge.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want send Nudge to task owner?')");
            BtnDelAllCmnt.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete selected comments?')");
            load_dropdowns();
            set_default_selection();
            

            if (Session["Task_Canned_msg"] == null || Session["Task_Canned_msg"].ToString() == "")
            {
                //==========================
                //ftbComment.Text = "";
                  ftbComment.Content = "";
                //==========================
            }
            else
            {

                try
                {
                    if (Session["Task_ShrtDescCanned_msg"] == null || Session["Task_ShrtDescCanned_msg"].ToString() == "")
                    {
                        txtShortDescr.Text = "";

                    }
                    else
                    {
                        txtShortDescr.Text = Session["Task_ShrtDescCanned_msg"].ToString();
                    }
                }

                catch { }
                //=======================================================
                //ftbComment.Text =  Session["Task_Canned_msg"].ToString();
                  ftbComment.Content = Session["Task_Canned_msg"].ToString();
                //=======================================================
            }

            if (id == 0)
            {
                if (reqid != 0)
                
                {
                    sql = "\nSelect * from ClientRequest where RequestId=" + reqid.ToString() + ";";

                    sql += "\nSelect * from ClientRequest_Details where RequestId=" + reqid.ToString() + " order by CommentId desc";

                    DataSet dsClientRequest = DatabaseHelper.getDataset(sql);

                    // change request
                    if (dsClientRequest.Tables[0].Rows.Count > 0)
                    {
                        try
                        {
                            txtShortDescr.Text = dsClientRequest.Tables[0].Rows[0]["ShortDescr"].ToString();
                            txtRelevantURL.Text = dsClientRequest.Tables[0].Rows[0]["WebsiteURL"].ToString();
                            lblProject.Text = dsClientRequest.Tables[0].Rows[0]["ProjectName"].ToString();
                            hdnProjectCompany.Value = dsClientRequest.Tables[0].Rows[0]["CompanyName"].ToString();
                            ///drpProjects.SelectedValue = dsClientRequest.Tables[0].Rows[0]["ProjectName"].ToString();
                            try
                            {
                                drpProjects.SelectedValue = drpProjects.Items.FindByText(dsClientRequest.Tables[0].Rows[0]["ProjectName"].ToString()).Value;
                            }
                            catch { }
                        }
                        catch { }
                        ///ftbComment.Text = "The following has been created from a Change Request added by a client. The words or some of the words are those of the client and therefore may not have been checked or amended for clarity.";

                    }
                    else
                    {
                        divMessage.InnerHtml = "<br>Request ID not found.";
                        divMessage.InnerHtml += "<p><a href='client_requests.aspx'>View change requests</a></p>";
                        DivEntry.Visible = false;
                        lnkBackToRequest.Visible = false;
                        return;
                    }

                    // merge change request comments
                    if (dsClientRequest.Tables[1].Rows.Count > 0)
                    {
                        //=======================================================================================================
                        //ftbComment.Text = "<span style=\"font-style:italic;\">" + "The following has been created from a Change Request added by a client. The words or some of the words are those of the client and therefore may not have been checked or amended for clarity." + "</span>" + "<br/><br/>";
                          ftbComment.Content = "<span style=\"font-style:italic;\">" + "The following has been created from a Change Request added by a client. The words or some of the words are those of the client and therefore may not have been checked or amended for clarity." + "</span>" + "<br/><br/>";
                        //=======================================================================================================
                        for (int i = 0; i < dsClientRequest.Tables[1].Rows.Count; i++)
                        {
                            //=============================================================================================
                            //ftbComment.Text += dsClientRequest.Tables[1].Rows[i]["Comment"].ToString() + "<br/><hr/><br/>";
                              ftbComment.Content += dsClientRequest.Tables[1].Rows[i]["Comment"].ToString() + "<br/><hr/><br/>";
                            //=============================================================================================
                        }
                    }

                    lnkSendAttachment.HRef = "upload_attachment.aspx?reqid=" + reqid.ToString();
                    lnkViewAttachment.HRef = "view_attachments.aspx?reqid=" + reqid.ToString();
                    DataSet dsTotalAttachments = DatabaseHelper.getAttachments(reqid, "");
                    lnkViewAttachment.InnerHtml = "View attachments (" + dsTotalAttachments.Tables[0].Rows.Count + ")";

                    divAttachments.Visible = false;
                    chkQuestion.Visible = false;
                    BtnDelAllCmnt.Visible = false;
                    CheckAllasRead.Visible = false;
                    CheckAllToDelete.Visible = false;
                    btnUpdate.Text = "Create";
                    btnDelete.Visible = false;
                    lblTaskId.Text = "New";
                    lblProject.Text = drpProjects.SelectedItem.Text;
                    lblAssignedTo.Text = drpUsers.SelectedItem.Text;
                }

                else if (IRTID != 0)
                {
                    divAttachments.Visible = true;
                    ChkIncludeRead.Visible = false;
                    chkShowAllHistory.Visible = false;
                    lnkSubscribers.HRef = "view_subscribers.aspx?id=" + IRTID.ToString();
                    lnkSubscribers.Visible = true;
                    lnkCreateNewCR.HRef = "add_changeRequest.aspx?id=" + IRTID.ToString();
                 
                    lnkCreateNewCR.Visible = false;
                    lnkHrsReport.HRef = "report_task_hrs.aspx?id=" + IRTID.ToString();
                    lnkHrsReport.Visible = true;
                    lnkNudge.Visible = true;
                    btnUpdate.Text = "Create";
                    btnDelete.Visible = false;
                    lblTaskId.Text = "New";
                    LblRefTaskId.Text = IRTID.ToString();
                    BindData();


                }

              
            }

           

            else
            {
                // Get this entry's data from the db and fill in the form

                divAttachments.Visible = true;
                lnkSubscribers.HRef = "view_subscribers.aspx?id=" + id.ToString();
                lnkSubscribers.Visible = true;
                lnkCreateNewCR.HRef = "add_changeRequest.aspx?id=" + id.ToString();
                lnkHrsReport.HRef = "report_task_hrs.aspx?id=" + id.ToString();
                lnkHrsReport.Visible = true;
                lnkCreateNewCR.Visible = false;
                lnkNudge.Visible = true;
                lblTaskId.Text = id.ToString();
                BindData();
            }

            if (!DatabaseHelper.can_Delete_Tasks(Session["admin"].ToString()))
            {
                btnDelete.Visible = false;
            }


       
           
        
         
          
       
           
            getClientRelated();
            
           /// getAllNotification();
            getOverduetasks();

            if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
            {
                LnkRelated.Visible = false;
                Lnkcomprelated.Visible = false;
                CompanyNotes.Visible = true;
                CRNotes.Visible = true;
                lnkCreateNewCR.Visible = false;
                BtnAnalytics.Visible = false;
                //Notification.Visible = true;
               //// CompanyNote.Visible = false;
            }
            else
            {
                LnkRelated.Visible = true;
                Lnkcomprelated.Visible = true;
                CompanyNotes.Visible = true;
                CRNotes.Visible = true;
                lnkCreateNewCR.Visible = true;
                BtnAnalytics.Visible = true;
              ////  Notification.Visible = true;
                ///CompanyNote.Visible = true;

            }
        }
       

        register1aTaskAllocated();
     

        ensureCurrentlyWorkingOnStatus();
        
        if (Session["message"] != null)
        {
            divMessage1.InnerHtml = Session["message"].ToString();
            Session.Remove("message");
        }

        if (Session["comment"] != null)
        {
            //===================================================
            //ftbComment.Text = Session["comment"].ToString();
            ftbComment.Content = Session["comment"].ToString();
            //===================================================
            Session.Remove("comment");
        }
        //===========================================================================================================
        string strCNotes = "window.open('companyNotes.aspx?CompName=" + hdnProjectCompany.Value + "','_blank');";
        CompanyNotes.Attributes.Add("onClick", strCNotes);
        string strCRNotes = "window.open('CRNotes.aspx?CompName=" + hdnProjectCompany.Value + "','_blank');";
        CRNotes.Attributes.Add("onClick", strCRNotes);
        //===========================================================================================================
    }


    void getOverduetasks()
    {

        try
        {

            int GvnETC = 0;
            int j = 0;
            DateTime crrntdate = DateTime.Now.Date;
            DateTime duedate;

            sql = @"select tasks.* from tasks 
                        where tasks.deleted <> 1 and  status <> 'closed' and  status <> 'checked' and status <>'parked'";

            if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
            {
                sql += " and assigned_to_user = '" + Session["admin"] + "' ";
            }

            DataSet ds = DatabaseHelper.getDataset(sql);

            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {


                ////BtnOverdue.Text = "(" + ds.Tables[0].Rows.Count + ")";
                ////immediatetask.Visible = true;

                ////for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                ////{
                ////    int ETC = Convert.ToInt32(ds.Tables[0].Rows[i]["ETC"].ToString());

                ////    ///GvnETC = "" + (ETC / 60) + " hrs " + (ETC % 60) + " mins";

                ////    int hrsTaken = getHoursTakenSoFar(ds.Tables[0].Rows[i]["task_id"].ToString());

                ////   //// e.Item.Cells[14].Text = "" + (hrsTaken / 60) + " hrs " + (hrsTaken % 60) + " mins";

                ////    ///int hrsLeft = ETC - hrsTaken;

                ////    if(hrsTaken > ETC)
                ////    {
                ////        j++;
                ////    }
                ////}



                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string strcrrntdate = DateTime.Parse(DateTime.Now.Date.ToString()).ToString("dd/MM/yyyy");
                    string djgfjs = ds.Tables[0].Rows[i]["DueDate"].ToString();
                    if (ds.Tables[0].Rows[i]["DueDate"].ToString() != null && ds.Tables[0].Rows[i]["DueDate"].ToString() != "")
                    {
                        string strduedate = DateTime.Parse(ds.Tables[0].Rows[i]["DueDate"].ToString()).ToString("dd/MM/yyyy");
                        duedate = DateTime.Parse(strduedate.ToString());
                        crrntdate = DateTime.Parse(strcrrntdate.ToString());
                        if (DateTime.Compare(duedate, crrntdate) < 0)
                        {

                            j++;

                        }


                    }

                }


                if (j > 0)
                {
                   /// BtnOverdue.Text = "(" + j.ToString() + ")";
                   ///OverDuetsk.Visible = true;
                }
                else
                {
                    //BtnOverdue.Visible = false;
                   /// OverDuetsk.Visible = false;
                }


            }
            else
            {
               /// BtnOverdue.Visible = false;
                ///OverDuetsk.Visible = false;
            }

        }
        catch { }

    }


   /// protected void BtnOverdue_Click(object sender, EventArgs e)
   /// {

       // Session["filter"] = " ";
        //Session["filterunread"] = "";
       // Session["OverdueTask"] = "True";
       /// Response.Redirect("tasks.aspx");


   //// }

  


  

    protected void LnkNotification_Click(object sender, EventArgs e)
    {
        Response.Redirect("Notification.aspx", false);
    }

    public void BindData()
    {
        sql = "select * from tasks where task_id = " + LblRefTaskId.Text.Trim();

        DataSet dsTask = DatabaseHelper.getDataset(sql);

        if (dsTask == null || dsTask.Tables.Count <= 0 || dsTask.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "<br>Task not found.<br><br><a href='tasks.aspx'>View tasks</a>";
            DivEntry.Visible = false;
            divTaskDetails.Visible = false;
        }
        else
        {
            if (dsTask.Tables[0].Rows[0]["deleted"].ToString() == "1")
            {
                if (!DatabaseHelper.can_Show_Deleted_Tasks(Session["admin"].ToString()))
                {
                    divMessage.InnerHtml = "<br>Task not found.<br><br><a href='tasks.aspx'>View tasks</a>";
                    DivEntry.Visible = false;
                    divTaskDetails.Visible = false;
                    return;
                }
                else
                {
                    btnUpdate.Text = "Mark as undeleted";
                    btnDelete.Text = "Delete permanently";
                }

                deletedFlag = true;
            }
            else
            {
                deletedFlag = false;
            }

            LblRefTaskId.Text = dsTask.Tables[0].Rows[0]["task_id"].ToString();
            lblIRTN.Text = "Linked ref task: <a href='edit_task.aspx?id=" + LblRefTaskId.Text.ToString() + "'>" + LblRefTaskId.Text.ToString() + "</a>";

            if (dsTask.Tables[0].Rows[0]["RequestId"].ToString() != "0")
            {


                lblIRN.Text = " Linked ref CR: <a href='edit_request.aspx?reqid=" + dsTask.Tables[0].Rows[0]["RequestId"].ToString() + "'>" + dsTask.Tables[0].Rows[0]["RequestId"].ToString() + "</a>";
                string sqlCR = "select RequestId from ClientRequest where RequestId = " + dsTask.Tables[0].Rows[0]["RequestId"].ToString() + " and Status  <> 'closed' and deleted <> 1 ";
                object ObjCRstatus = DatabaseHelper.executeScalar(sqlCR);
                if (ObjCRstatus != null)
                {
                    btnDelete.Attributes.Add("onclick", "javascript:return confirm('Please note that this task has a RELATED CHANGE REQUEST which is not yet closed or deleted. Should it still be deleted??');");

                }
                else
                {
                    btnDelete.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete task?');");
                }


              
            }
            else
            {
                lblIRN.Text = "";
                btnDelete.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete task?');");
            }

            if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
            {
                lblIRN.Visible = false;
            }
            string sqlResources = "select Resources from Resources where status = 'to check'";
            object ObjResources = DatabaseHelper.executeScalar(sqlResources);
                if (ObjResources != null)
                {

                 
                    FtbResources.Text = ObjResources.ToString();
                 
                  

                   // strresource = ObjResources.ToString().Replace("'", "").Replace("<br>", "\n");
                  
                }
                else
                {
                    strresource = "";
                }

                try
                {


                    lblLastUpdated.Text = " Last changed by <b>" + dsTask.Tables[0].Rows[0]["last_updated_user"].ToString() + "</b> on <b>" + DateTime.Parse(dsTask.Tables[0].Rows[0]["last_updated_date"].ToString()).ToString("dd MMM h:mm tt") + "</b>";
                    lblReportedBy.Text = "Reported by <b>" + dsTask.Tables[0].Rows[0]["reported_user"].ToString() + "</b> on <b>" + DateTime.Parse(dsTask.Tables[0].Rows[0]["reported_date"].ToString()).ToString("dd MMM h:mm tt") + "</b>";

                    lblProject.Text = dsTask.Tables[0].Rows[0]["project"].ToString();
                    lblAssignedTo.Text = dsTask.Tables[0].Rows[0]["assigned_to_user"].ToString();
                    try
                    {
                        drpProjects.SelectedValue = drpProjects.Items.FindByText(dsTask.Tables[0].Rows[0]["project"].ToString()).Value;
                    }
                    catch { }
                    try
                    {
                        drpUsers.SelectedValue = drpUsers.Items.FindByText(dsTask.Tables[0].Rows[0]["assigned_to_user"].ToString()).Value;
                    }
                    catch { }
                   /// drpStatuses.SelectedValue = drpStatuses.Items.FindByText(dsTask.Tables[0].Rows[0]["status"].ToString()).Value;
                    drpCategories.SelectedValue = drpCategories.Items.FindByText(dsTask.Tables[0].Rows[0]["category"].ToString()).Value;
                    drpPriorities.SelectedValue = drpPriorities.Items.FindByText(dsTask.Tables[0].Rows[0]["priority"].ToString()).Value;
                }
                catch { }

            try
            {
                drpProjectType.SelectedValue = drpProjectType.Items.FindByText(dsTask.Tables[0].Rows[0]["project_type"].ToString()).Value;
            }
            catch { }
            txtShortDescr.Text = dsTask.Tables[0].Rows[0]["short_desc"].ToString();
            txtRelevantURL.Text = dsTask.Tables[0].Rows[0]["Relevant_URL"].ToString();

            try
            {
                prevProject.Value = drpProjects.Items.FindByText(dsTask.Tables[0].Rows[0]["project"].ToString()).Value;
            }
            catch { }
            TaskProject.Value = dsTask.Tables[0].Rows[0]["project"].ToString();
            prevAssignedTo.Value = drpUsers.Items.FindByText(dsTask.Tables[0].Rows[0]["assigned_to_user"].ToString()).Value;
            prevStatus.Value = drpStatuses.Items.FindByText(dsTask.Tables[0].Rows[0]["status"].ToString()).Value;
            prevCategory.Value = drpCategories.Items.FindByText(dsTask.Tables[0].Rows[0]["category"].ToString()).Value;
            prevPriority.Value = drpPriorities.Items.FindByText(dsTask.Tables[0].Rows[0]["priority"].ToString()).Value;
            prevProjectType.Value = drpProjectType.Items.FindByText(dsTask.Tables[0].Rows[0]["project_type"].ToString()).Value;
            prevShortDescr.Value = dsTask.Tables[0].Rows[0]["short_desc"].ToString();
            prevRelevantUrl.Value = dsTask.Tables[0].Rows[0]["Relevant_URL"].ToString();
            prevETC.Value = dsTask.Tables[0].Rows[0]["ETC"].ToString();
            hdnProjectCompany.Value = "";
            string sqlcompany = "select [CompanyName] from [projects]  where [project_name] = '" + dsTask.Tables[0].Rows[0]["project"].ToString() + "'";
            object objResult = DatabaseHelper.executeScalar(sqlcompany);

            if (objResult != null)
            {
                hdnProjectCompany.Value = objResult.ToString();
                taskCompany = objResult.ToString();
            }
            else
            {
                hdnProjectCompany.Value = dsTask.Tables[0].Rows[0]["Company"].ToString();
                taskCompany = dsTask.Tables[0].Rows[0]["Company"].ToString();
            }

            try
            {

                string sqlSEO = "select [SEO] from [projects]  where [project_name] = '" + dsTask.Tables[0].Rows[0]["project"].ToString() + "'";
                object objResultSEO = DatabaseHelper.executeScalar(sqlSEO);

                if (objResultSEO.ToString() == "True")
                {
                    BtnAnalytics.ForeColor = Color.Red;
                }
                else
                {
                    BtnAnalytics.ForeColor = Color.Blue;
                    BtnAnalytics.Attributes.Add("onclick", "return alert('Please contact support as your subscription level does not allow to view analytics.');");
                }


            }
            catch { }


            string sqlcompanynote = "select [Notes] from [Company]  where Company_Name = '" + dsTask.Tables[0].Rows[0]["Company"].ToString() + "'";
            object objNoteResult = DatabaseHelper.executeScalar(sqlcompanynote);
            try
            {
                if (objNoteResult.ToString() != "")
                {
                    //CompanyNotes.ForeColor = Color.Red;
                    CompanyNotes.Style.Add("color", "#FF0000");

                }
                else
                {
                    //CompanyNotes.ForeColor = Color.Blue;
                    CompanyNotes.Style.Add("color", "#0033ff");
                }
            }
            catch { }
            string sqlCRnote = "select [ClientNotes] from [Company]  where Company_Name = '" + dsTask.Tables[0].Rows[0]["Company"].ToString() + "'";
            object objCRNoteResult = DatabaseHelper.executeScalar(sqlCRnote);

            try
            {
                if (objCRNoteResult.ToString() != "")
                {
                    //CRNotes.ForeColor = Color.Red;
                    CRNotes.Style.Add("color", "#FF0000");

                }
                else
                {
                   // CRNotes.ForeColor = Color.Blue;
                    CRNotes.Style.Add("color", "#0033ff");
                }
            }
            catch { }


            try
            {
                int etcMins = Convert.ToInt32(prevETC.Value);
                int hrs = etcMins / 60;
                int mins = etcMins % 60;

                drpHrs.SelectedValue = hrs.ToString();
                drpMins.SelectedValue = mins.ToString();
            }
            catch { }

            try
            {
                int ETC = Convert.ToInt32(dsTask.Tables[0].Rows[0]["ETC"].ToString());

                int hrsTaken = getHoursTakenSoFar(LblRefTaskId.Text.Trim());

                lblTotalHrs.Text = "Total hours so far: <b>" + (hrsTaken / 60) + " hrs " + (hrsTaken % 60) + " mins</b>";

                int hrsLeft = ETC - hrsTaken;

                if (hrsLeft < 0)
                {
                    hrsLeft = -1 * hrsLeft;
                    lblHrsLeft.Text = "Expected hrs left : <b>-" + (hrsLeft / 60) + " hrs " + (hrsLeft % 60) + " mins</b>";
                }
                else
                {
                    lblHrsLeft.Text = "Expected hrs left : <b>" + (hrsLeft / 60) + " hrs " + (hrsLeft % 60) + " mins</b>";
                }
            }
            catch { }

            lnkSendAttachment.HRef = "upload_task_attachment.aspx?id=" + dsTask.Tables[0].Rows[0]["task_id"].ToString();
            lnkViewAttachment.HRef = "task_attachments.aspx?id=" + dsTask.Tables[0].Rows[0]["task_id"].ToString();
            DataSet dsTotalAttachments = DatabaseHelper.getTaskAttachments(Convert.ToInt32(dsTask.Tables[0].Rows[0]["task_id"].ToString()), "");
            lnkViewAttachment.InnerHtml = "View attachments (" + dsTotalAttachments.Tables[0].Rows.Count + ")";
            lnkCreateNewCR.HRef = "add_changeRequest.aspx?id=" + dsTask.Tables[0].Rows[0]["task_id"].ToString();
            if (ChkIncludeRead.Checked)
            {
                sql = "select * from task_comments where deleted <> 1 and  task_id = " + LblRefTaskId.Text.Trim() + " order by tc_id desc";
            }
            else
            {
                sql = "select * from task_comments where deleted <> 1 and  task_id = " + LblRefTaskId.Text.Trim() + " and tc_id not in(select tc_id from read_comments where username ='" + Session["admin"].ToString() + "') order by tc_id desc";
            }

            DataSet dsTaskDetails = DatabaseHelper.getDataset(sql);

            if (dsTaskDetails == null || dsTaskDetails.Tables.Count <= 0 || dsTaskDetails.Tables[0].Rows.Count <= 0)
            {
                
                BtnDelAllCmnt.Visible = false;
                CheckAllasRead.Visible = false;
                CheckAllToDelete.Visible = false;

            }
            else
            {
                BtnDelAllCmnt.Visible = false;
                CheckAllasRead.Visible = false;
                CheckAllToDelete.Visible = false;

                
                    /// ftbComment.Text = "<span style=\"font-style:italic;\">" + "The following has been created from a Change Request added by a client. The words or some of the words are those of the client and therefore may not have been checked or amended for clarity." + "</span>" + "<br/><br/>";

                for (int i = 0; i < dsTaskDetails.Tables[0].Rows.Count; i++)
                    {
                        //==================================================================================================
                        //ftbComment.Text += dsTaskDetails.Tables[0].Rows[i]["Comment"].ToString() + "<br/><hr/><br/>";
                        ftbComment.Content += dsTaskDetails.Tables[0].Rows[i]["Comment"].ToString() + "<br/><hr/><br/>";
                        //==================================================================================================
                    }
               




            }
           /// DataGrid1.DataSource = dsTaskDetails.Tables[0];
            ///DataGrid1.DataBind();

            //--------------------------------

            sql = "select * from task_comments where task_id = " + LblRefTaskId.Text.Trim() + " order by tc_id desc";

            DataSet dsTaskDetails1 = DatabaseHelper.getDataset(sql);

            dsTaskDetails1.Tables[0].Columns.Add("temp");

            for (int i = 0; i < dsTaskDetails1.Tables[0].Rows.Count; i++)
            {
                dsTaskDetails1.Tables[0].Rows[i]["temp"] = "C";
            }

            sql = "select * from task_updates where task_id = " + LblRefTaskId.Text.Trim() + " order by tc_id desc";

            DataSet dsTaskUpdates = DatabaseHelper.getDataset(sql);

            System.Type myDataType;
            myDataType = System.Type.GetType("System.Int32");
            dsTaskUpdates.Tables[0].Columns.Add("deleted", myDataType);
            dsTaskUpdates.Tables[0].Columns.Add("temp");

            for (int i = 0; i < dsTaskUpdates.Tables[0].Rows.Count; i++)
            {
                dsTaskUpdates.Tables[0].Rows[i]["deleted"] = 0;
                dsTaskUpdates.Tables[0].Rows[i]["temp"] = "U";
            }

            dsTaskDetails1.Merge(dsTaskUpdates);


            DataView view = new DataView(dsTaskDetails1.Tables[0]);
            view.Sort = "post_date Desc, temp desc ";

            DataGrid2.DataSource = view;
            DataGrid2.DataBind();

            if (chkShowAllHistory.Checked == true)
            {
                divAllHistory.Style.Add("display", "block");
                divNoHistory.Style.Add("display", "none");
            }
            else
            {
                divAllHistory.Style.Add("display", "none");
                divNoHistory.Style.Add("display", "block");
            }
        }
    }

    bool ReturnValue()
    {

        return false;

    }


    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            etcMins = Convert.ToInt32(drpHrs.SelectedValue) * 60 + Convert.ToInt32(drpMins.SelectedValue);
        }
        catch { }

        if (hdnTocheckStatus.Value == "False")
        {
            return;
        }

        
            if (lblTaskId.Text == "New")
            {
                //=======================================
                //if (ftbComment.Text.Trim() == "")
                if (ftbComment.Content.Trim() == "") 
                //=======================================
                {
                    divMessage1.InnerHtml = "Comment is required.";
                    return;
                }
                else
                {
                    flagEmailNotification = true;
                }

                sql = @"DECLARE @TaskId INT;";
                sql += "DECLARE @CommentId INT;";
                sql += " insert into tasks([RequestId],[short_desc],[reported_user],[reported_date],[OrderId],[status],[priority],[category],[project],[project_type],[Relevant_URL],[assigned_to_user],[last_updated_user],[last_updated_date],[deleted],[ETC],Company,[Internalref]) ";
                sql += " values(" + reqid.ToString() + ",";
                sql += "'" + txtShortDescr.Text.Trim().Replace("'", "''") + "',";
                sql += "'" + Session["admin"].ToString().Replace("'", "''") + "',";
                sql += "getdate(),";
                sql += "99999,";
                sql += "'" + drpStatuses.SelectedItem.Text.Replace("'", "''") + "',";
                sql += "'" + drpPriorities.SelectedItem.Text.Replace("'", "''") + "',";
                sql += "'" + drpCategories.SelectedItem.Text.Replace("'", "''") + "',";
                sql += "'" + drpProjects.SelectedItem.Text.Replace("'", "''") + "',";
                sql += "'" + drpProjectType.SelectedItem.Text.Replace("'", "''") + "',";
                sql += "'" + txtRelevantURL.Text.Trim().Replace("'", "''") + "',";
                sql += "'" + drpUsers.SelectedItem.Text.Replace("'", "''") + "',";
                sql += "'" + Session["admin"].ToString().Replace("'", "''") + "',";
                sql += "getdate(),";
                sql += "0,";
                sql += "" + etcMins.ToString() + ",'" + hdnProjectCompany.Value.ToString() + "','"+ LblRefTaskId.Text +"'); ";
                sql += " SET @TaskId = SCOPE_IDENTITY(); ";
                //=======================================
                //if (ftbComment.Text.Trim() != "")
                  if (ftbComment.Content.Trim() != "") 
                //=======================================
                {
                    sql += " insert into task_comments([task_id],[username],[post_date],[comment],[deleted],[qflag],[QuesTo]) ";
                    sql += " values(@TaskId,";
                    sql += "'" + Session["admin"].ToString().Replace("'", "''") + "',";
                    sql += "getdate(),";
                    //=======================================
                    //sql += "'" + ftbComment.Text.Trim().Replace("'", "''") + "',";
                      sql += "'" + ftbComment.Content.Trim().Replace("'", "''") + "',";
                    //=======================================
                    sql += "0,";
                   /// sql += (drpCategories.SelectedItem.Text == "question" ? "1" : "0") + "); ";

                    if (drpCategories.SelectedItem.Text == "question")
                    {
                        sql += (drpCategories.SelectedItem.Text == "question" ? "1" : "0") + ", ";
                        sql += "'" + drpUsers.SelectedItem.Text.Replace("'", "''") + "' ); ";

                    }
                    else
                    {
                        string strquesto = "";
                        sql += (drpCategories.SelectedItem.Text == "question" ? "1" : "0") + ", ";
                        sql += "'" + strquesto.Replace("'", "''") + "' ); ";

                        ////sql += (chkQuestion.Checked ? "1" : "0") + ", ";
                        ////sql += "'" + strquesto.Replace("'", "''") + "' ); ";
                    }


                    sql += " SET @CommentId = SCOPE_IDENTITY(); ";
                    sql += " insert into read_comments([tc_id],[username]) ";
                    sql += " values(@CommentId,'" + Session["admin"] + "'); ";
                    sql += " insert into read_task([task_id],[username]) ";
                    sql += " values(@TaskId,'" + Session["admin"] + "'); ";
                }
                sql += "select @TaskId;";

                object objResult = DatabaseHelper.executeScalar(sql);


                if (objResult.ToString() != "0")
                {
                    // add subscribers for the task

                    try
                    {
                        string sqlUpdate = "";
                        sqlUpdate = "Update tasks set Internalref = '" + objResult.ToString() + "' where task_id=" + LblRefTaskId.Text + "";
                        int intUpdateResult = DatabaseHelper.executeNonQuery(sqlUpdate);
                    }
                    catch { }


                    add_subscription(objResult);

                    if (reqid != 0)
                    {
                        add_taskAttachments(objResult);
                    }
                    // assign Internal reference number (task id) for change request
                    //object objIRN = DatabaseHelper.updateInternalReference(reqid, objResult.ToString());

                    emailSubject = "Task ID:" + objResult.ToString() + " was added - " + txtShortDescr.Text.Trim() + " (Task ID:" + objResult.ToString() + ")";

                    task_action = ((string)DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + Session["admin"].ToString() + "'")) + " has added task ";

                    sendNotification(objResult.ToString());

                    Session["message"] = "Task was created.";

                    Response.Redirect("edit_task.aspx?id=" + objResult.ToString());
                }
                else
                {
                    divMessage.InnerHtml = "Task was not created.";
                }
            }
            else
            {
                sql = " update tasks set ";
                sql += " [short_desc]='" + txtShortDescr.Text.Trim().Replace("'", "''") + "',";
                sql += " [status]='" + drpStatuses.SelectedItem.Text.Replace("'", "''") + "',";
                sql += " [priority]='" + drpPriorities.SelectedItem.Text.Replace("'", "''") + "',";
                sql += " [category]='" + drpCategories.SelectedItem.Text.Replace("'", "''") + "',";
                sql += " [project]='" + drpProjects.SelectedItem.Text.Replace("'", "''") + "',";
                sql += " [project_type]='" + drpProjectType.SelectedItem.Text.Replace("'", "''") + "',";
                sql += " [Relevant_URL]='" + txtRelevantURL.Text.Trim().Replace("'", "''") + "',";
                sql += " [assigned_to_user]='" + drpUsers.SelectedItem.Text.Replace("'", "''") + "',";
                sql += " [last_updated_user]='" + Session["admin"].ToString().Replace("'", "''") + "',";
                sql += " [last_updated_date]=getdate(),";
                sql += " [ETC]=" + etcMins.ToString() + ",";
                sql += " [Company]= '" + hdnProjectCompany.Value.ToString() + "'";
                sql += " where task_id = " + LblRefTaskId.Text.Trim() + "; ";
                //=======================================
                //if (ftbComment.Text.Trim() != "")
                  if (ftbComment.Content.Trim() != "")
                //=======================================
                {
                    sql += "DECLARE @CommentId INT;";
                    sql += " insert into task_comments([task_id],[username],[post_date],[comment],[deleted],[qflag],[QuesTo]) ";
                    sql += " values(" + LblRefTaskId.Text.Trim() + ",";
                    sql += "'" + Session["admin"].ToString().Replace("'", "''") + "',";
                    sql += "getdate(),";
                    //=======================================
                    //sql += "'" + ftbComment.Text.Trim().Replace("'", "''") + "',";
                      sql += "'" + ftbComment.Content.Trim().Replace("'", "''") + "',";
                    //=======================================
                    sql += "0,";
                    ///sql += (chkQuestion.Checked ? "1" : "0") + "); ";
                    if (chkQuestion.Checked == true)
                    {
                        sql += (chkQuestion.Checked ? "1" : "0") + ", ";
                        sql += "'" + drpUsers.SelectedValue.Replace("'", "''") + "' ); ";

                    }
                    else
                    {
                        string strquesto = "";
                        sql += (chkQuestion.Checked ? "1" : "0") + ", ";
                        sql += "'" + strquesto.Replace("'", "''") + "' ); ";
                    }
                    sql += " SET @CommentId = SCOPE_IDENTITY(); ";
                    sql += " insert into read_comments([tc_id],[username]) ";
                    sql += " values(@CommentId,'" + Session["admin"] + "')";
                    sql += " insert into read_task([task_id],[username]) ";
                    sql += " values(" + LblRefTaskId.Text.Trim() + ",'" + Session["admin"] + "'); ";
                }

                sql += record_changes();

                int intResult = DatabaseHelper.executeNonQuery(sql);

                if (intResult != 0)
                {
                    if ((prevStatus.Value != drpStatuses.SelectedValue) && (drpStatuses.SelectedValue == "CWO"))
                    {
                        sql = @"select count(*) from hours_reporting 
                        where task_id = " + LblRefTaskId.Text.Trim() + " and finished_date is null and started_date > convert(datetime,'" + DateTime.Today.ToString() + "',103)";

                        object count = DatabaseHelper.executeScalar(sql);

                        if (count.ToString() == "0")
                        {
                            sql = "insert into hours_reporting ([task_id],[started_date],[username]) ";
                            sql += "values(" + LblRefTaskId.Text.Trim() + ",getdate(),'" + Session["admin"].ToString().Replace("'", "''") + "')";

                            intResult = DatabaseHelper.executeNonQuery(sql);
                        }
                    }
                    else if ((prevStatus.Value == "CWO") && (drpStatuses.SelectedValue != "CWO"))
                    {
                        sql = @"update hours_reporting set finished_date = getdate(), minutes = datediff(minute, started_date, getdate()) 
			                where task_id = " + LblRefTaskId.Text.Trim() + "  and finished_date is null";

                        intResult = DatabaseHelper.executeNonQuery(sql);
                    }


                    if (lblAssignedTo.Text.Trim() != drpUsers.SelectedItem.Text.Trim())
                    {
                        object objResult = DatabaseHelper.executeScalar("select count(*) from task_subscriptions where task_id=" + LblRefTaskId.Text.Trim() + " and username='" + drpUsers.SelectedItem.Text.Trim().Replace("'", "''") + "'");

                        if (objResult.ToString() == "0")
                        {
                            sql = "insert into task_subscriptions ([task_id],[username]) ";
                            sql += "values(" + LblRefTaskId.Text.Trim() + ",'" + drpUsers.Text.Trim().Replace("'", "''") + "')";

                            intResult = DatabaseHelper.executeNonQuery(sql);
                        }
                    }

                    if (btnUpdate.Text == "Mark as undeleted")
                    {
                        intResult = DatabaseHelper.executeNonQuery("update tasks set deleted = 0 where task_id = " + lblTaskId.Text.Trim());

                        sql = @" insert into task_updates([task_id],[username],[post_date],[comment]) 
                            values(" + LblRefTaskId.Text.Trim() + ",'" + Session["admin"].ToString().Replace("'", "''") + "',getdate(),'Restored deleted task.'); ";

                        intResult = DatabaseHelper.executeNonQuery(sql);

                        task_action = ((string)DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + Session["admin"].ToString() + "'")) + " has restored deleted task ";

                        if (chkQuestion.Checked == true)
                        {
                            emailSubject = "Task ID:" + LblRefTaskId.Text.Trim() + " was restored - Question asked - please respond. (Task ID:" + LblRefTaskId.Text.Trim() + ")";
                        }
                        else
                        {
                            emailSubject = "Task ID:" + LblRefTaskId.Text.Trim() + " was restored - " + txtShortDescr.Text.Trim() + " (Task ID:" + LblRefTaskId.Text.Trim() + ")";
                        }
                    }
                    else
                    {
                        task_action = ((string)DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + Session["admin"].ToString() + "'")) + " has updated task ";

                        if (chkQuestion.Checked == true)
                        {
                            emailSubject = "Task ID:" + LblRefTaskId.Text.Trim() + " was updated - Question asked - please respond. (Task ID:" + lblTaskId.Text.Trim() + ")";
                        }
                        else
                        {
                            emailSubject = "Task ID:" + LblRefTaskId.Text.Trim() + " was updated - " + txtShortDescr.Text.Trim() + " (Task ID:" + lblTaskId.Text.Trim() + ")";
                        }
                    }

                    sendNotification(LblRefTaskId.Text.Trim());

                    if (lblAssignedTo.Text.Trim() != drpUsers.SelectedItem.Text.Trim())
                    {
                        if (!DatabaseHelper.isAdmin(lblAssignedTo.Text.Trim()))
                        {
                            intResult = DatabaseHelper.executeNonQuery("delete from task_subscriptions where task_id=" + LblRefTaskId.Text.Trim() + " and username='" + lblAssignedTo.Text.Trim().Replace("'", "''") + "'");
                        }
                    }

                    try
                    {

                        string sqlinsert = "insert into read_task([task_id],[username]) ";
                        sqlinsert += " values(" + LblRefTaskId.Text.Trim() + ",'" + Session["admin"].ToString() + "')";

                        int intCommentResult = DatabaseHelper.executeNonQuery(sqlinsert);
                    }
                    catch { }


                    try
                    {

                        sql = @"select *  from task_comments where task_id =" + LblRefTaskId.Text + " and  tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "')";
                        /// sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

                        DataSet ds = DatabaseHelper.getDataset(sql);

                        if (ds != null && ds.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                try
                                {


                                    string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                                    sqlinsert += " values(" + ds.Tables[0].Rows[i]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                                    int intCommentResult = DatabaseHelper.executeNonQuery(sqlinsert);

                                }
                                catch
                                {
                                }
                            }



                        }
                    }
                    catch { }


                    Session["message"] = "Task was updated.";

                    Response.Redirect("edit_task.aspx?id=" + LblRefTaskId.Text.Trim());

                    //BindData();
                    //ftbComment.Text = "";
                    //divMessage1.InnerHtml = "Task was updated.";                
                }
                else
                {
                    divMessage1.InnerHtml = "Task was not updated.";
                }
            }
       /// }
    }

    private string record_changes()
    {
        string base_sql = @" insert into task_updates([task_id],[username],[post_date],[comment]) 
                            values($task_id,'$us',getdate(),'$comment'); ";

        base_sql = base_sql.Replace("$task_id", LblRefTaskId.Text.Trim());
        base_sql = base_sql.Replace("$us", Session["admin"].ToString().Replace("'", "''"));

        string strSql = "";

        if (prevRelevantUrl.Value != txtRelevantURL.Text.Trim())
        {
            strSql += base_sql.Replace(
               "$comment",
               "changed Relevant URL  from \""
               + prevRelevantUrl.Value.Replace("'", "''") + "\" to \""
               + txtRelevantURL.Text.Trim().Replace("'", "''") + "\"");

            //prevRelevantUrl.Value = txtRelevantURL.Text.Trim();
        }

        if (prevShortDescr.Value != txtShortDescr.Text.Trim())
        {
            strSql += base_sql.Replace(
                "$comment",
                "changed desc from \""
                + prevShortDescr.Value.Replace("'", "''") + "\" to \""
                + txtShortDescr.Text.Trim().Replace("'", "''") + "\"");

            //prevShortDescr.Value = txtShortDescr.Text.Trim();
        }

        if (etcMins.ToString() != prevETC.Value)
        {
            try
            {
                int prevMins = Convert.ToInt32(prevETC.Value);

                strSql += base_sql.Replace(
                   "$comment",
                   "changed estimated time to completion from \""
                   + (prevMins / 60) + " hrs " + (prevMins % 60) + " mins" + "\" to \""
                   + (etcMins / 60) + " hrs " + (etcMins % 60) + " mins" + "\"");

            }
            catch { }
            //prevProjectType.Value = drpProjectType.SelectedItem.Value;	    
        }

        if (drpProjectType.SelectedValue != prevProjectType.Value)
        {
            strSql += base_sql.Replace(
               "$comment",
               "changed project type from \""
               + prevProjectType.Value.Replace("'", "''") + "\" to \""
               + drpProjectType.SelectedItem.Text.Replace("'", "''") + "\"");

            //prevProjectType.Value = drpProjectType.SelectedItem.Value;	    
        }

        if (drpProjects.SelectedValue != prevProject.Value)
        {
            strSql += base_sql.Replace(
                "$comment",
                "changed project from \""
                + prevProject.Value.Replace("'", "''") + "\" to \""
                + drpProjects.SelectedItem.Text.Replace("'", "''") + "\"");

            //prevProject.Value = drpProjects.SelectedItem.Value;
        }

        if (prevCategory.Value != drpCategories.SelectedValue)
        {
            strSql += base_sql.Replace(
                "$comment",
                "changed category from \""
                + prevCategory.Value.Replace("'", "''") + "\" to \""
                + drpCategories.SelectedItem.Text.Replace("'", "''") + "\"");

            //prev_category.Value = drpCategories.SelectedValue;
        }

        if (prevPriority.Value != drpPriorities.SelectedValue)
        {

            strSql += base_sql.Replace(
                "$comment",
                "changed priority from \""
                + prevPriority.Value.Replace("'", "''") + "\" to \""
                + drpPriorities.SelectedItem.Text.Replace("'", "''") + "\"");

            //prevPriority.Value = drpPriorities.SelectedValue;
        }

        if (prevStatus.Value != drpStatuses.SelectedValue)
        {
            strSql += base_sql.Replace(
                "$comment",
                "changed status from \""
                + prevStatus.Value.Replace("'", "''") + "\" to \""
                + drpStatuses.SelectedItem.Text.Replace("'", "''") + "\"");

            //prevStatus.Value = drpStatuses.SelectedValue;
        }

        if (prevAssignedTo.Value != drpUsers.SelectedValue)
        {
            strSql += base_sql.Replace(
                "$comment",
                "changed assigned_to from \""
                + prevAssignedTo.Value.Replace("'", "''") + "\" to \""
                + drpUsers.SelectedItem.Text.Replace("'", "''") + "\"");

            //prevAssignedTo.Value = drpUsers.SelectedValue;		    
        }
        return strSql;
    }

    void sendNotification(string strTaskId)
    {
        sql = "select email, EmailNotification,PR_EmailNotification ";
        sql += " from users, task_subscriptions ";
        sql += " where users.username = task_subscriptions.username ";
        sql += " and task_subscriptions.task_id =" + strTaskId.ToString() + " and task_subscriptions.username <> '" + Session["admin"].ToString() + "'";

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            string strHtml = generateAdminEmail(strTaskId);

            string toEmails = "";
           

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (ds.Tables[0].Rows[i]["EmailNotification"].ToString() == "Y")
                {
                   
                    string reqPR = "";
                    string[] strPR = ds.Tables[0].Rows[i]["PR_EmailNotification"].ToString().Split(new char[] { ';' });
                    for (int j = 0; j < strPR.Length - 1; j++)
                 {
                     reqPR = strPR[j];
                     if (reqPR == "All")
                        {
                            toEmails += ds.Tables[0].Rows[i]["email"].ToString() + ";";
                        }
                        else if (reqPR == drpPriorities.SelectedValue)
                     {
                         toEmails += ds.Tables[0].Rows[i]["email"].ToString() + ";";
                     }

                 }

                   
                }
                else if ((ds.Tables[0].Rows[i]["EmailNotification"].ToString() == "N") && (flagEmailNotification == true))
                {
                   /// toEmails += ds.Tables[0].Rows[i]["email"].ToString() + ";";

                    string reqPR = "";
                    string[] strPR = ds.Tables[0].Rows[i]["PR_EmailNotification"].ToString().Split(new char[] { ';' });
                    for (int j = 0; j < strPR.Length - 1; j++)
                    {
                        reqPR = strPR[j];
                        if (reqPR == "All")
                        {
                            toEmails += ds.Tables[0].Rows[i]["email"].ToString() + ";";
                        }
                        else if (reqPR == drpPriorities.SelectedValue)
                        {
                            toEmails += ds.Tables[0].Rows[i]["email"].ToString() + ";";
                        }

                    }

                }              

            }

            toEmails = toEmails.TrimEnd(';');

            if (emailSubject == "")
            {
                emailSubject = "Task ID:" + strTaskId.ToString() + " was updated - " + txtShortDescr.Text.Trim() + " (Task ID:" + strTaskId.ToString() + ")";
            }
            if (chkEmailnotify.Checked)
            {
                bool flag = DatabaseHelper.sendEmailTasks(toEmails, emailSubject, strHtml);
            }
            else
            {
            }
        }
    }

    private string record_changes_for_email()
    {
        string strSql = "";

        if (drpProjects.SelectedValue != prevProject.Value)
        {
            strSql += "Project has been changed from \""
                + prevProject.Value.Replace("'", "''") + "\" to \""
                + drpProjects.SelectedItem.Text.Replace("'", "''") + "\"<br>";

            //prevProject.Value = drpProjects.SelectedItem.Value;
        }

        if (prevAssignedTo.Value != drpUsers.SelectedValue)
        {
            strSql += "Assigned_to has been changed from \""
                + prevAssignedTo.Value.Replace("'", "''") + "\" to \""
                + drpUsers.SelectedItem.Text.Replace("'", "''") + "\"<br>";

            //prevAssignedTo.Value = drpUsers.SelectedValue;		    
        }

        if (prevStatus.Value != drpStatuses.SelectedValue)
        {
            strSql += "Status has been changed from \""
                + prevStatus.Value.Replace("'", "''") + "\" to \""
                + drpStatuses.SelectedItem.Text.Replace("'", "''") + "\"<br>";

            //prevStatus.Value = drpStatuses.SelectedValue;
        }

        if (prevPriority.Value != drpPriorities.SelectedValue)
        {
            strSql += "Priority has been changed from \""
                + prevPriority.Value.Replace("'", "''") + "\" to \""
                + drpPriorities.SelectedItem.Text.Replace("'", "''") + "\"<br>";

            //prevPriority.Value = drpPriorities.SelectedValue;
        }

        if (prevCategory.Value != drpCategories.SelectedValue)
        {
            strSql += "Category has been changed from \""
                + prevCategory.Value.Replace("'", "''") + "\" to \""
                + drpCategories.SelectedItem.Text.Replace("'", "''") + "\"<br>";

            //prev_category.Value = drpCategories.SelectedValue;
        }

        if (drpProjectType.SelectedValue != prevProjectType.Value)
        {
            strSql += "Project type has been changed from \""
                + prevProjectType.Value.Replace("'", "''") + "\" to \""
               + drpProjectType.SelectedItem.Text.Replace("'", "''") + "\"<br>";

            //prevProjectType.Value = drpProjectType.SelectedItem.Value;	    
        }

        if (etcMins.ToString() != prevETC.Value)
        {
            try
            {
                int prevMins = Convert.ToInt32(prevETC.Value);

                strSql += "Estimated time to completion has been changed from \""
                   + (prevMins / 60) + " hrs " + (prevMins % 60) + " mins" + "\" to \""
                   + (etcMins / 60) + " hrs " + (etcMins % 60) + " mins" + "\"";
            }
            catch { }

            //prevProjectType.Value = drpProjectType.SelectedItem.Value;	    
        }

        if (prevRelevantUrl.Value != txtRelevantURL.Text.Trim())
        {
            strSql += "Relevant URL has been changed from  \""
                + prevRelevantUrl.Value.Replace("'", "''") + "\" to \""
               + txtRelevantURL.Text.Trim().Replace("'", "''") + "\"<br>";

            //prevRelevantUrl.Value = txtRelevantURL.Text.Trim();
        }

        if (prevShortDescr.Value != txtShortDescr.Text.Trim())
        {
            strSql += "Desc has been changed from \""
                + prevShortDescr.Value.Replace("'", "''") + "\" to \""
                + txtShortDescr.Text.Trim().Replace("'", "''") + "\"<br>";

            //prevShortDescr.Value = txtShortDescr.Text.Trim();
        }


        //=================================================================
        //if (ftbComment.Text.Trim() != "" && LblRefTaskId.Text != "New")
        if (ftbComment.Content.Trim() != "" && LblRefTaskId.Text != "New")
        //=================================================================
        {
            if (chkQuestion.Checked == true)
            {
                strSql += "Question has been added.<br>";
            }
            else
            {
                strSql += "Comment has been added.<br>";
            }

            // flagEmailNotification is set to verify the email notification setting according to user account settings.
            flagEmailNotification = true;
        }
        return strSql;
    }

    private string generateAdminEmail(string strTaskId)
    {
        DataSet dsTask = DatabaseHelper.getDataset("select * from tasks where task_id=" + strTaskId);
        string strBody = "";
        if (dsTask == null || dsTask.Tables.Count <= 0 || dsTask.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "Task not found.<br><br><a href='tasks.aspx'>View tasks</a>";
        }
        else
        {
            strBody += "Task: <span style=\"color:red\">";
            
            strBody += task_action;

            strBody += " (<a style=\"color:red\" href='"  + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_task.aspx?id=" + strTaskId + "'>" + strTaskId + "</a>) </span> " + DateTime.Parse(dsTask.Tables[0].Rows[0]["last_updated_date"].ToString()).ToString("dd MMM h:mm tt") + "<br><br>";

            if (lblTaskId.Text != "New")
            {
                string attribures = record_changes_for_email();

                if (attribures.ToString().Trim() != "")
                {
                    strBody += "The attributes below have been amended.<br>";
                    strBody += "<span style=\"color:red\">" + attribures + "</span><br>";
                }
            }

            strBody += "<a href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_task.aspx?id=" + strTaskId + "'>" + txtShortDescr.Text.Trim() + "</a>&nbsp;&nbsp;&nbsp;(" + dsTask.Tables[0].Rows[0]["priority"].ToString() + " : " + dsTask.Tables[0].Rows[0]["status"].ToString() + ")<br><br>";
            strBody += "Emp: " + ((string)DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + dsTask.Tables[0].Rows[0]["assigned_to_user"].ToString() + "'")) + "<br>";
            strBody += "Project: " + dsTask.Tables[0].Rows[0]["project"].ToString() + "<br>";
            strBody += "Relevant URL: <a href='" + dsTask.Tables[0].Rows[0]["Relevant_URL"].ToString() + "' target='_blank'>" + dsTask.Tables[0].Rows[0]["Relevant_URL"].ToString() + "</a><br>";
            strBody += "<br><br>";

            sql = "Select * from task_comments where task_id=" + strTaskId.ToString() + " and deleted <> 1 order by tc_id desc";

            DataSet dsTaskDetails = DatabaseHelper.getDataset(sql);

            strBody += "<table cellspacing=\"0\" border=\"1\" style=\"width: 99%;border-collapse: collapse;\">";
            strBody += "<tbody>";

            for (int i = 0; i < dsTaskDetails.Tables[0].Rows.Count; i++)
            {
                strBody += "<tr>";
                strBody += "<td>";
                strBody += "<div style=\"border: 1px none Green;\">";
                strBody += "<table width=\"100%\">";
                strBody += "<tbody>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                if (dsTaskDetails.Tables[0].Rows[i]["qflag"].ToString() == "1")
                {
                    strBody += "<span style=\"color:red;\">Question posted by " + dsTaskDetails.Tables[0].Rows[i]["username"].ToString() + " on " + DateTime.Parse(dsTaskDetails.Tables[0].Rows[i]["post_date"].ToString()).ToString("dd MMM h:mm tt") + "</span>";
                }
                else
                {
                    strBody += "<span style=\"color: Green;\">comment " + dsTaskDetails.Tables[0].Rows[i]["tc_id"].ToString() + " posted by " + dsTaskDetails.Tables[0].Rows[i]["username"].ToString() + " on " + DateTime.Parse(dsTaskDetails.Tables[0].Rows[i]["post_date"].ToString()).ToString("dd MMM h:mm tt") + "</span>";
                }
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\"> </td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span>" + dsTaskDetails.Tables[0].Rows[i]["comment"].ToString() + "</span>";
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "</tbody>";
                strBody += "</table>";
                strBody += "</div>";
                strBody += "</td>";
                strBody += "</tr>";
            }
            strBody += "</tbody>";
            strBody += "</table>";
        }
        return strBody;
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {

        if (btnDelete.Text == "Delete permanently")
        {
            task_action = ((string)DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + Session["admin"].ToString() + "'")) + " has permanently deleted task ";

            emailSubject = "Task ID:" + lblTaskId.Text.Trim() + " was permanently deleted  - " + txtShortDescr.Text.Trim() + " (Task ID:" + lblTaskId.Text.Trim() + ")";

            string strHtml = generateAdminEmail(lblTaskId.Text.Trim());
            string toEmails = "";

            sql = "select email,PR_EmailNotification ";
            sql += " from users, task_subscriptions ";
            sql += " where users.username = task_subscriptions.username ";
            sql += " and task_subscriptions.task_id =" + lblTaskId.Text.Trim() + " and task_subscriptions.username <> '" + Session["admin"].ToString() + "'";

            DataSet ds = DatabaseHelper.getDataset(sql);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (toEmails == "")
                    {
                        //toEmails = ds.Tables[0].Rows[i]["email"].ToString();
                        string reqPR = "";
                        string[] strPR = ds.Tables[0].Rows[i]["PR_EmailNotification"].ToString().Split(new char[] { ';' });
                        for (int j = 0; j < strPR.Length - 1; j++)
                        {
                            reqPR = strPR[j];
                            if (reqPR == "All")
                            {
                                toEmails = ds.Tables[0].Rows[i]["email"].ToString() + ";";
                            }
                            else if (reqPR == drpPriorities.SelectedValue)
                            {
                                toEmails = ds.Tables[0].Rows[i]["email"].ToString() + ";";
                            }

                        }

                    }
                    else
                    {

                        string reqPR = "";
                        string[] strPR = ds.Tables[0].Rows[i]["PR_EmailNotification"].ToString().Split(new char[] { ';' });
                        for (int j = 0; j < strPR.Length - 1; j++)
                        {
                            reqPR = strPR[j];
                            if (reqPR == "All")
                            {
                                toEmails += ds.Tables[0].Rows[i]["email"].ToString() + ";";
                            }
                            else if (reqPR == drpPriorities.SelectedValue)
                            {
                                toEmails += ds.Tables[0].Rows[i]["email"].ToString() + ";";
                            }

                        }
                        //toEmails += ";" + ds.Tables[0].Rows[i]["email"].ToString();
                    }
                }
            }

            sql = "delete from read_comments where tc_id in (select tc_id from task_comments where task_id ="+ lblTaskId.Text+"); ";
            sql += "delete from hours_reporting where task_id =" + lblTaskId.Text + "; ";
            sql += "delete from task_updates where task_id =" + lblTaskId.Text + "; ";
            sql += "delete from task_subscriptions where task_id =" + lblTaskId.Text + "; ";
            sql += "delete from task_comments where task_id =" + lblTaskId.Text + "; ";
            sql += "delete from task_attachments where task_id =" + lblTaskId.Text + "; ";
            sql += "delete from tasks where task_id =" + lblTaskId.Text + "; ";
            sql += "delete from read_task where task_id =" + lblTaskId.Text + "; ";

            int intResult = DatabaseHelper.executeNonQuery(sql);

            if (intResult != 0)
            {
                bool flag = DatabaseHelper.sendEmailTasks(toEmails, emailSubject, strHtml);

                try
                {

                    sql = @"select *  from task_comments where task_id =" + lblTaskId.Text + " and  tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "')";
                    /// sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

                    DataSet dscomment = DatabaseHelper.getDataset(sql);

                    if (dscomment != null && dscomment.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < dscomment.Tables[0].Rows.Count; i++)
                        {
                            try
                            {


                                string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                                sqlinsert += " values(" + dscomment.Tables[0].Rows[i]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                                int intCommentResult = DatabaseHelper.executeNonQuery(sqlinsert);

                            }
                            catch
                            {
                            }
                        }



                    }
                }
                catch { }




                Response.Redirect("tasks.aspx");
            }
            else
            {
                divMessage1.InnerHtml = "Task " + lblTaskId.Text + "  was not deleted permanently.";
                divMessage1.Visible = true;
            }
        }
        else
        {
            sql = "update tasks set deleted=1 where task_id = " + lblTaskId.Text + "; ";
            //sql += " Update task_comments set deleted=1 where task_id=" + lblTaskId.Text;

            int intResult = DatabaseHelper.executeNonQuery(sql);

            if (intResult != 0)
            {
                task_action = ((string)DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + Session["admin"].ToString() + "'")) + " has deleted task ";

                emailSubject = "Task ID:" + lblTaskId.Text.Trim() + " was deleted - " + txtShortDescr.Text.Trim() + " (Task ID:" + lblTaskId.Text.Trim() + ")";

                sendNotification(lblTaskId.Text.Trim());

                sql = @" insert into task_updates([task_id],[username],[post_date],[comment]) 
                            values(" + lblTaskId.Text.Trim() +",'"+ Session["admin"].ToString().Replace("'", "''") +"',getdate(),'Task was deleted.'); ";
                
                intResult = DatabaseHelper.executeNonQuery(sql);



                try
                {

                    sql = @"select *  from task_comments where task_id =" + lblTaskId.Text + " and  tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "')";
                    /// sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

                    DataSet dscomment = DatabaseHelper.getDataset(sql);

                    if (dscomment != null && dscomment.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < dscomment.Tables[0].Rows.Count; i++)
                        {
                            try
                            {


                                string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                                sqlinsert += " values(" + dscomment.Tables[0].Rows[i]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                                int intCommentResult = DatabaseHelper.executeNonQuery(sqlinsert);

                            }
                            catch
                            {
                            }
                        }



                    }
                }
                catch { }



                Response.Redirect("tasks.aspx");
            }
            else
            {
                divMessage1.InnerHtml = "Task " + lblTaskId.Text + "  was not deleted.";
                divMessage1.Visible = true;
            }
        }
    }

    protected void DataGrid1_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "edit")
        {
            Response.Redirect("edit_comment.aspx?id=" + e.Item.Cells[0].Text + "&tcid=" + e.Item.Cells[1].Text);
        }
        else if (e.CommandName == "delete")
        {
            sql = "Update task_comments set deleted=1 where tc_id=" + e.Item.Cells[1].Text;

            int intResult = DatabaseHelper.executeNonQuery(sql);

            if (intResult != 0)
            {
                string base_sql = @" insert into task_updates([task_id],[username],[post_date],[comment]) 
                            values(" + lblTaskId.Text.Trim() + ",'" + Session["admin"].ToString().Replace("'", "''") + "',getdate(),'Comment " + e.Item.Cells[1].Text + "  has been deleted.'); ";

                intResult = DatabaseHelper.executeNonQuery(base_sql);

                divMessage1.InnerHtml = "Comment " + e.Item.Cells[1].Text + "  was deleted.";

                divMessage1.Visible = true;
                //=================================================================
                //ftbComment.Text = "";
                  ftbComment.Content = "";
                //=================================================================
                BindData();
            }
            else
            {
                divMessage1.InnerHtml = "Comment " + e.Item.Cells[1].Text + "  was not deleted.";
                divMessage1.Visible = true;
            }
        }
        else if (e.CommandName == "ReadMark")
        {
            LinkButton readMarkButton = (LinkButton)e.Item.FindControl("lnkReadMark");

            if (readMarkButton.Text == "[<span style='color:red'>Unread.</span>  Mark as read]")
            {
                sql = @"insert into read_comments([tc_id],[username]) 
                        values("+ e.Item.Cells[1].Text +",'"+ Session["admin"].ToString() +"')";
                DatabaseHelper.executeNonQuery(sql);
                BindData();
            }
            else
            {
                sql = @"delete from read_comments where [tc_id]=" + e.Item.Cells[1].Text + " and [username]='" + Session["admin"] + "'";
                DatabaseHelper.executeNonQuery(sql);
                BindData();
            }
        }
    }

    protected void DataGrid1_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemIndex != -1)
        {
            Label lblPosted = (Label)e.Item.FindControl("lblPosted");
            LinkButton readMarkButton = (LinkButton)e.Item.FindControl("lnkReadMark");

        
            if (e.Item.Cells[6].Text == "1")
            {
                lblPosted.Text = "Question posted by " + e.Item.Cells[2].Text + " for  " + e.Item.Cells[7].Text + " on " + DateTime.Parse(e.Item.Cells[3].Text).ToString("dd MMM h:mm tt");
                lblPosted.ForeColor = System.Drawing.Color.Red;
                readMarkButton.Visible = false;
            }
            else
            {
                lblPosted.Text = "comment " + e.Item.Cells[1].Text + " posted by " + e.Item.Cells[2].Text + " on " + DateTime.Parse(e.Item.Cells[3].Text).ToString("dd MMM h:mm tt");

                sql = "select count(*) from read_comments where tc_id=" + e.Item.Cells[1].Text + " and username='" + Session["admin"] +"'";

                object objIsRead = DatabaseHelper.executeScalar(sql);
                if (objIsRead.ToString() == "0")
                {
                    lblPosted.ForeColor = System.Drawing.Color.Red;
                    readMarkButton.Text = "[<span style='color:red'>Unread.</span>  Mark as read]";
                }
                else
                {
                    lblPosted.ForeColor = System.Drawing.Color.Green;
                    readMarkButton.Text = "[<span style='color:green'>Read.</span> Mark as unread]";
                }
                readMarkButton.Visible = true;
            }

            Button deleteButton = (Button)e.Item.FindControl("lnkDelete");
            Button editButton = (Button)e.Item.FindControl("lnkEdit");

            //We can now add the onclick event handler
            deleteButton.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete comment " + e.Item.Cells[1].Text + "?')");
            
            if (!DatabaseHelper.can_Delete_Comments_Attachments(Session["admin"].ToString()))
            {
                deleteButton.Visible = false;
                editButton.Visible = false;
            }

            if (deletedFlag == true)
            {
                deleteButton.Visible = false;
                editButton.Visible = false;
            }
           
        }
    }

    protected void DataGrid2_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "edit")
        {
            Response.Redirect("edit_comment.aspx?id=" + e.Item.Cells[0].Text + "&tcid=" + e.Item.Cells[1].Text);
        }
        else if (e.CommandName == "delete")
        {
            sql = "Update task_comments set deleted=1 where tc_id=" + e.Item.Cells[1].Text;

            int intResult = DatabaseHelper.executeNonQuery(sql);

            if (intResult != 0)
            {
                divMessage1.InnerHtml = "Comment " + e.Item.Cells[1].Text + "  was deleted.";

                divMessage1.Visible = true;
                //=================================================================
                //ftbComment.Text = "";
                  ftbComment.Content = "";
                //=================================================================
                BindData();
            }
            else
            {
                divMessage1.InnerHtml = "Comment " + e.Item.Cells[1].Text + "  was not deleted.";
                divMessage1.Visible = true;
            }
        }
        else if (e.CommandName == "ReadMark")
        {
            LinkButton readMarkButton = (LinkButton)e.Item.FindControl("lnkReadMark");

            if (readMarkButton.Text == "[<span style='color:red'>Unread.</span>  Mark as read]")
            {
                sql = @"insert into read_comments([tc_id],[username]) 
                        values(" + e.Item.Cells[1].Text + ",'" + Session["admin"] + "')";
                DatabaseHelper.executeNonQuery(sql);
                BindData();
            }
            else
            {
                sql = @"delete from read_comments where [tc_id]=" + e.Item.Cells[1].Text + " and [username]='" + Session["admin"] + "'";
                DatabaseHelper.executeNonQuery(sql);
                BindData();
            }
        }
    }

    protected void DataGrid2_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemIndex != -1)
        {
            Label lblPosted = (Label)e.Item.FindControl("lblPosted");
            LinkButton readMarkButton = (LinkButton)e.Item.FindControl("lnkReadMark");
                       
            
            if (e.Item.Cells[7].Text == "C")
            {
                if (e.Item.Cells[8].Text == "1")
                {
                    lblPosted.Text = "Question posted by " + e.Item.Cells[2].Text + " on " + DateTime.Parse(e.Item.Cells[3].Text).ToString("dd MMM h:mm tt");
                    lblPosted.ForeColor = System.Drawing.Color.Red;
                    readMarkButton.Visible = false;
                }
                else
                {
                    lblPosted.Text = "comment " + e.Item.Cells[1].Text + " posted by " + e.Item.Cells[2].Text + " on " + DateTime.Parse(e.Item.Cells[3].Text).ToString("dd MMM h:mm tt");
                    
                    sql = "select count(*) from read_comments where tc_id=" + e.Item.Cells[1].Text + " and username='" + Session["admin"] + "'";

                    object objIsRead = DatabaseHelper.executeScalar(sql);
                    if (objIsRead.ToString() == "0")
                    {
                        lblPosted.ForeColor = System.Drawing.Color.Red;
                        readMarkButton.Text = "[<span style='color:red'>Unread.</span>  Mark as read]";
                    }
                    else
                    {
                        lblPosted.ForeColor = System.Drawing.Color.Green;
                        readMarkButton.Text = "[<span style='color:green'>Read.</span> Mark as unread]";
                    }
                    readMarkButton.Visible = true;
                }
            }
            else
            {
                ((Label)e.Item.FindControl("lblPosted")).Text = "changed by " + e.Item.Cells[2].Text + " on " + DateTime.Parse(e.Item.Cells[3].Text).ToString("dd MMM h:mm tt");
                readMarkButton.Visible = false;
            }

            LinkButton deleteButton = (LinkButton)e.Item.FindControl("lnkDelete");
            LinkButton editButton = (LinkButton)e.Item.FindControl("lnkEdit");

            //We can now add the onclick event handler
            deleteButton.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete comment " + e.Item.Cells[1].Text + "?')");

            if (!DatabaseHelper.can_Delete_Comments_Attachments(Session["admin"].ToString()) || e.Item.Cells[7].Text == "U" || e.Item.Cells[6].Text == "1")
            {
                deleteButton.Visible = false;
                editButton.Visible = false;
            }
            if (deletedFlag == true)
            {
                deleteButton.Visible = false;
                editButton.Visible = false;
            }
        }
    }

    void load_dropdowns()
    {
        // projects
        sql = @"select project_name
		from projects
		where active = 'Y' order by project_name;";

        // categories
        sql += "\nselect category_name from categories order by sort_seq, category_name;";

        // priorities
        sql += "\nselect priority_name from priorities order by sort_seq, priority_name;";

        // statuses
        sql += "\nselect status_name from statuses order by sort_seq, status_name;";

        // users
        sql += "\nselect username from users where active = 'Y' order by username;";

        // do a batch of sql statements
        DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);

        drpProjects.DataSource = ds_dropdowns.Tables[0];
        drpProjects.DataTextField = "project_name";
        drpProjects.DataValueField = "project_name";
        drpProjects.DataBind();
        drpProjects.Items.Insert(0, new ListItem("[no project]", ""));


        drpCategories.DataSource = ds_dropdowns.Tables[1];
        drpCategories.DataTextField = "category_name";
        drpCategories.DataValueField = "category_name";
        drpCategories.DataBind();
        drpCategories.Items.Insert(0, new ListItem("[no category]", ""));

        drpPriorities.DataSource = ds_dropdowns.Tables[2];
        drpPriorities.DataTextField = "priority_name";
        drpPriorities.DataValueField = "priority_name";
        drpPriorities.DataBind();
        drpPriorities.Items.Insert(0, new ListItem("[no priority]", ""));

        drpStatuses.DataSource = ds_dropdowns.Tables[3];
        drpStatuses.DataTextField = "status_name";
        drpStatuses.DataValueField = "status_name";
        drpStatuses.DataBind();
        drpStatuses.Items.Insert(0, new ListItem("[no status]", ""));
        
        drpUsers.DataSource = ds_dropdowns.Tables[4];
        drpUsers.DataTextField = "username";
        drpUsers.DataValueField = "username";
        drpUsers.DataBind();
        drpUsers.Items.Insert(0, new ListItem("[not assigned]", ""));
    }

    void set_default_selection()
    {
        sql = "\nselect top 1 project_name from projects where default_selection = 'Y' order by project_name;"; // 0
        sql += "\nselect top 1 category_name from categories where default_selection = 'Y' order by category_name;";  // 1
        sql += "\nselect top 1 priority_name from priorities where default_selection = 'Y' order by priority_name;"; // 2
        sql += "\nselect top 1 status_name from statuses where default_selection = 'Y' order by status_name;"; // 3

        DataSet ds_defaults = DatabaseHelper.getDataset(sql);

        string default_value;

        // projects
        if (ds_defaults.Tables[0].Rows.Count > 0)
        {
            default_value = ds_defaults.Tables[0].Rows[0][0].ToString();
        }
        else
        {
            default_value = "0";
        }
        foreach (ListItem li in drpProjects.Items)
        {
            if (li.Value == default_value)
            {
                li.Selected = true;
            }
            else
            {
                li.Selected = false;
            }
        }

        // categories
        if (ds_defaults.Tables[1].Rows.Count > 0)
        {
            default_value = ds_defaults.Tables[1].Rows[0][0].ToString();
        }
        else
        {
            default_value = "0";
        }
        foreach (ListItem li in drpCategories.Items)
        {
            if (li.Value == default_value)
            {
                li.Selected = true;
            }
            else
            {
                li.Selected = false;
            }
        }
        // priorities
        if (ds_defaults.Tables[2].Rows.Count > 0)
        {
            default_value = ds_defaults.Tables[2].Rows[0][0].ToString();
        }
        else
        {
            default_value = "0";
        }
        foreach (ListItem li in drpPriorities.Items)
        {
            if (li.Value == default_value)
            {
                li.Selected = true;
            }
            else
            {
                li.Selected = false;
            }
        }

        // statuses
        if (ds_defaults.Tables[3].Rows.Count > 0)
        {
            default_value = ds_defaults.Tables[3].Rows[0][0].ToString();
        }
        else
        {
            default_value = "0";
        }
        foreach (ListItem li in drpStatuses.Items)
        {
            if (li.Value == default_value)
            {
                li.Selected = true;
            }
            else
            {
                li.Selected = false;
            }
        }
    }

    void generate_task_from_client_request()
    {
        sql = "\nSelect * from ClientRequest where RequestId=" + reqid.ToString() + ";";

        sql += "\nSelect * from ClientRequest_Details where RequestId=" + reqid.ToString() + " order by CommentId desc";

        DataSet dsClientRequest = DatabaseHelper.getDataset(sql);

        // change request
        if (dsClientRequest.Tables[0].Rows.Count > 0)
        {
            txtShortDescr.Text = dsClientRequest.Tables[0].Rows[0]["ShortDescr"].ToString();
            txtRelevantURL.Text = dsClientRequest.Tables[0].Rows[0]["WebsiteURL"].ToString();
        }

        // merge change request comments
        if (dsClientRequest.Tables[1].Rows.Count > 0)
        {
            //=================================================================
            //ftbComment.Text = "";
              ftbComment.Content = "";
            //=================================================================

            for (int i = 0; i < dsClientRequest.Tables[1].Rows.Count; i++)
            {
                //=================================================================
                //ftbComment.Text += dsClientRequest.Tables[1].Rows[i]["Comment"].ToString() + "<br/><hr/><br/>";
                ftbComment.Content += dsClientRequest.Tables[1].Rows[i]["Comment"].ToString() + "<br/><hr/><br/>";
                //=================================================================
            }
        }
    }

    void add_subscription(object objTaskid)
    {
        int intResult;

        sql = "select username from users where subscribe_to_all_tasks='Y' and username != '" + Session["admin"].ToString().Replace("'", "''") + "' ";

        if (drpUsers.SelectedIndex != 0)
        {
            sql += " and username != '" + drpUsers.SelectedValue.Replace("'", "''") + "'";
        }

        DataSet dsSubscr = DatabaseHelper.getDataset(sql);

        if (dsSubscr != null && dsSubscr.Tables.Count > 0 && dsSubscr.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < dsSubscr.Tables[0].Rows.Count; i++)
            {
                sql = "insert into task_subscriptions ([task_id],[username]) ";
                sql += "values(" + objTaskid.ToString() + ",'" + dsSubscr.Tables[0].Rows[i]["username"].ToString() + "')";

                intResult = DatabaseHelper.executeNonQuery(sql);
            }
        }

        sql = "insert into task_subscriptions ([task_id],[username]) ";
        sql += "values(" + objTaskid.ToString() + ",'" + Session["admin"].ToString().Replace("'", "''") + "'); ";

        if (drpUsers.SelectedIndex > 0 && (drpUsers.SelectedValue != Session["admin"].ToString()))
        {
            sql += " insert into task_subscriptions ([task_id],[username]) ";
            sql += "values(" + objTaskid.ToString() + ",'" + drpUsers.SelectedValue.Replace("'", "''") + "'); ";
        }

        intResult = DatabaseHelper.executeNonQuery(sql);
    }

    void add_taskAttachments(object objTaskid)
    {
        int intResult;

        sql = "select * from ClientRequest_Attachment where RequestId="+ reqid.ToString();

        DataSet dsAttachments = DatabaseHelper.getDataset(sql);

        if (dsAttachments != null && dsAttachments.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < dsAttachments.Tables[0].Rows.Count; i++)
            {
                sql = "insert into task_attachments([task_id],[Pkey],[FileName],[Description],[UploadedBy],[UploadedOn])";
                sql +="values("+ objTaskid.ToString() +",";
                sql += "'" + dsAttachments.Tables[0].Rows[i]["Pkey"].ToString() + "',";
                sql += "'" + dsAttachments.Tables[0].Rows[i]["FileName"].ToString() + "',";
                sql += "'" + dsAttachments.Tables[0].Rows[i]["Description"].ToString() + "',";
                sql += "'" + Session["admin"].ToString().Replace("'", "''") + "',";
                sql += "getdate())";

                intResult = DatabaseHelper.executeNonQuery(sql);
            }
        }
    }    






   

    void register1aTaskAllocated()
    {
        arr1aAssignedUsers.Clear();

        sql = @"select distinct assigned_to_user from tasks 
                where tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.priority = '1a - DO NOW' ";

        DataSet ds = DatabaseHelper.getDataset(sql);

        foreach (DataRow row in ds.Tables[0].Rows)
        {
            try
            {
                if ((row["assigned_to_user"] != null && row["assigned_to_user"].ToString().Trim() != ""))
                {
                    arr1aAssignedUsers.Add(row["assigned_to_user"]);
                }
            }
            catch { }
        }

        ClientScriptManager cs = Page.ClientScript;

        string str1aAssignedUsers = "";

        if (arr1aAssignedUsers.Count > 0)
        {
            for (int j = 0; j < arr1aAssignedUsers.Count; j++)
            {
                try
                {
                    str1aAssignedUsers += "'" + arr1aAssignedUsers[j] + "',";
                }
                catch { }
            }
        }

        str1aAssignedUsers = str1aAssignedUsers.TrimEnd(new char[] { ',' });

        if (cs.IsClientScriptBlockRegistered("MyArr1aAssignedUsers") == false)
        {
            cs.RegisterArrayDeclaration("MyArr1aAssignedUsers", str1aAssignedUsers);
        }
    }



  

    protected void lnkNudge_Click(object sender, EventArgs e)
    {
        string FullName = "";
        sql = "select email, firstname +' '+ lastname as name ";
        sql += " from users, tasks";
        sql += " where users.username = tasks.assigned_to_user ";
        sql += " and tasks.task_id =" + lblTaskId.Text.Trim();

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            string toEmails = "";

            toEmails = ds.Tables[0].Rows[0]["email"].ToString();
            FullName = ds.Tables[0].Rows[0]["name"].ToString();

            emailSubject = ((string)DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + Session["admin"].ToString() + "'")) + " has nudged you regarding the task " + lblTaskId.Text.Trim();

            emailSubject = emailSubject.ToUpper(); 

            task_action = ((string)DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + Session["admin"].ToString() + "'")) + " has nudged you regarding the task ";

            task_action = task_action.ToUpper();

            string strHtml = generateNudgeEmail(lblTaskId.Text.Trim());

            bool flag = DatabaseHelper.sendEmailTasks(toEmails, emailSubject, strHtml);

            Session["message"] = "A nudge was sent to " + FullName + ".";

            Response.Redirect("edit_task.aspx?id=" + lblTaskId.Text.Trim());
        }
        else
        {
            Session["message"] = "Error while sending Nudge to task owner. Please confirm owner email address have been exist in database.";

            Response.Redirect("edit_task.aspx?id=" + lblTaskId.Text.Trim());
        }
    }

    private string generateNudgeEmail(string strTaskId)
    {
        DataSet dsTask = DatabaseHelper.getDataset("select * from tasks where task_id=" + strTaskId);
        string strBody = "";
        if (dsTask == null || dsTask.Tables.Count <= 0 || dsTask.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "Task not found.<br><br><a href='tasks.aspx'>View tasks</a>";
        }
        else
        {
            strBody += "Task: <span style=\"color:red\">";

            strBody += task_action;

            strBody += " (<a style=\"color:red\" href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_task.aspx?id=" + strTaskId + "'>" + strTaskId + "</a>) </span> " + DateTime.Parse(DateTime.Now.ToString()).ToString("dd MMM h:mm tt") + "<br><br>";

            if (lblTaskId.Text != "New")
            {
                string attribures = record_changes_for_email();

                if (attribures.ToString().Trim() != "")
                {
                    strBody += "The attributes below have been amended.<br>";
                    strBody += "<span style=\"color:red\">" + attribures + "</span><br>";
                }
            }

            strBody += "<a href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_task.aspx?id=" + strTaskId + "'>" + txtShortDescr.Text.Trim() + "</a>&nbsp;&nbsp;&nbsp;(" + dsTask.Tables[0].Rows[0]["priority"].ToString() + " : " + dsTask.Tables[0].Rows[0]["status"].ToString() + ")<br><br>";
            strBody += "Emp: " + ((string)DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + dsTask.Tables[0].Rows[0]["assigned_to_user"].ToString() + "'")) + "<br>";
            strBody += "Project: " + dsTask.Tables[0].Rows[0]["project"].ToString() + "<br>";
            strBody += "Relevant URL: <a href='" + dsTask.Tables[0].Rows[0]["Relevant_URL"].ToString() + "' target='_blank'>" + dsTask.Tables[0].Rows[0]["Relevant_URL"].ToString() + "</a><br>";
            strBody += "<br><br>";

            sql = "Select * from task_comments where task_id=" + strTaskId.ToString() + " and deleted <> 1 order by tc_id desc";

            DataSet dsTaskDetails = DatabaseHelper.getDataset(sql);

            strBody += "<table cellspacing=\"0\" border=\"1\" style=\"width: 99%;border-collapse: collapse;\">";
            strBody += "<tbody>";

            for (int i = 0; i < dsTaskDetails.Tables[0].Rows.Count; i++)
            {
                strBody += "<tr>";
                strBody += "<td>";
                strBody += "<div style=\"border: 1px none Green;\">";
                strBody += "<table width=\"100%\">";
                strBody += "<tbody>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                if (dsTaskDetails.Tables[0].Rows[i]["qflag"].ToString() == "1")
                {
                    strBody += "<span style=\"color:red;\">Question posted by " + dsTaskDetails.Tables[0].Rows[i]["username"].ToString() + " on " + DateTime.Parse(dsTaskDetails.Tables[0].Rows[i]["post_date"].ToString()).ToString("dd MMM h:mm tt") + "</span>";
                }
                else
                {
                    strBody += "<span style=\"color: Green;\">comment " + dsTaskDetails.Tables[0].Rows[i]["tc_id"].ToString() + " posted by " + dsTaskDetails.Tables[0].Rows[i]["username"].ToString() + " on " + DateTime.Parse(dsTaskDetails.Tables[0].Rows[i]["post_date"].ToString()).ToString("dd MMM h:mm tt") + "</span>";
                }
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\"> </td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span>" + dsTaskDetails.Tables[0].Rows[i]["comment"].ToString() + "</span>";
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "</tbody>";
                strBody += "</table>";
                strBody += "</div>";
                strBody += "</td>";
                strBody += "</tr>";
            }
            strBody += "</tbody>";
            strBody += "</table>";
        }
        return strBody;
    }

    void  ensureCurrentlyWorkingOnStatus()
    {
        sql = "select count(*) from tasks where  deleted <> 1 and status <>'closed' and status = 'CWO' and assigned_to_user ='"+ Session["admin"] +"' and task_id <> " + id.ToString();

        object count = DatabaseHelper.executeScalar(sql);

        if (count.ToString() != "0")
        {
            hdnStatusFlag.Value = "true";
        }
        else
        {
            hdnStatusFlag.Value = "false";
        }
    }

    protected int getHoursTakenSoFar(string task_id)
    {
        sql = @"select sum(datediff(minute,started_date,finished_date))
                from hours_reporting
                where finished_date is not null and task_id=" + task_id.ToString();

        int hrsTaken = 0;
        object objHrsTaken = DatabaseHelper.executeScalar(sql);
        if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
        {
            hrsTaken += (int)objHrsTaken;
        }
        else
        {
            hrsTaken = 0;
        }

        sql = @"select sum(datediff(minute,started_date,getdate()))
                from hours_reporting
                where finished_date is null and task_id=" + task_id.ToString();

        objHrsTaken = DatabaseHelper.executeScalar(sql);

        if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
        {
            hrsTaken += (int)objHrsTaken;
        }

        return hrsTaken;
    }



    void getClientRelated()
    {


        if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
        {
            LnkRelated.Visible = false;
        }
        else
        {
            LnkRelated.Visible = true;
        }

    }


  



    protected void Admin_Header1_Load(object sender, EventArgs e)
    {

    }
    protected void LnkRelated_Click(object sender, EventArgs e)
    {
        Session["ProjectRelated"] = drpProjects.SelectedValue;
        Response.Redirect("Related.aspx", false);

    }

    protected void Lnkcomprelated_Click(object sender, EventArgs e)
    {
        Session["CompanyRelated"] = hdnProjectCompany.Value.ToString();
        Response.Redirect("CompanyRelated.aspx", false);

    }
    protected void drpProjects_SelectedIndexChanged(object sender, EventArgs e)
    {
        string sqlcompany = "select [CompanyName] from [projects]  where [project_name] = '" + drpProjects.SelectedValue.ToString() + "'";
        object objResult = DatabaseHelper.executeScalar(sqlcompany);
        string sqlRelevanturl = "select [Relevanturl] from [projects]  where [project_name] = '" + drpProjects.SelectedValue.ToString() + "'";
        object objRelevantUrl = DatabaseHelper.executeScalar(sqlRelevanturl);

        if (objResult != null)
        {
            hdnProjectCompany.Value = objResult.ToString();
        }
        if (objRelevantUrl != null)
        {
            txtRelevantURL.Text = objRelevantUrl.ToString();
        }
    }
 
    //protected void CompanyNotes_Click(object sender, EventArgs e)
    //{
    //    Session["TaskComp"] = hdnProjectCompany.Value;
    //    Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> window.open('companyNotes.aspx','','status=no,scrollbars=yes,position=center,resizable=no,scrollbars=no,maximize=yes'); </script>");
    //}
  

    //protected void CRNotes_Click(object sender, EventArgs e)
    //{
    //    Session["TaskComp"] = hdnProjectCompany.Value;
    //    Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> window.open('CRNotes.aspx','','status=no,scrollbars=yes,position=center,resizable=no,scrollbars=no,maximize=yes'); </script>");
    //}
    protected void BtnDelAllCmnt_Click(object sender, EventArgs e)
    {
       /// bool atLeastOneRowDeleted = false; 
        // Iterate through the Products.Rows property
        int i = 0;
        foreach (DataGridItem row in DataGrid1.Items) 
        { 
            // Access the CheckBox 
            try
            {
                CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                if (cb != null && cb.Checked)
                {
                    i = i + 1;
                    sql = "Update task_comments set deleted=1 where tc_id=" + row.Cells[1].Text;

                    int intResult = DatabaseHelper.executeNonQuery(sql);

                    if (intResult != 0)
                    {
                        string base_sql = @" insert into task_updates([task_id],[username],[post_date],[comment]) 
                            values(" + lblTaskId.Text.Trim() + ",'" + Session["admin"].ToString().Replace("'", "''") + "',getdate(),'Comment " + row.Cells[1].Text + "  has been deleted.'); ";

                        intResult = DatabaseHelper.executeNonQuery(base_sql);


                    }

                }
            }
            catch { }
            }
            if (i > 0)
            {
                divMessage1.InnerHtml = "All comments deleted.";
            }
            else
            {
                divMessage1.InnerHtml = "Please select comment to delete.";
            }

            BindData();
    }
    //////protected void CheckAllToDelete_CheckedChanged(object sender, EventArgs e)
    //////{
    //////    bool checkState;
    //////    if (CheckAllToDelete.Checked)
    //////    {
    //////         checkState = true;
    //////    }
    //////    else
    //////    {
    //////       checkState = false;
    //////    }
    //////    foreach (DataGridItem row in DataGrid1.Items)
    //////    {
    //////        // Access the CheckBox
    //////        CheckBox cb = (CheckBox)row.FindControl("chkSelect");
    //////        if (cb != null)
    //////          cb.Checked = checkState;
    //////    }
    //////}


    protected void btnIncluderead_Click(object sender, EventArgs e)
    {
        BindData();
    }

    protected void btnCheckAllasRead_Click(object sender, EventArgs e)
    {
        if (CheckAllasRead.Checked)
        {
            sql = @"select *  from task_comments where task_id =" + lblTaskId.Text + " and  tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "')";
            /// sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

            DataSet ds = DatabaseHelper.getDataset(sql);

            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    try
                    {


                        string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                        sqlinsert += " values(" + ds.Tables[0].Rows[i]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                        int intResult = DatabaseHelper.executeNonQuery(sqlinsert);

                    }
                    catch
                    {
                    }
                }

                BindData();

            }
        }
    }
    protected void btnCheckAllToDelete_Click(object sender, EventArgs e)
    {
        bool checkState;
        if (CheckAllToDelete.Checked)
        {
            checkState = true;
        }
        else
        {
            checkState = false;
        }
        foreach (DataGridItem row in DataGrid1.Items)
        {
            // Access the CheckBox
            CheckBox cb = (CheckBox)row.FindControl("chkSelect");
            if (cb != null)
                cb.Checked = checkState;
        }
    }
    protected void BtnAnalytics_Click(object sender, EventArgs e)
    {
      ///  Session["TaskComp"] = TaskProject.Value;
        sql = @"select *  from projects where project_name ='" + TaskProject.Value + "'";
           
            DataSet ds = DatabaseHelper.getDataset(sql);

            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["SEO"].ToString() == "True")
                {

                    Session["GoogleUser"] = ds.Tables[0].Rows[0]["GoogleUserId"].ToString();
                    Session["GooglePwd"] = ds.Tables[0].Rows[0]["GooglePwd"].ToString();
                    Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> window.open('Analytics.aspx','','scrollbars=yes,width=620,height=580'); </script>");


                }
                else
                {
                    ///Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script>return alert('Please contact support as your subscription level does not allow to view analytics.'); </script>");
                    BtnAnalytics.Attributes.Add("onclick", "return alert('Analytics  not added for this project.');");
                }
            }

        
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {


    }
    protected void btnRemove_Click(object sender, EventArgs e)
    {

    }
  
}
