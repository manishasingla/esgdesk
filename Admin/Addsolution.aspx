﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Addsolution.aspx.cs" Inherits="Admin_Addsolution" %>

<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Add solution</title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../DashStyle.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <uc2:Admin_Header ID="Admin_Header1" runat="server" />
    <div>
        <%--<asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>--%>
        <div id="inner">
            <div class="pagearea" style="width: 80%; float: left">
                <div id="TicketSummary">
                    <%--<h3 style="font-size: 16px; font-weight: bold; padding: 5px 0">--%>
                    <%--    <h3 style="font-size: 16px; font-weight: bold; padding: 5px 0px 5px 6px; background-color: rgb(62, 63, 61);
                        color: white; border-top: 4px solid rgb(0, 166, 81);">
                        New solution category</h3>--%>
                    <div id="errorExplanation" class="errorExplanation" runat="server" visible="false">
                        <p>
                            There were problems with the following fields:</p>
                        <ul id="ulalready" runat="server" visible="false">
                            <li>Category name has already been taken</li></ul>
                        <ul id="ulerror" runat="server" visible="false">
                            <li>Some error while adding category name </li>
                        </ul>
                    </div>
                    <table width="100%" style="background: #EBEBEB; padding: 6px">
                        <tr>
                            <td>
                                Title *
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtSolutionname" runat="server" Style="width: 99%;"> </asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This field is required."
                                    ControlToValidate="txtSolutionname" ValidationGroup="error"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Description
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <telerik:RadEditor ID="ftbComment" runat="server" Height="300px" Width="100%">
                                    <Snippets>
                                        <telerik:EditorSnippet Name="Product Template" Value="<table><tr><td><div style='width:300px;border:2px outset #D8D2BD;padding:34px;font-family:Arial, Verdana; font-size:11px;background- color:#F1EFE6;'>Dear ____________________,<br>Thank you for inquiring about ____________. Your request will be processed in 48 hours and shipped at the address you have provided.<b>Please, contact us if you have any problems.</b></div></td></tr></table>">            
                                        </telerik:EditorSnippet>
                                        <telerik:EditorSnippet Name="Signature" Value="<strong>John Smith</strong><br>Sales Manager<br>">
                                        </telerik:EditorSnippet>
                                    </Snippets>
                                    <Links>
                                        <telerik:EditorLink Name="Telerik" Href="http://www.telerik.com">
                                            <telerik:EditorLink Name="Telerik Products" Href="http://www.telerik.com/products"
                                                Target="_blank" />
                                            <telerik:EditorLink Name="Telerik Support Center" Href="http://www.telerik.com/support/home.aspx"
                                                Target="_blank" />
                                            <telerik:EditorLink Name="Telerik Community" Href="http://www.telerik.com/community/home.aspx"
                                                Target="_blank" ToolTip="Telerik Community" />
                                        </telerik:EditorLink>
                                    </Links>
                                    <Tools>
                                    </Tools>
                                    <ImageManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                    <MediaManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                    <FlashManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                    <TemplateManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                    <DocumentManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images"  MaxUploadFileSize="1048576"/>
                                    <Modules>
                                        <telerik:EditorModule Name="RadEditorStatistics" Visible="False" Enabled="true" />
                                        <telerik:EditorModule Name="RadEditorDomInspector" Visible="False" Enabled="true" />
                                        <telerik:EditorModule Name="RadEditorNodeInspector" Visible="False" Enabled="true" />
                                        <telerik:EditorModule Name="RadEditorHtmlInspector" Visible="False" Enabled="true" />
                                    </Modules>
                                    <Content>
                                    </Content>
                                </telerik:RadEditor>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="This field is required."
                                    ControlToValidate="ftbComment" ValidationGroup="error"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Tag
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txttag" runat="server" Style="width: 99%;"> </asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td width="50%">
                                            <table>
                                                <tr>
                                                    <td>
                                                        Folder
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:DropDownList ID="ddlfolderlist" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="50%">
                                            <table>
                                                <tr>
                                                    <td>
                                                        Solution Type
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <%--<asp:DropDownList ID="ddlSolutionType" runat="server">
                                                            <asp:ListItem Selected="True">Permanent</asp:ListItem>
                                                            <asp:ListItem>Workaround</asp:ListItem>
                                                        </asp:DropDownList>--%>
                                                        <asp:DropDownList ID="ddlSolutionType" runat="server">
                                                            <asp:ListItem Value="Public">Public</asp:ListItem>
                                                            <asp:ListItem Value="Private">Private</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            <tr>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                Solution Status
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlSolutionStatus" runat="server">
                                                    <asp:ListItem>Draft</asp:ListItem>
                                                    <asp:ListItem Selected="True">Published</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                    </table>
                    <br />
                    <table width="100%">
                        <tr>
                            <td style="text-align: right">
                                <asp:Button ID="bntdel" runat="server" Text="Delete" Visible="false" Style="float: left"
                                    OnClick="bntdel_Click" />
                                <asp:Button ID="btnupdate" runat="server" Text="Update" Visible="false" ValidationGroup="error"
                                    OnClick="btnupdate_Click" />
                                <asp:Button ID="btnsave" runat="server" Text="Save" OnClick="btnsave_Click" ValidationGroup="error" />
                                <asp:Button ID="btnSaveAnother" runat="server" Text="Save and Create Another" OnClick="btnSaveAnother_Click"
                                    ValidationGroup="error" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
