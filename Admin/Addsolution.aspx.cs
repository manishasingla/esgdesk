﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Admin_Addsolution : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Response.Redirect("login.aspx");
            return;
        }
        else
        {
            if (!DatabaseHelper.isPrivateSolutions(Session["admin"].ToString()))
            {
                Response.Redirect("Solution.aspx");
            }
        }
        bntdel.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete this solution?')");
        if (!IsPostBack)
        {
            if (Request.QueryString["categoryid"] != null && Request.QueryString["folderid"] != null && Request.QueryString["solutionid"] != null)
            {
                getdropvalues(Request.QueryString["categoryid"].ToString());
                getsolution(Request.QueryString["categoryid"].ToString(), Request.QueryString["folderid"], Request.QueryString["solutionid"]);

                Page.Title = "Update solution";
            }
            else if (Request.QueryString["categoryid"] != null && Request.QueryString["folderid"] != null)
            {
                getdropvalues(Request.QueryString["categoryid"].ToString());
                getCfolder(Request.QueryString["categoryid"].ToString(), Request.QueryString["folderid"].ToString());
            }
            else if (Request.QueryString["categoryid"] != null)
            {
                getdropvalues(Request.QueryString["categoryid"].ToString());
                //ddlfolderlist.SelectedValue = Request.QueryString["folderid"].ToString();
                Page.Title = "Add solution";
            }
            else
            {
                Response.Redirect("solution.aspx");
            }
        }
    }
    void getdropvalues(string cid)
    {
        string sql = @"select * from category_folder where Category_Id=" + cid;

        DataSet ds = DatabaseHelper.getallstatus(sql);
        if (ds != null)
        {
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {

                ddlfolderlist.DataSource = ds.Tables[0];
                ddlfolderlist.DataTextField = "foldername";
                ddlfolderlist.DataValueField = "folder_id";
                ddlfolderlist.DataBind();

            }
        }
    }
    void getCfolder(string cid, string fid)
    {
        string sql = @"select * from category_folder where category_id=" + cid + " and folder_id= " + fid;

        DataSet ds = DatabaseHelper.getallstatus(sql);
        if (ds != null)
        {
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {


                ddlfolderlist.SelectedValue = ds.Tables[0].Rows[0]["Folder_Id"].ToString();
            }
        }
    }
    void getsolution(string cid, string fid, string sid)
    {
        string sql = @"select * from solutions where category_id=" + cid + " and folder_id= " + fid + " and solution_id=" + sid;

        DataSet ds = DatabaseHelper.getallstatus(sql);
        if (ds != null)
        {
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                txtSolutionname.Text = ds.Tables[0].Rows[0]["SolutionName"].ToString().Replace("|@", "'");
                ftbComment.Content = ds.Tables[0].Rows[0]["SolutionDescription"].ToString().Replace("|@", "'");
                txttag.Text = ds.Tables[0].Rows[0]["Tags"].ToString().Replace("|@", "'");
                ddlfolderlist.SelectedValue = ds.Tables[0].Rows[0]["Folder_Id"].ToString();
                //ddlSolutionType.SelectedItem.Text = ds.Tables[0].Rows[0]["SolutionType"].ToString();
                ddlSolutionType.SelectedValue = ds.Tables[0].Rows[0]["SolutionType"].ToString();
                ddlSolutionStatus.SelectedItem.Text = ds.Tables[0].Rows[0]["SolutionStatus"].ToString();
                bntdel.Visible = true;
                btnupdate.Visible = true;
                btnsave.Visible = false;
                btnSaveAnother.Visible = false;
            }
        }
    }
    private void CategoryInsert(string next)
    {

        string sql = @"insert into solutions 
(Folder_Id,category_id,SolutionName,SolutionDescription,Tags,SolutionType,SolutionStatus,reported_date)values
                     (" + ddlfolderlist.SelectedValue + "," + Request.QueryString["categoryid"].ToString() + ",'" + txtSolutionname.Text.Replace("'", "|@").Trim() + "',N'" + ftbComment.Content.Replace("'", "|@") + "','"
                         + txttag.Text.Replace("'", "|@").Trim() + "','" + ddlSolutionType.SelectedItem.Value + "','" + ddlSolutionStatus.SelectedItem.Text + "',getdate())";

        int intResult = DatabaseHelper.executeSQLquery(sql);

        if (intResult == 0)
        {
            errorExplanation.Visible = true;
            ulerror.Visible = true;
        }
        else
        {
            if (next == "")
            {
                if (Request.QueryString["categoryid"] != null && Request.QueryString["folderid"] != null)
                {
                    Response.Redirect("Solutionslist.aspx?categoryid=" + Request.QueryString["categoryid"].ToString() + "&folderid=" + Request.QueryString["folderid"].ToString());
                }
                else
                {
                    Response.Redirect("Solutionslist.aspx?categoryid=" + Request.QueryString["categoryid"].ToString());
                }
            }
            else
            {
                clear();
            }
        }

    }

    private void Categoryupdate(string cid, string fid, string sid)
    {


        string sql = @"update solutions 
set Folder_Id=" + ddlfolderlist.SelectedValue + ",SolutionName='" + txtSolutionname.Text.Replace("'", "|@").Trim() + "',SolutionDescription=N'" + ftbComment.Content.Replace("'", "|@") + "',Tags='" + txttag.Text.Replace("'", "|@").Trim() + "',SolutionType='" + ddlSolutionType.SelectedItem.Text + "',SolutionStatus='" + ddlSolutionStatus.SelectedItem.Text + "' where Category_Id=" + cid + " and Folder_Id=" + fid + " and solution_id=" + sid;


        int intResult = DatabaseHelper.executeSQLquery(sql);

        if (intResult == 0)
        {
            errorExplanation.Visible = true;
            ulerror.Visible = true;
        }
        else
        {

            //Response.Redirect("Solutionslist.aspx?cid=" + Request.QueryString["cid"].ToString() + "&id=" + Request.QueryString["fid"].ToString());
            Response.Redirect("Solutiondetail.aspx?categoryid=" + Request.QueryString["categoryid"].ToString() + "&folderid=" + ddlfolderlist.SelectedValue + "&solutionid=" + sid);

        }

    }

    private void solutiondelete(string Categoryid, string folderid, string solutionid)
    {
        string sqlsub = @"delete from solutions where Category_id=" + Categoryid + " and Folder_Id=" + folderid + " and solution_id=" + solutionid;

        int Result = DatabaseHelper.executeSQLquery(sqlsub);

        if (Result == 0)
        {
            errorExplanation.Visible = true;
            ulerror.Visible = true;
        }
        else
        {
            //Response.Redirect("Solutionslist.aspx?cid=" + Request.QueryString["cid"].ToString() + "&id=" + Request.QueryString["fid"].ToString());
            Response.Redirect("Solutionslist.aspx?categoryid=" + Request.QueryString["categoryid"].ToString() + "&folderid=" + ddlfolderlist.SelectedValue);
        }

    }

    void clear()
    {
        txtSolutionname.Text = string.Empty;
        ftbComment.Content = string.Empty;
        errorExplanation.Visible = false;
        ulalready.Visible = false;
        ulerror.Visible = false;
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        CategoryInsert("");
    }
    protected void btnSaveAnother_Click(object sender, EventArgs e)
    {
        CategoryInsert("Yes");
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        //if (Request.QueryString["cid"] != null && Request.QueryString["fid"] != null)
        //{
        //    Response.Redirect("Solutionslist.aspx?cid=" + Request.QueryString["cid"].ToString() + "&id=" + Request.QueryString["fid"].ToString());
        //}
        //else
        //{
        //    Response.Redirect("Solutionslist.aspx?cid=" + Request.QueryString["cid"].ToString());
        //}
        if (Request.QueryString["categoryid"] != null && Request.QueryString["folderid"] != null && Request.QueryString["solutionid"] != null)
        {
            Response.Redirect("Solutiondetail.aspx?categoryid=" + Request.QueryString["categoryid"].ToString() + "&folderid=" + ddlfolderlist.SelectedValue + "&solutionid=" + Request.QueryString["solutionid"]);
        }
        else if (Request.QueryString["categoryid"] != null && Request.QueryString["folderid"] != null)
        {
            Response.Redirect("Solutionslist.aspx?categoryid=" + Request.QueryString["categoryid"].ToString() + "&folderid=" + ddlfolderlist.SelectedValue);
        }
        else
        {
            Response.Redirect("Solutionslist.aspx?categoryid=" + Request.QueryString["categoryid"].ToString());
        }
    }
    protected void bntdel_Click(object sender, EventArgs e)
    {
        solutiondelete(Request.QueryString["categoryid"], Request.QueryString["folderid"], Request.QueryString["solutionid"]);
    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        Categoryupdate(Request.QueryString["categoryid"], Request.QueryString["folderid"], Request.QueryString["solutionid"]);
    }
}