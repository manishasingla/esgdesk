<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddsolutionCategory.aspx.cs"
    Inherits="Admin_categories" EnableEventValidation="false" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="Notifications.ascx" TagName="Notifications" TagPrefix="uc4" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Solution category</title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        table#DataGrid1_ctl00 tr th a
        {
            display: block;
            text-decoration: none;
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <uc2:Admin_Header ID="Admin_Header1" runat="server" />
        <div id="Content">
            <div id="Content_98">
                <div class="titleText" style="margin: 10px 0; display: none;">
                    Categories&nbsp;|&nbsp;</div>
                <div>
                    <uc4:Notifications ID="Notifications" runat="server" />
                </div>
                <br />
                <div width="100%" align="left" valign="middle" class="text_default" style="background: #ebebeb;
                    padding: 5px; margin-top: 5px;">
                    <span class="text_default">
                        <asp:LinkButton ID="lnkAddNew" runat="server" OnClick="lnkAddNew_Click" Style="border-bottom: 1px dotted #000;"
                            CausesValidation="False">Add new category</asp:LinkButton>
                        <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" Style="border-bottom: 1px dotted #000;"
                            CausesValidation="False">&nbsp;|&nbsp;Back to categories</asp:LinkButton></span><div
                                style="float: right; padding: 0px; text-align: right;">
                                <b>Records: </b>
                                <asp:Label ID="LblRecordsno" runat="server" Text=""></asp:Label></div>
                </div>
                <div id="DivEntry" runat="server" visible="false">
                    <table>
                        <tr>
                            <td align="left" valign="top" style="width: 434px">
                                <table style="width: 424px">
                                    <tr>
                                        <td style="width: 424px">
                                            <table width="424" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="left" valign="top">
                                                        <img height="16" src="../images/loginBox_top.png" width="420" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="background: url(../images/loginBox_mid.png) repeat-y left top;">
                                                        <table width="100%" align="left" cellpadding="8" cellspacing="0">
                                                            <tr>
                                                                <td width="10" align="left" valign="top" class="whitetext1">
                                                                    &nbsp;
                                                                </td>
                                                                <td width="140" align="left" valign="top" class="whitetext1">
                                                                    Category ID:
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext2">
                                                                    <asp:Label ID="lblId" runat="server">New</asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" valign="top" class="whitetext1">
                                                                    &nbsp;
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext1">
                                                                    Categorty name:
                                                                </td>
                                                                <td align="left" valign="top">
                                                                    <asp:TextBox ID="txtName" runat="server" MaxLength="50" Width="200"></asp:TextBox>
                                                                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator1">
                                                                    </cc1:ValidatorCalloutExtender>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName"
                                                                        ErrorMessage="Please enter category name.">*</asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" valign="top" class="whitetext1">
                                                                    &nbsp;
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext1">
                                                                    Sort sequence:
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext2">
                                                                    <asp:TextBox ID="txtSortSequece" runat="server" Width="200" MaxLength="2"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"
                                                                        TargetControlID="txtSortSequece">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" valign="top" class="whitetext1">
                                                                    &nbsp;
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext1">
                                                                    Default selection:
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext2">
                                                                    <asp:CheckBox ID="chkDefault" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 100px">
                                                        <img height="16" src="../images/loginBox_btm.png" width="420" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 424px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <div id="divMessage" runat="server" style="color: Red; font-weight: bold; float: left;">
                                            </div>
                                            <asp:Button ID="btnCreate" runat="server" Text="Create" OnClick="btnCreate_Click"
                                                CssClass="blueBtns" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div>
                    <%--**************************************************************************************************--%>
                    <%--<asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" CellPadding="3"
                        OnItemCommand="DataGrid1_ItemCommand" OnItemDataBound="DataGrid1_ItemDataBound"
                        Width="100%">
                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                            Font-Underline="False" HorizontalAlign="Center" />
                        <HeaderStyle Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False"
                            HorizontalAlign="Center" BackColor="#282C5F" ForeColor="#FFFFFF" />
                        <Columns>
                            <asp:BoundColumn DataField="category_id" HeaderText="Category id"></asp:BoundColumn>
                            <asp:BoundColumn DataField="category_name" HeaderText="Category name">
                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" HorizontalAlign="Left" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="sort_seq" HeaderText="Sort sequence"></asp:BoundColumn>
                            <asp:BoundColumn DataField="default_selection" HeaderText="Default selection"></asp:BoundColumn>
                            <asp:ButtonColumn CommandName="edit" HeaderText="Edit" Text="edit"></asp:ButtonColumn>
                            <asp:ButtonColumn CommandName="delete" HeaderText="Delete" Text="delete"></asp:ButtonColumn>
                        </Columns>
                    </asp:DataGrid>&nbsp;--%>
                    <telerik:RadGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" CellPadding="3"
                        Width="100%" OnItemCommand="DataGrid1_ItemCommand" OnItemDataBound="DataGrid1_ItemDataBound">
                        <PagerStyle Mode="NextPrevAndNumeric" />
                        <MasterTableView PagerStyle-AlwaysVisible="true">
                            <HeaderStyle Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False"
                                HorizontalAlign="Center" BackColor="#282C5F" />
                            <Columns>
                                <telerik:GridBoundColumn DataField="category_id" Display="false">
                                </telerik:GridBoundColumn>
                                <%--0--%>
                                <telerik:GridTemplateColumn HeaderText="Category id">
                                    <ItemTemplate>
                                        <asp:Label ID="lblcategoryid" runat="server" Text='<%#Bind("category_id") %>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <%--1--%>
                                <telerik:GridTemplateColumn HeaderText="Category name">
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblcategoryname" runat="server" Text='<%#Bind("category_name") %>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <%--2--%>
                                <telerik:GridTemplateColumn HeaderText="Sort sequence">
                                    <ItemTemplate>
                                        <asp:Label ID="lblsortseq" runat="server" Text='<%#Bind("sort_seq") %>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <%--3--%>
                                <telerik:GridTemplateColumn HeaderText="Default selection">
                                    <ItemTemplate>
                                        <asp:Label ID="lbldefaultselection" runat="server" Text='<%#Bind("default_selection") %>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <%--4--%>
                                <telerik:GridTemplateColumn HeaderText="Edit">
                                    <ItemTemplate>
                                        <asp:LinkButton CommandName="edit" ID="lnkedit" runat="server" Text="edit"></asp:LinkButton>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <%--5--%>
                                <telerik:GridTemplateColumn HeaderText="Delete">
                                    <ItemTemplate>
                                        <asp:LinkButton CommandName="delete" ID="lnkdelete" runat="server" Text="delete"></asp:LinkButton>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="Add notes">
                                    <ItemTemplate>
                                        <a href="solutioncategoriesnote.aspx?folderid=<%#Eval("category_id") %>">Add note</a>
                                        <%--<asp:Label ID="lbldefaultselection" runat="server" Text='<%#Bind("default_selection") %>'></asp:Label>--%>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="Notes count">
                                    <ItemTemplate>
                                        <a href="solutioncategoriesnotelist.aspx?folderid=<%#Eval ("category_id") %>">
                                            <div>
                                              View notes (<asp:Label ID="lblncount" runat="server"></asp:Label>) </div>
                                        </a>
                                        <%--<asp:Label ID="lbldefaultselection" runat="server" Text='<%#Bind("default_selection") %>'></asp:Label>--%>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                    <%--**************************************************************************************************--%>
                    <asp:HiddenField ID="prevName" runat="server" />
                </div>
            </div>
        </div>
    </div>
    <uc1:footer ID="Footer1" runat="server" />
    </form>
</body>
</html>
