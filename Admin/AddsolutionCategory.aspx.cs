using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
using Telerik.Web;

public partial class Admin_categories : System.Web.UI.Page
{
    string sql = "";
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = "categories.aspx";
            Response.Redirect("login.aspx");
            return;
        }

        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            Response.Redirect("tasks.aspx");
            return;
        }

        divMessage.InnerHtml = "";

        if (!Page.IsPostBack)
        {

            bindData();
        }
    }


    protected void btnCreate_Click(object sender, EventArgs e)
    {
        if (txtName.Text.Trim() == "")
        {
            divMessage.InnerHtml = "Please enter category name.";
            return;
        }

        if (btnCreate.Text == "Create")
        {
            object objResult = DatabaseHelper.executeScalar("select count(*) from Solution_categories where [category_name]='" + txtName.Text.Trim() + "'");

            if (objResult.ToString() != "0")
            {
                divMessage.InnerHtml = "Category name alredy exist.";
                return;
            }


            string strSql = " insert into Solution_categories([category_name],[sort_seq],[default_selection]) ";
            strSql += " values('" + txtName.Text.Trim().Replace("'", "''") + "'," + ((txtSortSequece.Text.Trim() != "") ? txtSortSequece.Text.Trim() : "0") + ",'" + (chkDefault.Checked ? "Y" : "N") + "')";

            int intResult = DatabaseHelper.executeNonQuery(strSql);

            if (intResult != 0)
            {
                divMessage.InnerHtml = "Category was created.";
                clearControls();
            }
            else
            {
                divMessage.InnerHtml = "Category was not created.";
            }
        }
        else if (btnCreate.Text == "Edit")
        {
            object objResult = DatabaseHelper.executeScalar("select count(*) from Solution_categories where [category_name]='" + txtName.Text.Trim() + "' and [category_id]!=" + lblId.Text);

            if (objResult.ToString() != "0")
            {
                divMessage.InnerHtml = "Category name alredy exist.";
                return;
            }

            string strSql = " update Solution_categories ";
            strSql += " set [category_name]= '" + txtName.Text.Trim().Replace("'", "''") + "',";
            strSql += " [sort_seq] = " + txtSortSequece.Text.Trim() + ",";
            strSql += " [default_selection] = '" + (chkDefault.Checked ? "Y" : "N") + "'";
            strSql += " where category_id = " + lblId.Text.Trim();

            int intResult = DatabaseHelper.executeNonQuery(strSql);

            if (intResult != 0)
            {
                //if (prevName.Value != txtName.Text.Trim().Replace("'", "''"))
                //{
                //    strSql = "update tasks set category = '" + txtName.Text.Trim().Replace("'", "''") +"' ";
                //    strSql+= " where category ='"+ prevName.Value.Replace("'","''") +"'";
                //    intResult = DatabaseHelper.executeNonQuery(strSql);
                //}

                divMessage.InnerHtml = "Category was updated.";
            }
            else
            {
                divMessage.InnerHtml = "Category was not updated.";
            }
        }
    }

    private void bindData()
    {
        string strSql = "select * from Solution_categories";

        DataSet ds = DatabaseHelper.getDataset(strSql);

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                DataGrid1.DataSource = ds.Tables[0];
                PagedDataSource objds = new PagedDataSource();
                objds.DataSource = ds.Tables[0].DefaultView;

                LblRecordsno.Text = objds.DataSourceCount.ToString();
                DataGrid1.DataBind();
                DataGrid1.Visible = true;
                lnkBack.Visible = false;
            }
            else
            {
                LblRecordsno.Text = "0";
                DataGrid1.DataSource = null;
                DataGrid1.DataBind();
                DataGrid1.Visible = false;
                lnkBack.Visible = false;
                divMessage.InnerHtml = "No category in the database.";
            }
        }
        else
        {



            DataGrid1.DataSource = null;
            DataGrid1.DataBind();
            DataGrid1.Visible = false;
            lnkBack.Visible = false;
            divMessage.InnerHtml = "No category in the database..";
        }
    }
    protected void lnkAddNew_Click(object sender, EventArgs e)
    {
        clearControls();
        DivEntry.Visible = true;
        DataGrid1.Visible = false;
        lnkBack.Visible = true;
        divMessage.InnerHtml = "";
        this.Title = "Add solution category";
    }

    protected void DataGrid1_ItemCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
    {
        GridDataItem dataItem = e.Item as GridDataItem;
        if (e.CommandName == "edit")
        {
            string strSql = "select * from Solution_categories where category_id=" + ((Label)dataItem.FindControl("lblcategoryid")).Text;
            DataSet ds = DatabaseHelper.getDataset(strSql);
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    lblId.Text = ds.Tables[0].Rows[0]["category_id"].ToString();
                    txtName.Text = ds.Tables[0].Rows[0]["category_name"].ToString();
                    prevName.Value = ds.Tables[0].Rows[0]["category_name"].ToString();
                    txtSortSequece.Text = ds.Tables[0].Rows[0]["sort_seq"].ToString();
                    chkDefault.Checked = ds.Tables[0].Rows[0]["default_selection"].ToString() == "Y" ? true : false;
                    btnCreate.Text = "Edit";
                    DivEntry.Visible = true;
                    DataGrid1.Visible = false;
                    lnkBack.Visible = true;
                    divMessage.InnerHtml = "";
                    this.Title = "Edit category - " + ds.Tables[0].Rows[0]["category_name"].ToString(); 
                }
            }
        }
        else if (e.CommandName == "delete")
        {
            //object objResult = DatabaseHelper.executeScalar("select count(*) from tasks where category=" + ((Label)dataItem.FindControl("lblcategoryid")).Text);
            //if (objResult.ToString() == "0")
            //{
            string strSql = "delete from Solution_categories where category_id=" + ((Label)dataItem.FindControl("lblcategoryid")).Text + " ; delete  from Solution_categoriesnotes where Folder_Id=" + ((Label)dataItem.FindControl("lblcategoryid")).Text;
            int intResult = DatabaseHelper.executeNonQuery(strSql);
            if (intResult != 0)
            {
                divMessage.InnerHtml = "Category was deleted.";
                bindData();
            }
            else
            {
                divMessage.InnerHtml = "Category was not deleted.";
                bindData();
            }
            //}
            //else
            //{
            //    divMessage.InnerHtml = "You can not delete this category. It is been refered by another table.";
            //}
        }
    }

    protected void DataGrid1_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        foreach (TableCell tc in e.Item.Cells)
        {
            tc.Attributes["style"] = "border:1px solid #00A651;";
        }
        if (e.Item is GridPagerItem)
        {
            GridPagerItem pagerItem = (GridPagerItem)e.Item;
            RadComboBox PageSizeComboBox = (RadComboBox)pagerItem.FindControl("PageSizeComboBox");
            PageSizeComboBox.Visible = false;
            Label changePageSizelbl = (Label)pagerItem.FindControl("ChangePageSizeLabel");
            changePageSizelbl.Visible = false;
        }
        if (e.Item.ItemType == GridItemType.Item || e.Item.ItemType == GridItemType.AlternatingItem)
        {
            GridDataItem dataItem = e.Item as GridDataItem;
            LinkButton deleteButton = (LinkButton)dataItem.FindControl("lnkdelete");
            deleteButton.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete this category?')");
        }
        if (e.Item.ItemIndex != -1)
        {
            int count = 0;
            try
            {
                if (e.Item.Cells.Count > 2)
                {
                    count = getnotcount(e.Item.Cells[2].Text);
                    if (count > 0)
                    {
                        Label lblcount = (Label)e.Item.FindControl("lblncount");
                        lblcount.Text = count.ToString();
                    }
                    else
                    {
                        Label lblcount = (Label)e.Item.FindControl("lblncount");
                        lblcount.Text = count.ToString();
                    }
                }
            }
            catch
            { }
        }


    }

    public int getnotcount(string Cid)
    {
        int count = 0;
        string sqlsub = @"select COUNT(*) as RowNumber from Solution_categoriesnotes where folder_id=" + Cid;


        DataSet dscount = DatabaseHelper.getallstatus(sqlsub);
        if (dscount != null)
        {
            if (dscount.Tables.Count > 0 && dscount.Tables[0].Rows.Count > 0)
            {
                count = int.Parse(dscount.Tables[0].Rows[0]["RowNumber"].ToString());
            }
        }
        return count;
    }

    private void clearControls()
    {
        lblId.Text = "New";
        txtName.Text = "";
        txtSortSequece.Text = "";
        chkDefault.Checked = false;
        btnCreate.Text = "Create";
    }

    protected void lnkBack_Click(object sender, EventArgs e)
    {
        clearControls();
        DivEntry.Visible = false;
        bindData();
    }
}
