using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class AdminAddProjectCategory : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        //if (Session["clientid"] == null || Session["username"] == null || Session["clientid"].ToString() == "" || Session["username"].ToString() == "")
        //{
        //    Session["returnUrl"] = "ClientRequests.aspx";
        //    Response.Redirect("login.aspx", false);
        //    return;
        //}
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = "admin.aspx";
            Response.Redirect("login.aspx");
            return;
        }
        bntdel.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete this category folder?')");
        if (!IsPostBack)
        {
            try
            {
                if (Server.UrlDecode(Request.QueryString["projectname"]) != null && Request.QueryString["folderid"] != null)
                {
                    try
                    {
                        GetFolderCategory(Server.UrlDecode(Request.QueryString["projectname"].ToString()), Request.QueryString["folderid"].ToString());
                    }
                    catch (Exception ex)
                    {
                        DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at AdminAddProjectCategory.aspx:2 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Page Load Outer ,Project Name:-" + Request.QueryString["projectname"] + " FolderId:-" + Request.QueryString["folderid"]);
                    }
                }
                else if (Server.UrlDecode(Request.QueryString["projectname"]) != null)
                {

                }
                else
                {
                    Response.Redirect("Solution.aspx");
                }
            }
            catch (Exception ex)
            {
                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at AdminAddProjectCategory.aspx:1 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Page Load Outer ,Project Name:-" + Request.QueryString["projectname"] + " FolderId:-" + Request.QueryString["folderid"]);
            }

        }

    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        try
        {
            FolderInsert("");
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at AdminAddProjectCategory.aspx:1 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "btnsave_Click");
        }
    }

    private void FolderInsert(string next)
    {
        string sqlsub = @"select * from Project_Folder where IsProject='False' and FolderName='" + txtFoldername.Text.Replace("'", "|@").Trim() + "' and Project_Name='" + Request.QueryString["projectname"].ToString() + "'";
        int Result = 0;
        try
        {
            Result = DatabaseHelper.checkuser(sqlsub);
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at AdminAddProjectCategory.aspx:1 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "FolderInsert" + sqlsub);
        }

        if (Result <= 0)
        {
            string sql = @"insert into Project_Folder (FolderName,FolderDescription,Project_Name,IsProject)values('" + txtFoldername.Text.Replace("'", "|@").Trim() + "','" + txtDescription.Text.Replace("'", "|@") + "','" + Request.QueryString["projectname"].ToString() + "','False')";
            int intResult = 0;
            try
            {
                intResult = DatabaseHelper.executeSQLquery(sql);
            }
            catch (Exception ex)
            {
                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at AdminAddProjectCategory.aspx:2 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "FolderInsert" + sql);
            }

            if (intResult == 0)
            {
                errorExplanation.Visible = true;
                ulerror.Visible = true;
            }
            else
            {
                if (next == "")
                {
                    Response.Redirect("AdminProjectNoteList.aspx?projectname=" + Server.UrlEncode(Request.QueryString["projectname"].ToString()));
                }
                else
                {
                    clear();
                }
            }
        }
        else
        {
            errorExplanation.Visible = true;
            ulalready.Visible = true;
        }

    }

    private void Folderdelete(string Categoryid, string folderid)
    {
        int DelteAllPro = 0;
        string delallFolder = @"delete from NotesStatus where IsProject='False' and Project_Name='" + Categoryid + "' and Folder_Id=" + folderid;
        try
        {
            DelteAllPro = DatabaseHelper.executeSQLquery(delallFolder);
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at AdminAddProjectCategory.aspx:11 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Folderdelete" + delallFolder);
        }


        string sqlsub = @"delete from ProjectNotes where IsProject='False' and Project_Name='" + Categoryid + "' and Folder_Id=" + folderid;
        int Result = 0;
        try
        {
            Result = DatabaseHelper.executeSQLquery(sqlsub);
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at AdminAddProjectCategory.aspx:1 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Folderdelete" + sqlsub);
        }
        if (Result != 0)
        {
            sqlsub = @"delete from Project_Folder where IsProject='False' and Project_Name='" + Categoryid + "' and Folder_Id=" + folderid;
            try
            {
                Result = DatabaseHelper.executeSQLquery(sqlsub);
            }
            catch (Exception ex)
            {
                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at AdminAddProjectCategory.aspx:2 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Folderdelete" + sqlsub);
            }

            if (Result == 0)
            {
                errorExplanation.Visible = true;
                ulerror.Visible = true;
            }
            else
            {
                Response.Redirect("AdminProjectNoteList.aspx?projectname=" + Server.UrlEncode(Request.QueryString["projectname"].ToString()));
            }
        }
        else
        {

            sqlsub = @" select count(*) from ProjectNotes where IsProject='False' and Project_Name='" + Categoryid + "' and Folder_Id=" + folderid;
            object objUserId = null;
            try
            {
                objUserId = DatabaseHelper.executeScalar(sqlsub);
            }
            catch (Exception ex)
            {
                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at AdminAddProjectCategory.aspx:3 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Folderdelete" + sqlsub);
            }
            if (objUserId.ToString() == "0")
            {
                sqlsub = @"delete from Project_Folder where IsProject='False' and Project_Name='" + Categoryid + "' and Folder_Id=" + folderid;
                try
                {
                    Result = DatabaseHelper.executeSQLquery(sqlsub);
                }
                catch (Exception ex)
                {
                    DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at AdminAddProjectCategory.aspx:4" + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Folderdelete" + sqlsub);
                }
                if (Result == 0)
                {
                    errorExplanation.Visible = true;
                    ulerror.Visible = true;
                }
                else
                {
                    Response.Redirect("AdminProjectNoteList.aspx?projectname=" + Server.UrlEncode(Request.QueryString["projectname"].ToString()));
                }
            }
        }

    }

    protected void btnSaveAnother_Click(object sender, EventArgs e)
    {
        try
        {
            FolderInsert("Yes");
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at AdminAddProjectCategory.aspx:1" + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "btnSaveAnother_Click");
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            if (Server.UrlDecode(Request.QueryString["projectname"]) != null && Request.QueryString["folderid"] != null)
            {
                Response.Redirect("AdminProjectNoteList.aspx?projectname=" + Server.UrlEncode(Request.QueryString["projectname"].ToString()) + "&folderid=" + Request.QueryString["folderid"].ToString());
            }
            else if (Request.QueryString["projectname"] != null)
            {
                Response.Redirect("AdminProjectNoteList.aspx?projectname=" + Server.UrlEncode(Request.QueryString["projectname"].ToString()));
            }
            else
            {
                Response.Redirect("AdminProjectNoteList.aspx");
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at AdminAddProjectCategory.aspx:1" + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "btnCancel_Click");
        }
    }
    void clear()
    {
        try
        {
            txtFoldername.Text = string.Empty;
            txtDescription.Text = string.Empty;
            errorExplanation.Visible = false;
            ulalready.Visible = false;
            ulerror.Visible = false;
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at AdminAddProjectCategory.aspx:1" + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "clear()");
        }
    }
    private void GetFolderCategory(string Categoryid, string folderid)
    {
        string sql = @"select * from Project_Folder where IsProject='False' and Project_Name='" + Categoryid + "' and Folder_Id=" + folderid;
        DataSet ds = null;
        try
        {
            ds = DatabaseHelper.getallstatus(sql);
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at AdminAddProjectCategory.aspx:1" + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "GetFolderCategory()" + sql);
        }

        if (ds != null)
        {
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                txtFoldername.Text = ds.Tables[0].Rows[0]["FolderName"].ToString().Replace("|@", "'");
                txtDescription.Text = ds.Tables[0].Rows[0]["FolderDescription"].ToString().Replace("|@", "'");
                bntdel.Visible = true;
                btnupdate.Visible = true;
                btnsave.Visible = false;
                btnSaveAnother.Visible = false;
            }
        }
        else
        {
            errorExplanation.Visible = true;
            ulalready.Visible = true;
        }

    }
    protected void bntdel_Click(object sender, EventArgs e)
    {
        try
        {
            Folderdelete(Server.UrlDecode(Request.QueryString["projectname"].ToString()), Request.QueryString["folderid"].ToString());
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at AdminAddProjectCategory.aspx:1" + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "bntdel_Click()");
        }
    }
    private void FolderUpdate(string cid, string fid)
    {

        //string sqlsub = @"select * from Project_Folder where FolderName='" + txtFoldername.Text.Replace("'", "|@").Trim() + "'";
        //int Result = 0;
        //try
        // {
        //Result = DatabaseHelper.checkuser(sqlsub);
        // }
        //catch (Exception ex)
        //{
        //DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at addcategoryfolder.aspx:1" + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "FolderUpdate()" + sqlsub);
        // }

        //if (Result <= 0)
        //{

        string sql = @"update Project_Folder set FolderName='" + txtFoldername.Text.Replace("'", "|@").Trim() + "',FolderDescription='" + txtDescription.Text.Replace("'", "|@") + "' where IsProject='False' and Project_Name='" + cid + "' and Folder_Id=" + fid;
        int intResult = 0;
        try
        {
            intResult = DatabaseHelper.executeSQLquery(sql);
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at AdminAddProjectCategory.aspx:2" + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "FolderUpdate()" + sql);
        }

        if (intResult == 0)
        {
            errorExplanation.Visible = true;
            ulerror.Visible = true;
        }
        else
        {
            Response.Redirect("AdminProjectNoteList.aspx?projectname=" + Server.UrlEncode(Request.QueryString["projectname"].ToString()) + "&folderid=" + Request.QueryString["folderid"].ToString());

        }

    }
        protected void btnupdate_Click(object sender, EventArgs e)
    {
        try
        {
            FolderUpdate(Server.UrlDecode(Request.QueryString["projectname"]), Request.QueryString["folderid"]);
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at AdminAddProjectCategory.aspx:2" + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "btnupdate_Click()");
        }
    }













































}
