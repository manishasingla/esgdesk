﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AdminProjectNoteList.aspx.cs" Inherits="AdminProjectNoteList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<link href="StyleSheet.css" rel="stylesheet" type="text/css" />
<link href="DashStyle.css" rel="stylesheet" type="text/css" /> 
    <title>Project note list</title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../DashStyle.css" rel="stylesheet" type="text/css" />
    <style>
     #nav
        {
            font-family: "Lucida Grande" ,Tahoma,Arial,Verdana,sans-serif;
            font-weight: bold;
            font-size: 11px;
            width: 150px;
        }
        #nav ul
        {
            margin: 0px;
            padding: 0px;
        }
        #nav li
        {
            list-style: none;
        }
        
        ul.top-level
        {
            background: #F8F8F8;
        }
        ul.top-level li
        {
            border: #D4D4D4 solid;
            border-width: 1px;
            border-radius: 4px 4px 4px 4px;
            padding: 5px 0px 6px;
        }
        #nav ul.sub-level
        {
            border: 1px solid #D4D4D4;
            border-radius: 4px 4px 4px 4px; /* padding: 5px 12px 6px;*/
            width: 147px;
        }
        #nav a
        {
            font-family: "Lucida Grande" ,Tahoma,Arial,Verdana,sans-serif;
            font-weight: bold;
            color: #000000;
            cursor: pointer;
            display: block;
            line-height: 12px; /* height: 25px;
            line-height: 25px;
            text-indent: 10px;*/
            text-decoration: none;
            width: 100%;
        }
        #nav a:hover
        {
            text-decoration: underline;
        }
        
        #nav li:hover
        {
            background: #00A651;
            position: relative;
        }
        ul.sub-level
        {
            display: none;
        }
        li:hover .sub-level
        {
            background: #F8F8F8;
            border: #D4D4D4 solid;
            border-width: 1px;
            display: block;
            position: absolute; /* left: 75px;*/
            top: 23px;
            border-radius: 4px 4px 4px 4px;
            padding: 5px 12px 6px;
            left: 0px;
        }
        ul.sub-level li
        {
            border: none;
            float: left;
            width: 148px;
        }
        
        #nav .sub-level
        {
            background: #FFFFFF;
        }
    </style> 
</head>
<body>
    <form id="form1" runat="server">
            <div>
        <div>
            <div id="TicketSummary">
                <div style="width: 20%; float: left">
                    <asp:Label ID="lblcategoryname" runat="server" Style="font-weight: bolder" Text="Category"></asp:Label>
                    <div id="nav" style="float: right">
                        <ul class="top-level">
                            <li style="text-align: center;"><a id="addcategory" runat="server" target="_self">Add
                                category</a>
                                <%--  <ul class="sub-level">
                                    <li><a>New</a></li>
                                    <li><a>Solutions</a></li>
                                    <li><a>View</a></li>
                                </ul>--%>
                                <asp:ListView ID="lstcategory" runat="server">
                                    <LayoutTemplate>
                                        <ul class="sub-level">
                                            <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                        </ul>
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <li>
                                            <%--<a href="Product.aspx?scid=<%#Eval("category_id")%>" class="top_link">
                                            <%#Eval("category_name")%></a>--%>
                                            <center>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:LinkButton ID="btninsert" runat="server" Text='<%#Eval("category_name")%>' OnClick="btninsert" />
                                                    </td>
                                                    <td >
                                                        <span style="color:Black">(<%#Eval("counts")%>)</span>
                                                    </td>
                                                </tr>
                                            </table>
                                            </center>
                                        </li>
                                    </ItemTemplate>
                                </asp:ListView>
                            </li>
                        </ul>
                    </div>
                    <br>
                    <asp:Label ID="lblmsg" runat="server" Style="color: Red"></asp:Label>
                    <br>
                    <hr />
                    <asp:ListView ID="Folderlstview" runat="server" OnItemDataBound="OnItemDataBound_Folderlstview">
                        <LayoutTemplate>
                            <div>
                                <ul style="height: auto; list-style: disc outside none">
                                    <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                </ul>
                            </div>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <li>
                            <asp:CheckBox ID="chkboxCategory" runat="server" Enabled="false"  Checked=<%#bool.Parse(Eval("AddCategoryStatus").ToString())%> AutoPostBack="true" />&nbsp;
                            <a id="ChangeColor" runat="server"  href='<%# "AdminProjectNoteList.aspx?projectname="+ HttpUtility.UrlEncode(Eval("Project_Name").ToString())+ "&folderid=" + Eval("folder_id")%>' style="margin-left: 10px;"   target="_self">
                                <%#Eval("foldername")%>
                                (<%# Eval("counts")%>)</a> </li>
                        </ItemTemplate>
                    </asp:ListView>
                </div>
                <div style="width: 79%; float: right; background: #EBEBEB;">
                    <div style="padding: 4px; border-bottom: 1px solid;">
                        <%--<a href="Solution.aspx" style="text-decoration: none">Solution</a> →--%>
                        <a id="lblPcatrgoryname" runat="server" target="_self"></a>
                        <asp:Label ID="lblPfoldername" runat="server"></asp:Label>
                    </div>
                    <br />
                    <div id="divaction" runat="server" style="height: 50px; padding: 4px">
                        <div style="float: left">
                            <asp:Label ID="lblFN" runat="server" Style="font-weight: bolder;"></asp:Label></div>
                        <div id="addedit" runat="server" style="float: right">
                            <a class="submit" id="addsolu" runat="server" target="_self">Add Note</a> <a class="submit"
                                id="editfolder" runat="server" target="_self">Edit</a>
                        </div>
                    </div>
                    <div style="background: white">
                        <br />
                    </div>
                    <div style="background: white; padding: 4px">
                        <asp:GridView ID="gridlist" runat="server" AutoGenerateColumns="false" ShowHeader="false" Width="100%">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <div style="padding: 2px;">
                                        <asp:CheckBox  ID="idnotestatus" Enabled="false"  runat="server" Checked=<%#bool.Parse(Eval("AddNoteStatus").ToString())%>   />&nbsp;
                                            <asp:Label ID="lblRcount" runat="server" Text='<%#Eval("RowNumber") %>' ></asp:Label>.&nbsp;
                                            <a id="gvanchor" runat="server"  href='<%# "AdminProjectDetail.aspx?projectname="+HttpUtility.UrlEncode(Eval("Project_Name").ToString())+ "&folderid="+ Eval("folder_id")+"&solutionid="+Eval("Note_Id") %>' target="_self">
                                                <%# Eval("NoteName")%>
                                            </a>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div id="addsolution" runat="server" style="text-align: center" visible="false">
                        <div class="no-info-text">
                            Add notes in this category
                        </div>
                        <a class="submit" id="btnaddsolution" runat="server" target="_self">Add Note</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
