<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Admin_Header.ascx.cs"
    Inherits="Admin_Admin_Header" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:ScriptManager ID="ScriptManager1" EnablePageMethods="true" runat="server">
</asp:ScriptManager>
<link rel="stylesheet" href="../css/newmenuesg.css" type="text/css" media="screen"
    charset="utf-8" />
<script src="http://www.google.com/jsapi"></script>
<script>    google.load("mootools", "1.2.1");</script>
<!-- Load the MenuMatic Class -->
<%--<script src="../js/MenuMatic_0.68.3.js" type="text/javascript" charset="utf-8"></script>--%>
<!-- Create a MenuMatic Instance -->
<%--<script type="text/javascript">
    window.addEvent('domready', function () {
        var myMenu = new MenuMatic();
    });		
</script>--%>
<!-- begin google tracking code -->
<script type="text/javascript">
    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
    var pageTracker = _gat._getTracker("UA-2180518-1");
    pageTracker._initData();
    pageTracker._trackPageview();
</script>
<!-- end google tracking code -->
<script type="text/javascript" src="../firstword.js"></script>
<script type="text/javascript" src="../func.js"></script>
<script src="SpryMenuBar.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
    function openPopup(flag) {

        if (flag == 1) {
            document.getElementById('frameQuickQuestion').style.display = "block";
            document.getElementById('frameCallTaken').style.display = "none";
            document.getElementById('frameQuickQuestion').src = ' quick_question.aspx';

        }
        else if (flag == 2) {
            document.getElementById('frameQuickQuestion').style.display = "none";
            document.getElementById('frameCallTaken').style.display = "block";
            document.getElementById('frameCallTaken').src = 'call_taken.aspx';

        }

        document.getElementById('Admin_Header1_blnClick').click();
    }
</script>
<script type="text/javascript" language="javascript">
    function GotoTask() {


        document.getElementById('Admin_Header1_lnkTasks').click();

    }
</script>
<script type="text/javascript" language="javascript">
    function GotoCurrentTask() {
        document.getElementById('Admin_Header1_LnkCrntlywrkTask').click();

    }
</script>
<script type="text/javascript" language="javascript">
    function GotoCurrentTaskstatus() {
        document.getElementById('Admin_Header1_LnkCrntlywrkstatus').click();

    }
</script>

<script type="text/javascript" language="javascript">
    function GotoCurrentcwostatus() {
        document.getElementById('Admin_Header1_LnkCrntlystatus').click();
        
    }
</script>

<script type="text/javascript" language ="javascript">
    function Gotoallempscurrently() {
        document.getElementById('Admin_Header1_LnkCrntly').click();
    }
</script>
<style type="text/css">
.SolutionLogo{position: relative; left: 30px; top: 1px;}
</style>

<link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link href="SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<div class="divBorder">
    <div>
        <table width="0" cellpadding="0" cellspacing="0" style="width: 100%; background: #ffffff;">
            <tr>
                <td height="80" align="left" valign="middle" class="yellowtext1" style="padding-left: 20px">
                    <img class="imagemargin" src="../../images/esg-desklogo.png" />
                    
                    <span id="pageHeader">
                        <asp:Label ID="lblTitle" runat="server" Text="Client Requests" CssClass="firstword"></asp:Label></span>

                        <a href="Solution.aspx" class="SolutionLogo"><img src="../../images/esg_solution_logo.jpg" /></a>
                </td>
                <td align="right" valign="bottom" style="height: 57px; padding-right: 10px;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td height="45" align="right" valign="middle">
                                <div id="divHeader1" runat="server" align="right" class="style1_bg">
                                    <a id="lnkLogin2" runat="server" href="../login.aspx"></a>
                                    <asp:LinkButton ID="LinkButton1" runat="server" Visible="false" CausesValidation="false"
                                        OnClick="LinkButton1_Click">Download</asp:LinkButton>
                                    &nbsp;<a id="lnkLogin" runat="server" href="../login.aspx"><img border="0" height="24"
                                        src="../../images/clientlogin_icon.png" />&nbsp;Client Login</a>&nbsp;<img src="../../images/support_icon.png"
                                            height="22" border="0" />&nbsp;<a id="lnkAdminLogin" runat="server" href="login.aspx">Support
                                                login</a>&nbsp;<span class="MenuBarHorizontal"> </span>&nbsp;<a id="lnkRegister"
                                                    style="display: none;" runat="server" href="../register.aspx">Register</a>
                                    <a id="A1" runat="server" href="http://www.logmein123.com" target="_blank">Remote desktop</a>
                                </div>
                               
                                <span class="style1_bg"><a id="lnkPriority_report" runat="server" href="../admin/Priority_reports.aspx"
                                    style="display: none">Task priority report</a> &nbsp; <a id="lnkunreadtask" runat="server"
                                        href="../admin/UnreadTask.aspx">Unread Task</a> <a id="lnkLogin4" runat="server"
                                            href="../login.aspx">
                                            <img src="../../images/support_icon.png" height="22" border="0" />&nbsp;</a>
                                    <a id="lnkUserName" style="border-bottom: 1px dotted #000;" runat="server" target="_blank">
                                    </a>&nbsp;&nbsp;|<asp:Label ID="lbltodaysDatetime" runat="server"></asp:Label>
                                    <asp:Button ID="lnkLogout" runat="server" Text="Logout" CausesValidation="False" OnClick="lnkLogout_Click" CssClass="blueBtns" />
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td height="28" align="right" valign="bottom">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="left" valign="middle">&nbsp;
                                            
                                        </td>
                                        <td width="400" align="center" valign="middle">
                                        </td>
                                        <td style="margin-bottom: 5px;" width="120" align="left" valign="middle">
                                            <a class="addthis_button" style="text-align: left;" href="http://www.addthis.com/bookmark.php?v=250&amp;pub=esatesolutions">
                                                <img src="http://s7.addthis.com/static/btn/lg-share-en.gif" alt="Bookmark and Share"
                                                    width="125" height="16" align="right" style="border: 0" /></a>
                                        </td>
                                    </tr>
                                </table>
                                <a href="http://www.esgdesk.com//Download/ESG_Desk.application"></a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left" valign="middle" bgcolor="#00a651">
                </td>
                <td align="right" valign="bottom" bgcolor="#00a651" style="height: 5px;">
                </td>
            </tr>
        </table>
    </div>
</div>
<div style="text-align: left; vertical-align: bottom; padding: 0px;" id="divHeader2"
    runat="server">
    <!-- /login -->
    <div id="container">
        <div id="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background: #3e3f3d;">
                <tr>
                    <td align="left">
                     <div class="menu" style="float: left;">
                    <ul>
                     <li><a href="Dashboard.aspx" id="A7" runat="server" style="background-color: Transparent;border:none" >My dashboard</a></li></ul>
                    </td>
                    </div>
                    <td height="32" style="min-width: 600px;">
                        <div class="menu">
                            <ul>
                                <li id="li_cr" runat="server" ><a style="padding-right: 8px;" href="/Admin/client_requests.aspx">
                                    <asp:Button ID="lnkClientRequests" runat="server" CausesValidation="false" BackColor="transparent"
                                        ForeColor="#FFFFFF" Text="Client requests " BorderStyle="none" Style="background-color: Transparent;
                                        border-style: none; color: #F3F3F3 !important; cursor: pointer; font-family: helvetica;
                                        font-size: 12px; font-weight: bold; margin-top: 5px; width: 105px;" OnClick="lnkClientRequests_Click" /></a>
                                    <ul style="margin-left: 20px; width: 179px;">
                                        <li><a href="../Admin/add_changeRequest.aspx" class="documents">New</a></li>
                                    </ul>
                                </li>
                                 <li id="li_NewRequest" runat="server" visible="false" ><a style="padding-right: 8px;" href="../Admin/client_requests_emps.aspx">
                                    <asp:Button ID="Button1" runat="server" CausesValidation="false" BackColor="transparent"
                                        ForeColor="#FFFFFF" Text="Client requests " BorderStyle="none" Style="background-color: Transparent;
                                        border-style: none; color: #F3F3F3 !important; cursor: pointer; font-family: helvetica;
                                        font-size: 12px; font-weight: bold; margin-top: 5px; width: 105px;" OnClick="lnkClientRequestsemp_Click" /></a>
                                    
                                </li>
                                
                                
                                
                                
                                
                                
                                
                                <li><a href="#" id="lnkTasks2" runat="server" style="background-color: Transparent"
                                    onclick="GotoTask();">Tasks</a>
                                    <ul>
                                        <li><a class="documents" href="../Admin/edit_task.aspx">New</a></li>
                                         <li>
                                        <%--<a id="lisolu" runat="server" class="solutions" href="../Solution.aspx">Solutions</a>--%>
                                        <a id="lisolu" runat="server" class="solutions" href="~/Admin/Solution.aspx">Solutions</a>
                                        <%--<a id="lisolu" runat="server" class="solutions" href="~/Admin/Solutionslist.aspx">Solutions</a>--%>
                                        </li>
                                        <li><a class="messages" href="#">View</a>
                                            <ul>
                                                <li><a class="messages" href="cwo.aspx" style="width: 125px" onclick="GotoCurrentTask();">Currently working on </a></li>
                                                 <li><a class="messages" href="#" style="width: 125px" onclick="GotoCurrentTaskstatus();">Check Status</a></li>
                                                 <li><a class="messages" href="user_status.aspx" style="width: 125px" onclick="GotoCurrentcwostatus();">Required CWO List</a></li>
                                                 <li><a class="messages" href="all-emps-currently.aspx" style="width: 125px" onclick="Gotoallempscurrently();">All emps currently</a></li>
                                            </ul>
                                        </li>
                                       
                                    </ul>
                                </li>
                                <li id="li_admin" runat="server"><a href="admin.aspx" id="lnkAdmin" runat="server">Admin</a>
                                    <ul>
                                        <li><a href="../Admin/GoogleAcList.aspx" class="analytics">Analytics</a></li>
                                        <li><a href="../Admin/categories.aspx" class="catagories">Categories</a></li>
                                        <li><a href="../Admin/client_details.aspx" class="clients" >Clients</a></li>
                                        <li><a href="../Admin/Company.aspx" class="companies">Companies</a></li>
                                        <li><a href="#" onclick="window.open('/Admin/Clientsendemail.aspx','','status=yes')" class="bulk_mail">Clients:Bulk email</a></li>
                                        <li><a href="#" onclick="window.open('/Admin/Customer_Support.aspx','','status=yes,width=760,height=560')" class="task_priority">CR Canned Message</a></li>
                                        
                                        
                                        <li><a href="../Admin/users.aspx" class="emplyees">Employees</a></li>
                                        <li><a href="#" onclick="window.open('../Admin/sendemail.aspx','','status=yes')" class="bulk_mail">Emps:Bulk email</a></li>
                                        <li><a style="height: 40px; line-height: 18px!important;" href="../Admin/LMSAdmin/LMS-Admin.aspx" class="leave_management">
                                            Leave Management System</a> </li>     
                                         <li><a href="#" class="manage_faq">Manage FAQ/Glossary</a>
                                         <ul>
                                        <li><a href="../Admin/Add-faq.aspx" id="A8" runat="server"  class="faq">Manage FAQ</a></li>
                                        <li><a href="../Admin/Add-glossary.aspx" id="A10" runat="server" class="glossary_icon">Manage Glossary</a></li>
                                        </ul>
                                        </li>
                                        
                                        
                                        <li><a href="../Admin/Notes_categories.aspx" class="notes">Notes categories</a></li>
                                        <li><a href="../Admin/newslist.aspx" class="news">News</a></li>
                                        <li><a href="../Admin/priorities.aspx" class="priority">Priorities</a>
                                            <ul>
                                                <li><a href="#" class="cr_priority">CR priorities</a></li>
                                                <li><a href="#" class="task_priority">Task priorities</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="../Admin/projects.aspx" class="projects">Projects</a>
                                        <ul>
                                        <li><a href="../Admin/AddsolutionCategory.aspx" id="A11" class="default_sol">Default solutions</a></li>
                                         </ul>
                                        </li>
                                        <li><a href="../Admin/Resources.aspx" class="resource">Resources</a></li>
                                        <li><a href="../Admin/Subscription_details.aspx" class="subscriptions">Subscriptions</a></li>
                                        <li><a href="../Admin/statuses.aspx" class="status">Statuses</a></li>
                                        <li><a href="../Admin/TypeList.aspx" class="support">Support Types</a></li>
                                        <%--===================================================--%>
                                        <%--<li><a href="sendemail.aspx">Emps:Bulk email</a></li>
                                        <li><a href="Clientsendemail.aspx">Clients:Bulk email</a></li>--%>
                                        
                                         
                                        <%--===================================================--%>
                                        <li><a href="#" onclick="window.open('/Admin/Task_Canned_message.aspx','','status=yes,width=760,height=560')" class="task_canned">
                                            Task Canned Message</a></li>
                                        
                                                           
                    <%--   <ul>   
                              <li style="width:200px;"><a href="/Admin/LMSAdmin/frmLeaveSetting.aspx" class="settings">Leave setting</a></li>
                              <li style="width:200px;"><a href="/Admin/LMSAdmin/EmployeeList.aspx?id=VL" class="view_leave">View leave</a></li>
                              <li style="width:200px;"><a href="/Admin/LMSAdmin/EmployeeList.aspx?id=AL"  class="add_leave">Add leave</a></li>
                              <li style="width:200px;"><a href="/Admin/LMSAdmin/EmployeeList.aspx?id=EL" class="edit_leave">Edit leave</a></li>  
                              <li style="width:200px;"><a href="/Admin/LMSAdmin/frmMonthlyLeaveTransactions.aspx" class="leave2days">Employee leave between two dates</a></li>
                              <li style="width:200px;"><a href="/Admin/LMSAdmin/frmMonthlyLeaveTransactionByEmp.aspx" class="documents">Employee leave details Between Two Dates</a></li>
                              <li style="width:200px;"><a href="/Admin/LMSAdmin/frmLeaveTransactionsLast10Days.aspx" class="leave10days">Employee absent in last 10 days</a></li>  
                              <li style="width:200px;"><a href="/Admin/LMSAdmin/frmWhoIsOnLeaveToady.aspx" class="leave_report">Employees on leave today</a></li>  
                              <li style="width:200px;"><a style="height:40px; line-height:20px;" href="/Admin/LMSAdmin/frmEmpLeavesReport.aspx" class="leave_report">Employee's complete leave report</a></li>  
                      
                        </ul>--%>
                    
                                           
                                           
                                       
                                     
                                    </ul>
                                </li>
                                <li><a href="#">User</a>
                                    <ul>
                                        <li><a href="../Admin/change_password.aspx" id="A4" runat="server" class="password">Change password</a></li>
                                         <li><a href="../Admin/frmManageHolidays.aspx" id="A6" runat="server" class="national_holidays">National Holidays</a></li>
                                         <li><a href="../Admin/EODC3.aspx" id="A3" runat="server" target="_blank" class="send_eodc">Send EODC</a></li>
                                         <li><a href="../Admin/SODC.aspx" id="A13" runat="server" class="send_eodc">Send SODC</a></li>
                                         <li><a href="../Admin/settings.aspx" id="lnkSettings" runat="server" class="settings">Settings</a></li>
                                         <li><a href="../Admin/ViewEODC.aspx" id="A12" runat="server" class="documents">View EODC</a></li>
                                          <li><a href="../Admin/ShowSODC.aspx" id="A14" runat="server" class="documents">View SODC</a></li>
                                          
                                       
                                       <%-- <li><a href="../Admin/EsgGallery/Default.aspx" id="A8" runat="server">ESG Gallery</a></li>--%>
                                    </ul>
                                </li>
                                 <li><a href="../Admin/EsgGallery/Default.aspx" >Gallery</a>
                                    <ul>
                                        <li><a href="/Admin/EsgGallery/createalbum.aspx" id="A9" runat="server" visible="false" class="gallery">Create Album</a></li>
                                    </ul>
                                </li>
                               
                                <li id="li1" runat="server"><a href="#" id="A5" runat="server">More</a>
                                    <ul>
                                        
                                        <li><a href="#" id="A2" onclick="openPopup(2);" runat="server" class="call_taken">Call taken</a></li>
                                        <li><a href="http://www.esgdesk.com//Download/ESG_Desk.application" class="desk_application">Desktop application</a></li>
                                         <li><a href="/Admin/Glossary.aspx" class="glossary_icon">Glossary</a></li>
                                        <li><a href="/Admin/Faq.aspx" class="faq">FAQ</a></li>
                                        <li><a href="#" id="lnkQuickQuestion" onclick="openPopup(1);" runat="server" class="quick_question">Quick question</a></li>
                                       
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </td>
                    <td align="right" style="width: 30px;">
                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"
                            TargetControlID="txtTaskId">
                        </cc1:FilteredTextBoxExtender>
                        <cc1:FilteredTextBoxExtender ID="Filteredtextboxextender2" runat="server" FilterType="Numbers"
                            TargetControlID="txtRequestId">
                        </cc1:FilteredTextBoxExtender>
                    </td>
                </tr>
            </table>
            <!-- login -->
            <!-- / login -->
        </div>
        <!-- / top -->
        <div class="clearfix">
        </div>
        <!-- / content -->
        <div class="clearfix">
        </div>
    </div>
    <!-- / container -->
    <div style="background: url(../images/quickserch.png) no-repeat left top; display: none;">
        <span style="font-size: 12px"></span>
        <table>
            <tr>
                <td valign="bottom" style="padding-bottom: 4px;">
                    <table id="tblSearchRequest" runat="server" border="0" cellpadding="0" cellspacing="2">
                        <tr>
                            <td>&nbsp;
                                
                            </td>
                            <td style="min-width: 70px; color: White; font-size: 14px">
                                Request ID:
                            </td>
                            <td class="whitetext2" width="40">
                                <asp:TextBox ID="txtRequestId" runat="server" Width="100%" Height="15px" MaxLength="10"
                                    Style="min-width: 20px;"></asp:TextBox>
                            </td>
                            <td style="color: White; font-size: 14px">
                                <asp:Button ID="btnGo" runat="server" Text="Go" CausesValidation="False" OnClick="btnGo_Click"
                                    CssClass="goBtn" />
                            </td>
                        </tr>
                    </table>
                </td>
                <td valign="bottom" style="padding-bottom: 4px;">
                    <table cellpadding="0" cellspacing="2">
                        <tr>
                            <td>
                            </td>
                            <td style="min-width: 40px; color: White; font-size: 14px">
                                Task ID
                            </td>
                            <td width="40">
                                <asp:TextBox ID="txtTaskId" runat="server" Width="100%" Height="15px" MaxLength="10"
                                    Style="min-width: 20px;"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="btnGotoTask" runat="server" Text="Go" CausesValidation="False" OnClick="btnGotoTask_Click"
                                    CssClass="goBtn" />
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="55%" style="padding-left: 3px; padding-bottom: 5px">
                    <table id="Tblsearchbyword" runat="server" border="0" cellpadding="0" cellspacing="2">
                        <tr>
                            <td align="left" colspan="2" style="color: White; font-size: 14px">
                                Search by word:
                                <asp:CheckBox ID="ChkTask" runat="server" Text="Task" />
                                <asp:CheckBox ID="ChkCrs" runat="server" Text="CRs" />
                                <asp:CheckBox ID="ChkIncludecomment" runat="server" Text="Include comments" />
                            </td>
                        </tr>
                        <tr style="padding-top: 2px">
                            <td width="100%">
                                <asp:TextBox ID="txtWord" runat="server" Width="100%" Height="15px"></asp:TextBox>
                            </td>
                            <td width="37" align="right">
                                <asp:Button ID="BtnGoWrdSearch" runat="server" Text="Go" CssClass="goBtn" CausesValidation="False"
                                    OnClick="BtnGoWrdSearch_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</div>
<asp:Panel ID="Panel1" runat="server" Width="755px" Style="display: none">
    <table border="0" cellspacing="0" cellpadding="0" width="100%" style="background-color: #ffffff;
        float: left">
        <tr>
            <td align="right" style="padding: 5px;">
                <asp:LinkButton ID="lnkClose" runat="server" Style="color: #811007; font-size: 16px;
                    font-weight: bolder; text-decoration: underline;" OnClick="lnkClose_Click"><img src="../images/close_icon.png" width="16" height="16" border="0"  /></asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td align="center" valign="top">
                <iframe id="frameQuickQuestion" src="" height="440"; width="745" frameborder="0" scrolling="no" ></iframe>
            <iframe id="frameCallTaken" src="" height="550"; width="745" frameborder="0" scrolling="no" ></iframe>
            </td>
        </tr>
    </table>
</asp:Panel>
<cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalBackground"
    CancelControlID="lnkClose" DropShadow="true" PopupControlID="Panel1" TargetControlID="blnClick">
</cc1:ModalPopupExtender>
<asp:Button ID="blnClick" runat="server" Text="Button" Style="display: none;" CausesValidation="false" />
<asp:Button ID="lnkTasks" runat="server" BackColor="transparent" BorderStyle="none"
    Style="display: none;" CausesValidation="false" OnClick="lnkTasks_Click" />
<asp:Button ID="LnkCrntlywrkTask" runat="server" BackColor="transparent" BorderStyle="none"
    Style="display: none;" CausesValidation="False" OnClick="LnkCrntlywrkTask_Click" />
<asp:Button ID="LnkCrntlywrkstatus" runat="server" BackColor="transparent" BorderStyle="none"
    Style="display: none;" CausesValidation="False" OnClick="LnkCrntlywrkstatus_Click" />
<!-- Login -->
<script type="text/javascript">
<!--
    var MenuBar1 = new Spry.Widget.MenuBar("MenuBar1", { imgDown: "SpryMenuBarDownHover.gif", imgRight: "SpryMenuBarRightHover.gif" });
//-->
</script>
