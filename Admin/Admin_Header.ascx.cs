using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net;
using System.Text;
using System.IO;
using IWshRuntimeLibrary;

public partial class Admin_Admin_Header : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lbltodaysDatetime.Text = DateTime.Today.ToString("ddd") + " " + DateTime.Today.ToString("dd/MM/yyyy") + " " + DateTime.Now.ToString("HH") + ":" + DateTime.Now.ToString("mm");
        if (!Page.IsPostBack)
        {
            if (Session["Task_Canned_msg"] == null || Session["Task_Canned_msg"].ToString() == "")
            {

            }
            else
            {
                Session.Remove("Task_Canned_msg");
            }


            if (Session["admin"] == null)
            {
                
                lnkUserName.Visible = false;
                divHeader2.Visible = false;
                lblTitle.Text = "Change Request System";
                lnkunreadtask.Visible = false;
                return;
            }
            else
            {
                
                string o_url = Request.Url.AbsolutePath.ToString();
                char[] splitter = { '/' };
                string[] urlInfo = o_url.Split(splitter);
                int index_no = urlInfo.Length - 1;
                string url = urlInfo[index_no];

                switch (url)
                {
                    case "tasks.aspx":
                        lblTitle.Text = "Tasks";
                        break;
                    case "admin.aspx":
                        lblTitle.Text = "Admin";
                        break;
                    case "Subscription_details.aspx":
                        lblTitle.Text = "Subscription details";
                        break;

                    case "login.aspx":
                        lblTitle.Text = "Login";
                        break;

                    case "categories.aspx":
                        lblTitle.Text = "Categories";
                        break;

                    case "Company.aspx":
                        lblTitle.Text = "Company";
                        break;

                    case "GoogleAcList.aspx":
                        lblTitle.Text = "Google Analytics";
                        break;

                    case "Notes_categories.aspx":
                        lblTitle.Text = "Notes categories";
                        break;

                    case "priorities.aspx":
                        lblTitle.Text = "Priorities";
                        break;

                    case "projects.aspx":
                        lblTitle.Text = "Projects";
                        break;

                    case "Resources.aspx":
                        lblTitle.Text = "Resources";
                        break;

                    case "client_details.aspx":
                        lblTitle.Text = "Client details";
                        break;

                    case "users.aspx":
                        lblTitle.Text = "Users";
                        break;

                    case "statuses.aspx":
                        lblTitle.Text = "Statuses";
                        break;

                    case "TypeList.aspx":
                        lblTitle.Text = "Support Type";
                        break;

                    case "edit_task.aspx":
                        lblTitle.Text = "Edit task";
                        break;

                    case "Related.aspx":
                        lblTitle.Text = "Related Projects";
                        break;

                    case "CompanyRelated.aspx":
                        lblTitle.Text = "Company Related";
                        break;

                    case "change_password.aspx":
                        lblTitle.Text = "Change Password";
                        break;

                    case "settings.aspx":
                        lblTitle.Text = "Settings";
                        break;

                    case "add_changeRequest.aspx":
                        lblTitle.Text = "Change Request";
                        break;

                    case "edit_comment.aspx":
                        lblTitle.Text = "Edit Comment";
                        break;

                    case "Client_Subscription.aspx":
                        lblTitle.Text = "Client Subscription";
                        break;

                    case "edit_company.aspx":
                        lblTitle.Text = "Edit Company";
                        break;

                    case "AddGoogleAc.aspx":
                        lblTitle.Text = "Edit Google Analytics";
                        break;

                    case "edit_project.aspx":
                        lblTitle.Text = "Edit Project";
                        break;

                    case "edit_client.aspx":
                        lblTitle.Text = "Edit Client";
                        break;

                    case "Customer_Support.aspx":
                        lblTitle.Text = "Customer Support";
                        break;

                    case "Add_Task.aspx":
                        lblTitle.Text = "Add Task";
                        break;

                    case "Allow_Remote_Desktop.aspx":
                        lblTitle.Text = "Allow Remote Desktop";
                        break;

                    case "Analytics.aspx":
                        lblTitle.Text = "Analytics";
                        break;

                    case "call_taken.aspx":
                        lblTitle.Text = "Call Taken";
                        break;

                    case "categories_old.aspx":
                        lblTitle.Text = "Categories Old";
                        break;

                    case "client_requests.aspx":
                        lblTitle.Text = "Client Requests";
                        break;

                    case "edit_user.aspx":
                        lblTitle.Text = "Edit User";
                        break;

                    case "edit_request.aspx":
                        lblTitle.Text = "Edit Request";
                        break;

                    case "esActivityLog.aspx":
                        lblTitle.Text = "ESActivity Log";
                        break;

                    case "subscribers.aspx":
                        lblTitle.Text = "subscribers";
                        break;

                    case "task_attachments.aspx":
                        lblTitle.Text = "Task Attachments";
                        break;

                    case "test.aspx":
                        lblTitle.Text = "Test";
                        break;

                    case "upload_attachment.aspx":
                        lblTitle.Text = "Upload Attachment";
                        break;

                    case "upload_task_attachment.aspx":
                        lblTitle.Text = "Upload Task Attachment";
                        break;

                    case "view_attachments.aspx":
                        lblTitle.Text = "View Attachments";
                        break;
                    case "WordSearchResult.aspx":
                        lblTitle.Text = "Word Search Result";
                        break;
                    case "Solution.aspx":
                        lblTitle.Text = "Solutions";
                        break;
                    case "Solutiondetail.aspx":
                        lblTitle.Text = "Solutions";
                        break;
                    case "Solutionslist.aspx":
                        lblTitle.Text = "Solutions";
                        break;
                    case "Addsolution.aspx":
                        lblTitle.Text = "Solutions";
                        break;
                    case "AddCategory.aspx":
                        lblTitle.Text = "Solutions";
                        break;
                    case "addcategoryfolder.aspx":
                        lblTitle.Text = "Solutions";
                        break;
                    case "newslist.aspx":
                        lblTitle.Text = "News";
                        break;
                    case "newsdetails.aspx":
                        lblTitle.Text = "News";
                        break;
                    case "AddNews.aspx":
                        lblTitle.Text = "News";
                        break;

                    case "Dashboard.aspx":
                        lblTitle.Text = "Dashboard";
                        break;

                    case "Default.aspx":
                        lblTitle.Text = "Gallery";
                        break;
                      
                    case "showalbum.aspx":
                        lblTitle.Text = "Gallery : Album";
                        break;

                    case "showphoto.aspx":
                        lblTitle.Text = "Gallery : Photo";
                        break;

                    case "slideshow.aspx":
                        lblTitle.Text = "Gallery : Slide show";
                        break;

                    case "createalbum.aspx":
                        lblTitle.Text = "Gallery : Create album";
                        break;

                    case "Add-faq.aspx":
                        lblTitle.Text = "Admin : Manage FAQ";
                        break;

                    case "Add-glossary.aspx":
                        lblTitle.Text = "Admin : Manage Glossary";
                        break;

                    case "Faq.aspx":
                        lblTitle.Text = "FAQ";
                        break;

                    case "Glossary.aspx":
                        lblTitle.Text = "Glossary";
                        break;

                    case "AddsolutionCategory.aspx":
                        lblTitle.Text = "Default Solutions";
                        break;


                    //case "LMS-admin.aspx":
                    //    lblTitle.Text = "Leave Management System : Home";
                    //    break;


                    //case "LMS-leave-setting.aspx":
                    //    lblTitle.Text = "Leave Management System : Leave setting";
                    //    break;


                    //case "LMS-Employees-list.aspx":
                    //    lblTitle.Text = "Leave Management System : Employees list";
                    //    break;


                    //case "LMS-Leaves-between-2dates-report.aspx":
                    //    lblTitle.Text = "Leave Management System : Employees leave report ,<br/> absent between two dates";
                    //    break;



                    //case "LMS-Complete-leaves-report.aspx":
                    //    lblTitle.Text = "Leave Management System : Employees complete<br/> leave report";
                    //    break;

                    default:
                        lblTitle.Text = "ESG DESK";
                        break;
                }
                divHeader1.Visible = false;
                lnkUserName.Visible = true;
                divHeader2.Visible = true;

                object objUserId = DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + Session["admin"].ToString() + "'");

                if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
                {
                    lnkUserName.InnerHtml = "Logged in as emp: " + objUserId.ToString();
                    lnkUserName.HRef = "../admin/Priority_reports.aspx?Ename=" + Session["admin"].ToString();
                    li_admin.Visible = false;
                    li_cr.Visible = false;
                    lnkunreadtask.Visible = false;
                    lnkPriority_report.Visible = false;
                   // lisolu.Visible = false;
                 
                }
                else
                {
                    lnkUserName.InnerHtml = "Logged in as admin: " + objUserId.ToString();
                    lnkUserName.HRef = "../admin/Priority_reports.aspx?Ename=" + Session["admin"].ToString();

                    li_admin.Visible = true;
                    li_cr.Visible = true;
                    lnkunreadtask.Visible = true;
                    lnkPriority_report.Visible = true;
                    A9.Visible = true;
                }


                if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
                {
                    lnkClientRequests.Visible = false;
                    ChkCrs.Visible = false;

                }
                else
                {
                    lnkClientRequests.Visible = true;
                    ChkCrs.Visible = true;

                }
                //==============================================================================
                //ClientRequest.deleted <> 1  and ClientRequest.status <> 'closed' and
                string qryforShowrequest = "Select * from NonesCRMusers where  ShowRequest IS NOT NULL and ShowRequest<>''";
                DataSet dsallnameniRequest=DatabaseHelper.getDataset(qryforShowrequest);  
                bool boolNameinShowrequest = false;
                if (dsallnameniRequest != null && dsallnameniRequest.Tables.Count > 0 && dsallnameniRequest.Tables[0].Rows.Count  > 0)
                {
                    for (int ireq = 0; ireq < dsallnameniRequest.Tables[0].Rows.Count; ireq++)
                    {
                        if(dsallnameniRequest.Tables[0].Rows[ireq]["ShowRequest"].ToString()!="")
                        {
                        string[] strToUser = dsallnameniRequest.Tables[0].Rows[ireq]["ShowRequest"].ToString().Split(',');
                        for (int ichkname = 0; ichkname < strToUser.Length; ichkname++)
                        {
                            if (strToUser[ichkname].ToUpper().ToString().Trim() == Session["admin"].ToString().ToUpper().Trim())
                            {
                                boolNameinShowrequest = true;
                                goto EndOfLoop;
                            }
                        }
                        
                        }

                    }EndOfLoop:;
                }

                if (boolNameinShowrequest == true)
                {
                    li_NewRequest.Visible = true;
                
                }
                //==============================================================================

                objUserId = DatabaseHelper.executeScalar("select user_id from users where username = '" + Session["admin"].ToString() + "'");

                lnkSettings.HRef = "settings.aspx?id=" + objUserId.ToString();


            }
        }
    }
    protected void lnkLogout_Click(object sender, EventArgs e)
    {
        HttpCookie aCookie;
        string cookieName;
        int limit = Request.Cookies.Count;
        for (int i = 0; i < limit; i++)
        {
            cookieName = Request.Cookies[i].Name;
            aCookie = new HttpCookie(cookieName);
            aCookie.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(aCookie);
        }
        Session.Remove("admin");
        Session["message"] = "You have been logged out successfully.";
        Response.Redirect("~/Admin/Login.aspx", false);
        //Response.Redirect("../confirmation.aspx", false);
    }
    protected void btnGo_Click(object sender, EventArgs e)
    {
        Response.Redirect("edit_request.aspx?reqid=" + txtRequestId.Text.Trim(), false);
    }
    protected void btnGotoTask_Click(object sender, EventArgs e)
    {
        Response.Redirect("edit_task.aspx?id=" + txtTaskId.Text.Trim(), false);
    }
    protected void lnkClose_Click(object sender, EventArgs e)
    {

    }
    protected void BtnGoWrdSearch_Click(object sender, EventArgs e)
    {
        if (ChkTask.Checked)
        {
            Session["TaskByWord"] = "true";
        }
        else
        {

            Session["TaskByWord"] = "false";
        }
        if (ChkCrs.Checked)
        {
            Session["CRsByWord"] = "true";
        }
        else
        {
            Session["CRsByWord"] = "false";
        }
        if (ChkTask.Checked == false && ChkCrs.Checked == false)
        {
            Session["TaskByWord"] = "true";

        }
        if (ChkIncludecomment.Checked)
        {
            Session["Includecomment"] = "true";
        }
        else
        {
            Session["Includecomment"] = "false";
        }

        Session["SearchWord"] = txtWord.Text.Trim();
        Response.Redirect("WordSearchResult.aspx", false);

    }

    protected void LnkCrntlywrkTask_Click(object sender, EventArgs e)
    {
        Session["filter"] = "CWO";
        Session["filterunread"] = "";
        Response.Redirect("tasks.aspx");

    }
    protected void LnkCrntlywrkstatus_Click(object sender, EventArgs e)
    {

        Response.Redirect("user_tasks_status.aspx");

    }


    protected void LnkCrntlystatus_Click(object sender, EventArgs e)
    {
     
        Response.Redirect("user_status.aspx");

    }
    protected void lnkClientRequests_Click(object sender, EventArgs e)
    {
        Session["filter"] = "";
        Session["filter"] = null;
        Session["reqHideClosed"] = "False"; ;
        Session["reqViewDeleted"] = "False";
        Session["reqChangedBy"] = null;
        Session["reqStatus"] = null;
        Session["reqPriority"] = null;
        Session["reqWebsiteURL"] = null;
        Session["reqOrderBy"] = null;
        Response.Redirect("client_requests.aspx", false);
    }
    protected void lnkClientRequestsemp_Click(object sender, EventArgs e)
    {
        Session["filter"] = "";
        Session["filter"] = null;
        Session["reqHideClosed"] = "False"; ;
        Session["reqViewDeleted"] = "False";
        Session["reqChangedBy"] = null;
        Session["reqStatus"] = null;
        Session["reqPriority"] = null;
        Session["reqWebsiteURL"] = null;
        Session["reqOrderBy"] = null;
        Response.Redirect("client_request_emps.aspx", false);
    }
    protected void lnkTasks_Click(object sender, EventArgs e)
    {
        Session["filter"] = "";
        Session["filter"] = null;
        Session["filterunread"] = "";
        Session["filterunread"] = null;
        Session["Status"] = null;
        Session["Status"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;


        Session["HideClosed"] = "False";
        Session["IncludeDeleted"] = "False";

        Session["Project"] = null;
        Session["Category"] = null;
        Session["ReportedBy"] = null;
        Session["Priority"] = null;
        Session["AssignedTo"] = null;
        Session["OrderBy"] = null;
        Session["ReadTask"] = "True";
        Response.Redirect("tasks.aspx", false);
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        try
        {
            string StrDesktopDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory).ToString();
            using (StreamWriter ShortcutWriter = new StreamWriter(StrDesktopDirectory + "\\Change_Request_System.url"))
            {
                ShortcutWriter.WriteLine("[InternetShortcut]");
                ShortcutWriter.WriteLine("URL=www.request.estatesolutions.eu");
                ShortcutWriter.Flush();

            }
        }
        catch (Exception ex)
        {
           LinkButton1.Text = ex.ToString();
        }
    }
    
}
