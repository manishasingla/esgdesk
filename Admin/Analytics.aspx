<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Analytics.aspx.cs" Inherits="Analytics" %>
<%@ Register Assembly="AjaxControls" Namespace="AjaxControls" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Analytics</title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>
     
   
    <div  style="width:450px;" >
    <div id="pageTitle" style="height:30px; width:100%; padding-left:6px"  runat="server">Analytics</div>
     
      <div id="DivNoAnalytics" runat="server" align="center" valign="middle" style="color:#062952; font-size:14px; height: 400px; vertical-align:middle">No record found. Please try again.</div>
     
          <div class="divBorder" id="showAnalytics" runat="server"  style="width:100%;  text-align:left; padding-left:6px" >
      
          <table width="100%"  style="background-color:Transparent; font-weight:bold; font-size:16px">
          <tr>
          <td width="100%" colspan="2" align="justify" valign="top" style="height: 430px"><div style="padding-right:10px;">
             

                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="middle"  background="images/report.gif"  height="80" ><table  width="100%" class="divBorder" border="0" cellspacing="0" cellpadding="0" align="center" >
                     
                        <tr style="display:none">
    <td height="37"  align="left" valign="middle" style="width: 52%" >From&nbsp; 
      <asp:DropDownList  ID="drpday" runat="server" style="width:40px;" AutoPostBack="True" 
            onselectedindexchanged="drpday_SelectedIndexChanged">
        <asp:ListItem value="01">01</asp:ListItem>
        <asp:ListItem value="02">02</asp:ListItem>
        <asp:ListItem value="03">03</asp:ListItem>
        <asp:ListItem value="04">04</asp:ListItem>
        <asp:ListItem value="05">05</asp:ListItem>
        <asp:ListItem value="06">06</asp:ListItem>
        <asp:ListItem value="07">07</asp:ListItem>
        <asp:ListItem value="08">08</asp:ListItem>
        <asp:ListItem value="09">09</asp:ListItem>
        <asp:ListItem value="10">10</asp:ListItem>
        <asp:ListItem value="11">11</asp:ListItem>
        <asp:ListItem value="12">12</asp:ListItem>
        <asp:ListItem value="13">13</asp:ListItem>
        <asp:ListItem value="14">14</asp:ListItem>
        <asp:ListItem value="15">15</asp:ListItem>
        <asp:ListItem value="16">16</asp:ListItem>
        <asp:ListItem value="17">17</asp:ListItem>
        <asp:ListItem value="18">18</asp:ListItem>
        <asp:ListItem value="19">19</asp:ListItem>
        <asp:ListItem value="20">20</asp:ListItem>
        <asp:ListItem value="21">21</asp:ListItem>
        <asp:ListItem value="22">22</asp:ListItem>
        <asp:ListItem value="23">23</asp:ListItem>
        <asp:ListItem value="24">24</asp:ListItem>
        <asp:ListItem value="25">25</asp:ListItem>
        <asp:ListItem value="26">26</asp:ListItem>
        <asp:ListItem value="27">27</asp:ListItem>
        <asp:ListItem value="28">28</asp:ListItem>
        <asp:ListItem value="29">29</asp:ListItem>
        <asp:ListItem value="30">30</asp:ListItem>
        <asp:ListItem value="31">31</asp:ListItem>
      </asp:DropDownList>
  &nbsp;
  <asp:DropDownList ID="drpmonth" runat="server" style="width:50px;" 
            AutoPostBack="True" onselectedindexchanged="drpmonth_SelectedIndexChanged">
    <asp:ListItem  value="01" >Jan</asp:ListItem>
    <asp:ListItem  value="02">Feb</asp:ListItem>
    <asp:ListItem  value="03">Mar</asp:ListItem>
    <asp:ListItem  value="04">Apr</asp:ListItem>
    <asp:ListItem  value="05">May</asp:ListItem>
    <asp:ListItem  value="06">Jun</asp:ListItem>
    <asp:ListItem  value="07">Jul</asp:ListItem>
    <asp:ListItem  value="08">Aug</asp:ListItem>
    <asp:ListItem  value="09">Sep</asp:ListItem>
    <asp:ListItem  value="10">Oct</asp:ListItem>
    <asp:ListItem  value="11">Nov</asp:ListItem>
    <asp:ListItem  value="12">Dec</asp:ListItem>
  </asp:DropDownList>
  &nbsp;
  <asp:DropDownList ID="drpyear" runat="server" AutoPostBack="True" 
            OnSelectedIndexChanged="drpyear_SelectedIndexChanged" >
    <%--<asp:ListItem>2010</asp:ListItem>
            <asp:ListItem>2009</asp:ListItem>
            <asp:ListItem>2008</asp:ListItem>
            <asp:ListItem>2007</asp:ListItem>
            <asp:ListItem>2006</asp:ListItem>
            <asp:ListItem>2005</asp:ListItem>--%>
  </asp:DropDownList>
    </td>
    <td  height="35"  valign="middle" width="48%">&nbsp; To&nbsp;
     <asp:DropDownList  ID="drpday2" runat="server" 
            style="width:40px;" onselectedindexchanged="drpday2_SelectedIndexChanged" 
            AutoPostBack="True">
       <asp:ListItem value="01">01</asp:ListItem>
  <asp:ListItem value="02">02</asp:ListItem>
  <asp:ListItem value="03">03</asp:ListItem>
  <asp:ListItem value="04">04</asp:ListItem>
  <asp:ListItem value="05">05</asp:ListItem>
  <asp:ListItem value="06">06</asp:ListItem>
  <asp:ListItem value="07">07</asp:ListItem>
  <asp:ListItem value="08">08</asp:ListItem>
  <asp:ListItem value="09">09</asp:ListItem>
  <asp:ListItem value="10">10</asp:ListItem>
  <asp:ListItem value="11">11</asp:ListItem>
  <asp:ListItem value="12">12</asp:ListItem>
  <asp:ListItem value="13">13</asp:ListItem>
  <asp:ListItem value="14">14</asp:ListItem>
  <asp:ListItem value="15">15</asp:ListItem>
  <asp:ListItem value="16">16</asp:ListItem>
  <asp:ListItem value="17">17</asp:ListItem>
  <asp:ListItem value="18">18</asp:ListItem>
  <asp:ListItem value="19">19</asp:ListItem>
  <asp:ListItem value="20">20</asp:ListItem>
  <asp:ListItem value="21">21</asp:ListItem>
  <asp:ListItem value="22">22</asp:ListItem>
  <asp:ListItem value="23">23</asp:ListItem>
  <asp:ListItem value="24">24</asp:ListItem>
  <asp:ListItem value="25">25</asp:ListItem>
  <asp:ListItem value="26">26</asp:ListItem>
  <asp:ListItem value="27">27</asp:ListItem>
  <asp:ListItem value="28">28</asp:ListItem>
  <asp:ListItem value="29">29</asp:ListItem>
  <asp:ListItem value="30">30</asp:ListItem>
  <asp:ListItem value="31">31</asp:ListItem></asp:DropDownList>
  &nbsp; <asp:DropDownList ID="drpmonth2" runat="server" style="width:50px;" 
            AutoPostBack="True" onselectedindexchanged="drpmonth2_SelectedIndexChanged" 
            >
  <asp:ListItem  value="01" >Jan</asp:ListItem>
  <asp:ListItem  value="02">Feb</asp:ListItem>
  <asp:ListItem  value="03">Mar</asp:ListItem>
  <asp:ListItem  value="04">Apr</asp:ListItem>
  <asp:ListItem  value="05">May</asp:ListItem>
  <asp:ListItem  value="06">Jun</asp:ListItem>
  <asp:ListItem  value="07">Jul</asp:ListItem>
  <asp:ListItem  value="08">Aug</asp:ListItem>
  <asp:ListItem  value="09">Sep</asp:ListItem>
  <asp:ListItem  value="10">Oct</asp:ListItem>
  <asp:ListItem  value="11">Nov</asp:ListItem>
  <asp:ListItem  value="12">Dec</asp:ListItem>
  </asp:DropDownList>&nbsp;
     <asp:DropDownList ID="drpyear2" runat="server" AutoPostBack="True" 
            onselectedindexchanged="drpyear2_SelectedIndexChanged">
       </asp:DropDownList>
   </td>
    
                        </tr>
                        <tr class="divBorder"><td style="width:52%;">
                       
                       <table width="100%"><tr><td valign="top"> From</td><td> <asp:Calendar ID="Calendar1" runat="server" Font-Size="11px" Height="84px" OnSelectionChanged="Calendar1_SelectionChanged"
                                ShowDayHeader="False" Width="168px"></asp:Calendar></td></tr></table>
                        
                     
                        </td><td style="width:48%;">
                        <table width="100%"><tr><td valign="top">  To</td><td> <asp:Calendar ID="Calendar2" runat="server" Font-Size="11px" Height="123px" OnSelectionChanged="Calendar2_SelectionChanged"
                                ShowDayHeader="False" Width="173px"></asp:Calendar></td></tr></table>
                      
                           
                        </td></tr>
  
    <tr><td colspan="2" align="right"  valign="middle" style="padding-bottom:7px; padding-right:7px"><asp:LinkButton ID="LinkButton4" runat="server" OnClick="Button1_Click" Font-Underline="true" Font-Size="small">Generate analytics</asp:LinkButton>      <%--<asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Report" />--%></td></tr>
</table></td>
  </tr>
</table>
       
                     
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
  <tr>
   <td  colspan="2" align="left" valign="middle" ><%--<asp:Button ID="btnfirst30" runat="server" onclick="btnfirst30_Click" 
                                                                    Text="First 30 days" />--%>
                                                                    <asp:LinkButton ID="LinkButton3" runat="server" Font-Underline="true" onclick="btnthis30_Click" 
                                                                    Font-Size="Small">Last 30 days</asp:LinkButton>
                                                               &nbsp;&nbsp;<span style="color:#3F72AB;">|</span>&nbsp;&nbsp; <asp:LinkButton ID="LinkButton2" Font-Underline="true" runat="server" 
                                                                    onclick="btnprevious_Click" Font-Size="Small">Previous 30 days</asp:LinkButton>
                                                                
                                                                <%--<asp:Button ID="btnprevious" runat="server" Text="Prev 30 days" 
                                                                    onclick="btnprevious_Click" />--%>&nbsp;&nbsp;<span style="color:#3F72AB;">|</span>&nbsp;&nbsp;<%--<asp:Button ID="btnthis30" runat="server" onclick="btnthis30_Click" 
                                                                    Text="Last 30 days" />--%> <asp:LinkButton ID="LinkButton1" Font-Underline="true" runat="server" onclick="btnfirst30_Click" 
                                                                    Font-Size="Small">First 30 days</asp:LinkButton>
            
                                                                &nbsp;&nbsp;<span style="color:#3F72AB;">|</span>&nbsp;&nbsp;<asp:LinkButton ID="LinkButton5" Font-Underline="true"  Font-Size="Small" runat="server" 
                                                                    onclick="LinkButton5_Click">All dates</asp:LinkButton>
            </td>
  </tr>
  <tr><td align="left" colspan="2" style="padding-top:10px; padding-bottom:10px"><asp:Label ID="lblmessage" Font-Size="small" runat="server" 
                                                        Text="Label"></asp:Label></td></tr>
</table>



                                                  
 
                                                    <ajaxToolkit:TabContainer ID="TabContainer1" runat="server">
                                                    
                                                    <ajaxToolkit:TabPanel ID="TabPanel3" runat="Server" HeaderText="Overview">
                        <ContentTemplate>
                        <div id="Overview" align="left" style="width:400px">
                          <table id="Table2" runat="server" width="100%" border="0" cellspacing="0" cellpadding="0">  
                                                
                            </table>
                          <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                    <br />
                    <br />
                    <asp:Label  ID="lblvisittext" runat="server" Text="Number of hits : "></asp:Label>
                    <asp:Label ID="Label1" runat="server" ></asp:Label>   
                    <br />
                    <br />     
                       <asp:Label ID="lblpageviewtext" runat="server" Text="Number of pageviews : "></asp:Label>
                       <asp:Label ID="lblPageViews" runat="server"></asp:Label>
                          <br />
                          <br /> 
                          <asp:Label ID="lblavgpageview" runat="server" Text="Average pageviews  :   "></asp:Label> 
                          <asp:Label ID="lblpagespervisit" runat="server"></asp:Label>
                              <asp:GridView
                ID="GridView1"  runat="server" AllowSorting="True" AutoGenerateColumns="False"
               
                BorderColor="#3F72AB"  BorderStyle="Solid" 
                BorderWidth="1px" CellPadding="3"                            
                            
                ShowFooter="True" AllowPaging="True" PageSize="15" onpageindexchanging="GrdDynamic_PageIndexChanging" 
                                                        onsorting="GrdDynamic_Sorting" >
                
                <FooterStyle BackColor="White" ForeColor="#000066" />
                <Columns>                  
                     
                         
                                  </Columns>
                <RowStyle ForeColor="Black" />
                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                <PagerStyle Font-Size="14px" HorizontalAlign="Left" />
                <HeaderStyle  Font-Bold="True" ForeColor="#3F72AB" />               
                <PagerSettings Mode="NumericFirstLast"  Position="TopAndBottom" />
                        </asp:GridView>           
                            </ContentTemplate>
                </asp:UpdatePanel>
                        </div>
                                                      </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="Server" HeaderText="Keywords searched">
                        <ContentTemplate>
                        <div id="GridView" align="left" style="width:400px">
                          <table id="TdNoGridProp" runat="server" width="100%" border="0" cellspacing="0" cellpadding="0">                                
                          <tr>
                                   <td colspan="2" align="center" valign="middle" style="color:#062952; font-size:14px; height: 200px;">No record found.</td>
                                </tr>
                            </table>
                          <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>                       
                              <asp:GridView
                ID="GrdDynamic"  runat="server" AllowSorting="True" AutoGenerateColumns="False" 
               
                BorderColor="#3F72AB"  BorderStyle="Solid" 
                BorderWidth="1px"  CellPadding="3"                            
                            
                ShowFooter="True" AllowPaging="True" PageSize="15" onpageindexchanging="GrdDynamic_PageIndexChanging" 
                                                        onsorting="GrdDynamic_Sorting">
                
                <FooterStyle BackColor="White" ForeColor="#000066" />
                <Columns>                  
                     
                         
                                  </Columns>
                <RowStyle ForeColor="Black" />
                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                <PagerStyle  Font-Size="14px" HorizontalAlign="Left" />
                <HeaderStyle  Font-Bold="True" ForeColor="#3F72AB" />               
                <PagerSettings Mode="NumericFirstLast"  Position="TopAndBottom" />
                        </asp:GridView>           
                            </ContentTemplate>
                </asp:UpdatePanel>
                        </div>
                          </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                      <ajaxToolkit:TabPanel ID="TabPanel2" runat="Server" HeaderText="Landing pages">
                        <ContentTemplate>
                        <div id="Pagenames" align="left" style="width:400px">
                          <table id="tblpages" runat="server" width="100%" border="0" cellspacing="0" cellpadding="0">                                
                            <tr>
                                   <td colspan="2" align="center" valign="middle" style="color:#062952; font-size:14px; height: 200px;">No record found.</td>
                                </tr>
                            </table>
                          <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>                       
                              <asp:GridView
                ID="gdvpages"  runat="server" AllowSorting="True" Width="400px" AutoGenerateColumns="False" 
               
                BorderColor="#3F72AB"  BorderStyle="Solid" 
                BorderWidth="1px" CellPadding="3"                            
                            
                ShowFooter="True" AllowPaging="True" PageSize="15" onpageindexchanging="gdvpages_PageIndexChanging" 
                                                        onsorting="gdvpages_Sorting">
                
                <FooterStyle BackColor="White" ForeColor="#000066" />
              <Columns>                  
                     
                         
                                  </Columns>
                <RowStyle ForeColor="Black" />
                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                <PagerStyle  Font-Size="14px" HorizontalAlign="Left" />
                <HeaderStyle  Font-Bold="True" ForeColor="#3F72AB" />               
                <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                        </asp:GridView>           
                            </ContentTemplate>
                </asp:UpdatePanel>
                        </div>
                          </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel4" runat="Server" HeaderText="Traffic sources">
                        <ContentTemplate>
                        <div id="Div2" align="left" style="width:400px">
                          <table id="tblwebsites" runat="server" width="100%" border="0" cellspacing="0" cellpadding="0">                                
                            <tr>
                                   <td colspan="2" align="center" valign="middle" style="color:#062952; font-size:14px; height:200px;">No record found.</td>
                                </tr>
                            </table>
                          <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>                       
                              <asp:GridView
                ID="Grdtraffic"  runat="server" AllowSorting="True" AutoGenerateColumns="False" 
               
                BorderColor="#3F72AB"  BorderStyle="Solid" 
                BorderWidth="1px" CellPadding="3"                            
                            
                ShowFooter="True" AllowPaging="True" PageSize="15" onpageindexchanging="Grdtraffic_PageIndexChanging" 
                                                        onsorting="Grdtraffic_Sorting">
                
                <FooterStyle BackColor="White" ForeColor="#000066" />
              <Columns>                  
                     
                         
                                  </Columns>
                <RowStyle ForeColor="Black" />
                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                <PagerStyle  Font-Size="14px"   HorizontalAlign="Left" />
                <HeaderStyle  Font-Bold="True" ForeColor="#3F72AB" />               
                <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                        </asp:GridView>           
                            </ContentTemplate>
                </asp:UpdatePanel>
                        </div>
                          </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                   
                                                   
                    </td></tr></table>
 
                                </div>
   
           
           </div>
          
           
                                    
   
    
    </form>
</body>
</html>
