﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CRNotes.aspx.cs" Inherits="CRNotes" %>

<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <title>CR notes</title>

    <script type="text/javascript" language="javascript">
            
                    
     function ActiveTabChanged(sender, e)
     {
     
      var index = $find("TabContainer1").get_activeTabIndex();
      document.getElementById("hdnCategories").value = index;
      //document.getElementById("BtnRdbCat").click();
     }
    </script>

    <script type="text/javascript" language="javascript">


function setClientCategories()
   {
	var hiddenClientCat = document.getElementById("hdnClientCategories");
	var chkbxs = document.getElementById("Repeater2").getElementsByTagName("input");
	hiddenClientCat.value = "";
	for(var i=0;i<chkbxs.length;i++)
	{
		if(chkbxs[i].checked==true) { hiddenClientCat.value += chkbxs[i].title + ";"; }
	}
  }




function SetClientCatOnload()
{

var hiddenClientCat = document.getElementById("hdnClientCategories");
var chkbxs = document.getElementById("Repeater2").getElementsByTagName("input");
if(hiddenClientCat.value !="")
{
var items = hiddenClientCat.value.split(";");
  for(var j=0;j<chkbxs.length;j++)
{


            for(var i=0;i<items.length-1;i++)
			{
			if(items[i] == chkbxs[j].title)
			{
		
			chkbxs[j].checked = true;
			
			}
			
			}
			
}
	
}

}

function ShowAllCat()
 {
 
 document.getElementById("TDAllCat").style.display ="block";
  document.getElementById("LnkHideTabArea").style.display ="Block";
   document.getElementById("LnkShowAllCat").style.display ="none";
  
 }
 function   HideAllCat()
 {
  
 document.getElementById("TDAllCat").style.display = "none";
document.getElementById("LnkHideTabArea").style.display ="none";
   document.getElementById("LnkShowAllCat").style.display ="block";
 }

function HideOnload()
{

 document.getElementById("TDAllCat").style.display ="none";
document.getElementById("LnkHideTabArea").style.display = "none";
}



    </script>

    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
</head>
<body onload="SetClientCatOnload(); HideOnload();" style="background-color: White">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="smanager" runat="server">
        </asp:ScriptManager>
        <div id="CompanyNote" runat="server" style="background-color: #ffffff; text-align: center;">
            <table width="100%">
                <tr>
                    <td align="left" valign="middle" class="titleText">
                        Client notes
                    </td>
                </tr>
                <tr>
                    <td align="right" valign="top">
                        <asp:LinkButton ID="LnkShowAllCat" runat="server" CssClass="blueBtns" Width="50px"
                            Style="text-align: center">Add tab</asp:LinkButton>
                        <asp:LinkButton ID="LnkHideTabArea" runat="server" CssClass="blueBtns" Width="50px"
                            Style="text-align: center">Hide</asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td id="TDAllCat" runat="server" align="left" style="width: 100%; border: solid 1px cdcdcd;">
                        <table id="TABLE1" onclick="return TABLE1_onclick()" cellspacing="0" cellpadding="0"
                            width="100%" border="0">
                            <tbody>
                                <tr>
                                    <td align="right" valign="top" bgcolor="#1d2357">
                                        <img src="../images/corner_l1.png" width="11" height="14">
                                    </td>
                                    <td height="14" bgcolor="#1d2357">
                                        &nbsp;
                                    </td>
                                    <td align="left" valign="top" bgcolor="#1d2357">
                                        <img src="../images/corner_r1.png" width="11" height="14">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="bottom" bgcolor="#1d2357">
                                        &nbsp;
                                    </td>
                                    <td width="100%" bgcolor="#1d2357">
                                        <div>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="150" style="color: #E1AC10;">
                                                        Client notes categories:-
                                                    </td>
                                                    <td>
                                                        <asp:DataList ID="Repeater2" runat="server" RepeatColumns="8" RepeatDirection="Horizontal">
                                                            <ItemTemplate>
                                                                <table border="0" width="100%" cellspacing="0" cellpadding="0">
                                                                    <tr align="left">
                                                                        <td align="left" style="padding-right: 5px; float: left; font-size: 12px; text-align: left;
                                                                            font-weight: 100; color: white; font-family: Arial, Helvetica, sans-serif;">
                                                                            <input type="checkbox" id="chkLocation" runat="server" title='<%# DataBinder.Eval(Container.DataItem,"Categoy_name") %>'
                                                                                onclick="javascript:setClientCategories();" />
                                                                            <%# DataBinder.Eval(Container.DataItem, "Categoy_name")%>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:DataList>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnAddCategory" runat="server" Text="Add category" OnClick="btnAddCategory_Click"
                                                            CssClass="clearfilter"></asp:Button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                    <td align="left" valign="bottom" bgcolor="#1d2357">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="bottom" bgcolor="#1d2357">
                                        <img src="../images/corner_l2.png" width="11" height="14">
                                    </td>
                                    <td height="14" bgcolor="#1d2357">
                                        &nbsp;
                                    </td>
                                    <td align="left" valign="bottom" bgcolor="#1d2357">
                                        <img src="../images/corner_r2.png" width="11" height="14">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 100%;">
                        <%--<cc1:TabContainer ID="TabContainer1" runat="server" Width="100%" AutoPostBack="true"
                        OnClientActiveTabChanged="ActiveTabChanged" OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                        EnableViewState="true">--%>
                        <cc1:TabContainer ID="TabContainer1" runat="server" OnClientActiveTabChanged="ActiveTabChanged"
                            Width="100%" AutoPostBack="true" EnableViewState="true">
                        </cc1:TabContainer>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width: 100%; border: solid 1px red; display: none">
                        <FTB:FreeTextBox ID="FtbCompNotes" runat="server" Height="625px" Width="100%">
                        </FTB:FreeTextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="left" valign="middle">
                                    <div id="div1" runat="server" style="color: Red">
                                    </div>
                                </td>
                                <td align="right" style="padding-right: 8px">
                                    <asp:HiddenField ID="hdnProjectCompany" runat="server"></asp:HiddenField>
                                    <asp:HiddenField ID="hdnClientCategories" runat="server" Value="All" />
                                    <asp:HiddenField ID="hdnCategories" runat="server" Value="All" />
                                    <asp:CheckBox ID="chkEmailnotify" runat="server" Checked="true" Font-Bold="True"
                                        Text="Email notify" />&nbsp;
                                    <asp:Button ID="BtnRdbCat" runat="server" Text="" BackColor="transparent" BorderStyle="none"
                                        OnClick="BtnRdbCat_Click" />
                                    <asp:Button ID="btnote" runat="server" Text="Update" OnClick="btnote_Click" CssClass="blueBtns">
                                    </asp:Button>
                                    <asp:Button ID="btnRemove" runat="server" OnClick="btnRemove_Click" Text="Delete"
                                        CssClass="blueBtns" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
