<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CRNotes_Sample.aspx.cs" Inherits="CRNotes_Sample" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Client notes</title>
    
     <script type="text/javascript" language="javascript">
            
                    
     function ActiveTabChanged(sender, e)
     
      {
 
       var index = $find("TabContainer1").get_activeTabIndex();
    
      document.getElementById("hdnCategories").value = index;
                  
    document.getElementById("BtnRdbCat").click();
             
      }
 </script>
 
 <script type="text/javascript" language="javascript">


function setClientCategories() {
	var hiddenClientCat = document.getElementById("hdnClientCategories");
	var chkbxs = document.getElementById("Repeater2").getElementsByTagName("input");
	hiddenClientCat.value = "";
	for(var i=0;i<chkbxs.length;i++)
	{
		if(chkbxs[i].checked==true) { hiddenClientCat.value += chkbxs[i].title + ";"; }
	}
}




function SetClientCatOnload()
{

var hiddenClientCat = document.getElementById("hdnClientCategories");
var chkbxs = document.getElementById("Repeater2").getElementsByTagName("input");
if(hiddenClientCat.value !="")
{
var items = hiddenClientCat.value.split(";");

            for(var i=0;i<items.length-1;i++)
			{
			if(items[i] != null)
			{
			chkbxs[i].checked = true;
			
			}
			
			}
			
			
			}

}

function ShowAllCat()
 {
 
 document.getElementById("TDAllCat").style.display ="block";
  document.getElementById("LnkHideTabArea").style.display ="Block";
   document.getElementById("LnkShowAllCat").style.display ="none";
  
 }
 function   HideAllCat()
 {
  
 document.getElementById("TDAllCat").style.display = "none";
document.getElementById("LnkHideTabArea").style.display ="none";
   document.getElementById("LnkShowAllCat").style.display ="block";
 }

function HideOnload()
{

 document.getElementById("TDAllCat").style.display ="none";
document.getElementById("LnkHideTabArea").style.display = "none";
}



</script>

</head>
<body onload="SetClientCatOnload(); HideOnload();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="smanager" runat="server"></asp:ScriptManager>
     
        <div id="CompanyNote" runat="server" style="background-color: #ffffff; text-align:center;">
     
        <table style="background-color: #ffffff;float:left; font-weight:bold; font-size:16px; border:solid 1px black;  width:100%; height:100%"><tr><td colspan="2" align="left" valign="middle" style="width:100%; padding-bottom:10px; padding-top:10px;  border:solid 1px black; font-size:18px; background-color: #e9e9e9 ">
            Client notes</td></tr>
             <tr><td colspan="2" align="right" style="padding-top:5px; padding-right:5px">
                    <asp:LinkButton ID="LnkShowAllCat" runat="server">Add tab</asp:LinkButton>
                  <asp:LinkButton ID="LnkHideTabArea" runat="server">Hide</asp:LinkButton></td></tr>
            <tr><td colspan="2" id="TDAllCat" runat="server" align="left" style="width:100%; border:solid 1px black; background-color: #e9e9e9">
            <div style="font-size:14px; color:Black; padding-bottom:7px">Client notes categories:- </div>
            <div>
           <asp:DataList id="Repeater2" runat="server" RepeatColumns="6" RepeatDirection="Horizontal">
                          <itemtemplate>
                            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                              <tr align="left">
                                <td align="left" style="padding-right:5px; float:left; font-size:12px; text-align:left; font-weight:100; color:Black; font-family:Arial, Helvetica, sans-serif;"><input type="checkbox" id="chkLocation" runat="server" title='<%# DataBinder.Eval(Container.DataItem,"Categoy_name") %>' onclick="javascript:setClientCategories();"  />
                                  <%# DataBinder.Eval(Container.DataItem, "Categoy_name")%></td>
                              </tr>
                            </table>
                          </itemtemplate>
                      </asp:DataList>
                      </div>
                      <div style=" text-align:right;padding-right:5px"> <asp:Button id="btnAddCategory" runat="server" Text="Add category" OnClick="btnAddCategory_Click" ></asp:Button></div>
                      
                      </td></tr>
                  
         
            <tr><td colspan="2" align="left" style="width:100%; padding-top:10px">
            
          <cc1:TabContainer ID="TabContainer1" runat="server" Width="100%" AutoPostBack="true" OnClientActiveTabChanged="ActiveTabChanged" OnActiveTabChanged="TabContainer1_ActiveTabChanged"  EnableViewState="true">
         
          
        </cc1:TabContainer>
            </td></tr>
          
           <tr><td colspan="2" align="left" valign="top" style="width:100%; border:solid 1px red;  display:none"><FTB:FreeTextBox ID="FtbCompNotes" runat="server" Height="625px"  Width="100%">
                                </FTB:FreeTextBox></td></tr><tr>
                                <td align="left" ><div id="div1" runat="server" style="color:Red"></div></td><td align="right" style="padding-right:8px">
                                    <asp:HiddenField id="hdnProjectCompany" runat="server"></asp:HiddenField>
                                    <asp:HiddenField ID="hdnClientCategories" runat="server" Value="All" />
                                <asp:HiddenField ID="hdnCategories" runat="server" Value="All" />
                                    <asp:CheckBox ID="chkEmailnotify" runat="server" Checked="true" Font-Bold="True"
                                        Text="Email notify" />&nbsp;
                                          <asp:Button id="BtnRdbCat" runat="server" Text="" BackColor="transparent" BorderStyle="none" OnClick="BtnRdbCat_Click" />
                                    <asp:Button id="btnote" runat="server" Text="Update" OnClick="btnote_Click"></asp:Button>
                                    <asp:Button ID="btnRemove" runat="server" OnClick="btnRemove_Click" Text="Delete" />
                                  </td></tr></table>
                               
  
                                </div>

    </form>
    </body>
</html>
