using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.IO;

public partial class CRNotes_Sample : System.Web.UI.Page
{
    string company;
    string sql = "";
    string emailSubject = "";
    static ArrayList arrAllClientCategories = new ArrayList();
    protected void Page_Load(object sender, EventArgs e)
    {
        btnRemove.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete client notes of selected category?');");
        LnkShowAllCat.Attributes.Add("onclick", "ShowAllCat();");
        LnkHideTabArea.Attributes.Add("onclick", "HideAllCat();");
        if (!Page.IsPostBack)
        {



            if (Session["TaskComp"] == null || Session["TaskComp"].ToString().Trim() == "")
            {

            }
            else
            {

                hdnCategories.Value = "";
                hdnProjectCompany.Value = Session["TaskComp"].ToString();
                try
                {

                    sql = "select * from Notes_Categories where Category_Type='Client'";


                    DataSet dsClientCategories = DatabaseHelper.getDataset(sql);


                    if (dsClientCategories != null && dsClientCategories.Tables.Count > 0 && dsClientCategories.Tables[0].Rows.Count > 0)
                    {



                        for (int i = 0; i < dsClientCategories.Tables[0].Rows.Count; i++)
                        {

                            arrAllClientCategories.Add(dsClientCategories.Tables[0].Rows[i]["Categoy_name"].ToString());
                        }


                        Repeater2.DataSource = dsClientCategories.Tables[0];
                        Repeater2.DataBind();


                    }
                }
                catch { }


                hdnProjectCompany.Value = Session["TaskComp"].ToString();
                if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
                {
                    FtbCompNotes.ReadOnly = true;
                    btnote.Enabled = false;
                    chkEmailnotify.Enabled = false;
                    btnRemove.Enabled = false;
                }
                else
                {
                    FtbCompNotes.ReadOnly = false;
                    btnote.Enabled = true;
                    chkEmailnotify.Enabled = true;
                    btnRemove.Enabled = false;
                }

                TabContainer1.ActiveTabIndex = 0;
                hdnCategories.Value = "0";
                TabContainer1_ActiveTabChanged(TabContainer1, null);



            }
        }


    }

    public void Page_Init(object sender, EventArgs e)
    {

        try
        {


            createTab();

        }
        catch
        {
        }
    }


    private void createTab()
    {
        if (Session["TaskComp"] == null || Session["TaskComp"].ToString().Trim() == "")
        {

        }
        else
        {
            hdnProjectCompany.Value = Session["TaskComp"].ToString();

            sql = "select * from Company_notes where Company_name ='" + hdnProjectCompany.Value + "' and Note_Type='Client' and Allow_Notes ='True'";


            DataSet ds = DatabaseHelper.getDataset(sql);


            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {

                AjaxControlToolkit.TabPanel tp = new AjaxControlToolkit.TabPanel();


                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {

                    /// i = i + 1;
                    tp = new AjaxControlToolkit.TabPanel();
                    tp.ID = ds.Tables[0].Rows[i]["Category_name"].ToString().Replace(" ", "").Replace("-", "").Replace(" ", "");
                    FreeTextBoxControls.FreeTextBox b = new FreeTextBoxControls.FreeTextBox();
                    /// Button b = new Button();
                    string hdfgh = ds.Tables[0].Rows[i]["Category_name"].ToString().Replace(" ", "_").Replace("-", "").Replace(" ", "");
                    b.ID = ds.Tables[0].Rows[i]["Category_name"].ToString().Replace(" ", "").Replace("-", "").Replace(" ", "");
                    b.Height = 500;

                    b.Width = TabContainer1.Width;

                    tp.Controls.Add(b);
                    tp.HeaderText = ds.Tables[0].Rows[i]["Category_name"].ToString();
                    TabContainer1.Controls.Add(tp);
                    //TabContainer1.Visible = true;
                    ///TabContainer1.Style["display"] = "block";


                    hdnClientCategories.Value += ds.Tables[0].Rows[i]["Category_name"].ToString() + ";";

                }




            }

        }
    }

    protected void btnote_Click(object sender, EventArgs e)
    {

        string r = TabContainer1.ActiveTabIndex.ToString();
        TabContainer1.ActiveTabIndex = int.Parse(hdnCategories.Value.ToString());

        FreeTextBoxControls.FreeTextBox FTBCompNotes = (FreeTextBoxControls.FreeTextBox)(TabContainer1.ActiveTab.FindControl(TabContainer1.ActiveTab.HeaderText.ToString().Replace(" ", "").Replace("-", "").Replace(" ", "")));

        if (FTBCompNotes.Text.Trim() == "")
        {
            div1.InnerHtml = "Please enter notes .";
            return;
        }

        ////string sqlcompanynote = "select [Notes] from [Company_notes]  where Company_name = '" + hdnProjectCompany.Value + "' and Note_Type ='FHS' and Category_name='" + TabContainer1.ActiveTab.HeaderText.ToString() + "'";
        ////object objNoteResult = DatabaseHelper.executeScalar(sqlcompanynote);
        ////if (objNoteResult != null)
        ////{
        string strSql = " update Company_notes ";
        strSql += " set [Notes]= '" + FTBCompNotes.Text.Trim().Replace("'", "''") + "'";
        strSql += " where Company_Name = '" + hdnProjectCompany.Value + "' and  Note_Type ='Client' and Category_name='" + TabContainer1.ActiveTab.HeaderText.ToString() + "' ";

        int intResult = DatabaseHelper.executeNonQuery(strSql);
        if (intResult != 0)
        {
            //sendNotification();
            if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
            {
                btnRemove.Enabled = false;
            }
            else
            {
                btnRemove.Enabled = true;
            }
            div1.InnerHtml = "Client notes Updated";
        }
        else
        {
            div1.InnerHtml = "Client notes not updated. please try again.";
        }
        // }



    }

    private string generateRemovednoteAdminEmail()
    {
        string strBody = "";
        strBody += "<br/>";
        strBody += "Client notes  have been removed for  " + hdnProjectCompany.Value + "  at  " + DateTime.Now.ToString("dd MMM h:mm tt") + "<br/><br>";
        strBody += FtbCompNotes.Text + "<br/><br/>";
        strBody += "<span style=\'font-size:11.0pt;font-family:Calibri,sans-serif;color:red\'>Please do not reply to this email as it does not take replies.</span><br/>";
        strBody += "<br/>" + "Regards," + "<br/>Support.";
        return strBody;
    }

    private string generateAdminEmail()
    {
        TabContainer1.ActiveTabIndex = int.Parse(hdnCategories.Value.ToString());

        FreeTextBoxControls.FreeTextBox FTBCompNotes = (FreeTextBoxControls.FreeTextBox)(TabContainer1.ActiveTab.FindControl(TabContainer1.ActiveTab.HeaderText.ToString().Replace(" ", "").Replace("-", "").Replace(" ", "")));
        string strBody = "";
        strBody += "<br/>";
        strBody += "Following client notes have been updated for  " + hdnProjectCompany.Value + "  at  " + DateTime.Now.ToString("dd MMM h:mm tt") + "<br/><br>";
        strBody += FTBCompNotes.Text + "<br/><br/>";
        strBody += "<span style=\'font-size:11.0pt;font-family:Calibri,sans-serif;color:red\'>Please do not reply to this email as it does not take replies.</span><br/>";
        strBody += "<br/>" + "Regards," + "<br/>Support.";
        return strBody;
    }


    void sendRemovedNotification()
    {
        sql = "select email, EmailNotification";
        sql += " from users";
        sql += " where admin = 'Y'";
        sql += " and EmailNotification ='Y' ";

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            string strHtml = generateRemovednoteAdminEmail();

            string toEmails = "";


            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (ds.Tables[0].Rows[i]["email"].ToString() != "")
                {


                    toEmails += ds.Tables[0].Rows[i]["email"].ToString() + ";";


                }


            }

            toEmails = toEmails.TrimEnd(';');

            if (emailSubject == "")
            {

                emailSubject = "Client notes have been removed for   " + hdnProjectCompany.Value;


            }
            if (chkEmailnotify.Checked)
            {
                bool flag = DatabaseHelper.sendEmailChangeRequestNorply(toEmails, emailSubject, strHtml);
            }

        }
    }
    void sendNotification()
    {
        sql = "select email, EmailNotification";
        sql += " from users";
        sql += " where admin = 'Y'";
        sql += " and EmailNotification ='Y' ";

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            string strHtml = generateAdminEmail();

            string toEmails = "";


            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (ds.Tables[0].Rows[i]["email"].ToString() != "")
                {


                    toEmails += ds.Tables[0].Rows[i]["email"].ToString() + ";";


                }
            }

            toEmails = toEmails.TrimEnd(';');

            if (emailSubject == "")
            {

                emailSubject = "Client notes have been added for   " + hdnProjectCompany.Value;


            }
            if (chkEmailnotify.Checked)
            {
                bool flag = DatabaseHelper.sendEmailChangeRequestNorply(toEmails, emailSubject, strHtml);
            }

        }
    }
    private string generateClientEmail(string fname, string surname, string username, string password)
    {
        TabContainer1.ActiveTabIndex = int.Parse(hdnCategories.Value.ToString());

        FreeTextBoxControls.FreeTextBox FTBCompNotes = (FreeTextBoxControls.FreeTextBox)(TabContainer1.ActiveTab.FindControl(TabContainer1.ActiveTab.HeaderText.ToString().Replace(" ", "").Replace("-", "").Replace(" ", "")));
        string strBody = "";
        strBody += "<br>" + fname.Trim() + " " + surname.Trim() + ",<br><br>";
        strBody += "Some notes have been added or changed on your Change Request System.The full notes are attached to this email for your reference or you can view them on: " + ConfigurationManager.AppSettings["WebAddress2"].ToString() + " using your login details:<br><br>";
        strBody += "Username: " + username.ToString() + "<br/>";
        strBody += "Password: " + password.ToString() + "<br/><br/>";
        strBody += "If you check the check box on login which allows you to save the password you will not have to login again if you open the website on this PC (requires you have cookies enabled)." + "<br/><br/>";
        strBody += "<br/><b>" + "The following notes have been updated for Change Request System  " + "  at  " + DateTime.Now.ToString("dd MMM h:mm tt") + ": " + "</b>" + "<br/><br/>";
        strBody += FTBCompNotes.Text + "<br/><br/>";
        strBody += "<span style=\'font-size:11.0pt;font-family:Calibri,sans-serif;color:red\'>Please do not reply to this email as it does not take replies.</span><br/>";
        strBody += "<br/>" + "Regards," + "<br/>Support.";
        return strBody;
    }

    void sendClientNotification()
    {
        sql = "select *";
        sql += " from NonesCRMusers";
        sql += " where CompanyName ='" + hdnProjectCompany.Value + "'";


        DataSet ds = DatabaseHelper.getDataset(sql);




        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {


            string toEmails = "";


            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (ds.Tables[0].Rows[i]["email"].ToString() != "")
                {
                    string firstame = ds.Tables[0].Rows[i]["FirstName"].ToString();
                    string surname = ds.Tables[0].Rows[i]["Surname"].ToString();
                    string username = ds.Tables[0].Rows[i]["UserName"].ToString();
                    string password = ds.Tables[0].Rows[i]["Password"].ToString();
                    string strHtml = generateClientEmail(firstame, surname, username, password);
                    toEmails = ds.Tables[0].Rows[i]["email"].ToString();
                    emailSubject = "Notes have been added to your Change Request System";
                    if (chkEmailnotify.Checked)
                    {
                        bool flag = DatabaseHelper.sendEmailChangeRequestNorply(toEmails, emailSubject, strHtml);
                    }

                }


            }


        }
    }
    protected void btnRemove_Click(object sender, EventArgs e)
    {
        string r = TabContainer1.ActiveTabIndex.ToString();
        TabContainer1.ActiveTabIndex = int.Parse(hdnCategories.Value.ToString());

        FreeTextBoxControls.FreeTextBox FTBCompNotes = (FreeTextBoxControls.FreeTextBox)(TabContainer1.ActiveTab.FindControl(TabContainer1.ActiveTab.HeaderText.ToString().Replace(" ", "").Replace("-", "").Replace(" ", "")));


        string strSql = "update Company_notes ";
        strSql += " set Allow_Notes= 'False'";
        strSql += " where Company_Name = '" + hdnProjectCompany.Value + "' and  Note_Type ='Client' and Category_name='" + TabContainer1.ActiveTab.HeaderText.ToString() + "' ";

        int intResult = DatabaseHelper.executeNonQuery(strSql);
        if (intResult != 0)
        {
            /// sendRemovedNotification();
            btnRemove.Enabled = false;
            //div1.InnerHtml = "Client notes removed";
            Response.Redirect("CRNotes_Sample.aspx", false);
        }
        else
        {
            // div1.InnerHtml = "Client notes not removed. please try again.";
            Response.Redirect("CRNotes_Sample.aspx", false);
        }
    }

    protected void BtnRdbCat_Click(object sender, EventArgs e)
    {


        string r = TabContainer1.ActiveTabIndex.ToString();
        TabContainer1.ActiveTabIndex = int.Parse(hdnCategories.Value.ToString());

        string sqlcompanynote = "select [Notes] from [Company_notes]  where Company_name = '" + hdnProjectCompany.Value + "' and Note_Type ='Client' and Category_name='" + TabContainer1.ActiveTab.HeaderText.ToString() + "'";
        object objNoteResult = DatabaseHelper.executeScalar(sqlcompanynote);

        if (objNoteResult != null)
        {
            FreeTextBoxControls.FreeTextBox CompNotes = (FreeTextBoxControls.FreeTextBox)(TabContainer1.ActiveTab.FindControl(TabContainer1.ActiveTab.HeaderText.ToString().Replace(" ", "").Replace("-", "").Replace(" ", "")));
            CompNotes.Text = objNoteResult.ToString();
            ///hdnProjectCompany.Value = objResult.ToString();
            if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
            {
                btnRemove.Enabled = false;
            }
            else
            {
                btnRemove.Enabled = true;

            }
        }
        else
        {
            btnRemove.Enabled = false;
            FtbCompNotes.Text = "";

        }




    }
    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        string r = TabContainer1.ActiveTabIndex.ToString();
        TabContainer1.ActiveTabIndex = int.Parse(hdnCategories.Value.ToString());
        string sqlcompanynote = "select [Notes] from [Company_notes]  where Company_name = '" + hdnProjectCompany.Value + "' and Note_Type ='Client' and Category_name='" + TabContainer1.ActiveTab.HeaderText.ToString() + "'";
        object objNoteResult = DatabaseHelper.executeScalar(sqlcompanynote);

        if (objNoteResult != null)
        {
            FreeTextBoxControls.FreeTextBox CompNotes = (FreeTextBoxControls.FreeTextBox)(TabContainer1.ActiveTab.FindControl(TabContainer1.ActiveTab.HeaderText.ToString().Replace(" ", "").Replace("-", "").Replace(" ", "")));
            CompNotes.Text = objNoteResult.ToString();
            ///hdnProjectCompany.Value = objResult.ToString();
            if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
            {
                btnRemove.Enabled = false;
            }
            else
            {
                btnRemove.Enabled = true;

            }
        }
        else
        {
            btnRemove.Enabled = false;
            FtbCompNotes.Text = "";

        }

    }

    protected void btnAddCategory_Click(object sender, EventArgs e)
    {


        try
        {

            string strClientCategories = hdnClientCategories.Value.Trim();

            ///strCkdCategories = strCkdCategories.ToLower() == "" || strCkdCategories.ToLower() == "select" ? "All" : strCkdCategories;


            string[] strClientCategory = strClientCategories.Split(new char[] { ';' });

            ArrayList ChkdClientCategories = new ArrayList();
            try
            {
                for (int k = 0; k < strClientCategory.Length - 1; k++)
                {
                    ChkdClientCategories.Add(strClientCategory[k].ToString());

                }
            }
            catch { }


            try
            {
                string ClientNotes = "";
                string strSqlCat = "";
                int intResultClientCat = 0;
                for (int j = 0; j < arrAllClientCategories.Count; j++)
                {
                    try
                    {
                        object ClientCatname = arrAllClientCategories[j].ToString();
                        if (ChkdClientCategories.Contains(ClientCatname.ToString()) == false)
                        {
                            object objResultCcat = DatabaseHelper.executeScalar("select count(*) from Company_notes where [Company_Name]='" + hdnProjectCompany.Value + "' and Note_Type ='Client'and Category_name ='" + ClientCatname.ToString() + "'");
                            if (objResultCcat.ToString() != "0")
                            {
                                strSqlCat = "Update Company_notes set Allow_Notes ='False' where Company_name = '" + hdnProjectCompany.Value.Replace("'", "''") + "' and Note_Type ='Client'and Category_name ='" + ClientCatname.ToString() + "'";


                                intResultClientCat = DatabaseHelper.executeNonQuery(strSqlCat);
                            }
                            else
                            {
                                strSqlCat = " insert into Company_notes ([Company_name],[Note_Type],[Category_name],[Notes],[Allow_Notes]) ";
                                strSqlCat += " values('" + hdnProjectCompany.Value.Replace("'", "''") + "','Client','" + ClientCatname.ToString() + "','" + ClientNotes + "','False')";

                                intResultClientCat = DatabaseHelper.executeNonQuery(strSqlCat);
                            }

                        }
                        else
                        {

                            object objResultCcat2 = DatabaseHelper.executeScalar("select count(*) from Company_notes where [Company_Name]='" + hdnProjectCompany.Value.Trim() + "' and Note_Type ='Client'and Category_name ='" + ClientCatname.ToString() + "'");
                            if (objResultCcat2.ToString() != "0")
                            {
                                strSqlCat = "Update Company_notes set Allow_Notes ='True' where Company_name = '" + hdnProjectCompany.Value.Replace("'", "''") + "' and Note_Type ='Client'and Category_name ='" + ClientCatname.ToString() + "'";

                                intResultClientCat = DatabaseHelper.executeNonQuery(strSqlCat);
                            }
                            else
                            {
                                strSqlCat = " insert into Company_notes ([Company_name],[Note_Type],[Category_name],[Notes],[Allow_Notes]) ";
                                strSqlCat += " values('" + hdnProjectCompany.Value.Replace("'", "''") + "','Client','" + ClientCatname.ToString() + "','" + ClientNotes + "','True')";

                                intResultClientCat = DatabaseHelper.executeNonQuery(strSqlCat);
                            }


                        }
                    }
                    catch
                    {
                       

                    }


                }

                Response.Redirect("CRNotes_Sample.aspx", false);
            }
            catch
            {
               
            }

            //int introw2 = DatabaseHelper.insertArea(agencyid, strReqAreas);

        }
        catch { }
    }
}

