using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient; 

public partial class Admin_ChangeType : System.Web.UI.Page
{
    static string strConnECRM = ConfigurationManager.AppSettings["strConnECR"];
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["ProjectName"] != null)
        {
            DataSet TaskIdDs = new DataSet();
            TaskIdDs =DatabaseHelper.getTaskdataset(Request.QueryString["ProjectName"]);
            Response.Clear();
            Response.ContentType = "text/xml";
            Response.Write(TaskIdDs.GetXml());
            Response.End();
        }
        else if (Request.QueryString["TaskId"] != null)
         {
            DataSet ProjectIdDs = new DataSet();
            ProjectIdDs = DatabaseHelper.getDetailDataset(Request.QueryString["TaskId"]);
            Response.Clear();
            Response.ContentType = "text/xml";
            Response.Write(ProjectIdDs.GetXml());
            Response.End();
         }

        if (Request.QueryString["Date"] != null)
        {

            DataSet dsdate = new DataSet();
            DateTime updatedate;
            string zoneId = "India Standard Time";
            TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById(zoneId);
            
            if (Request.QueryString["Date"] != DateTime.Now.ToString("dd/MM/yyyy"))
            {
                dsdate = DatabaseHelper.getDataset("select intime, '' as lastintime, last_updated_date, '' as updatedate from tblIntime It inner join tasks t on t.assigned_to_user = It.username " +
                   " where username ='" + Session["admin"].ToString() + "' and indate = '" + Request.QueryString["Date"] + "' and status = 'CWO but away' " +
                    //" where username ='surekha' and indate = '" + Request.QueryString["Date"] + "' and status = 'CWO but away' "+
                    " and convert(varchar(10),last_updated_date,103) = '" + Request.QueryString["Date"] + "' ");

                updatedate = Convert.ToDateTime(dsdate.Tables[0].Rows[0]["last_updated_date"]);
                updatedate = TimeZoneInfo.ConvertTimeFromUtc(updatedate, tzi);
                updatedate = updatedate.AddHours(-1);
            }
            else
            {
                dsdate = DatabaseHelper.getDataset("select intime, '' as lastintime, '' as updatedate from tblIntime " +
                            " where username ='" + Session["admin"].ToString() + "' and indate = '" + Request.QueryString["Date"] + "' ");
                updatedate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, tzi);
            }

            //dsdate.Tables[0].Rows[0]["updatedate"] = updatedate.ToShortTimeString().ToString();
            dsdate.Tables[0].Rows[0]["updatedate"] = updatedate.ToString("hh:mm tt");         //to show 12 hr clock format
            DateTime lastintime = Convert.ToDateTime(dsdate.Tables[0].Rows[0]["intime"]);
            dsdate.Tables[0].Rows[0]["lastintime"] = lastintime.ToString("hh:mm tt");         //to show 12 hr clock format
            //dsdate.Tables[0].Rows[0]["lastintime"] = lastintime.ToShortTimeString().ToString();

            dsdate.AcceptChanges();
            Response.Clear();
            Response.ContentType = "text/xml";
            Response.Write(dsdate.GetXml());
            Response.End();
        }
    }
}
