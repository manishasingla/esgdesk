<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Client_Detail.aspx.cs" Inherits="Admin_Client_Detail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Client details</title>
</head>
<body>
    <form id="form1" runat="server">
        <div align="center" id="hidediverror" runat="server">
            <table>
                <tr>
                    <td colspan="2" align="center">
                        <asp:Label ID="lblerror" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div align="center" id="hidediv" runat="server">
            <table border="0">
                <tr>
                    <td align="center" colspan="2" style="font-weight: bold;">
                        Client Details</td>
                </tr>
                <tr>
                    <td align="left">
                        Client id:
                    </td>
                    <td>
                        <asp:Label ID="lblclientId" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                    &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        User name 1:
                    </td>
                    <td>
                        <asp:Label ID="lblusername1" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        Password 1:
                    </td>
                    <td>
                        <asp:Label ID="lblpassword1" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                    &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        User name 2:
                    </td>
                    <td>
                        <asp:Label ID="lblusername2" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        Password 2:
                    </td>
                    <td>
                        <asp:Label ID="lblpassword2" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
