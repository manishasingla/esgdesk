using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Admin_Client_Detail : System.Web.UI.Page
{
    int id;
    protected void Page_Load(object sender, EventArgs e)
    {
        string var = Request.QueryString["ProjectId"];

        try
        {
            id = Convert.ToInt32(var);
        }
        catch
        {
            lblerror.Text = "Project ID must be an integer.";
            return;
        }
        hidediverror.Visible = false;
        GetProjectdata(id);
        
    }

    private void GetProjectdata(int id)
    {
        string strSql = "select * from projects where project_id=" + id;
        DataSet ds = DatabaseHelper.getDataset(strSql);

       
            if (ds != null&& ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["Client_Id"].ToString() != "" || ds.Tables[0].Rows[0]["UserName1"].ToString() != "" || ds.Tables[0].Rows[0]["Password1"].ToString() != "" || ds.Tables[0].Rows[0]["UserName2"].ToString() != "" || ds.Tables[0].Rows[0]["Password2"].ToString() != "")
                {
                    lblclientId.Text = ds.Tables[0].Rows[0]["Client_Id"].ToString().Replace("''", "'");
                    lblusername1.Text = ds.Tables[0].Rows[0]["UserName1"].ToString().Replace("''", "'");
                    lblpassword1.Text = ds.Tables[0].Rows[0]["Password1"].ToString().Replace("''", "'");
                    lblusername2.Text = ds.Tables[0].Rows[0]["UserName2"].ToString().Replace("''", "'");
                    lblpassword2.Text = ds.Tables[0].Rows[0]["Password2"].ToString().Replace("''", "'");
                    hidediverror.Visible = false;
                }
                else 
                {
                    hidediv.Visible = false;
                    lblerror.Text = "Client detail was not found.";
                    hidediverror.Visible = true;
                }
                


            }
            else
            {
                lblerror.Text = "Project was not found.";
                hidediverror.Visible = true;

            }
     }




}
