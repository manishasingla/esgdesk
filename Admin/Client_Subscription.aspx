<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Client_Subscription.aspx.cs"
    Inherits="Client_Subscription" %>

<%@ Register Src="Notifications.ascx" TagName="Notifications" TagPrefix="uc4" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        Client subscription</title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <uc2:Admin_Header ID="Admin_Header1" runat="server" />
        <div id="Content">
            <div>
                <uc4:Notifications ID="Notifications" runat="server" />
            </div>
            <div style="background: #ebebeb; padding: 5px;">
                <span id="divLinks" runat="server" class="text_default"><a href="Subscription_details.aspx">Back
                    to subscription</a> </span>
            </div>
            <table>
                <tr>
                    <td align="left" valign="top">
                        <table style="width: 420px">
                            <tr>
                                <td align="left" class="titleText" valign="top" style="width: 420px; display: none;">
                                    Users&nbsp;|&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 420px">
                                    <table width="420" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="left" valign="top">
                                                <img height="16" src="../images/loginBox_top.png" width="420" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="background: url(../images/loginBox_mid.png) repeat-y left top;">
                                                <div style="text-align: center">
                                                    <div id="DivEntry" runat="server">
                                                        <table width="100%" cellpadding="5">
                                                            <tr>
                                                                <td width="10" align="left" valign="top" class="whitetext1">
                                                                    &nbsp;
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext1">
                                                                    Subscription:
                                                                </td>
                                                                <td align="left" valign="top" width="250">
                                                                    <asp:TextBox ID="txtsubscription" runat="server" MaxLength="50" Width="220px"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtsubscription"
                                                                        ErrorMessage="Please enter subscription" Display="Dynamic">*</asp:RequiredFieldValidator>
                                                                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator1">
                                                                    </cc1:ValidatorCalloutExtender>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 100px">
                                                <img height="16" src="../images/loginBox_btm.png" width="420" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <hr />
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <div align="right" style="float: right;">
                                        <asp:LinkButton ID="btnCreate" class="blueBtns" Text="Create" runat="server" OnClick="btnCreate_Click"></asp:LinkButton></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <div id="divMessage" runat="server" style="color: Red; font-weight: bold">
            </div>
            <br />
            <div>
            </div>
        </div>
    </div>
    <uc1:footer ID="Footer1" runat="server" />
    </form>
</body>
</html>
