using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


public partial class Client_Subscription : System.Web.UI.Page
{
    int id;
    string sql = "";
    protected void Page_Load(object sender, EventArgs e)
    {

        //LnkMakecommentRead.Attributes.Add("onclick", "return confirm('Sure you want to mark every comment in every CR as read by you?');");
        //LnkMakeTaskcommentRead.Attributes.Add("onclick", "javascript:return confirm('Sure you want to mark every comment in each of your tasks as read by you (this means you must have read them all and acted on all that require action already)?');");
        //LnkMakeAllTaskCmmntRead.Attributes.Add("onclick", "javascript:return confirm('Sure you want to mark every comment in each of  tasks as read by you (this means you must have read them all and acted on all that require action already)?');");
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = (Request.QueryString["id"] == null || Request.QueryString["id"].ToString() == "" ? "tasks.aspx" : "edit_user.aspx?id="+ Request.QueryString["id"].ToString());
            Response.Redirect("login.aspx");
            return;
        }

        string var = Request.QueryString["id"];
        if (var == null)
        {
            id = 0;
            this.Title = "Add subscription";
        }
        else
        {
            try
            {
                id = Convert.ToInt32(var);
                
            }
            catch
            {
                divMessage.InnerHtml = "Subscription id must be an integer.";
                DivEntry.Visible = false;
                return;
            }
        }

        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            object objUserId = DatabaseHelper.executeScalar("select user_id from users where username = '" + Session["admin"].ToString() + "'");

            if (id.ToString() != objUserId.ToString())
            {
                Response.Redirect("tasks.aspx");
                return;
            }
            divLinks.Visible = false;
        }  

        if (!IsPostBack)
        {
           /// load_filterDropDown();
           /// drpFilter.SelectedValue = "Show my open tasks"; 
            // add or edit?
            if (id == 0)
            {
                clearControls();
            }
            else
            {

                string strSql = "select * from Client_subscription where SubscriptionID=" + id;
                DataSet ds = DatabaseHelper.getDataset(strSql);

                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        txtsubscription.Text = ds.Tables[0].Rows[0]["Subscription"].ToString();
                        btnCreate.Text = "Update";
                        this.Title = "Edit subscription - " + txtsubscription.Text;
                    }

                                       
                }

            }


            //getNewCR();
            //getCRNewComment();
            //getCRwaitingfrmClient();
            //getUnasweredQuestions();
            //getPR0Tasks();
            //getPR1aTasks();
            //getPR1bTasks();
            //getPR1cTasks();
            //getTskNewComment();
            //getAllTskNewComment();
            //getAllNotification();
            //getOverduetasks();
        }

        if (Session["message"] != null)
        {
            divMessage.InnerHtml = Session["message"].ToString();
            Session.Remove("message");
        }
    }
    protected int getHoursTakenSoFar(string task_id)
    {
        sql = @"select sum(datediff(minute,started_date,finished_date))
                from hours_reporting
                where finished_date is not null and task_id=" + task_id.ToString();

        int hrsTaken = 0;
        object objHrsTaken = DatabaseHelper.executeScalar(sql);
        if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
        {
            hrsTaken += (int)objHrsTaken;
        }
        else
        {
            hrsTaken = 0;
        }

        sql = @"select sum(datediff(minute,started_date,getdate()))
                from hours_reporting
                where finished_date is null and task_id=" + task_id.ToString();

        objHrsTaken = DatabaseHelper.executeScalar(sql);

        if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
        {
            hrsTaken += (int)objHrsTaken;
        }

        return hrsTaken;
    }

    void getOverduetasks()
    {

        try
        {

            int GvnETC = 0;
            int j = 0;
            DateTime crrntdate = DateTime.Now.Date;
            DateTime duedate;

            sql = @"select tasks.* from tasks 
                        where tasks.deleted <> 1 and  status <> 'closed' and  status <> 'checked' and status <>'parked'";

            if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
            {
                sql += " and assigned_to_user = '" + Session["admin"] + "' ";
            }

            DataSet ds = DatabaseHelper.getDataset(sql);

            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {


                ////BtnOverdue.Text = "(" + ds.Tables[0].Rows.Count + ")";
                ////immediatetask.Visible = true;

                ////for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                ////{
                ////    int ETC = Convert.ToInt32(ds.Tables[0].Rows[i]["ETC"].ToString());

                ////    ///GvnETC = "" + (ETC / 60) + " hrs " + (ETC % 60) + " mins";

                ////    int hrsTaken = getHoursTakenSoFar(ds.Tables[0].Rows[i]["task_id"].ToString());

                ////   //// e.Item.Cells[14].Text = "" + (hrsTaken / 60) + " hrs " + (hrsTaken % 60) + " mins";

                ////    ///int hrsLeft = ETC - hrsTaken;

                ////    if(hrsTaken > ETC)
                ////    {
                ////        j++;
                ////    }
                ////}



                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string strcrrntdate = DateTime.Parse(DateTime.Now.Date.ToString()).ToString("dd/MM/yyyy");
                    string djgfjs = ds.Tables[0].Rows[i]["DueDate"].ToString();
                    if (ds.Tables[0].Rows[i]["DueDate"].ToString() != null && ds.Tables[0].Rows[i]["DueDate"].ToString() != "")
                    {
                        string strduedate = DateTime.Parse(ds.Tables[0].Rows[i]["DueDate"].ToString()).ToString("dd/MM/yyyy");
                        duedate = DateTime.Parse(strduedate.ToString());
                        crrntdate = DateTime.Parse(strcrrntdate.ToString());
                        if (DateTime.Compare(duedate, crrntdate) < 0)
                        {

                            j++;

                        }


                    }

                }


                if (j > 0)
                {
                    //BtnOverdue.Text = "(" + j.ToString() + ")";
                    //OverDuetsk.Visible = true;
                }
                else
                {
                    //BtnOverdue.Visible = false;
                    //OverDuetsk.Visible = false;
                }


            }
            else
            {
                //BtnOverdue.Visible = false;
                //OverDuetsk.Visible = false;
            }

        }
        catch { }

    }


    protected void BtnOverdue_Click(object sender, EventArgs e)
    {

        Session["filter"] = " ";
        Session["filterunread"] = "";
        Session["OverdueTask"] = "True";
        Response.Redirect("tasks.aspx");


    }

    void getPR1cTasks()
    {

        sql = @"select tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '1c - normal' ";
        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            sql += " and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + Session["admin"] + "' ";
        }
        else
        {
            sql += " and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + Session["admin"] + "' ";
        }

        DataSet ds = DatabaseHelper.getDataset(sql);
        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            //Lnk1cTasks.Text = "(" + ds.Tables[0].Rows.Count + ")";
            //PR1CTask.Visible = true;
        }
        else
        {
            //Lnk1cTasks.Text = "(0)";
            //PR1CTask.Visible = false;
        }
    }

    protected void Lnk1cTasks_Click(object sender, EventArgs e)
    {
        Session["filter"] = "1c - normal";
        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        Response.Redirect("tasks.aspx");
    }


    void getAllNotification()
    {


        sql = @"select distinct Company_name from Company_notes 
                where Allow_Notes ='True' and Notes !=''";



        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {


            /////LnkNotification.Text = "(" + ds.Tables[0].Rows.Count + ")";
            //Notification.Visible = true;

        }
        else
        {

            //Notification.Visible = true;
            //LnkNotification.Text = "";


        }


    }

    protected void LnkNotification_Click(object sender, EventArgs e)
    {
        Response.Redirect("Notification.aspx", false);
    }
    void getCRwaitingfrmClient()
    {
        sql = @"select ClientRequest.* from ClientRequest 
                where ClientRequest.Status = 'awaiting client response- required' and status <> 'closed' and deleted <> 1 ";



        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {

            if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
            {

                //CRwocr.Visible = false;
            }
            else
            {
                //LnkCRWocr.Text = "(" + ds.Tables[0].Rows.Count + ")";
                //CRwocr.Visible = true;
            }

        }
        else
        {


            if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
            {

                //CRwocr.Visible = false;
            }
            else
            {
                    //LnkCRWocr.Text = "(0)";
                    //CRwocr.Visible = false;
            }


        }
    }

    void getCRNewComment()
    {


        //// sql = @"select ClientRequest.* from ClientRequest where ClientRequest.[RequestId] in (select [RequestId] from ClientRequest_Details where [CommentId] not in (select  CommentId from read_CR_comments)) and ClientRequest.deleted <> 1";
        ////  sql = @"select ClientRequest_Details.* from ClientRequest_Details where [CommentId] not in (select  CommentId from read_CR_comments where [UserName]='Support') and ClientRequest_Details.deleted <> 1";
        sql = "select ClientRequest.* from ClientRequest where RequestId in(select ClientRequest_Details.RequestId from ClientRequest_Details where [CommentId] not in (select  CommentId from read_CR_comments where read_CR_comments.username='" + Session["admin"].ToString() + "') and ClientRequest_Details.deleted <> 1)";

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
            {
                /// LnkNewCR.Text = "(" + ds.Tables[0].Rows.Count + ")";
                //CrNewComment.Visible = false;
                //LnkMakecommentRead.Visible = false;
            }
            else
            {
                //LnkCrNewComment.Text = "(" + ds.Tables[0].Rows.Count + ")";
                //LnkMakecommentRead.Text = "(" + "-" + ")";
                //LnkMakecommentRead.Visible = true;
                //CrNewComment.Visible = true;
            }

        }
        else
        {

            if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
            {
                /// LnkNewCR.Text = "(" + ds.Tables[0].Rows.Count + ")";
                //CrNewComment.Visible = false;
                //LnkMakecommentRead.Visible = false;
            }
            else
            {
                //LnkCrNewComment.Text = "(0)";
                //CrNewComment.Visible = false;
                //LnkMakecommentRead.Visible = false;
            }




        }
    }
    protected void LnkCRWocr_Click(object sender, EventArgs e)
    {

        Session["filter"] = "CRwocr";
        Response.Redirect("client_requests.aspx", false);


    }
    protected void LnkNewCR_Click(object sender, EventArgs e)
    {

        Session["filter"] = "NewCR";
        Response.Redirect("client_requests.aspx", false);
        /// bindPR1bTasks();

    }
    protected void LnkCrNewComment_Click(object sender, EventArgs e)
    {
        Session["filter"] = "CRNewComment";
        Response.Redirect("client_requests.aspx", false);

    }

    void getNewCR()
    {
        sql = @"select ClientRequest.* from ClientRequest 
                where ClientRequest.Status = 'new' and status <> 'closed' and deleted <> 1 ";

        ////if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        ////{
        ////    sql += " and status <> 'closed' and assigned_to_user = '" + Session["admin"] + "' ";
        ////}
        ////else
        ////{
        ////    sql += " and assigned_to_user = '" + Session["admin"] + "' ";
        ////}

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            ///  LnkNewCR.Text = "(" + ds.Tables[0].Rows.Count + ")";
            /// NewCR.Visible = true;
            if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
            {
                /// LnkNewCR.Text = "(" + ds.Tables[0].Rows.Count + ")";
                //NewCR.Visible = false;
            }
            else
            {
                //LnkNewCR.Text = "(" + ds.Tables[0].Rows.Count + ")";
                //NewCR.Visible = true;
            }

        }
        else
        {


            if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
            {
                /// LnkNewCR.Text = "(" + ds.Tables[0].Rows.Count + ")";
                //NewCR.Visible = false;
            }
            else
            {
                //LnkNewCR.Text = "(0)";
                //NewCR.Visible = false;
            }


        }
    }


    protected void LnkMakecommentRead_Click(object sender, EventArgs e)
    {
        string sql = "select *  from ClientRequest_Details where [CommentId] not in (select  CommentId  from read_CR_comments where read_CR_comments.username='" + Session["admin"].ToString() + "') and ClientRequest_Details.deleted <> 1";
        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {
                    string sqlinsert = "insert into read_CR_comments([CommentId],[UserName]) ";
                    sqlinsert += " values(" + ds.Tables[0].Rows[i]["CommentId"].ToString() + ",'" + Session["admin"].ToString() + "')";

                    int intResult = DatabaseHelper.executeNonQuery(sqlinsert);
                }
                catch
                {
                }
            }


        }

        getCRNewComment();

    }

    protected void lnkUnansweredQuestions_Click(object sender, EventArgs e)
    {
        Session["filter"] = "Unanswered";
        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        Response.Redirect("tasks.aspx");
    }

    protected void lnkPr0Task_Click(object sender, EventArgs e)
    {

        Session["filter"] = "Immediate";
        Session["filterunread"] = "";
        Response.Redirect("tasks.aspx");
    }

    protected void lnkHighestTasks_Click(object sender, EventArgs e)
    {
        Session["filter"] = "HighestTasks";
        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        Response.Redirect("tasks.aspx");
    }

    protected void lnkHighTasks_Click(object sender, EventArgs e)
    {
        Session["filter"] = "HighTasks";
        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        Response.Redirect("tasks.aspx");
    }

    void getUnasweredQuestions()
    {
//////        sql = @"select task_comments.* from task_comments,tasks 
//////                where tasks.task_id = task_comments.task_id and tasks.deleted <> 1 and tasks.status <> 'closed' and task_comments.deleted <> 1 and qflag=1 ";


//////        sql += " and assigned_to_user = '" + Session["admin"] + "' ";
        sql = @"select task_comments.* from task_comments,tasks 
                where tasks.task_id = task_comments.task_id and tasks.deleted <> 1 and tasks.status <> 'closed' and task_comments.deleted <> 1 and qflag=1 ";

        sql += " and task_comments.QuesTo = '" + Session["admin"] + "' ";


        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            //lnkUnansweredQuestions.Text = "(" + ds.Tables[0].Rows.Count + ")";
            //unansweredque.Visible = true;
        }
        else
        {
            //lnkUnansweredQuestions.Text = "(0)";
            //unansweredque.Visible = false;
        }
    }

    void getPR0Tasks()
    {
        sql = @"select tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '0 - IMMEDIATE' ";

        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            sql += " and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + Session["admin"] + "' ";
        }
        else
        {
            sql += " and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + Session["admin"] + "' ";
        }

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            //lnkPr0Task.Text = "(" + ds.Tables[0].Rows.Count + ")";
            //immediatetask.Visible = true;
        }
        else
        {
            //lnkPr0Task.Text = "(0)";
            //immediatetask.Visible = false;
        }
    }

    void getPR1aTasks()
    {
        sql = @"select tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '1a - DO NOW' ";

        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            sql += " and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + Session["admin"] + "' ";
        }
        else
        {
            sql += " and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + Session["admin"] + "' ";
        }

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            //lnkHighestTasks.Text = "(" + ds.Tables[0].Rows.Count + ")";
            //highestTasks.Visible = true;
        }
        else
        {
            //lnkHighestTasks.Text = "(0)";
            //highestTasks.Visible = false;
        }
    }

    void getPR1bTasks()
    {
        sql = @"select tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '1b - high' ";

        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            sql += " and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + Session["admin"] + "' ";
        }
        else
        {
            sql += " and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + Session["admin"] + "' ";
        }

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            //lnkHighTasks.Text = "(" + ds.Tables[0].Rows.Count + ")";
            //highTasks.Visible = true;
        }
        else
        {
            //lnkHighTasks.Text = "(0)";
            //highTasks.Visible = false;
        }
    }
    void getTskNewComment()
    {

        
        sql = "select tasks.* from tasks where task_id in(select task_comments.task_id from task_comments where tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 )";
       
        sql += "and tasks.deleted <> 1 and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            //LnkTskNewcomment.Text = "(" + ds.Tables[0].Rows.Count + ")";
            //LnkMakeTaskcommentRead.Text = "(" + "-" + ")";
            //LnkMakeTaskcommentRead.Visible = true;
            //TskNewComment.Visible = true;
        }
        else
        {
            //LnkTskNewcomment.Text = "(0)";
            //TskNewComment.Visible = false;
            //LnkMakeTaskcommentRead.Visible = false;
        }
    }
    protected void LnkTskNewcomment_Click(object sender, EventArgs e)
    {

        Session["filterunread"] = "TaskNewComment";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        Response.Redirect("tasks.aspx");


    }
    protected void LnkMakeTaskcommentRead_Click(object sender, EventArgs e)
    {
        sql = @"select *  from task_comments where task_id in (select task_id  from tasks where tasks.task_id = task_comments.task_id and  tasks.assigned_to_user = '" + Session["admin"].ToString() + "'  and tasks.deleted <> 1 and tasks.status <> 'closed') and task_comments.tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 ";
        /// sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {

                    //////string sqlgetcmntId = "select tc_id from task_comments where task_id ='" + ds.Tables[0].Rows[i]["task_id"].ToString() + "'";
                    //////object TskcmntID = DatabaseHelper.executeScalar(sqlgetcmntId);


                    //////if (TskcmntID!=null)
                    //////{
                    string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                    sqlinsert += " values(" + ds.Tables[0].Rows[i]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                    int intResult = DatabaseHelper.executeNonQuery(sqlinsert);

                    string sqlinsert2 = "insert into read_task([task_id],[username]) ";
                    sqlinsert2 += " values(" + ds.Tables[0].Rows[i]["task_id"].ToString() + ",'" + Session["admin"].ToString() + "')";
                    int intResult2 = DatabaseHelper.executeNonQuery(sqlinsert2);
                    //////}
                }
                catch
                {
                }
            }


        }

        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        Response.Redirect("tasks.aspx", false);
    }
    void getAllTskNewComment()
    {

        sql = "select tasks.* from tasks where task_id in(select task_comments.task_id from task_comments where tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 )";

       /// sql += "and tasks.deleted <> 1 and tasks.status <> 'closed'";

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
            {
                //LnkAllTsknewComment.Text = "(0)";
                //SpnAllTaskNewComment.Visible = false;
                //LnkMakeAllTaskCmmntRead.Visible = false;
                //LnkAllTsknewComment.Visible = false;

            }
            else
            {


                //LnkAllTsknewComment.Text = "(" + ds.Tables[0].Rows.Count + ")";
                //LnkMakeAllTaskCmmntRead.Text = "(" + "-" + ")";
                //LnkMakeAllTaskCmmntRead.Visible = true;
                //SpnAllTaskNewComment.Visible = true;

            }
        }
        else
        {
            //LnkAllTsknewComment.Text = "(0)";
            //SpnAllTaskNewComment.Visible = false;
            //LnkMakeAllTaskCmmntRead.Visible = false;
            //LnkAllTsknewComment.Visible = false;
        }


    }
    protected void LnkAllTsknewComment_Click(object sender, EventArgs e)
    {
        Session["filterunread"] = "AllTaskNewComment";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        Response.Redirect("tasks.aspx");
    }
    protected void LnkMakeAllTaskCmmntRead_Click(object sender, EventArgs e)
    {


        sql = @"select *  from task_comments where task_id in (select task_id  from tasks where tasks.task_id = task_comments.task_id) and task_comments.tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 ";
        /// sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {

                    string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                    sqlinsert += " values(" + ds.Tables[0].Rows[i]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                    int intResult = DatabaseHelper.executeNonQuery(sqlinsert);
                    string sqlinsert2 = "insert into read_task([task_id],[username]) ";
                    sqlinsert2 += " values(" + ds.Tables[0].Rows[i]["task_id"].ToString() + ",'" + Session["admin"].ToString() + "')";
                    int intResult2 = DatabaseHelper.executeNonQuery(sqlinsert2);
                    //////}
                }
                catch
                {
                }
            }


        }

        getAllTskNewComment();


    }

    protected void btnCreate_Click(object sender, EventArgs e)
    {
        
        bool flag =false;
        string strMsg="";
        if (txtsubscription.Text.Trim() == "")
        {
            strMsg += "- Subscription is required.<br>";
            flag =true;
        }
       
    

        if (flag)
        {
            divMessage.InnerHtml = strMsg;
            return;
        }
        
       
            //////object objResult = DatabaseHelper.executeScalar("select count(*) from users where [username]='" + txtUserName.Text.Trim() + "'");

            //////if (objResult.ToString() != "0")
            //////{
            //////    divMessage.InnerHtml = "Username already exists.   Choose another username.";
            //////    return;
            //////}

        if (btnCreate.Text == "Create")
        {
            string strSql = " insert into Client_subscription (Subscription) ";
            strSql += " values('" + txtsubscription.Text.Trim().Replace("'", "''") + "')";

            int introw = DatabaseHelper.executeNonQuery(strSql);

            if (introw > 0)
            {

                Session["message"] = "Subscription added.";

                Response.Redirect("Client_Subscription.aspx", false);
            }
            else
            {
                divMessage.InnerHtml = "Subscription not added.";
            }

        }
        else if (btnCreate.Text == "Update")
        {

            string strSql = " update  Client_subscription set Subscription ='" + txtsubscription.Text.Trim().Replace("'", "''") + "' where SubscriptionID = " + id + "";
           //// strSql += " values('" + txtsubscription.Text.Trim().Replace("'", "''") + "')";

            int introw = DatabaseHelper.executeNonQuery(strSql);

            if (introw > 0)
            {

                Session["message"] = "Subscription updated.";

                Response.Redirect("Client_Subscription.aspx", false);
            }
            else
            {
                divMessage.InnerHtml = "Subscription not updated.";
            }



        }
       //// }
       
    }

    private void clearControls()
    {
        txtsubscription.Text = "";
    }

    
}
