using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
using Telerik.Web;

public partial class Admin_Company : System.Web.UI.Page
{
    string sql = "";
    string strfilter = "";
    string rurl = "";
    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = "Company.aspx";
            Response.Redirect("login.aspx");
            return;
        }

        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            Response.Redirect("tasks.aspx");
            return;
        }


        divMessage.InnerHtml = "";

        if (!Page.IsPostBack)
        {
            lblOrderBy.Text = " ORDER BY Company_Name ASC";
            getFilterSession();
            bindData();
         }
    }
    private void setFileterSession()
    {
        Session["CompanyOrderBy"] = lblOrderBy.Text;
    }
    private void getFilterSession()
    {
        try
        {
            if (Session["CompanyOrderBy"] != null)
                lblOrderBy.Text = Session["CompanyOrderBy"].ToString();
        }
        catch
        {}
    }
    private void bindData()
    {
        string strSql = "select * from Company " + strfilter + lblOrderBy.Text;

        DataSet ds = DatabaseHelper.getDataset(strSql);

        if (ds != null)
        {
            try
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DataGrid1.DataSource = ds.Tables[0];
                    DataGrid1.DataBind();
                    PagedDataSource objds = new PagedDataSource();
                    objds.DataSource = ds.Tables[0].DefaultView;

                    LblRecordsno.Text = objds.DataSourceCount.ToString();
                }
                else
                {
                    LblRecordsno.Text = "0";
                    DataGrid1.DataSource = null;
                    DataGrid1.DataBind();
                    divMessage.InnerHtml = "No company in the database..";
                }
            }
            catch
            {
                DataGrid1.DataSource = null;
                DataGrid1.DataBind();
                LblRecordsno.Text = "0";
                divMessage.InnerHtml = "No company in the database..";
            }
        }
        else
        {
            try
            {
                DataGrid1.DataSource = null;
                DataGrid1.DataBind();
                LblRecordsno.Text = "0";
                divMessage.InnerHtml = "No company in the database..";
            }
            catch
            {
                DataGrid1.DataSource = null;
                DataGrid1.DataBind();
                LblRecordsno.Text = "0";
                divMessage.InnerHtml = "No company in the database..";
            }
        }
    }
    protected void DataGrid1_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        foreach (TableCell tc in e.Item.Cells)
        {
            tc.Attributes["style"] = "border:1px solid #00A651;";
        }
        if (e.Item is GridPagerItem)
        {
            GridPagerItem pagerItem = (GridPagerItem)e.Item;
            RadComboBox PageSizeComboBox = (RadComboBox)pagerItem.FindControl("PageSizeComboBox");
            PageSizeComboBox.Visible = false;
            Label changePageSizelbl = (Label)pagerItem.FindControl("ChangePageSizeLabel");
            changePageSizelbl.Visible = false;
        }
        if (e.Item.ItemType == GridItemType.Item || e.Item.ItemType == GridItemType.AlternatingItem)
        {
            GridDataItem dataItem = e.Item as GridDataItem;
            ImageButton deleteButton = (ImageButton)dataItem.FindControl("lblDelete");
            DropDownList CompProjects = (DropDownList)dataItem.FindControl("DrpProjects");
            DropDownList CompRelevanturl = (DropDownList)dataItem.FindControl("DrpRelevanturl");
            DropDownList CompClients = (DropDownList)dataItem.FindControl("DrpClients");
            //We can now add the onclick event handler
            deleteButton.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete this company?')");
            string sqlcompanynote = "select count(*) from [Company_notes]  where Company_Name = '" + ((Label)dataItem.FindControl("lblcompanysname")).Text + "'and Note_Type ='FHS' and Allow_Notes='True' and Notes !=''";
            int objNoteResult = int.Parse(DatabaseHelper.executeScalar(sqlcompanynote).ToString());
            try
            {
                if (objNoteResult > 0)
                {
                    string htmlColor = "red";
                    ((LinkButton)dataItem.FindControl("lblFhsNotes")).ForeColor = System.Drawing.ColorTranslator.FromHtml(htmlColor);
                }
                else
                {
                    string htmlColor = "blue";
                    ((LinkButton)dataItem.FindControl("lblFhsNotes")).ForeColor = System.Drawing.ColorTranslator.FromHtml(htmlColor);
                  
                }
            }
            catch(Exception ex) { }
            string sqlCRnote = "select count(*) from [Company_notes]  where Company_Name = '" + ((Label)dataItem.FindControl("lblcompanysname")).Text + "' and Note_Type ='Client'  and Allow_Notes='True' and Notes !=''";
            int objCRNoteResult = int.Parse(DatabaseHelper.executeScalar(sqlCRnote).ToString());
            try
            {
                if (objCRNoteResult > 0)
                {
                    string htmlColor = "red";
                    ((LinkButton)dataItem.FindControl("lblClientnotes")).ForeColor = System.Drawing.ColorTranslator.FromHtml(htmlColor);
                 
                }
                else
                {
                    string htmlColor = "blue";
                    ((LinkButton)dataItem.FindControl("lblClientnotes")).ForeColor = System.Drawing.ColorTranslator.FromHtml(htmlColor);
                }
            }
            catch { }
            try
            {
                sql = @"select project_id,project_name,Relevanturl from projects where active = 'Y' and CompanyName='" + ((Label)dataItem.FindControl("lblcompanysname")).Text + "' order by project_name;";
                DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);
                if (ds_dropdowns.Tables[0].Rows.Count > 0)
                {
                    try
                    {
                        CompProjects.DataSource = ds_dropdowns.Tables[0];
                        CompProjects.DataTextField = "project_name";
                        CompProjects.DataValueField = "project_id";
                        CompProjects.DataBind();
                        CompProjects.Items.Insert(0, new ListItem("[Projects]", ""));

                        CompRelevanturl.DataSource = ds_dropdowns.Tables[0];
                        CompRelevanturl.DataTextField = "Relevanturl";
                        CompRelevanturl.DataValueField = "project_id";
                        CompRelevanturl.DataBind();
                        CompRelevanturl.Items.Insert(0, new ListItem("[Relevanturl]", ""));

                    }
                    catch { }
                }
                else
                {
                    CompProjects.Items.Insert(0, new ListItem("[Projects]", ""));
                }
            }
            catch { }

            try
            {
                sql = @"select RegId,UserName	from NonesCRMusers where CompanyName='" + ((Label)dataItem.FindControl("lblcompanysname")).Text + "' order by UserName;";
                DataSet dsclient_dropdowns = DatabaseHelper.getDataset(sql);
                if (dsclient_dropdowns.Tables[0].Rows.Count > 0)
                {
                    try
                    {
                        CompClients.DataSource = dsclient_dropdowns.Tables[0];
                        CompClients.DataTextField = "UserName";
                        CompClients.DataValueField = "RegId";
                        CompClients.DataBind();
                        CompClients.Items.Insert(0, new ListItem("[Clients]", ""));
                    }
                    catch { }
                }
                else
                {
                    CompClients.Items.Insert(0, new ListItem("[Clients]", ""));
                }
           }
            catch { }
            try
            {
                string Sql = "select distinct CompanyName,GoogleUserId,GooglePwd from projects  where GoogleUserId is not null";
                DataSet ds = DatabaseHelper.getDataset(Sql);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        if (e.Item.Cells[1].Text == ds.Tables[0].Rows[i].ItemArray[0].ToString())
                        {
                            Label user = (Label)e.Item.FindControl("lblgoogleusername");
                            Label password = (Label)e.Item.FindControl("lblgooglepassword");
                            user.Text = ds.Tables[0].Rows[i].ItemArray[1].ToString();
                            password.Text = ds.Tables[0].Rows[i].ItemArray[2].ToString();
                            ((LinkButton)dataItem.FindControl("lblAnalytics")).ForeColor = System.Drawing.ColorTranslator.FromHtml("red"); 
                             break;
                            
                        }
                        else
                        {
                            
                            ((LinkButton)dataItem.FindControl("lblAnalytics")).ForeColor = System.Drawing.ColorTranslator.FromHtml("blue"); 
                        }
                    }
                }
            }
            catch
            {
            }
        }
    }
    protected void DataGrid1_ItemCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
    {
        GridDataItem dataItem = e.Item as GridDataItem;
        if (e.CommandName == "delete")
        {
         
            object objResult = DatabaseHelper.executeScalar("select count(*) from tasks where CompanyName =" + ((Label)dataItem.FindControl("lblcompanysname")).Text);
            object objResult2 = DatabaseHelper.executeScalar("select count(*) from ClientRequest where CompanyName =" + ((Label)dataItem.FindControl("lblcompanysname")).Text);
            if (objResult.ToString() == "0" && objResult2.ToString() == "0")
            {
                string strSql = "delete from Company where Company_id=" + ((Label)dataItem.FindControl("lblCompanyid")).Text;
                int intResult = DatabaseHelper.executeNonQuery(strSql);
                if (intResult != 0)
                {
                    string strSqlDel = "delete from Company_notes where Company_name=" + ((Label)dataItem.FindControl("lblcompanysname")).Text;
                    int intResultDel = DatabaseHelper.executeNonQuery(strSqlDel);
                    divMessage.InnerHtml = "Company was deleted.";
                    bindData();
                }
                else
                {
                    divMessage.InnerHtml = "Company was not deleted.";
                    bindData();
                }
            }

            else
            {
                divMessage.InnerHtml = "You can't delete company \"" + ((Label)dataItem.FindControl("lblcompanysname")).Text + "\"  because some tasks/CRs still reference it...";
                
            }
        }
        else if (e.CommandName == "FhsNotes")
        {
            //==============================================================================================================================
            //Session["TaskComp"] = ((Label)dataItem.FindControl("lblcompanysname")).Text;
            //Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> window.open('companyNotes.aspx','_blank'); </script>");
            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> window.open('companyNotes.aspx?CompName=" + ((Label)dataItem.FindControl("lblcompanysname")).Text + "','_blank'); </script>");
            //==============================================================================================================================

        }
        else if (e.CommandName == "Clientnote")
        {
            //==============================================================================================================================
            //Session["TaskComp"] = ((Label)dataItem.FindControl("lblcompanysname")).Text;
            //Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> window.open('CRNotes.aspx','_blank'); </script>");
            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> window.open('CRNotes.aspx?CompName=" + ((Label)dataItem.FindControl("lblcompanysname")).Text + "','_blank'); </script>");
            //==============================================================================================================================
        }
        else if (e.CommandName == "Related")
        {
            Session["CompanyRelated"] = ((Label)dataItem.FindControl("lblcompanysname")).Text;
            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> window.open('CompanyRelated.aspx','_blank'); </script>");
        }
        else if (e.CommandName == "Analytics")
        {
            Session["GoogleUser"] = ((Label)e.Item.FindControl("lblgoogleusername")).Text;
            Session["GooglePwd"] = ((Label)e.Item.FindControl("lblgooglepassword")).Text;
            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> window.open('Analytics.aspx','_blank'); </script>");
        }
        else if (e.CommandName == "esActivityLog")
        {
             Session["company"] = ((Label)dataItem.FindControl("lblcompanysname")).Text;
             Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> window.open('esActivityLog.aspx','_blank'); </script>");
        }
        
    }
    protected void DataGrid1_PageIndexChanged(object source, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        DataGrid1.CurrentPageIndex = e.NewPageIndex;
        bindData();
    }
    protected void DataGrid1_SortCommand(object source, Telerik.Web.UI.GridSortCommandEventArgs e)
    {
        if (e.SortExpression.ToString() == Session["companyColumn"])
        {
            //Reverse the sort order
            if (Session["CompanyOrderBy"] == "DESC")
            {
                lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " ASC";
                Session["CompanyOrderBy"] = "ASC";
            }
            else
            {
                lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " DESC";
                Session["CompanyOrderBy"] = "DESC";
            }
        }
        else
        {
             lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " DESC";
            Session["CompanyOrderBy"] = "DESC";
        }

        Session["companyColumn"] = e.SortExpression.ToString();
        Session["CompanyOrderBy"] = lblOrderBy.Text;
        bindData();
    }
        protected void DrpProjects_SelectedIndexChanged(object sender, EventArgs e)
    {

        System.Web.UI.WebControls.DropDownList lstProject = (System.Web.UI.WebControls.DropDownList)sender;
        string FrmCompany = "True";
        Response.Redirect("edit_project.aspx?id=" + lstProject.SelectedValue + "&Comp=" + FrmCompany, false);

    }

    protected void DrpRelevanturl_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            
            System.Web.UI.WebControls.DropDownList lsturl = (System.Web.UI.WebControls.DropDownList)sender;
            DataGridItem dgi = (DataGridItem)(((DropDownList)sender).Parent.Parent);
            int i = dgi.ItemIndex;
            ((Label)DataGrid1.Items[i].FindControl("lbl_url")).Text = "<br/><a href=" + lsturl.SelectedItem.Text + " target=" + "_blank>click here to view</a>";
            if (((Label)DataGrid1.Items[i].FindControl("lbl_url")).Text != "" || ((Label)DataGrid1.Items[i].FindControl("lbl_url")).Text != null)
            {
                ((Label)DataGrid1.Items[i].FindControl("lbl_url")).Visible = true;
            }
        }
        catch
        { }
    }
    protected void DrpClients_SelectedIndexChanged(object sender, EventArgs e)
    {
        System.Web.UI.WebControls.DropDownList lstClient = (System.Web.UI.WebControls.DropDownList)sender;
        string FrmCompany = "True";
        Response.Redirect("edit_client.aspx?id=" + lstClient.SelectedValue + "&Comp=" + FrmCompany, false);

    }

    protected void lnkAll_Click(object sender, EventArgs e)
    {
        strfilter = "";
        bindData();

    }
    protected void lnkA_Click(object sender, EventArgs e)
    {
        string strchar = "A";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkB_Click(object sender, EventArgs e)
    {
        string strchar = "B";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkC_Click(object sender, EventArgs e)
    {
        string strchar = "C";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkD_Click(object sender, EventArgs e)
    {
        string strchar = "D";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkE_Click(object sender, EventArgs e)
    {
        string strchar = "E";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkF_Click(object sender, EventArgs e)
    {
        string strchar = "F";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkG_Click(object sender, EventArgs e)
    {
        string strchar = "G";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkH_Click(object sender, EventArgs e)
    {
        string strchar = "H";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkI_Click(object sender, EventArgs e)
    {
        string strchar = "I";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkJ_Click(object sender, EventArgs e)
    {
        string strchar = "J";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkK_Click(object sender, EventArgs e)
    {
        string strchar = "K";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkL_Click(object sender, EventArgs e)
    {
        string strchar = "L";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();

    }
    protected void lnkM_Click(object sender, EventArgs e)
    {
        string strchar = "M";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkN_Click(object sender, EventArgs e)
    {
        string strchar = "N";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkO_Click(object sender, EventArgs e)
    {
        string strchar = "O";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();

    }
    protected void lnkP_Click(object sender, EventArgs e)
    {
        string strchar = "P";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();

    }
    protected void lnkQ_Click(object sender, EventArgs e)
    {
        string strchar = "Q";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkR_Click(object sender, EventArgs e)
    {
        string strchar = "R";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();

    }
    protected void lnkS_Click(object sender, EventArgs e)
    {
        string strchar = "S";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();

    }
    protected void lnkT_Click(object sender, EventArgs e)
    {
        string strchar = "T";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();

    }
    protected void lnkU_Click(object sender, EventArgs e)
    {

        string strchar = "U";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkV_Click(object sender, EventArgs e)
    {
        string strchar = "V";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkW_Click(object sender, EventArgs e)
    {
        string strchar = "W";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkX_Click(object sender, EventArgs e)
    {
        string strchar = "X";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkY_Click(object sender, EventArgs e)
    {
        string strchar = "Y";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkZ_Click(object sender, EventArgs e)
    {
        string strchar = "Z";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }

}
