<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Company31August.aspx.cs" Inherits="Admin_Company" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="Notifications.ascx" TagName="Notifications" TagPrefix="uc4" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <%--<script type="text/javascript">

        function OpenPopup(w, h) {
            var left = (screen.width / 2) - (w / 2);
            var top = (screen.height / 2) - (h / 2);
            var q = document.getElementById("DrpRelevanturl").selectedText;
            window.open(q, "List", "scrollbars=yes,menubar=no,toolbar=no,location=center,resizable=no,");
            //if (window.focus) {newwindow.focus()}

            return false;

        }
        function PopupCenter(pageURL, title, w, h) {
            var left = (screen.width / 2) - (w / 2);
            var top = (screen.height / 2) - (h / 2);
            var targetWin = window.open(pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
        } 
    </script>--%>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">

        <script type="text/javascript">
        function fnclick() {

            var ddlSelectVal = document.getElementById('DataGrid1_ctl03_DrpRelevanturl').options[document.getElementById('DataGrid1_ctl03_DrpRelevanturl').selectedIndex].value;

            window.open("NewPage.aspx?id=" + ddlSelectVal);
            return false;
        } function Navigate() {
            window.open("http://www.google.co.in");
        }
        </script>

    </telerik:RadCodeBlock>
    <title>
        <%= ConfigurationManager.AppSettings["CompanyName"].ToString()%>
    </title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
     table#DataGrid1_ctl00 tr th a
     {
      display:block;
      text-decoration:none;
      margin:0;
      padding:0;
      width:100%;
      height:100%;
     }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="wrapper">
            <uc2:Admin_Header ID="Admin_Header1" runat="server" />
            <div id="Content">
                <div id="Content_98" style="width: 100%">
                    <div class="titleText" style="margin: 10px 0 20px 0; display: none;">
                        Companies&nbsp;|&nbsp;</div>
                    <div>
                        <uc4:Notifications ID="Notifications" runat="server" />
                    </div>
                    <%--  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>--%>
                    <div id="divMessage" runat="server" style="color: Red; font-weight: bold">
                    </div>
                    <br />
                    <div class="divBorder" style="float: left">
                        <div style="padding: 5px;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="left" valign="middle" width="25%">
                                        <span class="text_default"><a href="edit_company.aspx">Add new company</a><a href="esActivityLog.aspx">&nbsp;|&nbsp;EstateCRM
                                            activity</a> </span>
                                    </td>
                                    <td align="center" valign="middle" width="50%">
                                        <asp:LinkButton ID="lnkAll" runat="server" Font-Bold="true" OnClick="lnkAll_Click">All</asp:LinkButton>
                                        &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp;
                                        <asp:LinkButton ID="lnkA" runat="server" Font-Bold="true" OnClick="lnkA_Click">A</asp:LinkButton>
                                        <asp:LinkButton ID="lnkB" runat="server" Font-Bold="true" OnClick="lnkB_Click">B</asp:LinkButton>
                                        <asp:LinkButton ID="lnkC" runat="server" Font-Bold="true" OnClick="lnkC_Click">C</asp:LinkButton>
                                        <asp:LinkButton ID="lnkD" runat="server" Font-Bold="true" OnClick="lnkD_Click">D</asp:LinkButton>
                                        <asp:LinkButton ID="lnkE" runat="server" Font-Bold="true" OnClick="lnkE_Click">E</asp:LinkButton>
                                        <asp:LinkButton ID="lnkF" runat="server" Font-Bold="true" OnClick="lnkF_Click">F</asp:LinkButton>
                                        <asp:LinkButton ID="lnkG" runat="server" Font-Bold="true" OnClick="lnkG_Click">G</asp:LinkButton>
                                        <asp:LinkButton ID="lnkH" runat="server" Font-Bold="true" OnClick="lnkH_Click">H</asp:LinkButton>
                                        <asp:LinkButton ID="lnkI" runat="server" Font-Bold="true" OnClick="lnkI_Click">I</asp:LinkButton>
                                        <asp:LinkButton ID="lnkJ" runat="server" Font-Bold="true" OnClick="lnkJ_Click">J</asp:LinkButton>
                                        <asp:LinkButton ID="lnkK" runat="server" Font-Bold="true" OnClick="lnkK_Click">K</asp:LinkButton>
                                        <asp:LinkButton ID="lnkL" runat="server" Font-Bold="true" OnClick="lnkL_Click">L</asp:LinkButton>
                                        <asp:LinkButton ID="lnkM" runat="server" Font-Bold="true" OnClick="lnkM_Click">M</asp:LinkButton>
                                        <asp:LinkButton ID="lnkN" runat="server" Font-Bold="true" OnClick="lnkN_Click">N</asp:LinkButton>
                                        <asp:LinkButton ID="lnkO" runat="server" Font-Bold="true" OnClick="lnkO_Click">O</asp:LinkButton>
                                        <asp:LinkButton ID="lnkP" runat="server" Font-Bold="true" OnClick="lnkP_Click">P</asp:LinkButton>
                                        <asp:LinkButton ID="lnkQ" runat="server" Font-Bold="true" OnClick="lnkQ_Click">Q</asp:LinkButton>
                                        <asp:LinkButton ID="lnkR" runat="server" Font-Bold="true" OnClick="lnkR_Click">R</asp:LinkButton>
                                        <asp:LinkButton ID="lnkS" runat="server" Font-Bold="true" OnClick="lnkS_Click">S</asp:LinkButton>
                                        <asp:LinkButton ID="lnkT" runat="server" Font-Bold="true" OnClick="lnkT_Click">T</asp:LinkButton>
                                        <asp:LinkButton ID="lnkU" runat="server" Font-Bold="true" OnClick="lnkU_Click">U</asp:LinkButton>
                                        <asp:LinkButton ID="lnkV" runat="server" Font-Bold="true" OnClick="lnkV_Click">V</asp:LinkButton>
                                        <asp:LinkButton ID="lnkW" runat="server" Font-Bold="true" OnClick="lnkW_Click">W</asp:LinkButton>
                                        <asp:LinkButton ID="lnkX" runat="server" Font-Bold="true" OnClick="lnkX_Click">X</asp:LinkButton>
                                        <asp:LinkButton ID="lnkY" runat="server" Font-Bold="true" OnClick="lnkY_Click">Y</asp:LinkButton>
                                        <asp:LinkButton ID="lnkZ" runat="server" Font-Bold="true" OnClick="lnkZ_Click">Z</asp:LinkButton>
                                    </td>
                                    <td align="right" valign="middle">
                                        <b>Records: </b>
                                        <asp:Label ID="LblRecordsno" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <%--*****************************************************************************************************************************--%>
                        <%--<asp:DataGrid ID="DataGrid11" runat="server" AutoGenerateColumns="False" CellPadding="5"
                            OnItemCommand="DataGrid1_ItemCommand" OnItemDataBound="DataGrid1_ItemDataBound"
                            AllowPaging="True" OnPageIndexChanged="DataGrid1_PageIndexChanged" PageSize="20"
                            AllowSorting="True" OnSortCommand="DataGrid1_SortCommand" Width="100%">
                            <PagerStyle NextPageText="Next" PrevPageText="Prev" />
                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                Font-Underline="False" HorizontalAlign="Center" />
                            <HeaderStyle Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False"
                                HorizontalAlign="Center" BackColor="#282C5F" CssClass="tblTitle1" />
                            <Columns>
                                <asp:BoundColumn DataField="Company_id" HeaderText="Company id" SortExpression="Company_id">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="Company_Name" HeaderText="Company name" SortExpression="Company_Name">
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" />
                                </asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="Projects">
                                    <ItemTemplate>
                                        <asp:DropDownList runat="server" Width="150px" ID="DrpProjects" AutoPostBack="true"
                                            OnSelectedIndexChanged="DrpProjects_SelectedIndexChanged" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Register clients">
                                    <ItemTemplate>
                                        <asp:DropDownList runat="server" Width="150px" ID="DrpClients" AutoPostBack="true"
                                            OnSelectedIndexChanged="DrpClients_SelectedIndexChanged" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Relevant url">
                                    <ItemTemplate>
                                        <asp:DropDownList runat="server" Width="150px" ID="DrpRelevanturl" AutoPostBack="true"
                                            OnSelectedIndexChanged="DrpRelevanturl_SelectedIndexChanged" />
                                        <asp:Label ID="lbl_url" runat="server" Visible="false"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="googleusername" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblgoogleusername" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="googlepassword" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblgooglepassword" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:ButtonColumn CommandName="Related" HeaderText="Company related tasks/CRs" Text="Related tasks/CRs">
                                </asp:ButtonColumn>
                                <asp:ButtonColumn CommandName="Analytics" HeaderText="Analytics" Text="Analytics"></asp:ButtonColumn>
                                <asp:ButtonColumn CommandName="esActivityLog" HeaderText="esActivityLog" Text="esActivityLog">
                                </asp:ButtonColumn>
                                <asp:BoundColumn DataField="active" HeaderText="Active" SortExpression="active"></asp:BoundColumn>
                                <asp:ButtonColumn CommandName="FhsNotes" HeaderText="FHS notes" Text="FHS notes"></asp:ButtonColumn>
                                <asp:ButtonColumn CommandName="Clientnote" HeaderText="Client notes" Text="Client notes">
                                </asp:ButtonColumn>
                                <asp:HyperLinkColumn DataNavigateUrlField="Company_id" DataNavigateUrlFormatString="edit_company.aspx?id={0}"
                                    HeaderText="Edit" NavigateUrl="edit_company.aspx" Text="edit"></asp:HyperLinkColumn>
                                <asp:ButtonColumn CommandName="Delete" HeaderText="Delete" Text="delete"></asp:ButtonColumn>
                            </Columns>
                        </asp:DataGrid>--%>
                        <telerik:RadGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" CellPadding="5"
                            OnItemCommand="DataGrid1_ItemCommand" OnItemDataBound="DataGrid1_ItemDataBound"
                            AllowPaging="True" OnPageIndexChanged="DataGrid1_PageIndexChanged" Width="100%"
                            OnSortCommand="DataGrid1_SortCommand" PageSize="20" AllowSorting="True">
                            <PagerStyle Mode="NextPrevAndNumeric" /> 
                            <MasterTableView PagerStyle-AlwaysVisible="true"  >
                                <Columns>
                                    <%--0--%>
                                    <telerik:GridTemplateColumn HeaderText="Company id" SortExpression="Company_id">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCompanyid" runat="server" Text='<%# bind("Company_id") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <%--1--%>
                                    <telerik:GridTemplateColumn HeaderText="Company name" SortExpression="Company_Name">
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblcompanysname" runat="server" Text='<%# bind("Company_Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <%--2--%>
                                    <telerik:GridTemplateColumn HeaderText="Projects">
                                        <ItemTemplate>
                                            <asp:DropDownList runat="server" Width="150px" ID="DrpProjects" AutoPostBack="true"
                                                OnSelectedIndexChanged="DrpProjects_SelectedIndexChanged" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <%--3--%>
                                    <telerik:GridTemplateColumn HeaderText="Register clients">
                                        <ItemTemplate>
                                            <asp:DropDownList runat="server" Width="150px" ID="DrpClients" AutoPostBack="true"
                                                OnSelectedIndexChanged="DrpClients_SelectedIndexChanged" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <%--4--%>
                                    <telerik:GridTemplateColumn HeaderText="Relevant url">
                                        <ItemTemplate>
                                            <asp:DropDownList runat="server" Width="150px" ID="DrpRelevanturl" AutoPostBack="true"
                                                OnSelectedIndexChanged="DrpRelevanturl_SelectedIndexChanged" />
                                            <asp:Label ID="lbl_url" runat="server" Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <%--5--%>
                                    <telerik:GridTemplateColumn HeaderText="googleusername" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblgoogleusername" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <%--6--%>
                                    <telerik:GridTemplateColumn HeaderText="googlepassword" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblgooglepassword" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <%--7--%>
                                    <telerik:GridButtonColumn CommandName="Related" HeaderText="Company related tasks/CRs"
                                        Text="Related tasks/CRs">
                                    </telerik:GridButtonColumn>
                                    <%--8--%>
                                    <%--<telerik:GridButtonColumn CommandName="Analytics" HeaderText="Analytics" Text="Analytics"></telerik:GridButtonColumn>--%>
                                    <telerik:GridTemplateColumn HeaderText="Analytics">
                                        <ItemTemplate>
                                         <asp:LinkButton CommandName="Analytics" ID="lblAnalytics" runat="server"  Text="Analytics"></asp:LinkButton>  
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    
                                    
                                    <%--9--%>
                                    <telerik:GridButtonColumn CommandName="esActivityLog" HeaderText="esActivityLog" Text="esActivityLog">
                                    </telerik:GridButtonColumn>
                                    <%--10--%>
                                    <telerik:GridTemplateColumn HeaderText="Active" SortExpression="active">
                                        <ItemTemplate>
                                            <asp:Label ID="lblactive" runat="server" Text='<%# bind("active") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <%--11--%>
                                     <%--<telerik:GridButtonColumn CommandName="FhsNotes" HeaderText="FHS notes" Text="FHS notes"  UniqueName="Ufhsnotes" ></telerik:GridButtonColumn>--%>
                                    <telerik:GridTemplateColumn HeaderText="FHS notes" >
                                        <ItemTemplate>
                                         <asp:LinkButton CommandName="FhsNotes" ID="lblFhsNotes" runat="server"  Text="FHS notes"></asp:LinkButton>  
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <%--12--%>
                                    <%--<telerik:GridButtonColumn CommandName="Clientnote" HeaderText="Client notes" Text="Client notes"></telerik:GridButtonColumn>--%>
                                    <telerik:GridTemplateColumn HeaderText="Client notes" >
                                        <ItemTemplate>
                                         <asp:LinkButton CommandName="Clientnote" ID="lblClientnotes" runat="server"  Text="Client notes"></asp:LinkButton>  
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                   
                                    <%--13--%>
                                    <telerik:GridHyperLinkColumn DataNavigateUrlFields="Company_id" DataNavigateUrlFormatString="edit_company.aspx?id={0}"
                                        HeaderText="Edit" NavigateUrl="edit_company.aspx" Text="edit">
                                    </telerik:GridHyperLinkColumn>
                                    <%--14--%>
                                     
                                     <telerik:GridTemplateColumn HeaderText="Delete" >
                                        <ItemTemplate>
                                         <asp:LinkButton ID="lblDelete" runat="server"  Text="delete" CommandName="delete"   ></asp:LinkButton>  
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    
                                    
                                    
                                    <%--<telerik:GridButtonColumn CommandName="Delete" HeaderText="Delete" Text="delete"></telerik:GridButtonColumn>--%>
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                        <%--*********************************************************************************************************************************--%>
                        &nbsp;
                    </div>
                    <%--</ContentTemplate>
                </asp:UpdatePanel>--%>
                </div>
            </div>
            <uc1:footer ID="Footer1" runat="server" />
            <asp:Label ID="lblOrderBy" runat="server" Text="" Visible="false"></asp:Label></div>
    </form>
</body>
</html>
