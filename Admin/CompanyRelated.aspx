<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CompanyRelated.aspx.cs" Inherits="Admin_ComapnyRelated" %>

<%@ Register Src="Notifications.ascx" TagName="Notifications" TagPrefix="uc4" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        Company</title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        function window.confirm(str) {
            execScript('n = msgbox("' + str + '","4132")', "vbscript");
            return (n == 6);
        }
    </script>

    <script type="text/javascript">
        function get_(div_) {

            div_ = div_.id + "1";

            document.getElementById(div_).style.display = "block";
        }
        function get_1(div_) {

            div_ = div_.id + "1";

            document.getElementById(div_).style.display = "none";
        }
    </script>

    <script type="text/javascript" language="javascript">
        function EnablePRlink() {
            var objPR = document.getElementById("DrpMultipleTaskPR").value;

            if (objPR != "") {

                document.getElementById("LnkBtnToChngPR").disabled = false;
                document.getElementById("DrpMultipleTaskStatus").value = "0";

                document.getElementById("LnkChngStatus").disabled = "disabled";
                document.getElementById("LnkChngAssingedTo").disabled = "disabled";
                document.getElementById("DrpMultipleTaskAssing").value = "0";


            }
            else {

                document.getElementById("LnkBtnToChngPR").disabled = "disabled";

                document.getElementById("LnkChngStatus").disabled = "disabled";

                document.getElementById("DrpMultipleTaskStatus").value = "0";
                document.getElementById("DrpMultipleTaskPR").value = "0";
                document.getElementById("LnkChngAssingedTo").disabled = "disabled";
                document.getElementById("DrpMultipleTaskAssing").value = "0";
            }

            return true;

        }


        function EnableAssinglink() {

            var objAssing = document.getElementById("DrpMultipleTaskAssing").value;

            if (objAssing != "0") {

                document.getElementById("LnkBtnToChngPR").disabled = "disabled";

                document.getElementById("LnkChngStatus").disabled = "disabled";
                ///document.getElementById("DrpMultipleTaskAssing").value = "0";
                document.getElementById("DrpMultipleTaskStatus").value = "0";
                document.getElementById("DrpMultipleTaskPR").value = "0";
                document.getElementById("LnkChngAssingedTo").disabled = false;
                ///document.getElementById("LnkChngAssingedTo").disabled = "disabled";
            }
            else {

                document.getElementById("LnkBtnToChngPR").disabled = "disabled";
                document.getElementById("LnkChngAssingedTo").disabled = "disabled";
                document.getElementById("LnkChngStatus").disabled = "disabled";
                document.getElementById("DrpMultipleTaskAssing").value = "0";
                document.getElementById("DrpMultipleTaskStatus").value = "0";
                document.getElementById("DrpMultipleTaskPR").value = "0";

            }

            return;
        }

        function EnableStatuslink() {
            var objStatus = document.getElementById("DrpMultipleTaskStatus").value;

            if (objStatus != "0") {


                document.getElementById("LnkBtnToChngPR").disabled = "disabled";

                document.getElementById("LnkChngStatus").disabled = false;


                document.getElementById("DrpMultipleTaskPR").value = "";
                document.getElementById("DrpMultipleTaskAssing").value = "0";
                document.getElementById("LnkChngAssingedTo").disabled = "disabled";

            }
            else {

                document.getElementById("LnkBtnToChngPR").disabled = "disabled";

                document.getElementById("LnkChngStatus").disabled = "disabled";

                document.getElementById("DrpMultipleTaskStatus").value = "0";
                document.getElementById("DrpMultipleTaskPR").value = "0";
                document.getElementById("DrpMultipleTaskAssing").value = "0";
                document.getElementById("LnkChngAssingedTo").disabled = "disabled";

            }
            return;

        }
    </script>

    <%--<script src="../js/popupscript.js" type="text/javascript" language="javascript"/>--%>
    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="slider.css" />

    <script type="text/javascript" src="slider.js"></script>

</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <uc2:Admin_Header ID="Admin_Header1" runat="server" />
        <div id="content">
           <%-- <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                <ProgressTemplate>
                    <div id="processmessage" class="processMessage">
                        <img alt="loading" src="images/ajax-loader.gif" style="width: 80px; height: 80px" />
                        <span style="color: black; font-weight: bold; font-size: 18px; vertical-align: super;">
                            Please wait... </span>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>--%>
                    <div id="Content_98">
                        <div class="titleText" style="display: none;">
                            Company related&nbsp;|&nbsp;<span class="text_default"><a href="edit_task.aspx">add
                                new task</a></span>
                         </div>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="left" valign="top">
                                    <div style="text-align: left">
                                        <div style="padding: 0px 0 0px 5px; margin: 2px  0 -2px 0;">
                                            <table>
                                                <tr>
                                                    <td valign="bottom" style="background: url(../images/yellowbar_filter.png) no-repeat left top;
                                                        width: 101px; height: 27px; text-align: center;">
                                                        <span class="link" onmousedown="slideContent('section-1')">Filter</span>
                                                    </td>
                                                    <td style="background: url(../images/yellowbar_filter.png) no-repeat left top; width: 101px;
                                                        height: 27px; text-align: center;" valign="bottom">
                                                        <span class="link" onmousedown="slideContent('section-2')">Action</span>
                                                    </td>
                                                    <td>
                                                        <div style="margin: 0px;">
                                                            <span id="TopFilterLable" runat="server" class="topfilter"></span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div>
                                            <table width="100%" border="0" cellpadding="0" cellspacing="0" onclick="return TABLE1_onclick()"
                                                id="TABLE1">
                                                <tr>
                                                    <td align="right" valign="bottom">
                                                        <img src="../images/filter_corner_l.png" width="11" height="59" />
                                                    </td>
                                                    <td bgcolor="#282C5F" width="100%">
                                                        <div class="slider">
                                                            <div class="slidercontent" id="slider">
                                                                <div id="section-1" class="section upper" style="margin-top: 0px;">
                                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" onclick="return TABLE1_onclick()"
                                                                        id="TABLE2">
                                                                        <tr>
                                                                            <td width="45" valign="bottom">
                                                                                <b style="color: #e1ac10;">Filter:</b>
                                                                            </td>
                                                                            <td valign="top">
                                                                                <br />
                                                                                <asp:DropDownList ID="drpFilter" runat="server" AutoPostBack="True" Width="96%" OnSelectedIndexChanged="drpFilter_SelectedIndexChanged"
                                                                                    CssClass="filerDrpodown">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td valign="top" style="color: White; font-size: 14px">
                                                                                Company<br />
                                                                                <asp:DropDownList ID="drpProjects" runat="server" CssClass="filerDrpodown" AutoPostBack="True"
                                                                                    OnSelectedIndexChanged="drpProjects_SelectedIndexChanged" Width="97%">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td valign="top" style="color: White; font-size: 14px">
                                                                                Priority<br />
                                                                                <asp:DropDownList ID="drpPriorities" runat="server" CssClass="filerDrpodown" AutoPostBack="True"
                                                                                    OnSelectedIndexChanged="drpPriorities_SelectedIndexChanged" Width="94%">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td valign="top" style="color: White; font-size: 14px">
                                                                                Status<br />
                                                                                <asp:DropDownList ID="drpStatuses" runat="server" CssClass="filerDrpodown" AutoPostBack="True"
                                                                                    OnSelectedIndexChanged="drpStatuses_SelectedIndexChanged" Width="96%">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td valign="bottom" class="whitetext2" width="85">
                                                                                <asp:Button ID="btnClearFilters" runat="server" OnClick="btnClearFilters_Click" Text="Clear filters"
                                                                                    CssClass="clearfilter" />
                                                                            </td>
                                                                            <td valign="bottom" style="color: White; font-size: 14px" width="140">
                                                                                <asp:CheckBox ID="chkViewDeleted" runat="server" Text="View only deleted" AutoPostBack="True"
                                                                                    OnCheckedChanged="chkViewDeleted_CheckedChanged" />
                                                                            </td>
                                                                            <td valign="bottom" style="color: White; font-size: 14px" width="100">
                                                                                <asp:CheckBox ID="chkHideClosed" runat="server" Text="View closed" AutoPostBack="True"
                                                                                    OnCheckedChanged="chkHideClosed_CheckedChanged" />
                                                                            </td>
                                                                            <td align="left" valign="bottom" class="whitetext2" width="90">
                                                                                <span style="color: #E1AC10"><b>&nbsp;Records: </b>
                                                                                    <asp:Label ID="LblRecordsno" runat="server" Text=""></asp:Label></span>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                                <div id="section-2" class="section upper" style="padding-top: 5px;">
                                                                    <div id="DivMiltiselection" runat="server" style="padding-bottom: 7px; padding-top: 5px">
                                                                        <b style="color: #e1ac10;">Action:</b>
                                                                        <asp:Button ID="LnkBtnToDeleteAll" runat="server" BackColor="transparent" ForeColor="#ffffff"
                                                                            BorderStyle="none" Text="Delete Selected" OnClick="LnkBtnToDeleteAll_Click" Style="padding: 0;" /><asp:LinkButton
                                                                                ID="LnkBtnToMarkAllRead" Visible="false" runat="server" OnClick="LnkBtnToMarkAllRead_Click">Mark read</asp:LinkButton>
                                                                        <asp:Button ID="LnkBtnToChngPR" runat="server" BackColor="transparent" ForeColor="#ffffff"
                                                                            BorderStyle="none" Text="Change priorirty to" OnClick="LnkBtnToChngPR_Click" /><asp:DropDownList
                                                                                ID="DrpMultipleTaskPR" runat="server" CssClass="filerDrpodown" AutoPostBack="true"
                                                                                Width="180px">
                                                                            </asp:DropDownList>
                                                                        <asp:Button ID="LnkChngAssingedTo" runat="server" BackColor="transparent" ForeColor="#ffffff"
                                                                            Visible="false" BorderStyle="none" Text="Change Assigned to" OnClick="LnkChngAssingedTo_Click" /><asp:DropDownList
                                                                                ID="DrpMultipleTaskAssing" runat="server" Visible="false" CssClass="filerDrpodown"
                                                                                AutoPostBack="false" Width="180px">
                                                                            </asp:DropDownList>
                                                                        <asp:Button ID="LnkChngStatus" runat="server" BackColor="transparent" ForeColor="#ffffff"
                                                                            BorderStyle="none" Text="Change status to" OnClick="LnkChngStatus_Click" /><asp:DropDownList
                                                                                ID="DrpMultipleTaskStatus" runat="server" CssClass="filerDrpodown" AutoPostBack="false"
                                                                                Width="180px">
                                                                            </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td valign="bottom">
                                                        <img src="../images/filter_corner_r.png" width="11" height="59" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div> 
                                </td>
                            </tr>
                        </table>
                        <div style="float: left">
                            <uc4:Notifications ID="Notifications" runat="server" />
                        </div>
                        <div id="divMessage" runat="server" style="color: Red; font-weight: bold;">
                        </div>
                        <div style="width: 100%; background: #ebebeb; padding: 5px 0; margin: 5px 0 1px 0;
                            float: left;" class="text_default">
                            &nbsp;&nbsp;<a href="edit_task.aspx">Add new task</a></div>
                            
                            <%--=========================================================================================================--%>
            <div>
            <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                <ProgressTemplate>
                    <div id="processmessage" class="processMessage">
                        <img alt="loading" src="images/ajax-loader.gif" style="width: 80px; height: 80px" />
                        <span style="color: black; font-weight: bold; font-size: 18px; vertical-align: super;">
                            Please wait... </span>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                <%--=========================================================================================================--%>
                            
                            
                        <asp:DataGrid ID="DataGrid1" runat="server" Width="100%" style="float:left;" AutoGenerateColumns="False"
                            AllowPaging="True" AllowSorting="True" CellPadding="5" PageSize="100" OnSortCommand="DataGrid1_SortCommand"
                            OnItemDataBound="DataGrid1_ItemDataBound" OnPageIndexChanged="DataGrid1_PageIndexChanged">
                            <PagerStyle NextPageText="Next" PrevPageText="Prev" ForeColor="Black" />
                            <HeaderStyle Font-Italic="False" Font-Overline="False" ForeColor="White" Font-Strikeout="False"
                                Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Top" BackColor="#282C5F"
                                CssClass="tblTitle1" />
                            <Columns>
                                <asp:BoundColumn DataField="ID" HeaderText="ID" Visible="False" SortExpression="ID">
                                </asp:BoundColumn>
                                <asp:TemplateColumn>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSelect" runat="server" /></ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="ID" SortExpression="ID">
                                    <ItemTemplate>
                                        <asp:Label Font-Size="13px" ID="lblID" runat="server" Text='<%# bind("ITEMID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Short Description" SortExpression="ShortDescr">
                                    <ItemTemplate>
                                        <asp:Label Font-Size="13px" ID="lblshrtDesc" runat="server" Width="200px" Text='<%# bind("ShortDescr") %>'></asp:Label>
                                        <div id="lblshrtDesc1" runat="server" style="display: none; position: absolute; background-color: #FEFFB3;
                                            width: 730px; border: solid 1px #333333; margin-top: 5px; padding-left: 3px">
                                            <asp:Label ID="LblLastcomment" runat="server" Text='<%# bind("ShowAllComment") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="Assigned" HeaderText="Assigned to" SortExpression="Assigned">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="Priority" HeaderText="Priority" SortExpression="Priority">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="status" HeaderText="Status" SortExpression="status">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="LastUpdatedBy" HeaderText="Last Updated By" SortExpression="LastUpdatedBy">
                                </asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="Last Updated" SortExpression="LastUpdatedOn">
                                    <ItemTemplate>
                                        <asp:Label ID="lblLastUpdatedOn" Font-Size="13px" Width="70px" CssClass="wordwrap"
                                            runat="server" Text='<%# bind("LastUpdatedOn") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Edit" SortExpression="ID">
                                    <ItemTemplate>
                                        <asp:Label ID="lbleditRelats" Font-Size="13px" runat="server" Text='<%# bind("editrelates") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Top" Font-Size="12px" />
                        </asp:DataGrid>
                       <%-- ===============================================================================================--%>
                       </ContentTemplate> 
                       </asp:UpdatePanel>
                       </div>
                        <%--===============================================================================================--%>
                        <div style="text-align: right" id="divTotalHrs" runat="server">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0" style="float:right;" >
                                <tr>
                                    <td align="right">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 85px;" align="right" valign="middle">
                                                    <b>Total Hrs:</b>
                                                </td>
                                                <td>
                                                    <table cellpadding="3" cellspacing="0" border="1">
                                                        <tr>
                                                            <td style="width: 85px" align="left" valign="top">
                                                                <asp:Label ID="lblETC" runat="server" Font-Bold="True" Font-Size="12px"></asp:Label>
                                                            </td>
                                                            <td style="width: 85px" align="left" valign="top">
                                                                <asp:Label ID="lblTotalHrsTaken" runat="server" Font-Bold="True" Font-Size="12px"></asp:Label>
                                                            </td>
                                                            <td style="width: 90px" align="left" valign="top">
                                                                <asp:Label ID="lblExpectedHrsLeft" runat="server" Font-Bold="True" Font-Size="12px"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="padding-right: 3px; padding-left: 3px; padding-bottom: 3px; width: 25px;
                                                    padding-top: 3px">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                <%--</ContentTemplate>
            </asp:UpdatePanel>--%>
        </div>
    </div>
    <uc1:footer ID="Footer1" runat="server" />
    <asp:Label ID="lblOrderBy" runat="server" Text="" Visible="false"></asp:Label>
    <asp:Label ID="LblCROredrBy" runat="server" Text="" Visible="false"></asp:Label>
    <div id="popup" class="popup" style="background-color: #e9e9e9; width: 650px; border: solid 1px #000000;
        border-bottom: solid 2px #000000; border-right: solid 2px #000000; padding: 10px 10px 10px 10px;
        position: absolute; display: none">
    </div>
    </form>
</body>
</html>
