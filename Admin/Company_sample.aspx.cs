using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Admin_Company_sample : System.Web.UI.Page
{
    string sql = "";
    string strfilter = "";
    protected void Page_Load(object sender, EventArgs e)
    {

        LnkMakecommentRead.Attributes.Add("onclick", "return confirm('Sure you want to mark every comment in every CR as read by you?');");
        LnkMakeTaskcommentRead.Attributes.Add("onclick", "javascript:return confirm('Sure you want to mark every comment in each of your tasks as read by you (this means you must have read them all and acted on all that require action already)?');");
        LnkMakeAllTaskCmmntRead.Attributes.Add("onclick", "javascript:return confirm('Sure you want to mark every comment in each of  tasks as read by you (this means you must have read them all and acted on all that require action already)?');");
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = "Company.aspx";
            Response.Redirect("login.aspx");
            return;
        }

        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            Response.Redirect("tasks.aspx");
            return;
        }
        
        
        divMessage.InnerHtml = "";

        if (!Page.IsPostBack)
        {
            lblOrderBy.Text = " ORDER BY Company_Name ASC";
            getFilterSession();
            bindData();
            getNewCR();
            getCRNewComment();
            getUnasweredQuestions();
            getPR0Tasks();
            getPR1aTasks();
            getPR1bTasks();
            getTskNewComment();
            getCRwaitingfrmClient();
            getAllTskNewComment();
        }
    }

    private void setFileterSession()
    {
        Session["CompanyOrderBy"] = lblOrderBy.Text;
    }

    private void getFilterSession()
    {
        try
        {
            if (Session["CompanyOrderBy"] != null)
                lblOrderBy.Text = Session["CompanyOrderBy"].ToString();
        }
        catch
        {

        }
    }

    private void bindData()
    {
        string strSql = "select * from Company " + strfilter + lblOrderBy.Text;

        DataSet ds = DatabaseHelper.getDataset(strSql);

        if (ds != null)
        {
            try
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DataGrid1.DataSource = ds.Tables[0];
                    DataGrid1.DataBind();
                    PagedDataSource objds = new PagedDataSource();
                    objds.DataSource = ds.Tables[0].DefaultView;

                    LblRecordsno.Text = objds.DataSourceCount.ToString();
                }
                else
                {
                    LblRecordsno.Text = "0";
                    DataGrid1.DataSource = null;
                    DataGrid1.DataBind();
                    divMessage.InnerHtml = "No company in the database..";
                }
            }
            catch
            {
                DataGrid1.DataSource = null;
                DataGrid1.DataBind();
                LblRecordsno.Text = "0";
                divMessage.InnerHtml = "No company in the database..";
            }
        }
        else
        {
            try
            {
                DataGrid1.DataSource = null;
                DataGrid1.DataBind();
                LblRecordsno.Text = "0";
                divMessage.InnerHtml = "No company in the database..";
            }
            catch
            {
                DataGrid1.DataSource = null;
                DataGrid1.DataBind();
                LblRecordsno.Text = "0";
                divMessage.InnerHtml = "No company in the database..";
            }
        }
    }
    protected void DataGrid1_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton deleteButton = (LinkButton)e.Item.Cells[10].Controls[0];
            DropDownList CompProjects = (DropDownList)e.Item.FindControl("DrpProjects");
            DropDownList CompClients = (DropDownList)e.Item.FindControl("DrpClients");

            //We can now add the onclick event handler
            deleteButton.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete this company?')");



            string sqlcompanynote = "select count(*) from [Company_notes]  where Company_Name = '" + e.Item.Cells[1].Text + "'and Note_Type ='FHS' and Allow_Notes='True' and Notes !=''";
            int objNoteResult = int.Parse(DatabaseHelper.executeScalar(sqlcompanynote).ToString());
            try
            {
                if (objNoteResult > 0)
                {
                    string htmlColor = "red";

                    e.Item.Cells[7].ForeColor = System.Drawing.ColorTranslator.FromHtml(htmlColor);
                    //e.Item.Cells[7].Enabled = true;

                }
                else
                {
                    string htmlColor = "blue";
                    e.Item.Cells[7].ForeColor = System.Drawing.ColorTranslator.FromHtml(htmlColor);
                   // e.Item.Cells[7].Enabled = false;
                }
            }
            catch { }


            string sqlCRnote = "select count(*) from [Company_notes]  where Company_Name = '" + e.Item.Cells[1].Text + "' and Note_Type ='Client'  and Allow_Notes='True' and Notes !=''";
            int objCRNoteResult = int.Parse(DatabaseHelper.executeScalar(sqlCRnote).ToString());

            try
            {
                if (objCRNoteResult > 0)
                {
                    string htmlColor = "red";

                    e.Item.Cells[8].ForeColor = System.Drawing.ColorTranslator.FromHtml(htmlColor);
                    //e.Item.Cells[8].Enabled = true;

                }
                else
                {
                    string htmlColor = "blue";
                    e.Item.Cells[8].ForeColor = System.Drawing.ColorTranslator.FromHtml(htmlColor);
                   // e.Item.Cells[8].Enabled = false;
                }
            }
            catch { }


           
            try
            {

                sql = @"select project_id,project_name
		from projects
		where active = 'Y' and CompanyName='" + e.Item.Cells[1].Text + "' order by project_name;";
                DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);
                if (ds_dropdowns.Tables[0].Rows.Count > 0)
                {
                    try
                    {
                        CompProjects.DataSource = ds_dropdowns.Tables[0];
                        CompProjects.DataTextField = "project_name";
                        CompProjects.DataValueField = "project_id";
                        CompProjects.DataBind();
                        CompProjects.Items.Insert(0, new ListItem("[Projects]", ""));

                    }
                    catch { }
                }
                else
                {
                    CompProjects.Items.Insert(0, new ListItem("[Projects]", ""));
                    ///drpProjects.Items.Insert(1, new ListItem("[Other]", "other"));
                }
            }
            catch { }

            try
            {


                sql = @"select RegId,UserName
		from NonesCRMusers
		where CompanyName='" + e.Item.Cells[1].Text + "' order by UserName;";
                DataSet dsclient_dropdowns = DatabaseHelper.getDataset(sql);
                if (dsclient_dropdowns.Tables[0].Rows.Count > 0)
                {
                    try
                    {
                        CompClients.DataSource = dsclient_dropdowns.Tables[0];
                        CompClients.DataTextField = "UserName";
                        CompClients.DataValueField = "RegId";
                        CompClients.DataBind();
                        CompClients.Items.Insert(0, new ListItem("[Clients]", ""));

                    }
                    catch { }
                }
                else
                {
                    CompClients.Items.Insert(0, new ListItem("[Clients]", ""));
                    ///drpProjects.Items.Insert(1, new ListItem("[Other]", "other"));
                }


            }
            catch { }


       }
    }
    protected void DataGrid1_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "delete")
        {
            object objResult = DatabaseHelper.executeScalar("select count(*) from tasks where CompanyName =" + e.Item.Cells[1].Text);
            object objResult2 = DatabaseHelper.executeScalar("select count(*) from ClientRequest where CompanyName =" + e.Item.Cells[1].Text);
            if (objResult.ToString() == "0" && objResult2.ToString() == "0")
            {
                string strSql = "delete from Company where Company_id=" + e.Item.Cells[0].Text;
                int intResult = DatabaseHelper.executeNonQuery(strSql);

                if (intResult != 0)
                {

                    string strSqlDel = "delete from Company_notes where Company_name" + e.Item.Cells[1].Text;
                    int intResultDel = DatabaseHelper.executeNonQuery(strSqlDel);
                   

                    divMessage.InnerHtml = "Company was deleted.";
                    bindData();
                }
                else
                {
                    divMessage.InnerHtml = "Company was not deleted.";
                    bindData();
                }
            }

            else
            {
                divMessage.InnerHtml = "You can't delete company \"" + e.Item.Cells[1].Text + "\"  because some tasks/CRs still reference it...";
            }
        }
        else if (e.CommandName == "FhsNotes")
        {
            Session["TaskComp"] = e.Item.Cells[1].Text;
            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> window.open('companyNotes_Sample.aspx','','status=no,scrollbars=yes,position=center,resizable=yes,width=screen.width,height=screen.height'); </script>");

        }
        else if (e.CommandName == "Clientnote")
        {
             Session["TaskComp"] = e.Item.Cells[1].Text;
             Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> window.open('CRNotes_Sample.aspx','','status=no,scrollbars=yes,position=center,resizable=yes,width=screen.width,height=screen.height'); </script>");

        }
        else if (e.CommandName == "Related")
        {

            Session["CompanyRelated"] = e.Item.Cells[1].Text;
            Response.Redirect("CompanyRelated.aspx", false);
        }
    }
    protected void DataGrid1_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        DataGrid1.CurrentPageIndex = e.NewPageIndex;
        bindData();
    }
    protected void DataGrid1_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        if (e.SortExpression.ToString() == Session["companyColumn"])
        {
            //Reverse the sort order
            if (Session["CompanyOrderBy"] == "DESC")
            {
                lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " ASC";
                Session["CompanyOrderBy"] = "ASC";
            }
            else
            {
                lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " DESC";
                Session["CompanyOrderBy"] = "DESC";                
            }
        }
        else
        {
            //Different column selected, so default to ascending order
            lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " DESC";
            Session["CompanyOrderBy"] = "DESC";
        }

        Session["companyColumn"] = e.SortExpression.ToString();
        Session["CompanyOrderBy"] = lblOrderBy.Text;
        bindData();
    }
    void getCRwaitingfrmClient()
    {
        sql = @"select ClientRequest.* from ClientRequest 
                where ClientRequest.Status = 'awaiting client response- required' and status <> 'closed' and deleted <> 1 ";



        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {

            if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
            {

                CRwocr.Visible = false;
            }
            else
            {
                LnkCRWocr.Text = "(" + ds.Tables[0].Rows.Count + ")";
                CRwocr.Visible = true;
            }

        }
        else
        {


            if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
            {

                CRwocr.Visible = false;
            }
            else
            {
                LnkCRWocr.Text = "(0)";
                CRwocr.Visible = false;
            }


        }
    }
    protected void LnkCRWocr_Click(object sender, EventArgs e)
    {

        Session["filter"] = "CRwocr";
        Response.Redirect("client_requests.aspx", false);


    }

    void getCRNewComment()
    {


        //// sql = @"select ClientRequest.* from ClientRequest where ClientRequest.[RequestId] in (select [RequestId] from ClientRequest_Details where [CommentId] not in (select  CommentId from read_CR_comments)) and ClientRequest.deleted <> 1";
        ////  sql = @"select ClientRequest_Details.* from ClientRequest_Details where [CommentId] not in (select  CommentId from read_CR_comments where [UserName]='Support') and ClientRequest_Details.deleted <> 1";
        sql = "select ClientRequest.* from ClientRequest where RequestId in(select ClientRequest_Details.RequestId from ClientRequest_Details where [CommentId] not in (select  CommentId from read_CR_comments where read_CR_comments.username='" + Session["admin"].ToString() + "') and ClientRequest_Details.deleted <> 1)";

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
            {
                /// LnkNewCR.Text = "(" + ds.Tables[0].Rows.Count + ")";
                CrNewComment.Visible = false;
                LnkMakecommentRead.Visible = false;
            }
            else
            {
                LnkCrNewComment.Text = "(" + ds.Tables[0].Rows.Count + ")";
                LnkMakecommentRead.Text = "(" + "-" + ")";
                LnkMakecommentRead.Visible = true;
                CrNewComment.Visible = true;
            }

        }
        else
        {

            if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
            {
                /// LnkNewCR.Text = "(" + ds.Tables[0].Rows.Count + ")";
                CrNewComment.Visible = false;
                LnkMakecommentRead.Visible = false;
            }
            else
            {
                LnkCrNewComment.Text = "(0)";
                CrNewComment.Visible = false;
                LnkMakecommentRead.Visible = false;
            }




        }
    }

    protected void LnkNewCR_Click(object sender, EventArgs e)
    {

        Session["filter"] = "NewCR";
        Response.Redirect("client_requests.aspx", false);
        /// bindPR1bTasks();

    }
    protected void LnkCrNewComment_Click(object sender, EventArgs e)
    {
        Session["filter"] = "CRNewComment";
        Response.Redirect("client_requests.aspx", false);

    }

    void getNewCR()
    {
        sql = @"select ClientRequest.* from ClientRequest 
                where ClientRequest.Status = 'new' and status <> 'closed' and deleted <> 1 ";

        ////if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        ////{
        ////    sql += " and status <> 'closed' and assigned_to_user = '" + Session["admin"] + "' ";
        ////}
        ////else
        ////{
        ////    sql += " and assigned_to_user = '" + Session["admin"] + "' ";
        ////}

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            ///  LnkNewCR.Text = "(" + ds.Tables[0].Rows.Count + ")";
            /// NewCR.Visible = true;
            if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
            {
                /// LnkNewCR.Text = "(" + ds.Tables[0].Rows.Count + ")";
                NewCR.Visible = false;
            }
            else
            {
                LnkNewCR.Text = "(" + ds.Tables[0].Rows.Count + ")";
                NewCR.Visible = true;
            }

        }
        else
        {


            if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
            {
                /// LnkNewCR.Text = "(" + ds.Tables[0].Rows.Count + ")";
                NewCR.Visible = false;
            }
            else
            {
                LnkNewCR.Text = "(0)";
                NewCR.Visible = false;
            }


        }
    }

    protected void LnkMakecommentRead_Click(object sender, EventArgs e)
    {
        string sql = "select *  from ClientRequest_Details where [CommentId] not in (select  CommentId  from read_CR_comments where read_CR_comments.username='" + Session["admin"].ToString() + "') and ClientRequest_Details.deleted <> 1";
        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {
                    string sqlinsert = "insert into read_CR_comments([CommentId],[UserName]) ";
                    sqlinsert += " values(" + ds.Tables[0].Rows[i]["CommentId"].ToString() + ",'" + Session["admin"].ToString() + "')";

                    int intResult = DatabaseHelper.executeNonQuery(sqlinsert);
                }
                catch
                {
                }
            }


        }

        getCRNewComment();

    }

    protected void lnkUnansweredQuestions_Click(object sender, EventArgs e)
    {
        Session["filter"] = "Unanswered";
        Session["filterunread"] = "";
        Response.Redirect("tasks.aspx");
    }

    protected void lnkPr0Task_Click(object sender, EventArgs e)
    {

        Session["filter"] = "Immediate";
        Session["filterunread"] = "";
        Response.Redirect("tasks.aspx");
    }

    protected void lnkHighestTasks_Click(object sender, EventArgs e)
    {
        Session["filter"] = "HighestTasks";
        Session["filterunread"] = "";
        Response.Redirect("tasks.aspx");
    }

    protected void lnkHighTasks_Click(object sender, EventArgs e)
    {
        Session["filter"] = "HighTasks";
        Session["filterunread"] = "";
        Response.Redirect("tasks.aspx");
    }

    void getUnasweredQuestions()
    {
//////        sql = @"select task_comments.* from task_comments,tasks 
//////                where tasks.task_id = task_comments.task_id and tasks.deleted <> 1 and tasks.status <> 'closed' and task_comments.deleted <> 1 and qflag=1 ";
       
//////        sql += " and assigned_to_user = '" + Session["admin"] + "' ";

        sql = @"select task_comments.* from task_comments,tasks 
                where tasks.task_id = task_comments.task_id and tasks.deleted <> 1 and tasks.status <> 'closed' and task_comments.deleted <> 1 and qflag=1 ";

        sql += " and task_comments.QuesTo = '" + Session["admin"] + "' ";


        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            lnkUnansweredQuestions.Text = "(" + ds.Tables[0].Rows.Count + ")";
            unansweredque.Visible = true;
        }
        else
        {
            lnkUnansweredQuestions.Text = "(0)";
            unansweredque.Visible = false;
        }
    }

    void getPR0Tasks()
    {
        sql = @"select tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '0 - IMMEDIATE' ";

        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            sql += " and status <> 'closed' and assigned_to_user = '" + Session["admin"] + "' ";
        }
        else
        {
            sql += " and assigned_to_user = '" + Session["admin"] + "' ";
        }

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            lnkPr0Task.Text = "(" + ds.Tables[0].Rows.Count + ")";
            immediatetask.Visible = true;
        }
        else
        {
            lnkPr0Task.Text = "(0)";
            immediatetask.Visible = false;
        }
    }

    void getPR1aTasks()
    {
        sql = @"select tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '1a - highest' ";

        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            sql += " and status <> 'closed' and assigned_to_user = '" + Session["admin"] + "' ";
        }
        else
        {
            sql += " and assigned_to_user = '" + Session["admin"] + "' ";
        }

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            lnkHighestTasks.Text = "(" + ds.Tables[0].Rows.Count + ")";
            highestTasks.Visible = true;
        }
        else
        {
            lnkHighestTasks.Text = "(0)";
            highestTasks.Visible = false;
        }
    }

    void getPR1bTasks()
    {
        sql = @"select tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '1b - high' ";

        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            sql += " and status <> 'closed' and assigned_to_user = '" + Session["admin"] + "' ";
        }
        else
        {
            sql += " and assigned_to_user = '" + Session["admin"] + "' ";
        }

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            lnkHighTasks.Text = "(" + ds.Tables[0].Rows.Count + ")";
            highTasks.Visible = true;
        }
        else
        {
            lnkHighTasks.Text = "(0)";
            highTasks.Visible = false;
        }
    }
    void getTskNewComment()
    {

       

        sql = "select tasks.* from tasks where task_id in(select task_comments.task_id from task_comments where tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 )";
      
        sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            LnkTskNewcomment.Text = "(" + ds.Tables[0].Rows.Count + ")";
            LnkMakeTaskcommentRead.Text = "(" + "-" + ")";
            LnkMakeTaskcommentRead.Visible = true;
            TskNewComment.Visible = true;
        }
        else
        {
            LnkTskNewcomment.Text = "(0)";
            TskNewComment.Visible = false;
            LnkMakeTaskcommentRead.Visible = false;
        }
    }
    protected void LnkTskNewcomment_Click(object sender, EventArgs e)
    {

        Session["filterunread"] = "TaskNewComment";
        ////Session["filter"] = "Immediate";
        Response.Redirect("tasks.aspx");


    }
    protected void LnkMakeTaskcommentRead_Click(object sender, EventArgs e)
    {
        sql = @"select *  from task_comments where task_id in (select task_id  from tasks where tasks.task_id = task_comments.task_id and  tasks.assigned_to_user = '" + Session["admin"].ToString() + "'  and tasks.deleted <> 1 and tasks.status <> 'closed') and task_comments.tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 ";
        /// sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {

                    //////string sqlgetcmntId = "select tc_id from task_comments where task_id ='" + ds.Tables[0].Rows[i]["task_id"].ToString() + "'";
                    //////object TskcmntID = DatabaseHelper.executeScalar(sqlgetcmntId);


                    //////if (TskcmntID!=null)
                    //////{
                    string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                    sqlinsert += " values(" + ds.Tables[0].Rows[i]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                    int intResult = DatabaseHelper.executeNonQuery(sqlinsert);
                    string sqlinsert2 = "insert into read_task([task_id],[username]) ";
                    sqlinsert2 += " values(" + ds.Tables[0].Rows[i]["task_id"].ToString() + ",'" + Session["admin"].ToString() + "')";
                    int intResult2 = DatabaseHelper.executeNonQuery(sqlinsert2);
                    //////}
                }
                catch
                {
                }
            }


        }

        // getTskNewComment();
        Response.Redirect("tasks.aspx", false);
    }


    void getAllTskNewComment()
    {

        sql = "select tasks.* from tasks where task_id in(select task_comments.task_id from task_comments where tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 )";

       //// sql += "and tasks.deleted <> 1 and tasks.status <> 'closed'";

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
            {
                LnkAllTsknewComment.Text = "(0)";
                SpnAllTaskNewComment.Visible = false;
                LnkMakeAllTaskCmmntRead.Visible = false;
                LnkAllTsknewComment.Visible = false;

            }
            else
            {


                LnkAllTsknewComment.Text = "(" + ds.Tables[0].Rows.Count + ")";
                LnkMakeAllTaskCmmntRead.Text = "(" + "-" + ")";
                LnkMakeAllTaskCmmntRead.Visible = true;
                SpnAllTaskNewComment.Visible = true;

            }
        }
        else
        {
            LnkAllTsknewComment.Text = "(0)";
            SpnAllTaskNewComment.Visible = false;
            LnkMakeAllTaskCmmntRead.Visible = false;
            LnkAllTsknewComment.Visible = false;
        }


    }
    protected void LnkAllTsknewComment_Click(object sender, EventArgs e)
    {
        Session["filterunread"] = "AllTaskNewComment";
        ////Session["filter"] = "Immediate";
        Response.Redirect("tasks.aspx");
    }
    protected void LnkMakeAllTaskCmmntRead_Click(object sender, EventArgs e)
    {


        sql = @"select *  from task_comments where task_id in (select task_id  from tasks where tasks.task_id = task_comments.task_id) and task_comments.tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 ";
        /// sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {

                    string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                    sqlinsert += " values(" + ds.Tables[0].Rows[i]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                    int intResult = DatabaseHelper.executeNonQuery(sqlinsert);
                    string sqlinsert2 = "insert into read_task([task_id],[username]) ";
                    sqlinsert2 += " values(" + ds.Tables[0].Rows[i]["task_id"].ToString() + ",'" + Session["admin"].ToString() + "')";
                    int intResult2 = DatabaseHelper.executeNonQuery(sqlinsert2);
                    //////}
                }
                catch
                {
                }
            }


        }

        getAllTskNewComment();


    }
    protected void DrpProjects_SelectedIndexChanged(object sender, EventArgs e)
    {

        System.Web.UI.WebControls.DropDownList lstProject = (System.Web.UI.WebControls.DropDownList)sender;   
        string FrmCompany = "True";
        Response.Redirect("edit_project.aspx?id=" + lstProject.SelectedValue + "&Comp=" + FrmCompany, false);

    }

    protected void DrpClients_SelectedIndexChanged(object sender, EventArgs e)
    {
        System.Web.UI.WebControls.DropDownList lstClient = (System.Web.UI.WebControls.DropDownList)sender;
          string FrmCompany = "True";
          Response.Redirect("edit_client.aspx?id=" + lstClient.SelectedValue + "&Comp=" + FrmCompany, false);

    }

    protected void lnkAll_Click(object sender, EventArgs e)
    {
        strfilter = "";
        bindData();

    }
    protected void lnkA_Click(object sender, EventArgs e)
    {
        string strchar ="A";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkB_Click(object sender, EventArgs e)
    {
        string strchar = "B";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkC_Click(object sender, EventArgs e)
    {
        string strchar = "C";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkD_Click(object sender, EventArgs e)
    {
        string strchar = "D";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkE_Click(object sender, EventArgs e)
    {
        string strchar = "E";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkF_Click(object sender, EventArgs e)
    {
        string strchar = "F";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkG_Click(object sender, EventArgs e)
    {
        string strchar = "G";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkH_Click(object sender, EventArgs e)
    {
        string strchar = "H";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkI_Click(object sender, EventArgs e)
    {
        string strchar = "I";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkJ_Click(object sender, EventArgs e)
    {
        string strchar = "J";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkK_Click(object sender, EventArgs e)
    {
        string strchar = "K";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkL_Click(object sender, EventArgs e)
    {
        string strchar = "L";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();

    }
    protected void lnkM_Click(object sender, EventArgs e)
    {
        string strchar = "M";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkN_Click(object sender, EventArgs e)
    {
        string strchar = "N";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkO_Click(object sender, EventArgs e)
    {
        string strchar = "O";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();

    }
    protected void lnkP_Click(object sender, EventArgs e)
    {
        string strchar = "P";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();

    }
    protected void lnkQ_Click(object sender, EventArgs e)
    {
        string strchar = "Q";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkR_Click(object sender, EventArgs e)
    {
        string strchar = "R";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();

    }
    protected void lnkS_Click(object sender, EventArgs e)
    {
        string strchar = "S";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();

    }
    protected void lnkT_Click(object sender, EventArgs e)
    {
        string strchar = "T";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();

    }
    protected void lnkU_Click(object sender, EventArgs e)
    {

        string strchar = "U";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkV_Click(object sender, EventArgs e)
    {
        string strchar = "V";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkW_Click(object sender, EventArgs e)
    {
        string strchar = "W";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkX_Click(object sender, EventArgs e)
    {
        string strchar = "X";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkY_Click(object sender, EventArgs e)
    {
        string strchar = "Y";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkZ_Click(object sender, EventArgs e)
    {
        string strchar = "Z";
        strfilter = "where Company_Name like '" + strchar + "%'";
        bindData();
    }
  
}
