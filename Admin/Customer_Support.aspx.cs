using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Admin_Customer_Support : System.Web.UI.Page
{
    int id;
    string sql = "";
    string task_action = "";
    string emailSubject = "";
    string shortDescription = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        btnAddCat.Attributes.Add("Onclick", "return checkCat();");
        btnUpdateCat.Attributes.Add("Onclick", "return chkcatEdit();");


        BtnDelMsg.Attributes.Add("onclick", "return confirm('Are you sure want to delete message " + DrpShrtDesc.SelectedItem + " ? ');");

        ////// if (Session["admin"] == null || Session["admin"].ToString() == "")
        ////// {
        //////     //Session["returnUrl"] = "quick_question.aspx";
        ////////     Response.Redirect("login.aspx");
        //////     return;
        ////// }

        divMessage1.InnerHtml = "";
        divMessage.InnerHtml = "";
        if (!Page.IsPostBack)
        {
            load_dropdowns();

            TRNewCat.Visible = false;
            //=============================
            BtnEditMsg.Visible = false;
            BtnDelMsg.Visible = false;
            //// BtnAddMsg.Visible = false;
            //=============================
            TREditCat.Visible = false;
            BtnDelcat.Visible = false;
            btnEditCat.Visible = false;


            //// load_ShortDescdrp();
            //======================================
            ////ftbComment.ReadOnly  = true;
            ////BtnEditMsg.CssClass = BtnEditMsg.CssClass.Replace("blueBtns", "");
            ////BtnEditMsg.CssClass = "BtnsNotHover";
            BtnEditMsg.CssClass = "blueBtns";

            BtnDelMsg.CssClass = BtnDelMsg.CssClass.Replace("blueBtns", "");
            BtnDelMsg.CssClass = "BtnsNotHover";

            ////BtnAddMsg.CssClass = BtnAddMsg.CssClass.Replace("blueBtns", "");
            ////BtnAddMsg.CssClass = "BtnsNotHover";
            BtnAddMsg.CssClass = "blueBtns";
            //======================================

        }

    }

    void load_ShortDescdrp()
    {
        // projects
        DrpShrtDesc.Items.Clear();
        sql = @"select *
		from Canned_message where CategoryId = '" + DrpMsgCat.SelectedValue + "' and Type = 'CR' order by shortDesc";


        // do a batch of sql statements
        DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);
        if (ds_dropdowns.Tables[0].Rows.Count > 0)
        {

            DrpShrtDesc.DataSource = ds_dropdowns.Tables[0];
            DrpShrtDesc.DataTextField = "shortDesc";
            DrpShrtDesc.DataValueField = "MessageId";
            DrpShrtDesc.DataBind();
            DrpShrtDesc.Items.Insert(0, new ListItem("[New message]", ""));

        }
        else
        {
            DrpShrtDesc.Items.Insert(0, new ListItem("[New message]", ""));

        }
        /// DrpShrtDesc.Items.Insert(0, new ListItem("[New message]", ""));
    }

    void load_dropdowns()
    {
        // projects
        sql = @"select *
		from Message_category where  Type = 'CR'
		 order by CategoryName;";


        // do a batch of sql statements
        DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);

        DrpMsgCat.DataSource = ds_dropdowns.Tables[0];
        DrpMsgCat.DataTextField = "CategoryName";
        DrpMsgCat.DataValueField = "CatId";
        DrpMsgCat.DataBind();
        DrpMsgCat.Items.Insert(0, new ListItem("[No category]", ""));
    }

    protected void BtnAddMsg_Click(object sender, EventArgs e)
    {
        sql += " insert into Canned_message([CategoryId],[shortDesc],[LongDesc],[Type]) ";
        
        //***********************************************************************************************
        //sql += " values('" + DrpMsgCat.SelectedValue.Replace("'", "''") + "','" + txtshrtDesc.Text.Replace("'", "''") + "','" + ftbComment.Text.Replace("'", "''") + "','CR')";
          sql += " values('" + DrpMsgCat.SelectedValue.Replace("'", "''") + "','" + txtshrtDesc.Text.Replace("'", "''") + "','" + ftbComment.Content.Replace("'", "''") + "','CR')";
        //***********************************************************************************************
        
        int intResult = DatabaseHelper.executeNonQuery(sql);

        if (intResult != 0)
        {
            load_ShortDescdrp();
            ///// DrpMsgCat.Text = txtshrtDesc.Text;
            //***********************************************************************************************
            //Session["Canned_msg"] = ftbComment.Text;
              Session["Canned_msg"] = ftbComment.Content;
            //***********************************************************************************************
            Page.ClientScript.RegisterStartupScript(typeof(Page), "test", "<script> updateParentWindow()</script>");
            //divMessage1.InnerHtml = "Canned message created successfully.";


        }
        else
        {
            divMessage1.InnerHtml = "Proccess failed,please try again.";
        }

    }
    protected void btnAddCat_Click(object sender, EventArgs e)
    {

        sql += " insert into Message_category([CategoryName],[Type]) ";
        sql += " values('" + txtCategory.Text.Trim().Replace("'", "''") + "','CR')";
        int intResult = DatabaseHelper.executeNonQuery(sql);

        if (intResult != 0)
        {
            load_dropdowns();
            txtCategory.Text = "";
            TRNewCat.Visible = false;
            Button1.Visible = true;
        }
        else
        {

            TRNewCat.Visible = true;
            lblcatmsg.Text = "Proccess failed , please try again.";

        }

    }
    protected void BtnDelcat_Click(object sender, EventArgs e)
    {
        sql += " Delete from Message_category where  CatId = '" + DrpMsgCat.SelectedValue + "' and Type = 'CR'";
        int intResult = DatabaseHelper.executeNonQuery(sql);
        sql = "";
        sql += "Delete from Canned_message where  CategoryId = '" + DrpMsgCat.SelectedValue + "' and Type = 'CR'";
        int intResult2 = DatabaseHelper.executeNonQuery(sql);
        if (intResult != 0)
        {
            load_dropdowns();
            load_ShortDescdrp();
            //***********************************************************************************************
             //ftbComment.Text = "";
            ftbComment.Content = "";
            //***********************************************************************************************
            
            //=============================
            //// BtnAddMsg.Visible=false;
            BtnDelMsg.Visible = false;
            BtnEditMsg.Visible = false;
            //=============================
            

            txtshrtDesc.Text = "";
        }

    }
    protected void btncloseCat_Click(object sender, EventArgs e)
    {
        TRNewCat.Visible = false;
        Button1.Visible = true;
        if (DrpMsgCat.SelectedItem.Value != "")
        {
            btnEditCat.Visible = true;

            BtnDelcat.Visible = true;
        }
        lblcatmsg.Text = "";

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        TRNewCat.Visible = true;
        load_ShortDescdrp();

        //***********************************************************************************************
        //ftbComment.Text = "";
          ftbComment.Content = "";
        //***********************************************************************************************

        //============================
        //============================
        ////BtnAddMsg.Visible = false;
        ////BtnDelMsg.Visible = false;
        ////BtnEditMsg.Visible = false;
        if (DrpShrtDesc.SelectedItem.Value == "")
        {
            BtnAddMsg.Visible = true;
            BtnEditMsg.Visible = false;
            BtnDelMsg.Visible = false;
        }
        Button1.Visible = false;
        btnEditCat.Visible = false;
        BtnDelcat.Visible = false;
        //============================
        //============================
        txtshrtDesc.Text = "";

    }
    protected void DrpMsgCat_SelectedIndexChanged(object sender, EventArgs e)
    {
        load_ShortDescdrp();
        //***********************************************************************************************
        //ftbComment.Text = "";
          ftbComment.Content  = "";
        //***********************************************************************************************
        txtshrtDesc.Text = "";
        TREditCat.Visible = false;
        if (DrpMsgCat.SelectedValue.ToString() != "")
        {
            BtnDelcat.Visible = true;
            btnEditCat.Visible = true;
            //==========================
            //==========================
            BtnAddMsg.Visible = true;
            ////BtnDelMsg.Visible = true;
            ////BtnEditMsg.Visible = true;

            ////BtnAddMsg.Enabled = false;
            ////BtnDelMsg.Enabled = false;
            ////BtnEditMsg.Enabled = false;

            //==========================
            //==========================
        }
        else
        {
            btnEditCat.Visible = false;
            BtnDelcat.Visible = false;
            TREditCat.Visible = false;
            //==========================
            //==========================
            ////BtnAddMsg.Visible = false;
            ////BtnDelMsg.Visible = false;
            ////BtnEditMsg.Visible = false;

            ////BtnAddMsg.Enabled = false;
            ////BtnDelMsg.Enabled = false;
            ////BtnEditMsg.Enabled = false;
            //==========================
            //==========================

        }

    }
    protected void DrpShrtDesc_SelectedIndexChanged(object sender, EventArgs e)
    {
        BtnDelMsg.CssClass = "blueBtns";

        ////txtshrtDesc.Enabled = false;



        //show but make disable the insert button
        //=====================================
        //BtnEditMsg.Enabled = false;
        //BtnDelMsg.Enabled = false;
        //BtnAddMsg.Enable = false;
        //BtnAddMsg.Visible = true;
        //BtnDelMsg.Visible = true;
        //BtnEditMsg.Visible = true;
        //=====================================

        //show but make disable the insert and Delete button
        //=====================================
        if (TREditCat.Visible == true)
        {
            BtnEditMsg.Text = "Update";
            BtnEditMsg.Visible = true;
        }
        else
        {
            BtnEditMsg.Text = "Insert";
            BtnEditMsg.Visible = true;
        }

        ////BtnEditMsg.Enabled = true;
        //+++++++++++++++++++++++++++++++
        //BtnDelMsg.Enabled = false;
        //+++++++++++++++++++++++++++++
        ////BtnAddMsg.Enabled = false;
        BtnDelMsg.Visible = true;
        BtnAddMsg.Visible = false;
        //=====================================

        sql = @"select *
		from Canned_message WHERE MessageId = '" + DrpShrtDesc.SelectedValue + "' and Type = 'CR'";



        // do a batch of sql statements
        DataSet ds_Message = DatabaseHelper.getDataset(sql);


        if (ds_Message != null && ds_Message.Tables.Count > 0 && ds_Message.Tables[0].Rows.Count > 0)
        {
            //***********************************************************************************************
            //ftbComment.Text = ds_Message.Tables[0].Rows[0]["LongDesc"].ToString();
            ftbComment.Content = ds_Message.Tables[0].Rows[0]["LongDesc"].ToString();
            //***********************************************************************************************

            txtshrtDesc.Text = ds_Message.Tables[0].Rows[0]["shortDesc"].ToString();
            //// DrpMsgCat.SelectedValue.ToString() ==  ds_Message.Tables[0].Rows[0]["CategoryId"].ToString();

            //======================================================================
            //======================================================================
            Session["Canned"] = ds_Message.Tables[0].Rows[0]["LongDesc"].ToString();
            //======================================================================
            //======================================================================
            //chkdisable.Visible = true;
        }
        else
        {
            //***********************************************************************************************
            //ftbComment.Text = "";
              ftbComment.Content = "";
            //***********************************************************************************************
            BtnEditMsg.Visible = false;
            BtnDelMsg.Visible = false;
            BtnAddMsg.Visible = true;
            //=============================
            //=============================
            //BtnAddMsg.Visible = true;
            //=============================
            //=============================
            /// txtshrtDesc.Enabled = true;
            txtshrtDesc.Text = "";
            //chkdisable.Visible = false;
        }

    }

    protected void BtnEditMsg_Click(object sender, EventArgs e)
    {
        try
        {
            if (BtnEditMsg.Text == "Update")
            {
                //***********************************************************************************************
                //sql += " update Canned_message set [CategoryId] = '" + DrpMsgCat.SelectedValue.Replace("'", "''") + "',[shortDesc] = '" + txtshrtDesc.Text.Replace("'", "''") + "',[LongDesc] = '" + ftbComment.Text.Replace("'", "''") + "' where MessageId ='" + DrpShrtDesc.SelectedValue + "' and Type = 'CR'";
                sql += " update Canned_message set [CategoryId] = '" + DrpMsgCat.SelectedValue.Replace("'", "''") + "',[shortDesc] = '" + txtshrtDesc.Text.Replace("'", "''") + "',[LongDesc] = '" + ftbComment.Content.Replace("'", "''") + "' where MessageId ='" + DrpShrtDesc.SelectedValue + "' and Type = 'CR'";
                
                //***********************************************************************************************
                int intResult = DatabaseHelper.executeNonQuery(sql);

                if (intResult != 0)
                {

                    load_ShortDescdrp();
                    //Session["Canned_msg"] = ftbComment.Text;
                    //Session["CannedShortDesc_msg"] = txtshrtDesc.Text;
                    //Page.ClientScript.RegisterStartupScript(typeof(Page), "test", "<script> updateParentWindow()</script>"); 

                }
                else
                {
                    divMessage1.InnerHtml = "Proccess failed,please try again.";
                }
            }
            else
            {
                //***********************************************************************************************
                //Session["Canned_msg"] = ftbComment.Text;
                Session["Canned_msg"] = ftbComment.Content;
                //***********************************************************************************************
                Session["CannedShortDesc_msg"] = txtshrtDesc.Text;
                Page.ClientScript.RegisterStartupScript(typeof(Page), "test", "<script> updateParentWindow()</script>");

            }

        }
        catch { }

    }
    protected void BtnDelMsg_Click(object sender, EventArgs e)
    {
        try
        {
            sql += "delete from Canned_message where [CategoryId] = '" + DrpMsgCat.SelectedValue.Replace("'", "''") + "'and [MessageId] = '" + DrpShrtDesc.SelectedValue + "' and Type = 'CR'";

            int intResult = DatabaseHelper.executeNonQuery(sql);

            if (intResult != 0)
            {
                //***********************************************************************************************
                //ftbComment.Text = "";
                ftbComment.Content  = "";
                //***********************************************************************************************
                txtshrtDesc.Text = "";
                load_ShortDescdrp();
                divMessage1.InnerHtml = "Message deleted successfully.";


            }
            else
            {
                divMessage1.InnerHtml = "Proccess failed,please try again.";
            }
        }
        catch { }
    }
    protected void btnEditCat_Click(object sender, EventArgs e)
    {

        TRNewCat.Visible = false;
        TREditCat.Visible = true;

        load_ShortDescdrp();
        txteditCat.Text = DrpMsgCat.SelectedItem.ToString();
        //***********************************************************************************************
        //ftbComment.Text = "";
         ftbComment.Content  = "";
        //***********************************************************************************************
        //==========================
        //==========================
        ////BtnAddMsg.Visible = false;
        ////BtnDelMsg.Visible = false;
        ////BtnEditMsg.Visible = false;
        if (DrpShrtDesc.SelectedItem.Value == "")
        {
            BtnAddMsg.Visible = true;
            BtnEditMsg.Visible = false;
            BtnDelMsg.Visible = false;
        }
        btnEditCat.Visible = false;
        Button1.Visible = false;
        BtnDelcat.Visible = false;
        //==========================
        //==========================

        txtshrtDesc.Text = "";


    }
    protected void btnCnclUpdate_Click(object sender, EventArgs e)
    {
        TREditCat.Visible = false;
        btnEditCat.Visible = true;
        Button1.Visible = true;
        BtnDelcat.Visible = true;
        BtnEditMsg.Text = "Insert";
    }
    protected void btnUpdateCat_Click(object sender, EventArgs e)
    {
        try
        {
            sql += "Update Message_category set CategoryName = '" + txteditCat.Text + "' where  CatId = " + DrpMsgCat.SelectedValue + " and Type = 'CR'";


            int intResult = DatabaseHelper.executeNonQuery(sql);
            if (intResult != 0)
            {
                load_dropdowns();
                txteditCat.Text = "";
                TREditCat.Visible = false;
                btnEditCat.Visible = true;
                Button1.Visible = true;
                BtnDelcat.Visible = true;
            }
            else
            {

                TREditCat.Visible = true;
                lbleditmessase.Text = "Proccess failed , please try again.";
            }
        }
        catch { }

    }
    //protected void BtnDismsg_Click(object sender, EventArgs e)
    //{
    //    DrpMsgCat.Enabled = true;
    //    Button1.Enabled = true; 
    //    DrpShrtDesc.Enabled = true;
    //    txtshrtDesc.Enabled = true;
    //    ftbComment.ReadOnly = false;
    //    BtnAddMsg.Enabled = true; 
    // }
    //protected void chkdisable_CheckedChanged(object sender, EventArgs e)
    //{
    //    if (chkdisable.Checked)
    //    {
    //        //***********************************************************************************************
    //        //ftbComment.ReadOnly = false;
    //        ftbComment.Enabled  = true ;
    //        //***********************************************************************************************
    //        //if (Session["Canned"] != null && Session["Canned"] != "")
    //        //{
    //        //    ftbComment.Text = Session["Canned"].ToString();
    //        //}
    //        BtnEditMsg.Enabled = true;
    //        BtnDelMsg.Enabled = true;
    //        BtnAddMsg.Enabled = true;
    //        //=======Apply CSS with Hover===
    //        BtnEditMsg.CssClass = BtnEditMsg.CssClass.Replace("BtnsNotHover", "");
    //        BtnDelMsg.CssClass = BtnDelMsg.CssClass.Replace("BtnsNotHover", "");
    //        BtnAddMsg.CssClass = BtnAddMsg.CssClass.Replace("blueBtns", "");
    //        BtnEditMsg.CssClass = "blueBtns";
    //        BtnDelMsg.CssClass = "blueBtns";
    //        BtnAddMsg.CssClass = "BtnsNotHover";
    //        //==============================
    //    }
    //    else
    //    {
    //        //***********************************************************************************************
    //        //ftbComment.ReadOnly = false;
    //          ftbComment.Enabled = true;
    //        //***********************************************************************************************
    //        ////if (Session["Canned"] != null && Session["Canned"] != "")
    //        ////{
    //        ////    ftbComment.Text = Session["Canned"].ToString();
    //        ////}
    //        BtnEditMsg.Enabled = true;
    //        BtnDelMsg.Enabled = false;
    //        BtnAddMsg.Enabled = true;

    //        ////BtnEditMsg.CssClass = BtnEditMsg.CssClass.Replace("blueBtns", "");
    //        BtnDelMsg.CssClass = BtnDelMsg.CssClass.Replace("blueBtns", "");
    //        ////BtnAddMsg.CssClass = BtnAddMsg.CssClass.Replace("blueBtns", "");
    //        BtnEditMsg.CssClass = "blueBtns";
    //        BtnDelMsg.CssClass = "BtnsNotHover";
    //        BtnAddMsg.CssClass = "blueBtns";
    //    }
    //}
}
