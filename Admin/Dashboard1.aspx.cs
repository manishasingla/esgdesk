﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Admin_Dashboard : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        EmpImg.Attributes.Add("onload", "imageLoaded(this);");

        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Response.Redirect("login.aspx");
            return;
        }
        //  To_Dogrid.i chkcompleted.a.Attributes.Add("onclick", "click123();");
        if (!IsPostBack)
        {
            getPR1cTasks();
            //getcomments();
            getTo_Do();
            getTo_Docancel();
            // getallnews();
            EmpImg.Src = DatabaseHelper.fnSetImgURL(Session["admin"].ToString());
        
        }
       
    }
    void getTo_Do()
    {

        string sql = @"select * from to_do where username='" + Session["admin"].ToString() + "' and Active=1 order by post_date desc";

        DataSet ds = DatabaseHelper.getallstatus(sql);
        To_Dogrid.DataSource = ds;
        To_Dogrid.DataBind();

    }
    void getTo_Docancel()
    {

        string sql = @"select * from to_do where username='" + Session["admin"].ToString() + "' and Active=0 order by post_date desc";

        DataSet ds = DatabaseHelper.getallstatus(sql);
        ToDogridcel.DataSource = ds;
        ToDogridcel.DataBind();

    }
    void getcomments()
    {
        //        string sql = @"select top 10 tasks.task_id,tasks.short_desc from task_comments,tasks 
        //        where tasks.task_id = task_comments.task_id and tasks.deleted <> 1 and tasks.status <> 'closed'
        //        and task_comments.deleted <> 1 and tasks.assigned_to_user = 'M' order by  task_comments.post_date desc ";

        ////        string sql = @"select top 10 tasks.task_id,tasks.short_desc,last_updated_date from tasks where   tasks.deleted <> 1 and tasks.status <> 'closed'
        ////   and tasks.last_updated_user= '" + Session["admin"].ToString() + "' order by  tasks.last_updated_date  desc";
        //and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' and tasks.last_updated_user = '" + Session["admin"].ToString() + "' and tasks.last_updated_user= '" + Session["admin"].ToString() + "' order by  tasks.last_updated_date  desc";

        //        string sql = @"select top 20 tasks.task_id,tasks.short_desc,task_comments.post_date,tasks.status from tasks join  task_comments
        //  on tasks.task_id = task_comments.task_id
        //where tasks.task_id = task_comments.task_id and tasks.deleted <> 1  
        //and task_comments.deleted <> 1 and task_comments.username = '" + Session["admin"].ToString() + "' order by  task_comments.post_date desc ";

        //        string sql = @"SELECT * FROM (select  top 20 
        //tasks.task_id  ,tasks.short_desc,max(task_comments.post_date)  as post_date ,tasks.status 
        //from tasks join  task_comments
        //on tasks.task_id = task_comments.task_id
        //where tasks.task_id = task_comments.task_id and tasks.deleted <> 1  
        //and task_comments.deleted <> 1 and task_comments.username = '" + Session["admin"].ToString() + "' group by tasks.task_id,tasks.short_desc,tasks.status order by post_date desc  )as c, task_comments where task_comments.post_date=c.post_date order by task_comments.post_date desc";

        string sql = @"SELECT * FROM (select  top 20 
tasks.task_id  ,tasks.short_desc,max(task_comments.post_date)  as post_date ,tasks.status 
from tasks join  task_comments
on tasks.task_id = task_comments.task_id
where tasks.task_id = task_comments.task_id and tasks.deleted <> 1  
and task_comments.deleted <> 1 and task_comments.username = '" + Session["admin"].ToString() + "' " +
"group by tasks.task_id,tasks.short_desc,tasks.status order by post_date desc  )as c, task_comments " +
"where task_comments.post_date=c.post_date and task_comments.task_id =c.task_id ";

        DataSet dsTaskDetails1 = DatabaseHelper.getallstatus(sql);
        DataGrid2.DataSource = dsTaskDetails1;
        DataGrid2.DataBind();


    }
    void getPR1cTasks()
    {

        string sql = @"select COUNT(*) as TotCount,tasks.status from tasks 
where tasks.deleted <> 1 and tasks.status in('New','in progress','ongoing','re-opened','to check','awaiting client response'  ) 
and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + Session["admin"].ToString() + "' group by tasks.status  ";

        //if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        //{
        //    sql += " and  status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + Session["admin"] + "' ";
        //}
        //else
        //{
        // sql += " and  status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + Session["admin"] + "' ";
        //}

        DataSet ds = DatabaseHelper.getallstatus(sql);
        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    switch (ds.Tables[0].Rows[i]["status"].ToString().ToLower())
                    {
                        case "new":
                            countNew.InnerHtml = ds.Tables[0].Rows[i]["TotCount"].ToString();
                            break;
                        case "in progress":
                            CountInprogress.InnerHtml = ds.Tables[0].Rows[i]["TotCount"].ToString();
                            break;
                        case "ongoing":
                            CountOngoing.InnerHtml = ds.Tables[0].Rows[i]["TotCount"].ToString();
                            break;
                        case "re-opened":
                            CountReopened.InnerHtml = ds.Tables[0].Rows[i]["TotCount"].ToString();
                            break;
                        case "to check":
                            CountTocheck.InnerHtml = ds.Tables[0].Rows[i]["TotCount"].ToString();
                            break;
                        case "awaiting client response":
                            CountACR.InnerHtml = ds.Tables[0].Rows[i]["TotCount"].ToString();

                            break;
                        default:

                            break;
                    }
                    //if (ds.Tables[0].Rows[i]["status"] == "New")
                    //{
                    //    countNew.InnerHtml = ds.Tables[0].Rows.Count.ToString();
                    //}
                    //else
                    //{
                    //    countNew.InnerHtml = "0";
                    //}

                    //if (ds.Tables[0].Rows[i]["status"] == "in progress")
                    //{

                    //    CountInprogress.InnerHtml = ds.Tables[1].Rows.Count.ToString();
                    //}
                    //else
                    //{
                    //    CountInprogress.InnerHtml = "0";
                    //}
                    //if (ds.Tables[0].Rows[i]["status"] == "ongoing")
                    //{
                    //    CountOngoing.InnerHtml = ds.Tables[2].Rows.Count.ToString();

                    //}
                    //else
                    //{
                    //    CountOngoing.InnerHtml = "0";
                    //}
                    //if (ds.Tables[0].Rows[i]["status"] == "re-opened")
                    //{
                    //    CountReopened.InnerHtml = ds.Tables[3].Rows.Count.ToString();

                    //}
                    //else
                    //{
                    //    CountReopened.InnerHtml = "0";

                    //}
                    //if (ds.Tables[0].Rows[i]["status"] == "to check")
                    //{
                    //    CountTocheck.InnerHtml = ds.Tables[4].Rows.Count.ToString();

                    //}
                    //else
                    //{
                    //    CountTocheck.InnerHtml = "0";
                    //}
                    //if (ds.Tables[0].Rows[i]["status"] == "awaiting client response")
                    //{
                    //    CountACR.InnerHtml = ds.Tables[5].Rows.Count.ToString();

                    //}
                    //else
                    //{
                    //    CountACR.InnerHtml = "0";
                    //}
                }
                ////string strtot = countNew.InnerHtml + "," + CountInprogress.InnerHtml.ToString() + "," + CountOngoing.InnerHtml.ToString() + "," + CountReopened.InnerHtml.ToString() + "," + CountTocheck.InnerHtml.ToString() + "," + CountACR.InnerHtml.ToString();
                ////string[] arr = strtot.Split(',');
                ////int total = 0;
                ////for (int i = 0; i < arr.Length; i++)
                ////{
                ////    if (arr[i].ToString() != "0")
                ////    {
                ////        total = total + int.Parse(arr[i].ToString());
                ////    }
                ////}
                ////CountAll.InnerHtml = total.ToString();
                string sql1 = @"select  * from tasks where tasks.deleted <> 1 and status <> 'closed' and  status <> 'checked' and
 status <>'parked' and assigned_to_user = '" + Session["admin"].ToString() + "'  ";

                DataSet ds1 = DatabaseHelper.getallstatus(sql1);
                if (ds1 != null)
                {
                    if (ds1.Tables[0].Rows.Count > 0)
                    {
                        CountAll.InnerHtml = ds1.Tables[0].Rows.Count.ToString();
                    }
                }
            }
            else
            {
                countNew.InnerHtml = "0";
                CountInprogress.InnerHtml = "0";
                CountOngoing.InnerHtml = "0";
                CountReopened.InnerHtml = "0";
                CountTocheck.InnerHtml = "0";
                CountACR.InnerHtml = "0";


            }
        }
    }

    protected void DataGrid2_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemIndex != -1)
        {
            Label lblPosted = (Label)e.Item.FindControl("lblPosted");
            Label lblComment = (Label)e.Item.FindControl("lblComment");






            //lblPosted.Text = "comment " + e.Item.Cells[1].Text + " posted by " + e.Item.Cells[2].Text + " on " + DateTime.Parse(e.Item.Cells[3].Text).ToString("dd MMM h:mm tt");
            //lblPosted.Text = "[ " + DateTime.Parse(e.Item.Cells[0].Text).ToString("dd MMM yyyy h:mm tt") + " ]";
            lblPosted.Text = DateTime.Parse(e.Item.Cells[0].Text).ToString("dd MMM yyyy hh:mm tt");
            if (e.Item.Cells[1].Text == "closed")
            {
                lblComment.Style.Add(HtmlTextWriterStyle.Color, "red");
            }

        }
    }

    protected void btnNewclick_Click(object sender, EventArgs e)
    {

        Session["filter"] = "New";
        Session["boolclose"] = "false";
        Response.Redirect("tasks.aspx");
    }
    protected void btnProcessclick_Click(object sender, EventArgs e)
    {

        Session["filter"] = "in progress";
        Session["boolclose"] = "false";
        Response.Redirect("tasks.aspx");
    }
    protected void btnOngingclick_Click(object sender, EventArgs e)
    {

        Session["filter"] = "ongoing";
        Session["boolclose"] = "false";
        Response.Redirect("tasks.aspx");
    }
    protected void btnReopenclick_Click(object sender, EventArgs e)
    {

        Session["filter"] = "re-opened";
        Session["boolclose"] = "false";
        Response.Redirect("tasks.aspx");
    }
    protected void btnTocheckclick_Click(object sender, EventArgs e)
    {

        Session["filter"] = "to check";
        Session["boolclose"] = "false";
        Response.Redirect("tasks.aspx");
    }
    protected void btnACRclick_Click(object sender, EventArgs e)
    {

        Session["filter"] = "awaiting client response";
        Session["boolclose"] = "false";
        Response.Redirect("tasks.aspx");
    }
    protected void btnAllclick_Click(object sender, EventArgs e)
    {

        Session["filter"] = "0";
        Session["boolclose"] = "false";
        Response.Redirect("tasks.aspx");
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {

        string sql = @"insert into to_do(username,post_date,comments,Active)values('" + Session["admin"].ToString() + "',getdate(),'" + txtrToDo.Text.Replace("'", "|@").Trim() + "',1)";
        int result = DatabaseHelper.executeSQLquery(sql);
        if (result != 0)
        {
            getTo_Do();
            clear();
        }

    }
    void clear()
    {
        txtrToDo.Text = string.Empty;
    }
    protected void To_Dogrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "delete")
        {
            string sql = " Delete from to_do where id=" + e.Item.Cells[0].Text;

            int intResult = DatabaseHelper.executeSQLquery(sql);

            if (intResult != 0)
            {
                getTo_Do();
                getTo_Docancel();
            }
        }


    }

    public void chkcompleted_OnCheckedChanged(object sender, EventArgs e)
    {
        CheckBox chk = (CheckBox)sender;
        string sql = " update to_do set Active=0 where id=" + chk.Text;

        int intResult = DatabaseHelper.executeSQLquery(sql);

        if (intResult != 0)
        {
            getTo_Do();
            getTo_Docancel();
        }

        //foreach (GridViewRow gvr in To_Dogrid.Items)
        //{
        //    if (((CheckBox)gvr.FindControl("CheckBox1")).Checked == true)
        //    {

        //        int uPrimaryid = gvr.Cells("uPrimaryID");
        //    }
        //}
        //CheckBox chkStatus = (CheckBox)sender;
        //GridViewRow gvr = (GridViewRow)chkStatus.NamingContainer;

        ////  CheckBox chkStatus = (CheckBox)sender;
        ////  ////GridViewRow row = (GridViewRow)chkStatus.NamingContainer;
        ////  //GridViewRow row = (GridViewRow)chkStatus.NamingContainer;
        ////  //string textValue = ((CheckBox)sender).Text;
        ////  //string yourKeyField = ((CheckBox)sender).Attributes["YourKeyField"];
        ////  //int rowNumber = Convert.ToInt32(((CheckBox)sender).Attributes["RowNumber"]);

        ////  CheckBox chkBx = ((CheckBox)To_Dogrid.Items[1].Cells[0].FindControl(chkStatus.ClientID.ToString()));
        ////  //CheckBox chkBx = (CheckBox)To_Dogrid.FindControl(chkStatus.ClientID.ToString());
        ////  GridViewRow gvr = (GridViewRow)chkStatus.Parent.Parent;   
        ////  GridViewRow gridRow = (sender as CheckBox).NamingContainer as GridViewRow; 
        //////  CheckBox chkBx = (CheckBox)chkStatus.Parent.FindControl(chkStatus.ClientID.ToString());

    }
    public void chkuncompleted_OnCheckedChanged(object sender, EventArgs e)
    {
        CheckBox chk = (CheckBox)sender;
        string sql = " update to_do set Active=1 where id=" + chk.Text;

        int intResult = DatabaseHelper.executeSQLquery(sql);

        if (intResult != 0)
        {
            getTo_Do();
            getTo_Docancel();
        }

    }

    private void getallnews()
    {
        try
        {

            string sql = @"select * from Newstbl where visible=1 and EmporCR='Task' order by addeddate desc";


            DataSet ds = DatabaseHelper.getallstatus(sql);



            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                NewsGrid.DataSource = ds.Tables[0];

                NewsGrid.DataBind();
                //lblmamarquee.InnerHtml = "<Marquee scrollamount=\"3\" onmouseover=\"this.stop();\" onmouseout=\"this.start();\" ><font-size:8px;><span style=\"color:#003C7F;font-size: small\">Todays news.....</span>" + DateTime.Now.ToString("dd/MM/yyyy") + "..... ";
                //// lblmamarquee.InnerHtml = "<Marquee scrollamount=\"3\" onmouseover=\"this.stop();\" onmouseout=\"this.start();\" ><font-size:8px;><span style=\"color:#003C7F;font-size: small\">Todays news.....</span> ";


                ////for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                ////{


                ////    //lblmamarquee.Text += "<a class=changeText style=\"font-family:trebuchet MS; text-decoration:underline\" href='" + System.Configuration.ConfigurationManager.AppSettings["WebAdd4Rewrite"] + "real_estate-news/costa-del-sol/" + ds.Tables[0].Rows[i]["ArticleName"].ToString() + "'>" + ds.Tables[0].Rows[i]["Newsheding"].ToString().Replace("|@", "'") + "</a>" + "....." + DateTime.Now.ToString("dd/MM/yyyy") + ".....";
                ////    ////lblmamarquee.InnerHtml += "<a   style=\"font-family:trebuchet MS; text-decoration:underline;font-size: small;color:#003C7F\" href='" + System.Configuration.ConfigurationManager.AppSettings["WebAdd4Rewrite"] + "outsourcing-news/" + ds.Tables[0].Rows[i]["ArticleName"].ToString() + "'>" + ds.Tables[0].Rows[i]["Newsheding"].ToString().Replace("|@", "'") + "</a>" + "..... , ";
                ////    lblmamarquee.InnerHtml += "<a   class=\"newsticker\" href='newsdetails.aspx?id=" + ds.Tables[0].Rows[i]["id"].ToString() + "'>" + ds.Tables[0].Rows[i]["Heading"].ToString().Replace("|@", "'") + "</a>" + "..... , ";
                ////    //nwsLink1 = ds.Tables[0].Rows[i]["ArticleName"].ToString()
                ////    //nwshead1 = ds.Tables[0].Rows[0]["Newsheding"].ToString().Replace("|@", "'");

                ////}
                ////lblmamarquee.InnerHtml = lblmamarquee.InnerHtml.ToString().TrimEnd(new char[] { ',', ' ' });
                ////lblmamarquee.InnerHtml += "</font></marquee>";
                ////lblmamarquee.InnerHtml = lblmamarquee.InnerHtml.ToString().Replace("<br>", "");
            }
            else
            {
                //divNews1.InnerHtml = "";
                //NewsHead1 = "";
            }

        }
        catch (Exception ex)
        {


        }
    }

    //protected void NewsGrid_DataBound(object sender, DataGridItemEventArgs e)
    //{
    //    if (e.Item.ItemIndex != -1)
    //    {
    //        Label lblPosted = (Label)e.Item.FindControl("lblPosted");
    //        //  Label lblComment = (Label)e.Item.FindControl("lblComment");






    //        //lblPosted.Text = "comment " + e.Item.Cells[1].Text + " posted by " + e.Item.Cells[2].Text + " on " + DateTime.Parse(e.Item.Cells[3].Text).ToString("dd MMM h:mm tt");
    //        //lblPosted.Text = "[ " + DateTime.Parse(e.Item.Cells[0].Text).ToString("dd MMM ") + " ]";
    //        lblPosted.Text = DateTime.Parse(e.Item.Cells[0].Text).ToString("dd MMM ");
    //        //if (e.Item.Cells[1].Text == "closed")
    //        //{
    //        //    lblComment.Style.Add(HtmlTextWriterStyle.Color, "red");
    //        //}

    //    }

    //}



    protected void NewsGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        NewsGrid.PageIndex = e.NewPageIndex;
        getallnews();
    }
    protected void Timer1_Tick(object sender, EventArgs e)
    {
        //TabContainer1.Style.Remove(HtmlTextWriterStyle.Display);
        //TabContainer1.Style.Add(HtmlTextWriterStyle.Display, "block");
        getallnews();
        //getcomments();
        Timer1.Enabled = false;
    }
    protected void Timer2_Tick(object sender, EventArgs e)
    {
        getcomments();
        Timer2.Enabled = false;
    }

    protected void NewsGrid_DataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.DataItemIndex != -1)
        {

            Label lblPosted = (Label)e.Row.FindControl("lblPosted");
            lblPosted.Text = Convert.ToDateTime(lblPosted.Text).ToString("dd MMM ");
            //lblPosted.Text = DateTime.Parse(e.Row.Cells[0].Text).ToString("dd MMM ");


        }
    }
}