﻿<%@ WebHandler Language="C#" Class="Databasequeryhandler" %>

using System;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net.Mail;
using System.Text.RegularExpressions;

public class Databasequeryhandler : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        
         string strConn = ConfigurationManager.AppSettings["strConn"];
         string strConnECRM = ConfigurationManager.AppSettings["strConnECR"];
         string strExceptionEmail = ConfigurationManager.AppSettings["ExceptionEmailId"];
         DataSet ds = new DataSet();      
        
        
        
        string strsql = " ALTER procedure [dbo].[NewuspGetTaskDetails_test] ";
        strsql += " ( @username varchar(100), @filter varchar(Max), ";
        strsql += " @nofoRecord varchar(100) ) as begin set nocount on; ";
        strsql += " Declare @query varchar(max); ";
        strsql += " select @query='select post_date,task_id,Deleted into #tmpreadDate ";
        strsql += " from task_comments;' + @nofoRecord + ' t.task_id as task_id, t.RequestId as RequestId, ";
        strsql += " case when t.short_desc='''' then '''' when t.short_desc=null then '''' else '''' + t.short_desc + '''' end as Short_desc, t.reported_user as reported_user, t.reported_date as reported_date, t.status as status, t.priority as priority, t.category as category,";
        strsql += " t.project as project, t.project_type as project_type, t.Relevant_URL as Relevant_URL, ";
        strsql += " t.assigned_to_user as assigned_to_user, t.last_updated_user as last_updated_user, ";
        strsql += " t.last_updated_date as last_updated_date, t.deleted as deleted, t.ETC as ETC, ";
        strsql += " t.Company as Company, t.Internalref as Internalref, t.StartDate as StartDate, ";
        strsql += " t.DueDate As DueDate, t.Isprivate as Isprivate, t.OrderId, tc.comment as comment,";
        strsql += " tc.deleted as deleted, tc.Ip as Ip, tc.Macid as Macid, tc.post_date as post_date, ";
        strsql += " tc.qflag as qflag, tc.QuesTo as QuesTo, tc.tc_id as tc_id, tc.username as username,";
        strsql += " case when qflag=''1'' then '''' + ''Question posted by '' + tc.username + '' for '' + tc.QuesTo + '' ";
        strsql += " on '' + CONVERT(VARCHAR(20), tc.post_date, 100) + '''' + tc.comment + '' ";
        strsql += " '' else '''' + ''Comment '' + Convert(varchar,tc.tc_id) + '' ";
        strsql += " posted by '' + tc.username + '' on '' + CONVERT(VARCHAR(20), tc.post_date, 100) + ''";
        strsql += " '' + tc.comment + ''";
        strsql += " '' end as ShowAllComment from tasks t inner join task_comments tc ";
        strsql += " on t.task_id = tc.task_id  ";
        strsql += " and tc.post_date=(select top 1 post_date from #tmpreadDate T1 ";
        strsql += " where tc.task_id = T1.task_id and T1.deleted<>1 ";
        strsql += " order by post_date desc) ";
        strsql += " order by Orderid ; ";
        strsql += " drop table #tmpreadDate;'; ";
        strsql += " execute(@query);  ";
        strsql += " end ";
        
        
        
         SqlConnection Conn = new SqlConnection(strConn);
         SqlCommand sqlCmd = new SqlCommand();
         Conn.Open();
         sqlCmd.Connection = Conn;
         sqlCmd.CommandType = CommandType.Text;
         sqlCmd.CommandText = strsql;
         sqlCmd.ExecuteNonQuery();
         context.Response.Write(strsql);
        
        


                //SqlConnection Conn = new SqlConnection(strConn);
                //SqlCommand sqlCmd = new SqlCommand();
                //sqlCmd.Connection = Conn;
                //sqlCmd.CommandType = CommandType.StoredProcedure;
                //sqlCmd.CommandText = "sp_helptext";

                //SqlParameter sqlParameter1 = new SqlParameter("@objname", SqlDbType.VarChar, Int32.MaxValue);
                //sqlParameter1.Value = "NewuspGetTaskDetails";
                //sqlCmd.Parameters.Add(sqlParameter1);

                //SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                //da.Fill(ds);
                //if (ds.Tables.Count > 0)
                //{
                //    for (int i = 0; i < ds.Tables[0].Rows.Count - 1; i++)
                //    {
                //        context.Response.Write(ds.Tables[0].Rows[i][0].ToString() + "\n");
                //    }
                //}
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}