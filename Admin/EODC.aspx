﻿<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="true" CodeFile="EODC.aspx.cs" Inherits="EODC" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%--<%@ Register Assembly="AjaxControls" Namespace="AjaxControls" TagPrefix="asp" %>--%>
<script type="text/javascript" src="js/TypeAjax.js"></script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Eodc form</title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="JavaScript" src="../js/bug_list.js"></script>

    <script type="text/javascript" language="javascript" src="../js/calendarDateInput.js"></script>

    <script type="text/javascript" language="javascript">
        function Enablebutton() {
            var txtupdate = document.getElementById("txtExpainUpdate").value;
            var txtnexttsk = document.getElementById("txtExpainNextTask").value;
            var drpvalue = document.getElementById("drpTaskUpdate").value;
            var drpNextvalue = document.getElementById("drpNextdayTask").value;
            if (drpvalue == "No" && drpNextvalue == "No") {
                if (txtupdate != "" && txtnexttsk != "") {
                    document.getElementById("Button1").disabled = "";
                }
                else {
                    document.getElementById("Button1").disabled = "disabled";
                }
            }
            else if (drpvalue == "No") {
                if (txtupdate != "") {
                    document.getElementById("Button1").disabled = "";
                }
                else {
                    document.getElementById("Button1").disabled = "disabled";
                }
            }
            else if (drpNextvalue == "No") {
                if (txtnexttsk != "") {
                    document.getElementById("Button1").disabled = "";
                }
                else {
                    document.getElementById("Button1").disabled = "disabled";
                }
            }
            else if (drpvalue == "Yes" && drpNextvalue == "Yes") {
                document.getElementById("Button1").disabled = "";
            }
            else {
                document.getElementById("Button1").disabled = "";
            }
        }

    </script>

    <script type="text/javascript" language="javascript">
        function checkupdate() {

            if (document.getElementById("drpTaskUpdate").value == "No" && (document.getElementById("txtExpainUpdate").value == "")) {
                alert("Please explain/give details");
                return false;
            }
            else if (document.getElementById("drpNextdayTask").value == "No" && (document.getElementById("txtExpainNextTask").value == "")) {
                alert("Please explain/give details");
                return false;
            }
            else {
                document.getElementById("Button1").click();
                return true;
            }
        }
    </script>

    <script type="text/javascript" language="javascript">
        function ShowDetail() {
            var drpvalue = document.getElementById("drpTaskUpdate").value;
            var txtupdate = document.getElementById("txtExpainUpdate").value;
            var txtnexttsk = document.getElementById("txtExpainNextTask").value;

            if (drpvalue == "No") {
                document.getElementById("TrUpdate").style.display = "Block";
                if (txtupdate != "" && txtnexttsk != "") {
                    document.getElementById("Button1").disabled = "";
                }
                else {
                    document.getElementById("Button1").disabled = "disabled";
                }
            }
            else {
                document.getElementById("TrUpdate").style.display = "None";
                document.getElementById("Button1").disabled = "";
            }
        }

        function ShowNextTaskDetail() {
            var drpNextvalue = document.getElementById("drpNextdayTask").value;

            var txtnexttsk = document.getElementById("txtExpainNextTask").value;
            var txtupdate = document.getElementById("txtExpainUpdate").value;

            if (drpNextvalue == "No") {
                document.getElementById("TrNextTask").style.display = "Block";
                if (txtupdate != "" && txtnexttsk != "") {
                    document.getElementById("Button1").disabled = "";
                }
                else {
                    document.getElementById("Button1").disabled = "disabled";
                }
            }
            else {
                document.getElementById("TrNextTask").style.display = "None";
                document.getElementById("Button1").disabled = "";
            }
        }

    </script>

    <script type="text/javascript" language="javascript">
        function closeDiv() {
            document.getElementById('DivTask0').style.display = "block";
            document.getElementById('DivTask1').style.display = "None";
            document.getElementById('DivTask2').style.display = "None";
            document.getElementById('DivTask3').style.display = "None";
            document.getElementById('DivTask4').style.display = "None";
            document.getElementById('DivTask5').style.display = "None";
            document.getElementById('DivTask6').style.display = "None";
            document.getElementById('DivTask7').style.display = "None";
            document.getElementById('DivTask8').style.display = "None";
            document.getElementById('DivTask9').style.display = "None";
        }
    </script>

    <script type="text/javascript" language="javascript">
        var timerID;
        function fncref() {
            if (timerID != 0) {
                timerID = setInterval("showmsg()", 500);
                
            }
        }

        function showmsg() {
            var currentTime = new Date();
            var months_long = new Array('January', 'February', 'March', 'April',
		   'May', 'June', 'July', 'August', 'September', 'October', 'November',
		   'December')
            var days_long = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday',
		   'Thursday', 'Friday', 'Saturday')
            var day = currentTime.getDay();
            day = days_long[day];
            var month = currentTime.getMonth();
            month = months_long[month];
            var date = currentTime.getDate();
            var year = currentTime.getFullYear();
            //year=year.substr(2,4);
            var time = currentTime.getTime();
            var minutes = currentTime.getMinutes();
            var hours = currentTime.getHours();
            var hour;
            var time = "";
            if (minutes < 10)
                minutes = "0" + minutes
            if (hours > 12)
                hour = parseInt(hours) - 12;
            else
                hour = parseInt(hours);
            time = (hour + ":" + minutes + " ")

            if (hours > 12) {
                hours = parseInt(hours) - 12;
                time = time + ("PM");
            } else {
                time = time + ("AM");
            }
            cuttent = time; ////+"   |  "+day+" "+date+" "+month+" "+year;
            ///document.getElementById("settimenow").innerHTML=cuttent;
            document.getElementById("txttime").value = "Time: " + cuttent;
        }
    </script>

    <script type="text/javascript" language="javascript">

        function ShowNoOfTask() {
            closeDiv();
            var Taskno = document.getElementById("drpNoOfTask").value;

            for (var i = 0; i < Taskno; i++) {
                document.getElementById("DivTask" + [i]).style.display = "Block";
            }
            return false;


        }
    </script>

</head>
<body onload="fncref(); closeDiv(); ShowDetail();ShowNextTaskDetail();ShowNoOfTask();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div>
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <div id="Mailcontent" runat="server">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="left" style="height: 232px" valign="top">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                        <tr>
                                            <td align="left">
                                                <div class="divBorder">
                                                    <table width="100%">
                                                        <tr>
                                                            <td width="55%">
                                                                <span class="yellowtext1" style="padding-left: 20px;">FHS Tasks</span>
                                                            </td>
                                                            <td align="right" valign="middle">
                                                                <table width="100%" align="right" cellpadding="3" cellspacing="0" style="height: 100%">
                                                                    <tr>
                                                                        <td align="right">
                                                                            <a id="lnkUserName" runat="server" href="#" target="_blank"></a>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right">
                                                                            <asp:TextBox ID="txttime" runat="server" ReadOnly="True" Font-Size="11px" Font-Names="Verdana"
                                                                                ForeColor="Green" BackColor="Transparent" BorderStyle="None"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <div id="pageTitle">
                                                    <div style="text-align: left">
                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td align="left" colspan="1" valign="bottom" class="whitetext2">
                                                                    No of task you did:&nbsp;
                                                                    <asp:DropDownList ID="drpNoOfTask" runat="server" Width="45px">
                                                                        <asp:ListItem Value="1">1</asp:ListItem>
                                                                        <asp:ListItem Value="2">2</asp:ListItem>
                                                                        <asp:ListItem Value="3">3</asp:ListItem>
                                                                        <asp:ListItem Value="4">4</asp:ListItem>
                                                                        <asp:ListItem Value="5">5</asp:ListItem>
                                                                        <asp:ListItem Value="6">6</asp:ListItem>
                                                                        <asp:ListItem Value="7">7</asp:ListItem>
                                                                        <asp:ListItem Value="8">8</asp:ListItem>
                                                                        <asp:ListItem Value="9">9</asp:ListItem>
                                                                        <asp:ListItem Value="10">10</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td width="30">
                                                                    &nbsp;
                                                                </td>
                                                                <td align="right" valign="bottom">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td align="left" valign="middle" class="whitetext2">
                                                                                Date?
                                                                            </td>
                                                                            <td width="220" align="right" valign="middle">

                                                                                <script>                                                                                    DateInput('callbackdate', true, '', 'DD-MON-YYYY')</script>

                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" height="28" valign="middle">
                                                <table width="750" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td align="left" valign="top">
                                                            
                                                               <asp:UpdateProgress ID="updateprogress3" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                                                <ProgressTemplate>
                                                                    <div id="processmessage" class="processMessage">
                                                                        <img alt="loading" src="images/ajax-loader.gif" style="width: 80px; height: 80px" />
                                                                        <span style="color: black; font-weight: bold; font-size: 18px; vertical-align: super;">
                                                                            Please wait... </span>
                                                                    </div>
                                                                </ProgressTemplate>
                                                            </asp:UpdateProgress>
                                                            <%--<asp:UpdatePanel ID="Up1" runat="server">
                                                                <ContentTemplate>--%>
                                                            <div id="DivTask0" runat="server" style="display: block; padding: 5px; margin: 5px;
                                                                border: solid 1px #cdcdcd; width: 750px; background: #f5f5f5;">
                                                                
                                                                
                                                                
                                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                                    <ContentTemplate>
                                                                
                                                                <table width="750" cellpadding="5" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" height="28" valign="middle" style="width: 197px">
                                                                            Hours spent:
                                                                        </td>
                                                                        <td align="left" valign="top" style="width: 563px">
                                                                            <asp:TextBox ID="txthorSpent0" runat="server" Width="40px"></asp:TextBox>Hours
                                                                            <asp:TextBox ID="txthrSpent0" runat="server" Width="40px"></asp:TextBox>
                                                                            Minutes<cc1:NumericUpDownExtender ID="NumericUpDownExtender2" runat="server" TargetControlID="txthrSpent0"
                                                                                RefValues="" Step="15" Width="80" ServiceDownMethod="" ServiceUpMethod="" TargetButtonDownID=""
                                                                                TargetButtonUpID="">
                                                                            </cc1:NumericUpDownExtender>
                                                                            <cc1:NumericUpDownExtender ID="NumericUpDownExtender12" runat="server" TargetControlID="txthorSpent0"
                                                                                RefValues="" Step="1" Width="80" ServiceDownMethod="" ServiceUpMethod="" TargetButtonDownID=""
                                                                                TargetButtonUpID="">
                                                                            </cc1:NumericUpDownExtender>
                                                                            &nbsp;&nbsp;&nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" valign="middle" style="width: 197px; height: 29px;">
                                                                            Project:
                                                                        </td>
                                                                        <td align="left" valign="middle" style="height: 29px; width: 563px;">
                                                                            <table border="0" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td style="width: 208px; height: 20px">
                                                                                    
                                                                                    
                                                                                        <%--<asp:DropDownList ID="drppro0" runat="server" Width="205px" OnSelectedIndexChanged="drppro0_SelectedIndexChanged"
                                                                                            AutoPostBack="True">
                                                                                            <asp:ListItem>Select</asp:ListItem>
                                                                                        </asp:DropDownList>--%>
                                                                                        
                                                                                       <asp:DropDownList ID="drppro0" runat="server" Width="205px" AutoPostBack="True">
                                                                                            <asp:ListItem>Select</asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                        
                                                                                        
                                                                                        
                                                                                    </td>
                                                                                    <td style="width: 10px">
                                                                                    </td>
                                                                                    <td>
                                                                                        Task ID:
                                                                                    </td>
                                                                                    <td style="height: 20px">
                                                                                        <%--<asp:DropDownList ID="drpTask0" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpTask0_SelectedIndexChanged"--%>
                                                                                        <asp:DropDownList ID="drpTask0" runat="server" AutoPostBack="True">
                                                                                         </asp:DropDownList>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" style="width: 197px" valign="middle">
                                                                            Description:
                                                                        </td>
                                                                        <td align="left" style="height: 28px; width: 563px;" valign="middle">
                                                                            <asp:TextBox ID="txtdesc0" runat="server" Width="556px" ForeColor="black" TextMode="MultiLine"
                                                                                Rows="2"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" style="width: 197px" valign="middle">
                                                                            Project url:
                                                                        </td>
                                                                        <td align="left" style="height: 28px; width: 563px;" valign="middle">
                                                                            <asp:TextBox ID="txtproUrl0" TextMode="MultiLine" Rows="3" runat="server" Width="556px"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" valign="middle" style="width: 197px;">
                                                                            Other comments:<br />
                                                                            <span style="font-size: 9px">(Any problems, questions, requests or suggestions)</span>
                                                                        </td>
                                                                        <td align="left" valign="middle" style="width: 556px;">
                                                                            <asp:TextBox ID="txtcomment0" runat="server" Width="554px" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                
                                                            </div>
                                                            <%--</ContentTemplate>
                                                                    </asp:UpdatePanel>--%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" valign="top">
                                                            <div id="DivTask1" runat="server" style="display: block; padding: 5px; margin: 5px;
                                                                border: solid 1px #cdcdcd; width: 750px; background: #f5f5f5;">
                                                                <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="UpdatePanel2">
                                                                    <ProgressTemplate>
                                                                        <div id="processmessage" class="processMessage">
                                                                            <img alt="loading" src="images/ajax-loader.gif" style="width: 80px; height: 80px" />
                                                                            <span style="color: black; font-weight: bold; font-size: 18px; vertical-align: super;">
                                                                                Please wait... </span>
                                                                        </div>
                                                                    </ProgressTemplate>
                                                                </asp:UpdateProgress>
                                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                                    <ContentTemplate>
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td align="left" height="28" valign="middle" style="width: 198px">
                                                                                    Hours spent:
                                                                                </td>
                                                                                <td align="left" valign="top">
                                                                                    <asp:TextBox ID="txthorSpent1" runat="server" Width="40px"></asp:TextBox>
                                                                                    Hours &nbsp;<asp:TextBox ID="txthrSpent1" runat="server" Width="40px"></asp:TextBox>
                                                                                    Minutes<cc1:NumericUpDownExtender ID="NumericUpDownExtender1" runat="server" TargetControlID="txthrSpent1"
                                                                                        RefValues="" Step="15" Width="80" ServiceDownMethod="" ServiceUpMethod="" TargetButtonDownID=""
                                                                                        TargetButtonUpID="">
                                                                                    </cc1:NumericUpDownExtender>
                                                                                    <cc1:NumericUpDownExtender ID="NumericUpDownExtender13" runat="server" TargetControlID="txthorSpent1"
                                                                                        RefValues="" Step="1" Width="80" ServiceDownMethod="" ServiceUpMethod="" TargetButtonDownID=""
                                                                                        TargetButtonUpID="">
                                                                                    </cc1:NumericUpDownExtender>
                                                                                    &nbsp;
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" style="width: 198px; height: 21px" valign="middle">
                                                                                    Project:
                                                                                </td>
                                                                                <td align="left" style="height: 21px" valign="top">
                                                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                                                        <tr>
                                                                                            <td style="height: 22px">
                                                                                                <%--<asp:DropDownList ID="drppro1" runat="server" Width="205px" AutoPostBack="True" OnSelectedIndexChanged="drppro1_SelectedIndexChanged">--%>
                                                                                                <asp:DropDownList ID="drppro1" runat="server" Width="205px" AutoPostBack="True">
                                                                                                    <asp:ListItem>Select</asp:ListItem>
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                            <td style="width: 10px">
                                                                                            </td>
                                                                                            <td style="width: 55px; height: 22px;">
                                                                                                Task ID:
                                                                                            </td>
                                                                                            <td style="height: 22px">
                                                                                                <%--<asp:DropDownList ID="drpTask1" runat="server" Width="64px" AutoPostBack="True" OnSelectedIndexChanged="drpTask1_SelectedIndexChanged">--%>
                                                                                                <asp:DropDownList ID="drpTask1" runat="server" Width="64px" AutoPostBack="True">
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" height="28" style="width: 198px" valign="middle">
                                                                                    Description:
                                                                                </td>
                                                                                <td align="left" valign="middle">
                                                                                    <asp:TextBox ID="txtdesc1" runat="server" ForeColor="black" Rows="2" TextMode="MultiLine"
                                                                                        Width="556px"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" valign="middle" style="width: 198px">
                                                                                    Project url:
                                                                                </td>
                                                                                <td align="left" valign="middle" style="height: 28px">
                                                                                    <asp:TextBox ID="txtproUrl1" runat="server" TextMode="MultiLine" Rows="3" Width="556px"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" valign="middle" style="width: 198px">
                                                                                    Other comments:<br />
                                                                                    <span style="font-size: 9px">(Any problems, questions, requests or suggestions)</span>
                                                                                </td>
                                                                                <td align="left" valign="middle" style="height: 28px">
                                                                                    <asp:TextBox ID="txtcomment1" runat="server" Width="556px" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                            <div id="DivTask2" runat="server" style="display: block; padding: 5px; margin: 5px;
                                                                border: solid 1px #cdcdcd; width: 750px; background: #f5f5f5;">
                                                                
                                                                <%--=======================================================--%>
                                                                
                                                                <asp:UpdateProgress ID="updateprogress4" runat="server" AssociatedUpdatePanelID="UpdatePanel3">
                                                                    <ProgressTemplate>
                                                                        <div id="processmessage" class="processMessage">
                                                                            <img alt="loading" src="images/ajax-loader.gif" style="width: 80px; height: 80px" />
                                                                            <span style="color: black; font-weight: bold; font-size: 18px; vertical-align: super;">
                                                                                Please wait... </span>
                                                                        </div>
                                                                    </ProgressTemplate>
                                                                </asp:UpdateProgress>
                                                                <%--=======================================================--%>
                                                                
                                                                
                                                                
                                                                
                                                                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                                                    <ContentTemplate>
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td align="left" height="28" valign="middle" style="width: 197px">
                                                                                    Hours spent:
                                                                                </td>
                                                                                <td align="left" valign="top">
                                                                                    <asp:TextBox ID="txthorSpent2" runat="server" Width="40px"></asp:TextBox>
                                                                                    Hours
                                                                                    <asp:TextBox ID="txthrSpent2" runat="server" Width="40px"></asp:TextBox>
                                                                                    Minutes<cc1:NumericUpDownExtender ID="NumericUpDownExtender3" runat="server" TargetControlID="txthrSpent2"
                                                                                        RefValues="" Step="15" Width="80" ServiceDownMethod="" ServiceUpMethod="" TargetButtonDownID=""
                                                                                        TargetButtonUpID="">
                                                                                    </cc1:NumericUpDownExtender>
                                                                                    <cc1:NumericUpDownExtender ID="NumericUpDownExtender14" runat="server" TargetControlID="txthorSpent2"
                                                                                        RefValues="" Step="1" Width="80" ServiceDownMethod="" ServiceUpMethod="" TargetButtonDownID=""
                                                                                        TargetButtonUpID="">
                                                                                    </cc1:NumericUpDownExtender>
                                                                                    &nbsp;&nbsp;
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" style="width: 197px; height: 20px" valign="middle">
                                                                                    Project:
                                                                                </td>
                                                                                <td align="left" style="height: 20px" valign="top">
                                                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <%--<asp:DropDownList ID="drppro2" runat="server" Width="205px" AutoPostBack="True" OnSelectedIndexChanged="drppro2_SelectedIndexChanged">--%>
                                                                                                <asp:DropDownList ID="drppro2" runat="server" Width="205px" AutoPostBack="True" >
                                                                                                    <asp:ListItem>Select</asp:ListItem>
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                            <td style="width: 10px">
                                                                                            </td>
                                                                                            <td>
                                                                                                Task ID:
                                                                                            </td>
                                                                                            <td>
                                                                                                <%--<asp:DropDownList ID="drpTask2" runat="server" Width="64px" AutoPostBack="True" OnSelectedIndexChanged="drpTask2_SelectedIndexChanged">--%>
                                                                                                <asp:DropDownList ID="drpTask2" runat="server" Width="64px" AutoPostBack="True">
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" style="width: 197px; height: 30px" valign="middle">
                                                                                    Description:
                                                                                </td>
                                                                                <td align="left" style="height: 30px" valign="middle">
                                                                                    <asp:TextBox ID="txtdesc2" runat="server" ForeColor="black" Rows="2" TextMode="MultiLine"
                                                                                        Width="556px"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" valign="middle" style="width: 197px">
                                                                                    Project url:
                                                                                </td>
                                                                                <td align="left" valign="middle" style="height: 28px">
                                                                                    <asp:TextBox ID="txtproUrl2" runat="server" TextMode="MultiLine" Rows="2" Width="557px"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" valign="middle" style="width: 197px">
                                                                                    Other comments:<br />
                                                                                    <span style="font-size: 9px">(Any problems, questions, requests or suggestions)</span>
                                                                                </td>
                                                                                <td align="left" valign="middle">
                                                                                    <asp:TextBox ID="txtcomment2" runat="server" Width="556px" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                            <div id="DivTask3" runat="server" style="display: block; padding: 5px; margin: 5px;
                                                                border: solid 1px #cdcdcd; width: 750px; background: #f5f5f5;">
                                                                 <%--=======================================================--%>
                                                                
                                                                <asp:UpdateProgress ID="updateprogress5" runat="server" AssociatedUpdatePanelID="UpdatePanel4">
                                                                    <ProgressTemplate>
                                                                        <div id="processmessage" class="processMessage">
                                                                            <img alt="loading" src="images/ajax-loader.gif" style="width: 80px; height: 80px" />
                                                                            <span style="color: black; font-weight: bold; font-size: 18px; vertical-align: super;">
                                                                                Please wait... </span>
                                                                        </div>
                                                                    </ProgressTemplate>
                                                                </asp:UpdateProgress>
                                                                <%--=======================================================--%>
                                                                
                                                                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                                    <ContentTemplate>
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td align="left" height="28" valign="middle" style="width: 196px">
                                                                                    Hours spent:
                                                                                </td>
                                                                                <td align="left" valign="top">
                                                                                    <asp:TextBox ID="txthorSpent3" runat="server" Width="40px"></asp:TextBox>
                                                                                    Hours
                                                                                    <asp:TextBox ID="txthrSpent3" runat="server" Width="40px"></asp:TextBox>
                                                                                    Minutes<cc1:NumericUpDownExtender ID="NumericUpDownExtender4" runat="server" TargetControlID="txthrSpent3"
                                                                                        RefValues="" Step="15" Width="80" ServiceDownMethod="" ServiceUpMethod="" TargetButtonDownID=""
                                                                                        TargetButtonUpID="">
                                                                                    </cc1:NumericUpDownExtender>
                                                                                    <cc1:NumericUpDownExtender ID="NumericUpDownExtender15" runat="server" TargetControlID="txthorSpent3"
                                                                                        RefValues="" Step="1" Width="80" ServiceDownMethod="" ServiceUpMethod="" TargetButtonDownID=""
                                                                                        TargetButtonUpID="">
                                                                                    </cc1:NumericUpDownExtender>
                                                                                    &nbsp;&nbsp;
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" style="width: 196px; height: 22px" valign="middle">
                                                                                    Project:
                                                                                </td>
                                                                                <td align="left" style="height: 22px" valign="top">
                                                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <%--<asp:DropDownList ID="drppro3" runat="server" Width="205px" AutoPostBack="True" OnSelectedIndexChanged="drppro3_SelectedIndexChanged">--%>
                                                                                                <asp:DropDownList ID="drppro3" runat="server" Width="205px" AutoPostBack="True">
                                                                                                    <asp:ListItem>Select</asp:ListItem>
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                            <td style="width: 10px">
                                                                                            </td>
                                                                                            <td>
                                                                                                Task url:
                                                                                            </td>
                                                                                            <td style="width: 65px">
                                                                                                <%--<asp:DropDownList ID="drpTask3" runat="server" Width="64px" AutoPostBack="True" OnSelectedIndexChanged="drpTask3_SelectedIndexChanged">--%>
                                                                                                <asp:DropDownList ID="drpTask3" runat="server" Width="64px" AutoPostBack="True" >
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" height="28" style="width: 196px" valign="middle">
                                                                                    Description:
                                                                                </td>
                                                                                <td align="left" valign="middle">
                                                                                    <asp:TextBox ID="txtdesc3" runat="server" ForeColor="black" Rows="2" TextMode="MultiLine"
                                                                                        Width="556px"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" valign="middle" style="width: 196px">
                                                                                    Project url:
                                                                                </td>
                                                                                <td align="left" valign="middle" style="height: 28px">
                                                                                    <asp:TextBox ID="txtproUrl3" runat="server" TextMode="MultiLine" Rows="2" Width="556px"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" valign="middle" style="width: 196px">
                                                                                    Other comments:<br />
                                                                                    <span style="font-size: 9px">(Any problems, questions, requests or suggestions)</span>
                                                                                </td>
                                                                                <td align="left" valign="middle">
                                                                                    <asp:TextBox ID="txtcomment3" runat="server" Width="556px" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                            <div id="DivTask4" runat="server" style="display: block; padding: 5px; margin: 5px;
                                                                border: solid 1px #cdcdcd; width: 750px; background: #f5f5f5;">
                                                                <%--=======================================================--%>
                                                                <asp:UpdateProgress ID="updateprogress6" runat="server" AssociatedUpdatePanelID="UpdatePanel5">
                                                                    <ProgressTemplate>
                                                                        <div id="processmessage" class="processMessage">
                                                                            <img alt="loading" src="images/ajax-loader.gif" style="width: 80px; height: 80px" />
                                                                            <span style="color: black; font-weight: bold; font-size: 18px; vertical-align: super;">
                                                                                Please wait... </span>
                                                                        </div>
                                                                    </ProgressTemplate>
                                                                </asp:UpdateProgress>
                                                                <%--=======================================================--%>
                                                                
                                                                
                                                                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                                                    <ContentTemplate>
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td align="left" height="28" valign="middle" style="width: 195px">
                                                                                    Hours spent:
                                                                                </td>
                                                                                <td align="left" valign="top">
                                                                                    <asp:TextBox ID="txthorSpent4" runat="server" Width="40px"></asp:TextBox>
                                                                                    Hours &nbsp;<asp:TextBox ID="txthrSpent4" runat="server" Width="40px"></asp:TextBox>
                                                                                    Minutes<cc1:NumericUpDownExtender ID="NumericUpDownExtender5" runat="server" TargetControlID="txthrSpent4"
                                                                                        RefValues="" Step="15" Width="80" ServiceDownMethod="" ServiceUpMethod="" TargetButtonDownID=""
                                                                                        TargetButtonUpID="">
                                                                                    </cc1:NumericUpDownExtender>
                                                                                    <cc1:NumericUpDownExtender ID="NumericUpDownExtender16" runat="server" TargetControlID="txthorSpent4"
                                                                                        RefValues="" Step="1" Width="80" ServiceDownMethod="" ServiceUpMethod="" TargetButtonDownID=""
                                                                                        TargetButtonUpID="">
                                                                                    </cc1:NumericUpDownExtender>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" style="width: 195px; height: 21px" valign="middle">
                                                                                    Project:
                                                                                </td>
                                                                                <td align="left" style="height: 21px" valign="top">
                                                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <%--<asp:DropDownList ID="drppro4" runat="server" Width="205px" AutoPostBack="True" OnSelectedIndexChanged="drppro4_SelectedIndexChanged">--%>
                                                                                                <asp:DropDownList ID="drppro4" runat="server" Width="205px" AutoPostBack="True">
                                                                                                    <asp:ListItem>Select</asp:ListItem>
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                            <td style="width: 10px">
                                                                                            </td>
                                                                                            <td>
                                                                                                Task ID:
                                                                                            </td>
                                                                                            <td>
                                                                                                <%--<asp:DropDownList ID="drpTask4" runat="server" Width="64px" AutoPostBack="True" OnSelectedIndexChanged="drpTask4_SelectedIndexChanged">--%>
                                                                                                <asp:DropDownList ID="drpTask4" runat="server" Width="64px" AutoPostBack="True">
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" style="width: 195px; height: 30px" valign="middle">
                                                                                    Description:
                                                                                </td>
                                                                                <td align="left" style="height: 30px" valign="middle">
                                                                                    <asp:TextBox ID="txtdesc4" runat="server" ForeColor="black" Rows="2" TextMode="MultiLine"
                                                                                        Width="555px"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" valign="middle" style="width: 195px">
                                                                                    Project url:
                                                                                </td>
                                                                                <td align="left" valign="middle" style="height: 28px">
                                                                                    <asp:TextBox ID="txtproUrl4" runat="server" TextMode="MultiLine" Rows="2" Width="556px"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" valign="middle" style="width: 195px">
                                                                                    Other comments:<br />
                                                                                    <span style="font-size: 9px">(Any problems, questions, requests or suggestions)</span>
                                                                                </td>
                                                                                <td align="left" valign="middle">
                                                                                    <asp:TextBox ID="txtcomment4" runat="server" Width="556px" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                            <div id="DivTask5" runat="server" style="display: block; padding: 5px; margin: 5px;
                                                                border: solid 1px #cdcdcd; width: 750px; background: #f5f5f5;">
                                                                <%--=======================================================--%>
                                                                <asp:UpdateProgress ID="updateprogress7" runat="server" AssociatedUpdatePanelID="UpdatePanel6">
                                                                    <ProgressTemplate>
                                                                        <div id="processmessage" class="processMessage">
                                                                            <img alt="loading" src="images/ajax-loader.gif" style="width: 80px; height: 80px" />
                                                                            <span style="color: black; font-weight: bold; font-size: 18px; vertical-align: super;">
                                                                                Please wait... </span>
                                                                        </div>
                                                                    </ProgressTemplate>
                                                                </asp:UpdateProgress>
                                                                <%--=======================================================--%>
                                                                
                                                                
                                                                <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                                                    <ContentTemplate>
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td align="left" height="28" valign="middle" style="width: 198px">
                                                                                    Hours spent:
                                                                                </td>
                                                                                <td align="left" valign="top">
                                                                                    <asp:TextBox ID="txthorSpent5" runat="server" Width="40px"></asp:TextBox>
                                                                                    Hours &nbsp;<asp:TextBox ID="txthrSpent5" runat="server" Width="40px"></asp:TextBox>
                                                                                    Minutes<cc1:NumericUpDownExtender ID="NumericUpDownExtender6" runat="server" TargetControlID="txthrSpent5"
                                                                                        RefValues="" Step="15" Width="80" ServiceDownMethod="" ServiceUpMethod="" TargetButtonDownID=""
                                                                                        TargetButtonUpID="">
                                                                                    </cc1:NumericUpDownExtender>
                                                                                    <cc1:NumericUpDownExtender ID="NumericUpDownExtender17" runat="server" TargetControlID="txthorSpent5"
                                                                                        RefValues="" Step="1" Width="80" ServiceDownMethod="" ServiceUpMethod="" TargetButtonDownID=""
                                                                                        TargetButtonUpID="">
                                                                                    </cc1:NumericUpDownExtender>
                                                                                    &nbsp;&nbsp;
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" style="width: 198px; height: 22px" valign="middle">
                                                                                    Project:
                                                                                </td>
                                                                                <td align="left" style="height: 22px" valign="top">
                                                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                                                        <tr>
                                                                                            <td style="height: 13px">
                                                                                                <%--<asp:DropDownList ID="drppro5" runat="server" Width="205px" AutoPostBack="True" OnSelectedIndexChanged="drppro5_SelectedIndexChanged">--%>
                                                                                                <asp:DropDownList ID="drppro5" runat="server" Width="205px" AutoPostBack="True">
                                                                                                    <asp:ListItem>Select</asp:ListItem>
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                            <td style="width: 10px">
                                                                                            </td>
                                                                                            <td style="height: 13px">
                                                                                                Task ID:
                                                                                            </td>
                                                                                            <td style="height: 13px">
                                                                                                <%--<asp:DropDownList ID="drpTask5" runat="server" Width="67px" AutoPostBack="True" OnSelectedIndexChanged="drpTask5_SelectedIndexChanged">--%>
                                                                                                <asp:DropDownList ID="drpTask5" runat="server" Width="67px" AutoPostBack="True">
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" height="28" valign="middle" style="width: 198px">
                                                                                </td>
                                                                                <td align="left" valign="middle">
                                                                                    &nbsp;
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" height="28" style="width: 198px" valign="middle">
                                                                                    Description:
                                                                                </td>
                                                                                <td align="left" valign="middle">
                                                                                    <asp:TextBox ID="txtdesc5" runat="server" ForeColor="black" Rows="2" TextMode="MultiLine"
                                                                                        Width="556px"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" valign="middle" style="width: 198px">
                                                                                    Project url:
                                                                                </td>
                                                                                <td align="left" valign="middle" style="height: 28px">
                                                                                    <asp:TextBox ID="txtproUrl5" runat="server" TextMode="MultiLine" Rows="2" Width="556px"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" valign="middle" style="width: 198px">
                                                                                    Other comments:<br />
                                                                                    <span style="font-size: 9px">(Any problems, questions, requests or suggestions)</span>
                                                                                </td>
                                                                                <td align="left" valign="middle" style="height: 28px">
                                                                                    <asp:TextBox ID="txtcomment5" runat="server" Width="555px" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                            <div id="DivTask6" runat="server" style="display: block; padding: 5px; margin: 5px;
                                                                border: solid 1px #cdcdcd; width: 750px; background: #f5f5f5;">
                                                                <%--=======================================================--%>
                                                                <asp:UpdateProgress ID="updateprogress8" runat="server" AssociatedUpdatePanelID="UpdatePanel7">
                                                                    <ProgressTemplate>
                                                                        <div id="processmessage" class="processMessage">
                                                                            <img alt="loading" src="images/ajax-loader.gif" style="width: 80px; height: 80px" />
                                                                            <span style="color: black; font-weight: bold; font-size: 18px; vertical-align: super;">
                                                                                Please wait... </span>
                                                                        </div>
                                                                    </ProgressTemplate>
                                                                </asp:UpdateProgress>
                                                                <%--=======================================================--%>
                                                                
                                                                
                                                                <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                                                    <ContentTemplate>
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td align="left" height="28" valign="middle" style="width: 198px">
                                                                                    Hours spent:
                                                                                </td>
                                                                                <td align="left" valign="top">
                                                                                    <asp:TextBox ID="txthorSpent6" runat="server" Width="40px"></asp:TextBox>
                                                                                    Hours &nbsp;<asp:TextBox ID="txthrSpent6" runat="server" Width="40px"></asp:TextBox>
                                                                                    Minutes<cc1:NumericUpDownExtender ID="NumericUpDownExtender7" runat="server" TargetControlID="txthrSpent6"
                                                                                        RefValues="" Step="15" Width="80" ServiceDownMethod="" ServiceUpMethod="" TargetButtonDownID=""
                                                                                        TargetButtonUpID="">
                                                                                    </cc1:NumericUpDownExtender>
                                                                                    <cc1:NumericUpDownExtender ID="NumericUpDownExtender18" runat="server" TargetControlID="txthorSpent6"
                                                                                        RefValues="" Step="1" Width="80" ServiceDownMethod="" ServiceUpMethod="" TargetButtonDownID=""
                                                                                        TargetButtonUpID="">
                                                                                    </cc1:NumericUpDownExtender>
                                                                                    &nbsp;&nbsp;
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" height="28" style="width: 198px" valign="middle">
                                                                                    Project:
                                                                                </td>
                                                                                <td align="left" valign="top">
                                                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <%--<asp:DropDownList ID="drppro6" runat="server" Width="205px" AutoPostBack="True" OnSelectedIndexChanged="drppro6_SelectedIndexChanged">--%>
                                                                                                <asp:DropDownList ID="drppro6" runat="server" Width="205px" AutoPostBack="True">
                                                                                                    <asp:ListItem>Select</asp:ListItem>
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                            <td style="width: 10px">
                                                                                            </td>
                                                                                            <td>
                                                                                                Task ID:
                                                                                            </td>
                                                                                            <td>
                                                                                                <%--<asp:DropDownList ID="drpTask6" runat="server" Width="64px" AutoPostBack="True" OnSelectedIndexChanged="drpTask6_SelectedIndexChanged">--%>
                                                                                                <asp:DropDownList ID="drpTask6" runat="server" Width="64px" AutoPostBack="True">
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" height="28" style="width: 198px" valign="middle">
                                                                                    Description:
                                                                                </td>
                                                                                <td align="left" valign="middle">
                                                                                    <asp:TextBox ID="txtdesc6" runat="server" ForeColor="black" Rows="2" TextMode="MultiLine"
                                                                                        Width="555px"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" valign="middle" style="width: 198px">
                                                                                    Project url:
                                                                                </td>
                                                                                <td align="left" valign="middle" style="height: 28px">
                                                                                    <asp:TextBox ID="txtproUrl6" TextMode="MultiLine" Rows="2" runat="server" Width="556px"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" valign="middle" style="width: 198px">
                                                                                    Other comments:<br />
                                                                                    <span style="font-size: 9px">(Any problems, questions, requests or suggestions)</span>
                                                                                </td>
                                                                                <td align="left" valign="middle">
                                                                                    <asp:TextBox ID="txtcomment6" runat="server" Width="556px" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                            <div id="DivTask7" runat="server" style="display: block; padding: 5px; margin: 5px;
                                                                border: solid 1px #cdcdcd; width: 750px; background: #f5f5f5;">
                                                                <%--=======================================================--%>
                                                                <asp:UpdateProgress ID="updateprogress9" runat="server" AssociatedUpdatePanelID="UpdatePanel8">
                                                                    <ProgressTemplate>
                                                                        <div id="processmessage" class="processMessage">
                                                                            <img alt="loading" src="images/ajax-loader.gif" style="width: 80px; height: 80px" />
                                                                            <span style="color: black; font-weight: bold; font-size: 18px; vertical-align: super;">
                                                                                Please wait... </span>
                                                                        </div>
                                                                    </ProgressTemplate>
                                                                </asp:UpdateProgress>
                                                                <%--=======================================================--%>
                                                                
                                                                <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                                                    <ContentTemplate>
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td align="left" height="28" valign="middle" style="width: 198px">
                                                                                    Hours spent:
                                                                                </td>
                                                                                <td align="left" valign="top">
                                                                                    <asp:TextBox ID="txthorSpent7" runat="server" Width="40px"></asp:TextBox>
                                                                                    Hours &nbsp;<asp:TextBox ID="txthrSpent7" runat="server" Width="40px"></asp:TextBox>
                                                                                    Minutes<cc1:NumericUpDownExtender ID="NumericUpDownExtender8" runat="server" TargetControlID="txthrSpent7"
                                                                                        RefValues="" Step="15" Width="80" ServiceDownMethod="" ServiceUpMethod="" TargetButtonDownID=""
                                                                                        TargetButtonUpID="">
                                                                                    </cc1:NumericUpDownExtender>
                                                                                    <cc1:NumericUpDownExtender ID="NumericUpDownExtender19" runat="server" TargetControlID="txthorSpent7"
                                                                                        RefValues="" Step="1" Width="80" ServiceDownMethod="" ServiceUpMethod="" TargetButtonDownID=""
                                                                                        TargetButtonUpID="">
                                                                                    </cc1:NumericUpDownExtender>
                                                                                    &nbsp;&nbsp;
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" style="width: 198px; height: 22px;" valign="middle">
                                                                                    Project:
                                                                                </td>
                                                                                <td align="left" valign="top" style="height: 22px">
                                                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <%--<asp:DropDownList ID="drppro7" runat="server" Width="205px" AutoPostBack="True" OnSelectedIndexChanged="drppro7_SelectedIndexChanged">--%>
                                                                                                <asp:DropDownList ID="drppro7" runat="server" Width="205px" AutoPostBack="True">
                                                                                                    <asp:ListItem>Select</asp:ListItem>
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                            <td style="width: 10px">
                                                                                            </td>
                                                                                            <td>
                                                                                                Task ID:
                                                                                            </td>
                                                                                            <td>
                                                                                                <%--<asp:DropDownList ID="drpTask7" runat="server" Width="64px" AutoPostBack="True" OnSelectedIndexChanged="drpTask7_SelectedIndexChanged">--%>
                                                                                                <asp:DropDownList ID="drpTask7" runat="server" Width="64px" AutoPostBack="True" >
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" height="28" style="width: 198px" valign="middle">
                                                                                    Description:
                                                                                </td>
                                                                                <td align="left" valign="middle">
                                                                                    <asp:TextBox ID="txtdesc7" runat="server" ForeColor="black" Rows="2" TextMode="MultiLine"
                                                                                        Width="556px"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" valign="middle" style="width: 198px">
                                                                                    Project url:
                                                                                </td>
                                                                                <td align="left" valign="middle" style="height: 28px">
                                                                                    <asp:TextBox ID="txtproUrl7" runat="server" TextMode="MultiLine" Rows="2" Width="556px"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" valign="middle" style="width: 198px">
                                                                                    Other comments:<br />
                                                                                    <span style="font-size: 9px">(Any problems, questions, requests or suggestions)</span>
                                                                                </td>
                                                                                <td align="left" valign="middle">
                                                                                    <asp:TextBox ID="txtcomment7" runat="server" Width="556px" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                            <div id="DivTask8" runat="server" style="display: block; padding: 5px; margin: 5px;
                                                                border: solid 1px #cdcdcd; width: 750px; background: #f5f5f5;">
                                                                 <%--=======================================================--%>
                                                                <asp:UpdateProgress ID="updateprogress10" runat="server" AssociatedUpdatePanelID="UpdatePanel9">
                                                                    <ProgressTemplate>
                                                                        <div id="processmessage" class="processMessage">
                                                                            <img alt="loading" src="images/ajax-loader.gif" style="width: 80px; height: 80px" />
                                                                            <span style="color: black; font-weight: bold; font-size: 18px; vertical-align: super;">
                                                                                Please wait... </span>
                                                                        </div>
                                                                    </ProgressTemplate>
                                                                </asp:UpdateProgress>
                                                                <%--=======================================================--%>
                                                                
                                                                
                                                                
                                                                <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                                                    <ContentTemplate>
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td align="left" height="28" valign="middle" style="width: 198px">
                                                                                    Hours spent:
                                                                                </td>
                                                                                <td align="left" valign="top">
                                                                                    <asp:TextBox ID="txthorSpent8" runat="server" Width="40px"></asp:TextBox>
                                                                                    Hours &nbsp;<asp:TextBox ID="txthrSpent8" runat="server" Width="40px"></asp:TextBox>
                                                                                    Minutes<cc1:NumericUpDownExtender ID="NumericUpDownExtender9" runat="server" TargetControlID="txthrSpent8"
                                                                                        RefValues="" Step="15" Width="80" ServiceDownMethod="" ServiceUpMethod="" TargetButtonDownID=""
                                                                                        TargetButtonUpID="">
                                                                                    </cc1:NumericUpDownExtender>
                                                                                    <cc1:NumericUpDownExtender ID="NumericUpDownExtender20" runat="server" TargetControlID="txthorSpent8"
                                                                                        RefValues="" Step="1" Width="80" ServiceDownMethod="" ServiceUpMethod="" TargetButtonDownID=""
                                                                                        TargetButtonUpID="">
                                                                                    </cc1:NumericUpDownExtender>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" style="width: 198px; height: 22px" valign="middle">
                                                                                    Project:
                                                                                </td>
                                                                                <td align="left" valign="top" style="height: 22px">
                                                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <%--<asp:DropDownList ID="drppro8" runat="server" Width="205px" AutoPostBack="True" OnSelectedIndexChanged="drppro8_SelectedIndexChanged">--%>
                                                                                                <asp:DropDownList ID="drppro8" runat="server" Width="205px" AutoPostBack="True">
                                                                                                    <asp:ListItem>Select</asp:ListItem>
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                            <td style="width: 10px">
                                                                                            </td>
                                                                                            <td>
                                                                                                Task ID:
                                                                                            </td>
                                                                                            <td>
                                                                                                <%--<asp:DropDownList ID="drpTask8" runat="server" Width="64px" AutoPostBack="True" OnSelectedIndexChanged="drpTask8_SelectedIndexChanged">--%>
                                                                                                <asp:DropDownList ID="drpTask8" runat="server" Width="64px" AutoPostBack="True" >
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" height="28" style="width: 198px" valign="middle">
                                                                                    Description:
                                                                                </td>
                                                                                <td align="left" valign="middle">
                                                                                    <asp:TextBox ID="txtdesc8" runat="server" ForeColor="black" Rows="2" TextMode="MultiLine"
                                                                                        Width="556px"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" valign="middle" style="width: 198px; height: 23px;">
                                                                                    Project url:
                                                                                </td>
                                                                                <td align="left" valign="middle" style="height: 23px">
                                                                                    <asp:TextBox ID="txtproUrl8" runat="server" TextMode="MultiLine" Rows="2" Width="556px"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" valign="middle" style="width: 198px">
                                                                                    Other comments:<br />
                                                                                    <span style="font-size: 9px">(Any problems, questions, requests or suggestions)</span>
                                                                                </td>
                                                                                <td align="left" valign="middle">
                                                                                    <asp:TextBox ID="txtcomment8" runat="server" Width="556px" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                            <div id="DivTask9" runat="server" style="display: block; padding: 5px; margin: 5px;
                                                                border: solid 1px #cdcdcd; width: 750px; background: #f5f5f5;">
                                                                 <%--=======================================================--%>
                                                                <asp:UpdateProgress ID="updateprogress11" runat="server" AssociatedUpdatePanelID="UpdatePanel10">
                                                                    <ProgressTemplate>
                                                                        <div id="processmessage" class="processMessage">
                                                                            <img alt="loading" src="images/ajax-loader.gif" style="width: 80px; height: 80px" />
                                                                            <span style="color: black; font-weight: bold; font-size: 18px; vertical-align: super;">
                                                                                Please wait... </span>
                                                                        </div>
                                                                    </ProgressTemplate>
                                                                </asp:UpdateProgress>
                                                                <%--=======================================================--%>
                                                                
                                                                <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                                                                    <ContentTemplate>
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td align="left" height="28" valign="middle" style="width: 198px">
                                                                                    Hours spent:
                                                                                </td>
                                                                                <td align="left" valign="top">
                                                                                    <asp:TextBox ID="txthorSpent9" runat="server" Width="40px"></asp:TextBox>
                                                                                    Hours &nbsp;<asp:TextBox ID="txthrSpent9" runat="server" Width="40px"></asp:TextBox>
                                                                                    Minutes<cc1:NumericUpDownExtender ID="NumericUpDownExtender10" runat="server" TargetControlID="txthrSpent9"
                                                                                        RefValues="" Step="15" Width="80" ServiceDownMethod="" ServiceUpMethod="" TargetButtonDownID=""
                                                                                        TargetButtonUpID="">
                                                                                    </cc1:NumericUpDownExtender>
                                                                                    <cc1:NumericUpDownExtender ID="NumericUpDownExtender21" runat="server" TargetControlID="txthorSpent9"
                                                                                        RefValues="" Step="1" Width="80" ServiceDownMethod="" ServiceUpMethod="" TargetButtonDownID=""
                                                                                        TargetButtonUpID="">
                                                                                    </cc1:NumericUpDownExtender>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" style="width: 198px; height: 20px" valign="middle">
                                                                                    Project:
                                                                                </td>
                                                                                <td align="left" style="height: 20px" valign="top">
                                                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <%--<asp:DropDownList ID="drppro9" runat="server" Width="205px" AutoPostBack="True" OnSelectedIndexChanged="drppro9_SelectedIndexChanged">--%>
                                                                                                <asp:DropDownList ID="drppro9" runat="server" Width="205px" AutoPostBack="True">
                                                                                                    <asp:ListItem>Select</asp:ListItem>
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                            <td style="width: 10px">
                                                                                            </td>
                                                                                            <td>
                                                                                                Task ID:
                                                                                            </td>
                                                                                            <td>
                                                                                                <%--<asp:DropDownList ID="drpTask9" runat="server" Width="64px" AutoPostBack="True" OnSelectedIndexChanged="drpTask9_SelectedIndexChanged">--%>
                                                                                                <asp:DropDownList ID="drpTask9" runat="server" Width="64px" AutoPostBack="True" >
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" height="28" style="width: 198px" valign="middle">
                                                                                    Description:
                                                                                </td>
                                                                                <td align="left" valign="middle">
                                                                                    <asp:TextBox ID="txtdesc9" runat="server" ForeColor="black" Rows="2" TextMode="MultiLine"
                                                                                        Width="556px"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" valign="middle" style="width: 198px">
                                                                                    Project url:
                                                                                </td>
                                                                                <td align="left" valign="middle" style="height: 28px">
                                                                                    <asp:TextBox ID="txtproUrl9" TextMode="MultiLine" Rows="2" runat="server" Width="556px"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" valign="middle" style="width: 198px">
                                                                                    Other comments:<br />
                                                                                    <span style="font-size: 9px">(Any problems, questions, requests or suggestions)</span>
                                                                                </td>
                                                                                <td align="left" valign="middle">
                                                                                    <asp:TextBox ID="txtcomment9" runat="server" Width="556px" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" valign="top">
                                                            Task which will be continued or started on the next day. If you expect the task
                                                            and others you have will be completed in less than 4 hours then please ensure that
                                                            you have asked to be given a new task before the end of today.
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" style="height: 15px" valign="middle">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" style="height: 35px" valign="middle">
                                                            <div style="display: block; padding: 5px; margin: 5px; border: solid 1px #cdcdcd;
                                                                width: 750px; background: #f5f5f5;">
                                                                <asp:UpdateProgress ID="updateprogress2" runat="server" AssociatedUpdatePanelID="UpdatePanel11">
                                                                    <ProgressTemplate>
                                                                        <div id="processmessage" class="processMessage">
                                                                            <img alt="loading" src="images/ajax-loader.gif" style="width: 80px; height: 80px" />
                                                                            <span style="color: black; font-weight: bold; font-size: 18px; vertical-align: super;">
                                                                                Please wait... </span>
                                                                        </div>
                                                                    </ProgressTemplate>
                                                                </asp:UpdateProgress>
                                                                <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                                                    <ContentTemplate>
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td align="left" height="28" valign="middle" style="width: 198px">
                                                                                    Hours which this task may take to complete:
                                                                                </td>
                                                                                <td align="left" valign="top">
                                                                                    <asp:TextBox ID="txttaskHrsETC" runat="server" Width="40px"></asp:TextBox>
                                                                                    Hours &nbsp;
                                                                                    <asp:TextBox ID="txttaskETC" runat="server" Width="40px"></asp:TextBox>
                                                                                    Minutes
                                                                                    <cc1:NumericUpDownExtender ID="NumericUpDownExtender11" runat="server" TargetControlID="txttaskETC"
                                                                                        RefValues="" Step="15" Width="80" ServiceDownMethod="" ServiceUpMethod="" TargetButtonDownID=""
                                                                                        TargetButtonUpID="">
                                                                                    </cc1:NumericUpDownExtender>
                                                                                    <cc1:NumericUpDownExtender ID="NumericUpDownExtender22" runat="server" TargetControlID="txttaskHrsETC"
                                                                                        RefValues="" Step="1" Width="80" ServiceDownMethod="" ServiceUpMethod="" TargetButtonDownID=""
                                                                                        TargetButtonUpID="">
                                                                                    </cc1:NumericUpDownExtender>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" style="width: 198px; height: 19px" valign="middle">
                                                                                    Project:
                                                                                </td>
                                                                                <td align="left" style="height: 19px" valign="top">
                                                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <%--<asp:DropDownList ID="drpNxtpro" runat="server" Width="205px" AutoPostBack="True"
                                                                                                    OnSelectedIndexChanged="drpNxtpro_SelectedIndexChanged">--%>
                                                                                                    <asp:DropDownList ID="drpNxtpro" runat="server" Width="205px" AutoPostBack="True">
                                                                                                    <asp:ListItem>Select</asp:ListItem>
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                            <td style="width: 10px">
                                                                                            </td>
                                                                                            <td>
                                                                                                Task ID:
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:ValidationSummary ID="ValidationSummary" runat="server" ShowMessageBox="True"
                                                                                                    ShowSummary="False" />
                                                                                                <%--<asp:DropDownList ID="drpNxtTask" runat="server" Width="61px" AutoPostBack="True"
                                                                                                    OnSelectedIndexChanged="drpNxtTask_SelectedIndexChanged">--%>
                                                                                                    <asp:DropDownList ID="drpNxtTask" runat="server" Width="61px" AutoPostBack="True">
                                                                                                </asp:DropDownList>
                                                                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="drpNxtTask"
                                                                                                    ErrorMessage="Please Select the next task and then send your EODC">*</asp:RequiredFieldValidator>--%>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" style="width: 198px; height: 30px" valign="middle">
                                                                                    Description:
                                                                                </td>
                                                                                <td align="left" style="height: 30px" valign="middle">
                                                                                    <asp:TextBox ID="TxtNextDesc" runat="server" ForeColor="black" Rows="2" TextMode="MultiLine"
                                                                                        Width="556px"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" valign="middle" style="width: 198px">
                                                                                    Project url:
                                                                                </td>
                                                                                <td align="left" valign="middle" style="height: 28px">
                                                                                    <asp:TextBox ID="txtProjecturl" runat="server" TextMode="MultiLine" Rows="2" Width="556px"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" valign="middle" style="width: 198px; height: 88px;">
                                                                                    Other comments:<br />
                                                                                    <span style="font-size: 9px">(Any problems, questions, requests or suggestions)</span>
                                                                                </td>
                                                                                <td align="left" valign="middle" style="height: 88px">
                                                                                    <asp:TextBox ID="txtTaskcomment" runat="server" Width="556px" TextMode="MultiLine"
                                                                                        Rows="3"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" valign="top">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" style="height: 18px; color: Red" valign="middle">
                                                            Please confirm
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <%--<td align="left" style="color: Black; height: 25px; border: solid 1px #000000; padding: 5px;"
                                                            valign="middle">
                                                            <span style="color: red">*</span><strong>This question is meant to remind you to do
                                                                update</strong> all FDS tasks which need updating. This might be a task whose
                                                            status needs updating or a question needs answering or progress has been made which
                                                            needs adding to the FDS task item.<strong>If there are no tasks which need updating
                                                                then the tasks are all updated and the answer is YES.</strong> Only answer no
                                                            if you havent updated tasks which have updates or if you havent updated tasks which
                                                            have questions. If a question is asked in a task you will get an email from FDS.
                                                            It is essential to answer that question ASAP after receiving the question.
                                                        </td>--%>
                                                       <%-- ====================================================================================================--%>
                                                        <td align="left" style="color: Black; height: 25px; border: solid 1px #000000; padding: 5px;"
                                                            valign="middle">
                                                            <span style="color: red">*</span><strong>This question is meant to remind you to do
                                                                update</strong> all ESG Desk tasks which need updating. This might be a task whose
                                                            status needs updating or a question needs answering or progress has been made which
                                                            needs adding to the ESG Desk task item.<strong>If there are no tasks which need updating
                                                                then the tasks are all updated and the answer is YES.</strong> Only answer no
                                                            if you havent updated tasks which have updates or if you havent updated tasks which
                                                            have questions. If a question is asked in a task you will get an email from ESG Desk.
                                                            It is essential to answer that question ASAP after receiving the question.
                                                        </td>
                                                        <%--=====================================================================================================--%>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" valign="top">
                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td align="left" colspan="2" valign="middle">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="middle" style="width: 206px">
                                                                    
                                                                        <%--You have checked and updated all tasks in FDS?--%>
                                                                        You have checked and updated all tasks in ESG Desk?<span style="color: red">*</span>
                                                                    </td>
                                                                    <td align="left" style="height: 35px" valign="middle">
                                                                        <asp:DropDownList ID="drpTaskUpdate" runat="server">
                                                                            <asp:ListItem Value="No">No</asp:ListItem>
                                                                            <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" valign="top">
                                                            <table id="TrUpdate" runat="server" border="0" width="100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td align="left" valign="middle" style="width: 206px; height: 38px;">
                                                                        Please explain/give details:
                                                                    </td>
                                                                    <td align="left" style="height: 38px" valign="middle">
                                                                        <asp:TextBox ID="txtExpainUpdate" runat="server" Rows="2" TextMode="MultiLine" Width="283px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" valign="top">
                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td align="left" valign="middle" style="width: 206px; height: 35px;">
                                                                        You have tasks for the next day for at least the first 4 hours?
                                                                    </td>
                                                                    <td align="left" style="height: 35px" valign="middle">
                                                                        <asp:DropDownList ID="drpNextdayTask" runat="server">
                                                                            <asp:ListItem Value="No">No</asp:ListItem>
                                                                            <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" valign="top">
                                                            <table id="TrNextTask" runat="server" width="100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td align="left" valign="middle" style="width: 206px">
                                                                        Please explain/give details:
                                                                    </td>
                                                                    <td align="left" style="height: 35px" valign="middle">
                                                                        <asp:TextBox ID="txtExpainNextTask" runat="server" Rows="2" TextMode="MultiLine"
                                                                            Width="283px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" valign="top">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td width="206" align="left" valign="middle">
                                                                        <span style="width: 102px; height: 35px">Today's time in:</span>
                                                                    </td>
                                                                    <td align="left" valign="middle" style="height: 35px">
                                                                        <select id="drpInHr" runat="server" class="bdatetime" name="select4">
                                                                            <option selected="selected">01</option>
                                                                            <option>02</option>
                                                                            <option>03</option>
                                                                            <option>04</option>
                                                                            <option>05</option>
                                                                            <option>06</option>
                                                                            <option>07</option>
                                                                            <option>08</option>
                                                                            <option>09</option>
                                                                            <option>10</option>
                                                                            <option>11</option>
                                                                            <option>12</option>
                                                                        </select>
                                                                        <select id="drpInMin" runat="server" class="bdatetime" name="select4">
                                                                            <option selected="selected">:00</option>
                                                                            <option>:05</option>
                                                                            <option>:10</option>
                                                                            <option>:15</option>
                                                                            <option>:20</option>
                                                                            <option>:25</option>
                                                                            <option>:30</option>
                                                                            <option>:35</option>
                                                                            <option>:40</option>
                                                                            <option>:45</option>
                                                                            <option>:50</option>
                                                                            <option>:55</option>
                                                                        </select>
                                                                        <select id="drpInTime" runat="server" class="bdatetime" name="select4">
                                                                            <option value=" am">am</option>
                                                                            <option selected="selected" value=" pm">pm</option>
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="middle">
                                                                        <span style="width: 102px; height: 28px">Today's time out:</span>
                                                                    </td>
                                                                    <td align="left" style="height: 28px" valign="middle">
                                                                        <select id="drpOutHr" runat="server" class="bdatetime" name="select4">
                                                                            <option selected="selected">01</option>
                                                                            <option>02</option>
                                                                            <option>03</option>
                                                                            <option>04</option>
                                                                            <option>05</option>
                                                                            <option>06</option>
                                                                            <option>07</option>
                                                                            <option>08</option>
                                                                            <option>09</option>
                                                                            <option>10</option>
                                                                            <option>11</option>
                                                                            <option>12</option>
                                                                        </select>
                                                                        <select id="drpOutMin" runat="server" class="bdatetime" name="select4">
                                                                            <option selected="selected">:00</option>
                                                                            <option>:05</option>
                                                                            <option>:10</option>
                                                                            <option>:15</option>
                                                                            <option>:20</option>
                                                                            <option>:25</option>
                                                                            <option>:30</option>
                                                                            <option>:35</option>
                                                                            <option>:40</option>
                                                                            <option>:45</option>
                                                                            <option>:50</option>
                                                                            <option>:55</option>
                                                                        </select>
                                                                        <select id="drpOutTime" runat="server" class="bdatetime" name="select4">
                                                                            <option value=" am">am</option>
                                                                            <option selected="selected" value=" pm">pm</option>
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="middle" style="height: 24px">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="right" style="height: 24px;" valign="middle">
                                                                        <asp:Button ID="Button2" runat="server" Text="Send eodc" Visible="False" CssClass="blueBtns" />
                                                                        &nbsp;
                                                                        <asp:Button ID="Button1" runat="server" Text="Send EODC" OnClick="Button1_Click"
                                                                            CssClass="blueBtns" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" valign="top">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="middle">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="middle">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="height: 3px" valign="top">
                                                <asp:Label ID="lblMessage" runat="server" Font-Bold="True" ForeColor="Red" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="width: 500px; height: 10px">
                                    &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
