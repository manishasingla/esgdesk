using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Mail;

public partial class EODC : System.Web.UI.Page
{
    DateTime Intime;
    DateTime outtime;

    string sql = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        //========================================================================
        
        drppro0.Attributes.Add("onchange", "return GetDataViaAJAXA();");
        drpTask0.Attributes.Add("onchange", "return GetDataViaAJAXforDescA();");

        drppro1.Attributes.Add("onchange", "return GetDataViaAJAXB();");
        drpTask1.Attributes.Add("onchange", "return GetDataViaAJAXforDescB();");

        drppro2.Attributes.Add("onchange", "return GetDataViaAJAXC();");
        drpTask2.Attributes.Add("onchange", "return GetDataViaAJAXforDescC();");

        drppro3.Attributes.Add("onchange", "return GetDataViaAJAXD();");
        drpTask3.Attributes.Add("onchange", "return GetDataViaAJAXforDescD();");

        drppro4.Attributes.Add("onchange", "return GetDataViaAJAXE();");
        drpTask4.Attributes.Add("onchange", "return GetDataViaAJAXforDescE();");

        drppro5.Attributes.Add("onchange", "return GetDataViaAJAXF();");
        drpTask5.Attributes.Add("onchange", "return GetDataViaAJAXforDescF();");

        drppro6.Attributes.Add("onchange", "return GetDataViaAJAXG();");
        drpTask6.Attributes.Add("onchange", "return GetDataViaAJAXforDescG();");

        drppro7.Attributes.Add("onchange", "return GetDataViaAJAXH();");
        drpTask7.Attributes.Add("onchange", "return GetDataViaAJAXforDescH();");

        drppro8.Attributes.Add("onchange", "return GetDataViaAJAXI();");
        drpTask8.Attributes.Add("onchange", "return GetDataViaAJAXforDescI();");

        drppro9.Attributes.Add("onchange", "return GetDataViaAJAXJ();");
        drpTask9.Attributes.Add("onchange", "return GetDataViaAJAXforDescJ();");

        drpNxtpro.Attributes.Add("onchange", "return GetDataViaAJAXK();");
        drpNxtTask.Attributes.Add("onchange", "return GetDataViaAJAXforDescK();");
        //========================================================================
        //drpTask0.Items.Insert(0, new ListItem("-Please Select-", "-1"));
        
        // ScriptManager.RegisterStartupScript(Page, typeof(Page), "init", "load();", true);
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = "EODC.aspx";

            Response.Redirect("login.aspx");
            return;
        }

        sql = "select count(*) from tasks where assigned_to_user ='" + Session["admin"] + "' and deleted <> 1 and status <>'closed' and status = 'CWO' ";

        object count = DatabaseHelper.executeScalar(sql);

        if (count.ToString() != "0")
        {
            //Response.Write("<script>alert(You already have task assigned status CWO or CWO but away'. Please ensure only one task always has assigned status: �CWO� or �CWO but away�.);</script>");
             
            Session["cwo"] = "y";
            Response.Redirect("tasks.aspx");
            return;

        }
        else
        {
            Session["cwo"] = "n";
        }

        drpNoOfTask.Attributes.Add("onchange", "ShowNoOfTask();");
        drpTaskUpdate.Attributes.Add("onchange", "ShowDetail();");
        drpNextdayTask.Attributes.Add("onchange", "ShowNextTaskDetail();");
        //listBox1.Items.Add("KeyPress event called of TextBox2");
        txtExpainUpdate.Attributes.Add("onkeypress", "Enablebutton();");
        txtExpainNextTask.Attributes.Add("onkeypress", "Enablebutton();");
        txtExpainNextTask.Attributes.Add("onblur", "Enablebutton();");
        txtExpainUpdate.Attributes.Add("onblur", "Enablebutton();");
        // Button1.Attributes.Add("onclick", "return checkupdate();");

        if (!IsPostBack)
        {
            object objUserName = DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + Session["admin"].ToString() + "'");

            if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
            {
                lnkUserName.InnerHtml = "Logged in as emp: " + objUserName.ToString();
            }
            else
            {
                lnkUserName.InnerHtml = "Logged in as admin: " + objUserName.ToString();
            }
            //====================================
             //bind_project_name();
             //bind_task_id();
            
             //DatabaseHelper.getAllProjectDs(ref drppro0);
             //DatabaseHelper.getAllProjectDs(ref drppro1);
             //DatabaseHelper.getAllProjectDs(ref drppro2);
             //DatabaseHelper.getAllProjectDs(ref drppro3);
             //DatabaseHelper.getAllProjectDs(ref drppro4);
             //DatabaseHelper.getAllProjectDs(ref drppro5);
             //DatabaseHelper.getAllProjectDs(ref drppro6);
             //DatabaseHelper.getAllProjectDs(ref drppro7);
             //DatabaseHelper.getAllProjectDs(ref drppro8);
             //DatabaseHelper.getAllProjectDs(ref drppro9);
             //DatabaseHelper.getAllProjectDs(ref drpNxtpro);
            
            BindallProject();

            //====================================
            
        }
        
    }

    void BindallProject()
    {
                DataSet dsProject=new DataSet(); 
                dsProject=DatabaseHelper.getProjectDs();
                drppro0.DataSource = dsProject.Tables[0];
                drppro1.DataSource = dsProject.Tables[0];
                drppro2.DataSource = dsProject.Tables[0];
                drppro3.DataSource = dsProject.Tables[0];
                drppro4.DataSource = dsProject.Tables[0];
                drppro5.DataSource = dsProject.Tables[0];
                drppro6.DataSource = dsProject.Tables[0];
                drppro7.DataSource = dsProject.Tables[0];
                drppro8.DataSource = dsProject.Tables[0];
                drppro9.DataSource = dsProject.Tables[0];
                drpNxtpro.DataSource = dsProject.Tables[0];

                drppro0.DataTextField = "project_name";
                drppro0.DataValueField = "project_name";
                drppro1.DataTextField = "project_name";
                drppro1.DataValueField = "project_name";
                drppro2.DataTextField = "project_name";
                drppro2.DataValueField = "project_name";
                drppro3.DataTextField = "project_name";
                drppro3.DataValueField = "project_name";
                drppro4.DataTextField = "project_name";
                drppro4.DataValueField = "project_name";
                drppro5.DataTextField = "project_name";
                drppro5.DataValueField = "project_name";
                drppro6.DataTextField = "project_name";
                drppro6.DataValueField = "project_name";
                drppro7.DataTextField = "project_name";
                drppro7.DataValueField = "project_name";
                drppro8.DataTextField = "project_name";
                drppro8.DataValueField = "project_name";
                drppro9.DataTextField = "project_name";
                drppro9.DataValueField = "project_name";
                drpNxtpro.DataTextField = "project_name";
                drpNxtpro.DataValueField = "project_name";
                drppro0.DataBind();
                drppro1.DataBind();
                drppro2.DataBind();
                drppro3.DataBind();
                drppro4.DataBind();
                drppro5.DataBind();
                drppro6.DataBind();
                drppro7.DataBind();
                drppro8.DataBind();
                drppro9.DataBind();
                drpNxtpro.DataBind();

    }








    void bind_project_name()
    {
        try
        {
    //==============================================================================================
            //DataSet ds = DatabaseHelper.getDataset(sql);
            //sql = "select project_id,project_name from projects order by project_name";
            DataSet ds = DatabaseHelper.getProjectDataset();
                  
    //==========================================================================================         
            if (ds != null && ds.Tables.Count > 0)
               {
                DataRow row = ds.Tables[0].NewRow();
                row["project_name"] = "Select";
                /// row["ID"] = "All";
                ds.Tables[0].Rows.InsertAt(row, 0);
                //drppro0.DataSource = ds.Tables[0];
                //drppro1.DataSource = ds.Tables[0];
                //drppro2.DataSource = ds.Tables[0];
                //drppro3.DataSource = ds.Tables[0];
                //drppro4.DataSource = ds.Tables[0];
                //drppro5.DataSource = ds.Tables[0];
                //drppro6.DataSource = ds.Tables[0];
                //drppro7.DataSource = ds.Tables[0];
                //drppro8.DataSource = ds.Tables[0];
                //drppro9.DataSource = ds.Tables[0];
                //drpNxtpro.DataSource = ds.Tables[0];
                //drppro0.DataTextField = "project_name";
                //drppro0.DataValueField = "project_name";
                //drppro1.DataTextField = "project_name";
                //drppro1.DataValueField = "project_name";
                //drppro2.DataTextField = "project_name";
                //drppro2.DataValueField = "project_name";
                //drppro3.DataTextField = "project_name";
                //drppro3.DataValueField = "project_name";
                //drppro4.DataTextField = "project_name";
                //drppro4.DataValueField = "project_name";
                //drppro5.DataTextField = "project_name";
                //drppro5.DataValueField = "project_name";
                //drppro6.DataTextField = "project_name";
                //drppro6.DataValueField = "project_name";
                //drppro7.DataTextField = "project_name";
                //drppro7.DataValueField = "project_name";
                //drppro8.DataTextField = "project_name";
                //drppro8.DataValueField = "project_name";
                //drppro9.DataTextField = "project_name";
                //drppro9.DataValueField = "project_name";
                //drpNxtpro.DataTextField = "project_name";
                //drpNxtpro.DataValueField = "project_name";
                //drppro0.DataBind();
                //drppro1.DataBind();
                //drppro2.DataBind();
                //drppro3.DataBind();
                //drppro4.DataBind();
                //drppro5.DataBind();
                //drppro6.DataBind();
                //drppro7.DataBind();
                //drppro8.DataBind();
                //drppro9.DataBind();
                //drpNxtpro.DataBind();
            }
             
        }
        catch { }
    }

    void bind_task_id()
    {
        try
        {
            //==========================================================================================
            //sql = "select task_id from tasks where deleted <> 1 order by task_id";

            //DataSet ds = DatabaseHelper.getDataset(sql);

            DataSet ds = DatabaseHelper.getTaskIdDataset();
            //==========================================================================================

            if (ds != null && ds.Tables.Count > 0)
            {
                DataRow row = ds.Tables[0].NewRow();
                ds.Tables[0].Rows.InsertAt(row, 0);

                //drpTask0.DataSource = ds.Tables[0];
                //drpTask1.DataSource = ds.Tables[0];
               //drpTask2.DataSource = ds.Tables[0];
                //drpTask3.DataSource = ds.Tables[0];
                //drpTask4.DataSource = ds.Tables[0];
                //drpTask5.DataSource = ds.Tables[0];
                //drpTask6.DataSource = ds.Tables[0];
                //drpTask7.DataSource = ds.Tables[0];
                //drpTask8.DataSource = ds.Tables[0];
                //drpTask9.DataSource = ds.Tables[0];
                //drpNxtTask.DataSource = ds.Tables[0];
                //drpTask0.DataTextField = "task_id";
                //drpTask0.DataValueField = "task_id";
                //drpTask1.DataTextField = "task_id";
                //drpTask1.DataValueField = "task_id";
                //drpTask2.DataTextField = "task_id";
                //drpTask2.DataValueField = "task_id";
                //drpTask3.DataTextField = "task_id";
                //drpTask3.DataValueField = "task_id";
                //drpTask4.DataTextField = "task_id";
                //drpTask4.DataValueField = "task_id";
                //drpTask5.DataTextField = "task_id";
                //drpTask5.DataValueField = "task_id";
                //drpTask6.DataTextField = "task_id";
                //drpTask6.DataValueField = "task_id";
                //drpTask7.DataTextField = "task_id";
                //drpTask7.DataValueField = "task_id";
                //drpTask8.DataTextField = "task_id";
                //drpTask8.DataValueField = "task_id";
                //drpTask9.DataTextField = "task_id";
                //drpTask9.DataValueField = "task_id";
                //drpNxtTask.DataTextField = "task_id";
                //drpNxtTask.DataValueField = "task_id";
                //drpTask0.DataBind();
                //drpTask1.DataBind();
                //drpTask2.DataBind();
                //drpTask3.DataBind();
                //drpTask4.DataBind();
                //drpTask5.DataBind();
                //drpTask6.DataBind();
                //drpTask7.DataBind();
                //drpTask8.DataBind();
                //drpTask9.DataBind();
                //drpNxtTask.DataBind();
            }
        }
        catch { }
    }

    void addToTaksComments()
    {

    }

    private string IOtimeGenerateHTML4HR()
    {
        object objUserName = DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + Session["admin"].ToString() + "'");

        string strHtml = "";
        strHtml += "<br>" + "Hi," + "<br>";
        strHtml += "Employee name: " + objUserName.ToString() + "<br>";
        strHtml += "Date: " + Request.Form["callbackdate"].ToString() + "<br>";
        string timein = drpInHr.Items[drpInHr.SelectedIndex].Text + drpInMin.Items[drpInMin.SelectedIndex].Text + drpInTime.Value;
        string timeout = drpOutHr.Items[drpOutHr.SelectedIndex].Text + drpOutMin.Items[drpOutMin.SelectedIndex].Text + drpOutTime.Value;
        Intime = DateTime.Parse(timein);
        outtime = DateTime.Parse(timeout);
        TimeSpan timediff = outtime.Subtract(Intime);
        strHtml += "<b>" + "In time: " + "</b>" + drpInHr.Items[drpInHr.SelectedIndex].Text + drpInMin.Items[drpInMin.SelectedIndex].Text + drpInTime.Value + "<br/>" + "<b>" + "Out time: " + "</b>" + drpOutHr.Items[drpOutHr.SelectedIndex].Text + drpOutMin.Items[drpOutMin.SelectedIndex].Text + drpOutTime.Value + "<br>" + "<b>" + "Total hours: " + "</b>" + timediff.ToString() + "<br/><br/>";
        strHtml += "Thanks & regards" + "<br/>";
        strHtml += objUserName.ToString();
        return strHtml;
    }

    private string EodcGenerateHTML()
    {
        string strHtml = "";

        object objUserName = DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + Session["admin"].ToString() + "'");

        strHtml += "<b>Eodc:</b> " + objUserName.ToString() + "  <b>Date:</b>" + " " + Request.Form["callbackdate"] + " " + Request.Form["txttime"].ToString() + " " + "  <b>Tasks:</b>(" + drpNoOfTask.SelectedValue + ")";

        strHtml += "<br><br>";

        strHtml += "<table width=\"95%\" cellpadding=\"1\">";
        int tottask = Int16.Parse(drpNoOfTask.SelectedValue);
        string hrspent = "";
        for (int j = 0; j < tottask; j++)
        {
            if (Request.Form["txthorSpent" + j].ToString() != "0")
            {
                if (Request.Form["txthrSpent" + j].ToString() != "0")
                {
                    hrspent = Request.Form["txthorSpent" + j].ToString() + " hours" + " " + Request.Form["txthrSpent" + j].ToString() + " minutes";
                }
                else
                {
                    hrspent = Request.Form["txthorSpent" + j].ToString() + " hours";
                }
            }
            else
            {
                if (Request.Form["txthrSpent" + j].ToString() != "0")
                {
                    hrspent = Request.Form["txthrSpent" + j].ToString() + " minutes";
                }
            }

            strHtml += "<tr><td><div style=\"border: 1px solid black;padding:5px;\">";
            strHtml += "<b>Project:</b> " + Request.Form["drppro" + j].ToString() + " (" + hrspent + ")<br>";
            strHtml += "<b>Description:</b> " + Request.Form["txtdesc" + j].ToString() + "<br>";

            if (Request.Form["drpTask" + j].ToString() != "")
            {
                strHtml += "<b>esgDesk link:</b> " + ConfigurationManager.AppSettings["TCRTaskLink"].ToString() + Request.Form["drpTask" + j].ToString() + "<br>";
            }
            strHtml += "<b>Project url:</b> " + Request.Form["txtproUrl" + j].ToString() + "<br>";

            strHtml += "<b>Other comments:</b> " + Request.Form["txtcomment" + j].ToString() + "<br>";

            strHtml += "</div></td></tr>";
        }

        if (txttaskHrsETC.Text.Trim() != "0")
        {
            if (txttaskETC.Text.Trim() != "0")
            {
                hrspent = txttaskHrsETC.Text.Trim() + " hours" + " " + txttaskETC.Text.Trim() + " minutes";
            }
            else
            {
                hrspent = txttaskHrsETC.Text.Trim() + " hours";
            }
        }
        else
        {
            if (txttaskETC.Text.Trim() != "0")
            {
                hrspent = txttaskETC.Text.Trim() + " minutes";
            }
            else
            {
                hrspent = txttaskHrsETC.Text.Trim() + " hours" + " " + txttaskETC.Text.Trim() + " minutes";
            }
        }

        strHtml += "<tr><td><div style=\"border: 1px solid black;padding:5px;\"><strong>Details of next task</strong><br><br>";
        strHtml += "<b>Project:</b> " + Request.Form["drpNxtpro"].ToString() + " (" + hrspent + ")<br>";
        strHtml += "<b>Description:</b> " + TxtNextDesc.Text.Trim() + "<br>";

        if (Request.Form["drpNxtTask"] != null)
        //if (Request.Form["drpNxtTask"].ToString() != "")
        {
            strHtml += "<b>esgDesk link:</b> " + ConfigurationManager.AppSettings["TCRTaskLink"].ToString() + Request.Form["drpNxtTask"].ToString() + "<br>";
        }
        strHtml += "<b>Project url:</b> " + txtProjecturl.Text.Trim() + "<br>";

        strHtml += "<b>Other comments:</b> " + txtTaskcomment.Text.Trim() + "<br>";

        strHtml += "</div></td></tr>";

        strHtml += "</table><br>";

        if (drpTaskUpdate.SelectedValue == "Yes")
        {
            //==================================================================
            strHtml += "Checked and updated all tasks in ESG Desk?: Yes<br><br>";
            //==================================================================
        }
        else
        {   //================================================================  
             strHtml += "Checked and updated all tasks in ESG Desk?:  No <br/>";
            //================================================================
            strHtml += "Please explain/give details:  " + txtExpainUpdate.Text + "<br><br>";
        }

        if (drpNextdayTask.SelectedValue == "Yes")
        {
            strHtml += "Tasks for the next day for at least the first 4 hours?: Yes <br><br>";
        }
        else
        {
            strHtml += "Tasks for the next day for at least the first 4 hours?: <font color=\"#ff0000\" size=\"6\"><b>No</b></font><br/>";
            strHtml += "Please explain/give details:  " + txtExpainNextTask.Text + "<br><br>";
        }

        string timein = drpInHr.Items[drpInHr.SelectedIndex].Text + drpInMin.Items[drpInMin.SelectedIndex].Text + drpInTime.Value;
        string timeout = drpOutHr.Items[drpOutHr.SelectedIndex].Text + drpOutMin.Items[drpOutMin.SelectedIndex].Text + drpOutTime.Value;
        Intime = DateTime.Parse(timein);
        outtime = DateTime.Parse(timeout);
        TimeSpan timediff = outtime.Subtract(Intime);

        strHtml += "<b>" + "In: " + "</b>" + drpInHr.Items[drpInHr.SelectedIndex].Text + drpInMin.Items[drpInMin.SelectedIndex].Text + drpInTime.Value + "<b>" + "  Out: " + "</b>" + drpOutHr.Items[drpOutHr.SelectedIndex].Text + drpOutMin.Items[drpOutMin.SelectedIndex].Text + drpOutTime.Value + "= " + timediff.ToString() + "<br/><br/>";
        strHtml += "Thanks & regards" + "<br/>";
        strHtml += Session["admin"];
        return strHtml;
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        //try
        //{
            // drpTask9.SelectedItem.Text = "";
            //if (drpNxtpro.SelectedItem.Text != "Select" && drpNxtTask.SelectedItem.Text != "")
            //{

        //if (Request.Form["drpNxtTask"].ToString() != "" || Request.Form["drpNxtTask"].ToString() != null)
        //    {

                string timein = drpInHr.Items[drpInHr.SelectedIndex].Text + drpInMin.Items[drpInMin.SelectedIndex].Text + drpInTime.Value;
                string timeout = drpOutHr.Items[drpOutHr.SelectedIndex].Text + drpOutMin.Items[drpOutMin.SelectedIndex].Text + drpOutTime.Value;
                Intime = DateTime.Parse(timein);
                outtime = DateTime.Parse(timeout);
                TimeSpan timediff = outtime.Subtract(Intime);

                string Eodcdat = DateTime.Parse(Request.Form["callbackdate"].ToString(), System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat).ToString("dd/MM/yyyy");

                sql = "SELECT count(*) from tblEmp where [username]='" + Session["admin"].ToString().Replace("'", "''")
                                  + "' And [EodcDate]= '" + Eodcdat + "'";

                object objEodc = DatabaseHelper.executeScalar(sql);
                

                if (objEodc.ToString() != "0")
                {
                    lblMessage.Text = "You have already sent eodc of this date";

                }
                else
                {

                    sql = @"DECLARE @eid INT;";
                    sql += " INSERT INTO [tblEmp] (username,EodcDate,EodcTime,NoOfTask,TimeIn,TimeOut,TotalHrs)";
                    sql += "VALUES ('" + Session["admin"].ToString().Replace("'", "''") + "',";
                    sql += "'" + Eodcdat + "',";
                    sql += "'" + Request.Form["txttime"].ToString() + "',";
                    sql += "" + drpNoOfTask.SelectedValue + ",";
                    sql += "'" + Intime.ToString() + "',";
                    sql += "'" + outtime.ToString() + "',";
                    sql += "'" + timediff.ToString() + "'";
                    sql += "); ";

                    sql += " SET @eid = SCOPE_IDENTITY(); ";
                    sql += " select @eid;";

                    object objResult = DatabaseHelper.executeScalar(sql);


                    if (objResult.ToString() != "0")
                    {
                        int tottask = Int16.Parse(drpNoOfTask.SelectedValue);

                        for (int j = 0; j < tottask; j++)
                        {
                            string hrspent = "";
                            string minspent = "";

                            hrspent = Request.Form["txthorSpent" + j].ToString();
                            minspent = Request.Form["txthrSpent" + j].ToString();

                            string project = Request.Form["drppro" + j].ToString();
                            string task_id = Request.Form["drpTask" + j].ToString();
                            string prourl = Request.Form["txtproUrl" + j].ToString();
                            string comment = Request.Form["txtcomment" + j].ToString();


                            sql = "INSERT INTO  [Eodc_Details] ([eid],[HrsSpent],[MinSpent],[project],[task_id],[ProjectUrl],[OtherComment])";
                            sql += " VALUES (" + objResult.ToString() + ",";
                            sql += "'" + hrspent + "',";
                            sql += "'" + minspent + "',";
                            sql += "'" + project.Replace("'", "''") + "',";
                            sql += "'" + task_id + "',";
                            sql += "'" + prourl.Replace("'", "''") + "',";
                            sql += "'" + comment.Replace("'", "''") + "');";

                            object objResult2 = DatabaseHelper.executeNonQuery(sql);


                            try
                            {
                                if (comment.ToString().Trim() != "" && task_id != "")
                                {
                                    sql = " insert into task_comments([task_id],[username],[post_date],[comment],[deleted],[qflag]) ";
                                    sql += " values(" + task_id + ",";
                                    sql += "'" + Session["admin"].ToString().Replace("'", "''") + "',";
                                    sql += "getdate(),";
                                    sql += "'" + "<span style=\"color:#841417;font-weight:bold;\">From EODC:</span> " + comment.ToString().Trim().Replace("'", "''") + "',";
                                    sql += "0,0)";

                                    objResult2 = DatabaseHelper.executeNonQuery(sql);
                                }
                            }
                            catch { }
                        }

                        bool RetMail1 = true;
                        bool RetMail2;
                        bool RetMail3;
                        string strBody = "";
                        string strBody4HR = "";
                        object email = DatabaseHelper.executeScalar("select email from users where username='" + Session["admin"].ToString() + "'");

                        strBody = EodcGenerateHTML();
                        strBody4HR = IOtimeGenerateHTML4HR();

                        RetMail1 = DatabaseHelper.sendEmailEODC("michaelr@estatesolutions.co.uk", email.ToString(), "EODC", strBody, 1);
                        RetMail2 = DatabaseHelper.sendEmailEODC(email.ToString(), email.ToString(), "A copy EODC ", strBody, 0);
                        RetMail2 = DatabaseHelper.sendEmailEODC("ritub@123sitesolutions.com", email.ToString(), "In time and out time from   " + Session["admin"].ToString(), strBody4HR, 0);
                        RetMail3 = DatabaseHelper.sendEmailEODC("priyab@123sitesolutions.com", email.ToString(), "A copy EODC ", strBody, 0);

                        if (RetMail1 == true)
                        {
                            try
                            {
                                sql = @"select top(1) task_id from tasks where deleted <> 1  and status = 'currently working on'
                                    and  assigned_to_user = '" + Session["admin"].ToString().Replace("'", "''") + "' order by last_updated_user";

                                object objTaskId = DatabaseHelper.executeScalar(sql);

                                if (objTaskId != null)
                                {

                                    sql = @"update hours_reporting set finished_date = getdate(), minutes = datediff(minute, started_date, getdate()) 
			                        where task_id = " + objTaskId.ToString() + "  and finished_date is null";

                                    int intResult = DatabaseHelper.executeNonQuery(sql);
                                }
                            }
                            catch { }

                            lblMessage.Text = "Your EODC have been sent.";
                        }
                        else
                        {
                            lblMessage.Text = "There was an error in sending mail";
                        }
                    }
                }
                //}
                //else
                //{
                //    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Please Select the next day task and then send ur EODC');", true);
                //}
           // }//removed Next Task Mandatory condition
        //}
        //catch
        //{
        //    lblMessage.Text = "An error occured in the processing of your request. Please try again later.";
        //}

    }

    protected void drppro0_SelectedIndexChanged(object sender, EventArgs e)
    {
        //drpTask0.Items.Insert(0, new ListItem("-Please Select-", "-1"));

        string project = drppro0.SelectedValue;

        //==========================================================================================================
        //sql = "select task_id from tasks where deleted <>1 and project = '" + project + "' order by task_id";
        //DataSet dstask = DatabaseHelper.getDataset(sql);
        DataSet dstask = DatabaseHelper.getDynamicTaskIdDs(project);
        //=========================================================================================================
        

       if (dstask != null && dstask.Tables.Count > 0)
        {
            DataRow row = dstask.Tables[0].NewRow();

            dstask.Tables[0].Rows.InsertAt(row, 0);
            drpTask0.DataSource = dstask.Tables[0];
            drpTask0.DataTextField = "task_id";
            drpTask0.DataValueField = "task_id";
            drpTask0.DataBind();
        }
        
    }
    protected void drppro1_SelectedIndexChanged(object sender, EventArgs e)
    {
        string project = drppro1.SelectedValue;

        //==========================================================================================================
        //sql = "select task_id from tasks where deleted <>1 and project = '" + project + "' order by task_id";
        //DataSet dstask = DatabaseHelper.getDataset(sql);
        DataSet dstask = DatabaseHelper.getDynamicTaskIdDs(project);
        //==========================================================================================================


        if (dstask != null && dstask.Tables.Count > 0)
        {
            DataRow row = dstask.Tables[0].NewRow();
            // row["bg_short_desc"] = "Select";
            /// row["ID"] = "All";
            dstask.Tables[0].Rows.InsertAt(row, 0);
            drpTask1.DataSource = dstask.Tables[0];
            drpTask1.DataTextField = "task_id";
            drpTask1.DataValueField = "task_id";
            drpTask1.DataBind();
        }

    }
    protected void drppro2_SelectedIndexChanged(object sender, EventArgs e)
    {
        string project = drppro2.SelectedValue;
        //=====================================================================================================
        //sql = "select task_id from tasks where  deleted <>1 and project = '" + project + "' order by task_id";
        //DataSet dstask = DatabaseHelper.getDataset(sql);
        DataSet dstask = DatabaseHelper.getDynamicTaskIdDs(project);
        //==========================================================================================================

        if (dstask != null && dstask.Tables.Count > 0)
        {
            DataRow row = dstask.Tables[0].NewRow();
            ///row["bg_short_desc"] = "Select";
            /// row["ID"] = "All";
            dstask.Tables[0].Rows.InsertAt(row, 0);
            drpTask2.DataSource = dstask.Tables[0];
            drpTask2.DataTextField = "task_id";
            drpTask2.DataValueField = "task_id";
            drpTask2.DataBind();
        }
    }
    protected void drppro3_SelectedIndexChanged(object sender, EventArgs e)
    {
        string project = drppro3.SelectedValue;

        
        
        //=====================================================================================================
        //sql = "select task_id from tasks where  deleted <>1 and  project = '" + project + "' order by task_id";
        //DataSet dstask = DatabaseHelper.getDataset(sql);
        DataSet dstask = DatabaseHelper.getDynamicTaskIdDs(project);
        //==========================================================================================================



        if (dstask != null && dstask.Tables.Count > 0)
        {
            DataRow row = dstask.Tables[0].NewRow();
            /// row["bg_short_desc"] = "Select";
            /// row["ID"] = "All";
            dstask.Tables[0].Rows.InsertAt(row, 0);
            drpTask3.DataSource = dstask.Tables[0];
            drpTask3.DataTextField = "task_id";
            drpTask3.DataValueField = "task_id";
            drpTask3.DataBind();
        }
    }
    protected void drppro4_SelectedIndexChanged(object sender, EventArgs e)
    {
        string project = drppro4.SelectedValue;
        
        //=====================================================================================================
        //sql = "select task_id from tasks where  deleted <>1 and  project = '" + project + "' order by task_id";
        //DataSet dstask = DatabaseHelper.getDataset(sql);
        DataSet dstask = DatabaseHelper.getDynamicTaskIdDs(project);
        //=====================================================================================================
        
        if (dstask != null && dstask.Tables.Count > 0)
        {
            DataRow row = dstask.Tables[0].NewRow();
            /// row["bg_short_desc"] = "Select";
            /// row["ID"] = "All";
            dstask.Tables[0].Rows.InsertAt(row, 0);
            drpTask4.DataSource = dstask.Tables[0];
            drpTask4.DataTextField = "task_id";
            drpTask4.DataValueField = "task_id";
            drpTask4.DataBind();
        }
    }
    protected void drppro5_SelectedIndexChanged(object sender, EventArgs e)
    {
        string project = drppro5.SelectedValue;


        //=====================================================================================================
        //sql = "select task_id from tasks where  deleted <>1 and project = '" + project + "' order by task_id";
        // DataSet dstask = DatabaseHelper.getDataset(sql);
        DataSet dstask = DatabaseHelper.getDynamicTaskIdDs(project);
        //=====================================================================================================

        if (dstask != null && dstask.Tables.Count > 0)
        {
            DataRow row = dstask.Tables[0].NewRow();
            ////row["bg_short_desc"] = "Select";
            /// row["ID"] = "All";
            dstask.Tables[0].Rows.InsertAt(row, 0);
            drpTask5.DataSource = dstask.Tables[0];
            drpTask5.DataTextField = "task_id";
            drpTask5.DataValueField = "task_id";
            drpTask5.DataBind();
        }
    }
    protected void drppro6_SelectedIndexChanged(object sender, EventArgs e)
    {
        string project = drppro6.SelectedValue;
        //=====================================================================================================
        //sql = "select task_id from tasks where  deleted <>1 and  project = '" + project + "' order by task_id";
        //DataSet dstask = DatabaseHelper.getDataset(sql);
        DataSet dstask = DatabaseHelper.getDynamicTaskIdDs(project);
        //======================================================================================================

        if (dstask != null && dstask.Tables.Count > 0)
        {

            DataRow row = dstask.Tables[0].NewRow();
            /// row["bg_short_desc"] = "Select";
            /// row["ID"] = "All";
            dstask.Tables[0].Rows.InsertAt(row, 0);
            drpTask6.DataSource = dstask.Tables[0];
            drpTask6.DataTextField = "task_id";
            drpTask6.DataValueField = "task_id";
            drpTask6.DataBind();
        }
    }
    protected void drppro7_SelectedIndexChanged(object sender, EventArgs e)
    {
        string project = drppro7.SelectedValue;

        //======================================================================================================
        //sql = "select task_id from tasks where  deleted <>1 and project = '" + project + "' order by task_id";
        //DataSet dstask = DatabaseHelper.getDataset(sql);
        DataSet dstask = DatabaseHelper.getDynamicTaskIdDs(project);
        //======================================================================================================
        if (dstask != null && dstask.Tables.Count > 0)
        {
            DataRow row = dstask.Tables[0].NewRow();
            ///row["bg_short_desc"] = "Select";
            /// row["ID"] = "All";
            dstask.Tables[0].Rows.InsertAt(row, 0);
            drpTask7.DataSource = dstask.Tables[0];
            drpTask7.DataTextField = "task_id";
            drpTask7.DataValueField = "task_id";
            drpTask7.DataBind();
        }
    }
    protected void drppro8_SelectedIndexChanged(object sender, EventArgs e)
    {
        string project = drppro8.SelectedValue;
        //======================================================================================================
        //sql = "select task_id from tasks where  deleted <>1 and  project = '" + project + "' order by task_id";
        //DataSet dstask = DatabaseHelper.getDataset(sql);
        DataSet dstask = DatabaseHelper.getDynamicTaskIdDs(project);
        //======================================================================================================

        if (dstask != null && dstask.Tables.Count > 0)
        {
            DataRow row = dstask.Tables[0].NewRow();
            //row["bg_short_desc"] = "Select";
            /// row["ID"] = "All";
            dstask.Tables[0].Rows.InsertAt(row, 0);
            drpTask8.DataSource = dstask.Tables[0];
            drpTask8.DataTextField = "task_id";
            drpTask8.DataValueField = "task_id";
            drpTask8.DataBind();
        }
    }
    protected void drppro9_SelectedIndexChanged(object sender, EventArgs e)
    {
        string project = drppro9.SelectedValue;
        
        //======================================================================================================
        //sql = "select task_id from tasks where  deleted <>1 and  project = '" + project + "' order by task_id";
        //DataSet dstask = DatabaseHelper.getDataset(sql);
        DataSet dstask = DatabaseHelper.getDynamicTaskIdDs(project);
        //======================================================================================================


        if (dstask != null && dstask.Tables.Count > 0)
        {

            DataRow row = dstask.Tables[0].NewRow();
            // row["bg_short_desc"] = "Select";
            /// row["ID"] = "All";
            dstask.Tables[0].Rows.InsertAt(row, 0);
            drpTask9.DataSource = dstask.Tables[0];
            drpTask9.DataTextField = "task_id";
            drpTask9.DataValueField = "task_id";
            drpTask9.DataBind();
        }
    }
    protected void drpNxtpro_SelectedIndexChanged(object sender, EventArgs e)
    {
        string project = drpNxtpro.SelectedValue;

        sql = "select task_id from tasks where  deleted <>1 and  project = '" + project + "' order by task_id";

        DataSet dstask = DatabaseHelper.getDataset(sql);
        //DataSet dstask = DatabaseHelper.getsp_gettaskdataset(sql);
        if (dstask != null && dstask.Tables.Count > 0)
        {

            DataRow row = dstask.Tables[0].NewRow();
            //row["bg_short_desc"] = "Select";
            /// row["ID"] = "All";
            dstask.Tables[0].Rows.InsertAt(row, 0);
            drpNxtTask.DataSource = dstask.Tables[0];
            drpNxtTask.DataTextField = "task_id";
            drpNxtTask.DataValueField = "task_id";
            drpNxtTask.DataBind();
        }
    }

    //protected void drpTask0_SelectedIndexChanged(object sender, EventArgs e)
    //{
    ////    string taskId = drpTask0.SelectedItem.ToString();

    ////    if (taskId != "")
    ////    {
    ////        //=============================================================================================
    ////        //sql = "SELECT [Relevant_URL],[short_desc] from tasks where [task_id]=" + taskId.ToString();
    ////        //DataSet ds = DatabaseHelper.getDataset(sql);
    ////        DataSet ds = DatabaseHelper.getDetailDataset(taskId);
    ////        //===============================================================================================
    ////        if (ds != null && ds.Tables[0].Rows.Count > 0)
    ////        {
    ////            txtproUrl0.Text = ds.Tables[0].Rows[0]["Relevant_URL"].ToString();
    ////            txtdesc0.Text = ds.Tables[0].Rows[0]["short_desc"].ToString();
    ////        }
    ////        else
    ////        {
    ////            txtproUrl0.Text = "";
    ////            txtdesc0.Text = "";
    ////        }
    ////    }
    ////    else
    ////    {
    ////        txtproUrl0.Text = "";
    ////        txtdesc0.Text = "";
    ////    }
    //}
    ////protected void drpTask1_SelectedIndexChanged(object sender, EventArgs e)
    ////{
    ////    string taskId = drpTask1.SelectedItem.ToString();

    ////    if (taskId != "")
    ////    {
    ////        //=============================================================================================
    ////        //sql = "SELECT [Relevant_URL],[short_desc] from tasks where [task_id]=" + taskId.ToString();
    ////        //DataSet ds = DatabaseHelper.getDataset(sql);
    ////        DataSet ds = DatabaseHelper.getDetailDataset(taskId);
    ////        //=============================================================================================

    ////        if (ds != null && ds.Tables[0].Rows.Count > 0)
    ////        {
    ////            txtproUrl1.Text = ds.Tables[0].Rows[0]["Relevant_URL"].ToString();
    ////            txtdesc1.Text = ds.Tables[0].Rows[0]["short_desc"].ToString();
    ////        }
    ////        else
    ////        {
    ////            txtproUrl1.Text = "";
    ////            txtdesc1.Text = "";
    ////        }
    ////    }
    ////    else
    ////    {
    ////        txtproUrl1.Text = "";
    ////        txtdesc1.Text = "";
    ////    }
    ////}
    ////protected void drpTask2_SelectedIndexChanged(object sender, EventArgs e)
    ////{
    ////    string taskId = drpTask2.SelectedItem.ToString();

    ////    if (taskId != "")
    ////    {
    ////        //=============================================================================================
    ////        //sql = "SELECT [Relevant_URL],[short_desc] from tasks where [task_id]=" + taskId.ToString();
    ////        //DataSet ds = DatabaseHelper.getDataset(sql);
    ////        DataSet ds = DatabaseHelper.getDetailDataset(taskId);

    ////        //=============================================================================================

            

    ////        if (ds != null && ds.Tables[0].Rows.Count > 0)
    ////        {
    ////            txtproUrl2.Text = ds.Tables[0].Rows[0]["Relevant_URL"].ToString();
    ////            txtdesc2.Text = ds.Tables[0].Rows[0]["short_desc"].ToString();
    ////        }
    ////        else
    ////        {
    ////            txtproUrl2.Text = "";
    ////            txtdesc2.Text = "";
    ////        }
    ////    }
    ////    else
    ////    {
    ////        txtproUrl2.Text = "";
    ////        txtdesc2.Text = "";
    ////    }
    ////}
    ////protected void drpTask3_SelectedIndexChanged(object sender, EventArgs e)
    ////{
    ////    string taskId = drpTask3.SelectedItem.ToString();

    ////    if (taskId != "")
    ////    {

            
            
    ////        //=============================================================================================
    ////        //sql = "SELECT [Relevant_URL],[short_desc] from tasks where [task_id]=" + taskId.ToString();
    ////        //DataSet ds = DatabaseHelper.getDataset(sql);
    ////        DataSet ds = DatabaseHelper.getDetailDataset(taskId);

    ////        //=============================================================================================

    ////        if (ds != null && ds.Tables[0].Rows.Count > 0)
    ////        {
    ////            txtproUrl3.Text = ds.Tables[0].Rows[0]["Relevant_URL"].ToString();
    ////            txtdesc3.Text = ds.Tables[0].Rows[0]["short_desc"].ToString();
    ////        }
    ////        else
    ////        {
    ////            txtproUrl3.Text = "";
    ////            txtdesc3.Text = "";
    ////        }
    ////    }
    ////    else
    ////    {
    ////        txtproUrl3.Text = "";
    ////        txtdesc3.Text = "";
    ////    }
    ////}
    ////protected void drpTask4_SelectedIndexChanged(object sender, EventArgs e)
    ////{
    ////    string taskId = drpTask4.SelectedItem.ToString();

    ////    if (taskId != "")
    ////    {
            

    ////        //DataSet ds = DatabaseHelper.getDataset(sql);
    ////        //=============================================================================================
    ////        //sql = "SELECT [Relevant_URL],[short_desc] from tasks where [task_id]=" + taskId.ToString();
    ////        //DataSet ds = DatabaseHelper.getDataset(sql);
    ////        DataSet ds = DatabaseHelper.getDetailDataset(taskId);

    ////        //=============================================================================================



    ////        if (ds != null && ds.Tables[0].Rows.Count > 0)
    ////        {
    ////            txtproUrl4.Text = ds.Tables[0].Rows[0]["Relevant_URL"].ToString();
    ////            txtdesc4.Text = ds.Tables[0].Rows[0]["short_desc"].ToString();
    ////        }
    ////        else
    ////        {
    ////            txtproUrl4.Text = "";
    ////            txtdesc4.Text = "";
    ////        }
    ////    }
    ////    else
    ////    {
    ////        txtproUrl4.Text = "";
    ////        txtdesc4.Text = "";
    ////    }
    ////}
    ////protected void drpTask5_SelectedIndexChanged(object sender, EventArgs e)
    ////{
    ////    string taskId = drpTask5.SelectedItem.ToString();

    ////    if (taskId != "")
    ////    {
            

            

    ////        //=============================================================================================
    ////        //sql = "SELECT [Relevant_URL],[short_desc] from tasks where [task_id]=" + taskId.ToString();
    ////        //DataSet ds = DatabaseHelper.getDataset(sql);
    ////        DataSet ds = DatabaseHelper.getDetailDataset(taskId);

    ////        //=============================================================================================



    ////        if (ds != null && ds.Tables[0].Rows.Count > 0)
    ////        {
    ////            txtproUrl5.Text = ds.Tables[0].Rows[0]["Relevant_URL"].ToString();
    ////            txtdesc5.Text = ds.Tables[0].Rows[0]["short_desc"].ToString();
    ////        }
    ////        else
    ////        {
    ////            txtproUrl5.Text = "";
    ////            txtdesc5.Text = "";
    ////        }
    ////    }
    ////    else
    ////    {
    ////        txtproUrl5.Text = "";
    ////        txtdesc5.Text = "";
    ////    }
    ////}
    ////protected void drpTask6_SelectedIndexChanged(object sender, EventArgs e)
    ////{
    ////    string taskId = drpTask6.SelectedItem.ToString();

    ////    if (taskId != "")
    ////    {
            

           

    ////        //=============================================================================================
    ////        //sql = "SELECT [Relevant_URL],[short_desc] from tasks where [task_id]=" + taskId.ToString();
    ////        //DataSet ds = DatabaseHelper.getDataset(sql);
    ////        DataSet ds = DatabaseHelper.getDetailDataset(taskId);

    ////        //=============================================================================================




    ////        if (ds != null && ds.Tables[0].Rows.Count > 0)
    ////        {
    ////            txtproUrl6.Text = ds.Tables[0].Rows[0]["Relevant_URL"].ToString();
    ////            txtdesc6.Text = ds.Tables[0].Rows[0]["short_desc"].ToString();
    ////        }
    ////        else
    ////        {
    ////            txtproUrl6.Text = "";
    ////            txtdesc6.Text = "";
    ////        }
    ////    }
    ////    else
    ////    {
    ////        txtproUrl6.Text = "";
    ////        txtdesc6.Text = "";
    ////    }
    ////}
    ////protected void drpTask7_SelectedIndexChanged(object sender, EventArgs e)
    ////{
    ////    string taskId = drpTask7.SelectedItem.ToString();

    ////    if (taskId != "")
    ////    {
            

            

    ////        //=============================================================================================
    ////        //sql = "SELECT [Relevant_URL],[short_desc] from tasks where [task_id]=" + taskId.ToString();
    ////        //DataSet ds = DatabaseHelper.getDataset(sql);
    ////        DataSet ds = DatabaseHelper.getDetailDataset(taskId);

    ////        //=============================================================================================



    ////        if (ds != null && ds.Tables[0].Rows.Count > 0)
    ////        {
    ////            txtproUrl7.Text = ds.Tables[0].Rows[0]["Relevant_URL"].ToString();
    ////            txtdesc7.Text = ds.Tables[0].Rows[0]["short_desc"].ToString();
    ////        }
    ////        else
    ////        {
    ////            txtproUrl7.Text = "";
    ////            txtdesc7.Text = "";
    ////        }
    ////    }
    ////    else
    ////    {
    ////        txtproUrl7.Text = "";
    ////        txtdesc7.Text = "";
    ////    }
    ////}
    ////protected void drpTask8_SelectedIndexChanged(object sender, EventArgs e)
    ////{
    ////    string taskId = drpTask8.SelectedItem.ToString();
    ////    if (taskId != "")
    ////    {
            
         

    ////        //=============================================================================================
    ////        //sql = "SELECT [Relevant_URL],[short_desc] from tasks where [task_id]=" + taskId.ToString();
    ////        //DataSet ds = DatabaseHelper.getDataset(sql);
    ////         DataSet ds = DatabaseHelper.getDetailDataset(taskId);
    ////        //=============================================================================================




    ////        if (ds != null && ds.Tables[0].Rows.Count > 0)
    ////        {
    ////            txtproUrl8.Text = ds.Tables[0].Rows[0]["Relevant_URL"].ToString();
    ////            txtdesc8.Text = ds.Tables[0].Rows[0]["short_desc"].ToString();
    ////        }
    ////        else
    ////        {
    ////            txtproUrl8.Text = "";
    ////            txtdesc8.Text = "";
    ////        }
    ////    }
    ////    else
    ////    {
    ////        txtproUrl8.Text = "";
    ////        txtdesc8.Text = "";
    ////    }
   // }
    ////protected void drpTask9_SelectedIndexChanged(object sender, EventArgs e)
    ////{
    ////    string taskId = drpTask9.SelectedItem.ToString();
    ////    if (taskId != "")
    ////    {
            

            

    ////        //=============================================================================================
    ////        //sql = "SELECT [Relevant_URL],[short_desc] from tasks where [task_id]=" + taskId.ToString();
    ////        //DataSet ds = DatabaseHelper.getDataset(sql);
    ////        DataSet ds = DatabaseHelper.getDetailDataset(taskId);

    ////        //=============================================================================================


    ////        if (ds != null && ds.Tables[0].Rows.Count > 0)
    ////        {
    ////            txtproUrl9.Text = ds.Tables[0].Rows[0]["Relevant_URL"].ToString();
    ////            txtdesc9.Text = ds.Tables[0].Rows[0]["short_desc"].ToString();
    ////        }
    ////        else
    ////        {
    ////            txtproUrl9.Text = "";
    ////            txtdesc9.Text = "";
    ////        }
    ////    }
    ////    else
    ////    {
    ////        txtproUrl9.Text = "";
    ////        txtdesc9.Text = "";
    ////    }
    ////}
    ////protected void drpNxtTask_SelectedIndexChanged(object sender, EventArgs e)
    ////{
    ////    string taskId = drpNxtTask.SelectedItem.ToString();
    ////    if (taskId != "")
    ////    {
    ////        sql = "SELECT [Relevant_URL],[short_desc] from tasks where [task_id]=" + taskId.ToString();

    ////        DataSet ds = DatabaseHelper.getDataset(sql);

    ////        if (ds != null && ds.Tables[0].Rows.Count > 0)
    ////        {
    ////            txtProjecturl.Text = ds.Tables[0].Rows[0]["Relevant_URL"].ToString();
    ////            TxtNextDesc.Text = ds.Tables[0].Rows[0]["short_desc"].ToString();
    ////        }
    ////        else
    ////        {
    ////            txtProjecturl.Text = "";
    ////            TxtNextDesc.Text = "";
    ////        }
    ////    }
    ////    else
    ////    {
    ////        txtProjecturl.Text = "";
    ////        TxtNextDesc.Text = "";
    ////    }
    ////}

}
