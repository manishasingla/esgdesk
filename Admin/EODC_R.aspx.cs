﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
using System.Web.Security;
using System.Globalization;

public partial class Admin_EODC_R : System.Web.UI.Page
{
    DateTime Intime;
    DateTime outtime;
    string sql = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        //========================================================================
        drppro0.Attributes.Add("onchange", "return GetDataViaAJAXA();");
        drpTask0.Attributes.Add("onchange", "return GetDataViaAJAXforDescA();");

        drppro1.Attributes.Add("onchange", "return GetDataViaAJAXB();");
        drpTask1.Attributes.Add("onchange", "return GetDataViaAJAXforDescB();");

        drppro2.Attributes.Add("onchange", "return GetDataViaAJAXC();");
        drpTask2.Attributes.Add("onchange", "return GetDataViaAJAXforDescC();");

        drppro3.Attributes.Add("onchange", "return GetDataViaAJAXD();");
        drpTask3.Attributes.Add("onchange", "return GetDataViaAJAXforDescD();");

        drppro4.Attributes.Add("onchange", "return GetDataViaAJAXE();");
        drpTask4.Attributes.Add("onchange", "return GetDataViaAJAXforDescE();");

        drppro5.Attributes.Add("onchange", "return GetDataViaAJAXF();");
        drpTask5.Attributes.Add("onchange", "return GetDataViaAJAXforDescF();");

        drppro6.Attributes.Add("onchange", "return GetDataViaAJAXG();");
        drpTask6.Attributes.Add("onchange", "return GetDataViaAJAXforDescG();");

        drppro7.Attributes.Add("onchange", "return GetDataViaAJAXH();");
        drpTask7.Attributes.Add("onchange", "return GetDataViaAJAXforDescH();");

        drppro8.Attributes.Add("onchange", "return GetDataViaAJAXI();");
        drpTask8.Attributes.Add("onchange", "return GetDataViaAJAXforDescI();");

        drppro9.Attributes.Add("onchange", "return GetDataViaAJAXJ();");
        drpTask9.Attributes.Add("onchange", "return GetDataViaAJAXforDescJ();");

        drpNxtpro.Attributes.Add("onchange", "return GetDataViaAJAXK();");
        drpNxtTask.Attributes.Add("onchange", "return GetDataViaAJAXforDescK();");

        txtDate.Attributes.Add("onchange", "return GetDataViaAJAXforDate();");
        //========================================================================

        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = "EODC.aspx";
            Response.Redirect("login.aspx");
            return;
        }

        sql = "select count(*) from tasks where assigned_to_user ='" + Session["admin"] + "' and deleted <> 1 and status <>'closed' and status = 'CWO' ";

        object count = DatabaseHelper.executeScalar(sql);

        if (count.ToString() != "0")
        {
            Session["cwo"] = "y";
            Response.Redirect("tasks.aspx");
            return;
        }
        else
        {
            Session["cwo"] = "n";
        }

        drpNoOfTask.Attributes.Add("onchange", "ShowNoOfTask();");
        drpTaskUpdate.Attributes.Add("onchange", "ShowDetail();");
        drpNextdayTask.Attributes.Add("onchange", "ShowNextTaskDetail();");
        txtExpainUpdate.Attributes.Add("onkeypress", "Enablebutton();");
        txtExpainNextTask.Attributes.Add("onkeypress", "Enablebutton();");
        txtExpainNextTask.Attributes.Add("onblur", "Enablebutton();");
        txtExpainUpdate.Attributes.Add("onblur", "Enablebutton();");

        if (!IsPostBack)
        {
            object objUserName = DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + Session["admin"].ToString() + "'");

            if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
            {
                lnkUserName.InnerHtml = "Logged in as emp: " + objUserName.ToString();
            }
            else
            {
                lnkUserName.InnerHtml = "Logged in as admin: " + objUserName.ToString();
            }
           
            BindallProject();

            //====================================
            txtDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

            object objIntime = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + Session["admin"].ToString() + "' and indate ='" + txtDate.Text.Trim() + "'");

            lblIntime.Text = Convert.ToString(objIntime);
            hdnIntime.Value = Convert.ToString(objIntime);

            string zoneId = "India Standard Time";
            TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById(zoneId);
            DateTime result = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, tzi);

            lblOutTime.Text = result.ToString("hh:mm tt");
            hdnOutTime.Value = result.ToString("hh:mm tt");

            //lblOutTime.Text = result.ToShortTimeString().ToString();
            //hdnOutTime.Value = result.ToShortTimeString().ToString();
        }
    }

    void BindallProject()
    {
        DataSet dsProject = new DataSet();
        dsProject = DatabaseHelper.getProjectDs();
        drppro0.DataSource = dsProject.Tables[0];
        drppro1.DataSource = dsProject.Tables[0];
        drppro2.DataSource = dsProject.Tables[0];
        drppro3.DataSource = dsProject.Tables[0];
        drppro4.DataSource = dsProject.Tables[0];
        drppro5.DataSource = dsProject.Tables[0];
        drppro6.DataSource = dsProject.Tables[0];
        drppro7.DataSource = dsProject.Tables[0];
        drppro8.DataSource = dsProject.Tables[0];
        drppro9.DataSource = dsProject.Tables[0];
        drpNxtpro.DataSource = dsProject.Tables[0];

        drppro0.DataTextField = "project_name";
        drppro0.DataValueField = "project_name";
        drppro1.DataTextField = "project_name";
        drppro1.DataValueField = "project_name";
        drppro2.DataTextField = "project_name";
        drppro2.DataValueField = "project_name";
        drppro3.DataTextField = "project_name";
        drppro3.DataValueField = "project_name";
        drppro4.DataTextField = "project_name";
        drppro4.DataValueField = "project_name";
        drppro5.DataTextField = "project_name";
        drppro5.DataValueField = "project_name";
        drppro6.DataTextField = "project_name";
        drppro6.DataValueField = "project_name";
        drppro7.DataTextField = "project_name";
        drppro7.DataValueField = "project_name";
        drppro8.DataTextField = "project_name";
        drppro8.DataValueField = "project_name";
        drppro9.DataTextField = "project_name";
        drppro9.DataValueField = "project_name";
        drpNxtpro.DataTextField = "project_name";
        drpNxtpro.DataValueField = "project_name";
        drppro0.DataBind();
        drppro1.DataBind();
        drppro2.DataBind();
        drppro3.DataBind();
        drppro4.DataBind();
        drppro5.DataBind();
        drppro6.DataBind();
        drppro7.DataBind();
        drppro8.DataBind();
        drppro9.DataBind();
        drpNxtpro.DataBind();

    }

    void bind_project_name()
    {
        try
        {
            //==============================================================================================
            DataSet ds = DatabaseHelper.getProjectDataset();

            //==========================================================================================         
            if (ds != null && ds.Tables.Count > 0)
            {
                DataRow row = ds.Tables[0].NewRow();
                row["project_name"] = "Select";
                ds.Tables[0].Rows.InsertAt(row, 0);
            }
        }
        catch { }
    }

    void bind_task_id()
    {
        try
        {
            //==========================================================================================
            DataSet ds = DatabaseHelper.getTaskIdDataset();
            //==========================================================================================

            if (ds != null && ds.Tables.Count > 0)
            {
                DataRow row = ds.Tables[0].NewRow();
                ds.Tables[0].Rows.InsertAt(row, 0);
            }
        }
        catch { }
    }

    void addToTaksComments()
    { }

    private string IOtimeGenerateHTML4HR()
    {
        object objUserName = DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + Session["admin"].ToString() + "'");

        string strHtml = "";
        strHtml += "<br>" + "Hi," + "<br>";
        strHtml += "Employee name: " + objUserName.ToString() + "<br>";
        strHtml += "Date: " + txtDate.Text.Trim() + "<br>";
        
        string format = "dd/MM/yyyy hh:mm tt";
        string timein = txtDate.Text.Trim() + " " + lblIntime.Text.Trim();
        string timeout = txtDate.Text.Trim() + " " + lblOutTime.Text.Trim();

        //Intime = DateTime.Parse(timein);
        //outtime = DateTime.Parse(timeout);
        Intime = DateTime.ParseExact(timein, format, CultureInfo.InvariantCulture);
        outtime = DateTime.ParseExact(timeout, format, CultureInfo.InvariantCulture);
        TimeSpan timediff = outtime.Subtract(Intime);

        strHtml += "<b>" + "In time: " + "</b>" + lblIntime.Text.Trim() + "<br/>" + "<b>" + "Out time: " + "</b>" + lblOutTime.Text.Trim() + "<br>" + "<b>" + "Total hours: " + "</b>" + timediff.ToString() + "<br/><br/>";
        strHtml += "Thanks & regards" + "<br/>";
        strHtml += objUserName.ToString();
        return strHtml;
    }

    private string EodcGenerateHTML()
    {
        string strHtml = "";
        object objUserName = DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + Session["admin"].ToString() + "'");

        strHtml += "<b>Eodc:</b> " + objUserName.ToString() + "  <b>Date:</b>" + " " + txtDate.Text.Trim() + " " + Request.Form["txttime"].ToString() + " " + "  <b>Tasks:</b>(" + drpNoOfTask.SelectedValue + ")";
        strHtml += "<br><br>";

        strHtml += "<table width=\"95%\" cellpadding=\"1\">";
        int tottask = Int16.Parse(drpNoOfTask.SelectedValue);
        string hrspent = "";
        for (int j = 0; j < tottask; j++)
        {
            if (Request.Form["txthorSpent" + j].ToString() != "0")
            {
                if (Request.Form["txthrSpent" + j].ToString() != "0")
                {
                    hrspent = Request.Form["txthorSpent" + j].ToString() + " hours" + " " + Request.Form["txthrSpent" + j].ToString() + " minutes";
                }
                else
                {
                    hrspent = Request.Form["txthorSpent" + j].ToString() + " hours";
                }
            }
            else
            {
                if (Request.Form["txthrSpent" + j].ToString() != "0")
                {
                    hrspent = Request.Form["txthrSpent" + j].ToString() + " minutes";
                }
            }

            strHtml += "<tr><td><div style=\"border: 1px solid black;padding:5px;\">";
            strHtml += "<b>Project:</b> " + Request.Form["drppro" + j].ToString() + " (" + hrspent + ")<br>";
            strHtml += "<b>Description:</b> " + Request.Form["txtdesc" + j].ToString() + "<br>";

            if (Request.Form["drpTask" + j].ToString() != "")
            {
                strHtml += "<b>esgDesk link:</b> " + ConfigurationManager.AppSettings["TCRTaskLink"].ToString() + Request.Form["drpTask" + j].ToString() + "<br>";
            }
            strHtml += "<b>Project url:</b> " + Request.Form["txtproUrl" + j].ToString() + "<br>";

            strHtml += "<b>Other comments:</b> " + Request.Form["txtcomment" + j].ToString() + "<br>";

            strHtml += "</div></td></tr>";
        }

        if (txttaskHrsETC.Text.Trim() != "0")
        {
            if (txttaskETC.Text.Trim() != "0")
            {
                hrspent = txttaskHrsETC.Text.Trim() + " hours" + " " + txttaskETC.Text.Trim() + " minutes";
            }
            else
            {
                hrspent = txttaskHrsETC.Text.Trim() + " hours";
            }
        }
        else
        {
            if (txttaskETC.Text.Trim() != "0")
            {
                hrspent = txttaskETC.Text.Trim() + " minutes";
            }
            else
            {
                hrspent = txttaskHrsETC.Text.Trim() + " hours" + " " + txttaskETC.Text.Trim() + " minutes";
            }
        }

        strHtml += "<tr><td><div style=\"border: 1px solid black;padding:5px;\"><strong>Details of next task</strong><br><br>";
        //=======================================================================================================================
          strHtml += "<b>Project:</b> " + Request.Form["drpNxtpro"].ToString() + " (" + hrspent + ")<br>";
        //=======================================================================================================================
        strHtml += "<b>Description:</b> " + TxtNextDesc.Text.Trim() + "<br>";

        if (Request.Form["drpNxtTask"] != null)
        //if (Request.Form["drpNxtTask"].ToString() != "")
        {
            strHtml += "<b>esgDesk link:</b> " + ConfigurationManager.AppSettings["TCRTaskLink"].ToString() + Request.Form["drpNxtTask"].ToString() + "<br>";
        }
        strHtml += "<b>Project url:</b> " + txtProjecturl.Text.Trim() + "<br>";
        //*********************************************************************************
        //strHtml += "<b>Other comments:</b> " + txtTaskcomment.Text.Trim() + "<br>";
        strHtml += "<b>Other comments:</b> " + txtTaskcomment.Content.Trim() + "<br>";
        //*********************************************************************************
        strHtml += "</div></td></tr>";

        strHtml += "</table><br>";

        if (drpTaskUpdate.SelectedValue == "Yes")
        {
            //==================================================================
              strHtml += "Checked and updated all tasks in ESG Desk?: Yes<br><br>";
            //==================================================================
        }
        else
        {
            //================================================================  
              strHtml += "Checked and updated all tasks in ESG Desk?:  No <br/>";
            //================================================================
              strHtml += "Please explain/give details:  " + txtExpainUpdate.Text + "<br><br>";
        }

        if (drpNextdayTask.SelectedValue == "Yes")
        {
            strHtml += "Tasks for the next day for at least the first 4 hours?: Yes <br><br>";
        }
        else
        {
            strHtml += "Tasks for the next day for at least the first 4 hours?: <font color=\"#ff0000\" size=\"6\"><b>No</b></font><br/>";
            strHtml += "Please explain/give details:  " + txtExpainNextTask.Text + "<br><br>";
        }

        string format = "dd/MM/yyyy hh:mm tt";
        string timein = txtDate.Text.Trim() + " " + lblIntime.Text.Trim();
        string timeout = txtDate.Text.Trim() + " " + lblOutTime.Text.Trim();
        //Intime = DateTime.Parse(timein);
        //outtime = DateTime.Parse(timeout);
        Intime = DateTime.ParseExact(timein, format, CultureInfo.InvariantCulture);
        outtime = DateTime.ParseExact(timeout, format, CultureInfo.InvariantCulture);
        TimeSpan timediff = outtime.Subtract(Intime);

        strHtml += "<b>" + "In: " + "</b>" + lblIntime.Text.Trim() + "<b>" + "  Out: " + "</b>" + lblOutTime.Text.Trim() + "= " + timediff.ToString() + "<br/><br/>";
        strHtml += "Thanks & regards" + "<br/>";
        strHtml += Session["admin"];
        return strHtml;
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            lblIntime.Text = hdnIntime.Value;
            lblOutTime.Text = hdnOutTime.Value;

           // if (Request.Form["drpNxtTask"].ToString() != "" || Request.Form["drpNxtTask"].ToString() != null)
           // {
                string format = "dd/MM/yyyy hh:mm tt";
                string timein = txtDate.Text.Trim() + " " + lblIntime.Text.Trim();
                string timeout = txtDate.Text.Trim() + " " + lblOutTime.Text.Trim();
                TimeSpan timediff;
                try
                {
                    //Intime = DateTime.Parse(timein);
                    //outtime = DateTime.Parse(timeout);
                    Intime = DateTime.ParseExact(timein, format, CultureInfo.InvariantCulture);
                    outtime = DateTime.ParseExact(timeout, format, CultureInfo.InvariantCulture);
                    timediff = outtime.Subtract(Intime);
                }
                catch (Exception ex)
                {
                    lblMessage.Text = "Datetime Error: Intime: "+timein + " OutTime: "+timeout;
                    HandleException(ex, "Project: esgDesk; Form: EODC3; Button1_click event timein error: ", timein +" Out: " + timeout);
                    return;
                }
                string Eodcdat = txtDate.Text.Trim();
                sql = "SELECT count(*) from tblEmp where [username]='" + Session["admin"].ToString().Replace("'", "''")
                                  + "' And [EodcDate]= '" + Eodcdat + "'";

                object objEodc = DatabaseHelper.executeScalar(sql);

                if (objEodc.ToString() != "0")
                {
                    lblMessage.Text = "You have already sent eodc of this date";
                }
                else
                {
                    sql = @"DECLARE @eid INT;";
                    sql += " INSERT INTO [tblEmp] (username,EodcDate,EodcTime,NoOfTask,TimeIn,TimeOut,TotalHrs)";
                    sql += "VALUES ('" + Session["admin"].ToString().Replace("'", "''") + "',";
                    sql += "'" + Eodcdat + "',";
                    sql += "'" + Request.Form["txttime"].ToString() + "',";
                    sql += "" + drpNoOfTask.SelectedValue + ",";
                    sql += "'" + Intime.ToString() + "',";
                    sql += "'" + outtime.ToString() + "',";
                    sql += "'" + timediff.ToString() + "'";
                    sql += "); ";
                    sql += " SET @eid = SCOPE_IDENTITY(); ";
                    sql += " select @eid;";

                    object objResult = DatabaseHelper.executeScalar(sql);

                    if (objResult.ToString() != "0")
                    {
                        int tottask = Int16.Parse(drpNoOfTask.SelectedValue);

                        for (int j = 0; j < tottask; j++)
                        {
                            string hrspent = "";
                            string minspent = "";

                            hrspent = Request.Form["txthorSpent" + j].ToString();
                            minspent = Request.Form["txthrSpent" + j].ToString();

                            string project = Request.Form["drppro" + j].ToString();
                            string task_id = Request.Form["drpTask" + j].ToString();
                            string prourl = Request.Form["txtproUrl" + j].ToString();
                            string comment = Request.Form["txtcomment" + j].ToString();

                            sql = "INSERT INTO  [Eodc_Details] ([eid],[HrsSpent],[MinSpent],[project],[task_id],[ProjectUrl],[OtherComment])";
                            sql += " VALUES (" + objResult.ToString() + ",";
                            sql += "'" + hrspent + "',";
                            sql += "'" + minspent + "',";
                            sql += "'" + project.Replace("'", "''") + "',";
                            sql += "'" + task_id + "',";
                            sql += "'" + prourl.Replace("'", "''") + "',";
                            sql += "'" + comment.Replace("'", "''") + "');";

                            object objResult2 = DatabaseHelper.executeNonQuery(sql);
                          
                            try
                            {
                                if (comment.ToString().Trim() != "" && task_id != "")
                                {
                                    sql = " insert into task_comments([task_id],[username],[post_date],[comment],[deleted],[qflag]) ";
                                    sql += " values(" + task_id + ",";
                                    sql += "'" + Session["admin"].ToString().Replace("'", "''") + "',";
                                    sql += "getdate(),";
                                    sql += "'" + "<span style=\"color:#841417;font-weight:bold;\">From EODC:</span> " + comment.ToString().Trim().Replace("'", "''") + "',";
                                    sql += "0,0)";
                                    objResult2 = DatabaseHelper.executeNonQuery(sql);
                                }
                            }
                            catch { }
                        }

                        bool RetMail1 = true;
                        bool RetMail2;
                        bool RetMail3;
                        string strBody = "";
                        string strBody4HR = "";
                        object email = DatabaseHelper.executeScalar("select email from users where username='" + Session["admin"].ToString() + "'");

                        strBody = EodcGenerateHTML();
                        strBody4HR = IOtimeGenerateHTML4HR();

                        //RetMail1 = DatabaseHelper.sendEmailEODC("kirann@123sitesolutions.com", email.ToString(), "EODC", strBody, 1);
                        //RetMail2 = DatabaseHelper.sendEmailEODC("kirann@123sitesolutions.com", email.ToString(), "In time and out time from   " + Session["admin"].ToString(), strBody4HR, 0);

                        RetMail1 = DatabaseHelper.sendEmailEODC("michaelr@estatesolutions.co.uk", email.ToString(), "EODC", strBody, 1);
                        RetMail2 = DatabaseHelper.sendEmailEODC(email.ToString(), email.ToString(), "A copy EODC ", strBody, 0);
                        RetMail2 = DatabaseHelper.sendEmailEODC("ritub@123sitesolutions.com", email.ToString(), "In time and out time from   " + Session["admin"].ToString(), strBody4HR, 0);
                        RetMail3 = DatabaseHelper.sendEmailEODC("priyab@123sitesolutions.com", email.ToString(), "A copy EODC ", strBody, 0);

                        if (RetMail1 == true)
                        {
                            try
                            {
                                sql = @"select top(1) task_id from tasks where deleted <> 1  and status = 'currently working on'
                                and  assigned_to_user = '" + Session["admin"].ToString().Replace("'", "''") + "' order by last_updated_user";

                                object objTaskId = DatabaseHelper.executeScalar(sql);

                                if (objTaskId != null)
                                {
                                    sql = @"update hours_reporting set finished_date = getdate(), minutes = datediff(minute, started_date, getdate()) 
                                            where task_id = " + objTaskId.ToString() + "  and finished_date is null";
                                    int intResult = DatabaseHelper.executeNonQuery(sql);
                                }
                            }
                            catch { }
                            lblMessage.Text = "Your EODC have been sent.";
                        }
                        else
                        {
                            lblMessage.Text = "There was an error in sending mail";
                        }
                    }
                }
            //} //Next task condition removed
        }
        catch (Exception ex)
        {
            HandleException(ex, "Project: esgDesk; Form: EODC3; Button1_click event Outer try-catch error: ", sql);
        }
    }

    public static void HandleException(Exception ex, string strSubject, string strQuery)
    {
        String strError = "Error Message: " + ex.Message + "<br><br>";
        if (ex.InnerException != null)
        {
            strError += "InnerException: " + Convert.ToString(ex.InnerException) + "<br><br>";
        }
        strError += "Stack Trace: " + ex.StackTrace + "<br><br>";
        strError += "source: " + ex.Source + "<br><br>";

        if (strQuery != null && strQuery != "")
        {
            strError += "\r\n <br> Query: " + strQuery;
        }
        DatabaseHelper.SendEmailList("kirann@123sitesolutions.com", strSubject, strError);
    }
}
