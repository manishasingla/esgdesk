<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Edit_OverdueDate.aspx.cs"
    Inherits="Admin_Edit_OverdueDate" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        Edit overdue date</title>

    <script language="javascript" type="text/javascript" src="calendarDateInput.js"></script>

    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        function checkChecked()
        {
            if(document.getElementById("chkShowAllHistory").checked==true)
            {
                document.getElementById("divNoHistory").style.display="none";
                document.getElementById("divAllHistory").style.display="block";       
            }
            else
            {
                document.getElementById("divNoHistory").style.display="block";
                document.getElementById("divAllHistory").style.display="none";       
            }
        }
    </script>

    <script type="text/javascript">
function ChangeCalendarView(sender,args)
{
   sender._switchMode("years", true);           
}
    </script>

    <script type="text/javascript">
    function checkDate(sender,args)
{
   
 if (sender._selectedDate < new Date().format(sender._format)) 
            {
                alert("You cannot select a day earlier than today!");
                sender._selectedDate = new Date(); 
                
                document.getElementById("txtStartDate").innerText="";
            }
}

function checkDate1(sender,args)
{
 if (sender._selectedDate < new Date().format(sender._format)) 
            {
                alert("You cannot select a day earlier than today!");
                sender._selectedDate = new Date(); 
                
                document.getElementById("txtDueDate").innerText="";
            }
}



    </script>

    <script type="text/javascript" language="javascript">
function confirm()
{

///if (e.keyCode==110)
if (confirm ==true)


var btndel = document.getElementById("BtnDelMsg");
btndel.click();  



else


return false;

}
    </script>

    <script language="javascript" type="text/javascript">
    function checkCat()
    {
    var cat =  document.getElementById("txtCategory").value;
    if(cat =="")
    {
    alert("Please enter category name");
    return false;
    }
    else
    {
    var btnsend = document.getElementById("btnAddCat");
btnsend.click();  
    }
    
    }
    
    </script>

    <script language="javascript" type="text/javascript">
    function chkcatEdit()
    {
    var cat =  document.getElementById("txteditCat").value;
    if(cat =="")
    {
    alert("Please enter category name");
    return false;
    }
    else
    {
    var btnsend = document.getElementById("btnUpdateCat");
    btnsend.click();  
    }
    }
    
    </script>

    <script language="javascript" type="text/javascript">
    function updateParentWindow()
    {
    
    var londesc = document.getElementById('ftbComment').value; 
   //// window.opener.updateValues(londesc);   
    window.opener.location.reload(true);   
    window.close(); 
    
    
    }
    
    </script>

</head>
<body onload="checkChecked();" style="background-color:White">
    <form id="form1" runat="server">
    <div id="wrapper">
        <div id="Content">
            <div id="Content_98">
                <asp:ScriptManager ID="ScriptManager1" EnableScriptGlobalization="true" EnableScriptLocalization="true"
                    runat="server">
                </asp:ScriptManager>
                <div class="titleText" style="margin: 10px 0;">
                    Update Overdue date</div>
                <div id="divMessage" runat="server" style="color: Red; font-weight: bold">
                </div>
                <div id="DivEntry" runat="server">
                    <div id="divTask" runat="server" class="divBorder">
                        <table width="72%" cellpadding="2" style="padding: 0 0 0 10px;">
                            <tr>
                                <td align="left" colspan="4" valign="top" style="height: 149px">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="height: 31px; width: 142px;">
                                                Project:
                                            </td>
                                            <td style="height: 31px;" colspan="4">
                                                <asp:Label ID="lblProject" runat="server" Font-Bold="True"></asp:Label>&nbsp;
                                            </td>
                                        </tr>
                                        <tr id="TRNewCat" runat="server" style="margin-top: 10px">
                                            <td style="height: 34px;">
                                                Short description:
                                            </td>
                                            <td colspan="4" style="height: 34px">
                                                <asp:TextBox ID="txtShortDescr" runat="server" Width="710px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="TREditCat" runat="server">
                                            <td>
                                                Start date:
                                            </td>
                                            <td style="width: 574px">
                                                <asp:TextBox ID="txtStartDate" runat="server" Width="180"></asp:TextBox>
                                                &nbsp;&nbsp; Status:&nbsp;
                                                <asp:Label ID="Lblstatus" runat="server" Font-Bold="True"></asp:Label>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td style="height: 30px">
                                                &nbsp;
                                            </td>
                                            <td style="height: 30px">
                                            </td>
                                        </tr>
                                        <tr runat="server" id="Tr1">
                                            <td style="height: 30px">
                                                Due date:
                                            </td>
                                            <td style="width: 574px; height: 30px;">
                                                <asp:TextBox ID="txtDueDate" runat="server" Width="180"></asp:TextBox>
                                                &nbsp;&nbsp; Priority:
                                                <asp:Label ID="LblPriority" runat="server" Font-Bold="True"></asp:Label>&nbsp; Task
                                                Owner:
                                                <asp:Label ID="LblOwner" runat="server" Font-Bold="True"></asp:Label>
                                            </td>
                                            <td style="height: 30px">
                                            </td>
                                            <td style="height: 30px">
                                            </td>
                                            <td style="height: 30px">
                                            </td>
                                        </tr>
                                        <tr runat="server">
                                            <td style="height: 30px">
                                                <asp:CheckBox ID="ChkReminder" Text="Reminder" runat="server" />:
                                            </td>
                                            <td style="width: 574px; height: 30px">
                                                <asp:TextBox ID="txtRemiderDate" runat="server" Width="180px"></asp:TextBox>
                                                &nbsp;&nbsp; Time:
                                                <select id="drpHour" runat="server" class="bdatetime" name="select4">
                                                    <option selected="selected">01</option>
                                                    <option>02</option>
                                                    <option>03</option>
                                                    <option>04</option>
                                                    <option>05</option>
                                                    <option>06</option>
                                                    <option>07</option>
                                                    <option>08</option>
                                                    <option>09</option>
                                                    <option>10</option>
                                                    <option>11</option>
                                                    <option>12</option>
                                                </select>
                                                <select id="drpMin" runat="server" class="bdatetime" name="select4">
                                                    <option selected="selected">:00</option>
                                                    <option>:05</option>
                                                    <option>:10</option>
                                                    <option>:15</option>
                                                    <option>:20</option>
                                                    <option>:25</option>
                                                    <option>:30</option>
                                                    <option>:35</option>
                                                    <option>:40</option>
                                                    <option>:45</option>
                                                    <option>:50</option>
                                                    <option>:55</option>
                                                </select>
                                                <select id="drpAMPM" runat="server" class="bdatetime" name="select4">
                                                    <option value=" am">am</option>
                                                    <option selected="selected" value=" pm">pm</option>
                                                </select>
                                            </td>
                                            <td style="height: 30px">
                                            </td>
                                            <td style="height: 30px">
                                            </td>
                                            <td style="height: 30px">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="4" valign="top">
                                    Notes for owner:
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="4" valign="bottom" style="height: 222px">
                                <%--***************************************************************************************************--%>
                                    <%--<FTB:FreeTextBox ID="ftbComment" runat="server" Height="200px" Width="98%"></FTB:FreeTextBox>--%>
                                    <telerik:RadEditor ID="ftbComment" runat="server" Height="300px" Width="100%">
                                        <Snippets>
                                            <telerik:EditorSnippet Name="Product Template" Value="<table><tr><td><div style='width:300px;border:2px outset #D8D2BD;padding:34px;font-family:Arial, Verdana; font-size:11px;background- color:#F1EFE6;'>Dear ____________________,<br>Thank you for inquiring about ____________. Your request will be processed in 48 hours and shipped at the address you have provided.<b>Please, contact us if you have any problems.</b></div></td></tr></table>">            
                                            </telerik:EditorSnippet>
                                            <telerik:EditorSnippet Name="Signature" Value="<strong>John Smith</strong><br>Sales Manager<br>">
                                            </telerik:EditorSnippet>
                                        </Snippets>
                                        <Links>
                                            <telerik:EditorLink Name="Telerik" Href="http://www.telerik.com">
                                                <telerik:EditorLink Name="Telerik Products" Href="http://www.telerik.com/products"
                                                    Target="_blank" />
                                                <telerik:EditorLink Name="Telerik Support Center" Href="http://www.telerik.com/support/home.aspx"
                                                    Target="_blank" />
                                                <telerik:EditorLink Name="Telerik Community" Href="http://www.telerik.com/community/home.aspx"
                                                    Target="_blank" ToolTip="Telerik Community" />
                                            </telerik:EditorLink>
                                        </Links>
                                        <Tools>
                                        </Tools>
                                        <ImageManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                        <MediaManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                        <FlashManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                        <TemplateManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                        <DocumentManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                        <Modules>
                                            <telerik:EditorModule Name="RadEditorStatistics" Visible="False" Enabled="true" />
                                            <telerik:EditorModule Name="RadEditorDomInspector" Visible="False" Enabled="true" />
                                            <telerik:EditorModule Name="RadEditorNodeInspector" Visible="False" Enabled="true" />
                                            <telerik:EditorModule Name="RadEditorHtmlInspector" Visible="False" Enabled="true" />
                                        </Modules>
                                        <Content>
                                        </Content>
                                    </telerik:RadEditor>
                                <%--***************************************************************************************************--%>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top" colspan="3" style="height: 28px">
                                    <div id="divMessage1" runat="server" style="color: Red">
                                    </div>
                                </td>
                                <td align="left" valign="top" style="height: 28px">
                                    <div style="width: 98%" align="right">
                                        <asp:Button ID="BtnUpdate" runat="server" Text="Update" OnClick="BtnUpdate_Click"
                                            CssClass="blueBtns" />
                                        <asp:Button ID="BtnClose" runat="server" Text="Close" OnClick="BtnClose_Click" CssClass="blueBtns" /></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtDueDate"
        Format="ddd dd/MMM/yyyy">
    </cc1:CalendarExtender>
    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtStartDate"
        Format="ddd dd/MMM/yyyy">
    </cc1:CalendarExtender>
    <cc1:CalendarExtender ID="Calendarextender3" runat="server" TargetControlID="txtRemiderDate"
        Format="ddd dd/MMM/yyyy" />
    <asp:HiddenField ID="prevStartDate" runat="server" />
    <asp:HiddenField ID="PrevDueDate" runat="server" />
    </form>
</body>
</html>
