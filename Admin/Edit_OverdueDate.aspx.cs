using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Admin_Edit_OverdueDate : System.Web.UI.Page
{
    int id;
    string sql = "";
    string task_action = "";
    string emailSubject = "";
    string shortDescription = "";
    bool deletedFlag = false;
    bool flagEmailNotification = false;
    string task_id = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        //btnAddCat.Attributes.Add("Onclick", "return checkCat();");
       // btnUpdateCat.Attributes.Add("Onclick", "return chkcatEdit();");
        if (Request.QueryString["id"] == null || Request.QueryString["id"].ToString() == "")
        {

        }
        else
        {
         task_id = Request.QueryString["id"];
        }
        if (!Page.IsPostBack)
        {
            

            sql = "select * from tasks where task_id = " + task_id.Trim();

            DataSet dsTask = DatabaseHelper.getDataset(sql);

            if (dsTask == null || dsTask.Tables.Count <= 0 || dsTask.Tables[0].Rows.Count <= 0)
            {
            }
            else
            {
                lblProject.Text = dsTask.Tables[0].Rows[0]["project"].ToString();
                LblOwner.Text = dsTask.Tables[0].Rows[0]["assigned_to_user"].ToString();
                Lblstatus.Text = dsTask.Tables[0].Rows[0]["status"].ToString();

                LblPriority.Text = dsTask.Tables[0].Rows[0]["priority"].ToString();

                txtShortDescr.Text = dsTask.Tables[0].Rows[0]["short_desc"].ToString();
                try
                {
                    txtStartDate.Text = DateTime.Parse(dsTask.Tables[0].Rows[0]["StartDate"].ToString()).ToString("ddd  dd MMM yyyy");
                    prevStartDate.Value = txtStartDate.Text;
                }
                catch
                {
                    txtStartDate.Text = "";
                    prevStartDate.Value = txtStartDate.Text;
                }
                try
                {
                    txtDueDate.Text = DateTime.Parse(dsTask.Tables[0].Rows[0]["DueDate"].ToString()).ToString("ddd  dd MMM yyyy");
                    PrevDueDate.Value = txtDueDate.Text;
                }
                catch
                {
                    txtDueDate.Text = "";
                    PrevDueDate.Value = txtDueDate.Text;
                }


            }
        }

    }
    protected void BtnUpdate_Click(object sender, EventArgs e)
    {
        ////if(txtStartDate.Text == "")
        ////{
        ////    Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('Please select start date.');</script>");

        ////    return;

        ////}
        ////if(txtDueDate.Text == "")
        ////{
        ////      Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('Please select due date.');</script>");

        ////    return;
        ////}

        sql = " update tasks set ";
        sql += " [StartDate]='" + txtStartDate.Text.Trim().Replace("'", "''") + "',";
        sql += " [DueDate]='" + txtDueDate.Text.Trim().Replace("'", "''") + "',";
        sql += " [last_updated_user]='" + Session["admin"].ToString().Replace("'", "''") + "',";
        sql += " [last_updated_date]=getdate()";
        sql += " where task_id = " + task_id.Trim() + "; ";
        //*************************************************************
        //if (ftbComment.Text.Trim() != "")
        if (ftbComment.Content.Trim() != "")
        //*************************************************************
        {
            sql += "DECLARE @CommentId INT;";
            sql += " insert into task_comments([task_id],[username],[post_date],[comment],[deleted],[qflag],[QuesTo]) ";
            sql += " values(" + task_id.Trim() + ",";
            sql += "'" + Session["admin"].ToString().Replace("'", "''") + "',";
            sql += "getdate(),";
            //**********************************************************************
            //sql += "'" + ftbComment.Text.Trim().Replace("'", "''") + "',";
            sql += "'" + ftbComment.Content.Trim().Replace("'", "''") + "',";
            //**********************************************************************
            string strquesto = "";
            sql += "0,'" + strquesto + "','" + strquesto + "' ); ";
            sql += " SET @CommentId = SCOPE_IDENTITY(); ";
            sql += " insert into read_comments([tc_id],[username]) ";
            sql += " values(@CommentId,'" + Session["admin"] + "')";
            sql += " insert into read_task([task_id],[username]) ";
            sql += " values(" + task_id.Trim() + ",'" + Session["admin"] + "'); ";
        }

        sql += record_changes();
        int intResult = DatabaseHelper.executeNonQuery(sql);
        if (intResult != 0)
        {
            task_action = ((string)DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + Session["admin"].ToString() + "'")) + " has updated task ";

            emailSubject = "Task ID:" + task_id.Trim() + " was updated - " + txtShortDescr.Text.Trim() + " (Task ID:" + task_id.Trim() + ")";

            sendNotification(task_id.Trim());
           
          
        }
        Page.ClientScript.RegisterStartupScript(typeof(Page), "test", "<script> updateParentWindow()</script>"); 

    }
    private string generateAdminEmail(string strTaskId)
    {
        DataSet dsTask = DatabaseHelper.getDataset("select * from tasks where task_id=" + strTaskId);
        string strBody = "";
        if (dsTask == null || dsTask.Tables.Count <= 0 || dsTask.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "Task not found.<br><br><a href='tasks.aspx'>View tasks</a>";
        }
        else
        {
            strBody += "Task: <span style=\"color:red\">";

            strBody += task_action;

            strBody += " (<a style=\"color:red\" href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_task.aspx?id=" + strTaskId + "'>" + strTaskId + "</a>) </span> " + DateTime.Parse(dsTask.Tables[0].Rows[0]["last_updated_date"].ToString()).ToString("dd MMM h:mm tt") + "<br><br>";

            if (task_id.ToString() != "")
            {
                string attribures = record_changes_for_email();

                if (attribures.ToString().Trim() != "")
                {
                    strBody += "The attributes below have been amended.<br>";
                    strBody += "<span style=\"color:red\">" + attribures + "</span><br>";
                }
            }

            strBody += "<a href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_task.aspx?id=" + strTaskId + "'>" + txtShortDescr.Text.Trim() + "</a>&nbsp;&nbsp;&nbsp;(" + dsTask.Tables[0].Rows[0]["priority"].ToString() + " : " + dsTask.Tables[0].Rows[0]["status"].ToString() + ")<br><br>";
            strBody += "Emp: " + ((string)DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + dsTask.Tables[0].Rows[0]["assigned_to_user"].ToString() + "'")) + "<br>";
            strBody += "Project: " + dsTask.Tables[0].Rows[0]["project"].ToString() + "<br>";
            strBody += "Relevant URL: <a href='" + dsTask.Tables[0].Rows[0]["Relevant_URL"].ToString() + "' target='_blank'>" + dsTask.Tables[0].Rows[0]["Relevant_URL"].ToString() + "</a><br>";
            strBody += "Start date: " + dsTask.Tables[0].Rows[0]["StartDate"].ToString() + "<br>";
            strBody += "Due date: " + dsTask.Tables[0].Rows[0]["DueDate"].ToString() + "<br>";
            strBody += "<br><br>";

            sql = "Select * from task_comments where task_id=" + strTaskId.ToString() + " and deleted <> 1 order by tc_id desc";

            DataSet dsTaskDetails = DatabaseHelper.getDataset(sql);

            strBody += "<table cellspacing=\"0\" border=\"1\" style=\"width: 99%;border-collapse: collapse;\">";
            strBody += "<tbody>";

            for (int i = 0; i < dsTaskDetails.Tables[0].Rows.Count; i++)
            {
                strBody += "<tr>";
                strBody += "<td>";
                strBody += "<div style=\"border: 1px none Green;\">";
                strBody += "<table width=\"100%\">";
                strBody += "<tbody>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                if (dsTaskDetails.Tables[0].Rows[i]["qflag"].ToString() == "1")
                {
                    strBody += "<span style=\"color:red;\">Question posted by " + dsTaskDetails.Tables[0].Rows[i]["username"].ToString() + " on " + DateTime.Parse(dsTaskDetails.Tables[0].Rows[i]["post_date"].ToString()).ToString("dd MMM h:mm tt") + "</span>";
                }
                else
                {
                    strBody += "<span style=\"color: Green;\">comment " + dsTaskDetails.Tables[0].Rows[i]["tc_id"].ToString() + " posted by " + dsTaskDetails.Tables[0].Rows[i]["username"].ToString() + " on " + DateTime.Parse(dsTaskDetails.Tables[0].Rows[i]["post_date"].ToString()).ToString("dd MMM h:mm tt") + "</span>";
                }
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\"> </td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span>" + dsTaskDetails.Tables[0].Rows[i]["comment"].ToString() + "</span>";
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "</tbody>";
                strBody += "</table>";
                strBody += "</div>";
                strBody += "</td>";
                strBody += "</tr>";
            }
            strBody += "</tbody>";
            strBody += "</table>";
        }
        return strBody;
    }

    void sendNotification(string strTaskId)
    {
        try
        {
            sql = "select email, EmailNotification,PR_EmailNotification ";
            sql += " from users, task_subscriptions ";
            sql += " where users.username = task_subscriptions.username ";
            sql += " and task_subscriptions.task_id =" + strTaskId.ToString() + " and task_subscriptions.username <> '" + Session["admin"].ToString() + "'";

            DataSet ds = DatabaseHelper.getDataset(sql);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                string strHtml = generateAdminEmail(strTaskId);

                string toEmails = "";


                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (ds.Tables[0].Rows[i]["EmailNotification"].ToString() == "Y")
                    {

                        string reqPR = "";
                        string[] strPR = ds.Tables[0].Rows[i]["PR_EmailNotification"].ToString().Split(new char[] { ';' });
                        for (int j = 0; j < strPR.Length - 1; j++)
                        {
                            reqPR = strPR[j];
                            if (reqPR == "All")
                            {
                                toEmails += ds.Tables[0].Rows[i]["email"].ToString() + ";";
                            }
                            else if (reqPR == LblPriority.Text)
                            {
                                toEmails += ds.Tables[0].Rows[i]["email"].ToString() + ";";
                            }

                        }


                    }
                    else if ((ds.Tables[0].Rows[i]["EmailNotification"].ToString() == "N") && (flagEmailNotification == true))
                    {
                        /// toEmails += ds.Tables[0].Rows[i]["email"].ToString() + ";";

                        string reqPR = "";
                        string[] strPR = ds.Tables[0].Rows[i]["PR_EmailNotification"].ToString().Split(new char[] { ';' });
                        for (int j = 0; j < strPR.Length - 1; j++)
                        {
                            reqPR = strPR[j];
                            if (reqPR == "All")
                            {
                                toEmails += ds.Tables[0].Rows[i]["email"].ToString() + ";";
                            }
                            else if (reqPR == LblPriority.Text)
                            {
                                toEmails += ds.Tables[0].Rows[i]["email"].ToString() + ";";
                            }

                        }

                    }

                }

                toEmails = toEmails.TrimEnd(';');

                if (emailSubject == "")
                {
                    emailSubject = "Task ID:" + strTaskId.ToString() + " was updated - " + txtShortDescr.Text.Trim() + " (Task ID:" + strTaskId.ToString() + ")";
                }
                bool flag = DatabaseHelper.sendEmailTasks(toEmails, emailSubject, strHtml);

            }
        }
        catch { }
    }

    private string record_changes()
    {
       
            string base_sql = @" insert into task_updates([task_id],[username],[post_date],[comment]) 
                            values($task_id,'$us',getdate(),'$comment'); ";

            base_sql = base_sql.Replace("$task_id", task_id.Trim());
            base_sql = base_sql.Replace("$us", Session["admin"].ToString().Replace("'", "''"));

            string strSql = "";


            if (txtStartDate.Text != prevStartDate.Value.Trim())
            {
                strSql += base_sql.Replace(
                    "$comment",
                    "changed start date  from \""
                    + prevStartDate.Value.Replace("'", "''") + "\" to \""
                    + txtStartDate.Text.Trim().Replace("'", "''") + "\"");

                //prevShortDescr.Value = txtShortDescr.Text.Trim();
            }

            if (txtDueDate.Text.ToString() != PrevDueDate.Value)
            {
                strSql += base_sql.Replace(
                   "$comment",
                   "changed due date type from \""
                   + PrevDueDate.Value.Replace("'", "''") + "\" to \""
                   + txtDueDate.Text.Replace("'", "''") + "\"");

                //prevProjectType.Value = drpProjectType.SelectedItem.Value;	    
            }
       
  
        return strSql;
    }

    private string record_changes_for_email()
    {
        string strSql = "";

        if (txtStartDate.Text != prevStartDate.Value)
        {
            strSql += "Start date has been changed from \""
                + prevStartDate.Value.Replace("'", "''") + "\" to \""
                + txtStartDate.Text.Replace("'", "''") + "\"<br>";

            //prevProject.Value = drpProjects.SelectedItem.Value;
        }

        if (txtDueDate.Text != PrevDueDate.Value)
        {
            strSql += "Due date has been changed from \""
                + PrevDueDate.Value.Replace("'", "''") + "\" to \""
                + txtDueDate.Text.Replace("'", "''") + "\"<br>";

            //prevAssignedTo.Value = drpUsers.SelectedValue;		    
        }
        //*******************************************************
        //if (ftbComment.Text.Trim() != "")
          if (ftbComment.Content.Trim() != "")
        //*******************************************************
        {
            strSql += "Comment has been added.<br>";
             
            flagEmailNotification = true;
        }
        return strSql;
    }

    protected void BtnClose_Click(object sender, EventArgs e)
    {
        Response.Write("<script language='javascript'> { self.close() }</script>");
    }
}
