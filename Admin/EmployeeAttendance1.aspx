<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EmployeeAttendance1.aspx.cs"
    Inherits="Admin_EmployeeAttendance" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagName="WeeklyReport" TagPrefix="WR" Src="~/Admin/LMSAdmin/LMS-Weekly-report.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server" style="font-size: small;">
    <div>
      <table style="width: 100%; padding: 10px; height: 373" class="publicloginTable" onkeypress="javascript:keypressHandler(event)">
        <tr>
            <td valign="top">
                Starting date
            </td>
            <td align="center" valign="top">
                :
            </td>
            <td valign="top" style="width: 287px">
                <asp:TextBox ID="GMDStartDate" runat="server" CssClass="input_boxline"></asp:TextBox>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="GMDStartDate"
                    Format="dd/MM/yyyy" PopupButtonID="GMDStartDate">
                </ajaxToolkit:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please select a start date"
                    ControlToValidate="GMDStartDate" ValidationGroup="g1"></asp:RequiredFieldValidator>
                <br />
                <asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="Please enter a valid  date"
                    Operator="DataTypeCheck" ControlToValidate="GMDStartDate" Type="Date" 
                    ValidationGroup="g1"></asp:CompareValidator>
            </td>
            <td style="width: 5%; height: 10px" valign="top">
                Ending date
            </td>
            <td align="center" style="width: 2%" valign="top">
                :
            </td>
            <td valign="top" class="style1">
                <asp:TextBox ID="GMDEndDate" runat="server" CssClass="input_boxline"></asp:TextBox>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="GMDEndDate"
                    Format="dd/MM/yyyy" PopupButtonID="GMDEndDate">
                </ajaxToolkit:CalendarExtender>
                <asp:CompareValidator ID="CompareValidator3" runat="server" ErrorMessage="Please enter a valid date"
                    Operator="DataTypeCheck" ControlToValidate="GMDEndDate" Type="Date" 
                    ValidationGroup="g1"></asp:CompareValidator>
                <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="End date should be greater or equal to start date."
                    ControlToCompare="GMDStartDate" ControlToValidate="GMDEndDate" Operator="GreaterThanEqual"
                    Type="Date" ValidationGroup="g1"></asp:CompareValidator>
                &nbsp;&nbsp;&nbsp;&nbsp;
            </td>
            <td>
            </td>
            <td valign="top">
                Department
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:DropDownList ID="ddlDept" runat="server" Height="18px" Width="201px">
                </asp:DropDownList>
            </td>
        </tr>
        </table>
    </div>
    <div>
        <asp:DropDownList ID="druser" runat="server" AutoPostBack="true" OnSelectedIndexChanged="druser_SelectedIndexChanged" Visible ="false">
            <%--<asp:ListItem  Value="PHG">PHG</asp:ListItem>
            <asp:ListItem  Selected="True" Value="ALL">ALL</asp:ListItem>
            <asp:ListItem Value="Sarika">Sarika</asp:ListItem>
            <asp:ListItem Value="avinash">avinash</asp:ListItem>--%>
        </asp:DropDownList>
    </div>
    <div>
        <table>
            <tr>
                <td colspan="5" >
                    <asp:ScriptManager ID="ScriptManager1" EnablePageMethods="true" runat="server">
                    </asp:ScriptManager>
                    <asp:TextBox ID="GMDStartDate1" runat="server"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="GMDStartDate1"
                        Format="yyyy-MM-dd" PopupButtonID="GMDStartDate1">
                    </ajaxToolkit:CalendarExtender>
                    <asp:Button ID="btnshow" runat="server" Text="Show time sheet" OnClick="btnshow_Click" />
                    <asp:Button ID="btnexport" runat="server" Text="Export to Excel" OnClick="btnexport_Click" />
                </td>
            </tr>
        </table>
    </div>
    <div id="HtmlDesign" runat="server">
    </div>
    <div id="DateWiseReport" runat="server">
    <WR:WeeklyReport runat="server"/>
    </div>
    </form>
</body>
</html>
