<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EmployeeAttendance3.aspx.cs"
    Inherits="Admin_EmployeeAttendance" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server" style="font-size: small;">
    <div>
        <asp:DropDownList ID="druser" runat="server" AutoPostBack="true" OnSelectedIndexChanged="druser_SelectedIndexChanged">
            <%--<asp:ListItem  Value="PHG">PHG</asp:ListItem>
            <asp:ListItem  Selected="True" Value="ALL">ALL</asp:ListItem>
            <asp:ListItem Value="Sarika">Sarika</asp:ListItem>
            <asp:ListItem Value="avinash">avinash</asp:ListItem>--%>
        </asp:DropDownList>
    </div>
    <div>
        <table >
            <tr>
                <td colspan="5"      >
                    <asp:ScriptManager ID="ScriptManager1" EnablePageMethods="true" runat="server">
                    </asp:ScriptManager>
                    <asp:TextBox ID="GMDStartDate" runat="server"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="GMDStartDate"
                        Format="yyyy-MM-dd" PopupButtonID="GMDStartDate">
                    </ajaxToolkit:CalendarExtender>
                    <asp:Button ID="btnshow" runat="server" Text="Show time sheet" OnClick="btnshow_Click" />
                    <asp:Button ID="btnexport" runat="server" Text="Export to Excel" OnClick="btnexport_Click" />
                </td>
            </tr>
        </table>
    </div>
  
    <div id="HtmlDesign" runat="server">
    </div>
    </form>
</body>
</html>
