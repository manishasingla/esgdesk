using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Admin_EmployeeAttendance : System.Web.UI.Page
{
    static string strreportname = "";
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
              
            string strdate = @"SELECT 
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0),103)   as Date_of_Monday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+1,103) as Date_of_Tuesday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+2,103) as Date_of_Wednesday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+3,103) as Date_of_Thusday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+4,103) as Date_of_Friday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+5,103) as Date_of_Saturday";
            //*****************************For ALL Employee**********************************************

            DataSet ds = DatabaseHelper.getDataset(strdate);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                load_filterDropDown();
                clear();
                dynamically(ds);
                if (druser.SelectedItem.Text == "PHG")
                {
                    strreportname = ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "_To_" + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString();
                }
                else 
                {
                    strreportname = ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "_To_" + ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString();
                }
                
            }
        }

    }
    
    public void clear()
    {}
    void load_filterDropDown()
    {
        druser.Items.Clear();
        string sql = "Select username from users where IsCW='Y' order by username";

        DataSet dsAdmin = DatabaseHelper.getDataset(sql);
        druser.Items.Add(new ListItem("PHG", "PHG"));
        druser.Items.Add(new ListItem("ALL", "ALL"));
        for (int i = 1; i < dsAdmin.Tables[0].Rows.Count; i++)
        {
            ListItem item = new ListItem(dsAdmin.Tables[0].Rows[i]["username"].ToString(), dsAdmin.Tables[0].Rows[i]["username"].ToString());
            druser.Items.Add(item);
        }
        
    }       
    

    public void dynamically(DataSet ds)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        string strAllEmployee="";
        if (druser.SelectedItem.Text == "ALL")
        {
            strAllEmployee = @"select * from users where IsCW='Y' order by username ";
        }
        else if (druser.SelectedItem.Text == "PHG")
        {
            strAllEmployee = @"select * from users where IsCW='Y' and username in ('Satyendra','Sheereen','Aditi','Suraj') order by username";
            //strAllEmployee = @"select * from users where IsCW='Y' and username in ('Ashish','Sarika','namita','Surekha')";
        }
        else
        {
            strAllEmployee = @"select * from users where IsCW='Y' and username = '" + druser.SelectedItem.Text + "'; order by username ";
        }
        DataSet dsAllEmp = DatabaseHelper.getDataset(strAllEmployee);
        if (dsAllEmp != null && dsAllEmp.Tables.Count > 0 && dsAllEmp.Tables[0].Rows.Count > 0)
        {
            sb.Append("<table width='100%' border='0' cellpadding='0' cellspacing='0'>");
            sb.Append("<tr>");
            sb.Append("<td height='10px' colspan='8' align='left' style='font-size:medium'>");
            sb.Append("<b>Resource planning reports, Mumbai ( from ");
            sb.Append(ds.Tables[0].Rows[0]["Date_of_Monday"].ToString());
            sb.Append(" to ");
            if (druser.SelectedItem.Text == "PHG")
            {
                sb.Append(ds.Tables[0].Rows[0]["Date_of_Friday"].ToString() + ")");
            }
            else
            {
                sb.Append(ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString() + ")");
            }
            sb.Append("</b></td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td height='10px'>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("<table width='100%' border='0' cellpadding='0' cellspacing='0'>");


            sb.Append("<tr>");
            sb.Append("<td height='37px'>");
           
            sb.Append("<br><table border='1' width='100%' height='37px' cellpadding='0' cellspacing='0'><tr><td height='37px' rowspan='2' ><b>Employee name</b></td></tr></table>");
            sb.Append("</td>");


            sb.Append("<td>");
            
            sb.Append("<br><table border='1' width='100%'height='37px' cellpadding='0' cellspacing='0'><tr><td height='37px' rowspan='2'>Skill</td></tr></table>");
            sb.Append("</td>");
            sb.Append("<td>");
           

            sb.Append("<br><table width='100%' border='1' cellpadding='0' cellspacing='0'>");
            sb.Append("<tr>");



            if (druser.SelectedItem.Text == "PHG")
            {
                sb.Append("<td width='20%'>");
            }
            else
            {
                sb.Append("<td width='17%'>");
            }




            sb.Append(" " + ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + " (Mon)");
            sb.Append("<br><table border='0' cellpadding='0' cellspacing='0'><tr><td align='center'>In/Out (GMT)</td><td> Total </td></tr></table>");
            sb.Append("</td>");


            if (druser.SelectedItem.Text == "PHG")
            {
                sb.Append("<td width='20%'>");
            }
            else
            {
                sb.Append("<td width='17%'>");
            }

            sb.Append(" " + ds.Tables[0].Rows[0]["Date_of_Tuesday"].ToString() + " (Tue)");
            sb.Append("<br><table border='0' ><tr><td align='center'>In/Out (GMT)</td><td> Total </td></tr></table>");
            sb.Append("</td>");


            if (druser.SelectedItem.Text == "PHG")
            {
                sb.Append("<td width='20%'>");
            }
            else
            {
                sb.Append("<td width='17%'>");
            }


            sb.Append(" " + ds.Tables[0].Rows[0]["Date_of_Wednesday"].ToString() + " (Wed)");
            sb.Append("<br><table border='0' cellpadding='0' cellspacing='0'><tr><td align='center'>In/Out (GMT)</td><td> Total </td></tr></table>");
            sb.Append("</td>");


            if (druser.SelectedItem.Text == "PHG")
            {
                sb.Append("<td width='20%'>");
            }
            else
            {
                sb.Append("<td width='17%'>");
            }


            sb.Append(" " + ds.Tables[0].Rows[0]["Date_of_Thusday"].ToString() + " (Thur)");
            sb.Append("<br><table border='0' cellpadding='0' cellspacing='0'><tr><td align='center'>In/Out (GMT)</td><td> Total </td></tr></table>");
            sb.Append("</td>");

            if (druser.SelectedItem.Text == "PHG")
            {
                sb.Append("<td width='20%'>");
            }
            else
            {
                sb.Append("<td width='17%'>");
            }




            sb.Append(" " + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString() + " (Fri)");
            sb.Append("<br><table border='0' cellpadding='0' cellspacing='0'><tr><td align='center'>In/Out (GMT)</td><td> Total </td></tr></table>");
            sb.Append("</td>");


            if (druser.SelectedItem.Text != "PHG")
            {
                sb.Append("<td width='20%'>");
                sb.Append(" " + ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString() + " (Sat)");
                sb.Append("<br><table border='0' cellpadding='0' cellspacing='0'><tr><td align='center'>In/Out (GMT)</td><td> Total </td></tr></table>");
                sb.Append("</td>");
            }


            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("</td>");
           
            sb.Append("</tr>");
            for (int iEmp = 0; iEmp < dsAllEmp.Tables[0].Rows.Count; iEmp++)
            {
                if (iEmp % 2 == 0)
                {
                   sb.Append("<tr style='background:#fde9d9;'>");
                }
                else
                {
                    sb.Append("<tr>");
                }
                //sb.Append("<td align='left' style='width: 100.75pt; border-width: 1pt 1pt 1pt medium; border-style: solid solid solid none;border-color: black black black -moz-use-text-color; -moz-border-top-colors: none;-moz-border-right-colors: none; -moz-border-bottom-colors: none; -moz-border-left-colors: none;border-image: none; background: none repeat scroll 0% 0%; padding: 0cm 5.4pt;'>");
                sb.Append("<td align='left' style='width:100.75pt;' >");
                sb.Append("<table border='1' width='100%' cellpadding='0' cellspacing='0'><tr><td><b>" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "</b></td></tr></table>");
                if (dsAllEmp.Tables[0].Rows[iEmp]["skills"].ToString() != "")
                {
                    sb.Append("</td><td align='left' ><table border='1' width='100%' height='100%' cellpadding='0' cellspacing='0'><tr><td>" + dsAllEmp.Tables[0].Rows[iEmp]["skills"].ToString() + "</td></tr></table></td>");
                }
                else 
                {
                    sb.Append("</td><td align='left' ><table border='1' width='100%' height='100%' cellpadding='0' cellspacing='0'><tr><td> -/ </td></tr></table></td>");
                }
                //******************************************* Create In Time ******************************************************************
                sb.Append("<td>");
                //sb.Append("<td style='border-width: medium 1pt 1pt medium; border-style: none solid solid none;");
                //sb.Append("border-color: -moz-use-text-color black black -moz-use-text-color; background: none repeat scroll 0% 0%;");
                //sb.Append("padding: 0cm 5.4pt;'>");
                sb.Append("<table width='100%' border='1' cellpadding='0' cellspacing='0'>");
                sb.Append("<tr>");


                if (druser.SelectedItem.Text == "PHG")
                {
                    sb.Append("<td width='20%'>");
                }
                else
                {
                    sb.Append("<td width='17%'>");
                }

                sb.Append("<table width='100%' border='0' height='100%' cellpadding='0' cellspacing='0'>");
                sb.Append("<tr>");
                sb.Append("<td colspan='2'>");

                string strInout1 = "";
                object objIntimeALL1 = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "'");
                if (objIntimeALL1 != null && objIntimeALL1.ToString() != "")
                {
                    DateTime dtALL1 = Convert.ToDateTime(objIntimeALL1).AddHours(-5).AddMinutes(-30);
                    strInout1 = dtALL1.ToString("HH:mm").ToString() + "/";
                }
                else
                {
                  strInout1 = "-/";
                }


                object objOuttimeALL1 = DatabaseHelper.executeScalar("select Outtime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "'");
                if (objOuttimeALL1 != null && objOuttimeALL1.ToString() != "")
                {
                    DateTime dtoutALL1 = Convert.ToDateTime(objOuttimeALL1).AddHours(-5).AddMinutes(-30);
                    strInout1 += dtoutALL1.ToString("HH:mm");
                }
                else
                {
                    strInout1 += "-";
                }

                sb.Append(strInout1);
                //sb.Append("</td>");
                //sb.Append("<td>");

                object objTotalHours1 = DatabaseHelper.executeScalar("select TotalHrs from tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "'  and EodcDate='" + ds.Tables[0].Rows[0]["Date_of_Monday"].ToString().Trim() + "'");
                if (objTotalHours1 != null && objTotalHours1.ToString() != "")
                {
                    string totime = "";
                    string[] strbreak = objTotalHours1.ToString().Split(':');
                    totime = strbreak[0] + ":" + strbreak[1];
                    sb.Append("--" + totime);
                   //sb.Append("-" + objTotalHours1.ToString());
                    
                }
                else
                {
                    sb.Append("-");

                }

                sb.Append("</td>");
               
                string firstuserdate1 = "";
                if (GMDStartDate.Text != "")
                {
                    firstuserdate1 = @"Select CONVERT(VARCHAR(10),AbsentDate,103) as LeaveDate from tbl_LeaveDetails where UserName='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and  (AbsentDate >=DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text + "'),0) and AbsentDate <= DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text + "'),0)+4)";
                }
                else
                {
                    firstuserdate1 = @"Select CONVERT(VARCHAR(10),AbsentDate,103)  as LeaveDate from tbl_LeaveDetails where UserName='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and (AbsentDate >=DATEADD(week, DATEDIFF(week, 0,GETDATE()),0) and AbsentDate <= DATEADD(week, DATEDIFF(week, 0,GETDATE()),0)+4)";
                }
                DataSet dsFirstdate = DatabaseHelper.getDataset(firstuserdate1);
                sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("</td>");
                //----------------------------------------------------------------
                //----------------------------------------------------------------
                if (druser.SelectedItem.Text == "PHG")
                {
                    sb.Append("<td width='20%'>");
                }
                else
                {
                    sb.Append("<td width='17%'>");
                }
                sb.Append("<table width='100%' border='0' height='100%' cellpadding='0' cellspacing='0'>");
                sb.Append("<tr>");
                sb.Append("<td colspan='2'>");
                
                string strInout2 = "";
                object objIntimeALL2 = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Tuesday"].ToString() + "'");
                if (objIntimeALL2 != null && objIntimeALL2.ToString() != "")
                {
                    DateTime dtALL2 = Convert.ToDateTime(objIntimeALL2).AddHours(-5).AddMinutes(-30);

                    strInout2 = dtALL2.ToString("HH:mm").ToString() + "/"; 
                }
                else
                {
                   
                    strInout2="-/";
                }

                object objOuttimeALL2 = DatabaseHelper.executeScalar("select Outtime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Tuesday"].ToString() + "'");
                if (objOuttimeALL2 != null && objOuttimeALL2.ToString() != "")
                {
                    DateTime dtoutALL2 = Convert.ToDateTime(objOuttimeALL2).AddHours(-5).AddMinutes(-30);

                    strInout2 += dtoutALL2.ToString("HH:mm");
                }
                else
                {
                   
                    strInout2 += "-";
                }

                sb.Append(strInout2);

                //sb.Append("</td>");
                //sb.Append("<td>");

                object objTotalHours2 = DatabaseHelper.executeScalar("select TotalHrs from tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and EodcDate='" + ds.Tables[0].Rows[0]["Date_of_Tuesday"].ToString().Trim() + "'");
                if (objTotalHours2 != null && objTotalHours2.ToString() != "")
                {
                    string totime2 = "";
                    string[] strbreak2 = objTotalHours2.ToString().Split(':');
                    totime2 = strbreak2[0] + ":" + strbreak2[1];
                    sb.Append("--" + totime2);
                    //sb.Append("-" + objTotalHours2.ToString());
                }
                else
                {

                    sb.Append("-");

                }
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("</td>");

                //---------------------------------------------------------------
                //----------------------------------------------------------------
                string strInout3 = "";




                if (druser.SelectedItem.Text == "PHG")
                {
                    sb.Append("<td width='20%'>");
                }
                else
                {
                    sb.Append("<td width='17%'>");
                }



                sb.Append("<table width='100%' border='0' height='100%' cellpadding='0' cellspacing='0'>");
                sb.Append("<tr>");
                sb.Append("<td colspan='2'>");
                object objIntimeALL3 = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Wednesday"].ToString() + "'");
                if (objIntimeALL3 != null && objIntimeALL3.ToString() != "")
                {
                    DateTime dtALL3 = Convert.ToDateTime(objIntimeALL3).AddHours(-5).AddMinutes(-30);
                
                    strInout3=dtALL3.ToString("HH:mm tt")+"/";
                    
                }
                else
                {
                   
                    strInout3 = "-/";
                }
                object objOuttimeALL3 = DatabaseHelper.executeScalar("select Outtime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Wednesday"].ToString() + "'");
                if (objOuttimeALL3 != null && objOuttimeALL3.ToString() != "")
                {
                    DateTime dtoutALL3 = Convert.ToDateTime(objOuttimeALL3).AddHours(-5).AddMinutes(-30);

                    strInout3 += dtoutALL3.ToString("HH:mm");
                }
                else
                {
                   
                    strInout3 += "-";
                }
                sb.Append(strInout3);

                //sb.Append("</td>");
                //sb.Append("<td>");



                object objTotalHours3 = DatabaseHelper.executeScalar("select TotalHrs from tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and EodcDate='" + ds.Tables[0].Rows[0]["Date_of_Wednesday"].ToString().Trim() + "'");
                if (objTotalHours3 != null && objTotalHours3.ToString() != "")
                {
                    string totime3 = "";
                    string[] strbreak3 = objTotalHours3.ToString().Split(':');
                    totime3 = strbreak3[0] + ":" + strbreak3[1];
                    sb.Append("--" + totime3);
                    //sb.Append("-" + objTotalHours3.ToString());
                }
                else
                {
                    sb.Append("-");
                }
                
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("</td>");

                //---------------------------------------------------------------
                //----------------------------------------------------------------
                string strInout4="";


                if (druser.SelectedItem.Text == "PHG")
                {
                    sb.Append("<td width='20%'>");
                }
                else
                {
                    sb.Append("<td width='17%'>");
                }





                sb.Append("<table width='100%' border='0' height='100%' cellpadding='0' cellspacing='0'>");
                sb.Append("<tr>");
                sb.Append("<td colspan='2'>");

                object objIntimeALL4 = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Thusday"].ToString() + "'");
                if (objIntimeALL4 != null && objIntimeALL4.ToString() != "")
                {
                    DateTime dtALL4 = Convert.ToDateTime(objIntimeALL4).AddHours(-5).AddMinutes(-30);

                    strInout4 = dtALL4.ToString("HH:mm") + "/";
                }
                else
                {
                    
                    strInout4 = "-/";
                }

                object objOuttimeALL4 = DatabaseHelper.executeScalar("select Outtime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Thusday"].ToString() + "'");
                if (objOuttimeALL4 != null && objOuttimeALL4.ToString() != "")
                {
                    DateTime dtoutALL4 = Convert.ToDateTime(objOuttimeALL4).AddHours(-5).AddMinutes(-30);

                    strInout4 += dtoutALL4.ToString("HH:mm");
                }
                else
                {
                    
                    strInout4 += "-";
                }

                sb.Append(strInout4);

                //sb.Append("</td>");
                //sb.Append("<td>");


                object objTotalHours4 = DatabaseHelper.executeScalar("select TotalHrs from tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and EodcDate='" + ds.Tables[0].Rows[0]["Date_of_Thusday"].ToString().Trim() + "'");
                if (objTotalHours4 != null && objTotalHours4.ToString() != "")
                {
                    string totime4 = "";
                    string[] strbreak4 = objTotalHours4.ToString().Split(':');
                    totime4 = strbreak4[0] + ":" + strbreak4[1];
                    sb.Append("--" + totime4);
                    //sb.Append("-" + objTotalHours4.ToString());
                }
                else
                {
                    sb.Append("-");
                }
                
                sb.Append("</td>");
                 sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("</td>");
                //----------------------------------------------------------------
                //----------------------------------------------------------------
                string strInout5 = "";


                if (druser.SelectedItem.Text == "PHG")
                {
                    sb.Append("<td width='20%'>");
                }
                else
                {
                    sb.Append("<td width='17%'>");
                }




                sb.Append("<table width='100%' border='0' height='100%' cellpadding='0' cellspacing='0'>");
                sb.Append("<tr>");
                sb.Append("<td colspan='2'>");

                object objIntimeALL5 = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString() + "'");
                if (objIntimeALL5 != null && objIntimeALL5.ToString() != "")
                {
                    DateTime dtALL5 = Convert.ToDateTime(objIntimeALL5).AddHours(-5).AddMinutes(-30);

                    strInout5 = dtALL5.ToString("HH:mm") + "/";
                }
                else
                {
                    
                    strInout5 = "-/";
                }
                object objOuttimeALL5 = DatabaseHelper.executeScalar("select Outtime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString() + "'");
                if (objOuttimeALL5 != null && objOuttimeALL5.ToString() != "")
                {
                    DateTime dtoutALL5 = Convert.ToDateTime(objOuttimeALL5).AddHours(-5).AddMinutes(-30);

                    strInout5 += dtoutALL5.ToString("HH:mm");
                }
                else
                {
                    
                    strInout5 += "-";
                }
                sb.Append(strInout5);
                //sb.Append("</td>");
                //sb.Append("<td>");

                object objTotalHours5 = DatabaseHelper.executeScalar("select TotalHrs from tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and EodcDate='" + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString().Trim() + "'");
                if (objTotalHours5 != null && objTotalHours5.ToString() != "")
                {
                    string totime5 = "";
                    string[] strbreak5 = objTotalHours5.ToString().Split(':');
                    totime5 = strbreak5[0] + ":" + strbreak5[1];
                    sb.Append("--" + totime5);
                    //sb.Append("-" + objTotalHours5.ToString());
                }
                
                sb.Append("</td>");
                
                sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("</td>");
                //----------------------------------------------------------------
                //----------------------------------------------------------------
                if (druser.SelectedItem.Text != "PHG")
                {
                    string strInout6 = "";
                    sb.Append("<td width='20%'>");
                    sb.Append("<table width='100%' border='0' height='100%' cellpadding='0' cellspacing='0'>");
                    sb.Append("<tr>");
                    sb.Append("<td colspan='2'>");


                    object objIntimeALL6 = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString() + "'");
                    if (objIntimeALL6 != null && objIntimeALL6.ToString() != "")
                    {
                        DateTime dtALL6 = Convert.ToDateTime(objIntimeALL6).AddHours(-5).AddMinutes(-30);

                        strInout6 = dtALL6.ToString("HH:mm") + "/";
                    }
                    else
                    {

                        strInout6 = "-/";
                    }
                    object objOuttimeALL6 = DatabaseHelper.executeScalar("select Outtime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString() + "'");
                    if (objOuttimeALL6 != null && objOuttimeALL6.ToString() != "")
                    {
                        DateTime dtoutALL6 = Convert.ToDateTime(objOuttimeALL6).AddHours(-5).AddMinutes(-30);

                        strInout6 += dtoutALL6.ToString("HH:mm");
                    }
                    else
                    {

                        strInout6 += "-";
                    }
                    sb.Append(strInout6);
                    //sb.Append("</td>");
                    //sb.Append("<td>");

                    object objTotalHours6 = DatabaseHelper.executeScalar("select TotalHrs from tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and EodcDate='" + ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString().Trim() + "'");
                    if (objTotalHours6 != null && objTotalHours6.ToString() != "")
                    {
                        string totime6 = "";
                        string[] strbreak6 = objTotalHours6.ToString().Split(':');
                        totime6 = strbreak6[0] + ":" + strbreak6[1];
                        sb.Append("--" + totime6);
                        //sb.Append("-" + objTotalHours6.ToString());
                    }

                    sb.Append("</td>");
                    sb.Append("</tr>");
                    sb.Append("</table>");
                    sb.Append("</td>");
                }


                    sb.Append("</table>");
                    sb.Append("</td>");
                
                //******************************************* End Create In Time ******************************************************************
                sb.Append("</tr>");
            }
            sb.Append("</table>");
            HtmlDesign.InnerHtml = sb.ToString();
        }
    
    }
    protected void btnshow_Click(object sender, EventArgs e)
    {

        string strdate = @"SELECT  CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0),103) as Date_of_Monday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+1,103) as Date_of_Tuesday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+2,103) as Date_of_Wednesday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+3,103) as Date_of_Thusday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+4,103) as Date_of_Friday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+5,103) as Date_of_Saturday";

        DataSet ds = DatabaseHelper.getDataset(strdate);
        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            clear();

           

            strreportname = ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "_To_" + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString();
            dynamically(ds);

           
        }
        //showAttendence();
    }
    protected void btnexport_Click(object sender, EventArgs e)
    {
        Response.ContentType = "application/x-msexcel";
        Response.AddHeader("Content-Disposition", "attachment; filename=Timesheet_report_from_'" + strreportname + "'.xls");
        //Response.ContentEncoding = Encoding.UTF8;
        System.IO.StringWriter tw = new System.IO.StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(tw);
        HtmlDesign.RenderControl(hw);
        Response.Write(tw.ToString());
        Response.End();
    }

    protected void druser_SelectedIndexChanged(object sender, EventArgs e)
    {
        string strdate="";
        if (GMDStartDate.Text.ToString() == "")
        {
            strdate = @"SELECT 
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0),103)   as Date_of_Monday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+1,103) as Date_of_Tuesday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+2,103) as Date_of_Wednesday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+3,103) as Date_of_Thusday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+4,103) as Date_of_Friday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+5,103) as Date_of_Saturday";
        }
        else
        {
            strdate = @"SELECT  CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0),103) as Date_of_Monday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+1,103) as Date_of_Tuesday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+2,103) as Date_of_Wednesday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+3,103) as Date_of_Thusday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+4,103) as Date_of_Friday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+5,103) as Date_of_Saturday";
        
        }
        DataSet ds = DatabaseHelper.getDataset(strdate);
        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
          dynamically(ds);
          if (druser.SelectedItem.Text == "PHG")
          {
              strreportname = ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "_To_" + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString();
          }
          else
          {
              strreportname = ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "_To_" + ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString();
          }
        }
    }
}
