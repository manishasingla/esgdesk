﻿<%@ Page Language="C#" AutoEventWireup="true" EnableViewState="true" CodeFile="EmployeesReports.aspx.cs"
    Inherits="Admin_grid" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

    protected void DataGrid1_ItemCommand(object source, DataGridCommandEventArgs e)
    {

    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Employees report</title>
    <link rel="stylesheet" type="text/css" href="slider.css" />
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <center>
        <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                    <ProgressTemplate>
                        <div id="processmessage" class="processMessage">
                            <img alt="loading" src="images/ajax-loader.gif" />
                            <span style="color: blue; font-weight: bold; font-size: 13px; vertical-align: super;">
                                Please wait... </span>
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
--%>
        <div style="width: 100%">
            <div style="padding-top: 18px; width: 98%; height: 40px;">
                <div style="float: left; margin-left: 10px">
                    Days:&nbsp;<asp:DropDownList ID="ddldays" runat="server" Style="width: 175px" AutoPostBack="true"
                        OnSelectedIndexChanged="ddldays_SelectedIndexChanged">
                        <asp:ListItem Value="0">--Select--</asp:ListItem>
                        <asp:ListItem Value="1">Today</asp:ListItem>
                        <%--<asp:ListItem>last 24 hours</asp:ListItem>--%>
                        <asp:ListItem Value="2">Yesterday</asp:ListItem>
                        <asp:ListItem Value="3">Last 7 days</asp:ListItem>
                        <asp:ListItem Value="4">Last 31 days</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div style="float: left; margin-left: 10px">
                    Employees names:&nbsp;<asp:DropDownList ID="ddlemp" runat="server" Style="width: 175px"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlemp_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
            </div>
            <div style="width: 98%;">
                <asp:DataGrid ID="DataGrid1" runat="server" Width="100%" AutoGenerateColumns="False"
                    AllowSorting="True" CellPadding="5" BorderColor="#cccccc" OnItemDataBound="DataGrid1_ItemDataBound"
                    OnSortCommand="DataGrid1_SortCommand" OnItemCommand="DataGrid1_ItemCommand">
                    <HeaderStyle Font-Bold="false" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Top" BackColor="#282c5f"
                        CssClass="tblTitle1" />
                    <Columns>
                        <asp:BoundColumn DataField="assigned_to_user" HeaderText="Assigned to" SortExpression="assigned_to_user">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="task_id" HeaderText="ID" SortExpression="task_id" Visible="false">
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="ID">
                            <ItemTemplate>
                                <a href="edit_task.aspx?id=<%# Eval("task_id")%>"><asp:Label ID="lblLastUpdatedOn" runat="server" Text='<%# bind("task_id") %>'></asp:Label></a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <%--<asp:ButtonColumn DataTextField="task_id" HeaderText="ID" SortExpression="task_id"
                            CommandName="link"></asp:ButtonColumn>--%>
                        <asp:BoundColumn DataField="last_updated_user" HeaderText="Last Updated By" SortExpression="last_updated_user">
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="last updated on" SortExpression="last_updated_date">
                            <ItemTemplate>
                                <asp:Label ID="lblLastUpdatedOn" runat="server" Text='<%# bind("last_updated_date") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn HeaderText="ETC" DataField="ETC">
                            <HeaderStyle Width="85px" />
                        </asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Time to date">
                            <HeaderStyle Width="85px" />
                        </asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Balance">
                            <HeaderStyle Width="90px" />
                        </asp:BoundColumn>
                        <%--<asp:BoundColumn DataField="last_updated_date" HeaderText="LU" Visible="false"></asp:BoundColumn>--%>
                    </Columns>
                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Top" Font-Size="12px" />
                </asp:DataGrid>
                <asp:Label ID="lblError" runat="server" Visible="false" Style="font-size: large;
                    color: Red"></asp:Label>
            </div>
            <div>
                <asp:Label ID="lblOrderBy" runat="server" Text="" Visible="false"></asp:Label></div>
            <div style="clear: both">
            </div>
        </div>
        <%-- </ContentTemplate>
        </asp:UpdatePanel>--%>
    </center>
    </form>
</body>
</html>
