﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Admin_grid : System.Web.UI.Page
{
    string sql = "";
    string qureystring = "";
    DataSet ds;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                DataSet empname = DatabaseHelper.getEmpsName();
                if (empname != null && empname.Tables[0].Rows.Count > 0)
                {
                    ddlemp.DataSource = empname.Tables[0];
                    ddlemp.DataTextField = "assigned_to_user";
                    ddlemp.DataValueField = "assigned_to_user";
                    ddlemp.DataBind();
                }
                if (Request.QueryString.Get("id") != null && Request.QueryString.Get("id") != null)
                {
                    qureystring = Request.QueryString.Get("id");
                    sql = @"select assigned_to_user,last_updated_user,last_updated_date,ETC,a.task_id from  tasks a join  hours_reporting b ON a.task_id = b.task_id where a.task_id ='" + qureystring + "'order by last_updated_date  desc";
                    ds = DatabaseHelper.getDataset(sql);
                    if (ds != null && ds.Tables[0].Rows.Count > 0)
                    {
                        ddlemp.SelectedItem.Text = ds.Tables[0].Rows[0]["assigned_to_user"].ToString();
                        DataGrid1.DataSource = ds.Tables[0];
                        DataGrid1.DataBind();
                    }
                }


            }
        }
        catch
        {
        }
    }
    protected int getHoursTakenSoFar(string task_id)
    {

        sql = @"select sum(datediff(minute,started_date,finished_date))
                from hours_reporting
                where finished_date is not null and task_id=" + task_id;

        int hrsTaken = 0;
        object objHrsTaken = DatabaseHelper.executeScalar(sql);
        if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
        {
            hrsTaken += (int)objHrsTaken;
        }
        else
        {
            hrsTaken = 0;
        }

        sql = @"select sum(datediff(minute,started_date,getdate()))
                from hours_reporting
                where finished_date is null and task_id=" + task_id;

        objHrsTaken = DatabaseHelper.executeScalar(sql);

        if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
        {
            hrsTaken += (int)objHrsTaken;
        }

        return hrsTaken;

    }

    protected void DataGrid1_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemIndex != -1)
            {
                int ETC = Convert.ToInt32(e.Item.Cells[5].Text);

                e.Item.Cells[5].Text = "" + (ETC / 60) + "' " + (ETC % 60) + '"';

                int hrsTaken = getHoursTakenSoFar(e.Item.Cells[3].Text);

                e.Item.Cells[6].Text = "" + (hrsTaken / 60) + "' " + (hrsTaken % 60) + '"';

                int hrsLeft = ETC - hrsTaken;

                if (hrsLeft < 0)
                {
                    hrsLeft = -1 * hrsLeft;
                    e.Item.Cells[7].Text = "-" + (hrsLeft / 60) + "' " + (hrsLeft % 60) + '"';
                }
                else
                {
                    e.Item.Cells[7].Text = "" + (hrsLeft / 60) + "' " + (hrsLeft % 60) + '"';
                }

            }
        }
        catch
        {
        }
    }
    protected void ddldays_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {

            if (ddldays.SelectedItem.Value == "1" || ddldays.SelectedItem.Value == "2" || ddldays.SelectedItem.Value == "3" || ddldays.SelectedItem.Value == "4")
            {
                string dcount = "";

                if (ddldays.SelectedItem.Value == "1")
                {
                    dcount = "0";
                }
                else if (ddldays.SelectedItem.Value == "2")
                {
                    dcount = "1";
                }
                else if (ddldays.SelectedItem.Value == "3")
                {
                    dcount = "7";
                }
                else
                {
                    dcount = "31";
                }
                bind();

                double dday = Convert.ToDouble(dcount);
                DateTime sd = DateTime.Parse(System.DateTime.Now.ToShortDateString(), System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat);

                DateTime ed = sd;
                ed = ed.AddDays(-dday);
                DataView dv1 = new DataView(ds.Tables[0]);
                DataTable dt = dv1.ToTable();
                DataView dv = new DataView(dt);
                string ss = sd.ToString("MMMM/dd/yyyy");
                string ee = ed.ToString("MMMM/dd/yyyy");
                if (sd != null && ed != null)
                {
                    if (ddldays.SelectedItem.Value == "1")
                    {

                        dv.RowFilter = "assigned_to_user='" + ddlemp.SelectedItem.Text + "' and Date =#" + ss + "#";
                    }
                    else if (ddldays.SelectedItem.Value == "2")
                    {

                        dv.RowFilter = "assigned_to_user='" + ddlemp.SelectedItem.Text + "' and Date =#" + ee + "#";
                    }
                    else
                    {
                        dv.RowFilter = "assigned_to_user='" + ddlemp.SelectedItem.Text + "' and Date <=#" + ss + "# and Date>=#" + ee + "#";
                    }
                }
                if (dv.Count >= 1)
                {
                    DataGrid1.DataSource = null;
                    DataGrid1.DataBind();
                    DataGrid1.DataSource = dv.ToTable();
                    DataGrid1.DataBind();
                    // Response.Write("Record found");
                    if (DataGrid1.Visible == false)
                    {

                        DataGrid1.Visible = true;
                    }

                    if (lblError.Visible == true)
                    {
                        lblError.Visible = false;
                    }

                }
                else
                {
                    DataGrid1.DataSource = null;
                    DataGrid1.DataBind();
                    DataGrid1.DataSource = dv.ToTable();
                    DataGrid1.DataBind();
                    if (DataGrid1.Visible == true)
                    {

                        DataGrid1.Visible = false;
                    }
                    lblError.Text = "Record not found";
                    if (lblError.Visible == false)
                    {
                        lblError.Visible = true;
                    }
                }

            }
        }
        catch
        {

        }
    }
    protected void ddlemp_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            ddldays.SelectedIndex = 0;
            bind();
        }
        catch
        {
        }
    }

    private void bind()
    {
        try
        {
            if (DataGrid1.Items.Count > 0)
            {
                DataGrid1.DataSource = null;
                DataGrid1.DataBind();
            }
            sql = @"select assigned_to_user,last_updated_user,last_updated_date,ETC,a.task_id from  tasks a join  hours_reporting b ON a.task_id = b.task_id where a.assigned_to_user ='" + ddlemp.SelectedItem.Text + "'order by last_updated_date  desc";
            ds = DatabaseHelper.getDataset(sql);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                ds.Tables[0].Columns.Add("Date");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (ds.Tables[0].Rows[i]["last_updated_date"] != null && ds.Tables[0].Rows[i]["last_updated_date"].ToString().Trim() != "")
                    {
                        DateTime dt = Convert.ToDateTime(ds.Tables[0].Rows[i]["last_updated_date"].ToString());
                        string date = dt.ToString("MMMM/dd/yyyy");
                        ds.Tables[0].Rows[i]["Date"] = date;

                    }

                }

                ddlemp.SelectedItem.Text = ds.Tables[0].Rows[0]["assigned_to_user"].ToString();
                DataGrid1.DataSource = ds.Tables[0];
                DataGrid1.DataBind();
                if (DataGrid1.Visible == false)
                {
                    DataGrid1.Visible = true;
                }
                if (lblError.Visible == true)
                {
                    lblError.Visible = false;
                }
                // DataGrid1.DataBind();
            }
            else
            {
                if (DataGrid1.Visible == true)
                {

                    DataGrid1.Visible = false;
                }
                lblError.Text = "Record not found";
                if (lblError.Visible == false)
                {
                    lblError.Visible = true;
                }
            }
        }
        catch
        {

        }
    }
    protected void DataGrid1_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        try
        {
            if (e.SortExpression.ToString() == Session["Column"])
            {

                if (Session["Order"] == "ASC")
                {
                    lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " DESC";
                    Session["Order"] = "DESC";
                }
                else
                {
                    lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " ASC";
                    Session["Order"] = "ASC";
                }
            }
            else
            {

                lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " ASC";
                Session["Order"] = "ASC";
            }

            Session["Column"] = e.SortExpression.ToString();
            Session["OrderBy"] = lblOrderBy.Text;
            bind();
        }
        catch { }
    }

}
