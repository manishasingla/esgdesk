using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

public partial class Admin_AdminMasterMenu : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null)
        {
            Response.Redirect("~/Admin/login.aspx");

        }
        else
        {
            //if (DatabaseHelper.isAdmin(Session["admin"].ToString()))
            //{
            //    createAlbumMenu.Visible = true;
            //}
            Session["username"] = Session["admin"].ToString();
        }
    }


    protected void LnkCrntlywrkTask_Click(object sender, EventArgs e)
    {
        Session["filter"] = "CWO";
        Session["filterunread"] = "";
        Response.Redirect("tasks.aspx");

    }

    protected void lnkClientRequests_Click(object sender, EventArgs e)
    {
        Session["filter"] = "";
        Session["filter"] = null;
        Session["reqHideClosed"] = "False"; ;
        Session["reqViewDeleted"] = "False";
        Session["reqChangedBy"] = null;
        Session["reqStatus"] = null;
        Session["reqPriority"] = null;
        Session["reqWebsiteURL"] = null;
        Session["reqOrderBy"] = null;
        Response.Redirect("client_requests.aspx", false);
    }
    protected void lnkTasks_Click(object sender, EventArgs e)
    {
        Session["filter"] = "";
        Session["filter"] = null;
        Session["filterunread"] = "";
        Session["filterunread"] = null;
        Session["Status"] = null;
        Session["Status"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;


        Session["HideClosed"] = "False";
        Session["IncludeDeleted"] = "False";

        Session["Project"] = null;
        Session["Category"] = null;
        Session["ReportedBy"] = null;
        Session["Priority"] = null;
        Session["AssignedTo"] = null;
        Session["OrderBy"] = null;
        Session["ReadTask"] = "True";
        Response.Redirect("tasks.aspx", false);
    }
    //protected void LinkButton1_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        string StrDesktopDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory).ToString();
    //        using (StreamWriter ShortcutWriter = new StreamWriter(StrDesktopDirectory + "\\Change_Request_System.url"))
    //        {
    //            ShortcutWriter.WriteLine("[InternetShortcut]");
    //            ShortcutWriter.WriteLine("URL=www.request.estatesolutions.eu");
    //            ShortcutWriter.Flush();

    //        }
    //    }
    //    catch (Exception ex)
    //    {

    //        LinkButton1.Text = ex.ToString();
    //    }
    //}




}
