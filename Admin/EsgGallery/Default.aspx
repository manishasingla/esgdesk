﻿<%@ Page Language="C#" MasterPageFile="~/Admin/EsgGallery/AdminMasterMenu.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" Title="Gallery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script type="text/javascript">

       function setListingsImage(obj) 
       {
           var img = new Image();
           img.src = obj.src;
           if (img.height > img.width) 
           {
               obj.height = 220;
               var width = (200 * img.width) / img.height;
               width = width > 220 ? 220 : width;
               obj.width = (width > 0 ? width : 220);
           }
           else 
           {
               obj.width = 200;
               var height = (220 * img.height) / img.width;
               height = height > 200 ? 200 : height;
               obj.height = (height > 0 ? (height - 6) : 220);

           }
          
       }

        

    </script>
    
  <style type="text/css">
  .infoblock {
    background: none repeat scroll 0 0 #FFFFFF;
    border: 1px solid #E9EAED;
    height: 277px;
    margin: 0 0 0 10px;
  /*  padding: 1px 8px 1px 1px;*/
    width: 210px;
}

.header002{
	height:80px;
	background-color:#f6f7f8;
	border:1px solid #d3d6db;
	margin-top:10px;
	margin-bottom:10px;}


.heading  {
     text-align:left;
    /*text-shadow: 1px 0 0 rgba(0, 0, 0, 0.25);
	border-radius: 6px 6px 0 0;
    text-transform: uppercase;*/
	letter-spacing: 1px;
    margin: -13px 0 0 5px;
    color: #3e3f3d;
    font-size: 12px;
}
  </style>  
    
    <center>
  <%--  <h2>My Albums</h2>--%>
  
  
  <div align="left">
     <%-- <asp:LinkButton ID="LinkButton1" runat="server">Create new album</asp:LinkButton>
     <a href="createalbum.aspx">Create new album</a>--%>
     
 <div class="header002">
<img style="padding-left: 15px; padding-top: 9px; float:left;" src="images/map.png" align="absmiddle" /> <span style="color: rgb(51, 51, 51); float: left; padding-top: 10px; font-size: 18px; float:left; margin-left: 8px;">Photos</span>
<span><img style="float:right; margin-top:10px;"src="images/gallery-btn.png" /></span>
<div style="font-weight: bold; color:#00a651; margin-left: 15px; position: absolute; margin-top: 45px;"> <strong style="color:#000000;">Photos</strong> &nbsp Albums</div>
 </div>
  <div>
    <asp:ListView ID="ListView1" runat="server" 
        GroupItemCount ="6" ondatabound="ListView1_DataBound" >
     <LayoutTemplate>
     <center>
      <table runat="server">
 
       <tr runat="server" id="groupPlaceholder"/>
      </table>
      <p />
         <asp:DataPager ID="DataPager1" runat="server" PagedControlID ="ListView1">
          <Fields>
           <asp:NextPreviousPagerField  ShowNextPageButton ="false" ShowPreviousPageButton ="true" Buttontype="Link" PreviousPageText="<" />
           <asp:NumericPagerField />
           <asp:NextPreviousPagerField  ShowPreviousPageButton ="false" ShowNextPageButton ="true" Buttontype="Link" NextPageText=">" />
          </Fields>
         </asp:DataPager>
      </center>
     </LayoutTemplate>
     
     <GroupTemplate>
         
         <tr runat="server">
          <td id="itemPlaceholder" runat="server" />
         </tr>
     </GroupTemplate>
    
     <ItemTemplate>
         
          <td>
            <div class="infoblock">
            <a style="color:#d1d1d1;" href='showalbum.aspx?aid=<%# Eval("aid")%>&atitle=<%# Eval("title") %>'>
            <img src='photos/<%# Eval("title")%>/<%# Eval("firstphoto")%>.jpg' style="max-width: 210px; width: 220px; min-width: 200px; height: 220px; max-height: 200px; min-height: 200px; border-bottom: 1px solid #E9EAED;"  onload="javascript:setListingsImage(this)" id="AlbumCover" OnLoad="setCheckBoxValue(this)" onerror="this.src='photos/image_small.jpg'" border="0" /> </a>
            &nbsp;&nbsp;&nbsp;&nbsp;
           <div class="heading">
            <span style="font-weight:bold; color:#282C5F;"><%#Eval("title")%> (<%#Eval("nophotos")%>)</span>
            <br />
             <%#Eval("eventdate")%> <img style="margin-top: 16px; margin-left: 56px;" src="images/processing.png" align="absmiddle" />
             <br />
            <asp:Panel runat="server" ID="innerPanel" Visible="false">
            <a href='uploadphotos.aspx?aid=<%# Eval("aid")%>&atitle=<%# Eval("title")%>'>Upload photos</a>
            <br />
           <%-- <a href='sharealbum.aspx?aid=<%# Eval("aid")%>'>Share Album</a>--%>
            </asp:Panel></div>
          
            </div>
           <div style="height:20px;"></div>
           </td>
    
              
     </ItemTemplate>
    
     <EmptyDataTemplate>
       <h4>You have no albums currently.</h4>
     </EmptyDataTemplate>
    </asp:ListView>
    </div>
    
 </div>   
</center>    
    
  <%--  
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
        SelectMethod="GetAlbums" TypeName="PhotosDAL">
       <SelectParameters>
            <asp:SessionParameter Name="username" SessionField="username" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>--%>
</asp:Content>


