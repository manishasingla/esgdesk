﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Session["username"] = "rajesh";
        fnBindData();
    }
    protected void ListView1_DataBound(object sender, EventArgs e)
    {
        try
        {
            DataPager pager = (DataPager)ListView1.FindControl("DataPager1");
            if (pager != null)
                pager.Visible = (pager.PageSize < pager.TotalRowCount);

            if (Session["admin"] != null && Session["admin"].ToString() != "")
            {

                if (DatabaseHelper.isAdmin(Session["admin"].ToString()))
                {
                    foreach (ListViewItem item in ListView1.Items)
                    {
                        var innerpnl = item.FindControl("innerPanel") as Panel;

                        innerpnl.Visible = true;
                    }
                }

            }
            else
            {
                Response.Redirect("~/Admin/login.aspx");
                return;
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }

    }
    public void fnBindData()
    {
        try
        {
            DataTable dt = new DataTable();
            dt = PhotosDAL.GetAlbums();
            if (dt != null && dt.Rows.Count > 0)
            {
                ListView1.Visible = true;
                ListView1.DataSource = dt;
                ListView1.DataBind();
            }
        }
        catch (Exception ex)
        {
        }
    }
}
