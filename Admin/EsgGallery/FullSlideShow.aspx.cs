﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;

public partial class full_slide_show : System.Web.UI.Page
{
    public string images;
    public string title = "";
   
  //  public static string WebAdd4Rewrite = ConfigurationManager.AppSettings["WebAdd4Rewrite"].ToString();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] != null && Session["admin"].ToString() != "")
        {
            string title = "";
            if (Request.QueryString["aid"] != null && Request.QueryString["atitle"] != null)
            {
                string aid = Request.QueryString["aid"].ToString();
                title = Request.QueryString["atitle"].ToString();
                if (aid != null)
                    Session.Add("aid", aid);
                if (title != null)
                    Session.Add("atitle", title);
                getPropertyDetails();
            }
            else
            {
                Response.Write("Parameters mismatched !");
                Response.Redirect("Default.aspx");
            }
        }
        else
        {
            Response.Redirect("~/Admin/login.aspx");
            return;
        }
    }

    private void getPropertyDetails()
    {
        try
        {
            DataTable dt = PhotosDAL.GetPhotosFromAlbum(Session["aid"].ToString());

            //localhost:51629/Esgdesk New/

            string ImageSlide = "http://www.esgdesk.com/Admin/EsgGallery/photos/"; ;
          //  string ImageThumb = ConfigurationManager.AppSettings["ThumbImagePath"];
            string mainImage = dt.Rows[0]["photoid"].ToString().Trim();

            mainImage = (mainImage == "" ? "photos/image_soon.jpg" : mainImage);
            title = Session["atitle"].ToString();

            if (mainImage.ToString() != "")
            {
                mainImage = mainImage + ".jpg";
                string[] strimagename = mainImage.Split(new char[] { '.' });
                string strImage = strimagename[0];
                string strexten = strimagename[1];
               
                mainImage = mainImage != "" ? ImageSlide +title+"/"+ strImage + "." + strexten : "/image_soon.jpg";
            }
            else
            {
                mainImage = "photos/image_soon.jpg";
            }

            images = "{image : '" + mainImage + "', title : '', thumb : '" + mainImage + "', url : 'http://www.nonsensesociety.com/2011/04/maria-kazvan/'}";
          //  images = "{image : '" + mainImage.Replace("_thumb", "_slide") + "', title : '', thumb : '" + ds.Tables[0].Rows[0]["image"].ToString() + "', url : 'http://www.nonsensesociety.com/2011/04/maria-kazvan/'}";
         

            for (int i = 1; i < dt.Rows.Count; i++)
            {
                string str = "http://www.esgdesk.com/Admin/EsgGallery/photos/"+title+"/"+dt.Rows[i]["photoid"].ToString().Trim() + ".jpg";
                str = (str == "" ? "http://www.esgdesk.com/Admin/EsgGallery/photos/" + "image_soon.jpg" : str);
                if (str != "")
                {
                    if (images == "")
                        images += "{image : '" + str + "', title : '', thumb : '" + str + "', url : 'http://www.esgdesk.com/'}";
                    else
                        images += ",{image : '"  + str + "', title : '', thumb : '" + str + "', url : 'http://www.esgdesk.com/'}";
                }
            }
        }


        catch { Response.Redirect("Default.aspx", false); }
    }
}
