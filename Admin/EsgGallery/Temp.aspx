﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="propertylistdetails.aspx.cs"
    Inherits="TestMap_propertylistdetails" %>

<%@ Register Src="Header.ascx" TagName="Header" TagPrefix="uc1" %>
<%@ Register Src="Footer.ascx" TagName="Footer" TagPrefix="uc2" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= System.Configuration.ConfigurationManager.AppSettings["companyname"] %>
    </title>
    <script type="text/javascript" src="js/script.js"></script>
    <script type="text/javascript" src="js/featprop.js"></script>
    <script src="js/localmap.js" type="text/javascript"></script>
    <%--<script type='text/javascript' src='http://maps.google.com/maps?file=api&amp;v=2.x&amp;key=ABQIAAAAoBHdtUE2YPiacUuBgkb7VRT7X15Kd2x6LRiVy22RhmHvWbmWFhT5y5wqeqwAF3IjVOd0GyL91w_vrg'></script>--%>
    <script type='text/javascript' src='http://maps.google.com/maps?file=api&amp;v=2.x&amp;key=<%= System.Configuration.ConfigurationManager.AppSettings["gKey"] %>'></script>
    <style type="text/css">
      
        
        
        .Slash
        {
            max-width: 316px;
            max-height: 316px;
            margin: 0 auto;
        }
        .SplashFrame
        {
            padding-top: 3px;
            padding-right: 3px;
            padding-bottom: 3px;
            padding-left: 3px;
            max-width: 316px;
            max-height: 316px;
            border: 0px outset #999999;
            cursor: pointer;
            margin: 0 auto;
        }
        .SlashEff
        {
        }
        
        .Slash1
        {
            max-width: 500px;
            max-height: 500px;
            margin: 0 auto;
        }
        .SplashFrame1
        {
            padding-top: 3px;
            padding-right: 3px;
            padding-bottom: 3px;
            padding-left: 3px;
            max-width: 500px;
            max-height: 500px;
            border: 0px outset #999999;
            cursor: pointer;
            margin: 0 auto;
        }
        .SlashEff1
        {
        }
    </style>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/Groject.ImageSwitch.102.yui.js"></script>
    <script type="text/javascript" language="javascript">

        function fullScreen(theURL) {


            window.open(theURL, '', 'fullscreen=yes,scrollbars=no');
        }

    </script>
    <script type="text/javascript">

        $(document).ready(function () {

            $(".SlashEff").mouseover(function () {

                $(".Slash").ImageStop(true, true);

                $(".Slash").ImageSwitch({ Type: $(this).attr("rel"),
                    NewImage: $(this).attr("src")
                });
            }
			);
            $(".SlashEff1").mouseover(function () {

                $(".Slash1").ImageStop(true, true);

                $(".Slash1").ImageSwitch({ Type: $(this).attr("rel"),
                    NewImage: $(this).attr("src")
                });
            }
		);

        });
    </script>
    <script language="javascript" type="text/javascript">

        var flag = false;
        function closedive() {
            if (flag == true) {
                flag = false;
                return;
            }
            var browser = String.format("Your browser is {0} {1}", Sys.Browser.name, Sys.Browser.version);
            if (Sys.Browser.agent == Sys.Browser.InternetExplorer) {
                document.getElementById("iframeStreet").style.display = 'none';
            }
            else {
                document.getElementById("iframeStreet").style.display = 'none';
                document.getElementById("streetTab").style.display = 'none';
            }
        }
        function ShowMap() {

            try {
                var map = new GMap(document.getElementById('map2'));
                var latlon = document.getElementById('hdnLatLon').value.split(",");
                var vqhd = latlon[0];
                var vpa = latlon[1];
                map.addControl(new GSmallMapControl());
                map.addControl(new GScaleControl());
                map.addControl(new GMapTypeControl());
                var point = new GPoint(vpa, vqhd);
                map.centerAndZoom(point, 2);
                var marker = new GMarker(point);
                map.addOverlay(marker);
            }
            catch (e) { }
        }

        function checkquerry() {

            var query = window.location.search.substring(1);
            var parms = query.split('street');


            if (parms[1] == "=True") {

                $find('Tabs').set_activeTabIndex(3);

            }
            else {
                var timeout_id = setTimeout("closedive()", 1000);
            }
            var browser = String.format("Your browser is {0} {1}", Sys.Browser.name, Sys.Browser.version);
            if (Sys.Browser.agent == Sys.Browser.InternetExplorer) {
                document.getElementById("iframeStreet").style.display = 'none';
            }
            else {
                document.getElementById("iframeStreet").style.display = 'block';
                document.getElementById("streetTab").style.display = 'none';
            }
        }

        function ActiveTabChanged(sender, e) {

            var index = $find("Tabs").get_activeTabIndex();

            if (index == 2) {
                chkloaded2();
                document.getElementById("iframeStreet").style.display = 'none';
            }
            else if (index == 3) {
                var browser = String.format("Your browser is {0} {1}", Sys.Browser.name, Sys.Browser.version);
                if (Sys.Browser.agent == Sys.Browser.InternetExplorer) {

                }
                else {
                    document.getElementById("iframeStreet").style.display = 'block';
                    document.getElementById("iframeStreet").style.width = '100%';
                }
            }
            else if (index == 0) {
                document.getElementById("iframeStreet").style.display = 'none';
                document.getElementById("iframeStreet").style.width = '100%';

                ShowMap();
            }
            else {
                document.getElementById("iframeStreet").style.display = 'none';
            }

            var browser = String.format("Your browser is {0} {1}", Sys.Browser.name, Sys.Browser.version);
            if (Sys.Browser.agent == Sys.Browser.InternetExplorer) {
                document.getElementById("iframeStreet").style.display = 'none';

            }

        }
   
   
    </script>
    <link href="class/class.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
<!--
.style1 {color: #FFFFFF}
-->
</style>
    <link href="../class/class.css" rel="stylesheet" type="text/css" />
</head>
<body onload="setControls(); animateImages();  ShowMap();checkquerry();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <div id="wrapper">
        <table width="100%" height="171" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td align="right" valign="middle">
                    <uc1:Header ID="Header1" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td align="left" valign="top">
                                <table id="tablePropertyDetails" runat="server" width="100%" border="0" cellspacing="0"
                                    cellpadding="0">
                                    <tr>
                                        <td height="301" align="center" valign="middle">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td height="47" colspan="2" align="left" valign="middle">
                                                        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td height="36" valign="top">
                                                                    <table width="100%" border="0">
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <td align="left">
                                                                                        </td>
                                                                                        <td align="left">
                                                                                        </td>
                                                                                        <td>
                                                                                        </td>
                                                                                        <td align="left">
                                                                                        </td>
                                                                                        <td>
                                                                                        </td>
                                                                                        <td align="left">
                                                                                        </td>
                                                                                        <td>
                                                                                        </td>
                                                                                        <td align="left">
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="80%" align="left">
                                                                                <table width="99%" border="0">
                                                                                    <tr>
                                                                                        <td width="4%" align="left">
                                                                                            <img src="images/icons/enquary.jpg" alt="Property Enquiry" width="28" height="18"
                                                                                                border="0" align="left" />
                                                                                        </td>
                                                                                        <td width="15%" align="left">
                                                                                            <a href="mailto:info@xample.co.uk?subject=<%= hdnEnquirySub.Value %>&amp;body=<%= hdnAddressForEnquiry.Value %>"
                                                                                                style="font-size: 12px; text-decoration: none; font-weight: normal; color: #790000">
                                                                                                Property Enquiry</a>
                                                                                        </td>
                                                                                        <td width="4%" align="left">
                                                                                            <img src="images/icons/sendtomail.jpg" alt="Send to friend" width="25" height="18"
                                                                                                border="0" align="left" />
                                                                                        </td>
                                                                                        <td width="15%" align="left">
                                                                                            <span style="font-size: 12px; text-decoration: none; font-weight: normal; color: #790000;
                                                                                                cursor: pointer;"><a href="#" style="font-size: 12px; text-decoration: none; font-weight: normal;
                                                                                                    color: #790000;" onclick="window.open('sendtoafriend.aspx?id=<%= PropId %>','','scrollbars=yes,width=600,height=410')">
                                                                                                    Send to a friend</a></span>
                                                                                        </td>
                                                                                        <td width="5%" align="left">
                                                                                            <img src="images/icons/save.jpg" alt="ADD TO MY FAVOURITES" width="21" height="18"
                                                                                                border="0" align="right" />
                                                                                        </td>
                                                                                        <td width="19%">
                                                                                            <asp:LinkButton ID="lnkShortlist" runat="server" Style="font-size: 12px; text-decoration: none;
                                                                                                font-weight: normal; color: #790000" OnClick="lnkShortlist_Click">Add to my favourites</asp:LinkButton>
                                                                                        </td>
                                                                                        <td width="4%" align="left">
                                                                                            <img src="images/icons/slide.jpg" alt="Slide show" width="23" height="18" border="0"
                                                                                                align="right" />
                                                                                        </td>
                                                                                        <td width="13%" align="left">
                                                                                            <span style="font-size: 12px; text-decoration: none; font-weight: normal; color: #790000;
                                                                                                cursor: pointer"><a href="#" style="font-size: 12px; text-decoration: none; font-weight: normal;
                                                                                                    color: #790000;" onclick="window.open('slideshow.aspx?id=<%= PropId %>','','scrollbars=yes,width=600,height=760')">
                                                                                                    Slide show</a></span>
                                                                                        </td>
                                                                                        <td width="30%" align="left">
                                                                                            <table width="102%" border="0">
                                                                                                <tr>
                                                                                                    <td width="10%" align="left" id="fptd1" runat="server">
                                                                                                        <img id="fplink1" runat="server" src="images/icons/map.jpg" alt="Plan" width="19"
                                                                                                            height="18" border="0" align="right" style="cursor: pointer;" />
                                                                                                    </td>
                                                                                                    <td width="57%" align="left" id="fptd2" runat="server">
                                                                                                        &nbsp;<a id="fplink2" runat="server" href="#" target="_blank" style="font-size: 12px;
                                                                                                            text-decoration: none; font-weight: normal; color: #8F0000">Floor plan</a>
                                                                                                    </td>
                                                                                                    <td width="14%" align="right">
                                                                                                        <img id="video1" runat="server" src="images/icons/banner.jpg" width="23" height="18"
                                                                                                            style="cursor: pointer;" onclick="window.open('http://www.youtube.com/watch?v=H58gALb3ePs');" />
                                                                                                    </td>
                                                                                                    <td width="19%" align="right">
                                                                                                        &nbsp;<a id="video2" runat="server" href="http://www.youtube.com/watch?v=H58gALb3ePs"
                                                                                                            target="_blank" style="font-size: 11px; text-decoration: none; font-weight: normal;
                                                                                                            color: #8F0000;">360 view</a>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <td width="20%" align="right">
                                                                                <asp:LinkButton ID="backtosearch" runat="server" Style="font-size: 12px; text-decoration: none;
                                                                                    font-weight: normal; color: #790000;" OnClick="backtosearch_Click"> << Back to search listings </asp:LinkButton>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" align="left" valign="middle" class="headwrap3">
                                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td style="width: 80%; font-weight: bold">
                                                                                            <asp:Label ID="lblAddress" runat="server" Text=""></asp:Label><asp:Label ID="lblPrice"
                                                                                                runat="server" Text=""></asp:Label><asp:Label ID="lblMisc" runat="server" Text=""></asp:Label>
                                                                                        </td>
                                                                                        <td align="right" style="width: 20%;">
                                                                                            <a href="javascript:void(0);" onclick="fullScreen('Full_Screen_slideshow.aspx?id=<%= PropId %>');"
                                                                                                style="font-size: 12px; text-decoration: underline; font-weight: normal; color: #790000;
                                                                                                font-style: normal">Full screen slideshow</a>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <div>
                                                                        <span style="font-size: 11px; color: #800000; font-weight: 100;">If Street View or Maps
                                                                            dont show properly you may need to install <a href="http://www.gates.esuk.co.uk/install_flash_player_10_active_x.zip"
                                                                                target="_blank" style="text-align: center; color: #770000; font-size: 11px; text-decoration: underline;">
                                                                                Flash Player</a></span><br />
                                                                        <ajaxToolkit:TabContainer runat="server" ID="Tabs" OnClientActiveTabChanged="ActiveTabChanged">
                                                                            <ajaxToolkit:TabPanel runat="Server" ID="Panel1" HeaderText="Summary">
                                                                                <ContentTemplate>
                                                                                    <br />
                                                                                    <div id="summary">
                                                                                        <table width="100%" height="500px" border="0" cellspacing="0" cellpadding="0">
                                                                                            <tr>
                                                                                                <td width="580" align="left" valign="top" style="padding-left: 15px; font-size: 11px;">
                                                                                                    <div id="map2" style="width: 97%; height: 412px; border: 1px solid #000000;">
                                                                                                    </div>
                                                                                                </td>
                                                                                                <td width="351" height="316" align="center" valign="top" style="border-left: 2px dotted #CCCCCC;">
                                                                                                    <div style="height:272px;" class="SplashFrame">
                                                                                                        <img id="mainimg2" runat="server" class="Slash" src="images/image_soon.jpg" width="156"
                                                                                                            height="173" />
                                                                                                    </div>
                                                                                                     <table style="margin-top:20px;" width="68%" border="0" cellspacing="0">
  <tr>
    <td width="51%"><p style="font-size:18px;line-height: 26px;">Download <br />
      or scan <br />
      this property</p></td>
    <td width="7%">&nbsp;</td>
    <td width="42%"><div>
                                                      <img align="right" style="height:100px; width:100px;" id="imgQrCode" runat="server" /></div></td>
  </tr>
</table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td valign="top" style="padding-left: 15px; border-top: 2px dotted #CCCCCC; padding-right: 15px;">
                                                                                                    <div id="divshrtdesc" runat="server" style="text-align: justify">
                                                                                                    </div>
                                                                                                </td>
                                                                                                <td width="351" align="center" valign="top" style="border-left: 2px dotted #CCCCCC;
                                                                                                    border-top: 2px dotted #CCCCCC">
                                                                                                    <asp:DataList ID="dtlstImages2" runat="server" RepeatDirection="Horizontal" RepeatColumns="3">
                                                                                                        <ItemTemplate>
                                                                                                            <table width="32%" border="0" align="center" cellpadding="2" cellspacing="2">
                                                                                                                <tr>
                                                                                                                    <td width="100%" align="center" valign="middle">
                                                                                                                        <img rel="ScrollIn" class="SlashEff" src="<%# DataBinder.Eval(Container.DataItem,"item") %>"
                                                                                                                            onerror="this.src='images/Prop_image_soon.jpg'" style="border: 1px solid #666666;
                                                                                                                            cursor: pointer;" width="100" height="100" onload="javascript:setDetThumbImage(this);" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:DataList>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </ContentTemplate>
                                                                            </ajaxToolkit:TabPanel>
                                                                            <ajaxToolkit:TabPanel runat="Server" ID="Panel2" HeaderText="Images">
                                                                                <ContentTemplate>
                                                                                    <div id="showimage">
                                                                                        <table width="100%" height="500px" border="0">
                                                                                            <tr>
                                                                                                <td id="mainimtab" width="620px" height="620px" align="center" valign="middle" style="border-left: 2px dotted #CCCCCC;
                                                                                                    border-top: 2px dotted #CCCCCC; border-right: 2px dotted #cccccc; border-bottom: 2px dotted #cccccc">
                                                                                                    <div class="SplashFrame1">
                                                                                                        <img id="mainimg" runat="server" class="Slash1" src="images/image_soon.jpg" width="356"
                                                                                                            height="373" />
                                                                                                    </div>
                                                                                                </td>
                                                                                                <td id="imgthunbtab" width="300px" align="center" valign="top" style="border-left: 2px dotted #CCCCCC;
                                                                                                    border-right: 2px dotted #cccccc; border-bottom: 2px dotted #cccccc; border-top: 2px dotted #cccccc">
                                                                                                    <asp:DataList ID="dtlstImages" runat="server" RepeatDirection="Horizontal" RepeatColumns="2">
                                                                                                        <ItemTemplate>
                                                                                                            <table width="32%" border="0" align="center" cellpadding="2" cellspacing="2">
                                                                                                                <tr>
                                                                                                                    <td width="100%" align="center" style="padding-top: 20px" valign="middle">
                                                                                                                        <img rel="ScrollIn" class="SlashEff1" style="border: 1px solid #666666; cursor: pointer;"
                                                                                                                            src="<%# DataBinder.Eval(Container.DataItem,"item") %>" onerror="this.src='images/Prop_image_soon.jpg'"
                                                                                                                            width="100" height="80" onload="javascript:setDetThumbImage(this);" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:DataList>
                                                                                                    
                                                                                                    <div>
                                                                                                    </div>
                                                                                                   

                                                                                              </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </ContentTemplate>
                                                                            </ajaxToolkit:TabPanel>
                                                                            <ajaxToolkit:TabPanel runat="Server" ID="Panel3" HeaderText="Map">
                                                                                <ContentTemplate>
                                                                                    <div id="mapTab">
                                                                                        <table width="100%" height="500px" border="0">
                                                                                            <tr>
                                                                                                <td id="tablePOIs" align="left" colspan="2" valign="top" bgcolor="#F7F7F9">
                                                                                                    <asp:DataList ID="Repeater2" runat="server" RepeatDirection="Horizontal" RepeatColumns="7">
                                                                                                        <ItemTemplate>
                                                                                                            <table width="135px" border="0" cellspacing="0" cellpadding="0">
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <img src="images/<%# DataBinder.Eval(Container.DataItem,"categoryid") %>.gif" width="30"
                                                                                                                            height="28" border="0" />
                                                                                                                    </td>
                                                                                                                    <td height="40" align="left">
                                                                                                                        <span id="<%# DataBinder.Eval(Container.DataItem,"categoryid") %>">
                                                                                                                            <%# DataBinder.Eval(Container.DataItem,"item") %>
                                                                                                                            <label style="font-size: 10px">
                                                                                                                                <%# DataBinder.Eval(Container.DataItem,"name") %></label>
                                                                                                                        </span>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:DataList>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td align="center" colspan="2" valign="middle">
                                                                                                    <div id="map1" style="padding-top: 10px; width: 100%; height: 500px; border: 1px solid #000000;">
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </ContentTemplate>
                                                                            </ajaxToolkit:TabPanel>
                                                                            <ajaxToolkit:TabPanel runat="Server" ID="Panel4" HeaderText="Street View">
                                                                                <ContentTemplate>
                                                                                    <div id="streetTab">
                                                                                        <table width="100%" height="500px" border="0">
                                                                                            <td id="Street" width="100%" valign="top" bgcolor="#000000">
                                                                                                <iframe src="http://www.estatesolutions.co.uk/StreetViewMap1.htm?l=<% Response.Write(lat); %>,<% Response.Write(log); %><% Response.Write(streetattribute); %>"
                                                                                                    scrolling="no" width="100%" align="middle" height="500" frameborder="0"></iframe>
                                                                                            </td>
                                                                                        </table>
                                                                                    </div>
                                                                                </ContentTemplate>
                                                                            </ajaxToolkit:TabPanel>
                                                                            <ajaxToolkit:TabPanel runat="Server" ID="Panel5" HeaderText="More Details">
                                                                                <ContentTemplate>
                                                                                    <div id="Desctab">
                                                                                        <table width="100%" height="500px" border="0">
                                                                                            <td valign="top" style="padding-top: 20px">
                                                                                                <div id="divbedroom2" runat="server">
                                                                                                </div>
                                                                                                <div id="divbedroom" runat="server">
                                                                                                </div>
                                                                                                <div id="lblDesc" runat="server" style="text-align: justify; width: 100%;">
                                                                                                </div>
                                                                                             <%-- <div>
                                                                                                    <img id="imgQrCode" runat="server" /></div>--%>
                                                                                            </td>
                                                                                        </table>
                                                                                    </div>
                                                                                </ContentTemplate>
                                                                            </ajaxToolkit:TabPanel>
                                                                        </ajaxToolkit:TabContainer>
                                                                        <br />
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="36" valign="middle" width="900px" style="border: 0; border-color: White;">
                                                        <table id="iframeStreet" width="100%" height="500px" border="0" valign="middle" style="border: 0;
                                                            border-color: White;">
                                                            <tr>
                                                                <td id="Streetd" style="border: 0; border-color: White;" width="900px" valign="middle"
                                                                    bgcolor="#000000">
                                                                    <iframe src="http://www.estatesolutions.co.uk/StreetViewMap1.htm?l=<% Response.Write(lat); %>,<% Response.Write(log); %><% Response.Write(streetattribute); %>"
                                                                        scrolling="no" width="100%" align="middle" height="500" frameborder="0" marginheight="0"
                                                                        marginwidth="0"></iframe>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">&nbsp;
                                            
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <uc2:Footer ID="Footer1" runat="server" />
    <script language="javascript" type="text/javascript">
        if (top.location != self.location) {
            top.location.replace(self.location); // pop to top
        }
    </script>
    <asp:HiddenField ID="hdnLatLon" runat="server" />
    <asp:HiddenField ID="hdnEnquirySub" runat="server" />
    <asp:HiddenField ID="hdnAddressForEnquiry" runat="server" />
    <asp:HiddenField ID="hdnViewing" runat="server" />
    </form>
    <!-- BoldChat Visitor Monitor HTML v1.40 (Website=- None -,ChatWindow=- None -,ChatInvitation=xample 1) -->
    <script language="JavaScript" type="text/javascript"><!--
        document.write('<scr' + 'ipt language="JavaScript" type="text/javascript" src="http://vms.boldchat.com/aid/2178106990504207724/bc.vms/vms.js?url=' + escape(document.location.href) + '&amp;referrer=' + escape(document.referrer) + '&amp;cidid=8129246933384324650&amp;cp=http&amp;cw=640&amp;ch=480"></scr' + 'ipt>');
//-->
    </script>
    <noscript>
        <a href="http://www.boldchat.com" title="Live Chat Software" target="_blank">
            <img alt="Live Chat Software" src="http://vms.boldchat.com/aid/2178106990504207724/bc.vmi"
                border="0" width="1" height="1" /></a>
    </noscript>
</body>
</html>

