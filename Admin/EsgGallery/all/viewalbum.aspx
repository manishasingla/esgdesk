﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="viewalbum.aspx.cs" Inherits="all_viewalbum" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>View Album</title>
</head>
<body>
<form id="form1" runat="server">
<div class="title"> esgdesk.com</div>
<table width="100%" height="600px">
<tr>
<td valign="top" width="20%" style="background-color:#dddddd">
    <asp:FormView ID="FormView1" runat="server" DataSourceID ="ObjectDataSource1">
      <ItemTemplate>
      <center>
      <img src='../photos/<%#Eval("firstphoto")%>.jpg' 
          width="150px" height="100px" />
      </center>
      <p />
      <h4> <%# Eval("title") %> (<%# Eval("nophotos") %>) </h4>
      <%# Eval("createdon")%>
      <p />
      <%# Eval("description") %>
    </ItemTemplate>
    </asp:FormView>
    
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
              SelectMethod="GetAlbumDetails" TypeName="PhotosDAL">
        <SelectParameters>
            <asp:QueryStringParameter Name="aid" QueryStringField="aid" Type="String" />
        </SelectParameters>
          </asp:ObjectDataSource>
</td>
<td valign="top">
<asp:ListView ID="ListView1" runat="server" 
DataSourceID="odsPhotos" GroupItemCount ="3">
     <LayoutTemplate>
     <center>
      <table id="Table1" runat="server">
       <tr runat="server" id="groupPlaceholder"/>
      </table>
      <p />
      <asp:DataPager ID="DataPager1" runat="server" PagedControlID = "ListView1" PageSize="6">
        <Fields>
            <asp:NumericPagerField />
        </Fields>
       </asp:DataPager>
      </center>
     </LayoutTemplate>
     
     <GroupTemplate>
         <tr id="Tr1" runat="server">
             <td id="itemPlaceholder" runat="server" />
         </tr>
     </GroupTemplate>
     
     <ItemTemplate>
          <td>
            <a href='../photos/<%# Eval("photoid")%>.jpg'> 
            <img src='../photos/<%# Eval("photoid")%>.jpg' width="150px" height="100px" />
            </a>
            <br />
            <%#Eval("title")%> 
            <br />
          </td>
     </ItemTemplate>
   </asp:ListView>
   <p />
   
     
    <asp:ObjectDataSource ID="odsPhotos" runat="server" SelectMethod="GetPhotosFromAlbum" 
        TypeName="PhotosDAL">
        <SelectParameters>
            <asp:QueryStringParameter Name="aid" QueryStringField="aid" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</td>
</tr>
</table>

    </form>
</body>
</html>
