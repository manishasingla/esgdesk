﻿<%@ Page Language="C#" MasterPageFile="~/Admin/EsgGallery/AdminMasterMenu.master" AutoEventWireup="true" CodeFile="createalbum.aspx.cs" Inherits="createalbum" Title="Create album" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h2>Create Album</h2>
<table>
<tr>
<td>Album Title</td>
<td><asp:TextBox ID="txtTitle" columns="40"
 runat="server" Height="22px" Width="341px"></asp:TextBox> <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
        ControlToValidate="txtTitle" ErrorMessage="Please select a title"></asp:RequiredFieldValidator></td>
</tr>
<tr>
<td>Event date</td>
<td> <asp:TextBox ID="txtEventDate" runat="server"></asp:TextBox>
                               <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" 
                            TargetControlID="txtEventDate"
                            Format="dd/MM/yyyy" PopupButtonID="txtEventDate" >
                            </ajaxToolkit:CalendarExtender>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
        ControlToValidate="txtEventDate" ErrorMessage="Please select an event date"></asp:RequiredFieldValidator>
    </td>
</tr>
<tr>
<td>Album Description</td>
<td><asp:TextBox ID="txtDescription" 
     Rows="3" columns="40" TextMode="MultiLine"
     runat="server"></asp:TextBox> </td>
</tr>
</table>
<p />
    <asp:Button ID="btnCreate" runat="server" 
    Text="Create Album" onclick="btnCreate_Click" />
    <p />
    <asp:Label ID="lblMsg" runat="server"></asp:Label>
</asp:Content>

