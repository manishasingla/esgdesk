﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

public partial class createalbum : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnCreate_Click(object sender, EventArgs e)
    {
        //string struserName = Session["username"].ToString();
        if (Session["admin"] != null && Session["admin"].ToString() != "")
        {
            if (txtEventDate.Text.Trim() != "" && txtTitle.Text.Trim() != "")
            {
                int albumid = PhotosDAL.CreateAlbum(Session["username"].ToString(), txtTitle.Text.Trim(), txtDescription.Text, txtEventDate.Text);
                if (albumid > 0)
                {
                    lblMsg.Text = "Album Created Successfully!";
                    string dir = Server.MapPath("photos/") + txtTitle.Text.Trim();
                    Response.Redirect("uploadphotos.aspx?aid=" + albumid + "&atitle=" + txtTitle.Text.Trim());
                }
                else
                    lblMsg.Text = "Sorry! Album could not be created!";
            }
        }
        else
        {
            Response.Redirect("~/Admin/login.aspx");
            return;
        }
    }
}
