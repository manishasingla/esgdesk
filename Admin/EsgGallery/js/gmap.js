// JavaScript Document

var n=0; 
function chkloaded() {
//alert("Start");
  if ((typeof(GBrowserIsCompatible)).toLowerCase() == 'undefined') { 
      setTimeout('chkloaded()',1000); 
      document.getElementById("map").innerHTML = "trying times this is!! " + n++ ; 
  } 
  else { 
     load();
  }
}   

var myLatitudeArray = new Array();
var myLongitudeArray = new Array();
var myImgArray = new Array();
var myBedroomArray = new Array();
var myReceptionArray = new Array();
var myAddressArray = new Array();
var myCostArray = new Array();
var myIdArray = new Array();
var propTypeArray = new Array();
var propNumArray = new Array();
var propStreet = new Array();
var geocoder;
var bounds;
var baseIcon;
var map;
var point;
var zoomlevel=16;
var cm_mapMarkers = new Array();
var cm_mapHTMLS = new Array();
var icon;
	
function CapitalCitiesCache()
{
	GGeocodeCache.apply(this);
}

var northeastlat;
var northeastlon;
var southwestlat;
var southwestlon;

function load()
{
///alert("Start2");

    if (GBrowserIsCompatible()) 
  	{
		document.getElementById("map").style.display = "block";
		map = new GMap2(document.getElementById("map"));
		geocoder = new GClientGeocoder();
		map.addControl(new GMapTypeControl());
		map.enableDoubleClickZoom();
		//map.addControl( new GLargeMapControl()); 
		map.enableContinuousZoom();
		//map.setCenter(new GLatLng(51.506325,-0.127144),15);
		map.setCenter(new GLatLng(myLatitudeArray[0],myLongitudeArray[0]), 16);
		baseIcon = new GIcon();
		baseIcon.iconSize = new GSize(24, 23);
		baseIcon.iconAnchor = new GPoint(9, 34);
		baseIcon.infoWindowAnchor = new GPoint(9, 2);
		baseIcon.infoShadowAnchor = new GPoint(18, 25);
		
		bounds = map.getBounds();
		PropertyMarkers();
		
		northeastlat = bounds.getNorthEast().lat();
		northeastlon = bounds.getNorthEast().lng();
		southwestlat = bounds.getSouthWest().lat();
		southwestlon = bounds.getSouthWest().lng();
		map.setZoom(map.getBoundsZoomLevel(bounds));
	  	zoomlevel = map.getZoom();
		var clat = (northeastlat + southwestlat) /2;
		var clng = (northeastlon + southwestlon) /2;
		map.setCenter(new GLatLng(clat,clng));
		try {
			var checklist = document.getElementById('tablePOIs');
			var spanitems = checklist.getElementsByTagName('span');
			for(var i=0;i<spanitems.length;i++)
			{
				if(hashMarkers[spanitems[i].id] == null) { hashMarkers[spanitems[i].id] = new Array(); }
			}
		}
		catch (e) { }
		GEvent.addListener(map,'moveend',function() {
		getsetPOIsOnZoom();
		});		
	}
}
	
function PropertyMarkers() {
//alert("Start3");
var rank =0;
	for (var i=0;i<myLatitudeArray.length;i++)
	{
		if (parseFloat(myLatitudeArray[i]) && parseFloat(myLongitudeArray[i]))
		{
			point = new GLatLng(myLatitudeArray[i],myLongitudeArray[i]);
			
			bounds.extend(point);
			
			if(point.lat() && point.lng())
			{
		///	rank = i;
		var streerpoint = myLatitudeArray[i]+","+ myLongitudeArray[i];
				///map.addOverlay(createPropMarker(myIdArray[i],point,myImgArray[i],myBedroomArray[i],myAddressArray[i],myCostArray[i],propTypeArray[i],rank));
				//alert(myIdArray[i]);
				///alert(propNumArray[i]);
				map.addOverlay(cm_createMarker(myIdArray[i],point,myImgArray[i],myBedroomArray[i],myAddressArray[i],myCostArray[i],propTypeArray[i],propNumArray[i],propStreet[i],streerpoint));
				
			//cm_mapMarkers.push(createPropMarker(myIdArray[i],point,myImgArray[i],myBedroomArray[i],myAddressArray[i],myCostArray[i],propTypeArray[i],rank));
				
			}
		}
	}
}
function cm_markerClicked(markerNum)
 {
 
//alert(markerNum);
//alert(cm_mapHTMLS[markerNum]);
/////alert(cm_mapMarkers[markerNum]);
  cm_mapMarkers[markerNum].openInfoWindowHtml(cm_mapHTMLS[markerNum]);
  
  }

function cm_createMarker(id,point,img,bed,add,cost,type,propnum,street,StreetP)
 {

 var icon = new GIcon(baseIcon);
	icon.image="images/home.png";
	var marker = new GMarker(point, icon);
	
var details = "propertylistdetails.aspx?id="+id+"&type="+type;
 ///var html1="<div style='width:223px;max-height:300px; font-size:12px; font-family:Arial,Helvetica,sans-serif;color:#666666'><table bgcolor='#FFFFFF' width='223' border='0' cellspacing='0' cellpadding='0'><tr><td><h4>Property Information</h4></td> </tr><tr><td align='center' valign='middle' width='100%' height='150px'><table><tr><td><div style='padding-top:3px; padding-right:3px; padding-bottom:3px; padding-left:3px; border:2px outset #999999'><img src='"+img+"'width='125' height='134' onload='setListingsImage(this);' /></div></td></tr></table></td></tr><tr><td align='left'><strong>"+add+"</strong></td></tr> <tr><td align='left'>"+cost+"</td> </tr><tr><td align='left'><strong>"+bed+"</strong></td></tr><tr><td align='right'><a href='"+details+"' style='color:#666666;'>More Details &gt;&gt; </a></td></tr><tr><td>"+propnum+"</td></tr></table></div>";

 var html1="<div style='width:223px;max-height:300px; font-size:12px; font-family:Arial,Helvetica,sans-serif;color:#666666'><table bgcolor='#FFFFFF' width='223' border='0' cellspacing='0' cellpadding='0'><tr><td><h4>Property Information</h4></td> </tr><tr><td align='center' valign='middle' width='100%' height='150px'><table><tr><td><div style='padding-top:3px; padding-right:3px; padding-bottom:3px; padding-left:3px; border:2px outset #999999'><img src='"+img+"'width='125' height='134' onload='setListingsImage(this);' /></div></td></tr></table></td></tr><tr><td align='left'><strong>"+add+"</strong></td></tr> <tr><td align='left'>"+cost+"</td> </tr><tr><td align='left'><strong>"+bed+"</strong></td></tr><tr><td align='right'><a href='"+details+"' style='color:#666666;'>More Details &gt;&gt; </a></td></tr></table></div>";
		
		http://www.estatesolutions.co.uk/StreetViewMap1.htm?l="+ point +" street +"><% Response.Write(lat); %>,<% Response.Write(log); %><% Response.Write(streetattribute); %>
		var streetTab = "http://www.estatesolutions.co.uk/StreetViewMap1.htm?l="+ StreetP+""+ street;
		//alert(streetTab);
		var htmlstreet ="<div><iframe width='223' height='300' src ='"+ streetTab +"'></iframe></div>";
		 var tabs = [new GInfoWindowTab("Details",html1), new GInfoWindowTab("Street",htmlstreet)];
		cm_mapHTMLS.push(tabs);
  GEvent.addListener(marker, "click", function() 
  {
 /// alert("Start2");
	
		marker.openInfoWindowHtml( [new GInfoWindowTab("Details",html1), new GInfoWindowTab("Street",htmlstreet)], {maxUrl:"http://dotnetricks.blogspot.com"});
  });

 
 cm_mapMarkers.push(marker);
  return marker;
}

function createPropMarker(id,point,img,bed,add,cost,type)
 {

	var icon = new GIcon(baseIcon);
	icon.image="images/home.png";
	var marker = new GMarker(point, icon); 
	var details = "propertymapdetails.aspx?id="+id+"&type="+type;
	
	GEvent.addListener(marker, "click", function() 
	{
	
	var html1="<div style='width:223px;max-height:300px; font-size:12px; font-family:Arial,Helvetica,sans-serif;color:#666666'><table bgcolor='#FFFFFF' width='223' border='0' cellspacing='0' cellpadding='0'><tr><td><h4>Property Information</h4></td> </tr><tr><td align='center' valign='middle' width='100%' height='150px'><table><tr><td><div style='padding-top:3px; padding-right:3px; padding-bottom:3px; padding-left:3px; border:2px outset #999999'><img src='"+img+"'width='125' height='134' onload='setListingsImage(this);' /></div></td></tr></table></td></tr><tr><td align='left'><strong>"+add+"</strong></td></tr> <tr><td align='left'>"+cost+"</td> </tr><tr><td align='left'><strong>"+bed+"</strong></td></tr><tr><td align='right'><a href='"+details+"' style='color:#666666;'>More Details &gt;&gt; </a></td></tr></table></div>";
	marker.openInfoWindowHtml(html1);
	
	});
	return marker;
}

function getPOIs() 
{
	//alert("fired................");
	var oldzoomlevel = zoomlevel;
	zoomlevel = map.getZoom();
	if(zoomlevel <= oldzoomlevel)
	{
		var obj;
		if (window.XMLHttpRequest)
		{
			// Mozilla, Safari, ...
			obj = new XMLHttpRequest();
			if (obj.overrideMimeType)
			{
				obj.overrideMimeType('text/xml');
			}
		} 
		else if (window.ActiveXObject) 
		{ // IE
			try
			{
				obj = new ActiveXObject("Msxml2.XMLHTTP");
			}
			catch (e)
			{
				try
				{
					obj = new ActiveXObject("Microsoft.XMLHTTP");
				}
				catch (e1)
				{
					obj = null;
				}
			}
		}
		if (obj==null)
		{
			alert('Giving up :( Cannot create an XMLHTTP instance');
		}
		else
		{		
			obj.onreadystatechange = function() {
				if(obj.readyState == 4 && obj.status == 200)
				{
					//alert(obj.responseText);
					var dsRoot=obj.responseXML.documentElement;
					//alert(dsRoot);
					if(dsRoot.childNodes.length > 0) { POIsMarkers(dsRoot); }
				}
				else
				{
					//alert("Error retrieving data!");
				}
			}
			var url = window.location.href.split("//")[1].split("/")[0];
			url = url.match("localhost")?(url+"/gates.esuk.co.uk"):url;
			
			bounds = map.getBounds();
		  
			northeastlat = bounds.getNorthEast().lat();
			northeastlon = bounds.getNorthEast().lng();
			southwestlat = bounds.getSouthWest().lat();
			southwestlon = bounds.getSouthWest().lng();
			
			var checklist = document.getElementById('tablePOIs');
			var spanitems = checklist.getElementsByTagName('span');
			var checkeditems = checklist.getElementsByTagName('input');
			var qs="";
			for(var i=0;i<checkeditems.length;i++)
			{
				qs += checkeditems[i].type=='checkbox' && checkeditems[i].checked?(spanitems[i].id.toString()+";"):"";
			}
			obj.open("GET", "http://"+url+"/getPOIsData.aspx?categoryid="+qs+"&northeastlat="+northeastlat+"&northeastlon="+northeastlon+"&southwestlat="+southwestlat+"&southwestlon="+southwestlon, true);
			obj.send(null);
		}
	}
	return false;	
}




function checkboxClicked(objcheckbox,categoryid) 
{
	getsetPOIs(objcheckbox,categoryid)
}

function getsetPOIs(objcheckbox,categoryid) {
	//alert("fired................");
	if(objcheckbox.checked == false)
	{
		for (var x=0;x<hashMarkers[categoryid].length;x++)
		{
			hashMarkers[categoryid][x].hide();
		}
	}
	else {
		var obj;
		if (window.XMLHttpRequest)
		{
			obj = new XMLHttpRequest();
			if (obj.overrideMimeType)
			{
				obj.overrideMimeType('text/xml');
			}
		} 
		else if (window.ActiveXObject) 
		{
			try
			{
				obj = new ActiveXObject("Msxml2.XMLHTTP");
			}
			catch (e)
			{
				try
				{
					obj = new ActiveXObject("Microsoft.XMLHTTP");
				}
				catch (e1)
				{
					obj = null;
				}
			}
		}
		if (obj==null)
		{
			alert('Giving up :( Cannot create an XMLHTTP instance');
		}
		else
		{		
			obj.onreadystatechange = function() {
				if(obj.readyState == 4 && obj.status == 200)
				{
					//alert(obj.responseText);
					var dsRoot=obj.responseXML.documentElement;
					
					if(dsRoot.childNodes.length > 0) { 
						if(dsRoot.childNodes.length > 500) { 
							var confirmvalue=confirm("More than 500 POIs are about to be plotted and may take some time. Either click cancel and zoom in to reduce the number of POIs or click Ok to continue regardless"); 
							objcheckbox.checked = confirmvalue; 
							if(confirmvalue==true) {if(hashMarkers[categoryid] == null) { hashMarkers[categoryid] = new Array(); }
								POIsMarkers(dsRoot);
							}
							else {
								for (var x=0;x<hashMarkers[categoryid].length;x++)
								{
									hashMarkers[categoryid][x].hide();
								}
							}
						}
						else { if(hashMarkers[categoryid] == null) { hashMarkers[categoryid] = new Array(); } POIsMarkers(dsRoot); }
					}
				}
				else
				{
					//alert("Error retrieving data!");
				}
			}
			var url = window.location.href.split("//")[1].split("/")[0];
			url = url.match("localhost")?(url+"/gates.esuk.co.uk"):url;
			
			bounds = map.getBounds();
		  
			northeastlat = bounds.getNorthEast().lat();
			northeastlon = bounds.getNorthEast().lng();
			southwestlat = bounds.getSouthWest().lat();
			southwestlon = bounds.getSouthWest().lng();
			
			obj.open("GET", "http://"+url+"/getPOIsData.aspx?categoryid="+categoryid+"&northeastlat="+northeastlat+"&northeastlon="+northeastlon+"&southwestlat="+southwestlat+"&southwestlon="+southwestlon, true);
			obj.send(null);
		}
	}
}

var hashMarkers = new Array();

function POIsMarkers(values) {
	var name;
	var lat;
	var lon;
	var desc;
	var cat;
	
	for(var i=0;i<values.childNodes.length;i++)
	{
		name="";
		lat="";
		lon="";
		desc="";
		cat="";
		for(var j=0; j< values.childNodes[i].childNodes.length;j++)
		{
			var objitem = values.childNodes[i].childNodes[j];
			switch(objitem.nodeName.toLowerCase())
			{
				case "name":
					name = (objitem.textContent || objitem.innerText || objitem.text || "");
					break;
				case "description":
					desc = (objitem.textContent || objitem.innerText || objitem.text || "");
					break;
				case "latitude":
					lat =(objitem.textContent || objitem.innerText || objitem.text || "");
					break;
				case "longitude":
					lon = (objitem.textContent || objitem.innerText || objitem.text || "");
					break;
				case "categoryid":
					cat = (objitem.textContent || objitem.innerText || objitem.text || "");
					break;
				default:
					break;
			}
		}
		point = new GLatLng(lat,lon);
		//bounds.extend(point);
		createPOIMarker(name,lat,lon,desc,cat);		
	}
}




function createPOIMarker(name,lat,lon,desc,cat) {
	var icon = new GIcon(baseIcon);
	icon.image= "images/"+cat+".gif";
	var marker = new GMarker(point, icon);
	try { if(hashMarkers[cat][marker] == null) { hashMarkers[cat].push(marker); }
	else { hashMarkers[cat][marker].show(); return; } }
	catch(e) {}
	GEvent.addListener(marker, "click", function() {
	var html1="<div style='width:150px; font-size:12px;'><table width='150'><tr><td align='left'><h3>"+name+"</h3>Other details : <br>"+desc+"</td></tr>";
	html1+="<tr><td align='left' width='150'>Geographical coordinates : <br />Latitude :"+lat+"<br />Longitude : "+lon+"</td></tr></table></div>";
	marker.openInfoWindowHtml(html1);
	});
	map.addOverlay(marker);
}

function getsetPOIsOnZoom() {
	//alert("fired................");
	
	var oldzoomlevel = zoomlevel;
	zoomlevel = map.getZoom();
	
	if(zoomlevel >= oldzoomlevel)
	{
		var obj;
		if (window.XMLHttpRequest)
		{ 
			// Mozilla, Safari, ...
			obj = new XMLHttpRequest();
			if (obj.overrideMimeType)
			{
				obj.overrideMimeType('text/xml');
			}
		} 
		else if (window.ActiveXObject) 
		{ // IE
			try
			{
				obj = new ActiveXObject("Msxml2.XMLHTTP");
			}
			catch (e)
			{
				try
				{
					obj = new ActiveXObject("Microsoft.XMLHTTP");
				}
				catch (e1)
				{
					obj = null;
				}
			}
		}
		if (obj==null)
		{
			alert('Giving up :( Cannot create an XMLHTTP instance');
		}
		else
		{		
			obj.onreadystatechange = function() {
				if(obj.readyState == 4 && obj.status == 200)
				{
					//alert(obj.responseText);
						var dsRoot=obj.responseXML.documentElement;
						if(dsRoot.childNodes.length > 0) { 
							if(dsRoot.childNodes.length > 500) { 
								var confirmvalue=confirm("More than 500 POIs are about to be plotted and may take some time. Either click cancel and zoom in to reduce the number of POIs or click Ok to continue regardless");
								if(confirmvalue==true) {
								POIsMarkers(dsRoot);
							}
						}
						else { POIsMarkers(dsRoot); }
						}
				}
				else
				{
					//alert("Error retrieving data!");
				}
			}
			var url = window.location.href.split("//")[1].split("/")[0];
			url = url.match("localhost")?(url+"/gates.esuk.co.uk"):url;
			
			bounds = map.getBounds();
		  
			northeastlat = bounds.getNorthEast().lat();
			northeastlon = bounds.getNorthEast().lng();
			southwestlat = bounds.getSouthWest().lat();
			southwestlon = bounds.getSouthWest().lng();
			
			var checklist = document.getElementById('tablePOIs');
			var spanitems = checklist.getElementsByTagName('span');
			var checkeditems = checklist.getElementsByTagName('input');
			var qs="";
			for(var i=0;i<checkeditems.length;i++)
			{
				qs += checkeditems[i].type=='checkbox' && checkeditems[i].checked?(spanitems[i].id.toString()+";"):"";
			}
			obj.open("GET", "http://"+url+"/getPOIsData.aspx?categoryid="+qs+"&northeastlat="+northeastlat+"&northeastlon="+northeastlon+"&southwestlat="+southwestlat+"&southwestlon="+southwestlon, true);
			obj.send(null);
		}
	}
	return false;
}

function newPan(pDirection) {
		//var intArea = document.getElementById('mLocation').value;
		var strDirection	= pDirection;
		var center			= map.getCenter();
		var strTicks		= map.getBounds();
		var intZoomLevel	= map.getZoom();
		var intZoomMultiply	= 1;

		var intLng			= center.lng();
		var intLat			= center.lat();

		if (intZoomLevel == 16)
		{intZoomMultiply = 2
		}
		
		if (intZoomLevel == 15)
		{intZoomMultiply = 4
		}
		
		if (intZoomLevel == 14)
		{intZoomMultiply = 8
		}
		
		if (intZoomLevel == 13)
		{intZoomMultiply = 16
		}
		
		if (intZoomLevel == 12)
		{intZoomMultiply = 32
		}
		
		if (strDirection == 1)
		{intLng = intLng - (0.05 * intZoomMultiply);
		}

		if (strDirection == 2)
		{intLng = intLng + (0.05 * intZoomMultiply);
		}

		if (strDirection == 3)
		{intLat = intLat + (0.04 * (intZoomMultiply/2));
		}

		if (strDirection == 4)
		{intLat = intLat - (0.04 * (intZoomMultiply/2));
		}

		//map.getZoom() = intZoomLevel
		//var currLocation = map.setCenter(new GLatLng(intLat, intLng), 14);
		//getData(center.lat(), center.lng(), strTicks);

		//map.panTo(new GLatLng(intLat,intLng), 10);
		
		map.panTo(new GLatLng(intLat,intLng), intZoomLevel);
		//getData(center.lat(), center.lng(), strTicks);
		//checkForm()
}

function newZoom(zLevel) {

		var intCurrentZoomLevel	= map.getZoom();
		var intNewZoomLevel		= zLevel;
		var center				= map.getCenter();
		var strTicks			= map.getBounds();
		
		if (intNewZoomLevel == 1)
		{intNewZoomLevel = intCurrentZoomLevel + 1
		}

		if (intNewZoomLevel == 2)
		{intNewZoomLevel = intCurrentZoomLevel - 1
		}
		if (intNewZoomLevel == 8)
		{intNewZoomLevel = 9
		}
		
		map.setCenter(new GLatLng(center.lat(),center.lng()), intNewZoomLevel);
	}
	
window.onunload = function() {
  GUnload();
};

