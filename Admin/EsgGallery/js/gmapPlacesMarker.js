﻿//script for map with places-marker-----------------------------
var map, places, iw;
var markers = [];
var autocomplete;
var image = 'images/home.png';
var MarkerImage = '';
var WebsiteURL = 'http://gates.esuk.co.uk/';

function initialize() {
    var latlon = document.getElementById('hdnLatLon').value.split(",");
    var vlat = latlon[0];
    var vlan = latlon[1];
    //      alert(vlat);
    //      alert(vlan);
    var myLatlng = new google.maps.LatLng(vlat, vlan);
    var myOptions = {
        zoom: 14,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map//,
        //icon: image
    });

    attachMessage(marker, vlat, vlan);

    places = new google.maps.places.PlacesService(map);
    google.maps.event.addListener(map, 'tilesloaded', tilesLoaded);
    autocomplete = new google.maps.places.Autocomplete(document.getElementById('autocomplete'));
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        showSelectedPlace();
    });
}

function attachMessage(marker, lat, lng) {
    var infowindow = new google.maps.InfoWindow(
      { content: 'Lattitude: <b>' + Math.round(lat * 100000) / 100000 + '</b></br> Longitude : <b>' + Math.round(lng * 100000) / 100000 + '</b>',
          size: new google.maps.Size(50, 50)
      });
    google.maps.event.addListener(marker, 'click', function () {
        infowindow.open(map, marker);
    });
}

function tilesLoaded() {
    google.maps.event.clearListeners(map, 'tilesloaded');
    google.maps.event.addListener(map, 'zoom_changed', search);
    google.maps.event.addListener(map, 'dragend', search);
    search();
}

function showSelectedPlace() {
    clearResults();
    clearMarkers();
    var place = autocomplete.getPlace();
    map.panTo(place.geometry.location);
    markers[0] = new google.maps.Marker({
        position: place.geometry.location,
        map: map
    });
    iw = new google.maps.InfoWindow({
        content: getIWContent(place)
    });
    iw.open(map, markers[0]);
}

function search() {
    //    alert("Hi");
    var type;
    for (var i = 0; i < document.forms[0].type.length; i++) {
        if (document.forms[0].type[i].checked) {
            type = document.forms[0].type[i].value;
        }
    }
    //    for (var i = 0; i < document.controls.type.length; i++) {
    //        if (document.controls.type[i].checked) {
    //            type = document.controls.type[i].value;
    //        }
    //    }    
    autocomplete.setBounds(map.getBounds());

    var search = {
        bounds: map.getBounds()
    };
        
    if (type != 'establishment') {
        search.types = [type];
    }
    else {
        search.types = ["restaurant", "lodging", "school", "bank", "pharmacy", "shopping_mall"]
    }
        
    places.search(search, function (results, status) {
        clearResults();
        clearMarkers();
        if (status == google.maps.places.PlacesServiceStatus.OK) {            
            switch (search.types[0]) {
                case "establishment":
                    MarkerImage = WebsiteURL + "images/All.png";
                    break;
                case "restaurant":
                    MarkerImage = WebsiteURL + "images/Restaurants.png";
                    break;
                case "lodging":                    
                    MarkerImage = WebsiteURL + "images/Loadging.png";
                    break;
                case "school":
                    MarkerImage = WebsiteURL + "images/Schools.png";
                    break;
                case "bank":
                    MarkerImage = WebsiteURL + "images/bank.png";
                    break;
                case "pharmacy":
                    MarkerImage = WebsiteURL + "images/Pharmacy.png";
                    break;
                case "shopping_mall":
                    MarkerImage = WebsiteURL + "images/ShoppingMalls.png";
                    break;

                default:
                    MarkerImage = WebsiteURL + "images/All.png";
                    break;
            }

            for (var i = 0; i < results.length; i++) {

                if (search.types[0] == "restaurant" && search.types[1] == "lodging" && search.types[2] == "school" && search.types[3] == "bank"
            && search.types[4] == "pharmacy" && search.types[5] == "shopping_mall") {                    
                    //if (MarkerImage == WebsiteURL + "images/All.png") {
                    if (results[i].types.indexOf("restaurant") != -1) {
                        MarkerImage = WebsiteURL + "images/Restaurants.png";
                    }
                    else if (results[i].types.indexOf("lodging") != -1) {
                        MarkerImage = WebsiteURL + "images/Loadging.png";
                    }
                    else if (results[i].types.indexOf("school") != -1) {
                        MarkerImage = WebsiteURL + "images/Schools.png";
                    }
                    else if (results[i].types.indexOf("bank") != -1) {
                        MarkerImage = WebsiteURL + "images/bank.png";
                    }
                    else if (results[i].types.indexOf("pharmacy") != -1) {
                        MarkerImage = WebsiteURL + "images/Pharmacy.png";
                    }
                    else if (results[i].types.indexOf("shopping_mall") != -1) {
                        MarkerImage = WebsiteURL + "images/ShoppingMalls.png";
                    }
                    else {
                        MarkerImage = WebsiteURL + "images/All.png";
                    }
                }
                markers[i] = new google.maps.Marker({
                    position: results[i].geometry.location,
                    animation: google.maps.Animation.DROP,
                    icon: MarkerImage
                });
                google.maps.event.addListener(markers[i], 'click', getDetails(results[i], i));
                setTimeout(dropMarker(i), i * 100);
                addResult(results[i], i);
            }
        }
    })
}

function clearMarkers() {
    for (var i = 0; i < markers.length; i++) {
        if (markers[i]) {
            markers[i].setMap(null);
            markers[i] == null;
        }
    }
}

function dropMarker(i) {
    return function () {
        markers[i].setMap(map);
    }
}

function addResult(result, i) {
    var results = document.getElementById("results");
    var tr = document.createElement('tr');
    tr.style.backgroundColor = (i % 2 == 0 ? '#F0F0F0' : '#FFFFFF');
    tr.onclick = function () {
        google.maps.event.trigger(markers[i], 'click');
    };

    var iconTd = document.createElement('td');
    var nameTd = document.createElement('td');
    var icon = document.createElement('img');
    icon.src = result.icon;
    icon.setAttribute("class", "placeIcon");
    icon.setAttribute("className", "placeIcon");
    var name = document.createTextNode(result.name);
    iconTd.appendChild(icon);
    nameTd.appendChild(name);
    tr.appendChild(iconTd);
    tr.appendChild(nameTd);
    results.appendChild(tr);
}

function clearResults() {
    var results = document.getElementById("results");
    while (results.childNodes[0]) {
        results.removeChild(results.childNodes[0]);
    }
}

function getDetails(result, i) {
    return function () {
        places.getDetails({
            reference: result.reference
        }, showInfoWindow(i));
    }
}

function showInfoWindow(i) {
    return function (place, status) {
        if (iw) {
            iw.close();
            iw = null;
        }

        if (status == google.maps.places.PlacesServiceStatus.OK) {
            iw = new google.maps.InfoWindow({
                content: getIWContent(place)
            });
            iw.open(map, markers[i]);
        }
    }
}

function getIWContent(place) {
    //debugger;
    var content = "";
    content += '<table>';
    content += '<tr><td>';
    content += '<img class="placeIcon" src="' + place.icon + '"/></td>';
    content += '<td><b><a href="' + place.url + '">' + place.name + '</a></b>';
    content += '</td></tr>';
    content += '<tr>';
    content += '<td colspan="2">Lattitude : <b>' + Math.round(place.geometry.location.lat() * 100000) / 100000 + '</b>';
    content += '<td colspan="2">Longitude : <b>' + Math.round(place.geometry.location.lng() * 100000) / 100000 + '</b>';
    //    content += '<td colspan="2">Lattitude : <b>' + place.geometry.location.Xa + '</b>';
    //    content += '<td colspan="2">Longitude : <b>' + place.geometry.location.Ya + '</b>';
    content += '</td></tr>';

    content += '</table>';
    return content;
}
//--------------------------------------------------------------