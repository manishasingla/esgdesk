// JavaScript Document

var n=0; 
function chkloaded() {

  if ((typeof( GBrowserIsCompatible ) ).toLowerCase() == 'undefined') { 
      setTimeout('chkloaded()',1000); 
      document.getElementById("map").innerHTML = "trying times this is!! " + n++ ; 
  } else {
	  try { GUnload(); } catch(e) { }
     loadMap(); 
  }
}

function chkloaded2() {
  if ((typeof( GBrowserIsCompatible) ).toLowerCase() == 'undefined') { 
      setTimeout('chkloaded2()',1000); 
      document.getElementById("map1").innerHTML = "trying times this is!! " + n++ ; 
  } else {
	  try { GUnload(); } catch(e) { }
     loadMap2(); 
  }
}


var geocoder;
var bounds;
var baseIcon;
var map;
var point;
var zoomlevel=15;

var northeastlat;
var northeastlon;
var southwestlat;
var southwestlon;

function loadMap()
{
	if(GBrowserIsCompatible())
	{
		map = new GMap2(document.getElementById('map'));
		geocoder = new GClientGeocoder();
		map.addControl(new GMapTypeControl());
		map.enableDoubleClickZoom();
		map.addControl(new GLargeMapControl()); 
		map.enableContinuousZoom();
		var latlon = document.getElementById('hdnLatLon').value.split(",");
		var vqhd = latlon[0];
		var vpa = latlon[1];
		map.setCenter(new GLatLng(vqhd, vpa),2);
		baseIcon = new GIcon();
		baseIcon.iconSize = new GSize(24, 23);
		baseIcon.iconAnchor = new GPoint(9, 34);
		baseIcon.infoWindowAnchor = new GPoint(9, 2);
		baseIcon.infoShadowAnchor = new GPoint(18, 25);
		
		var point = new GPoint(vpa,vqhd);
		//var marker = new GMarker(point);
		var icon = new GIcon(baseIcon);
		icon.image="images/home.png";
		var marker = new GMarker(point, icon);
		
		var details = document.getElementById('hdnpropdetails').value;
		GEvent.addListener(marker, "mouseover", function() {
			var html1="<div style='width:223px;max-height:300px; font-size:12px;'><table bgcolor='#FFFFFF' width='223' border='0' cellspacing='0' cellpadding='0'><tr><td><h4>Property Information</h4></td> </tr><tr><td align='center' valign='middle' width='100%' height='150px'><table><tr><td><div style='padding-top:3px; padding-right:3px; padding-bottom:3px; padding-left:3px; border:2px outset #999999'><img src='"+document.getElementById('hdnpropimg').value+"'width='150' height='150' onload='setListingsImage(this);' /></div></td></tr></table></td></tr><tr><td align='left'><strong>"+document.getElementById('lblAddress').innerHTML+"</strong></td></tr> <tr><td align='left'>"+document.getElementById('lblPrice').innerHTML+"</td> </tr><tr><td align='left'><strong>"+document.getElementById('lblMisc').innerHTML+"</strong></td></tr><tr><td align='right'><a href='"+details+"' style='color:#006599;'>More Details &gt;&gt; </a></td></tr></table></div>";
		marker.openInfoWindowHtml(html1);
		});
	
		map.addOverlay(marker);
		map.setCenter(new GLatLng(vqhd, vpa));
		map.setZoom(zoomlevel);
		bounds = map.getBounds();
	  	zoomlevel = map.getZoom();
		northeastlat = bounds.getNorthEast().lat();
		northeastlon = bounds.getNorthEast().lng();
		southwestlat = bounds.getSouthWest().lat();
		southwestlon = bounds.getSouthWest().lng();
		try {
			var checklist = document.getElementById('Repeater2');
			var spanitems = checklist.getElementsByTagName('span');
			for(var i=0;i<spanitems.length;i++)
			{
				if(hashMarkers[spanitems[i].id] == null) { hashMarkers[spanitems[i].id] = new Array(); }
			}
		}
		catch (e) { }
		GEvent.addListener(map,'moveend',function() {
		getsetPOIsOnZoom();
		});
		/*GEvent.addListener(map,'zoomend',function() {
		getPOIs();
		});*/
	}
}


function loadMap2()
{
	if(GBrowserIsCompatible())
	{
	
		map = new GMap2(document.getElementById('map1'));
		geocoder = new GClientGeocoder();
		map.addControl(new GMapTypeControl());
		map.enableDoubleClickZoom();
		map.addControl(new GLargeMapControl()); 
		map.enableContinuousZoom();
		var latlon = document.getElementById('hdnLatLon').value.split(",");
		var vqhd = latlon[0];
		var vpa = latlon[1];
		map.setCenter(new GLatLng(vqhd, vpa),2);
		baseIcon = new GIcon();
		baseIcon.iconSize = new GSize(24, 23);
		baseIcon.iconAnchor = new GPoint(9, 34);
		baseIcon.infoWindowAnchor = new GPoint(9, 2);
		baseIcon.infoShadowAnchor = new GPoint(18, 25);
		var point = new GPoint(vpa,vqhd);
		//var marker = new GMarker(point);
		var icon = new GIcon(baseIcon);
		icon.image="images/home.png";
		var marker = new GMarker(point, 2);
		
		/*var details = document.getElementById('hdnpropdetails').value;
		GEvent.addListener(marker, "mouseover", function() {
			var html1="<div style='width:223px;max-height:300px; font-size:12px;'><table bgcolor='#FFFFFF' width='223' border='0' cellspacing='0' cellpadding='0'><tr><td><h4>Property Information</h4></td> </tr><tr><td align='center' valign='middle' width='100%' height='150px'><table><tr><td><div style='padding-top:3px; padding-right:3px; padding-bottom:3px; padding-left:3px; border:2px outset #999999'><img src='"+document.getElementById('hdnpropimg').value+"'width='150' height='150' onload='setListingsImage(this);' /></div></td></tr></table></td></tr><tr><td align='left'><strong>"+document.getElementById('lblAddress').innerHTML+"</strong></td></tr> <tr><td align='left'>"+document.getElementById('lblPrice').innerHTML+"</td> </tr><tr><td align='left'><strong>"+document.getElementById('lblMisc').innerHTML+"</strong></td></tr><tr><td align='right'><a href='"+details+"' style='color:#006599;'>More Details &gt;&gt; </a></td></tr></table></div>";
		marker.openInfoWindowHtml(html1);
		});*/
	
		map.addOverlay(marker);
		map.setCenter(new GLatLng(vqhd, vpa));
		map.setZoom(zoomlevel);
		bounds = map.getBounds();
	  	zoomlevel = map.getZoom();
		northeastlat = bounds.getNorthEast().lat();
		northeastlon = bounds.getNorthEast().lng();
		southwestlat = bounds.getSouthWest().lat();
		southwestlon = bounds.getSouthWest().lng();
		try {
			var checklist = document.getElementById('Repeater2');
			var spanitems = checklist.getElementsByTagName('span');
			for(var i=0;i<spanitems.length;i++)
			{
				if(hashMarkers[spanitems[i].id] == null) { hashMarkers[spanitems[i].id] = new Array(); }
			}
		}
		catch (e) { }
		GEvent.addListener(map,'moveend',function() {
		getsetPOIsOnZoom();
		});
		/*GEvent.addListener(map,'zoomend',function() {
		getPOIs();
		});*/
	}
}



function getPOIs() {
	//alert("fired................");
	var oldzoomlevel = zoomlevel;
	zoomlevel = map.getZoom();
	if(zoomlevel <= oldzoomlevel)
	{
		var obj;
		if (window.XMLHttpRequest)
		{ 
			// Mozilla, Safari, ...
			obj = new XMLHttpRequest();
			if (obj.overrideMimeType)
			{
				obj.overrideMimeType('text/xml');
			}
		} 
		else if (window.ActiveXObject) 
		{ // IE
			try
			{
				obj = new ActiveXObject("Msxml2.XMLHTTP");
			}
			catch (e)
			{
				try
				{
					obj = new ActiveXObject("Microsoft.XMLHTTP");
				}
				catch (e1)
				{
					obj = null;
				}
			}
		}
		if (obj==null)
		{
			alert('Giving up :( Cannot create an XMLHTTP instance');
		}
		else
		{		
			obj.onreadystatechange = function() {
				if(obj.readyState == 4 && obj.status == 200)
				{
					//alert(obj.responseText);
					var dsRoot=obj.responseXML.documentElement;
					//alert(dsRoot);
					if(dsRoot.childNodes.length > 0) { POIsMarkers(dsRoot); }
				}
				else
				{
					//alert("Error retrieving data!");
				}
			}
			var url = window.location.href.split("//")[1].split("/")[0];
			//url = url.match(".com") == null?(url+"/hs"):url;
			
			bounds = map.getBounds();
		  
			northeastlat = bounds.getNorthEast().lat();
			northeastlon = bounds.getNorthEast().lng();
			southwestlat = bounds.getSouthWest().lat();
			southwestlon = bounds.getSouthWest().lng();
			
			var checklist = document.getElementById('Repeater2');
			var spanitems = checklist.getElementsByTagName('span');
			var checkeditems = checklist.getElementsByTagName('input');
			var qs="";
			for(var i=0;i<checkeditems.length;i++)
			{
				qs += checkeditems[i].type=='checkbox' && checkeditems[i].checked?(spanitems[i].id.toString()+";"):"";
			}
			obj.open("GET", "http://"+url+"/getPOIsData.aspx?categoryid="+qs+"&northeastlat="+northeastlat+"&northeastlon="+northeastlon+"&southwestlat="+southwestlat+"&southwestlon="+southwestlon, true);
			obj.send(null);
		}
	}
	return false;	
}

function checkboxClicked(objcheckbox,categoryid) {
	getsetPOIs(objcheckbox,categoryid)
}

function getsetPOIs(objcheckbox,categoryid) {
	//alert("fired................");
	if(objcheckbox.checked == false)
	{
		for (var x=0;x<hashMarkers[categoryid].length;x++)
		{
			hashMarkers[categoryid][x].hide();
		}
	}
	else {
		var obj;
		if (window.XMLHttpRequest)
		{
			obj = new XMLHttpRequest();
			if (obj.overrideMimeType)
			{
				obj.overrideMimeType('text/xml');
			}
		} 
		else if (window.ActiveXObject) 
		{
			try
			{
				obj = new ActiveXObject("Msxml2.XMLHTTP");
			}
			catch (e)
			{
				try
				{
					obj = new ActiveXObject("Microsoft.XMLHTTP");
				}
				catch (e1)
				{
					obj = null;
				}
			}
		}
		if (obj==null)
		{
			alert('Giving up :( Cannot create an XMLHTTP instance');
		}
		else
		{		
			obj.onreadystatechange = function() {
				if(obj.readyState == 4 && obj.status == 200)
				{
					//alert(obj.responseText);
					var dsRoot=obj.responseXML.documentElement;
					
					if(dsRoot.childNodes.length > 0) { 
						if(dsRoot.childNodes.length > 500) { 
							var confirmvalue=confirm("More than 500 POIs are about to be plotted and may take some time. Either click cancel and zoom in to reduce the number of POIs or click Ok to continue regardless"); 
							objcheckbox.checked = confirmvalue; 
							if(confirmvalue==true) {if(hashMarkers[categoryid] == null) { hashMarkers[categoryid] = new Array(); }
								POIsMarkers(dsRoot);
							}
							else {
								for (var x=0;x<hashMarkers[categoryid].length;x++)
								{
									hashMarkers[categoryid][x].hide();
								}
							}
						}
						else { if(hashMarkers[categoryid] == null) { hashMarkers[categoryid] = new Array(); } POIsMarkers(dsRoot); }
					}
				}
				else
				{
					//alert("Error retrieving data!");
				}
			}
			var url = window.location.href.split("//")[1].split("/")[0];
			//url = url.match(".com") == null?(url+"/hs"):url;
			
			bounds = map.getBounds();		  
			northeastlat = bounds.getNorthEast().lat();
			northeastlon = bounds.getNorthEast().lng();
			southwestlat = bounds.getSouthWest().lat();
			southwestlon = bounds.getSouthWest().lng();
			
			obj.open("GET", "http://"+url+"/getPOIsData.aspx?categoryid="+categoryid+"&northeastlat="+northeastlat+"&northeastlon="+northeastlon+"&southwestlat="+southwestlat+"&southwestlon="+southwestlon, true);
			obj.send(null);
		}
	}
}

var hashMarkers = new Array();

function POIsMarkers(values) {
	var name;
	var lat;
	var lon;
	var desc;
	var cat;
	
	for(var i=0;i<values.childNodes.length;i++)
	{
		name="";
		lat="";
		lon="";
		desc="";
		cat="";
		for(var j=0; j< values.childNodes[i].childNodes.length;j++)
		{
			var objitem = values.childNodes[i].childNodes[j];
			switch(objitem.nodeName.toLowerCase())
			{
				case "name":
					name = (objitem.textContent || objitem.innerText || objitem.text || "");
					break;
				case "description":
					desc = (objitem.textContent || objitem.innerText || objitem.text || "");
					break;
				case "latitude":
					lat =(objitem.textContent || objitem.innerText || objitem.text || "");
					break;
				case "longitude":
					lon = (objitem.textContent || objitem.innerText || objitem.text || "");
					break;
				case "categoryid":
					cat = (objitem.textContent || objitem.innerText || objitem.text || "");
					break;
				default:
					break;
			}
		}
		point = new GLatLng(lat,lon);
		//bounds.extend(point);
		createPOIMarker(name,lat,lon,desc,cat);		
	}
}

function createPOIMarker(name,lat,lon,desc,cat) {
	var icon = new GIcon(baseIcon);
	icon.image= "images/"+cat+".gif";
	var marker = new GMarker(point, icon);
	try { if(hashMarkers[cat][marker] == null) { hashMarkers[cat].push(marker); }
	else { hashMarkers[cat][marker].show(); return; } }
	catch(e) {}
	GEvent.addListener(marker, "click", function() {
	var html1="<div style='width:150px; font-size:12px;'><table width='150'><tr><td align='left'><h3>"+name+"</h3>Other details : <br>"+desc+"</td></tr>";
	html1+="<tr><td align='left' width='150'>Geographical coordinates : <br />Latitude :"+lat+"<br />Longitude : "+lon+"</td></tr></table></div>";
	marker.openInfoWindowHtml(html1);
	});
	map.addOverlay(marker);
}

function getsetPOIsOnZoom() {
	//alert("fired................");
	var oldzoomlevel = zoomlevel;
	zoomlevel = map.getZoom();
	if(zoomlevel <= oldzoomlevel)
	{
		var obj;
		if (window.XMLHttpRequest)
		{ 
			// Mozilla, Safari, ...
			obj = new XMLHttpRequest();
			if (obj.overrideMimeType)
			{
				obj.overrideMimeType('text/xml');
			}
		} 
		else if (window.ActiveXObject) 
		{ // IE
			try
			{
				obj = new ActiveXObject("Msxml2.XMLHTTP");
			}
			catch (e)
			{
				try
				{
					obj = new ActiveXObject("Microsoft.XMLHTTP");
				}
				catch (e1)
				{
					obj = null;
				}
			}
		}
		if (obj==null)
		{
			alert('Giving up :( Cannot create an XMLHTTP instance');
		}
		else
		{		
			obj.onreadystatechange = function() {
				if(obj.readyState == 4 && obj.status == 200)
				{
				////alert(obj.responseText);
						var dsRoot=obj.responseXML.documentElement;
						
						if(dsRoot.childNodes.length > 0) { 
							if(dsRoot.childNodes.length > 500) { 
								var confirmvalue=confirm("More than 500 POIs are about to be plotted and may take some time. Either click cancel and zoom in to reduce the number of POIs or click Ok to continue regardless");
								if(confirmvalue==true) {
								POIsMarkers(dsRoot);
							}
						}
						else { POIsMarkers(dsRoot); }
						}
				}
				else
				{
					//alert("Error retrieving data!");
				}
			}
			var url = window.location.href.split("//")[1].split("/")[0];
			//url = url.match(".com") == null?(url+"/hs"):url;
			
			bounds = map.getBounds();
		  
			northeastlat = bounds.getNorthEast().lat();
			northeastlon = bounds.getNorthEast().lng();
			southwestlat = bounds.getSouthWest().lat();
			southwestlon = bounds.getSouthWest().lng();
			
			var checklist = document.getElementById('Tabs_Panel3_Repeater2');
			var spanitems = checklist.getElementsByTagName('span');
			var checkeditems = checklist.getElementsByTagName('input');

			var qs="";
			for(var i=0;i<checkeditems.length;i++)
			{

				qs += checkeditems[i].type=='checkbox' && checkeditems[i].checked?(spanitems[i].id.toString()+";"):"";
			}

			obj.open("GET", "http://"+url+"/getPOIsData.aspx?categoryid="+qs+"&northeastlat="+northeastlat+"&northeastlon="+northeastlon+"&southwestlat="+southwestlat+"&southwestlon="+southwestlon, true);
			obj.send(null);
		}
	}
	return false;
}

window.onunload = function() {
  GUnload();
};

