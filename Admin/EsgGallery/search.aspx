﻿<%@ Page Language="C#" MasterPageFile="~/Admin/EsgGallery/AdminMasterMenu.master" AutoEventWireup="true" CodeFile="search.aspx.cs" Inherits="search" Title="Search Photos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h2>Search Photos</h2>
Enter string that is part of title or tag : 
    <asp:TextBox ID="txtString" runat="server" Width="234px"></asp:TextBox>*
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"  ControlToValidate ="txtString"
        ErrorMessage="You must enter a string to start search!"></asp:RequiredFieldValidator>
    <p />
    <asp:Button ID="btnSearch" runat="server" Text="Search" onclick="btnSearch_Click" />
    
    <p />
        &nbsp;<asp:ListView ID="ListView1" runat="server" DataSourceID="odsPhotos" 
        GroupItemCount ="3" ondatabound="ListView1_DataBound">
     <LayoutTemplate>
     <center>
      <table id="Table1" runat="server">
       <tr runat="server" id="groupPlaceholder"/>
      </table>
      <p />
      <asp:DataPager ID="DataPager1" runat="server" PagedControlID = "ListView1" PageSize="6">
        <Fields>
            <asp:NumericPagerField />
        </Fields>
       </asp:DataPager>
      </center>
     </LayoutTemplate>
     
     <GroupTemplate>
         <tr id="Tr1" runat="server">
             <td id="itemPlaceholder" runat="server" />
         </tr>
     </GroupTemplate>
     
      <ItemTemplate>
          <td>
            <a href='showphoto.aspx?photoid=<%#Eval("photoid")%>&aid=<%#Eval("aid")%>'>
            <img src='photos/<%# Eval("photoid")%>.jpg' width="150px" height="100px" />
            </a>
            <br />
            <%#Eval("title")%> 
            <br />
          </td>
     </ItemTemplate>
     
     <EmptyDataTemplate>
       <h4>No photos matched the given search pattern!</h4>
     </EmptyDataTemplate>
     
     
    </asp:ListView>
    
        <asp:ObjectDataSource ID="odsPhotos" runat="server" 
            SelectMethod="SearchForPhotos" TypeName="PhotosDAL">
            <SelectParameters>
                <asp:ControlParameter ControlID="txtString" Name="pattern" PropertyName="Text" 
                    Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>

    
</asp:Content>

