﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class search : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ListView1.Visible = Page.IsPostBack;
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
    }
    protected void ListView1_DataBound(object sender, EventArgs e)
    {
        DataPager pager = (DataPager)ListView1.FindControl("DataPager1");
        if (pager != null)
            pager.Visible = (pager.PageSize < pager.TotalRowCount);

    }
}
