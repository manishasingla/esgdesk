﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net.Mail;

public partial class sharealbum : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSend_Click(object sender, EventArgs e)
    {
        // send mail 
        try
        {
            string strBody = "";
            bool blnIsSent1 = false;
            bool blnIsSent2 = false;
            string emailid1 = "", emailid2 = "", strEmailSucess = "", strSubject="";
            strSubject = txtSubject.Text;
            emailid1 = txtEmail.Text;
            emailid2 = System.Configuration.ConfigurationManager.AppSettings["LMS2"].ToString();


            strBody = "Hello, <p/>Click here to view photos .<p/> <a target='_blank' href=http://" +
                     Request.ServerVariables["http_host"] +
                     "/photos/all/viewalbum.aspx?aid=" + Request.QueryString["aid"] + ">Click here to view photos </a>" +
                     "<p/>Best Wishes<br/>ESG";

            blnIsSent1 = DatabaseHelper.sendEmailEsgGallery(emailid1, strSubject, strBody);
            blnIsSent2 = DatabaseHelper.sendEmailEsgGallery(emailid2, strSubject, strBody);
           

           
            if (blnIsSent1)
                strEmailSucess += "A mail has been sent to your friend to enable your friend to view your album. ";
            //if (blnIsSent2)
              //  strEmailSucess += "Rituraj Kaur<br/>";
            lblMsg.Text = strEmailSucess;
        
        }
        catch (Exception ex)
        {
            Trace.Write("Error -> " + ex.Message);
            lblMsg.Text = "Sorry! Could not send mail to your friend.";
        }


              

    }
}
