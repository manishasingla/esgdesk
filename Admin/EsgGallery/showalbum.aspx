﻿<%@ Page Language="C#" MasterPageFile="~/Admin/EsgGallery/AdminMasterMenu.master" AutoEventWireup="true" CodeFile="showalbum.aspx.cs" Inherits="showalbum"  Trace="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">

    function imageLoaded(img) {
        var loadingImgId = img.alt;
        document.getElementById(loadingImgId).style.visibility = 'hidden';

        img.style.visibility = 'visible';
        setListingsImage(img);
    }

</script>
   <style type="text/css">

        .image-holder
        {
            float: left;
            width: 270px;
          /*  height: 340px;*/
            padding: 10px;
            border: 1px solid #ddd;
            background: url(../images/ajax-loader-mini.gif) no-repeat;
            display: inline;
			height:260px;
        }
        
        
   /*  .show-album01
     {
         width:300px;
          height: 50px;
          clear:both;} */
		  
		  
   .addto-description{
	  color: #0054A6;
    float: left;
    font-size: 14px;
    font-style: normal;
    margin-left: 15px;}	
	
	
	.show-button{
background: #d3d3d3; /* Old browsers */
background: -moz-linear-gradient(top,  #d3d3d3 0%, #fffbfb 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#d3d3d3), color-stop(100%,#fffbfb)); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  #d3d3d3 0%,#fffbfb 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  #d3d3d3 0%,#fffbfb 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  #d3d3d3 0%,#fffbfb 100%); /* IE10+ */
background: linear-gradient(to bottom,  #d3d3d3 0%,#fffbfb 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d3d3d3', endColorstr='#fffbfb',GradientType=0 ); /* IE6-9 */
    border-radius: 2px 2px 2px 2px;
    color: #333333;
    font-size: 13px;
    font-style: normal;
    padding: 5px;
	border: 1px solid #C1C1C1;
}

	.show-button:hover{
background: #d3d3d3; /* Old browsers */
background: -moz-linear-gradient(top,  #d3d3d3 0%, #fffbfb 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#d3d3d3), color-stop(100%,#fffbfb)); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  #d3d3d3 0%,#fffbfb 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  #d3d3d3 0%,#fffbfb 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  #d3d3d3 0%,#fffbfb 100%); /* IE10+ */
background: linear-gradient(to bottom,  #d3d3d3 0%,#fffbfb 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d3d3d3', endColorstr='#fffbfb',GradientType=0 ); /* IE6-9 */

    border-radius: 2px 2px 2px 2px;
	border: 1px solid #C1C1C1;
    color: #333333;
    font-size: 13px;
    font-style: normal;
    padding: 5px;
}
	
		    
     
    </style>

<script type="text/javascript">

    function setListingsImage(obj)
     {
        var img = new Image();
        img.src = obj.src;
        if (img.height > img.width) {
            obj.height = 340;
            var width = (300 * img.width) / img.height;
            width = width > 340 ? 340 : width;
            obj.width = (width > 0 ? width : 320);
        }
        else {
            obj.width = 300;
            var height = (340 * img.height) / img.width;
            height = height > 400 ? 400 : height;
            obj.height = (height > 0 ? (height - 6) : 340);

        }
    }
       </script>

  <link href="css/mytooltip.css" rel="stylesheet" type="text/css" />
    <table width="100%">
      
<tr>
<td valign="top" width="20%" style="background-color:#ffffff; border:1px solid #cccccc;">

<!--<div class="show-album01"><img style="float: left; margin-top: 15px; margin-left: 16px; margin-right: 10px;" src="images/flower.jpg" align="absmiddle" />&nbsp;
<span style="font-size: 15px; color: rgb(0, 166, 81); font-weight: normal; display: block; margin-top: 0px;">Test album</span> 
<span style="display: block; margin-left: 57px;">January 18</span></div>
<br/><br/>
<div class="addto-description">Add a description</div> <br/>
<div style="margin-left:12px;"><img src="images/showalbum.png" /></div>-->
    <asp:FormView ID="FormView1" runat="server"
      ondatabound="FormView1_DataBound">
      <ItemTemplate>
      <div>
      
<div class="show-album01"><img style="float: left; margin-top: 22px; margin-left: 16px; margin-right: 10px;" src="images/flower.jpg" align="absmiddle" />&nbsp;
<span style="font-size: 15px; color: rgb(0, 166, 81); font-weight: normal; display: block; margin-top: 0px; padding-right: 201px;"><%# Eval("title")%> (<%# Eval("nophotos") %>)</span> 
<span style="display: block; margin-left: 57px; padding-right:168px;"><%# Eval("createdon")%></span>
<span style="padding-right:175px;"><%# Eval("description") %></span>
</div>
<br/><br/>
<div class="addto-description">Add a description</div> 
<div style="margin-left:12px;"><img src="images/showalbum.png" /></div>
      <div style=" z-index:-1px; text-align:center;">
     <img id='<%#Eval("firstphoto")%>CoverToHide' class="image-holder" align="middle" style="z-index:9999"/>
     </div> 
     
     <div style="position: absolute; z-index: 9999; text-align:center; margin: 0 0 0 13px;">
     <img ID="PhotoToDisplay" src='photos/<%# Eval("title")%>/<%# Eval("firstphoto")%>.jpg' alt='<%#Eval("firstphoto")%>CoverToHide' style="visibility:hidden;margin: 0px 0 0 0px;
         cursor:pointer;z-index:-1000" width="300" height="340"  onload="javascript:imageLoaded(this);" />
         <div style="height:10px;"></div>
     </div>
     
     
    
      </div>
      </p>
 
   <!-- <h4><%# Eval("title")%> (<%# Eval("nophotos") %>) </h4>
      <%# Eval("createdon")%>
      <p />
      <%# Eval("description") %>
      <p />-->

    
    <p style="clear: both; position: absolute; top: 557px; left: 17px;">
      <asp:Panel runat="server" ID="innerPanel" Visible="false">
      <a href='uploadphotos.aspx?aid=<%# Eval("aid")%>&atitle=<%# Eval("title")%>'>Upload photos</a><br />
      </asp:Panel>
     
      <a class="show-button" href='FullSlideShow.aspx?aid=<%#Eval("aid")%>&atitle=<%#Eval("title")%>'>Full slide show</a>
    </p>
    </ItemTemplate>
    </asp:FormView>
     <asp:Panel runat="server" ID="outerPanel" Visible="false">
   
  
    <asp:LinkButton ID="LinkButton1" runat="server" onclick="LinkButton1_Click" 
        OnClientClick="return window.confirm('Deleting album deletes all photos in the album. Do you want to delete album?')">Delete album</asp:LinkButton>
 
     <a href='Default.aspx'>Back</a>
    
      </asp:Panel>
           
<%# Eval("description")%>
   
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
              SelectMethod="GetAlbumDetails" TypeName="PhotosDAL">
        <SelectParameters>
            <asp:QueryStringParameter Name="aid" QueryStringField="aid" Type="String" />
        </SelectParameters>
          </asp:ObjectDataSource>
    <asp:HiddenField ID="hdnCounter" runat="server" Value="0" />
</td>
<td valign="top"  width="90%">
<asp:ListView 
<asp:ListView ID="ListView1" runat="server" GroupItemCount ="5" ondatabound="ListView1_DataBound" 
 OnSelectedIndexChanged="ListView1_SelectedIndexChanged"    >
     <LayoutTemplate>
     <div>
      <table id="Table1" runat="server" cellpadding="0" cellspacing="0">
       <tr>
       <td  runat="server" id="groupPlaceholder"></td>
       </tr>
      </table>
      <asp:DataPager ID="DataPager1" runat="server" PagedControlID = "ListView1" >
        <Fields>
            <asp:NumericPagerField />
        </Fields>
       </asp:DataPager>
      </div>
     </LayoutTemplate>
     <GroupTemplate>
         <tr id="Tr1" runat="server">
             <td id="itemPlaceholder" runat="server" />
         </tr>
     </GroupTemplate>
     <ItemTemplate>
          <td>
          
            <%--<a href='showphoto.aspx?photoid=<%#Eval("photoid")%>&aid=<%#Eval("aid")%>&atitle=<%#Eval("atitle")%>'>--%>
            
            <a href='<%# href  %>'>
            <%-- <div style=" z-index:-1px; text-align:center;">
             <img id='<%#Eval("photoid")%>ToHide' class="image-holder" align="middle" style="z-index:9999;"/>
                              <img id='<%#Eval("atitle")%><%#Eval("photoid")%>uniqToDisplay' src="<%# DataBinder.Eval(Container.DataItem,"item") %>"ImageUrl='photos/<%# Eval("atitle") %>/<%#Eval("photoid")%>.jpg'  
                 alt='<%#Eval("photoid")%>ToHide' style="visibility:hidden;margin: 0px 0 0 0px;
                 cursor:pointer;z-index:-1000" width="300" height="340" />

             </div>--%>
             
             <div style="z-index: 9999; margin-left:7px; text-align:center;">
                        <img style="opacity: 0.7; width: 200px; height="340" max-height: 150px; min-height: 150px;" src="<%# DataBinder.Eval(Container.DataItem,"item") %>"
                        onerror="images/coming_soon1.jpg" border="0" class="image0" onload="javascript:setListingsImage(this);">

                 <%--<img  src="<%# DataBinder.Eval(Container.DataItem,"item") %>" 
                  style="visibility:hidden;margin: 0px 0 0 0px;
                 cursor:pointer;z-index:-1000" width="300" height="340" />--%>
                 
                 
                 


            </div>
            </a>
   <%-- </a>--%>
     <strong><span style="color: #008080">
       <%--<%#Eval("title")%> --%>
     </td>
     </ItemTemplate>
     <EmptyDataTemplate>
    
     <h4>Currently there are no photos in this album. </h4>
   
     </EmptyDataTemplate>
   </asp:ListView>
   
   
   <%--<div class="ad-nav" align="center">
                        <table border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" valign="top">
                                    <div class="ad-thumbs" style="width: 845px; margin: 0 auto;">
                                        <ul class="ad-thumb-list">
                                            <asp:Repeater ID="Repeater1" runat="server">
                                                <ItemTemplate>
                                                    <li><a href="<%# DataBinder.Eval(Container.DataItem,"item") %>" onclick="RepeaterData('<%#DataBinder.Eval(Container.DataItem, "item")%>');return false;">
                                                        <asp:Label ID="lblID" CssClass="IdLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "item")%>'
                                                            Visible="false" />
                                                            <img style="opacity: 0.7; width: 200px; height: 180px; max-height: 150px; min-height: 150px;" src="<%# DataBinder.Eval(Container.DataItem,"item") %>"
                                                            onerror="images/coming_soon1.jpg" border="0" class="image0" onload="javascript:setListingsImage(this);">
                                                    </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                          
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>--%>
   
   
   
   
   
   
   
   <asp:Label runat="server" ID="lblSucessMsg"></asp:Label>
     
    <asp:ObjectDataSource ID="odsPhotos" runat="server" SelectMethod="GetPhotosFromAlbum" 
        TypeName="PhotosDAL">
        <SelectParameters>
            <asp:QueryStringParameter Name="aid" QueryStringField="aid" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</td>
</tr>
</table>

    <asp:HiddenField ID="hdnfldChkBxValue" runat="server" />
</asp:Content>



