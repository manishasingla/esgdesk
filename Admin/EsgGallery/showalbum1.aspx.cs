﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;

public partial class showalbum : System.Web.UI.Page
{

    private bool _doSetCover = false;



    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
           
          
            this.Title = "Show album : " + Request.QueryString["atitle"].ToString();
            Session.Add("aid", Request.QueryString["aid"]);
            DataPager dp = (DataPager)ListView1.FindControl("DataPager1");
            fnBindData();
        }
        catch (Exception ex)
        {
            lblSucessMsg.Visible = true;
            lblSucessMsg.Text = ex.Message;
        }

    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        // delete album and remove all photos from photos folder also
        try
        {
            if (PhotosDAL.DeleteAlbum(Request.QueryString["aid"]))
            {
                Response.Redirect("default.aspx");  // go to home page
            }
        }
        catch (Exception ex)
        {
            lblSucessMsg.Visible = true;
            lblSucessMsg.Text = ex.Message;
        }


    }
    protected void ListView1_DataBound(object sender, EventArgs e)
    {
        DataPager pager = (DataPager)ListView1.FindControl("DataPager1");
        if (pager != null)
            pager.Visible = (pager.PageSize < pager.TotalRowCount);

    }



    protected void ListView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void FormView1_DataBound(object sender, EventArgs e)
    {
        try
        {
            if (Session["admin"] != null && Session["admin"].ToString() != "")
            {

                if (DatabaseHelper.isAdmin(Session["admin"].ToString()))
                {

                    Panel innerpnl = (Panel)FormView1.FindControl("innerPanel");
                    // Panel outerpnl = (Panel)FormView1.FindControl("outerPanel");
                    innerpnl.Visible = true;
                    outerPanel.Visible = true;
                }

            }
            else
            {
                Response.Redirect("~/Admin/login.aspx");
                return;
            }
        }
        catch (Exception ex)
        {
            lblSucessMsg.Visible = true;
            lblSucessMsg.Text = ex.Message;
        }
    }

    public void fnBindData()
    {
        try
        {
            DataTable dt1, dt2;
            dt1 = PhotosDAL.GetAlbumDetails(Request.QueryString["aid"]);
            if (dt1 != null && dt1.Rows.Count > 0)
            {
                FormView1.Visible = true;
                FormView1.DataSource = dt1;
                FormView1.DataBind();
            }
            else
            {
                FormView1.Visible = false;
            }
            dt2 = PhotosDAL.GetPhotosFromAlbum(Request.QueryString["aid"]);
            if (dt1 != null && dt1.Rows.Count > 0)
            {
                ListView1.Visible = true;
                lblSucessMsg.Visible = false;
                ListView1.DataSource = dt2;
                ListView1.DataBind();

            }
            else
            {
                ListView1.Visible = false;
                lblSucessMsg.Visible = true;
                lblSucessMsg.Text = "Album is not found";
            }
        }
        catch (Exception ex)
        {
            lblSucessMsg.Visible = true;
            lblSucessMsg.Text =ex.Message;
        }

    }

    //private void Save_File(string oldPath, string newpath, string strfilename)
    //{
    //    try
    //    {
    //        string newFileName = strfilename;
    //        FileInfo f1 = new FileInfo(oldPath);
    //        if (f1.Exists)
    //        {
    //            if (!Directory.Exists(newpath))
    //            {
    //                Directory.CreateDirectory(newpath);
    //            }
    //            f1.CopyTo(string.Format("{0}{1}{2}", newpath, newFileName, f1.Extension));
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        lblSucessMsg.Text = ex.Message;
    //    }
    //}

    //protected void lnkbtnSetCover_Click(object sender, EventArgs e)
    //{

    //    string oldPath = "";
    //    string newPath = "";
    //    try
    //    {
    //        //foreach (ListViewDataItem item in ListView1.Items)
    //        //{
    //        //    var chk = item.FindControl("chkbxCover") as System.Web.UI.HtmlControls.HtmlInputCheckBox;

    //        //    chk.Visible = true;

    //        //    if (chk != null && chk.Checked)
    //        //    {
    //        //        string value = chk.Value;
    //        //    }
    //        //}
    //        if (lnkbtnSetCover.Text == "Set Cover Photo")
    //        {
    //            lnkbtnSetCover.Text = "Set Now";

    //            foreach (ListViewDataItem item in ListView1.Items)
    //            {
    //                var chk = item.FindControl("chkbxCover") as System.Web.UI.HtmlControls.HtmlInputCheckBox;
    //                chk.Visible = true;
    //            }
    //            _doSetCover = true;
    //        }

    //        else
    //        {
    //            lnkbtnSetCover.Text = "Set Cover Photo";
    //            foreach (ListViewDataItem item in ListView1.Items)
    //            {
    //                var chk = item.FindControl("chkbxCover") as System.Web.UI.HtmlControls.HtmlInputCheckBox;
    //                chk.Visible = false;
    //            }
    //            string photoid1 = hdnfldChkBxValue.Value.ToString();
    //            int photoid2 = PhotosDAL.SetCoverPhoto(photoid1, Request.QueryString["aid"]);

    //            //oldPath = Server.MapPath("photos/" + photoid2 + ".jpg");
    //            //newPath = Server.MapPath("Temp/");
    //            //Save_File(oldPath, newPath, photoid1.ToString());


    //            //oldPath = Server.MapPath("photos/" + photoid1 + ".jpg");
    //            //newPath = Server.MapPath("Temp/");
    //            //Save_File(oldPath, newPath, photoid2.ToString());


    //            //System.IO.File.Delete(Server.MapPath("photos/" + photoid1 + ".jpg"));
    //            //System.IO.File.Delete(Server.MapPath("photos/" + photoid2 + ".jpg"));

    //            //oldPath = Server.MapPath("Temp/" + photoid1 + ".jpg");
    //            //newPath = Server.MapPath("photos/");
    //            //Save_File(oldPath, newPath, photoid1.ToString());

    //            //oldPath = Server.MapPath("Temp/" + photoid2 + ".jpg");
    //            //newPath = Server.MapPath("photos/");
    //            //Save_File(oldPath, newPath, photoid2.ToString());

    //            //System.IO.File.Delete(Server.MapPath("Temp/" + photoid1 + ".jpg"));
    //            //System.IO.File.Delete(Server.MapPath("Temp/" + photoid2 + ".jpg"));

    //            ListView1.DataSourceID = odsPhotos.ID;
    //            ListView1.DataBind();
    //            //PhotosDAL.GetPhotosFromAlbum(Session["aid"].ToString());

    //            FormView1.DataSourceID = ObjectDataSource1.ID;
    //            FormView1.DataBind();

    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        lblSucessMsg.Text = ex.Message;
    //    }

    //}
}
