﻿<%@ Page Language="C#" MasterPageFile="~/Admin/EsgGallery/AdminMasterMenu.master"
    AutoEventWireup="true" CodeFile="showphoto.aspx.cs" Inherits="showphoto" Title="Photos slide show"
    Trace="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" language="javascript" src="js/featprop.js"></script>
    <script type="text/javascript" language="javascript" src="js/script.js"></script>
    <script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
   <%-- <link rel="stylesheet" type="text/css" href="css/detailspagestyle.css" />--%>
    <link rel="stylesheet" type="text/css" href="css/font/open-sans.css" />
    <link href="css/juizDropDownMenu.css" rel="stylesheet" type="text/css" />
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="css/jquery.ad-gallery.css" />
    <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/juizDropDownMenu-1.5.min.js"></script>
    <script type="text/javascript" src="js/jquery.ad-gallery.js"></script>
    <script type="text/javascript" src="js/gmapPlacesMarker.js"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=places"></script>
    <script type="text/javascript">
        $(function () {
            $("#dropdown").juizDropDownMenu({
                'showEffect': 'fade',
                'hideEffect': 'slide'
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            var galleries = $('.ad-gallery').adGallery();
            $('#switch-effect').change(
      function () {
          galleries[0].settings.effect = $(this).val();
          return false;
      }
    );
            $('#toggle-slideshow').click(
      function () {
          galleries[0].slideshow.toggle();
          return false;
      }
    );
            $('#toggle-description').click(
      function () {
          if (!galleries[0].settings.description_wrapper) {
              galleries[0].settings.description_wrapper = $('#descriptions');
          } else {
              galleries[0].settings.description_wrapper = false;
          }
          return false;
      }
    );
        });

        function AdjustTabs(o) {
            //debugger;
            //alert(o);
            o.className = "tabLink activeLink";

            if (o.id == "cont-1") //description 
            {
                document.getElementById("cont-2").className = "tabLink ";
                document.getElementById("cont-3").className = "tabLink ";

                document.getElementById("cont-1-1").className = "tabcontent ";
                document.getElementById("cont-2-1").className = "tabcontent hide";
                document.getElementById("cont-3-1").className = "tabcontent hide";
                document.getElementById("cont-2-1").style.display = "none";
            }
            else if (o.id == "cont-2") //map 
            {
                document.getElementById("cont-1").className = "tabLink ";
                document.getElementById("cont-3").className = "tabLink ";

                document.getElementById("cont-2-1").className = "tabcontent ";
                document.getElementById("cont-1-1").className = "tabcontent hide";
                document.getElementById("cont-3-1").className = "tabcontent hide";
                document.getElementById("cont-2-1").style.display = "inline";
            }
            else if (o.id == "cont-3") //street view 
            {
                document.getElementById("cont-2").className = "tabLink ";
                document.getElementById("cont-1").className = "tabLink ";

                document.getElementById("cont-3-1").className = "tabcontent ";
                document.getElementById("cont-2-1").className = "tabcontent hide";
                document.getElementById("cont-1-1").className = "tabcontent hide";
                document.getElementById("cont-2-1").style.display = "none";
            }

        }

        function hideStreet() {
            try {
                var name = "street";
                name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
                var regexS = "[\\?&]" + name + "=([^&#]*)";
                var regex = new RegExp(regexS);
                var results = regex.exec(window.location.search);
                if (results != null) {
                    if (decodeURIComponent(results[1].replace(/\+/g, " ")).toLowerCase() == "true") {
                        AdjustTabs(document.getElementById('cont-3'));
                    }
                }
                else {
                    document.getElementById("cont-2-1").className = "tabcontent hide";
                    document.getElementById("cont-3-1").className = "tabcontent hide";
                }
            } catch (e) {
                document.getElementById("cont-2-1").className = "tabcontent hide";
                document.getElementById("cont-3-1").className = "tabcontent hide";
            }
        }

        function setValue(source) {
            alert(rptr.value.toString());

        }

        function RepeaterData(source) {
            document.getElementById('<%= hdnfldImgIdValue.ClientID %>').value = source;
            var name = document.getElementById('<%= hdnfldImgIdValue.ClientID %>').value.toString();

            var btn = document.getElementById('<%=lbEventRaiser.ClientID%>');
            btn.click();
        }
           
    </script>
    <script type="text/javascript">

        function setListingsImage(obj) {
            var img = new Image();
            img.src = obj.src;
            if (img.height > img.width) {
                obj.height = 220;
                var width = (200 * img.width) / img.height;
                width = width > 220 ? 220 : width;
                obj.width = (width > 0 ? width : 220);
            }
            else {
                obj.width = 200;
                var height = (220 * img.height) / img.width;
                height = height > 200 ? 200 : height;
                obj.height = (height > 0 ? (height - 6) : 220);

            }
//            var img = new Image();
//            img.src = obj.src;
//            if (img.height > img.width) {
//                obj.height = 300;
//                var width = (300 * img.width) / img.height;
//                width = width > 310 ? 310 : width;
//                obj.width = (width > 0 ? width : 310);
//            }
//            else {
//                obj.width = 300;
//                var height = (300 * img.height) / img.width;
//                height = height > 300 ? 300 : height;

//                obj.height = (height > 0 ? (height - 6) : 300);

//            }
        }


        var preImg = "";
        function setImageListingMain(obj) {
            try {
                var img = new Image();
                img.src = obj.src;

                if (img.height > img.width) {
                    obj.height = 600;
                    var width = (600 * img.width) / img.height;
                    width = width > 600 ? 600 : width;
                    obj.width = (width > 0 ? width : 600);
                }
                else {
                    obj.width = 600;
                    var height = (600 * img.height) / img.width;
                    height = height > 600 ? 600 : height;
                    obj.height = (height > 0 ? height : 600);
                }
            }
            catch (e) { obj.width = 600; obj.height = 600; }
        }
    </script>
    <table width="80%" style="margin:auto;">
        <tr runat="server" >
            <td valign="top" align="center" runat="server" id="tdimgslide">
                <%--<img src='photos/<%=Request.QueryString["photoid"]%>.jpg'  />--%>
                <div id="gallery" class="ad-gallery" runat="server" >
                    <div class="ad-controls" style="margin: 27px auto 10px; width: 841px;">
                    </div>
                    <div class="ad-nav" align="center">
                        <table border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" valign="top">
                                    <div class="ad-thumbs" style="width: 845px; margin: 0 auto;">
                                        <ul class="ad-thumb-list">
                                         
                                            <asp:Repeater ID="Repeater1" runat="server">
                                                <ItemTemplate>
                                                    <li><a href="<%# DataBinder.Eval(Container.DataItem,"item") %>" onclick="RepeaterData('<%#DataBinder.Eval(Container.DataItem, "item")%>');return false;">
                                                        <asp:Label ID="lblID" CssClass="IdLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "item")%>'
                                                            Visible="false" />
                                                            <img style="opacity: 0.7; width: 200px; height: 180px; max-height: 150px; min-height: 150px;" src="<%# DataBinder.Eval(Container.DataItem,"item") %>"
                                                            onerror="images/coming_soon1.jpg" border="0" class="image0" onload="javascript:setListingsImage(this);">
                                                    </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                          
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="ad-image-wrapper">
                    </div>
                </div>
                <%--<img src='photos/<%=Request.QueryString["photoid"]%>.jpg'  style="vertical-align:middle; min-width:350px; min-height:300px; max-height:750px; max-width:700px" />--%>
            </td>
            <td width="30%" style="margin-left: 10px; border:1px solid #cccccc; vertical-align: top" id="tdimgdetails" runat="server">
         <div style="width:250px;">
         
                <asp:UpdatePanel ID="FVUP" runat="server">
                
                    <ContentTemplate>
                    
                        <asp:FormView ID="FormView1" runat="server" OnItemDeleted="FormView1_ItemDeleted"
                            EnableModelValidation="True" OnDataBound="FormView1_DataBound" DataKeyNames="photoid"
                            OnItemUpdating="FormView1_ItemUpdating" OnModeChanging="FormView1_ModeChanging"
                            OnItemInserting="FormView1_ItemInserting" OnItemDeleting="FormView1_ItemDeleting">
                        
                           <ItemTemplate>
                                <img  style="width:300px; max-width:247px;" alt='<%# Eval("photoid") %>' src='photos/<%#Eval("atitle")%>/<%#Eval("photoid")%>.jpg' id="SideImage"onload="javascript:setListingsImage(this);" />
                                <br />
                                <asp:Panel runat="server" ID="pnlImgDetails" Height="0">
                                    <asp:Label ID="lblTitle" CssClass="IdLabel" runat="server" Text="Title :" Visible="true" />
                                    <asp:Label ID="lblTitleValue" CssClass="IdLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "title")%>'
                                        Visible="true" />
                                    <br />
                                    <asp:Label ID="lblTags" CssClass="IdLabel" runat="server" Text="Tags :" Visible="true" />
                                    <asp:Label ID="lblTagsValue" CssClass="IdLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "tags")%>'
                                        Visible="true" />
                                    <br />
                                </asp:Panel>
                                Added on :
                                <%# Eval("addedon") %>
                                <p />
                                <asp:Panel runat="server" ID="innerPanel" Visible="false">
                                    <asp:LinkButton runat="server" Text="Edit photo details" CommandName="Edit" ID="lbEdit" />
                                    <p />
                                    <asp:LinkButton ID="lbDelete" runat="server" Text="Delete this photo" CommandName="Delete"
                                        OnClientClick="return confirm('Do you want to delete this photo?')" />
                                </asp:Panel>
                                <p />
                                <a style="width: 200px; background-color: rgb(35, 41, 109); color: rgb(255, 255, 255); border-radius: 2px 2px 2px 2px; padding: 5px; margin-left: 2px;" href='showalbum.aspx?aid=<%=Session["aid"]%>&atitle=<%#Eval("atitle")%>'>Show album</a>
                            </ItemTemplate> 
                            
                          
                            
                            
                            <EditItemTemplate>
                                <%-- <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>--%>
                                Added On :
                                <%# Eval("addedon") %>
                                <p />
                                <asp:TextBox Visible="false" runat="server" ID="photoid" Text='<%# Bind("photoid") %>' />
                                <p />
                                Title :
                                <asp:TextBox runat="server" ID="title" Text='<%# Bind("title") %>' />
                                <p />
                                Tags :
                                <asp:TextBox runat="server" ID="tags" Text='<%# Bind("tags") %>' />
                                <p />
                                <asp:LinkButton runat="server" Text="Update details" CommandName="Update" ID="lbUpdate" />
                                <asp:LinkButton runat="server" Text="Cancel changes" CommandName="Cancel" ID="lbCancel" />
                                <%-- </ContentTemplate>
      </asp:UpdatePanel>--%>
                            </EditItemTemplate>
                        </asp:FormView>
                          
                        <asp:LinkButton runat="server" ID="lnkbtnSetCover" Text="Set cover photo" OnClick="lnkbtnSetCover_Click"
                            OnClientClick="return window.confirm('Are you sure want to set this photo as cover of album?')"
                            Visible="false" />
                            
                    </ContentTemplate>
                    
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="lbEventRaiser" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                
                </div>
                
                
                <asp:LinkButton runat="server" Text="" ID="lbEventRaiser" OnClick="lbEventRaiser_Click" />
            </td>
        </tr>
           <tr><td><asp:Label ID="lblANF" runat="server" Text="Album is not found"></asp:Label></td></tr>
    </table>
    <asp:HiddenField ID="hdnfldImgIdValue" runat="server" />
</asp:Content>
