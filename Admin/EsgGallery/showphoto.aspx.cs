﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class showphoto : System.Web.UI.Page
{
    public static bool blnIsNotEditEvent = true;
    string atitle;
    protected void Page_Load(object sender, EventArgs e)
    {
        //  if album id is passed then place it in session variable 
        if (Session["admin"] != null && Session["admin"].ToString() != "")
        {
            string photoid = "";
            if (Request.QueryString["aid"] != null && Request.QueryString["photoid"] != null && Request.QueryString["atitle"]!=null)
            {
                string aid = Request.QueryString["aid"].ToString();
                atitle = Request.QueryString["atitle"].Trim();
                if (Request.QueryString["photoid"] != null)
                    photoid = Request.QueryString["photoid"].ToString();
                System.Data.DataTable objDataTable = new System.Data.DataTable();
                ViewState["photoid"] = photoid;
                if (aid != null)
                    Session.Add("aid", aid);
                //fnBindData();
                if (!IsPostBack)
                {
                    BindFormView();
                    fnBindData1();
                }
            }
            else
            {
                Response.Write("Paraqmeters mismatched !");
                Response.Redirect("Default.aspx");
               
            }
        }
        else
        {
            Response.Redirect("~/Admin/login.aspx");
            return;
        }


    }
    protected void FormView1_ItemDeleted(object sender, FormViewDeletedEventArgs e)
    {
        Response.Redirect("showalbum.aspx?aid=" + Session["aid"].ToString());
    }
    protected void FormView1_DataBound(object sender, EventArgs e)
    {
        if (Session["admin"] != null && Session["admin"].ToString() != "")
        {
            try
            {
                //if (!IsPostBack)
                //{
                if (DatabaseHelper.isAdmin(Session["admin"].ToString()))
                {
                    Panel innerpnl = (Panel)FormView1.FindControl("innerPanel");
                    innerpnl.Visible = true;
                    lnkbtnSetCover.Visible = true;
                }
                Label lblTitle = (Label)FormView1.FindControl("lblTitle");
                Label lblTags = (Label)FormView1.FindControl("lblTags");
                Label lblTitleValue = (Label)FormView1.FindControl("lblTitleValue");
                Label lblTagsValue = (Label)FormView1.FindControl("lblTagsValue");
                Panel pnlImgDetails = (Panel)FormView1.FindControl("pnlImgDetails");
                int pnlHieght = 0;
                if (lblTagsValue.Text.Trim() == "")
                {
                    lblTags.Visible = false;
                }
                else
                {
                    pnlHieght += 17;
                }
                if (lblTitleValue.Text.Trim() == "")
                {
                    lblTitle.Visible = false;
                }
                else
                {
                    pnlHieght += 17;
                }
                pnlImgDetails.Height = pnlHieght;
                //}
            }
            catch
            {
            }
        }
        else
        {
            Response.Redirect("~/Admin/login.aspx");
            return;
        }
    }



    private void BindFormView()
    {
        try
        {
            string strParameter = hdnfldImgIdValue.Value.ToString();
            if (strParameter.Trim() != "")
            {
                string[] strArray = strParameter.Split(new char[] { '/' }, StringSplitOptions.None);
                strArray = strArray[2].Split(new char[] { '.' }, StringSplitOptions.None);

                ViewState["photoid"] = strArray[0];
            }

            if (Session["strTemp"] != null && ViewState["photoid"].ToString().Trim() == "")
            {

                FormView1.DataSource = Session["strTemp"] as DataTable;
                FormView1.DataBind();

            }
            else
            {
                DataTable dt = new DataTable();
                if (ViewState["photoid"].ToString().Trim() == "")
                {
                    dt = PhotosDAL.GetPhotoDetails(Request.QueryString["photoid"]);
                }
                else
                {
                    dt = PhotosDAL.GetPhotoDetails(ViewState["photoid"].ToString());
                }
                DataColumn[] keyColumn = new DataColumn[0];
                dt.PrimaryKey = keyColumn;
                FormView1.DataSource = dt;
                // FormView1.DataSource = getPhotoDetailsTable();
                FormView1.DataBind();

                Session["strTemp"] = dt;

            }
        }
        catch (Exception ex)
        {
        }

    }

    protected void ObjectDataSource1_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
    }


    public void fnBindData1()
    {
        DataTable dt = PhotosDAL.GetPhotosFromAlbum(Session["aid"].ToString());

        string mainimage = "";

        try
        {
            if (dt != null && dt.Rows.Count > 0)
            {
                mainimage = atitle + "/" + dt.Rows[0]["photoid"].ToString().Trim();
                string strImgslidePath = "photos/";
                if (mainimage.ToString() != "")
                {
                    mainimage = mainimage + ".jpg";
                    string[] strimagename = mainimage.Split(new char[] { '.' });
                    string strImage = strimagename[0];
                    string strexten = strimagename[1];

                    mainimage = mainimage != "" ? strImgslidePath + strImage + "." + strexten : "photos/image_soon.jpg";
                }
                else
                {
                    mainimage = "photos/image_soon.jpg";
                }

                int imagescount = Convert.ToInt32(dt.Rows.Count);
                DataTable dt1 = new DataTable();

                string strThumbImgPath = "photos/";
                dt1.Columns.Add("item");
                dt1.Columns.Add("class");

                if (mainimage != "")
                    dt1.Rows.Add(new object[] { mainimage, "image0" });

                for (int k = 1; k < imagescount; k++)
                {
                    string str = atitle + "/" + dt.Rows[k]["photoid"].ToString().Trim() + ".jpg";
                    string[] strimagename = str.Split(new char[] { '.' });
                    string strImage = strimagename[0];
                    string strexten = strimagename[1];

                    if (str != "")
                    {
                        dt1.Rows.Add(new object[] { strThumbImgPath + strImage + "." + strexten });
                    }
                }
                tdimgdetails.Visible = true;
                tdimgslide.Visible = true;
                lblANF.Visible = false;
                Repeater1.DataSource = dt1;
                Repeater1.DataBind();
            }
            else
            {
                tdimgdetails.Visible = false;
                tdimgslide.Visible = false;
                lblANF.Visible=true;
            }
        }
        catch(Exception ex)
        {
            tdimgdetails.Visible = false;
            tdimgslide.Visible = false;
            lblANF.Visible = true;
        }

    }
    protected void lbEventRaiser_Click(object sender, EventArgs e)
    {
        BindFormView();
        FormView1.DataBind();

    }



    protected void ObjectDataSource1_Updating(object sender, ObjectDataSourceMethodEventArgs e)
    {
    }


    protected void FormView1_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        blnIsNotEditEvent = false;
        if (e.NewMode.ToString() == "Edit")
        {

            FormView1.ChangeMode(e.NewMode);
        }
        else if (e.NewMode.ToString() == "Insert")
        {
            FormView1.ChangeMode(e.NewMode);
        }
        else
            FormView1.ChangeMode(e.NewMode);

        BindFormView();
    }


    protected void FormView1_ItemUpdating(object sender, FormViewUpdateEventArgs e)
    {
        string photoid = FormView1.DataKey.Value.ToString();
        DataTable dt = Session["strTemp"] as DataTable;
        dt.PrimaryKey = new DataColumn[] { dt.Columns["photoid"] };
        FormView1.DataSource = dt;
        //DataRow dr;
        //dr = dt.NewRow();
        //dr = dt.Rows.Find(photoid);


        string strTitle = ((TextBox)FormView1.FindControl("title")).Text;
        string strtags = ((TextBox)FormView1.FindControl("tags")).Text;
        //dr[3] = ((TextBox)FormView1.FindControl("addedon")).Text;
        PhotosDAL.UpdatePhoto(photoid, strTitle, strtags);
        Session["strTemp"] = null;

        //dt.GetChanges();
        //Session["strTemp"] = dt;
        FormView1.ChangeMode(FormViewMode.ReadOnly);
        BindFormView();

    }


    protected void FormView1_ItemInserting(object sender, FormViewInsertEventArgs e)
    {
        DataTable dt = Session["strTemp"] as DataTable;
        DataRow dr;
        dr = dt.NewRow();

        dr[1] = ((TextBox)FormView1.FindControl("title")).Text;
        dr[2] = ((TextBox)FormView1.FindControl("tags")).Text;
        //dr[3] = ((TextBox)FormView1.FindControl("addedon")).Text;

        dt.Rows.Add(dr);
        Session["strTemp"] = dt;
        FormView1.ChangeMode(FormViewMode.ReadOnly);
        BindFormView();

    }


    protected void FormView1_ItemDeleting(object sender, FormViewDeleteEventArgs e)
    {
        string photoid = FormView1.DataKey.Value.ToString();
        PhotosDAL.DeletePhoto(photoid);
        Session["strTemp"] = null;
        FormView1.ChangeMode(FormViewMode.ReadOnly);
        BindFormView();
        ViewState["photoid"] = "";
    }

    //public DataTable getPhotoDetailsTable()
    //{


    //   DataTable dt = new DataTable();
    //   dt = PhotosDAL.GetPhotoDetails(Request.QueryString["photoid"]);

    //   System.Data.DataTable objDataTable = new System.Data.DataTable();

    //    //Create three columns with string as their type

    //    objDataTable.Columns.Add("ISBN", typeof(string));
    //    objDataTable.Columns.Add("photoid", typeof(string));
    //    objDataTable.Columns.Add("title", typeof(string));
    //    objDataTable.Columns.Add("tags", typeof(string));
    //    objDataTable.Columns.Add("addedon", typeof(string));

    //    DataColumn[] dcPk = new DataColumn[1];
    //    dcPk[0] = objDataTable.Columns["ISBN"];
    //    objDataTable.PrimaryKey = dcPk;
    //    objDataTable.Columns["ISBN"].AutoIncrement = true;
    //    objDataTable.Columns["ISBN"].AutoIncrementSeed = 1;
    //    //Adding some data in the rows of this DataTable
    //    DataRow dr;
    //    for (int i = 0; i < dt.Rows.Count; i++)
    //    {
    //        dr = objDataTable.NewRow();

    //        dr[1] = dt.Rows[i]["photoid"];
    //        dr[2] = dt.Rows[i]["title"] ;
    //        dr[3] = dt.Rows[i]["tags"];
    //        dr[4] = dt.Rows[i]["addedon"]; 

    //        objDataTable.Rows.Add(dr);
    //    }
    //    Session["strTemp"] = objDataTable;
    //    return objDataTable;
    //}


    protected void lnkbtnSetCover_Click(object sender, EventArgs e)
    {
        try
        {
            string photoid1 = "";
            string strParameter = hdnfldImgIdValue.Value.ToString();

            if (strParameter.Trim() != "")
            {
                string[] strArray = strParameter.Split(new char[] { '/' }, StringSplitOptions.None);
                strArray = strArray[2].Split(new char[] { '.' }, StringSplitOptions.None);

                photoid1 = strArray[0];
               
                Session["CI"] = strArray[0];
            }
            else
            {
                photoid1 =  Request.QueryString["photoid"];
            }

            int photoid2 = PhotosDAL.SetCoverPhoto(photoid1, Request.QueryString["aid"]);
            BindFormView();
        }
        catch (Exception ex)
        {
            //lblSucessMsg.Text = ex.Message;
        }

    }
}
