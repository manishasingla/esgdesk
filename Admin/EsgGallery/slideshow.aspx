﻿<%@ Page Language="C#" MasterPageFile="~/Admin/EsgGallery/AdminMasterMenu.master" AutoEventWireup="true" CodeFile="slideshow.aspx.cs" Inherits="slideshow" Title="Photo" Trace="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  <script type="text/javascript" language="javascript" src="js/featprop.js"></script>
    <script type="text/javascript" language="javascript" src="js/script.js"></script>
    <script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="css/detailspagestyle.css" />
    <link rel="stylesheet" type="text/css" href="css/font/open-sans.css" />
    <link href="css/juizDropDownMenu.css" rel="stylesheet" type="text/css" />
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="css/jquery.ad-gallery.css" />
    <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/juizDropDownMenu-1.5.min.js"></script>
    <script type="text/javascript" src="js/jquery.ad-gallery.js"></script>
    <script type="text/javascript" src="js/gmapPlacesMarker.js"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=places"></script>
    <script type="text/javascript">
        $(function () {
            $("#dropdown").juizDropDownMenu({
                'showEffect': 'fade',
                'hideEffect': 'slide'
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            var galleries = $('.ad-gallery').adGallery();
            $('#switch-effect').change(
      function () {
          galleries[0].settings.effect = $(this).val();
          return false;
      }
    );
            $('#toggle-slideshow').click(
      function () {
          galleries[0].slideshow.toggle();
          return false;
      }
    );
            $('#toggle-description').click(
      function () {
          if (!galleries[0].settings.description_wrapper) {
              galleries[0].settings.description_wrapper = $('#descriptions');
          } else {
              galleries[0].settings.description_wrapper = false;
          }
          return false;
      }
    );
        });

        function AdjustTabs(o) {
            //debugger;
            //alert(o);
            o.className = "tabLink activeLink";

            if (o.id == "cont-1") //description 
            {
                document.getElementById("cont-2").className = "tabLink ";
                document.getElementById("cont-3").className = "tabLink ";

                document.getElementById("cont-1-1").className = "tabcontent ";
                document.getElementById("cont-2-1").className = "tabcontent hide";
                document.getElementById("cont-3-1").className = "tabcontent hide";
                document.getElementById("cont-2-1").style.display = "none";
            }
            else if (o.id == "cont-2") //map 
            {
                document.getElementById("cont-1").className = "tabLink ";
                document.getElementById("cont-3").className = "tabLink ";

                document.getElementById("cont-2-1").className = "tabcontent ";
                document.getElementById("cont-1-1").className = "tabcontent hide";
                document.getElementById("cont-3-1").className = "tabcontent hide";
                document.getElementById("cont-2-1").style.display = "inline";
            }
            else if (o.id == "cont-3") //street view 
            {
                document.getElementById("cont-2").className = "tabLink ";
                document.getElementById("cont-1").className = "tabLink ";

                document.getElementById("cont-3-1").className = "tabcontent ";
                document.getElementById("cont-2-1").className = "tabcontent hide";
                document.getElementById("cont-1-1").className = "tabcontent hide";
                document.getElementById("cont-2-1").style.display = "none";
            }

        }

        function hideStreet() {
            try {
                var name = "street";
                name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
                var regexS = "[\\?&]" + name + "=([^&#]*)";
                var regex = new RegExp(regexS);
                var results = regex.exec(window.location.search);
                if (results != null) {
                    if (decodeURIComponent(results[1].replace(/\+/g, " ")).toLowerCase() == "true") {
                        AdjustTabs(document.getElementById('cont-3'));
                    }
                }
                else {
                    document.getElementById("cont-2-1").className = "tabcontent hide";
                    document.getElementById("cont-3-1").className = "tabcontent hide";
                }
            } catch (e) {
                document.getElementById("cont-2-1").className = "tabcontent hide";
                document.getElementById("cont-3-1").className = "tabcontent hide";
            }
        }

        function setValue(source) {
            alert(rptr.value.toString());

        }

                
</script>
       
  
    </script>
<asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
        DeleteMethod="DeletePhoto" SelectMethod="GetPhotoDetails" 
        TypeName="PhotosDAL" UpdateMethod="UpdatePhoto" >
    <DeleteParameters>
     <%--   <asp:Parameter Name="photoid" Type="String" />--%>
          <asp:QueryStringParameter Name="photoid" QueryStringField="photoid" 
            Type="String" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:Parameter Name="photoid" Type="String" />
        <asp:Parameter Name="title" Type="String" />
        <asp:Parameter Name="tags" Type="String" />
    </UpdateParameters>
    <SelectParameters>
        <asp:QueryStringParameter Name="photoid" QueryStringField="photoid" 
            Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>

<table width="100%">
<tr>
<td valign="top" onload="initialize()">
 <%--<img src='photos/<%=Request.QueryString["photoid"]%>.jpg'  />--%>


   <div id="gallery" class="ad-gallery" runat="server">
                                                            <div class="ad-image-wrapper">
                                                            </div>
                                                            <div class="ad-controls" style="margin: 27px auto 0; width: 504px;">
                                                            </div>
                                                            <div class="ad-nav" align="center">
                                                                <table border="0" align="center" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="center" valign="top">
                                                                            <div class="ad-thumbs" style="width: 550px; margin: 0 auto; height: 106px;">
                                                                                <ul class="ad-thumb-list">
                                                                                    <asp:Repeater ID="Repeater1" runat="server">
                                                                                        <ItemTemplate>
                                                                                            <li><a href="<%# DataBinder.Eval(Container.DataItem,"item") %>" CssClass="myLink" value="<%# DataBinder.Eval(Container.DataItem,"item") %>" onclick="javascript:setValue(this);" >
                                                                                                <img src="<%# DataBinder.Eval(Container.DataItem,"item") %>"  onerror="images/coming_soon1.jpg'"
                                                                                                    height="100" width="100" border="0" class="image0">

                                                                                            </a></li>
                                                                                        </ItemTemplate>
                                                                                    </asp:Repeater>
                                                                                </ul>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
</td>
<td width="30%">
 
    <p />
</td>
<asp:HiddenField ID="hdnfldImgIdValue" runat="server" />
</table>
    

    </asp:Content>

