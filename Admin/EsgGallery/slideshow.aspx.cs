using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class slideshow : System.Web.UI.Page
{


    protected void Page_Load(object sender, EventArgs e)
    {
        //  if album id is passed then place it in session variable 
        string aid = Request.QueryString["aid"].ToString();
        if (aid != null)
            Session.Add("aid", aid);
        fnBindData();
    }
    protected void FormView1_ItemDeleted(object sender, FormViewDeletedEventArgs e)
    {
        Response.Redirect("showalbum.aspx?aid=" + Session["aid"].ToString());
    }

    public void fnBindData()
    {
        DataTable dt = PhotosDAL.GetPhotosFromAlbum(Session["aid"].ToString());


        string mainimage = "";


        mainimage = dt.Rows[0]["photoid"].ToString().Trim();
        string strImgslidePath = "photos/";
        if (mainimage.ToString() != "")
        {
            mainimage = mainimage + ".jpg";
            string[] strimagename = mainimage.Split(new char[] { '.' });
            string strImage = strimagename[0];
            string strexten = strimagename[1];

            mainimage = mainimage != "" ? strImgslidePath + strImage + "." + strexten : "photos/image_soon.jpg";
        }
        else
        {
            mainimage = "photos/image_soon.jpg";
        }

        int imagescount = Convert.ToInt32(dt.Rows.Count);
        DataTable dt1 = new DataTable();

        string strThumbImgPath = "photos/";
        dt1.Columns.Add("item");
        dt1.Columns.Add("class");

        if (mainimage != "")
            dt1.Rows.Add(new object[] { mainimage, "image0" });

        for (int k = 1; k < imagescount; k++)
        {
            string str = dt.Rows[k]["photoid"].ToString().Trim() + ".jpg";
            string[] strimagename = str.Split(new char[] { '.' });
            string strImage = strimagename[0];
            string strexten = strimagename[1];

            if (str != "")
            {
                dt1.Rows.Add(new object[] { strThumbImgPath + strImage + "." + strexten });
            }
        }
        
        Repeater1.DataSource = dt1;
        Repeater1.DataBind();


        //dtlstImages2.DataSource = PhotosDAL.GetPhotosFromAlbum(Session["aid"].ToString());
        //dtlstImages2.DataBind();
    }
}
