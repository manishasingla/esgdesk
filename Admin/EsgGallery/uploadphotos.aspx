﻿<%@ Page Language="C#" MasterPageFile="~/Admin/EsgGallery/AdminMasterMenu.master" AutoEventWireup="true" CodeFile="uploadphotos.aspx.cs" Inherits="uploadphotos" Title="Upload photos"  %>

<asp:Content ID="UploadPhotos" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h2>Upload Photos</h2>
    <table>
    
    <tr>
    <th>Select Photo</th>
    <th>Title</th>
    <th>Tags</th>
    </tr>
    <tr>
    <td><asp:FileUpload ID="FileUpload1" runat="server"  size="100" /></td>
    <td><asp:TextBox ID="txtTitle1" runat="server"></asp:TextBox></td>
    <td><asp:TextBox ID="txtTags1" runat="server"></asp:TextBox></td>
    </tr>
    <tr>
    <td><asp:FileUpload ID="FileUpload2" runat="server" size="100"  /></td>
    <td><asp:TextBox ID="txtTitle2" runat="server"></asp:TextBox></td>
    <td><asp:TextBox ID="txtTags2" runat="server"></asp:TextBox></td>
    </tr>
   
    <tr>
    <td><asp:FileUpload ID="FileUpload3" runat="server" size="100" /></td>
    <td><asp:TextBox ID="txtTitle3" runat="server"></asp:TextBox></td>
    <td><asp:TextBox ID="txtTags3" runat="server"></asp:TextBox></td>
    </tr>
      <tr>
    <td><asp:FileUpload ID="FileUpload4" runat="server" size="100" /></td>
    <td><asp:TextBox ID="txtTitle4" runat="server"></asp:TextBox></td>
    <td><asp:TextBox ID="txtTags4" runat="server"></asp:TextBox></td>
    </tr>
      <tr>
    <td><asp:FileUpload ID="FileUpload5" runat="server" size="100" /></td>
    <td><asp:TextBox ID="txtTitle5" runat="server"></asp:TextBox></td>
    <td><asp:TextBox ID="txtTags5" runat="server"></asp:TextBox></td>
    </tr>
      <tr>
    <td><asp:FileUpload ID="FileUpload6" runat="server" size="100" /></td>
    <td><asp:TextBox ID="txtTitle6" runat="server"></asp:TextBox></td>
    <td><asp:TextBox ID="txtTags6" runat="server"></asp:TextBox></td>
    </tr>
      <tr>
    <td><asp:FileUpload ID="FileUpload7" runat="server" size="100" /></td>
    <td><asp:TextBox ID="txtTitle7" runat="server"></asp:TextBox></td>
    <td><asp:TextBox ID="txtTags7" runat="server"></asp:TextBox></td>
    </tr>
      <tr>
    <td><asp:FileUpload ID="FileUpload8" runat="server" size="100" /></td>
    <td><asp:TextBox ID="txtTitle8" runat="server"></asp:TextBox></td>
    <td><asp:TextBox ID="txtTags8" runat="server"></asp:TextBox></td>
    </tr>
      <tr>
    <td><asp:FileUpload ID="FileUpload9" runat="server" size="100" /></td>
    <td><asp:TextBox ID="txtTitle9" runat="server"></asp:TextBox></td>
    <td><asp:TextBox ID="txtTags9" runat="server"></asp:TextBox></td>
    </tr>
      <tr>
    <td><asp:FileUpload ID="FileUpload10" runat="server" size="100" /></td>
    <td><asp:TextBox ID="txtTitle10" runat="server"></asp:TextBox></td>
    <td><asp:TextBox ID="txtTags10" runat="server"></asp:TextBox></td>
    </tr>
    <tr><td><asp:Button ID="btnUpload" runat="server" Text="Upload Photos" 
            onclick="btnUpload_Click" /></td><td></td>
            <td>  <asp:Button ID="btnShowAlbum" runat="server" Text="View previous photos" 
            onclick="btnShowAlbum_Click" /></td></tr>
            <tr><td colspan="3"></td></tr>
    </table>
<asp:Label ID="lblMsg" runat="server" Text="" size="1000"></asp:Label>
 
    
</asp:Content>

