﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;

    public partial class uploadphotos : System.Web.UI.Page
    {
        static int sintFirstPhotoId;
        string dir = "";
        String title = "";
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public bool ThumbnailCallback()
        {
            return true;
        }
        protected void btnUpload_Click(object sender, EventArgs e)
        {
           
            String aid = Request.QueryString["aid"];
            int id;
            if (Request.QueryString["atitle"] != null)
                title = Request.QueryString["atitle"].Trim();
            // add photos 
                dir = Server.MapPath("photos/") + title.Trim();
           if (!Directory.Exists(dir))  // if it doesn't exist, create
                Directory.CreateDirectory(dir);
            try
            {
                if (FileUpload1.HasFile)
                {
                    id = PhotosDAL.AddPhoto(aid,FileUpload1.FileName, txtTitle1.Text, txtTags1.Text);
                    // save photo 
                    sintFirstPhotoId = id;
                                 
                  //  FileUpload1.SaveAs(Server.MapPath("photos/" + title + "_" + id + ".jpg"));
                  
                    FileUpload1.SaveAs(Server.MapPath("photos/"+title+"/"+ id + ".jpg"));


                  
                        // create an image object, using the filename we just retrieved
                        System.Drawing.Image image = System.Drawing.Image.FromFile(Server.MapPath("photos/ "+title+"/"+ id + ".jpg"));

                        // create the actual thumbnail image
                        System.Drawing.Image thumbnailImage = image.GetThumbnailImage(64, 64, new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback), IntPtr.Zero);

                        // make a memory stream to work with the image bytes
                        MemoryStream imageStream = new MemoryStream();

                        // put the image into the memory stream
                        thumbnailImage.Save(Server.MapPath("thumbs/" + title + "/" + id + ".jpg"), System.Drawing.Imaging.ImageFormat.Jpeg);


                  



                }

                if (FileUpload2.HasFile)
                {
                    id = PhotosDAL.AddPhoto(aid, FileUpload2.FileName, txtTitle2.Text, txtTags2.Text);
                    // save photo 
                    FileUpload2.SaveAs(Server.MapPath("photos/" + title + "/" + id + ".jpg"));
                }


                if (FileUpload3.HasFile)
                {
                    id = PhotosDAL.AddPhoto(aid, FileUpload3.FileName, txtTitle3.Text, txtTags3.Text);
                    // save photo 
                    FileUpload3.SaveAs(Server.MapPath("photos/" + title + "/" + id + ".jpg"));
                }
                if (FileUpload4.HasFile)
                {
                    id = PhotosDAL.AddPhoto(aid, FileUpload4.FileName, txtTitle4.Text, txtTags4.Text);
                    // save photo 
                    FileUpload4.SaveAs(Server.MapPath("photos/" + title + "/" + id + ".jpg"));
                }

                if (FileUpload5.HasFile)
                {
                    id = PhotosDAL.AddPhoto(aid, FileUpload5.FileName, txtTitle5.Text, txtTags5.Text);
                    // save photo 
                    FileUpload5.SaveAs(Server.MapPath("photos/" + title + "/" + id + ".jpg"));
                }


                if (FileUpload6.HasFile)
                {
                    id = PhotosDAL.AddPhoto(aid, FileUpload6.FileName, txtTitle6.Text, txtTags6.Text);
                    // save photo 
                    FileUpload6.SaveAs(Server.MapPath("photos/" + title + "/" + id + ".jpg"));
                }
                if (FileUpload7.HasFile)
                {
                    id = PhotosDAL.AddPhoto(aid, FileUpload7.FileName, txtTitle7.Text, txtTags7.Text);
                    // save photo 
                    FileUpload7.SaveAs(Server.MapPath("photos/" + title + "/" + id + ".jpg"));
                }

                if (FileUpload8.HasFile)
                {
                    id = PhotosDAL.AddPhoto(aid, FileUpload8.FileName, txtTitle8.Text, txtTags8.Text);
                    // save photo 
                    FileUpload8.SaveAs(Server.MapPath("photos/" + title + "/" + id + ".jpg"));
                }

                if (FileUpload9.HasFile)
                {
                    id = PhotosDAL.AddPhoto(aid, FileUpload9.FileName, txtTitle9.Text, txtTags9.Text);
                    // save photo 
                    FileUpload9.SaveAs(Server.MapPath("photos/" + title + "/" + id + ".jpg"));
                }
                if (FileUpload10.HasFile)
                {
                    id = PhotosDAL.AddPhoto(aid, FileUpload3.FileName, txtTitle10.Text, txtTags10.Text);
                    // save photo 
                    FileUpload10.SaveAs(Server.MapPath("photos/" + title + "/" + id + ".jpg"));
                }

                lblMsg.Text = "Uploaded Photos Successfully!";
                Response.Redirect("showalbum.aspx?aid=" + Request.QueryString["aid"] + "&atitle=" + title);
            }
            catch (Exception ex)
            {
                throw ex;
                lblMsg.Text = "Upload Failed -> " + ex.InnerException + "  "+ ex.Source;
            }

        }
        protected void btnShowAlbum_Click(object sender, EventArgs e)
        {
            Response.Redirect("showalbum.aspx?photoid=" + sintFirstPhotoId + "&aid=" + Request.QueryString["aid"]+"&atitle="+title);
        }
    }
