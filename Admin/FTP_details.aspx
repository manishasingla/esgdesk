﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FTP_details.aspx.cs" Inherits="Admin_FTP_details" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><%= ConfigurationManager.AppSettings["CompanyName"].ToString()%> - ftp details</title>
</head>
<body>
    <form id="form1" runat="server">
     <div align="center" id="hidegridError" runat="server">
            <table>
                <tr>
                    <td colspan="2" align="center">
                        <asp:Label ID="lblerror" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
     <div align="center" id="hidegrid" runat="server">
            <table>
                <tr>
                    <td colspan="2" align="center">
                        <asp:Label ID="lblwebsite" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Host address/IP:
                    </td>
                    <td>
                        <asp:Label ID="lblIP" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        User name:
                    </td>
                    <td>
                        <asp:Label ID="lblUsername" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Password:
                    </td>
                    <td>
                        <asp:Label ID="lblPassword" runat="server"></asp:Label>
                    </td>
                </tr>
                <%--*************************************************************--%>
                <tr>
                    <td colspan="2" align="center">
                        <asp:Label ID="lblTestDomainwebsite" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Host address/IP:
                    </td>
                    <td>
                        <asp:Label ID="lblTestIP" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        User name:
                    </td>
                    <td>
                        <asp:Label ID="lblTestUsername" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Password:
                    </td>
                    <td>
                        <asp:Label ID="lblTestPassword" runat="server"></asp:Label>
                    </td>
                </tr>
                <%--*************************************************************--%>
            </table>
        </div>
    <%--<div>
        <table>
            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="lblwebsite" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Host address/IP:
                </td>
                <td>
                    <asp:Label ID="lblIP" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    User name:
                </td>
                <td>
                    <asp:Label ID="lblUsername" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Password:
                </td>
                <td>
                    <asp:Label ID="lblPassword" runat="server"></asp:Label>
                </td>
            </tr>
            
            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="lblTestDomainwebsite" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Host address/IP:
                </td>
                <td>
                    <asp:Label ID="lblTestIP" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    User name:
                </td>
                <td>
                    <asp:Label ID="lblTestUsername" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Password:
                </td>
                <td>
                    <asp:Label ID="lblTestPassword" runat="server"></asp:Label>
                </td>
            </tr>
            
            
            
            
            
            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="lblerror" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        
    </div>--%>
    </form>
</body>
</html>
