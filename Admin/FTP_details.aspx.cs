﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Admin_FTP_details : System.Web.UI.Page
{
    int id;
    protected void Page_Load(object sender, EventArgs e)
    {

        string var = Request.QueryString["id"];

        try
        {
            id = Convert.ToInt32(var);
        }
        catch
        {
            lblerror.Text = "Project ID must be an integer.";
            return;
        }
        //===============================
        hidegridError.Visible = false;
        //===============================
        GetProjectdata(id);

    }
    private void GetProjectdata(int id)
    {
        string strSql = "select * from projects where project_id=" + id;
        DataSet ds = DatabaseHelper.getDataset(strSql);
        //********************************************************************************************
        
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["HostAddress_IP"].ToString() != "" || ds.Tables[0].Rows[0]["UserName"].ToString() != "" || ds.Tables[0].Rows[0]["Password"].ToString() != "" || ds.Tables[0].Rows[0]["TestHostAddress_IP"].ToString() != "" || ds.Tables[0].Rows[0]["TestUserName"].ToString() != "" || ds.Tables[0].Rows[0]["TestPassword"].ToString()!="")
                {
                lblwebsite.Text = ds.Tables[0].Rows[0]["WebsiteUrl"].ToString().Replace("''", "'");
                lblIP.Text = ds.Tables[0].Rows[0]["HostAddress_IP"].ToString().Replace("''", "'");
                lblUsername.Text = ds.Tables[0].Rows[0]["UserName"].ToString().Replace("''", "'");
                lblPassword.Text = ds.Tables[0].Rows[0]["Password"].ToString().Replace("''", "'");

                lblTestDomainwebsite.Text = ds.Tables[0].Rows[0]["TestDomainUrl"].ToString().Replace("''", "'");
                lblTestIP.Text = ds.Tables[0].Rows[0]["TestHostAddress_IP"].ToString().Replace("''", "'");
                lblTestUsername.Text = ds.Tables[0].Rows[0]["TestUserName"].ToString().Replace("''", "'");
                lblTestPassword.Text = ds.Tables[0].Rows[0]["TestPassword"].ToString().Replace("''", "'");
                hidegridError.Visible = false;
                hidegrid.Visible = true;
                }
                else
                {
                    lblerror.Text = "Project record was not found.";
                    hidegridError.Visible = true;
                    hidegrid.Visible = false;
                
                }
                
            }
            else
            {
                lblerror.Text = "Project was not found.";
                hidegridError.Visible = true;
                hidegrid.Visible = false;

            }
        
    }
               //********************************************************************************************




    

}