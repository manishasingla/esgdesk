﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Faq.aspx.cs" Inherits="faq" %>
<%@ Register Src="Notifications.ascx" TagName="Notifications" TagPrefix="uc4" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title>ESG desk - FAQ</title>
<link rel="stylesheet" href="class/style.css" type="text/css" media="screen, projection" />
<link rel="stylesheet" href="class/layout.css" type="text/css" media="screen, projection" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<link rel="stylesheet" href="class/tabs.css"/>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript" src="jquery/tabexpand.js">

/***********************************************
* Accordion Content script- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* Visit http://www.dynamicDrive.com for hundreds of DHTML scripts
* This notice must stay intact for legal use
***********************************************/

</script>
<script type="text/javascript">


tabexpand.init({
	headerclass: "submenuheader", //Shared CSS class name of headers group
	contentclass: "submenu", //Shared CSS class name of contents group
	revealtype: "click", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"
	mouseoverdelay: 200, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
	collapseprev: true, //Collapse previous content (so only one open at any time)? true/false 
	defaultexpanded: [0], //index of content(s) open by default [index1, index2, etc] [] denotes no content
	onemustopen: false, //Specify whether at least one header should be open always (so never all headers closed)
	animatedefault: false, //Should contents open by default be animated into view?
	persiststate: false, //persist state of opened contents within browser session?
	toggleclass: ["", ""], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
	togglehtml: ["suffix", "<img src='../images/plus-icon.png' class='statusicon' />", "<img src='../images/minus-icon.png' class='statusicon' />"], //Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
	animatespeed: "fast", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
	oninit:function(headers, expandedindices){ //custom code to run when headers have initalized
		//do nothing
	},
	onopenclose:function(header, index, state, isuseractivated){ //custom code to run whenever a header is opened or closed
		//do nothing
	}
})

function keypressHandler(evt) {

    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode == 13)
        var btn = document.getElementById('<%=btnSearch.ClientID%>');
    btn.focus();
    btn.click();
}
</script>

</head>
<body>
<form id="form1" runat="server">
<div id="wrapper">
<uc2:Admin_Header ID="Admin_Header" runat="server" />
 <div>
           <uc4:Notifications ID="Notifications" runat="server" />
            </div>
  <div id="wrap" class="container clearfix">
  
    <div class="borderdiv2"> </div>
    <div class="divider4"> </div>
    <div class="middivfaq">
      <div>
        <h3 class="htitle">Frequently Asked Questions (FAQs)
</h3>
      </div>
      <div id="divResult" runat="server" class="left_content" style="width:100%; float:left;">
          <%= strFaq %>
        </div>
        <div id="divNoResult" runat="server" class="left_content" style="width:100%; float:left; display:none">No related search item found</div>
    </div>
    <div class="rightsidebuttons">
    
    <div class="panelsearch"> <img src="../images/faq-img.png" alt="0" border="0" /> </div>
    
    <div class="faqright">Still can’t find an answer to what you’re looking for? </div>
      <div class="panelsearch"><asp:TextBox ToolTip="Keyword search" ID="txtKeyword" runat="server" Width="165px" onkeypress="javascript:keypressHandler(event)"></asp:TextBox>
      <asp:RequiredFieldValidator ID="Reqkeyword" runat="server" ControlToValidate="txtKeyword" 
                                                    ErrorMessage="Please enter keyword." Display="None">*</asp:RequiredFieldValidator>
      </div>
      <div class="panelsearch"> <asp:Button CssClass="button" ID="btnSearch"
              runat="server" Text="Search" onclick="btnSearch_Click" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <asp:Button CssClass="button" ID="btnCancel" 
              runat="server" Text="Cancel" onclick="btnCancel_Click" CausesValidation="false" />
              <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                                ShowSummary="False" />
      </div>
      <div class="panelsearch" style="margin-top:10px;"> <div class="rightbuttons"><a href="mailto:#">Email us</a></div> </div>
    </div>
    <div class="dividerfaq"> </div>
    <uc1:footer ID="Footer1" runat="server" />
  </div>
  </div>
</form>
</body>
</html>
