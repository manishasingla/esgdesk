﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class faq : System.Web.UI.Page
{
   
    public string strFaq = "";
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = (Request.QueryString["id"] == null || Request.QueryString["id"].ToString() == "" ? "tasks.aspx" : "Faq.aspx");
            Response.Redirect("login.aspx");
            return;
        }
        else
        {
            if (!IsPostBack)
            {
                BindAllRecords();
            }
        }
    }

    void BindRecords(string searchstring)
    {
        try
        {
            DataSet dsPages = new DataSet();
            strFaq = "";

            dsPages = DatabaseHelper.GetRecords(searchstring.ToLower().Replace(" of ", " ").Replace(" and ", " ").Replace(" the ", " ").Replace(" a ", " ").Replace(" an ", " ").Replace(" with ", " ").Replace(" and ", " ").Replace("?", " "));

            if (dsPages != null && dsPages.Tables.Count > 0)
            {
                if (dsPages.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i <= dsPages.Tables[0].Rows.Count - 1; i++)
                    {
                        strFaq += "<h2><a class=\"menuitem submenuheader\">" + (i + 1) + "." + dsPages.Tables[0].Rows[i]["question"].ToString().Replace("|@", "'").Trim() + "</a></h2>";
                        strFaq += "<div class=\"submenu\"><p>";
                        strFaq += dsPages.Tables[0].Rows[i]["answer"].ToString().Replace("|@", "'").Trim() + "</p></div>";
                    }
                    if (strFaq == "")
                    {
                        divNoResult.Style.Add("display", "none");
                        divResult.Style.Add("display", "inline");
                    }
                }
                else
                {
                    divNoResult.Style.Add("display", "inline");
                    divResult.Style.Add("display", "none");
                }
            }
            else
            {
                divNoResult.Style.Add("display", "inline");
                divResult.Style.Add("display", "none");
            }
        }
        catch { }      
    }

    void BindAllRecords()
    {
        try
        {
            DataSet dsPages = new DataSet();
            strFaq = "";

            dsPages = DatabaseHelper.GetAllRecords();

            if (dsPages != null && dsPages.Tables.Count > 0)
            {
                if (dsPages.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i <= dsPages.Tables[0].Rows.Count - 1; i++)
                    {
                        strFaq += "<h2><a class=\"menuitem submenuheader\">" + (i + 1) + "." + dsPages.Tables[0].Rows[i]["question"].ToString().Replace("|@", "'").Trim()  +"</a></h2>";
                        strFaq += "<div class=\"submenu\"><p>";
                        strFaq += dsPages.Tables[0].Rows[i]["answer"].ToString().Replace("|@", "'").Trim() + "</p></div>";
                        //dsPages.Tables[0].Rows[i]["Question"].ToString();
                    }
                    if (strFaq != "")
                    {
                        divNoResult.Style.Add("display", "none");
                        divResult.Style.Add("display", "inline");
                    }
                }
                else
                {
                    divNoResult.Style.Add("display", "inline");
                    divResult.Style.Add("display", "none");
                }
            }
            else
            {
                divNoResult.Style.Add("display", "inline");
                divResult.Style.Add("display", "none");
            }
        }
        catch { }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindRecords(txtKeyword.Text);
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtKeyword.Text = "";
        BindAllRecords();
    }
}
