﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class glossary : System.Web.UI.Page
{

    public string strGlossary = "";
    protected void Page_Load(object sender, EventArgs e)
    {

        //if (Session["admin"] == null || Session["admin"].ToString() == "")
        //{
        //    Response.Redirect("login.aspx");
        //    return;
        //}
        //else
        //{
        //    if (!IsPostBack)
        //    {
        //        BindAllRecords();
        //    }
        //}

        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = (Request.QueryString["id"] == null || Request.QueryString["id"].ToString() == "" ? "tasks.aspx" : "glossary.aspx");
            Response.Redirect("login.aspx");
            return;
        }
        else
        {
            if (!IsPostBack)
            {
                BindAllRecords();
            }
        }
    }

     

    void BindRecords(string searchstring)
    {
        try
        {
            DataSet dsPages = new DataSet();
            strGlossary = "";

            dsPages = DatabaseHelper.GetGlossaryRecords(searchstring.ToLower().Replace(" of ", " ").Replace(" and ", " ").Replace(" the ", " ").Replace(" a ", " ").Replace(" an ", " ").Replace(" with ", " ").Replace(" and ", " ").Replace("?", " "));

            if (dsPages != null && dsPages.Tables.Count > 0)
            {
                if (dsPages.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i <= dsPages.Tables[0].Rows.Count - 1; i++)
                    {
                        if (i == 0)
                        {
                            strGlossary += "<p style=\"margin-top:5px;\" class=\"glossarytext\">" + dsPages.Tables[0].Rows[i]["Word"].ToString().Replace("|@", "'").Trim() + "</p>";
                            strGlossary += "<p>";
                            strGlossary += dsPages.Tables[0].Rows[i]["Desc"].ToString() + "</p><div class=\"ggap\"></div>";
                        }
                        else
                        {
                            strGlossary += "<p class=\"glossarytext\">" + dsPages.Tables[0].Rows[i]["Word"].ToString().Replace("|@","'").Trim() + "</p>";
                            strGlossary += "<p>";
                            strGlossary += dsPages.Tables[0].Rows[i]["Desc"].ToString().Replace("@", "'").Trim() + "</p><div class=\"ggap\"></div>";
                        }
                    }
                    if (strGlossary != "")
                    {
                        divNoResult.Style.Add("display", "none");
                        divResult.Style.Add("display", "inline");
                    }
                }
                else
                {
                    divNoResult.Style.Add("display", "inline");
                    divResult.Style.Add("display", "none");
                }
            }
            else
            {
                divNoResult.Style.Add("display", "inline");
                divResult.Style.Add("display", "none");
            }
        }
        catch { }
    }

    void BindAllRecords()
    {
        try
        {
            DataSet dsPages = new DataSet();
            strGlossary = "";

            dsPages = DatabaseHelper.GetAllGlossaryRecords();

            if (dsPages != null && dsPages.Tables.Count > 0)
            {
                if (dsPages.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i <= dsPages.Tables[0].Rows.Count - 1; i++)
                    {
                        if (i == 0)
                        {
                            strGlossary += "<p style=\"margin-top:5px;\" class=\"glossarytext\">" + dsPages.Tables[0].Rows[i]["Word"].ToString().Replace("|@", "'").Trim() + "</p>";
                            strGlossary += "<p>";
                            strGlossary += dsPages.Tables[0].Rows[i]["Desc"].ToString() + "</p><div class=\"ggap\"></div>";
                        }
                        else
                        {
                            strGlossary += "<p class=\"glossarytext\">" + dsPages.Tables[0].Rows[i]["Word"].ToString().Replace("|@", "'").Trim() + "</p>";
                            strGlossary += "<p>";
                            strGlossary += dsPages.Tables[0].Rows[i]["Desc"].ToString() + "</p><div class=\"ggap\"></div>";
                        }
                    }
                    if (strGlossary != "")
                    {
                        divNoResult.Style.Add("display", "none");
                        divResult.Style.Add("display", "inline");
                    }
                }
                else
                {
                    divNoResult.Style.Add("display", "inline");
                    divResult.Style.Add("display", "none");
                }
            }
            else
            {
                divNoResult.Style.Add("display", "inline");
                divResult.Style.Add("display", "none");
            }
        }
        catch { }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindRecords(txtGlossary.Text);
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtGlossary.Text = "";
        BindAllRecords();
    }
}
