using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
using Telerik.Web;

public partial class Admin_GoogleAcList : System.Web.UI.Page
{
    string sql = "";
    string strfilter = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = "GoogleAcList.aspx";
            Response.Redirect("login.aspx");
            return;
        }

        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            Response.Redirect("tasks.aspx");
            return;
        }

        divMessage.InnerHtml = "";
        if (!Page.IsPostBack)
        {
            lblOrderBy.Text = " ORDER BY project_name ASC";
            getFilterSession();
            bindData();
        }
    }
    private void setFileterSession()
    {
        Session["projectOrderBy"] = lblOrderBy.Text;
    }

    private void getFilterSession()
    {
        try
        {
            if (Session["projectOrderBy"] != null)
                lblOrderBy.Text = Session["projectOrderBy"].ToString();
        }
        catch
        {
        }
    }

    private void bindData()
    {
        string strSql = "";
        if (strfilter != "")
        {
            strSql = "select * from projects " + strfilter + " and SEO ='True' " + lblOrderBy.Text;
        }
        else
        {
            strSql = "select * from projects where SEO ='True' " + lblOrderBy.Text;
        }

        DataSet ds = DatabaseHelper.getDataset(strSql);

        if (ds != null)
        {
            try
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    PagedDataSource objds = new PagedDataSource();
                    objds.DataSource = ds.Tables[0].DefaultView;

                    LblRecordsno.Text = objds.DataSourceCount.ToString();
                    DataGrid1.DataSource = ds.Tables[0];
                    DataGrid1.DataBind();
                }
                else
                {
                    LblRecordsno.Text = "0";
                    DataGrid1.DataSource = null;
                    DataGrid1.DataBind();
                    divMessage.InnerHtml = "No projects in the database..";
                }
            }
            catch
            {
                DataGrid1.DataSource = null;
                DataGrid1.DataBind();
                divMessage.InnerHtml = "No projects in the database..";
            }
        }
        else
        {
            try
            {
                DataGrid1.DataSource = null;
                DataGrid1.DataBind();
                divMessage.InnerHtml = "No projects in the database..";
            }
            catch
            {
                DataGrid1.DataSource = null;
                DataGrid1.DataBind();
                divMessage.InnerHtml = "No projects in the database..";
            }
        }
    }
    protected void DataGrid1_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        foreach (TableCell tc in e.Item.Cells)
        {
            tc.Attributes["style"] = "border:1px solid #00A651;";
        }
        if (e.Item is GridPagerItem)
        {
            GridPagerItem pagerItem = (GridPagerItem)e.Item;
            RadComboBox PageSizeComboBox = (RadComboBox)pagerItem.FindControl("PageSizeComboBox");
            PageSizeComboBox.Visible = false;
            Label changePageSizelbl = (Label)pagerItem.FindControl("ChangePageSizeLabel");
            changePageSizelbl.Visible = false;
        }
        if (e.Item.ItemType == GridItemType.Item || e.Item.ItemType == GridItemType.AlternatingItem)
        {
            GridDataItem dataItem = e.Item as GridDataItem;
            LinkButton deleteButton = (LinkButton)dataItem.FindControl("lnkdelete");
            LinkButton btnAnalytics = (LinkButton)dataItem.FindControl("lnkanalytics");
            deleteButton.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete this project?')");
        }
    }
    protected void DataGrid1_ItemCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
    {
        GridDataItem dataItem = e.Item as GridDataItem;
        if (e.CommandName == "delete")
        {
            object objResult = DatabaseHelper.executeScalar("select count(*) from tasks where project =" + ((Label)dataItem.FindControl("lblprojectname")).Text);
            object objResult2 = DatabaseHelper.executeScalar("select count(*) from ClientRequest where ProjectName =" + ((Label)dataItem.FindControl("lblprojectname")).Text);
            if (objResult.ToString() == "0" && objResult2.ToString() == "0")
            {
                string strSql = "delete from projects where project_id=" + ((Label)dataItem.FindControl("lblprojectid")).Text;
                int intResult = DatabaseHelper.executeNonQuery(strSql);
                if (intResult != 0)
                {
                    divMessage.InnerHtml = "project was deleted.";
                    bindData();
                }
                else
                {
                    divMessage.InnerHtml = "Project was not deleted.";
                    bindData();
                }
            }
            else
            {
                divMessage.InnerHtml = "You can't delete project \"" + ((Label)dataItem.FindControl("lblprojectname")).Text + "\"  because some tasks/CRs still reference it...";
            }
        }
        else if (e.CommandName == "Analytics")
        {
            Session["GoogleUser"] = ((Label)dataItem.FindControl("lblprojectname")).Text;
            Session["GooglePwd"] = ((Label)dataItem.FindControl("lblprojectname")).Text;
            if (e.Item.Cells[4].Text.ToString() != "")
            {
                if (e.Item.Cells[4].Text.ToString() != "&nbsp;")
                {
                    Session["GoogleUser"] = e.Item.Cells[4].Text;
                    Session["GooglePwd"] = e.Item.Cells[5].Text;
                    Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> window.open('Analytics.aspx','_blank'); </script>");
                }
                else
                {
                    Session["GoogleUser"] = null;//e.Item.Cells[9].Text;
                    Session["GooglePwd"] = null;//e.Item.Cells[10].Text;
                    Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> alert('Analytics  not added for this project.')</script>");
                }
            }
            else
            {
                Session["GoogleUser"] = null;//e.Item.Cells[9].Text;
                Session["GooglePwd"] = null;//e.Item.Cells[10].Text;
                Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> alert('Analytics  not added for this project.')</script>");
            }
        }
    }
    protected void DataGrid1_PageIndexChanged(object source, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        DataGrid1.CurrentPageIndex = e.NewPageIndex;
        bindData();
    }
    protected void DataGrid1_SortCommand(object source, Telerik.Web.UI.GridSortCommandEventArgs e)
    {
        if (e.SortExpression.ToString() == Session["projectColumn"])
        {
            //Reverse the sort order
            if (Session["projectOrder"] == "DESC")
            {
                lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " ASC";
                Session["projectOrder"] = "ASC";
            }
            else
            {
                lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " DESC";
                Session["projectOrder"] = "DESC";
            }
        }
        else
        {
            //Different column selected, so default to ascending order
            lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " DESC";
            Session["projectOrder"] = "DESC";
        }

        Session["projectColumn"] = e.SortExpression.ToString();
        Session["projectOrderBy"] = lblOrderBy.Text;
        bindData();
    }
    protected void lnkAll_Click(object sender, EventArgs e)
    {
        strfilter = "";
        bindData();
    }
    protected void lnkA_Click(object sender, EventArgs e)
    {
        string strchar = "A";
        strfilter = "where project_name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkB_Click(object sender, EventArgs e)
    {
        string strchar = "B";
        strfilter = "where project_name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkC_Click(object sender, EventArgs e)
    {
        string strchar = "C";
        strfilter = "where project_name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkD_Click(object sender, EventArgs e)
    {
        string strchar = "D";
        strfilter = "where project_name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkE_Click(object sender, EventArgs e)
    {
        string strchar = "E";
        strfilter = "where project_name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkF_Click(object sender, EventArgs e)
    {
        string strchar = "F";
        strfilter = "where project_name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkG_Click(object sender, EventArgs e)
    {
        string strchar = "G";
        strfilter = "where project_name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkH_Click(object sender, EventArgs e)
    {
        string strchar = "H";
        strfilter = "where project_name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkI_Click(object sender, EventArgs e)
    {
        string strchar = "I";
        strfilter = "where project_name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkJ_Click(object sender, EventArgs e)
    {
        string strchar = "J";
        strfilter = "where project_name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkK_Click(object sender, EventArgs e)
    {
        string strchar = "K";
        strfilter = "where project_name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkL_Click(object sender, EventArgs e)
    {
        string strchar = "L";
        strfilter = "where project_name like '" + strchar + "%'";
        bindData();

    }
    protected void lnkM_Click(object sender, EventArgs e)
    {
        string strchar = "M";
        strfilter = "where project_name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkN_Click(object sender, EventArgs e)
    {
        string strchar = "N";
        strfilter = "where project_name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkO_Click(object sender, EventArgs e)
    {
        string strchar = "O";
        strfilter = "where project_name like '" + strchar + "%'";
        bindData();

    }
    protected void lnkP_Click(object sender, EventArgs e)
    {
        string strchar = "P";
        strfilter = "where project_name like '" + strchar + "%'";
        bindData();

    }
    protected void lnkQ_Click(object sender, EventArgs e)
    {
        string strchar = "Q";
        strfilter = "where project_name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkR_Click(object sender, EventArgs e)
    {
        string strchar = "R";
        strfilter = "where project_name like '" + strchar + "%'";
        bindData();

    }
    protected void lnkS_Click(object sender, EventArgs e)
    {
        string strchar = "S";
        strfilter = "where project_name like '" + strchar + "%'";
        bindData();

    }
    protected void lnkT_Click(object sender, EventArgs e)
    {
        string strchar = "T";
        strfilter = "where project_name like '" + strchar + "%'";
        bindData();

    }
    protected void lnkU_Click(object sender, EventArgs e)
    {

        string strchar = "U";
        strfilter = "where project_name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkV_Click(object sender, EventArgs e)
    {
        string strchar = "V";
        strfilter = "where project_name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkW_Click(object sender, EventArgs e)
    {
        string strchar = "W";
        strfilter = "where project_name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkX_Click(object sender, EventArgs e)
    {
        string strchar = "X";
        strfilter = "where project_name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkY_Click(object sender, EventArgs e)
    {
        string strchar = "Y";
        strfilter = "where project_name like '" + strchar + "%'";
        bindData();
    }
    protected void lnkZ_Click(object sender, EventArgs e)
    {
        string strchar = "Z";
        strfilter = "where project_name like '" + strchar + "%'";
        bindData();
    }
}
