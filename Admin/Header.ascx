<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Header.ascx.cs" Inherits="Admin_Header" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<script src="SpryMenuBar.js" type="text/javascript"></script>
<link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<script type="text/javascript" language="javascript">
    function openPopup(flag)
    {    
        if(flag==1)
        {
            document.getElementById('frameQuickQuestion').style.display="block"; 
            document.getElementById('frameCallTaken').style.display="none";
        }
        else
        {
            document.getElementById('frameQuickQuestion').style.display="none"; 
            document.getElementById('frameCallTaken').style.display="block";
        }
        document.getElementById('Header1_blnClick').click();
    }
</script>


<div style="height:120px" class="divBorder">
    <div style="text-align:right;padding:5px;height:50px">
    <table width="0" cellpadding="0" cellspacing="0" style="width:100%">
    <tr>
    <td align="left" valign="top" style="height: 57px">
        <span id="pageHeader">
       <asp:Label ID="lblTitle" runat="server" Text="Client Requests"></asp:Label></span></td>
        <td style="padding-right: 20px; height: 57px;" valign="top" align="right">
        <div id="divHeader1" runat="server">
        <a ID="lnkLogin" runat="server" href="../login.aspx">Client Login</a>
        &nbsp;&nbsp;<a ID="lnkAdminLogin" runat="server" href="login.aspx">Support login</a>
        &nbsp;&nbsp;<a id="lnkRegister"  runat="server" style="display:none;"  href="../register.aspx">Register</a>
        </div>        
        <a id="lnkUserName"  runat="server" href="#" target="_blank"></a>
        </td>
        <td align="right" valign="top" style="width:200px; height: 57px;">
        <a class="addthis_button" style="text-align:left;" href="http://www.addthis.com/bookmark.php?v=250&amp;pub=esatesolutions"><img src="http://s7.addthis.com/static/btn/lg-share-en.gif" alt="Bookmark and Share" width="125" height="16" align="right" style="border:0"/></a><br />
        Call: 020 7609 2800<br />
        </td>
    </tr>
    </table>        
    </div>     
    <div style="text-align:left;vertical-align:bottom;padding:5px;height:50px" id="divHeader2" runat="server">
       <table border="0" cellpadding="0" cellspacing="0" width="100%">
       <tr>
       <td><asp:LinkButton ID="lnkClientRequests" runat="server" CausesValidation="false" BackColor="transparent" OnClick="lnkClientRequests_Click" > Client requests&nbsp;</asp:LinkButton> </td>
        <td>
            
<ul id="MenuBar1" class="MenuBarHorizontal" style="background-color:Transparent;">
  
  <li> <asp:LinkButton ID="lnkTasks" runat="server" BackColor="transparent" CausesValidation="false" OnClick="lnkTasks_Click"> Task</asp:LinkButton> 
      <ul>
        <li><a  href="#">View ></a>
            <ul>
              <li><asp:LinkButton ID="LnkCrntlywrkTask" runat="server" CausesValidation="False" OnClick="LnkCrntlywrkTask_Click" Width="146px" > > Currently working on</asp:LinkButton></li>
              
            </ul>
        </li>
        <li><a href="edit_task.aspx">New</a></li>
       
      </ul>
  </li>
 
</ul> 

        </td>
        <td><a href="admin.aspx" id="lnkAdmin" runat="server">Admin&nbsp;</a> &nbsp; 
            <a href = "#" id="lnkQuickQuestion" onclick="openPopup(1);" runat="server">Quick question&nbsp;</a> &nbsp; 
            <a href="#" id="A2" onclick="openPopup(2);" runat="server">Call taken&nbsp;</a> &nbsp;
            <a href="change_password.aspx" id="A4" runat="server">Change password&nbsp;</a> &nbsp;
            <a href="settings.aspx" id="lnkSettings" runat="server">Settings&nbsp;</a> &nbsp; 
            <a href="EODC.aspx" id="A3" runat="server" target="_blank">Send EODC&nbsp;</a> &nbsp; 
            <asp:LinkButton ID="lnkLogout" runat="server" CausesValidation="False" OnClick="lnkLogout_Click">Logout</asp:LinkButton></td>
        <td align="right">
        
        <table>
        <tr>
            <td >
                <table id = "tblSearchRequest" runat="server" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td>Request Id:
                    </td>
                    <td>
                        <asp:TextBox ID="txtRequestId" runat="server" Width="40px" MaxLength="10"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Button ID="btnGo" runat="server" Text="Go" CausesValidation="False" OnClick="btnGo_Click" />
                     </td>
                </tr>
                </table>
            </td>
            <td >
                <table>
                <tr>
                    <td ></td>
                     <td>Task Id:
                    </td>
                    <td>
                        <asp:TextBox ID="txtTaskId" runat="server" Width="40px" MaxLength="10"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Button ID="btnGotoTask" runat="server" Text="Go" CausesValidation="False" OnClick="btnGotoTask_Click"  />
                    </td>
                </tr>
                </table>
            </td>
             <td style="padding-left:3px; padding-bottom:5px" >
                <table id = "Tblsearchbyword" runat="server" border="0" cellpadding="0" cellspacing="0">
               
                 <tr>
                    <td align="left" colspan="2" style="font-size:12px">Search by word:
                           <asp:CheckBox ID="ChkTask" runat="server" Text="Task"/><asp:CheckBox ID="ChkCrs" runat="server" Text="CRs"   /><asp:CheckBox ID="ChkIncludecomment"
                                runat="server" Text="Include comments" />
                    </td>
                    
                </tr>
                <tr style="padding-top:2px">
                    
                    <td >
                        <asp:TextBox ID="txtWord" runat="server" Width="221px"></asp:TextBox>
                    </td>
                    <td align="right">
                        <asp:Button ID="BtnGoWrdSearch" runat="server" Text="Go" CausesValidation="False" OnClick="BtnGoWrdSearch_Click"  />
                    </td>
                </tr>
              
                  
                </table>
            </td>
        </tr>
        </table>
            <cc1:filteredtextboxextender id="FilteredTextBoxExtender1" runat="server" filtertype="Numbers"
                targetcontrolid="txtTaskId"></cc1:filteredtextboxextender><cc1:filteredtextboxextender id="Filteredtextboxextender2" runat="server" filtertype="Numbers"
                targetcontrolid="txtRequestId">
                </cc1:FilteredTextBoxExtender>
        </td>
       </tr>
       </table> 
    </div>
</div>
<asp:Panel ID="Panel1" runat="server" Width="755px" Style="display: none">
<table border="0" cellspacing="0" cellpadding="0" width="100%" style="background-color: #ffffff;float:left">
    <tr>
        <td align="right" style="padding-right:10px;">
            <asp:LinkButton ID="lnkClose" runat="server" style="color:#811007;font-size:16px;font-weight:bolder;text-decoration:underline;">Close</asp:LinkButton>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top">
        <iframe id="frameQuickQuestion" src="quick_question.aspx" height="550"; width="745" frameborder="0" scrolling="no" ></iframe>
        <iframe id="frameCallTaken" src="call_taken.aspx" height="550"; width="745" frameborder="0" scrolling="no" ></iframe>
        </td>
    </tr>
                       
                    </table>
</asp:Panel>
<cc1:ModalPopupExtender 
ID="ModalPopupExtender1"
    runat="server"
    BackgroundCssClass="modalBackground"
    CancelControlID="lnkClose"
    DropShadow="true"
    PopupControlID="Panel1"
    TargetControlID="blnClick">
</cc1:ModalPopupExtender>
<asp:Button ID="blnClick" runat="server" Text="Button" style="display:none;" CausesValidation="false"/>

<script type="text/javascript">
<!--
var MenuBar1 = new Spry.Widget.MenuBar("MenuBar1", {imgDown:"SpryAssets/SpryMenuBarDownHover.gif", imgRight:"SpryAssets/SpryMenuBarRightHover.gif"});
//-->
</script>

