using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.Drawing.Drawing2D;
using System.IO;

public partial class Admin_Header : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["Canned_msg"] == null || Session["Canned_msg"].ToString() == "")
        {
           
        }
        else
        {
            Session.Remove("Canned_msg");
        }
        if (!Page.IsPostBack)
        {
            if (Session["admin"] == null)
            {
                divHeader1.Visible = true;
                lnkUserName.Visible = false;
                divHeader2.Visible = false;
                lblTitle.Text = "Change Request System";
                lblTitle.ForeColor = Color.Red;
            }
            else
            {
                lblTitle.Text = "Change Requests";
                lblTitle.ForeColor = Color.Red;
                divHeader1.Visible = false;
                lnkUserName.Visible = true;
                divHeader2.Visible = true;

                object objUserId = DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + Session["admin"].ToString() + "'");
                

                if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
                {
                    lnkUserName.InnerHtml = "Logged in as emp: " + objUserId.ToString();
                    
                    lnkAdmin.Visible = false;
                    
                }
                else
                {
                    lnkUserName.InnerHtml = "Logged in as admin: " + objUserId.ToString();
                    lnkAdmin.Visible = true;
                }

                if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
                {
                    lnkClientRequests.Visible = false;
                    ChkCrs.Visible = false;
                }
                else
                {
                    lnkClientRequests.Visible = true;
                    ChkCrs.Visible = true;
                    
                }

                objUserId = DatabaseHelper.executeScalar("select user_id from users where username = '" + Session["admin"].ToString() + "'");

                lnkSettings.HRef = "settings.aspx?id=" + objUserId.ToString();
            }
        }
    }
    protected void lnkLogout_Click(object sender, EventArgs e)
    {
        Session.Remove("admin");
        Session["message"] = "You have been logged out successfully.";
        Response.Redirect("../confirmation.aspx", false);
    }
    protected void btnGo_Click(object sender, EventArgs e)
    {
        Response.Redirect("edit_request.aspx?reqid=" + txtRequestId.Text.Trim(), false);
    }
    protected void btnGotoTask_Click(object sender, EventArgs e)
    {
        Response.Redirect("edit_task.aspx?id=" + txtTaskId.Text.Trim(), false);
    }
    protected void BtnGoWrdSearch_Click(object sender, EventArgs e)
    {
        if (ChkTask.Checked)
        {
            Session["TaskByWord"] = "true";
        }
        else
        {

            Session["TaskByWord"] = "false";
        }
        if (ChkCrs.Checked)
        {
            Session["CRsByWord"] = "true";
        }
        else
        {
            Session["CRsByWord"] = "false";
        }
        if (ChkTask.Checked == false && ChkCrs.Checked == false)
        {
            Session["TaskByWord"] = "true";

        }
        if (ChkIncludecomment.Checked)
        {
            Session["Includecomment"] = "true";
        }
        else
        {
            Session["Includecomment"] = "false";
        }

        Session["SearchWord"] = txtWord.Text.Trim();
        Response.Redirect("WordSearchResult.aspx", false);
    }

  
    protected void LnkCrntlywrkTask_Click(object sender, EventArgs e)
    {
        Session["filter"] = "CWO";
        Session["filterunread"] = "";
        Response.Redirect("tasks.aspx");

    }
    protected void lnkTasks_Click(object sender, EventArgs e)
    {      
        Session["filter"] = "";
        Session["filter"] = null;
        Session["filterunread"] = "";
        Session["filterunread"] = null;
        Session["Status"] = null;
        Session["Status"] = "";
        Session["HideClosed"] = "False";
        Session["IncludeDeleted"] = "False";
        Session["Project"] = null;
        Session["Category"] = null;
        Session["ReportedBy"] = null;
        Session["Priority"] = null;
        Session["AssignedTo"] = null;
        Session["OrderBy"] = null;
        Session["ReadTask"] = "True";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        Response.Redirect("tasks.aspx", false);

    }

    protected void lnkClientRequests_Click(object sender, EventArgs e)
    {
        Session["filter"] = "";
        Session["filter"] = null;
        Session["reqHideClosed"] = "False"; ;
        Session["reqViewDeleted"] = "False";
        Session["reqChangedBy"] = null;
        Session["reqStatus"] = null;
        Session["reqPriority"] = null;
        Session["reqWebsiteURL"] = null;
        Session["reqOrderBy"] = null;
        Response.Redirect("client_requests.aspx", false);
    }
}
