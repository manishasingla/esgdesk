﻿<%@ Control Language="C#"  AutoEventWireup="true" CodeFile="EmployeeList.ascx.cs" Inherits="LMSAdmin_EmployeeList" %>
 <script type="text/javascript" language="javascript">

     function isNumberKey(evt) {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
             return false;

         return true;
     }
     function isStringKey(evt) {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if ((charCode >= 48 && charCode <= 57))
             return false;

         return true;
     }
   
</script>
    
    <table style="width:81%"  class="publicloginTable">
     <tr>
                        <td colspan="6" class="LoginTitle"><h1 style="color:Black">
                            <asp:Label runat="server" ID="lblPageHeading" ></asp:Label>  </h1>
                        </td>
                          
                           
                    </tr>
    <tr>
    <td valign="top" align="left">
       <table width="100%" class="admintablestyle">
                    
        <tr>
        <td colspan="4" class="LoginTitle"></td>
        </tr>
                    
        <tr>
        <td style="width: 18%">
            Employee name</td>
        <td align ="center"  style="width: 2%">:</td>
        <td style="width: 18%"><asp:TextBox ID="txtEmpName" runat="server" OnKeypress="return isStringKey(event)"></asp:TextBox></td>
        <td></td>
        <td style="height: 26px"></td>
        <td align ="center" style="height: 26px" ></td>
        <td style="height: 26px"><asp:TextBox ID="txtPageSize" OnKeypress="return isNumberKey(event)" runat="server" Width="50px" visible="false"></asp:TextBox>
        <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtPageSize"
        CssClass="lblerror" ErrorMessage="Invalid Number" MaximumValue="999" MinimumValue="1"
        Style="position: relative" Type="Integer"></asp:RangeValidator></td>
        <td style="height: 26px"><asp:Button ID="btnSearch" runat="server" Text="Search" Width="55px" 
                Style="position: relative; top: 0px; left: 0px;" OnClick="btnSearch_Click" /></td>
        </tr>
                    
       
                    
        <tr>
        <td colspan="8"></td>
        </tr>
        <tr>
        <td colspan="8">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td width="50%">
            &nbsp;</td>
        <td align="right">
            &nbsp;</td>
        </tr>
        
        <tr>
        <td colspan="2">
  
          <asp:GridView ID="gvEmp" runat="server" 
                               AllowSorting="true"  AutoGenerateColumns="False" Width="100%" 
                               OnRowCommand="gvEmp_RowCommand" OnSorting="gvEmp_Sorting" 
                               OnPageIndexChanging="gvEmp_PageIndexChanging" 
                               onselectedindexchanged="gvEmp_SelectedIndexChanged" 
                               onrowdatabound="gvEmp_RowDataBound" PageSize="20">
                                <Columns> 

                              <asp:TemplateField HeaderText="User name" SortExpression="UserName" Visible="false">
                               <ItemTemplate>
                               <asp:Label runat="server" ID="lblUserName" Text='<%#Eval("username")%>'></asp:Label>                           
                               </ItemTemplate>
                              </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Employee name" SortExpression="EmpName">
                                               <HeaderStyle Width="35%" />
                                                    <ItemTemplate>                                                                                         
                                                      <asp:LinkButton ID="lnkBtnCountryName" Text='<%#Eval("EmpName")%>' runat="server"  CommandArgument= '<%#string.Format("{0}|{1}|{2}",Eval("UserName"),Eval("user_id"),Eval("EmpName"))%>' CommandName="Edit"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Email address">
                                                    <ItemTemplate>
                                                        <%#Eval("Email")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Contact number" Visible="false">
                                    <ItemTemplate>
                                    <%#Eval("ContactNo")%>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Total Balance">
                                    <ItemTemplate>
                                    <asp:Label ID="lblTotalBalance" runat="server"  CommandName="Update"></asp:Label>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
        </td>
        </tr>
                               
        <tr>
        <td width="45%">
        <asp:Label ID="lblError" runat="server"  Visible="false" CssClass="lblerror"></asp:Label></td>
        <td align="right" >
        <asp:Label ID="Lbl_Pageinfo" runat="server" Style="position: relative"></asp:Label></td>
        </tr>
        </table>
        </td>
        </tr>
        
        <tr>
        <td colspan="4"></td>
        </tr>
        </table>
        </td>
        </tr>
    </table>