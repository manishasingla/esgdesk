﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class LMSAdmin_EmployeeList : System.Web.UI.UserControl
{
    clsEmployee_Logic1 objEmployee = new clsEmployee_Logic1();
    private string strError = "No Data Available";
    DataView dv = null;
    clsLeave_Logic objLeave = new clsLeave_Logic();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fnSetPageTitle();
            BindData();
        }
       
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            strError = "No data matching with your searching criteria";
            gvEmp.PageIndex = 0;
            ViewState["EN"] = txtEmpName.Text;
            BindData();
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "EmployeeList.ascx.cs : btnSearch_Click event");
        }

    }


    void BindData()
    {
        try{
        objEmployee.EmpName = txtEmpName.Text.Trim();
        objEmployee.Sort_On = "";
        if (ViewState["Sort_On"] != null)
           objEmployee.Sort_On = ViewState["Sort_On"].ToString() + " " + ViewState["Sort_By"].ToString();
        lblError.Visible = false;
      
        DataSet dsTemp = objEmployee.GetEmployee();
        if (dsTemp != null)
        {
            DataTable dtTemp = dsTemp.Tables[0];

            dv = new DataView(dtTemp);
            dv.Sort = objEmployee.Sort_On;

            if (ViewState["EN"] != null && ViewState["EN"].ToString().Trim() != "")
            {
                string strFilter = "EmpName Like'%" + ViewState["EN"].ToString() + "%'";
                dv.RowFilter = strFilter;
            }

            if (dv.ToTable().Rows.Count > 0)
            {
                lblError.Visible = false;
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = strError;

            }
            if (this.txtPageSize.Text != "")
            {
                if (System.Convert.ToInt32(this.txtPageSize.Text) > 0)
                {
                    this.gvEmp.PageSize = System.Convert.ToInt32(this.txtPageSize.Text);
                }
            }
            else
            {
                gvEmp.PageSize = dsTemp.Tables[0].Rows.Count;
            }
            gvEmp.DataSource = dv;
            gvEmp.DataBind();
            if (dv.ToTable().Rows.Count == 0)
            {
                this.Lbl_Pageinfo.Visible = false;
            }
            else
            {
                Int16 intTo;
                Int16 intFrom;
                if (gvEmp.PageSize * (gvEmp.PageIndex + 1) < dv.ToTable().Rows.Count)
                {
                    intTo = System.Convert.ToInt16(gvEmp.PageSize * (gvEmp.PageIndex + 1));
                }
                else
                {
                    intTo = System.Convert.ToInt16(dv.ToTable().Rows.Count);
                }
                intFrom = System.Convert.ToInt16((gvEmp.PageSize * gvEmp.PageIndex) + 1);
                this.Lbl_Pageinfo.Text = "Record(s) " + intFrom + " to " + intTo + " of " + dv.ToTable().Rows.Count;
                this.Lbl_Pageinfo.Visible = true;
            }
        }
    }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "EmployeeList.ascx.cs : BindData Method");
        }
    }


    protected void gvEmp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            if (ViewState["Sort_On"] != null)
                objEmployee.Sort_On = ViewState["Sort_On"].ToString();
            else
                objEmployee.Sort_On = "";
            gvEmp.PageIndex = e.NewPageIndex;
            BindData();
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "EmployeeList.ascx.cs : gvEmp_PageIndexChanging event");
        }
    }


    protected void gvEmp_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            string sortByAndArrangeBy = (e.CommandArgument).ToString();

            char[] separator = { '|' };

            string[] sortByAndArrangeByArray = sortByAndArrangeBy.Split(separator);

            if (Request["id"].ToString() == "EL")
            {

                Response.Redirect("frmLeaveEditByEmp.aspx?id=" + sortByAndArrangeByArray[0] + "&EId=" + sortByAndArrangeByArray[1] + "&EN=" + sortByAndArrangeByArray[2]);
            }

            else if (Request["id"].ToString() == "VL")
            {

                Response.Redirect("frmEmpBalanceLeaveDetails.aspx?UserName=" + sortByAndArrangeByArray[0] + "&EN=" + sortByAndArrangeByArray[2]);
            }

            else if (Request["id"].ToString() == "AL")
            {

                Response.Redirect("frmLeaveAddByEmp.aspx?id=" + sortByAndArrangeByArray[0] + "&EId=" + sortByAndArrangeByArray[1] + "&EN=" + sortByAndArrangeByArray[2]);

            }
        }
        catch (Exception ex)
        {
           // DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "EmployeeList.ascx.cs : gvEmp_RowCommand event");
        }

            
    }

    protected void gvEmp_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gvEmp_Sorting(object sender, GridViewSortEventArgs e)
    {
        objEmployee.Sort_On = e.SortExpression;
        ViewState["Sort_On"] = objEmployee.Sort_On;
        if (ViewState["Sort_By"] == null)
            ViewState["Sort_By"] = "Asc";
        if (ViewState["Sort_By"].ToString() == "Asc")
        {
            ViewState["Sort_By"] = "Desc";
        }
        else
        {
            ViewState["Sort_By"] = "Asc";
        }

        BindData();
    }

    protected void gvEmp_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            clsLeave_Logic objLeave = new clsLeave_Logic();
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblUSerNamel = ((Label)e.Row.FindControl("lblUserName"));
                double dblUsed = 0.0;
                string strTotal = "";
                double dblTotal = 0.0;
                double dblTemp = 0.0;
                objLeave.UserName = lblUSerNamel.Text;
                objLeave.GetTotalBalanceDaysInOne(DateTime.Now.Year);
                // returns remaining balance and set seperately each category's used leaves
                dblUsed = objLeave.CasualBalanceLeaves + objLeave.MedicalBalanceLeaves + objLeave.EarnBalanceLeaves + objLeave.HalfPaidBalanceLeaves;

                strTotal = objLeave.fnGetTotalOfAllotedLeavesByEmp(DateTime.Now.Year);//fnGetTotalOfAllotedLeavesByEmp() Return total of leaves alloted to employee.

                dblTotal = Convert.ToDouble(strTotal);
                // e.Row.Cells[4].Text = objLeave.fnGetTotalOfBalanceLeave(DateTime.Now.Year) + " / " + strTotal;//fnGetTotalOfBalanceLeave() return total of leaves taken by employee.
                if (objLeave.ExtraBalanceLeaves == 0.0)
                    dblUsed = objLeave.CasualBalanceLeaves + objLeave.MedicalBalanceLeaves + objLeave.EarnBalanceLeaves + objLeave.HalfPaidBalanceLeaves;
                else
                    if (dblUsed <= dblTotal)
                    {
                        dblTemp = dblTotal - dblUsed;
                        dblUsed = (dblTemp - objLeave.ExtraBalanceLeaves);
                    }
                    else
                    {
                        dblUsed = -(objLeave.ExtraBalanceLeaves);
                    }

                e.Row.Cells[4].Text = dblUsed + " / " + strTotal;//fnGetTotalOfBalanceLeave() return total of leaves taken by employee.

            }
        }
        catch (Exception ex)
        {
          //  DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "EmployeeList.ascx.cs : gvEmp_RowDataBound event");
        }

    }

    public void fnSetPageTitle()
    {
        try
        {
            if (Request["id"].ToString() == "EL")
                lblPageHeading.Text = "Employee list to Edit leave";
            if (Request["id"].ToString() == "VL")
                lblPageHeading.Text = "Employee list to View leave";
            if (Request["id"].ToString() == "AL")
                lblPageHeading.Text = "Employee list to Add leave";
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "fnSetPageTitle()");
        }
    }

}