<%@ Page Language="C#" MasterPageFile="~/Admin/LMSAdmin/AdminMasterMenu.master" AutoEventWireup="true" CodeFile="LMS-Complete-leaves-report.aspx.cs" Inherits="Admin_frmEmpLeavesReport" Title="Leave report" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  <script type="text/javascript" language="javascript">
function toggleAllCheckboexs(toggle)
{
				n = document.forms[0].length;
				var frm = document.forms[0];
				for(i=0;i<frm.length;i++)
				
					if(frm.elements[i].type=="checkbox")
						if (frm.elements[i].name.indexOf('Cbx')==0)
							frm.elements[i].checked=toggle;
			}
			
			
			function onlyNumbers(evt)
{
    var e = event || evt;
    var charCode = e.which || e.keyCode;
	if ((charCode > 47 && charCode < 58))
    return true;		             
    else
    return false ;		                            
}
			
			function onlyNumbers(evt)
{
    var e = event || evt;
    var charCode = e.which || e.keyCode;
	if ((charCode > 47 && charCode < 58))
    return true;		             
    else
    return false ;		                            
}
</script>
  <script language="javascript" type="text/javascript">
    function hideCalendar() {
        $find("<%=CalendarExtender1.ClientID%>").hide();
        $find("<%=CalendarExtender2.ClientID%>").hide();
        return false;
    } 
</script>
  <style>
      
<!--  .leavetbl { border-collapse: collapse; } -->  
      
.leavetbl td{ border:1px solid #999999; padding:3px;}

</style>
  
  <!--<table style="width:81%"  class="publicloginTable">
    <tr>
      <td colspan="4" class="LoginTitle"><h1 style="color:Black"> Employee Leave Report</h1></td>
    </tr>
    
      <td valign="top" align="left"><table width="100%" class="admintablestyle" style="height: 578px">
          <tr>
            <td colspan="4" class="LoginTitle">Manage Employee</td>
          </tr>
          <tr>
            <td colspan="8"></td>
          </tr>
          <tr>
            <td></td>
          </tr>
          <tr>
            <td colspan="8"><table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                <tr>
                  <td colspan="4">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="0" style="width: 20%; height:100%; vertical-align:top"><asp:Label ID="Label1" runat="server" style="valign:top">Select an employee </asp:Label></td>
                  <td style="vertical-align:top; width:40%"></td>
                  <td style="vertical-align:top; width:20%"><asp:Panel ID="pnlEmpEmailSend" runat="server" Visible="true" >
                      <table>
                        <tr>
                          <td style="width:50%"><asp:Label ID="lblEmpName1" runat="server" style="valign:top">Select first emp :</asp:Label></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td colspan="2"></td>
                        </tr>
                        <tr>
                          <td style="width:50%"><asp:Label ID="lblEmpName2" runat="server" style="valign:top">Select second emp :</asp:Label></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td colspan="2" align="right"></td>
                        </tr>
                        <tr>
                          <td colspan="2"></td>
                        </tr>
                      </table>
                      </asp:Panel>
                    <table>
                      <tr>
                        <td style="width: 17%"> Contact number </td>
                        <td style="width: 2%"> : </td>
                        <td style="width: 161px"></td>
                      </tr>
                      <tr>
                        <td style="width: 17%"> Email address </td>
                        <td style="width: 2%"> : </td>
                        <td style="width: 161px"></td>
                      </tr>
                      <tr>
                        <td style="width: 17%"> Address </td>
                        <td style="width: 2%"> : </td>
                        <td style="width: 161px"></td>
                      </tr>
                      <tr>
                        <td> Designation </td>
                        <td> : </td>
                        <td style="width: 161px"></td>
                      </tr>
                      <tr>
                        <td> CL balance</td>
                        <td> : </td>
                        <td style="width: 161px"></td>
                      </tr>
                      <tr>
                        <td> ML balance</td>
                        <td> : </td>
                        <td style="width: 161px"></td>
                      </tr>
                      <tr style="display:none" >
                        <td> EL balance</td>
                        <td> : </td>
                        <td style="width: 161px"></td>
                      </tr>
                      <tr style="display:none">
                        <td> HPL balance</td>
                        <td> : </td>
                        <td style="width: 161px"></td>
                      </tr>
                      <tr>
                        <td> Extra Leaves</td>
                        <td> : </td>
                        <td style="width: 161px"></td>
                      </tr>
                      <tr>
                        <td> Balance </td>
                        <td> : </td>
                        <td style="width: 161px"></td>
                      </tr>
                    </table></td>
                </tr>
                <tr>
                  <td style="width: 25%"><asp:Label ID="lblError" runat="server"  Visible="false" CssClass="lblerror"></asp:Label>
                    <asp:Label ID="Lbl_Pageinfo" runat="server" Style=""></asp:Label></td>
                  <td align="left" >&nbsp;</td>
                  <td></td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td colspan="4"></td>
          </tr>
        </table></td>
    </tr>
  </table>-->
  
  <div class="tab-content-holder">
    <h2 style="font-weight: normal !important; font-size: 20px !important; margin-top: 5px; margin-bottom: -5px; padding-left: 20px;">Leave Management System: Employees complete leave reports</h2>
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td width="11%" align="left" valign="top">
        <div class="treeview_div_left" style="height:1397px;">
            <ul id="treemenu2" class="treeview" style="padding-left: 10px;">
              <li><a href="../LMSAdmin/LMS-Admin.aspx">LMS home</a></li>
              <li><a href="#">Manage</a>
                <ul>
                  <li><a href="../LMSAdmin/LMS-Leave-setting.aspx" class="settings">Annum</a></li>
                  <li><a href="../LMSAdmin/LMS-Add-a-leave.aspx" class="edit_leave">Emps</a></li>
                </ul>
              </li>
              <li><a href="#">Reports</a>
                <ul>
                  <li><a href="../LMSAdmin/LMS-Leaves-between-2dates-report.aspx" class="documents">All</a></li>
                  <li><a href="../LMSAdmin/LMS-Complete-leaves-report.aspx" class="leave_report">Detail</a></li>
                  <li><a href="../LMSAdmin/LMS-In-and-outtime-report.aspx"class="leave_report">In/outtime</a></li>
                  <li><a href="../LMSAdmin/work-from-home-report.aspx" class="leave_report">Work from home</a></li>
                </ul>
              </li>
            </ul>
          </div></td>
        <td width="78%" align="left" valign="top">
        <table width="99%" border="0" align="left" cellpadding="0" cellspacing="0" style="margin:0px;">
            <tr>
              <td width="99%" valign="top">
              	<div style="line-height:18px;">
                  <div>
                    <fieldset  style="height: auto; margin-top:13px; margin-left:0px; margin-top:3px\9;">
                      <legend style="padding-left:0px;">
                      <asp:DropDownList ID="ddlEmpList" runat="server" style=" border:1px solid #cccccc; padding:2px;" AutoPostBack="True" ValidationGroup="g2" CausesValidation="true" Height="25px" onselectedindexchanged="ddlEmpList_SelectedIndexChanged" Width="147px"> </asp:DropDownList>
                      </legend>
                      <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin:0px; padding-top:10px\9;">
                        <tr>
                          <td width="10%" valign="top">
                          <table width="98%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td><img runat="server" id="EmpImage" style="margin: 0px 0 0 0px; cursor: pointer; padding:4px; border:1px solid #999; border-radius:2px; background:#fff;" width="142" height="146" /></td>
                              </tr>
                              <!--<tr>
                              <td style="height:9px;"></td>
                              </tr>-->
                              
                              <tr>
                                <td><fieldset style="margin:0px; padding-top:0px; width:170px;">
                                    <legend style="font-size: 16px; padding: 0 2px;"><strong>Send email</strong></legend>
                                    <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
                                      <tr>
                                        <td height="25" align="left" valign="middle">Select first emp</td>
                                      </tr>
                                      <tr>
                                        <td height="25" align="left" valign="middle"><asp:DropDownList ID="ddlEmpList1" runat="server" style=" border:1px solid #cccccc; padding:2px;"
                    onselectedindexchanged="ddlEmpList1_SelectedIndexChanged"> </asp:DropDownList></td>
                                      </tr>
                                      <tr>
                                        <td height="25" align="left" valign="middle">Select second emp</td>
                                      </tr>
                                      <tr>
                                        <td height="25" align="left" valign="middle"><asp:DropDownList ID="ddlEmpList2" runat="server" style=" border:1px solid #cccccc; padding:2px;"
                    onselectedindexchanged="ddlEmpList2_SelectedIndexChanged"> </asp:DropDownList></td>
                                      </tr>
                                      <tr>
                                        <td height="40" align="left" valign="middle"><asp:Button ID="btnSendEmail" runat="server" onclick="btnSendEmail_Click"
                                              ValidationGroup="g3"
                                                CssClass="blueBtns" CausesValidation= "true" Text="Send Email" /></td>
                                      </tr>
                                      <tr>
                                        <td><asp:Label ID="lblEmailReport" runat="server"></asp:Label></td>
                                      </tr>
                                    </table>
                                    </fieldset></td>
                              </tr>
                              
                            </table></td>

                
                         <td width="90%" valign="top">
                         <div style="padding-left: 10px;">
                       <table cellpadding="0" cellspacing="0" border="0" width="99%">
                      <tr>
                                  <td width="300px">Starting date&nbsp;&nbsp;<asp:TextBox ID="GMDStartDate" runat="server" style="height:20px; border:1px solid #cccccc; width:95px; text-align:left; padding-left:4px;"  onkeypress="javascript:EnterEvent(event)" ValidationGroup="g1" CausesValidation="true"></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" OnClientDateSelectionChanged='hideCalendar'
                            TargetControlID="GMDStartDate"
                            Format="dd/MM/yyyy" > </ajaxToolkit:CalendarExtender>&nbsp;&nbsp;&nbsp;&nbsp;Ending date&nbsp;&nbsp;<asp:TextBox ID="GMDEndDate" runat="server" style="height:20px; border:1px solid #cccccc; width:95px; text-align:left; padding-left:4px;" ValidationGroup="g1" CausesValidation="true"
                               onkeypress="javascript:EnterEvent(event)" AutoPostBack="True" 
                               ontextchanged="GMDEndDate_TextChanged" ></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" OnClientDateSelectionChanged='hideCalendar'
                            TargetControlID="GMDEndDate"
                            Format="dd/MM/yyyy"> </ajaxToolkit:CalendarExtender></td>
                                  <td width="100px" align="right" style="font-size:14px;">Totals :
                                    <asp:Label ID="lblLeaveBWdates" runat="server" style="valign:top"/></td>
                                </tr>
                      </table>
                          	
                             <table width="100%" border="0" cellspacing="0" cellpadding="0">
                             <%--<tr>
                                  <td width="10%">Starting date</td>
                                  <td width="20%"><asp:TextBox ID="GMDStartDate" runat="server" style="height:20px; border:1px solid #cccccc;"  onkeypress="javascript:EnterEvent(event)" ValidationGroup="g1" CausesValidation="true"></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" OnClientDateSelectionChanged='hideCalendar'
                            TargetControlID="GMDStartDate"
                            Format="dd/MM/yyyy" > </ajaxToolkit:CalendarExtender></td>
                                  <td width="10%">Ending date</td>
                                  <td><asp:TextBox ID="GMDEndDate" runat="server" style="height:20px; border:1px solid #cccccc;" ValidationGroup="g1" CausesValidation="true"
                               onkeypress="javascript:EnterEvent(event)" AutoPostBack="True" 
                               ontextchanged="GMDEndDate_TextChanged" ></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" OnClientDateSelectionChanged='hideCalendar'
                            TargetControlID="GMDEndDate"
                            Format="dd/MM/yyyy"> </ajaxToolkit:CalendarExtender></td>
                                  <td width="50%" align="right" style="font-size:14px;">Totals :
                                    <asp:Label ID="lblLeaveBWdates" runat="server" style="valign:top"/></td>
                                </tr>--%>

                                <tr>
                                <td>
                                
                                 <div>
                           <fieldset>
                                    <legend  style="font-size: 16px; padding: 0 2px;"><strong>Custom</strong></legend>
                                    <table border="0" align="left" cellpadding="0" cellspacing="0">
                                      <tr>
                                        <td height="25" width="15%" style="font-weight:bold;">Contact number</td>
                                        <td height="25" style="font-weight:bold;">Email address</td>
                                        <td height="25" style="font-weight:bold;">Address</td>
                                        <td height="25" style="font-weight:bold;">Designation</td>
                                         <td height="25" style="font-weight:bold;">CL</td>
                                         <td height="25" style="font-weight:bold;">ML</td>
                                         <td height="25" style="font-weight:bold;">Extra leaves</td>
                                          <td height="25" style="font-weight:bold;">Balance</td>
                                      </tr>
                                      <tr>
                                 <td height="25"><asp:Label ID="txtContactNo" runat="server" style="
                                 position:relative; top:-10px; width:140px;" ReadOnly="True" Text="Not found"></asp:Label></td>
                                 
                                 <td height="25" style="line-height:0px; margin-left:5px; width:190px;"><asp:Label ID="txtEmailId" runat="server"  ReadOnly="True" Text="Not found" 
                                 BorderColor="#999999" BorderWidth="0px" Height="25px" ></asp:Label></td>
                                 
                                <td height="25" style="line-height:30px; margin-left:5px; width:110px;"><asp:Label ID="txtAddress" TextMode="MultiLine" runat="server" ReadOnly="True"
                                 Height="49px" BorderColor="#999999" BorderWidth="0px" Text="Not found" ></asp:Label></td>
                                
                                <td height="25" style="line-height:2px; margin-left:5px; width:120px;"><asp:Label ID="txtDesig" Text="Not found" runat="server" ReadOnly="True"  Height="25px"
                                BorderColor="#999999" BorderWidth="0px" ></asp:Label></td>
                                
                                <td height="25" style="margin-left:5px; width:69px; vertical-align:top; padding-top:4px;"><asp:Label ID="txtCLBal" Text="0.0" runat="server" ReadOnly="True" 
                               BorderColor="#999999" BorderWidth="0px" ></asp:Label></td>
                               
                               <td height="25"  style="vertical-align:top; padding-top:4px; width:53px; margin-left:5px;"><asp:Label ID="txtMLBal" Text="0.0" runat="server"  ReadOnly="True" 
                               BorderColor="#999999" BorderWidth="0px" ></asp:Label></td>
                               
                               <td height="25"  style="vertical-align:top; padding-top:4px; margin-left:5px; width:110px;"><asp:Label ID="txtExtraLeaves" Text="0.0" runat="server"  ReadOnly="True"  
                                BorderColor="#999999" BorderWidth="0px" ></asp:Label></td>
                                
                                <td height="25" style="vertical-align:top; padding-top:4px; margin-left:5px; width:140px;"><asp:Label ID="txtTotalBalance" runat="server"  ReadOnly="True" 
                                BorderColor="#999999" BorderWidth="0px" ></asp:Label></td>
                                </tr>
                                      
                                       
                                     
                                    
                               </table>
                               </fieldset>
                               </div>
                               
                               
                               </td>
                              </tr>
                              </table>
                             
                              <fieldset style="height: auto; margin-top:17px;">
                                <legend style="font-size: 16px; padding:0px 2px 4px 0px;"><strong>Employee leave details for current and previous year </strong></legend>
                                <div style=" height:auto;">
                                  <asp:Panel ID="pnlLeaveDetails" runat="server"  Direction="LeftToRight"   Height="100%" HorizontalAlign="Center" Width="100%" style="valign:top; line-height:25px"  Visible="true">
                                    <asp:Label ID="lblLeaveDetails" runat="server" style="valign:top" ></asp:Label>
                                    </asp:Panel>
                                </div>
                               
                              </fieldset>
                            </div></td>
                          </tr>
                       
                      </table>
                    </fieldset>
                  </div>
                </div></td>
            </tr>
          </table></td>
      </tr>
    </table>
    <script type="text/javascript">
            ddtreemenu.createTree("treemenu2", true, 5)

        </script>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableClientScript="true" ShowMessageBox="true" ShowSummary="false" ValidationGroup="g1"  DisplayMode="BulletList"  />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="g1"
                                Display="None" ErrorMessage="Please select a start date" ControlToValidate="GMDStartDate">* </asp:RequiredFieldValidator>
    
    <asp:CompareValidator ID="CompareValidator2" runat="server" Display="None"  ValidationGroup="g1"
                            ErrorMessage="Please enter a valid  date" Operator="DataTypeCheck" 
                            ControlToValidate="GMDStartDate" Type="Date">*</asp:CompareValidator>
    
    <asp:CompareValidator ID="CompareValidator3" runat="server" Display="None"   ValidationGroup="g1"
                            ErrorMessage="Please enter a valid date" Operator="DataTypeCheck" 
                            ControlToValidate="GMDEndDate" Type="Date">*</asp:CompareValidator>
   
    <asp:CompareValidator ID="CompareValidator1" runat="server" Display="None"  ValidationGroup="g1"
                                ErrorMessage="End date should be greater or equal to start date." 
                                ControlToCompare="GMDStartDate" ControlToValidate="GMDEndDate" 
                                Operator="GreaterThanEqual" Type="Date">*</asp:CompareValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please select an employee" 
                     ControlToValidate="ddlEmpList" ValidationGroup="g2" initialvalue="-----Select-----" Display="None" >*</asp:RequiredFieldValidator>
    <asp:ValidationSummary ID="ValidationSummary2" runat="server" EnableClientScript="true" ShowMessageBox="true" ShowSummary="false" ValidationGroup="g2"  DisplayMode="BulletList"  />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please select an employee" 
                     ControlToValidate="ddlEmpList1" ValidationGroup="g3" initialvalue="-----Select-----" Display="None" >*</asp:RequiredFieldValidator>
    <asp:ValidationSummary ID="ValidationSummary3" runat="server" EnableClientScript="true" ShowMessageBox="true" ShowSummary="false" ValidationGroup="g3"  DisplayMode="BulletList"  />
  </div>
</asp:Content>
