using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;

public partial class Admin_frmEmpLeavesReport : System.Web.UI.Page
{
    clsEmployee_Logic1 objEmployee = new clsEmployee_Logic1();
    clsLeave_Logic objLeave = new clsLeave_Logic();
    private string strError = "No records found";
    public static string strHeader = "";
    DataView dv = null;

    string strSUserName = "";
    string strSEmpName = "";
    string strSEmpName1 = "";
    string strSEmailId1 = "";
    string strSEmpName2 = "";
    string strSEmailId2 = "";

    string lstrReason = "";
    string dtSDate;
    string dtEDate;
    DateTime datetime;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["admin"] != null && Session["admin"].ToString() != "")
            {

                if (DatabaseHelper.isAdmin(Session["admin"].ToString()) == false)
                {
                    Response.Redirect("~/Admin/login.aspx");
                    return;
                }
            }
            if (!Page.IsPostBack)
            {
                //this.txtPageSize.Text = "20";
                fnSetDates();
                BindData();
                BindData4EmpDetails();
                if (ddlEmpList.SelectedItem != null)
                {
                    ViewState["EN"] = ddlEmpList.SelectedItem.Text;
                    strSUserName = ddlEmpList.SelectedValue.ToString();
                    ViewState["UserName"] = strSUserName;
                    EmpImage.Src = "~/Admin/" + DatabaseHelper.fnSetImgURL(strSUserName);
                }
                lblLeaveDetails.Text = GenerateHTML();
                fnGetTotalLeavesBWDates();
            }
        }
        catch
        {
        }
    }

    void BindData()
    {
        try
        {
            // objEmployee.EmpName = txtEmpName.Text.Trim();
            objEmployee.Sort_On = "";
            if (ViewState["Sort_On"] != null)
                objEmployee.Sort_On = ViewState["Sort_On"].ToString() + " " + ViewState["Sort_By"].ToString();
            lblError.Visible = false;
            DataSet dsTemp = objEmployee.GetEmployee();
            // DataRow dr = dsTemp.Tables[0].NewRow();
            //  dr[0] = 0; dr[1] = 0; dr[2] = 0; dr[3] = 0; dr[4] = "0"; dr[5] = "-----Select-----";

            //  dsTemp.Tables[0].Rows.InsertAt(dr,0);
            if (dsTemp != null)
            {
                if (dsTemp.Tables.Count > 0)
                {
                    DataTable dtTemp = dsTemp.Tables[0];

                    ddlEmpList1.DataTextField = "EmpName";
                    ddlEmpList1.DataValueField = "email";
                    ddlEmpList1.DataSource = dsTemp.Tables[0];

                   
                    ddlEmpList1.DataBind();
                    ddlEmpList1.Items.Insert(0, "-----Select-----");
                    ddlEmpList1.SelectedIndex = 0;

                    ddlEmpList2.DataTextField = "EmpName";
                    ddlEmpList2.DataValueField = "email";
                    ddlEmpList2.DataSource = dsTemp.Tables[0];

                    
                    ddlEmpList2.DataBind();
                    ddlEmpList2.Items.Insert(0, "-----Select-----");
                    ddlEmpList2.SelectedIndex = 0;


                    ddlEmpList.DataTextField = "EmpName";
                    ddlEmpList.DataValueField = "username";
                    ddlEmpList.DataSource = dsTemp.Tables[0];
                    ddlEmpList.SelectedIndex = 0;
                    ddlEmpList.DataBind();
                    ddlEmpList.Items.Insert(0, "-----Select-----");

                    dv = new DataView(dtTemp);

                    dv.Sort = objEmployee.Sort_On;

                    dv = new DataView(dtTemp);

                    dv.Sort = objEmployee.Sort_On;

                    string strFilter = "EmpName Not Like'%" + "-----Select-----" + "%'";
                    dv.RowFilter = strFilter;
                    if (ViewState["EN"] != null && ViewState["EN"].ToString().Trim() != "")
                    {
                        strSEmpName = ViewState["EN"].ToString();
                        strFilter = "EmpName Like'%" + strSEmpName + "%'";
                        dv.RowFilter = strFilter;
                    }

                    if (dv.ToTable().Rows.Count > 0)
                    {
                        lblError.Visible = false;
                        //btnDelete.Visible = true;
                    }
                    else
                    {
                        lblError.Visible = true;
                        lblError.Text = strError;
                        //  btnDelete.Visible = false;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Complete-leaves-report : BindData()");
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }

    }

    public void fnSetDates()
    {
        DateTime dt1, dt2;
        try
        {
            if (GMDStartDate.Text.Trim() == "")
            {

                dtSDate = FirstDayOfYear(DateTime.Today).ToString("dd/MM/yyyy");
                GMDStartDate.Text = dtSDate;
                dt1 = Convert.ToDateTime(dtSDate);
                dtSDate = dt1.ToString("yyyy/MM/dd");


            }
            else
            {
                dt1 = Convert.ToDateTime(GMDStartDate.Text);
                dtSDate = dt1.ToString("yyyy/MM/dd");
                objLeave.StartingDate = dtSDate;

            }

            if (GMDEndDate.Text.Trim() == "")
            {
                dtEDate = LastDayOfYear(DateTime.Today).ToString("dd/MM/yyyy");
                GMDEndDate.Text = dtEDate;
                dt2 = Convert.ToDateTime(dtEDate);
                dtEDate = dt2.ToString("yyyy/MM/dd");

            }
            else
            {
                dt2 = Convert.ToDateTime(GMDEndDate.Text);
                dtEDate = dt2.ToString("yyyy/MM/dd");

            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Complete-leaves-report : fnSetDates()");
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            dtSDate = GMDStartDate.Text;
            dtEDate = GMDEndDate.Text;

            datetime = Convert.ToDateTime(dtSDate);
            dtSDate = datetime.ToString("yyyy/MM/dd");

            if (GMDEndDate.Text.Trim() != "")
            {
                datetime = Convert.ToDateTime(dtEDate);
                dtEDate = datetime.ToString("yyyy/MM/dd");
            }
            // objLeave.UserName = txtUserName.Text.Trim();
            objLeave.StartingDate = dtSDate;
            objLeave.EndingDate = dtEDate;


            //   gvLeaveDetails.PageIndex = 0;
            // if (txtUserName.Text.Trim() != "")
            BindData();
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Complete-leaves-report : btnSearch_Click event");
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }
    }

    private string GenerateHTML()
    {
        double dblUsedLeaveCurrentYear = 0.0;
        double dblUsedLeavePreviousYear = 0.0;
        double[] dblArrayOfTotalAndUsedLeave = new double[2];
        strSEmpName = ViewState["EN"].ToString();
        strHeader = "";
        string serverpath = ConfigurationManager.AppSettings["WebAddress"];
        //body of mail for the person
        //objEmployee.UserName = Request["id"].ToString();
        DateTime d1 = DateTime.Now;
       
        string dtSDate = FirstDayOfYear(d1).ToString("yyyy-MM-dd");//GMDStartDate.DateString;
        string dtEDate = LastDayOfYear(d1).ToString("yyyy-MM-dd");

        string strSql1 = "select convert(varchar,AbsentDate,103) as AbsentDate ,Reason,PaidOrNPaid , LeaveType from tbl_AbsentDetails where UserName='" + strSUserName + "' and AbsentDate >='" + dtSDate + "'"
                               + " and AbsentDate <='" + dtEDate + "' order by cast(AbsentDate as DateTime) desc ";

        DataSet dsTemp1 = objEmployee.GetAbsentDates(strSql1);
        dblArrayOfTotalAndUsedLeave = objLeave.fnGetTotalAndUsedLeave(strSUserName, d1.Year);
        dblUsedLeaveCurrentYear = dblArrayOfTotalAndUsedLeave[0];

        DateTime d2 = DateTime.Now.AddYears(-1);
        dtSDate = FirstDayOfYear(d2).ToString("yyyy-MM-dd");//GMDStartDate.DateString;
        dtEDate = LastDayOfYear(d2).ToString("yyyy-MM-dd");

        string strSql2 = "select convert(varchar,AbsentDate,103) as AbsentDate ,Reason,PaidOrNPaid ,LeaveType, IsEHPL, IsPlanned, Deductable from tbl_AbsentDetails where UserName='" + strSUserName + "' and AbsentDate >='" + dtSDate + "'"
                            + " and AbsentDate <='" + dtEDate + "' order by cast(AbsentDate as DateTime) desc ";

        DataSet dsTemp2 = objEmployee.GetAbsentDates(strSql2);
        dblArrayOfTotalAndUsedLeave = objLeave.fnGetTotalAndUsedLeave(strSUserName, d2.Year);
        dblUsedLeavePreviousYear = dblArrayOfTotalAndUsedLeave[0];
        DataTable dtTemp1 = null, dtTemp2 = null;


        //body of mail for the person
        try
        {


            strHeader += "<br/>";
            DateTime dt = DateTime.Now;

            //strHeader += "Employee leave details"; //at 07:00 pm on 30-April-2012";

            strHeader += @"<table border=""0"" width=""100%"" cellspacing=""0"" cellpadding=""0"" class=""leavetbl"">";

            strHeader += @"<tr><td colspan=""2""><b><u><strong >" + strSEmpName + "'</strong> leave details are:</u></b></td></tr>";

            strHeader += @"<tr><td colspan=""2""></td></tr>";

            strHeader += @"<tr><td colspan=""2""><strong >Leave details for year :" + d1.Year.ToString() + "(Absent Days : " + dblUsedLeaveCurrentYear.ToString() + " )</strong></td></tr>";

            if (dsTemp1 != null)
            {
                if (dsTemp1.Tables.Count > 0)
                {
                    dtTemp1 = dsTemp1.Tables[0];

                    if (dtTemp1.Rows.Count > 0)
                    {
                        strHeader += @"<tr><td width=""10%""  align=""left""><strong >Date</strong></td><td width=""90%"" align=""left""><strong>Reason</strong></td></tr>";

                        for (int i = 0; i < dtTemp1.Rows.Count; i++) // logic to group  absent employee on same date.
                        {
                            if (dtTemp1.Rows[i]["Reason"].ToString().Trim() != "")
                                lstrReason = dtTemp1.Rows[i]["Reason"].ToString().Replace("@", "'").Trim();
                            strHeader += @"<tr><td  align=""left"">" + dtTemp1.Rows[i]["AbsentDate"].ToString().Trim() + @"</td><td  align=""left"">" + lstrReason + "</td></tr>";

                        }
                    }
                    else
                        strHeader += @"<tr><td colspan=""2""> No leave was taken in this year</td></tr>";

                    strHeader += @"<tr><td colspan=""2""></td></tr>";

                    strHeader += @"<tr><td colspan=""2""><strong >Leave details for year :" + d2.Year.ToString() + "(Absent Days : " + dblUsedLeavePreviousYear.ToString() + " )</strong></td></tr>";
                }
                else
                    strHeader += @"<tr><td colspan=""2""> No leave was taken in this year</td></tr>";
            }
            else
                strHeader += @"<tr><td colspan=""2""> No leave was taken in this year</td></tr>";


            if (dsTemp2 != null)
            {
                if (dsTemp2.Tables.Count > 0)
                {
                    dtTemp2 = dsTemp2.Tables[0];

                    if (dtTemp2.Rows.Count > 0)
                    {
                        strHeader += @"<tr><td width=""10%"" align=""left""><strong> Date</strong></td><td width=""90%"" align=""left""><strong>Reason </strong></td></tr>";

                        for (int i = 0; i < dtTemp2.Rows.Count; i++) // logic to group  absent employee on same date.
                        {
                            if (dtTemp2.Rows[i]["Reason"].ToString().Trim() != "")
                                lstrReason = dtTemp2.Rows[i]["Reason"].ToString().Replace("@", "'").Trim();
                            strHeader += @"<tr><td align=""left"">" + dtTemp2.Rows[i]["AbsentDate"].ToString().Trim() + @"</td><td align=""left"">" + lstrReason + "</td></tr>";

                        }
                    }
                    else
                        strHeader += @"<tr><td colspan=""2"">No leave was taken in this year</td></tr>";
                }
                else
                    strHeader += @"<tr><td colspan=""2""> No leave was taken in this year</td></tr>";
            }
            else
                strHeader += @"<tr><td colspan=""2""> No leave was taken in this year</td></tr>";

            strHeader += "</table>";

            strHeader += "";

            //    strHeader += "------------------";
            //    strHeader += "<br><br>";
            //    strHeader += ConfigurationManager.AppSettings["CompanyName"];
            //    strHeader += "<br>";
            //    //strHeader += ConfigurationManager.AppSettings["WebAddress"];
            //    strHeader += "<a href=" + ConfigurationManager.AppSettings["WebAddress"] + ">";
            //    strHeader += ConfigurationManager.AppSettings["WebAddressToShow"];
            //    strHeader += "</a>";
        }

        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Complete-leaves-report : GenrateHTML()");
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }

        return strHeader;
    }

    static DateTime LastDayOfYear()
    {
        return LastDayOfYear(DateTime.Today);
    }

    static DateTime LastDayOfYear(DateTime d)
    {
        // 1
        // Get first of next year
        DateTime n = new DateTime(d.Year + 1, 1, 1);
        // 2
        // Subtract 1 from it
        return n.AddDays(-1);
    }

    static DateTime FirstDayOfYear()
    {
        return FirstDayOfYear(DateTime.Today);
    }

    static DateTime FirstDayOfYear(DateTime y)
    {
        return new DateTime(y.Year, 1, 1);
    }

    protected void btnSendEmail_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlEmpList.SelectedValue != "-----Select-----")
            {
                if ((ddlEmpList1.SelectedIndex + ddlEmpList2.SelectedIndex) > 0)
                {
                    strSUserName = ViewState["UserName"].ToString();

                    if (ViewState["emp1"] != null)
                    {
                        strSEmpName1 = ViewState["emp1"].ToString();
                    }

                    if (ViewState["email1"] != null)
                    {
                        strSEmailId1 = ViewState["email1"].ToString();
                    }

                    if (ViewState["emp2"] != null)
                    {
                        strSEmpName2 = ViewState["emp2"].ToString();
                    }

                    if (ViewState["email2"] != null)
                    {
                        strSEmailId2 = ViewState["email2"].ToString();
                    }

                    sendEmail(strSEmpName1, strSEmailId1, strSEmpName2, strSEmailId2);
                }
                else
                {
                    lblEmailReport.Text = "Please select an employee from List.";
                }
            }
            else
            {
                lblEmailReport.Text = "Select an employee !";
                ddlEmpList.SelectedIndex = 0;
            }
        }
        catch(Exception ex)
        {
            lblEmailReport.Text = "An error occured please try again .";
            DatabaseHelper.HandleException(ex, "LMS:Complete-Leave-Report", "SendEmail function");
        }
    }

    protected void sendEmail(string strEmpName1, string strEmailId1, string strEmpName2, string strEmailId2)
    {

        try
        {
            string strEmailSucess = "";
            bool blnIsSent1 = false;
            bool blnIsSent2 = false;

            string emailid1 = "", emailid2 = "";

            emailid1 = strEmailId1;
            emailid2 = strEmailId2;
            //emailid2 = System.Configuration.ConfigurationManager.AppSettings["LMS2"].ToString();
            // emailid3 = System.Configuration.ConfigurationManager.AppSettings["LMS3"].ToString();
            string strBody = "";
            strBody = GenerateHTML();

            strSEmpName = ViewState["EN"].ToString();
            string strSubject = "Employee " + strSEmpName + " Leave details";
            blnIsSent1 = DatabaseHelper.sendEmailLMSReport(emailid1, strSubject, strBody);
            blnIsSent2 = DatabaseHelper.sendEmailLMSReport(emailid2, strSubject, strBody);

            strEmailSucess = "Email has been sent to ";
            if (blnIsSent1)
                strEmailSucess += strSEmpName1 + "<br/>";
            if (blnIsSent2)
                strEmailSucess += strSEmpName2 + "<br/>";

            lblEmailReport.Text = strEmailSucess;
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Complete-leaves-report : sendEmail()");
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }
    }

    protected void ddlEmpList2_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            strSEmailId2 = ddlEmpList2.SelectedValue.ToString();
            ViewState["email2"] = strSEmailId2;
            strSEmpName2 = ddlEmpList2.SelectedItem.Text;
            ViewState["emp2"] = strSEmpName2;
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Complete-leaves-report : Event ddlEmpList2_SelectedIndexChanged");
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }
    }

    protected void ddlEmpList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            strSEmailId1 = ddlEmpList1.SelectedValue.ToString();
            ViewState["email1"] = strSEmailId1;
            strSEmpName1 = ddlEmpList1.SelectedItem.Text;
            ViewState["emp1"] = strSEmpName1;
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Complete-leaves-report : Event ddlEmpList1_SelectedIndexChanged");
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }
    }

    protected void ddlEmpList_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            RequiredFieldValidator2.Validate();
            strSUserName = ddlEmpList.SelectedValue.ToString();
            strSEmpName = ddlEmpList.SelectedItem.Text;
            //Response.Redirect("frmLeaveAddByEmp.aspx?Id=" + e.CommandArgument.ToString());
            //string strSql = "select sum(NoOfDays) from tbl_ApplicationData a,tbl_Status s,tbl_LeaveType l where a.ApplicationStatusId=s.StatusId and a.LeaveTypeId=l.LeaveTypeId and a.ApplicationStatusId=1 ";
            //Response.Redirect("frmLeaveAddByEmp.aspx?id=" + sortByAndArrangeByArray[0] +"&EId="+sortByAndArrangeByArray[1]+ "&EN=" + sortByAndArrangeByArray[2]);
            ViewState["UserName"] = strSUserName;

            string strImgUrl  = DatabaseHelper.fnSetImgURL(strSUserName);
            EmpImage.Src = "~/Admin/" + strImgUrl;
            ViewState["EN"] = strSEmpName;
            lblLeaveDetails.Text = GenerateHTML();
            pnlLeaveDetails.Visible = true;
            pnlEmpEmailSend.Visible = true;
            fnGetTotalLeavesBWDates();
            BindData4EmpDetails();
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Complete-leaves-report : Event ddlEmpList_SelectedIndexChanged");
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }
    }


    #region Date Filter

    public void fnGetTotalLeavesBWDates()
    {
        try
        {
            DateTime d = DateTime.Now;

            if (GMDStartDate.Text == null || GMDStartDate.Text.Trim() == "")
            {
                dtSDate = FirstDayOfYear(d).ToString("dd/MM/yyyy");
                GMDStartDate.Text = dtSDate;
                datetime = Convert.ToDateTime(dtSDate);
                dtSDate = datetime.ToString("yyyy/MM/dd");
                objLeave.StartingDate = dtSDate;
            }
            else
            {
                datetime = Convert.ToDateTime(GMDStartDate.Text.Trim());
                dtSDate = datetime.ToString("yyyy/MM/dd");
                objLeave.StartingDate = dtSDate;
            }
            if (GMDEndDate.Text == null || GMDEndDate.Text.Trim() == "")
            {
                dtEDate = LastDayOfYear(d).ToString("dd/MM/yyyy");
                GMDEndDate.Text = dtEDate;
                datetime = Convert.ToDateTime(dtEDate);
                dtEDate = datetime.ToString("yyyy/MM/dd");
                objLeave.EndingDate = dtEDate;
            }
            else
            {
                datetime = Convert.ToDateTime(GMDEndDate.Text.Trim());
                dtEDate = datetime.ToString("yyyy/MM/dd");
                objLeave.EndingDate = dtEDate;
            }

            double dblUsed = 0.0;
            objLeave.UserName = ddlEmpList.SelectedValue.ToString();
            objLeave.GetTotalBalanceDaysInOneBWDate(DateTime.Now.Year); // Return Reamining balance i.e(Alloted- used as Totalbalance and Seperate leaves taken by Emp
            // e.Row.Cells[4].Text = objLeave.fnGetTotalOfBalanceLeave(DateTime.Now.Year) + " / " + strTotal;//fnGetTotalOfBalanceLeave() return total of leaves taken by employee.
            dblUsed = objLeave.CasualBalanceLeaves + objLeave.MedicalBalanceLeaves;

            if (dblUsed != null)
            {
                if (dblUsed.ToString().Trim() != "")
                    lblLeaveBWdates.Text = dblUsed.ToString();
                else
                    lblLeaveBWdates.Text = "0";
            }
            else
            {
                lblLeaveBWdates.Text = "0";
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, "Method : fnGetTotalLeavesBWDates", "");
        }
    }

    #endregion Date Filter

    protected void GMDEndDate_TextChanged(object sender, EventArgs e)
    {
        try
        {
            fnGetTotalLeavesBWDates();
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, "DateChanged", "");
        }
    }

    #region Show employee details

    void BindData4EmpDetails()
    {
        //  string strSortbyOrder;
        try
        {

            objEmployee.UserName = ddlEmpList.SelectedValue.ToString();// Request["id"].ToString();
            objEmployee.GetEmployeeDetails();
            double[] dblUsedAndAlloted = objLeave.fnGetTotalAndUsedLeave(ddlEmpList.SelectedValue.ToString(), DateTime.Now.Year);
            // txtUserName.Text = Request["EN"].ToString();
            txtContactNo.Text = objEmployee.ContactNo;
            txtAddress.Text = objEmployee.Address;
            txtEmailId.Text = objEmployee.EmailId;
            txtDesig.Text = objEmployee.Designation;
            objLeave.UserName = ddlEmpList.SelectedValue.ToString();// Request["id"].ToString();

            objLeave.GetTotalBalanceDaysInOne(DateTime.Now.Year);

            txtCLBal.Text = objLeave.CasualBalanceLeaves.ToString();
            txtMLBal.Text = objLeave.MedicalBalanceLeaves.ToString();
            // txtEL.Text = objLeave.EarnBalanceLeaves.ToString();
            // txtHPLBal.Text = dblHPL.ToString();
            double temp = Convert.ToDouble(dblUsedAndAlloted[1].ToString()); //Convert.ToDouble(objLeave.AllotedLeaves.ToString()) - Convert.ToDouble(dblUsedAndAlloted[0]);
            if (temp < 0)
                txtExtraLeaves.Text = temp.ToString();
            else
                txtExtraLeaves.Text = "0";
            //  objEmployee.EmpId = Convert.ToInt32(Request["EId"]);
            txtTotalBalance.Text = objLeave.UsedLeaves.ToString() + "/" + objLeave.AllotedLeaves.ToString();

            if( txtContactNo.Text.Trim()=="")
                txtContactNo.Text="Not found";

            if(txtAddress.Text.Trim()=="")
            txtAddress.Text = "Not found";

             if(txtEmailId.Text.Trim()=="")
             txtEmailId.Text="Not found";

            if(txtDesig.Text.Trim()=="")
            txtDesig.Text = "Not found";
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Complete-leaves-resport :BindData4EmpDetails()");
        }
    }

    #endregion employee details


}
