<%@ Page Language="C#" MasterPageFile="~/Admin/LMSAdmin/AdminMasterMenu.master" AutoEventWireup="true"
    CodeFile="LMS-In-and-outtime-report.aspx.cs" Inherits="Admin_frmAdminHome" Title="In/out time report" %>
    <%@ Register TagPrefix="uc1" TagName="IOTR" Src="~/Controls/InOutTR.ascx"%>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <div class="tab-content-holder">
        <h2 style="font-weight: normal !important; font-size: 20px !important; margin-top: 5px;
            margin-bottom: -5px; padding-left: 20px;">
            Leave Management System: Employee In/out time</h2>
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td width="11%" align="left" valign="top">
                    <div class="treeview_div_left" style="height: 1140px;">
                        <ul id="treemenu2" class="treeview" style="padding-left: 10px;">
                            <li><a href="../LMSAdmin/LMS-In-and-outtime-report.aspx">LMS home</a></li>
                            <li><a href="#">Manage</a>
                                <ul rel="open" style="display: block;">
                                    <li><a href="../LMSAdmin/LMS-Leave-setting.aspx" class="settings">Annum</a></li>
                                    <li><a href="../LMSAdmin/LMS-Add-a-leave.aspx" class="edit_leave">Emps</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Reports</a>
                                <ul rel="open" style="display: block;">
                                    <li><a href="../LMSAdmin/LMS-Leaves-between-2dates-report.aspx" class="documents">All</a></li>
                                    <li><a href="../LMSAdmin/LMS-Complete-leaves-report.aspx" class="leave_report">Detail</a></li>
                                    <li><a href="../LMSAdmin/LMS-In-and-outtime-report.aspx" class="leave_report">In/outtime</a></li>
                                    <li><a href="../LMSAdmin/work-from-home-report.aspx" class="leave_report">Work from home</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </td>
                <td width="78%" align="left" valign="top">
                    <table width="99%" cellpadding="0" cellspacing="0" border="0" style="margin: 0px;">
                        <tr>
                            <td width="99%" valign="top">
                                <div style="background-color: #f3f3f3; border: 1px solid #999999; margin-top: 25px;">
                                    
                                <uc1:IOTR runat="server" id="inToutT"/>


                            </div>
    </td> </tr> </table></td>
              </tr> </table>
    
  
 

    </div>
</asp:Content>
