using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Globalization;
using System.Xml.Linq;
using System.Collections.ObjectModel;


public partial class Admin_frmAdminHome : System.Web.UI.Page
{
    DateTime dtStartingDate;
    DateTime dtEndingDate;
     
    clsLeave_Logic objLeave = new clsLeave_Logic();
    DataTable dtTemp = null;
    DataTable dtTemp1 = null , dtLR4R1= null, dtLR4R2= null;
    public int DayNo = 0, DayToAdd=0;
    public  bool blnIsDateInterval = false;
    DataView dv2 = new DataView();

    clsEmployee_Logic1 objEmployee = new clsEmployee_Logic1();
    string strError = "No records found";
    List<DateTime> dates = new List<DateTime>();
    public string strreportname = "";
    string dtSDate;
    string dtEDate;
  
    System.Text.StringBuilder sb = new System.Text.StringBuilder();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] != null && Session["admin"].ToString() != "")
        {

            if (DatabaseHelper.isAdmin(Session["admin"].ToString()) == false)
            {
                Response.Redirect("~/Admin/login.aspx");
                return;
            }
        }

        if (!IsPostBack)
        {
            ddlTimeInterval.SelectedIndex = 1;
            fnbinddropdown();
            fnSetDefaultDate();
            fnBindEmpddl();
            Session["TI"] = "TW";
            lblLR4R1Title.Text = "Leave report for this week";
            fnGenerateTimeSheet();

        }
    }

    public void fnBindEmpddl()
    {
        DataSet dsTemp = DatabaseHelper.getDataset(@"select u.FirstName + ' '+u.lastName as EmpName , u.user_id from users u where IsCW='Y' order by EmpName");
        try
        {
            if (dsTemp != null)
            {
                ddlEmp.DataSource = dsTemp;
                ddlEmp.DataTextField = "EmpName";
                ddlEmp.DataValueField = "user_id";
                ddlEmp.DataBind();
                ddlEmp.Items.Insert(0, "--Select--");
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Leaves-between-2dates-report : fnBindddl()");
          //  lblError.Text = ex.Message;
           // lblError.Visible = true;
        }

    }

    protected void ddlTimeInterval_SelectedIndexChanged(object sender, EventArgs e)
    {
        blnIsDateInterval = false;

        if (ddlTimeInterval.SelectedValue.ToString() == "1") //Today
        {
            DayNo = 0;
            Session["TI"] = null;
            lblLR4R1Title.Text = "Leave report for today";
        }

        else if (ddlTimeInterval.SelectedValue.ToString() == "3")//Last 7 days 
        {
            DayNo = -7;
            Session["TI"] = null;
            lblLR4R1Title.Text = "Leave report for last 7 days";
        }


        else if (ddlTimeInterval.SelectedValue.ToString() == "5")// Last 31 days
        {
            DayNo = -31;
            Session["TI"] = null;
            lblLR4R1Title.Text = "Leave report for last 31 days";
        }

        else if (ddlTimeInterval.SelectedValue.ToString() == "4")// This month
        {
            Session["TI"] = "TM";
            lblLR4R1Title.Text = "Leave report for this month";
        }

        else if (ddlTimeInterval.SelectedValue.ToString() == "2") //This Week
        {
            Session["TI"] = "TW";
            lblLR4R1Title.Text = "Leave report for this week";
        }
        fnGenerateTimeSheet();
      
    }

    public void fnSetDefaultDate()
    {

        // lblError.Visible = false;
        String DayName = DateTime.Now.DayOfWeek.ToString();

        if (DayName == "Monday")
        {
            DayNo = 1;
            DayToAdd = 7;
        }
        else if (DayName == "Tuesday")
        {
            DayNo = 2;
            DayToAdd = 6;
        }

        else if (DayName == "Wednesday")
        {
            DayNo = 3;
            DayToAdd = 5;
        }

        else if (DayName == "Thursday")
        {
            DayNo = 4;
            DayToAdd = 4;
        }
        else if (DayName == "Friday")
        {
            DayNo = 5;
            DayToAdd = 3;
        }
        else if (DayName == "Saturday")
        {
            DayNo = 6;
            DayToAdd = 2;
        }

         dtStartingDate = DateTime.Today.AddDays(-DayNo + 1);    // Monday
         dtEndingDate = DateTime.Today.AddDays(6 - DayNo);      //Saturday
         GMDStartDate.Text = dtStartingDate.ToString("yyyy/MM/dd");
         GMDEndDate.Text = dtEndingDate.ToString("yyyy/MM/dd");

    }

    protected void GMDEndDate_TextChanged(object sender, EventArgs e)
    {
        // CompareValidator1.Validate();
        blnIsDateInterval = true;
        // fnBindLR4Reater1();
        dynamicallyCustom();
    }
    protected void ddlEmp_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {

            // RequiredFieldValidator2.Validate();
            string strdate = "";
            fnGenerateTimeSheet();

            if (GMDStartDate.Text.ToString() == "")
            {
                strdate = @"SELECT 
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0),103)   as Date_of_Monday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+1,103) as Date_of_Tuesday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+2,103) as Date_of_Wednesday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+3,103) as Date_of_Thusday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+4,103) as Date_of_Friday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+5,103) as Date_of_Saturday";
            }
            else
            {
                string strDate = Convert.ToDateTime(GMDStartDate.Text).ToString("yyyy/MM/dd");

                strdate = @"SELECT  CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + strDate + "') , 0),103) as Date_of_Monday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + strDate + "') , 0)+1,103) as Date_of_Tuesday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + strDate + "') , 0)+2,103) as Date_of_Wednesday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + strDate + "') , 0)+3,103) as Date_of_Thusday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + strDate + "') , 0)+4,103) as Date_of_Friday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + strDate + "') , 0)+5,103) as Date_of_Saturday";

            }
            DataSet ds = DatabaseHelper.getDataset(strdate);
            
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                if (rdoid.SelectedValue.ToString() == "1")
                {
                    dynamically(ds);
                }
                else
                {
                    dynamicallyCustom();
                }
                //if (ddlEmp.SelectedItem.Text != "ESG") //(ddlEmp.SelectedItem.Text == "PHG")
                //{
                //    strreportname = ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "_To_" + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString();
                //}
                //else
                //{
                    strreportname = ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "_To_" + ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString();
               // }
            }
        }
        catch
        {
        }
    }

    
    void fnGenerateTimeSheet()
    {
       
        string strQuery = "";   
  
        //==========================================
        string strDate="";
         string strchangedate = "";
        DateTime dtstart;
       
             dtstart = Convert.ToDateTime(GMDStartDate.Text); //Ammended by R
             strDate = dtstart.ToString("yyyy/MM/dd"); //Ammended by R
 
        
        strQuery = @"SELECT  CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + strDate + "') , 0),103) as Date_of_Monday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + strDate + "') , 0)+1,103) as Date_of_Tuesday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + strDate + "') , 0)+2,103) as Date_of_Wednesday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + strDate + "') , 0)+3,103) as Date_of_Thusday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + strDate + "') , 0)+4,103) as Date_of_Friday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + strDate + "') , 0)+5,103) as Date_of_Saturday";
        DataSet ds = DatabaseHelper.getDataset(strQuery);
        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            strreportname = ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "_To_" + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString();

            DateTime dt = Convert.ToDateTime(ds.Tables[0].Rows[0]["Date_of_Monday"].ToString()); //DateTime.ParseExact(ds.Tables[0].Rows[0]["Date_of_Monday"].ToString(), "dd/MM/yyyy", null);
            GMDStartDate.Text = dt.ToString("dd/MM/yyyy"); //String.Format("{0:dd/MM/yyyy}", dt);

            //GMDStartDate.Text = Convert.ToDateTime(ds.Tables[0].Rows[0]["Date_of_Monday"]).ToString("dd/MM/yyyy");
            DateTime dt2 = Convert.ToDateTime(ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString());//DateTime.ParseExact(ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString(), "dd/MM/yyyy", null);
            GMDEndDate.Text = dt2.ToString("dd/MM/yyyy"); //String.Format("{0:dd/MM/yyyy}", dt2);

            //GMDEndDate.Text = Convert.ToDateTime(ds.Tables[0].Rows[0]["Date_of_Saturday"]).ToString("dd/MM/yyyy");
            dynamically(ds);
        }
    }


    void fnGenerateTimeSheetFirst()
    {
        string strQuery = "";
        string strDate = Convert.ToDateTime(GMDStartDate.Text).ToString("yyyy/MM/dd");
        strQuery = @"SELECT  CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + strDate + "') , 0),103) as Date_of_Monday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + strDate + "') , 0)+1,103) as Date_of_Tuesday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + strDate + "') , 0)+2,103) as Date_of_Wednesday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + strDate + "') , 0)+3,103) as Date_of_Thusday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + strDate + "') , 0)+4,103) as Date_of_Friday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + strDate + "') , 0)+5,103) as Date_of_Saturday";

        DataSet ds = DatabaseHelper.getDataset(strQuery);
        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            strreportname = ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "_To_" + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString();
            GMDStartDate.Text = Convert.ToDateTime(ds.Tables[0].Rows[0]["Date_of_Monday"]).ToString("dd/MM/yyyy");
            GMDEndDate.Text = Convert.ToDateTime(ds.Tables[0].Rows[0]["Date_of_Saturday"]).ToString("dd/MM/yyyy");

            dynamicallyFirst(ds);
        }
    }
    
    public void dynamically(DataSet ds)
    {
        try
        {
            //System.Text.StringBuilder sb = new System.Text.StringBuilder();
            string strQuery = "";
            DataTable dtgrid = new DataTable();
            dtgrid.Columns.Add("Employee_name");
            dtgrid.Columns.Add("Skills");
            dtgrid.Columns.Add("Date_of_Monday");
            dtgrid.Columns.Add("Date_of_Monday1");
            dtgrid.Columns.Add("Date_of_Monday2");
            dtgrid.Columns.Add("Date_of_Tuesday");
            dtgrid.Columns.Add("Date_of_Tuesday1");
            dtgrid.Columns.Add("Date_of_Tuesday2");
            dtgrid.Columns.Add("Date_of_Wednesday");
            dtgrid.Columns.Add("Date_of_Wednesday1");
            dtgrid.Columns.Add("Date_of_Wednesday2");
            dtgrid.Columns.Add("Date_of_Thusday");
            dtgrid.Columns.Add("Date_of_Thusday1");
            dtgrid.Columns.Add("Date_of_Thusday2");
            dtgrid.Columns.Add("Date_of_Friday");
            dtgrid.Columns.Add("Date_of_Friday1");
            dtgrid.Columns.Add("Date_of_Friday2");
            dtgrid.Columns.Add("Date_of_Saturday");
            dtgrid.Columns.Add("Date_of_Saturday1");
            dtgrid.Columns.Add("Date_of_Saturday2");

          
                HtmlDesign.Visible = true;
                

                strQuery = @"select u.* from users u where IsCW='Y' and u.username not like '%amit%' ";

                    if (ddlEmp.SelectedItem.Text != "--Select--")
                    {
                        strQuery += " and u.user_id='" + ddlEmp.SelectedValue.ToString() + "'";
                    }
                    strQuery += " order by username ";

                    DataSet dsAllEmp = DatabaseHelper.getDataset(strQuery);

                    if (dsAllEmp != null && dsAllEmp.Tables.Count > 0 && dsAllEmp.Tables[0].Rows.Count > 0)
                    {
                        sb.Append("<table width='100%' border='0' cellpadding='0' cellspacing='0'>");
                        sb.Append("<tr>");
                        sb.Append("<td height='10px' colspan='8' align='left' style='font-size:medium'>");
                        sb.Append("<b>Employee attendance report (from ");
                        // sb.Append(ds.Tables[0].Rows[0]["Date_of_Monday"].ToString());
                        sb.Append(ds.Tables[0].Rows[0]["Date_of_Monday"].ToString());
                        sb.Append(" to ");
                        if (ddlEmp.SelectedItem.Text == "PHG")
                        {
                            sb.Append(ds.Tables[0].Rows[0]["Date_of_Friday"].ToString() + ")");
                        }
                        else
                        {
                            sb.Append(ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString() + ")");
                        }
                        sb.Append("</b></td>");
                        sb.Append("</tr>");
                        //sb.Append("<tr>");
                        //sb.Append("<td height='10px'>");
                        //sb.Append("</td>");
                        //sb.Append("</tr>");
                        sb.Append("</table>");
                        sb.Append("<table width='100%' border='0' cellpadding='0' cellspacing='0'>");
                        sb.Append("<tr style='background:#e1e1e1'>");
                        sb.Append("<td height='37px' style='border:1px solid #999'>");
                        sb.Append("<table border='0' width='100%' height='37px' cellpadding='0' cellspacing='0'><tr><td height='45px' rowspan='2' align='center'><b>Employee name</b></td></tr></table>");
						////sb.Append("<br><table border='0' width='100%' height='37px' cellpadding='0' cellspacing='0'  class='leavetbl' style='background:#e1e1e1'><tr><td height='45px' rowspan='2' align='center'><b>Employee name</b></td></tr></table>");
                        sb.Append("</td>");
                        

                        sb.Append("<td style='border:1px solid #999'>");
                        sb.Append("<table border='0' width='100%'height='37px' cellpadding='0' cellspacing='0'><tr><td height='45px' rowspan='2' align='center'><b>Skills</b></td></tr></table>");
						////sb.Append("<br><table border='0' width='100%'height='37px' cellpadding='0' cellspacing='0'  class='leavetbl' style='background:#e1e1e1'><tr><td height='45px' rowspan='2' align='center'><b>Skills</b></td></tr></table>");
                        sb.Append("</td>");
                        ////sb.Append("<td>");
                        

                        ////sb.Append("<br><table width='100%' border='0' cellpadding='0' cellspacing='0'  class='leavetbl' style='background:#e1e1e1'>");
                        ////sb.Append("<tr align='center'>");



                        if (ddlEmp.SelectedItem.Text == "PHG")
                        {
                            sb.Append("<td width='20%' style='border:1px solid #999'>");
                        }
                        else
                        {
                            sb.Append("<td width='17%' height='45px' style='border:1px solid #999'>");
                        }

                        sb.Append("<b>Mon " + ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "</b>");
                        sb.Append("<br><b>In|Out(GMT)|Total</b>");
                        //sb.Append("<table border='0' cellpadding='0' cellspacing='0' align='center'><tr><td align='center'>In/Out (GMT)</td><td> Total </td></tr></table>");
                        sb.Append("</td>");

                        


                        if (ddlEmp.SelectedItem.Text == "PHG")
                        {
                            sb.Append("<td width='20%' style='border:1px solid #999'>");
                        }
                        else
                        {
                            sb.Append("<td width='17%' style='border:1px solid #999'>");
                        }

                        sb.Append("<b>Tue " + ds.Tables[0].Rows[0]["Date_of_Tuesday"].ToString() + "</b>");
                        sb.Append("<br><b>In|Out(GMT)|Total</b>");
                        //sb.Append("<table border='0' cellpadding='0' cellspacing='0' align='center'><tr><td align='center'>In/Out (GMT)</td><td> Total </td></tr></table>");
                        sb.Append("</td>");
                        

                        if (ddlEmp.SelectedItem.Text == "PHG")
                        {
                            sb.Append("<td width='20%' style='border:1px solid #999'>");
                        }
                        else
                        {
                            sb.Append("<td width='17%' style='border:1px solid #999'>");
                        }


                        sb.Append("<b>Wed " + ds.Tables[0].Rows[0]["Date_of_Wednesday"].ToString() + " </b>");
                        sb.Append("<br><b>In|Out(GMT)|Total</b>");
                        //sb.Append("<table border='0' cellpadding='0' cellspacing='0' align='center'><tr><td align='center'>In/Out (GMT)</td><td> Total </td></tr></table>");
                        sb.Append("</td>");
                        

                        if (ddlEmp.SelectedItem.Text == "PHG")
                        {
                            sb.Append("<td width='20%' style='border:1px solid #999'>");
                        }
                        else
                        {
                            sb.Append("<td width='17%' style='border:1px solid #999'>");
                        }


                        sb.Append("<b>Thur " + ds.Tables[0].Rows[0]["Date_of_Thusday"].ToString() + "</b>");
                        sb.Append("<br><b>In|Out(GMT)|Total</b>");
                        //sb.Append("<table border='0' cellpadding='0' cellspacing='0' align='center'><tr><td align='center'>In/Out (GMT)</td><td> Total </td></tr></table>");
                        sb.Append("</td>");
                        

                        if (ddlEmp.SelectedItem.Text == "PHG")
                        {
                            sb.Append("<td width='20%' style='border:1px solid #999'>");
                        }
                        else
                        {
                            sb.Append("<td width='17%' style='border:1px solid #999'>");
                        }

                        sb.Append("<b>Fri " + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString() + "</b>");
                        sb.Append("<br><b>In|Out(GMT)|Total</b>");
                        //sb.Append("<table border='0' cellpadding='0' cellspacing='0' align='center'><tr><td align='center'>In/Out (GMT)</td><td> Total </td></tr></table>");
                        sb.Append("</td>");
                        


                        if (ddlEmp.SelectedItem.Text != "PHG")
                        {
                            sb.Append("<td width='20%' style='border:1px solid #999'>");
                            sb.Append("<b>Sat " + ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString() + "</b>");
                            sb.Append("<br><b>In|Out(GMT)|Total</b>");
                            //sb.Append("<table border='0' cellpadding='0' cellspacing='0' align='center'><tr><td align='center'>In/Out (GMT)</td><td> Total </td></tr></table>");
                            sb.Append("</td>");
                        }
                       

                        ////sb.Append("</tr>");
                        ////sb.Append("</table>");
                        sb.Append("</td>");

                        sb.Append("</tr>");
                        for (int iEmp = 0; iEmp < dsAllEmp.Tables[0].Rows.Count; iEmp++)
                        {
                            if (iEmp % 2 == 0)
                            {
                                sb.Append("<tr style='background:#f7f7f7; height:30px;'>");
                            }
                            else
                            {
                                sb.Append("<tr style='background:#ffffff; height:30px;'>");
                            }
                            sb.Append("<td align='left' style='border:1px solid #999' >");
                            string skills;
                            string EmpName;
                            sb.Append("<table border='0' width='100%' cellpadding='0' cellspacing='0'><tr  style='height:30px'><td>" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "</td></tr></table>");
                            EmpName = dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString();
                            if (dsAllEmp.Tables[0].Rows[iEmp]["skills"].ToString() != "")
                            {
                                sb.Append("</td><td align='left' style='border:1px solid #999'><table border='0' width='100%' height='100%' cellpadding='0' cellspacing='0'><tr style='height:30px'><td >" + dsAllEmp.Tables[0].Rows[iEmp]["skills"].ToString() + "</td></tr></table></td>");
                                skills = dsAllEmp.Tables[0].Rows[iEmp]["skills"].ToString();
                            }
                            else
                            {
                                sb.Append("</td><td align='left' style='border:1px solid #999' ><table border='0' width='100%' height='100%' cellpadding='0' cellspacing='0'><tr  style='height:30px'><td  align='center'> -/ </td></tr></table></td>");
                                skills = "-/";
                            }

                            //******************************************* Create In Time ******************************************************************
                      
                            sb.Append("<td style='border:1px solid #999'>");
                            string monday;
                            string monday1;

                            string strInout1 = "";
                            object objIntimeALL1 = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "'");
                            if (objIntimeALL1 != null && objIntimeALL1.ToString() != "")
                            {

                                DateTime dtALL1 = SUbTimeZones(Convert.ToDateTime(objIntimeALL1).AddHours(-4).AddMinutes(-30));
                                    //Convert.ToDateTime(objIntimeALL1).AddHours(-5).AddMinutes(-30);
                                strInout1 = "<span style='color:green'>"+ dtALL1.ToString("HH:mm").ToString() +"</span>" +"|";
                                monday = dtALL1.ToString("HH:mm").ToString();

                            }
                            else
                            {
                                strInout1 = "-/";
                                monday = "-/";
                            }


                            string tuesday;
                            object objOuttimeALL1 = DatabaseHelper.executeScalar("select Outtime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "'");
                            if (objOuttimeALL1 != null && objOuttimeALL1.ToString() != "")
                            {
                                DateTime dtoutALL1 = SUbTimeZones(Convert.ToDateTime(objOuttimeALL1).AddHours(-4).AddMinutes(-30));
                                    //Convert.ToDateTime(objOuttimeALL1).AddHours(-5).AddMinutes(-30);
                                strInout1 +="<span style='color:#0070c0'>"+ dtoutALL1.ToString("HH:mm")+"</span>";
                                monday1 =  dtoutALL1.ToString("HH:mm");
                            }
                            else
                            {
                                strInout1 += "-";
                                monday1 = "-";
                            }
                            sb.Append(strInout1);


                            string monday2;

                            object objTotalHours1 = DatabaseHelper.executeScalar("select TotalHrs from tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "'  and EodcDate='" + ds.Tables[0].Rows[0]["Date_of_Monday"].ToString().Trim() + "'");
                            if (objTotalHours1 != null && objTotalHours1.ToString() != "")
                            {
                                try
                                {
                                    string totime = "";
                                    string[] strbreak = objTotalHours1.ToString().Split(':');
                                    totime = strbreak[0] + ":" + strbreak[1];
                                    sb.Append("|" + "<span style='color:red'>" + totime + "</span>");
                                    monday2 = totime;

                                }
                                catch (Exception)
                                {

                                    monday2 = "-";
                                    
                                }
                                
                            }
                            else
                            {
                                sb.Append("-");
                                monday2 = "-";
                            }

                            sb.Append("</td>");

                            string firstuserdate1 = "";
                            if (GMDStartDate.Text != "")
                            {
                                firstuserdate1 = @"Select CONVERT(VARCHAR(10),AbsentDate,103) as LeaveDate from tbl_LeaveDetails where UserName='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and  (AbsentDate >=DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text + "'),0) and AbsentDate <= DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text + "'),0)+4)";
                            }
                            else
                            {
                                firstuserdate1 = @"Select CONVERT(VARCHAR(10),AbsentDate,103)  as LeaveDate from tbl_LeaveDetails where UserName='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and (AbsentDate >=DATEADD(week, DATEDIFF(week, 0,GETDATE()),0) and AbsentDate <= DATEADD(week, DATEDIFF(week, 0,GETDATE()),0)+4)";
                            }

                            DataSet dsFirstdate = DatabaseHelper.getDataset(firstuserdate1);

                            ////sb.Append("</tr>");
                            ////sb.Append("</table>");
                            ////sb.Append("</td>");
                            //----------------------------------------------------------------
                            //----------------------------------------------------------------
                            ////if (ddlEmp.SelectedItem.Text == "PHG")
                            ////{
                            //// sb.Append("<td width='20%'>");
                            //// }
                            ////else
                            ////{
                            ////    sb.Append("<td width='17%'>");
                            ////}
                            ////sb.Append("<table width='100%' border='0' height='100%' cellpadding='0' cellspacing='0' class='leavetbl'>");
                            ////sb.Append("<tr  style='height:30px'>");

                            string Tuesday;
                            sb.Append("<td style='border:1px solid #999'>");

                            string strInout2 = "";
                            object objIntimeALL2 = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Tuesday"].ToString() + "'");
                            if (objIntimeALL2 != null && objIntimeALL2.ToString() != "")
                            {
                                DateTime dtALL2 = SUbTimeZones(Convert.ToDateTime(objIntimeALL2).AddHours(-4).AddMinutes(-30));
                                    //Convert.ToDateTime(objIntimeALL2).AddHours(-5).AddMinutes(-30);

                                strInout2 = "<span style='color:green'>"+dtALL2.ToString("HH:mm").ToString() +"</span>"+ "|";
                                Tuesday = dtALL2.ToString("HH:mm").ToString();
                            }
                            else
                            {

                                strInout2 = "-/";
                                Tuesday = "-/";
                            }

                            string Tuesday1;
                            object objOuttimeALL2 = DatabaseHelper.executeScalar("select Outtime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Tuesday"].ToString() + "'");
                            if (objOuttimeALL2 != null && objOuttimeALL2.ToString() != "")
                            {
                                DateTime dtoutALL2 = SUbTimeZones(Convert.ToDateTime(objOuttimeALL2).AddHours(-4).AddMinutes(-30));
                                    //Convert.ToDateTime(objOuttimeALL2).AddHours(-5).AddMinutes(-30);
                                strInout2 +="<span style='color:#0070c0'>"+ dtoutALL2.ToString("HH:mm")+"</span>";
                                Tuesday1 = dtoutALL2.ToString("HH:mm");
                            }
                            else
                            {

                                strInout2 += "-";
                                Tuesday1 = "-";
                            }

                            sb.Append(strInout2);

                            string Tuesday2;

                            object objTotalHours2 = DatabaseHelper.executeScalar("select TotalHrs from tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and EodcDate='" + ds.Tables[0].Rows[0]["Date_of_Tuesday"].ToString().Trim() + "'");
                            if (objTotalHours2 != null && objTotalHours2.ToString() != "")
                            {
                                try
                                {
                                    string totime2 = "";
                                    string[] strbreak2 = objTotalHours2.ToString().Split(':');
                                    totime2 = strbreak2[0] + ":" + strbreak2[1];
                                    sb.Append("|" + "<span style='color:red'>" + totime2 + "</span>");
                                    Tuesday2 = totime2;

                                }
                                catch (Exception)
                                {
                                    Tuesday2 = "-";
                                }
                            }
                            else
                            {

                                sb.Append("-");
                                Tuesday2 = "-";

                            }
                            sb.Append("</td>");
                            ////sb.Append("</tr>");
                            ////sb.Append("</table>");
                            ////sb.Append("</td>");

                            //---------------------------------------------------------------
                            //----------------------------------------------------------------
                            string strInout3 = "";

                            ////if (ddlEmp.SelectedItem.Text == "PHG")
                            ////{
                            ////    sb.Append("<td width='20%'>");
                            ////}
                            ////else
                            ////{
                            ////    sb.Append("<td width='17%'>");
                            ////}


                            ////sb.Append("<table width='100%' border='0' height='100%' cellpadding='0' cellspacing='0' class='leavetbl'>");
                            ////sb.Append("<tr  style='height:30px'>");

                            string wenesday;
                            sb.Append("<td style='border:1px solid #999'>");
                            object objIntimeALL3 = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Wednesday"].ToString() + "'");
                            if (objIntimeALL3 != null && objIntimeALL3.ToString() != "")
                            {
                                DateTime dtALL3 = SUbTimeZones(Convert.ToDateTime(objIntimeALL3).AddHours(-4).AddMinutes(-30));
                                    //Convert.ToDateTime(objIntimeALL3).AddHours(-5).AddMinutes(-30);

                                strInout3 ="<span style='color:green'>"+ dtALL3.ToString("HH:mm") + "</span>"+"|";
                                wenesday = dtALL3.ToString("HH:mm");

                            }
                            else
                            {
                                strInout3 = "-/";
                                wenesday = "-/";
                            }

                            string wenesday1;
                            object objOuttimeALL3 = DatabaseHelper.executeScalar("select Outtime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Wednesday"].ToString() + "'");
                            if (objOuttimeALL3 != null && objOuttimeALL3.ToString() != "")
                            {
                                DateTime dtoutALL3 = SUbTimeZones(Convert.ToDateTime(objOuttimeALL3).AddHours(-4).AddMinutes(-30));
                                    //Convert.ToDateTime(objOuttimeALL3).AddHours(-5).AddMinutes(-30);

                                strInout3 += "<span style='color:#0070c0'>"+dtoutALL3.ToString("HH:mm")+"</span>";
                                wenesday1 = dtoutALL3.ToString("HH:mm");
                            }
                            else
                            {

                                strInout3 += "-";
                                wenesday1 = "-";
                            }
                            sb.Append(strInout3);

                            string wenesday2;
                            object objTotalHours3 = DatabaseHelper.executeScalar("select TotalHrs from tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and EodcDate='" + ds.Tables[0].Rows[0]["Date_of_Wednesday"].ToString().Trim() + "'");
                            if (objTotalHours3 != null && objTotalHours3.ToString() != "")
                            {
                                try
                                {
                                    string totime3 = "";
                                    string[] strbreak3 = objTotalHours3.ToString().Split(':');
                                    totime3 = strbreak3[0] + ":" + strbreak3[1];
                                    sb.Append("|" + "<span style='color:red'>" + totime3 + "</span>");
                                    //sb.Append("-" + objTotalHours3.ToString());
                                    wenesday2 = totime3;

                                }
                                catch (Exception)
                                {

                                    wenesday2 = "-";
                                }
                               
                            }
                            else
                            {
                                sb.Append("-");
                                wenesday2 = "-";
                            }

                            sb.Append("</td>");
                            ////sb.Append("</tr>");
                            ////sb.Append("</table>");
                            ////sb.Append("</td>");

                            //---------------------------------------------------------------
                            //----------------------------------------------------------------
                            string strInout4 = "";


                            ////if (ddlEmp.SelectedItem.Text == "PHG")
                            ////{
                            ////    sb.Append("<td width='20%'>");
                            ////}
                            ////else
                            ////{
                            ////    sb.Append("<td width='17%'>");
                            ////}

                            ////sb.Append("<table width='100%' border='0' height='100%' cellpadding='0' cellspacing='0' class='leavetbl'>");
                            ////sb.Append("<tr  style='height:30px'>");
                            string thursday;
                            sb.Append("<td style='border:1px solid #999'>");

                            object objIntimeALL4 = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Thusday"].ToString() + "'");
                            if (objIntimeALL4 != null && objIntimeALL4.ToString() != "")
                            {
                                DateTime dtALL4 = SUbTimeZones(Convert.ToDateTime(objIntimeALL4).AddHours(-4).AddMinutes(-30));
                                    //Convert.ToDateTime(objIntimeALL4).AddHours(-5).AddMinutes(-30);

                                strInout4 = "<span style='color:green'>"+ dtALL4.ToString("HH:mm") +"</span>"+ "|";
                                thursday = dtALL4.ToString("HH:mm");
                            }
                            else
                            {

                                strInout4 = "-/";
                                thursday = "-";
                            }

                            string thursday1;
                            object objOuttimeALL4 = DatabaseHelper.executeScalar("select Outtime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Thusday"].ToString() + "'");
                            if (objOuttimeALL4 != null && objOuttimeALL4.ToString() != "")
                            {
                                //DateTime dtoutALL4 = Convert.ToDateTime(objOuttimeALL4).AddHours(-5).AddMinutes(-30);
                                DateTime dtoutALL4 = SUbTimeZones(Convert.ToDateTime(objOuttimeALL4).AddHours(-4).AddMinutes(-30));
                                    //Convert.ToDateTime(objOuttimeALL4).AddHours(-5).AddMinutes(-30);
                                

                                strInout4 += "<span style='color:#0070c0'>"+dtoutALL4.ToString("HH:mm")+"</span>";
                                thursday1 = dtoutALL4.ToString("HH:mm");
                            }
                            else
                            {

                                strInout4 += "-";
                                thursday1 = "-";
                            }

                            sb.Append(strInout4);
                            string thursday2;
                            object objTotalHours4 = DatabaseHelper.executeScalar("select TotalHrs from tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and EodcDate='" + ds.Tables[0].Rows[0]["Date_of_Thusday"].ToString().Trim() + "'");
                            if (objTotalHours4 != null && objTotalHours4.ToString() != "")
                            {
                                try
                                {
                                    string totime4 = "";
                                    string[] strbreak4 = objTotalHours4.ToString().Split(':');
                                    totime4 = strbreak4[0] + ":" + strbreak4[1];
                                    sb.Append("|" + "<span style='color:red'>" + totime4 + "</span>");
                                    thursday2 = totime4;
                                }
                                catch (Exception)
                                {
                                    thursday2 = "-";
                                    
                                }
                               

                            }
                            else
                            {
                                sb.Append("-");
                                thursday2 = "-";
                            }

                            sb.Append("</td>");
                            ////sb.Append("</tr>");
                            ////sb.Append("</table>");
                            ////sb.Append("</td>");
                            //----------------------------------------------------------------
                            //----------------------------------------------------------------
                            string strInout5 = "";


                            ////if (ddlEmp.SelectedItem.Text == "PHG")
                            ////{
                               //// sb.Append("<td width='20%'>");
                            ////}
                            ////else
                            ////{
                             ////   sb.Append("<td width='17%'>");
                            ////}

                            ////sb.Append("<table width='100%' border='0' height='100%' cellpadding='0' cellspacing='0' class='leavetbl'>");
                            ////sb.Append("<tr  style='height:30px'>");
                            string Friday;
                            sb.Append("<td style='border:1px solid #999'>");

                            object objIntimeALL5 = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString() + "'");
                            if (objIntimeALL5 != null && objIntimeALL5.ToString() != "")
                            {
                                DateTime dtALL5 = SUbTimeZones(Convert.ToDateTime(objIntimeALL5).AddHours(-4).AddMinutes(-30));
                                    //Convert.ToDateTime(objIntimeALL5).AddHours(-5).AddMinutes(-30);

                                strInout5 = "<span style='color:green'>"+dtALL5.ToString("HH:mm") + "</span>"+"|";
                                Friday = dtALL5.ToString("HH:mm").ToString();
                            }
                            else
                            {

                                strInout5 = "-/";
                                Friday = "-";
                            }
                            string Friday1;
                            object objOuttimeALL5 = DatabaseHelper.executeScalar("select Outtime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString() + "'");
                            if (objOuttimeALL5 != null && objOuttimeALL5.ToString() != "")
                            {
                                DateTime dtoutALL5 = SUbTimeZones(Convert.ToDateTime(objOuttimeALL5).AddHours(-4).AddMinutes(-30));
                                    //Convert.ToDateTime(objOuttimeALL5).AddHours(-5).AddMinutes(-30);

                                strInout5 +="<span style='color:#0070c0'>"+ dtoutALL5.ToString("HH:mm")+"</span>";
                                Friday1 = dtoutALL5.ToString("HH:mm").ToString();
                                
                            }
                            else
                            {

                                strInout5 += "-";
                                Friday1 = "-";

                            }
                            sb.Append(strInout5);
                            string Friday2;
                            object objTotalHours5 = DatabaseHelper.executeScalar("select TotalHrs from tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and EodcDate='" + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString().Trim() + "'");
                            if (objTotalHours5 != null && objTotalHours5.ToString() != "")
                            {
                                try
                                {
                                    string totime5 = "";
                                    string[] strbreak5 = objTotalHours5.ToString().Split(':');
                                    totime5 = strbreak5[0] + ":" + strbreak5[1];
                                    sb.Append("|" + "<span style='color:red'>" + totime5 + "</span>");
                                    Friday2 = totime5;

                                }
                                catch (Exception)
                                {
                                    Friday2 = "-";
                                    
                                }
                               

                            }
                            else
                            {
                                sb.Append("-");
                                Friday2 = "-";
                            }

                            sb.Append("</td>");

                            ////sb.Append("</tr>");
                            ////sb.Append("</table>");
                            ////sb.Append("</td>");
                            //----------------------------------------------------------------
                            //----------------------------------------------------------------
                            string Saturday="";
                            string Saturday1="";
                            string Saturday2="";

                            if (ddlEmp.SelectedItem.Text != "PHG")
                            {
                                string strInout6 = "";
                                ////sb.Append("<td width='20%'>");
                                ////sb.Append("<table width='100%' border='0' height='100%' cellpadding='0' cellspacing='0' class='leavetbl'>");
                                ////sb.Append("<tr  style='height:30px'>");
                                sb.Append("<td style='border:1px solid #999'>");


                                object objIntimeALL6 = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString() + "'");
                                if (objIntimeALL6 != null && objIntimeALL6.ToString() != "")
                                {

                                    DateTime dtALL6 = SUbTimeZones(Convert.ToDateTime(objIntimeALL6).AddHours(-4).AddMinutes(-30));
                                        //Convert.ToDateTime(objIntimeALL6).AddHours(-5).AddMinutes(-30);

                                    strInout6 = "<span style='color:green'>"+dtALL6.ToString("HH:mm") +"</span>"+ "|";
                                    Saturday = dtALL6.ToString("HH:mm").ToString();
                                }
                                else
                                {

                                    strInout6 = "-/";
                                    Saturday = "-/";
                                }
                                object objOuttimeALL6 = DatabaseHelper.executeScalar("select Outtime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString() + "'");
                                
                                if (objOuttimeALL6 != null && objOuttimeALL6.ToString() != "")
                                {
                                    DateTime dtoutALL6 = SUbTimeZones(Convert.ToDateTime(objOuttimeALL6).AddHours(-4).AddMinutes(-30));
                                        //Convert.ToDateTime(objOuttimeALL6).AddHours(-5).AddMinutes(-30);

                                    //strInout6 +="<span style='color:#0070c0'>"+ dtoutALL6.ToString("HH:mm")+"</span>";
                                    Saturday1 = dtoutALL6.ToString("HH:mm").ToString();
                                }
                                else
                                {

                                    strInout6 += "-";
                                    Saturday1 = "-";
                                }
                                sb.Append(strInout6);

                              
                                object objTotalHours6 = DatabaseHelper.executeScalar("select TotalHrs from tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and EodcDate='" + ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString().Trim() + "'");
                                if (objTotalHours6 != null && objTotalHours6.ToString() != "")
                                {
                                    try
                                    {
                                        string totime6 = "";
                                        string[] strbreak6 = objTotalHours6.ToString().Split(':');
                                        totime6 = strbreak6[0] + ":" + strbreak6[1];
                                        sb.Append("|" + "<span style='color:red'>" + totime6 + "</span>");
                                        Saturday2 = totime6;

                                    }
                                    catch (Exception)
                                    {
                                        Saturday2 = "-";
                                        
                                    }
                                   
                                    //sb.Append("-" + objTotalHours6.ToString());
                                }
                                else
                                {
                                    Saturday2 = "-";
                                }
                                //sb.Append("</td>");
                                ////sb.Append("</tr>");
                                ////sb.Append("</table>");
                                ////sb.Append("</td>");
                            }


                            ////sb.Append("</table>");
                            ////sb.Append("</td>");


                            //******************************************* End Create In Time ******************************************************************

                           // sb.Append("</tr>");
                            dtgrid.Rows.Add(EmpName, skills, monday, monday1, monday2, Tuesday, Tuesday1, Tuesday2, wenesday, wenesday1, wenesday2, thursday, thursday1, thursday2, Friday, Friday1, Friday2, Saturday, Saturday1, Saturday2);
                                                        
                        }
                        Session["dtgrid"] = dtgrid.DefaultView;
                        GridView1.DataSource = dtgrid;
                        GridView1.DataBind();
                        sb.Append("</table>");
                        showexportcontent.InnerHtml = sb.ToString();
                        //showexl = sb;
                    }

               // }
           // }
        }
        catch (Exception ex)
        {
        }
    }

    public void dynamicallyFirst(DataSet ds)
    {
        try
        {
            
            string strQuery = "";
            DataTable dtgrid = new DataTable();
            dtgrid.Columns.Add("Employee_name");
            dtgrid.Columns.Add("Skills");
            dtgrid.Columns.Add("Date_of_Monday");
            dtgrid.Columns.Add("Date_of_Monday1");
            dtgrid.Columns.Add("Date_of_Monday2");
            dtgrid.Columns.Add("Date_of_Tuesday");
            dtgrid.Columns.Add("Date_of_Tuesday1");
            dtgrid.Columns.Add("Date_of_Tuesday2");
            dtgrid.Columns.Add("Date_of_Wednesday");
            dtgrid.Columns.Add("Date_of_Wednesday1");
            dtgrid.Columns.Add("Date_of_Wednesday2");
            dtgrid.Columns.Add("Date_of_Thusday");
            dtgrid.Columns.Add("Date_of_Thusday1");
            dtgrid.Columns.Add("Date_of_Thusday2");
            dtgrid.Columns.Add("Date_of_Friday");
            dtgrid.Columns.Add("Date_of_Friday1");
            dtgrid.Columns.Add("Date_of_Friday2");
            dtgrid.Columns.Add("Date_of_Saturday");
            dtgrid.Columns.Add("Date_of_Saturday1");
            dtgrid.Columns.Add("Date_of_Saturday2");

            // if (ddlProj.SelectedItem != null)
            // {
            //string lcval = "update tblEmp set TotalHrs = '09:00:00' where TotalHrs = '-'";
            //DatabaseHelper.executeNonQuery(lcval);

            HtmlDesign.Visible = true;


            //if (ddlProj.SelectedItem.Text.Trim() != "--Select--")
            //   if (ddlProj.SelectedIndex != 0)
            // {
            strQuery = @"select u.* from users u where IsCW='Y' and u.username not like '%amit%' ";
            //strQuery += " and pd.Project_id= " + ddlProj.SelectedValue.ToString() + " and pd.CompanyName='" + Session["CompanyName"].ToString() + "'";

            if (ddlEmp.SelectedItem.Text != "--Select--")
            {
                strQuery += " and u.user_id='" + ddlEmp.SelectedValue.ToString() + "'";
            }
            strQuery += " order by username ";

            DataSet dsAllEmp = DatabaseHelper.getDataset(strQuery);
            if (dsAllEmp != null && dsAllEmp.Tables.Count > 0 && dsAllEmp.Tables[0].Rows.Count > 0)
            {
                sb.Append("<table width='100%' border='0' cellpadding='0' cellspacing='0'>");
                sb.Append("<tr>");
                sb.Append("<td height='10px' colspan='8' align='left' style='font-size:medium'>");
                sb.Append("<b>Employee attendance report (from ");
                // sb.Append(ds.Tables[0].Rows[0]["Date_of_Monday"].ToString());
                sb.Append(ds.Tables[0].Rows[0]["Date_of_Monday"].ToString());
                sb.Append(" to ");
                if (ddlEmp.SelectedItem.Text == "PHG")
                {
                    sb.Append(ds.Tables[0].Rows[0]["Date_of_Friday"].ToString() + ")");
                }
                else
                {
                    sb.Append(ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString() + ")");
                }
                sb.Append("</b></td>");
                sb.Append("</tr>");
                //sb.Append("<tr>");
                //sb.Append("<td height='10px'>");
                //sb.Append("</td>");
                //sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("<table width='100%' border='0' cellpadding='0' cellspacing='0'>");
                sb.Append("<tr style='background:#e1e1e1'>");
                sb.Append("<td height='37px' style='border:1px solid #999'>");
                sb.Append("<table border='0' width='100%' height='37px' cellpadding='0' cellspacing='0'><tr><td height='45px' rowspan='2' align='center'><b>Employee name</b></td></tr></table>");
                ////sb.Append("<br><table border='0' width='100%' height='37px' cellpadding='0' cellspacing='0'  class='leavetbl' style='background:#e1e1e1'><tr><td height='45px' rowspan='2' align='center'><b>Employee name</b></td></tr></table>");
                sb.Append("</td>");


                sb.Append("<td style='border:1px solid #999'>");
                sb.Append("<table border='0' width='100%'height='37px' cellpadding='0' cellspacing='0'><tr><td height='45px' rowspan='2' align='center'><b>Skills</b></td></tr></table>");
                ////sb.Append("<br><table border='0' width='100%'height='37px' cellpadding='0' cellspacing='0'  class='leavetbl' style='background:#e1e1e1'><tr><td height='45px' rowspan='2' align='center'><b>Skills</b></td></tr></table>");
                sb.Append("</td>");
                ////sb.Append("<td>");


                ////sb.Append("<br><table width='100%' border='0' cellpadding='0' cellspacing='0'  class='leavetbl' style='background:#e1e1e1'>");
                ////sb.Append("<tr align='center'>");



                if (ddlEmp.SelectedItem.Text == "PHG")
                {
                    sb.Append("<td width='20%' style='border:1px solid #999'>");
                }
                else
                {
                    sb.Append("<td width='17%' height='45px' style='border:1px solid #999'>");
                }

                sb.Append("<b>Mon " + ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "</b>");
                sb.Append("<br><b>In|Out(GMT)|Total</b>");
                //sb.Append("<table border='0' cellpadding='0' cellspacing='0' align='center'><tr><td align='center'>In/Out (GMT)</td><td> Total </td></tr></table>");
                sb.Append("</td>");




                if (ddlEmp.SelectedItem.Text == "PHG")
                {
                    sb.Append("<td width='20%' style='border:1px solid #999'>");
                }
                else
                {
                    sb.Append("<td width='17%' style='border:1px solid #999'>");
                }

                sb.Append("<b>Tue " + ds.Tables[0].Rows[0]["Date_of_Tuesday"].ToString() + "</b>");
                sb.Append("<br><b>In|Out(GMT)|Total</b>");
                //sb.Append("<table border='0' cellpadding='0' cellspacing='0' align='center'><tr><td align='center'>In/Out (GMT)</td><td> Total </td></tr></table>");
                sb.Append("</td>");


                if (ddlEmp.SelectedItem.Text == "PHG")
                {
                    sb.Append("<td width='20%' style='border:1px solid #999'>");
                }
                else
                {
                    sb.Append("<td width='17%' style='border:1px solid #999'>");
                }


                sb.Append("<b>Wed " + ds.Tables[0].Rows[0]["Date_of_Wednesday"].ToString() + " </b>");
                sb.Append("<br><b>In|Out(GMT)|Total</b>");
                //sb.Append("<table border='0' cellpadding='0' cellspacing='0' align='center'><tr><td align='center'>In/Out (GMT)</td><td> Total </td></tr></table>");
                sb.Append("</td>");


                if (ddlEmp.SelectedItem.Text == "PHG")
                {
                    sb.Append("<td width='20%' style='border:1px solid #999'>");
                }
                else
                {
                    sb.Append("<td width='17%' style='border:1px solid #999'>");
                }


                sb.Append("<b>Thur " + ds.Tables[0].Rows[0]["Date_of_Thusday"].ToString() + "</b>");
                sb.Append("<br><b>In|Out(GMT)|Total</b>");
                //sb.Append("<table border='0' cellpadding='0' cellspacing='0' align='center'><tr><td align='center'>In/Out (GMT)</td><td> Total </td></tr></table>");
                sb.Append("</td>");


                if (ddlEmp.SelectedItem.Text == "PHG")
                {
                    sb.Append("<td width='20%' style='border:1px solid #999'>");
                }
                else
                {
                    sb.Append("<td width='17%' style='border:1px solid #999'>");
                }

                sb.Append("<b>Fri " + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString() + "</b>");
                sb.Append("<br><b>In|Out(GMT)|Total</b>");
                //sb.Append("<table border='0' cellpadding='0' cellspacing='0' align='center'><tr><td align='center'>In/Out (GMT)</td><td> Total </td></tr></table>");
                sb.Append("</td>");



                if (ddlEmp.SelectedItem.Text != "PHG")
                {
                    sb.Append("<td width='20%' style='border:1px solid #999'>");
                    sb.Append("<b>Sat " + ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString() + "</b>");
                    sb.Append("<br><b>In|Out(GMT)|Total</b>");
                    //sb.Append("<table border='0' cellpadding='0' cellspacing='0' align='center'><tr><td align='center'>In/Out (GMT)</td><td> Total </td></tr></table>");
                    sb.Append("</td>");
                }


                ////sb.Append("</tr>");
                ////sb.Append("</table>");
                sb.Append("</td>");

                sb.Append("</tr>");
                for (int iEmp = 0; iEmp < dsAllEmp.Tables[0].Rows.Count; iEmp++)
                {
                    if (iEmp % 2 == 0)
                    {
                        sb.Append("<tr style='background:#f7f7f7; height:30px;'>");
                    }
                    else
                    {
                        sb.Append("<tr style='background:#ffffff; height:30px;'>");
                    }
                    sb.Append("<td align='left' style='border:1px solid #999' >");
                    string skills;
                    string EmpName;
                    sb.Append("<table border='0' width='100%' cellpadding='0' cellspacing='0'><tr  style='height:30px'><td>" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "</td></tr></table>");
                    EmpName = dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString();
                    if (dsAllEmp.Tables[0].Rows[iEmp]["skills"].ToString() != "")
                    {
                        sb.Append("</td><td align='left' style='border:1px solid #999'><table border='0' width='100%' height='100%' cellpadding='0' cellspacing='0'><tr style='height:30px'><td >" + dsAllEmp.Tables[0].Rows[iEmp]["skills"].ToString() + "</td></tr></table></td>");
                        skills = dsAllEmp.Tables[0].Rows[iEmp]["skills"].ToString();
                    }
                    else
                    {
                        sb.Append("</td><td align='left' style='border:1px solid #999'><table border='0' width='100%' height='100%' cellpadding='0' cellspacing='0'><tr  style='height:30px'><td  align='center'> -/ </td></tr></table></td>");
                        skills = "-/";
                    }

                    //******************************************* Create In Time ******************************************************************
                    ////sb.Append("<td>");

                    ////sb.Append("<table width='100%' border='0' cellpadding='0' cellspacing='0'>");
                    ////sb.Append("<tr  style='height:30px'>");


                    ////if (ddlEmp.SelectedItem.Text == "PHG")
                    ////{
                    ////    sb.Append("<td width='20%'>");
                    ////}
                    ////else
                    ////{
                    ////    sb.Append("<td width='17%'>");
                    ////}

                    ////sb.Append("<table width='100%' border='0' height='100%' cellpadding='0' cellspacing='0' class='leavetbl'>");
                    ////sb.Append("<tr  style='height:30px'>");

                    sb.Append("<td style='border:1px solid #999'>");
                    string monday;
                    string monday1;

                    string strInout1 = "";
                    object objIntimeALL1 = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "'");
                    if (objIntimeALL1 != null && objIntimeALL1.ToString() != "")
                    {

                        DateTime dtALL1 = Convert.ToDateTime(objIntimeALL1).AddHours(-5).AddMinutes(-30);
                        //Convert.ToDateTime(objIntimeALL1).AddHours(-5).AddMinutes(-30);
                        strInout1 = "<span style='color:green'>" + dtALL1.ToString("HH:mm").ToString() + "</span>" + "|";
                        monday = dtALL1.ToString("HH:mm").ToString();

                    }
                    else
                    {
                        strInout1 = "-/";
                        monday = "-/";
                    }


                    string tuesday;
                    object objOuttimeALL1 = DatabaseHelper.executeScalar("select Outtime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "'");
                    if (objOuttimeALL1 != null && objOuttimeALL1.ToString() != "")
                    {
                        DateTime dtoutALL1 = Convert.ToDateTime(objOuttimeALL1).AddHours(-5).AddMinutes(-30);
                        //Convert.ToDateTime(objOuttimeALL1).AddHours(-5).AddMinutes(-30);
                        strInout1 += "<span style='color:#0070c0'>" + dtoutALL1.ToString("HH:mm") + "</span>";
                        monday1 = dtoutALL1.ToString("HH:mm");
                    }
                    else
                    {
                        strInout1 += "-";
                        monday1 = "-";
                    }
                    sb.Append(strInout1);


                    string monday2;

                    object objTotalHours1 = DatabaseHelper.executeScalar("select TotalHrs from tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "'  and EodcDate='" + ds.Tables[0].Rows[0]["Date_of_Monday"].ToString().Trim() + "'");
                    if (objTotalHours1 != null && objTotalHours1.ToString() != "")
                    {
                        try
                        {
                            string totime = "";
                            string[] strbreak = objTotalHours1.ToString().Split(':');
                            totime = strbreak[0] + ":" + strbreak[1];
                            sb.Append("|" + "<span style='color:red'>" + totime + "</span>");
                            monday2 = totime;

                        }
                        catch (Exception)
                        {

                            monday2 = "-";

                        }

                    }
                    else
                    {
                        sb.Append("-");
                        monday2 = "-";
                    }


                    sb.Append("</td>");


                    string firstuserdate1 = "";
                    if (GMDStartDate.Text != "")
                    {
                        firstuserdate1 = @"Select CONVERT(VARCHAR(10),AbsentDate,103) as LeaveDate from tbl_LeaveDetails where UserName='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and  (AbsentDate >=DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text + "'),0) and AbsentDate <= DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text + "'),0)+4)";
                    }
                    else
                    {
                        firstuserdate1 = @"Select CONVERT(VARCHAR(10),AbsentDate,103)  as LeaveDate from tbl_LeaveDetails where UserName='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and (AbsentDate >=DATEADD(week, DATEDIFF(week, 0,GETDATE()),0) and AbsentDate <= DATEADD(week, DATEDIFF(week, 0,GETDATE()),0)+4)";
                    }

                    DataSet dsFirstdate = DatabaseHelper.getDataset(firstuserdate1);

                    ////sb.Append("</tr>");
                    ////sb.Append("</table>");
                    ////sb.Append("</td>");
                    //----------------------------------------------------------------
                    //----------------------------------------------------------------
                    ////if (ddlEmp.SelectedItem.Text == "PHG")
                    ////{
                    //// sb.Append("<td width='20%'>");
                    //// }
                    ////else
                    ////{
                    ////    sb.Append("<td width='17%'>");
                    ////}
                    ////sb.Append("<table width='100%' border='0' height='100%' cellpadding='0' cellspacing='0' class='leavetbl'>");
                    ////sb.Append("<tr  style='height:30px'>");

                    string Tuesday;
                    sb.Append("<td style='border:1px solid #999'>");

                    string strInout2 = "";
                    object objIntimeALL2 = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Tuesday"].ToString() + "'");
                    if (objIntimeALL2 != null && objIntimeALL2.ToString() != "")
                    {
                        DateTime dtALL2 = Convert.ToDateTime(objIntimeALL2).AddHours(-5).AddMinutes(-30);
                      //Convert.ToDateTime(objIntimeALL2).AddHours(-5).AddMinutes(-30);

                        strInout2 = "<span style='color:green'>" + dtALL2.ToString("HH:mm").ToString() + "</span>" + "|";
                        Tuesday = dtALL2.ToString("HH:mm").ToString();
                    }
                    else
                    {

                        strInout2 = "-/";
                        Tuesday = "-/";
                    }

                    string Tuesday1;
                    object objOuttimeALL2 = DatabaseHelper.executeScalar("select Outtime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Tuesday"].ToString() + "'");
                    if (objOuttimeALL2 != null && objOuttimeALL2.ToString() != "")
                    {
                        DateTime dtoutALL2 = Convert.ToDateTime(objOuttimeALL2).AddHours(-5).AddMinutes(-30);
                        //Convert.ToDateTime(objOuttimeALL2).AddHours(-5).AddMinutes(-30);
                        strInout2 += "<span style='color:#0070c0'>" + dtoutALL2.ToString("HH:mm") + "</span>";
                        Tuesday1 = dtoutALL2.ToString("HH:mm");
                    }
                    else
                    {

                        strInout2 += "-";
                        Tuesday1 = "-";
                    }

                    sb.Append(strInout2);

                    string Tuesday2;

                    object objTotalHours2 = DatabaseHelper.executeScalar("select TotalHrs from tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and EodcDate='" + ds.Tables[0].Rows[0]["Date_of_Tuesday"].ToString().Trim() + "'");
                    if (objTotalHours2 != null && objTotalHours2.ToString() != "")
                    {
                        try
                        {
                            string totime2 = "";
                            string[] strbreak2 = objTotalHours2.ToString().Split(':');
                            totime2 = strbreak2[0] + ":" + strbreak2[1];
                            sb.Append("|" + "<span style='color:red'>" + totime2 + "</span>");
                            Tuesday2 = totime2;

                        }
                        catch (Exception)
                        {
                            Tuesday2 = "-";

                        }
                    }
                    else
                    {

                        sb.Append("-");
                        Tuesday2 = "-";

                    }
                    sb.Append("</td>");
                    ////sb.Append("</tr>");
                    ////sb.Append("</table>");
                    ////sb.Append("</td>");

                    //---------------------------------------------------------------
                    //----------------------------------------------------------------
                    string strInout3 = "";

                    ////if (ddlEmp.SelectedItem.Text == "PHG")
                    ////{
                    ////    sb.Append("<td width='20%'>");
                    ////}
                    ////else
                    ////{
                    ////    sb.Append("<td width='17%'>");
                    ////}


                    ////sb.Append("<table width='100%' border='0' height='100%' cellpadding='0' cellspacing='0' class='leavetbl'>");
                    ////sb.Append("<tr  style='height:30px'>");

                    string wenesday;
                    sb.Append("<td style='border:1px solid #999'>");
                    object objIntimeALL3 = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Wednesday"].ToString() + "'");
                    if (objIntimeALL3 != null && objIntimeALL3.ToString() != "")
                    {
                        DateTime dtALL3 = Convert.ToDateTime(objIntimeALL3).AddHours(-5).AddMinutes(-30);
                        //Convert.ToDateTime(objIntimeALL3).AddHours(-5).AddMinutes(-30);

                        strInout3 = "<span style='color:green'>" + dtALL3.ToString("HH:mm") + "</span>" + "|";
                        wenesday = dtALL3.ToString("HH:mm");

                    }
                    else
                    {
                        strInout3 = "-/";
                        wenesday = "-/";
                    }

                    string wenesday1;
                    object objOuttimeALL3 = DatabaseHelper.executeScalar("select Outtime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Wednesday"].ToString() + "'");
                    if (objOuttimeALL3 != null && objOuttimeALL3.ToString() != "")
                    {
                        DateTime dtoutALL3 = Convert.ToDateTime(objOuttimeALL3).AddHours(-5).AddMinutes(-30);
                        //Convert.ToDateTime(objOuttimeALL3).AddHours(-5).AddMinutes(-30);

                        strInout3 += "<span style='color:#0070c0'>" + dtoutALL3.ToString("HH:mm") + "</span>";
                        wenesday1 = dtoutALL3.ToString("HH:mm");
                    }
                    else
                    {

                        strInout3 += "-";
                        wenesday1 = "-";
                    }
                    sb.Append(strInout3);

                    string wenesday2;
                    object objTotalHours3 = DatabaseHelper.executeScalar("select TotalHrs from tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and EodcDate='" + ds.Tables[0].Rows[0]["Date_of_Wednesday"].ToString().Trim() + "'");
                    if (objTotalHours3 != null && objTotalHours3.ToString() != "")
                    {
                        try
                        {
                            string totime3 = "";
                            string[] strbreak3 = objTotalHours3.ToString().Split(':');
                            totime3 = strbreak3[0] + ":" + strbreak3[1];
                            sb.Append("|" + "<span style='color:red'>" + totime3 + "</span>");
                            //sb.Append("-" + objTotalHours3.ToString());
                            wenesday2 = totime3;

                        }
                        catch (Exception)
                        {

                            wenesday2 = "-";
                        }

                    }
                    else
                    {
                        sb.Append("-");
                        wenesday2 = "-";
                    }

                    sb.Append("</td>");
                    ////sb.Append("</tr>");
                    ////sb.Append("</table>");
                    ////sb.Append("</td>");

                    //---------------------------------------------------------------
                    //----------------------------------------------------------------
                    string strInout4 = "";


                    ////if (ddlEmp.SelectedItem.Text == "PHG")
                    ////{
                    ////    sb.Append("<td width='20%'>");
                    ////}
                    ////else
                    ////{
                    ////    sb.Append("<td width='17%'>");
                    ////}

                    ////sb.Append("<table width='100%' border='0' height='100%' cellpadding='0' cellspacing='0' class='leavetbl'>");
                    ////sb.Append("<tr  style='height:30px'>");
                    string thursday;
                    sb.Append("<td style='border:1px solid #999'>");

                    object objIntimeALL4 = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Thusday"].ToString() + "'");
                    if (objIntimeALL4 != null && objIntimeALL4.ToString() != "")
                    {
                        DateTime dtALL4 = Convert.ToDateTime(objIntimeALL4).AddHours(-5).AddMinutes(-30);
                        //Convert.ToDateTime(objIntimeALL4).AddHours(-5).AddMinutes(-30);

                        strInout4 = "<span style='color:green'>" + dtALL4.ToString("HH:mm") + "</span>" + "|";
                        thursday = dtALL4.ToString("HH:mm");
                    }
                    else
                    {

                        strInout4 = "-/";
                        thursday = "-";
                    }

                    string thursday1;
                    object objOuttimeALL4 = DatabaseHelper.executeScalar("select Outtime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Thusday"].ToString() + "'");
                    if (objOuttimeALL4 != null && objOuttimeALL4.ToString() != "")
                    {
                        DateTime dtoutALL4 = Convert.ToDateTime(objOuttimeALL4).AddHours(-5).AddMinutes(-30);
                        //DateTime dtoutALL4 = SUbTimeZones(Convert.ToDateTime(objOuttimeALL4));
                        //Convert.ToDateTime(objOuttimeALL4).AddHours(-5).AddMinutes(-30);


                        strInout4 += "<span style='color:#0070c0'>" + dtoutALL4.ToString("HH:mm") + "</span>";
                        thursday1 = dtoutALL4.ToString("HH:mm");
                    }
                    else
                    {

                        strInout4 += "-";
                        thursday1 = "-";
                    }

                    sb.Append(strInout4);
                    string thursday2;
                    object objTotalHours4 = DatabaseHelper.executeScalar("select TotalHrs from tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and EodcDate='" + ds.Tables[0].Rows[0]["Date_of_Thusday"].ToString().Trim() + "'");
                    if (objTotalHours4 != null && objTotalHours4.ToString() != "")
                    {
                        try
                        {
                            string totime4 = "";
                            string[] strbreak4 = objTotalHours4.ToString().Split(':');
                            totime4 = strbreak4[0] + ":" + strbreak4[1];
                            sb.Append("|" + "<span style='color:red'>" + totime4 + "</span>");
                            thursday2 = totime4;
                        }
                        catch (Exception)
                        {
                            thursday2 = "-";

                        }


                    }
                    else
                    {
                        sb.Append("-");
                        thursday2 = "-";
                    }

                    sb.Append("</td>");
                    ////sb.Append("</tr>");
                    ////sb.Append("</table>");
                    ////sb.Append("</td>");
                    //----------------------------------------------------------------
                    //----------------------------------------------------------------
                    string strInout5 = "";


                    ////if (ddlEmp.SelectedItem.Text == "PHG")
                    ////{
                    //// sb.Append("<td width='20%'>");
                    ////}
                    ////else
                    ////{
                    ////   sb.Append("<td width='17%'>");
                    ////}

                    ////sb.Append("<table width='100%' border='0' height='100%' cellpadding='0' cellspacing='0' class='leavetbl'>");
                    ////sb.Append("<tr  style='height:30px'>");
                    string Friday;
                    sb.Append("<td style='border:1px solid #999'>");

                    object objIntimeALL5 = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString() + "'");
                    if (objIntimeALL5 != null && objIntimeALL5.ToString() != "")
                    {
                        DateTime dtALL5 = Convert.ToDateTime(objIntimeALL5).AddHours(-5).AddMinutes(-30);
                        //Convert.ToDateTime(objIntimeALL5).AddHours(-5).AddMinutes(-30);

                        strInout5 = "<span style='color:green'>" + dtALL5.ToString("HH:mm") + "</span>" + "|";
                        Friday = dtALL5.ToString("HH:mm").ToString();
                    }
                    else
                    {

                        strInout5 = "-/";
                        Friday = "-";
                    }
                    string Friday1;
                    object objOuttimeALL5 = DatabaseHelper.executeScalar("select Outtime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString() + "'");
                    if (objOuttimeALL5 != null && objOuttimeALL5.ToString() != "")
                    {
                        DateTime dtoutALL5 = Convert.ToDateTime(objOuttimeALL5).AddHours(-5).AddMinutes(-30);
                        //Convert.ToDateTime(objOuttimeALL5).AddHours(-5).AddMinutes(-30);

                        strInout5 += "<span style='color:#0070c0'>" + dtoutALL5.ToString("HH:mm") + "</span>";
                        Friday1 = dtoutALL5.ToString("HH:mm").ToString();

                    }
                    else
                    {

                        strInout5 += "-";
                        Friday1 = "-";

                    }
                    sb.Append(strInout5);
                    string Friday2;
                    object objTotalHours5 = DatabaseHelper.executeScalar("select TotalHrs from tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and EodcDate='" + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString().Trim() + "'");
                    if (objTotalHours5 != null && objTotalHours5.ToString() != "")
                    {
                        try
                        {
                            string totime5 = "";
                            string[] strbreak5 = objTotalHours5.ToString().Split(':');
                            totime5 = strbreak5[0] + ":" + strbreak5[1];
                            sb.Append("|" + "<span style='color:red'>" + totime5 + "</span>");
                            Friday2 = totime5;

                        }
                        catch (Exception)
                        {
                            Friday2 = "-";

                        }


                    }
                    else
                    {
                        sb.Append("-");
                        Friday2 = "-";
                    }

                    sb.Append("</td>");

                    ////sb.Append("</tr>");
                    ////sb.Append("</table>");
                    ////sb.Append("</td>");
                    //----------------------------------------------------------------
                    //----------------------------------------------------------------
                    string Saturday = "";
                    string Saturday1 = "";
                    string Saturday2 = "";

                    if (ddlEmp.SelectedItem.Text != "PHG")
                    {
                        string strInout6 = "";
                        ////sb.Append("<td width='20%'>");
                        ////sb.Append("<table width='100%' border='0' height='100%' cellpadding='0' cellspacing='0' class='leavetbl'>");
                        ////sb.Append("<tr  style='height:30px'>");
                        sb.Append("<td style='border:1px solid #999'>");


                        object objIntimeALL6 = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString() + "'");
                        if (objIntimeALL6 != null && objIntimeALL6.ToString() != "")
                        {

                            //DateTime dtALL6 = SUbTimeZones(Convert.ToDateTime(objIntimeALL6));
                            DateTime dtALL6 = Convert.ToDateTime(objIntimeALL6).AddHours(-5).AddMinutes(-30);

                            strInout6 = "<span style='color:green'>" + dtALL6.ToString("HH:mm") + "</span>" + "|";
                            Saturday = dtALL6.ToString("HH:mm").ToString();
                        }
                        else
                        {

                            strInout6 = "-/";
                            Saturday = "-/";
                        }
                        object objOuttimeALL6 = DatabaseHelper.executeScalar("select Outtime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString() + "'");

                        if (objOuttimeALL6 != null && objOuttimeALL6.ToString() != "")
                        {
                            //DateTime dtoutALL6 = SUbTimeZones(Convert.ToDateTime(objOuttimeALL6));
                            DateTime dtoutALL6 = Convert.ToDateTime(objOuttimeALL6).AddHours(-5).AddMinutes(-30);

                            //strInout6 +="<span style='color:#0070c0'>"+ dtoutALL6.ToString("HH:mm")+"</span>";
                            Saturday1 = dtoutALL6.ToString("HH:mm").ToString();
                        }
                        else
                        {

                            strInout6 += "-";
                            Saturday1 = "-";
                        }
                        sb.Append(strInout6);


                        object objTotalHours6 = DatabaseHelper.executeScalar("select TotalHrs from tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and EodcDate='" + ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString().Trim() + "'");
                        if (objTotalHours6 != null && objTotalHours6.ToString() != "")
                        {
                            try
                            {
                                string totime6 = "";
                                string[] strbreak6 = objTotalHours6.ToString().Split(':');
                                totime6 = strbreak6[0] + ":" + strbreak6[1];
                                sb.Append("|" + "<span style='color:red'>" + totime6 + "</span>");
                                Saturday2 = totime6;

                            }
                            catch (Exception)
                            {
                                Saturday2 = "-";

                            }

                            //sb.Append("-" + objTotalHours6.ToString());
                        }
                        else
                        {
                            Saturday2 = "-";
                        }
                        //sb.Append("</td>");
                        ////sb.Append("</tr>");
                        ////sb.Append("</table>");
                        ////sb.Append("</td>");
                    }


                    ////sb.Append("</table>");
                    ////sb.Append("</td>");


                    //******************************************* End Create In Time ******************************************************************

                    // sb.Append("</tr>");
                    dtgrid.Rows.Add(EmpName, skills, monday, monday1, monday2, Tuesday, Tuesday1, Tuesday2, wenesday, wenesday1, wenesday2, thursday, thursday1, thursday2, Friday, Friday1, Friday2, Saturday, Saturday1, Saturday2);

                }
                Session["dtgrid"] = dtgrid.DefaultView;
                GridView1.DataSource = dtgrid.DefaultView;
                GridView1.DataBind();
                sb.Append("</table>");
                //HtmlDesign.InnerHtml = sb.ToString();
                 
            }

            // }
            // }
        }
        catch (Exception ex)
        {
        }
    }

    protected void btnexport_Click(object sender, ImageClickEventArgs e)
    {
        
        
        if (rdoid.SelectedIndex == 1)
        {
            Response.ContentType = "application/x-msexcel";
            Response.AddHeader("Content-Disposition", "attachment; filename=Timesheet_report_from_'" + strreportname + "'.xls");
            //Response.ContentEncoding = Encoding.UTF8;
            System.IO.StringWriter tw = new System.IO.StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(tw);
            main.RenderControl(hw);
            Response.Write(tw.ToString());
            Response.End();
        }
        else
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.Charset = "";
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
            HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=UserReport.xls");
            HttpContext.Current.Response.Write(showexportcontent.InnerHtml);
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.Close(); 
           
        }
        
        

    }

    public void fnFillDates()
    {
        if (GMDStartDate.Text.Trim() == "")
        {
            dtSDate = DateTime.Today.AddDays(-DayNo + 1).ToString("yyyy-MM-dd");  //FirstDayOfYear(dt).ToString("dd/MM/yyyy");
            GMDStartDate.Text = dtSDate;
            dtStartingDate = DateTime.Today.AddDays(-DayNo + 1);
        }
        else
        {
            dtStartingDate = Convert.ToDateTime(GMDStartDate.Text);
            dtSDate = dtStartingDate.ToString("yyyy-MM-dd");
        }

        if (GMDEndDate.Text.Trim() == "")
        {
            dtEDate = DateTime.Today.AddDays(6 - DayNo).ToString("yyyy-MM-dd"); //LastDayOfYear(dt).ToString("dd/MM/yyyy");
            GMDEndDate.Text = dtEDate;
            dtEndingDate = DateTime.Today.AddDays(6 - DayNo);

        }
        else
        {
            dtEndingDate = Convert.ToDateTime(GMDEndDate.Text);
            dtEDate = dtEndingDate.ToString("yyyy-MM-dd");
        }
    }

    protected void GMDStartDate_TextChanged(object sender, EventArgs e)
    {
        //==========================
        //fnGenerateTimeSheet();
        //==========================
        if (rdoid.SelectedIndex == 1)
        {
            GMDEndDate.Enabled = true;
            dynamicallyCustom();
        }
        else
        {
           
            GMDEndDate.Enabled = false;
         
           //  Response.Redirect("../LMSAdmin/LMS-In-and-outtime-report.aspx"); 
          fnGenerateTimeSheet();

        }
    }
    protected void rdoid_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rdoid.SelectedIndex == 1)
        {
            GMDEndDate.Enabled =  true;
            dynamicallyCustom();
        }
        else
        {
            GMDEndDate.Enabled = false;
             
            //=========================
            Response.Redirect("../LMSAdmin/LMS-In-and-outtime-report.aspx");  
            //../LMSAdmin/LMS-In-and-outtime-report.aspx
            //ddlTimeInterval.SelectedIndex = 1;
            //fnbinddropdown();
            //fnSetDefaultDate();
            //fnBindEmpddl();
            //Session["TI"] = "TW";
            //lblLR4R1Title.Text = "Leave report for this week";
            //fnGenerateTimeSheet();
            //fnGenerateTimeSheet();
            //=======================

        }
    }

    DateTime  SUbTimeZones(DateTime dt)
    {      
        DateTime dtTMK = DateTime.Now;
        dtTMK = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(dt, TimeZoneInfo.Local.Id,DrpZone.SelectedItem.Value);
        return dtTMK;
    }
    
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToUpper() == "UPDATE")
        {
            string strstring = "";
            int state = 0;
            int index = int.Parse(e.CommandArgument.ToString());
            GridViewRow row = GridView1.Rows[index];
            Label lblname = (Label)row.FindControl("lblempname1");
            TextBox TXtMON = (TextBox)row.FindControl("TXtMON");
            TextBox TXtMON1 = (TextBox)row.FindControl("TXtMON1");
            TextBox TXtMON2 = (TextBox)row.FindControl("TXtMON2");
            TextBox TXtTues = (TextBox)row.FindControl("TXtTues");
            TextBox TXtTues1 = (TextBox)row.FindControl("TXtTues1");
            TextBox TXtTues2 = (TextBox)row.FindControl("TXtTues2");
            TextBox TXtWed = (TextBox)row.FindControl("TXtWed");
            TextBox TXtWed1 = (TextBox)row.FindControl("TXtWed1");
            TextBox TXtWed2 = (TextBox)row.FindControl("TXtWed2");
            TextBox TXtThus = (TextBox)row.FindControl("TXtThus");
            TextBox TXtThus1 = (TextBox)row.FindControl("TXtThus1");
            TextBox TXtThus2 = (TextBox)row.FindControl("TXtThus2");
            TextBox TXtFrid = (TextBox)row.FindControl("TXtFrid");
            TextBox TXtFrid1 = (TextBox)row.FindControl("TXtFrid1");
            TextBox TXtFrid2 = (TextBox)row.FindControl("TXtFrid2");
            TextBox TXtSat = (TextBox)row.FindControl("TXtSat");
            TextBox TXtSat1 = (TextBox)row.FindControl("TXtSat1");
            TextBox TXtSat2 = (TextBox)row.FindControl("TXtSat2");


            string strQuery = "";
            
            //==============================================================================
            //string strDate = Convert.ToDateTime(GMDStartDate.Text).ToString("yyyy/MM/dd");
            DateTime dt = Convert.ToDateTime(GMDStartDate.Text.ToString()); //DateTime.ParseExact(GMDStartDate.Text.ToString(), "dd/MM/yyyy", null);
            string strDate = dt.ToString("dd/MM/yyyy");// String.Format("{0:yyyy/MM/dd}", dt);
           //===============================================================================




            strQuery = @"SELECT  CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + strDate + "') , 0),103) as Date_of_Monday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + strDate + "') , 0)+1,103) as Date_of_Tuesday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + strDate + "') , 0)+2,103) as Date_of_Wednesday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + strDate + "') , 0)+3,103) as Date_of_Thusday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + strDate + "') , 0)+4,103) as Date_of_Friday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + strDate + "') , 0)+5,103) as Date_of_Saturday";
            DataSet ds = DatabaseHelper.getDataset(strQuery);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                strstring += " update tblEmp set TotalHrs = '" + TXtMON2.Text.ToString() + "' where username = '" + lblname.Text + "' and EodcDate = '" + ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "';";

                //tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and EodcDate='" + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString().Trim() + "'");

                strstring = " update tblEmp set TotalHrs = '9:30' where username = 'Amit' and EodcDate = '" + ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "';";
                strstring += " update tblIntime set intime = '" + TXtMON.Text.ToString() + "',Outtime = '" + TXtMON1.Text.ToString() + "' where username = '" + lblname.Text + "' and indate = '" + ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "';";
                strstring += " update tblEmp set TotalHrs = '" + TXtMON2.Text.ToString() + "' where username = '" + lblname.Text + "' and EodcDate = '" + ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "';";
                strstring += "update tblIntime set intime = '" + TXtTues.Text.ToString() + "',Outtime = '" + TXtTues1.Text.ToString() + "' where username = '" + lblname.Text + "' and indate = '" + ds.Tables[0].Rows[0]["Date_of_Tuesday"].ToString() + "';";
                strstring += " update tblEmp set TotalHrs = '" + TXtTues2.Text.ToString() + "' where username = '" + lblname.Text + "' and EodcDate = '" + ds.Tables[0].Rows[0]["Date_of_Tuesday"].ToString() + "';";
                strstring += "update tblIntime set intime = '" + TXtWed.Text.ToString() + "',Outtime = '" + TXtWed1.Text.ToString() + "' where username = '" + lblname.Text + "' and indate = '" + ds.Tables[0].Rows[0]["Date_of_Wednesday"].ToString() + "';";
                strstring += " update tblEmp set TotalHrs = '" + TXtWed2.Text.ToString() + "' where username = '" + lblname.Text + "' and EodcDate = '" + ds.Tables[0].Rows[0]["Date_of_Wednesday"].ToString() + "';";
                strstring += "update tblIntime set intime = '" + TXtThus.Text.ToString() + "',Outtime = '" + TXtThus1.Text.ToString() + "' where username = '" + lblname.Text + "' and indate = '" + ds.Tables[0].Rows[0]["Date_of_Thusday"].ToString() + "';";
                strstring += " update tblEmp set TotalHrs = '" + TXtThus2.Text.ToString() + "' where username = '" + lblname.Text + "' and EodcDate = '" + ds.Tables[0].Rows[0]["Date_of_Thusday"].ToString() + "';";
                strstring += "update tblIntime set intime = '" + TXtFrid.Text.ToString() + "',Outtime = '" + TXtFrid1.Text.ToString() + "' where username = '" + lblname.Text + "' and indate = '" + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString() + "';";
                strstring += " update tblEmp set TotalHrs = '" + TXtFrid2.Text.ToString() + "' where username = '" + lblname.Text + "' and EodcDate = '" + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString() + "';";
                strstring += "update tblIntime set intime = '" + TXtSat.Text.ToString() + "',Outtime = '" + TXtSat1.Text.ToString() + "' where username = '" + lblname.Text + "' and indate = '" + ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString() + "';";
                strstring += " update tblEmp set TotalHrs = '" + TXtSat2.Text.ToString() + "' where username = '" + lblname.Text + "' and EodcDate = '" + ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString() + "';";
                
                DatabaseHelper.executeNonQuery(strstring);

            }
        }
    }

    
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        
        GridView1.DataSource = Session["dtgrid"];
        GridView1.DataBind();


    }
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
        GridView1.DataSource = Session["dtgrid"];
        GridView1.DataBind();


    }

    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        GridView1.EditIndex = -1;
        fnGenerateTimeSheet();
       
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.Header)
        {
            string strQuery = "";
            //===============================================================================
            DateTime dt = Convert.ToDateTime(GMDStartDate.Text); //DateTime.ParseExact(GMDStartDate.Text.ToString(), "dd/MM/yyyy", null);
            string strDate = dt.ToString("yyyy/MM/dd");//String.Format("{0:yyyy/MM/dd}", dt);
            //string strDate = Convert.ToDateTime(GMDStartDate.Text).ToString("yyyy/MM/dd");
            //===============================================================================

            strQuery = @"SELECT  CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + strDate + "') , 0),103) as Date_of_Monday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + strDate + "') , 0)+1,103) as Date_of_Tuesday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + strDate + "') , 0)+2,103) as Date_of_Wednesday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + strDate + "') , 0)+3,103) as Date_of_Thusday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + strDate + "') , 0)+4,103) as Date_of_Friday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + strDate + "') , 0)+5,103) as Date_of_Saturday";
            DataSet ds = DatabaseHelper.getDataset(strQuery);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                e.Row.Cells[2].Text = "<b>Mon " + ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "</b><br><b>In|Out(GMT)|Total</b>";
                e.Row.Cells[3].Text = "<b>Tue " + ds.Tables[0].Rows[0]["Date_of_Tuesday"].ToString() + "</b><br><b>In|Out(GMT)|Total</b>";
                e.Row.Cells[4].Text = "<b>Wed " + ds.Tables[0].Rows[0]["Date_of_Wednesday"].ToString() + "</b><br><b>In|Out(GMT)|Total</b>";
                e.Row.Cells[5].Text = "<b>Thur " + ds.Tables[0].Rows[0]["Date_of_Thusday"].ToString() + "</b><br><b>In|Out(GMT)|Total</b>";
                e.Row.Cells[6].Text = "<b>Fri " + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString() + "</b><br><b>In|Out(GMT)|Total</b>";
                e.Row.Cells[7].Text = "<b>Sat " + ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString() + "</b><br><b>In|Out(GMT)|Total</b>";
            }
        }
    }

    void fnbinddropdown()
    {
        System.Collections.ObjectModel.ReadOnlyCollection<TimeZoneInfo> TimeZoneColl = TimeZoneInfo.GetSystemTimeZones();

        DrpZone.DataSource = TimeZoneColl;
        DrpZone.DataTextField = "DisplayName";
        DrpZone.DataValueField = "Id";
        DrpZone.DataBind();
        for (int i = 0; i < DrpZone.Items.Count - 1; i++)
        {
            if (DrpZone.Items[i].Value == TimeZoneInfo.Local.Id)
            {
                DrpZone.SelectedIndex = i;
                break;
            }
        }
       
    }
    protected void DrpZone_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rdoid.SelectedItem.ToString() == "Weekly")
            fnGenerateTimeSheet();
        else
            dynamicallyCustom();
    }

    public void dynamicallyCustom()
    {
        try
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            string strQuery = "";
            
            
            HtmlDesign.Visible = true;

            strQuery = @"select u.* from users u where IsCW='Y'";
            
            if (ddlEmp.SelectedItem.Text != "--Select--")
            {
                strQuery += " and u.user_id='" + ddlEmp.SelectedValue.ToString() + "'";
            }
            strQuery += " order by username ";

            DataSet dsAllEmp = DatabaseHelper.getDataset(strQuery);
            sb.Append("<table width='100%' border='0' cellpadding='0' cellspacing='0'>");
            sb.Append("<tr>");
            sb.Append("<td height='37px' style='border:1px solid #999'>");
            sb.Append("<table border='0' width='100%' height='37px' cellpadding='0' cellspacing='0'><tr><td height='45px' rowspan='2' align='center'><b>DATE</b></td></tr></table>");
            sb.Append("</td>");
            for (int iEmp = 0; iEmp < dsAllEmp.Tables[0].Rows.Count; iEmp++)
            {
                sb.Append("<td height='37px' style='border:1px solid #999'>");
                sb.Append("<table border='0' width='100%' height='37px' cellpadding='0' cellspacing='0'><tr><td height='45px' rowspan='2' align='center'><b>"+ dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString()  +"</b></td></tr></table>");
                sb.Append("</td>");
            }
            sb.Append("</tr>");
            
           
                int days = 0;
            
                DateTime dtend =Convert.ToDateTime(GMDEndDate.Text.ToString());//DateTime.ParseExact(GMDEndDate.Text.ToString(), "dd/MM/yyyy", null);
                //==============================================

                try
                {
                    days = dtend.Subtract(Convert.ToDateTime(GMDStartDate.Text.ToString())).Days;//DateTime.ParseExact(GMDStartDate.Text.ToString(), "dd/MM/yyyy", null)).Days;//;
                }
                catch(Exception ex)
                {
                  DatabaseHelper.HandleException(ex,"Error on Date substraction","");
                }
               
                for (int i = 0; i <= days; i++)
                {
                    DateTime dateadd = new DateTime();
                    string strInout1 = "";
                
                    DateTime dtmatch;
              
                   dtmatch = Convert.ToDateTime(GMDStartDate.Text.ToString());// Ammended by R
                    //  dtmatch=  DateTime.ParseExact(GMDStartDate.Text.ToString(), "dd/MM/yyyy", null);
               
                    DateTime dtAddedDate = dtmatch.AddDays(i);
                    string EndDate = String.Format("{0:dd/MM/yyyy}", dtAddedDate);
                    string EndDate4q = dtAddedDate.ToString("dd/MM/yyyy", CultureInfo.CurrentUICulture);
                        
                    //string EndDate = Convert.ToDateTime(dateadd).ToString("dd/MM/yyyy");
                    //======================================================================
                    sb.Append("<tr>");
                    sb.Append("<td style='border:1px solid #999'>");
                    strInout1 = "<span style='color:black ;font-weight:Bold'>" + EndDate + "</span>&nbsp;";
                    sb.Append(strInout1);
                    sb.Append("</td>");

                    for (int iEmp = 0; iEmp < dsAllEmp.Tables[0].Rows.Count; iEmp++)
                    {
                        sb.Append("<td style='border:1px solid #999'>");
                        object objIntimeALL1 = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + EndDate4q + "'");
                        if (objIntimeALL1 != null && objIntimeALL1.ToString() != "")
                        {
                            try
                            {
                                DateTime dtALL1 = SUbTimeZones(Convert.ToDateTime(objIntimeALL1).AddHours(-4).AddMinutes(-30));
                                strInout1 = "<span style='color:green'>" + dtALL1.ToString("HH:mm") + "</span>" + "|";
                            }
                            catch(Exception ex)
                            { 
                                strInout1 = "<span style='color:green'>" +objIntimeALL1.ToString() + "</span>" + "|";
                                DatabaseHelper.HandleException(ex, "Error on time zone", "");
                            }

                        }
                        else
                        {
                            strInout1 = "-/";
                        }


                        object objOuttimeALL1 = DatabaseHelper.executeScalar("select Outtime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + EndDate4q + "'");
                        if (objOuttimeALL1 != null && objOuttimeALL1.ToString() != "")
                        {
                            //string strDate = (String)objOuttimeALL1;
                            //string[] strDateArray = strDate.Split(new char[] {'/' });
                            //strDate = strDateArray
                            try
                            {
                                DateTime dtoutALL1 = SUbTimeZones(Convert.ToDateTime(objOuttimeALL1).AddHours(-4).AddMinutes(-30));
                                strInout1 += "<span style='color:#0070c0'>" + dtoutALL1.ToString("HH:mm") + "</span>";
                            }
                            catch( Exception ex)
                            {
                                strInout1 += "<span style='color:#0070c0'>" + objOuttimeALL1.ToString() + "</span>";
                                DatabaseHelper.HandleException(ex, "Error on time zone", "");
                            }
                        }
                        else
                        {
                            strInout1 += "-";
                        }
                        sb.Append(strInout1);


                        object objTotalHours1 = DatabaseHelper.executeScalar("select TotalHrs from tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "'  and EodcDate='" + EndDate4q + "'");
                        if (objTotalHours1 != null && objTotalHours1.ToString() != "")
                        {
                            string totime = "";
                            string[] strbreak = objTotalHours1.ToString().Split(':');
                            totime = strbreak[0] + ":" + strbreak[1];
                            sb.Append("|" + "<span style='color:red'>" + totime + "</span>");
                        }
                        else
                        {
                            sb.Append("-");
                        }
                        sb.Append("</td>");
                    }
                    sb.Append("</tr>");
                }
                //string EndDate = Convert.ToDateTime(GMDStartDate.Text).ToString("yyyy/MM/dd");
               
                sb.Append("</table>");
                HtmlDesign.InnerHtml = sb.ToString();
         }
        catch (Exception ex)
        {

            DatabaseHelper.HandleException(ex, "Exception in LMS :Intime/Outtime Form", "");
        }
    }

    //public void TXtMON_No_TextChanged(object sender, GridViewCommandEventArgs e)
    //{

    //    string strstring = "";
    //    int state = 0;
        
    //    DateTime Intime;
    //    DateTime outtime;
    //    int index = int.Parse(e.CommandArgument.ToString());
    //    GridViewRow row = GridView1.Rows[index];
    //    TextBox TXtMON = (TextBox)row.FindControl("TXtMON");
    //    TextBox TXtMON1 = (TextBox)row.FindControl("TXtMON1");
    //    TextBox TXtMON2 = (TextBox)row.FindControl("TXtMON2");
    //    DateTime dtc = new DateTime();
    //    string format = "dd/MM/yyyy hh:mm tt";
    //    string timein = DateTime.Today.ToString("dd/MM/yyyy").Trim() + " " + TXtMON.Text.Trim();
    //    string timeout = DateTime.Today.ToString("dd/MM/yyyy").Trim() + " " + TXtMON1.Text.Trim();
    //    Intime = DateTime.ParseExact(timein, format, CultureInfo.InvariantCulture);
    //    outtime = DateTime.ParseExact(timeout, format, CultureInfo.InvariantCulture);
    //    TimeSpan timediff = outtime.Subtract(Intime);
    //    TXtMON2.Text  = timediff.ToString();
    //}

    protected void txt_TextChanged(object sender, EventArgs e)
    {
        TextBox txtmon = ((TextBox)GridView1.Rows[GridView1.EditIndex].FindControl("TXtMON"));
        TextBox txtmon1 = ((TextBox)GridView1.Rows[GridView1.EditIndex].FindControl("TXtMON1"));
        TextBox txtmon2 = ((TextBox)GridView1.Rows[GridView1.EditIndex].FindControl("TXtMON2"));

        if (txtmon1.Text.Trim() == "-/")
        {
            txtmon1.Text = "00:00";
        }
        else if (txtmon1.Text.Trim() == "-")
        {
            txtmon1.Text = "00:00";
        }

        if (txtmon2.Text.Trim() == "-/")
        {
            txtmon2.Text = "00:00";
        }
        else if (txtmon2.Text.Trim() == "-")
        {
            txtmon2.Text = "00:00";
        }

        DateTime dtc = new DateTime();
        DateTime Intime;
        DateTime outtime;
        string format = "dd/MM/yyyy hh:mm tt";
        DateTime timein = Convert.ToDateTime(DateTime.Today.ToString("dd/MM/yyyy").Trim() + " " + txtmon.Text.Trim()).ToUniversalTime();
        DateTime timeout = Convert.ToDateTime(DateTime.Today.ToString("dd/MM/yyyy").Trim() + " " + txtmon1.Text.Trim()).ToUniversalTime();
        Intime = timein;
        outtime = timeout;
        TimeSpan timediff = outtime.Subtract(Intime);
        txtmon2.Text = timediff.ToString();

    }

    protected void TXtTues_TextChanged(object sender, EventArgs e)
    {
        TextBox txtmon = ((TextBox)GridView1.Rows[GridView1.EditIndex].FindControl("TXtTues"));
        TextBox txtmon1 = ((TextBox)GridView1.Rows[GridView1.EditIndex].FindControl("TXtTues1"));
        TextBox txtmon2 = ((TextBox)GridView1.Rows[GridView1.EditIndex].FindControl("TXtTues2"));

        if (txtmon1.Text.Trim() == "-/")
        {
            txtmon1.Text = "00:00";
        }
        else if (txtmon1.Text.Trim() == "-")
        {
            txtmon1.Text = "00:00";
        }

        if (txtmon2.Text.Trim() == "-/")
        {
            txtmon2.Text = "00:00";
        }
        else if (txtmon2.Text.Trim() == "-")
        {
            txtmon2.Text = "00:00";
        }

        DateTime dtc = new DateTime();
        DateTime Intime;
        DateTime outtime;
        string format = "dd/MM/yyyy hh:mm tt";
        DateTime timein = Convert.ToDateTime(DateTime.Today.ToString("dd/MM/yyyy").Trim() + " " + txtmon.Text.Trim()).ToUniversalTime();
        DateTime timeout = Convert.ToDateTime(DateTime.Today.ToString("dd/MM/yyyy").Trim() + " " + txtmon1.Text.Trim()).ToUniversalTime();
        Intime = timein;
        outtime = timeout;
        TimeSpan timediff = outtime.Subtract(Intime);
        txtmon2.Text = timediff.ToString();

    }

    protected void TXtWed_TextChanged(object sender, EventArgs e)
    {
        TextBox txtmon = ((TextBox)GridView1.Rows[GridView1.EditIndex].FindControl("TXtWed"));
        TextBox txtmon1 = ((TextBox)GridView1.Rows[GridView1.EditIndex].FindControl("TXtWed1"));
        TextBox txtmon2 = ((TextBox)GridView1.Rows[GridView1.EditIndex].FindControl("TXtWed2"));

        if (txtmon1.Text.Trim() == "-/")
        {
            txtmon1.Text = "00:00";
        }
        else if (txtmon1.Text.Trim() == "-")
        {
            txtmon1.Text = "00:00";
        }

        if (txtmon2.Text.Trim() == "-/")
        {
            txtmon2.Text = "00:00";
        }
        else if (txtmon2.Text.Trim() == "-")
        {
            txtmon2.Text = "00:00";
        }

        DateTime dtc = new DateTime();
        DateTime Intime;
        DateTime outtime;
        string format = "dd/MM/yyyy hh:mm tt";
        DateTime timein = Convert.ToDateTime(DateTime.Today.ToString("dd/MM/yyyy").Trim() + " " + txtmon.Text.Trim()).ToUniversalTime();
        DateTime timeout = Convert.ToDateTime(DateTime.Today.ToString("dd/MM/yyyy").Trim() + " " + txtmon1.Text.Trim()).ToUniversalTime();
        Intime = timein;
        outtime = timeout;
        TimeSpan timediff = outtime.Subtract(Intime);
        txtmon2.Text = timediff.ToString();

    }

    protected void TXtThus_TextChanged(object sender, EventArgs e)
    {
        TextBox txtmon = ((TextBox)GridView1.Rows[GridView1.EditIndex].FindControl("TXtThus"));
        TextBox txtmon1 = ((TextBox)GridView1.Rows[GridView1.EditIndex].FindControl("TXtThus1"));
        TextBox txtmon2 = ((TextBox)GridView1.Rows[GridView1.EditIndex].FindControl("TXtThus2"));

        if (txtmon1.Text.Trim() == "-/")
        {
            txtmon1.Text = "00:00";
        }
        else if (txtmon1.Text.Trim() == "-")
        {
            txtmon1.Text = "00:00";
        }

        if (txtmon2.Text.Trim() == "-/")
        {
            txtmon2.Text = "00:00";
        }
        else if (txtmon2.Text.Trim() == "-")
        {
            txtmon2.Text = "00:00";
        }

        DateTime dtc = new DateTime();
        DateTime Intime;
        DateTime outtime;
        string format = "dd/MM/yyyy hh:mm tt";
        DateTime timein = Convert.ToDateTime(DateTime.Today.ToString("dd/MM/yyyy").Trim() + " " + txtmon.Text.Trim()).ToUniversalTime();
        DateTime timeout = Convert.ToDateTime(DateTime.Today.ToString("dd/MM/yyyy").Trim() + " " + txtmon1.Text.Trim()).ToUniversalTime();
        Intime = timein;
        outtime = timeout;
        TimeSpan timediff = outtime.Subtract(Intime);
        txtmon2.Text = timediff.ToString();

    }

    protected void TXtFrid_TextChanged(object sender, EventArgs e)
    {
        TextBox txtmon = ((TextBox)GridView1.Rows[GridView1.EditIndex].FindControl("TXtFrid"));
        TextBox txtmon1 = ((TextBox)GridView1.Rows[GridView1.EditIndex].FindControl("TXtFrid1"));
        TextBox txtmon2 = ((TextBox)GridView1.Rows[GridView1.EditIndex].FindControl("TXtFrid2"));

        if (txtmon1.Text.Trim() == "-/")
        {
            txtmon1.Text = "00:00";
        }
        else if (txtmon1.Text.Trim() == "-")
        {
            txtmon1.Text = "00:00";
        }

        if (txtmon2.Text.Trim() == "-/")
        {
            txtmon2.Text = "00:00";
        }
        else if (txtmon2.Text.Trim() == "-")
        {
            txtmon2.Text = "00:00";
        }

        DateTime dtc = new DateTime();
        DateTime Intime;
        DateTime outtime;
        string format = "dd/MM/yyyy hh:mm tt";
        DateTime timein = Convert.ToDateTime(DateTime.Today.ToString("dd/MM/yyyy").Trim() + " " + txtmon.Text.Trim()).ToUniversalTime();
        DateTime timeout = Convert.ToDateTime(DateTime.Today.ToString("dd/MM/yyyy").Trim() + " " + txtmon1.Text.Trim()).ToUniversalTime();
        Intime = timein;
        outtime = timeout;
        TimeSpan timediff = outtime.Subtract(Intime);
        txtmon2.Text = timediff.ToString();

    }

    protected void TXtSat_TextChanged(object sender, EventArgs e)
    {
        TextBox txtmon = ((TextBox)GridView1.Rows[GridView1.EditIndex].FindControl("TXtSat"));
        TextBox txtmon1 = ((TextBox)GridView1.Rows[GridView1.EditIndex].FindControl("TXtSat1"));
        TextBox txtmon2 = ((TextBox)GridView1.Rows[GridView1.EditIndex].FindControl("TXtSat2"));

        if (txtmon1.Text.Trim() == "-/")
        {
            txtmon1.Text= "00:00";
        }
        else if (txtmon1.Text.Trim() == "-")
        {
            txtmon1.Text = "00:00";
        }

        if (txtmon2.Text.Trim() == "-/")
        {
            txtmon2.Text = "00:00";
        }
        else if (txtmon2.Text.Trim() == "-")
        {
            txtmon2.Text = "00:00";
        }

        DateTime dtc = new DateTime();
        DateTime Intime;
        DateTime outtime;
        string format = "dd/MM/yyyy hh:mm tt";
        DateTime timein = Convert.ToDateTime(DateTime.Today.ToString("dd/MM/yyyy").Trim() + " " + txtmon.Text.Trim()).ToUniversalTime();
        DateTime timeout = Convert.ToDateTime(DateTime.Today.ToString("dd/MM/yyyy").Trim() + " " + txtmon1.Text.Trim()).ToUniversalTime();
        Intime = timein;
        outtime = timeout;
        TimeSpan timediff = outtime.Subtract(Intime);
        txtmon2.Text = timediff.ToString();

    }
    
}




