<%@ Page Language="C#" MasterPageFile="~/Admin/LMSAdmin/AdminMasterMenu.master" AutoEventWireup="true"
    CodeFile="LMS-Leave-setting.aspx.cs" Inherits="Admin_frmEmpLeaveDetails" Title="Leave setting" %>

<%@ Register Assembly="GMDatePicker" Namespace="GrayMatterSoft" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server" >

   <!--<div class="publicloginTable" style="height:81% ;background-color:#C2CFDC;" >

    <div style="float:left; width: 47%; height:100%" class="publicloginTable" id="leftdiv" runat="server">                            
    <table style="width: 94%" >
                      <tr>
                        <td colspan="6" class="LoginTitle"><h1 style="color:Black">
                            Set leave for year </h1>
                        </td>
                          
                           
                    </tr>
    <tr>
                        <td align="left" valign="top" colspan="3">&nbsp;
                            </td>
                    </tr>
                    <tr><td>Year </td><td>:</td><td>
                           
                            </td></tr>
                            <tr><td colspan="3"></td>
                         </tr>
                     <tr><td>Leave</td><td>:</td><td>
                        
                         </td></tr>
                          <tr><td colspan="3"></td>
                         </tr>
                 
                          <tr><td colspan="3"></td>
                         </tr>
                         <tr><td>&nbsp;</td><td>&nbsp;</td><td>
                            
                         </td></tr>
                         <tr><td colspan="3"></td>
                          <td colspan="3">
                           <asp:Label ID="lblSuccessMsg" runat="server"></asp:Label></td>
                         </tr>
                    </table>
                    <span>
        &nbsp;&nbsp;&nbsp; 
                        </span>
                    </div>
                    <div  style="display:none">
                    <table  width="50%"><tr><td colspan="4"> Add / Edit balance employee wise :</td></tr>
                    <tr><td width="25%">Select an employee</td><td width="5%">:</td><td>
        <span style="width: 18%; vertical-align:bottom">
            <asp:DropDownList ID="ddlEmpList" runat="server" 
                onselectedindexchanged="ddlEmpList_SelectedIndexChanged" 
                AutoPostBack="True">
            </asp:DropDownList>
         </span>
           
                        </td><td></td></tr>
                         <tr><td width="25%">Balance</td><td width="5%">:</td><td>
                         <asp:TextBox ID="txtLPY" runat="server" Width="50px" BorderColor="Silver" 
                                 ForeColor="#666699" ReadOnly="True"></asp:TextBox>
                         </td><td></td></tr>
                    <tr><td width="25%">Earn Balance</td><td width="5%">:</td><td>
                         <asp:TextBox ID="txtEB" runat="server" Width="50px"></asp:TextBox>
                         </td><td></td></tr>
                    <tr><td></td><td></td><td style="padding-top:15px" >
                             <asp:Button ID="btnSetEB" runat="server" Text="Submit" 
                                 onclick="btnSetEB_Click"/>
                         </td><td></td></tr>
                   <tr><td colspan="4">
                           <asp:Label ID="lblEBStatus" runat="server"></asp:Label></td></tr>
                    </table>
                    </div>
</div> --> 



<div class="tab-content-holder">
   <h2 style="font-weight:normal !important; font-size:20px !important; margin-top:5px; margin-bottom:-5px; padding-left:20px;">Leave Management System: Leave setting</h2>
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td width="11%" align="left" valign="top"><div class="treeview_div_left" >
            <ul id="treemenu2" class="treeview" style="padding-left:10px;">
              <li><a href="../LMSAdmin/LMS-Admin.aspx">LMS home</a></li>
              <li><a href="#" >Manage</a>
                <ul rel="open" style="display: block;">
                  <li><a href="../LMSAdmin/LMS-Leave-setting.aspx" class="settings">Annum</a></li>
                  <li><a href="../LMSAdmin/LMS-Add-a-leave.aspx" class="edit_leave">Emps</a></li>
                </ul>
              </li>
              <li><a href="#" >Reports</a>
                <ul rel="open" style="display: block;">
                  <li><a href="../LMSAdmin/LMS-Leaves-between-2dates-report.aspx" class="documents">All</a></li>
                  <li><a href="../LMSAdmin/LMS-Complete-leaves-report.aspx" class="leave_report">Detail</a></li>
                  <li><a href="../LMSAdmin/LMS-In-and-outtime-report.aspx" class="leave_report">In/outtime</a></li>
                  <li><a href="../LMSAdmin/work-from-home-report.aspx" class="leave_report">Work from home</a></li>
                </ul>
              </li>
            </ul>
          </div></td>
        <td width="78%" align="left" valign="top">
        	<table width="100%" cellpadding="0" cellspacing="0" border="0" style="margin:0px;">
            <tr>
              <td width="99%" valign="top">
              <div style="background-color:#f3f3f3; border:1px solid #999999; margin-top:25px; margin-right:20px; line-height:23px; padding-left:20px; height:820px;">
                  <table width="99%" cellpadding="0" cellspacing="0" border="0">
                    <tr valign="top">
                      <td width="100%">
                      <div>
                          <fieldset style="margin-top:10px;">
                            <legend style="font-size:16px; padding:0 2px;"> <strong>Set leave for year</strong></legend>
                            <table width="60%" cellpadding="0" cellspacing="0" border="0" align="left">
                              <tr>
                                <td height="25" align="left" valign="bottom" style="padding-left:5px;">Year&nbsp;&nbsp;<asp:DropDownList ID="ddlYear" runat="server" style=" width:105px; height:25px; padding:4px; border:1px solid #cccccc;" AutoPostBack="True" 
                                onselectedindexchanged="ddlYear_SelectedIndexChanged"></asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;Leave&nbsp;&nbsp;<asp:TextBox ID="txtTotalLeave" runat="server" style="width: 105px; border: 1px solid rgb(204, 204, 204); height: 21px; padding-left: 4px;" ></asp:TextBox>
                                &nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="btnSetLeave" runat="server" Text="Submit" onclick="btnSetLeave_Click" CssClass="blueBtns" style="margin-top:0px; margin-bottom:0px;"/></td>
                              </tr>
                              
                              <tr>
                               
                              </tr>
                            </table>
                          </fieldset>
                        </div></td>
                      <!--<td width="75%">&nbsp;</td>-->
                    </tr>
                  </table>
                </div></td>
            </tr>
          </table></td>
      </tr>
    </table>
    
    <script type="text/javascript">
        ddtreemenu.createTree("treemenu2", true, 5)

</script> 

  </div>
</asp:Content>
