﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LMS-Weekly-report.ascx.cs" Inherits="Admin_LMSAdmin_LMS_Weekly_report" %>
    <style type="text/css">
        .style1
        {
            width: 13%;
        }
    </style>
    <table style="width: 100%; padding: 10px; height: 373" class="publicloginTable" onkeypress="javascript:keypressHandler(event)">
   <%--     <tr>
            <td valign="top">
                Starting date
            </td>
            <td align="center" valign="top">
                :
            </td>
            <td valign="top" style="width: 287px">
                <asp:TextBox ID="GMDStartDate" runat="server" CssClass="input_boxline"></asp:TextBox>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="GMDStartDate"
                    Format="dd/MM/yyyy" PopupButtonID="GMDStartDate">
                </ajaxToolkit:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please select a start date"
                    ControlToValidate="GMDStartDate"></asp:RequiredFieldValidator>
                <br />
                <asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="Please enter a valid  date"
                    Operator="DataTypeCheck" ControlToValidate="GMDStartDate" Type="Date"></asp:CompareValidator>
            </td>
            <td style="width: 5%; height: 10px" valign="top">
                Ending date
            </td>
            <td align="center" style="width: 2%" valign="top">
                :
            </td>
            <td valign="top" class="style1">
                <asp:TextBox ID="GMDEndDate" runat="server" CssClass="input_boxline"></asp:TextBox>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="GMDEndDate"
                    Format="dd/MM/yyyy" PopupButtonID="GMDEndDate">
                </ajaxToolkit:CalendarExtender>
                <asp:CompareValidator ID="CompareValidator3" runat="server" ErrorMessage="Please enter a valid date"
                    Operator="DataTypeCheck" ControlToValidate="GMDEndDate" Type="Date"></asp:CompareValidator>
                <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="End date should be greater or equal to start date."
                    ControlToCompare="GMDStartDate" ControlToValidate="GMDEndDate" Operator="GreaterThanEqual"
                    Type="Date"></asp:CompareValidator>
                &nbsp;&nbsp;&nbsp;&nbsp;
            </td>
            <td>
            </td>
            <td valign="top">
                Department
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:DropDownList ID="ddlDept" runat="server" Height="18px" Width="201px">
                </asp:DropDownList>
            </td>
        </tr>--%>
        <tr>
            <td colspan="10">
                                    <table style="height: 300px; color: #333333; font-size: 26px;" width="100%">
                                        <tr align="center">
                                            
                                            <td>   Weekly Report</td>
                                            
                                        </tr>
                                        <tr>
                                            <td align="center" >
                                            
                                                <asp:Label ID="lbl_pageinfo" runat="server" Text="" Visible="false"></asp:Label>
                                                <asp:Repeater ID="rptAbsentEmpByWeek" runat="server" OnItemDataBound="rptAbsentEmpByWeek_ItemDataBound">
                                                    <ItemTemplate>
                                                        <table style="font-size: 15px" width="100%" border="1">
                                                            <tr>
                                                                <td colspan="2" style="font-size: 20px">
                                                                    <b>
                                                                        <%# String.Format("{0:D}",(Container.DataItem)) %>
                                                                        (<%# Convert.ToDateTime(Container.DataItem).DayOfWeek %>)</b>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <asp:Repeater ID="Repeater2" runat="server">
                                                                        <FooterTemplate>
                                                                            <%="</ul>" %>
                                                                        </FooterTemplate>
                                                                        <HeaderTemplate>
                                                                            <table style="font-size: 12px" width="100%" border="1">
                                                                            <tr style="font-weight:bold; font-size:14px">
                                                                            <td  width="8%">Employee name</td> <td  width="8%">Planned</td> <td  width="8%">Unplanned</td> <td  width="73%">Reason</td>
                                                                            </tr>
                                                                            </table>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                    
                                                                         <table style="font-size: 12px" width="100%" border="1">
                                                                                <tr>
                                                                                    <td width="8%">
                                                                                        <%#((System.Data.DataRow)Container.DataItem)[0] %>
                                                                                    </td>

                                                                                    <td width="8%">
                                                                                       <%#((System.Data.DataRow)Container.DataItem)[5] %>
                                                                                    </td>

                                                                                     <td width="8%">
                                                                                       <%#((System.Data.DataRow)Container.DataItem)[6] %>
                                                                                    </td>

                                                                                    <td width="73%">
                                                                                        <%# ((System.Data.DataRow)Container.DataItem)[2]%>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </td>
                                        </tr>
                                    </table>
            </td>
        </tr>
    </table>
