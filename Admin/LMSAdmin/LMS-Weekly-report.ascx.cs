﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Linq;

public partial class Admin_LMSAdmin_LMS_Weekly_report : System.Web.UI.UserControl
{


    clsEmployee_Logic1 objEmployee = new clsEmployee_Logic1();
    clsLeave_Logic objLeave = new clsLeave_Logic();
    private string strError = "No records found.";
    static DataSet dsAbsentDates = new DataSet();
    static DataView dv = null;
    static string sortByAndArrangeBy = "";
    static double SdblUsed = 0.0;
    string dtSDate;
    string dtEDate;
    DateTime datetime;
    DataSet dsTemp;
    DataTable dtTemp, dtTemp1;
    DateTime dtStartingDate ;  // Monday
    DateTime dtEndingDate;
    static int DayNo = 0;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            BindData1();
            fnSetDefaultDate();
            if (dtTemp != null)
            {
                if (dtTemp.Rows.Count > 0)
                {
                    rptAbsentEmpByWeek.DataSource = (from x in dtTemp1.AsEnumerable() select x["AbsentDate"]).Distinct();
                    rptAbsentEmpByWeek.DataBind();
                }
            }
        }

    }

    public void fnSetDefaultDate()
    {

        // lblError.Visible = false;
        String DayName = DateTime.Now.DayOfWeek.ToString();
      
        if (DayName == "Monday")
        {
            DayNo = 1;
        }
        else if (DayName == "Tuesday")
        {
            DayNo = 2;
        }

        else if (DayName == "Wednesday")
        {
            DayNo = 3;
        }

        else if (DayName == "Thursday")
        {
            DayNo = 4;
        }
        else if (DayName == "Friday")
        {
            DayNo = 5;
        }


      dtStartingDate = DateTime.Today.AddDays(-DayNo + 1);    // Monday
      dtEndingDate = DateTime.Today.AddDays(5 - DayNo);      //Friday
    }

    void BindData1()
    {
        try
        {
           // TextBox txt = this.Parent.FindControl("txtVisitorName") as TextBox;
           // txt.Text = "Krishna"; //u can set ur value here
            TextBox d1= this.Parent.FindControl("GMDStartDate") as TextBox;
            TextBox d2= this.Parent.FindControl("GMDEndDate") as TextBox;
       

            if (d1.Text.Trim() == "")
            {
                dtSDate = DateTime.Today.AddDays(-DayNo + 1).ToString("dd/MM/yyyy");  //FirstDayOfYear(dt).ToString("dd/MM/yyyy");
                d1.Text = dtSDate;
                dtStartingDate = DateTime.Today.AddDays(-DayNo + 1); 
            }
            else
            {
                dtEndingDate = Convert.ToDateTime(d1.Text);
            }

            if (d2.Text.Trim() == "")
            {
                dtEDate = DateTime.Today.AddDays(5 - DayNo).ToString("dd/MM/yyyy"); //LastDayOfYear(dt).ToString("dd/MM/yyyy");
                d2.Text = dtEDate;
                dtEndingDate = DateTime.Today.AddDays(5 - DayNo);
            }
            else
            {
                dtEndingDate = Convert.ToDateTime(d2.Text);
            }

            DataSet dsTemp = objLeave.getEmployeeAbsentDetailsByDayInGroup(dtStartingDate, dtEndingDate);
            dtTemp = dsTemp.Tables[0];

            List<string> strDateArray = new List<string>();

            if (dtTemp != null)
            {
                if (dtTemp.Rows.Count > 0)
                {
                    dtTemp.Columns.Add("Planned", typeof(string));
                    dtTemp.Columns.Add("Unplanned", typeof(string));
                    dtTemp1 = dtTemp.Clone();

                    foreach (DataColumn dc in dtTemp.Columns)
                    {
                        if (dc.ColumnName == "AbsentDate")
                        {
                            dc.DataType = typeof(DateTime);
                        }
                    }

                    foreach (DataRow dr in dtTemp.Rows)
                    {
                        DateTime ldt = Convert.ToDateTime(dr["AbsentDate"].ToString());
                        string strReason = dr["Reason"].ToString();
                        dr["AbsentDate"] = ldt.ToString("yyyy/MM/dd hh:mm:ss.fff");
                        dr["Reason"] = strReason.Replace("@", "'").Trim();
                        if (dr["IsPlanned"].ToString().Trim() == "True")
                        {
                            dr["Planned"] = "Yes";
                            dr["Unplanned"] = "No";
                            dr["Reason"] = "";
                        }
                        else
                        {
                            dr["Planned"] = "No";
                            dr["Unplanned"] = "Yes";
                        }
                        dtTemp1.ImportRow(dr);
                    }

                }

                //End Datatype conversion for AbsentDate Column in dtTemp
            }
            else
            {
                // this.lbl_pageinfo.Text = "No one was absent in last 5 days";
                //  this.lbl_pageinfo.Visible = true;
            }

        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, "Exception in LMS frmAdminHome.aspx.cs", "Method : Binddata()");
        }
    }
    protected void rptAbsentEmpByWeek_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item |
                     e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater rptr = (Repeater)e.Item.FindControl("Repeater2");
                rptr.DataSource = dtTemp1.AsEnumerable().Where(x => x["AbsentDate"].Equals(e.Item.DataItem));
                rptr.DataBind();
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, "Exception in LMS frmAdminHome.aspx.cs", "Event : rptAbsentEmpByWeek_ItemDataBound()");
        }
    }

    static DateTime LastDayOfYear()
    {
        return LastDayOfYear(DateTime.Today);
    }

    static DateTime LastDayOfYear(DateTime d)
    {
        // 1
        // Get first of next year
        DateTime n = new DateTime(d.Year + 1, 1, 1);
        // 2
        // Subtract 1 from it
        return n.AddDays(-1);
    }

    static DateTime FirstDayOfYear()
    {
        return FirstDayOfYear(DateTime.Today);
    }

    static DateTime FirstDayOfYear(DateTime y)
    {
        return new DateTime(y.Year, 1, 1);
    }

}