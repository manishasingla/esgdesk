<%@ Page Language="C#" MasterPageFile="~/Admin/LMSAdmin/AdminMasterMenu.master" AutoEventWireup="true"
    CodeFile="LMS-Admin.aspx.cs" Inherits="Admin_frmAdminHome" Title="Leave Management System" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<script language="javascript" type="text/javascript">
    function hideCalendar() {
        $find("<%=CalendarExtender1.ClientID%>").hide();
        $find("<%=CalendarExtender2.ClientID%>").hide();
        return false;
    } 
</script>
  <div class="tab-content-holder">
    <h2 style="font-weight:normal !important; font-size:20px !important; margin-top:5px; margin-bottom:-5px; padding-left:20px;">LMS dashboard</h2>
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td width="11%" align="left" valign="top"><div class="treeview_div_left">
            <ul id="treemenu2" class="treeview" style="padding-left:10px;">
              <li><a href="../LMSAdmin/LMS-Admin.aspx">LMS home</a></li>
              <li><a href="#" >Manage</a>
                <ul rel="open" style="display: block;">
                  <li><a href="../LMSAdmin/LMS-Leave-setting.aspx" class="settings">Annum</a></li>
                  <li><a href="../LMSAdmin/LMS-Add-a-leave.aspx" class="edit_leave">Emps</a></li>
                  <li><a href="../LMSAdmin/SaturdaySundayLeave.aspx" class="edit_leave">Weekend</a></li>
                </ul>
              </li>
              <li><a href="#" >Reports</a>
                <ul rel="open" style="display: block;">
                  <li><a href="../LMSAdmin/LMS-Leaves-between-2dates-report.aspx"
                            class="documents">All</a></li>
                  <li><a href="../LMSAdmin/LMS-Complete-leaves-report.aspx"
                            class="leave_report">Detail</a></li>
                  <li><a href="../LMSAdmin/LMS-In-and-outtime-report.aspx"
                            class="leave_report">In/outtime</a></li>
                  <li><a href="../LMSAdmin/LeaveReport.aspx"
                            class="leave_report">Leave Report</a></li>
                            <li><a href="../LMSAdmin/work-from-home-report.aspx" class="leave_report">Work from home</a></li>
                </ul>
              </li>
            </ul>
          </div></td>
        <td width="78%" align="left" valign="top">
        <table width="100%" cellpadding="0" cellspacing="0" border="0">            
        	<tr>
              <td width="99%" valign="top"><div style="background-color:#f3f3f3; border:1px solid #999999; margin-top:25px; margin-right:20px; line-height:23px; padding-left:20px; height:auto;">
                  <table width="99%" cellpadding="0" cellspacing="0" border="0">
                    <tr valign="top">
                      <td width="15%">
                      	<div>
                          <fieldset style="margin-top:10px;">
                          <legend style="font-size:16px; padding:0 2px;"><strong>Custom</strong></legend>
                            <table width="100%" cellpadding="0" cellspacing="0" border="0" align="left">
                              <tr>
                                <td height="25" align="left" valign="middle" style="padding-left:5px;" width="30%">Interval&nbsp;&nbsp;<asp:DropDownList ID="ddlTimeInterval" runat="server" valign="top" style="width:105px; height:25px; border:1px solid #cccccc; padding:4px;" onselectedindexchanged="ddlTimeInterval_SelectedIndexChanged" AutoPostBack="True" >
                                    <asp:ListItem Value="1">Today</asp:ListItem>
                                    <asp:ListItem Value="2">This week</asp:ListItem>
                                    <asp:ListItem Value="3">Last 7 days</asp:ListItem>
                                    <asp:ListItem Value="4">This month</asp:ListItem>
                                    <asp:ListItem Value="5">Last 31 days</asp:ListItem>
                                  </asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;From&nbsp;&nbsp;
                                <asp:TextBox ID="GMDStartDate" runat="server" CssClass="input_boxline" style="width:95px; height:22px; text-align:center;" CausesValidation="True" ValidationGroup="g1" ></asp:TextBox>
                            <%--    <img id="Image1" runat="server" alt="Date" src="~/Admin/LMSAdmin/images/cs.png"/>--%>
                              <%--    <asp:ImageButton runat="Server" ID="Image1" ImageUrl="~/Admin/LMSAdmin/images/cs.png" AlternateText="Click to show calendar"/> --%>
                                  <ajaxtoolkit:calendarextender ID="CalendarExtender1"  runat="server"  TargetControlID="GMDStartDate" OnClientDateSelectionChanged='hideCalendar'
                                  Format="dd/MM/yyyy"> </ajaxtoolkit:calendarextender>&nbsp;&nbsp;&nbsp;&nbsp;To&nbsp;&nbsp;<asp:TextBox ID="GMDEndDate" runat="server" CssClass="input_boxline" 
                           AutoPostBack="True" ontextchanged="GMDEndDate_TextChanged" style="width:95px; height:22px; text-align:center;"  CausesValidation="True" ValidationGroup="g1" ></asp:TextBox>
                    <%-- <asp:ImageButton runat="Server" ID="Image2" ImageUrl="~/Admin/LMSAdmin/images/cs.png" AlternateText="Click to show calendar"    OnClientClick="return false" /> --%>
                   <%-- <img id="Image2" runat="server" alt="Date" src="~/Admin/LMSAdmin/images/cs.png" />--%>
                                  <ajaxToolkit:CalendarExtender ID="CalendarExtender2"  runat="server"  TargetControlID="GMDEndDate" OnClientDateSelectionChanged='hideCalendar'
                                   Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender></td>
                                   
                              </tr>
                          
                              <tr>
                                <td align="left">
                               </td>
                              </tr>
                            </table>
                           
                          </fieldset>
                        </div></td>
                        
                        
                    </tr>
                    <tr>
                    <td width="85%"><div style="margin-bottom:20px; ">
                          <fieldset style="height:auto; margin-top:10px; line-height:15px;">
                            <legend style="font-size:16px; padding:0 2px;"><strong>At a glance</strong></legend>                              
                            <div style="height:auto">
                            <table style=" color: #333333; font-size: 26px;" width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr valign="top">
                                  <td Width="50%">
                                  <asp:Label ID="lblLR4R1Title" runat="server" style="font-size:20px;"></asp:Label><br /><br />
                                          <asp:Repeater ID="rptAbsentEmpByWeek" runat="server" OnItemDataBound="rptAbsentEmpByWeek_ItemDataBound">
                                      <ItemTemplate>
                                        <table style="font-size: 15px" width="100%" cellpadding="0" cellspacing="0" border="0">
                                          <tr>
                                            <td><b> <%# String.Format("{0:D}",(Container.DataItem)) %> (<%# Convert.ToDateTime(Container.DataItem).DayOfWeek %>)</b></td>
                                          </tr>
                                          <tr><td style="height:10px"></td></tr>
                                          <tr>
                                            <td><asp:Repeater ID="Repeater2" runat="server" OnItemDataBound="RepeaterIn1_ItemDataBound">
                                                <FooterTemplate> <%="</ul>" %> </FooterTemplate>
                                                <HeaderTemplate>
                                                  <!--<table style="font-size: 12px" width="100%">
                                                  </table>-->
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                  <table style="font-size: 12px" width="100%" cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                      <td width="17%" valign="top"><%#((System.Data.DataRow)Container.DataItem)[0] %></td>
                                                      <td width="69%" valign="top"><%# ((System.Data.DataRow)Container.DataItem)[2]%></td>
                                                      <td width="7%" valign="top" style="padding-left:5px;" id="tdPaidOUP" runat="server"><img style="margin-top:4px;" id="imgPaidOUP" runat="server"  alt='<%# ((System.Data.DataRow)Container.DataItem)[3]%>' /></td>
                                                      <td width="7%" valign="top" runat="server" id="tdPOUP"><img style="margin-top:4px;" id="imgPOUP" runat="server" alt='<%# ((System.Data.DataRow)Container.DataItem)[4]%>'/></td>
                                                    
                                                    </tr>
                                                    <tr><td style="height:10px" colspan="4"></td></tr>
                                                  </table>
                                                </ItemTemplate>
                                              </asp:Repeater></td>
                                          </tr>
                                        </table>
                                      </ItemTemplate>
                                    </asp:Repeater>
                                       <span style="font-size:12px"> <asp:Label ID="lbl_pageinfo" runat="server" Text="" Visible="false" ></asp:Label></span> <br />

                                    </td>

                                <%--upcoming 3 week's leaves--%>
                                    <td width="50%"> 
                                    <asp:Label ID="lblLR4U3WTitle" runat="server" Text="Upcoming 3 week's leave report" style="font-size:20px;"></asp:Label><br /><br />
                                    <asp:Repeater ID="rptLR4U3W" runat="server" onitemdatabound="rptLR4U3W_ItemDataBound">
                                      <ItemTemplate>
                                        <table style="font-size: 15px" width="100%" cellpadding="0" cellspacing="0" border="0">
                                          <tr>
                                            <td colspan="2"><b> <%# String.Format("{0:D}",(Container.DataItem)) %> (<%# Convert.ToDateTime(Container.DataItem).DayOfWeek %>)</b></td>
                                          </tr>
                                           <tr><td style="height:10px"></td></tr>
                                          <tr>
                                            <td colspan="2"><asp:Repeater ID="Repeater3" runat="server"  onitemdatabound="RepeaterIn2_ItemDataBound">
                                                <FooterTemplate> <%="</ul>" %> </FooterTemplate>
                                                <HeaderTemplate>
                                                  <table style="font-size: 12px" width="100%" cellpadding="0" cellspacing="0" border="0">
                                                  </table>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                  <table style="font-size: 12px" width="100%" cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                      <td width="17%" valign="top"><%#((System.Data.DataRow)Container.DataItem)[0] %></td>
                                                      <td width="69%" valign="top"><%# ((System.Data.DataRow)Container.DataItem)[2]%></td>
                                                      <td width="7%" valign="top" style="padding-left:5px;" id="tdPaidOUP" runat="server"><img style="margin-top:4px;" id="imgPaidOUP" runat="server" alt='<%# ((System.Data.DataRow)Container.DataItem)[3]%>'  /></td>
                                                      <td width="7%" valign="top" runat="server" id="tdPOUP"><img style="margin-top:4px;" id="imgPOUP" runat="server" alt='<%# ((System.Data.DataRow)Container.DataItem)[4]%>'/></td>
                                                    
                                                    </tr>
                                                     <tr><td style="height:10px" colspan="4"></td></tr>

                                                  </table>
                                                </ItemTemplate>
                                              </asp:Repeater></td>
                                          </tr>
                                        </table>
                                      </ItemTemplate>
                                    </asp:Repeater>
                                    <span style="font-size:12px"> <asp:Label ID="lblLR4U3W" runat="server" Text="" Visible="false" ></asp:Label></span>
                                    </td>
                                <%--upcoming 3 week's leaves--%>
                                </tr>
                              
                            </table></div>
                          </fieldset>
                        </div></td>
                    </tr>
                  </table>
                </div></td>
            </tr>
          </table></td>
      </tr>
    </table>
    <script type="text/javascript">
        ddtreemenu.createTree("treemenu2", true, 5)

        validatePage = function (button)
         {
            var isValid = Page_ClientValidate();
            button.disabled = isValid;

            //return validation - if true then postback
            return isValid;
        }
</script> 
 <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableClientScript="true" ShowMessageBox="true" ShowSummary="false" ValidationGroup="g1"  DisplayMode="BulletList"  />
  <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="g1"
                                Display="None" ErrorMessage="Please select a start date" ControlToValidate="GMDStartDate">*
                                 </asp:RequiredFieldValidator>
                                  <br/>
                                  <asp:CompareValidator ID="CompareValidator2" runat="server" Display="None"  ValidationGroup="g1"
                            ErrorMessage="Please enter a valid  date" Operator="DataTypeCheck" 
                            ControlToValidate="GMDStartDate" Type="Date">*</asp:CompareValidator>
                                  <br/>
                                  <asp:CompareValidator ID="CompareValidator3" runat="server" Display="None"   ValidationGroup="g1"
                            ErrorMessage="Please enter a valid date" Operator="DataTypeCheck" 
                            ControlToValidate="GMDEndDate" Type="Date">*</asp:CompareValidator>
                                  <br/>
                                  <asp:CompareValidator ID="CompareValidator1" runat="server" Display="None"  ValidationGroup="g1"
                                ErrorMessage="End date should be greater or equal to start date." 
                                ControlToCompare="GMDStartDate" ControlToValidate="GMDEndDate" 
                                Operator="GreaterThanEqual" Type="Date">*</asp:CompareValidator>

                              
        



  </div>
</asp:Content>
