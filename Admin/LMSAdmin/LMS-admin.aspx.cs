using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Globalization;

public partial class Admin_frmAdminHome : System.Web.UI.Page
{
    DateTime dtStartingDate;
    DateTime dtEndingDate;
    clsLeave_Logic objLeave = new clsLeave_Logic();
    DataTable dtTemp = null;
    DataTable dtTemp1 = null, dtLR4R1 = null, dtLR4R2 = null;
    public int DayNo = 0, DayToAdd = 0;
    public bool blnIsDateInterval = false;
    //DataView dv2 = new DataView();

    clsEmployee_Logic1 objEmployee = new clsEmployee_Logic1();
    string strError = "No records found";
    List<DateTime> dates = new List<DateTime>();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] != null && Session["admin"].ToString() != "")
        {

            if (DatabaseHelper.isAdmin(Session["admin"].ToString()) == false)
            {
                Response.Redirect("~/Admin/login.aspx");
                return;
            }
        }

        if (!IsPostBack)
        {
            ddlTimeInterval.SelectedIndex = 1;
            fnSetDefaultDate();
            Session["TI"] = "TW";
            lblLR4R1Title.Text = "Leave report for this week";
            fnBindLR4Reater1();
            fnBindLR4Repeater2();
       
        }
    }


    void fnBindLR4Reater1()
    {
        try
        {
            // lblError.Visible = false;
            if (blnIsDateInterval == false)
            {
                if (Session["TI"] == null)
                {
                    dtStartingDate = DateTime.Today.AddDays(DayNo);
                    dtEndingDate = DateTime.Today;
                }

                else
                {
                    if (Session["TI"] == "TW")
                    {
                        fnSetDefaultDate();
                    }
                    else if (Session["TI"] == "TM")
                    {
                        DateTime today = DateTime.Today;
                        DateTime FirstDate = new DateTime(today.Year, today.Month, 1);
                        today = new DateTime(today.Year, today.Month + 1, 1);
                        DateTime LastDate = today.AddDays(-1);
                        dtStartingDate = FirstDate;
                        dtEndingDate = LastDate;
                    }
                }
                GMDStartDate.Text = dtStartingDate.ToString("dd/MM/yyyy");
                GMDEndDate.Text = dtEndingDate.ToString("dd/MM/yyyy");
            }
            else
            {
                if (GMDStartDate.Text.Trim() == "")
                {
                    GMDStartDate.Text = dtStartingDate.ToString();
                }
                else
                {
                    dtStartingDate = Convert.ToDateTime(GMDStartDate.Text);
                }

                if (GMDEndDate.Text.Trim() == "")
                {
                    GMDEndDate.Text = dtEndingDate.ToString();
                }
                else
                {
                    dtEndingDate = Convert.ToDateTime(GMDEndDate.Text);
                }
            }

            dtLR4R1 = fnGetLeaveRecord(dtStartingDate, dtEndingDate);
            DataView dv1 = new DataView(dtLR4R1);
            
            if (Session["TI"] != null && Session["TI"].ToString().Trim() != ""&& Session["TI"].ToString().Trim()=="TW")
            {
                DateTime dt = DateTime.Today.AddDays(1); //Convert.ToDateTime(strArrayDOA[i]);
                string exp = "AbsentDate<=#" + dt.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture) + "#";// and Username='" + username + "'";
                dv1.RowFilter = exp;
            } 
           
            if (dv1 != null && dv1.ToTable().Rows.Count > 0)
            {

                rptAbsentEmpByWeek.Visible = true;
                this.lbl_pageinfo.Visible = false;
                rptAbsentEmpByWeek.DataSource = (from x in dv1.ToTable().AsEnumerable() select x["AbsentDate"]).Distinct();
                rptAbsentEmpByWeek.DataBind();
            }
            else
            {
                this.lbl_pageinfo.Text = "Record(s) are not found.";
                this.lbl_pageinfo.Visible = true;
                rptAbsentEmpByWeek.Visible = false;
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, "Exception in LMS-Admin.aspx.cs", "Method : fnBindLR4Reater1()");
        }

    }


    void fnBindLR4Repeater2()
    {
        try
        {
            fnSetDefaultDate();
            dtStartingDate = DateTime.Today.AddDays(1);//.AddDays(DayToAdd); //DateTime.Today.AddDays(DayToAdd);
            dtEndingDate = dtStartingDate.AddDays(20);
            dtLR4R2 = fnGetLeaveRecord(dtStartingDate, dtEndingDate);
            DataView dv2 = new DataView(dtLR4R2);
            //if (Session["TW"] != null && Session["TW"].ToString() != "")
            //{
            //    DateTime dt = Convert.ToDateTime(GMDEndDate.Text); //Convert.ToDateTime(strArrayDOA[i]);
            //    string exp = "AbsentDate>=#" + dt.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture) + "#";// "# And AbsentDate<=#" + dt.AddDays(20).ToString("yyyy/MM/dd", CultureInfo.InvariantCulture)+"#";// and Username='" + username + "'";
            //    dv2.RowFilter = exp;
            //}
          

            if (dv2 != null && dv2.ToTable().Rows.Count > 0)
            {
                rptLR4U3W.Visible = true;
                this.lblLR4U3W.Visible = false;
                rptLR4U3W.DataSource = (from x in dv2.ToTable().AsEnumerable() select x["AbsentDate"]).Distinct();
                rptLR4U3W.DataBind();
            }
            else
            {
                this.lblLR4U3W.Text = "Record(s) for upcoming 3 weeks are not found.";
                this.lblLR4U3W.Visible = true;
                rptLR4U3W.Visible = false;
            }
        }
        catch
        {
        }

    }


    static DateTime LastDayOfYear()
    {
        return LastDayOfYear(DateTime.Today);
    }

    static DateTime LastDayOfYear(DateTime d)
    {
        // 1
        // Get first of next year
        DateTime n = new DateTime(d.Year + 1, 1, 1);
        // 2
        // Subtract 1 from it
        return n.AddDays(-1);
    }

    public DataTable fnGetLeaveRecord(DateTime dt1, DateTime dt2)
    {
        try
        {
            DataSet dsTemp = objLeave.getEmployeeAbsentDetailsByDayInGroup(dt1, dt2);

            if (dsTemp != null && dsTemp.Tables.Count > 0 && dsTemp.Tables[0].Rows.Count > 0)
            {
                dtTemp = dsTemp.Tables[0];

                List<string> strDateArray = new List<string>();

                if (dtTemp != null && dtTemp.Rows.Count > 0)
                {
                    dtTemp1 = dtTemp.Clone();

                    foreach (DataColumn dc in dtTemp.Columns)
                    {
                        if (dc.ColumnName == "AbsentDate")
                        {
                            dc.DataType = typeof(DateTime);
                        }

                    }

                    foreach (DataRow dr in dtTemp.Rows)
                    {
                        DateTime ldt = Convert.ToDateTime(dr["AbsentDate"].ToString());
                        string strReason = dr["Reason"].ToString();
                        dr["AbsentDate"] = ldt.ToString("yyyy/MM/dd hh:mm:ss.fff");
                        dr["Reason"] = strReason.Replace("@", "'").Trim();
                        dtTemp1.ImportRow(dr);
                    }

                    //End Datatype conversion for AbsentDate Column in dtTemp
                }
                else
                {
                    dtTemp1 = null;
                }
            }
            else
            {

                dtTemp1 = null;
            }
        }
        catch
        {
            dtTemp1 = null;
        }

        return dtTemp1;

    }


    protected void ddlTimeInterval_SelectedIndexChanged(object sender, EventArgs e)
    {
        blnIsDateInterval = false;

        if (ddlTimeInterval.SelectedValue.ToString() == "1") //Today
        {
            DayNo = 0;
            Session["TI"] = null;
            lblLR4R1Title.Text = "Leave report for today";
        }

        else if (ddlTimeInterval.SelectedValue.ToString() == "3")//Last 7 days 
        {
            DayNo = -7;
            Session["TI"] = null;
            lblLR4R1Title.Text = "Leave report for last 7 days";
        }


        else if (ddlTimeInterval.SelectedValue.ToString() == "5")// Last 31 days
        {
            DayNo = -31;
            Session["TI"] = null;
            lblLR4R1Title.Text = "Leave report for last 31 days";
        }

        else if (ddlTimeInterval.SelectedValue.ToString() == "4")// This month
        {
            Session["TI"] = "TM";
            lblLR4R1Title.Text = "Leave report for this month";
        }

        else if (ddlTimeInterval.SelectedValue.ToString() == "2") //This Week
        {
            Session["TI"] = "TW";
            lblLR4R1Title.Text = "Leave report for this week";
        }
        fnBindLR4Reater1();
       // fnBindLR4Repeater2();
    }


    public void fnSetDefaultDate()
    {

        // lblError.Visible = false;
        String DayName = DateTime.Now.DayOfWeek.ToString();

        if (DayName == "Monday")
        {
            DayNo = 1;
            DayToAdd = 7;
        }
        else if (DayName == "Tuesday")
        {
            DayNo = 2;
            DayToAdd = 6;
        }

        else if (DayName == "Wednesday")
        {
            DayNo = 3;
            DayToAdd = 5;
        }

        else if (DayName == "Thursday")
        {
            DayNo = 4;
            DayToAdd = 4;
        }
        else if (DayName == "Friday")
        {
            DayNo = 5;
            DayToAdd = 3;
        }
        else if (DayName == "Saturday")
        {
            DayNo = 6;
            DayToAdd = 2;
        }

        dtStartingDate = DateTime.Today.AddDays(-DayNo + 1);    // Monday
        dtEndingDate = DateTime.Today.AddDays(6 - DayNo);      //Saturday

        GMDStartDate.Text = dtStartingDate.ToString("dd/MM/yyyy");
        GMDEndDate.Text = dtEndingDate.ToString("dd/MM/yyyy");
    }

    protected void GMDEndDate_TextChanged(object sender, EventArgs e)
    {
        CompareValidator1.Validate();
        blnIsDateInterval = true;
        fnBindLR4Reater1();
        fnBindLR4Repeater2();
    }

    protected void rptAbsentEmpByWeek_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item | e.Item.ItemType == ListItemType.AlternatingItem)
            {

                Repeater rptr = (Repeater)e.Item.FindControl("Repeater2");
                rptr.DataSource =
                dtLR4R1.AsEnumerable().Where(x => x["AbsentDate"].Equals(e.Item.DataItem));
                rptr.DataBind();
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, "Exception in LMS-Admin.aspx.cs", "Event : rptAbsentEmpByWeek_ItemDataBound()");
        }
    }

    protected void rptLR4U3W_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item | e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater rptr = (Repeater)e.Item.FindControl("Repeater3");
                rptr.DataSource =
                    dtLR4R2.AsEnumerable().Where(x => x["AbsentDate"].Equals(e.Item.DataItem));
                rptr.DataBind();
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, "Exception in LMS-Admin.aspx.cs", "Event : rptLR4U3W_ItemDataBound()");
        }
    }

    protected void RepeaterIn1_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item | e.Item.ItemType == ListItemType.AlternatingItem)
            {

                HtmlTableCell td1 = (HtmlTableCell)e.Item.FindControl("tdPaidOUP");
                HtmlTableCell td2 = (HtmlTableCell)e.Item.FindControl("tdPOUP");

                if (td1 != null)
                {
                    HtmlImage img1 = (HtmlImage)e.Item.FindControl("imgPaidOUP");

                    if (img1.Alt.ToString() == "Paid")
                        img1.Src = "~/Admin/images/paid.png";
                    else
                        img1.Src = "~/Admin/images/unpaid.png";
                }

                if (td2 != null)
                {
                    HtmlImage img2 = (HtmlImage)e.Item.FindControl("imgPOUP");

                    if (img2.Alt.ToString() == "True")
                        img2.Src = "~/Admin/images/planned.png";
                    else
                        img2.Src = "~/Admin/images/unplanned.png";
                }
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, "Exception in LMS-Admin.aspx.cs", "Event : RepeaterIn1_ItemDataBound()");
        }
    }


    protected void RepeaterIn2_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item | e.Item.ItemType == ListItemType.AlternatingItem)
            {

                HtmlTableCell td1 = (HtmlTableCell)e.Item.FindControl("tdPaidOUP");
                HtmlTableCell td2 = (HtmlTableCell)e.Item.FindControl("tdPOUP");

                if (td1 != null)
                {
                    HtmlImage img1 = (HtmlImage)e.Item.FindControl("imgPaidOUP");

                    if (img1.Alt.ToString() == "Paid")
                    {
                        img1.Src = "../images/paid.png";
                        //img1.Alt = "Paid";
                    }
                    else
                    {
                        img1.Src = "../images/unpaid.png";
                       // img1.Alt = "Unpaid";
                    }
                }

                if (td2 != null)
                {
                    HtmlImage img2 = (HtmlImage)e.Item.FindControl("imgPOUP");

                    if (img2.Alt.ToString() == "True")
                    {
                        img2.Src = "../images/planned.png";
                       // img2.Alt = "planned";
                    }
                    else
                    {
                        img2.Src = "../images/unplanned.png";
                       // img2.Alt = "unplanned";
                    }
                }
            }

            }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, "Exception in LMS-Admin.aspx.cs", "Event : RepeaterIn2_ItemDataBound()");
        }
    }
}


