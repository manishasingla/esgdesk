<%@ Page Language="C#" MasterPageFile="~/Admin/LMSAdmin/AdminMasterMenu.master" AutoEventWireup="true" CodeFile="frmAdminHome.aspx.cs" Inherits="Admin_frmAdminHome" Title="Leave Management System" %>
 <%@ Import  Namespace="System.Data" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="divBorderhm">
    <table style="height:300px; color:#333333; font-size:26px;" width="100%"><tr><td align="center">At a glance</td></tr>
    <tr>
    <td>
       
<asp:Label ID="lbl_pageinfo" runat="server" Text="" Visible="false"></asp:Label>
           <asp:Repeater ID="rptAbsentEmpByWeek" runat="server" OnItemDataBound="rptAbsentEmpByWeek_ItemDataBound">
          <ItemTemplate>
          <table style="font-size:15px" width="100%">
          <tr>
          
                <td colspan="2" font-size:20px"><b><%# String.Format("{0:D}",(Container.DataItem)) %> (<%# Convert.ToDateTime(Container.DataItem).DayOfWeek %>)</b></td>
                  </tr>
                  
                  <tr>
                  <td colspan="2">
                  <asp:Repeater ID="Repeater2" runat="server">
                 
                      <FooterTemplate>
                          <%="</ul>" %>
                      </FooterTemplate>
                      <HeaderTemplate>
                      <table  width=100% style="font-size:12px">
                         </table>
                      </HeaderTemplate>
                      <ItemTemplate>
                      
                       <table  width="100%" style="font-size:12px">
                          <tr>
                          <td width="8%" >
                               <%#((System.Data.DataRow)Container.DataItem)[0] %></td><td width="2%">:</td> <td width="73%"><%# ((System.Data.DataRow)Container.DataItem)[2]%></td>
                          </tr>
                       </table>
                      </ItemTemplate>
                  </asp:Repeater>
                  </td>
                  </tr>
              </table>              
          </ItemTemplate>
      </asp:Repeater>
    </td>
    </tr></table>
    </div>
</asp:Content>

