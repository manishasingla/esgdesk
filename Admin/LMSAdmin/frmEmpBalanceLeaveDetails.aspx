<%@ Page Language="C#" MasterPageFile="~/Admin/LMSAdmin/AdminMasterMenu.master" AutoEventWireup="true" CodeFile="frmEmpBalanceLeaveDetails.aspx.cs" Inherits="Admin_frmEmpLeaveDetails" Title="Balanced leaves" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <%@ Register Assembly="GMDatePicker" Namespace="GrayMatterSoft" TagPrefix="cc1" %>
 <table width="100%" height="373" class="publicloginTable">
        <tr>
            <td valign="top" align="left">
                <table width="100%" class="admintablestyle">
                  <tr>
                        <td colspan="6" class="LoginTitle"><h1 style="color:Black">
                            Employee Balance Leave Details</h1>
                        </td>
                          
                           
                    </tr>
                    <tr>
                        <td colspan="3" class="LoginTitle">
                            Employee Balance Leave Details
                        </td><td style="width: 98px">
                            Select a year 
                            :</td><td><asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="True" 
                                onselectedindexchanged="ddlYear_SelectedIndexChanged">
                            </asp:DropDownList>
                            </td> 
                           
                    </tr>
                     <tr>
                        
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 17%">
                            Employee name</td>
                        <td style="width: 2%">
                            :</td>
                        <td style="width: 161px">
                            <asp:TextBox ID="txtUserName" runat="server" ReadOnly="True" 
                                BackColor="#CCCCCC"></asp:TextBox>
                        </td>
                        <td style="width: 98px" colspan="9" rowspan="8">
                        <asp:Panel ID="pnlAbsentDetails" runat="server" width="50%" style="margin-left:10px;margin-right:20px"
                                   BackColor="#CCCCFF" BorderColor="#6600FF" 
                BorderStyle="Ridge" Height="100%"
                                   Visible="false" GroupingText="Past 10 leaves Details" 
                Direction="LeftToRight" HorizontalAlign="Left" ToolTip="Absent Details">
                                <table cellpadding="0" cellspacing="0" style="height: 100%; width:100%">
                                 
                                    
                                    <tr><td colspan="2">
                                        <asp:GridView ID="gvAbsentDates" runat="server" AllowPaging="True" 
                                            Height="100%" style="margin-top: 9px" Width="100%" visible="false" 
                                            onrowdatabound="gvAbsentDates_RowDataBound">
                                           
                                           
                                           
                                        </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr><td><asp:Label ID="lblStatus" runat="server" 
                                   Text="Data not found" Visible="False"></asp:Label></td></tr>                                </table>                                                         
                            </asp:Panel></td>
                    </tr> 
                    <tr>
                        <td style="width: 17%">
                            Contact number</td>
                        <td style="width: 2%">
                            :</td>
                        <td style="width: 161px">
                            <asp:TextBox ID="txtContactNo" runat="server" ReadOnly="True" 
                                BackColor="#CCCCCC"></asp:TextBox>
                        </td>
                        
                    </tr>
                     <tr>
                        <td style="width: 17%">
                            Email address</td>
                        <td style="width: 2%">
                            :</td>
                        <td style="width: 161px">
                            <asp:TextBox ID="txtEmailId" runat="server" ReadOnly="True" BackColor="#CCCCCC"></asp:TextBox>
                        </td>
                        
                    </tr>
                     
                     <tr>
                        <td style="width: 17%">
                            Address</td>
                        <td style="width: 2%">
                            :</td>
                        <td style="width: 161px">
                            <asp:TextBox ID="txtAddress" TextMode="MultiLine" runat="server" 
                                ReadOnly="True" Width="150px" Height="49px" BackColor="#CCCCCC"></asp:TextBox>
                        </td>
                       
                    </tr>
               

                           <tr><td colspan="4">
                           <table  height="100%" runat="server" id="tblLeave" style="width: 353px">
                            <tr>
                            <td >CL Bal</td><td>:</td><td style="width: 161px">
                                <asp:TextBox ID="txtCLBal" runat="server" ReadOnly="True" BackColor="#CCCCCC"></asp:TextBox></td>
                               </tr>
                            
                            <tr><td>ML Bal</td><td>:</td><td style="width: 161px">
                                <asp:TextBox ID="txtMLBal" runat="server" ReadOnly="True" BackColor="#CCCCCC"></asp:TextBox></td>
                                </tr>
                                <tr><td>EL Bal</td><td>:</td><td style="width: 161px">
                                    <asp:TextBox ID="txtEL"   runat="server" Height="17px" ReadOnly="True" 
                                        BackColor="#CCCCCC"></asp:TextBox></td>
                                    </tr>
                            <tr><td>HPL Bal</td><td>:</td><td style="width: 161px">
                                <asp:TextBox ID="txtHPLBal" runat="server" ReadOnly="True" BackColor="#CCCCCC"></asp:TextBox>
                                </td>
                                
                            </tr>
                                 <tr><td>Extra Bal</td><td>:</td><td style="width: 161px">
                                <asp:TextBox ID="txtExtraBal" runat="server" ReadOnly="True" BackColor="#CCCCCC"></asp:TextBox>
                                </td>
                                
                            </tr>

                                                        <tr><td>Total Bal</td><td>:</td><td style="width: 161px">
                                <asp:TextBox ID="txtTotalBal" runat="server" ReadOnly="True" BackColor="#CCCCCC"></asp:TextBox>
                                </td>
                                
                            </tr>
                            </table>
                            <asp:Label ID="lblLNF" runat="server" 
                                   Text="Leaves are not defined for selected year" Visible="False"></asp:Label>

                               &nbsp;</td></tr>
                           <tr><td colspan="2">
                               <asp:Button ID="btnAddLeave" runat="server" Text="Add leave" 
                                   OnClick="btnAddLeave_Click" /></td><td style="width: 161px">
                               &nbsp;<asp:Button ID="btnBack" runat="server" Text="Back" OnClick="btnBack_Click" /></td></tr>
                           </table></td>
                            
                    </tr>
                   
                   
                 </table>

</asp:Content>

