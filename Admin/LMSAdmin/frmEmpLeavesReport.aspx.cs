using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;

public partial class Admin_frmEmpLeavesReport : System.Web.UI.Page
{
    clsEmployee_Logic1 objEmployee = new clsEmployee_Logic1();
    clsLeave_Logic objLeave = new clsLeave_Logic();
    private string strError = "No records found";
    public static string strHeader = "";
    DataView dv = null;
    string strSUserName = "";
    static string strSEmpName = "";
    static string strSEmpName1 = "";
    static string strSEmailId1 = "";
    static string strSEmpName2 = "";
    static string strSEmailId2 = "";
    string lstrReason = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null)
        {
            Response.Redirect("~/Admin/login.aspx");
        }
        else
        {
        }
        if (!Page.IsPostBack)
        {
           //this.txtPageSize.Text = "20";
            BindData();
           
        }
    }
    void BindData()
    {
        objEmployee.EmpName = txtEmpName.Text.Trim();
        objEmployee.Sort_On = "";
        if (ViewState["Sort_On"] != null)
            objEmployee.Sort_On = ViewState["Sort_On"].ToString() + " " + ViewState["Sort_By"].ToString();
        lblError.Visible = false;
        DataSet dsTemp = objEmployee.GetEmployee();
       // DataRow dr = dsTemp.Tables[0].NewRow();
      //  dr[0] = 0; dr[1] = 0; dr[2] = 0; dr[3] = 0; dr[4] = "0"; dr[5] = "-----Select-----";
        
      //  dsTemp.Tables[0].Rows.InsertAt(dr,0);
        DataTable dtTemp = dsTemp.Tables[0];

        ddlEmpList1.DataTextField = "EmpName";
        ddlEmpList1.DataValueField = "email";
        ddlEmpList1.DataSource = dsTemp.Tables[0];
       
        ddlEmpList1.SelectedIndex = 0;
        ddlEmpList1.DataBind();
        ddlEmpList1.Items.Insert(0, "-----Select-----");
        ddlEmpList2.DataTextField = "EmpName";
        ddlEmpList2.DataValueField = "email";
        ddlEmpList2.DataSource = dsTemp.Tables[0];
      
        ddlEmpList1.SelectedIndex = 0;
        ddlEmpList2.DataBind();
        ddlEmpList2.Items.Insert(0, "-----Select-----");
        dv = new DataView(dtTemp);
        dv.Sort = objEmployee.Sort_On;

        string strFilter = "EmpName Not Like'%" + "-----Select-----" + "%'";
        dv.RowFilter = strFilter;
        if (ViewState["EN"] != null && ViewState["EN"].ToString().Trim() != "")
        {
            strFilter = "EmpName Like'%" + ViewState["EN"].ToString() + "%'";
            dv.RowFilter = strFilter;
        }
           
        if (dv.ToTable().Rows.Count > 0)
        {
            lblError.Visible = false;
           //btnDelete.Visible = true;
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = strError;
          //  btnDelete.Visible = false;
        }
        if (this.txtPageSize.Text != "")
        {
            if (System.Convert.ToInt32(this.txtPageSize.Text) > 0)
            {
                this.gvEmp.PageSize = System.Convert.ToInt32(this.txtPageSize.Text);
            }
        }
        else
        {
            gvEmp.PageSize = dsTemp.Tables[0].Rows.Count;
        }
        gvEmp.DataSource = dv;
        gvEmp.DataBind();
        if (dv.ToTable().Rows.Count == 0)
        {
            this.Lbl_Pageinfo.Visible = false;
        }
        else
        {
            Int16 intTo;
            Int16 intFrom;
            if (gvEmp.PageSize * (gvEmp.PageIndex + 1) < dv.ToTable().Rows.Count)
            {
                intTo = System.Convert.ToInt16(gvEmp.PageSize * (gvEmp.PageIndex + 1));
            }
            else
            {
                intTo = System.Convert.ToInt16(dv.ToTable().Rows.Count);
            }
            intFrom = System.Convert.ToInt16((gvEmp.PageSize * gvEmp.PageIndex) + 1);
            this.Lbl_Pageinfo.Text = "Record(s) " + intFrom + " to " + intTo + " of " + dv.ToTable().Rows.Count;
            this.Lbl_Pageinfo.Visible = true;
        }


    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        strError = "No data matching with your searching criteria";
        gvEmp.PageIndex = 0;
        ViewState["EN"] = txtEmpName.Text;
        BindData(); 
    }
  
  
    protected void gvEmp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (ViewState["Sort_On"] != null)
            objEmployee.Sort_On = ViewState["Sort_On"].ToString();
        else
            objEmployee.Sort_On = "";
        gvEmp.PageIndex = e.NewPageIndex;
        BindData();
    }
    protected void gvEmp_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToUpper() == "ADD")
        {

            string sortByAndArrangeBy = (e.CommandArgument).ToString();

            char[] separator = { '|' };

            string[] sortByAndArrangeByArray = sortByAndArrangeBy.Split(separator);
            strSUserName = sortByAndArrangeByArray[0];
            strSEmpName = sortByAndArrangeByArray[2];
            // Response.Redirect("frmLeaveAddByEmp.aspx?Id=" + e.CommandArgument.ToString());
            // string strSql = "select sum(NoOfDays) from tbl_ApplicationData a,tbl_Status s,tbl_LeaveType l where a.ApplicationStatusId=s.StatusId and a.LeaveTypeId=l.LeaveTypeId and a.ApplicationStatusId=1 ";
            //Response.Redirect("frmLeaveAddByEmp.aspx?id=" + sortByAndArrangeByArray[0] +"&EId="+sortByAndArrangeByArray[1]+ "&EN=" + sortByAndArrangeByArray[2]);
            ViewState["UserName"] = strSUserName;
            ViewState["EN"] = strSEmpName;
            lblLeaveDetails.Text = GenerateHTML();
            pnlLeaveDetails.Visible = true;
            pnlEmpEmailSend.Visible = true;

        }
    }


    protected void gvEmp_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gvEmp_Sorting(object sender, GridViewSortEventArgs e)
    {
        objEmployee.Sort_On = e.SortExpression;
        ViewState["Sort_On"] = objEmployee.Sort_On;
        if (ViewState["Sort_By"] == null)
            ViewState["Sort_By"] = "Asc";
        if (ViewState["Sort_By"].ToString() == "Asc")
        {
            ViewState["Sort_By"] = "Desc";
        }
        else
        {
            ViewState["Sort_By"] = "Asc";
        }

        BindData();
    }

    private string GenerateHTML()
    {
        double dblUsedLeaveCurrentYear = 0.0;
        double dblUsedLeavePreviousYear = 0.0;
        double[] dblArrayOfTotalAndUsedLeave = new double[2];
      
        strHeader = "";
        string serverpath = ConfigurationManager.AppSettings["WebAddress"];
        //body of mail for the person
        //objEmployee.UserName = Request["id"].ToString();
        DateTime d1 = DateTime.Now;

        string dtSDate = FirstDayOfYear(d1).ToString("yyyy-MM-dd");//GMDStartDate.DateString;
        string dtEDate = LastDayOfYear(d1).ToString("yyyy-MM-dd");

        string strSql1 = "select convert(varchar,AbsentDate,103) as AbsentDate ,Reason,PaidOrNPaid as LeaveT, LeaveType from tbl_AbsentDetails where UserName='" + strSUserName + "' and AbsentDate >='" + dtSDate + "'"
                               + " and AbsentDate <='" + dtEDate + "' order by cast(AbsentDate as DateTime) desc ";

        DataSet dsTemp1 = objEmployee.GetAbsentDates(strSql1);
        dblArrayOfTotalAndUsedLeave = objLeave.fnGetTotalAndUsedLeave(strSUserName, d1.Year);
        dblUsedLeaveCurrentYear = dblArrayOfTotalAndUsedLeave[0];

        DateTime d2 = DateTime.Now.AddYears(-1);
        dtSDate = FirstDayOfYear(d2).ToString("yyyy-MM-dd");//GMDStartDate.DateString;
        dtEDate = LastDayOfYear(d2).ToString("yyyy-MM-dd");

        string strSql2 = "select convert(varchar,AbsentDate,103) as AbsentDate ,Reason,PaidOrNPaid as LeaveT,LeaveType from tbl_AbsentDetails where UserName='" + strSUserName + "' and AbsentDate >='" + dtSDate + "'"
                            + " and AbsentDate <='" + dtEDate + "' order by cast(AbsentDate as DateTime) desc ";

        DataSet dsTemp2 = objEmployee.GetAbsentDates(strSql2);
        dblArrayOfTotalAndUsedLeave = objLeave.fnGetTotalAndUsedLeave(strSUserName, d2.Year);
        dblUsedLeavePreviousYear = dblArrayOfTotalAndUsedLeave[0];
        DataTable dtTemp1 = dsTemp1.Tables[0];
        DataTable dtTemp2 = dsTemp2.Tables[0];


        //body of mail for the person
        try
        {


            strHeader += "<br/>";
            DateTime dt = DateTime.Now;

            //strHeader += "Employee leave details"; //at 07:00 pm on 30-April-2012";

            strHeader += @"<table border=""1"" width=""100%"">";


            strHeader += @"<tr><td colspan=""3""><b><u><strong >" + ViewState["EN"].ToString() + "'s</strong> leave details are:</u></b></td></tr>";

            strHeader += @"<tr><td colspan=""3""></td></tr>";

            strHeader += @"<tr><td colspan=""3""><strong >Leave details for year :" + d1.Year.ToString() + "(Absent Days : "+dblUsedLeaveCurrentYear.ToString()+ " )</strong></td></tr>";
            if (dtTemp1.Rows.Count > 0)
            {
                strHeader += @"<tr><td width=""30%""><strong >Date</strong></td><td colspan=""2""><strong>Reason</strong></td></tr>";

                for (int i = 0; i < dtTemp1.Rows.Count; i++) // logic to group  absent employee on same date.
                {
                    if (dtTemp1.Rows[i]["Reason"].ToString().Trim() != "")
                        lstrReason = dtTemp1.Rows[i]["Reason"].ToString().Replace("@", "'").Trim();
                    strHeader += "<tr><td>" + dtTemp1.Rows[i]["AbsentDate"].ToString().Trim() + @"</td><td colspan=""2"">" + lstrReason + "</td></tr>";

                }
            }
            else
                strHeader += @"<tr><td colspan=""3""> No leave was taken in this year</td></tr>";

            strHeader += @"<tr><td colspan=""3""></td></tr>";

            strHeader += @"<tr><td colspan=""3""><strong >Leave details for year :" + d2.Year.ToString() + "(Absent Days : "+dblUsedLeavePreviousYear.ToString() + " )</strong></td></tr>";

            if (dtTemp2.Rows.Count > 0)
            {
                strHeader += @"<tr><td width=""30%""><strong> Date</strong></td><td colspan=""2""><strong>Reason </strong></td></tr>";


                for (int i = 0; i < dtTemp2.Rows.Count; i++) // logic to group  absent employee on same date.
                {
                    if (dtTemp2.Rows[i]["Reason"].ToString().Trim() != "")
                        lstrReason = dtTemp2.Rows[i]["Reason"].ToString().Replace("@", "'").Trim();
                    strHeader += "<tr><td>" + dtTemp2.Rows[i]["AbsentDate"].ToString().Trim() + @"</td><td colspan=""2""> " + lstrReason + "</td></tr>";

                }
            }
            else
                strHeader += @"<tr><td colspan=""3"">No leave was taken in this year</td></tr>";


            strHeader += "</table>";
          
            strHeader += "<br/><br/>";

            //    strHeader += "------------------";
            //    strHeader += "<br><br>";
            //    strHeader += ConfigurationManager.AppSettings["CompanyName"];
            //    strHeader += "<br>";
            //    //strHeader += ConfigurationManager.AppSettings["WebAddress"];
            //    strHeader += "<a href=" + ConfigurationManager.AppSettings["WebAddress"] + ">";
            //    strHeader += ConfigurationManager.AppSettings["WebAddressToShow"];
            //    strHeader += "</a>";


        }

        catch
        {
        }

        return strHeader;
    }

    static DateTime LastDayOfYear()
    {
        return LastDayOfYear(DateTime.Today);
    }


    static DateTime LastDayOfYear(DateTime d)
    {
        // 1
        // Get first of next year
        DateTime n = new DateTime(d.Year + 1, 1, 1);
        // 2
        // Subtract 1 from it
        return n.AddDays(-1);
    }

    static DateTime FirstDayOfYear()
    {
        return FirstDayOfYear(DateTime.Today);
    }


    static DateTime FirstDayOfYear(DateTime y)
    {
        return new DateTime(y.Year, 1, 1);
    }

    protected void btnSendEmail_Click(object sender, EventArgs e)
    {
        
        if ((ddlEmpList1.SelectedIndex+ddlEmpList2.SelectedIndex)>0)
        {
            strSUserName = ViewState["UserName"].ToString();
            sendEmail(strSEmpName1, strSEmailId1, strSEmpName2, strSEmailId2);
        }
        else
        {
            lblEmailReport.Text = "Please select an employee from List.";
        }
       
    }
    

    protected void sendEmail(string strEmpName1, string strEmailId1,string strEmpName2, string strEmailId2)
    {

        try
        {
            string strEmailSucess = "";
            bool blnIsSent1 = false;
            bool blnIsSent2 = false;

            string emailid1 = "", emailid2 = "";

            emailid1 = strEmailId1;
            emailid2 = strEmailId2;
            //emailid2 = System.Configuration.ConfigurationManager.AppSettings["LMS2"].ToString();
            // emailid3 = System.Configuration.ConfigurationManager.AppSettings["LMS3"].ToString();
            string strBody = "";
            strBody = GenerateHTML();

            string strSubject = "Employee " + ViewState["EN"].ToString() + " Leave details";
            blnIsSent1 = DatabaseHelper.sendEmailLMSReport(emailid1, strSubject, strBody);
            blnIsSent2 = DatabaseHelper.sendEmailLMSReport(emailid2, strSubject, strBody);

            strEmailSucess = "Email has been sent to ";
            if (blnIsSent1)
                strEmailSucess += strSEmpName1 + "<br/>";
            if (blnIsSent2)
                strEmailSucess += strSEmpName2 + "<br/>";

            lblEmailReport.Text = strEmailSucess;
        }
        catch { }
    }
    protected void ddlEmpList2_SelectedIndexChanged(object sender, EventArgs e)
    {
        strSEmailId2 = ddlEmpList2.SelectedValue.ToString();
        strSEmpName2 = ddlEmpList2.SelectedItem.Text;
    }
    protected void ddlEmpList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        strSEmailId1 = ddlEmpList1.SelectedValue.ToString();
        strSEmpName1 = ddlEmpList1.SelectedItem.Text;
    }
}
