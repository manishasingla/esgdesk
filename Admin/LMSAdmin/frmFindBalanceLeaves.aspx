<%@ Page Language="C#" MasterPageFile="~/Admin/LMSAdmin/AdminMasterMenu.master" AutoEventWireup="true" CodeFile="frmFindBalanceLeaves.aspx.cs" Inherits="Admin_frmFindBalanceLeaves" Title="Find balanced leaves" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script type="text/javascript" language="javascript">
function toggleAllCheckboexs(toggle)
{
				n = document.forms[0].length;
				var frm = document.forms[0];
				for(i=0;i<frm.length;i++)
				
					if(frm.elements[i].type=="checkbox")
						if (frm.elements[i].name.indexOf('Cbx')==0)
							frm.elements[i].checked=toggle;
			}
				function onlyNumbers(evt)
{
    var e = event || evt;
    var charCode = e.which || e.keyCode;
	if ((charCode > 47 && charCode < 58))
    return true;		             
    else
    return false ;		                            
}
</script>
    <table height="373" class="publicloginTable" style="width:81%;">
     <tr>
                        <td colspan="4" class="LoginTitle"><h1 style="color:Black">
                            Employee list for viewing used leave count</h1>
                        </td>
                          
                           
                    </tr>
        <tr>
            <td valign="top" align="left">
                <table width="100%" class="admintablestyle">
                    <tr>
                        <td colspan="4" class="LoginTitle">
                            Find Balance Leaves</td>
                    </tr>
                    <tr>
                        <td style="width: 17%">
                            Employee&nbsp; name</td>
                        <td style="width: 2%">
                            :</td>
                        <td style="width: 18%">
                            <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
                        </td>
                        <td>
                        </td>
                  
                        <td style="height: 26px">
                            Page size</td>
                        <td style="height: 26px">
                            :</td>
                        <td style="height: 26px">
                            <asp:TextBox ID="txtPageSize" OnKeypress="return onlyNumbers(event)" runat="server" Width="50px"></asp:TextBox>
                            <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtPageSize"
                                CssClass="lblerror" ErrorMessage="Invalid Number" MaximumValue="999" MinimumValue="1"
                                 Type="Integer"></asp:RangeValidator>
                        </td>
                        <td style="height: 26px"> <asp:Button ID="btnSearch" runat="server" Text="Search" Width="55px" OnClick="btnSearch_Click" /></td>
                            </td>
                    </tr>
                    
                    <tr>
                        <td colspan="3">
                            &nbsp;
                        </td>
                        <td>
                         </td>   
                    </tr>
                    <tr>
                        <td colspan="8">
                        <table border="0" width="100%" ><tr>
                        <td align="left" style="width: 321px">
                            </td> 
                        <td align="right">
                            </td></tr>
                       <tr><td colspan="8">                       
                           <asp:GridView ID="gvEmp" runat="server" AllowPaging="true" 
                               AllowSorting="true"  AutoGenerateColumns="False" Width="100%" 
                               OnRowCommand="gvEmp_RowCommand" OnSorting="gvEmp_Sorting" 
                               OnPageIndexChanging="gvEmp_PageIndexChanging" 
                               onselectedindexchanged="gvEmp_SelectedIndexChanged" 
                               onrowdatabound="gvEmp_RowDataBound" PageSize="20">
                                <Columns> 

                              <asp:TemplateField HeaderText="User name" SortExpression="UserName" Visible="false">
                               <ItemTemplate>
                               <asp:Label runat="server" ID="lblUserName" Text='<%#Eval("username")%>'></asp:Label>                           
                               </ItemTemplate>
                              </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Employee name" SortExpression="EmpName">
                                               <HeaderStyle Width="35%" />
                                                    <ItemTemplate>
                                                      <asp:LinkButton ID="lnkBtnCountryName" Text='<%#Eval("EmpName")%>' runat="server" CommandArgument='<%#Eval("UserName")%>' CommandName="View"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Email address">
                                                    <ItemTemplate>
                                                        <%#Eval("Email")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Contact number">
                                    <ItemTemplate>
                                    <%#Eval("ContactNo")%>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Total Balance">
                                    <ItemTemplate>
                                    <asp:Label ID="lblTotalBalance" runat="server"  CommandName="Update"></asp:Label>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView> </td></tr>
                            
                        
                   
                    <tr>
                        <td width="50%">
                            <asp:Label ID="lblError" CssClass="lblerror" Visible="false"  runat="server" Text=""></asp:Label></td>
                       <td align="right">
                           <asp:Label ID="Lbl_Pageinfo" runat="server" Text=""></asp:Label></td> 
                        </tr> 
                    
                </table>
                 </td></tr>
                 </table>
    
    
    
    </td>
    </tr>
    </table>

</asp:Content>

