<%@ Page Language="C#"  MasterPageFile="~/Admin/LMSAdmin/AdminMasterMenu.master" AutoEventWireup="true"
    CodeFile="frmLeaveAddByEmp.aspx.cs" Inherits="Admin_frmEmpLeaveDetails" Title=" Add leave" %>
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="GMDatePicker" Namespace="GrayMatterSoft" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server" >

<div class="publicloginTable" style="height:81% ;background-color:#C2CFDC;" >
<div style="float:left;height:100%;width:50%" class="publicloginTable">
    <table   style="height:100%; width: 89%;">
     <tr>
     <td colspan="6" class="LoginTitle"><h1 style="color:Black">
         Add leave for employee</h1>
     </td>
                          
                           
                    </tr>
        <tr>
            <td valign="top" align="left">
                <table class="admintablestyle">
                    <tr>
                        <td colspan="3" class="LoginTitle">
                            Employee Balance Leave Details
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 17%">
                            Employee name
                        </td>
                        <td style="width: 2%">
                            :
                        </td>
                        <td style="width: 180px">
                            <asp:TextBox ID="txtUserName" runat="server" ReadOnly="True" Width="210px" 
                                style="height: 22px"></asp:TextBox>
                        </td>
                        </tr>
                    
                    <tr>
                        <td colspan="3">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Start date
                        </td>
                        <td>
                            :
                        </td>
                        <td valign="top">
                         
                            <asp:TextBox ID="GMDStartDate" runat="server"></asp:TextBox>
                               <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" 
                            TargetControlID="GMDStartDate"
                            Format="dd/MM/yyyy" PopupButtonID="GMDStartDate" >
                            </ajaxToolkit:CalendarExtender>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                ErrorMessage="Please select a start date" ControlToValidate="GMDStartDate"></asp:RequiredFieldValidator>
                        </td>
                      
                    </tr>
                    <tr>
                      <td>
                            End&nbsp; date
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                       
                            <asp:TextBox ID="GMDEndDate" runat="server"></asp:TextBox>
                             <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" 
                            TargetControlID="GMDEndDate"
                            Format="dd/MM/yyyy"  PopupButtonID="GMDEndDate" >
                            </ajaxToolkit:CalendarExtender>
                            <asp:CompareValidator ID="CompareValidator1" runat="server" 
                                ErrorMessage="End date should be greater or equal to start date." 
                                ControlToCompare="GMDStartDate" ControlToValidate="GMDEndDate" 
                                Operator="GreaterThanEqual" Type="Date" ></asp:CompareValidator>
                        </td>
                    </tr>
                   
                                <tr>
                                    <td>
                                        Paid/Nonpaid</td>
                                    <td>:</td>
                                    <td>
                                        <asp:DropDownList ID="ddlLeaveTypePOrNP" runat="server" 
                                            onselectedindexchanged="ddlLeaveTypePOrNP_SelectedIndexChanged">
                                            <asp:ListItem Value="0">Unpaid</asp:ListItem>
                                            <asp:ListItem Value="1">Paid</asp:ListItem>
                                        </asp:DropDownList>
    
  
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                ErrorMessage="Please select a type"  InitialValue="Select"
        ControlToValidate="ddlLeaveTypePOrNP"></asp:RequiredFieldValidator>
    
  
                                    </td>
                                </tr>
                           <tr>
<td style="height: 25px" >
Leave type
</td>
<td style="height: 25px">:</td>
<td style="height: 25px">
    <asp:DropDownList ID="ddlLeaveType" runat="server" 
        OnSelectedIndexChanged="ddlLeaveType_SelectedIndexChanged">
        <asp:ListItem>Select</asp:ListItem>
    </asp:DropDownList>
 
        <asp:CheckBox ID="chkbHPL" runat="server" AutoPostBack="True" 
        oncheckedchanged="chkbHPL_CheckedChanged" Text="Is HPL?" Visible="False" />
    
  
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                ErrorMessage="Please select a leave type"  InitialValue="Select"
        ControlToValidate="ddlLeaveType"></asp:RequiredFieldValidator>
    
  
    </td>
</tr>

<
                            <tr>
                        <td>
                            Reason
                                             <td>
                            :
                        </td>
                        <td>
                            <asp:TextBox ID="txtReason" runat="server" TextMode="MultiLine" Width="210px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="btnAddLeave" runat="server" Text="Add Leave" OnClick="btnAddLeave_Click" />
                        </td>
                        <td style="width: 161px">
                            &nbsp;<asp:Button ID="btnBack" runat="server" Text="Back" 
                                OnClick="btnBack_Click" CausesValidation="False" />
                        </td>
                       
                    </tr>
                    <tr><td colspan="3"></td></tr>
                    <tr><td colspan="3"><asp:Label ID="lblStatusMsg" runat="server" Font-Bold="True"></asp:Label></td></tr>
                </table>
            </td>
        </tr>
    </table>
    </div>

    <div style="float:right; width: 47%; height:100%" class="publicloginTable"> 
       <table>
                    <tr>
                        <td style="width: 17%">
                            Contact number
                        </td>
                        <td style="width: 2%">
                            :
                        </td>
                        <td style="width: 161px">
                            <asp:TextBox ID="txtContactNo" runat="server" ReadOnly="True" Width="210px" 
                                BackColor="#CCCCCC"></asp:TextBox>
                        </td>
                       
                    </tr>
                    <tr>
                        <td style="width: 17%">
                            Email address
                        </td>
                        <td style="width: 2%">
                            :
                        </td>
                        <td style="width: 161px">
                            <asp:TextBox ID="txtEmailId" runat="server" ReadOnly="True" Width="210px" 
                                BackColor="#CCCCCC"></asp:TextBox>
                        </td>
                        
                    </tr>
                    <tr>
                        <td style="width: 17%">
                            Address
                        </td>
                        <td style="width: 2%">
                            :
                        </td>
                        <td style="width: 161px">
                            <asp:TextBox ID="txtAddress" TextMode="MultiLine" runat="server" ReadOnly="True"
                                Width="210px" Height="49px" BackColor="#CCCCCC" BorderColor="White"></asp:TextBox>
                        </td>
                       
                    </tr>
                    <tr>
                        <td>
                            Designation
                        </td>
                        <td>
                            :
                        </td>
                        <td style="width: 161px">
                            <asp:TextBox ID="txtDesig" runat="server" ReadOnly="True" Width="210px" 
                                BackColor="#CCCCCC"></asp:TextBox>
                        </td>
                       
                    </tr>
                    <tr>
                        <td>
                            CL balance</td>
                        <td>
                            :
                        </td>
                        <td style="width: 161px">
                            <asp:TextBox ID="txtCLBal" runat="server" ReadOnly="True" Width="210px" 
                                BackColor="#CCCCCC"></asp:TextBox>
                        </td>
                        
                    </tr>
                    <tr>
                        <td>
                            ML balance</td>
                        <td>
                            :
                        </td>
                        <td style="width: 161px">
                            <asp:TextBox ID="txtMLBal" runat="server" ReadOnly="True" Width="210px" 
                                BackColor="#CCCCCC"></asp:TextBox>
                        </td>
                       
                    </tr>
                    <tr>
                        <td>
                            EL balance</td>
                        <td>
                            :
                        </td>
                        <td style="width: 161px">
                            <asp:TextBox ID="txtEL" runat="server" Height="17px" ReadOnly="True" 
                                Width="210px" BackColor="#CCCCCC"></asp:TextBox>
                        </td>
                       
                    </tr>
                    <tr>
                        <td>
                            HPL balance</td>
                        <td>
                            :
                        </td>
                        <td style="width: 161px">
                            <asp:TextBox ID="txtHPLBal" runat="server" ReadOnly="True" Width="210px" 
                                BackColor="#CCCCCC"></asp:TextBox>
                        </td>
                       
                    </tr>
                     <tr>
                        <td>
                            Extra Leaves</td>
                        <td>
                            :
                        </td>
                        <td style="width: 161px">
                            <asp:TextBox ID="txtExtraLeaves" runat="server" ReadOnly="True" Width="210px" 
                                BackColor="#CCCCCC"></asp:TextBox>
                        </td>
                       
                    </tr>
                     <tr>
                        <td>
                           Balance </td>
                        <td>
                            :
                        </td>
                        <td style="width: 161px">
                            <asp:TextBox ID="txtTotalBalance" runat="server" ReadOnly="True" Width="210px" 
                                BackColor="#CCCCCC"></asp:TextBox>
                        </td>
                       
                    </tr>

                    </table>
                    <br />                          
    <table width="100%" >
    <tr><td class="LoginTitle"><asp:Label ID="lblGridHeader" runat="server" 
            Text="Last 6 Months Leave Details" Font-Bold="True"></asp:Label></td></tr><tr>
                        <td rowspan="14" align="left" valign="top" >
                            <asp:GridView ID="gvAbsentDates" runat="server" Visible="false" 
                                PageSize="14"
                                Style="position: relative; top: 0px; left: 0px; width: 95%;" OnPageIndexChanging="gvAbsentDates_PageIndexChanging"
                                OnRowDataBound="gvAbsentDates_RowDataBound" OnSelectedIndexChanged="gvAbsentDates_SelectedIndexChanged"
                                AutoGenerateColumns="False" 
                                AllowSorting="True" onsorting="gvAbsentDates_Sorting" CaptionAlign="Top">
                                <Columns>
                               <%-- <asp:TemplateField>
                                <ItemTemplate>
                                <asp:Label runat="server" Text="Last 6 Months Leave Details"></asp:Label>
                                </ItemTemplate>
                                </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="Absent date" SortExpression="AbsentDate">
                                        <HeaderStyle Width="20%" />
                                        <ItemTemplate>
                                            <%#Eval("AbsentDate")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Reason" SortExpression="Reason">
                                    <HeaderStyle Width="60%" />
                                        <ItemTemplate>
                                            <%#Eval("Reason")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Leave type" SortExpression="PaidOrNPaid">
                                    <HeaderStyle Width="20%" />
                                        <ItemTemplate>
                                            <%#Eval("PaidOrNPaid")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle Height="20px" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                <RowStyle Height="30px" HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:GridView>
                        </td>
                    </tr>
                    
                    </table>
                    <div><asp:Label ID="Lbl_Pageinfo" runat="server" Text=""></asp:Label></div>
                    </div>
</div>

</asp:Content>
