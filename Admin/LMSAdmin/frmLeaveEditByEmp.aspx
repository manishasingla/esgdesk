<%@ Page Language="C#" MasterPageFile="~/Admin/LMSAdmin/AdminMasterMenu.master" AutoEventWireup="true"
    CodeFile="frmLeaveEditByEmp.aspx.cs" Inherits="Admin_frmEmpLeaveDetails" Title="Edit leave for employee" %>

<%@ Register Assembly="GMDatePicker" Namespace="GrayMatterSoft" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="publicloginTable" style="height: 81%; background-color: #C2CFDC;">
        <div style="float: right; height: 100%; width: 50%" class="publicloginTable" id="rightdiv"
            runat="server" visible="false">
            <table width="50%" style="height: 100%">
     
                <tr>
                               <td>
                        <table class="admintablestyle">
                            <tr>
                                <td colspan="3" class="LoginTitle">
                                    Employee Balance Leave Details
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 17%">
                                    User name
                                </td>
                                <td style="width: 2%">
                                    :
                                </td>
                                <td style="width: 180px">
                                    <asp:TextBox ID="txtUserName" runat="server" ReadOnly="True" Width="210px" 
                                        Style="height: 22px" BackColor="#CCCCCC"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 17%">
                                    Contact number
                                </td>
                                <td style="width: 2%">
                                    :
                                </td>
                                <td style="width: 161px">
                                    <asp:TextBox ID="txtContactNo" runat="server" ReadOnly="True" Width="210px" 
                                        BackColor="#CCCCCC"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 17%">
                                    Email address
                                </td>
                                <td style="width: 2%">
                                    :
                                </td>
                                <td style="width: 161px">
                                    <asp:TextBox ID="txtEmailId" runat="server" ReadOnly="True" Width="210px" 
                                        BackColor="#CCCCCC"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 17%">
                                    Address
                                </td>
                                <td style="width: 2%">
                                    :
                                </td>
                                <td style="width: 161px">
                                    <asp:TextBox ID="txtAddress" TextMode="MultiLine" runat="server" ReadOnly="True" Width="210px"
                                        Height="49px" BackColor="#CCCCCC"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Designation
                                </td>
                                <td>
                                    :
                                </td>
                                <td style="width: 161px">
                                    <asp:TextBox ID="txtDesig" runat="server" ReadOnly="True" Width="210px" 
                                        BackColor="#CCCCCC"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    CL balance
                                </td>
                                <td>
                                    :
                                </td>
                                <td style="width: 161px">
                                    <asp:TextBox ID="txtCLBal" runat="server" ReadOnly="True" Width="210px" 
                                        BackColor="#CCCCCC"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    ML balance
                                </td>
                                <td>
                                    :
                                </td>
                                <td style="width: 161px">
                                    <asp:TextBox ID="txtMLBal" runat="server" ReadOnly="True" Width="210px" 
                                        BackColor="#CCCCCC"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    EL balance
                                </td>
                                <td>
                                    :
                                </td>
                                <td style="width: 161px">
                                    <asp:TextBox ID="txtEL" runat="server" Height="17px" ReadOnly="True" 
                                        Width="210px" BackColor="#CCCCCC"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    HPL balance
                                </td>
                                <td>
                                    :
                                </td>
                                <td style="width: 161px">
                                    <asp:TextBox ID="txtHPLBal" runat="server" ReadOnly="True" Width="210px" 
                                        BackColor="#CCCCCC"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Leave type
                                </td>
                                <td>
                                    :
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlLeaveType" runat="server" OnSelectedIndexChanged="ddlLeaveType_SelectedIndexChanged"
                                        Enabled="False" Font-Bold="True" ForeColor="Black">
                                        <asp:ListItem>Select</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Absent date:
                                </td>
                                <td>
                                    :
                                </td>
                                <td>
                                    <asp:TextBox ID="txtAbsentDate" runat="server" Width="210px" ReadOnly="True" 
                                        BackColor="#CCCCCC"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    &nbsp;</td>
                            </tr>
                        </table>
                    </td>
           
                    <td valign="top" align="left">
                        <table class="admintablestyle">
                            <tr>
                                <td>
                                    Paid/Nonpaid
                                </td>
                                <td>
                                    :
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlLeaveTypePOrNP" runat="server">
                                        <asp:ListItem Value="0">Unpaid</asp:ListItem>
                                        <asp:ListItem Value="1">Paid</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                           
                            <tr>
                                <td>
                                    Reason
                                </td>
                                <td>
                                    :
                                </td>
                                <td>
                                    <asp:TextBox ID="txtReason" runat="server" TextMode="MultiLine" Width="210px"></asp:TextBox>
                                </td>
                                <tr>
                                    <td colspan="3">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnUpdate" runat="server" Text="Update" OnClick="btnUpdate_Click" />
                                    </td>
                                    <td style="width: 161px" valign="top">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:Label ID="lblStatusMsg" runat="server" Font-Bold="True"></asp:Label>
                                    </td>
                                </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div style="float: left; width: 37%; height: 216px" class="publicloginTable" id="leftdiv"
            runat="server">
            <table style="width: 133%">
                    <tr>
                        <td colspan="6" class="LoginTitle"><h1 style="color:Black">
                            Edit leave for employee</h1>
                        </td>
                          
                           
                    </tr>
                <tr>
                    <td class="LoginTitle">
                        Leave Details
                   <span style="color:Black">Total Balance :<asp:Label ID="lblTotalBalance" runat="server" Text="Label"></asp:Label></span> 
                    </td>
                </tr>
                <tr>
                    <td rowspan="0"  valign="top">
                        <asp:GridView ID="gvAbsentDates" runat="server" PageSize="20" Style="position: relative;
                            top: 0px; left: 0px; width: 95%;" OnPageIndexChanging="gvAbsentDates_PageIndexChanging"
                            OnRowDataBound="gvAbsentDates_RowDataBound" OnSelectedIndexChanged="gvAbsentDates_SelectedIndexChanged"
                            OnDataBound="gvAbsentDates_DataBound" AutoGenerateColumns="False" AllowSorting="True"
                            OnSorting="gvAbsentDates_Sorting" CaptionAlign="Top" OnRowCommand="gvAbsentDates_RowCommand"
                            OnRowEditing="gvAbsentDates_RowEditing" OnRowDeleting="gvAbsentDates_RowDeleting"
                            OnRowUpdating="gvAbsentDates_RowUpdating">
                            <Columns>
                                <asp:TemplateField HeaderText="Absent date" SortExpression="AbsentDate">
                                    <HeaderStyle Width="20%" />
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkBtnUserName" Text='<%#Eval("AbsentDate")%>' runat="server"
                                            CommandArgument='<%#string.Format("{0}|{1}|{2}|{3}",Eval("UserName"),Eval("AbsentDate"),Eval("LeaveType"),Eval("PaidOrNPaid"))%>'
                                            CommandName="Edt"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Reason" SortExpression="Reason">
                                    <HeaderStyle Width="60%" />
                                    <ItemTemplate>
                                        <%#Eval("Reason")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Paid Or Unpaid" SortExpression="PaidOrNPaid">
                                    <HeaderStyle Width="20%" />
                                    <ItemTemplate>
                                        <%#Eval("PaidOrNPaid")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Leave type" SortExpression="LeaveType">
                                    <HeaderStyle Width="20%" />
                                    <ItemTemplate>
                                        <%#Eval("LeaveType")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Is HPL?" SortExpression="">
                                    <HeaderStyle Width="20%" />
                                    <ItemTemplate>
                                        <%#Eval("IsEHPL")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Delete" SortExpression="AbsentDate">
                                    <HeaderStyle Width="20%" />
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkBtnDelete" Text="Delete" runat="server" CommandArgument='<%#string.Format("{0}|{1}|{2}|{3}|{4}",Eval("UserName"),Eval("AbsentDate"),Eval("LeaveType"),Eval("PaidOrNPaid"),Eval("IsEHPL"))%>'
                                            CommandName="Del"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Edit" SortExpression="AbsentDate">
                                    <HeaderStyle Width="20%" />
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkBtnUpdate" Text="Edit" runat="server" CommandArgument='<%#string.Format("{0}|{1}|{2}|{3}|{4}",Eval("UserName"),Eval("AbsentDate"),Eval("LeaveType"),Eval("PaidOrNPaid"),Eval("Reason"))%>'
                                            CommandName="Upd"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle Height="20px" />
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <RowStyle Height="30px" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:GridView>
                    </td>
                </tr>
               
               <tr>
               <td>  <span>
        <asp:Label ID="lblError" runat="server"  Visible="false" CssClass="lblerror"></asp:Label>
        <asp:Label ID="Lbl_Pageinfo" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp; <asp:Button ID="btnBack" runat="server" Text="Back" 
                                OnClick="btnBack_Click" CausesValidation="False" />
                        </span></td>
                        <tr><td style="width:10%"></td></tr>
               </tr>
   
            </table>
        </div>
    </div>
</asp:Content>
