<%@ Page Language="C#" MasterPageFile="~/Admin/LMSAdmin/AdminMasterMenu.master" AutoEventWireup="true"
    CodeFile="frmLeaveSetting.aspx.cs" Inherits="Admin_frmEmpLeaveDetails" Title="Leave setting" %>

<%@ Register Assembly="GMDatePicker" Namespace="GrayMatterSoft" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server" >

    <div class="publicloginTable" style="height:81% ;background-color:#C2CFDC;" >

    <div style="float:left; width: 47%; height:100%" class="publicloginTable" id="leftdiv" runat="server">                            
    <table width="100%" >
                      <tr>
                        <td colspan="6" class="LoginTitle"><h1 style="color:Black">
                            Year wise leave setting </h1>
                        </td>
                          
                           
                    </tr>
    <tr>
                        <td align="left" valign="top" colspan="3">
                            &nbsp;</td>
                    </tr>
                    <tr><td>Select a year </td><td>:</td><td>
                            <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="True" 
                                onselectedindexchanged="ddlYear_SelectedIndexChanged">
                            </asp:DropDownList>
                            </td></tr>
                            <tr><td colspan="3"></td>
                         </tr>
                     <tr><td>Balance leave</td><td>:</td><td>
                         <asp:TextBox ID="txtTotalLeave" runat="server" Width="50px"></asp:TextBox>
                         </td></tr>
                          <tr><td colspan="3"></td>
                         </tr>
                 
                          <tr><td colspan="3"></td>
                         </tr>
                         <tr><td>&nbsp;</td><td>&nbsp;</td><td>
                             <asp:Button ID="btnSetLeave" runat="server" Text="Set Now" 
                                 onclick="btnSetLeave_Click" />
                         </td></tr>
                         <tr><td colspan="3"></td>
                          <tr><td colspan="3">
                              <asp:Label ID="lblSuccessMsg" runat="server"></asp:Label></td>
                         </tr>
                    </table>
                    <span>
        &nbsp;&nbsp;&nbsp; 
                        </span>
                    </div>
</div>

</asp:Content>
