<%@ Page Language="C#" MasterPageFile="~/Admin/LMSAdmin/AdminMasterMenu.master" AutoEventWireup="true" CodeFile="frmLeaveTransactionsLast10Days.aspx.cs" Inherits="Admin_frmMonthlyLeaveTransactions"  Title="Last 10 day leaves tarnsaction" %>

<%@ Register Assembly="GMDatePicker" Namespace="GrayMatterSoft" TagPrefix="cc1" %>
<%@ Register Assembly="GMDatePicker" Namespace="GrayMatterSoft" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script type="text/javascript" language="javascript">

function onlyNumbers(evt)
{
    var e = event || evt;
    var charCode = e.which || e.keyCode;
	if ((charCode > 47 && charCode < 58))
    return true;		             
    else
    return false ;		                            
}
</script>
    <table width="100%"  Height="373" class="publicloginTable">
     <tr>
                        <td colspan="6" class="LoginTitle"><h1 style="color:Black">
                             Employee absent in last 10 days</h1>
                        </td>
                          
                           
                    </tr>
        <tr>
            <td valign="top" align="left">
                <table width="100%" class="admintablestyle">
                    <tr>
                        <td colspan="6" class="LoginTitle">
                            Employee absent in last 10 days</td>
                    </tr>
                    <tr>
                        <td style="width: 17%">
                            User name</td>
                        <td style="width: 2%">
                            :</td>
                        <td style="width: 18%">
                            <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
                        </td>
                        <td style="height: 28px">
                            Page size</td>
                        <td style="height: 28px">
                            :</td>
                        <td style="height: 28px">
                            <asp:TextBox ID="txtPageSize" OnKeypress="return onlyNumbers(event)" runat="server" Width="50px"></asp:TextBox>
                            <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtPageSize"
                                CssClass="lblerror" ErrorMessage="Invalid Number" MaximumValue="999" MinimumValue="1"
                                 Type="Integer"></asp:RangeValidator>
                        </td>
                        <td colspan="3" style="height: 28px"> <asp:Button ID="btnSearch" runat="server" Text="Search" Width="55px" OnClick="btnSearch_Click" /></td>
                        </table>
             </td>
       </tr>                  
                   
                    <tr>
                        <td colspan="3">
                            &nbsp;
                        </td>
                        <td colspan="3">
                         </td>   
                    </tr>
                    <tr>
                        <td colspan="6">
                        <table border="0" width="80%" ><tr>
                        <td align="left" style="width: 321px">

                       <tr><td colspan="2">                       
                           <asp:GridView ID="gvLast10DaysAbsentDetails" runat="server" AllowPaging="true" 
                               AllowSorting="true"  AutoGenerateColumns="False" Width="100%"  
                               OnSorting="gvLast10DaysAbsentDetails_Sorting" OnPageIndexChanging="gvLast10DaysAbsentDetails_PageIndexChanging" 
                               PageSize="20" 
                               onrowdatabound="gvLast10DaysAbsentDetails_RowDataBound">
                                <Columns>
                 <asp:TemplateField HeaderText="User name" SortExpression="UserName">
                                   <HeaderStyle Width="18%" />
                                    <ItemTemplate>       
              
                                       <%#Eval("UserName")%>
                                   </ItemTemplate>

                                 </asp:TemplateField>
                                
                                     <asp:TemplateField HeaderText="Absent date ( DD-MM-YYYY )" SortExpression="AbsentDate">

                                     <HeaderStyle Width="28%" />
                                    <ItemTemplate>
                                    <%#Eval("AbsentDate")%>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Reason" SortExpression="Reason">                             
                                    <ItemTemplate>
                                    <%#Eval("Reason")%>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView> </td></tr>                          
                    <tr>
                        <td width="50%">
                            <asp:Label ID="lblError" CssClass="lblerror" Visible="false"  runat="server" Text=""></asp:Label></td>
                       <td align="right">
                           <asp:Label ID="Lbl_Pageinfo" runat="server" Text=""></asp:Label></td> 
                        </tr> 
                    
                </table>
                 </td></tr>
                 </table>
</asp:Content>

