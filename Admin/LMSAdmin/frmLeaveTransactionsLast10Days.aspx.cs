using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;


public partial class Admin_frmMonthlyLeaveTransactions : System.Web.UI.Page
{
    clsLeave_Logic objLeave = new clsLeave_Logic();
    clsEmployee_Logic1 objEmployee = new clsEmployee_Logic1();
    string strError = "No records found";
    List<DateTime> dates = new List<DateTime>();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["admin"] == null)
            {
                Response.Redirect("~/Admin/login.aspx");
            }

            if (!Page.IsPostBack)
            {
                this.txtPageSize.Text = "10";

                BindData();
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "frmLeaveTransactionsLast10Days : Page_Load event");

        }

    }
    void BindData()
    {
        try
        {

            objEmployee.Sort_On = "";
            string strOrderBy = "";

            if (ViewState["Sort_On"] != null)
            {
                objEmployee.Sort_On = ViewState["Sort_On"].ToString() + " " + ViewState["Sort_By"].ToString();
                strOrderBy = ViewState["Sort_On"].ToString() + " " + ViewState["Sort_By"].ToString(); ;
            }

            lblError.Visible = false;

            DateTime dtStartingDate = DateTime.Today.AddDays(-10);
            DateTime dtEndingDate = DateTime.Today;

            for (DateTime dt = dtStartingDate; dt < dtEndingDate; dt = dt.AddDays(1))
            {
                dates.Add(dt);
            }

            DataSet dsTemp = objLeave.getEmployeeAbsentDetailsByDay(dtStartingDate, dtEndingDate.AddDays(-1));

            if (dsTemp != null)
            {
                DataTable dtTemp = dsTemp.Tables[0];
                bool blnDATEEXIST = false;

                List<string> strDateArray = new List<string>();
                string strdt1 = "";
                string strdt2 = "";

                if (dtTemp.Rows.Count > 0)
                {
                    for (int i = 0; i < dates.Count; i++)
                    {

                        for (int j = 0; j < dtTemp.Rows.Count; j++)
                        {
                            strdt1 = dates[i].ToString("yyyy/MM/dd 00:00:00.000");

                            DateTime ldt = Convert.ToDateTime(dtTemp.Rows[j]["AbsentDate"].ToString());

                            strdt2 = ldt.ToString("yyyy/MM/dd 00:00:00.000");

                            if (strdt1.Trim() == strdt2.Trim())
                            {
                                blnDATEEXIST = true;

                            }

                        }
                        if (blnDATEEXIST == false)
                            strDateArray.Add(strdt1);
                        blnDATEEXIST = false;
                    }

                    foreach (string ldt1 in strDateArray)
                    {
                        DataRow dr = dtTemp.NewRow(); //08/08/2012 
                        dr["AbsentDate"] = ldt1.ToString();
                        dr["UserName"] = "No one absent";
                        dtTemp.Rows.Add(dr);

                    }

                    int intRowIndex = 0;
                    bool blnIsFtime = true;
                    bool blnIsMatchFirst = true;
                    string temp = "";
                    string temp1 = "";
                    string temp2 = "";
                    List<string> temp3 = new List<string>();
                    for (intRowIndex = 0; intRowIndex < dtTemp.Rows.Count; intRowIndex++) // logic to group employees absent on same date.
                    {

                        for (int j = 0; j < dtTemp.Rows.Count; j++)
                        {
                            //    j = 1;
                            if (dtTemp.Rows[intRowIndex]["AbsentDate"].ToString().Trim() == dtTemp.Rows[j]["AbsentDate"].ToString().Trim() && (intRowIndex != j))
                            {
                                if (blnIsFtime == true)
                                {
                                    temp3.Add(dtTemp.Rows[intRowIndex]["AbsentDate"].ToString().Trim());
                                }
                                if (!temp3.Contains(dtTemp.Rows[intRowIndex]["AbsentDate"].ToString().Trim()))
                                {
                                    blnIsMatchFirst = true;
                                    temp3.Add(dtTemp.Rows[intRowIndex]["AbsentDate"].ToString().Trim());
                                }
                                else
                                {
                                    if (blnIsFtime == true)
                                    {
                                        blnIsMatchFirst = true;
                                    }
                                    else
                                    {
                                        blnIsMatchFirst = false;
                                        temp3.Add(dtTemp.Rows[intRowIndex]["AbsentDate"].ToString().Trim());
                                    }
                                    //temp3.Add(dtTemp.Rows[intRowIndex]["AbsentDate"].ToString().Trim());
                                }

                                if (dtTemp.Rows[intRowIndex]["Reason"].ToString().Trim() == "")
                                    dtTemp.Rows[intRowIndex]["Reason"] = "Not Defined";
                                if (dtTemp.Rows[j]["Reason"].ToString().Trim() == "")
                                    dtTemp.Rows[j]["Reason"] = "Not Defined";


                                if (blnIsMatchFirst)
                                {

                                    temp1 = "<b>" + dtTemp.Rows[intRowIndex]["UserName"].ToString().Trim() + " : " + "</b>" + dtTemp.Rows[intRowIndex]["Reason"].ToString().Trim();
                                    temp2 = "<b>" + dtTemp.Rows[j]["UserName"].ToString().Trim() + " : " + "</b>" + dtTemp.Rows[j]["Reason"].ToString().Trim();
                                    temp2 = temp1 + "  ,  " + temp2;
                                }
                                else
                                {
                                    if (intRowIndex > j)
                                    {
                                        temp1 = "<b>" + dtTemp.Rows[intRowIndex]["UserName"].ToString().Trim() + " : " + "</b>" + dtTemp.Rows[intRowIndex]["Reason"].ToString().Trim();
                                        temp2 = dtTemp.Rows[j]["Reason"].ToString().Trim() + "  ,  " + temp1;
                                    }
                                    else
                                    {
                                        temp1 = "<b>" + dtTemp.Rows[j]["UserName"].ToString().Trim() + " : " + "</b>" + dtTemp.Rows[j]["Reason"].ToString().Trim();
                                        temp2 = dtTemp.Rows[intRowIndex]["Reason"].ToString().Trim() + "  ,  " + temp1;
                                    }

                                }
                                temp = dtTemp.Rows[intRowIndex]["UserName"].ToString().Trim() + "  ,  " + dtTemp.Rows[j]["UserName"].ToString().Trim();

                                dtTemp.Rows[intRowIndex]["UserName"] = temp;
                                dtTemp.Rows[intRowIndex]["Reason"] = temp2.Replace("@", "'");
                                dtTemp.Rows[j].Delete();
                                // dtTemp.Rows[j].AcceptChanges();
                                j = j - 1;
                                dtTemp.AcceptChanges();
                                blnIsMatchFirst = false;
                                blnIsFtime = false;
                            }


                        }
                    }

                    //Datatype conversion for AbsentDate Column in dtTemp


                    DataTable dtTemp1 = dtTemp.Clone();

                    foreach (DataColumn dc in dtTemp.Columns)
                    {
                        if (dc.ColumnName == "AbsentDate")
                        {
                            dc.DataType = typeof(DateTime);
                        }
                    }

                    foreach (DataRow dr in dtTemp.Rows)
                    {
                        DateTime ldt = Convert.ToDateTime(dr["AbsentDate"].ToString());
                        dr["AbsentDate"] = ldt.ToString("yyyy/MM/dd hh:mm:ss.fff");
                        string strReason = dr["Reason"].ToString();


                        dr["Reason"] = strReason.Replace("@", "'").Trim();
                        dtTemp1.ImportRow(dr);
                    }

                    //End Datatype conversion for AbsentDate Column in dtTemp


                    DataView dv = new DataView(dtTemp1);
                    if (strOrderBy.Trim() != "")
                        dv.Sort = strOrderBy;
                    else
                        dv.Sort = "AbsentDate Desc";
                    if (ViewState["UN"] != null && ViewState["UN"].ToString().Trim() != "")
                    {
                        string strFilter = "UserName Like'%" + ViewState["UN"].ToString() + "%'";
                        dv.RowFilter = strFilter;
                    }
                    if (this.txtPageSize.Text != "")
                    {
                        if (System.Convert.ToInt32(this.txtPageSize.Text) > 0)
                        {
                            this.gvLast10DaysAbsentDetails.PageSize = System.Convert.ToInt32(this.txtPageSize.Text);
                        }
                    }

                    gvLast10DaysAbsentDetails.DataSource = dv;
                    gvLast10DaysAbsentDetails.DataBind();

                    if (dv.ToTable().Rows.Count == 0)
                    {
                        this.Lbl_Pageinfo.Visible = false;
                        lblError.Visible = true;
                        lblError.Text = strError;
                    }
                    else
                    {
                        Int16 intTo;
                        Int16 intFrom;
                        if (gvLast10DaysAbsentDetails.PageSize * (gvLast10DaysAbsentDetails.PageIndex + 1) < dv.ToTable().Rows.Count)
                        {
                            intTo = System.Convert.ToInt16(gvLast10DaysAbsentDetails.PageSize * (gvLast10DaysAbsentDetails.PageIndex + 1));
                        }
                        else
                        {
                            intTo = System.Convert.ToInt16(dv.ToTable().Rows.Count);
                        }
                        intFrom = System.Convert.ToInt16((gvLast10DaysAbsentDetails.PageSize * gvLast10DaysAbsentDetails.PageIndex) + 1);
                        this.Lbl_Pageinfo.Text = "Record(s) " + intFrom + " to " + intTo + " of " + dv.ToTable().Rows.Count;
                        this.Lbl_Pageinfo.Visible = true;
                    }
                }
                else
                {
                    this.Lbl_Pageinfo.Text = "No one was absent in last 10 days";
                    this.Lbl_Pageinfo.Visible = true;
                }

            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "frmLeaveTransactionsLast10Days : BindData Method");
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            objLeave.UserName = txtUserName.Text.Trim();
            objLeave.StartingDate = DateTime.Now.AddDays(-10).ToString();
            objLeave.EndingDate = DateTime.Now.ToString();
            gvLast10DaysAbsentDetails.PageIndex = 0;
            ViewState["UN"] = txtUserName.Text;
            BindData();
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "frmLeaveTransactionsLast10Days : btnSearch_Click event");
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }
    }
    protected void gvLast10DaysAbsentDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            if (ViewState["Sort_On"] != null)
                objLeave.Sort_On = ViewState["Sort_On"].ToString();
            else
                objLeave.Sort_On = "";
            gvLast10DaysAbsentDetails.PageIndex = e.NewPageIndex;
            BindData();
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "frmLeaveTransactionsLast10Days : gvLast10DaysAbsentDetails_PageIndexChanging event");
        }
    }

    protected void gvLast10DaysAbsentDetails_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            objEmployee.Sort_On = e.SortExpression;
            ViewState["Sort_On"] = objEmployee.Sort_On;
            if (ViewState["Sort_By"] == null)
                ViewState["Sort_By"] = "Asc";
            if (ViewState["Sort_By"].ToString() == "Asc")
            {
                ViewState["Sort_By"] = "Desc";
            }
            else
            {
                ViewState["Sort_By"] = "Asc";
            }

            BindData();
        }

        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "frmLeaveTransactionsLast10Days : gvLast10DaysAbsentDetails_Sorting event");
        }
    }



    protected void gvLast10DaysAbsentDetails_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DateTime dt = Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "AbsentDate"));  

                e.Row.Cells[1].Text = dt.ToString("dd/MM/yyyy");
               
            }
        }

        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "frmLeaveTransactionsLast10Days : gvLast10DaysAbsentDetails_RowDataBound event");
        }


    }
}
