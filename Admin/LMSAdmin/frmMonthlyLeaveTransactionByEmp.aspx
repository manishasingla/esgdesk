<%@ Page Language="C#" MasterPageFile="~/Admin/LMSAdmin/AdminMasterMenu.master" AutoEventWireup="true" CodeFile="frmMonthlyLeaveTransactionByEmp.aspx.cs" Inherits="Admin_frmManageEmp" Title="Monthly leave transaction" %>
<%@ Register Assembly="GMDatePicker" Namespace="GrayMatterSoft" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server"> 
  <script type="text/javascript" language="javascript">

function onlyNumbers(evt)
{
    var e = event || evt;
    var charCode = e.which || e.keyCode;
	if ((charCode > 47 && charCode < 58))
    return true;		              
    else
    return false ;		                            
}

function keypressHandler(evt) {

    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode == 13)
        var btn = document.getElementById('<%=btnSearch.ClientID%>');
    btn.focus();
    btn.click();
}
</script>
  <table style="width:100%; padding:10px; height:373" class="publicloginTable" onkeypress="javascript:keypressHandler(event)">
    <tr>
      <td colspan="6" class="LoginTitle"><h1 style="color:Black; font-size:22px;"> Employee leave details , absent between two dates</h1></td>
    </tr>
    
    <tr>
      <td colspan="6" class="LoginTitle" style="color:#333333;"><div style="color:#ffffff; height:25px; margin-bottom: 15px; width:274%; padding-left: 10px; padding-top: 5px; font-weight:bold; display:block; background-color:#282C5F; margin-bottom:15px;">Employee absence details between two dates</div></td>
    </tr>
    
      <tr>
      <td style="width:6%; height:10px" valign="top"> Employee name</td>
      <td align ="center" style="width:1%" valign="top">:</td>
      <td style="width: 10%" valign="top"><asp:TextBox ID="txtEmpName" runat="server" CssClass="input_boxline" ></asp:TextBox></td>
      <td style="height: 10px; width:5%;" valign="top">Page size</td>
      <td align ="center" style="height: 10px; width:2%;" valign="top">:</td>
      <td style="height: 10px; width:1%;" valign="top">
       <asp:TextBox ID="txtPageSize" OnKeypress="return onlyNumbers(event)" runat="server" CssClass="input_boxline" Width="50px"></asp:TextBox>
       <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtPageSize"
       CssClass="lblerror" ErrorMessage="Invalid Number" MaximumValue="999" MinimumValue="1"
       Style="position:relative; height:10px" Type="Integer"></asp:RangeValidator></td>
       <td style="width: 60%; left:10px;  position: absolute; top: 320px; "><asp:Button ID="btnSearch" runat="server" CssClass="btn005" Text="Search" Width="55px" Style="position: relative" OnClick="btnSearch_Click" /></td>
       </tr>
   
      <td valign="top" >Starting date</td>
      <td align="center" valign="top" >:</td>
      <td valign="top" ><asp:TextBox ID="GMDStartDate" runat="server" CssClass="input_boxline" ></asp:TextBox>
        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" 
                            TargetControlID="GMDStartDate"
                            Format="dd/MM/yyyy" PopupButtonID="GMDStartDate" > </ajaxToolkit:CalendarExtender>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                ErrorMessage="Please select a start date" ControlToValidate="GMDStartDate"></asp:RequiredFieldValidator>
        <br />
        <asp:CompareValidator ID="CompareValidator2" runat="server" 
                            ErrorMessage="Please enter a valid  date" Operator="DataTypeCheck" 
                            ControlToValidate="GMDStartDate" Type="Date"></asp:CompareValidator></td>
      <td style="width:5%; height:10px" valign="top" >Ending date</td>
      <td align="center" style="width:2%" valign="top" >:</td>
      <td style="width:12%;" valign="top" >
      <asp:TextBox ID="GMDEndDate" runat="server" CssClass="input_boxline"></asp:TextBox>
        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" 
                            TargetControlID="GMDEndDate"
                            Format="dd/MM/yyyy" PopupButtonID="GMDEndDate" > </ajaxToolkit:CalendarExtender>

        <asp:CompareValidator ID="CompareValidator3" runat="server" 
                            ErrorMessage="Please enter a valid date" Operator="DataTypeCheck" 
                            ControlToValidate="GMDEndDate" Type="Date"></asp:CompareValidator>
     
        <asp:CompareValidator ID="CompareValidator1" runat="server" 
                                ErrorMessage="End date should be greater or equal to start date." 
                                ControlToCompare="GMDStartDate" ControlToValidate="GMDEndDate" 
                                Operator="GreaterThanEqual" Type="Date"></asp:CompareValidator>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <%--<asp:Button ID="btnFilterByDate" runat="server" Text="Filter By Date" 
                            onclick="btnFilterByDate_Click" />--%></td>
    </tr>
   <!-- <tr>
      <td colspan="7" style=" height:10px"></td>
    </tr>-->
    <tr>
      <td colspan="7" valign="top">
      <asp:GridView ID="gvEmp" runat="server" AutoGenerateColumns="False"
        Style="width: 100%; height:100%;" 
                AllowPaging="True" AllowSorting="True" 
                OnPageIndexChanging="gvEmp_PageIndexChanging" OnSorting="gvEmp_Sorting" 
                OnRowCommand="gvEmp_RowCommand" 
                 HorizontalAlign="Left" 
                PageSize="20" onrowdatabound="gvEmp_RowDataBound">
          <Columns>
          <asp:TemplateField HeaderText="User name" SortExpression="UserName" Visible="false">
            <ItemTemplate>
              <asp:Label runat="server" ID="lblUserName" Text='<%#Eval("username")%>'></asp:Label>
            </ItemTemplate>
          </asp:TemplateField>
          <asp:TemplateField HeaderText="Employee name" SortExpression="EmpName">
            <HeaderStyle Width="35%" />
            <ItemTemplate>
              <asp:LinkButton ID="lnkBtnCountryName" Text='<%#Eval("EmpName")%>' runat="server"  CommandArgument= '<%#string.Format("{0}",Eval("UserName"))%>' CommandName="DETAILS"></asp:LinkButton>
            </ItemTemplate>
          </asp:TemplateField>
          <asp:TemplateField HeaderText="Email address">
            <ItemTemplate> <%#Eval("Email")%> </ItemTemplate>
          </asp:TemplateField>
          <asp:TemplateField HeaderText="Contact number">
            <ItemTemplate> <%#Eval("ContactNo")%> </ItemTemplate>
          </asp:TemplateField>
          <asp:TemplateField HeaderText="Absent Days">
            <ItemTemplate>
              <asp:Label ID="lblTotalBalance" runat="server"></asp:Label>
            </ItemTemplate>
          </asp:TemplateField>
          </Columns>
        </asp:GridView></td>
      <td colspan="6"><asp:Panel ID="pnlAbsentDetails" runat="server" width="90%" style="margin-left:10px;margin-right:20px"
                                   BackColor="#CCCCFF" BorderColor="#6600FF" 
                BorderStyle="Ridge" Height="100%" 
                                   Visible="False" GroupingText="Absent details" 
                Direction="LeftToRight" HorizontalAlign="Left" ToolTip="Absent Details">
          <table cellpadding="0" cellspacing="0" style="height: 100%; width:100%">
            <tr style="height:20px">
              <td><asp:Label ID="lblEmpName" runat="server" Font-Bold="True" Font-Size="Medium">Employee name :</asp:Label></td>
              <td><asp:Label ID="lblEmpNameValue" runat="server" Font-Bold="True"></asp:Label></td>
            </tr>
            <tr style="height:20px">
              <td><asp:Label ID="lblLeaveDays" runat="server" Font-Bold="True" Font-Size="Medium" >Absent Days :</asp:Label></td>
              <td><asp:Label ID="lblLeaveDaysValue" runat="server" Font-Bold="True" 
                                                ForeColor="Red"></asp:Label></td>
            </tr>
            <tr>
              <td colspan="2"><asp:GridView ID="gvAbsentDates" runat="server" AllowPaging="True" 
                                            Height="100%" PageSize="20" style="margin-top: 9px" Width="100%" 
                                            onpageindexchanging="gvAbsentDates_PageIndexChanging" 
                                            onrowdatabound="gvAbsentDates_RowDataBound"> </asp:GridView></td>
            </tr>
          </table>
          </asp:Panel></td>
    </tr>
    <tr>
      <td colspan="7"><asp:Label ID="lblError" runat="server"  Visible="false" CssClass="lblerror"></asp:Label>
        <asp:Label ID="Lbl_Pageinfo" runat="server" Style="position: relative"></asp:Label></td>
      <td align="right" colspan="7">&nbsp;</td>
    </tr>
  </table>
</asp:Content>
