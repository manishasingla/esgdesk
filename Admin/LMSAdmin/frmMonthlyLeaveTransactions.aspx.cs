using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Admin_frmMonthlyLeaveTransactions : System.Web.UI.Page
{
    clsLeave_Logic objLeave = new clsLeave_Logic();
    clsEmployee_Logic1 objEmployee = new clsEmployee_Logic1();
    private string strError = "No records found.";
    DataView dv = null;
    string dtSDate;
    string dtEDate;
    DateTime datetime;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null)
        {
            Response.Redirect("~/Admin/login.aspx");
        }
        else
        {

        }
        if (!Page.IsPostBack)
        {
            //this.txtPageSize.Text = "10";

            BindData();
        }
    }


    static DateTime LastDayOfYear()
    {
        return LastDayOfYear(DateTime.Today);
    }


    static DateTime LastDayOfYear(DateTime d)
    {
        // 1
        // Get first of next year
        DateTime n = new DateTime(d.Year + 1, 1, 1);
        // 2
        // Subtract 1 from it
        return n.AddDays(-1);
    }

    static DateTime FirstDayOfYear()
    {
        return FirstDayOfYear(DateTime.Today);
    }


    static DateTime FirstDayOfYear(DateTime y)
    {
        return new DateTime(y.Year, 1, 1);
    }
   

    void BindData()
  {
        try
        {           
            objLeave.Sort_On = "";
            if (ViewState["Sort_On"] != null)
                objLeave.Sort_On = ViewState["Sort_On"].ToString() + " " + ViewState["Sort_By"].ToString();

            DateTime d = DateTime.Now;

            if (GMDStartDate.Text == null || GMDStartDate.Text.Trim() == "")
            {
                dtSDate = FirstDayOfYear(d).ToString("dd/MM/yyyy");
                GMDStartDate.Text = dtSDate;
                datetime=   Convert.ToDateTime(dtSDate);
                dtSDate = datetime.ToString("yyyy/MM/dd");

                objLeave.StartingDate = dtSDate;
                
            }
            else
            {
                datetime = Convert.ToDateTime(GMDStartDate.Text.Trim());
                dtSDate = datetime.ToString("yyyy/MM/dd");
                objLeave.StartingDate = dtSDate; 
            }
            if (GMDEndDate.Text == null || GMDEndDate.Text.Trim() == "")
            {
                dtEDate = LastDayOfYear(d).ToString("dd/MM/yyyy");
                GMDEndDate.Text = dtEDate;
                datetime = Convert.ToDateTime(dtEDate);
                dtEDate = datetime.ToString("yyyy/MM/dd");
                objLeave.EndingDate = dtEDate;
            }
            else
            {

                datetime = Convert.ToDateTime(GMDEndDate.Text.Trim());
                dtEDate = datetime.ToString("yyyy/MM/dd");
                objLeave.EndingDate = dtEDate;
            }
            DataSet dsTemp = objLeave.GetMonthlyLeavetransactions();
            if (dsTemp != null && dsTemp.Tables.Count > 0 && dsTemp.Tables[0].Rows.Count > 0)
            {
                DataTable dtTemp = dsTemp.Tables[0];

                dv = new DataView(dtTemp);

                if (txtUserName.Text.Trim() != "")
                {
                    dv.RowFilter = "UserName Like'%" + txtUserName.Text.Trim() + "%'";
                }

                if (this.txtPageSize.Text != "")
                {
                    if (System.Convert.ToInt32(this.txtPageSize.Text) > 0)
                    {
                        this.gvLeaveDetails.PageSize = System.Convert.ToInt32(this.txtPageSize.Text);
                    }
                }
                gvLeaveDetails.Visible = true;
                gvLeaveDetails.DataSource = dv;
                gvLeaveDetails.DataBind();

                if (dv.ToTable().Rows.Count == 0)
                {
                    this.Lbl_Pageinfo.Visible = false;
                    lblError.Visible = true;
                    lblError.Text = strError;
                }
                else
                {
                    Int16 intTo;
                    Int16 intFrom;
                    lblError.Visible = false;
                    if (gvLeaveDetails.PageSize * (gvLeaveDetails.PageIndex + 1) < dv.ToTable().Rows.Count)
                    {
                        intTo = System.Convert.ToInt16(gvLeaveDetails.PageSize * (gvLeaveDetails.PageIndex + 1));
                    }
                    else
                    {
                        intTo = System.Convert.ToInt16(dtTemp.Rows.Count);
                    }
                    intFrom = System.Convert.ToInt16((gvLeaveDetails.PageSize * gvLeaveDetails.PageIndex) + 1);
                    this.Lbl_Pageinfo.Text = "Record(s) " + intFrom + " to " + intTo + " of " + dv.ToTable().Rows.Count;
                    this.Lbl_Pageinfo.Visible = true;
                }
            }
            else
            {
                gvLeaveDetails.Visible = false;
                Lbl_Pageinfo.Visible = false;
                lblError.Text = "Records not found";
                lblError.Visible = true;
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "frmMonthlyLeaveTransaction : Binddata() Method");
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            dtSDate = GMDStartDate.Text;
            dtEDate = GMDEndDate.Text;

            datetime = Convert.ToDateTime(dtSDate);
            dtSDate = datetime.ToString("yyyy/MM/dd");

            if (GMDEndDate.Text.Trim() != "")
            {
                datetime = Convert.ToDateTime(dtEDate);
                dtEDate = datetime.ToString("yyyy/MM/dd");
            }
            objLeave.UserName = txtUserName.Text.Trim();
            objLeave.StartingDate = dtSDate;
            objLeave.EndingDate = dtEDate;


            gvLeaveDetails.PageIndex = 0;
            // if (txtUserName.Text.Trim() != "")
            BindData();
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "frmMonthlyLeaveTransaction : btnSearch_Click event");
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }
    }
    protected void gvLeaveDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            if (ViewState["Sort_On"] != null)
                objLeave.Sort_On = ViewState["Sort_On"].ToString();
            else
                objLeave.Sort_On = "";
            gvLeaveDetails.PageIndex = e.NewPageIndex;
            BindData();
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "frmMonthlyLeaveTransaction : gvLeaveDetails_PageIndexChanging event");
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }
    }

    protected void gvLeaveDetails_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            objEmployee.Sort_On = e.SortExpression;
            ViewState["Sort_On"] = objEmployee.Sort_On;
            if (ViewState["Sort_By"] == null)
                ViewState["Sort_By"] = "Asc";
            if (ViewState["Sort_By"].ToString() == "Asc")
            {
                ViewState["Sort_By"] = "Desc";
            }
            else
            {
                ViewState["Sort_By"] = "Asc";
            }

            BindData();
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "frmMonthlyLeaveTransaction : gvLeaveDetails_Sorting event");
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }
    }
    protected void gvLeaveDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
    }

    protected void gvLeaveDetails_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblUSerNamel = ((Label)e.Row.FindControl("lblUserName"));
                double dblUsed = 0.0;
                objLeave.UserName = lblUSerNamel.Text;
                objLeave.GetTotalBalanceDaysInOneBWDate(DateTime.Now.Year);
                // e.Row.Cells[4].Text = objLeave.fnGetTotalOfBalanceLeave(DateTime.Now.Year) + " / " + strTotal;//fnGetTotalOfBalanceLeave() return total of leaves taken by employee.
                dblUsed = objLeave.CasualBalanceLeaves + objLeave.MedicalBalanceLeaves + objLeave.EarnBalanceLeaves + objLeave.HalfPaidBalanceLeaves + objLeave.ExtraBalanceLeaves;
                 e.Row.Cells[1].Text = dblUsed.ToString();//fnGetTotalOfBalanceLeave() return total of leaves taken by employee.
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "frmMonthlyLeaveTransaction : gvLeaveDetails_RowDataBound event");
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }
   
    }


   
}
