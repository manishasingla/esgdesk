<%@ Page Language="C#" MasterPageFile="~/Admin/LMSAdmin/AdminMasterMenu.master" AutoEventWireup="true" CodeFile="frmWhoIsOnLeaveToady.aspx.cs" Inherits="Admin_frmMonthlyLeaveTransactions" Title="Employee on leave today" %>

<%@ Register Assembly="GMDatePicker" Namespace="GrayMatterSoft" TagPrefix="cc1" %>
<%@ Register Assembly="GMDatePicker" Namespace="GrayMatterSoft" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


    <script type="text/javascript" language="javascript">
function toggleAllCheckboexs(toggle) {


				n = document.forms[0].length;
				var frm = document.forms[0];
				for(i=0;i<frm.length;i++)
				
					if(frm.elements[i].type=="checkbox")
						if (frm.elements[i].name.indexOf('Cbx')==0)
							frm.elements[i].checked=toggle;
			}
				function onlyNumbers(evt)
{
    var e = event || evt;
    var charCode = e.which || e.keyCode;
	if ((charCode > 47 && charCode < 58))
    return true;		             
    else
    return false ;		                            
}
</script>

    <table style="width:81%" height="373" class="publicloginTable">
    
                <tr>
                        <td colspan="6" class="LoginTitle"><h1 style="color:Black">
                             Employee on leave todays</h1>
                        </td>
                          
                           
                    </tr>     
        <tr>
            <td valign="top" align="left">
                <table width="100%" class="admintablestyle">
                    <tr>
                        <td colspan="6" class="LoginTitle">
                            &nbsp;Leave Transactions
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 17%">
                            User name</td>
                        <td style="width: 2%">
                            :</td>
                        <td style="width: 18%">
                            <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
                        </td>
                        <td colspan="3">
                        </td>
                   
                        <td style="height: 28px">
                            Page size</td>
                        <td style="height: 28px">
                            :</td>
                        <td style="height: 28px">
                            <asp:TextBox ID="txtPageSize" OnKeypress="return onlyNumbers(event)" runat="server" Width="50px"></asp:TextBox>
                            <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtPageSize"
                                CssClass="lblerror" ErrorMessage="Invalid Number" MaximumValue="999" MinimumValue="1"
                                 Type="Integer"></asp:RangeValidator>
                        </td>
                        <td style="height: 28px">
                            </td>
                    
                        <td>
                            <asp:Button ID="btnSearch" runat="server" Text="Search" Width="55px" OnClick="btnSearch_Click" /></td>
                           
                    </tr>
                    <tr>
                        <td colspan="11">
                            &nbsp;
                        </td>
                         
                    </tr>
                    <tr>
                        <td colspan="11">
                        <table border="0" width="100%" ><tr>
                        <td align="left" style="width: 321px">
                            &nbsp;</td> 
                        <td align="right">
                            &nbsp;</td></tr>
                       <tr><td colspan="6">                       
                           <asp:GridView ID="gvMonth" runat="server" AllowPaging="true" 
                               AllowSorting="true"  AutoGenerateColumns="False" Width="100%"  
                               OnSorting="gvMonth_Sorting" OnPageIndexChanging="gvMonth_PageIndexChanging" 
                               onrowcommand="gvMonth_RowCommand" PageSize="20">
                                <Columns>
                              
                                  
                 <asp:TemplateField HeaderText="Employee name" SortExpression="EmpName">
                                   <HeaderStyle Width="30%" />
                                    <ItemTemplate>     
                                         <%#Eval("EmpName")%>  
<%--                 <asp:LinkButton ID="lnkBtnUserName" Text='<%#Eval("EmpName")%>' runat="server" CommandArgument= '<%#string.Format("{0}",Eval("UserName"))%>' CommandName="CALCULATELEAVE"></asp:LinkButton>    --%>    
                                   </ItemTemplate>
                                 </asp:TemplateField>
                                  <%--  <asp:TemplateField HeaderText="LeaveType" SortExpression="LeaveTypeName">
                                                    <ItemTemplate>
                                                        <%#Eval("LeaveTypeName")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                    <asp:TemplateField HeaderText="StartingDate" SortExpression="StartingDate">
                                    <ItemTemplate>
                                    <%#Eval("StartingDate") %>
                                    </ItemTemplate>
                                    </asp:TemplateField>--%>

                                      <asp:TemplateField HeaderText="Reason" SortExpression="Reason">
                                    <ItemTemplate>
                                    <%#Eval("Reason")%>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView> </td></tr>
                            
                        
                   
                    <tr>
                        <td width="50%">
                            <asp:Label ID="lblError" CssClass="lblerror" Visible="false"  runat="server" Text=""></asp:Label></td>
                       <td align="right">
                           <asp:Label ID="Lbl_Pageinfo" runat="server" Text=""></asp:Label></td> 
                        </tr> 
                    
                </table>
                 </td></tr>
                 </table>
    
    
    
    </td>
    </tr>
    </table>

</asp:Content>

