using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Admin_frmMonthlyLeaveTransactions : System.Web.UI.Page
{
    clsLeave_Logic objMonth = new clsLeave_Logic();
    clsEmployee_Logic1 objEmployee = new clsEmployee_Logic1();
    private string strError = "No records found";
    DataView dv = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null)
        {
            Response.Redirect("~/Admin/login.aspx");
        }
        else
        {

        }
        if (!Page.IsPostBack)
        {
            this.txtPageSize.Text = "10";

            BindData();
        }
    }
    void BindData()
    {
        try
        {
            
            objMonth.Sort_On = "";
            if (ViewState["Sort_On"] != null)
                objMonth.Sort_On = ViewState["Sort_On"].ToString() + " " + ViewState["Sort_By"].ToString();
            lblError.Visible = false;
          
            DataSet dsTemp = objMonth.getEmployeeOnLeaveToday();
            DataTable dtTemp = dsTemp.Tables[0];

            if (dtTemp.Rows.Count > 0)
            {
                lblError.Visible = false;
              
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = strError;
              
            }

            dv = new DataView(dtTemp);
            dv.Sort = objMonth.Sort_On;

            if (ViewState["EN"] != null && ViewState["EN"].ToString().Trim() != "")
            {
                string strFilter = "EmpName Like'%" + ViewState["EN"].ToString() + "%'";
                dv.RowFilter = strFilter;
            }
           
         

            if (this.txtPageSize.Text != "")
            {
                if (System.Convert.ToInt32(this.txtPageSize.Text) > 0)
                {
                    this.gvMonth.PageSize = System.Convert.ToInt32(this.txtPageSize.Text);
                }
            }
            if (dv.ToTable().Rows.Count>0)
            {
                gvMonth.Visible = true;
                gvMonth.DataSource = dv;
                gvMonth.DataBind();
            }
            else
            {
                gvMonth.Visible = false;
                lblError.Visible = true;
                lblError.Text = strError;

            }
            if (dv.ToTable().Rows.Count <= 0)
            {
                this.Lbl_Pageinfo.Visible = false;
            }
            else
            {
                Int16 intTo;
                Int16 intFrom;
                if (gvMonth.PageSize * (gvMonth.PageIndex + 1) < dv.ToTable().Rows.Count)
                {
                    intTo = System.Convert.ToInt16(gvMonth.PageSize * (gvMonth.PageIndex + 1));
                }
                else
                {
                    intTo = System.Convert.ToInt16(dv.ToTable().Rows.Count);
                }
                intFrom = System.Convert.ToInt16((gvMonth.PageSize * gvMonth.PageIndex) + 1);
                this.Lbl_Pageinfo.Text = "Record(s) " + intFrom + " to " + intTo + " of " + dv.ToTable().Rows.Count;
                this.Lbl_Pageinfo.Visible = true;
            }
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string strError;

            ViewState["EN"] = txtUserName.Text.Trim();        
        
        strError = "No records found";
        gvMonth.PageIndex = 0;
        if (txtUserName.Text.Trim() != "")
        BindData();
    }
    protected void gvMonth_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (ViewState["Sort_On"] != null)
            objMonth.Sort_On = ViewState["Sort_On"].ToString();
        else
            objMonth.Sort_On = "";
        gvMonth.PageIndex = e.NewPageIndex;
        BindData();
    }
    protected void gvMonth_Sorting(object sender, GridViewSortEventArgs e)
    {
        //objMonth.Sort_On = e.SortExpression;
        //ViewState["Sort_On"] = objMonth.Sort_On;
        //if (ViewState["Sort_By"] == null)
        //    ViewState["Sort_By"] = "Asc";
        //if (ViewState["Sort_By"].ToString() == "Asc")
        //{
        //    ViewState["Sort_By"] = "Desc";
        //}
        //else
        //{
        //    ViewState["Sort_By"] = "Asc";
        //}

        //BindData();
        objEmployee.Sort_On = e.SortExpression;
        ViewState["Sort_On"] = objEmployee.Sort_On;
        if (ViewState["Sort_By"] == null)
            ViewState["Sort_By"] = "Asc";
        if (ViewState["Sort_By"].ToString() == "Asc")
        {
            ViewState["Sort_By"] = "Desc";
        }
        else
        {
            ViewState["Sort_By"] = "Asc";
        }

        BindData();
    }
    protected void gvMonth_RowCommand(object sender, GridViewCommandEventArgs e)
    {
       
        
    }
}
