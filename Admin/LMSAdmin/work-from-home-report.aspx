﻿<%@ Page Title="Work from home report" Language="C#" MasterPageFile="~/Admin/LMSAdmin/AdminMasterMenu.master"
    AutoEventWireup="true" CodeFile="work-from-home-report.aspx.cs" Inherits="Admin_LMSAdmin_LeaveReport" EnableEventValidation = "false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="tab-content-holder">
        <h2 style="font-weight: normal !important; font-size: 20px !important; margin-top: 5px;
            margin-bottom: -5px; padding-left: 20px;">
            Work from home report</h2>
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td width="11%" align="left" valign="top">
                    <div class="treeview_div_left" style="height: 968px;">
                        <ul id="treemenu2" class="treeview" style="padding-left: 10px;">
                            <li><a href="../LMSAdmin/LMS-Admin.aspx">LMS home</a></li>
                            <li><a href="#">Manage</a>
                                <ul rel="open" style="display: block;">
                                    <li><a href="../LMSAdmin/LMS-Leave-setting.aspx" class="settings">Annum</a></li>
                                    <li><a href="../LMSAdmin/LMS-Add-a-leave.aspx" class="edit_leave">Emps</a></li>
                                    <li><a href="../LMSAdmin/SaturdaySundayLeave.aspx" class="edit_leave">Weekend</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Reports</a>
                                <ul rel="open" style="display: block;">
                                    <li><a href="../LMSAdmin/LMS-Leaves-between-2dates-report.aspx" class="documents">All</a></li>
                                    <li><a href="../LMSAdmin/LMS-Complete-leaves-report.aspx" class="leave_report">Detail</a></li>
                                    <li><a href="../LMSAdmin/LMS-In-and-outtime-report.aspx" class="leave_report">In/outtime</a></li>
                                    <li><a href="../LMSAdmin/LeaveReport.aspx" class="leave_report">Leave Report</a></li>
                                    <li><a href="../LMSAdmin/work-from-home-report.aspx" class="leave_report">Work from home</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </td>
                <td width="78%" align="left" valign="top">
                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td width="99%" valign="top">
                                <div style="line-height: 18px; height: auto;">
                                    <table width="99%" cellpadding="0" cellspacing="0" border="0">
                                        <tr valign="top">
                                            <td width="22%">
                                                <div style="margin-bottom: 20px;">
                                                    <fieldset style="border:0; background:none;">
                                                    <legend style="font-size: 11px; padding: 0 2px; width:100%; margin:0 auto;">
                                                        <table width="100%">
                                                            <tr>
                                                                <td>                                                                 
                                                                    Select EmpName:
                                                                        <asp:DropDownList ID="ddlEmpList" runat="server" 
                                                                             OnSelectedIndexChanged="ddlEmpList_SelectedIndexChanged" AutoPostBack="True"
                                                                            CausesValidation="true" ValidationGroup="g1">
                                                                        </asp:DropDownList> 
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" 
                                                                                                    runat="server" 
                                                                                                    ControlToValidate="ddlEmpList"
                                                                                                    InitialValue="0"
                                                                                                    ErrorMessage="Please select user."
                                                                                                    Text="*"
                                                                                                     ValidationGroup="g1">
                                                                       </asp:RequiredFieldValidator>
                                                                   
                                                               
                                                                   
                                                                        Select Year:
                                                                        <asp:DropDownList ID="ddlYear" runat="server" 
                                                                           AutoPostBack="True" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged">
                                                                           </asp:DropDownList> 
                                                                         <asp:RequiredFieldValidator ID="RfvYear" 
                                                                                                    runat="server" 
                                                                                                    ControlToValidate="ddlYear"
                                                                                                    InitialValue="0"
                                                                                                    ErrorMessage="Please select Year."
                                                                                                    Text="*"
                                                                                                     ValidationGroup="g1">
                                                                       </asp:RequiredFieldValidator>                                                                   
                                                               
                                                                        <asp:Button ID="btnShow" runat="server" Text="Show Report" OnClick="btnShow_Click" ValidationGroup="g1" />                                                                

                                                                </td>
                                                                <td><asp:ImageButton ID="btnExport" runat="server" ImageUrl="~/xls.png" OnClick = "ExportToExcel" /></td>
                                                            </tr>
                                                        </table>
                                                        <table width="99%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-top: 10px;
                                                            margin-left: 2px;">
                                                              <tr>
                                                                <td align="center" valign="top" bgcolor="#F4F4F4" class="leave_border align">
                                                                    <!--<div style=" height:578px; overflow-y:scroll; background:#f3f3f3;">-->
                                                                    <asp:GridView ID="gvAbsentEdit" runat="server" PageSize="20" Style="position: relative;
                                                                        top: 0px; left: 0px; width: 100%;" OnPageIndexChanging="gvAbsentEdit_PageIndexChanging"
                                                                        AutoGenerateColumns="False" AllowSorting="True" CaptionAlign="Top" BackColor="#ffffff"
                                                                        CssClass="leavetbl">
                                                                        <AlternatingRowStyle BackColor="#f7f7f7" />
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="Absent date" SortExpression="AbsentDate">
                                                                                <HeaderStyle Width="16%" Height="35px" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="lnkBtnUserName" Text='<%#Eval("AbsentDate")%>' runat="server"
                                                                                        CommandArgument='<%#string.Format("{0}|{1}|{2}|{3}",Eval("UserName"),Eval("AbsentDate"),Eval("LeaveType"),Eval("PaidOrNPaid"))%>'
                                                                                        CommandName="Edt"></asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Day" SortExpression="Day">
                                                                                <HeaderStyle Width="10%" Height="35px" />
                                                                                <ItemTemplate>
                                                                                    <%#Eval("Day")%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Reason" SortExpression="Reason">
                                                                                <HeaderStyle Width="36%" Height="35px" />
                                                                                <ItemTemplate>
                                                                                    <%#Eval("Reason")%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Paid Or Unpaid" SortExpression="PaidOrNPaid">
                                                                                <HeaderStyle Width="10%" Height="35px" />
                                                                                <ItemTemplate>
                                                                                    <%#Eval("PaidOrNPaid")%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Leave type" SortExpression="LeaveType">
                                                                                <HeaderStyle Width="9%" Height="35px" />
                                                                                <ItemTemplate>
                                                                                    <%#Eval("LeaveType")%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Is HPL?" SortExpression="IsEHPL">
                                                                                <HeaderStyle Width="6%" Height="35px" />
                                                                                <ItemTemplate>
                                                                                    <%#Eval("IsEHPL")%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Informed?" SortExpression="">
                                                                                <HeaderStyle Width="6%" />
                                                                                <ItemTemplate>
                                                                                    <%#Eval("IsPlanned")%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Deductable?" SortExpression="">
                                                                                <HeaderStyle Width="8%" />
                                                                                <ItemTemplate>
                                                                                    <%#Eval("Deductable")%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                        <FooterStyle Height="20px" />
                                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                        <RowStyle Height="30px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    </asp:GridView>
                                                                    <!--</div>-->
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="trNoRecord" style="display: none;">
                                                                <td>
                                                                    No Record Found For Given Employee
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="40" align="left" valign="middle" style="font-size: 14px;">
                                                                    <asp:Label runat="server" ID="lblTotalTitle" Text="Totals : " Visible="false"></asp:Label>
                                                                    <asp:Label ID="lblTotalBalance" runat="server" Text=""></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                      </legend>
                                                    </fieldset>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <script type="text/javascript">
            ddtreemenu.createTree("treemenu2", true, 5)
        </script>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableClientScript="true"
            ShowMessageBox="true" ShowSummary="false" ValidationGroup="g1" DisplayMode="BulletList" />      
    </div>
    <div id="MaskedDiv" class="MaskedDiv">
    </div>
    <!-- <div id="ModalPopupDiv" class="ModalPopup" style="top: 299px; left: 42%;"> </div>-->
    <style type="text/css">
        div.MaskedDiv
        {
            visibility: hidden;
            position: absolute;
            left: 0px;
            top: 0px;
            font-family: verdana;
            font-weight: bold;
            padding: 40px;
            z-index: 100; /* background-image:url(Mask.png); */ /* ieWin only stuff */ /* _background-image:none; */
            background-color: #333333;
            opacity: 0.7;
            filter: alpha(opacity=70); /*_filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled=true, sizingMethod=scale src='Mask.png'); */
        }
        
        
        div.ModalPopup
        {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 11px;
            font-style: normal;
            background-color: #CCCCCC;
            position: absolute; /* set z-index higher than possible */
            z-index: 10000;
            visibility: hidden;
            color: Black;
            border-style: solid;
            border-color: #999999;
            border-width: 1px;
            width: 1000px;
            height: auto;
            left: 42%;
        }
    </style>
    <asp:HiddenField ID="hdnPOUP" runat="server" />
    <asp:HiddenField ID="hdnDOND" runat="server" />
    <asp:HiddenField ID="hdnIsHPL" runat="server" />
</asp:Content>
