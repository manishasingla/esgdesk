﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Drawing;

public partial class Admin_LMSAdmin_LeaveReport : System.Web.UI.Page
{
    clsEmployee_Logic1 objEmployee = new clsEmployee_Logic1();
    LeaveSatardaySunday dal = new LeaveSatardaySunday();
    clsLeave_Logic objLeave = new clsLeave_Logic();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fnBindEmpddl();
            BindddlYear();
            bindGird();
            if (ddlEmpList.SelectedIndex != 0)
            {
                string strUserName = ddlEmpList.SelectedItem.Text;
                this.Title = "Work from home - " + strUserName;
            }
        }
        
    }
    public void fnBindEmpddl()
    {
        try
        {
            DataSet ds = objEmployee.GetEmployee(); //DatabaseHelper.getDataset("Select (firstname+' '+lastname) as EmpName , username from users where IsCW='Y' order by EmpName Asc");
            if (ds != null)
            {
                if (ds.Tables.Count > 0)
                {
                    ddlEmpList.DataSource = ds;
                    ddlEmpList.DataValueField = "username";
                    ddlEmpList.DataTextField = "EmpName";
                    ddlEmpList.Items.Insert(0, "--Select--");
                    ddlEmpList.DataBind();
                    ddlEmpList.Items.Insert(0, new ListItem("--Select----", "0"));
                }
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LeaveReport : fnBindEmpddl");
        }

    }
    public void BindddlYear()
    {
        try
        {
            int year;
            year = DateTime.Now.Year;
            int YearAtMid = year - 6;
            int j = 1;
            for (int i = 0; i <= 12; i++)
            {
                if (i <= 6)
                {
                    ddlYear.Items.Add(Convert.ToString(YearAtMid + i));
                    
                }
                else
                {
                    ddlYear.Items.Add(Convert.ToString(year + j));
                    j++;
                }
                ddlYear.SelectedIndex = 6;
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LeaveReport : BindddlYear");
        }
    }
    public void bindGird()
    {
        string strUserName = ddlEmpList.SelectedValue;//.SelectedItem.Text;
        string strYear = ddlYear.SelectedItem.Text;
        int nYear = Convert.ToInt32(strYear);

        DataSet dsGrid = dal.GetAbsentDetails(strUserName, nYear,"2");
        if (dsGrid.Tables.Count > 0)
        {
            if (dsGrid.Tables[0].Rows.Count > 0)
            {
                gvAbsentEdit.DataSource = null;
                gvAbsentEdit.DataBind();
                gvAbsentEdit.DataSource = dsGrid;
                gvAbsentEdit.DataBind();
                trNoRecord.Attributes.Add("style", "display:none;");
            }
            else
            {
                gvAbsentEdit.DataSource = null;
                gvAbsentEdit.DataBind();
                trNoRecord.Attributes.Add("style", "display:block;");
            }
        }
        else
        {
            gvAbsentEdit.DataSource = null;
            gvAbsentEdit.DataBind();
            trNoRecord.Attributes.Add("style", "display:block;");
        }
        if (ddlEmpList.SelectedIndex != 0)
        {
            this.Title = "Work from home - " + ddlEmpList.SelectedItem.Text;
        }
    }
    protected void ddlEmpList_SelectedIndexChanged(object sender, EventArgs e)
    {
        gvAbsentEdit.DataSource = null;
        gvAbsentEdit.DataBind();
        trNoRecord.Attributes.Add("style", "display:block;");
        if (ddlEmpList.SelectedIndex != 0)
        {
            bindGird();
        }
       
    }
    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        gvAbsentEdit.DataSource = null;
        gvAbsentEdit.DataBind();
        trNoRecord.Attributes.Add("style", "display:block;");
        if (ddlYear.SelectedIndex != 0)
        {
            bindGird();
        }
    }
    protected void gvAbsentEdit_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvAbsentEdit.PageIndex = e.NewPageIndex;
        bindGird();
    }
    protected void btnShow_Click(object sender, EventArgs e)
    {
        bindGird();
    }

    protected void ExportToExcel(object sender, EventArgs e)
    {
        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename="+ddlEmpList.SelectedItem.Text+"_WFH.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        using (StringWriter sw = new StringWriter())
        {
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            //To Export all pages
            //gvAbsentEdit.AllowPaging = false;
            this.bindGird();
            if (gvAbsentEdit.Rows.Count > 0)
            {
                gvAbsentEdit.RenderControl(hw);


                //style to format numbers to string
                string style = @"<style> .textmode { } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
}