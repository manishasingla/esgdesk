using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Globalization;

public partial class Admin_frmEmpLeaveDetails : System.Web.UI.Page
{
    clsEmployee_Logic1 objEmployee = new clsEmployee_Logic1();
    clsLeave_Logic objLeave = new clsLeave_Logic();
    String dtDayOfLeave = "";
    //static DataView dv = null;
    //static string strLeaveType = "";
    //static string strPONP = "";

    DataView dv = null;
    string strLeaveType = "";
    string strPONP = "";

    bool blnIsBalanceAvailable = true;
    double TotalBalanceLeave = 0;
    string strHeader = "";
    string strLeaveInfo = "";
    string strEmailSucess = "";
    int intCounter = 0;
    string strDay = "";
    bool blnCME = false;
    bool blnHPL = false;
    DataSet ds = new DataSet();


    //static double sdblUsedLeave = 0.0;
    //static double sdblTotal = 0.0;
    //static double sdblRBal = 0.0;
    //static DateTime DTSDate;
    //static DateTime dtDayOfLeave1 = System.DateTime.Now;


    double sdblUsedLeave = 0.0;
    double sdblTotal = 0.0;
    double sdblRBal = 0.0;

    DateTime DTSDate;
    DateTime dtDayOfLeave1 = System.DateTime.Now;

    DataSet dsTemp = new DataSet();
    DataTable dtTemp = new DataTable();
    bool blnIsPlanned = false;
    bool blnIsDOND = false;
    object obj = 1;
   // public static int sintYear;


    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["admin"] != null && Session["admin"].ToString() != "")
            {

                if (DatabaseHelper.isAdmin(Session["admin"].ToString())==false)
                {
                    Response.Redirect("~/Admin/login.aspx");
                    return;
                }
             }

                    if (!IsPostBack)
                    {
                        Session["Edit"] = null;

                        fnBindEmpddl();
                       // BindData();
                       // BindData4EditGrid();
                       // BindLeavetype();
                          BindddlYear();

                    }
              

        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Add-a-Leave : Page_Load Event");
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }
    }

    void BindData()
    {
        try
        {
            if (ddlEmpList.SelectedItem != null)
            {
                if (ddlEmpList.SelectedItem.Text != "--Select--")
                {
                   // txtUserName.Text = ddlEmpList.SelectedItem.Text.Trim();

                    if (Session["Edit"] != null)
                    {
                        GMDEndDate.Visible = false;
                        GMDEndDate.Enabled = false;
                      //  trLeaveType.Visible = true;

                        if (Session["Reason"] != null)
                        {
                            txtReason.Text = Session["Reason"].ToString();
                        }

                        fnSetPaidORUnpaidOnEdit();

                        if (Session["AbsentDate"] != null)
                        {
                            GMDStartDate.Text = Convert.ToDateTime(Session["AbsentDate"].ToString()).ToString("dd/MM/yyyy");
                        }

                        if (Session["IsEHPL"] != null)
                        {
                            if (Session["IsEHPL"].ToString() == "1")
                                chkbHPL.Checked = true;
                            else
                                chkbHPL.Checked = false;
                        }
                    }
                    else
                    {
                        GMDEndDate.Visible = true;
                       // trLeaveType.Visible = true;
                        btnUpdateLeave.Visible = false;
                        GMDStartDate.Text = "";
                        BindLeavetype();

                    }
                }
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Add-a-Leave : BindData method");
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }
    }

    public void BindData4EditGrid()
    {
        string strSortbyOrder = "";
        try
        {

            if (ViewState["Sort_On"] != null)
            {
                objEmployee.Sort_On = ViewState["Sort_On"].ToString() + " " + ViewState["Sort_By"].ToString();
                strSortbyOrder = objEmployee.Sort_On;
            }

            if (ddlEmpList.SelectedValue != null && ddlYear.SelectedValue!=null)
            {
                if(ddlEmpList.SelectedValue!="--Select--"&& ddlYear.SelectedItem.Text!="")
                {
                objLeave.UserName = ddlEmpList.SelectedValue.ToString().Trim();

                objLeave.Year = Convert.ToInt32(ddlYear.SelectedItem.Text);

                ds = objLeave.getEmployeeAbsentDetails();

                objLeave.fnGetTotalAndUsedLeave(objLeave.UserName, objLeave.Year); // Set the value of UsedLeave and Alloted Leaves to a perticular user

                if (objLeave.UsedLeaves != null)
                    // lblTotalBalance.Text = objLeave.UsedLeaves.ToString() + " / " + objLeave.AllotedLeaves.ToString();
                    lblTotalBalance.Text = (Convert.ToDouble(objLeave.CasualBalanceLeaves) + Convert.ToDouble(objLeave.MedicalBalanceLeaves)).ToString() + " taken out of " + objLeave.AllotedLeaves.ToString() + "&nbsp;&nbsp&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp&nbsp;&nbsp Pending leaves : " + objLeave.BalanceLeaves;

                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        gvAbsentEdit.Visible = true;
                        System.Data.DataView dv = new DataView(ds.Tables[0]);

                        if (ddlEmpList.SelectedValue != null)
                        {
                            string strFilter = "UserName ='" + ddlEmpList.SelectedValue.ToString() + "'";
                            dv.RowFilter = strFilter;
                        }
                        if (dv.ToTable().Rows.Count > 0)
                        {
                            lblError.Visible = false;

                        }
                        else
                        {
                            lblError.Visible = true;
                            lblError.Text = "Records not found";

                        }
                        dv.Sort = strSortbyOrder;
                        ViewState["dv"] = dv.ToTable();

                        if (dv != null)
                        {
                            gvAbsentEdit.DataSource = dv;
                            gvAbsentEdit.DataBind();
                        }
                    }
                    else
                    {
                        gvAbsentEdit.Visible = false;

                    }

                }
                else
                {
                    gvAbsentEdit.Visible = false;
                }
            }
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Add-a-Leave : fnBindData4EditGrid method");
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {

    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        //Response.Redirect("EmployeeList.aspx?id=AL");
        fnCallCloseModelPopupScript();
        // BindData4EditGrid();
    }

    protected void btnAddLeave_Click(object sender, EventArgs e)
    {
        lock (obj)
        {
            try
            {
                int IsEHPL = 0;
                try
                {
                    int year = DateTime.Now.Year;
                    objLeave.UserName = ddlEmpList.SelectedValue.ToString().Trim();
                    //fnCheckBal();
                    TotalBalanceLeave = objLeave.GetTotalBalanceDaysInOne(year);
                    objLeave.LeaveTypeId = Convert.ToInt32(ddlLeaveType.SelectedValue);
                    Session["LeaveTypeId"] = ddlLeaveType.SelectedValue.ToString();
                    fnGetLeaveType();
                    fnSetInformedAndDeductable();
                }
                catch (Exception ex)
                {
                    DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Add-a-Leave : btnAddLeave_Click");
                    lblError.Text = ex.Message;
                    lblError.Visible = true;
                    return;
                }

                Random _r = new Random();
                objLeave.ApplicationNo = _r.Next();

                DateTime dt2;
                string strDate2 = "";

                List<DateTime> DTAddedStartAndEnd = new List<DateTime>();
                string strTemp = "";
                //region after using Ajax Calender Extender
                DateTime dt1 = Convert.ToDateTime(GMDStartDate.Text);
                string strDate1 = dt1.ToString("yyyy/MM/dd");
                if (GMDEndDate.Text.Trim() != "")
                {
                    dt2 = Convert.ToDateTime(GMDEndDate.Text);
                }
                else
                {
                    dt2 = dt1;
                }
                strDate2 = dt2.ToString("yyyy/MM/dd");

                lblStatusMsg.Text = "";
                int intNoOfRowAdded = 0;
                DTSDate = DateTime.Now;
                DateTime DTEDate = DateTime.Now;

                DateTime dtDayOfLeave = dt1;
                clsLeave_Logic objclsll = new clsLeave_Logic();

                objLeave.AbsentDate = dt2;

                objEmployee.UserName = ddlEmpList.SelectedValue.ToString().Trim();

                objLeave.UserName = ddlEmpList.SelectedValue.ToString().Trim();
                //  objEmployee.EmpId = Convert.ToInt32(Request["EId"]);
                bool blnIsExist = false;
                // DTSDate = GMDStartDate.Date;
                //DTEDate = GMDEndDate.Date;
                DTSDate = dt1;
                DTEDate = dt2;

                List<DateTime> dates = new List<DateTime>();

                if (DTEDate <= DTSDate)
                {
                    DTEDate = DTSDate;
                }

                for (DateTime dt = DTSDate; dt <= DTEDate; dt = dt.AddDays(1))
                {
                    dates.Add(dt);
                }

                foreach (DateTime dt in dates)
                {
                    string strReason = "";
                    string leaveDates = "";

                    try
                    {

                        // fnCheckBal();
                        blnIsExist = objclsll.CheckLeaveDateInAbsentDetails(dt, ddlEmpList.SelectedValue.ToString().Trim());
                        TotalBalanceLeave = Convert.ToDouble(objLeave.GetTotalBalanceDaysInOne(dt.Year));
                        strDay = dt.DayOfWeek.ToString();
                        strReason = txtReason.Text.Replace("'", "@");
                        if (Session["LeaveTypeId"] != null)
                        {
                            objLeave.LeaveTypeId = Convert.ToInt32(Session["LeaveTypeId"].ToString());
                        }
                        if (Session["LeaveTypeText"] != null)
                        {
                            strLeaveType = Session["LeaveTypeText"].ToString();
                        }
                        if (Session["IsPlanned"] != null)
                        {
                            if (Session["IsPlanned"].ToString() == "True")
                                blnIsPlanned = true;
                            else
                                blnIsPlanned = false;
                        }

                        if (Session["IsDOND"] != null)
                        {
                            if (Session["IsDOND"].ToString() == "True")
                                blnIsDOND = true;
                            else
                                blnIsDOND = false;
                        }

                        if (blnIsExist == false && strDay != "Sunday")
                        {
                            fnSetIsHPLORNot();
                            // setHPL();
                            fnSetPaidORUnpaid();

                            if (strDay == "Saturday" || objLeave.IsHpl == 1)
                            {
                                IsEHPL = 1;
                            }
                            else
                            {
                                IsEHPL = 0;
                            }

                            intNoOfRowAdded = objLeave.AddLeaveDetails(dt.Year, dt, objLeave.LeaveTypeId, IsEHPL);// this for tbl_LeaveDetail

                            if (intNoOfRowAdded >= 1)
                            {
                                intNoOfRowAdded = objclsll.AddLeaves(dt, objEmployee.UserName, strPONP, strReason, strLeaveType, IsEHPL, blnIsPlanned, blnIsDOND); //For tbl_AbsentDetails
                                DTAddedStartAndEnd.Add(dt);
                                TotalBalanceLeave = objLeave.GetTotalBalanceDaysInOne(dt.Year);
                                objLeave.TotalBalance = TotalBalanceLeave;
                                //lblStatusMsg.ForeColor = Color.Green;                            

                                BindData();
                                BindData4EditGrid();
                                blnHPL = false;
                               
                                if (intNoOfRowAdded >= 1)
                                {
                                    intCounter++;
                                }
                                else
                                {
                                    //lblStatusMsg.ForeColor = Color.Red;
                                    lblStatusMsg.Text = "An error happend during process.";
                                    blnIsBalanceAvailable = false;
                                    BindLeavetype();
                                }
                            }
                        }
                        else
                        {
                            //lblStatusMsg.ForeColor = Color.Red;

                            if (blnIsExist == true)
                                strTemp += @"<span style=""color:red"">Leave has been already added for date:</span> " + dt.ToShortDateString() + "<br/>";
                            else
                            {
                                if (strDay == "Sunday")
                                {
                                    strTemp += @"<span style=""color:red"">As its Sunday on</span> " + dt.ToString("dd/MM/yyyy") + @"<span style=""color:red""> so leave cannot be added for this day.</span><br/>";
                                }
                            }

                        }
                    }
                    catch
                    {
                        leaveDates += dt.ToString("dd/MM/yy");
                        lblStatusMsg.Text = "An error occured ! Sorry leave could not add to date :" + leaveDates;
                        return;
                    }

                }
                //Reset Informed and Deductable value
                Session["IsPlanned"] = null;
                Session["IsDOND"] = null;
                hdnPOUP.Value = "";
                hdnDOND.Value="";
                //

                if (intCounter > 0)
                {
                    if (intCounter == 1)
                    {

                        lblStatusMsg.Text = @"<span style=""color:green"">Leave has been added on the date(s): </span>" + DTAddedStartAndEnd[0].ToString("dd/MM/yyyy");
                        if (DTAddedStartAndEnd[0].ToString("dd/MM/yyyy") == DateTime.Now.ToString("dd/MM/yyyy"))
                            strLeaveInfo = "<strong >" + ddlEmpList.SelectedItem.Text.Trim() + "</strong> will not be coming to office today as he/she " + txtReason.Text.Trim();
                        else
                            strLeaveInfo = "<strong >" + ddlEmpList.SelectedItem.Text.Trim() + "</strong> will not be coming to office on " + DTAddedStartAndEnd[0].ToString("dd/MM/yyyy") + " as he/she " + txtReason.Text.Trim();
                        sendEmail();

                        //lblStatusMsg.Text += "<br/>" + strTemp;
                    }
                    else
                    {
                        //XYZ will be on leave from dd/mm/yy till dd/mm/yy as he/she is going out of station and will join back from {date}
                        lblStatusMsg.Text = @"<span style=""color:green"">Leave has been added on the date(s):</span>" + DTAddedStartAndEnd[0].ToString("dd/MM/yyyy");
                        lblStatusMsg.Text += @"<span style=""color:green""> to </span>" + DTAddedStartAndEnd[DTAddedStartAndEnd.Count - 1].ToString("dd/MM/yyyy");
                        strLeaveInfo = "<strong >" + ddlEmpList.SelectedItem.Text.Trim() + "</strong> will be on leave from "
                                        + DTAddedStartAndEnd[0].ToString("dd/MM/yyyy") + " to " + DTAddedStartAndEnd[DTAddedStartAndEnd.Count - 1].ToString("dd/MM/yyyy") + " as he/she " + txtReason.Text.Trim();
                        sendEmail();

                        // lblStatusMsg.Text += "<br/>" + strTemp;
                    }

                    BindLeavetype();
                }
                else
                {
                    if (strDay == "Sunday")
                    {
                        // lblStatusMsg.Text += strTemp;
                    }
                }
                lblStatusMsg.Text += "<br/>" + strTemp + "<br/>" + strEmailSucess;
                fnReset();
            }
            catch (Exception ex)
            {
                DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Add-a-Leave : btnAddLeave_Click Event");
                lblError.Text = ex.Message;
                lblError.Visible = true;
            }
            BindData4EditGrid();
        }
    }

    protected void ddlLeaveType_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblStatusMsg.Text = "";
    }

    public void fnSetInformedAndDeductable()
    {
        try
        {
            if (hdnPOUP.Value == "Yes")
            {
                Session["IsPlanned"] = "True";
            }
            else
            {
                Session["IsPlanned"] = "False";
            }

            if (hdnDOND.Value == "Yes")
            {
                Session["IsDOND"] = "True";
            }
            else
            {
                Session["IsDOND"] = "False";
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Add-a-Leave : fnSetInformedAndDeductable method");
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }
    }

    void BindLeavetype()
    {
        try
        {
            DataSet dsLeavetype = objLeave.GetLeaveType();
            objLeave.UserName = ddlEmpList.SelectedValue.ToString();
            // fnCheckBal();

            if (dsLeavetype != null)
            {
                if (dsLeavetype.Tables.Count > 0)
                {
                    dv = dsLeavetype.Tables[0].DefaultView;
                    ddlLeaveType.DataSource = dv;
                    ddlLeaveType.DataTextField = "LeaveTypeName";
                    ddlLeaveType.DataValueField = "LeaveTypeId";
                    ddlLeaveType.DataBind();
                    ddlLeaveType.Items.Insert(0, "Select");
                }
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Add-a-Leave : BindLeavetype method");
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }
    }

    protected void ddlLeaveTypePOrNP_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    public void fnGetLeaveType()
    {
        try
        {

            if (ddlLeaveType.SelectedValue != null)
            {
                if (Convert.ToInt32(ddlLeaveType.SelectedValue) == 1)
                    strLeaveType = "C";
                else if (Convert.ToInt32(ddlLeaveType.SelectedValue) == 2)
                    strLeaveType = "M";

                Session["LeaveTypeText"] = strLeaveType;
                Session["LeaveTypeId"] = ddlLeaveType.SelectedValue;
            }

            else
            {
                Session["LeaveTypeText"] = strLeaveType;
                Session["LeaveTypeId"] = ddlLeaveType.SelectedValue;
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Add-a-Leave : fnGetLeaveType method");
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }
        // 
    }

    #region send Email

    protected void sendEmail()
    {

        try
        {
            bool blnIsSent1 = false;
            bool blnIsSent2 = false;
            bool blnIsSent3 = false;
            bool blnIsSent4 = false;
            string emailid1 = "", emailid2 = "", emailid3 = "", emailid4 = "";

            emailid1 = System.Configuration.ConfigurationManager.AppSettings["LMS1"].ToString();
            // emailid2 = "rajeshs@esolutionsgroup.co.uk";
            emailid2 = System.Configuration.ConfigurationManager.AppSettings["LMS2"].ToString();
            emailid3 = System.Configuration.ConfigurationManager.AppSettings["LMS3"].ToString();
            emailid4 = System.Configuration.ConfigurationManager.AppSettings["LMS4"].ToString();

            string strBody = "";

            strBody = GenerateHTML();

            string strSubject = "Employee on " + strLeaveType + " Leave :" + Session["EmpName"].ToString();
            blnIsSent1 = DatabaseHelper.sendEmailLMS(emailid1, strSubject, strBody);
            blnIsSent2 = DatabaseHelper.sendEmailLMS(emailid2, strSubject, strBody);
            blnIsSent3 = DatabaseHelper.sendEmailLMS(emailid3, strSubject, strBody);
            //blnIsSent4 = DatabaseHelper.sendEmailLMS(emailid4, strSubject, strBody);
            strEmailSucess = "Email has been sent to ";
            if (blnIsSent1)
                strEmailSucess += "Juhi Shetty,  ";
            if (blnIsSent4)
                strEmailSucess += "Rituraj Kaur,  ";
            if (blnIsSent2)
                strEmailSucess += "Rohit Dixit,  ";
            if (blnIsSent3)
                strEmailSucess += " Michael Robinson";

        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Add-a-Leave : sendEmail method");
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }
    }

    private string GenerateHTML()
    {
        try
        {

            string lstrReason = "";
            string serverpath = ConfigurationManager.AppSettings["WebAddress"];
            DataTable dtTemp1, dtTemp2;
            DataSet dsTemp1, dsTemp2;
            //body of mail for the person
            objLeave.UserName = ddlEmpList.SelectedValue.ToString().Trim();
            DateTime d1 = DateTime.Now;

            string dtSDate = FirstDayOfYear(d1).ToString("yyyy-MM-dd");//GMDStartDate.DateString;
            string dtEDate = LastDayOfYear(d1).ToString("yyyy-MM-dd");
            string dtDOL = DTSDate.ToString("yyyy-MM-dd");

            //string strSql1 = "select convert(varchar,AbsentDate,103) as AbsentDate ,Reason,PaidOrNPaid as LeaveT, LeaveType from tbl_AbsentDetails where UserName='" + ddlEmpList.SelectedValue.ToString().Trim() + "' and AbsentDate <'" + dtDOL + "'"
            //                      + " order by cast(AbsentDate as DateTime) desc ";

            string strSql1 = "select convert(varchar,AbsentDate,103) as AbsentDate ,Reason,PaidOrNPaid as LeaveT,LeaveType from tbl_AbsentDetails where UserName='" + ddlEmpList.SelectedValue.ToString().Trim() + "' and AbsentDate >='" + dtSDate + "'"
                             + " and AbsentDate <='" + dtEDate + "' order by cast(AbsentDate as DateTime) desc ";

            dsTemp1 = objEmployee.GetAbsentDates(strSql1);

            DateTime d2 = DateTime.Now.AddYears(-1);

            dtSDate = FirstDayOfYear(d2).ToString("yyyy-MM-dd");//GMDStartDate.DateString;
            dtEDate = LastDayOfYear(d2).ToString("yyyy-MM-dd");

            string strSql2 = "select convert(varchar,AbsentDate,103) as AbsentDate ,Reason,PaidOrNPaid as LeaveT,LeaveType from tbl_AbsentDetails where UserName='" + ddlEmpList.SelectedValue.ToString().Trim() + "' and AbsentDate >='" + dtSDate + "'"
                                + " and AbsentDate <='" + dtEDate + "' order by cast(AbsentDate as DateTime) desc ";

            try
            {
                if (dsTemp1 != null)
                {
                    dtTemp1 = dsTemp1.Tables[0];

                    //body of mail for the person

                    DateTime dt = DateTime.Now;

                    strHeader += "Hi Michael ,";
                    strHeader += "<br/><br/><br/>";

                    // strHeader += strLeaveInfo + dtTemp1.Rows[0]["Reason"].ToString().Replace("@", "'").Trim() + ", Employee's past leave details are:";
                    strHeader += strLeaveInfo + ", Employee's past leave details are:";
                    strHeader += "<br/><br/>";


                    strHeader += @"<b><u><strong >" + ddlEmpList.SelectedItem.Text.Trim() + "'s</strong> leave details:</u></b>";

                    strHeader += "<br/><br/>";
                    strHeader += @"<table border=""1"">";

                    strHeader += @"<tr><td colspan=""3""><strong >Leave details for year :" + d1.Year.ToString() + "</strong></td></tr>";

                    if (dtTemp1.Rows.Count > 0)
                    {
                        strHeader += @"<tr><td width=""15%""><strong >Date</strong></td><td colspan=""2""><strong>Reason</strong></td></tr>";

                        for (int i = 0; i < dtTemp1.Rows.Count; i++) // logic to group  absent employee on same date.
                        {
                            if (dtTemp1.Rows[i]["Reason"].ToString().Trim() != "")
                                lstrReason = dtTemp1.Rows[i]["Reason"].ToString().Replace("@", "'").Trim();
                            strHeader += "<tr><td>" + dtTemp1.Rows[i]["AbsentDate"].ToString().Trim() + @"</td><td colspan=""2"">" + lstrReason + "</td></tr>";

                        }
                    }
                    else
                        strHeader += @"<tr><td colspan=""3""> No leave was taken before today in this year </td></tr>";

                }
            }
            catch (Exception ex)
            {
                DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, strSql1);
            }
            strHeader += @"<tr><td colspan=""3"" height=""10px""></td></tr>";

            strHeader += @"<tr><td colspan=""3""><strong >Leave details for year :" + d2.Year.ToString() + "</strong></td></tr>";
            try
            {

                dsTemp2 = objEmployee.GetAbsentDates(strSql2);

                if (dsTemp2 != null)
                {
                    dtTemp2 = dsTemp2.Tables[0];

                    if (dtTemp2.Rows.Count > 0)
                    {
                        strHeader += @"<tr><td width=""15%""><strong> Date</strong></td><td colspan=""2""><strong>Reason </strong></td></tr>";


                        for (int i = 0; i < dtTemp2.Rows.Count; i++) // logic to group  absent employee on same date.
                        {
                            if (dtTemp2.Rows[i]["Reason"].ToString().Trim() != "")
                                lstrReason = dtTemp2.Rows[i]["Reason"].ToString().Replace("@", "'").Trim();
                            strHeader += "<tr><td>" + dtTemp2.Rows[i]["AbsentDate"].ToString().Trim() + @"</td><td colspan=""2""> " + lstrReason + "</td></tr>";

                        }
                    }
                    else
                        strHeader += @"<tr><td colspan=""3"">No leave was taken in this year</td></tr>";
                }

            }
            catch (Exception ex)
            {
                DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, strSql2);
            }
            strHeader += @"<tr><td colspan=""3"" height=""10px""></td></tr>";

            strHeader += @"<tr><td colspan=""3""><strong >Past 10 days leave details</strong></td></tr>";
            try
            {
                DataTable dtTemp3 = fnGetPast10DaysLeaveDetails();
                DataView dv = new DataView(dtTemp3);
                dv.Sort = "AbsentDate desc";
                dtTemp3 = dv.ToTable();

                if (dtTemp3 != null)
                {
                    if (dtTemp3.Rows.Count > 0)
                    {
                        string strAbsentdate = "";
                        string strUsername = "";
                        strHeader += @"<tr><td width=""15%""><strong >Date</strong></td><td width=""20%""><strong>User name</strong></td><td width=""65%""><strong>Reason</strong></td></tr>";

                        for (int i = 0; i < dtTemp3.Rows.Count; i++) // logic to group  absent employee on same date.
                        {
                            lstrReason = "";
                            DateTime ldt = Convert.ToDateTime(dtTemp3.Rows[i]["AbsentDate"].ToString());
                            strAbsentdate = ldt.ToString("dd/MM/yyyy");
                            strUsername = dtTemp3.Rows[i]["UserName"].ToString().Replace("@", "'").Trim();
                            if (dtTemp3.Rows[i]["Reason"].ToString().Trim() != "")
                                lstrReason = dtTemp3.Rows[i]["Reason"].ToString().Replace("@", "'").Trim();
                            strHeader += "<tr><td>" + strAbsentdate.Trim() + @"<td> " + strUsername + "</td>" + @"</td><td>" + lstrReason + "</td></tr>";

                        }
                    }
                    else
                        strHeader += @"<tr><td colspan=""3"">No leave was taken in last 10 days</td></tr>";
                }
            }
            catch (Exception ex)
            {
                DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Add-a-leave : fnGetPast10DaysLeaveDetails()");
            }

            strHeader += "</table>";

            strHeader += "<br/><br/><br/><br/><br/><br/>";
            strHeader += "Regards<br/>";

            strHeader += "Juhi Shetty (HR)<br/>";
            strHeader += "eSolutions Group (ESG)<br/>";
            strHeader += "Tel: +91 2232505575<br/>";
            strHeader += "Mob: +91 8698009200<br/>";
            strHeader += "201 Rushabh Arcade . Old Station Road . Thane-West . 400 601<br/>";
            strHeader += "Email: juhis@esolutionsgroup.co.uk<br/>";
            strHeader += "Website: www.esolutionsgroup.co.uk<br/>";
            strHeader += "<br/><br/>";

            //    strHeader += "------------------";
            //    strHeader += "<br><br>";
            //    strHeader += ConfigurationManager.AppSettings["CompanyName"];
            //    strHeader += "<br>";
            //    //strHeader += ConfigurationManager.AppSettings["WebAddress"];
            //    strHeader += "<a href=" + ConfigurationManager.AppSettings["WebAddress"] + ">";
            //    strHeader += ConfigurationManager.AppSettings["WebAddressToShow"];
            //    strHeader += "</a>";
        }

        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Add-a-Leave : GenerateHTML method");
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }
        return strHeader;
    }

    #endregion send Email
    //region read all leave

    static DateTime LastDayOfYear()
    {
        return LastDayOfYear(DateTime.Today);
    }

    static DateTime LastDayOfYear(DateTime d)
    {
        // 1
        // Get first of next year
        DateTime n = new DateTime(d.Year + 1, 1, 1);
        // 2
        // Subtract 1 from it
        return n.AddDays(-1);
    }

    static DateTime FirstDayOfYear()
    {
        return FirstDayOfYear(DateTime.Today);
    }

    static DateTime FirstDayOfYear(DateTime y)
    {
        return new DateTime(y.Year, 1, 1);
    }

    public void fnCheckBal()
    {
        try
        {
            string strTotal = objLeave.fnGetTotalOfAllotedLeavesByEmp(Convert.ToInt32(ddlYear.SelectedItem.Text));//fnGetTotalOfAllotedLeavesByEmp() Return total of leaves alloted to employee.
            // returns remaining balance and set seperately each category's used leaves
            sdblTotal = Convert.ToDouble(strTotal);
            //ViewState["sdblTotal"] = sdblTotal;
            TotalBalanceLeave = objLeave.GetTotalBalanceDaysInOne(Convert.ToInt32(ddlYear.SelectedItem.Text));
            sdblUsedLeave = objLeave.CasualBalanceLeaves + objLeave.MedicalBalanceLeaves;
            sdblRBal = sdblTotal - sdblUsedLeave;
            // ViewState["sdblRBal"] = sdblRBal;
            if (sdblUsedLeave < sdblTotal)
                blnIsBalanceAvailable = true;
            else
                blnIsBalanceAvailable = false;
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Add-a-leave : fnCheckBal()");
        }
    }

    public DataTable fnGetPast10DaysLeaveDetails()
    {
        DataTable dtTemp3 = null;
        int intRowIndex = 0;
        bool blnIsFtime = true;
        bool blnIsMatchFirst = true;
        string temp = "";
        string temp1 = "";
        string temp2 = "";
        try
        {
            objEmployee.Sort_On = "";
            string strOrderBy = "";
            List<DateTime> dates = new List<DateTime>();

            if (ViewState["Sort_On"] != null)
            {
                objEmployee.Sort_On = ViewState["Sort_On"].ToString() + " " + ViewState["Sort_By"].ToString();
                strOrderBy = ViewState["Sort_On"].ToString() + " " + ViewState["Sort_By"].ToString(); ;
            }

            //lblError.Visible = false;

            DateTime dtStartingDate = DateTime.Today.AddDays(-10);
            DateTime dtEndingDate = DateTime.Today;

            for (DateTime dt = dtStartingDate; dt < dtEndingDate; dt = dt.AddDays(1))
            {
                dates.Add(dt);
            }

            DataSet dsTemp = objLeave.getEmployeeAbsentDetailsByDay(dtStartingDate, dtEndingDate);

            if (dsTemp != null)
            {
                if (dsTemp.Tables.Count > 0)
                {
                    DataTable dtTemp = dsTemp.Tables[0];
                    dtTemp.DefaultView.Sort = "AbsentDate Desc";
                    bool blnDATEEXIST = false;

                    List<string> strDateArray = new List<string>();
                    string strdt1 = "";
                    string strdt2 = "";
                    if (dtTemp.Rows.Count > 0)
                    {
                        for (int i = 0; i < dates.Count; i++)
                        {

                            for (int j = 0; j < dtTemp.Rows.Count; j++)
                            {
                                strdt1 = dates[i].ToString("yyyy/MM/dd 00:00:00.000");

                                DateTime ldt = Convert.ToDateTime(dtTemp.Rows[j]["AbsentDate"].ToString());

                                strdt2 = ldt.ToString("yyyy/MM/dd 00:00:00.000");

                                //strdt2 = ldt.ToString("dd/MM/yyyy");

                                if (strdt1.Trim() == strdt2.Trim())
                                {
                                    blnDATEEXIST = true;
                                }

                            }
                            if (blnDATEEXIST == false)
                                strDateArray.Add(strdt1);
                            blnDATEEXIST = false;
                        }

                        foreach (string ldt1 in strDateArray)
                        {
                            DataRow dr = dtTemp.NewRow(); //08/08/2012 

                            dr["AbsentDate"] = ldt1;
                            dr["UserName"] = "No one absent";
                            dtTemp.Rows.Add(dr);

                        }


                        List<string> temp3 = new List<string>();
                        for (intRowIndex = 0; intRowIndex < dtTemp.Rows.Count; intRowIndex++) // logic to group  absent employee on same date.
                        {

                            for (int j = 0; j < dtTemp.Rows.Count; j++)
                            {

                                //    j = 1;
                                if (dtTemp.Rows[intRowIndex]["AbsentDate"].ToString().Trim() == dtTemp.Rows[j]["AbsentDate"].ToString().Trim() && (intRowIndex != j))
                                {
                                    if (blnIsFtime == true)
                                    {
                                        temp3.Add(dtTemp.Rows[intRowIndex]["AbsentDate"].ToString().Trim());
                                    }
                                    if (!temp3.Contains(dtTemp.Rows[intRowIndex]["AbsentDate"].ToString().Trim()))
                                    {
                                        blnIsMatchFirst = true;
                                        temp3.Add(dtTemp.Rows[intRowIndex]["AbsentDate"].ToString().Trim());
                                    }
                                    else
                                    {
                                        if (blnIsFtime == true)
                                        {
                                            blnIsMatchFirst = true;
                                        }
                                        else
                                        {
                                            blnIsMatchFirst = false;
                                            temp3.Add(dtTemp.Rows[intRowIndex]["AbsentDate"].ToString().Trim());
                                        }

                                    }

                                    if (dtTemp.Rows[intRowIndex]["Reason"].ToString().Trim() == "")
                                        dtTemp.Rows[intRowIndex]["Reason"] = "Not Defined";
                                    if (dtTemp.Rows[j]["Reason"].ToString().Trim() == "")
                                        dtTemp.Rows[j]["Reason"] = "Not Defined";


                                    if (blnIsMatchFirst)
                                    {

                                        temp1 = "<b>" + dtTemp.Rows[intRowIndex]["UserName"].ToString().Trim() + " : " + "</b>" + dtTemp.Rows[intRowIndex]["Reason"].ToString().Trim();
                                        temp2 = "<b>" + dtTemp.Rows[j]["UserName"].ToString().Trim() + " : " + "</b>" + dtTemp.Rows[j]["Reason"].ToString().Trim();
                                        temp2 = temp1 + "  ,  " + temp2;
                                    }
                                    else
                                    {
                                        if (intRowIndex > j)
                                        {
                                            temp1 = "<b>" + dtTemp.Rows[intRowIndex]["UserName"].ToString().Trim() + " : " + "</b>" + dtTemp.Rows[intRowIndex]["Reason"].ToString().Trim();
                                            temp2 = dtTemp.Rows[j]["Reason"].ToString().Trim() + "  ,  " + temp1;
                                        }
                                        else
                                        {
                                            temp1 = "<b>" + dtTemp.Rows[j]["UserName"].ToString().Trim() + " : " + "</b>" + dtTemp.Rows[j]["Reason"].ToString().Trim();
                                            temp2 = dtTemp.Rows[intRowIndex]["Reason"].ToString().Trim() + "  ,  " + temp1;
                                        }

                                    }
                                    temp = dtTemp.Rows[intRowIndex]["UserName"].ToString().Trim() + "  ,  " + dtTemp.Rows[j]["UserName"].ToString().Trim();



                                    dtTemp.Rows[intRowIndex]["UserName"] = temp;
                                    dtTemp.Rows[intRowIndex]["Reason"] = temp2.Replace("@", "'");
                                    dtTemp.Rows[j].Delete();
                                    // dtTemp.Rows[j].AcceptChanges();
                                    j = j - 1;
                                    dtTemp.AcceptChanges();

                                    blnIsMatchFirst = false;
                                    blnIsFtime = false;
                                }


                            }
                        }

                        //Datatype conversion for AbsentDate Column in dtTemp

                        dtTemp3 = dtTemp.Clone();

                        foreach (DataColumn dc in dtTemp.Columns)
                        {
                            if (dc.ColumnName == "AbsentDate")
                            {
                                dc.DataType = typeof(DateTime);
                            }
                        }

                        foreach (DataRow dr in dtTemp.Rows)
                        {
                            DateTime ldt = Convert.ToDateTime(dr["AbsentDate"].ToString());
                            dr["AbsentDate"] = ldt.ToString("yyyy/MM/dd hh:mm:ss.fff");
                            dtTemp3.ImportRow(dr);
                        }

                        //End Datatype conversion for AbsentDate Column in dtTemp
                    }
                }

            }

        }

        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Add-a-leave : fnGetPast10DaysLeaveDetails");
        }
        //lblError.Text = ex.Message;
        // lblError.Visible = true;

        dtTemp3.DefaultView.Sort = "AbsentDate Desc";
        return dtTemp3;

    }

    protected void setHPL()
    {
        try
        {

            if (hdnIsHPL.Value == "Yes" || Session["IsEHPL"].ToString() == "1")
            {
                objLeave.IsHpl = 1;
            }
            else
            {
                objLeave.IsHpl = 0;
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Add-a-leave : setHPL Method");
        }
    }

    public void fnSetPaidORUnpaid()
    {
        try
        {
            if (Convert.ToInt32(ddlLeaveTypePOrNP.SelectedValue) == 1)
                strPONP = "Paid";
            else
                strPONP = "Non paid";
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Add-a-leave : fnSetPaidORUnpaid");
        }
    }

    public void fnSetPaidORUnpaidOnEdit()
    {
        try
        {
            if (Session["PaidOrNPaid"].ToString() == "Paid")
                ddlLeaveTypePOrNP.SelectedIndex = 1;
            else
                ddlLeaveTypePOrNP.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Add-a-leave : fnSetPaidORUnpaidOnEdit");
        }
    }

    public void fnSetIsHPLORNot()
    {
        try
        {
            if (chkbHPL.Checked)
            {
                objLeave.IsHpl = 1;

            }
            else
            {
                objLeave.IsHpl = 0;
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Add-a-leave : fnSetIsHPLORNot");
        }
    }

    #region Edit Leave


    protected void gvAbsentEdit_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvAbsentEdit.PageIndex = e.NewPageIndex;
            if (ViewState["dv"] != null)
            {
                System.Data.DataTable dt =(DataTable)ViewState["dv"];
                gvAbsentEdit.DataSource = dt;
                gvAbsentEdit.DataBind();
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Add-a-leave : gvAbsentEdit_PageIndexChanging event");
        }

    }

    protected void gvAbsentEdit_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            string strIsEHPL, strIsPOUP, strIsDOND;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DateTime dt = Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "AbsentDate"));
                string strcellText = dt.ToString("dd/MM/yyyy");
                e.Row.Cells[0].Text = strcellText;
                e.Row.Cells[1].Text = DataBinder.Eval(e.Row.DataItem, "Reason").ToString().Replace("@", "'");

                strIsEHPL = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "IsEHPL"));
                strIsPOUP = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "IsPlanned"));
                strIsDOND = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Deductable"));

                if (strIsEHPL == "1")
                {
                    e.Row.Cells[4].Text = "Yes";
                }
                else
                {
                    e.Row.Cells[4].Text = "No";
                }

                if (strIsDOND == "True")
                {
                    e.Row.Cells[6].Text = "Yes";
                }
                else
                {
                    e.Row.Cells[6].Text = "No";
                }


                if (strIsPOUP == "True")
                {
                    e.Row.Cells[5].Text = "Yes";
                }
                else
                {
                    e.Row.Cells[5].Text = "No";
                }
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Add-a-leave : gvAbsentEdit_RowDataBound event");
        }

    }

    protected void gvAbsentEdit_DataBound(object sender, EventArgs e)
    {
        // this.gvAbsentEdit.Columns[1].Visible = false;
    }

    protected void gvAbsentEdit_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            objEmployee.Sort_On = e.SortExpression;
            ViewState["Sort_On"] = objEmployee.Sort_On;
            if (ViewState["Sort_By"] == null)
                ViewState["Sort_By"] = "Asc";
            if (ViewState["Sort_By"].ToString() == "Asc")
            {
                ViewState["Sort_By"] = "Desc";
            }
            else
            {
                ViewState["Sort_By"] = "Asc";
            }

            BindData4EditGrid();

        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Add-a-leave : gvAbsentEdit_Sorting event");
        }
    }

    public void SetPaidUnpaidDDL()
    {
        try
        {
            dv = (DataView)ViewState["dv"] as DataView;

            if (dv != null)
            {
                if (dv.Count > 0)
                {
                    if (dv[0]["PaidOrNPaid"].ToString().Trim() != "" && dv[0]["PaidOrNPaid"] != null)
                    {
                        if (dv[0]["PaidOrNPaid"].ToString() == "Paid")
                        {
                            ddlLeaveTypePOrNP.SelectedIndex = 1;
                        }
                        else
                        {
                            ddlLeaveTypePOrNP.SelectedIndex = 0;
                        }
                    }
                }
            }
        }

        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Add-a-leave : SetPaidUnpaidDDL Method");
        }
    }

    protected void gvAbsentEdit_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName.ToUpper() == "UPD")
            {
                try
                {
                    string sortByAndArrangeBy = (e.CommandArgument).ToString();
                    lblStatusMsg.Text = "";
                    char[] separator = { '|' };
                    string[] sortByAndArrangeByArray = sortByAndArrangeBy.Split(separator);
                    Session["UserName"] = ddlEmpList.SelectedValue.ToString().Trim();
                    string[] DateSpan1 = sortByAndArrangeByArray[1].Split(new string[] { "/" }, StringSplitOptions.None);
                    //string strNewDate =DateSpan1[2]+"/"+DateSpan1[1]+"/"+DateSpan1[0];
                    dtDayOfLeave1 = Convert.ToDateTime(sortByAndArrangeByArray[1]);
                    Session["AbsentDate"] = sortByAndArrangeByArray[1];
                    Session["LeaveType"] = sortByAndArrangeByArray[2];
                    Session["PaidOrNPaid"] = sortByAndArrangeByArray[3];
                    Session["Reason"] = sortByAndArrangeByArray[4];
                    Session["IsPlanned"] = sortByAndArrangeByArray[5];
                    Session["IsDOND"] = sortByAndArrangeByArray[6];
                    Session["IsEHPL"] = sortByAndArrangeByArray[7];

                    if (Session["IsPlanned"].ToString() == "False" || Session["IsPlanned"].ToString().Trim() == "")
                    {
                        hdnPOUP.Value = "No";
                        chkPOUP.Checked = false;
                    }
                    else
                    {
                        hdnPOUP.Value = "Yes";
                        chkPOUP.Checked = true;
                    }

                    if (Session["IsDOND"].ToString() == "False" || Session["IsPlanned"].ToString().Trim() == "")
                    {
                        hdnDOND.Value = "No";
                        chkDOND.Checked = false;
                    }
                    else
                    {
                        hdnDOND.Value="Yes";
                        chkDOND.Checked = true;
                    }

                    Session["Edit"] = "Edit";

                    if (Session["IsEHPL"].ToString() == "1")
                        chkbHPL.Checked = true;
                    else
                        chkbHPL.Checked = false;

                    BindData();
                    BindData4EditGrid();
                    UpdateDDLLeaveType();

                    btnUpdateLeave.Visible = true;
                    btnAddLeave.Visible = false;
                    //trLeaveType.Visible = true;
                    GMDStartDate.Enabled = false;

                }
                catch (Exception ex)
                {
                    DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Add-a-leave : gvAbsentEdit_RowCommand event Update command");
                }
            }


            else if (e.CommandName.ToUpper() == "DEL")
            {
                try
                {
                    string sortByAndArrangeBy1 = (e.CommandArgument).ToString();

                    char[] separator = { '|' };
                    string[] sortByAndArrangeByArray1 = sortByAndArrangeBy1.Split(separator);
                    Session["UserName"] = ddlEmpList.SelectedValue.ToString().Trim();
                    Session["AbsentDate"] = sortByAndArrangeByArray1[1];
                    Session["LeaveType"] = sortByAndArrangeByArray1[2];
                    Session["PaidOrNPaid"] = sortByAndArrangeByArray1[3];
                    Session["IsEHPL"] = sortByAndArrangeByArray1[4];

                    UpdateDDLLeaveType();

                    objLeave.UserName = ddlEmpList.SelectedValue.ToString().Trim();
                    string strDT = sortByAndArrangeByArray1[1].ToString();
                    dtDayOfLeave1 = Convert.ToDateTime(sortByAndArrangeByArray1[1]);
                    int intYearOfLeave = dtDayOfLeave1.Year;
                    string[] DateSpan = sortByAndArrangeByArray1[1].Split(new string[] { "/" }, StringSplitOptions.None);
                    objLeave.LeaveTypeId = ddlLeaveType.SelectedIndex;
                    int intNoOfRowDeleted = objLeave.DeleteLeaves(dtDayOfLeave1, objLeave.UserName, objLeave.LeaveTypeId, Convert.ToInt32(sortByAndArrangeByArray1[4]));

                    if (intNoOfRowDeleted > 0)
                    {
                        fnReset();
                        objLeave.GetBalanceDays(intYearOfLeave);// Here we update leave balance taken by employee respect to category wise
                        gvAbsentEdit.DataBind();
                        BindData4EditGrid();
                        BindLeavetype();
                        // Page.ClientScript.RegisterStartupScript(this.GetType(),"Edit Window", "OpenModelPopup();", true);
                    }
                    else
                    {
                        lblStatusMsg.Text = "Operation was not successful please try again.";
                    }

                }
                catch (Exception ex)
                {
                    DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS_Add-a-leave : gvAbsentEdit_RowCommand event Delete Command ");
                }
            }
        }

        catch (Exception ex)
        {
            lblStatusMsg.Text = ex.Message;
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Add-a-leave : gvAbsentEdit_RowCommand event");
        }

    }

    protected void gvAbsentEdit_RowEditing(object sender, GridViewEditEventArgs e)
    {

    }

    protected void btnUpdateLeave_Click(object sender, EventArgs e)
    {
        try
        {
            Session["Edit"] = "Edit";
            lblStatusMsg.Text = "";
            int intNoOfRowEditted = 0, i = 0;
            DateTime DTSDate = DateTime.Now, DTEDate = DateTime.Now;

            objEmployee.UserName = ddlEmpList.SelectedValue.ToString().Trim();
            objLeave.UserName = ddlEmpList.SelectedValue.ToString().Trim();
            strLeaveType = ddlLeaveType.SelectedItem.Text;
            strPONP = ddlLeaveTypePOrNP.SelectedItem.Text;

            fnSetInformedAndDeductable();
            objLeave.IsHpl = 0;
            setHPL();

            if (Session["IsPlanned"].ToString() == "False")
                blnIsPlanned = false;
            else
                blnIsPlanned = true;

            if (Session["IsDOND"].ToString() == "False")
                blnIsDOND = false;
            else
                blnIsDOND = true;


            string strReason = "";

            if (txtReason.Text.Trim() != "")
                strReason = txtReason.Text.Replace("'", "@");
            if (Session["AbsentDate"] != null)
            {
                dtDayOfLeave1 = Convert.ToDateTime(Session["AbsentDate"]);
                intNoOfRowEditted = objLeave.UpdateLeaves(dtDayOfLeave1, objEmployee.UserName, strPONP, strReason, strLeaveType, objLeave.IsHpl, blnIsPlanned, blnIsDOND);

                if (intNoOfRowEditted >= 1)
                {
                    objLeave.UserName = ddlEmpList.SelectedItem.Text; // txtUserName.Text;

                    if (ddlLeaveType.SelectedItem.Text != "Select")
                        objLeave.LeaveTypeId = Convert.ToInt32(ddlLeaveType.SelectedValue);
                    gvAbsentEdit.DataBind();
                    BindData4EditGrid();

                    Session["PaidOrNPaid"] = ddlLeaveTypePOrNP.SelectedItem.Text;
                    BindData();

                    objLeave.IsHpl = 0;
                   // chkbHPL.Checked = false;
                    Session["IsEHPL"] = "0";

                    lblStatusMsg.Text = "Record has been updated.";

                    lblError.Text = "Record has been updated.";
                    i++;
                }
                else
                {
                    lblStatusMsg.Text = "There is a Error in reporting.";
                    lblError.Text = "There is a Error in reporting.";
                }
            }
            else
            {
                lblStatusMsg.Text = "There is a Error in reporting.";
                lblError.Text = "There is a Error in reporting.";
            }

        }

        catch (Exception ex)
        {           
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Add-a-leave : btnUpdate_Click event");
            lblStatusMsg.Text = "There is a Error:" + ex;

            objLeave.IsHpl = 0;
            chkbHPL.Checked = false;
            Session["IsEHPL"] = "0";
        }

    }

    protected void gvAbsentEdit_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            int categoryID = (int)gvAbsentEdit.DataKeys[e.RowIndex].Value;
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Add-a-leave : gvAbsentEdit_RowDeleting event");
            lblStatusMsg.Text = "There is a Error:" + ex;
        }
    }

    protected void gvAbsentEdit_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }

    public void UpdateDDLLeaveType()
    {
        // BindLeaveType4Edit();
        try
        {
            if (Session["LeaveType"].ToString() == "C" || Session["LeaveType"].ToString() == "CL" || Session["LeaveType"].ToString() == "EL" || Session["LeaveType"].ToString() == "Extra Leave" || Session["LeaveType"].ToString() == "HPL")
            {
                ddlLeaveType.SelectedIndex = 1;
            }
            else if (Session["LeaveType"].ToString() == "M" || Session["LeaveType"].ToString() == "ML")
            {
                ddlLeaveType.SelectedIndex = 2;
            }
            if (Session["IsEHPL"].ToString() == "1")
            {
                objLeave.IsHpl = 1;
            }
            else
            {
                objLeave.IsHpl = 0;
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject,  "LMS-Add-a-leave  : UpdateDDLLeaveType Method");

            lblStatusMsg.Text = "There is a Error:" + ex;
        }

    }

    public void UpdateDDLPOrNP()
    {
        try
        {
            if (Session["PaidOrNPaid"].ToString() == "Paid")
                ddlLeaveTypePOrNP.SelectedIndex = 1;
            else
                ddlLeaveTypePOrNP.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Add-a-leave  : UpdateDDLPOrNP Method");

            lblStatusMsg.Text = "There is a Error:" + ex;
        }
    }

    protected void rdbtnEdit_CheckedChanged(object sender, EventArgs e)
    {
        Session["Edit"] = "True";
        fnCallOpenModelPopupScript();
    }

    public void fnCallCloseModelPopupScript()
    {
        string script = "<SCRIPT LANGUAGE='JavaScript'> ";
        script += "Submit();";
        script += "</SCRIPT>";
        Page.RegisterClientScriptBlock("ClientScript", script);
    }

    public void fnCallOpenModelPopupScript()
    {
        string script = "<SCRIPT LANGUAGE='JavaScript'>";
        script += "OpenModelPopup();";
        script += "</SCRIPT>";
        Page.RegisterClientScriptBlock("ClientScript", script);
    }

    #endregion Edit Leave

    protected void rdbtnAddNew_CheckedChanged(object sender, EventArgs e)
    {
        Session["Edit"] = null;
        BindData();
    }

    protected void LnkbtnNewLeave_Click(object sender, EventArgs e)
    {
        try
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "selectAndFocus", "$get('" + GMDStartDate.ClientID + "').focus();$get('" + GMDStartDate.ClientID + "').select();", true);

            Session["Edit"] = null;
            GMDEndDate.Visible = true;
            GMDEndDate.Enabled = true;
            GMDStartDate.Text = "";
            GMDEndDate.Text = "";
            GMDStartDate.Enabled = true;
           // lblStatusMsg.Text = "";
            // trLeaveType4Edit.Visible = false;
           // trLeaveType.Visible = true;
            btnAddLeave.Visible = true;
            btnUpdateLeave.Visible = false;
            txtReason.Text = "";
            chkDOND.Checked = false;
            chkPOUP.Checked = false;
            BindData();

            BindData4EditGrid();
            BindLeavetype();
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Add-a-leave  : LnkbtnNewLeave_Click Event ");

            lblStatusMsg.Text = "There is a Error:" + ex;
        }
    }

    protected void chkbxIsPlanned_CheckedChanged(object sender, EventArgs e)
    {
        //if (chkbxIsPlanned.Checked == true)
        //{
        //    blnIsPlanned = true;
        //    Session["IsPlanned"] = "True";
        //}
        //else
        //{
        //    Session["IsPlanned"] = "False";
        //    blnIsPlanned = false;
        //}
        BindData4EditGrid();
    }

    protected void ddlEmpList_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlEmpList.SelectedItem.Text != "--Select--")
            {
                lblTotalTitle.Visible = true;
                lblStatusMsg.Text = "";
                fnReset();
                Session["EmpName"] = ddlEmpList.SelectedItem.Text;
                Session["IsPlanned"] = null;
                Session["IsDOND"] = null;
                btnAddLeave.Visible = true;
                btnUpdateLeave.Visible = false;
                BindData();
                BindData4EditGrid();
            }
            else
            {
                gvAbsentEdit.Visible = false;
                lblTotalTitle.Visible = false;
                lblTotalBalance.Text = "";
               // txtUserName.Text = "";
                fnReset();
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Add-a-leave  : ddlEmpList_SelectedIndexChanged Event");

            lblStatusMsg.Text = "There is a Error:" + ex;
        }
    }

    public void fnBindEmpddl()
    {
        try
        {
            DataSet ds = objEmployee.GetEmployee(); //DatabaseHelper.getDataset("Select (firstname+' '+lastname) as EmpName , username from users where IsCW='Y' order by EmpName Asc");
                if(ds!=null)
                {
                    if(ds.Tables.Count>0)
                    {

            ddlEmpList.DataSource = ds;
            ddlEmpList.DataValueField = "username";
            ddlEmpList.DataTextField = "EmpName";
            ddlEmpList.DataBind();
            ddlEmpList.Items.Insert(0, "--Select--");
                    }
                }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Add-a-leave  : fnBindEmpddl Method");

            lblStatusMsg.Text = "There is a Error:" + ex;
        }

    }

    public void BindddlYear()
    {
        try
        {
            int year;
            year = DateTime.Now.Year;
            int YearAtMid = year - 6;
            int j = 1;
            for (int i = 0; i <= 12; i++)
            {
                if (i <= 6)
                {
                    ddlYear.Items.Add(Convert.ToString(YearAtMid + i));

                }
                else
                {
                    ddlYear.Items.Add(Convert.ToString(year + j));
                    j++;
                }
                ddlYear.SelectedIndex = 6;
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Add-a-leave : BindddlYear Method");
        }
    }

    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {           
            lblStatusMsg.Text = "";
            BindData4EditGrid();
            BindLeavetype();
            fnReset();
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Add-a-leave : ddlYear_SelectedIndexChanged Event");
        }
    }

    public void fnReset()
    {
        try
        {
            Session["Edit"] = null;
            chkDOND.Checked = false;
            chkPOUP.Checked = false;
            chkbHPL.Checked = false;
            txtReason.Text = "";
            GMDEndDate.Text = "";
            GMDStartDate.Text = "";
            GMDStartDate.Visible = true;
            GMDEndDate.Visible = true;
            GMDStartDate.Enabled = true;
            GMDEndDate.Enabled = true;
            GMDEndDate.Visible = true;
            ddlLeaveType.Enabled = true;
            ddlLeaveType.SelectedIndex = -1;
            ddlLeaveTypePOrNP.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Add-a-leave : fnReset Method");
        }

    }
}
