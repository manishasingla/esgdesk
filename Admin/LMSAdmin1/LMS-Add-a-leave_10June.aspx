<%@ Page Language="C#" MasterPageFile="~/Admin/LMSAdmin/AdminMasterMenu.master" AutoEventWireup="true"  
CodeFile="LMS-Add-a-leave_10June.aspx.cs" Inherits="Admin_frmEmpLeaveDetails" Title=" Add leave for employee" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="GMDatePicker" Namespace="GrayMatterSoft" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">


        function OpenModelPopup() {
            //  document.getElementById('tdDisplayName').innerHTML = '';
            //  document.getElementById('txtName').value = '';
            //  alert("Called");
            document.getElementById('ModalPopupDiv').style.visibility = 'visible';
            document.getElementById('ModalPopupDiv').style.display = '';
            document.getElementById('ModalPopupDiv').style.top = Math.round((document.documentElement.clientHeight / 2) + document.documentElement.scrollTop) - 100 + 'px';
            document.getElementById('ModalPopupDiv').style.left = '400px';

            document.getElementById('MaskedDiv').style.display = '';
            document.getElementById('MaskedDiv').style.visibility = 'visible';
            document.getElementById('MaskedDiv').style.top = '0px';
            document.getElementById('MaskedDiv').style.left = '0px';
            document.getElementById('MaskedDiv').style.width = '100%'; // document.documentElement.clientWidth + 'px';
            document.getElementById('MaskedDiv').style.height = '1500px'; //document.documentElement.clientHeight + 'px';
        }


        function CloseModelPopup() {
            document.getElementById('MaskedDiv').style.display = 'none';
            document.getElementById('ModalPopupDiv').style.display = 'none';
        }

        function Submit() {
            //    if (document.getElementById('txtName').value != '') {
            document.getElementById('MaskedDiv').style.display = 'none';
            document.getElementById('ModalPopupDiv').style.display = 'none';
            // document.getElementById('tdDisplayName').innerHTML = '<H1> Hi ' + document.getElementById('txtName').value + ' !</H1>';
            //}
            //  else 
            // {
            //     alert('Please enter your name');
            //  }
        }
        function test() {
            alert('Called');

        }

        function fnSetPOUP(obj) {
            debugger;
            if (obj.checked) {
                document.getElementById("<%=hdnPOUP.ClientID%>").value = "Yes";
            }
            else {
                document.getElementById("<%=hdnPOUP.ClientID%>").value = "No";
            }

        }

        function fnSetDOND(obj) {
            if (obj.checked) {
                document.getElementById("<%=hdnDOND.ClientID%>").value = "Yes";
            }
            else {
                document.getElementById("<%=hdnDOND.ClientID%>").value = "No";
            }
        }



        function fnSetHPL(obj) {
            if (obj.checked) {
                document.getElementById("<%=hdnIsHPL.ClientID%>").value = "Yes";
            }
            else {
                document.getElementById("<%=hdnIsHPL.ClientID%>").value = "No";
            }
        }

    </script>
    <div class="publicloginTable" style="height: 81%; background-color: #C2CFDC;">
        <div>
            <span>
                <asp:LinkButton ID="LnkbtnNewLeave" runat="server" Style="color: Blue; font-weight: bold"
                    OnClick="LnkbtnNewLeave_Click">Add New Leave</asp:LinkButton></span>&nbsp;<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        Employee name</span> <span style="width: 2%;" align="center">:</span>
            <span style="width: 18%; vertical-align: bottom">
                <asp:DropDownList ID="ddlEmpList" runat="server" OnSelectedIndexChanged="ddlEmpList_SelectedIndexChanged"
                    AutoPostBack="True" ValidationGroup="g1">
                </asp:DropDownList>
            </span><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                Year</span> <span align="center" style="width: 2%">:</span> <span style="width: 18%;
                    vertical-align: bottom">
                    <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged">
                    </asp:DropDownList>
                </span><span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" InitialValue="--Select--"
                        ErrorMessage="Please select an employee first." ControlToValidate="ddlEmpList"
                        ValidationGroup="g1">
                    </asp:RequiredFieldValidator></span>
        </div>
        <%--  <div style="float: left; height: 100%; width: 31%" class="publicloginTable" id="divAdd"
            runat="server">
            <table style="height: 100%; width: 89%;">
                <tr>
                    <td colspan="6" class="LoginTitle">
                        <h1 style="color: Black">
                            Add/Edit leave for employee</h1>
                    </td>
                </tr>
                <tr>
                    <td valign="top" align="left">
                        <table class="admintablestyle">
                            <tr>
                                <td colspan="3" class="LoginTitle">
                                    Employee Balance Leave Details
                                </td>
                            </tr>


                         
                            <tr>
                                <td colspan="4">
                                    <asp:Label ID="lblStatusMsg" runat="server" Font-Bold="True"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>--%>
        <div>
        </div>
        <div style="float: left; width: 100%; height: 100%" class="publicloginTable">
            <br />
            <table width="100%">
                <tr>
                    <td rowspan="14" align="left" valign="top">
                        <table style="width: 100%">
                            <tr>
                                <td colspan="6" class="LoginTitle">
                                    <h1 style="color: Black">
                                    </h1>
                                </td>
                                <%-- <td>
                    <a href="javascript:CloseModelPopup();" rel="country2" id="A3">X</a>
                </td>--%>
                            </tr>
                            <tr>
                                <td Colspan="9">
                                    <span style="color: Black">
                                        <asp:Label runat="server" ID="lblTotalTitle" Text="Totals : " Visible="false"></asp:Label><asp:Label
                                            ID="lblTotalBalance" runat="server" Text=""></asp:Label></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="7">
                                </td>
                            </tr>
                            <tr>
                                <td rowspan="0" valign="top">
                                    <asp:GridView ID="gvAbsentEdit" runat="server" PageSize="20" Style="position: relative;
                                        top: 0px; left: 0px; width: 200%;" OnPageIndexChanging="gvAbsentEdit_PageIndexChanging"
                                        OnRowDataBound="gvAbsentEdit_RowDataBound" OnDataBound="gvAbsentEdit_DataBound"
                                        AutoGenerateColumns="False" AllowSorting="True" OnSorting="gvAbsentEdit_Sorting"
                                        CaptionAlign="Top" OnRowCommand="gvAbsentEdit_RowCommand" OnRowEditing="gvAbsentEdit_RowEditing"
                                        OnRowDeleting="gvAbsentEdit_RowDeleting" OnRowUpdating="gvAbsentEdit_RowUpdating">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Absent date" SortExpression="AbsentDate">
                                                <HeaderStyle Width="20%" />
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkBtnUserName" Text='<%#Eval("AbsentDate")%>' runat="server"
                                                        CommandArgument='<%#string.Format("{0}|{1}|{2}|{3}",Eval("UserName"),Eval("AbsentDate"),Eval("LeaveType"),Eval("PaidOrNPaid"))%>'
                                                        CommandName="Edt"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Reason" SortExpression="Reason">
                                                <HeaderStyle Width="50%" />
                                                <ItemTemplate>
                                                    <%#Eval("Reason")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Paid Or Unpaid" SortExpression="PaidOrNPaid">
                                                <HeaderStyle Width="20%" />
                                                <ItemTemplate>
                                                    <%#Eval("PaidOrNPaid")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Leave type" SortExpression="LeaveType">
                                                <HeaderStyle Width="20%" />
                                                <ItemTemplate>
                                                    <%#Eval("LeaveType")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Is HPL?" SortExpression="IsEHPL">
                                                <HeaderStyle Width="20%" />
                                                <ItemTemplate>
                                                    <%#Eval("IsEHPL")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Informed?" SortExpression="">
                                                <HeaderStyle Width="20%" />
                                                <ItemTemplate>
                                                    <%#Eval("IsPlanned")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Deductable?" SortExpression="">
                                                <HeaderStyle Width="20%" />
                                                <ItemTemplate>
                                                    <%#Eval("Deductable")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Delete" SortExpression="AbsentDate">
                                                <HeaderStyle Width="20%" />
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkBtnDelete" Text="Delete" runat="server" CommandArgument='<%#string.Format("{0}|{1}|{2}|{3}|{4}",Eval("UserName"),Eval("AbsentDate"),Eval("LeaveType"),Eval("PaidOrNPaid"),Eval("IsEHPL"))%>'
                                                        CommandName="Del"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Edit" SortExpression="AbsentDate">
                                                <HeaderStyle Width="10%" />
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkBtnUpdate" Text="Edit" runat="server" CommandArgument='<%#string.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}",Eval("UserName"),Eval("AbsentDate"),Eval("LeaveType"),Eval("PaidOrNPaid"),Eval("Reason"),Eval("IsPlanned"),Eval("Deductable"),Eval("IsEHPL"))%>'
                                                        CommandName="Upd"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle Height="20px" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <RowStyle Height="30px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table style="width: 187%">
                                        <tr>
                                            <td valign="top" colspan="2" class="style10">
                                            Start date:<br />
                                                <asp:TextBox ID="GMDStartDate" runat="server"></asp:TextBox>
                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="GMDStartDate"
                                                    Format="dd/MM/yyyy" PopupButtonID="GMDStartDate">
                                                </ajaxToolkit:CalendarExtender>
                                                <br />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please select a start date"
                                                    ControlToValidate="GMDStartDate" ValidationGroup="g1"></asp:RequiredFieldValidator><br />
                                                   End date :<br />
                                                <asp:TextBox ID="GMDEndDate" runat="server" Visible="false"></asp:TextBox>
                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="GMDEndDate"
                                                    Format="dd/MM/yyyy" PopupButtonID="GMDEndDate">
                                                </ajaxToolkit:CalendarExtender>
                                                <br />
                                                <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="End date should be greater or equal to start date."
                                                    ControlToCompare="GMDStartDate" ControlToValidate="GMDEndDate" Operator="GreaterThanEqual"
                                                    Type="Date" ValidationGroup="g1"></asp:CompareValidator>
                                            </td>
                                            <td class="style2">
                                               <asp:TextBox ID="txtReason" runat="server" TextMode="MultiLine" Width="210px"></asp:TextBox>
                                             
                                            </td>
                                            <td class="style7">
                                            <asp:DropDownList ID="ddlLeaveTypePOrNP" runat="server" OnSelectedIndexChanged="ddlLeaveTypePOrNP_SelectedIndexChanged">
                                                    <asp:ListItem Value="0">Non paid</asp:ListItem>
                                                    <asp:ListItem Value="1">Paid</asp:ListItem>
                                                </asp:DropDownList>
                                                <br />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please select a type"
                                                    InitialValue="Select" ControlToValidate="ddlLeaveTypePOrNP" ValidationGroup="g1"></asp:RequiredFieldValidator>
                                             
                                            </td>
                                            <td valign="middle" class="style6">  <asp:DropDownList ID="ddlLeaveType" runat="server" OnSelectedIndexChanged="ddlLeaveType_SelectedIndexChanged">
                                                    <asp:ListItem>Select</asp:ListItem>
                                                </asp:DropDownList><br />
                                              
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please select a leave type"
                                                    InitialValue="Select" ControlToValidate="ddlLeaveType" ValidationGroup="g1"></asp:RequiredFieldValidator>
                                            </td>
                                            <td colspan="2" class="style8">
                                                <input id="chkbHPL" type="checkbox" visible="True" onclick="fnSetHPL(this);" runat="server" />  
                                            </td>
                                            <td colspan="2" class="style8" >
                                                <input id="chkPOUP" type="checkbox" onclick="fnSetPOUP(this);" runat="server" />
                                            </td>
                                            <td colspan="2" width="20%">
                                                <input id="chkDOND" type="checkbox" onclick="fnSetDOND(this);" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Button ID="btnAddLeave" runat="server" Text="Add Leave" OnClick="btnAddLeave_Click"
                                        ValidationGroup="g1" />
                                    <asp:Button ID="btnUpdateLeave" runat="server" Text="Update Leave" OnClick="btnUpdateLeave_Click"
                                        Visible="false" ValidationGroup="g1" />
                                </td>
                                <td style="width: 161px" colspan="2">
                                    &nbsp;<asp:Button ID="btnBack" runat="server" Text="Back" OnClick="btnBack_Click"
                                        Visible="false" CausesValidation="False" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span>
                                        <asp:Label ID="lblError" runat="server" Visible="false" CssClass="lblerror"></asp:Label>
                                        <asp:Label ID="lblStatusMsg" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;
 
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <div>
                <asp:Label ID="Lbl_Pageinfo" runat="server" Text=""></asp:Label></div>
        </div>
    </div>
    <div id="MaskedDiv" class="MaskedDiv">
    </div>
    <div id="ModalPopupDiv" class="ModalPopup" style="top: 299px; left: 42%;">
    </div>
    <style type="text/css">
        div.MaskedDiv
        {
            visibility: hidden;
            position: absolute;
            left: 0px;
            top: 0px;
            font-family: verdana;
            font-weight: bold;
            padding: 40px;
            z-index: 100; /* background-image:url(Mask.png); */ /* ieWin only stuff */ /* _background-image:none; */
            background-color: #333333;
            opacity: 0.7;
            filter: alpha(opacity=70); /*_filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled=true, sizingMethod=scale src='Mask.png'); */
        }
        
        
        div.ModalPopup
        {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 11px;
            font-style: normal;
            background-color: #CCCCCC;
            position: absolute; /* set z-index higher than possible */
            z-index: 10000;
            visibility: hidden;
            color: Black;
            border-style: solid;
            border-color: #999999;
            border-width: 1px;
            width: 1000px;
            height: auto;
            left: 42%;
        }
        .style2
        {
            width: 39%;
        }
        .style6
        {
            width: 9%;
        }
        .style7
        {
            width: 17%;
        }
        .style8
        {
            width: 7%;
        }
        .style10
        {
            width: 16%;
        }
    </style>
    <asp:HiddenField ID="hdnPOUP" runat="server" />
    <asp:HiddenField ID="hdnDOND" runat="server" />
    <asp:HiddenField ID="hdnIsHPL" runat="server" />
</asp:Content>
