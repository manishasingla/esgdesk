<%@ Page Language="C#" MasterPageFile="~/Admin/LMSAdmin/AdminMasterMenu.master" AutoEventWireup="true" CodeFile="LMS-Complete-leaves-report.aspx.cs" Inherits="Admin_frmEmpLeavesReport" Title="Employee leaves report" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  <script type="text/javascript" language="javascript">
function toggleAllCheckboexs(toggle)
{
				n = document.forms[0].length;
				var frm = document.forms[0];
				for(i=0;i<frm.length;i++)
				
					if(frm.elements[i].type=="checkbox")
						if (frm.elements[i].name.indexOf('Cbx')==0)
							frm.elements[i].checked=toggle;
			}
			
			
			function onlyNumbers(evt)
{
    var e = event || evt;
    var charCode = e.which || e.keyCode;
	if ((charCode > 47 && charCode < 58))
    return true;		             
    else
    return false ;		                            
}
			
			function onlyNumbers(evt)
{
    var e = event || evt;
    var charCode = e.which || e.keyCode;
	if ((charCode > 47 && charCode < 58))
    return true;		             
    else
    return false ;		                            
}
</script>
  <style>
      
  <!--.leavetbl { border-collapse: collapse; }   -->
      
.leavetbl td{ border:1px solid #999999; padding:3px;}

</style>
  
 <!--<table style="width:81%"  class="publicloginTable">
    <tr>
      <td colspan="4" class="LoginTitle"><h1 style="color:Black"> Employee Leave Report</h1></td>
    </tr>
    
      <td valign="top" align="left"><table width="100%" class="admintablestyle" style="height: 578px">
          <tr>
            <td colspan="4" class="LoginTitle">Manage Employee</td>
          </tr>
          <tr>
            <td colspan="8"></td>
          </tr>
          <tr>
            <td><table>
                <tr>
                  <td valign="top">Starting date</td>
                  <td valign="top">:</td>
                  <td valign = "top">
                    <br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                        ErrorMessage="Please select a start date" ControlToValidate="GMDStartDate"></asp:RequiredFieldValidator>
                    <br />
                    <asp:CompareValidator ID="CompareValidator2" runat="server" 
                            ErrorMessage="Please enter a valid  date" Operator="DataTypeCheck" 
                            ControlToValidate="GMDStartDate" Type="Date"></asp:CompareValidator></td>
                  <td valign="top">Ending date</td>
                  <td valign="top">:</td>
                  <td valign="top">
                    <br />
                    <asp:CompareValidator ID="CompareValidator1" runat="server" Operator="GreaterThanEqual" 
                                ErrorMessage="End date should be greater or equal to start date." 
                                ControlToCompare="GMDStartDate" ControlToValidate="GMDEndDate" Type="Date"></asp:CompareValidator>
                    <br />
                    <asp:CompareValidator ID="CompareValidator3" runat="server" 
                            ErrorMessage="Please enter a valid date" Operator="DataTypeCheck" 
                            ControlToValidate="GMDEndDate" Type="Date"></asp:CompareValidator></td>
                  <td valign="top"></td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td colspan="8"><table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                <tr>
                  <td colspan="4">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="0" style="width: 20%; height:100%; vertical-align:top"><asp:Label ID="Label1" runat="server" style="valign:top">Select an employee </asp:Label></td>
                  <td style="vertical-align:top; width:40%"></td>
                  <td style="vertical-align:top; width:20%"><asp:Panel ID="pnlEmpEmailSend" runat="server" Visible="true" >
                      <table>
                        <tr>
                          <td style="width:50%"><asp:Label ID="lblEmpName1" runat="server" style="valign:top">Select first emp :</asp:Label></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td colspan="2"></td>
                        </tr>
                        <tr>
                          <td style="width:50%"><asp:Label ID="lblEmpName2" runat="server" style="valign:top">Select second emp :</asp:Label></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td colspan="2" align="right"></td>
                        </tr>
                        <tr>
                          <td colspan="2"><asp:Label ID="lblEmailReport" runat="server"></asp:Label></td>
                        </tr>
                      </table>
                      </asp:Panel>
                    <table>
                      <tr>
                        <td style="width: 17%"> Contact number </td>
                        <td style="width: 2%"> : </td>
                        <td style="width: 161px"></td>
                      </tr>
                      <tr>
                        <td style="width: 17%"> Email address </td>
                        <td style="width: 2%"> : </td>
                        <td style="width: 161px"></td>
                      </tr>
                      <tr>
                        <td style="width: 17%"> Address </td>
                        <td style="width: 2%"> : </td>
                        <td style="width: 161px"></td>
                      </tr>
                      <tr>
                        <td> Designation </td>
                        <td> : </td>
                        <td style="width: 161px"></td>
                      </tr>
                      <tr>
                        <td> CL balance</td>
                        <td> : </td>
                        <td style="width: 161px"></td>
                      </tr>
                      <tr>
                        <td> ML balance</td>
                        <td> : </td>
                        <td style="width: 161px"></td>
                      </tr>
                      <tr style="display:none" >
                        <td> EL balance</td>
                        <td> : </td>
                        <td style="width: 161px"></td>
                      </tr>
                      <tr style="display:none">
                        <td> HPL balance</td>
                        <td> : </td>
                        <td style="width: 161px"></td>
                      </tr>
                      <tr>
                        <td> Extra Leaves</td>
                        <td> : </td>
                        <td style="width: 161px"></td>
                      </tr>
                      <tr>
                        <td> Balance </td>
                        <td> : </td>
                        <td style="width: 161px"></td>
                      </tr>
                    </table></td>
                </tr>
                <tr>
                  <td style="width: 25%"><asp:Label ID="lblError" runat="server"  Visible="false" CssClass="lblerror"></asp:Label>
                    <asp:Label ID="Lbl_Pageinfo" runat="server" Style=""></asp:Label></td>
                  <td align="left" >&nbsp;</td>
                  <td></td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td colspan="4"></td>
          </tr>
        </table></td>
    </tr>
  </table>-->
  <div class="tab-content-holder">
    <h2 style="font-weight: normal !important; font-size: 30px !important; margin-top: 5px;
            margin-bottom: -5px; padding-left: 20px;"> LMS dashboard</h2>
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td width="11%" align="left" valign="top"><div class="treeview_div_left">
            <ul id="treemenu2" class="treeview" style="padding-left: 10px;">
              <li><a href="../LMSAdmin/LMS-Admin.aspx">LMS home</a></li>
              <li><a href="#">Manage</a>
                <ul>
                  <li><a href="../LMSAdmin/LMS-Leave-setting.aspx" class="settings">Annum</a></li>
                  <li><a href="../LMSAdmin/LMS-Add-a-leave.aspx" class="edit_leave">Emps</a></li>
                </ul>
              </li>
              <li><a href="#">Reports</a>
                <ul>
                  <li><a href="../LMSAdmin/LMS-Leaves-between-2dates-report.aspx" class="documents">All</a></li>
                  <li><a href="../LMSAdmin/LMS-Complete-leaves-report.aspx" class="leave_report">Detail</a></li>
                </ul>
              </li>
            </ul>
          </div></td>
        <td width="78%" align="left" valign="top"><table width="100%">
            <tr>
              <td width="99%" valign="top"><div style="background-color: #f3f3f3; border: 1px solid #999999; margin-top: 17px; margin-right: 20px; line-height: 18px; padding-left: 20px; height: auto;">
                  <div>
                    <fieldset  style="height: auto; margin-top: 10px; margin-right:20px; margin-bottom:20px;">
                      <legend>
                      <asp:DropDownList ID="ddlEmpList" runat="server" AutoPostBack="True" 
               Height="25px" onselectedindexchanged="ddlEmpList_SelectedIndexChanged" 
               Width="147px"> </asp:DropDownList>
                      </legend>
                      <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-top:10px;">
                        <tr>
                          <td width="20%" valign="top"><table width="98%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td><fieldset style="height:160px;">
                                    <legend style="font-size: 16px; padding: 0 2px;"><strong>Send email</strong></legend>
                                    <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
                                      <tr>
                                        <td height="25" align="left" valign="middle">Select first emp</td>
                                      </tr>
                                      <tr>
                                        <td height="25" align="left" valign="middle"><asp:DropDownList ID="ddlEmpList1" runat="server" 
                    onselectedindexchanged="ddlEmpList1_SelectedIndexChanged"> </asp:DropDownList></td>
                                      </tr>
                                      <tr>
                                        <td height="25" align="left" valign="middle">Select second emp</td>
                                      </tr>
                                      <tr>
                                        <td height="25" align="left" valign="middle"><asp:DropDownList ID="ddlEmpList2" runat="server" 
                    onselectedindexchanged="ddlEmpList2_SelectedIndexChanged"> </asp:DropDownList></td>
                                      </tr>
                                      <tr>
                                        <td height="40" align="right" valign="middle"><asp:Button ID="btnSendEmail" runat="server" onclick="btnSendEmail_Click" 
                    Text="Send Email" /></td>
                                      </tr>
                                      <tr>
                                        <td></td>
                                      </tr>
                                    </table>
                                  </fieldset></td>
                              </tr>
                              <tr>
                                <td><br />
                                  <fieldset>
                                    <legend  style="font-size: 16px; padding: 0 2px;"><strong>Custom</strong></legend>
                                    <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
                                      <tr>
                                        <td height="25">Contact number</td>
                                      </tr>
                                      <tr>
                                        <td height="25"><asp:Label ID="txtContactNo" runat="server" ReadOnly="True" Width="210px" 
                                BackColor="#CCCCCC"></asp:Label></td>
                                      </tr>
                                      <tr>
                                        <td height="25">Email address</td>
                                      </tr>
                                      <tr>
                                        <td height="25" style="line-height:23px; margin-left:5px; box-shadow:0 1px 5px rgba(0, 0, 0, 0.1) inset;"><asp:Label ID="txtEmailId" runat="server" ReadOnly="True" Width="205px" BorderColor="#999999" BorderWidth="1px" Height="25px" ></asp:Label></td>
                                      </tr>
                                      <tr>
                                        <td height="25">Address</td>
                                      </tr>
                                      <tr>
                                        <td height="25" style="line-height:23px; margin-left:5px; box-shadow:0 1px 5px rgba(0, 0, 0, 0.1) inset;"><asp:Label ID="txtAddress" TextMode="MultiLine" runat="server" ReadOnly="True"
                                Width="205px" Height="49px" BorderColor="#999999" BorderWidth="1px" ></asp:Label></td>
                                      </tr>
                                      <tr>
                                        <td height="25">Designation</td>
                                      </tr>
                                      <tr>
                                        <td height="25" style="line-height:23px; margin-left:5px; box-shadow:0 1px 5px rgba(0, 0, 0, 0.1) inset;"><asp:Label ID="txtDesig" runat="server" ReadOnly="True" Width="205px" Height="25px"
                                BorderColor="#999999" BorderWidth="1px" ></asp:Label></td>
                                      </tr>
                                      <tr>
                                        <td height="25">CL balance</td>
                                      </tr>
                                      <tr>
                                        <td height="25" style="line-height:23px; margin-left:5px; box-shadow:0 1px 5px rgba(0, 0, 0, 0.1) inset;"><asp:Label ID="txtCLBal" runat="server" ReadOnly="True" Width="205px" 
                               BorderColor="#999999" BorderWidth="1px" ></asp:Label></td>
                                      </tr>
                                      <tr>
                                        <td height="25">ML balance</td>
                                      </tr>
                                      <tr>
                                        <td height="25" style="line-height:23px; margin-left:5px; box-shadow:0 1px 5px rgba(0, 0, 0, 0.1) inset;"><asp:Label ID="txtMLBal" runat="server" ReadOnly="True" Width="205px" 
                               BorderColor="#999999" BorderWidth="1px" ></asp:Label></td>
                                      </tr>
                                      <tr>
                                        <td height="25">EL balance</td>
                                      </tr>
                                      <tr>
                                        <td height="25" style="line-height:23px; margin-left:5px; box-shadow:0 1px 5px rgba(0, 0, 0, 0.1) inset;"><asp:Label ID="txtEL" runat="server" Height="23px" ReadOnly="True" 
                                Width="205px" BorderColor="#999999" BorderWidth="1px" ></asp:Label></td>
                                      </tr>
                                      <tr>
                                        <td height="25">Extra leaves</td>
                                      </tr>
                                      <tr>
                                        <td height="25" style="line-height:23px; margin-left:5px; box-shadow:0 1px 5px rgba(0, 0, 0, 0.1) inset;"><asp:Label ID="txtExtraLeaves" runat="server" ReadOnly="True" Width="205px" 
                                BorderColor="#999999" BorderWidth="1px" ></asp:Label></td>
                                      </tr>
                                      <tr>
                                        <td height="25">Balance</td>
                                      </tr>
                                      <tr>
                                        <td height="25" style="line-height:23px; margin-left:5px; box-shadow:0 1px 5px rgba(0, 0, 0, 0.1) inset;"><asp:Label ID="txtTotalBalance" runat="server" ReadOnly="True" Width="205px" 
                                BorderColor="#999999" BorderWidth="1px" ></asp:Label></td>
                                      </tr>
                                    </table>
                                  </fieldset></td>
                              </tr>
                            </table></td>
                          <td width="80%" valign="top"><div style="padding-left: 10px; margin-bottom: 20px;">
                              <table width="98%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td width="10%">Starting date</td>
                                  <td width="20%"><asp:TextBox ID="GMDStartDate" runat="server"  onkeypress="javascript:EnterEvent(event)"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" 
                            TargetControlID="GMDStartDate"
                            Format="dd/MM/yyyy" PopupButtonID="GMDStartDate" > </ajaxToolkit:CalendarExtender></td>
                                  <td width="10%">Ending date</td>
                                  <td><asp:TextBox ID="GMDEndDate" runat="server"  
                               onkeypress="javascript:EnterEvent(event)" AutoPostBack="True" 
                               ontextchanged="GMDEndDate_TextChanged" ></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" 
                            TargetControlID="GMDEndDate"
                            Format="dd/MM/yyyy"  PopupButtonID="GMDEndDate"> </ajaxToolkit:CalendarExtender></td>
                            <td width="50%" align="right" style="font-size:14px;">Totals :                              <asp:Label ID="lblLeaveBWdates" runat="server" style="valign:top"/></td>
                                </tr>
                              </table>
                              <br />
                              <fieldset style="height: auto;">
                                <legend style="font-size: 16px; padding: 0 2px;"><strong>Employee leave details for current and previous year </strong></legend>
                                <div style="overflow-y:scroll; height:612px;">
                                  <asp:Panel ID="pnlLeaveDetails" runat="server"  Direction="LeftToRight"   Height="100%" HorizontalAlign="Center" Width="100%" style="valign:top; line-height:25px"  Visible="true">
                                    <asp:Label ID="lblLeaveDetails" runat="server" style="valign:top" ></asp:Label>
                                    </asp:Panel>
                                </div>
                                <br />
                              </fieldset>
                            </div></td>
                        </tr>
                        <tr>
                          <td></td>
                        </tr>
                      </table>
                    </fieldset>
                  </div>
                </div></td>
            </tr>
          </table></td>
      </tr>
    </table>
    <script type="text/javascript">
            ddtreemenu.createTree("treemenu2", true, 5)

        </script> 
  </div>
</asp:Content>
