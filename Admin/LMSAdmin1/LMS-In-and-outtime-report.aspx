<%@ Page Language="C#" MasterPageFile="~/Admin/LMSAdmin/AdminMasterMenu.master" AutoEventWireup="true"
    CodeFile="LMS-In-and-outtime-report.aspx.cs" Inherits="Admin_frmAdminHome" Title="Welcome to Leave Management System" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">



<script language="javascript" type="text/javascript">
    function hideCalendar() {
        $find("<%=CalendarExtender1.ClientID%>").hide();
        $find("<%=CalendarExtender2.ClientID%>").hide();
        return false;
    } 
</script>
<style>
.leavetbl td,th{ border:1px solid #999999;}
</style>


  <div class="tab-content-holder">
    <h2 style="font-weight:normal !important; font-size:30px !important; margin-top:5px; margin-bottom:-5px; padding-left:20px;">LMS dashboard</h2>
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td width="11%" align="left" valign="top"><div class="treeview_div_left" >
            <ul id="treemenu2" class="treeview" style="padding-left:10px;">
              <li><a href="../LMSAdmin/LMS-Admin.aspx">LMS home</a></li>
              <li><a href="#" >Manage</a>
                <ul rel="open" style="display: block;">
                  <li><a href="../LMSAdmin/LMS-Leave-setting.aspx" class="settings">Annum</a></li>
                  <li><a href="../LMSAdmin/LMS-Add-a-leave.aspx" class="edit_leave">Emps</a></li>
                </ul>
              </li>
              <li><a href="#" >Reports</a>
                <ul rel="open" style="display: block;">
                  <li><a href="../LMSAdmin/LMS-Leaves-between-2dates-report.aspx"
                            class="documents">All</a></li>
                  <li><a href="../LMSAdmin/LMS-Complete-leaves-report.aspx"
                            class="leave_report">Detail</a></li>
   <li><a href="../LMSAdmin/LMS-In-and-outtime-report.aspx"
                            class="leave_report">In/outtime</a></li>
                </ul>
              </li>
            </ul>
          </div></td>
        <td width="80%" align="left" valign="top"><table width="100%">
            <tr>
              <td width="99%" valign="top"><div style="background-color:#f3f3f3; border:1px solid #999999; margin-top:17px; margin-right:20px; padding-left:20px; height:800px;">
                  <table width="98%" cellpadding="0" cellspacing="0" border="0">
                    <tr valign="top">
                      <td width="18%"><div>
                          <fieldset style="height:180px; margin-top:10px;">
                            <legend style="font-size:16px; padding:0 2px;"> <strong>Custom</strong></legend>
                            <table width="98%" cellpadding="0" cellspacing="0" border="0" align="center">
                              <tr>
                                <td height="25" align="left" valign="middle" style="padding-left:5px; display:none">Select interval</td>
                              </tr>
                              <tr>
                                <td align="left"  style="padding-left:5px;"><asp:DropDownList ID="ddlTimeInterval" runat="server" valign="top" 
                        Height="25px" onselectedindexchanged="ddlTimeInterval_SelectedIndexChanged" 
                        Width="176px" AutoPostBack="True" Visible="false" >
                                    <asp:ListItem Value="1">Today</asp:ListItem>
                                    <asp:ListItem Value="2">This week</asp:ListItem>
                                    <asp:ListItem Value="3">Last 7 days</asp:ListItem>
                                    <asp:ListItem Value="4">This month</asp:ListItem>
                                    <asp:ListItem Value="5">Last 31 days</asp:ListItem>
                                  </asp:DropDownList></td>
                              </tr>
                              <tr>
                                <td height="25" align="left" valign="middle"  style="padding-left:5px;">Select a week day</td>
                              </tr>
                              <tr>
                                <td height="25" align="left"  style="padding-left:5px;">
                                <asp:TextBox ID="GMDStartDate" runat="server" CssClass="input_boxline" style="width:172px;" CausesValidation="True" ValidationGroup="g1"
                                 ontextchanged="GMDEndDate_TextChanged" AutoPostBack="true"></asp:TextBox>
                            <%--    <img id="Image1" runat="server" alt="Date" src="~/Admin/LMSAdmin/images/cs.png"/>--%>
                              <%--    <asp:ImageButton runat="Server" ID="Image1" ImageUrl="~/Admin/LMSAdmin/images/cs.png" AlternateText="Click to show calendar"/> --%>
                                  <ajaxtoolkit:calendarextender ID="CalendarExtender1"  runat="server"  
                                        TargetControlID="GMDStartDate" OnClientDateSelectionChanged='hideCalendar'
                                  Format="yyyy-MM-dd"> </ajaxtoolkit:calendarextender></td>
                              </tr>
                              <tr>
                                <td align="left"  style="padding-left:5px;display:none" >To</td>
                              </tr>
                              <tr>
                                <td height="25" align="left"  style="padding-left:5px;"><asp:TextBox ID="GMDEndDate" runat="server" CssClass="input_boxline" 
              AutoPostBack="True" ontextchanged="GMDEndDate_TextChanged" style="width:172px;"  CausesValidation="True" ValidationGroup="g1" Visible="false" ></asp:TextBox>
                    <%-- <asp:ImageButton runat="Server" ID="Image2" ImageUrl="~/Admin/LMSAdmin/images/cs.png" AlternateText="Click to show calendar"    OnClientClick="return false" /> --%>
                   <%-- <img id="Image2" runat="server" alt="Date" src="~/Admin/LMSAdmin/images/cs.png" />--%>
                                  <ajaxToolkit:CalendarExtender ID="CalendarExtender2"  runat="server"  TargetControlID="GMDEndDate" OnClientDateSelectionChanged='hideCalendar'
                                   Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender></td>
                              </tr>
                              <tr>
                                <td align="left">
                               </td>
                              </tr>
                            </table>
                          </fieldset>
                        </div></td>
                      <td width="82%"><div style="padding-left:10px; margin-bottom:20px; ">
                          <fieldset style="height:auto; margin-top:10px;">
                           <legend style="font-size:16px; padding:0 2px;">
                          
                           <asp:DropDownList ID="ddlEmp" runat="server" AutoPostBack="true" 
                           OnSelectedIndexChanged="ddlEmp_SelectedIndexChanged" Visible ="true" 
                           ValidationGroup="g1" Height="22px" Width="176px"></asp:DropDownList>
                           </legend> 
                           <legend style="margin-left:87%; margin-top:-27px;">
                            <asp:Button ID="btnexport" runat="server" Text="Export to Excel" OnClick="btnexport_Click" 
                           ValidationGroup="g1" /> 
                            </legend></span>
                         
                            <asp:Label ID="lblLR4R1Title" runat="server"  Visible="false" Font-Underline="True" ></asp:Label><br /><br />
                            <div id="main" runat="server">                           
                           <div style="height:675px; overflow-y:scroll;" id="HtmlDesign" runat="server">
                           </div>
                           </div>
                             
                          </fieldset>
                        </div></td>
                    </tr>
                  </table>
                </div></td>
            </tr>
          </table></td>
      </tr>
    </table>
    <script type="text/javascript">
        ddtreemenu.createTree("treemenu2", true, 5)

        validatePage = function (button)
         {
            var isValid = Page_ClientValidate();
            button.disabled = isValid;

            //return validation - if true then postback
            return isValid;
        }
</script> 
 <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableClientScript="true" ShowMessageBox="true" ShowSummary="false" ValidationGroup="g1"  DisplayMode="BulletList"  />
  <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="g1"
                                Display="None" ErrorMessage="Please select a start date" ControlToValidate="GMDStartDate">*
                                 </asp:RequiredFieldValidator>
                                  <br/>
                                  <asp:CompareValidator ID="CompareValidator2" runat="server" Display="None"  ValidationGroup="g1"
                            ErrorMessage="Please enter a valid  date" Operator="DataTypeCheck" 
                            ControlToValidate="GMDStartDate" Type="Date">*</asp:CompareValidator>
                                  <br/>
                                  <asp:CompareValidator ID="CompareValidator3" runat="server" Display="None"   ValidationGroup="g1"
                            ErrorMessage="Please enter a valid date" Operator="DataTypeCheck" 
                            ControlToValidate="GMDEndDate" Type="Date">*</asp:CompareValidator>
                                  <br/>
                                  <asp:CompareValidator ID="CompareValidator1" runat="server" Display="None"  ValidationGroup="g1"
                                ErrorMessage="End date should be greater or equal to start date." 
                                ControlToCompare="GMDStartDate" ControlToValidate="GMDEndDate" 
                                Operator="GreaterThanEqual" Type="Date">*</asp:CompareValidator>

                              
        



  </div>
</asp:Content>
