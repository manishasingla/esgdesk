using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections.Generic;

public partial class Admin_frmAdminHome : System.Web.UI.Page
{
    DateTime dtStartingDate;
    DateTime dtEndingDate;
    clsLeave_Logic objLeave = new clsLeave_Logic();
    DataTable dtTemp = null;
    DataTable dtTemp1 = null , dtLR4R1= null, dtLR4R2= null;
    public int DayNo = 0, DayToAdd=0;
    public  bool blnIsDateInterval = false;
    DataView dv2 = new DataView();

    clsEmployee_Logic1 objEmployee = new clsEmployee_Logic1();
    string strError = "No records found";
    List<DateTime> dates = new List<DateTime>();
    public string strreportname = "";
    string dtSDate;
    string dtEDate;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] != null && Session["admin"].ToString() != "")
        { 

            if (DatabaseHelper.isAdmin(Session["admin"].ToString()) == false)
            {
                Response.Redirect("~/Admin/login.aspx");
                return;
            }
        }

        if (!IsPostBack)
        {
            ddlTimeInterval.SelectedIndex = 1;
            fnSetDefaultDate();
            fnBindEmpddl();
            Session["TI"] = "TW";
            lblLR4R1Title.Text = "Leave report for this week";
            fnGenerateTimeSheet();

        }
    }

    public void fnBindEmpddl()
    {
        DataSet dsTemp = DatabaseHelper.getDataset(@"select u.FirstName + ' '+u.lastName as EmpName , u.user_id from users u where IsCW='Y' order by EmpName");
        try
        {
            if (dsTemp != null)
            {
                ddlEmp.DataSource = dsTemp;
                ddlEmp.DataTextField = "EmpName";
                ddlEmp.DataValueField = "user_id";
                ddlEmp.DataBind();
                ddlEmp.Items.Insert(0, "--Select--");
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Leaves-between-2dates-report : fnBindddl()");
          //  lblError.Text = ex.Message;
           // lblError.Visible = true;
        }

    }

    protected void ddlTimeInterval_SelectedIndexChanged(object sender, EventArgs e)
    {
        blnIsDateInterval = false;

        if (ddlTimeInterval.SelectedValue.ToString() == "1") //Today
        {
            DayNo = 0;
            Session["TI"] = null;
            lblLR4R1Title.Text = "Leave report for today";
        }

        else if (ddlTimeInterval.SelectedValue.ToString() == "3")//Last 7 days 
        {
            DayNo = -7;
            Session["TI"] = null;
            lblLR4R1Title.Text = "Leave report for last 7 days";
        }


        else if (ddlTimeInterval.SelectedValue.ToString() == "5")// Last 31 days
        {
            DayNo = -31;
            Session["TI"] = null;
            lblLR4R1Title.Text = "Leave report for last 31 days";
        }

        else if (ddlTimeInterval.SelectedValue.ToString() == "4")// This month
        {
            Session["TI"] = "TM";
            lblLR4R1Title.Text = "Leave report for this month";
        }

        else if (ddlTimeInterval.SelectedValue.ToString() == "2") //This Week
        {
            Session["TI"] = "TW";
            lblLR4R1Title.Text = "Leave report for this week";
        }
        fnGenerateTimeSheet();
      
    }

    public void fnSetDefaultDate()
    {

        // lblError.Visible = false;
        String DayName = DateTime.Now.DayOfWeek.ToString();

        if (DayName == "Monday")
        {
            DayNo = 1;
            DayToAdd = 7;
        }
        else if (DayName == "Tuesday")
        {
            DayNo = 2;
            DayToAdd = 6;
        }

        else if (DayName == "Wednesday")
        {
            DayNo = 3;
            DayToAdd = 5;
        }

        else if (DayName == "Thursday")
        {
            DayNo = 4;
            DayToAdd = 4;
        }
        else if (DayName == "Friday")
        {
            DayNo = 5;
            DayToAdd = 3;
        }
        else if (DayName == "Saturday")
        {
            DayNo = 6;
            DayToAdd = 2;
        }

         dtStartingDate = DateTime.Today.AddDays(-DayNo + 1);    // Monday
         dtEndingDate = DateTime.Today.AddDays(6 - DayNo);      //Saturday
         GMDStartDate.Text = dtStartingDate.ToString("yyyy/MM/dd");
         GMDEndDate.Text = dtEndingDate.ToString("yyyy/MM/dd");

    }

    protected void GMDEndDate_TextChanged(object sender, EventArgs e)
    {
        CompareValidator1.Validate();
        blnIsDateInterval = true;
       // fnBindLR4Reater1();
        fnGenerateTimeSheet();
    }

    protected void ddlEmp_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {

            // RequiredFieldValidator2.Validate();
            string strdate = "";
            fnGenerateTimeSheet();

            if (GMDStartDate.Text.ToString() == "")
            {
                strdate = @"SELECT 
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0),103)   as Date_of_Monday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+1,103) as Date_of_Tuesday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+2,103) as Date_of_Wednesday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+3,103) as Date_of_Thusday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+4,103) as Date_of_Friday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+5,103) as Date_of_Saturday";
            }
            else
            {
                strdate = @"SELECT  CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0),103) as Date_of_Monday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+1,103) as Date_of_Tuesday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+2,103) as Date_of_Wednesday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+3,103) as Date_of_Thusday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+4,103) as Date_of_Friday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+5,103) as Date_of_Saturday";

            }
            DataSet ds = DatabaseHelper.getDataset(strdate);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                dynamically(ds);
                if (ddlEmp.SelectedItem.Text != "ESG") //(ddlEmp.SelectedItem.Text == "PHG")
                {
                    strreportname = ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "_To_" + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString();
                }
                else
                {
                    strreportname = ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "_To_" + ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString();
                }
            }
        }
        catch
        {
        }
    }



    void fnGenerateTimeSheet()
    {
        string strQuery = "";

        //if (blnIsDateInterval == false) // Means 
        //{
        //    if (Session["TI"] == null)
        //    {
        //        dtStartingDate = DateTime.Today.AddDays(DayNo); // DayNo value has been set by dropdown. 
        //        dtEndingDate = DateTime.Today;
        //    }

        //    else
        //    {
        //        if (Session["TI"] == "TW")
        //        {
        //            fnSetDefaultDate(); //Set dates Inteval for Week values for dtStartingDate,dtEndingDate.
        //        }
        //        else if (Session["TI"] == "TM")
        //        {
        //            DateTime today = DateTime.Today;
        //            DateTime FirstDate = new DateTime(today.Year, today.Month, 1);
        //            today = new DateTime(today.Year, today.Month + 1, 1);
        //            DateTime LastDate = today.AddDays(-1);
        //            dtStartingDate = FirstDate;
        //            dtEndingDate = LastDate;
        //        }
        //    }


        //    dtSDate = dtStartingDate.ToString("yyyy/MM/dd");
        //    dtEDate = dtEndingDate.ToString("yyyy/MM/dd");

        //    GMDStartDate.Text = dtSDate.ToString();
        //    GMDEndDate.Text = dtEDate.ToString();
        //}
        //else
        //{
        //    fnFillDates();
        //}

        strQuery = @"SELECT  CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0),103) as Date_of_Monday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+1,103) as Date_of_Tuesday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+2,103) as Date_of_Wednesday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+3,103) as Date_of_Thusday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+4,103) as Date_of_Friday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+5,103) as Date_of_Saturday";

        DataSet ds = DatabaseHelper.getDataset(strQuery);
        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            strreportname = ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "_To_" + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString();
            dynamically(ds);
        }
    }

    public void dynamically(DataSet ds)
    {
        try
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            string strQuery = "";

           // if (ddlProj.SelectedItem != null)
           // {
                HtmlDesign.Visible = true;

                //if (ddlProj.SelectedItem.Text.Trim() != "--Select--")
             //   if (ddlProj.SelectedIndex != 0)
               // {
                    strQuery = @"select u.* from users u where IsCW='Y'";
                    //strQuery += " and pd.Project_id= " + ddlProj.SelectedValue.ToString() + " and pd.CompanyName='" + Session["CompanyName"].ToString() + "'";

                    if (ddlEmp.SelectedItem.Text != "--Select--")
                    {
                        strQuery += " and u.user_id='" + ddlEmp.SelectedValue.ToString() + "'";
                    }
                    strQuery += " order by username ";
                    DataSet dsAllEmp = DatabaseHelper.getDataset(strQuery);
                    if (dsAllEmp != null && dsAllEmp.Tables.Count > 0 && dsAllEmp.Tables[0].Rows.Count > 0)
                    {
                        sb.Append("<table width='100%' border='0' cellpadding='0' cellspacing='0'>");
                        sb.Append("<tr>");
                        sb.Append("<td height='10px' colspan='8' align='left' style='font-size:medium'>");
                        sb.Append("<b>Employee's in/out time report (from ");
                        // sb.Append(ds.Tables[0].Rows[0]["Date_of_Monday"].ToString());
                        sb.Append(ds.Tables[0].Rows[0]["Date_of_Monday"].ToString());
                        sb.Append(" to ");
                        if (ddlEmp.SelectedItem.Text == "PHG")
                        {
                            sb.Append(ds.Tables[0].Rows[0]["Date_of_Friday"].ToString() + ")");
                        }
                        else
                        {
                            sb.Append(ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString() + ")");
                        }
                        sb.Append("</b></td>");
                        sb.Append("</tr>");
                        //sb.Append("<tr>");
                        //sb.Append("<td height='10px'>");
                        //sb.Append("</td>");
                        //sb.Append("</tr>");
                        sb.Append("</table>");
                        sb.Append("<table width='100%' border='0' cellpadding='0' cellspacing='0'>");


                        sb.Append("<tr>");
                        sb.Append("<td height='37px'>");

                        sb.Append("<br><table border='0' width='100%' height='37px' cellpadding='0' cellspacing='0'  class='leavetbl' style='background:#e1e1e1'><tr><td height='56px' rowspan='2' align='center'><b>Employee name</b></td></tr></table>");
                        sb.Append("</td>");


                        sb.Append("<td>");

                        sb.Append("<br><table border='0' width='100%'height='37px' cellpadding='0' cellspacing='0'  class='leavetbl' style='background:#e1e1e1'><tr><td height='56px' rowspan='2' align='center'><b>Skill</b></td></tr></table>");
                        sb.Append("</td>");
                        sb.Append("<td>");


                        sb.Append("<br><table width='100%' border='0' cellpadding='0' cellspacing='0'  class='leavetbl' style='background:#e1e1e1'>");
                        sb.Append("<tr align='center'>");



                        if (ddlEmp.SelectedItem.Text == "PHG")
                        {
                            sb.Append("<td width='20%'>");
                        }
                        else
                        {
                            sb.Append("<td width='17%'>");
                        }

                        sb.Append(" " + ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + " (Mon)");
                        sb.Append("<br>In/Out (GMT) Total");
                        //sb.Append("<table border='0' cellpadding='0' cellspacing='0' align='center'><tr><td align='center'>In/Out (GMT)</td><td> Total </td></tr></table>");
                        sb.Append("</td>");


                        if (ddlEmp.SelectedItem.Text == "PHG")
                        {
                            sb.Append("<td width='20%'>");
                        }
                        else
                        {
                            sb.Append("<td width='17%'>");
                        }

                        sb.Append(" " + ds.Tables[0].Rows[0]["Date_of_Tuesday"].ToString() + " (Tue)");
                        sb.Append("<br>In/Out (GMT) Total");
                        //sb.Append("<table border='0' cellpadding='0' cellspacing='0' align='center'><tr><td align='center'>In/Out (GMT)</td><td> Total </td></tr></table>");
                        sb.Append("</td>");


                        if (ddlEmp.SelectedItem.Text == "PHG")
                        {
                            sb.Append("<td width='20%'>");
                        }
                        else
                        {
                            sb.Append("<td width='17%'>");
                        }


                        sb.Append(" " + ds.Tables[0].Rows[0]["Date_of_Wednesday"].ToString() + " (Wed)");
                        sb.Append("<br>In/Out (GMT) Total");
                        //sb.Append("<table border='0' cellpadding='0' cellspacing='0' align='center'><tr><td align='center'>In/Out (GMT)</td><td> Total </td></tr></table>");
                        sb.Append("</td>");


                        if (ddlEmp.SelectedItem.Text == "PHG")
                        {
                            sb.Append("<td width='20%'>");
                        }
                        else
                        {
                            sb.Append("<td width='17%'>");
                        }


                        sb.Append(" " + ds.Tables[0].Rows[0]["Date_of_Thusday"].ToString() + " (Thur)");
                        sb.Append("<br>In/Out (GMT) Total");
                        //sb.Append("<table border='0' cellpadding='0' cellspacing='0' align='center'><tr><td align='center'>In/Out (GMT)</td><td> Total </td></tr></table>");
                        sb.Append("</td>");

                        if (ddlEmp.SelectedItem.Text == "PHG")
                        {
                            sb.Append("<td width='20%'>");
                        }
                        else
                        {
                            sb.Append("<td width='17%'>");
                        }

                        sb.Append(" " + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString() + " (Fri)");
                        sb.Append("<br>In/Out (GMT) Total");
                        //sb.Append("<table border='0' cellpadding='0' cellspacing='0' align='center'><tr><td align='center'>In/Out (GMT)</td><td> Total </td></tr></table>");
                        sb.Append("</td>");


                        if (ddlEmp.SelectedItem.Text != "PHG")
                        {
                            sb.Append("<td width='20%'>");
                            sb.Append(" " + ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString() + " (Sat)");
                            sb.Append("<br>In/Out (GMT) Total");
                            //sb.Append("<table border='0' cellpadding='0' cellspacing='0' align='center'><tr><td align='center'>In/Out (GMT)</td><td> Total </td></tr></table>");
                            sb.Append("</td>");
                        }


                        sb.Append("</tr>");
                        sb.Append("</table>");
                        sb.Append("</td>");

                        sb.Append("</tr>");
                        for (int iEmp = 0; iEmp < dsAllEmp.Tables[0].Rows.Count; iEmp++)
                        {
                            if (iEmp % 2 == 0)
                            {
                                sb.Append("<tr style='background:#f7f7f7; height:30px;'>");
                            }
                            else
                            {
                                sb.Append("<tr style='background:#ffffff; height:30px;'>");
                            }
                            sb.Append("<td align='left' style='width:100.75pt;' >");
                            sb.Append("<table border='0' width='100%' cellpadding='0' cellspacing='0' class='leavetbl'><tr><td>" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "</td></tr></table>");
                            if (dsAllEmp.Tables[0].Rows[iEmp]["skills"].ToString() != "")
                            {
                                sb.Append("</td><td align='left' ><table border='0' width='100%' height='100%' cellpadding='0' cellspacing='0' class='leavetbl'><tr><td >" + dsAllEmp.Tables[0].Rows[iEmp]["skills"].ToString() + "</td></tr></table></td>");
                            }
                            else
                            {
                                sb.Append("</td><td align='left' ><table border='0' width='100%' height='100%' cellpadding='0' cellspacing='0' class='leavetbl'><tr><td  align='center'> -/ </td></tr></table></td>");
                            }
                            //******************************************* Create In Time ******************************************************************
                            sb.Append("<td>");

                            sb.Append("<table width='100%' border='0' cellpadding='0' cellspacing='0'>");
                            sb.Append("<tr>");


                            if (ddlEmp.SelectedItem.Text == "PHG")
                            {
                                sb.Append("<td width='20%'>");
                            }
                            else
                            {
                                sb.Append("<td width='17%'>");
                            }

                            sb.Append("<table width='100%' border='0' height='100%' cellpadding='0' cellspacing='0' class='leavetbl'>");
                            sb.Append("<tr>");
                            sb.Append("<td colspan='2'>");

                            string strInout1 = "";
                            object objIntimeALL1 = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "'");
                            if (objIntimeALL1 != null && objIntimeALL1.ToString() != "")
                            {
                                DateTime dtALL1 = Convert.ToDateTime(objIntimeALL1).AddHours(-5).AddMinutes(-30);
                                strInout1 = dtALL1.ToString("HH:mm").ToString() + "/";
                            }
                            else
                            {
                                strInout1 = "-/";
                            }


                            object objOuttimeALL1 = DatabaseHelper.executeScalar("select Outtime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "'");
                            if (objOuttimeALL1 != null && objOuttimeALL1.ToString() != "")
                            {
                                DateTime dtoutALL1 = Convert.ToDateTime(objOuttimeALL1).AddHours(-5).AddMinutes(-30);
                                strInout1 += dtoutALL1.ToString("HH:mm");
                            }
                            else
                            {
                                strInout1 += "-";
                            }

                            sb.Append(strInout1);


                            object objTotalHours1 = DatabaseHelper.executeScalar("select TotalHrs from tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "'  and EodcDate='" + ds.Tables[0].Rows[0]["Date_of_Monday"].ToString().Trim() + "'");
                            if (objTotalHours1 != null && objTotalHours1.ToString() != "")
                            {
                                string totime = "";
                                string[] strbreak = objTotalHours1.ToString().Split(':');
                                totime = strbreak[0] + ":" + strbreak[1];
                                sb.Append("--" + totime);


                            }
                            else
                            {
                                sb.Append("-");

                            }

                            sb.Append("</td>");

                            string firstuserdate1 = "";
                            if (GMDStartDate.Text != "")
                            {
                                firstuserdate1 = @"Select CONVERT(VARCHAR(10),AbsentDate,103) as LeaveDate from tbl_LeaveDetails where UserName='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and  (AbsentDate >=DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text + "'),0) and AbsentDate <= DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text + "'),0)+4)";
                            }
                            else
                            {
                                firstuserdate1 = @"Select CONVERT(VARCHAR(10),AbsentDate,103)  as LeaveDate from tbl_LeaveDetails where UserName='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and (AbsentDate >=DATEADD(week, DATEDIFF(week, 0,GETDATE()),0) and AbsentDate <= DATEADD(week, DATEDIFF(week, 0,GETDATE()),0)+4)";
                            }
                            DataSet dsFirstdate = DatabaseHelper.getDataset(firstuserdate1);
                            sb.Append("</tr>");
                            sb.Append("</table>");
                            sb.Append("</td>");
                            //----------------------------------------------------------------
                            //----------------------------------------------------------------
                            if (ddlEmp.SelectedItem.Text == "PHG")
                            {
                                sb.Append("<td width='20%'>");
                            }
                            else
                            {
                                sb.Append("<td width='17%'>");
                            }
                            sb.Append("<table width='100%' border='0' height='100%' cellpadding='0' cellspacing='0' class='leavetbl'>");
                            sb.Append("<tr>");
                            sb.Append("<td colspan='2'>");

                            string strInout2 = "";
                            object objIntimeALL2 = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Tuesday"].ToString() + "'");
                            if (objIntimeALL2 != null && objIntimeALL2.ToString() != "")
                            {
                                DateTime dtALL2 = Convert.ToDateTime(objIntimeALL2).AddHours(-5).AddMinutes(-30);

                                strInout2 = dtALL2.ToString("HH:mm").ToString() + "/";
                            }
                            else
                            {

                                strInout2 = "-/";
                            }

                            object objOuttimeALL2 = DatabaseHelper.executeScalar("select Outtime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Tuesday"].ToString() + "'");
                            if (objOuttimeALL2 != null && objOuttimeALL2.ToString() != "")
                            {
                                DateTime dtoutALL2 = Convert.ToDateTime(objOuttimeALL2).AddHours(-5).AddMinutes(-30);

                                strInout2 += dtoutALL2.ToString("HH:mm");
                            }
                            else
                            {

                                strInout2 += "-";
                            }

                            sb.Append(strInout2);


                            object objTotalHours2 = DatabaseHelper.executeScalar("select TotalHrs from tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and EodcDate='" + ds.Tables[0].Rows[0]["Date_of_Tuesday"].ToString().Trim() + "'");
                            if (objTotalHours2 != null && objTotalHours2.ToString() != "")
                            {
                                string totime2 = "";
                                string[] strbreak2 = objTotalHours2.ToString().Split(':');
                                totime2 = strbreak2[0] + ":" + strbreak2[1];
                                sb.Append("--" + totime2);

                            }
                            else
                            {

                                sb.Append("-");

                            }
                            sb.Append("</td>");
                            sb.Append("</tr>");
                            sb.Append("</table>");
                            sb.Append("</td>");

                            //---------------------------------------------------------------
                            //----------------------------------------------------------------
                            string strInout3 = "";

                            if (ddlEmp.SelectedItem.Text == "PHG")
                            {
                                sb.Append("<td width='20%'>");
                            }
                            else
                            {
                                sb.Append("<td width='17%'>");
                            }


                            sb.Append("<table width='100%' border='0' height='100%' cellpadding='0' cellspacing='0' class='leavetbl'>");
                            sb.Append("<tr>");
                            sb.Append("<td colspan='2'>");
                            object objIntimeALL3 = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Wednesday"].ToString() + "'");
                            if (objIntimeALL3 != null && objIntimeALL3.ToString() != "")
                            {
                                DateTime dtALL3 = Convert.ToDateTime(objIntimeALL3).AddHours(-5).AddMinutes(-30);

                                strInout3 = dtALL3.ToString("HH:mm") + "/";

                            }
                            else
                            {

                                strInout3 = "-/";
                            }
                            object objOuttimeALL3 = DatabaseHelper.executeScalar("select Outtime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Wednesday"].ToString() + "'");
                            if (objOuttimeALL3 != null && objOuttimeALL3.ToString() != "")
                            {
                                DateTime dtoutALL3 = Convert.ToDateTime(objOuttimeALL3).AddHours(-5).AddMinutes(-30);

                                strInout3 += dtoutALL3.ToString("HH:mm");
                            }
                            else
                            {

                                strInout3 += "-";
                            }
                            sb.Append(strInout3);

                            object objTotalHours3 = DatabaseHelper.executeScalar("select TotalHrs from tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and EodcDate='" + ds.Tables[0].Rows[0]["Date_of_Wednesday"].ToString().Trim() + "'");
                            if (objTotalHours3 != null && objTotalHours3.ToString() != "")
                            {
                                string totime3 = "";
                                string[] strbreak3 = objTotalHours3.ToString().Split(':');
                                totime3 = strbreak3[0] + ":" + strbreak3[1];
                                sb.Append("--" + totime3);
                                //sb.Append("-" + objTotalHours3.ToString());
                            }
                            else
                            {
                                sb.Append("-");
                            }

                            sb.Append("</td>");
                            sb.Append("</tr>");
                            sb.Append("</table>");
                            sb.Append("</td>");

                            //---------------------------------------------------------------
                            //----------------------------------------------------------------
                            string strInout4 = "";


                            if (ddlEmp.SelectedItem.Text == "PHG")
                            {
                                sb.Append("<td width='20%'>");
                            }
                            else
                            {
                                sb.Append("<td width='17%'>");
                            }

                            sb.Append("<table width='100%' border='0' height='100%' cellpadding='0' cellspacing='0' class='leavetbl'>");
                            sb.Append("<tr>");
                            sb.Append("<td colspan='2'>");

                            object objIntimeALL4 = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Thusday"].ToString() + "'");
                            if (objIntimeALL4 != null && objIntimeALL4.ToString() != "")
                            {
                                DateTime dtALL4 = Convert.ToDateTime(objIntimeALL4).AddHours(-5).AddMinutes(-30);

                                strInout4 = dtALL4.ToString("HH:mm") + "/";
                            }
                            else
                            {

                                strInout4 = "-/";
                            }

                            object objOuttimeALL4 = DatabaseHelper.executeScalar("select Outtime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Thusday"].ToString() + "'");
                            if (objOuttimeALL4 != null && objOuttimeALL4.ToString() != "")
                            {
                                DateTime dtoutALL4 = Convert.ToDateTime(objOuttimeALL4).AddHours(-5).AddMinutes(-30);

                                strInout4 += dtoutALL4.ToString("HH:mm");
                            }
                            else
                            {

                                strInout4 += "-";
                            }

                            sb.Append(strInout4);
                            object objTotalHours4 = DatabaseHelper.executeScalar("select TotalHrs from tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and EodcDate='" + ds.Tables[0].Rows[0]["Date_of_Thusday"].ToString().Trim() + "'");
                            if (objTotalHours4 != null && objTotalHours4.ToString() != "")
                            {
                                string totime4 = "";
                                string[] strbreak4 = objTotalHours4.ToString().Split(':');
                                totime4 = strbreak4[0] + ":" + strbreak4[1];
                                sb.Append("--" + totime4);

                            }
                            else
                            {
                                sb.Append("-");
                            }

                            sb.Append("</td>");
                            sb.Append("</tr>");
                            sb.Append("</table>");
                            sb.Append("</td>");
                            //----------------------------------------------------------------
                            //----------------------------------------------------------------
                            string strInout5 = "";


                            if (ddlEmp.SelectedItem.Text == "PHG")
                            {
                                sb.Append("<td width='20%'>");
                            }
                            else
                            {
                                sb.Append("<td width='17%'>");
                            }

                            sb.Append("<table width='100%' border='0' height='100%' cellpadding='0' cellspacing='0' class='leavetbl'>");
                            sb.Append("<tr>");
                            sb.Append("<td colspan='2'>");

                            object objIntimeALL5 = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString() + "'");
                            if (objIntimeALL5 != null && objIntimeALL5.ToString() != "")
                            {
                                DateTime dtALL5 = Convert.ToDateTime(objIntimeALL5).AddHours(-5).AddMinutes(-30);

                                strInout5 = dtALL5.ToString("HH:mm") + "/";
                            }
                            else
                            {

                                strInout5 = "-/";
                            }
                            object objOuttimeALL5 = DatabaseHelper.executeScalar("select Outtime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString() + "'");
                            if (objOuttimeALL5 != null && objOuttimeALL5.ToString() != "")
                            {
                                DateTime dtoutALL5 = Convert.ToDateTime(objOuttimeALL5).AddHours(-5).AddMinutes(-30);

                                strInout5 += dtoutALL5.ToString("HH:mm");
                            }
                            else
                            {

                                strInout5 += "-";
                            }
                            sb.Append(strInout5);

                            object objTotalHours5 = DatabaseHelper.executeScalar("select TotalHrs from tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and EodcDate='" + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString().Trim() + "'");
                            if (objTotalHours5 != null && objTotalHours5.ToString() != "")
                            {
                                string totime5 = "";
                                string[] strbreak5 = objTotalHours5.ToString().Split(':');
                                totime5 = strbreak5[0] + ":" + strbreak5[1];
                                sb.Append("--" + totime5);

                            }

                            sb.Append("</td>");

                            sb.Append("</tr>");
                            sb.Append("</table>");
                            sb.Append("</td>");
                            //----------------------------------------------------------------
                            //----------------------------------------------------------------
                            if (ddlEmp.SelectedItem.Text != "PHG")
                            {
                                string strInout6 = "";
                                sb.Append("<td width='20%'>");
                                sb.Append("<table width='100%' border='0' height='100%' cellpadding='0' cellspacing='0' class='leavetbl'>");
                                sb.Append("<tr>");
                                sb.Append("<td colspan='2'>");


                                object objIntimeALL6 = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString() + "'");
                                if (objIntimeALL6 != null && objIntimeALL6.ToString() != "")
                                {
                                    DateTime dtALL6 = Convert.ToDateTime(objIntimeALL6).AddHours(-5).AddMinutes(-30);

                                    strInout6 = dtALL6.ToString("HH:mm") + "/";
                                }
                                else
                                {

                                    strInout6 = "-/";
                                }
                                object objOuttimeALL6 = DatabaseHelper.executeScalar("select Outtime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString() + "'");
                                if (objOuttimeALL6 != null && objOuttimeALL6.ToString() != "")
                                {
                                    DateTime dtoutALL6 = Convert.ToDateTime(objOuttimeALL6).AddHours(-5).AddMinutes(-30);

                                    strInout6 += dtoutALL6.ToString("HH:mm");
                                }
                                else
                                {

                                    strInout6 += "-";
                                }
                                sb.Append(strInout6);


                                object objTotalHours6 = DatabaseHelper.executeScalar("select TotalHrs from tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and EodcDate='" + ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString().Trim() + "'");
                                if (objTotalHours6 != null && objTotalHours6.ToString() != "")
                                {
                                    string totime6 = "";
                                    string[] strbreak6 = objTotalHours6.ToString().Split(':');
                                    totime6 = strbreak6[0] + ":" + strbreak6[1];
                                    sb.Append("--" + totime6);
                                    //sb.Append("-" + objTotalHours6.ToString());
                                }

                                sb.Append("</td>");
                                sb.Append("</tr>");
                                sb.Append("</table>");
                                sb.Append("</td>");
                            }


                            sb.Append("</table>");
                            sb.Append("</td>");


                            //******************************************* End Create In Time ******************************************************************

                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");
                        HtmlDesign.InnerHtml = sb.ToString();
                    }

               // }
           // }
        }
        catch (Exception ex)
        {
        }
    }

    protected void btnexport_Click(object sender, EventArgs e)
    {
        Response.ContentType = "application/x-msexcel";
        Response.AddHeader("Content-Disposition", "attachment; filename=Timesheet_report_from_'" + strreportname + "'.xls");
        //Response.ContentEncoding = Encoding.UTF8;
        System.IO.StringWriter tw = new System.IO.StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(tw);
        main.RenderControl(hw);
        Response.Write(tw.ToString());
        Response.End();

    }

    public void fnFillDates()
    {
        if (GMDStartDate.Text.Trim() == "")
        {
            dtSDate = DateTime.Today.AddDays(-DayNo + 1).ToString("yyyy-MM-dd");  //FirstDayOfYear(dt).ToString("dd/MM/yyyy");
            GMDStartDate.Text = dtSDate;
            dtStartingDate = DateTime.Today.AddDays(-DayNo + 1);
        }
        else
        {
            dtStartingDate = Convert.ToDateTime(GMDStartDate.Text);
            dtSDate = dtStartingDate.ToString("yyyy-MM-dd");
        }

        if (GMDEndDate.Text.Trim() == "")
        {
            dtEDate = DateTime.Today.AddDays(6 - DayNo).ToString("yyyy-MM-dd"); //LastDayOfYear(dt).ToString("dd/MM/yyyy");
            GMDEndDate.Text = dtEDate;
            dtEndingDate = DateTime.Today.AddDays(6 - DayNo);

        }
        else
        {
            dtEndingDate = Convert.ToDateTime(GMDEndDate.Text);
            dtEDate = dtEndingDate.ToString("yyyy-MM-dd");
        }
    }
}


