using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
public partial class Admin_frmEmpLeaveDetails : System.Web.UI.Page
{
    clsEmployee_Logic1 objEmployee = new clsEmployee_Logic1();
    clsLeave_Logic objLeave = new clsLeave_Logic();
    String dtDayOfLeave = "";
    static DateTime dtDayOfLeave1 = System.DateTime.Now;
    static DataView dv = null;
    static string strLeaveType = "";
    static string strPONP = "";
    private string strError = "No records found.";
    static double   NoOfLPY, EB;
    static int Year,id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fillddl();

            fnBindEmpddl();
  
            fnFillLeaveTextBox();                       
        }
    }

        void  fillddl()
        {
            try
            {
                int year;
                year = DateTime.Now.Year;
                int YearAtMid = year - 6;
                int j = 1;
                for (int i = 0; i <= 12; i++)
                {
                    if (i <= 6)
                    {
                        ddlYear.Items.Add(Convert.ToString(YearAtMid + i));

                    }
                    else
                    {
                        ddlYear.Items.Add(Convert.ToString(year + j));
                        j++;
                    }
                    ddlYear.SelectedIndex = 6;
                }
            }
            catch (Exception ex)
            {
                DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "frmLeaveSetting : fillddl Method");

            }

        }


        public void fnBindEmpddl()
        {
            ddlEmpList.DataSource = DatabaseHelper.getDataset("Select (firstname+' '+lastname) as EmpName , user_id from users ");
            ddlEmpList.DataValueField = "user_id";
            ddlEmpList.DataTextField = "EmpName";
            ddlEmpList.DataBind();
            ddlEmpList.Items.Insert(0, "--Select--");

        }

        

        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if(ddlYear.SelectedItem!=null)
                Year = Convert.ToInt32( ddlYear.SelectedItem.Text);
                fnFillLeaveTextBox();
                getEB();
            }
            catch (Exception ex)
            {
                DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "frmLeaveSetting : ddlYear_SelectedIndexChanged event");

            }
        }

        public void fnFillLeaveTextBox()
        {
            try
            {
                lblSuccessMsg.Text = "";
                objLeave.fnGetLeaveDetails(Convert.ToInt32(ddlYear.SelectedItem.Text));

                txtTotalLeave.Text = objLeave.TotalBalance.ToString();
            }
            catch (Exception ex)
            {
                DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "frmLeaveSetting : fnFillLeaveTextBox method");
                lblSuccessMsg.Text = ex.Message;
            }
        }

    public void fnSetLeave4AllUser(string strYear)
    {
        try
        {
            string strDate = "01/01/" + strYear;
            DateTime dtOfDdl = Convert.ToDateTime(strDate);
            DataSet dsUserIdList = objLeave.fnGetUserId();
          
            try
            {
                if (dsUserIdList != null)
                {
                    DataTable dtUserIdList = dsUserIdList.Tables[0];

                    for (int i = 0; i < dtUserIdList.Rows.Count; i++)
                    {
                        try
                        {
                            string strUserId = dtUserIdList.Rows[i]["user_id"].ToString();
                            if (dtUserIdList.Rows[i]["DOJ"].ToString().Trim() != "")
                            {
                                string strDOJ = dtUserIdList.Rows[i]["DOJ"].ToString();
                                DateTime dtDOJ = Convert.ToDateTime(strDOJ);

                                if (dtDOJ.Year == dtOfDdl.Year)
                                {
                                    objLeave.fnInsertLeavesDetail(dtDOJ, strUserId, true);// INSERT LEAVES FOR CURRENT YEAR
                                }
                                else                            // INSERT LEAVES FOR YEAR OF DATE OF JOINING ALONG WITH CURRENT YEAR
                                {
                                    objLeave.fnInsertLeavesDetail(dtDOJ, strUserId, false);
                                    objLeave.fnInsertLeavesDetail(dtOfDdl, strUserId, false);
                                }

                            }
                            else
                            {
                                objLeave.fnInsertLeavesDetail(dtOfDdl, strUserId, false);
                            }
                        }
                        catch (Exception ex)
                        {
                            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "frmLeaveSetting : fnSetLeave4AllUser method");
                            lblSuccessMsg.Text = ex.Message;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "frmLeaveSetting : fnSetLeave4AllUser method");
                lblSuccessMsg.Text = ex.Message;
            }
        }
    
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "frmLeaveSetting : fnSetLeave4AllUser method");
            lblSuccessMsg.Text = ex.Message;
        }
    }

    protected void btnSetLeave_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlYear.SelectedItem != null)
            {
                int y1, y2;
                y1=Convert.ToInt32(ddlYear.SelectedItem.Text);
                y2 = Convert.ToInt32(DateTime.Now.Year.ToString());
                if (y1>=y2)
                {

                    int intNoOfLeave = 0;

                    if (txtTotalLeave.Text.Trim() != "")
                    {
                        intNoOfLeave = Convert.ToInt32(txtTotalLeave.Text);

                        int intIsSuccess = objLeave.SetLeaveDetails(Convert.ToInt32(ddlYear.SelectedItem.Text), intNoOfLeave);

                        if (intIsSuccess >= 1)
                        {
                            lblSuccessMsg.Text = "Operation performed successfully.";
                            fnSetLeave4AllUser(ddlYear.SelectedItem.Text);
                        }
                        else
                            lblSuccessMsg.Text = "Please try again , process could not be completed successfully at this moment.";
                    }
                    else
                    {
                        lblSuccessMsg.Text = "Please enter the balance.";
                    }

                }
                else
                {
                    lblSuccessMsg.Text = "Unable to update leaves for previous years!";
                }
            }
            
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "frmLeaveSetting : btnSetLeave_Click event");
            lblSuccessMsg.Text = ex.Message;
        }

    }
    protected void ddlEmpList_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlEmpList.SelectedItem.Text != "--Select--")
        {
            id = Convert.ToInt32(ddlEmpList.SelectedValue.ToString());
            Session["id"] = id;
            getEB();
            lblEBStatus.Text = "";
                lblSuccessMsg.Text="";
       
           // BindData4EditGrid();
        }
    }


    public void getEB()
    {
        EB = 0.0;
        NoOfLPY = 0.0;
       
       DataSet ds = DatabaseHelper.getDataset("Select NoOfLeavesPerYear , EBL from tbl_UsersLeaves where user_id ='"+ ddlEmpList.SelectedValue.ToString() +"' and WYear="+ddlYear.SelectedItem.Text);
       if (ds != null& ds.Tables.Count>0)
       {
           if (ds.Tables[0].Rows.Count > 0)
           {
               if (ds.Tables[0].Rows[0][0] != null && ds.Tables[0].Rows[0][0].ToString() != "")
                   NoOfLPY = Convert.ToDouble(ds.Tables[0].Rows[0][0].ToString());

               if (ds.Tables[0].Rows[0][1] != null && ds.Tables[0].Rows[0][1].ToString() != "")
                   EB = Convert.ToDouble(ds.Tables[0].Rows[0][1].ToString());

               txtEB.Text = EB.ToString();
               txtLPY.Text = NoOfLPY.ToString();
           }
           else
           {
               txtEB.Text = "0";
               txtLPY.Text = "0";
           }
       }

    }

    protected void btnSetEB_Click(object sender, EventArgs e)
    {

      int i =  objLeave.SetLeaveDetails4Users(Convert.ToInt32(Session["id"]), Convert.ToInt32(ddlYear.SelectedItem.Text), NoOfLPY,Convert.ToDouble(txtEB.Text), 1);
      if (i > 0)
          lblEBStatus.Text = " Operation is performed successfully.";
      else
          lblEBStatus.Text = "An error occured.";

    }
}
   


   
   
   

   
   
   
   
  

   
    


    

 
