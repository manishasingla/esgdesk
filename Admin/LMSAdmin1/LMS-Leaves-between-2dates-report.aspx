<%@ Page Language="C#" MasterPageFile="~/Admin/LMSAdmin/AdminMasterMenu.master"
 AutoEventWireup="true" CodeFile="LMS-Leaves-between-2dates-report.aspx.cs" Inherits="Admin_frmManageEmp" Title=" Employee absent between two dates leave details" %>
<%--<%@ Register Assembly="GMDatePicker" Namespace="GrayMatterSoft" TagPrefix="cc1" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  <script type="text/javascript" language="javascript">

function onlyNumbers(evt)
{
    var e = event || evt;
    var charCode = e.which || e.keyCode;
	if ((charCode > 47 && charCode < 58))
    return true;		              
    else
    return false ;		                            
}

function keypressHandler(evt) {

    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode == 13)
        var btn = document.getElementById('<%=btnSearch.ClientID%>');
    btn.focus();
    btn.click();
}
</script>
  <script language="javascript" type="text/javascript">
    function hideCalendar() {
        $find("<%=CalendarExtender1.ClientID%>").hide();
        $find("<%=CalendarExtender2.ClientID%>").hide();
        return false;
    } 
</script>
  <style>
.leavetbl td,th{ border:1px solid #999999;
height:25px;}

</style>
  <script>
  
  
        function OpenModelPopup() {
            //  document.getElementById('tdDisplayName').innerHTML = '';
            //  document.getElementById('txtName').value = '';
            //  alert("Called");
            document.getElementById('ModalPopupDiv').style.visibility = 'visible';
            document.getElementById('ModalPopupDiv').style.display = '';
            document.getElementById('ModalPopupDiv').style.top = Math.round((document.documentElement.clientHeight / 2) + document.documentElement.scrollTop) - 100 + 'px';
            document.getElementById('ModalPopupDiv').style.left = '200px';

            document.getElementById('MaskedDiv').style.display = '';
            document.getElementById('MaskedDiv').style.visibility = 'visible';
            document.getElementById('MaskedDiv').style.top = '0px';
            document.getElementById('MaskedDiv').style.left = '0px';
            document.getElementById('MaskedDiv').style.width = '100%'; // document.documentElement.clientWidth + 'px';
            document.getElementById('MaskedDiv').style.height = '1500px'; //document.documentElement.clientHeight + 'px';
        }


        function CloseModelPopup() {
            document.getElementById('MaskedDiv').style.display = 'none';
            document.getElementById('ModalPopupDiv').style.display = 'none';
        }

        function Submit() {
            //    if (document.getElementById('txtName').value != '') {
            document.getElementById('MaskedDiv').style.display = 'none';
            document.getElementById('ModalPopupDiv').style.display = 'none';
            // document.getElementById('tdDisplayName').innerHTML = '<H1> Hi ' + document.getElementById('txtName').value + ' !</H1>';
            //}
            //  else 
            // {
            //     alert('Please enter your name');
            //  }
        }
  
  </script>
  
  <div> </div>
  <div class="tab-content-holder">
    <h2 style="font-weight: normal !important; font-size: 30px !important; margin-top: 5px;
            margin-bottom: -5px; padding-left: 20px;"> LMS dashboard</h2>
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td width="11%" align="left" valign="top"><div class="treeview_div_left">
            <ul id="treemenu2" class="treeview" style="padding-left: 10px;">
              <li><a href="../LMSAdmin/LMS-Admin.aspx">LMS home</a></li>
              <li><a href="#">Manage</a>
                <ul rel="open" style="display: block;">
                  <li><a href="../LMSAdmin/LMS-Leave-setting.aspx" class="settings">Annum</a></li>
                  <li><a href="../LMSAdmin/LMS-Add-a-leave.aspx" class="edit_leave">Emps</a></li>
                </ul>
              </li>
              <li><a href="#">Reports</a>
                <ul  rel="open" style="display: block;">
                  <li><a href="../LMSAdmin/LMS-Leaves-between-2dates-report.aspx" class="documents">All</a></li>
                  <li><a href="../LMSAdmin/LMS-Complete-leaves-report.aspx" class="leave_report">Detail</a></li>
                  <li><a href="../LMSAdmin/LMS-In-and-outtime-report.aspx"
                            class="leave_report">In/outtime</a></li>
                </ul>
              </li>
            </ul>
          </div></td>
        <td width="78%" align="left" valign="top"><table width="100%">
            <tr>
              <td width="99%" valign="top"><div style="background-color: #f3f3f3; border: 1px solid #999999; margin-top: 17px; margin-right: 20px; line-height: 18px; padding-left: 20px; height: auto;">
                  <div>
                    <fieldset  style="height: auto; margin-top: 10px; margin-right:20px; margin-bottom:20px;">
                      <legend>
                      <asp:DropDownList ID="ddlEmpList" runat="server" AutoPostBack="True" onselectedindexchanged="ddlEmpList_SelectedIndexChanged" CausesValidation="true" ValidationGroup="g2" > </asp:DropDownList>
                      </legend>
                      <br />
                      <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                          <td width="18%" valign="top"><fieldset style="height:130px;">
                              <legend style="font-size: 16px; padding: 0 2px;"><strong>Custom</strong></legend>
                              <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                  <td height="25" align="left" valign="middle">Starting date</td>
                                </tr>
                                <tr>
                                  <td height="25" align="left" valign="middle"><asp:TextBox ID="GMDStartDate" runat="server" CssClass="input_boxline" style="width:176px;" CausesValidation="true"  ValidationGroup="g1"></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" OnClientDateSelectionChanged='hideCalendar'
                                     TargetControlID="GMDStartDate" Format="dd/MM/yyyy" > </ajaxToolkit:CalendarExtender></td>
                                </tr>
                                <tr>
                                  <td height="25" align="left" valign="middle">Ending date</td>
                                </tr>
                                <tr>
                                  <td height="25" align="left" valign="middle"><asp:TextBox ID="GMDEndDate" runat="server" CssClass="input_boxline" ontextchanged="GMDEndDate_TextChanged" AutoPostBack="True"  style="width:176px;"  CausesValidation="true" ValidationGroup="g1"> </asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="GMDEndDate" Format="dd/MM/yyyy" OnClientDateSelectionChanged='hideCalendar'> </ajaxToolkit:CalendarExtender></td>
                                </tr>
                                <tr>
                                  <td height="25" valign="middle">&nbsp;</td>
                                </tr>
                                <tr>
                                  <td height="25" valign="middle">&nbsp;</td>
                                </tr>
                              </table>
                            </fieldset></td>
                          <td width="82%" valign="top" style="color:#333333 !important; line-height:30px;"><div style="padding-left: 10px; margin-bottom: 20px;">
                              <fieldset style="height: auto; margin-top:-6px;">
                                <legend style="font-size: 16px; padding: 0 2px;"><strong>Employee absence details</strong></legend>
                                <div style=" height:631px; overflow-y:scroll; background:#f3f3f3;">
                                  <asp:GridView ID="gvEmp" runat="server" AutoGenerateColumns="False" Style="width: 100%; height:100%; background:#ffffff;" AllowSorting="True" 
                OnPageIndexChanging="gvEmp_PageIndexChanging" OnSorting="gvEmp_Sorting" 
                OnRowCommand="gvEmp_RowCommand" 
                HorizontalAlign="Left" 
                PageSize="20" onrowdatabound="gvEmp_RowDataBound" CssClass="leavetbl">
                                    <AlternatingRowStyle BackColor="#f7f7f7" />
                                    <Columns>
                                    <asp:TemplateField HeaderText="User name" SortExpression="UserName" Visible="false">
                                      <HeaderStyle Width="35%"/>
                                      <ItemTemplate>
                                        <asp:Label runat="server" ID="lblUserName" Text='<%#Eval("username")%>'></asp:Label>
                                      </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Employee name" SortExpression="EmpName" HeaderStyle-BackColor="#e1e1e1" >
                                      <HeaderStyle Width="35%"/>
                                      <ItemTemplate>
                                        <asp:LinkButton ID="lnkBtnCountryName" Text='<%#Eval("EmpName")%>' runat="server"  CommandArgument= '<%#string.Format("{0}",Eval("UserName"))%>' CommandName="DETAILS"></asp:LinkButton>
                                      </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Email address" SortExpression="Email" HeaderStyle-BackColor="#e1e1e1" >
                                      <ItemTemplate> <%#Eval("Email")%> </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Contact number" Visible="false">
                                      <ItemTemplate> <%#Eval("ContactNo")%> </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Absent Days" HeaderStyle-BackColor="#e1e1e1">
                                      <ItemTemplate>
                                        <asp:Label ID="lblTotalBalance" runat="server"></asp:Label>
                                      </ItemTemplate>
                                    </asp:TemplateField>
                                    </Columns>
                                  </asp:GridView>
                                </div>
                              </fieldset>
                              <asp:Label ID="Lbl_Pageinfo" runat="server" Style="position: relative"></asp:Label>
                            </div></td>
                        </tr>
                      </table>
                    </fieldset>
                  </div>
                </div></td>
            </tr>
          </table></td>
      </tr>
    </table>
    <script type="text/javascript">
            ddtreemenu.createTree("treemenu2", true, 5)

        </script>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableClientScript="true" ShowMessageBox="true" ShowSummary="false" ValidationGroup="g1"  DisplayMode="BulletList"  />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="g1"
                                Display="None" ErrorMessage="Please select a start date" ControlToValidate="GMDStartDate">* </asp:RequiredFieldValidator>
    <br/>
    <asp:CompareValidator ID="CompareValidator2" runat="server" Display="None"  ValidationGroup="g1"
                            ErrorMessage="Please enter a valid  date" Operator="DataTypeCheck" 
                            ControlToValidate="GMDStartDate" Type="Date">*</asp:CompareValidator>
    <br/>
    <asp:CompareValidator ID="CompareValidator3" runat="server" Display="None"   ValidationGroup="g1"
                            ErrorMessage="Please enter a valid date" Operator="DataTypeCheck" 
                            ControlToValidate="GMDEndDate" Type="Date">*</asp:CompareValidator>
    <br/>
    <asp:CompareValidator ID="CompareValidator1" runat="server" Display="None"  ValidationGroup="g1"
                                ErrorMessage="End date should be greater or equal to start date." 
                                ControlToCompare="GMDStartDate" ControlToValidate="GMDEndDate" 
                                Operator="GreaterThanEqual" Type="Date">*</asp:CompareValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please select an employee" 
                     ControlToValidate="ddlEmpList" ValidationGroup="g2" initialvalue="--Select--" Display="None" >*</asp:RequiredFieldValidator>
    <asp:ValidationSummary ID="ValidationSummary2" runat="server" EnableClientScript="true" ShowMessageBox="true" ShowSummary="false" ValidationGroup="g2"  DisplayMode="BulletList"  />
  </div>
  <div id="MaskedDiv" class="MaskedDiv"> </div>
  <div id="ModalPopupDiv" class="ModalPopup" style="top: 240px; left: 42%;">
  
    <table style="width:100%; padding:10px; height:373" class="publicloginTable" onkeypress="javascript:keypressHandler(event)">
      <tr style="display:none">
        <td><asp:TextBox ID="txtPageSize" OnKeypress="return onlyNumbers(event)" runat="server" CssClass="input_boxline" Width="50px" Visible="false"></asp:TextBox></td>
        <td style="width: 60%; left:10px;  position: absolute; top: 320px; "><asp:Button ID="btnSearch" runat="server" CssClass="btn005" Text="Search" visible="false"
        Width="55px" Style="position: relative; top: 0px; left: 0px;"  OnClick="btnSearch_Click" /></td>
      </tr>
      <tr>
        <td colspan="14" align="right"><asp:Button ID="Button1" runat="server" 
            CssClass="btn005" Text="X"
        Width="55px" Style="position: relative; top: 0px; left: 0px;" 
          onclick="Button1_Click" /></td>
          </td>
      </tr>
      <tr>
        <td colspan="7" valign="top"></td>
        <td colspan="6"><asp:Panel ID="pnlAbsentDetails" runat="server" style="margin-left:10px;margin-right:20px" Height="100%"  Visible="False" GroupingText="Absent details"   Direction="LeftToRight" HorizontalAlign="Left" ToolTip="Absent Details">
            <table align="center" cellpadding="0" cellspacing="0" style="height: 100%; width:100%; margin-top:10px;">
              <tr style="height:25px">
                <td width="15%" valign="top"><asp:Label ID="lblEmpName" runat="server"  Font-Size="14px">Employee name :</asp:Label></td>
                <td width="85%" valign="top"><asp:Label ID="lblEmpNameValue" runat="server" Font-Size="14px"></asp:Label></td>
              </tr>
              <tr valign="top" style="height:25px">
                <td valign="top"><asp:Label ID="lblLeaveDays" runat="server"  Font-Size="14px" >Absent Days :</asp:Label></td>
                <td valign="top"><asp:Label ID="lblLeaveDaysValue" runat="server" Font-Size="14px" 
                                                ForeColor="Red"></asp:Label></td>
              </tr>
              <tr>
                <td colspan="2"  class="leavetbl"><asp:GridView ID="gvAbsentDates" runat="server" AllowPaging="True"  AutoGenerateColumns="false"
               Height="100%" PageSize="20" style="margin-top: 9px" Width="100%" onpageindexchanging="gvAbsentDates_PageIndexChanging" OnSorting="gvAbsentDates_Sorting" 
               onrowdatabound="gvAbsentDates_RowDataBound" AllowSorting="true">
               
                    <Columns>
                    <asp:TemplateField HeaderText="AbsentDate" SortExpression="AbsentDate">
                    <HeaderStyle Width="12%" Height="35px" BackColor="#e1e1e1" HorizontalAlign="Center"/>
                     <ItemTemplate>
                        <asp:Label runat="server" ID="lblAbsentDate" Text='<%#Eval("AbsentDate")%>'></asp:Label>
                      </ItemTemplate>
                      </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Reason" SortExpression="Reason">
                      <HeaderStyle Width="42%" Height="35px" BackColor="#e1e1e1" HorizontalAlign="Center"/>
                      <ItemTemplate>
                        <asp:Label runat="server" ID="lblAbsentDate" Text='<%#Eval("Reason")%>'></asp:Label>
                      </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Paid/Non paid" SortExpression="Paid/Nonpaid">
                    <HeaderStyle Width="12%" Height="35px" BackColor="#e1e1e1" HorizontalAlign="Center"/>
                      <ItemTemplate>
                        <asp:Label runat="server" ID="lblAbsentDate" Text='<%#Eval("PaidOrNonpaid")%>'></asp:Label>
                      </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Leave type" SortExpression="LeaveType">
                    <HeaderStyle Width="9%" Height="35px" BackColor="#e1e1e1" HorizontalAlign="Center"/>
                      <ItemTemplate>
                        <asp:Label runat="server" ID="lblAbsentDate"  Text='<%#Eval("LeaveType")%>'></asp:Label>
                      </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Is HPL?" SortExpression="IsEHPL">
                    <HeaderStyle Width="6%" Height="35px" BackColor="#e1e1e1" HorizontalAlign="Center"/>
                      <HeaderStyle  />
                      <ItemTemplate>
                        <asp:Label runat="server" ID="lblIsHPL" Text='<%#Eval("IsEHPL")%>'></asp:Label>
                      </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Informed ?" SortExpression="IsPlanned">
                    <HeaderStyle Width="9%" Height="35px" BackColor="#e1e1e1" HorizontalAlign="Center"/>
                      <ItemTemplate>
                        <asp:Label runat="server" ID="lblIsInformed" Text='<%#Eval("IsPlanned")%>'></asp:Label>
                      </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Deductable" SortExpression="Deductable">
                    <HeaderStyle Width="10%" Height="35px" BackColor="#e1e1e1" HorizontalAlign="Center"/>
                      <ItemTemplate>
                        <asp:Label runat="server" ID="lblDeductable" Text='<%#Eval("Deductable")%>' style='text-align:center'></asp:Label>
                      </ItemTemplate>
                    </asp:TemplateField>
                    
                    </Columns>
                  </asp:GridView></td>
              </tr>
            </table>
            </asp:Panel></td>
      </tr>
      <tr>
        <td colspan="7"><asp:Label ID="lblError" runat="server"  Visible="false" CssClass="lblerror"></asp:Label></td>
        <td align="right" colspan="7">&nbsp;</td>
      </tr>
    </table>
  </div>
  <style type="text/css">
        div.MaskedDiv
        {
            visibility: hidden;
            position: fixed;
            left: 0px;
            top: 0px;
            font-family: verdana;
            font-weight: bold;
            padding: 40px;
            z-index: 100; /* background-image:url(Mask.png); */ /* ieWin only stuff */ /* _background-image:none; */
            background-color: #333333;
            opacity: 0.7;
            filter: alpha(opacity=70); /*_filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled=true, sizingMethod=scale src='Mask.png'); */
        }
        
        
        div.ModalPopup
        {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 11px;
            font-style: normal;
            background-color: #F3F3F3;
            position: absolute; /* set z-index higher than possible */
            z-index: 10000;
            visibility: hidden;
            color: Black;
            border-style: solid;
            border-color: #999999;
            border-width: 1px;
            width: 1000px;
            height: auto;
            left: 42%;
        }
        </style>
</asp:Content>
