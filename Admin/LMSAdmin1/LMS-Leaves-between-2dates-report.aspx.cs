using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Admin_frmManageEmp : System.Web.UI.Page
{
    clsEmployee_Logic1 objEmployee = new clsEmployee_Logic1();
    clsLeave_Logic objLeave = new clsLeave_Logic();
    private string strError = "No records found.";
    DataSet dsAbsentDates = new DataSet();
    DataView dv = null;
    string sortByAndArrangeBy = "";
    double SdblUsed = 0.0;
    string dtSDate;
    string dtEDate;
    DateTime datetime;
    DataSet dsTemp;
    DataView dvAbsentDates; 
    DataView dvEmpList;

    string strSortToEmpList;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["admin"] != null && Session["admin"].ToString() != "")
        {

            if (DatabaseHelper.isAdmin(Session["admin"].ToString())==false)
            {
                Response.Redirect("~/Admin/login.aspx");
                return;
            }
        }
        if (!Page.IsPostBack)
        {
          //  this.txtPageSize.Text = "10";
            BindData();
            fnBindddl();
        }
    }

    void BindData()
    {
        try
        {
      
        DateTime dt1, dt2;
       // objEmployee.EmpName = ddlEmpList.SelectedItem.Text;// txtEmpName.Text.Trim();
        objEmployee.Sort_On = "";
        string strSortbyOrder = "";

        if (ViewState["Sort_On"] != null)
        {
            objEmployee.Sort_On = ViewState["Sort_On"].ToString() + " " + ViewState["Sort_By"].ToString();
            strSortbyOrder = objEmployee.Sort_On;
        }

        DateTime d = DateTime.Now;

        if (GMDStartDate.Text.Trim() == "")
        {
            dtSDate = FirstDayOfYear(d).ToString("dd/MM/yyyy");
            GMDStartDate.Text = dtSDate;
            dt1 = Convert.ToDateTime(dtSDate);
            dtSDate = dt1.ToString("yyyy/MM/dd");
        }
        else
        {
            dt1 = Convert.ToDateTime(GMDStartDate.Text);
            dtSDate = dt1.ToString("yyyy/MM/dd");
            objLeave.StartingDate = dtSDate;
        }

        if (GMDEndDate.Text.Trim() == "")
        {
            dtEDate = LastDayOfYear(d).ToString("dd/MM/yyyy");
            GMDEndDate.Text = dtEDate;
            dt2 = Convert.ToDateTime(dtEDate);
            dtEDate = dt2.ToString("yyyy/MM/dd");
        }
        else
        {
            dt2 = Convert.ToDateTime(GMDEndDate.Text);
            dtEDate = dt2.ToString("yyyy/MM/dd");
         
        }


        lblError.Visible = false;
        dsTemp = objEmployee.GetEmployeeAbsentBetween2Dates(dtSDate, dtEDate);

        if (dsTemp != null && dsTemp.Tables.Count > 0 && dsTemp.Tables[0].Rows.Count > 0)
        {
            DataTable dtTemp = dsTemp.Tables[0];

            dvEmpList = new DataView(dtTemp);

            Session["dvEmpList"] = dvEmpList;

            dvEmpList.Sort = strSortbyOrder;

            if (ddlEmpList.SelectedItem != null && ddlEmpList.SelectedItem.Text!="--Select--")
            {
                string strFilter = "EmpName Like'%" + ddlEmpList.SelectedItem.Text.Trim() + "%'";
                dvEmpList.RowFilter = strFilter;
            }

            if (dvEmpList != null)
            {
                if (dvEmpList.ToTable().Rows.Count > 0)
                {
                    lblError.Visible = false;

                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = strError;

                }

                if (this.txtPageSize.Text != "")
                {
                    if (System.Convert.ToInt32(this.txtPageSize.Text) > 0)
                    {
                        this.gvEmp.PageSize = System.Convert.ToInt32(this.txtPageSize.Text);
                    }
                }
                gvEmp.Visible = true;
                gvEmp.DataSource = dvEmpList;
                gvEmp.DataBind();
            }
        }
        else
        {
            Lbl_Pageinfo.Visible = false;
            gvEmp.Visible = false;
            lblError.Visible = true;
            lblError.Text = strError;
        }
        if (dvEmpList != null)
        {
            if (dvEmpList.ToTable().Rows.Count <= 0)
            {
                this.Lbl_Pageinfo.Visible = false;
            }

            else
            {
                Int16 intTo;
                Int16 intFrom;
                if (gvEmp.PageSize * (gvEmp.PageIndex + 1) < dvEmpList.ToTable().Rows.Count)
                {
                    intTo = System.Convert.ToInt16(gvEmp.PageSize * (gvEmp.PageIndex + 1));
                }
                else
                {
                    intTo = System.Convert.ToInt16(dvEmpList.ToTable().Rows.Count);
                }
                intFrom = System.Convert.ToInt16((gvEmp.PageSize * gvEmp.PageIndex) + 1);
                this.Lbl_Pageinfo.Text = "Record(s) " + intFrom + " to " + intTo + " of " + dvEmpList.ToTable().Rows.Count;
                this.Lbl_Pageinfo.Visible = true;
            }
        }
    }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Leaves-between-2dates-report : BindData()");
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            strError = "No records found.";
            gvEmp.PageIndex = 0;
            string InfoArray = ddlEmpList.SelectedItem.Text.Trim()+"|"+GMDStartDate.Text+ "|"+ GMDEndDate.Text;
            pnlAbsentDetails.Visible = false;
            BindData();
            lblError.Visible = false;
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Leaves-between-2dates-report : btnSearch_Click event");
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }

    }

    protected void gvEmp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            if (Session["dvEmpList"] != null)
            {
                if (Session["dvEmpList"].ToString() != "")
                {
                    dvEmpList = (DataView)Session["EmpList"] as DataView;
                    gvEmp.PageIndex = e.NewPageIndex;
                    gvEmp.DataSource = dvEmpList;
                    gvEmp.DataBind();
                    lblError.Visible = false;
                }
                else
                {
                    BindData();
                }
            }
            else
            {
                BindData();
            }
       }
         catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Leaves-between-2dates-report : gvEmp_PageIndexChanging event");
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }
    }

    protected void gvEmp_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName.ToUpper() == "DETAILS")
            {
                if (GMDStartDate.Text != "" || GMDEndDate.Text != "")
                {
                    if (GMDEndDate.Text.Trim() == "" || GMDEndDate.Text == null)
                    {
                        GMDEndDate.Text = GMDStartDate.Text;
                    }
                    if (GMDStartDate.Text.Trim() == "" || GMDStartDate.Text == null)
                    {
                        GMDStartDate.Text = GMDEndDate.Text;
                    }
                    sortByAndArrangeBy = (e.CommandArgument).ToString();
                    fnFillLeaveDetails(GMDStartDate.Text, GMDEndDate.Text, sortByAndArrangeBy);
                    objLeave.UserName = sortByAndArrangeBy;
          
                    datetime = Convert.ToDateTime(GMDStartDate.Text);
                    objLeave.StartingDate = datetime.ToString("yyyy/MM/dd");  //ViewState["SD"].ToString();

                    if (GMDEndDate.Text.Trim() != null)
                    {
                        datetime = Convert.ToDateTime(GMDEndDate.Text);
                        objLeave.EndingDate = datetime.ToString("yyyy/MM/dd");
                    }

                    objLeave.GetTotalBalanceDaysInOneBWDate(DateTime.Now.Year);
                    SdblUsed = objLeave.CasualBalanceLeaves + objLeave.MedicalBalanceLeaves + objLeave.EarnBalanceLeaves + objLeave.HalfPaidBalanceLeaves + objLeave.ExtraBalanceLeaves;
                    lblLeaveDaysValue.Text = SdblUsed.ToString();
                    Page.ClientScript.RegisterStartupScript(this.GetType(),"Details", "OpenModelPopup();", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "hwa", "alert('Please select a Starting Date and Ending Date');", true);
                }

            }

        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Leaves-between-2dates-report : gvEmp_RowCommand event");
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }
    }

    protected void gvEmp_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            objEmployee.Sort_On = e.SortExpression;
            ViewState["Sort_On"] = objEmployee.Sort_On;
            if (ViewState["Sort_By"] == null)
                ViewState["Sort_By"] = "Asc";
            if (ViewState["Sort_By"].ToString() == "Asc")
            {
                ViewState["Sort_By"] = "Desc";
            }
            else
            {
                ViewState["Sort_By"] = "Asc";
            }

            BindData();
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Leaves-between-2dates-report : gvEmp_Sorting event");
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }
    }

    protected void gvAbsentDates_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            if (Session["dvAbsentDates"] != null)
            {
                if (Session["dvAbsentDates"].ToString() != "")
                {
                    dvAbsentDates = (DataView)Session["dvAbsentDates"] as DataView;
                    gvAbsentDates.PageIndex = e.NewPageIndex;
                    gvAbsentDates.DataSource = dvAbsentDates;// dsAbsentDates;
                    gvAbsentDates.DataBind();
                    lblError.Visible = false;
                }
                else
                {
                    BindData();
                }
            }
            else
            {
                BindData();
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Leaves-between-2dates-report : gvAbsentDates_PageIndexChanging event");
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }

    }

    protected void gvAbsentDates_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        string strIsEHPL, strIsPOUP, strIsDOND;


        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //if (e.Row.RowType == DataControlRowType.DataRow)
                //{
                    datetime = Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "AbsentDate"));
                    e.Row.Cells[0].Text = datetime.ToString("dd/MM/yyyy");
                    e.Row.Cells[1].Text = DataBinder.Eval(e.Row.DataItem, "Reason").ToString().Replace("@", "'").Trim();

                    strIsEHPL = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "IsEHPL"));
                    strIsPOUP = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "IsPlanned"));
                    strIsDOND = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Deductable"));

                    if (strIsEHPL == "1")
                    {
                        e.Row.Cells[4].Text = "Yes";
                    }
                    else
                    {
                        e.Row.Cells[4].Text = "No";
                    }

                    if (strIsDOND == "True")
                    {
                        e.Row.Cells[6].Text = "Yes";
                    }
                    else
                    {
                        e.Row.Cells[6].Text = "No";
                    }


                    if (strIsPOUP == "True")
                    {
                        e.Row.Cells[5].Text = "Yes";
                    }
                    else
                    {
                        e.Row.Cells[5].Text = "No";
                    }
              //  }
              
            }
            BindData();
            lblError.Visible = false;
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Leaves-between-2dates-report : gvAbsentDates_RowDataBound event");
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }


    }

    static DateTime LastDayOfYear()
    {
        return LastDayOfYear(DateTime.Today);
    }

    static DateTime LastDayOfYear(DateTime d)
    {
        // 1
        // Get first of next year
        DateTime n = new DateTime(d.Year + 1, 1, 1);
        // 2
        // Subtract 1 from it
        return n.AddDays(-1);
    }

    static DateTime FirstDayOfYear()
    {
        return FirstDayOfYear(DateTime.Today);
    }

    static DateTime FirstDayOfYear(DateTime y)
    {
        return new DateTime(y.Year, 1, 1);
    }
  
    public void fnFillLeaveDetails(string StartDate, string EndDate, string strInfo)
    {
        try
        {
            DateTime dt2;
            string strDate2 = "";
            double dblUsed = 0.0;
            string strTotal = "";
            double dblTotal = 0.0;
            double dblTemp = 0.0;

            //region after using Ajax Calender Extender
            datetime = Convert.ToDateTime(StartDate);
            string strDate1 = datetime.ToString("yyyy/MM/dd");
            if (EndDate.Trim() != "")
            {
                dt2 = Convert.ToDateTime(EndDate);
            }
            else
            {
                dt2 = datetime;
            }
            strDate2 = dt2.ToString("yyyy/MM/dd");
            sortByAndArrangeBy = strInfo;

            char[] separator = { '|' };

            string[] sortByAndArrangeByArray = sortByAndArrangeBy.Split(separator);
            objLeave.UserName = sortByAndArrangeByArray[0];

            string strSql1 = "select count(*) from tbl_AbsentDetails where UserName='" + sortByAndArrangeByArray[0] + "' and AbsentDate >='" + strDate1 + "'"
                             + " and AbsentDate <='" + strDate2 + "' ";

            string strSql2 = "select cast(AbsentDate as DateTime) as AbsentDate ,Reason,PaidOrNPaid as PaidOrNonpaid, LeaveType ,IsEHPL, IsPlanned , Deductable  from tbl_AbsentDetails where UserName='" + sortByAndArrangeByArray[0] + "' and AbsentDate >='" + strDate1 + "'"
                             + " and AbsentDate <='" + strDate2 + "' order by AbsentDate desc ";


            clsLeave_Logic objclsll = new clsLeave_Logic();

            // int intAbsentDays = objclsll.getNoOfDaysInMonth(strSql1);
            objLeave.UserName = sortByAndArrangeByArray[0];

            objLeave.GetTotalBalanceDaysInOne(DateTime.Now.Year);

            // returns remaining balance and set seperately each category's used leaves
            dblUsed = objLeave.CasualBalanceLeaves + objLeave.MedicalBalanceLeaves + objLeave.EarnBalanceLeaves + objLeave.HalfPaidBalanceLeaves;

            strTotal = objLeave.fnGetTotalOfAllotedLeavesByEmp(DateTime.Now.Year);//fnGetTotalOfAllotedLeavesByEmp() Return total of leaves alloted to employee.

            dblTotal = Convert.ToDouble(strTotal);
            // e.Row.Cells[4].Text = objLeave.fnGetTotalOfBalanceLeave(DateTime.Now.Year) + " / " + strTotal;//fnGetTotalOfBalanceLeave() return total of leaves taken by employee.
            if (objLeave.ExtraBalanceLeaves == 0.0)
                dblUsed = objLeave.CasualBalanceLeaves + objLeave.MedicalBalanceLeaves;
            else
                if (SdblUsed <= dblTotal)
                {
                    dblTemp = dblTotal - dblUsed;
                    dblUsed = (dblTemp - objLeave.ExtraBalanceLeaves);
                }
                else
                {
                    dblUsed = -(objLeave.ExtraBalanceLeaves);
                }

            clsEmployee_Logic1 objclsel = new clsEmployee_Logic1();

              DataSet dsEd = objclsel.GetEmployee(sortByAndArrangeByArray[0]);

              dsAbsentDates = objclsel.GetAbsentDates(strSql2);

                if (dsAbsentDates != null && dsAbsentDates.Tables.Count > 0)
                {
                   if (dsAbsentDates.Tables[0].Rows.Count > 0)
                   {
                    dvAbsentDates = new DataView(dsAbsentDates.Tables[0]);

                    Session["dvAbsentDates"] = dvAbsentDates;

                    gvAbsentDates.DataSource = dsAbsentDates.Tables[0].DefaultView;

                    gvAbsentDates.DataBind();

                    if (dsEd != null)
                    {
                        if (dsEd.Tables.Count > 0)
                        {
                            if (dsEd.Tables[0].Rows.Count > 0)
                            {
                                string strEmpName = dsEd.Tables[0].Rows[0]["EmpName"].ToString();

                                lblEmpNameValue.Text = strEmpName;
                            }
                        }
                    }
                    pnlAbsentDetails.Visible = true;
                }
            }
            else
            {
                pnlAbsentDetails.Visible = false;
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Leaves-between-2dates-report : fnFillLeaveDetails method");
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }
    }

    protected void gvEmp_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            Label lblUSerNamel = ((Label)e.Row.FindControl("lblUserName"));
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                objLeave.StartingDate = dtSDate; 
                objLeave.EndingDate = dtEDate;
                objLeave.UserName = lblUSerNamel.Text;
              
                objLeave.GetTotalBalanceDaysInOneBWDate(DateTime.UtcNow.Year);
                SdblUsed = objLeave.CasualBalanceLeaves + objLeave.MedicalBalanceLeaves + objLeave.EarnBalanceLeaves + objLeave.HalfPaidBalanceLeaves + objLeave.ExtraBalanceLeaves;
               
                e.Row.Cells[4].Text = SdblUsed.ToString();
                lblError.Visible = false;
            }

        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Leaves-between-2dates-report : gvEmp_RowDataBound event");
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }
    }

    protected void ddlEmpList_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            RequiredFieldValidator2.Validate();
            pnlAbsentDetails.Visible = false;

            if (ddlEmpList.SelectedItem.Text != "--Select--")
            {
                strError = "No records found.";
                gvEmp.PageIndex = 0;
                string InfoArray = ddlEmpList.SelectedItem.Text.Trim() + "|" + GMDStartDate.Text + "|" + GMDEndDate.Text;
                BindData();
                lblError.Visible = false;
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Leaves-between-2dates-report : ddlEmpList_SelectedIndexChanged event");
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }
    }

    public void fnBindddl()
    {
        try
        {
            if (dsTemp != null)
            {
                ddlEmpList.DataSource = dsTemp;
                ddlEmpList.DataTextField = "EmpName";
                ddlEmpList.DataValueField = "EmpName";
                ddlEmpList.DataBind();
                ddlEmpList.Items.Insert(0, "--Select--");
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Leaves-between-2dates-report : fnBindddl()");
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }

    }

    protected void GMDEndDate_TextChanged(object sender, EventArgs e)
     {
         try
         {
             CompareValidator1.Validate();
             strError = "No records found.";
             gvEmp.PageIndex = 0;
             string InfoArray = ddlEmpList.SelectedItem.Text.Trim() + "|" + GMDStartDate.Text + "|" + GMDEndDate.Text;
             pnlAbsentDetails.Visible = false;
             BindData();
             lblError.Visible = false;
         }
         catch (Exception ex)
         {
             DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Leaves-between-2dates-report : GMDEndDate_TextChanged event");
             lblError.Text = ex.Message;
             lblError.Visible = true;
         }

     }

    protected void gvAbsentDates_Sorting(object sender, GridViewSortEventArgs e)
     {
         string strSortbyOrder = "";
         try
         {
             objEmployee.Sort_On = e.SortExpression;
             ViewState["Sort_On"] = objEmployee.Sort_On;

             if (ViewState["Sort_By"] == null)
                 ViewState["Sort_By"] = "Asc";
             if (ViewState["Sort_By"].ToString() == "Asc")
             {
                 ViewState["Sort_By"] = "Desc";
             }
             else
             {
                 ViewState["Sort_By"] = "Asc";
             }
             
            if (ViewState["Sort_On"] != null)
            {
                objEmployee.Sort_On = ViewState["Sort_On"].ToString() + " " + ViewState["Sort_By"].ToString();
                strSortbyOrder = objEmployee.Sort_On;
            }

            if (Session["dvAbsentDates"] != null)
            {
                if (Session["dvAbsentDates"].ToString() != "")
                {
                    dvAbsentDates = (DataView)Session["dvAbsentDates"] as DataView;
                    dvAbsentDates.Sort = strSortbyOrder;
                    gvAbsentDates.DataSource = dvAbsentDates;
                    gvAbsentDates.DataBind();
                }
                else
                {
                    BindData();
                }
            }
            if (Session["dvEmpList"] != null)
            {
                if (Session["dvEmpList"].ToString() != "")
                {
                    dvEmpList = (DataView)Session["dvEmpList"] as DataView;
                    gvEmp.Visible = true;
                    gvEmp.DataSource = dvEmpList;
                    gvEmp.DataBind();
                }
                else
                {
                    BindData();
                }
            }
            else
            {
                BindData();
            }
         }
         catch (Exception ex)
         {
             DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "LMS-Leaves-between-2dates-report : gvAbsentDates_Sorting event");
             lblError.Text = ex.Message;
             lblError.Visible = true;
         }
     }

    protected void Button1_Click(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(GetType(), "Close", "Submit()", true);
        BindData();
    }
}
