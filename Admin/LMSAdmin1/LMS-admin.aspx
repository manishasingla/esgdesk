<%@ Page Language="C#" MasterPageFile="~/Admin/LMSAdmin/AdminMasterMenu.master" AutoEventWireup="true"
    CodeFile="LMS-Admin.aspx.cs" Inherits="Admin_frmAdminHome" Title="Welcome to Leave Management System" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">



<script language="javascript" type="text/javascript">
    function hideCalendar() {
        $find("<%=CalendarExtender1.ClientID%>").hide();
        $find("<%=CalendarExtender2.ClientID%>").hide();
        return false;
    } 
</script>


  <div class="tab-content-holder">
    <h2 style="font-weight:normal !important; font-size:30px !important; margin-top:5px; margin-bottom:-5px; padding-left:20px;">LMS dashboard</h2>
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td width="11%" align="left" valign="top"><div class="treeview_div_left" >
            <ul id="treemenu2" class="treeview" style="padding-left:10px;">
              <li><a href="../LMSAdmin/LMS-Admin.aspx">LMS home</a></li>
              <li><a href="#" >Manage</a>
                <ul rel="open" style="display: block;">
                  <li><a href="../LMSAdmin/LMS-Leave-setting.aspx" class="settings">Annum</a></li>
                  <li><a href="../LMSAdmin/LMS-Add-a-leave.aspx" class="edit_leave">Emps</a></li>
                </ul>
              </li>
              <li><a href="#" >Reports</a>
                <ul rel="open" style="display: block;">
                  <li><a href="../LMSAdmin/LMS-Leaves-between-2dates-report.aspx"
                            class="documents">All</a></li>
                  <li><a href="../LMSAdmin/LMS-Complete-leaves-report.aspx"
                            class="leave_report">Detail</a></li>
   <li><a href="../LMSAdmin/LMS-In-and-outtime-report.aspx"
                            class="leave_report">In/outtime</a></li>
                </ul>
              </li>
            </ul>
          </div></td>
        <td width="80%" align="left" valign="top"><table width="100%">
            <tr>
              <td width="99%" valign="top"><div style="background-color:#f3f3f3; border:1px solid #999999; margin-top:17px; margin-right:20px; line-height:25px; padding-left:20px; height:800px;">
                  <table width="98%" cellpadding="0" cellspacing="0" border="0">
                    <tr valign="top">
                      <td width="18%"><div>
                          <fieldset style="height:180px; margin-top:10px;">
                            <legend style="font-size:16px; padding:0 2px;"> <strong>Custom</strong></legend>
                            <table width="98%" cellpadding="0" cellspacing="0" border="0" align="center">
                              <tr>
                                <td height="25" align="left" valign="middle" style="padding-left:5px;">Select interval</td>
                              </tr>
                              <tr>
                                <td align="left"  style="padding-left:5px;"><asp:DropDownList ID="ddlTimeInterval" runat="server" valign="top" 
                        Height="25px" onselectedindexchanged="ddlTimeInterval_SelectedIndexChanged" 
                        Width="176px" AutoPostBack="True" >
                                    <asp:ListItem Value="1">Today</asp:ListItem>
                                    <asp:ListItem Value="2">This week</asp:ListItem>
                                    <asp:ListItem Value="3">Last 7 days</asp:ListItem>
                                    <asp:ListItem Value="4">This month</asp:ListItem>
                                    <asp:ListItem Value="5">Last 31 days</asp:ListItem>
                                  </asp:DropDownList></td>
                              </tr>
                              <tr>
                                <td height="25" align="left" valign="middle"  style="padding-left:5px;">Select dates from</td>
                              </tr>
                              <tr>
                                <td height="25" align="left"  style="padding-left:5px;">
                                <asp:TextBox ID="GMDStartDate" runat="server" CssClass="input_boxline" style="width:172px;" CausesValidation="True" ValidationGroup="g1" ></asp:TextBox>
                            <%--    <img id="Image1" runat="server" alt="Date" src="~/Admin/LMSAdmin/images/cs.png"/>--%>
                              <%--    <asp:ImageButton runat="Server" ID="Image1" ImageUrl="~/Admin/LMSAdmin/images/cs.png" AlternateText="Click to show calendar"/> --%>
                                  <ajaxtoolkit:calendarextender ID="CalendarExtender1"  runat="server"  TargetControlID="GMDStartDate" OnClientDateSelectionChanged='hideCalendar'
                                  Format="dd/MM/yyyy"> </ajaxtoolkit:calendarextender></td>
                              </tr>
                              <tr>
                                <td align="left"  style="padding-left:5px;">To</td>
                              </tr>
                              <tr>
                                <td height="25" align="left"  style="padding-left:5px;"><asp:TextBox ID="GMDEndDate" runat="server" CssClass="input_boxline" 
              AutoPostBack="True" ontextchanged="GMDEndDate_TextChanged" style="width:172px;"  CausesValidation="True" ValidationGroup="g1" ></asp:TextBox>
                    <%-- <asp:ImageButton runat="Server" ID="Image2" ImageUrl="~/Admin/LMSAdmin/images/cs.png" AlternateText="Click to show calendar"    OnClientClick="return false" /> --%>
                   <%-- <img id="Image2" runat="server" alt="Date" src="~/Admin/LMSAdmin/images/cs.png" />--%>
                                  <ajaxToolkit:CalendarExtender ID="CalendarExtender2"  runat="server"  TargetControlID="GMDEndDate" OnClientDateSelectionChanged='hideCalendar'
                                   Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender></td>
                              </tr>
                              <tr>
                                <td align="left">
                               </td>
                              </tr>
                            </table>
                          </fieldset>
                        </div></td>
                      <td width="82%"><div style="padding-left:10px; margin-bottom:20px; ">
                          <fieldset style="height:auto; margin-top:10px;">
                            <legend style="font-size:16px; padding:0 2px;"><strong>At a glance</strong></legend>                              
                            <div style="height:725px; overflow-y:scroll;">
                            <table style=" color: #333333; font-size: 26px;" width="100%">
                                <tr valign="top">
                                  <td Width="50%" ><asp:Label ID="lblLR4R1Title" runat="server" 
                                          Font-Underline="True" ></asp:Label><br /><br />
                                          <asp:Repeater ID="rptAbsentEmpByWeek" runat="server" OnItemDataBound="rptAbsentEmpByWeek_ItemDataBound">
                                      <ItemTemplate>
                                        <table style="font-size: 15px" width="100%">
                                          <tr>
                                            <td colspan="2" style="font-size: 20px"><b> <%# String.Format("{0:D}",(Container.DataItem)) %> (<%# Convert.ToDateTime(Container.DataItem).DayOfWeek %>)</b></td>
                                          </tr>
                                          <tr>
                                            <td colspan="2"><asp:Repeater ID="Repeater2" runat="server">
                                                <FooterTemplate> <%="</ul>" %> </FooterTemplate>
                                                <HeaderTemplate>
                                                  <table style="font-size: 12px" width="100%">
                                                  </table>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                  <table style="font-size: 12px" width="100%">
                                                    <tr>
                                                      <td width="15%" valign="top"><%#((System.Data.DataRow)Container.DataItem)[0] %></td>
                                                      <td width="2%" valign="top"> : </td>
                                                      <td width="66%" valign="top"><%# ((System.Data.DataRow)Container.DataItem)[2]%></td>
                                                    </tr>
                                                  </table>
                                                </ItemTemplate>
                                              </asp:Repeater></td>
                                          </tr>
                                        </table>
                                      </ItemTemplate>
                                    </asp:Repeater>
                                       <span style="font-size:12px"> <asp:Label ID="lbl_pageinfo" runat="server" Text="" Visible="false" ></asp:Label></span> <br />

                                    </td>

                                 <%--upcoming 3 week's leaves--%>
                                    <td > <asp:Label ID="lblLR4U3WTitle" runat="server" 
                                            Text="Upcoming 3 week's leave report" Font-Underline="True"  ></asp:Label><br /><br />

                                    <asp:Repeater ID="rptLR4U3W" runat="server" 
                                            onitemdatabound="rptLR4U3W_ItemDataBound">
                                      <ItemTemplate>
                                        <table style="font-size: 15px" width="100%">
                                          <tr>
                                            <td colspan="2" style="font-size: 20px"><b> <%# String.Format("{0:D}",(Container.DataItem)) %> (<%# Convert.ToDateTime(Container.DataItem).DayOfWeek %>)</b></td>
                                          </tr>
                                          <tr>
                                            <td colspan="2"><asp:Repeater ID="Repeater3" runat="server">
                                                <FooterTemplate> <%="</ul>" %> </FooterTemplate>
                                                <HeaderTemplate>
                                                  <table style="font-size: 12px" width="100%">
                                                  </table>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                  <table style="font-size: 12px" width="100%">
                                                    <tr>
                                                      <td width="15%" valign="top"><%#((System.Data.DataRow)Container.DataItem)[0] %></td>
                                                      <td width="2%" valign="top"> : </td>
                                                      <td width="66%" valign="top"><%# ((System.Data.DataRow)Container.DataItem)[2]%></td>
                                                    </tr>
                                                  </table>
                                                </ItemTemplate>
                                              </asp:Repeater></td>
                                          </tr>
                                        </table>
                                      </ItemTemplate>
                                    </asp:Repeater>
                                    <span style="font-size:12px"> <asp:Label ID="lblLR4U3W" runat="server" Text="" Visible="false" ></asp:Label></span>
                                    </td>
                                        <%--upcoming 3 week's leaves--%>
                                </tr>
                              
                            </table></div>
                          </fieldset>
                        </div></td>
                    </tr>
                  </table>
                </div></td>
            </tr>
          </table></td>
      </tr>
    </table>
    <script type="text/javascript">
        ddtreemenu.createTree("treemenu2", true, 5)

        validatePage = function (button)
         {
            var isValid = Page_ClientValidate();
            button.disabled = isValid;

            //return validation - if true then postback
            return isValid;
        }
</script> 
 <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableClientScript="true" ShowMessageBox="true" ShowSummary="false" ValidationGroup="g1"  DisplayMode="BulletList"  />
  <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="g1"
                                Display="None" ErrorMessage="Please select a start date" ControlToValidate="GMDStartDate">*
                                 </asp:RequiredFieldValidator>
                                  <br/>
                                  <asp:CompareValidator ID="CompareValidator2" runat="server" Display="None"  ValidationGroup="g1"
                            ErrorMessage="Please enter a valid  date" Operator="DataTypeCheck" 
                            ControlToValidate="GMDStartDate" Type="Date">*</asp:CompareValidator>
                                  <br/>
                                  <asp:CompareValidator ID="CompareValidator3" runat="server" Display="None"   ValidationGroup="g1"
                            ErrorMessage="Please enter a valid date" Operator="DataTypeCheck" 
                            ControlToValidate="GMDEndDate" Type="Date">*</asp:CompareValidator>
                                  <br/>
                                  <asp:CompareValidator ID="CompareValidator1" runat="server" Display="None"  ValidationGroup="g1"
                                ErrorMessage="End date should be greater or equal to start date." 
                                ControlToCompare="GMDStartDate" ControlToValidate="GMDEndDate" 
                                Operator="GreaterThanEqual" Type="Date">*</asp:CompareValidator>

                              
        



  </div>
</asp:Content>
