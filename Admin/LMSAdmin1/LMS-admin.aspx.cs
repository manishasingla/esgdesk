using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections.Generic;

public partial class Admin_frmAdminHome : System.Web.UI.Page
{
    DateTime dtStartingDate;
    DateTime dtEndingDate;
    clsLeave_Logic objLeave = new clsLeave_Logic();
    DataTable dtTemp = null;
    DataTable dtTemp1 = null;
    public static int DayNo = 0;
    public static bool blnIsDateInterval = false;
    DataView dv2 = new DataView();

    clsEmployee_Logic1 objEmployee = new clsEmployee_Logic1();
    string strError = "No records found";
    List<DateTime> dates = new List<DateTime>();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] != null && Session["admin"].ToString() != "")
        {

            if (DatabaseHelper.isAdmin(Session["admin"].ToString()) == false)
            {
                Response.Redirect("~/Admin/login.aspx");
                return;
            }
        }

        if (!IsPostBack)
        {
            ddlTimeInterval.SelectedIndex = 1;
            fnSetDefaultDate();
            Session["TI"] = "TW";
            BindData();

        }
    }


    void BindData()
    {
        try
        {
            // lblError.Visible = false;
            if (blnIsDateInterval == false)
            {
                if (Session["TI"] == null)
                {
                    dtStartingDate = DateTime.Today.AddDays(DayNo);
                    dtEndingDate = DateTime.Today;
                }

                else
                {
                    if (Session["TI"] == "TW")
                    {
                        fnSetDefaultDate();
                    }
                    else if (Session["TI"] == "TM")
                    {
                        DateTime today = DateTime.Today;
                        DateTime FirstDate = new DateTime(today.Year, today.Month, 1);
                        today = new DateTime(today.Year, today.Month + 1, 1);
                        DateTime LastDate = today.AddDays(-1);
                        dtStartingDate = FirstDate;
                        dtEndingDate = LastDate;
                    }
                }
            }
            else
            {
                if (GMDStartDate.Text.Trim() == "")
                {
                    GMDStartDate.Text = dtStartingDate.ToString();
                }
                else
                {
                    dtStartingDate =Convert.ToDateTime(GMDStartDate.Text);

                }

                if (GMDEndDate.Text.Trim() == "")
                {
                    GMDEndDate.Text = dtEndingDate.ToString();

                }
                else
                {
                    dtEndingDate = Convert.ToDateTime(GMDEndDate.Text);
                }

            }

                DataSet dsTemp = objLeave.getEmployeeAbsentDetailsByDayInGroup(dtStartingDate, dtEndingDate);
                dtTemp = dsTemp.Tables[0];

                List<string> strDateArray = new List<string>();

                if (dtTemp != null)
                {
                    if (dtTemp.Rows.Count > 0)
                    {

                        dtTemp1 = dtTemp.Clone();

                        foreach (DataColumn dc in dtTemp.Columns)
                        {
                            if (dc.ColumnName == "AbsentDate")
                            {
                                dc.DataType = typeof(DateTime);
                            }
                        }

                        foreach (DataRow dr in dtTemp.Rows)
                        {
                            DateTime ldt = Convert.ToDateTime(dr["AbsentDate"].ToString());
                            string strReason = dr["Reason"].ToString();
                            dr["AbsentDate"] = ldt.ToString("yyyy/MM/dd hh:mm:ss.fff");
                            dr["Reason"] = strReason.Replace("@", "'").Trim();
                            dtTemp1.ImportRow(dr);
                        }

                    }

                    if (dtTemp != null)
                    {
                        if (dtTemp.Rows.Count > 0)
                        {
                            rptAbsentEmpByWeek.Visible = true;
                            this.lbl_pageinfo.Visible = false;
                            rptAbsentEmpByWeek.DataSource = (from x in dtTemp1.AsEnumerable() select x["AbsentDate"]).Distinct();
                            rptAbsentEmpByWeek.DataBind();
                        }
                        else
                        {
                            this.lbl_pageinfo.Text = "Record(s) are not found.";
                            this.lbl_pageinfo.Visible = true;
                            rptAbsentEmpByWeek.Visible = false;
                        }
                    }

                    //End Datatype conversion for AbsentDate Column in dtTemp
                }
                else
                {
                    this.lbl_pageinfo.Text = "Record(s) are not found.";
                    this.lbl_pageinfo.Visible = true;
                    rptAbsentEmpByWeek.Visible = false;
                }
            

        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, "Exception in LMS frmAdminHome.aspx.cs", "Method : Binddata()");
        }

    }


    protected void rptAbsentEmpByWeek_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item |
                     e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater rptr = (Repeater)e.Item.FindControl("Repeater2");
                rptr.DataSource =
                    dtTemp1.AsEnumerable().Where(x => x["AbsentDate"].Equals(e.Item.DataItem));
                rptr.DataBind();
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, "Exception in LMS frmAdminHome.aspx.cs", "Event : rptAbsentEmpByWeek_ItemDataBound()");
        }
    }


    protected void ddlTimeInterval_SelectedIndexChanged(object sender, EventArgs e)
    {
        blnIsDateInterval = false;

        if (ddlTimeInterval.SelectedValue.ToString() == "1") //Today
        {
            DayNo = 0;
            Session["TI"] = null;
        }

        else if (ddlTimeInterval.SelectedValue.ToString() == "3")//Last 7 days 
        {
            DayNo = -7;
            Session["TI"] = null;
        }


        else if (ddlTimeInterval.SelectedValue.ToString() == "5")// Last 31 days
        {
            DayNo = -31;
            Session["TI"] = null;
        }

        else if (ddlTimeInterval.SelectedValue.ToString() == "4")// This month
        {
            Session["TI"] = "TM";
        }

        else if (ddlTimeInterval.SelectedValue.ToString() == "2") //This Week
        {
            Session["TI"] = "TW";
        }
        BindData();
    }


    public void fnSetDefaultDate()
    {

        // lblError.Visible = false;
        String DayName = DateTime.Now.DayOfWeek.ToString();

        if (DayName == "Monday")
        {
            DayNo = 1;
        }
        else if (DayName == "Tuesday")
        {
            DayNo = 2;
        }

        else if (DayName == "Wednesday")
        {
            DayNo = 3;
        }

        else if (DayName == "Thursday")
        {
            DayNo = 4;
        }
        else if (DayName == "Friday")
        {
            DayNo = 5;
        }


         dtStartingDate = DateTime.Today.AddDays(-DayNo + 1);    // Monday
         dtEndingDate = DateTime.Today.AddDays(5 - DayNo);      //Friday
    }

    protected void GMDEndDate_TextChanged(object sender, EventArgs e)
    {
        blnIsDateInterval = true;
        BindData();
    }
}


