using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections.Generic;

public partial class Admin_frmAdminHome : System.Web.UI.Page
{
    clsLeave_Logic objLeave = new clsLeave_Logic();
    clsEmployee_Logic1 objEmployee = new clsEmployee_Logic1();
    DataTable dtTemp=null;
    DataTable dtTemp1 = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        BindData();
        if (dtTemp != null)
        {
            if (dtTemp.Rows.Count > 0)
            {
                rptAbsentEmpByWeek.DataSource = (from x in dtTemp1.AsEnumerable() select x["AbsentDate"]).Distinct();
                rptAbsentEmpByWeek.DataBind();
            }
        }
    }

 
   void BindData()
   {
       try
       {

           // lblError.Visible = false;

           DateTime dtStartingDate = DateTime.Now.AddDays(-5);
           DateTime dtEndingDate = DateTime.Now;

           DataSet dsTemp = objLeave.getEmployeeAbsentDetailsByDayInGroup(dtStartingDate, dtEndingDate);
           dtTemp = dsTemp.Tables[0];
      
           List<string> strDateArray = new List<string>();
       
           if(dtTemp!=null)
           {
               if (dtTemp.Rows.Count > 0)
               {

                   dtTemp1 = dtTemp.Clone();

                   foreach (DataColumn dc in dtTemp.Columns)
                   {
                       if (dc.ColumnName == "AbsentDate")
                       {
                           dc.DataType = typeof(DateTime);
                       }
                   }

                   foreach (DataRow dr in dtTemp.Rows)
                   {
                       DateTime ldt = Convert.ToDateTime(dr["AbsentDate"].ToString());
                       string strReason = dr["Reason"].ToString();
                       dr["AbsentDate"] = ldt.ToString("yyyy/MM/dd hh:mm:ss.fff");
                       dr["Reason"] = strReason.Replace("@", "'").Trim();
                       dtTemp1.ImportRow(dr);
                   }

               }

               //End Datatype conversion for AbsentDate Column in dtTemp
           }
           else
           {
               this.lbl_pageinfo.Text = "No one was absent in last 5 days";
               this.lbl_pageinfo.Visible = true;
           }
  
       }
       catch (Exception ex)
       {
           DatabaseHelper.HandleException(ex, "Exception in LMS frmAdminHome.aspx.cs", "Method : Binddata()");
       }


}
   protected void rptAbsentEmpByWeek_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
   {
       try
       {
           if (e.Item.ItemType == ListItemType.Item |
                    e.Item.ItemType == ListItemType.AlternatingItem)
           {
               Repeater rptr = (Repeater)e.Item.FindControl("Repeater2");
               rptr.DataSource =
                   dtTemp1.AsEnumerable().Where(x => x["AbsentDate"].Equals(e.Item.DataItem));
               rptr.DataBind();
           }
       }
       catch (Exception ex)
       {
           DatabaseHelper.HandleException(ex, "Exception in LMS frmAdminHome.aspx.cs", "Event : rptAbsentEmpByWeek_ItemDataBound()");
       }
   }
}


