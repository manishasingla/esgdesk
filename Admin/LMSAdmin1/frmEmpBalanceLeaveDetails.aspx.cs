using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Globalization;
public partial class Admin_frmEmpLeaveDetails : System.Web.UI.Page
{
    clsEmployee_Logic1 objEmployee = new clsEmployee_Logic1();
    clsLeave_Logic objLeave = new clsLeave_Logic();
    public static string strSUserName = "";
    DataSet dsTemp;
    DataTable dtTemp;
    public static DataView dv;
    double[] dblArrayOfTotalAndUsedLeave = new double[2];

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request["UserName"] == null)
        {
            Response.Redirect("EmployeeList.aspx?id=VL");
           
        }
        if (!IsPostBack)
        {
            BindData();
            BindData4Past10DaysGrid();
            fillddl();
            ddlYear.SelectedIndex = 10;
        }
       
    }

    void fillddl()
    {
        try
        {
            int year;
            year = DateTime.Now.Year;

            for (int i = -10; i < 10; i++)
            {
                ddlYear.Items.Add(Convert.ToString(year + i));
            }
        }
        catch (Exception ex)
        {
            lblLNF.Visible = true;
            lblLNF.Text = ex.Message;
        }

    }
    void BindData()
    {
        try
        {
            dsTemp = objEmployee.GetEmployee();

            if (dsTemp != null)
            {
                dtTemp = new DataTable();
                dtTemp = dsTemp.Tables[0];
                dv = new DataView(dtTemp);
                strSUserName = Request["UserName"].ToString();
                objLeave.UserName = strSUserName;
                dv.RowFilter = "EmpName Like'%" + Request["EN"].ToString() + "%'";
                objEmployee.UserName = Request["UserName"].ToString();

                objEmployee.GetEmployeeDetails(); // Set Employee's Details by Calling GetEmployeeDetails() Method
               
                if (Request["EN"] != null)
                    txtUserName.Text = Request["EN"].ToString();
                else
                    txtUserName.Text = "Not available";
                txtContactNo.Text = objEmployee.ContactNo;
                txtAddress.Text = objEmployee.Address;
                txtEmailId.Text = objEmployee.EmailId;

                fnSetLeaveDetails(DateTime.Now.Year); // Set Leave details on Textbox
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "GetEmployee()");
        }

    }

    void BindData4Past10DaysGrid()
    {
        try
        {
            CultureInfo culture = new CultureInfo("pt-BR");
            DateTime dt = DateTime.Now;
            DateTime dt1 = dt.AddDays(-9);
            string strEndDate = dt.ToString("d", culture);
            string strStartDate = dt1.ToString("d", culture);
            // string strSql = "select convert(varchar,AbsentDate,103) as AbsentDate,Reason,PaidOrNPaid as LeaveType from tbl_AbsentDetails where UserName='" + Request["UserName"].ToString() + "' and convert(varchar,AbsentDate,103)>='" + strStartDate + "'"
            //  + " and convert(varchar,AbsentDate,103) <='" + strEndDate + "' order by AbsentDate desc ";
            string strSql = "select top(10) cast(AbsentDate as DateTime) as AbsentDate,Reason,PaidOrNPaid as LeaveType from tbl_AbsentDetails where UserName='" + Request["UserName"].ToString() + "' order by AbsentDate desc ";

            DataSet dsAbsentDates = objEmployee.GetAbsentDates(strSql);
            if (dsAbsentDates != null)
            {
                gvAbsentDates.DataSource = dsAbsentDates.Tables[0].DefaultView;

                gvAbsentDates.DataBind();

                if (dsAbsentDates.Tables[0].Rows.Count > 0)
                {
              
                    lblStatus.Visible = false;
                    gvAbsentDates.Visible = true;
                    pnlAbsentDetails.Visible = true;
                }
                else
                {
                    lblStatus.Visible = true;
                    gvAbsentDates.Visible = false;
                    pnlAbsentDetails.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "BindData4Past10DaysGrid()");
            lblStatus.Visible = true;
            lblStatus.Text = "Error:" + ex.Message;
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {

    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("EmployeeList.aspx?id=VL");
    }

    protected void btnAddLeave_Click(object sender, EventArgs e)
    {
        //  dv.RowFilter = "username = ' " + strSUserName+ " '";
        try
        {
            Response.Redirect("frmLeaveAddByEmp.aspx?id=" + dv.ToTable().Rows[0]["UserName"] + "&EId=" + dv.ToTable().Rows[0]["user_id"] + "&EN=" + dv.ToTable().Rows[0]["EmpName"]);
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "femEmpBalanceLeaveDetails : btnAddLeave_Click event");
            lblLNF.Visible = true;
            lblLNF.Text = ex.Message;
        }
    }

    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            objLeave.UserName = Request["UserName"].ToString();
            fnSetLeaveDetails(Convert.ToInt32(ddlYear.SelectedItem.Text));
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "femEmpBalanceLeaveDetails : ddlYear_SelectedIndexChanged event");
            lblLNF.Visible = true;
            lblLNF.Text=ex.Message;
        }
    }

    protected void gvAbsentDates_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DateTime ldt = Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "AbsentDate"));
                e.Row.Cells[0].Text = ldt.ToString("dd/MM/yyyy");
                e.Row.Cells[1].Text = DataBinder.Eval(e.Row.DataItem, "Reason").ToString().Replace("@", "'").Trim();
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "femEmpBalanceLeaveDetails : gvAbsentDates_RowDataBound event");
            lblLNF.Visible = true;
            lblLNF.Text = ex.Message;
        }
    }



    public void fnSetLeaveDetails(int intYear)
    {
        try
        {
            objLeave.fnGetTotalAndUsedLeave(strSUserName, intYear); // fnGetTotalAndUsedLeave() will set 
            //taken leave's count by Leave types(e.g CL,ML,EL,HPL) and 
            //return Alloted leaves ,used leaves(e.g CL + ML + EL + HPL + ExtraLeaves) , 
            //Remaining balance of employee
            //(e.g Alloted Leaves -(CL + ML + EL + HPL + ExtraLeaves) )
            if (objLeave.AllotedLeaves != null && objLeave.AllotedLeaves != 0)
            {
                lblLNF.Visible = false;
                tblLeave.Visible = true;
                double dblHPL = 2 * (objLeave.HalfPaidBalanceLeaves);

                // --------------------------------------------------------------

                txtCLBal.Text = objLeave.CasualBalanceLeaves + " / " + objLeave.AllotedLeaves.ToString();
                txtMLBal.Text = objLeave.MedicalBalanceLeaves + " / " + objLeave.AllotedLeaves.ToString();
                txtEL.Text = objLeave.EarnBalanceLeaves + " / " + objLeave.AllotedLeaves.ToString();
                txtHPLBal.Text = dblHPL.ToString() + " / " + objLeave.AllotedLeaves.ToString();
                txtExtraBal.Text = objLeave.ExtraBalanceLeaves.ToString();
                txtTotalBal.Text = objLeave.UsedLeaves.ToString() + " / " + objLeave.AllotedLeaves.ToString();
            }
            else
            {
                lblLNF.Visible = true;
                tblLeave.Visible = false;
                lblLNF.Text = "Leaves are not defined for selected year";

            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "femEmpBalanceLeaveDetails : fnSetLeaveDetails()");
            lblStatus.Text = "Error:" + ex.Message;
        }

    }

}
