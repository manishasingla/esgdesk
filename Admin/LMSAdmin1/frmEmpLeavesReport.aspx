<%@ Page Language="C#" MasterPageFile="~/Admin/LMSAdmin/AdminMasterMenu.master" AutoEventWireup="true" CodeFile="frmEmpLeavesReport.aspx.cs" Inherits="Admin_frmEmpLeavesReport" Title="Employee leaves report" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script type="text/javascript" language="javascript">
function toggleAllCheckboexs(toggle)
{
				n = document.forms[0].length;
				var frm = document.forms[0];
				for(i=0;i<frm.length;i++)
				
					if(frm.elements[i].type=="checkbox")
						if (frm.elements[i].name.indexOf('Cbx')==0)
							frm.elements[i].checked=toggle;
			}
			
			
			function onlyNumbers(evt)
{
    var e = event || evt;
    var charCode = e.which || e.keyCode;
	if ((charCode > 47 && charCode < 58))
    return true;		             
    else
    return false ;		                            
}
			
			function onlyNumbers(evt)
{
    var e = event || evt;
    var charCode = e.which || e.keyCode;
	if ((charCode > 47 && charCode < 58))
    return true;		             
    else
    return false ;		                            
}
</script>
    
    <table style="width:81%"  class="publicloginTable">
     <tr>
                        <td colspan="4" class="LoginTitle"><h1 style="color:Black">
                            Employee Leave Report</h1>
                        </td>
                          
                           
                    </tr>
    <tr>
    <td valign="top" align="left">
       <table width="100%" class="admintablestyle">
                    
        <tr>
        <td colspan="4" class="LoginTitle">Manage Employee</td>
        </tr>
                    
        <tr>
        <td style="width: 18%">
            Employee name</td>
        <td align ="center"  style="width: 2%">:</td>
        <td style="width: 18%"><asp:TextBox ID="txtEmpName" runat="server"></asp:TextBox></td>
        <td></td>
       
        <td style="height: 26px">Page Size</td>
        <td align ="center" style="height: 26px" >:</td>
        <td style="height: 26px"><asp:TextBox ID="txtPageSize" OnKeypress="return onlyNumbers(event)" runat="server" Width="50px"></asp:TextBox>
        <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtPageSize"
        CssClass="lblerror" ErrorMessage="Invalid Number" MaximumValue="999" MinimumValue="1"
        Style="position: relative" Type="Integer"></asp:RangeValidator></td>
        <td style="height: 26px"><asp:Button ID="btnSearch" runat="server" Text="Search" Width="55px" 
                Style="position: relative; top: 0px; left: 0px;" OnClick="btnSearch_Click" /></td>
        </tr>
                    
                    
        <tr>
        <td colspan="8"></td>
        </tr>
        <tr>
        <td colspan="8">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
        <td colspan="4">
            &nbsp;</td>
        
        </tr>
        
        <tr>
       <td colspan="0" style="width: 20%; height:100%; vertical-align:top">
        <asp:GridView ID="gvEmp" runat="server" AutoGenerateColumns="False" Width="100%"
        Style="position: relative" AllowPaging="True" AllowSorting="True" 
                OnPageIndexChanging="gvEmp_PageIndexChanging" OnSorting="gvEmp_Sorting" 
                OnRowCommand="gvEmp_RowCommand" 
                OnSelectedIndexChanged="gvEmp_SelectedIndexChanged" PageSize="20">
        <Columns>
   
        <asp:TemplateField HeaderText="Employee name" SortExpression="EmpName">
                                                
        <HeaderStyle Width="30%" />
        <ItemTemplate>
        <%--<asp:LinkButton ID="lnkBtnTypeName" Text='<%#Eval("EmpName")%>' runat="server"  CommandArgument= '<%#Eval("EmpId")%>' CommandName="Add"></asp:LinkButton>--%>
         <asp:LinkButton ID="lnkBtnUserName" Text='<%#Eval("EmpName")%>' runat="server" CommandArgument= '<%#string.Format("{0}|{1}|{2}",Eval("UserName"),Eval("user_id"),Eval("EmpName"))%>' CommandName="Add"></asp:LinkButton>
       
        </ItemTemplate>
        </asp:TemplateField>
        
       

        <asp:TemplateField HeaderText="Address" SortExpression="Address" Visible="false">
        <ItemTemplate>
        <%#Eval("Address")%>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="ContactNo" SortExpression="ContactNo"  Visible="false">
        <ItemTemplate>
        <%#Eval("ContactNo")%>
        </ItemTemplate>
        </asp:TemplateField>
        </Columns>
        </asp:GridView>
        </td>
        <td style="vertical-align:top; width:40%">
            <asp:Panel ID="pnlLeaveDetails" runat="server"  Direction="LeftToRight" 
                GroupingText="Employee Leave details for current and previous year" 
                Height="100%" HorizontalAlign="Center" Width="100%" style="valign:top" 
                Visible="False">
                <asp:Label ID="lblLeaveDetails" runat="server" style="valign:top"></asp:Label>
            </asp:Panel>
            </td>
            <td style="vertical-align:top; width:20%">
            <asp:Panel ID="pnlEmpEmailSend" runat="server" Visible="False" >
               <table>
               <tr>
               <td style="width:50%">
                <asp:Label ID="lblEmpName1" runat="server" style="valign:top">Select first emp :</asp:Label></td><td>
                <asp:DropDownList ID="ddlEmpList1" runat="server" 
                    onselectedindexchanged="ddlEmpList1_SelectedIndexChanged">
                </asp:DropDownList></td></tr>
                 <tr><td colspan="2"></td></tr>
                <tr>
               <td style="width:50%">
                <asp:Label ID="lblEmpName2" runat="server" style="valign:top">Select second emp :</asp:Label></td><td>
                <asp:DropDownList ID="ddlEmpList2" runat="server" 
                    onselectedindexchanged="ddlEmpList2_SelectedIndexChanged">
                </asp:DropDownList></td></tr>
               
                <tr>
                <td colspan="2" align="right">
                <asp:Button ID="btnSendEmail" runat="server" onclick="btnSendEmail_Click" 
                    Text="Send Email" /></td>
                    </tr>
                    <tr>
                    <td colspan="2">
                    <asp:Label ID="lblEmailReport" runat="server"></asp:Label>
                    </td>
                    </tr>
                    </table>
            </asp:Panel>
                
            </td>
        </tr>
                               
        <tr>
        <td style="width: 25%">
        <asp:Label ID="lblError" runat="server"  Visible="false" CssClass="lblerror"></asp:Label>
        <asp:Label ID="Lbl_Pageinfo" runat="server" Style=""></asp:Label></td>
        <td align="left" >
            &nbsp;</td>
        </tr>
        </table>
        </td>
        </tr>
        
        <tr>
        <td colspan="4"></td>
        </tr>
    </table>
    </td>
    </tr>
    </table>
</asp:Content>

