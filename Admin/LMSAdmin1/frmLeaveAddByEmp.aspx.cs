using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Globalization;

public partial class Admin_frmEmpLeaveDetails : System.Web.UI.Page
{
    clsEmployee_Logic1 objEmployee = new clsEmployee_Logic1();
    clsLeave_Logic objLeave = new clsLeave_Logic();
    String dtDayOfLeave = "";
    static DataView dv = null;
    static string strLeaveType = "";
    static string strPONP = "";
    bool blnIsBalanceAvailable = true;
    double TotalBalanceLeave = 0;
    string strHeader = "";
    string strLeaveInfo = "";
    string strEmailSucess = "";
    int intCounter = 0;
    string strDay = "";
    bool blnCME = false;
    bool blnHPL = false;
    DataSet ds = new DataSet();
    static bool sblnIsChecked = false;
    static double sdblUsedLeave = 0.0;
    static double sdblTotal = 0.0;
    static double sdblRBal = 0.0;
    static DateTime DTSDate;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request["id"] == null)
        {
            Response.Redirect("EmployeeList.aspx?id=AL");
        }

        if (!IsPostBack)
        {
            BindData();

            BindLeavetype();

        }
        BindData1();

    }
    void BindData()
    {

        objEmployee.UserName = Request["id"].ToString();
        objEmployee.GetEmployeeDetails();
        double[] dblUsedAndAlloted = objLeave.fnGetTotalAndUsedLeave(objEmployee.UserName, DateTime.Now.Year);
        txtUserName.Text = Request["EN"].ToString();
        txtContactNo.Text = objEmployee.ContactNo;
        txtAddress.Text = objEmployee.Address;
        txtEmailId.Text = objEmployee.EmailId;
        txtDesig.Text = objEmployee.Designation;
        objLeave.UserName = Request["id"].ToString();
     
        objLeave.GetTotalBalanceDaysInOne(DateTime.Now.Year);
        double dblHPL = 2 * (objLeave.HalfPaidBalanceLeaves);
        txtCLBal.Text = objLeave.CasualBalanceLeaves.ToString();
        txtMLBal.Text = objLeave.MedicalBalanceLeaves.ToString();
        txtEL.Text = objLeave.EarnBalanceLeaves.ToString();
        txtHPLBal.Text = dblHPL.ToString();
        txtExtraLeaves.Text = objLeave.ExtraBalanceLeaves.ToString();
        objEmployee.EmpId = Convert.ToInt32(Request["EId"]);
        txtTotalBalance.Text = objLeave.UsedLeaves.ToString()+ "/" + objLeave.AllotedLeaves.ToString();
    }



    public void BindData1()
    {
        try
        {
            lblGridHeader.Visible = false;
            objEmployee.Sort_On = "";
            string strOrderBy = "";

            if (ViewState["Sort_On"] != null)
            {
                objEmployee.Sort_On = ViewState["Sort_On"].ToString() + " " + ViewState["Sort_By"].ToString();
                strOrderBy = ViewState["Sort_On"].ToString() + " " + ViewState["Sort_By"].ToString(); ;
            }

            // lblError.Visible = false;

            DateTime dtStartingDate = DateTime.Now.AddMonths(-6);
            DateTime dtEndingDate = DateTime.Now;
            clsLeave_Logic objMonth = new clsLeave_Logic();
            DataSet dsTemp = objMonth.getEmployeeAbsentDetailsByDay(dtStartingDate, dtEndingDate);
            DataTable dtTemp = dsTemp.Tables[0];

            dv = new DataView(dtTemp);
            dv.Sort = strOrderBy;

            string strFilter = "UserName='" + Request["id"].ToString() + "'";
            dv.RowFilter = strFilter;

            gvAbsentDates.DataSource = dv;

            gvAbsentDates.DataBind();

            if (dv.ToTable().Rows.Count == 0)
            {
                this.Lbl_Pageinfo.Visible = false;
                lblGridHeader.Visible = false;
                gvAbsentDates.Visible = false;
            }
            else
            {
                lblGridHeader.Visible = true;
                gvAbsentDates.Visible = true;
                Int16 intTo;
                Int16 intFrom;
                if (gvAbsentDates.PageSize * (gvAbsentDates.PageIndex + 1) < dv.ToTable().Rows.Count)
                {
                    intTo = System.Convert.ToInt16(gvAbsentDates.PageSize * (gvAbsentDates.PageIndex + 1));
                }
                else
                {
                    intTo = System.Convert.ToInt16(dv.ToTable().Rows.Count);
                }
                intFrom = System.Convert.ToInt16((gvAbsentDates.PageSize * gvAbsentDates.PageIndex) + 1);
                this.Lbl_Pageinfo.Text = "Record(s) " + intFrom + " to " + intTo + " of " + dv.ToTable().Rows.Count;
                this.Lbl_Pageinfo.Visible = true;
            }
        }
        catch (Exception ex)
        {
            // lblError.Text = ex.Message;
            // lblError.Visible = true;
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {

    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("EmployeeList.aspx?id=AL");
    }

    protected void btnAddLeave_Click(object sender, EventArgs e)
    {
        try
        {
            int IsEHPL = 0;
            try
            {
                int year = DateTime.Now.Year;
                objLeave.UserName = Request["id"].ToString();
                fnCheckBal();
                TotalBalanceLeave = objLeave.GetTotalBalanceDaysInOne(year);
                objLeave.LeaveTypeId = Convert.ToInt32(ddlLeaveType.SelectedValue);
                fnGetLeaveType();

            }
            catch
            {
            }

            if ((sdblRBal > .50 || (sdblRBal == .50 && strLeaveType == "HPL") || Convert.ToInt32(ddlLeaveType.SelectedValue) == 5))
            {

                Random _r = new Random();
                objLeave.ApplicationNo = _r.Next();

                DateTime dt2;
                string strDate2 = "";
                fnGetLeaveType();
                List<DateTime> DTAddedStartAndEnd = new List<DateTime>();
                string strTemp = "";
                //region after using Ajax Calender Extender
                DateTime dt1 = Convert.ToDateTime(GMDStartDate.Text);
                string strDate1 = dt1.ToString("yyyy/MM/dd");
                if (GMDEndDate.Text.Trim() != "")
                {
                    dt2 = Convert.ToDateTime(GMDEndDate.Text);
                }
                else
                {
                    dt2 = dt1;
                }
                strDate2 = dt2.ToString("yyyy/MM/dd");

                lblStatusMsg.Text = "";
                int intNoOfRowAdded = 0, i = 0;
                DTSDate = DateTime.Now;
                DateTime DTEDate = DateTime.Now;
                //DateTime dtDayOfLeave = GMDStartDate.Date;
                DateTime dtDayOfLeave = dt1;
                clsLeave_Logic objclsll = new clsLeave_Logic();
                //objLeave.AbsentDate = GMDStartDate.Date;
                objLeave.AbsentDate = dt2;

                objEmployee.UserName = Request["id"].ToString();
                objLeave.UserName = Request["id"].ToString();
                objEmployee.EmpId = Convert.ToInt32(Request["EId"]);
                bool blnIsExist = false;
                // DTSDate = GMDStartDate.Date;
                //DTEDate = GMDEndDate.Date;
                DTSDate = dt1;
                DTEDate = dt2;

                List<DateTime> dates = new List<DateTime>();

                if (DTEDate <= DTSDate)
                {
                    DTEDate = DTSDate;
                }

                for (DateTime dt = DTSDate; dt <= DTEDate; dt = dt.AddDays(1))
                {
                    dates.Add(dt);

                }

                foreach (DateTime dt in dates)
                {
                    fnCheckBal();

                    blnIsExist = objclsll.CheckLeaveDateInAbsentDetails(dt, Request["id"].ToString());

                    TotalBalanceLeave = Convert.ToDouble(objLeave.GetTotalBalanceDaysInOne(dt.Year));
                    strDay = dt.DayOfWeek.ToString();
                    string strReason = txtReason.Text.Replace("'", "@");
                    objLeave.LeaveTypeId = Convert.ToInt32(ddlLeaveType.SelectedValue);

                    if ((((sdblRBal > .50) || (sdblRBal == .50 && strLeaveType == "HPL")) || (Convert.ToInt32(ddlLeaveType.SelectedValue) == 5)) && (blnIsExist == false && strDay != "Sunday"))
                    {
                        fnSetIsHPLORNot();
                        fnSetPaidORUnpaid();

                        if (strDay == "Saturday")
                        {
                            if (objLeave.LeaveTypeId != 5)
                            {
                                objLeave.IsHpl = 1;
                                objLeave.LeaveTypeId = 4;
                                strLeaveType = "HPL";
                            }
                            else
                            {
                                IsEHPL = 1;
                            }
                            // this for tbl_LeaveDetail
                        }
                        else
                        {
                            fnGetLeaveType();
                        }

                        if ((objLeave.IsHpl == 1) && (objLeave.LeaveTypeId == 5) || ((strDay == "Saturday") && (objLeave.LeaveTypeId == 5)))
                        {
                            IsEHPL = 1;
                        }
                        else
                        {
                            IsEHPL = 0;
                        }
                        objLeave.AddLeaveDetails(dt.Year, dt, objLeave.LeaveTypeId, IsEHPL);// this for tbl_LeaveDetail
                        intNoOfRowAdded = objclsll.AddLeaves(dt, objEmployee.UserName, strPONP, strReason, strLeaveType, IsEHPL);
                        DTAddedStartAndEnd.Add(dt);
                        TotalBalanceLeave = objLeave.GetTotalBalanceDaysInOne(dt.Year);
                        objLeave.TotalBalance = TotalBalanceLeave;
                        //lblStatusMsg.ForeColor = Color.Green;                            
                        gvAbsentDates.DataBind();
                        BindData();
                        BindData1();
                        blnHPL = false;

                        if (intNoOfRowAdded >= 1)
                        {
                            intCounter++;
                        }
                        else
                        {
                            //lblStatusMsg.ForeColor = Color.Red;
                            lblStatusMsg.Text = "An error happend during process.";
                            blnIsBalanceAvailable = false;
                            BindLeavetype();
                        }

                    }
                    else
                    {
                        //lblStatusMsg.ForeColor = Color.Red;
                        if (blnIsExist == true)
                            strTemp += @"<span style=""color:red"">Leave has been already added for date:</span> " + dt.ToShortDateString() + "<br/>";
                        else
                        {
                            if (strDay == "Sunday")
                            {
                                strTemp += @"<span style=""color:red"">As its Sunday on</span> " + dt.ToString("dd/MM/yyyy") + @"<span style=""color:red""> so leave cannot be added for this day.</span><br/>";
                            }

                            //else if (TotalBalanceLeave <= 0)
                            else if (sdblRBal <= .50)
                            {
                                ClientScript.RegisterStartupScript(GetType(), "hwa", "alert('You do not have sufficient balance ! Please select Extra Leave or HPL as leave category.');", true);
                                blnIsBalanceAvailable = true;
                                BindLeavetype();
                                break;
                            }

                        }

                    }

                }

                if (intCounter > 0)
                {
                    if (intCounter == 1)
                    {

                        lblStatusMsg.Text = @"<span style=""color:green"">Leave has been added on the date(s):</span>" + DTAddedStartAndEnd[0].ToString("dd/MM/yyyy");
                        if (DTAddedStartAndEnd[0].ToString("dd/MM/yyyy") == DateTime.Now.ToString("dd/MM/yyyy"))
                            strLeaveInfo = "<strong >" + Request["EN"].ToString() + "</strong> will not be coming to office today as he/she " + txtReason.Text.Trim();
                        else
                            strLeaveInfo = "<strong >" + Request["EN"].ToString() + "</strong> will not be coming to office on " + DTAddedStartAndEnd[0].ToString("dd/MM/yyyy") + " as he/she " + txtReason.Text.Trim();
                        sendEmail();

                        //lblStatusMsg.Text += "<br/>" + strTemp;
                    }
                    else
                    {
                        //XYZ will be on leave from dd/mm/yy till dd/mm/yy as he/she is going out of station and will join back from {date}
                        lblStatusMsg.Text = @"<span style=""color:green"">Leave has been added on the date(s):</span>" + DTAddedStartAndEnd[0].ToString("dd/MM/yyyy");
                        lblStatusMsg.Text += @"<span style=""color:green""> to </span>" + DTAddedStartAndEnd[DTAddedStartAndEnd.Count - 1].ToString("dd/MM/yyyy");
                        strLeaveInfo = "<strong >" + Request["EN"].ToString() + "</strong> will be on leave from "
                                        + DTAddedStartAndEnd[0].ToString("dd/MM/yyyy") + " to " + DTAddedStartAndEnd[DTAddedStartAndEnd.Count - 1].ToString("dd/MM/yyyy") + " as he/she " + txtReason.Text.Trim();
                        sendEmail();

                        // lblStatusMsg.Text += "<br/>" + strTemp;
                    }

                    BindLeavetype();
                }
                else
                {
                    if (strDay == "Sunday")
                    {
                        // lblStatusMsg.Text += strTemp;
                    }
                }
                lblStatusMsg.Text += "<br/>" + strTemp + "<br/>" + strEmailSucess;
                fnClearControl();

            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "hwa", "alert('You do not have sufficient leave balance  ! Please select Extra Leave as leave category.');", true);
                blnIsBalanceAvailable = false;
                BindLeavetype();

            }
            // }


        }


        catch (Exception ex)
        {

            lblStatusMsg.Text = "There is a Error:" + ex;
        }

    }
    protected void gvAbsentDates_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvAbsentDates.PageIndex = e.NewPageIndex;
        gvAbsentDates.DataSource = dv;
        gvAbsentDates.DataBind();

    }

    protected void gvAbsentDates_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void gvAbsentDates_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[1].Text = DataBinder.Eval(e.Row.DataItem, "Reason").ToString().Replace("@", "'");
            DateTime dt = Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "AbsentDate"));
            string strcellText = dt.ToString("dd/MM/yyyy");
            e.Row.Cells[0].Text = strcellText;
            e.Row.Cells[1].Text = DataBinder.Eval(e.Row.DataItem, "Reason").ToString().Replace("@", "'");

        }
    }


    protected void gvAbsentDates_Sorting(object sender, GridViewSortEventArgs e)
    {
        objEmployee.Sort_On = e.SortExpression;
        ViewState["Sort_On"] = objEmployee.Sort_On;
        if (ViewState["Sort_By"] == null)
            ViewState["Sort_By"] = "Asc";
        if (ViewState["Sort_By"].ToString() == "Asc")
        {
            ViewState["Sort_By"] = "Desc";
        }
        else
        {
            ViewState["Sort_By"] = "Asc";
        }

        BindData1();
    }

    protected void ddlLeaveType_SelectedIndexChanged(object sender, EventArgs e)
    {
        // GetLeaveBalance(strLeaveType);
    }


    void BindLeavetype()
    {
        fnCheckBal();
        DataSet dsLeavetype = objLeave.GetLeaveType();
        DataView dv = dsLeavetype.Tables[0].DefaultView;

        sdblRBal = sdblTotal - sdblUsedLeave;

        if (sdblRBal > .50)
        {
            dv.RowFilter = "LeaveTypeId <> 5";
            ddlLeaveType.DataSource = dv;
            chkbHPL.Visible = false;
        }
        else
        {
            if (sdblRBal == .50)
            {
                dv.RowFilter = "LeaveTypeId>=4"; // Means only Extra Leave and HPL will be displayed 
                ddlLeaveType.DataSource = dv;
                chkbHPL.Visible = false;
            }
            else if (sdblRBal <= 0)
            {
                dv.RowFilter = "LeaveTypeId >= 5"; // Means only Extra Leave will be displayed 
                ddlLeaveType.DataSource = dv;
                chkbHPL.Visible = true;
            }

        }
        ddlLeaveType.DataTextField = "LeaveTypeName";
        ddlLeaveType.DataValueField = "LeaveTypeId";
        ddlLeaveType.DataBind();
        ddlLeaveType.Items.Insert(0, "Select");
    }



    protected void ddlLeaveTypePOrNP_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    public void fnGetLeaveType()
    {
        if (ddlLeaveType.SelectedIndex == 0)
            strLeaveType = "Not Selected";
        else if (Convert.ToInt32(ddlLeaveType.SelectedValue) == 1)
            strLeaveType = "CL";
        else if (Convert.ToInt32(ddlLeaveType.SelectedValue) == 2)
            strLeaveType = "ML";
        else if (Convert.ToInt32(ddlLeaveType.SelectedValue) == 3)
            strLeaveType = "EL";
        else if (Convert.ToInt32(ddlLeaveType.SelectedValue) == 4)
            strLeaveType = "HPL";
        else if (Convert.ToInt32(ddlLeaveType.SelectedValue) == 5)
            strLeaveType = "Extra Leave";
    }

    #region send Email



    protected void sendEmail()
    {

        try
        {
            bool blnIsSent1 = false;
            bool blnIsSent2 = false;
            bool blnIsSent3 = false;
            bool blnIsSent4 = false;
            string emailid1 = "", emailid2 = "", emailid3 = "", emailid4 = "";

            emailid1 = System.Configuration.ConfigurationManager.AppSettings["LMS1"].ToString();
            // emailid2 = "rajeshs@esolutionsgroup.co.uk";
            emailid2 = System.Configuration.ConfigurationManager.AppSettings["LMS2"].ToString();
            emailid3 = System.Configuration.ConfigurationManager.AppSettings["LMS3"].ToString();
            emailid4 = System.Configuration.ConfigurationManager.AppSettings["LMS4"].ToString();

            string strBody = "";
            strBody = GenerateHTML();



            string strSubject = "Employee on " + strLeaveType + " Leave :" + Request["EN"].ToString();
            blnIsSent1 = DatabaseHelper.sendEmailLMS(emailid1, strSubject, strBody);
            blnIsSent2 = DatabaseHelper.sendEmailLMS(emailid2, strSubject, strBody);
            blnIsSent3 = DatabaseHelper.sendEmailLMS(emailid3, strSubject, strBody);
            //blnIsSent4 = DatabaseHelper.sendEmailLMS(emailid4, strSubject, strBody);
            strEmailSucess = "Email has been sent to ";
            if (blnIsSent1)
                strEmailSucess += "Juhi Shetty<br/>";
            if (blnIsSent4)
                strEmailSucess += "Rituraj Kaur<br/>";
            if (blnIsSent2)
                strEmailSucess += "Rohit Dixit<br/>";
            if (blnIsSent3)
                strEmailSucess += "Michael Robinson<br/>";

        }
        catch
        {
        }
    }

    private string GenerateHTML()
    {
        try
        {

            string lstrReason = "";
            string serverpath = ConfigurationManager.AppSettings["WebAddress"];
            DataTable dtTemp1, dtTemp2;
            DataSet dsTemp1, dsTemp2;
            //body of mail for the person
            objLeave.UserName = Request["id"].ToString();
            DateTime d1 = DateTime.Now;

            string dtSDate = FirstDayOfYear(d1).ToString("yyyy-MM-dd");//GMDStartDate.DateString;
            string dtEDate = LastDayOfYear(d1).ToString("yyyy-MM-dd");
            string dtDOL = DTSDate.ToString("yyyy-MM-dd");

            //string strSql1 = "select convert(varchar,AbsentDate,103) as AbsentDate ,Reason,PaidOrNPaid as LeaveT, LeaveType from tbl_AbsentDetails where UserName='" + Request["id"].ToString() + "' and AbsentDate <'" + dtDOL + "'"
            //                      + " order by cast(AbsentDate as DateTime) desc ";

            string strSql1 = "select convert(varchar,AbsentDate,103) as AbsentDate ,Reason,PaidOrNPaid as LeaveT,LeaveType from tbl_AbsentDetails where UserName='" + Request["id"].ToString() + "' and AbsentDate >='" + dtSDate + "'"
                             + " and AbsentDate <='" + dtEDate + "' order by cast(AbsentDate as DateTime) desc ";
           
            dsTemp1 = objEmployee.GetAbsentDates(strSql1);

            DateTime d2 = DateTime.Now.AddYears(-1);

            dtSDate = FirstDayOfYear(d2).ToString("yyyy-MM-dd");//GMDStartDate.DateString;
            dtEDate = LastDayOfYear(d2).ToString("yyyy-MM-dd");

            string strSql2 = "select convert(varchar,AbsentDate,103) as AbsentDate ,Reason,PaidOrNPaid as LeaveT,LeaveType from tbl_AbsentDetails where UserName='" + Request["id"].ToString() + "' and AbsentDate >='" + dtSDate + "'"
                                + " and AbsentDate <='" + dtEDate + "' order by cast(AbsentDate as DateTime) desc ";

            try
            {
                if (dsTemp1 != null)
                {
                    dtTemp1 = dsTemp1.Tables[0];

                    //body of mail for the person

                    DateTime dt = DateTime.Now;

                    strHeader += "Hi Michael ,";
                    strHeader += "<br/><br/><br/>";

                    // strHeader += strLeaveInfo + dtTemp1.Rows[0]["Reason"].ToString().Replace("@", "'").Trim() + ", Employee's past leave details are:";
                    strHeader += strLeaveInfo + ", Employee's past leave details are:";
                    strHeader += "<br/><br/>";


                    strHeader += @"<b><u><strong >" + Request["EN"].ToString() + "'s</strong> leave details:</u></b>";

                    strHeader += "<br/><br/>";
                    strHeader += @"<table border=""1"">";

                    strHeader += @"<tr><td colspan=""3""><strong >Leave details for year :" + d1.Year.ToString() + "</strong></td></tr>";

                    if (dtTemp1.Rows.Count > 0)
                    {
                        strHeader += @"<tr><td width=""15%""><strong >Date</strong></td><td colspan=""2""><strong>Reason</strong></td></tr>";

                        for (int i = 0; i < dtTemp1.Rows.Count; i++) // logic to group  absent employee on same date.
                        {
                            if (dtTemp1.Rows[i]["Reason"].ToString().Trim() != "")
                                lstrReason = dtTemp1.Rows[i]["Reason"].ToString().Replace("@", "'").Trim();
                            strHeader += "<tr><td>" + dtTemp1.Rows[i]["AbsentDate"].ToString().Trim() + @"</td><td colspan=""2"">" + lstrReason + "</td></tr>";

                        }
                    }
                    else
                        strHeader += @"<tr><td colspan=""3""> No leave was taken before today in this year </td></tr>";

                }
            }
            catch (Exception ex)
            {
                DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, strSql1);
            }
            strHeader += @"<tr><td colspan=""3"" height=""10px""></td></tr>";

            strHeader += @"<tr><td colspan=""3""><strong >Leave details for year :" + d2.Year.ToString() + "</strong></td></tr>";
            try
            {

                dsTemp2 = objEmployee.GetAbsentDates(strSql2);

                if (dsTemp2 != null)
                {
                    dtTemp2 = dsTemp2.Tables[0];

                    if (dtTemp2.Rows.Count > 0)
                    {
                        strHeader += @"<tr><td width=""15%""><strong> Date</strong></td><td colspan=""2""><strong>Reason </strong></td></tr>";


                        for (int i = 0; i < dtTemp2.Rows.Count; i++) // logic to group  absent employee on same date.
                        {
                            if (dtTemp2.Rows[i]["Reason"].ToString().Trim() != "")
                                lstrReason = dtTemp2.Rows[i]["Reason"].ToString().Replace("@", "'").Trim();
                            strHeader += "<tr><td>" + dtTemp2.Rows[i]["AbsentDate"].ToString().Trim() + @"</td><td colspan=""2""> " + lstrReason + "</td></tr>";

                        }
                    }
                    else
                        strHeader += @"<tr><td colspan=""3"">No leave was taken in this year</td></tr>";
                }

            }
            catch (Exception ex)
            {
                DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, strSql2);
            }
            strHeader += @"<tr><td colspan=""3"" height=""10px""></td></tr>";

            strHeader += @"<tr><td colspan=""3""><strong >Past 10 days leave details</strong></td></tr>";
            try
            {
                DataTable dtTemp3 = fnGetPast10DaysLeaveDetails();
                DataView dv = new DataView(dtTemp3);
                dv.Sort = "AbsentDate desc";
                dtTemp3 = dv.ToTable();

                if (dtTemp3 != null)
                {
                    if (dtTemp3.Rows.Count > 0)
                    {
                        string strAbsentdate = "";
                        string strUsername = "";
                        strHeader += @"<tr><td width=""15%""><strong >Date</strong></td><td width=""20%""><strong>User name</strong></td><td width=""65%""><strong>Reason</strong></td></tr>";

                        for (int i = 0; i < dtTemp3.Rows.Count; i++) // logic to group  absent employee on same date.
                        {
                            lstrReason = "";
                            DateTime ldt = Convert.ToDateTime(dtTemp3.Rows[i]["AbsentDate"].ToString());
                            strAbsentdate = ldt.ToString("dd/MM/yyyy");
                            strUsername = dtTemp3.Rows[i]["UserName"].ToString().Replace("@", "'").Trim();
                            if (dtTemp3.Rows[i]["Reason"].ToString().Trim() != "")
                                lstrReason = dtTemp3.Rows[i]["Reason"].ToString().Replace("@", "'").Trim();
                            strHeader += "<tr><td>" + strAbsentdate.Trim() + @"<td> " + strUsername + "</td>" + @"</td><td>" + lstrReason + "</td></tr>";

                        }
                    }
                    else
                        strHeader += @"<tr><td colspan=""3"">No leave was taken in last 10 days</td></tr>";
                }
            }
            catch (Exception ex)
            {
                DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "fnGetPast10DaysLeaveDetails()");
            }

            strHeader += "</table>";

            strHeader += "<br/><br/><br/><br/><br/><br/>";
            strHeader += "Regards<br/>";

            strHeader += "Juhi Shetty (HR)<br/>";
            strHeader += "eSolutions Group (ESG)<br/>";
            strHeader += "Tel: +91 2232505575<br/>";
            strHeader += "Mob: +91 8698009200<br/>";
            strHeader += "201 Rushabh Arcade . Old Station Road . Thane-West . 400 601<br/>";
            strHeader += "Email: juhis@esolutionsgroup.co.uk<br/>";
            strHeader += "Website: www.esolutionsgroup.co.uk<br/>";
            strHeader += "<br/><br/>";

            //    strHeader += "------------------";
            //    strHeader += "<br><br>";
            //    strHeader += ConfigurationManager.AppSettings["CompanyName"];
            //    strHeader += "<br>";
            //    //strHeader += ConfigurationManager.AppSettings["WebAddress"];
            //    strHeader += "<a href=" + ConfigurationManager.AppSettings["WebAddress"] + ">";
            //    strHeader += ConfigurationManager.AppSettings["WebAddressToShow"];
            //    strHeader += "</a>";
        }

        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "GenerateHTML()");
        }


        return strHeader;
    }

    //region read all leave




    static DateTime LastDayOfYear()
    {
        return LastDayOfYear(DateTime.Today);
    }


    static DateTime LastDayOfYear(DateTime d)
    {
        // 1
        // Get first of next year
        DateTime n = new DateTime(d.Year + 1, 1, 1);
        // 2
        // Subtract 1 from it
        return n.AddDays(-1);
    }

    static DateTime FirstDayOfYear()
    {
        return FirstDayOfYear(DateTime.Today);
    }


    static DateTime FirstDayOfYear(DateTime y)
    {
        return new DateTime(y.Year, 1, 1);
    }

    #endregion send Email


    public void fnCheckBal()
    {
        try
        {
            string strTotal = objLeave.fnGetTotalOfAllotedLeavesByEmp(DateTime.Now.Year);//fnGetTotalOfAllotedLeavesByEmp() Return total of leaves alloted to employee.
            // returns remaining balance and set seperately each category's used leaves
            sdblTotal = Convert.ToDouble(strTotal);
            TotalBalanceLeave = objLeave.GetTotalBalanceDaysInOne(DateTime.Now.Year);
            sdblUsedLeave = objLeave.CasualBalanceLeaves + objLeave.MedicalBalanceLeaves + objLeave.EarnBalanceLeaves + objLeave.HalfPaidBalanceLeaves + objLeave.ExtraBalanceLeaves;
            sdblRBal = sdblTotal - sdblUsedLeave;
            if (sdblUsedLeave < sdblTotal)
                blnIsBalanceAvailable = true;
            else
                blnIsBalanceAvailable = false;
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "fnCheckBal()");
        }
    }

    public void fnClearControl()
    {
        txtAddress.Text = "";
        txtReason.Text = "";
    }

    public DataTable fnGetPast10DaysLeaveDetails()
    {
        DataTable dtTemp3 = null;
        int intRowIndex = 0;
        bool blnIsFtime = true;
        bool blnIsMatchFirst = true;
        string temp = "";
        string temp1 = "";
        string temp2 = "";
        try
        {
            objEmployee.Sort_On = "";
            string strOrderBy = "";
            List<DateTime> dates = new List<DateTime>();

            if (ViewState["Sort_On"] != null)
            {
                objEmployee.Sort_On = ViewState["Sort_On"].ToString() + " " + ViewState["Sort_By"].ToString();
                strOrderBy = ViewState["Sort_On"].ToString() + " " + ViewState["Sort_By"].ToString(); ;
            }

            //lblError.Visible = false;

            DateTime dtStartingDate = DateTime.Today.AddDays(-10);
            DateTime dtEndingDate = DateTime.Today;

            for (DateTime dt = dtStartingDate; dt < dtEndingDate; dt = dt.AddDays(1))
            {
                dates.Add(dt);
            }

            DataSet dsTemp = objLeave.getEmployeeAbsentDetailsByDay(dtStartingDate, dtEndingDate);

            if (dsTemp != null)
            {
                DataTable dtTemp = dsTemp.Tables[0];
                dtTemp.DefaultView.Sort = "AbsentDate Desc";
                bool blnDATEEXIST = false;

                List<string> strDateArray = new List<string>();
                string strdt1 = "";
                string strdt2 = "";
                if (dtTemp.Rows.Count > 0)
                {
                    for (int i = 0; i < dates.Count; i++)
                    {

                        for (int j = 0; j < dtTemp.Rows.Count; j++)
                        {
                            strdt1 = dates[i].ToString("yyyy/MM/dd 00:00:00.000");

                            DateTime ldt = Convert.ToDateTime(dtTemp.Rows[j]["AbsentDate"].ToString());

                            strdt2 = ldt.ToString("yyyy/MM/dd 00:00:00.000");

                            //strdt2 = ldt.ToString("dd/MM/yyyy");

                            if (strdt1.Trim() == strdt2.Trim())
                            {
                                blnDATEEXIST = true;
                            }

                        }
                        if (blnDATEEXIST == false)
                            strDateArray.Add(strdt1);
                        blnDATEEXIST = false;
                    }

                    foreach (string ldt1 in strDateArray)
                    {
                        DataRow dr = dtTemp.NewRow(); //08/08/2012 

                        dr["AbsentDate"] = ldt1;
                        dr["UserName"] = "No one absent";
                        dtTemp.Rows.Add(dr);

                    }

                    
                    List<string> temp3 = new List<string>();
                    for (intRowIndex = 0; intRowIndex < dtTemp.Rows.Count; intRowIndex++) // logic to group  absent employee on same date.
                    {

                        for (int j = 0; j < dtTemp.Rows.Count; j++)
                        {

                            //    j = 1;
                            if (dtTemp.Rows[intRowIndex]["AbsentDate"].ToString().Trim() == dtTemp.Rows[j]["AbsentDate"].ToString().Trim() && (intRowIndex != j))
                            {
                                if (blnIsFtime == true)
                                {
                                    temp3.Add(dtTemp.Rows[intRowIndex]["AbsentDate"].ToString().Trim());
                                }
                                if (!temp3.Contains(dtTemp.Rows[intRowIndex]["AbsentDate"].ToString().Trim()))
                                {
                                    blnIsMatchFirst = true;
                                    temp3.Add(dtTemp.Rows[intRowIndex]["AbsentDate"].ToString().Trim());
                                }
                                else
                                {
                                    if (blnIsFtime == true)
                                    {
                                        blnIsMatchFirst = true;
                                    }
                                    else
                                    {
                                        blnIsMatchFirst = false;
                                        temp3.Add(dtTemp.Rows[intRowIndex]["AbsentDate"].ToString().Trim());
                                    }

                                }

                                if (dtTemp.Rows[intRowIndex]["Reason"].ToString().Trim() == "")
                                    dtTemp.Rows[intRowIndex]["Reason"] = "Not Defined";
                                if (dtTemp.Rows[j]["Reason"].ToString().Trim() == "")
                                    dtTemp.Rows[j]["Reason"] = "Not Defined";


                                if (blnIsMatchFirst)
                                {

                                    temp1 = "<b>" + dtTemp.Rows[intRowIndex]["UserName"].ToString().Trim() + " : " + "</b>" + dtTemp.Rows[intRowIndex]["Reason"].ToString().Trim();
                                    temp2 = "<b>" + dtTemp.Rows[j]["UserName"].ToString().Trim() + " : " + "</b>" + dtTemp.Rows[j]["Reason"].ToString().Trim();
                                    temp2 = temp1 + "  ,  " + temp2;
                                }
                                else
                                {
                                    if (intRowIndex > j)
                                    {
                                        temp1 = "<b>" + dtTemp.Rows[intRowIndex]["UserName"].ToString().Trim() + " : " + "</b>" + dtTemp.Rows[intRowIndex]["Reason"].ToString().Trim();
                                        temp2 = dtTemp.Rows[j]["Reason"].ToString().Trim() + "  ,  " + temp1;
                                    }
                                    else
                                    {
                                        temp1 = "<b>" + dtTemp.Rows[j]["UserName"].ToString().Trim() + " : " + "</b>" + dtTemp.Rows[j]["Reason"].ToString().Trim();
                                        temp2 = dtTemp.Rows[intRowIndex]["Reason"].ToString().Trim() + "  ,  " + temp1;
                                    }

                                }
                                temp = dtTemp.Rows[intRowIndex]["UserName"].ToString().Trim() + "  ,  " + dtTemp.Rows[j]["UserName"].ToString().Trim();



                                dtTemp.Rows[intRowIndex]["UserName"] = temp;
                                dtTemp.Rows[intRowIndex]["Reason"] = temp2.Replace("@", "'");
                                dtTemp.Rows[j].Delete();
                                // dtTemp.Rows[j].AcceptChanges();
                                j = j - 1;
                                dtTemp.AcceptChanges();

                                blnIsMatchFirst = false;
                                blnIsFtime = false;
                            }


                        }
                    }

                    //Datatype conversion for AbsentDate Column in dtTemp

                    dtTemp3 = dtTemp.Clone();

                    foreach (DataColumn dc in dtTemp.Columns)
                    {
                        if (dc.ColumnName == "AbsentDate")
                        {
                            dc.DataType = typeof(DateTime);
                        }
                    }

                    foreach (DataRow dr in dtTemp.Rows)
                    {
                        DateTime ldt = Convert.ToDateTime(dr["AbsentDate"].ToString());
                        dr["AbsentDate"] = ldt.ToString("yyyy/MM/dd hh:mm:ss.fff");
                        dtTemp3.ImportRow(dr);
                    }

                    //End Datatype conversion for AbsentDate Column in dtTemp
                }
            }

        }

        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "fnGetPast10DaysLeaveDetails");
        }
        //lblError.Text = ex.Message;
        // lblError.Visible = true;

        dtTemp3.DefaultView.Sort = "AbsentDate Desc";
        return dtTemp3;

    }



    protected void chkbHPL_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            if (sblnIsChecked == false)
            {
                sblnIsChecked = true;
                chkbHPL.Checked = true;
                objLeave.IsHpl = 0;
            }
            else
            {
                chkbHPL.Checked = false;
                sblnIsChecked = false;
                objLeave.IsHpl = 1;
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, null);
        }
    }
    public void fnSetPaidORUnpaid()
    {
        try
        {
            if (Convert.ToInt32(ddlLeaveTypePOrNP.SelectedValue) == 1)
                strPONP = "Paid";
            else
                strPONP = "Unpaid";
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, null);
        }
    }

    public void fnSetIsHPLORNot()
    {
        try
        {
            if (chkbHPL.Checked)
                objLeave.IsHpl = 1;
            else
                objLeave.IsHpl = 0;
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, null);
        }
    }



}
