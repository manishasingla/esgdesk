using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
public partial class Admin_frmEmpLeaveDetails : System.Web.UI.Page
{
    clsEmployee_Logic1 objEmployee = new clsEmployee_Logic1();
    clsLeave_Logic objLeave = new clsLeave_Logic();
    String dtDayOfLeave = "";
    static DateTime dtDayOfLeave1 = System.DateTime.Now;
    static DataView dv = null;
    static string strLeaveType = "";
    static string strPONP = "";
    private string strError = "No records found.";

    DateTime dt = new DateTime();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request["id"] == null)
            {
                Response.Redirect("EmployeeList.aspx?id=EL");
            }

            if (!IsPostBack)
            {
                //BindData();
                BindData1();
                BindLeavetype();
                SetPaidUnpaidDDL();
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "frmLeaveEditByEmp : Page_Load event");
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }

    }
    void BindData()
    {
        try
        {
        objEmployee.UserName = Session["UserName"].ToString();
        objEmployee.GetEmployeeDetails();  // GetEmployeeDetails method Set Value for employee Contact no, Address, emailId etc.
        txtUserName.Text = Session["UserName"].ToString();
        txtContactNo.Text = objEmployee.ContactNo;
        txtAddress.Text = objEmployee.Address;
        txtEmailId.Text = objEmployee.EmailId;
        txtDesig.Text = objEmployee.Designation;
        objLeave.UserName = Session["UserName"].ToString();
        objLeave.GetTotalBalanceDaysInOne(DateTime.Now.Year);
        double dblHPL = 2 * (objLeave.HalfPaidBalanceLeaves);
        txtCLBal.Text = objLeave.CasualBalanceLeaves.ToString();
        txtMLBal.Text = objLeave.MedicalBalanceLeaves.ToString();
        txtEL.Text = objLeave.EarnBalanceLeaves.ToString();
        txtHPLBal.Text = dblHPL.ToString();
        txtAbsentDate.Text = Convert.ToDateTime(Session["AbsentDate"].ToString()).ToString("dd/MM/yyyy");
        if (Session["Reason"] != null)
            txtReason.Text = Session["Reason"].ToString().Replace("@", "'");
        else
            txtReason.Text = "Not Defined.";
        UpdateDDLLeaveType();
        UpdateDDLPOrNP();
       }
         catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "frmLeaveEditByEmp : BindData Method");
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }
        //objEmployee.EmpId = Convert.ToInt32(Request["EId"]);
    }



    public void BindData1()
    {
        try
        {
            objEmployee.Sort_On = "";
            string strOrderBy = "";
            objLeave.fnGetTotalAndUsedLeave(Request["id"].ToString(), DateTime.Now.Year);
            lblTotalBalance.Text = objLeave.UsedLeaves.ToString() + " / " + objLeave.AllotedLeaves.ToString();

            if (ViewState["Sort_On"] != null)
            {
                objEmployee.Sort_On = ViewState["Sort_On"].ToString() + " " + ViewState["Sort_By"].ToString();
                strOrderBy = ViewState["Sort_On"].ToString() + " " + ViewState["Sort_By"].ToString();

            }

            lblError.Visible = false;

            DateTime dtStartingDate = DateTime.Now.AddMonths(-6);
            DateTime dtEndingDate = DateTime.Now;
           
            objLeave.UserName = Request["id"].ToString();
            DataSet dsTemp = objLeave.getEmployeeAbsentDetails();

            if (dsTemp != null)
            {
                DataTable dtTemp = dsTemp.Tables[0];

                dv = new DataView(dtTemp);
                dv.Sort = strOrderBy;
                string strFilter = "UserName='" + Request["id"].ToString() + "'";
                dv.RowFilter = strFilter;

                gvAbsentDates.DataSource = dv;

                gvAbsentDates.DataBind();

                //  txtAbsentDate=
                if (dv.ToTable().Rows.Count == 0)
                {
                    this.Lbl_Pageinfo.Visible = false;
                    lblError.Visible = true;
                    lblError.Text = strError;
                }
                else
                {
                    Int16 intTo;
                    Int16 intFrom;
                    if (gvAbsentDates.PageSize * (gvAbsentDates.PageIndex + 1) < dv.ToTable().Rows.Count)
                    {
                        intTo = System.Convert.ToInt16(gvAbsentDates.PageSize * (gvAbsentDates.PageIndex + 1));
                    }
                    else
                    {
                        intTo = System.Convert.ToInt16(dv.ToTable().Rows.Count);
                    }
                    intFrom = System.Convert.ToInt16((gvAbsentDates.PageSize * gvAbsentDates.PageIndex) + 1);
                    this.Lbl_Pageinfo.Text = "Record(s) " + intFrom + " to " + intTo + " of " + dv.ToTable().Rows.Count.ToString();
                    this.Lbl_Pageinfo.Visible = true;
                }
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = "Unable to retrieve data.";
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "frmLeaveEditByEmp : BindData1 Method");
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {

    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("EmployeeList.aspx?id=EL");
    }


    protected void gvAbsentDates_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvAbsentDates.PageIndex = e.NewPageIndex;
            gvAbsentDates.DataSource = dv;
            gvAbsentDates.DataBind();
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "frmLeaveEditByEmp : gvAbsentDates_PageIndexChanging event");
        }

    }
    protected void gvAbsentDates_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gvAbsentDates_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DateTime dt = Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "AbsentDate"));
                string strcellText = dt.ToString("dd/MM/yyyy");
                e.Row.Cells[0].Text = strcellText;
                e.Row.Cells[1].Text = DataBinder.Eval(e.Row.DataItem, "Reason").ToString().Replace("@", "'");
                string strIsEHPL = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "IsEHPL"));
                if (strIsEHPL == "1")
                {
                    e.Row.Cells[4].Text = "Yes";
                }
                else
                {
                    e.Row.Cells[4].Text = "No";
                }

            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "frmLeaveEditByEmp : gvAbsentDates_RowDataBound event");
        }
       
        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //   // dt = Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "AbsentDate"));
        //   // string strcellText = dt.ToString("dd/MM/yyyy");
        //  // e.Row.Cells[0].Text = strcellText;
        //}

    }
    protected void gvAbsentDates_DataBound(object sender, EventArgs e)
    {
        // this.gvAbsentDates.Columns[1].Visible = false;
    }


    protected void gvAbsentDates_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            objEmployee.Sort_On = e.SortExpression;
            ViewState["Sort_On"] = objEmployee.Sort_On;
            if (ViewState["Sort_By"] == null)
                ViewState["Sort_By"] = "Asc";
            if (ViewState["Sort_By"].ToString() == "Asc")
            {
                ViewState["Sort_By"] = "Desc";
            }
            else
            {
                ViewState["Sort_By"] = "Asc";
            }

            BindData1();

        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "frmLeaveEditByEmp : gvAbsentDates_Sorting event");
        }
    }
    protected void ddlLeaveType_SelectedIndexChanged(object sender, EventArgs e)
    {
        //strLeaveType = ddlLeaveType.SelectedItem.Text;
        // GetLeaveBalance(strLeaveType);
    }


    void BindLeavetype()
    {
        try
        {
            DataSet dsLeavetype = objLeave.GetLeaveType();
            ddlLeaveType.DataSource = dsLeavetype.Tables[0];
            ddlLeaveType.DataTextField = "LeaveTypeName";
            ddlLeaveType.DataValueField = "LeaveTypeId";
            ddlLeaveType.DataBind();
            ddlLeaveType.Items.Insert(0, "Select");
            DataTable dtLeaveType = dsLeavetype.Tables[0];
            DataView dvLeaveIndexValue = new DataView(dtLeaveType);
            if (dvLeaveIndexValue.ToTable().Rows.Count > 0)
            {
                dvLeaveIndexValue.RowFilter = "LeaveTypeName='" + dvLeaveIndexValue[0]["LeaveTypeName"].ToString() + "'";
                int intddlLeaveTypeId = Convert.ToInt32(dvLeaveIndexValue[0]["LeaveTypeId"]);
                ddlLeaveType.SelectedIndex = intddlLeaveTypeId;
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "frmLeaveEditByEmp : BindLeavetype Method");
        }
    }


    public void SetPaidUnpaidDDL()
    {
        try
        {
            if (dv.Count > 0)
            {
                if (dv[0]["PaidOrNPaid"].ToString().Trim() != "" && dv[0]["PaidOrNPaid"] != null)
                {
                    if (dv[0]["PaidOrNPaid"].ToString() == "Paid")
                    {
                        ddlLeaveTypePOrNP.SelectedIndex = 1;
                    }
                    else
                    {
                        ddlLeaveTypePOrNP.SelectedIndex = 0;
                    }
                }
            }
        }

        catch
        {
        }
    }


    protected void gvAbsentDates_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {

            if (e.CommandName.ToUpper() == "UPD")
            {
                try
                {
                    string sortByAndArrangeBy = (e.CommandArgument).ToString();
                    lblStatusMsg.Text = "";
                    char[] separator = { '|' };
                    string[] sortByAndArrangeByArray = sortByAndArrangeBy.Split(separator);
                    Session["UserName"] = Request["id"].ToString();
                    string[] DateSpan1 = sortByAndArrangeByArray[1].Split(new string[] { "/" }, StringSplitOptions.None);
                    //string strNewDate =DateSpan1[2]+"/"+DateSpan1[1]+"/"+DateSpan1[0];
                    dtDayOfLeave1 = Convert.ToDateTime(sortByAndArrangeByArray[1]);
                    Session["AbsentDate"] = sortByAndArrangeByArray[1];
                    Session["LeaveType"] = sortByAndArrangeByArray[2];
                    Session["PaidOrNPaid"] = sortByAndArrangeByArray[3];

                    Session["Reason"] = sortByAndArrangeByArray[4];
                    BindData();
                    BindData1();
                    rightdiv.Visible = true;
                }
                catch (Exception ex)
                {
                    DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "frmLeaveEditByEmp : gvAbsentDates_RowCommand event Update command");
                }


            }


            else if (e.CommandName.ToUpper() == "DEL")
            {
                try
                {
                    string sortByAndArrangeBy1 = (e.CommandArgument).ToString();

                    char[] separator = { '|' };
                    string[] sortByAndArrangeByArray1 = sortByAndArrangeBy1.Split(separator);
                    Session["UserName"] = Request["id"].ToString();
                    Session["AbsentDate"] = sortByAndArrangeByArray1[1];
                    Session["LeaveType"] = sortByAndArrangeByArray1[2];
                    Session["PaidOrNPaid"] = sortByAndArrangeByArray1[3];
                    Session["IsEHPL"] = sortByAndArrangeByArray1[4];

                    UpdateDDLLeaveType();
                  
                    objLeave.UserName = Request["id"].ToString();
                    string strDT = sortByAndArrangeByArray1[1].ToString();
                    dtDayOfLeave1 = Convert.ToDateTime(sortByAndArrangeByArray1[1]);
                    int intYearOfLeave = dtDayOfLeave1.Year;
                    string[] DateSpan = sortByAndArrangeByArray1[1].Split(new string[] { "/" }, StringSplitOptions.None);
                    objLeave.LeaveTypeId = ddlLeaveType.SelectedIndex;
                    int intNoOfRowDeleted = objLeave.DeleteLeaves(dtDayOfLeave1, objLeave.UserName, objLeave.LeaveTypeId, Convert.ToInt32(sortByAndArrangeByArray1[4]));

                    if (intNoOfRowDeleted > 0)
                    {
                        objLeave.GetBalanceDays(intYearOfLeave);// Here we update leave balance taken by employee respect to category wise
                        // And Total balance
                        rightdiv.Visible = false;
                        gvAbsentDates.DataBind();
                        BindData();
                        BindData1();
                    }
                    else
                    {
                        lblStatusMsg.Text = "Operation was not successful please try again.";
                    }

                }
                catch (Exception ex)
                {
                    DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "frmLeaveEditByEmp : gvAbsentDates_RowCommand event Delete Command ");
                }


            }
        }


        catch (Exception ex)
        {
            lblStatusMsg.Text = ex.Message;
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "frmLeaveEditByEmp : gvAbsentDates_RowCommand event");
        }

    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {


    }
    protected void gvAbsentDates_RowEditing(object sender, GridViewEditEventArgs e)
    {

    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            lblStatusMsg.Text = "";
            int intNoOfRowEditted = 0, i = 0;
            DateTime DTSDate = DateTime.Now, DTEDate = DateTime.Now;
          
            objEmployee.UserName = Request["id"].ToString();
            objLeave.UserName = Request["id"].ToString();
            strLeaveType = ddlLeaveType.SelectedItem.Text;
            strPONP = ddlLeaveTypePOrNP.SelectedItem.Text;
            string strReason = "";
            if (txtReason.Text.Trim() != "")
                strReason = txtReason.Text.Replace("'", "@");
            intNoOfRowEditted = objLeave.UpdateLeaves(dtDayOfLeave1, objEmployee.UserName, strPONP, strReason, strLeaveType);

            if (intNoOfRowEditted >= 1)
            {
                Random _r = new Random();
                objLeave.ApplicationNo = _r.Next();
                objLeave.UserName = txtUserName.Text;
                if (ddlLeaveType.SelectedItem.Text != "Select")
                    objLeave.LeaveTypeId = Convert.ToInt32(ddlLeaveType.SelectedValue);
                gvAbsentDates.DataBind();
                BindData1();
                // Session["AbsentDate"] = dv[0]["AbsentDate"].ToString();
                Session["PaidOrNPaid"] = ddlLeaveTypePOrNP.SelectedItem.Text;

                BindData();
                rightdiv.Visible = false;
                lblStatusMsg.Text = "Record has been updated.";
                lblError.Visible = true;
                lblError.Text = "Record has been updated.";
                i++;
            }
            else
            {
                lblStatusMsg.Text = "There is a Error in reporting.";
                lblError.Text = "There is a Error in reporting.";
            }

        }

        catch (Exception ex)
        {

            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "frmLeaveEditByEmp : btnUpdate_Click event");
        
            lblStatusMsg.Text = "There is a Error:" + ex;
        }

    }
    protected void gvAbsentDates_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int categoryID = (int)gvAbsentDates.DataKeys[e.RowIndex].Value;

    }
    protected void gvAbsentDates_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }
    public void UpdateDDLLeaveType()
    {
        try
        {
            if (Session["LeaveType"].ToString() == "CL")
            {
                ddlLeaveType.SelectedIndex = 1;

            }
            else if (Session["LeaveType"].ToString() == "ML")
            {
                ddlLeaveType.SelectedIndex = 2;
            }
            else if (Session["LeaveType"].ToString() == "EL")
            {
                ddlLeaveType.SelectedIndex = 3;
            }
            else if (Session["LeaveType"].ToString() == "HPL")
            {
                ddlLeaveType.SelectedIndex = 4;
            }

            else if (Session["LeaveType"].ToString() == "Extra Leave")
            {
                ddlLeaveType.SelectedIndex = 5;
            }
        }
        catch (Exception ex)
        {

            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "frmLeaveEditByEmp : UpdateDDLLeaveType Method");

            lblStatusMsg.Text = "There is a Error:" + ex;
        }

    }
    public void UpdateDDLPOrNP()
    {
        try
        {
            if (Session["PaidOrNPaid"].ToString() == "Paid")
                ddlLeaveTypePOrNP.SelectedIndex = 1;
            else
                ddlLeaveTypePOrNP.SelectedIndex = 0;
        }
        catch (Exception ex)
        {

            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "frmLeaveEditByEmp : UpdateDDLPOrNP Method");

            lblStatusMsg.Text = "There is a Error:" + ex;
        }
    }


}
