<%@ Page Language="C#" MasterPageFile="~/AdminMasterMenu.master" AutoEventWireup="true"
    CodeFile="frmManageHolidays.aspx.cs" Inherits="Admin_frmEmpLeaveDetails" Title="Untitled Page" %>
    <%@ Register Assembly="GMDatePicker" Namespace="GrayMatterSoft" TagPrefix="cc1" %>
<%@ Register Assembly="GMDatePicker" Namespace="GrayMatterSoft" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server" >

    <div class="publicloginTable" style="height:81% ;background-color:#C2CFDC;" >

    <div style="float:left; width: 47%; height:100%" class="publicloginTable" id="leftdiv" runat="server"> 
    <table>
    
    <tr>
    <td>
       <asp:Panel ID="pnlAdmin" runat="server" Visible="False" Font-Size="Small" 
            GroupingText="Manage Holidays">
        <table width="100%" >
    <tr><td class="LoginTitle" colspan="3">&nbsp;</td></tr>
    <tr>
                        <td align="left" valign="top" colspan="3">
                            &nbsp;</td>
                    </tr>
                    <tr><td>Select a date </td><td>:</td>
                    <td>
                           
                            <cc1:GMDatePicker ID="GMDDate" runat="server" CalendarOffsetX="-127px" DateFormat="dd-MMM-yyyy"
                                InitialValueMode="Null" Width="210px">
                            </cc1:GMDatePicker>
                       
                      </td></tr>
                            <tr><td colspan="3"></td>
                         </tr>
                     <tr><td>Holiday&#39;S name </td><td>:</td><td>
                         <asp:TextBox ID="txtHolidayName" runat="server" Width="289px"></asp:TextBox>
                         </td></tr>
                          <tr><td colspan="3"></td>
                         </tr>
            <%--   <tr><td>ML</td><td>:</td><td>
                         <asp:TextBox ID="txtML" runat="server" Width="50px" Wrap="False"></asp:TextBox>
                         </td></tr>
                         <tr><td colspan="3"></td>
                         </tr>
                     <tr><td>EL</td><td>:</td><td>
                         <asp:TextBox ID="txtEL" runat="server" Width="50px"></asp:TextBox>
                         </td></tr>
                         <tr><td colspan="3"></td>
                         </tr>
                      <tr><td>HPL</td><td>:</td><td>
                          <asp:TextBox ID="txtHPL" runat="server" Width="50px"></asp:TextBox>
                          </td></tr>
                          <tr><td colspan="3"></td>
                         </tr>--%>
                         <tr><td>
                             <asp:Button ID="btnUpdate" runat="server" Text="Update Now" 
                                 onclick="btnUpdate_Click" Visible="False" />
                             </td><td>&nbsp;</td><td>
                             <asp:Button ID="Button1" runat="server" Text="Add Now" 
                                 onclick="Button1_Click" />
                         </td></tr>
                        
                    </table>

    
     </asp:Panel>
    </td>

    <td></td>
     
       

    <td style="width: 372px" >
    
    
                    <div align="right" valign ="top"> <asp:GridView ID="gvHolidaysDetails" runat="server"    
                            AutoGenerateColumns="False" Height="100%" PageSize="20" 
                            style="margin-left: 20px; vertical-align:top" Width="104%" 
                            onrowcommand="gvHolidaysDetails_RowCommand" 
                            onrowdatabound="gvHolidaysDetails_RowDataBound" >
                    <Columns> 
                      <asp:TemplateField HeaderText=" Date" SortExpression="LDate">
                                        <HeaderStyle Width="20%" />
                                        <ItemTemplate>
                                            <%#Eval("LDate")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="HoliDay Name" SortExpression="LName">
                                    <HeaderStyle Width="60%" />
                                        <ItemTemplate>
                                            <%#Eval("LName")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                     <asp:TemplateField HeaderText="Delete" SortExpression="LDate" Visible="false">
                                        <HeaderStyle Width="20%" />
                                        <ItemTemplate>
                                       <asp:LinkButton ID="lnkBtnDelete" Text="Delete" runat="server" CommandArgument= '<%#Eval("LDate")%>' CommandName="Del"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Update" SortExpression="LName" Visible="false">
                                        <HeaderStyle Width="20%" />
                                        <ItemTemplate>
                                       <asp:LinkButton ID="lnkBtnUpdate" Text="Update" runat="server" CommandArgument= '<%#Eval("LDate")%>' CommandName="Upd"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                      </Columns>
</asp:GridView></div>
                            <div>
                              <asp:Label ID="lblSuccessMsg" runat="server"></asp:Label>
</div>
    
    
    </td>
    </tr>
    
    </table>
    
    
                               
                    <span>
        &nbsp;&nbsp;&nbsp; 
                        </span>
                    </div>
</div>

</asp:Content>

