<%@ Page Language="C#" MasterPageFile="~/Admin/LMSAdmin/AdminMasterMenu.master" AutoEventWireup="true" CodeFile="frmMonthlyLeaveTransactions.aspx.cs" Inherits="Admin_frmMonthlyLeaveTransactions" Title="Employee's leave count between two dates" %>

<%@ Register Assembly="GMDatePicker" Namespace="GrayMatterSoft" TagPrefix="cc1" %>
<%@ Register Assembly="GMDatePicker" Namespace="GrayMatterSoft" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script type="text/javascript" language="javascript">
function keypressHandler(evt) {

    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode == 13)
        var btn = document.getElementById('<%=btnSearch.ClientID%>');
    btn.click();
}

function EnterEvent(e) {
    if (e.keyCode == 13) {
        var btn = document.getElementById('<%=btnSearch.ClientID%>');
        btn.click();
    }
}

</script>
    <table style="width:81%" height="373" class="publicloginTable" onkeypress="javascript:keypressHandler(event)">
    
                <tr>
                        <td colspan="6" class="LoginTitle"><h1 style="color:Black">
                            Employee's leave count between two dates </h1>
                        </td>
                          
                           
                    </tr>  
        <tr>
            <td valign="top" align="left">
                <table width="100%" class="admintablestyle">
                    <tr>
                        <td colspan="6" class="LoginTitle">
                            &nbsp;Leave transactions
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 17%">
                            User name</td>
                        <td style="width: 2%">
                            :</td>
                        <td style="width: 18%">
                            <asp:TextBox ID="txtUserName" runat="server"  onkeypress="return EnterEvent(event)"></asp:TextBox>
                        </td>
                        <td style="height: 28px">
                            Page size</td>
                        <td style="height: 28px">
                            :</td>
                        <td style="height: 28px">
                            <asp:TextBox ID="txtPageSize" OnKeypress="return onlyNumbers(event)" runat="server" Width="50px"></asp:TextBox>
                            <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtPageSize"
                                CssClass="lblerror" ErrorMessage="Invalid Number" MaximumValue="999" MinimumValue="1"
                                 Type="Integer"></asp:RangeValidator>
                        </td>
                        <td>
                            <asp:Button ID="btnSearch" runat="server" Text="Search" Width="55px" OnClick="btnSearch_Click" /></td>
                    </tr>
                    <tr><td valign="top">Starting date</td><td>:</td><td>
               
     <asp:TextBox ID="GMDStartDate" runat="server"  onkeypress="javascript:EnterEvent(event)"></asp:TextBox>
                               <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" 
                            TargetControlID="GMDStartDate"
                            Format="dd/MM/yyyy" PopupButtonID="GMDStartDate" >
                            </ajaxToolkit:CalendarExtender><br />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                ErrorMessage="Please select a start date" ControlToValidate="GMDStartDate"></asp:RequiredFieldValidator>                    
                       <br /> <asp:CompareValidator ID="CompareValidator2" runat="server" 
                            ErrorMessage="Please enter a valid  date" Operator="DataTypeCheck" 
                            ControlToValidate="GMDStartDate" Type="Date"></asp:CompareValidator>
                       </td><td style="width: 18%" valign="top">Ending date</td><td style="width: 2%">:</td>
                       <td valign="top">
                      
                      <asp:TextBox ID="GMDEndDate" runat="server"  onkeypress="javascript:EnterEvent(event)" ></asp:TextBox>
                             <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" 
                            TargetControlID="GMDEndDate"
                            Format="dd/MM/yyyy"  PopupButtonID="GMDEndDate">
                            </ajaxToolkit:CalendarExtender><br />
                            <asp:CompareValidator ID="CompareValidator1" runat="server" Operator="GreaterThanEqual" 
                                ErrorMessage="End date should be greater or equal to start date." 
                                ControlToCompare="GMDStartDate" ControlToValidate="GMDEndDate" Type="Date"></asp:CompareValidator> <br />
                                  <asp:CompareValidator ID="CompareValidator3" runat="server" 
                            ErrorMessage="Please enter a valid date" Operator="DataTypeCheck" 
                            ControlToValidate="GMDEndDate" Type="Date"></asp:CompareValidator></td>
                            </tr>
                    <tr>
                        
                        <td colspan="3" style="height: 28px">
                            </td>
                    </tr>

                    <tr>
                        <td colspan="3">
                            &nbsp;
                        </td>
                        <td colspan="3">
                         </td>   
                    </tr>
                    <tr>
                        <td colspan="6">
                        <table border="0" width="100%" ><tr>
                        <td align="left" style="width: 321px">
                            &nbsp;</td> 
                        <td align="right">
                            &nbsp;</td></tr>
                       <tr><td colspan="2">                       
                           <asp:GridView ID="gvLeaveDetails" runat="server" AllowPaging="true" 
                               AllowSorting="true"  AutoGenerateColumns="False" Width="100%"  
                               OnSorting="gvLeaveDetails_Sorting" OnPageIndexChanging="gvLeaveDetails_PageIndexChanging" 
                               onrowcommand="gvLeaveDetails_RowCommand" PageSize="20" 
                               onrowdatabound="gvLeaveDetails_RowDataBound">
                                <Columns>
                                  
                 <asp:TemplateField HeaderText="User name" SortExpression="UserName">
                                   <HeaderStyle Width="30%" />
                                    <ItemTemplate>       
             
                  <asp:Label ID="lblUserName" runat="server" Text='<%#Eval("UserName")%>'></asp:Label>
                                   </ItemTemplate>
                                 </asp:TemplateField>
                              
                                     <asp:TemplateField HeaderText="Absent days">
                                    <ItemTemplate>
                                    <asp:Label ID="lblTotalBalance" runat="server"></asp:Label>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView> </td></tr>
                            
                        
                   
                    <tr>
                        <td width="50%">
                            <asp:Label ID="lblError" CssClass="lblerror"  runat="server"></asp:Label></td>
                       <td align="right">
                           <asp:Label ID="Lbl_Pageinfo" runat="server" Text=""></asp:Label></td> 
                        </tr> 
                    
                </table>
                 </td></tr>
                 </table>
    
    
    
    </td>
    </tr>
    </table>

</asp:Content>

