using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Admin_edit_comment : System.Web.UI.Page
{
    int id;
    int tc_id;
    string sql = "";
    string task_action = "";
    string emailSubject = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Session["admin"] == null || Session["admin"].ToString() == "")
        //{
        //    Session["returnUrl"] = (Request.QueryString["id"] == null || Request.QueryString["id"].ToString() == "" || Request.QueryString["tcid"] == null || Request.QueryString["tcid"].ToString() == "" ? "tasks.aspx" : "edit_task.aspx?id=" + Request.QueryString["id"].ToString() + "&tcid=" + Request.QueryString["tcid"].ToString());
        //    Response.Redirect("login.aspx");
        //    return;
        //}
        divMessage1.InnerHtml = "";
        divMessage.InnerHtml = "";

        string task_id = Request["id"];
        string comment_id = Request["tcid"];
        //string task_id = "5953";
        //string comment_id = "24353";

        if (task_id == null || task_id == "0" || task_id == "" || comment_id == null || comment_id == "0" || comment_id == "")
        {
            id = 0;
            tc_id = 0;
        }
        else
        {
            if (DatabaseHelper.is_int(task_id) && DatabaseHelper.is_int(comment_id))
            {
                id = Convert.ToInt32(task_id);
                tc_id = Convert.ToInt32(comment_id);
            }
            else
            {
                // Display an error because the bugid must be an integer
                DivEntry.Visible = false;
                return;
            }
        }

        if (!Page.IsPostBack)
        {
            if (id == 0 || tc_id == 0)
            {
                btnUpdate.Enabled = false;
            }
            else
            {
                // Get this entry's data from the db and fill in the form

                lnkBack.HRef = "edit_task.aspx?id=" + id.ToString(); 
                BindData();
            }
        }
    }

    public void BindData()
    {
        sql = sql = "select * from task_comments where deleted <> 1 and  task_id = " + id.ToString() + " and tc_id="+ tc_id.ToString();
        
        DataSet dsTask = DatabaseHelper.getDataset(sql);

        if (dsTask == null || dsTask.Tables.Count <= 0 || dsTask.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "<br>Comment not found.<br><br><a href='tasks.aspx'>View task</a>";
            DivEntry.Visible = false;
        }
        else
        {
            //================================================================
            //ftbComment.Text = dsTask.Tables[0].Rows[0]["comment"].ToString();
            ftbComment.Content = dsTask.Tables[0].Rows[0]["comment"].ToString();
            //================================================================
            hdnShortDescr.Value = (string)DatabaseHelper.executeScalar("select short_desc from tasks where task_id = "+ id.ToString());
        }
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        //======================================================================
        if (ftbComment.Content.Trim() == "")
        //======================================================================
        {
            divMessage1.InnerHtml = "Comment is required.";
            return;
        }
        else
        {
            sql = " update tasks set ";
            sql += " [last_updated_user]='" + Session["admin"].ToString().Replace("'", "''") + "',";
            sql += " [last_updated_date]=getdate()";
            sql += " where task_id = " + id.ToString() + "; ";
            //===========================================================
            //if (ftbComment.Text.Trim() != "")
            if (ftbComment.Content.Trim() != "")
                
            //===========================================================
            {
                if (chkAnswer.Checked == true)
                {
                    //============================================================================================================
                    //sql += " update task_comments set [comment]='" + ftbComment.Text.Trim().Replace("'", "''") + "', qflag=0 ";
                    sql += " update task_comments set [comment]='" + ftbComment.Content.Trim().Replace("'", "''") + "', qflag=0 ";
                    //============================================================================================================
                    sql += " where task_id=" + id.ToString() + " and tc_id=" + tc_id.ToString() + ";";
                }
                else
                {
                    //===========================================================================================================
                   //sql += " update task_comments set [comment]='" + ftbComment.Text.Trim().Replace("'", "''") + "'";
                    sql += " update task_comments set [comment]='" + ftbComment.Content.Trim().Replace("'", "''") + "'";
                    //===========================================================================================================
                    sql += " where task_id=" + id.ToString() + " and tc_id=" + tc_id.ToString() + ";";
                }
            }

            int intResult = DatabaseHelper.executeNonQuery(sql);

            if (intResult != 0)
            {
                string base_sql = @" insert into task_updates([task_id],[username],[post_date],[comment]) 
                            values(" + id.ToString() + ",'" + Session["admin"].ToString().Replace("'", "''") + "',getdate(),'Comment " + tc_id.ToString() + "  has been updated.'); ";

                intResult = DatabaseHelper.executeNonQuery(base_sql);
                
                base_sql = @"delete from read_comments where [tc_id] = " + tc_id.ToString() + "; ";
                base_sql += " insert into read_comments([tc_id],[username]) ";
                base_sql += " values(" + tc_id.ToString() + ",'" + Session["admin"] + "')";

                DatabaseHelper.executeNonQuery(base_sql);

                if (chkAnswer.Checked == true)
                {
                    task_action = ((string)DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + Session["admin"].ToString() + "'")) + " has answered question comment (" + tc_id.ToString() + ") to task ";
                }
                else
                {
                    task_action = ((string)DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + Session["admin"].ToString() + "'")) + " has updated comment (" + tc_id.ToString() + ") to task ";
                }
                
                sendNotification(id.ToString());
                
                Response.Redirect("edit_task.aspx?id="+ id.ToString());
            }
            else
            {
                divMessage1.InnerHtml = "Task was not updated.";
            }
        }
    }

    void sendNotification(string strTaskId)
    {
        sql = "select email,PR_EmailNotification ";
        sql += " from users, task_subscriptions ";
        sql += " where users.username = task_subscriptions.username ";
        sql += " and task_subscriptions.task_id =" + strTaskId.ToString() + " and task_subscriptions.username <> '" + Session["admin"].ToString() + "'";

        DataSet ds = DatabaseHelper.getDataset(sql);
        object TaskPR = DatabaseHelper.executeScalar("select priority from tasks where task_id = " + strTaskId.ToString() + "");

        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            string toEmails = "";

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (toEmails == "")
                {
                    string reqPR = "";
                    string[] strPR = ds.Tables[0].Rows[i]["PR_EmailNotification"].ToString().Split(new char[] { ';' });
                    for (int j = 0; j < strPR.Length - 1; j++)
                    {
                        reqPR = strPR[j];
                        if (reqPR == "All")
                        {
                            toEmails = ds.Tables[0].Rows[i]["email"].ToString() + ";";
                        }
                        else if (reqPR == TaskPR.ToString())
                        {
                            toEmails = ds.Tables[0].Rows[i]["email"].ToString() + ";";
                        }

                    }



                   /// toEmails = ds.Tables[0].Rows[i]["email"].ToString();
                }
                else
                {
                    string reqPR = "";
                    string[] strPR = ds.Tables[0].Rows[i]["PR_EmailNotification"].ToString().Split(new char[] { ';' });
                    for (int j = 0; j < strPR.Length - 1; j++)
                    {
                        reqPR = strPR[j];
                        if (reqPR == "All")
                        {
                            toEmails += ds.Tables[0].Rows[i]["email"].ToString() + ";";
                        }
                        else if (reqPR == TaskPR.ToString())
                        {
                            toEmails += ds.Tables[0].Rows[i]["email"].ToString() + ";";
                        }

                    }


                    //toEmails += ";" + ds.Tables[0].Rows[i]["email"].ToString();
                }
            }
            
            string strHtml = generateAdminEmail(strTaskId);

            if (chkAnswer.Checked == true)
            {
                emailSubject = " Answer has been submitted.";
            }
            else
            {
                emailSubject = hdnShortDescr.Value;
            }
            if (chkEmailnotify.Checked)
            {
                bool flag = DatabaseHelper.sendEmailTasks(toEmails, "Task: " + strTaskId + " was updated - " + emailSubject + " (Task ID:" + strTaskId.ToString() + ")", strHtml);
            }
        }
        else
        {

        }
    }

    private string generateAdminEmail(string strTaskId)
    {
        DataSet dsTask = DatabaseHelper.getDataset("select * from tasks where task_id=" + strTaskId);
        string strBody = "";
        if (dsTask == null || dsTask.Tables.Count <= 0 || dsTask.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "Task not found.<br><br><a href='tasks.aspx'>View tasks</a>";
        }
        else
        {
            strBody += "Task: <span style=\"color:red\">";

            strBody += task_action;

            strBody += " (<a style=\"color:red\" href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_task.aspx?id=" + strTaskId + "'>" + strTaskId + "</a>) </span> " + DateTime.Parse(dsTask.Tables[0].Rows[0]["last_updated_date"].ToString()).ToString("dd MMM h:mm tt") + "<br><br>";

            strBody += "<a href='"  + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_task.aspx?id=" + strTaskId + "'>" + dsTask.Tables[0].Rows[0]["short_desc"].ToString() + "</a>&nbsp;&nbsp;&nbsp;(" + dsTask.Tables[0].Rows[0]["priority"].ToString() + " : " + dsTask.Tables[0].Rows[0]["status"].ToString() + ")<br><br>";
            strBody += "Emp: " + ((string)DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + dsTask.Tables[0].Rows[0]["assigned_to_user"].ToString() + "'")) + "<br>";
            strBody += "Project: " + dsTask.Tables[0].Rows[0]["project"].ToString() + "<br>";
            strBody += "Relevant URL: <a href='"  + dsTask.Tables[0].Rows[0]["Relevant_URL"].ToString() + "' target='_blank'>" + dsTask.Tables[0].Rows[0]["Relevant_URL"].ToString() + "</a><br>";
            strBody += "<br><br>";

            sql = "Select * from task_comments where task_id=" + strTaskId.ToString() + " and deleted <> 1 order by tc_id desc";

            DataSet dsTaskDetails = DatabaseHelper.getDataset(sql);

            strBody += "<table cellspacing=\"0\" border=\"1\" style=\"width: 99%;border-collapse: collapse;\">";
            strBody += "<tbody>";

            for (int i = 0; i < dsTaskDetails.Tables[0].Rows.Count; i++)
            {
                strBody += "<tr>";
                strBody += "<td>";
                strBody += "<div style=\"border: 1px none Green;\">";
                strBody += "<table width=\"100%\">";
                strBody += "<tbody>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                if (dsTaskDetails.Tables[0].Rows[i]["qflag"].ToString() == "1")
                {
                    strBody += "<span style=\"color:red;\">Question posted by " + dsTaskDetails.Tables[0].Rows[i]["username"].ToString() + " on " + DateTime.Parse(dsTaskDetails.Tables[0].Rows[i]["post_date"].ToString()).ToString("dd MMM h:mm tt") + "</span>";
                }
                else
                {
                    strBody += "<span style=\"color: Green;\">comment " + dsTaskDetails.Tables[0].Rows[i]["tc_id"].ToString() + " posted by " + dsTaskDetails.Tables[0].Rows[i]["username"].ToString() + " on " + DateTime.Parse(dsTaskDetails.Tables[0].Rows[i]["post_date"].ToString()).ToString("dd MMM h:mm tt") + "</span>";
                }
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\"> </td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span>" + dsTaskDetails.Tables[0].Rows[i]["comment"].ToString() + "</span>";
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "</tbody>";
                strBody += "</table>";
                strBody += "</div>";
                strBody += "</td>";
                strBody += "</tr>";
            }
            strBody += "</tbody>";
            strBody += "</table>";
        }
        return strBody;
    }
}
