<%@ Page Language="C#" AutoEventWireup="true" CodeFile="News.aspx.cs" Inherits="Admin_News" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    News
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" EnablePageMethods="true" runat="server">
        </asp:ScriptManager>
        <div>
            <table>
                <tr>
                    <td>
                        News Heading</td>
                    <td>
                        <asp:TextBox ID="txtnews" runat="server" TextMode="MultiLine" Width="546px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        News Description</td>
                    <td>
                        <%--<FTB:FreeTextBox ID="ftbnews" runat="server" Height="400px" Width="800px">
                        </FTB:FreeTextBox>--%>
                        <telerik:RadEditor ID="ftbnews" runat="server" Height="300px" Width="100%">
                                    <Snippets>
                                        <telerik:EditorSnippet Name="Product Template" Value="<table><tr><td><div style='width:300px;border:2px outset #D8D2BD;padding:34px;font-family:Arial, Verdana; font-size:11px;background- color:#F1EFE6;'>Dear ____________________,<br>Thank you for inquiring about ____________. Your request will be processed in 48 hours and shipped at the address you have provided.<b>Please, contact us if you have any problems.</b></div></td></tr></table>">            
                                        </telerik:EditorSnippet>
                                        <telerik:EditorSnippet Name="Signature" Value="<strong>John Smith</strong><br>Sales Manager<br>">
                                        </telerik:EditorSnippet>
                                    </Snippets>
                                    <Links>
                                        <telerik:EditorLink Name="Telerik" Href="http://www.telerik.com">
                                            <telerik:EditorLink Name="Telerik Products" Href="http://www.telerik.com/products"
                                                Target="_blank" />
                                            <telerik:EditorLink Name="Telerik Support Center" Href="http://www.telerik.com/support/home.aspx"
                                                Target="_blank" />
                                            <telerik:EditorLink Name="Telerik Community" Href="http://www.telerik.com/community/home.aspx"
                                                Target="_blank" ToolTip="Telerik Community" />
                                        </telerik:EditorLink>
                                    </Links>
                                    <Tools>
                                    </Tools>
                                    <ImageManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                    <MediaManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                    <FlashManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                    <TemplateManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                    <DocumentManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                    <Modules>
                                        <telerik:EditorModule Name="RadEditorStatistics" Visible="False" Enabled="true" />
                                        <telerik:EditorModule Name="RadEditorDomInspector" Visible="False" Enabled="true" />
                                        <telerik:EditorModule Name="RadEditorNodeInspector" Visible="False" Enabled="true" />
                                        <telerik:EditorModule Name="RadEditorHtmlInspector" Visible="False" Enabled="true" />
                                    </Modules>
                                    <Content>
                                    </Content>
                                </telerik:RadEditor>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
