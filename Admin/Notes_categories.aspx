<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Notes_categories.aspx.cs"
    Inherits="Admin_Notes_categories" EnableEventValidation="false" %>
    
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="Notifications.ascx" TagName="Notifications" TagPrefix="uc4" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        Note categories</title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
   <style type="text/css">
     table#DataGrid1_ctl00 tr th a
     {
      display:block;
      text-decoration:none;
      margin:0;
      padding:0;
      width:100%;
      height:100%;
     }
    </style>

</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <uc2:Admin_Header ID="Admin_Header1" runat="server" />
        <div id="Content">
            <div id="Content_98">
                <div class="titleText" style="margin: 10px 0 20px 0; display: none;">
                    Categories</div>
                <div>
                <uc4:Notifications ID="Notifications" runat="server" />
                </div>
                <div style="float: right; width: 100%; padding: 5px 0px; background: #ebebeb;" align="left">
                    <span>&nbsp;&nbsp;<asp:LinkButton ID="lnkAddNew" style="Border-bottom:1px dotted #000;float:left;" runat="server" OnClick="lnkAddNew_Click"
                        CausesValidation="False">Add new notes category</asp:LinkButton>
                        <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="False">&nbsp;|&nbsp;Back to notes categories</asp:LinkButton>
                    </span><span style="float: right"><b>Records: </b>
                        <asp:Label ID="LblRecordsno" runat="server" Text=""></asp:Label>&nbsp;&nbsp;</span></div>
                <div style="clear: both">
                </div>
                <div id="DivEntry" runat="server" visible="false">
                    <table>
                        <tr>
                            <td align="left" valign="top" style="width: 434px">
                                <table style="width: 424px">
                                    <tr>
                                        <td style="width: 424px">
                                            <table width="424" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="left" valign="top">
                                                        <img height="16" src="../images/loginBox_top.png" width="420" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="background: url(../images/loginBox_mid.png) repeat-y left top;">
                                                        <table width="100%" cellpadding="8" cellspacing="0">
                                                            <tr>
                                                                <td width="2">
                                                                </td>
                                                                <td class="whitetext1">
                                                                    Category ID:
                                                                </td>
                                                                <td>
                                                                    <asp:Label CssClass="whitetext2" ID="lblId" runat="server">New</asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                                <td class="whitetext1">
                                                                    Categorty name:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtName" runat="server" MaxLength="50"></asp:TextBox><cc1:ValidatorCalloutExtender
                                                                        ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator1">
                                                                    </cc1:ValidatorCalloutExtender>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName"
                                                                        ErrorMessage="Please enter category name.">&nbsp;*</asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                                <td class="whitetext1">
                                                                    Category type:
                                                                </td>
                                                                <td class="whitetext2">
                                                                    <asp:RadioButton ID="RdbFhs" runat="server" Text="FHS notes" GroupName="Notes" />
                                                                    &nbsp;
                                                                    <asp:RadioButton ID="RdbClient" runat="server" Text="Client notes" GroupName="Notes" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 100px">
                                                        <img height="16" src="../images/loginBox_btm.png" width="420" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 424px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td align="right" valign="top">
                                                        &nbsp;
                                                    </td>
                                                    <td align="right" valign="top">
                                                        <asp:Button ID="btnCreate" runat="server" Text="Create" OnClick="btnCreate_Click"
                                                            CssClass="blueBtns" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="divMessage" runat="server" style="color: Red; font-weight: bold">
                </div>
                <div>
                  <%--*************************************************************************************************************--%>
                    <telerik:RadGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" CellPadding="3" OnItemCommand="DataGrid1_ItemCommand" OnItemDataBound="DataGrid1_ItemDataBound" Width="100%">
                     <MasterTableView PagerStyle-AlwaysVisible="true">
                      <Columns>
                      <%--0--%>
                      
                      <telerik:GridTemplateColumn HeaderText="NoteCat ID">
                      <ItemTemplate>
                      <asp:Label id="lblNoteCatid" runat="server"  Text='<%#Bind("NoteCat_id") %>'></asp:Label>
                      </ItemTemplate>  
                      </telerik:GridTemplateColumn> 
                      
                      <%--1--%>
                      
                      <telerik:GridTemplateColumn HeaderText="Categoy name">
                      <ItemTemplate>
                      <asp:Label  ID="lblCategoyname" runat="server" Text='<%# Bind("Categoy_name") %>'>
                      </asp:Label>
                      </ItemTemplate> 
                      </telerik:GridTemplateColumn> 
                      <%--2--%>
                      
                      <telerik:GridTemplateColumn HeaderText="Category type">
                      <ItemTemplate>
                      <asp:Label id="lblCategorytype" runat="server"  Text='<%#Bind("Category_Type") %>'></asp:Label>
                      </ItemTemplate> 
                      </telerik:GridTemplateColumn>
                      
                      <%--3--%> 
                      
                      <telerik:GridTemplateColumn HeaderText="Edit">
                      <ItemTemplate>
                      <asp:LinkButton id="lnkedit" runat="server" CommandName="edit" Text="edit"></asp:LinkButton>
                      </ItemTemplate> 
                      </telerik:GridTemplateColumn> 
                      <%--4--%> 
                      
                      <telerik:GridTemplateColumn HeaderText="Delete">
                      <ItemTemplate>
                      <asp:LinkButton id="lbldelete" CommandName="delete" runat="server" Text="delete"></asp:LinkButton>
                      </ItemTemplate> 
                      </telerik:GridTemplateColumn> 
                     
                      </Columns> 
                      </MasterTableView> 
                      </telerik:RadGrid> &nbsp;
                      <%--*************************************************************************************************************--%>
                    
                    
                    
                    
                    
                    
                    <asp:HiddenField ID="prevName" runat="server" />
                </div>
            </div>
        </div>
    </div>
    <uc1:footer ID="Footer1" runat="server" />
    </form>
</body>
</html>
