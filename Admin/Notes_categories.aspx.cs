using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
using Telerik.Web;

public partial class Admin_Notes_categories : System.Web.UI.Page
{
    string sql = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = "categories.aspx";
            Response.Redirect("login.aspx");
            return;
        }

        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
           Response.Redirect("tasks.aspx");
           return;
        } 

        divMessage.InnerHtml = "";

        if (!Page.IsPostBack)
        {
            bindData();

        }
    }



    protected void btnCreate_Click(object sender, EventArgs e)
    {
        if (txtName.Text.Trim() == "")
        {
            divMessage.InnerHtml = "Please enter category name.";
            return;
        }

        if (btnCreate.Text == "Create")
        {
            string NoteType = "";
            if (RdbFhs.Checked)
            {
                NoteType = "FHS";
            }
            else if (RdbClient.Checked)
            {
                NoteType = "Client";
            }
            object objResult = DatabaseHelper.executeScalar("select count(*) from Notes_Categories where [Categoy_name]='" + txtName.Text.Trim() + "' and Category_Type='" + NoteType + "'");

            if (objResult.ToString() != "0")
            {
                divMessage.InnerHtml = "Category name alredy exist.";
                return;
            }
            string strSql = " insert into Notes_Categories([Categoy_name],[Category_Type]) ";
            strSql += " values('" + txtName.Text.Trim().Replace("'", "''") + "','" + NoteType.ToString() + "')";

            int intResult = DatabaseHelper.executeNonQuery(strSql);
            if (intResult != 0)
            {
                divMessage.InnerHtml = "Category was created.";
                clearControls();
            }
            else
            {
                divMessage.InnerHtml = "Category was not created.";
            }
        }
        else if (btnCreate.Text == "Edit")
        {
            string NoteType = "";

            if (RdbFhs.Checked)
            {
                NoteType = "FHS";
            }
            else if (RdbClient.Checked)
            {
                NoteType = "Client";
            }

            string strSql = " update Notes_Categories ";
            strSql += " set [Categoy_name]= '" + txtName.Text.Trim().Replace("'", "''") + "',";
            strSql += " [Category_Type] = '" + NoteType.ToString() + "'";
            strSql += " where NoteCat_id = " + lblId.Text.Trim();
            int intResult = DatabaseHelper.executeNonQuery(strSql);
            if (intResult != 0)
            {
                if (prevName.Value != txtName.Text.Trim().Replace("'", "''"))
                {
                    strSql = "update Company_notes set Category_name = '" + txtName.Text.Trim().Replace("'", "''") + "' ";
                    strSql += " where Category_name ='" + prevName.Value.Replace("'", "''") + "' and Note_Type='" + NoteType.ToString() + "'";
                    intResult = DatabaseHelper.executeNonQuery(strSql);
                }

                divMessage.InnerHtml = "Category was updated.";
            }
            else
            {
                divMessage.InnerHtml = "Category was not updated.";
            }
            
        }
    }

    private void bindData()
    {
        string strSql = "select * from Notes_Categories";

        DataSet ds = DatabaseHelper.getDataset(strSql);

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                DataGrid1.DataSource = ds.Tables[0];
                PagedDataSource objds = new PagedDataSource();
                objds.DataSource = ds.Tables[0].DefaultView;

                LblRecordsno.Text = objds.DataSourceCount.ToString();
                DataGrid1.DataBind();
                DataGrid1.Visible = true;
                lnkBack.Visible = false;
            }
            else
            {
                LblRecordsno.Text = "0";
                DataGrid1.DataSource = null;
                DataGrid1.DataBind();
                DataGrid1.Visible = false;
                lnkBack.Visible = false;
                divMessage.InnerHtml = "No category in the database.";
            }
        }
        else
        {
            DataGrid1.DataSource = null;
            DataGrid1.DataBind();
            DataGrid1.Visible = false;
            lnkBack.Visible = false;
            divMessage.InnerHtml = "No category in the database..";
        }
    }
    protected void lnkAddNew_Click(object sender, EventArgs e)
    {
        clearControls();
        DivEntry.Visible=true;
        DataGrid1.Visible=false;
        lnkBack.Visible = true;
        divMessage.InnerHtml = "";
        this.Title = "Add note category";
    }

    protected void DataGrid1_ItemCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
    {
        GridDataItem dataItem = e.Item as GridDataItem;
        if (e.CommandName == "edit")
        {
            string strSql = "select * from Notes_Categories where NoteCat_id=" + ((Label)dataItem.FindControl("lblNoteCatid")).Text;
            DataSet ds = DatabaseHelper.getDataset(strSql);
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    lblId.Text = ds.Tables[0].Rows[0]["NoteCat_id"].ToString();
                    txtName.Text = ds.Tables[0].Rows[0]["Categoy_name"].ToString();
                    prevName.Value = ds.Tables[0].Rows[0]["Categoy_name"].ToString();
                    string NoteType = ds.Tables[0].Rows[0]["Category_Type"].ToString();

                    if (NoteType == "FHS")
                    {
                        RdbFhs.Checked = true;
                    }
                    else if (NoteType == "Client")
                    {
                        RdbClient.Checked = true;
                    }
                    btnCreate.Text = "Edit";
                    DivEntry.Visible = true;
                    DataGrid1.Visible = false;
                    lnkBack.Visible = true;
                    divMessage.InnerHtml = "";
                    this.Title = "Edit category - " + txtName.Text;
                }
            }
            
        }
        else if (e.CommandName == "delete")
        {
           object objResult = DatabaseHelper.executeScalar("select count(*) from Company_notes where Categoy_name='" + ((Label)dataItem.FindControl("lblCategoyname")).Text + "' and Category_Type='" + ((Label)dataItem.FindControl("lblCategorytype")).Text + "'");
           if (objResult.ToString() == "0")
            {
                string strSql = "delete from Notes_Categories where NoteCat_id=" + ((Label)dataItem.FindControl("lblNoteCatid")).Text;
                int intResult = DatabaseHelper.executeNonQuery(strSql);
                if (intResult != 0)
                {
                    divMessage.InnerHtml = "Category was deleted.";
                    bindData();
                }
                else
                {
                    divMessage.InnerHtml = "Category was not deleted.";
                    bindData();
                }
            }
            else
            {
                divMessage.InnerHtml = "You can not delete this category. It is been refered by another table.";
            }
        }
    }

    protected void DataGrid1_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        foreach (TableCell tc in e.Item.Cells)
        {

            tc.Attributes["style"] = "border:1px solid #00A651;";


        }
        if (e.Item.ItemType == GridItemType.Item || e.Item.ItemType == GridItemType.AlternatingItem)
        {
            GridDataItem dataItem = e.Item as GridDataItem;
            LinkButton deleteButton = (LinkButton)dataItem.FindControl("lbldelete");
            deleteButton.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete this category?')");
        }
    }

    private void clearControls()
    {
        lblId.Text = "New";
        txtName.Text = "";
        RdbClient.Checked = false;
        RdbFhs.Checked = false;
        btnCreate.Text = "Create";
    }

    protected void lnkBack_Click(object sender, EventArgs e)
    {
        clearControls();
        DivEntry.Visible = false;
        bindData();
    }
}
