<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Notification.aspx.cs" Inherits="Admin_Notification" %>

<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Notification</title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <uc2:Admin_Header ID="Admin_Header1" runat="server" />
        <div id="Content">
        <div id="pageTitle" style="height:30px;font-weight:bold;text-align:left">Companies</div>
        <div style="float:right">
         <span style="color:Black; font-size:16px" id="Notification" runat="server">Notifications:<asp:LinkButton id="LnkNotification" runat="server" ForeColor="red" OnClick="LnkNotification_Click" ></asp:LinkButton></span>&nbsp;
              <span style="color:green;" id="NewCR" runat="server">New CRs<asp:LinkButton id="LnkNewCR" runat="server" OnClick="LnkNewCR_Click"></asp:LinkButton></span>&nbsp;
               <span style="color:green;" id="CRwocr" runat="server">CR(wocr)<asp:LinkButton id="LnkCRWocr" runat="server" OnClick="LnkCRWocr_Click"></asp:LinkButton></span>&nbsp;
             <span style="color:green;" id="CrNewComment" runat="server">CRs with new comment<asp:LinkButton id="LnkCrNewComment" runat="server" OnClick="LnkCrNewComment_Click"></asp:LinkButton></span>
              <span style="color:green;" id="MakeCommentRead" runat="server"><asp:LinkButton id="LnkMakecommentRead" runat="server" OnClick="LnkMakecommentRead_Click"></asp:LinkButton></span>&nbsp;
              <span style="color:Red;" id="unansweredque" runat="server">My unanswered questions<asp:LinkButton id="lnkUnansweredQuestions" runat="server" OnClick="lnkUnansweredQuestions_Click" CausesValidation="False"></asp:LinkButton></span>&nbsp;
          <span style="color:Red;" id="immediatetask" runat="server">My immediate tasks<asp:LinkButton id="lnkPr0Task" runat="server" OnClick="lnkPr0Task_Click" CausesValidation="False"></asp:LinkButton></span>&nbsp;
          <span style="color:#EE82EE;" id="highestTasks" runat="server">My 1a  tasks<asp:LinkButton id="lnkHighestTasks" runat="server" OnClick="lnkHighestTasks_Click" CausesValidation="False"></asp:LinkButton></span>&nbsp;
          <span style="color:Black;" id="highTasks" runat="server">My high tasks<asp:LinkButton id="lnkHighTasks" runat="server" OnClick="lnkHighTasks_Click" CausesValidation="False"></asp:LinkButton></span>&nbsp;
          <span style="color:Black;" id="PR1CTask" runat="server">My 1c tasks<asp:LinkButton id="Lnk1cTasks" runat="server"  CausesValidation="False" OnClick="Lnk1cTasks_Click"></asp:LinkButton></span>&nbsp;
           <span style="color:Red;" id="TskNewComment" runat="server">My tasks with new comment<asp:LinkButton id="LnkTskNewcomment" runat="server" OnClick="LnkTskNewcomment_Click"></asp:LinkButton></span>
           <span style="color:green;" id="spnMakeTskcmntRead" runat="server"><asp:LinkButton id="LnkMakeTaskcommentRead" runat="server" OnClick="LnkMakeTaskcommentRead_Click"></asp:LinkButton></span>&nbsp;
           <span style="color:Red;" id="SpnAllTaskNewComment" runat="server">Tasks with new comment<asp:LinkButton id="LnkAllTsknewComment" runat="server" OnClick="LnkAllTsknewComment_Click"></asp:LinkButton></span>
           <span style="color:green;" id="Span2" runat="server"><asp:LinkButton id="LnkMakeAllTaskCmmntRead" runat="server" OnClick="LnkMakeAllTaskCmmntRead_Click"></asp:LinkButton></span>&nbsp;
                                </div>
        <div >
            <a href="edit_company.aspx">add new company</a> &nbsp;<a href="esActivityLog.aspx">estateCRM activity</a>
        </div>
             
        <div id="divMessage" runat="server" style="color:Red;font-weight:bold"></div>
        <br />
      
        <div class="divBorder"> 
          <div style="padding-bottom:10px"> <asp:LinkButton ID="lnkAll"  runat="server" Font-Bold="true" OnClick="lnkAll_Click">All</asp:LinkButton>&nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp;
                                           <asp:LinkButton ID="lnkA" runat="server" Font-Bold="true"   OnClick="lnkA_Click">A</asp:LinkButton>
                                        <asp:LinkButton ID="lnkB" runat="server"  Font-Bold="true"  OnClick="lnkB_Click">B</asp:LinkButton>
                                            <asp:LinkButton ID="lnkC" runat="server"  Font-Bold="true"  OnClick="lnkC_Click">C</asp:LinkButton>
                                            <asp:LinkButton ID="lnkD" runat="server" Font-Bold="true"   OnClick="lnkD_Click">D</asp:LinkButton>
                                              <asp:LinkButton ID="lnkE" runat="server"  Font-Bold="true"  OnClick="lnkE_Click">E</asp:LinkButton>
                                              <asp:LinkButton ID="lnkF" runat="server" Font-Bold="true"   OnClick="lnkF_Click">F</asp:LinkButton>
                                              <asp:LinkButton ID="lnkG" runat="server" Font-Bold="true"   OnClick="lnkG_Click">G</asp:LinkButton>
                                              <asp:LinkButton ID="lnkH" runat="server"  Font-Bold="true"  OnClick="lnkH_Click">H</asp:LinkButton>
                                              <asp:LinkButton ID="lnkI" runat="server" Font-Bold="true"   OnClick="lnkI_Click">I</asp:LinkButton>
                                                 <asp:LinkButton ID="lnkJ" runat="server" Font-Bold="true"   OnClick="lnkJ_Click">J</asp:LinkButton>
                                              <asp:LinkButton ID="lnkK" runat="server" Font-Bold="true"  OnClick="lnkK_Click">K</asp:LinkButton>
                                              <asp:LinkButton ID="lnkL" runat="server" Font-Bold="true"  OnClick="lnkL_Click">L</asp:LinkButton>
                                              <asp:LinkButton ID="lnkM" runat="server" Font-Bold="true"  OnClick="lnkM_Click">M</asp:LinkButton>
                                              <asp:LinkButton ID="lnkN" runat="server" Font-Bold="true"  OnClick="lnkN_Click">N</asp:LinkButton>
                                              <asp:LinkButton ID="lnkO" runat="server" Font-Bold="true"  OnClick="lnkO_Click">O</asp:LinkButton>
                                              <asp:LinkButton ID="lnkP" runat="server" Font-Bold="true"  OnClick="lnkP_Click">P</asp:LinkButton>
                                              <asp:LinkButton ID="lnkQ" runat="server" Font-Bold="true"  OnClick="lnkQ_Click">Q</asp:LinkButton>
                                              <asp:LinkButton ID="lnkR" runat="server" Font-Bold="true"  OnClick="lnkR_Click">R</asp:LinkButton>
                                              <asp:LinkButton ID="lnkS" runat="server" Font-Bold="true"  OnClick="lnkS_Click">S</asp:LinkButton>
                                              <asp:LinkButton ID="lnkT" runat="server" Font-Bold="true"  OnClick="lnkT_Click">T</asp:LinkButton>
                                              <asp:LinkButton ID="lnkU" runat="server" Font-Bold="true"  OnClick="lnkU_Click">U</asp:LinkButton>
                                              <asp:LinkButton ID="lnkV" runat="server" Font-Bold="true"  OnClick="lnkV_Click">V</asp:LinkButton>
                                              <asp:LinkButton ID="lnkW" runat="server" Font-Bold="true"  OnClick="lnkW_Click">W</asp:LinkButton>
                                              <asp:LinkButton ID="lnkX" runat="server" Font-Bold="true"  OnClick="lnkX_Click">X</asp:LinkButton>
                                              <asp:LinkButton ID="lnkY" runat="server" Font-Bold="true"  OnClick="lnkY_Click">Y</asp:LinkButton>
                                              <asp:LinkButton ID="lnkZ" runat="server" Font-Bold="true"   OnClick="lnkZ_Click">Z</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>Records: </b> <asp:Label ID="LblRecordsno" runat="server" Text="" ></asp:Label>   </div>
            <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" CellPadding="5" OnItemCommand="DataGrid1_ItemCommand" OnItemDataBound="DataGrid1_ItemDataBound" AllowPaging="True" OnPageIndexChanged="DataGrid1_PageIndexChanged" PageSize="20" AllowSorting="True" OnSortCommand="DataGrid1_SortCommand" >
                <PagerStyle NextPageText="Next" PrevPageText="Prev" />
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" HorizontalAlign="Center" />
                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" HorizontalAlign="Center" BackColor="Gainsboro" />
                <Columns>
                    <asp:BoundColumn DataField="Company_id" HeaderText="Company id" SortExpression="Company_id"></asp:BoundColumn>
                    <asp:BoundColumn DataField="Company_Name" HeaderText="Company name" SortExpression="Company_Name">
                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                            Font-Underline="False" HorizontalAlign="Left" />
                    </asp:BoundColumn>
                    <asp:TemplateColumn HeaderText="Projects" HeaderStyle-ForeColor="blue">
                    <ItemTemplate>
                     <asp:DropDownList ru<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Notification.aspx.cs" Inherits="Admin_Notification" %>

<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= ConfigurationManager.AppSettings["CompanyName"].ToString()%></title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <uc2:Admin_Header ID="Admin_Header1" runat="server" />
        <div id="Content">
        <div id="pageTitle" style="height:30px;font-weight:bold;text-align:left">Companies</div>
        <div style="float:right">
         <span style="color:Black; font-size:16px" id="Notification" runat="server">Notifications:<asp:LinkButton id="LnkNotification" runat="server" ForeColor="red" OnClick="LnkNotification_Click" ></asp:LinkButton></span>&nbsp;
              <span style="color:green;" id="NewCR" runat="server">New CRs<asp:LinkButton id="LnkNewCR" runat="server" OnClick="LnkNewCR_Click"></asp:LinkButton></span>&nbsp;
               <span style="color:green;" id="CRwocr" runat="server">CR(wocr)<asp:LinkButton id="LnkCRWocr" runat="server" OnClick="LnkCRWocr_Click"></asp:LinkButton></span>&nbsp;
             <span style="color:green;" id="CrNewComment" runat="server">CRs with new comment<asp:LinkButton id="LnkCrNewComment" runat="server" OnClick="LnkCrNewComment_Click"></asp:LinkButton></span>
              <span style="color:green;" id="MakeCommentRead" runat="server"><asp:LinkButton id="LnkMakecommentRead" runat="server" OnClick="LnkMakecommentRead_Click"></asp:LinkButton></span>&nbsp;
              <span style="color:Red;" id="unansweredque" runat=