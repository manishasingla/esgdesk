<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Notifications.ascx.cs"
    CodeBehind="Notifications.ascx.cs" Inherits="Admin_Notifications" %>

<script language="javascript" type="text/javascript">
    function window.confirm(str) {
        execScript('n = msgbox("' + str + '","4132")', "vbscript");
        return (n == 6);
    }
</script>

<table cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <%--<td style="padding: 0 0 0 10px;">--%>
        <td style="padding: 0 0 0 0px;">
            <span id="Notification" runat="server" style="font-size: 16px; color: #282C5F;"><%--Notifications:--%></span>
            
                    MINE:
                    <%-----------------------------------------------------------------------------------------%>
                    <span id="immediatetask" runat="server" style="color:red">
                            <span style="color:red"></span>Immediate<asp:LinkButton ID="lnkPr0Task"
                                runat="server" ForeColor="red" style="Border-bottom:1px dotted #000;"  CausesValidation="False" OnClick="lnkPr0Task_Click" PostBackUrl="~/Admin/tasks.aspx"></asp:LinkButton></span>
                                
                    <%---------------------------------------------------------------------------------------------%>
                                 <span id="highestTasks" runat="server" style="color:#FF1493">
                        Do now<asp:LinkButton ID="lnkHighestTasks" style="Border-bottom:1px dotted #000;" ForeColor="#FF1493"  runat="server" CausesValidation="False"
                            OnClick="lnkHighestTasks_Click" PostBackUrl="~/Admin/tasks.aspx"></asp:LinkButton></span>
                    <%---------------------------------------------------------------------------------------------%>
                                                                       
                   <span id="highTasks" runat="server" style="color:black">High<asp:LinkButton ID="lnkHighTasks"
                                    runat="server" style="Border-bottom:1px dotted #000;" CausesValidation="False" OnClick="lnkHighTasks_Click" PostBackUrl="~/Admin/tasks.aspx"></asp:LinkButton></span>
                    <%---------------------------------------------------------------------------------------------%>
                                        
                                                         
                    <span id="PR1CTask" runat="server" style="color:black">Normal<asp:LinkButton
                    ID="Lnk1cTasks" runat="server" style="Border-bottom:1px dotted #000;" CausesValidation="False" OnClick="Lnk1cTasks_Click" PostBackUrl="~/Admin/tasks.aspx"></asp:LinkButton></span>
                    <%---------------------------------------------------------------------------------------------%> 
                   
                    <span id="PRlowTask" runat="server" style="color:black">Low<asp:LinkButton
                    ID="LinklowTasks" runat="server" style="Border-bottom:1px dotted #000;" CausesValidation="False" 
                    onclick="LinklowTasks_Click" PostBackUrl="~/Admin/tasks.aspx"></asp:LinkButton></span>
                    <%---------------------------------------------------------------------------------------------%>  
                    <span id="TskNewComment" runat="server" style="color:#FF1493">Unread comments<asp:LinkButton
                    ID="LnkTskNewcomment" runat="server" CausesValidation="false" style="Border-bottom:1px dotted #000;" ForeColor="#FF1493" OnClick="LnkTskNewcomment_Click" PostBackUrl="~/Admin/tasks.aspx"></asp:LinkButton></span>
                    <span id="spnMakeTskcmntRead" runat="server" style="color:#FF1493;margin-left:-5px">
                    <asp:LinkButton ID="LnkMakeTaskcommentRead" runat="server" ForeColor="#FF1493"  CausesValidation="false"
                    OnClick="LnkMakeTaskcommentRead_Click" PostBackUrl="~/Admin/tasks.aspx"></asp:LinkButton></span>
                    <%---------------------------------------------------------------------------------------------%> 
                    <span id="unansweredque" runat="server" style="color:black"><%--<span style="color:black">&nbsp;|&nbsp;
                    </span>--%>To answer<asp:LinkButton ID="lnkUnansweredQuestions" style="Border-bottom:1px dotted #000;" runat="server"
                    CausesValidation="False" OnClick="lnkUnansweredQuestions_Click" PostBackUrl="~/Admin/tasks.aspx"></asp:LinkButton></span>
                    <span style="color:#333333"></span>
                    <%---------------------------------------------------------------------------------------------%>  
                   
                    <%---------------------------------------------------------------------------------------------%>
                    <span id="firstpipe" runat="server"  style="color:black;font-weight:bold;">&nbsp;|&nbsp;</span>
                    <span id="OverDuetsk" runat="server" style="color:red">Overdue<asp:LinkButton  ID="BtnOverdue" runat="server" BackColor="transparent" BorderStyle="none" CausesValidation="False" OnClick="BtnOverdue_Click" Style="cursor:pointer;color:red;Border-bottom:1px dotted #000;" PostBackUrl="~/Admin/tasks.aspx"></asp:LinkButton></span>
                    <%--<asp:Button ID="BtnOverdue" runat="server"  BackColor="transparent" BorderStyle="none" CausesValidation="False" OnClick="BtnOverdue_Click" Style="cursor:pointer;color:red"/> --%>
                    
                    <%---------------------------------------------------------------------------------------------%>
                    <span id="allsign" runat="server" style="color:Black;">ALL:&nbsp;</span>
                    <span id="SpnAllTaskNewComment" runat="server" style="color:#FF1493">Unread comments<asp:LinkButton ID="LnkAllTsknewComment" style="Border-bottom:1px dotted #000;" runat="server" OnClick="LnkAllTsknewComment_Click" ForeColor="#FF1493" PostBackUrl="~/Admin/tasks.aspx"></asp:LinkButton></span>
                    <span id="Span1" runat="server" style="color:#FF1493;margin-left:-5px"><asp:LinkButton ID="LnkMakeAllTaskCmmntRead" runat="server" CausesValidation="false"
                    OnClick="LnkMakeAllTaskCmmntRead_Click" ForeColor="#FF1493" PostBackUrl="~/Admin/tasks.aspx"></asp:LinkButton></span>
                 <%----------------------------------------------------------------------------------------------------------------------------------------------%>
                    <span id="AllOverDuetsk" runat="server" style="color:red"><%--<span style="color:Black;">&nbsp;|&nbsp;</span>--%><%--<span style="color:Black;">ALL:&nbsp;</span>--%>Overdue<asp:LinkButton ID="BtnallOverdue" runat="server"  BackColor="transparent" BorderStyle="none" CausesValidation="False" Style="width:auto;cursor:pointer;color:red;Border-bottom:1px dotted #000;" OnClick="BtnallOverdue_Click" PostBackUrl="~/Admin/tasks.aspx"></asp:LinkButton></span>
                    <%--<asp:Button ID="Button1" runat="server" BackColor="transparent" BorderStyle="none" CausesValidation="False" Style="width:auto;cursor:pointer;color:red" onclick="BtnallOverdue_Click"/>--%>
                    <%------------------------------------------------------------------------------------------------------%>
                     <span id="tobeansweres" runat="server" style="color:black">To be answered<asp:LinkButton ID="lnktobeansweres" runat="server"
                     CausesValidation="False" style="Border-bottom:1px dotted #000;" OnClick="lnktobeansweres_Click" PostBackUrl="~/Admin/tasks.aspx"></asp:LinkButton></span>
                    <%----------------------------------------------------------------------------------------------------------------------------------%>
                    <span id="UnApprovedCR" runat="server" style="color:red">
                    <%--<span style="color:Black ">&nbsp;|&nbsp;</span>--%>UnApproved<asp:LinkButton ID="LnkNewUnApproved" runat="server" style="Border-bottom:1px dotted #000;" ForeColor="red" OnClick="LnkNewUnApproved_Click" PostBackUrl="~/Admin/tasks.aspx"></asp:LinkButton></span>
                    
                    <%---------------------------------------------------------------------------------------------%>
                    <span id="NewCrComment" runat="server" style="color:green">UnApproved Comment
                    <asp:LinkButton ID="NewCRCommentinRed" runat="server" style="Border-bottom:1px dotted #000;" ForeColor="green" OnClick="NewCRCommentinRed_Click" PostBackUrl="~/Admin/tasks.aspx"></asp:LinkButton></span>&nbsp;
                    <span id="secondpipe" runat="server" style="color:black;font-weight:bold;">&nbsp;|&nbsp;</span>
                    
                    
                   
                    <%--------------------------------------------------------------------------------------------------------------------------------%>
                    <span id="CRsign" runat="server" style="color:Green;">CR:&nbsp;</span>
                    <span id="NewCR" runat="server" style="color:Green">
                    <%--<span style="color:Black ">&nbsp;|&nbsp;</span>--%>New<asp:LinkButton ID="LnkNewCR" runat="server" ForeColor="green" style="Border-bottom:1px dotted #000;" OnClick="LnkNewCR_Click" PostBackUrl="~/Admin/tasks.aspx"></asp:LinkButton></span>
                    <span style="color:#333333"></span>
                    <%-------------------------------------------------------------------------------------------------------------------------%>
                     <span id="CrNewComment" runat="server" ><span>Unread comments</span><asp:LinkButton
                    ID="LnkCrNewComment" runat="server" style="Border-bottom:1px dotted #000;" OnClick="LnkCrNewComment_Click" PostBackUrl="~/Admin/tasks.aspx"></asp:LinkButton></span>
                    <span style="color:#FF1493"></span><span id="MakeCommentRead" runat="server" style="margin-left:-5px">
                    <asp:LinkButton ID="LnkMakecommentRead" runat="server" OnClick="LnkMakecommentRead_Click" PostBackUrl="~/Admin/tasks.aspx"></asp:LinkButton></span>
                    &nbsp;
                    <%----------------------------------------------------------------------------------------------------------------------------%>
                    
                    <%--============================End Without change name code===============================================================================--%>
                    <asp:ImageButton ID="refreshbtn" runat="server" ImageUrl="~/Admin/images/reload.png" style="vertical-align:bottom;" Height="17px" Width="20px" OnClick="refreshbtn_Click" /> 
                    <%--<asp:Button id="btnsubmit" runat="server" Text="Refresh"  style="background: #282c5f;border:none;cursor:hand;Color:#FFFFFF;padding:2px 4px;" OnClick="btnsubmit_Click"  />--%>
       </td> <%----%>
    </tr>
    <%--style="background: none repeat scroll 0% 0% transparent; border:medium none;cursor:pointer;width:auto;padding:0"--%>
</table>
