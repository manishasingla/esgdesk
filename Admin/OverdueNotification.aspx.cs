using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Admin_OverdueNotification : System.Web.UI.Page
{
    int id;
    string sql = "";
    string task_action = "";
    string emailSubject = "";
    string shortDescription = "";
    bool deletedFlag = false;
    bool flagEmailNotification = false;
    string task_id = "";
    DateTime duedate;

   
    protected void Page_Load(object sender, EventArgs e)
    {



        string strConnESG = "Data Source=88.208.204.121,1615;Network Library=DBMSSOCN;Initial Catalog=ChangeRequest;User ID=sa;Password=fhsadmin123";
        string strConESnESG = "Data Source=88.208.204.121,1615;Network Library=DBMSSOCN;Initial Catalog=estateCRM;User ID=sa;Password=fhsadmin123";
  
    DataSet ds = new DataSet();
        ///


        try
        {
          
            SqlConnection Conn = new SqlConnection(strConnESG);
            string strQuery = "select * from tasks where status <> 'closed' and deleted <> 1 and status <> 'checked' ORDER BY last_updated_date desc ";
            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    DateTime crrntdate = DateTime.Now.Date;
                    string djgfjs = ds.Tables[0].Rows[i]["DueDate"].ToString();
                    if (ds.Tables[0].Rows[i]["DueDate"].ToString() != null && ds.Tables[0].Rows[i]["DueDate"].ToString() != "")
                    {
                        string strduedate = DateTime.Parse(ds.Tables[0].Rows[i]["DueDate"].ToString()).ToString("dd/MM/yyyy");
                        duedate = DateTime.Parse(strduedate.ToString());
                       if (DateTime.Compare(duedate, crrntdate) < 0)
                       {

                           sendNotification(ds.Tables[0].Rows[i]["task_id"].ToString());

                       }
                  
                   
                    }
  
                }
                
                
            }



        }
        catch
        {
        }


    }

    private string generateAdminEmail(string strTaskId)
    {

        string strConnESG = "Data Source=88.208.204.121,1615;Network Library=DBMSSOCN;Initial Catalog=ChangeRequest;User ID=sa;Password=fhsadmin123";
        DataSet dsTask = new DataSet();
        SqlConnection Conn = new SqlConnection(strConnESG);
        string strQuery = "select * from tasks where task_id=" + strTaskId;
        SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
        da.Fill(dsTask);
           ////  DatabaseHelper.getDataset("select * from tasks where task_id=" + strTaskId);

        string strBody = "";
        if (dsTask == null || dsTask.Tables.Count <= 0 || dsTask.Tables[0].Rows.Count <= 0)
        {
            ///divMessage.InnerHtml = "Task not found.<br><br><a href='tasks.aspx'>View tasks</a>";
        }
        else
        {
            strBody += "Task: <span style=\"color:red\">";

            strBody += task_action;

            strBody += " (<a style=\"color:red\" href='http://www.esgdesk.com/admin/edit_task.aspx?id=" + strTaskId + "'>" + strTaskId + "</a>) </span> " + DateTime.Parse(dsTask.Tables[0].Rows[0]["last_updated_date"].ToString()).ToString("dd MMM h:mm tt") + "<br><br>";

            if (strTaskId.ToString() != "")
            {
               
                    strBody += "Overdue date notification for task.<br>";
                    strBody += "<span style=\"color:red\">Tes email format for overdue tasks.</span><br>";
                
            }
            shortDescription = dsTask.Tables[0].Rows[0]["short_desc"].ToString();
            strBody += "<a href='http://www.esgdesk.com/admin/edit_task.aspx?id=" + strTaskId + "'>" + dsTask.Tables[0].Rows[0]["short_desc"].ToString() + "</a>&nbsp;&nbsp;&nbsp;(" + dsTask.Tables[0].Rows[0]["priority"].ToString() + " : " + dsTask.Tables[0].Rows[0]["status"].ToString() + ")<br><br>";
            strBody += "Emp: " + ((string)ObjexecuteScalar("select firstname +' '+ lastname as name from users where username = '" + dsTask.Tables[0].Rows[0]["assigned_to_user"].ToString() + "'")) + "<br>";
            strBody += "Project: " + dsTask.Tables[0].Rows[0]["project"].ToString() + "<br>";
            strBody += "Relevant URL: <a href='" + dsTask.Tables[0].Rows[0]["Relevant_URL"].ToString() + "' target='_blank'>" + dsTask.Tables[0].Rows[0]["Relevant_URL"].ToString() + "</a><br>";
            strBody += "Start date: " + dsTask.Tables[0].Rows[0]["StartDate"].ToString() + "<br>";
            strBody += "Due date: " + dsTask.Tables[0].Rows[0]["DueDate"].ToString() + "<br>";
            strBody += "<br><br>";

            sql = "Select * from task_comments where task_id=" + strTaskId.ToString() + " and deleted <> 1 order by tc_id desc";

            DataSet dsTaskDetails = getDataset(sql);

            strBody += "<table cellspacing=\"0\" border=\"1\" style=\"width: 99%;border-collapse: collapse;\">";
            strBody += "<tbody>";

            for (int i = 0; i < dsTaskDetails.Tables[0].Rows.Count; i++)
            {
                strBody += "<tr>";
                strBody += "<td>";
                strBody += "<div style=\"border: 1px none Green;\">";
                strBody += "<table width=\"100%\">";
                strBody += "<tbody>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                if (dsTaskDetails.Tables[0].Rows[i]["qflag"].ToString() == "1")
                {
                    strBody += "<span style=\"color:red;\">Question posted by " + dsTaskDetails.Tables[0].Rows[i]["username"].ToString() + " on " + DateTime.Parse(dsTaskDetails.Tables[0].Rows[i]["post_date"].ToString()).ToString("dd MMM h:mm tt") + "</span>";
                }
                else
                {
                    strBody += "<span style=\"color: Green;\">comment " + dsTaskDetails.Tables[0].Rows[i]["tc_id"].ToString() + " posted by " + dsTaskDetails.Tables[0].Rows[i]["username"].ToString() + " on " + DateTime.Parse(dsTaskDetails.Tables[0].Rows[i]["post_date"].ToString()).ToString("dd MMM h:mm tt") + "</span>";
                }
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\"> </td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span>" + dsTaskDetails.Tables[0].Rows[i]["comment"].ToString() + "</span>";
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "</tbody>";
                strBody += "</table>";
                strBody += "</div>";
                strBody += "</td>";
                strBody += "</tr>";
            }
            strBody += "</tbody>";
            strBody += "</table>";
        }
        return strBody;
    }


    public static DataSet getDataset(string strSql)
    {
        string strConnESG = "Data Source=88.208.204.121,1615;Network Library=DBMSSOCN;Initial Catalog=ChangeRequest;User ID=sa;Password=fhsadmin123";
        DataSet ds = new DataSet();
        using (SqlConnection conn = new SqlConnection(strConnESG))
        {
            using (SqlDataAdapter da = new SqlDataAdapter(strSql, conn))
            {

                try
                {

                    da.Fill(ds);
                    conn.Close(); // redundant, but just to be clear
                }
                catch { }
                return ds;


            }
        }

    }

    public static object ObjexecuteScalar(string strSql)
    {
        object objResult = null;
        string strConnESG = "Data Source=88.208.204.121,1615;Network Library=DBMSSOCN;Initial Catalog=ChangeRequest;User ID=sa;Password=fhsadmin123";
        SqlConnection sqlConn = new SqlConnection(strConnESG);
        SqlCommand sqlCmd = new SqlCommand(strSql, sqlConn);
        try
        {
            sqlConn.Close();
            sqlConn.Open();
            objResult = sqlCmd.ExecuteScalar();
        }
        catch
        {
            objResult = 0;
        }
        finally
        {
            sqlConn.Close();
        }
        return objResult;
    }

    void sendNotification(string strTaskId)
    {
        try
        {
            

            sql = "select email, EmailNotification,PR_EmailNotification ";
            sql += " from users, task_subscriptions ";
            sql += " where users.username = task_subscriptions.username ";
            sql += " and task_subscriptions.task_id =" + strTaskId.ToString();

            DataSet ds = getDataset(sql);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                string strHtml = generateAdminEmail(strTaskId);

                string toEmails = "";


                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (ds.Tables[0].Rows[i]["EmailNotification"].ToString() == "Y")
                    {

                        ////string reqPR = "";
                        ////string[] strPR = ds.Tables[0].Rows[i]["PR_EmailNotification"].ToString().Split(new char[] { ';' });
                        ////for (int j = 0; j < strPR.Length - 1; j++)
                        ////{
                        ////    reqPR = strPR[j];
                            
                           toEmails += ds.Tables[0].Rows[i]["email"].ToString() + ";";
                          

                        ///}


                    }
                    else if ((ds.Tables[0].Rows[i]["EmailNotification"].ToString() == "N") && (flagEmailNotification == true))
                    {
                        /// toEmails += ds.Tables[0].Rows[i]["email"].ToString() + ";";

                        ////string reqPR = "";
                        ////string[] strPR = ds.Tables[0].Rows[i]["PR_EmailNotification"].ToString().Split(new char[] { ';' });
                        ////for (int j = 0; j < strPR.Length - 1; j++)
                        ////{
                        ////    reqPR = strPR[j];
                            
                                toEmails += ds.Tables[0].Rows[i]["email"].ToString() + ";";
                            
                        ////}

                    }

                }

                toEmails = toEmails.TrimEnd(';');

                if (emailSubject == "")
                {
                    emailSubject = "Task ID:" + strTaskId.ToString() + " was updated - " + shortDescription.Trim() + " (Task ID:" + strTaskId.ToString() + ")";
                }
               //// bool flag = DatabaseHelper.sendEmailTasks(toEmails, emailSubject, strHtml);

                bool blndone = false;
                try
                {
                    
                    
                    System.Web.Mail.MailMessage msg = new System.Web.Mail.MailMessage();
                    msg.To = toEmails;
                    msg.Cc = "alkas@123sitesolutions.com";
                    msg.From = "tasks@estatesolutions.co.uk";
                    msg.Subject = emailSubject;
                    msg.Body = strHtml;
                    msg.Priority = System.Web.Mail.MailPriority.High;
                    msg.BodyFormat = System.Web.Mail.MailFormat.Html;
                    System.Web.Mail.SmtpMail.Send(msg);
                    blndone = true;


                }

                catch 
                {
                    blndone = false; 
                }
            

            }
        }
        catch { }
    }
}
