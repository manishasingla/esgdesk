﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Priority_reports.aspx.cs"
    Inherits="Admin_Priority_reports" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="Notifications.ascx" TagName="Notifications" TagPrefix="uc4" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        <%=System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(assigned_user)%> priority report
    </title>
    <link rel="stylesheet" type="text/css" href="slider.css" />
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="slider.js"></script>

    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        
         .image-holder
        {
            float: left;
            width: 200px;
            height: 220px;
            padding: 10px;
         
            border: 1px solid #ddd;
            background: url(images/ajax-loader-mini.gif) 50% 50% no-repeat;
            display: inline;
        }
     table#DataGrid1_ctl00 tr th a
     {
      display:block;
      text-decoration:none;
      margin:0;
      padding:0;
      width:100%;
      height:100%;
     }
   .RadGrid_Default .rgSelectedRow  
  {
      
       background:#828282 0 -3900px repeat-x url('Grid/sprite.gif') !important;color:#fff; 
   }
      
  </style>
    <style type="text/css">
    div.RadToolTip_Default table.rtWrapper td.rtWrapperContent
   {
    background-color: #FEFFB3 !important;
   }
</style>

    <script type="text/javascript">


    function imageLoaded(img) 
    {
        document.getElementById('loadingImage').style.visibility = 'hidden';
        img.style.visibility = 'visible';
    }

    </script>

    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">

        <script type="text/javascript" language="javascript">
        

        
           function EnablePRlink() {
            var objPR = document.getElementById("DrpMultipleTaskPR").value;
            if (objPR != "0")
             {
                document.getElementById("LnkBtnToChngPR").disabled = false;
                // document.getElementById("LnkChngAssingedTo").disabled = "disabled";
                document.getElementById("LnkChngStatus").disabled = "disabled";
                document.getElementById("LnkChngAssingedTo").disabled = "disabled";
                document.getElementById("DrpMultipleTaskAssing").value = "0";
                document.getElementById("DrpMultipleTaskStatus").value = "0";

            }
            else
            {
                document.getElementById("LnkBtnToChngPR").disabled = "disabled";
                document.getElementById("LnkChngStatus").disabled = "disabled";
                document.getElementById("LnkChngAssingedTo").disabled = "disabled";
                document.getElementById("DrpMultipleTaskAssing").value = "0";
                document.getElementById("DrpMultipleTaskStatus").value = "0";
                document.getElementById("DrpMultipleTaskPR").value = "0";
            }

            return true;

        }


        function EnableAssinglink() {

            var objAssing = document.getElementById("DrpMultipleTaskAssing").value;

            if (objAssing != "0")
             {

                document.getElementById("LnkBtnToChngPR").disabled = "disabled";
                document.getElementById("LnkChngAssingedTo").disabled = false;
                document.getElementById("LnkChngStatus").disabled = "disabled";
                ///document.getElementById("DrpMultipleTaskAssing").value = "0";
                document.getElementById("DrpMultipleTaskStatus").value = "0";
                document.getElementById("DrpMultipleTaskPR").value = "0";
                ///document.getElementById("LnkChngAssingedTo").disabled = "disabled";
            }
            else {

                document.getElementById("LnkBtnToChngPR").disabled = "disabled";
                document.getElementById("LnkChngAssingedTo").disabled = "disabled";
                document.getElementById("LnkChngStatus").disabled = "disabled";
                document.getElementById("DrpMultipleTaskAssing").value = "0";
                document.getElementById("DrpMultipleTaskStatus").value = "0";
                document.getElementById("DrpMultipleTaskPR").value = "0";

            }

            return;
        }

        function EnableStatuslink() {
            var objStatus = document.getElementById("DrpMultipleTaskStatus").value;

            if (objStatus != "0")
             {
                document.getElementById("LnkBtnToChngPR").disabled = "disabled";
                document.getElementById("LnkChngAssingedTo").disabled = "disabled";
                document.getElementById("LnkChngStatus").disabled = false;
                document.getElementById("DrpMultipleTaskAssing").value = "0";
                ///document.getElementById("DrpMultipleTaskStatus").value = "0";
                document.getElementById("DrpMultipleTaskPR").value = "0";
            }
            else {

                document.getElementById("LnkBtnToChngPR").disabled = "disabled";
                document.getElementById("LnkChngAssingedTo").disabled = "disabled";
                document.getElementById("LnkChngStatus").disabled = "disabled";
                document.getElementById("DrpMultipleTaskAssing").value = "0";
                document.getElementById("DrpMultipleTaskStatus").value = "0";
                document.getElementById("DrpMultipleTaskPR").value = "0";

            }
            return;

        }


       //Validate Mark Button & Delete Button present inside Action area
        function validateonbtn(a) {
            var chkGridcontrols = document.getElementById("<%=DataGrid1.ClientID%>").getElementsByTagName("input");
            var count = 0;
            for (var i = 0; i < chkGridcontrols.length; i++) {
                if (chkGridcontrols[i].checked)
                    count = 1;
            }

            if (count > 0) {
                if (a == 0) {
                    if (!confirm("Are you sure you want to delete the selected tasks?"))
                        return false;
                    else
                        return true;
                }
                else if (a == 1) { }

            }
            else {
                alert("Please select at least one task");
                return false;
            }
        }
        //function to select all checkbox of gridview DataGrid1
        function setCheckAll(obj) {
            var chkGridcontrols = document.getElementById("<%=DataGrid1.ClientID%>").getElementsByTagName("input");
            for (var i = 0; i < chkGridcontrols.length; i++) {
                chkGridcontrols[i].checked = obj.checked;
            }

            return true;
        }
        
         //function to uncheck 'select all' when any checkbox in gridview is unchecked
        function validate(obj) {
            debugger;
            var chkGridcontrols = document.getElementById("<%=DataGrid1.ClientID%>").getElementsByTagName("input");
            //var chkheaderSelect = document.getElementById("chkheaderSelect");
            var chkheaderSelect = document.getElementById("DataGrid1_ctl01_chkheaderSelect");

            if (!obj.checked)
                chkheaderSelect.checked = false;
        }



        function setListingsImage(obj) {
            var img = new Image();
            img.src = obj.src;
            if (img.height > img.width) {
                obj.height = 240;
                var width = (220 * img.width) / img.height;
                width = width > 240 ? 240 : width;
                obj.width = (width > 0 ? width : 240);
            }
            else {
                obj.width = 220;
                var height = (240 * img.height) / img.width;
                height = height > 220 ? 220 : height;
                obj.height = (height > 0 ? (height - 6) : 240);

            }
        }

        </script>

    </telerik:RadCodeBlock>
</head>
<body>
    <form id="form2" runat="server">
        <telerik:RadScriptBlock runat="server" ID="scriptBlock">

            <script type="text/javascript">
        //<![CDATA[
        
            function onRowDropping(sender, args) {
                if (sender.get_id() == "<%=DataGrid1.ClientID %>") {
                    var node = args.get_destinationHtmlElement();
                    //alert("Dropped target element id is: " + "<%=DataGrid1.ClientID %>");
                    if (!isChildOf('<%=DataGrid1.ClientID %>', node))
                    {
                        args.set_cancel(true);
                    }
                }
                else {
                    var node = args.get_destinationHtmlElement();
                    if (!isChildOf('trashCan', node)) {
                        args.set_cancel(true);
                    }
                    else {
                        if (confirm("Are you sure you want to delete this order?"))
                            args.set_destinationHtmlElement($get('trashCan'));
                        else
                            args.set_cancel(true);
                    }
                }
            }

            function isChildOf(parentId, element) {
                while (element) {
                    if (element.id && element.id.indexOf(parentId) > -1) {
                        return true;
                    }
                    element = element.parentNode;
                }
                return false;
            }


            //]]>
            </script>

        </telerik:RadScriptBlock>
        <div id="wrapper">
            <uc2:Admin_Header ID="Admin_Header1" runat="server" />
            <div id="Content">
                <div id="Content_98">
                    <div class="titleText" style="margin: 10px 0 10px 0; display: none;">
                        Categories&nbsp;|&nbsp;</div>
                    <div>
                        <%--<uc4:Notifications ID="Notifications" runat="server"  />--%>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="left" valign="top">
                                    <div style="padding: 0px 0 0px 5px; margin: 14px  0 -2px 0;">
                                        <table>
                                            <tr>
                                                <td id="s1">
                                                    <span class="function-menu" onmousedown="slideContent('section-1')">Filter</span>
                                                </td>
                                                <td id="s2">
                                                    <span class="function-menu" onmousedown="slideContent('section-2')">Action</span>
                                                </td>
                                                <td id="s3">
                                                    <span class="function-menu-1" onmousedown="slideContent('section-3')">Quick Search</span>
                                                </td>
                                                <td>
                                                    <span id="TopFilterLable" runat="server" class="topfilter"></span>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div>
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" onclick="return TABLE1_onclick()"
                                            id="TABLE1">
                                            <tr>
                                                <td style="border-radius: 9px 9px 9px 9px; height: 62px;" bgcolor="#3e3f3d" width="100%">
                                                    <div class="slider">
                                                        <div class="slidercontent" id="slider" style="left: 0px; top: 2px" onclick="return slider_onclick()">
                                                            <div id="section-1" class="section upper" style="margin-top: 0px;">
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td width="45" valign="top" style="padding-top: 20px;">
                                                                            <b style="color: #ffffff;">Filter:</b>
                                                                        </td>
                                                                        <td valign="top" class="whitetext2" width="9%">
                                                                            <br />
                                                                            <span class="whitetext2">
                                                                                <%--<asp:DropDownList ID="drpFilter" runat="server" AutoPostBack="True" Width="96%" OnSelectedIndexChanged="drpFilter_SelectedIndexChanged"--%>
                                                                                <asp:DropDownList ID="drpFilter" runat="server" AutoPostBack="True" Width="96%" CssClass="filerDrpodown"
                                                                                    OnSelectedIndexChanged="drpFilter_SelectedIndexChanged">
                                                                                </asp:DropDownList>
                                                                            </span>
                                                                        </td>
                                                                        <%--====================================================================--%>
                                                                        <%--<td width="9%" valign="top" style="color: White; font-size: 14px">
                                                                            Pool<br />
                                                                            <span class="whitetext2"><span class="whitetext2"><span class="whitetext2">
                                                                                <asp:DropDownList ID="Drpool" runat="server" CssClass="filerDrpodown" AutoPostBack="True" Width="96%" OnSelectedIndexChanged="Drpool_SelectedIndexChanged">
                                                                                <asp:ListItem Value="0">[no filter]</asp:ListItem>
                                                                                <asp:ListItem Value="Developers Pool">Show my developers pool tasks</asp:ListItem> 
                                                                                <asp:ListItem Value="Designers Pool">Show my designers pool tasks</asp:ListItem> 
                                                                                </asp:DropDownList>
                                                                            </span></span></span>
                                                                        </td>--%>
                                                                        <%--====================================================================--%>
                                                                        <td width="9%" valign="top" style="color: White; font-size: 14px">
                                                                            Project&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Arch.<br />
                                                                            <span class="whitetext2"><span class="whitetext2"><span class="whitetext2">
                                                                                <%--<asp:DropDownList ID="drpProjects" runat="server" CssClass="filerDrpodown" AutoPostBack="True" OnSelectedIndexChanged="drpProjects_SelectedIndexChanged" Width="96%">--%>
                                                                                <asp:DropDownList ID="drpProjects" runat="server" CssClass="filerDrpodown" AutoPostBack="True"
                                                                                    OnSelectedIndexChanged="drpProjects_SelectedIndexChanged" Width="70%">
                                                                                </asp:DropDownList>
                                                                                <asp:CheckBox ID="chkArchive" runat="server" AutoPostBack="True" OnCheckedChanged="chkArchive_CheckedChanged" />
                                                                            </span></span></span>
                                                                        </td>
                                                                        <td width="9%" valign="top" style="color: White; font-size: 14px">
                                                                            Category<br />
                                                                            <%--<asp:DropDownList ID="drpCategories" runat="server" CssClass="filerDrpodown" AutoPostBack="True" OnSelectedIndexChanged="drpCategories_SelectedIndexChanged" Width="96%">--%>
                                                                            <asp:DropDownList ID="drpCategories" runat="server" CssClass="filerDrpodown" AutoPostBack="True"
                                                                                OnSelectedIndexChanged="drpCategories_SelectedIndexChanged" Width="96%">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td width="9%" valign="top" style="color: White; font-size: 14px">
                                                                            Reported by<br />
                                                                            <%--<asp:DropDownList ID="drpReportedBy" runat="server" CssClass="filerDrpodown" AutoPostBack="True"  OnSelectedIndexChanged="drpReportedBy_SelectedIndexChanged" Width="96%">--%>
                                                                            <asp:DropDownList ID="drpReportedBy" runat="server" CssClass="filerDrpodown" OnSelectedIndexChanged="drpReportedBy_SelectedIndexChanged"
                                                                                AutoPostBack="True" Width="96%">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td width="9%" valign="top" style="color: White; font-size: 14px">
                                                                            Priority<br />
                                                                            <%--<asp:DropDownList ID="drpPriorities" runat="server" CssClass="filerDrpodown" AutoPostBack="True" OnSelectedIndexChanged="drpPriorities_SelectedIndexChanged" Width="96%">--%>
                                                                            <asp:DropDownList ID="drpPriorities" runat="server" CssClass="filerDrpodown" AutoPostBack="True"
                                                                                OnSelectedIndexChanged="drpPriorities_SelectedIndexChanged" Width="96%">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td id="tdAssignedTo" runat="server" width="9%" valign="top" style="color: White;
                                                                            font-size: 14px">
                                                                            Assigned to<br />
                                                                            <span class="whitetext2">
                                                                                <%--<asp:DropDownList ID="DropDownList1" runat="server" CssClass="filerDrpodown" AutoPostBack="True" OnSelectedIndexChanged="drpAssignedTo_SelectedIndexChanged" Width="96%">--%>
                                                                                <asp:DropDownList ID="drpAssignedToNew" runat="server" CssClass="filerDrpodown" OnSelectedIndexChanged="drpAssignedToNMew_SelectedIndexChanged"
                                                                                    AutoPostBack="True" Width="96%">
                                                                                </asp:DropDownList>
                                                                            </span>
                                                                        </td>
                                                                        <td width="9%" valign="top" style="color: White; font-size: 14px">
                                                                            Status<br />
                                                                            <%--<asp:DropDownList ID="drpStatuses" runat="server" CssClass="filerDrpodown" AutoPostBack="True"  OnSelectedIndexChanged="drpStatuses_SelectedIndexChanged" Width="96%">--%>
                                                                            <asp:DropDownList ID="drpStatuses" runat="server" CssClass="filerDrpodown" OnSelectedIndexChanged="drpStatuses_SelectedIndexChanged"
                                                                                AutoPostBack="True" Width="96%">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td width="5%" valign="top" style="color: White; font-size: 14px">
                                                                            Items<br />
                                                                            <%--<asp:DropDownList ID="drpPerPage1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpPerPage1_SelectedIndexChanged">--%>
                                                                            <asp:DropDownList ID="drpPerPage1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpPerPage1_SelectedIndexChanged">
                                                                                <asp:ListItem Value="20" Selected="True">20</asp:ListItem>
                                                                                <asp:ListItem Value="50">50</asp:ListItem>
                                                                                <asp:ListItem Value="100">100</asp:ListItem>
                                                                                <asp:ListItem Value="00">All</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td valign="top" width="85" style="padding-top: 20px">
                                                                            <%-- <asp:Button ID="btnClearFilters" runat="server" OnClick="btnClearFilters_Click" Text="Clear filters"--%>
                                                                            <asp:Button ID="btnClearFilters" runat="server" Text="Clear filters" OnClick="btnClearFilters_Click"
                                                                                CssClass="clearfilter" />
                                                                        </td>
                                                                        <td valign="top" class="whitetext2">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td valign="top" class="whitetext2" style="min-width: 140px; color: White; font-size: 14px">
                                                                            <%--<asp:CheckBox ID="chkViewDeleted" runat="server" Text="View only deleted" AutoPostBack="True"  OnCheckedChanged="chkViewDeleted_CheckedChanged" /><br />--%>
                                                                            <asp:CheckBox ID="chkViewDeleted" runat="server" Text="View only deleted" AutoPostBack="True"
                                                                                OnCheckedChanged="chkViewDeleted_CheckedChanged" /><br />
                                                                            <%--<asp:CheckBox ID="chkHideClosed" runat="server" Text="View closed" AutoPostBack="True" OnCheckedChanged="chkHideClosed_CheckedChanged" />--%>
                                                                            <asp:CheckBox ID="chkHideClosed" runat="server" Text="View closed" AutoPostBack="True"
                                                                                OnCheckedChanged="chkHideClosed_CheckedChanged" />
                                                                        </td>
                                                                        <td valign="bottom" class="whitetext2" style="min-width: 170px; color: White; font-size: 15px">
                                                                            <%--<asp:CheckBox ID="ChkReadTask" runat="server" Text="Include read&nbsp;&nbsp;" Checked="true" AutoPostBack="True" OnCheckedChanged="ChkReadTask_CheckedChanged" />--%>
                                                                            <asp:CheckBox ID="ChkReadTask" runat="server" Text="Include read&nbsp;&nbsp;" Checked="true"
                                                                                AutoPostBack="True" OnCheckedChanged="ChkReadTask_CheckedChanged" />
                                                                            <span style="color: #ffffff; float: right;"><b>Records: </b>
                                                                                <asp:Label ID="LblRecordsno" runat="server" Text=""></asp:Label></span>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <div id="section-2" class="section upper" style="padding-top: 5px; height: 36px;">
                                                                <div id="DivMiltiselection" runat="server" style="color: White; font-size: 14px">
                                                                    <b style="color: #ffffff;">Action:</b>
                                                                    <%--<asp:LinkButton ID="LnkBtnToDeleteAll" runat="server" OnClick="LnkBtnToDeleteAll_Click" CssClass="whitetext2" OnClientClick="return validateonbtn(0);">Delete selected</asp:LinkButton>&nbsp;&nbsp;--%>
                                                                    <asp:LinkButton ID="LnkBtnToDeleteAll" runat="server" OnClick="LnkBtnToDeleteAll_Click"
                                                                        CssClass="whitetext2" OnClientClick="return validateonbtn(0);">Delete selected</asp:LinkButton>&nbsp;&nbsp;
                                                                    <%--<asp:LinkButton ID="LnkBtnToMarkAllRead" runat="server" OnClientClick="return validateonbtn(1);" OnClick="LnkBtnToMarkAllRead_Click" CssClass="whitetext2">Mark read</asp:LinkButton><br />--%>
                                                                    <asp:LinkButton ID="LnkBtnToMarkAllRead" runat="server" CssClass="whitetext2" OnClientClick="return validateonbtn(1);"
                                                                        OnClick="LnkBtnToMarkAllRead_Click">Mark read</asp:LinkButton><br />
                                                                    <span style="float: left;"><span style="color: Red;">&nbsp;</span></span>
                                                                    <%--<asp:Button ID="LnkBtnToChngPR" runat="server" BackColor="transparent" BorderStyle="none" Text="Change priority to" OnClick="LnkBtnToChngPR_Click" Style="color:White;font-size: 14px; margin-left:-15px" />--%>
                                                                    <asp:Button ID="LnkBtnToChngPR" runat="server" BackColor="transparent" BorderStyle="none"
                                                                        Text="Change priority to" OnClick="LnkBtnToChngPR_Click" Style="color: White;
                                                                        font-size: 14px; margin-left: -15px" />
                                                                    <asp:DropDownList ID="DrpMultipleTaskPR" runat="server" CssClass="filerDrpodown"
                                                                        AutoPostBack="true" Width="180px">
                                                                    </asp:DropDownList>
                                                                    &nbsp;&nbsp;
                                                                    <%--<asp:Button ID="LnkChngAssingedTo" runat="server" BackColor="transparent" BorderStyle="none" Text="Change assigned to" OnClick="LnkChngAssingedTo_Click"   Style="color: White; font-size: 14px" />--%>
                                                                    <asp:Button ID="LnkChngAssingedTo" runat="server" BackColor="transparent" BorderStyle="none"
                                                                        Text="Change assigned to" OnClick="LnkChngAssingedTo_Click" Style="color: White;
                                                                        font-size: 14px" />
                                                                    <asp:DropDownList ID="DrpMultipleTaskAssing" runat="server" CssClass="filerDrpodown"
                                                                        AutoPostBack="false" Width="180px">
                                                                    </asp:DropDownList>
                                                                    &nbsp;&nbsp;
                                                                    <%--<asp:Button ID="LnkChngStatus" runat="server" BackColor="transparent" BorderStyle="none" Text="Change status to" OnClick="LnkChngStatus_Click" Style="color: White;font-size: 14px" />--%>
                                                                    <asp:Button ID="LnkChngStatus" runat="server" BackColor="transparent" BorderStyle="none"
                                                                        Text="Change status to" OnClick="LnkChngStatus_Click" Style="color: White; font-size: 14px" />
                                                                    <asp:DropDownList ID="DrpMultipleTaskStatus" runat="server" CssClass="filerDrpodown"
                                                                        AutoPostBack="false" Width="180px">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div id="section-3" class="section upper" style="padding: 0px;">
                                                                <table>
                                                                    <tr>
                                                                        <td valign="bottom" style="padding-bottom: 4px;">
                                                                            <table id="tblSearchRequest" runat="server" border="0" cellpadding="0" cellspacing="2">
                                                                                <tr>
                                                                                    <td>
                                                                                        &nbsp;
                                                                                    </td>
                                                                                    <td style="min-width: 70px; color: White; font-size: 14px">
                                                                                        Request ID:
                                                                                    </td>
                                                                                    <td class="whitetext2" width="40">
                                                                                        <asp:TextBox ID="txtRequestId" runat="server" Width="100%" Height="15px" MaxLength="10"
                                                                                            Style="min-width: 20px;"></asp:TextBox>
                                                                                    </td>
                                                                                    <td style="color: White; font-size: 14px">
                                                                                        <%--<asp:Button ID="btnGo" runat="server" Text="Go" CausesValidation="False" OnClick="btnGo_Click" CssClass="goBtn" />--%>
                                                                                        <asp:Button ID="btnGo" runat="server" Text="Go" CausesValidation="False" CssClass="goBtn"
                                                                                            OnClick="btnGo_Click" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td valign="bottom" style="padding-bottom: 4px;">
                                                                            <table cellpadding="0" cellspacing="2">
                                                                                <tr>
                                                                                    <td>
                                                                                    </td>
                                                                                    <td style="min-width: 40px; color: White; font-size: 14px">
                                                                                        Task ID
                                                                                    </td>
                                                                                    <td width="40">
                                                                                        <asp:TextBox ID="txtTaskId" runat="server" Width="100%" Height="15px" MaxLength="10"
                                                                                            Style="min-width: 20px;"></asp:TextBox>
                                                                                    </td>
                                                                                    <td>
                                                                                        <%--<asp:Button ID="btnGotoTask" runat="server" Text="Go" CausesValidation="False" OnClick="btnGotoTask_Click" CssClass="goBtn" />--%>
                                                                                        <asp:Button ID="btnGotoTask" runat="server" Text="Go" CausesValidation="False" CssClass="goBtn"
                                                                                            OnClick="btnGotoTask_Click" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td width="55%" style="padding-left: 3px; padding-bottom: 5px">
                                                                            <table id="Tblsearchbyword" runat="server" border="0" cellpadding="0" cellspacing="2">
                                                                                <tr>
                                                                                    <td align="left" colspan="2" style="color: White; font-size: 14px">
                                                                                        Search by word:
                                                                                        <asp:CheckBox ID="ChkTask" runat="server" Text="Task" />
                                                                                        <asp:CheckBox ID="ChkCrs" runat="server" Text="CRs" />
                                                                                        <asp:CheckBox ID="ChkIncludecomment" runat="server" Text="Include comments" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr style="padding-top: 2px">
                                                                                    <td width="100%">
                                                                                        <asp:TextBox ID="txtWord" runat="server" Width="100%" Height="15px"></asp:TextBox>
                                                                                    </td>
                                                                                    <td width="37" align="right">
                                                                                        <%--<asp:Button ID="BtnGoWrdSearch" runat="server" Text="Go" CssClass="goBtn" CausesValidation="False" OnClick="BtnGoWrdSearch_Click" />--%>
                                                                                        <asp:Button ID="BtnGoWrdSearch" runat="server" Text="Go" CssClass="goBtn" CausesValidation="False"
                                                                                            OnClick="BtnGoWrdSearch_Click" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <%--<br/>
                    <br/>--%>
                    <div style="height: 80px">
                        <div id="dvemp" runat="server">
                            All tasks priority
                        </div>
                        <%-- <br />--%>
                        <div>
                            <%--==================0-Immediate task========================================================--%>
                            <asp:Label ID="lbl_Immediate" runat="server"></asp:Label><asp:LinkButton ID="lnkPr0Task"
                                runat="server" CausesValidation="False" OnClick="lnkPr0Task_Click"></asp:LinkButton>
                            <%--==========================================================================--%>
                            <%--&nbsp;|&nbsp;--%>
                            <%--======================1-a Do-NOW task====================================================--%>
                            <asp:Label ID="lbl_1adonow" runat="server"></asp:Label>
                            <asp:LinkButton ID="lnkHighestTasks" runat="server" CausesValidation="False" OnClick="lnkHighestTasks_Click"></asp:LinkButton>
                            <%--==========================================================================--%>
                            <%--&nbsp;|&nbsp;--%>
                            <%--=====================1-b high task=====================================================--%>
                            <asp:Label ID="lbl_1bhigh" runat="server"></asp:Label>
                            <asp:LinkButton ID="lnkHighTasks" runat="server" CausesValidation="False" OnClick="lnkHighTasks_Click"></asp:LinkButton>
                            <%--==========================================================================--%>
                            <%--&nbsp;|&nbsp;--%>
                            <%--======================1-c normal task====================================================--%>
                            <asp:Label ID="lbl_1cnormal" runat="server"></asp:Label>
                            <asp:LinkButton ID="Lnk1cTasks" runat="server" CausesValidation="False" OnClick="Lnk1cTasks_Click"></asp:LinkButton>
                            <%--==========================================================================--%>
                            <%--&nbsp;|&nbsp;--%>
                            <%--======================2-not urgent task================================================--%>
                            <asp:Label ID="lbl_2noturgent" runat="server"></asp:Label>
                            <asp:LinkButton ID="Link2notTasks" runat="server" CausesValidation="False" OnClick="Link2notTasks_Click"></asp:LinkButton>
                            <%--==========================================================================--%>
                            <%--&nbsp;|&nbsp;--%>
                            <%--======================3 Low task====================================================--%>
                            <asp:Label ID="lbl_3low" runat="server"></asp:Label>
                            <asp:LinkButton ID="Link3lowTasks" runat="server" CausesValidation="False" OnClick="Link3lowTasks_Click1"></asp:LinkButton>
                            <%--==========================================================================--%>
                            <%--&nbsp;|&nbsp;--%>
                            <%--======================4 very low prioriity==============================================--%>
                            <asp:Label ID="lbl4verylow" runat="server"></asp:Label>
                            <asp:LinkButton ID="Link4lowTasks" runat="server" CausesValidation="False" OnClick="Link4lowTasks_Click"></asp:LinkButton>
                            <%--==========================================================================--%>
                            <%--&nbsp;|&nbsp;--%>
                            <%--=======================5-parked===================================================--%>
                            <asp:Label ID="lbl5parked" runat="server"></asp:Label>
                            <asp:LinkButton ID="Link5parkedTasks" runat="server" CausesValidation="False" OnClick="Link5parkedTasks_Click"></asp:LinkButton>
                            <%--==================================================================================--%>
                            <%--&nbsp;|&nbsp;--%>
                            <%--=========================Unread comments======================================================--%>
                            <asp:Label ID="lblUnreadComment" runat="server"></asp:Label>
                            <asp:LinkButton ID="LnkTskNewcomment" runat="server" CausesValidation="false" ForeColor="#FF1493"
                                OnClick="LnkTskNewcomment_Click"></asp:LinkButton>
                            <%--=============================================================================================--%>
                            <%--&nbsp;|&nbsp;--%>
                            <%--=======================Question =======================================================--%>
                            <asp:Label ID="lblQuestion" runat="server"></asp:Label>
                            <asp:LinkButton ID="lnkUnansweredQuestions" runat="server" CausesValidation="False"
                                OnClick="lnkUnansweredQuestions_Click"></asp:LinkButton>
                            <%--==============================================================================================--%>
                            <%-- &nbsp;|&nbsp;--%>
                            <%--=======================Overdue==================================================================--%>
                            <asp:Label ID="lblOverdue" runat="server"></asp:Label>
                            <asp:LinkButton ID="BtnOverdue" runat="server" BackColor="transparent" BorderStyle="none"
                                CausesValidation="False" OnClick="BtnOverdue_Click"></asp:LinkButton>
                            <%--==================================================================================================--%>
                            <span id="adminpart" runat="server">
                                <%--&nbsp;|&nbsp;--%>
                                |&nbsp;ALL:&nbsp;
                                <%--===================================Tasks with new comment===============================================--%>
                                <asp:Label ID="lbltaskNewComment" runat="server"></asp:Label>
                                <asp:LinkButton ID="LnkMakeAllTaskCmmntRead" runat="server" CausesValidation="false"
                                    OnClick="LnkMakeAllTaskCmmntRead_Click"></asp:LinkButton>
                                <%--========================================================================================================--%>
                                <%--&nbsp;|&nbsp;--%>
                                <%--=======================All Overdue==================================================================--%>
                                <asp:Label ID="lblallOverDue" runat="server"></asp:Label>
                                <asp:LinkButton ID="BtnallOverdue" runat="server" BackColor="transparent" BorderStyle="none"
                                    CausesValidation="False" OnClick="BtnallOverdue_Click"></asp:LinkButton>
                                <%--=======================Overdue==================================================================--%>
                                <%--&nbsp;|&nbsp;--%>
                                <%--=================================================================================================================--%>
                                <asp:Label ID="lbltobeanswered" runat="server"></asp:Label>
                                <asp:LinkButton ID="lnktobeanswered" runat="server" CausesValidation="false" OnClick="lnktobeanswered_Click1"></asp:LinkButton>
                                <%--=================================================================================================================--%>
                                <%--&nbsp;|&nbsp;--%>
                                |&nbsp;<span style="color: Green;">CR:</span>&nbsp;
                                <%--=======================CR:New======================================================================--%>
                                <asp:Label ID="lblNewCR" runat="server"></asp:Label>
                                <asp:LinkButton ID="LnkNewCR" runat="server" OnClick="LnkNewCR_Click"></asp:LinkButton>
                                <%--=====================================================================================================--%>
                                <%--&nbsp;|&nbsp;--%>
                                <%--=======================Unread comments================================================================--%>
                                <asp:Label ID="lblNotreadcomment" runat="server"></asp:Label>
                                <asp:LinkButton ID="LnkCrNewComment" runat="server" OnClick="LnkCrNewComment_Click"></asp:LinkButton>
                                <%--=======================================================================================================--%>
                            </span>
                            <asp:ImageButton ID="refreshbtn" runat="server" ImageUrl="~/Admin/images/reload.png"
                                Style="vertical-align: bottom;" Height="17px" Width="20px" OnClick="refreshbtn_Click" />
                        </div>
                        <br />
                        <div>
                          <div id="dvadmin" runat="server" style="float:left;width:300px">
                           
                            Select employee name
                            <asp:DropDownList ID="drpAssignedTo" runat="server" OnSelectedIndexChanged="drpAssignedTo_SelectedIndexChanged"
                                AutoPostBack="True">
                            </asp:DropDownList>
                          </div>&nbsp;
                           <div id="Hidelink" runat="server" style="float:left;width:150px">
                            <asp:Button ID="lnkbtnforemail" runat="server" OnClick="LinkButton1_Click" Text="Advise emp of re-ordering">
                            </asp:Button>
                         </div>
                          <div id="leaveId" runat="server"  style="float:left;font-weight:bold;width:500px;">
                         <h3>
                          <span style="color: red; display: block; margin-left: 26px; margin-top: -13px; float: right; font-size: 19px;"> 
                            &nbsp;(On leave
                            <asp:Label ID="lblleavefrom" runat="server"></asp:Label>
                            &nbsp;returns
                            <asp:Label ID="lblleaveto" runat="server"></asp:Label>)
                            </span>
                          </h3>
                          </div>
                         
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%--<div id="divMessage" runat="server" style="color: Red; font-weight: bold;"> </div>--%>
        <div id="statusgrid" runat="server">
           
            <%--****************************************************************************************************************************************--%>
            
            <asp:HiddenField ID="hdnOrderby" runat="server" />
                        
            <%-- ===========================================================================--%>
            <telerik:RadGrid ID="DataGrid1" runat="server" Width="100%" ShowFooter="True" OnRowDrop="grdPendingOrders_RowDrop"
                AllowMultiRowSelection="true" EnableEmbeddedSkins="true" OnItemCommand="DataGrid1_RowCommand"
                OnItemDataBound="DataGrid1_RowDataBound" BorderColor="#cccccc" AutoGenerateColumns="False"
                CellPadding="5" AllowSorting="true" OnSortCommand="DataGrid1_OnSortCommand">
                <MasterTableView PagerStyle-AlwaysVisible="true" DataKeyNames="task_id">
                    <Columns>
                        <%--0--%>
                        <telerik:GridTemplateColumn HeaderText="ID" Visible="False" SortExpression="task_id">
                            <ItemTemplate>
                                <asp:Label ID="lbltaskidV" runat="server" Text='<%#bind("task_id") %>'></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <%--==================================================================================================--%>
                        <telerik:GridTemplateColumn HeaderStyle-Width="15px" ItemStyle-Width="10px" FooterStyle-Width="10px">
                            <HeaderTemplate>
                                <%--<asp:CheckBox ID="chkheaderSelect"  runat="server" onclick="return setCheckAll(this);" Text="Select all" />--%>
                                <asp:CheckBox ID="chkheaderSelect" runat="server" onclick="return setCheckAll(this);" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server" onclick="return validate(this);" />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <%--==================================================================================================--%>
                        <%--1--%>
                        <telerik:GridTemplateColumn HeaderText="ShrtDesc" Visible="False" SortExpression="short_desc">
                            <ItemTemplate>
                                <asp:Label ID="lblshortdescV" runat="server" Text='<%#bind("short_desc") %>'></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <%--2--%>
                        <telerik:GridTemplateColumn HeaderText="Short description" SortExpression="short_desc">
                            <ItemTemplate>
                                <asp:Label ID="lblshortdesc" runat="server" Text='<%# bind("short_desc") %>'></asp:Label>
                                <telerik:RadToolTip ID="RadToolTip1" BackColor="#FEFFB3" runat="server" OffsetX="75"
                                    AutoCloseDelay="60000" HideEvent="LeaveTargetAndToolTip" OffsetY="10" TargetControlID="lblshortdesc"
                                    Width="500px" RelativeTo="Element" Position="BottomLeft" ShowEvent="OnMouseOver">
                                    <%# DataBinder.Eval(Container, "DataItem.ShowAllComment")%>
                                </telerik:RadToolTip>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <%--3--%>
                        <telerik:GridHyperLinkColumn Text="(+)" Visible="False" DataNavigateUrlFields="task_id"
                            NavigateUrl="edit_task.aspx" DataNavigateUrlFormatString="edit_task.aspx?id={0}"
                            SortExpression="short_desc">
                        </telerik:GridHyperLinkColumn>
                        <%--4--%>
                        <telerik:GridTemplateColumn HeaderText="Read/Unread">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkReadMark" runat="server" Width="130px" CommandName="ReadMark">(Unread. Mark as read)</asp:LinkButton>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <%--5--%>
                        <telerik:GridTemplateColumn HeaderText="Project" SortExpression="project">
                            <ItemTemplate>
                                <asp:Label ID="lblproject" runat="server" Text='<%# Eval("project")%>'></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <%--6--%>
                        <telerik:GridTemplateColumn HeaderText="Status" SortExpression="status">
                            <ItemTemplate>
                                <asp:Label ID="lblstatus" runat="server" Text='<%#bind("status") %>'></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                         <telerik:GridTemplateColumn HeaderText="Due Date" SortExpression="Duedate">
                    <ItemTemplate>
                      <asp:Label ID="lblduedate" runat="server" Text='<%# bind("Duedate") %>'></asp:Label>
                    </ItemTemplate>
                  </telerik:GridTemplateColumn>
                        <%--7--%>
                        <telerik:GridTemplateColumn HeaderText="Priority" SortExpression="priority">
                            <ItemTemplate>
                                <asp:Label ID="lblpriority" Visible="false" runat="server" Text='<%#bind("priority") %>'></asp:Label>
                                <asp:DropDownList ID="ddlpriority" runat="server" Width="100px" OnSelectedIndexChanged="ddlpriority_SelectedIndexChanged"
                                    AutoPostBack="true" />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <%--8--%>
                        <telerik:GridTemplateColumn HeaderText="Assigned to" SortExpression="assigned_to_user">
                            <ItemTemplate>
                                <asp:Label ID="lblassignedtouser" runat="server" Text='<%# Eval("assigned_to_user") %>'></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <%--9--%>
                        <telerik:GridTemplateColumn HeaderText="Last Updated By" SortExpression="last_updated_user">
                            <ItemTemplate>
                                <asp:Label ID="lbllastupdateduser" runat="server" Text='<%# Eval("last_updated_user")%>'></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <%--10--%>
                        <telerik:GridTemplateColumn HeaderText="Last Updated" SortExpression="last_updated_date">
                            <ItemTemplate>
                                <asp:Label ID="lblLastUpdatedOn" runat="server" Text='<%# bind("last_updated_date") %>'></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <%--11--%>
                        <telerik:GridTemplateColumn HeaderText="Category" SortExpression="category">
                            <ItemTemplate>
                                <asp:Label ID="lblcategory" runat="server" Text='<%#bind("category") %>'></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <%--12--%>
                        <telerik:GridTemplateColumn HeaderText="ETC">
                            <ItemTemplate>
                                <asp:Label ID="lbletc" runat="server"></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <%--13--%>
                        <telerik:GridTemplateColumn HeaderText="Time to date">
                            <ItemTemplate>
                                <asp:Label ID="lblTimetodate" runat="server"></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <%--14--%>
                        <telerik:GridTemplateColumn HeaderText="Balance">
                            <ItemTemplate>
                                <asp:Label ID="lblbalance" runat="server"></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <%--15--%>
                        <telerik:GridTemplateColumn HeaderText="Reported by" SortExpression="reported_user"
                            Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblreporteduser" runat="server" Text='<%#bind("reported_user") %>'></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <%--16--%>
                        <telerik:GridTemplateColumn HeaderText="Reported on" SortExpression="reported_date"
                            Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblReportedOn" runat="server" Text='<%# Bind("reported_date") %>'></asp:Label>
                                <asp:HiddenField ID="hd" runat="server" Value='<%# Eval("ETC")%>' />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <%--17--%>
                        <telerik:GridTemplateColumn HeaderText="ETC" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbletcV" runat="server" Text='<%# bind("ETC") %>'></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <%--18--%>
                        <telerik:GridTemplateColumn HeaderText="ID" SortExpression="task_id">
                            <ItemTemplate>
                                <asp:Label ID="lbltaskid" runat="server" Text='<%# bind("task_id") %>'></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <%--19--%>
                        <telerik:GridTemplateColumn HeaderText="Order" SortExpression="OrderId">
                            <ItemTemplate>
                                <asp:TextBox ID="txtorder" runat="server" Width="50px" MaxLength="5" AutoPostBack="true"
                                    Text='<%# bind("OrderId") %>' OnTextChanged="TextBox2_textchanged"></asp:TextBox>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                    </Columns>
                </MasterTableView>
                <ClientSettings EnableRowHoverStyle="false" AllowRowsDragDrop="True" AllowColumnsReorder="true"
                    ReorderColumnsOnClient="true" AllowAutoScrollOnDragDrop="false">
                    <Selecting AllowRowSelect="True" EnableDragToSelectRows="false" UseClientSelectColumnOnly="true">
                    </Selecting>
                    <ClientEvents OnRowDropping="onRowDropping"></ClientEvents>
                    <Scrolling AllowScroll="false" UseStaticHeaders="true"></Scrolling>
                </ClientSettings>
            </telerik:RadGrid>
            <%--</ContentTemplate>
            </asp:UpdatePanel>--%>
            <%--****************************************************************************************************************************************--%>
        </div>
        <div style="font-size: 12px" id="clientGrid" runat="server">
            <asp:DataGrid ID="DataGrid2" runat="server" Width="100%" AutoGenerateColumns="False"
                CellPadding="5">
                <HeaderStyle Font-Bold="false" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Top" BackColor="#282C5F"
                    CssClass="tblTitle1" />
                <Columns>
                    <asp:BoundColumn DataField="RequestId" Visible="false" HeaderText="ID" SortExpression="RequestId">
                    </asp:BoundColumn>
                    <asp:TemplateColumn>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkSelect" runat="server" /></ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Short description" SortExpression="ShortDescr" HeaderStyle-CssClass="wordwrap">
                        <HeaderStyle Width="300px" />
                        <ItemTemplate>
                            <asp:Label ID="lblshrtDesc" runat="server" CssClass="wordwrap" Text='<%# bind("ShortDescr") %>'></asp:Label>
                            <div id="lblshrtDesc1" runat="server" style="display: none; position: absolute; background-color: #FEFFB3;
                                width: 730px; border: solid 1px #333333; margin-top: 5px; padding-left: 3px">
                                <asp:Label ID="LblLastcomment" runat="server" Text='<%# bind("ShowAllComment") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Read/Unread" SortExpression="" HeaderStyle-CssClass="wordwrap">
                        <HeaderStyle Width="128px" />
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkReadMark" runat="server" CssClass="wordwrap" CommandName="ReadMark">(Unread. Mark as read)</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="ProjectName" HeaderText="Project name" SortExpression="ProjectName">
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="Status" HeaderText="Status" SortExpression="ClientRequest.Status">
                    </asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Priority" SortExpression="ClientRequest.Priority"></asp:BoundColumn>
                    <asp:BoundColumn DataField="Priority" HeaderText="Priority" SortExpression="ClientRequest.Priority"
                        Visible="False"></asp:BoundColumn>
                    <asp:BoundColumn DataField="CompanyName" HeaderText="Company name" SortExpression="CompanyName">
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="LastUpdatedBy" HeaderText="Last updated by" SortExpression="ClientRequest.LastUpdatedBy">
                    </asp:BoundColumn>
                    <asp:TemplateColumn HeaderText="Last updated" SortExpression="ClientRequest.LastUpdatedOn">
                        <ItemTemplate>
                            <asp:Label ID="lblLastUpdatedOn" runat="server" Text='<%# bind("LastUpdatedOn") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="AddedBy" HeaderText="Change added by" SortExpression="ClientRequest.AddedBy">
                        <HeaderStyle Width="60px" />
                    </asp:BoundColumn>
                    <asp:TemplateColumn HeaderText="Change added on" SortExpression="ClientRequest.RequestDate">
                        <ItemTemplate>
                            <asp:Label ID="lblChangeAddedOn" runat="server" Text='<%# bind("RequestDate") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Type" SortExpression="">
                        <HeaderStyle Width="66px" />
                        <ItemTemplate>
                            <asp:LinkButton ID="Supportype" runat="server" CommandName="SupprtLink">Support type</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="RequestId" HeaderText="ID" SortExpression="RequestId"></asp:BoundColumn>
                    <asp:HyperLinkColumn HeaderText="Edit" Text="edit" DataNavigateUrlField="RequestId"
                        DataNavigateUrlFormatString="edit_request.aspx?reqid={0}" NavigateUrl="edit_request.aspx">
                    </asp:HyperLinkColumn>
                </Columns>
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Top" />
            </asp:DataGrid>
        </div>
        <div class="exFooter">
            <asp:Panel ID="Panel1" runat="server">
                <div style="float: left; margin: 15px 10px 10px;">
                    <table>
                        <tr>
                            <td align="right">
                                <div class="img">
                                    <div style="z-index: -1px; text-align: center;">
                                        <img id='loadingImage' class="image-holder" align="middle" style="z-index: 9999" />
                                    </div>
                                    <div style="position: absolute; z-index: 9999; text-align: center;">
                                        <img id="EmpImg" alt="Error on loading" runat="server" style="margin: 0px 0 0 0px;
                                            cursor: pointer; z-index: -1000; border: 5px" width="220" height="240" />
                                    </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <a id="trashCan" href="#" onclick="return false;"></a>
            <div class="exMessage" runat="server" id="msg" visible="false" enableviewstate="false">
            </div>
        </div>
        <table id="ShowdetailsofEmp" runat="server">
            <tr>
                <td>
                    &nbsp;&nbsp;Name:</td>
                <td>
                    <asp:Label ID="lblname" runat="server" Font-Bold="true"></asp:Label></td>
            </tr>
            <tr>
                <td>
                    &nbsp;&nbsp;Title:</td>
                <td>
                    <asp:Label ID="lbltitle" runat="server" Font-Bold="true"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;&nbsp;Skills:</td>
                <td>
                    <asp:Label ID="lblskill" runat="server" Font-Bold="true"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;&nbsp;DOJ:</td>
                <td>
                    <asp:Label ID="lbldob" runat="server" Font-Bold="true"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;Resume:</td>
                <td>
                    <asp:LinkButton ID="downloadcv" runat="server" Font-Bold="true" Text="Download CV"
                        OnClick="downloadcv_Click"></asp:LinkButton>
                        <%--<asp:HiddenField ID="hdnclass" runat="server" />--%>
                </td>
            </tr>
        </table>
        <uc1:footer ID="Footer1" runat="server" />
    </form>
    <div id="hdnclass" style="display:block" runat="server"></div>
     <script src="http://code.jquery.com/jquery-1.9.1.js" type="text/javascript"></script>
     <script type="text/javascript">


         $(document).ready(function () { 
             if ($('#hdnclass').is(':hidden')) {
                 $("#DataGrid1 .rgMasterTable tbody tr:last-child").addClass("Priority");
             }
             else {
                 $("#DataGrid1 .rgMasterTable tbody tr:last-child").removeClass("Priority");
             }

             $("#DataGrid1 .rgMasterTable tbody tr:last-child").mouseenter(function () {
                 $("#DataGrid1 .rgMasterTable tbody tr:last-child").removeClass("Priority");
             });
         });

        

          
         
       
          
         
    </script>
    <style type="text/css">
            .Priority { background:red !important;}
    </style>
</body>
</html>
