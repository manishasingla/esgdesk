﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using Telerik.Web.UI;
using Telerik.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Drawing;
public partial class Admin_Priority_reports : System.Web.UI.Page
{
    StringBuilder sql = new StringBuilder();
    string newsql;
    public string assigned_user = "";
    bool blnadmin;
    bool hidecheck;
    bool adminhide;
    static string strConn = ConfigurationManager.AppSettings["strConn"];

    protected void Page_Load(object sender, EventArgs e)
    {

     
       
        try
        {
            EmpImg.Attributes.Add("onload", "imageLoaded(this);");
            EmpImg.Attributes.Add("onload", "setListingsImage(this);");
            DrpMultipleTaskPR.Attributes.Add("onchange", "return EnablePRlink();");
            DrpMultipleTaskAssing.Attributes.Add("onchange", "return EnableAssinglink();");
            DrpMultipleTaskStatus.Attributes.Add("onchange", "return EnableStatuslink();");


            if (Session["admin"] == null || Session["admin"].ToString() == "")
            {
                Response.Redirect("login.aspx");
                return;
            }
            blnadmin = DatabaseHelper.isAdmin(Session["admin"].ToString());
            try
            {
                if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
                {
                    Session["adm"] = "no";
                    tdAssignedTo.Visible = false;
                }
                else
                {
                    Session["adm"] = "yes";
                    tdAssignedTo.Visible = true;
                }
            }
            catch
            {

            }
            //=================================
            if (blnadmin)
            {
                adminpart.Visible = true;
            }
            else
            {
                adminpart.Visible = false;
            }
            //=================================

            if (!blnadmin)
            {
                dvemp.Visible = true;
                dvadmin.Visible = false;
            }
            else
            {
                dvemp.Visible = false;
                dvadmin.Visible = true;
            }
            if (Request.QueryString["Ename"] != null && Request.QueryString["Ename"].ToString() != "" && blnadmin)
            {
                if (Request.QueryString["a"] != null && Request.QueryString["a"].ToString() == "1")
                {
                    assigned_user = Session["AssignUser"].ToString();

                }
                else
                {
                    assigned_user = Request.QueryString["Ename"].ToString();

                }
                this.Title = assigned_user + " priority report"; 
            }
           
            if (!IsPostBack)
            {
                hdnclass.Attributes["style"] = "display: none;";


                if (blnadmin)
                {
                    Hidelink.Visible = true;



                }
                else
                {
                    Hidelink.Visible = false;
                }



                if (Request.QueryString["Ename"] != null && Request.QueryString["Ename"].ToString() != "" && blnadmin)
                {
                    if (Request.QueryString["a"] != null && Request.QueryString["a"].ToString() == "1")
                    {
                        assigned_user = Session["AssignUser"].ToString();

                    }
                    else
                    {
                        assigned_user = Request.QueryString["Ename"].ToString();

                    }
                    this.Title = assigned_user + " priority report";
                }
                else
                {
                    assigned_user = Session["admin"].ToString();
                }
                this.Title = assigned_user + " priority report";
                loaddropdown();
                loadtask();
                //=====================================================================================
                DataSet ds = new DataSet();

                string sql = @"select * from tasks where  status <> 'closed' and status <> 'Checked' and status <>'parked' and  deleted <> 1  and assigned_to_user = '" + assigned_user + "'   ORDER BY OrderId ASC";
                //string sql = @"select * from tasks where  status <> 'closed' and status <> 'Checked' and status <>'parked' and  deleted <> 1  and assigned_to_user = '" + assigned_user + "'   ORDER BY last_updated_date desc ";
                BindData(sql);
                statusgrid.Visible = true;
                string sqlclient = "select ClientRequest.* from ClientRequest where ClientRequest.Status = 'new' and status <> 'closed' and deleted <> 1 ";
                BindDataclient(sqlclient);
                clientGrid.Visible = false;
                load_filterDropDown();
                load_dropdowns();


                //=========================================================================================


                //By sushama dt:18 jun 2015

                if (Request.QueryString["a"] != null && Request.QueryString["a"].ToString() == "1")
                {


                    //str22 = drpUsers.SelectedItem.Text;
                    BindGrid();
                }
                
            }


            else 
            { 
                
                hdnclass.Attributes["style"] = "display: inherit;"; 
            }

            //=======================================================================================================
            //=======================================================================================================

            lblleavefrom.Text = "";
            lblleaveto.Text = "";
            string EnddateQry = "", StartdateQry = "", Maxdate = "", Mindate = "";
            DataSet dsMaxdate = null, dsMindate = null;
            if (Session["admin"].ToString().ToLower().Trim() == drpAssignedTo.SelectedItem.Text.ToString().ToLower().Trim())
            {
                EnddateQry = @"select * from tbl_LeaveDetails where AbsentDate=(Select Max(AbsentDate) from tbl_LeaveDetails where AbsentDate >= CONVERT(datetime, CONVERT(varchar,DATEADD(day,0, GETDATE()), 102)) And UserName='" + Session["admin"].ToString() + "' and ApplicationNo IS NOT NULL) and UserName='" + Request.QueryString["Ename"] + "' and ApplicationNo IS NOT NULL";
            }
            else
            {
                EnddateQry = @"select * from tbl_LeaveDetails where AbsentDate=(Select Max(AbsentDate) from tbl_LeaveDetails where AbsentDate >= CONVERT(datetime, CONVERT(varchar,DATEADD(day,0, GETDATE()), 102)) And UserName='" + drpAssignedTo.SelectedItem.Text + "' and ApplicationNo IS NOT NULL) and UserName='" + drpAssignedTo.SelectedItem.Text + "' and ApplicationNo IS NOT NULL";
            }
            try
            {
                //DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "EnddateQry at Priority Report:1 " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), EnddateQry);
                dsMaxdate = DatabaseHelper.getDataset(EnddateQry);
            }
            catch (Exception ex)
            {
                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at Priority Report:1 " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Qry" + EnddateQry);
            }
            if (dsMaxdate != null && dsMaxdate.Tables.Count > 0 && dsMaxdate.Tables[0].Rows.Count > 0)
            {
                if (Session["admin"].ToString().ToLower().Trim() == drpAssignedTo.SelectedItem.Text.ToString().ToLower().Trim())
                {
                    StartdateQry = @"Select Min(AbsentDate) AS Mindate from tbl_LeaveDetails where ApplicationNo='" + Convert.ToInt32(dsMaxdate.Tables[0].Rows[0]["ApplicationNo"].ToString()) + "'And UserName='" + Session["admin"].ToString() + "' And  ApplicationNo IS NOT NULL";
                }
                else
                {
                    StartdateQry = @"Select Min(AbsentDate) AS Mindate  from tbl_LeaveDetails where ApplicationNo='" + Convert.ToInt32(dsMaxdate.Tables[0].Rows[0]["ApplicationNo"].ToString()) + "'And UserName='" + drpAssignedTo.SelectedItem.Text + "' And ApplicationNo IS NOT NULL";
                }
                try
                {
                    dsMindate = DatabaseHelper.getDataset(StartdateQry);
                    //DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "StartdateQry at Priority Report:1 " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), StartdateQry);
                }
                catch (Exception ex)
                {
                    DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at Priority Report:1 " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Qry" + StartdateQry);
                }

                if (dsMindate != null && dsMindate.Tables.Count > 0 && dsMindate.Tables[0].Rows.Count > 0)
                {
                    Maxdate = dsMaxdate.Tables[0].Rows[0]["AbsentDate"].ToString();
                    Mindate = dsMindate.Tables[0].Rows[0]["Mindate"].ToString();
                    lblleavefrom.Text =Convert.ToDateTime(dsMindate.Tables[0].Rows[0]["Mindate"]).ToString("ddd")+" "+ Convert.ToDateTime(dsMindate.Tables[0].Rows[0]["Mindate"]).ToString("dd/MM/yyyy");
                    lblleaveto.Text =  Convert.ToDateTime(dsMaxdate.Tables[0].Rows[0]["AbsentDate"]).AddDays(1).ToString("ddd")+" "+Convert.ToDateTime(dsMaxdate.Tables[0].Rows[0]["AbsentDate"]).AddDays(1).ToString("dd/MM/yyyy");
                    leaveId.Visible = true;
                    
                }
                
            }
            else
            {
                leaveId.Visible = false;

            }
            //==========================================================================================================



        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at Priority Report:1 " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Qry Not" );
        }
        //==================================
        setRowValue();
        //==================================
        //=========================================================================================
        if (DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            ShowdetailsofEmp.Visible = true;
            string sqluser = "", strDtDOB = "";
            DataSet dsuser = null;
            if (Session["admin"].ToString().ToLower().Trim() == drpAssignedTo.SelectedItem.Text.ToString().ToLower().Trim())
            {
                //sql = @"select * from tasks where  status <> 'closed' and status <> 'Checked' and status <>'parked' and  deleted <> 1  and assigned_to_user = '" + Session["admin"] + "'ORDER BY last_updated_date desc";
                sqluser = @"select * from users where username = '" + Session["admin"] + "'";
            }
            else
            {
                sqluser = @"select * from users where username = '" + drpAssignedTo.SelectedItem.Text + "'";

            }
            dsuser = DatabaseHelper.getDataset(sqluser);
            if (dsuser != null && dsuser.Tables.Count > 0 && dsuser.Tables[0].Rows.Count > 0)
            {
                //======================================================
                if (dsuser.Tables[0].Rows[0]["DOJ"].ToString() != "")
                {
                    strDtDOB = dsuser.Tables[0].Rows[0]["DOJ"].ToString();
                    DateTime dtbirth = Convert.ToDateTime(strDtDOB);
                    strDtDOB = dtbirth.ToString("dd/MM/yyyy");
                }
                lblname.Text = dsuser.Tables[0].Rows[0]["firstname"].ToString().Replace("''", "'") + " " + dsuser.Tables[0].Rows[0]["lastname"].ToString().Replace("''", "'");
                lbltitle.Text = dsuser.Tables[0].Rows[0]["Title"].ToString().Replace("''", "'");
                lblskill.Text = dsuser.Tables[0].Rows[0]["Skills"].ToString().Replace("''", "'");
                lbldob.Text = strDtDOB;

                //---------------------------------file Link Hide or Show------------------------------------------------------
                string sqluserqry = "", Fullfilename = "", originalfilename = "";
                if (Session["admin"].ToString().ToLower().Trim() == drpAssignedTo.SelectedItem.Text.ToString().ToLower().Trim())
                {

                    sqluserqry = @"select *,pkey +'_'+ Cv_name as FileName1 from users where username = '" + Session["admin"] + "'";
                }
                else
                {
                    sqluserqry = @"select *,pkey +'_'+ Cv_name as FileName1 from users where username = '" + drpAssignedTo.SelectedItem.Text + "'";

                }
                DataSet dsforfilename = DatabaseHelper.getDataset(sqluserqry);
                if (dsforfilename != null && dsforfilename.Tables.Count > 0 && dsforfilename.Tables[0].Rows.Count > 0)
                {
                    Fullfilename = dsforfilename.Tables[0].Rows[0]["FileName1"].ToString();
                    originalfilename = dsforfilename.Tables[0].Rows[0]["Cv_name"].ToString().Replace(" ", "_");
                    string path = Server.MapPath(Request.ApplicationPath + "\\Attachment\\backup");

                    path += "\\" + Fullfilename;
                    System.IO.FileInfo file = new System.IO.FileInfo(path);

                    if (file.Exists)
                    {
                        downloadcv.Visible = true;

                    }
                    else
                    {
                        downloadcv.Visible = false;
                    }
                }
                //---------------------------------file Link Hide or Show------------------------------------------------------



                //======================================================
            }
        }
        else
        {
            ShowdetailsofEmp.Visible = false;
        }


        //=============================================================================================

    }
    protected void grdPendingOrders_RowDrop(object sender, GridDragDropEventArgs e)
    {
        if (e.DestDataItem != null && e.DestDataItem.OwnerGridID == DataGrid1.ClientID)
        {
            //==============to Check no one is 99999==================================================================
            int iTaskQ = Convert.ToInt16(e.DestDataItem.GetDataKeyValue("task_id"));

            string sqlQuestion = "Select * from Tasks where task_id='" + iTaskQ + "'";
            DataSet dsforStatusQ = DatabaseHelper.getDataset(sqlQuestion);
            int DragQ = 0;
            foreach (GridDataItem draggedItem in e.DraggedItems)
            {
                DragQ = Convert.ToInt16(draggedItem.GetDataKeyValue("task_id"));

            }
            string sqlQ = "Select * from Tasks where task_id='" + DragQ + "'";
            DataSet dsDragQ = DatabaseHelper.getDataset(sqlQ);
            if (Convert.ToInt32(dsforStatusQ.Tables[0].Rows[0]["OrderId"].ToString()) != 99999 && Convert.ToInt32(dsDragQ.Tables[0].Rows[0]["OrderId"].ToString()) != 99999)
            {
                //========================================================================================================

                string selectQry = "";
                if (Session["admin"].ToString().ToLower().Trim() == drpAssignedTo.SelectedItem.Text.ToString().ToLower().Trim())
                {
                    selectQry = @"select * from tasks where  status <> 'closed' and status <> 'Checked' and status <>'parked' and  deleted <> 1  and assigned_to_user = '" + Session["admin"] + "' ORDER BY last_updated_date desc";
                }
                else
                {
                    selectQry = @"select * from tasks where  status <> 'closed' and status <> 'Checked' and status <>'parked' and  deleted <> 1  and assigned_to_user = '" + drpAssignedTo.SelectedItem.Text + "'ORDER BY last_updated_date desc";
                }
                DataSet dsforStatus = DatabaseHelper.getDataset(selectQry);



                int iTaskID = Convert.ToInt16(e.DestDataItem.GetDataKeyValue("task_id"));//-----Drop place Task_Id
                int DragPrimaryId = 0;
                foreach (GridDataItem draggedItem in e.DraggedItems)
                {
                    DragPrimaryId = Convert.ToInt16(draggedItem.GetDataKeyValue("task_id"));//----Drag Task_Id
                }

                string DropOrderIdQ = "Select * from tasks where Task_Id='" + iTaskID + "'";
                DataSet dsDrop = DatabaseHelper.getDataset(DropOrderIdQ);
                int iDropOId = Convert.ToInt32(dsDrop.Tables[0].Rows[0]["OrderId"].ToString());
                string DragOrderIdQ = "Select * from tasks where Task_Id='" + DragPrimaryId + "'";
                DataSet dsDrag = DatabaseHelper.getDataset(DragOrderIdQ);
                int iDragOId = Convert.ToInt32(dsDrag.Tables[0].Rows[0]["OrderId"].ToString());


                string updateQ = "", BetweenQ = "";
                string updateQH = "", BetweenHQ = "";

                if (iDropOId < iDragOId)
                {
                    updateQ = "Update tasks set OrderId='" + iDropOId + "' where Task_Id='" + DragPrimaryId + "'";

                    if (Session["admin"].ToString().ToLower().Trim() == drpAssignedTo.SelectedItem.Text.ToString().ToLower().Trim())
                    {
                        BetweenQ = " Select * from tasks where status <> 'closed' and status <> 'Checked' and status <>'parked'";
                        BetweenQ += " and  deleted <> 1  and assigned_to_user = '" + Session["admin"] + "' ";
                        BetweenQ += " and OrderId >= " + iDropOId + " and OrderId < " + iDragOId + " ORDER BY OrderId ASC;";
                    }
                    else
                    {
                        BetweenQ = " Select * from tasks where status <> 'closed' and status <> 'Checked' and status <>'parked'";
                        BetweenQ += " and  deleted <> 1  and assigned_to_user = '" + drpAssignedTo.SelectedItem.Text + "' ";
                        BetweenQ += " and OrderId >= " + iDropOId + " and OrderId < " + iDragOId + " ORDER BY OrderId ASC;";
                    }


                    DataSet dsDropBetween = DatabaseHelper.getDataset(BetweenQ);
                    int noexeQ = DatabaseHelper.executeNonQuery(updateQ);


                    string IncreseQ = "";
                    for (int icount = 0; icount < Convert.ToInt32(dsDropBetween.Tables[0].Rows.Count.ToString()); icount++)
                    {
                        int vOrderId = Convert.ToInt32(dsDropBetween.Tables[0].Rows[icount]["OrderId"].ToString()) + 1;
                        IncreseQ = "Update tasks set OrderId=" + vOrderId + " where Task_id=" + Convert.ToInt32(dsDropBetween.Tables[0].Rows[icount]["Task_id"].ToString()) + ";";
                        int chki = DatabaseHelper.executeNonQuery(IncreseQ);
                    }
                }
                if (iDropOId > iDragOId)
                {
                    updateQH = "Update tasks set OrderId='" + iDropOId + "' where Task_Id='" + DragPrimaryId + "'";

                    if (Session["admin"].ToString().ToLower().Trim() == drpAssignedTo.SelectedItem.Text.ToString().ToLower().Trim())
                    {
                        BetweenHQ = " Select * from tasks where status <> 'closed' and status <> 'Checked' and status <>'parked'";
                        BetweenHQ += " and  deleted <> 1  and assigned_to_user = '" + Session["admin"] + "' ";
                        BetweenHQ += " and OrderId <= " + iDropOId + " and OrderId > " + iDragOId + " ORDER BY OrderId ASC;";
                    }
                    else
                    {
                        BetweenHQ = " Select * from tasks where status <> 'closed' and status <> 'Checked' and status <>'parked'";
                        BetweenHQ += " and  deleted <> 1  and assigned_to_user = '" + drpAssignedTo.SelectedItem.Text + "' ";
                        BetweenHQ += " and OrderId <= " + iDropOId + " and OrderId > " + iDragOId + " ORDER BY OrderId ASC;";
                    }


                    DataSet dsDropBetweenH = DatabaseHelper.getDataset(BetweenHQ);
                    int noexeQH = DatabaseHelper.executeNonQuery(updateQH);
                    string IncreseQH = "";
                    for (int icountH = 0; icountH < Convert.ToInt32(dsDropBetweenH.Tables[0].Rows.Count.ToString()); icountH++)
                    {
                        int vOrderIdH = Convert.ToInt32(dsDropBetweenH.Tables[0].Rows[icountH]["OrderId"].ToString()) - 1;
                        IncreseQH = "Update tasks set OrderId=" + vOrderIdH + " where Task_id=" + Convert.ToInt32(dsDropBetweenH.Tables[0].Rows[icountH]["Task_id"].ToString()) + ";";
                        int chkiH = DatabaseHelper.executeNonQuery(IncreseQH);
                    }
                }



                string sqlselectQry = "";
                if (Session["admin"].ToString().ToLower().Trim() == drpAssignedTo.SelectedItem.Text.ToString().ToLower().Trim())
                {
                    sqlselectQry = @"select * from tasks where  status <> 'closed' and status <> 'Checked' and status <>'parked' and  deleted <> 1  and assigned_to_user = '" + Session["admin"] + "' ORDER BY OrderId ASC";
                }
                else
                {
                    sqlselectQry = @"select * from tasks where  status <> 'closed' and status <> 'Checked' and status <>'parked' and  deleted <> 1  and assigned_to_user = '" + drpAssignedTo.SelectedItem.Text + "' ORDER BY OrderId ASC";
                }

                DataSet BinddsforOrder;
                BinddsforOrder = DatabaseHelper.getDataset(sqlselectQry);
                BinddsforOrder = formatTaskDataSet(BinddsforOrder);
                DataGrid1.DataSource = BinddsforOrder;
                DataGrid1.Rebind();


            }
            if (((Convert.ToInt32(dsforStatusQ.Tables[0].Rows[0]["OrderId"].ToString()) == 99999 && Convert.ToInt32(dsDragQ.Tables[0].Rows[0]["OrderId"].ToString()) != 99999)) || ((Convert.ToInt32(dsforStatusQ.Tables[0].Rows[0]["OrderId"].ToString()) != 99999 && Convert.ToInt32(dsDragQ.Tables[0].Rows[0]["OrderId"].ToString()) == 99999)))
            {
                //int iTaskID = Convert.ToInt16(e.DestDataItem.GetDataKeyValue("task_id"));//-----Drop place Task_Id
                //int DragPrimaryId = 0;
                //foreach (GridDataItem draggedItem in e.DraggedItems)
                //{
                //    DragPrimaryId = Convert.ToInt16(draggedItem.GetDataKeyValue("task_id"));//----Drag Task_Id
                //}
                int task_idA = Convert.ToInt32(dsforStatusQ.Tables[0].Rows[0]["task_id"].ToString());
                int orderIdA = Convert.ToInt32(dsforStatusQ.Tables[0].Rows[0]["OrderId"].ToString());

                int task_idB = Convert.ToInt32(dsDragQ.Tables[0].Rows[0]["task_id"].ToString());
                int orderIdB = Convert.ToInt32(dsDragQ.Tables[0].Rows[0]["OrderId"].ToString());

                string maxOrderQry = "";
                if (Session["admin"].ToString().ToLower().Trim() == drpAssignedTo.SelectedItem.Text.ToString().ToLower().Trim())
                {
                    maxOrderQry = " Select Max(OrderId) from tasks where status <> 'closed' and status <> 'Checked' and status <>'parked'";
                    maxOrderQry += " and  deleted <> 1 and OrderId!=99999 and assigned_to_user = '" + Session["admin"] + "' ";
                }
                else
                {
                    maxOrderQry = " Select Max(OrderId) from tasks where status <> 'closed' and status <> 'Checked' and status <>'parked'";
                    maxOrderQry += " and  deleted <> 1 and  OrderId!=99999 and assigned_to_user = '" + drpAssignedTo.SelectedItem.Text + "' ";
                }

                int maxOrderId = ((int)DatabaseHelper.executeScalar(maxOrderQry)) + 1;


                string updateQryA = "", updateQryAA = "", updateQryB = "", updateQryBB = "";
                if (orderIdA == 99999)
                {
                    updateQryB = "update tasks  set OrderId='" + maxOrderId + "' where task_id='" + task_idB + "'";
                    updateQryBB = "update tasks  set OrderId='" + orderIdB + "' where task_id='" + task_idA + "'";
                    int iB = DatabaseHelper.executeNonQuery(updateQryB);
                    int iBB = DatabaseHelper.executeNonQuery(updateQryBB);

                }
                if (orderIdB == 99999)
                {
                    updateQryA = "update tasks  set OrderId='" + maxOrderId + "' where task_id='" + task_idA + "'";
                    updateQryAA = "update tasks  set OrderId='" + orderIdA + "' where task_id='" + task_idB + "'";
                    int iA = DatabaseHelper.executeNonQuery(updateQryA);
                    int iAA = DatabaseHelper.executeNonQuery(updateQryAA);
                }


                string sqltQry = "";
                if (Session["admin"].ToString().ToLower().Trim() == drpAssignedTo.SelectedItem.Text.ToString().ToLower().Trim())
                {
                    sqltQry = @"select * from tasks where  status <> 'closed' and status <> 'Checked' and status <>'parked' and  deleted <> 1  and assigned_to_user = '" + Session["admin"] + "' ORDER BY OrderId ASC";
                }
                else
                {
                    sqltQry = @"select * from tasks where  status <> 'closed' and status <> 'Checked' and status <>'parked' and  deleted <> 1  and assigned_to_user = '" + drpAssignedTo.SelectedItem.Text + "' ORDER BY OrderId ASC";
                }
                DataSet BinddsQ;
                BinddsQ = DatabaseHelper.getDataset(sqltQry);
                BinddsQ = formatTaskDataSet(BinddsQ);
                DataGrid1.DataSource = BinddsQ;
                DataGrid1.Rebind();

            }
        }

    }
    protected void TextBox2_textchanged(object sender, EventArgs e)
    {
        if (DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            TextBox textbox = (TextBox)sender;
            GridDataItem editItem = (GridDataItem)textbox.NamingContainer;
            TextBox txtbox1 = editItem.FindControl("txtorder") as TextBox;
            string str1 = txtbox1.Text;
            if (txtbox1.Text.ToString() != "")
            {
                if (IsNumeric(txtbox1.Text.ToString()))
                {
                    Label lbltaskId = editItem.FindControl("lbltaskid") as Label;
                    Label assignuser = editItem.FindControl("lblassignedtouser") as Label;
                    string str2 = lbltaskId.Text;
                    string strupdateQry;
                    if (txtbox1.Text != "")
                    {
                        strupdateQry = "Update tasks set OrderId='" + Convert.ToInt32(txtbox1.Text.ToString()) + "' where task_Id='" + Convert.ToInt32(lbltaskId.Text) + "'";
                    }
                    else
                    {
                        strupdateQry = "Update tasks set OrderId=CAST(NULL As nvarchar(100)) where task_Id='" + Convert.ToInt32(lbltaskId.Text) + "'";
                    }
                    
                    DatabaseHelper.executeNonQuery(strupdateQry);
                    if (assigned_user != "" && assigned_user != null)
                    {
                        this.Title = assigned_user + " priority report";
                    }
                    else
                    {
                        assigned_user = assignuser.Text.ToString();
                        this.Title = assigned_user + " priority report";
                    }
                }
            }
        }
        
    }
    public static bool IsNumeric(string NumericText)
    {
        bool isnumber = true;
        foreach (char c in NumericText)
        {
            isnumber = char.IsNumber(c);
            if (!isnumber)
            {
                return isnumber;
            }
        }
        return isnumber;
    }

    private void loadtask()
    {
        try
        {
            assigned_user = drpAssignedTo.SelectedItem.Text;

            sql.Append(@"select top 20 tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '1a - DO NOW' ");

            if (!blnadmin)
            {
                sql.Append(" and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + assigned_user + "'; ");
            }
            else
            {
                sql.Append(" and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + assigned_user + "'; ");
            }

            sql.Append(@"select tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '0 - IMMEDIATE' ");

            if (!blnadmin)
            {
                sql.Append(" and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + assigned_user + "'; ");
            }
            else
            {
                sql.Append(" and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + assigned_user + "'; ");
            }
            sql.Append(@"select tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '1b - high' ");

            if (!blnadmin)
            {
                sql.Append(" and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + assigned_user + "'; ");
            }
            else
            {
                sql.Append(" and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + assigned_user + "'; ");
            }
            sql.Append(@"select tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '1c - normal' ");
            if (!blnadmin)
            {
                sql.Append(" and  status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + assigned_user + "'; ");
            }
            else
            {
                sql.Append(" and  status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + assigned_user + "'; ");
            }
            sql.Append(@"select tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '2 - not urgent' ");
            if (!blnadmin)
            {
                sql.Append(" and  status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + assigned_user + "'; ");
            }
            else
            {
                sql.Append(" and  status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + assigned_user + "'; ");
            }

            sql.Append(@"select tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '3 - low priority' ");
            if (!blnadmin)
            {
                sql.Append(" and  status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + assigned_user + "'; ");
            }
            else
            {
                sql.Append(" and  status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + assigned_user + "'; ");
            }

            sql.Append(@"select tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '4 - very low priority' ");
            if (!blnadmin)
            {
                sql.Append(" and  status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + assigned_user + "'; ");
            }
            else
            {
                sql.Append(" and  status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + assigned_user + "'; ");
            }

            sql.Append(@"select tasks.* from tasks  where tasks.deleted <> 1 and tasks.priority = '5 - parked' ");
            if (!blnadmin)
            {
                sql.Append(" and  status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + assigned_user + "'; ");
            }
            else
            {
                sql.Append(" and  status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + assigned_user + "'; ");
            }
            //==========Questions======8====================
            sql.Append(@"select distinct tasks.* from task_comments,tasks where tasks.task_id = task_comments.task_id and  tasks.deleted <> 1 and task_comments.deleted <> 1 and qflag=1  ");
            if (!DatabaseHelper.can_Show_Closed_Tasks(assigned_user))
            {
                sql.Append(" and status <> 'closed' ");
            }
            sql.Append(" and task_comments.QuesTo = '" + assigned_user + "';  ");
            //==========Unread comments======9====================
            if (!DatabaseHelper.isAdmin(assigned_user))
            {
                sql.Append(@"select tasks.* from tasks where task_id in(select task_comments.task_id from task_comments where tc_id  not in (select  tc_id from read_comments where read_comments.username='" + assigned_user + "') and task_comments.deleted <> 1 ) ");
                sql.Append(" and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + assigned_user + "'  ");
            }
            else
            {
                sql.Append(@"select tasks.* from tasks where task_id in(select task_comments.task_id from task_comments where tc_id  not in (select  tc_id from read_comments where read_comments.username='" + assigned_user + "') and task_comments.deleted <> 1 ) ");
                sql.Append(" and tasks.deleted <> 1 and tasks.assigned_to_user = '" + assigned_user + "' ");
            }
            //==========Overdue===========10====================
            sql.Append(@"select * from tasks where  status <> 'closed' and deleted <> 1");
            sql.Append(" and status <> 'checked' and status <> 'parked' and assigned_to_user = '" + assigned_user + "' and  DueDate <> ''");
            sql.Append(" ORDER BY last_updated_date desc ");
            //==========All overdue=======11====================
            sql.Append(@"select * from tasks where deleted <> 1 and status <> 'checked' and status <> 'closed'and status <> 'parked' and DueDate <> '' ");
            //===========CR:New============12====================
            sql.Append(@"select ClientRequest.* from ClientRequest where ClientRequest.Status = 'new' and status <> 'closed' and deleted <> 1  ");
            //==========Unread comments=======13=================
            sql.Append(@"select ClientRequest.* from ClientRequest where RequestId in(select ClientRequest_Details.RequestId from ClientRequest_Details where [CommentId] not in (select  CommentId from read_CR_comments where read_CR_comments.username='" + assigned_user + "') and ClientRequest_Details.deleted <> 1)");
            //========Tasks with new comment===14======================
            sql.Append(@"select tasks.* from tasks where task_id in(select task_comments.task_id from task_comments where tc_id  not in (select  tc_id from read_comments where read_comments.username='" + assigned_user + "') and task_comments.deleted <> 1 )");
            //========To be Answered===15======================
            sql.Append(@"select distinct tasks.* from task_comments,tasks where tasks.task_id = task_comments.task_id and  tasks.deleted <> 1 and task_comments.deleted <> 1 and qflag=1 and status <> 'closed' and task_comments.username = '" + assigned_user + "' ");
            DataSet ds = DatabaseHelper.getDataset(sql.ToString());
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                lnkHighestTasks.Text = "(" + ds.Tables[0].Rows.Count + ")";
                lnkHighestTasks.Style.Add(HtmlTextWriterStyle.Color, "#FF1493");
                lbl_1adonow.Text = "Do now";
                lbl_1adonow.Visible = true;
                lbl_1adonow.Style.Add(HtmlTextWriterStyle.Color, "#FF1493");
            }
            else
            { }
            if (ds != null && ds.Tables[1].Rows.Count > 0)
            {
                lnkPr0Task.Text = "(" + ds.Tables[1].Rows.Count + ")";
                lnkPr0Task.Style.Add(HtmlTextWriterStyle.Color, "RED");
                lbl_Immediate.Text = "Immediate";
                lbl_Immediate.Visible = true;
                lbl_Immediate.Style.Add(HtmlTextWriterStyle.Color, "RED");
            }
            else
            { }
            if (ds != null && ds.Tables[2].Rows.Count > 0)
            {
                lnkHighTasks.Text = "(" + ds.Tables[2].Rows.Count + ")";
                lnkHighTasks.Style.Add(HtmlTextWriterStyle.Color, "BLACK");
                lbl_1bhigh.Text = "High";
                lbl_1bhigh.Visible = true;
                lbl_1bhigh.Style.Add(HtmlTextWriterStyle.Color, "BLACK");
            }
            else
            { }
            if (ds != null && ds.Tables[3].Rows.Count > 0)
            {
                Lnk1cTasks.Text = "(" + ds.Tables[3].Rows.Count + ")";
                Lnk1cTasks.Style.Add(HtmlTextWriterStyle.Color, "BLACK");
                lbl_1cnormal.Text = "Normal";
                lbl_1cnormal.Visible = true;
                lbl_1cnormal.Style.Add(HtmlTextWriterStyle.Color, "BLACK");
            }
            else
            { }
            if (ds != null && ds.Tables[4].Rows.Count > 0)
            {
                Link2notTasks.Text = "(" + ds.Tables[4].Rows.Count + ")";
                Link2notTasks.Style.Add(HtmlTextWriterStyle.Color, "BLACK");
                lbl_2noturgent.Text = "Low";
                lbl_2noturgent.Visible = true;
                lbl_2noturgent.Style.Add(HtmlTextWriterStyle.Color, "BLACK");
            }
            else
            { }
            if (ds != null && ds.Tables[5].Rows.Count > 0)
            {
                Link3lowTasks.Style.Add(HtmlTextWriterStyle.Color, "GREEN");
                lbl_3low.Visible = true;
                lbl_3low.Style.Add(HtmlTextWriterStyle.Color, "GREEN");
            }
            else
            { }
            if (ds != null && ds.Tables[6].Rows.Count > 0)
            {
                Link4lowTasks.Style.Add(HtmlTextWriterStyle.Color, "#FF1493");
                lbl4verylow.Visible = true;
                lbl4verylow.Style.Add(HtmlTextWriterStyle.Color, "#FF1493");
            }
            else
            { }
            if (ds != null && ds.Tables[7].Rows.Count > 0)
            {
                Link5parkedTasks.Style.Add(HtmlTextWriterStyle.Color, "RED");
                lbl5parked.Visible = true;
                lbl5parked.Style.Add(HtmlTextWriterStyle.Color, "RED");
            }
            else
            { }
            //===============================Question==========================================================
            if (ds != null && ds.Tables[8].Rows.Count > 0)
            {
                lnkUnansweredQuestions.Text = "(" + ds.Tables[8].Rows.Count + ")";
                lnkUnansweredQuestions.Style.Add(HtmlTextWriterStyle.Color, "BLACK");
                lblQuestion.Text = "To answer";
                lblQuestion.Visible = true;
                lblQuestion.Style.Add(HtmlTextWriterStyle.Color, "BLACK");

            }
            else
            { }
            //===============================Unread comments==================================================
            if (ds != null && ds.Tables[9].Rows.Count > 0)
            {
                LnkTskNewcomment.Text = "(" + ds.Tables[9].Rows.Count + ")";
                LnkTskNewcomment.Style.Add(HtmlTextWriterStyle.Color, "#FF1493");
                lblUnreadComment.Text = "Unread-Comment";
                lblUnreadComment.Visible = true;
                lblUnreadComment.Style.Add(HtmlTextWriterStyle.Color, "#FF1493");
            }
            else
            { }
            //=================================Overdue=========================================================
            if (ds != null && ds.Tables[10].Rows.Count > 0)
            {
                BtnOverdue.Text = "(" + ds.Tables[10].Rows.Count + ")";
                BtnOverdue.Style.Add(HtmlTextWriterStyle.Color, "RED");
                lblOverdue.Text = "Overdue";
                lblOverdue.Visible = true;
                lblOverdue.Style.Add(HtmlTextWriterStyle.Color, "RED");
            }
            else
            { }
            //=================================All overdue======================================================
            if (ds != null && ds.Tables[11].Rows.Count > 0)
            {
                BtnallOverdue.Text = "(" + ds.Tables[11].Rows.Count + ")";
                BtnallOverdue.Style.Add(HtmlTextWriterStyle.Color, "RED");
                lblallOverDue.Text = "Overdue";
                lblallOverDue.Visible = true;
                lblallOverDue.Style.Add(HtmlTextWriterStyle.Color, "RED");
            }
            else
            { }
            if (ds != null && ds.Tables[12].Rows.Count > 0)
            {
                LnkNewCR.Text = "(" + ds.Tables[12].Rows.Count + ")";
                LnkNewCR.Style.Add(HtmlTextWriterStyle.Color, "GREEN");
                lblNewCR.Text = "New";
                lblNewCR.Visible = true;
                lblNewCR.Style.Add(HtmlTextWriterStyle.Color, "GREEN");
            }
            else
            {
            }
            //=========================Unread comments===========================================================
            if (ds != null && ds.Tables[13].Rows.Count > 0)
            {
                LnkCrNewComment.Text = "(" + ds.Tables[13].Rows.Count + ")";
                LnkCrNewComment.Style.Add(HtmlTextWriterStyle.Color, "#FF1493");
                lblNotreadcomment.Text = "Unread Comment";
                lblNotreadcomment.Visible = true;
                lblNotreadcomment.Style.Add(HtmlTextWriterStyle.Color, "#FF1493");
            }
            else
            { }
            //=======================Tasks with new comment======================================================
            if (ds != null && ds.Tables[14].Rows.Count > 0)
            {
                LnkMakeAllTaskCmmntRead.Text = "(" + ds.Tables[14].Rows.Count + ")";
                LnkMakeAllTaskCmmntRead.Style.Add(HtmlTextWriterStyle.Color, "#FF1493");
                lbltaskNewComment.Text = "Unread Comments";
                lbltaskNewComment.Visible = true;
                lbltaskNewComment.Style.Add(HtmlTextWriterStyle.Color, "#FF1493");
            }
            else
            { }
            //=======================To be answered======================================================
            if (ds != null && ds.Tables[15].Rows.Count > 0)
            {
                lnktobeanswered.Text = "(" + ds.Tables[15].Rows.Count + ")";
                lnktobeanswered.Style.Add(HtmlTextWriterStyle.Color, "BLACK");
                lbltobeanswered.Text = "To be answered";
                lbltobeanswered.Visible = true;
                lbltobeanswered.Style.Add(HtmlTextWriterStyle.Color, "#BLACK");
            }
            else
            { }
        }
        catch (Exception ex)
        {
        }
    }

    private void loaddropdown()
    {
        try
        {
            string sql1 = "select username from users where active = 'Y' order by username";
            DataSet ds_dropdowns = DatabaseHelper.getDataset(sql1);
            drpAssignedTo.DataSource = ds_dropdowns.Tables[0];
            drpAssignedTo.DataTextField = "username";
            drpAssignedTo.DataValueField = "username";
            drpAssignedTo.DataBind();
            SetDDLs(drpAssignedTo, assigned_user);
        }
        catch (Exception ex)
        {
        }
    }
    private void SetDDLs(DropDownList d, string val)
    {
        ListItem li;
        for (int i = 0; i < d.Items.Count; i++)
        {
            li = d.Items[i];
            if (li.Value.ToUpper().Trim() == val.ToUpper().Trim())
            {
                d.SelectedIndex = i;
                break;
            }
        }
    }

    protected void drpAssignedTo_SelectedIndexChanged(object sender, EventArgs e)
    {
        //==============show selected user======================================
        try
        {
            lnkPr0Task.Text = "";
            lnkHighestTasks.Text = "";
            lnkHighTasks.Text = "";
            Lnk1cTasks.Text = "";
            Link2notTasks.Text = "";
            Link3lowTasks.Text = "";
            Link4lowTasks.Text = "";
            Link5parkedTasks.Text = "";
            lnkUnansweredQuestions.Text = "";
            LnkTskNewcomment.Text = "";
            BtnOverdue.Text = "";
            BtnallOverdue.Text = "";
            LnkNewCR.Text = "";
            LnkCrNewComment.Text = "";
            LnkMakeAllTaskCmmntRead.Text = "";

            if (Request.QueryString["a"] != null && Request.QueryString["a"].ToString() == "1")
            {
                assigned_user = Session["AssignUser"].ToString();
            }
            else
            {
                assigned_user = drpAssignedTo.SelectedItem.Text;
            }

            DataGrid1.DataSource = null;
            DataGrid1.DataBind();
            //string sqlforall = @"select * from tasks where  status <> 'closed' and status <> 'Checked' and status <>'parked' and  deleted <> 1  and assigned_to_user = '" + assigned_user + "'   ORDER BY last_updated_date desc ";
            string sqlforall = @"select * from tasks where  status <> 'closed' and status <> 'Checked' and status <>'parked' and  deleted <> 1  and assigned_to_user = '" + assigned_user + "' ORDER BY OrderId ASC";
            BindData(sqlforall);
            loadtask();
            statusgrid.Visible = true;
            clientGrid.Visible = false;
            //sushama 13 july 2015           
            EmpImg.Src = DatabaseHelper.fnSetImgURL(assigned_user);
        }
        catch (Exception ex)
        {
        }
    }

    //By sushama dt:18 jun 2015

    void BindGrid()
    {
        lnkPr0Task.Text = "";
        lnkHighestTasks.Text = "";
        lnkHighTasks.Text = "";
        Lnk1cTasks.Text = "";
        Link2notTasks.Text = "";
        Link3lowTasks.Text = "";
        Link4lowTasks.Text = "";
        Link5parkedTasks.Text = "";
        lnkUnansweredQuestions.Text = "";
        LnkTskNewcomment.Text = "";
        BtnOverdue.Text = "";
        BtnallOverdue.Text = "";
        LnkNewCR.Text = "";
        LnkCrNewComment.Text = "";
        LnkMakeAllTaskCmmntRead.Text = "";              
        
       // drpAssignedTo.SelectedItem.Text = "Manisha";

        drpAssignedTo.SelectedValue = Session["AssignUser"].ToString(); 
        string user = drpAssignedTo.SelectedItem.Text;

        DataGrid1.DataSource = null;
        DataGrid1.DataBind();
        //string sqlforall = @"select * from tasks where  status <> 'closed' and status <> 'Checked' and status <>'parked' and  deleted <> 1  and assigned_to_user = '" + assigned_user + "'   ORDER BY last_updated_date desc ";
        string sqlforall = @"select * from tasks where  status <> 'closed' and status <> 'Checked' and status <>'parked' and  deleted <> 1  and assigned_to_user = '" + user + "' ORDER BY OrderId ASC";
        BindData(sqlforall);
        loadtask();
        statusgrid.Visible = true;
        clientGrid.Visible = false;
        EmpImg.Src = DatabaseHelper.fnSetImgURL(assigned_user);
    }
    //End By sushama dt:18 jun 2015


    protected void lnkPr0Task_Click(object sender, EventArgs e)
    {
        statusgrid.Visible = true;
        clientGrid.Visible = false;
        //==============Show 0-Immediate level task=============
        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            assigned_user = Session["admin"].ToString();
        }
        else
        {
            assigned_user = drpAssignedTo.SelectedItem.Text;
        }
        DataSet ds = new DataSet();
        string sql = @"select * from tasks where ";
        sql += "status <> 'closed' and status <> 'Checked' and status <>'parked' and   deleted <> 1  and ";
        sql += "assigned_to_user ='" + assigned_user.ToString() + "'  and priority = '0 - IMMEDIATE'   ORDER BY last_updated_date desc";
        BindData(sql);
    }
    protected void lnkHighestTasks_Click(object sender, EventArgs e)
    {
        statusgrid.Visible = true;
        clientGrid.Visible = false;
        //=========show 1a-do now level task========================
        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            assigned_user = Session["admin"].ToString();
        }
        else
        {
            assigned_user = drpAssignedTo.SelectedItem.Text;
        }
        DataSet ds = new DataSet();
        string sql = @"select * from tasks where";
        sql += " status <> 'closed' and status <> 'Checked' and status <>'parked' and   deleted <> 1  and";
        sql += " assigned_to_user = '" + assigned_user.ToString() + "'  and priority = '1a - DO NOW'   ORDER BY last_updated_date desc";
        BindData(sql);
    }
    protected void lnkHighTasks_Click(object sender, EventArgs e)
    {
        statusgrid.Visible = true;
        clientGrid.Visible = false;
        //=========show 1b-high level task========================
        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            assigned_user = Session["admin"].ToString();
        }
        else
        {
            assigned_user = drpAssignedTo.SelectedItem.Text;
        }
        DataSet ds = new DataSet();
        string sql = @"select * from tasks where";
        sql += " status <> 'closed' and status <> 'Checked' and status <>'parked' and   deleted <> 1  and";
        sql += " assigned_to_user = '" + assigned_user.ToString() + "'  and priority = '1b - high'   ORDER BY last_updated_date desc";
        BindData(sql);
    }
    protected void Lnk1cTasks_Click(object sender, EventArgs e)
    {
        statusgrid.Visible = true;
        clientGrid.Visible = false;
        //==========Show 1-c level task==========================
        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            assigned_user = Session["admin"].ToString();
        }
        else
        {
            assigned_user = drpAssignedTo.SelectedItem.Text;
        }
        DataSet ds = new DataSet();
        string sql = @"select * from tasks where";
        sql += " status <> 'closed' and status <> 'Checked' and status <>'parked' and   deleted <> 1  and";
        sql += " assigned_to_user = '" + assigned_user.ToString() + "'  and priority = '1c - normal'   ORDER BY last_updated_date desc";
        BindData(sql);
    }
    protected void Link2notTasks_Click(object sender, EventArgs e)
    {
        statusgrid.Visible = true;
        clientGrid.Visible = false;
        //===============Show 2-Not level task=================== 
        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            assigned_user = Session["admin"].ToString();
        }
        else
        {
            assigned_user = drpAssignedTo.SelectedItem.Text;
        }
        DataSet ds = new DataSet();
        string sql = @"select * from tasks where";
        sql += " status <> 'closed' and status <> 'Checked' and status <>'parked' and   deleted <> 1  and";
        sql += " assigned_to_user = '" + assigned_user.ToString() + "'  and priority = '2 - not urgent'   ORDER BY last_updated_date desc";
        BindData(sql);
    }
    protected void Link5parkedTasks_Click(object sender, EventArgs e)
    {
        statusgrid.Visible = true;
        clientGrid.Visible = false;
        //=========Show 5-Parked level task=====================
        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            assigned_user = Session["admin"].ToString();
        }
        else
        {
            assigned_user = drpAssignedTo.SelectedItem.Text;
        }
        DataSet ds = new DataSet();
        string sql = @"select * from tasks where";
        sql += " status <> 'closed' and status <> 'Checked' and status <>'parked' and   deleted <> 1  and";
        sql += " assigned_to_user = '" + assigned_user.ToString() + "'  and priority = '5 - parked'   ORDER BY last_updated_date desc";
        BindData(sql);
    }
    private void BindData(string strsql)
    {
        DataSet ds = DatabaseHelper.getDataset(strsql);

        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            //divMessage.InnerHtml = "";
            ds = formatTaskDataSet(ds);
            LblRecordsno.Text = ds.Tables[0].Rows.Count.ToString();
            PagedDataSource objds = new PagedDataSource();
            objds.DataSource = ds.Tables[0].DefaultView;

            DataGrid1.DataSource = null;
            DataGrid1.DataBind();

            DataGrid1.DataSource = objds;
            DataGrid1.DataBind();
            EmpImg.Src = DatabaseHelper.fnSetImgURL(assigned_user);
            this.Title = assigned_user + " priority report";
        }
        else
        {
            //divMessage.InnerHtml = "No task yet.";
            //DataGrid1.DataSource = null;
            //DataGrid1.DataBind();
            //drpPerPage1.Visible = false;
            //LblRecordsno.Text = "0";
            //hidecheck = false;
        }
    }

    private DataSet formatTaskDataSet(DataSet ds)
    {
        try
        {
            ds.Tables[0].Columns.Add("ShowAllComment");
            /// ds.Tables[0].Columns.Add("ItemPriority", typeof(string));

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {
                    if (ds.Tables[0].Rows[i]["short_desc"] != null && ds.Tables[0].Rows[i]["short_desc"].ToString().Trim() != "")
                    {
                        ds.Tables[0].Rows[i]["short_desc"] = "<a  target=\"_blank\"  href=\"edit_task.aspx?id=" + ds.Tables[0].Rows[i]["task_id"].ToString() + "  \">" + ds.Tables[0].Rows[i]["short_desc"].ToString() + "</a>";
                    }
                    else
                    {
                        ds.Tables[0].Rows[i]["short_desc"] = "";

                    }
                    try
                    {
                        string LastComment = "";
                        newsql = @"select top 1.* from task_comments where task_comments.deleted <> 1  and ";
                        newsql += " task_id =" + ds.Tables[0].Rows[i]["task_id"].ToString() + " order by tc_id desc";
                        //testing by rohit
                        DataSet dslastcomment = DatabaseHelper.getDataset(newsql);

                        if (dslastcomment != null && dslastcomment.Tables[0].Rows.Count > 0)
                        {

                            // for (int j = 0; j < dslastcomment.Tables[0].Rows.Count; j++)
                            for (int j = 0; j < 1; j++)
                            {

                                if (dslastcomment.Tables[0].Rows[j]["qflag"].ToString() == "1")
                                {

                                    LastComment += "<span style='color:Red'>" + "Question posted by " + dslastcomment.Tables[0].Rows[j]["username"].ToString() + " for  " + dslastcomment.Tables[0].Rows[j]["QuesTo"].ToString() + " on " + DateTime.Parse(dslastcomment.Tables[0].Rows[j]["post_date"].ToString()).ToString("dd MMM yyyy h:mm tt") + "</span>";

                                    LastComment += "<br/><span>" + dslastcomment.Tables[0].Rows[j]["comment"].ToString() + "</span>";

                                    LastComment += "<span style=\"border-bottom: solid 1px green; margin-bottom: 2px; height:1px\"></span><br/><hr />";

                                }
                                else
                                {

                                    LastComment += "<span style='color:Green'>" + "Comment " + dslastcomment.Tables[0].Rows[j]["tc_id"].ToString() + " posted by " + dslastcomment.Tables[0].Rows[j]["username"].ToString() + " on " + DateTime.Parse(dslastcomment.Tables[0].Rows[j]["post_date"].ToString()).ToString("dd MMM yyyy h:mm tt") + "</span>";
                                    LastComment += "<br/><span>" + dslastcomment.Tables[0].Rows[j]["comment"].ToString() + "</span>";

                                    LastComment += "<span style=\"border-bottom: solid 1px green; margin-bottom: 2px; height:1px\"></span><br/><hr />";
                                }
                            }
                        }

                        ds.Tables[0].Rows[i]["ShowAllComment"] = LastComment.ToString();
                    }
                    catch
                    {

                    }

                }
                catch { }
            }
        }
        catch
        {
            Server.ClearError();
        }
        return ds;
    }

    protected void Link3lowTasks_Click1(object sender, EventArgs e)
    {
        statusgrid.Visible = true;
        clientGrid.Visible = false;
        //============Show 3-low level task=====================
        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            assigned_user = Session["admin"].ToString();
        }
        else
        {
            assigned_user = drpAssignedTo.SelectedItem.Text;
        }
        DataSet ds = new DataSet();
        string sql = @"select * from tasks where";
        sql += " status <> 'closed' and status <> 'Checked' and status <>'parked' and   deleted <> 1  and";
        sql += " assigned_to_user = '" + assigned_user.ToString() + "'  and priority = '3 - low priority'   ORDER BY last_updated_date desc";
        BindData(sql);
    }

    protected void Link4lowTasks_Click(object sender, EventArgs e)
    {
        statusgrid.Visible = true;
        clientGrid.Visible = false;
        //==============Show 4-low level task ==================
        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            assigned_user = Session["admin"].ToString();
        }
        else
        {
            assigned_user = drpAssignedTo.SelectedItem.Text;
        }
        DataSet ds = new DataSet();
        string sql = @"select * from tasks where";
        sql += " status <> 'closed' and status <> 'Checked' and status <>'parked' and   deleted <> 1  and";
        sql += " assigned_to_user = '" + assigned_user.ToString() + "'  and priority = '4 - very low priority'   ORDER BY last_updated_date desc";
        BindData(sql);
    }
    protected void ddlpriority_SelectedIndexChanged(object sender, EventArgs e)
    {
        //DropDownList ddl = (DropDownList)sender;
        //GridDataItem dataItem = (GridDataItem)ddl.NamingContainer;
        //int itaskid = Convert.ToInt16(((Label)dataItem.FindControl("lbltaskid")).Text.ToString());
        //string strpriority = ((DropDownList)dataItem.FindControl("ddlpriority")).Text.ToString();
        //string Query = "Update tasks set priority='" + strpriority + "' where task_id=" + itaskid + "";
        //DatabaseHelper.executeNonQuery(Query);
        //string sql = "";
        //if (Session["admin"].ToString().ToLower().Trim() == drpAssignedTo.SelectedItem.Text.ToString().ToLower().Trim())
        //{
        //    sql = @"select * from tasks where  status <> 'closed' and status <> 'Checked' and status <>'parked' and  deleted <> 1  and assigned_to_user = '" + Session["admin"] + "'ORDER BY last_updated_date desc";
        //}
        //else
        //{
        //    sql = @"select * from tasks where  status <> 'closed' and status <> 'Checked' and status <>'parked' and  deleted <> 1  and assigned_to_user = '" + drpAssignedTo.SelectedItem.Text + "' ORDER BY last_updated_date desc";
        //}
        //BindData(sql);
        DropDownList ddl = (DropDownList)sender;
        GridDataItem dataItem = (GridDataItem)ddl.NamingContainer;
        int itaskid = Convert.ToInt16(((Label)dataItem.FindControl("lbltaskid")).Text.ToString());
        string strpriority = ((DropDownList)dataItem.FindControl("ddlpriority")).Text.ToString();

        //==================================================================
        //Check 
        //==================================================================
        if (strpriority == "1a - DO NOW")
        {
            string sqlchkdonow = "";
            if (Session["admin"].ToString().ToLower().Trim() == drpAssignedTo.SelectedItem.Text.ToString().ToLower().Trim())
            {
                sqlchkdonow = "select * from tasks where status <> 'closed' and status <> 'Checked' and status <>'parked' and deleted<>1 and(priority='1a - DO NOW') and  assigned_to_user='" + Session["admin"].ToString().Replace("'", "''") + "'";
            }
            else
            {

                sqlchkdonow = "select * from tasks where status <> 'closed' and status <> 'Checked' and status <>'parked' and deleted<>1 and(priority='1a - DO NOW') and  assigned_to_user='" + drpAssignedTo.SelectedItem.Text.ToString().Replace("'", "''") + "'";
            }
            DataSet dsdoNow = DatabaseHelper.getDataset(sqlchkdonow);
            if (dsdoNow != null && dsdoNow.Tables.Count > 0)
            {
                if (dsdoNow.Tables[0].Rows.Count >= 1)
                {
                    string sqlQuery = "";
                    if (Session["admin"].ToString().ToLower().Trim() == drpAssignedTo.SelectedItem.Text.ToString().ToLower().Trim())
                    {
                        //sqlQuery = @"select * from tasks where  status <> 'closed' and status <> 'Checked' and status <>'parked' and  deleted <> 1  and assigned_to_user = '" + Session["admin"] + "'ORDER BY last_updated_date desc";
                        sqlQuery = @"select * from tasks where  status <> 'closed' and status <> 'Checked' and status <>'parked' and  deleted <> 1  and assigned_to_user = '" + Session["admin"] + "'ORDER BY OrderId ASC";
                    }
                    else
                    {
                        //sqlQuery = @"select * from tasks where  status <> 'closed' and status <> 'Checked' and status <>'parked' and  deleted <> 1  and assigned_to_user = '" + drpAssignedTo.SelectedItem.Text + "' ORDER BY last_updated_date desc";
                        sqlQuery = @"select * from tasks where  status <> 'closed' and status <> 'Checked' and status <>'parked' and  deleted <> 1  and assigned_to_user = '" + drpAssignedTo.SelectedItem.Text + "' ORDER BY OrderId ASC";
                    }
                    BindData(sqlQuery);
                    Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('This user already have the 1a-DO NOW  task.');</script>");
                    return;
                }
            }
        }
        else if (strpriority == "0 - IMMEDIATE")
        {
            string sqlchkImmidiate = "";
            if (Session["admin"].ToString().ToLower().Trim() == drpAssignedTo.SelectedItem.Text.ToString().ToLower().Trim())
            {
                sqlchkImmidiate = "select * from tasks where status <> 'closed' and status <> 'Checked' and status <>'parked' and deleted<>1 and(priority='0 - IMMEDIATE') and assigned_to_user='" + Session["admin"].ToString().ToString().Replace("'", "''") + "'";
            }
            else
            {
                sqlchkImmidiate = "select * from tasks where status <> 'closed' and status <> 'Checked' and status <>'parked' and deleted<>1 and(priority='0 - IMMEDIATE') and assigned_to_user='" + drpAssignedTo.SelectedItem.Text.ToString().ToString().Replace("'", "''") + "'";
            }
            DataSet dsImmidiate = DatabaseHelper.getDataset(sqlchkImmidiate);
            if (dsImmidiate != null && dsImmidiate.Tables.Count > 0)
            {
                if (dsImmidiate.Tables[0].Rows.Count >= 1)
                {
                    string sqlQueryImme = "";

                    if (Session["admin"].ToString().ToLower().Trim() == drpAssignedTo.SelectedItem.Text.ToString().ToLower().Trim())
                    {
                        sqlQueryImme = @"select * from tasks where  status <> 'closed' and status <> 'Checked' and status <>'parked' and  deleted <> 1  and assigned_to_user = '" + Session["admin"] + "'ORDER BY OrderId ASC";
                        //sqlQueryImme = @"select * from tasks where  status <> 'closed' and status <> 'Checked' and status <>'parked' and  deleted <> 1  and assigned_to_user = '" + Session["admin"] + "'ORDER BY last_updated_date desc";
                    }
                    else
                    {
                        sqlQueryImme = @"select * from tasks where  status <> 'closed' and status <> 'Checked' and status <>'parked' and  deleted <> 1  and assigned_to_user = '" + drpAssignedTo.SelectedItem.Text + "' ORDER BY OrderId ASC";
                        //sqlQueryImme = @"select * from tasks where  status <> 'closed' and status <> 'Checked' and status <>'parked' and  deleted <> 1  and assigned_to_user = '" + drpAssignedTo.SelectedItem.Text + "' ORDER BY last_updated_date desc";
                    }
                    BindData(sqlQueryImme);





                    Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('This user already have 0 - IMMEDIATE task.');</script>");
                    return;
                }
            }
        }
        //else
        //{
        //==================================================================
        string strprePri = "", PreviousPri = "", strSql = ""; ;

        DataSet dsPre = null;
        strprePri = "Select * from Tasks where task_id=" + itaskid + "";
        dsPre = DatabaseHelper.getDataset(strprePri);
        if (dsPre != null && dsPre.Tables.Count > 0 && dsPre.Tables[0].Rows.Count > 0)
        {
            PreviousPri = dsPre.Tables[0].Rows[0]["priority"].ToString();
        }

        string base_sql = @" insert into task_updates([task_id],[username],[post_date],[comment]) 
                            values($task_id,'$us',getdate(),'$comment'); ";

        base_sql = base_sql.Replace("$task_id", itaskid.ToString());
        base_sql = base_sql.Replace("$us", Session["admin"].ToString().Replace("'", "''"));
        strSql += base_sql.Replace(
                "$comment",
                "changed priority from \""
                + PreviousPri.Replace("'", "''") + "\" to \""
                + strpriority.Replace("'", "''") + "\"");
        DatabaseHelper.executeNonQuery(strSql);

        //==================================================================
        // string Query = "Update tasks set priority='" + strpriority + "' where task_id=" + itaskid + "";
        string Query = "Update tasks set priority='" + strpriority + "',last_updated_user='" + Session["admin"].ToString().Replace("'", "''") + "',last_updated_date=getdate() where task_id=" + itaskid + "";
        DatabaseHelper.executeNonQuery(Query);
        string sql = "";
        if (Session["admin"].ToString().ToLower().Trim() == drpAssignedTo.SelectedItem.Text.ToString().ToLower().Trim())
        {
            //sql = @"select * from tasks where  status <> 'closed' and status <> 'Checked' and status <>'parked' and  deleted <> 1  and assigned_to_user = '" + Session["admin"] + "'ORDER BY last_updated_date desc";
            sql = @"select * from tasks where  status <> 'closed' and status <> 'Checked' and status <>'parked' and  deleted <> 1  and assigned_to_user = '" + Session["admin"] + "'ORDER BY OrderId ASC";
        }
        else
        {
            //sql = @"select * from tasks where  status <> 'closed' and status <> 'Checked' and status <>'parked' and  deleted <> 1  and assigned_to_user = '" + drpAssignedTo.SelectedItem.Text + "' ORDER BY last_updated_date desc";
            sql = @"select * from tasks where  status <> 'closed' and status <> 'Checked' and status <>'parked' and  deleted <> 1  and assigned_to_user = '" + drpAssignedTo.SelectedItem.Text + "' ORDER BY OrderId ASC";
        }
        BindData(sql);


    }
    protected void DataGrid1_RowDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        foreach (TableCell tc in e.Item.Cells)
        {
            tc.Attributes["style"] = "border:1px solid #00A651;";
        }
        if (e.Item is GridDataItem)
        {
            GridDataItem dataItem = e.Item as GridDataItem;

            if (Convert.ToInt32(((TextBox)dataItem.FindControl("txtorder")).Text.ToString()) == 99999)
            {
                DataSet ds = new DataSet();
                Label lbltaskId = (Label)dataItem.FindControl("lbltaskid") as Label;
                Label assignuser = (Label)dataItem.FindControl("lblassignedtouser") as Label;
                TextBox txtor=(TextBox)dataItem.FindControl("txtorder") as TextBox;
                string order = assignuser.Text;
                SqlConnection Conn = new SqlConnection(strConn);
                string orderid = "SELECT MAX(OrderId) as orderid FROM tasks where assigned_to_user='" + order + "' and status not in('closed','checked') and orderid <> 99999";

                 SqlDataAdapter da = new SqlDataAdapter(orderid, Conn);
                 da.Fill(ds);
                 int id = Convert.ToInt32(ds.Tables[0].Rows[0]["orderid"].ToString());
                 id = id + 1;
                 ((TextBox)dataItem.FindControl("txtorder")).Text = id.ToString() ;
                 try
                 {
                     string IncreseQ = "Update tasks set OrderId=" + id + " where Task_id=" + Convert.ToInt32(lbltaskId.Text) + ";";
                     int chki = DatabaseHelper.executeNonQuery(IncreseQ);
                 }
                 catch (Exception ex) { }

            
            }
            string newsql = "";
            newsql = @"Select * from dbo.priorities order by sort_seq";
            DropDownList ddl = (DropDownList)e.Item.FindControl("ddlpriority");
            DataSet dspriority = DatabaseHelper.getDataset(newsql);
            ddl.DataSource = dspriority;
            ddl.DataTextField = "priority_name";
            ddl.DataValueField = "priority_name";
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("Select", "0"));
            ((DropDownList)e.Item.FindControl("ddlpriority")).SelectedValue = ((Label)dataItem.FindControl("lblpriority")).Text;
            string htmlColor = (string)DatabaseHelper.executeScalar("select bg_color from priorities where priority_name='" + ((Label)dataItem.FindControl("lblpriority")).Text + "'");
            dataItem.BackColor = System.Drawing.ColorTranslator.FromHtml(htmlColor);
            LinkButton readMarkButton = (LinkButton)dataItem.FindControl("lnkReadMark");
            int ETC = Convert.ToInt32(((HiddenField)dataItem.FindControl("hd")).Value);
            ((Label)dataItem.FindControl("lbletc")).Text = "" + (ETC / 60) + "' " + (ETC % 60) + '"';

            int hrsTaken = getHoursTakenSoFar(((Label)dataItem.FindControl("lbltaskid")).Text);
            ((Label)dataItem.FindControl("lblTimetodate")).Text = "" + (hrsTaken / 60) + "' " + (hrsTaken % 60) + '"';
            int hrsLeft = ETC - hrsTaken;
            if (hrsLeft < 0)
            {
                hrsLeft = -1 * hrsLeft;
                ((Label)dataItem.FindControl("lblTimetodate")).Text = "-" + (hrsLeft / 60) + "' " + (hrsLeft % 60) + '"';
                ((Label)dataItem.FindControl("lblTimetodate")).ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                ((Label)dataItem.FindControl("lblbalance")).Text = "" + (hrsLeft / 60) + "' " + (hrsLeft % 60) + '"';
                ((Label)dataItem.FindControl("lblTimetodate")).ForeColor = System.Drawing.Color.Black;
            }
            int objIsRead;
            try
            {
                string strReadUnread;
                strReadUnread = "select * from task_comments where tc_id not in(select tc_id from read_comments where username ='" + Session["admin"] + "')and task_comments.deleted <> 1 and task_id =" + ((Label)dataItem.FindControl("lbltaskid")).Text;
                DataSet ds = DatabaseHelper.getDataset(strReadUnread);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    readMarkButton.Text = "[<span style='color:red'>Unread.</span>  Mark as read]";
                }
                else
                {
                    readMarkButton.Text = "[<span style='color:green'>Read.</span> Mark as unread]";
                }
            }
            catch
            { }
            //Label lblassignedUser = (Label)dataItem.FindControl("lblassignedtouser");
            //if (lblassignedUser.Text != "" && lblassignedUser.Text != null)
            //{
            //    this.Title = lblassignedUser.Text+" priority report";
            //}
        }
    }

    protected int getHoursTakenSoFar(string task_id)
    {
        string sqlqry;
        sqlqry = "select sum(datediff(minute,started_date,finished_date)) from hours_reporting where finished_date is not null and task_id=" + task_id;

        int hrsTaken = 0;
        object objHrsTaken = DatabaseHelper.executeScalar(sqlqry);
        if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
        {
            hrsTaken += (int)objHrsTaken;
        }
        else
        {
            hrsTaken = 0;
        }
        string qryfornew;
        qryfornew = "select sum(datediff(minute,started_date,getdate())) from hours_reporting where finished_date is null and task_id=" + task_id;

        objHrsTaken = DatabaseHelper.executeScalar(qryfornew);

        if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
        {
            hrsTaken += (int)objHrsTaken;
        }

        return hrsTaken;
    }
    protected void DataGrid1_RowCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
    }
    protected void refreshbtn_Click(object sender, ImageClickEventArgs e)
    {
        loadtask();
    }
    protected void lnkUnansweredQuestions_Click(object sender, EventArgs e)
    {
        statusgrid.Visible = true;
        clientGrid.Visible = false;
        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            assigned_user = Session["admin"].ToString();
        }
        else
        {
            assigned_user = drpAssignedTo.SelectedItem.Text;
        }
        string sql = "select distinct tasks.* from task_comments,tasks where tasks.task_id = task_comments.task_id and  tasks.deleted <> 1 and task_comments.deleted <> 1 and qflag=1 ";
        if (!DatabaseHelper.can_Show_Closed_Tasks(assigned_user.ToString()))
        {
            sql += " and status <> 'closed' ";
        }
        sql += " and task_comments.QuesTo = '" + assigned_user.ToString() + "' ";
        BindData(sql);

    }
    protected void LnkTskNewcomment_Click(object sender, EventArgs e)
    {
        statusgrid.Visible = true;
        clientGrid.Visible = false;
        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            assigned_user = Session["admin"].ToString();
        }
        else
        {
            assigned_user = drpAssignedTo.SelectedItem.Text;
        }
        string sql;
        if (!DatabaseHelper.isAdmin(assigned_user))
        {
            sql = "select tasks.* from tasks where task_id in(select task_comments.task_id from task_comments where tc_id  not in (select  tc_id from read_comments where read_comments.username='" + assigned_user + "') and task_comments.deleted <> 1 )";
            sql += " and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + assigned_user + "' ";
        }
        else
        {
            sql = "select tasks.* from tasks where task_id in(select task_comments.task_id from task_comments where tc_id  not in (select  tc_id from read_comments where read_comments.username='" + assigned_user + "') and task_comments.deleted <> 1 )";
            sql += " and tasks.deleted <> 1 and tasks.assigned_to_user = '" + assigned_user + "' ";
        }
        BindData(sql);
    }
    protected void BtnOverdue_Click(object sender, EventArgs e)
    {
        statusgrid.Visible = true;
        clientGrid.Visible = false;
        try
        {
            if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
            {
                assigned_user = Session["admin"].ToString();
            }
            else
            {
                assigned_user = drpAssignedTo.SelectedItem.Text;
            }
            string sql = "select * from tasks where  status <> 'closed' and deleted <> 1";
            sql += " and status <> 'checked' and status <> 'parked' and assigned_to_user = '" + assigned_user + "' and  DueDate <> ''";
            sql += " ORDER BY last_updated_date desc ";
            BindData(sql);
        }
        catch (Exception ex)
        { }

    }
    protected void LnkNewCR_Click(object sender, EventArgs e)
    {
        statusgrid.Visible = false;
        clientGrid.Visible = true;
        string sql = "select ClientRequest.* from ClientRequest where ClientRequest.Status = 'new' and status <> 'closed' and deleted <> 1 ";
        BindDataclient(sql);

    }
    private void BindDataclient(string strsql)
    {
        DataSet ds = DatabaseHelper.getDataset(strsql);
        ds = formatCrsDataSet(ds);
        PagedDataSource objds = new PagedDataSource();
        objds.DataSource = ds.Tables[0].DefaultView;

        DataGrid2.DataSource = null;
        DataGrid2.DataBind();

        DataGrid2.DataSource = objds;
        DataGrid2.DataBind();
    }
    private DataSet formatCrsDataSet(DataSet ds)
    {
        try
        {
            ds.Tables[0].Columns.Add("ShowAllComment");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {
                    if (ds.Tables[0].Rows[i]["ShortDescr"] != null && ds.Tables[0].Rows[i]["ShortDescr"].ToString().Trim() != "")
                    {
                        ds.Tables[0].Rows[i]["ShortDescr"] = "<a  href=\"edit_request.aspx?reqid=" + ds.Tables[0].Rows[i]["RequestId"].ToString() + "  \">" + ds.Tables[0].Rows[i]["ShortDescr"].ToString() + "</a>";
                    }
                    else
                    {

                        ds.Tables[0].Rows[i]["ShortDescr"] = "";
                    }
                    try
                    {

                        string LastComment = "";
                        string sql;
                        sql = "Select top 1.* from ClientRequest_Details where deleted <> 1 and RequestId=" + ds.Tables[0].Rows[i]["RequestId"].ToString() + " order by CommentId desc";
                        DataSet dslastcomment = DatabaseHelper.getDataset(sql);
                        if (dslastcomment != null && dslastcomment.Tables[0].Rows.Count > 0)
                        {
                            for (int j = 0; j < dslastcomment.Tables[0].Rows.Count; j++)
                            {
                                LastComment += "<span style='color:Green'>" + "Comment " + dslastcomment.Tables[0].Rows[j]["CommentId"].ToString() + " posted by " + dslastcomment.Tables[0].Rows[j]["PostedBy"].ToString() + " on " + DateTime.Parse(dslastcomment.Tables[0].Rows[j]["PostedOn"].ToString()).ToString("dd MMM yyyy h:mm tt") + "</span>";
                                LastComment += "<br/><span>" + dslastcomment.Tables[0].Rows[j]["Comment"].ToString() + "</span>";
                                LastComment += "<span style=\"border-bottom: solid 1px green; margin-bottom: 2px; height:1px\"></span><br/><hr />";

                            }
                        }

                        ds.Tables[0].Rows[i]["ShowAllComment"] = LastComment.ToString();
                    }
                    catch
                    {

                    }

                }
                catch { }
            }
        }
        catch
        {
            Server.ClearError();
        }
        return ds;
    }
    protected void LnkCrNewComment_Click(object sender, EventArgs e)
    {
        statusgrid.Visible = false;
        clientGrid.Visible = true;
        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            assigned_user = Session["admin"].ToString();
        }
        else
        {
            assigned_user = drpAssignedTo.SelectedItem.Text;
        }

        string sql = "select * from ClientRequest where RequestId in(select ClientRequest_Details.RequestId from";
        sql += " ClientRequest_Details where [CommentId] not in (select  CommentId from read_CR_comments";
        sql += " where read_CR_comments.username='" + assigned_user + "')  and ClientRequest_Details.deleted <> 1) ";
        sql += " ORDER BY LastUpdatedOn desc";
        BindDataclient(sql);

    }


    protected void BtnallOverdue_Click(object sender, EventArgs e)
    {
        statusgrid.Visible = true;
        clientGrid.Visible = false;
        string sql = "";
        try
        {
            sql = "select * from tasks where deleted <> 1 and status <> 'checked' and status <> 'closed'and status <> 'parked' and DueDate <> ''";
        }
        catch
        {

        }
        BindData(sql);

    }
    protected void LnkMakeAllTaskCmmntRead_Click(object sender, EventArgs e)
    {
        statusgrid.Visible = true;
        clientGrid.Visible = false;
        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            assigned_user = Session["admin"].ToString();
        }
        else
        {
            assigned_user = drpAssignedTo.SelectedItem.Text;
        }
        string sql = "select tasks.* from tasks where task_id in(select task_comments.task_id from task_comments where tc_id  not in (select  tc_id from read_comments where read_comments.username='" + assigned_user + "') and task_comments.deleted <> 1 )";
        BindData(sql);
    }
    protected void lnktobeanswered_Click(object sender, EventArgs e)
    {
        statusgrid.Visible = true;
        clientGrid.Visible = false;
        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            assigned_user = Session["admin"].ToString();
        }
        else
        {
            assigned_user = drpAssignedTo.SelectedItem.Text;
        }
        string sql = @"select distinct tasks.* from task_comments,tasks where tasks.task_id = task_comments.task_id and  tasks.deleted <> 1 and task_comments.deleted <> 1 and qflag=1 and status <> 'closed' and task_comments.username = '" + assigned_user + "'";
        BindData(sql);
    }
    protected void lnktobeanswered_Click1(object sender, EventArgs e)
    {
        statusgrid.Visible = true;
        clientGrid.Visible = false;
        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            assigned_user = Session["admin"].ToString();
        }
        else
        {
            assigned_user = drpAssignedTo.SelectedItem.Text;
        }
        string sql = @"select distinct tasks.* from task_comments,tasks where tasks.task_id = task_comments.task_id and  tasks.deleted <> 1 and task_comments.deleted <> 1 and qflag=1 and status <> 'closed' and task_comments.username = '" + assigned_user + "'";
        BindData(sql);
    }
    protected void DataGrid1_OnSortCommand(object source, GridSortCommandEventArgs e)
    {
        if (e.SortExpression.ToString() == Session["Column"])
        {
            if (Session["Order"] == "ASC")
            {
                hdnOrderby.Value = " ORDER BY " + e.SortExpression.ToString() + " DESC";
                Session["Order"] = "DESC";
            }
            else
            {
                hdnOrderby.Value = " ORDER BY " + e.SortExpression.ToString() + " ASC";
                Session["Order"] = "ASC";
            }
        }
        else
        {
            hdnOrderby.Value = " ORDER BY " + e.SortExpression.ToString() + " ASC";
            Session["Order"] = "ASC";
        }

        Session["Column"] = e.SortExpression.ToString();
        string sql = "";
        if (Session["admin"].ToString().ToLower().Trim() == drpAssignedTo.SelectedItem.Text.ToString().ToLower().Trim())
        {
            sql = @"select * from tasks where  status <> 'closed' and status <> 'Checked' and status <>'parked' and  deleted <> 1  and assigned_to_user = '" + Session["admin"] + "' " + hdnOrderby.Value;
        }
        else
        {
            sql = @"select * from tasks where  status <> 'closed' and status <> 'Checked' and status <>'parked' and  deleted <> 1  and assigned_to_user = '" + drpAssignedTo.SelectedItem.Text + "' " + hdnOrderby.Value;
        }
        BindData(sql);
    }
    protected void drpFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        load_dropdowns();
        BindData();

    }
    void load_filterDropDown()
    {
        string sqlQuery = "";
        drpFilter.Items.Clear();
        if (DatabaseHelper.isAdmin(Session["Admin"].ToString()))
        {
            sqlQuery = @"select username from users where active='Y' and admin='Y' and username <> '" + Session["Admin"] + "' order by username";

            DataSet dsAdmin = DatabaseHelper.getDataset(sqlQuery);

            if (dsAdmin != null)
            {
                if (dsAdmin.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsAdmin.Tables[0].Rows.Count; i++)
                    {
                        drpFilter.Items.Add("Assigned by " + dsAdmin.Tables[0].Rows[i]["username"]);
                    }
                }
            }
            drpFilter.Items.Add("Assigned by " + Session["Admin"]);
            drpFilter.Items.Add("Assigned by me");
            drpFilter.Items.Add("All tasks by last updated");
            drpFilter.Items.Add("All emps CWO");
            drpFilter.Items.Add("Developers Pool");
            drpFilter.Items.Add("Designers Pool");
            drpFilter.Items.Add("Show my open tasks");
            drpFilter.SelectedValue = (string)DatabaseHelper.executeScalar("select default_selected_item from users where username='" + Session["admin"] + "'");
        }
        else
        {
            drpFilter.Items.Add("Show my open tasks");
            drpFilter.Items.Add("Developers Pool");
            drpFilter.Items.Add("Designers Pool");
        }
    }
    void load_dropdowns()
    {
        string strcombo = "";

        try
        {
            DataSet ds_dropdowns;
            if (Cache["dropdowns"] == null)
            {
                strcombo = @"select project_name from projects where archive!='Y' and  active = 'Y' order by project_name;";

                // categories
                strcombo += "\nselect category_name from categories order by sort_seq, category_name;";
                // priorities
                strcombo += "\nselect priority_name from priorities order by sort_seq, priority_name;";
                // statuses
                strcombo += "\nselect status_name from statuses order by sort_seq, status_name;";

                // users
                strcombo += "\nselect username from users where active = 'Y' order by username;";

                ds_dropdowns = DatabaseHelper.getDataset(strcombo);
                Cache.Insert("dropdowns", ds_dropdowns, null, DateTime.Now.AddHours(6), TimeSpan.Zero);
            }
            else
            {
                ds_dropdowns = (DataSet)Cache["dropdowns"];
            }
            drpProjects.DataSource = ds_dropdowns.Tables[0];
            drpProjects.DataTextField = "project_name";
            drpProjects.DataValueField = "project_name";
            drpProjects.DataBind();
            drpProjects.Items.Insert(0, new ListItem("[no filter]", "0"));


            drpCategories.DataSource = ds_dropdowns.Tables[1];
            drpCategories.DataTextField = "category_name";
            drpCategories.DataValueField = "category_name";
            drpCategories.DataBind();
            drpCategories.Items.Insert(0, new ListItem("[no filter]", "0"));

            drpPriorities.DataSource = ds_dropdowns.Tables[2];
            drpPriorities.DataTextField = "priority_name";
            drpPriorities.DataValueField = "priority_name";
            drpPriorities.DataBind();
            drpPriorities.Items.Insert(0, new ListItem("[no filter]", "0"));
            DrpMultipleTaskPR.DataSource = ds_dropdowns.Tables[2];
            DrpMultipleTaskPR.DataTextField = "priority_name";
            DrpMultipleTaskPR.DataValueField = "priority_name";
            DrpMultipleTaskPR.DataBind();
            DrpMultipleTaskPR.Items.Insert(0, new ListItem("[no filter]", "0"));

            drpStatuses.DataSource = ds_dropdowns.Tables[3];
            drpStatuses.DataTextField = "status_name";
            drpStatuses.DataValueField = "status_name";
            drpStatuses.DataBind();
            drpStatuses.Items.Insert(0, new ListItem("[no filter]", "0"));

            DrpMultipleTaskStatus.DataSource = ds_dropdowns.Tables[3];
            DrpMultipleTaskStatus.DataTextField = "status_name";
            DrpMultipleTaskStatus.DataValueField = "status_name";
            DrpMultipleTaskStatus.DataBind();
            DrpMultipleTaskStatus.Items.Insert(0, new ListItem("[no filter]", "0"));

            drpReportedBy.DataSource = ds_dropdowns.Tables[4];
            drpReportedBy.DataTextField = "username";
            drpReportedBy.DataValueField = "username";
            drpReportedBy.DataBind();
            drpReportedBy.Items.Insert(0, new ListItem("[no filter]", "0"));

            drpAssignedToNew.DataSource = ds_dropdowns.Tables[4];
            drpAssignedToNew.DataTextField = "username";
            drpAssignedToNew.DataValueField = "username";
            drpAssignedToNew.DataBind();
            drpAssignedToNew.Items.Insert(0, new ListItem("[no filter]", "0"));

            DrpMultipleTaskAssing.DataSource = ds_dropdowns.Tables[4];
            DrpMultipleTaskAssing.DataTextField = "username";
            DrpMultipleTaskAssing.DataValueField = "username";
            DrpMultipleTaskAssing.DataBind();
            DrpMultipleTaskAssing.Items.Insert(0, new ListItem("[no filter]", "0"));
        }
        catch { }
    }


    private DataSet NewformatTaskDataSet(DataSet ds)
    {
        try
        {
            ds.Tables[0].Columns.Add("ShowAllComment");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {
                    if (ds.Tables[0].Rows[i]["short_desc"] != null && ds.Tables[0].Rows[i]["short_desc"].ToString().Trim() != "")
                    {
                        ds.Tables[0].Rows[i]["short_desc"] = "<a  href=\"edit_task.aspx?id=" + ds.Tables[0].Rows[i]["task_id"].ToString() + "  \">" + ds.Tables[0].Rows[i]["short_desc"].ToString() + "</a>";
                    }
                    else
                    {
                        ds.Tables[0].Rows[i]["short_desc"] = "";
                    }
                    try
                    {
                        string LastComment = "";
                        if (ds.Tables[0].Rows[i]["qflag"].ToString() == "1")
                        {
                            LastComment += "<span style='color:Red'>" + "Question posted by " + ds.Tables[0].Rows[i]["username"].ToString() + " for  " + ds.Tables[0].Rows[i]["QuesTo"].ToString() + " on " + DateTime.Parse(ds.Tables[0].Rows[i]["post_date"].ToString()).ToString("dd MMM yyyy h:mm tt") + "</span>";
                            LastComment += "<br/><span>" + ds.Tables[0].Rows[i]["comment"].ToString() + "</span>";
                            LastComment += "<span style=\"border-bottom: solid 1px green; margin-bottom: 2px; height:1px\"></span><br/><hr />";
                        }
                        else
                        {
                            LastComment += "<span style='color:Green'>" + "Comment " + ds.Tables[0].Rows[i]["tc_id"].ToString() + " posted by " + ds.Tables[0].Rows[i]["username"].ToString() + " on " + DateTime.Parse(ds.Tables[0].Rows[i]["post_date"].ToString()).ToString("dd MMM yyyy h:mm tt") + "</span>";
                            LastComment += "<br/><span>" + ds.Tables[0].Rows[i]["comment"].ToString() + "</span>";
                            LastComment += "<span style=\"border-bottom: solid 1px green; margin-bottom: 2px; height:1px\"></span><br/><hr />";
                        }
                        ds.Tables[0].Rows[i]["ShowAllComment"] = LastComment.ToString();
                    }
                    catch
                    {
                    }

                }
                catch { }
            }
        }
        catch (Exception ex)
        {
            Server.ClearError();

        }
        return ds;
    }

    protected void drpProjects_SelectedIndexChanged(object sender, EventArgs e)
    {

        BindData();
    }
    private void BindData()
    {
        drpPerPage1.Visible = true;
        string filter = "where ";
        string filter2 = "and";
        string filter3 = " and ";
        LblRecordsno.Text = "";
        DateTime duedate;

        string topFilterLable = "";
        if (!DatabaseHelper.can_Show_Closed_Tasks(Session["admin"].ToString()))
        {
            if (Session["filterunread"] == null || Session["filterunread"].ToString() == "")
            {
                filter += " status <> 'closed' and status <> 'Checked' and status <>'parked' and  ";
                filter2 += " status <> 'closed' and status <> 'Checked' and status <>'parked' and  ";
            }
            else
            {
                filter += " status <> 'closed' and ";
                filter2 += " status <> 'closed' and ";
            }
            chkHideClosed.Visible = false;
        }
        else
        {
            if (chkHideClosed.Checked == true)
            {
                if (Session["filterunread"] == null || Session["filterunread"].ToString() == "")
                {
                    filter += " status <> 'checked' and status <>'parked' and status = 'closed' and ";
                    filter2 += " status <> 'checked' and status <>'parked' and status = 'closed' and ";
                    filter3 += " and status <> 'checked' and status <>'parked' and status = 'closed' and ";
                }

            }
            else
            {
                if (Session["filterunread"] == null || Session["filterunread"].ToString() == "")
                {

                    filter += " status <> 'closed' and status <> 'Checked' and status <>'parked' and ";
                    filter2 += " status <> 'closed' and status <> 'Checked' and status <>'parked' and ";
                    filter3 += " status <> 'closed' and status <> 'Checked' and status <>'parked' and ";

                }
                else
                {

                    filter += " status <> 'closed' and  ";
                    filter2 += " status <> 'closed' and ";
                    filter3 += " status <> 'closed' and ";

                }
            }
        }
        if (chkViewDeleted.Checked == true)
        {
            filter += " deleted = 1 ";
            filter2 += " deleted = 1 ";
            filter3 += " deleted = 1 ";
        }
        else
        {

            filter += " deleted <> 1 ";
            filter2 += " deleted <> 1 ";
            filter3 += " deleted <> 1 ";

        }

        string drpValue = drpFilter.SelectedValue;
        string username = "";


        if (drpValue.Contains("Assigned by me"))
        {
            filter += " and reported_user = '" + username + "' ";
            filter2 += " and reported_user = '" + username + "' ";
        }
        else if (drpValue.Contains("Developers Pool"))
        {

            filter += " and assigned_to_user = '" + drpFilter.SelectedValue + "' ";
            filter2 += " and assigned_to_user = '" + drpFilter.SelectedValue + "' ";

        }
        else if (drpValue.Contains("Designers Pool"))
        {
            filter += " and assigned_to_user = '" + drpFilter.SelectedValue + "' ";
            filter2 += " and assigned_to_user = '" + drpFilter.SelectedValue + "' ";

        }
        else if (drpValue.Contains("Assigned by "))
        {
            username = drpValue.Replace("Assigned by ", "");

            filter += " and reported_user = '" + username + "' ";
            filter2 += " and reported_user = '" + username + "' ";
        }
        else if (drpValue.Contains("All emps CWO"))
        {
            filter += " and status = 'CWO' ";
            filter2 += " and status = 'CWO' ";
            filter3 += " and status = 'CWO' ";
        }
        else if (drpValue.Contains("Show my open tasks"))
        {
            filter += " and assigned_to_user = '" + Session["admin"] + "' ";
            filter2 += " and assigned_to_user = '" + Session["admin"] + "' ";
            adminhide = false;

        }
        else if (drpValue.Contains("All tasks by last updated"))
        {
            if (Session["adm"] == "no" || adminhide == true)
            {
                filter += " and assigned_to_user = '" + Session["admin"] + "' ";
                filter2 += " and assigned_to_user = '" + Session["admin"] + "' ";
                adminhide = false;
            }
        }
        if (drpProjects.SelectedIndex != 0)
        {
            filter += " and project = '" + drpProjects.SelectedValue + "' ";
            filter2 += " and project = '" + drpProjects.SelectedValue + "' ";
            filter3 += " and project = '" + drpProjects.SelectedValue + "' ";

            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Project: " + drpProjects.SelectedValue;
            }
            else
            {
                topFilterLable += " / Project: " + drpProjects.SelectedValue;
            }
        }

        if (drpCategories.SelectedIndex != 0)
        {
            filter += " and category = '" + drpCategories.SelectedValue + "' ";
            filter2 += " and category = '" + drpCategories.SelectedValue + "' ";
            filter3 += " and category = '" + drpCategories.SelectedValue + "' ";

            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Category: " + drpCategories.SelectedValue;
            }
            else
            {
                topFilterLable += " / Category: " + drpCategories.SelectedValue;
            }
        }

        if (drpReportedBy.SelectedIndex != 0)
        {
            filter += " and reported_user = '" + drpReportedBy.SelectedValue + "' ";
            filter2 += " and reported_user = '" + drpReportedBy.SelectedValue + "' ";
            filter3 += " and reported_user = '" + drpReportedBy.SelectedValue + "' ";

            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Reported by: " + drpReportedBy.SelectedValue;
            }
            else
            {
                topFilterLable += " / Reported by:" + drpReportedBy.SelectedValue;
            }
        }
        if (hidecheck == false)
        {
            if (drpPriorities.SelectedIndex != 0)
            {
                filter += " and priority = '" + drpPriorities.SelectedValue + "' ";
                filter2 += " and priority = '" + drpPriorities.SelectedValue + "' ";
                filter3 += " and priority = '" + drpPriorities.SelectedValue + "' ";

                if (topFilterLable == "")
                {
                    topFilterLable = "Filtered by Priority: " + drpPriorities.SelectedValue;
                }
                else
                {
                    topFilterLable += " / Priority:" + drpPriorities.SelectedValue;
                }
            }
        }

        if (drpAssignedToNew.SelectedIndex != 0)
        {
            if (Session["adm"] != "yes")
            {
                filter = filter.Replace("and assigned_to_user = '" + Session["admin"] + "' ", " and assigned_to_user = '" + drpAssignedToNew.SelectedValue + "' ");
                filter2 = filter2.Replace("and assigned_to_user = '" + Session["admin"] + "' ", " and assigned_to_user = '" + drpAssignedToNew.SelectedValue + "' ");
                filter3 = filter3.Replace("and assigned_to_user = '" + Session["admin"] + "' ", " and assigned_to_user = '" + drpAssignedToNew.SelectedValue + "' ");
            }
            else
            {
                filter = filter.Replace("and assigned_to_user = '" + Session["admin"] + "' ", " and assigned_to_user = '" + drpAssignedToNew.SelectedValue + "' ");
                filter2 = filter2.Replace("and assigned_to_user = '" + Session["admin"] + "' ", " and assigned_to_user = '" + drpAssignedToNew.SelectedValue + "' ");
                filter3 = filter3.Replace("and assigned_to_user = '" + Session["admin"] + "' ", " and assigned_to_user = '" + drpAssignedToNew.SelectedValue + "' ");

            }
            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Assigned to: " + drpAssignedTo.SelectedValue;
            }
            else
            {
                topFilterLable += " / Assigned to: " + drpAssignedTo.SelectedValue;
            }
        }

        if (drpStatuses.SelectedIndex != 0)
        {
            if (drpStatuses.SelectedValue == "CWO")
            {
                filter = filter.Replace("and status <> 'Checked'", "").Replace("and status <>'parked'", "");
                filter2 = filter2.Replace("and status <> 'Checked'", "").Replace("and status <>'parked'", "");
                filter3 = filter3.Replace("and status <> 'Checked'", "").Replace("and status <>'parked'", "");
                filter += " and status = '" + drpStatuses.SelectedValue + "' ";
                filter2 += " and status = '" + drpStatuses.SelectedValue + "' ";
                filter3 += " and status = '" + drpStatuses.SelectedValue + "' ";

                if (topFilterLable == "")
                {
                    topFilterLable = "Filtered by Status: " + drpStatuses.SelectedValue;
                }
                else
                {
                    topFilterLable += " / Status: " + drpStatuses.SelectedValue;
                }
            }
            else if (drpStatuses.SelectedValue == "CWO but away")
            {
                filter = filter.Replace("and status <> 'Checked'", "").Replace("and status <>'parked'", "");
                filter2 = filter2.Replace("and status <> 'Checked'", "").Replace("and status <>'parked'", "");
                filter3 = filter3.Replace("and status <> 'Checked'", "").Replace("and status <>'parked'", "");
                filter += " and status =  '" + drpStatuses.SelectedValue + "' ";
                filter2 += " and status = '" + drpStatuses.SelectedValue + "' ";
                filter3 += " and status = '" + drpStatuses.SelectedValue + "' ";

                if (topFilterLable == "")
                {
                    topFilterLable = "Filtered by Status: " + drpStatuses.SelectedValue;
                }
                else
                {
                    topFilterLable += " / Status: " + drpStatuses.SelectedValue;
                }
            }
            else
            {
                filter = filter.Replace("and status <> 'Checked'", "").Replace("and status <>'parked'", "");
                filter2 = filter2.Replace("and status <> 'Checked'", "").Replace("and status <>'parked'", "");
                filter3 = filter3.Replace("and status <> 'Checked'", "").Replace("and status <>'parked'", "");
                filter += " and status =  '" + drpStatuses.SelectedValue + "' ";
                filter2 += " and status = '" + drpStatuses.SelectedValue + "' ";
                filter3 += " and status = '" + drpStatuses.SelectedValue + "' ";

                if (topFilterLable == "")
                {
                    topFilterLable = "Filtered by Status: " + drpStatuses.SelectedValue;
                }
                else
                {
                    topFilterLable += " / Status: " + drpStatuses.SelectedValue;
                }
            }
        }
        if (ChkReadTask.Checked)
        {
        }
        else
        {
            filter += " and  tasks.task_id  in(select task_id from task_comments where tc_id not in(select tc_id from read_comments where username='" + Session["admin"].ToString() + "')) ";
            filter2 += " and  tasks.task_id  in(select task_id from task_comments where tc_id not in(select tc_id from read_comments where username='" + Session["admin"].ToString() + "')) ";
            filter3 += " and  tasks.task_id  in(select task_id from task_comments where tc_id not in(select tc_id from read_comments where username='" + Session["admin"].ToString() + "')) ";
        }
        if (chkViewDeleted.Checked == true)
        {
            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by View only deleted";
            }
            else
            {
                topFilterLable += " / View only deleted";
            }
        }
        if (chkHideClosed.Checked == true)
        {
            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by view closed";
            }
            else
            {
                topFilterLable += " / view closed";
            }
        }

        TopFilterLable.InnerHtml = topFilterLable;

        DataSet ds = null;

        int ipagesize;
        string startquery = "";
        if (drpPerPage1.SelectedValue != "00")
        {

            ipagesize = Convert.ToInt16(drpPerPage1.SelectedItem.Text);
            startquery = "Select top " + ipagesize + "";
        }
        else
        {
            startquery = "Select";
        }
        string sqlQuery = "";
        try
        {
            sqlQuery = startquery;
            sqlQuery += @" t.*, tc.*,
                (select count(*) as commenttype from task_comments where tc_id not in 
                (select tc_id from read_comments where username ='" + Session["admin"].ToString() + @"')
                and task_comments.deleted <> 1 and task_id =t.task_id)
                from tasks t inner join task_comments tc on 
                t.task_id = tc.task_id and tc.post_date=(select top 1 post_date from task_comments where tc.task_id = task_id and deleted<>1 
                order by post_date desc)" + filter.Replace("deleted <> 1", "t.deleted <> 1") + " ORDER BY last_updated_date desc";
            ds = DatabaseHelper.getDataset(sqlQuery);
            ds = NewformatTaskDataSet(ds);
        }
        catch (Exception ex)
        { }

        try
        {
            DataSet NoofRecords = DatabaseHelper.getDataset(@"Select t.*, tc.*,
                (select count(*) as commenttype from task_comments where tc_id not in 
                (select tc_id from read_comments where username ='" + Session["admin"].ToString() + @"')
                and task_comments.deleted <> 1 and task_id =t.task_id) from tasks t inner join task_comments tc on 
                t.task_id = tc.task_id and tc.post_date=(select top 1 post_date from task_comments where tc.task_id = task_id and deleted<>1 
                order by post_date desc)" + filter.Replace("deleted <> 1", "t.deleted <> 1") + " ORDER BY last_updated_date desc");
            if (NoofRecords != null && NoofRecords.Tables.Count > 0 && NoofRecords.Tables[0].Rows.Count > 0)
            {
                LblRecordsno.Text = NoofRecords.Tables[0].Rows.Count.ToString();
            }
            else
            {
                LblRecordsno.Text = "0";
            }
        }
        catch (Exception ex)
        { }

        if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0].Rows.Count <= 0)
        {
            DataGrid1.DataSource = null;
            DataGrid1.DataBind();

        }
        else
        {
            PagedDataSource objds = new PagedDataSource();
            objds.DataSource = ds.Tables[0].DefaultView;
            objds.AllowPaging = true;

            if (drpPerPage1.SelectedValue != "00")
                objds.PageSize = Convert.ToInt16(drpPerPage1.SelectedItem.Text);
            else
                objds.PageSize = ds.Tables[0].Rows.Count;
            DataGrid1.DataSource = null;
            DataGrid1.DataBind();
            DataGrid1.DataSource = objds;
            try
            {
                DataGrid1.DataBind();
            }
            catch (Exception ex)
            { }
            //setRowValue();
        }
    }
    private void setRowValue()
    {
        //===================================================================================
        try
        {
            bool chkbool;
            chkbool = DatabaseHelper.can_Delete_Tasks(drpAssignedTo.SelectedItem.Text.ToString());
            DataGrid1.Columns[1].Visible = false;



            if (!chkbool)
            {
                DataGrid1.Columns[1].Visible = false;

            }
            else
            {
                DataGrid1.Columns[1].Visible = true;

            }

        }
        catch (Exception Ex)
        { }
        //=================================================================================

    }
    protected void drpCategories_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindData();
    }
    protected void drpReportedBy_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindData();
    }
    protected void drpPriorities_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindData();
    }
    protected void drpStatuses_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindData();
    }
    protected void drpPerPage1_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindData();
    }
    protected void drpAssignedToNMew_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindData();
    }
    protected void btnClearFilters_Click(object sender, EventArgs e)
    {
        load_dropdowns();
        load_filterDropDown();
        chkViewDeleted.Checked = false;
        chkHideClosed.Checked = false;
        ChkReadTask.Checked = true;
        BindData();
    }
    protected void chkViewDeleted_CheckedChanged(object sender, EventArgs e)
    {
        load_dropdowns();
        load_filterDropDown();
        BindData();
    }
    protected void chkHideClosed_CheckedChanged(object sender, EventArgs e)
    {
        BindData();
    }
    protected void ChkReadTask_CheckedChanged(object sender, EventArgs e)
    {
        BindData();
    }
    protected void btnGo_Click(object sender, EventArgs e)
    {
        Response.Redirect("edit_request.aspx?reqid=" + txtRequestId.Text.Trim(), false);
    }
    protected void btnGotoTask_Click(object sender, EventArgs e)
    {
        Response.Redirect("edit_task.aspx?id=" + txtTaskId.Text.Trim(), false);
    }
    protected void BtnGoWrdSearch_Click(object sender, EventArgs e)
    {
        if (ChkTask.Checked)
        {
            Session["TaskByWord"] = "true";
        }
        else
        {
            Session["TaskByWord"] = "false";
        }
        if (ChkCrs.Checked)
        {
            Session["CRsByWord"] = "true";
        }
        else
        {
            Session["CRsByWord"] = "false";
        }
        if (ChkTask.Checked == false && ChkCrs.Checked == false)
        {
            Session["TaskByWord"] = "true";
        }
        if (ChkIncludecomment.Checked)
        {
            Session["Includecomment"] = "true";
        }
        else
        {
            Session["Includecomment"] = "false";
        }

        Session["SearchWord"] = txtWord.Text.Trim();
        Response.Redirect("WordSearchResult.aspx", false);
    }


    protected void LnkBtnToDeleteAll_Click(object sender, EventArgs e)
    {
        string Sqldelete = "";
        int i = 0;
        foreach (GridDataItem row in DataGrid1.Items)
        {

            try
            {
                CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                if (cb != null && cb.Checked)
                {
                    i = i + 1;
                    Sqldelete = "Update tasks set deleted=1,last_updated_date = getdate(),last_updated_user='" + Session["admin"].ToString().Replace("'", "''") + "' where task_id=" + Convert.ToInt32(((Label)row.FindControl("lbltaskId")).Text);
                    int intResult = DatabaseHelper.executeNonQuery(Sqldelete);
                    if (intResult != 0)
                    {
                        Sqldelete = @" insert into task_updates([task_id],[username],[post_date],[comment])values(" + Convert.ToInt32(((Label)row.FindControl("lbltaskId")).Text) + ",'" + Session["admin"].ToString().Replace("'", "''") + "',getdate(),'Task was deleted.'); ";
                        intResult = DatabaseHelper.executeNonQuery(Sqldelete);
                        try
                        {
                            Sqldelete = @"select *  from task_comments where task_id =" + Convert.ToInt32(((Label)row.FindControl("lbltaskId")).Text) + " and  tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "')";
                            DataSet dscomment = DatabaseHelper.getDataset(Sqldelete);
                            if (dscomment != null && dscomment.Tables[0].Rows.Count > 0)
                            {
                                for (int j = 0; j < dscomment.Tables[0].Rows.Count; j++)
                                {
                                    try
                                    {
                                        string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                                        sqlinsert += " values(" + dscomment.Tables[0].Rows[j]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";
                                        int intCommentResult = DatabaseHelper.executeNonQuery(sqlinsert);
                                    }
                                    catch
                                    {
                                    }
                                }

                            }
                        }
                        catch { }
                    }

                }
            }
            catch { }
        }
        if (i > 0)
        {
            Response.Redirect("Priority_reports.aspx", false);
        }
        else
        {

            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('Please select task to delete.');</script>");
            return;
        }
    }

    protected void LnkBtnToMarkAllRead_Click(object sender, EventArgs e)
    {
        string sqlmarkread = "";

        int i = 0;
        foreach (GridDataItem row in DataGrid1.Items)
        {
            try
            {
                CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                if (cb != null && cb.Checked)
                {
                    i = i + 1;
                    sqlmarkread = "insert into read_task([task_id],[username])values(" + Convert.ToInt32(((Label)row.FindControl("lbltaskId")).Text) + ",'" + Session["admin"].ToString() + "')";
                    DatabaseHelper.executeNonQuery(sqlmarkread);
                    sqlmarkread = @"select *  from task_comments where task_id =" + Convert.ToInt32(((Label)row.FindControl("lbltaskId")).Text) + " and  tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "')";
                    DataSet ds = DatabaseHelper.getDataset(sqlmarkread);
                    if (ds != null && ds.Tables[0].Rows.Count > 0)
                    {
                        for (int j = 0; j < ds.Tables[0].Rows.Count; j++)
                        {
                            try
                            {
                                string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                                sqlinsert += " values(" + ds.Tables[0].Rows[j]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";
                                int intResult = DatabaseHelper.executeNonQuery(sqlinsert);

                            }
                            catch
                            {
                            }
                        }
                    }

                    Response.Redirect("Priority_reports.aspx", false);
                }
            }

            catch { }
        }
        if (i > 0)
        {
            Response.Redirect("Priority_reports.aspx", false);
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('Please select task to mark as read.');</script>");

            return;

        }


    }

    protected void LnkBtnToChngPR_Click(object sender, EventArgs e)
    {
        string SqlchangePR = "";
        int i = 0;
        foreach (GridDataItem row in DataGrid1.Items)
        {

            try
            {
                CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                if (cb != null && cb.Checked)
                {
                    i = i + 1;
                    SqlchangePR = "Update tasks set priority='" + DrpMultipleTaskPR.SelectedItem.Text.Replace("'", "''") + "',last_updated_date =getdate(),last_updated_user='" + Session["admin"].ToString().Replace("'", "''") + "' where task_id=" + Convert.ToInt32(((Label)row.FindControl("lbltaskId")).Text);
                    int intResult = DatabaseHelper.executeNonQuery(SqlchangePR);

                    if (intResult != 0)
                    {
                        string cmnt = "";
                        cmnt = "changed priority from " + ((Label)row.FindControl("priorityvalue")).Text + " to " + DrpMultipleTaskPR.SelectedItem;
                        SqlchangePR = @" insert into task_updates([task_id],[username],[post_date],[comment])values(" + Convert.ToInt32(((Label)row.FindControl("lbltaskId")).Text) + ",'" + Session["admin"].ToString().Replace("'", "''") + "',getdate(),'" + cmnt.ToString() + "'); ";
                        intResult = DatabaseHelper.executeNonQuery(SqlchangePR);
                    }
                }
            }
            catch { }
        }
        if (i > 0)
        {
            Response.Redirect("Priority_reports.aspx", false);
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('Please select task to change priority.');</script>");

            return;

        }
    }
    protected void LnkChngAssingedTo_Click(object sender, EventArgs e)
    {
        string sqlassignedTo = "";
        int i = 0;
        foreach (GridDataItem row in DataGrid1.Items)
        {
            try
            {
                CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                if (cb != null && cb.Checked)
                {
                    i = i + 1;
                    sqlassignedTo = "Update tasks set assigned_to_user='" + DrpMultipleTaskAssing.SelectedItem.Text.Replace("'", "''") + "',last_updated_date =getdate(),last_updated_user='" + Session["admin"].ToString().Replace("'", "''") + "' where task_id=" + Convert.ToInt32(((Label)row.FindControl("lbltaskId")).Text);
                    int intResult = DatabaseHelper.executeNonQuery(sqlassignedTo);
                    if (intResult != 0)
                    {
                        string cmnt = "";
                        cmnt = "changed assinged to from " + ((Label)row.FindControl("lblassignedtouser")).Text + " to  " + DrpMultipleTaskAssing.SelectedItem.Text.Replace("'", "''");
                        sqlassignedTo = @" insert into task_updates([task_id],[username],[post_date],[comment])values(" + Convert.ToInt32(((Label)row.FindControl("lbltaskId")).Text) + ",'" + Session["admin"].ToString().Replace("'", "''") + "',getdate(),'" + cmnt.ToString() + "'); ";
                        intResult = DatabaseHelper.executeNonQuery(sqlassignedTo);
                    }

                }
            }
            catch { }
        }
        if (i > 0)
        {
            Response.Redirect("Priority_reports.aspx", false);
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('Please select task to change assigned to.');</script>");
            return;

        }

    }

    protected void LnkChngStatus_Click(object sender, EventArgs e)
    {
        string sqlstatus = "";

        int i = 0;

        foreach (GridDataItem row in DataGrid1.Items)
        {
            try
            {
                CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                if (cb != null && cb.Checked)
                {
                    i = i + 1;
                    sqlstatus = "Update tasks set status='" + DrpMultipleTaskStatus.SelectedItem.Text.Replace("'", "''") + "',last_updated_date =getdate(),last_updated_user='" + Session["admin"].ToString().Replace("'", "''") + "' where task_id=" + Convert.ToInt32(((Label)row.FindControl("lbltaskId")).Text);
                    int intResult = DatabaseHelper.executeNonQuery(sqlstatus);
                    if (intResult != 0)
                    {
                        string cmnt = "";
                        cmnt = "changed assinged to from " + ((Label)row.FindControl("lblassignedtouser")).Text + " to  " + DrpMultipleTaskAssing.SelectedItem.Text.Replace("'", "''");
                        sqlstatus = @" insert into task_updates([task_id],[username],[post_date],[comment])values(" + Convert.ToInt32(((Label)row.FindControl("lbltaskId")).Text) + ",'" + Session["admin"].ToString().Replace("'", "''") + "',getdate(),'" + cmnt.ToString() + "'); ";
                        intResult = DatabaseHelper.executeNonQuery(sqlstatus);

                    }

                }
            }
            catch { }
        }
        if (i > 0)
        {
            Response.Redirect("Priority_reports.aspx", false);
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('Please select task to change status.');</script>");

            return;

        }


    }
    protected void chkArchive_CheckedChanged(object sender, EventArgs e)
    {
        if (chkArchive.Checked)
        {
            load_AllProject();

        }
        else
        {
            load_notArchive();
        }
    }


    void load_notArchive()
    {
        string strcomboArc = "";

        try
        {
            DataSet ds_Archive;
            if (Cache["Dropdowns_For_NotArchive"] == null)
            {
                strcomboArc = @"select project_name from projects where archive !='Y' and active = 'Y' order by project_name;";
                ds_Archive = DatabaseHelper.getDataset(strcomboArc);
                Cache.Insert("Dropdowns_For_NotArchive", ds_Archive, null, DateTime.Now.AddHours(6), TimeSpan.Zero);
            }
            else
            {
                ds_Archive = (DataSet)Cache["Dropdowns_For_NotArchive"];
            }
            drpProjects.DataSource = ds_Archive.Tables[0];
            drpProjects.DataTextField = "project_name";
            drpProjects.DataValueField = "project_name";
            drpProjects.DataBind();
            drpProjects.Items.Insert(0, new ListItem("[no filter]", "0"));

        }
        catch { }
    }
    void load_AllProject()
    {
        string strNonArc = "";

        try
        {
            DataSet ds_NonArc;
            if (Cache["Dropdowns_For_AllProject"] == null)
            {
                strNonArc = @"select project_name from projects where  active = 'Y' order by project_name;";
                ds_NonArc = DatabaseHelper.getDataset(strNonArc);
                Cache.Insert("Dropdowns_For_AllProject", ds_NonArc, null, DateTime.Now.AddHours(6), TimeSpan.Zero);
            }
            else
            {
                ds_NonArc = (DataSet)Cache["Dropdowns_For_AllProject"];
            }
            drpProjects.DataSource = ds_NonArc.Tables[0];
            drpProjects.DataTextField = "project_name";
            drpProjects.DataValueField = "project_name";
            drpProjects.DataBind();
            drpProjects.Items.Insert(0, new ListItem("[no filter]", "0"));

        }
        catch { }
    }

    protected void downloadcv_Click(object sender, EventArgs e)
    {
        string sqluser = "", Fullfilename = "", originalfilename = "";
        if (Session["admin"].ToString().ToLower().Trim() == drpAssignedTo.SelectedItem.Text.ToString().ToLower().Trim())
        {

            sqluser = @"select *,pkey +'_'+ Cv_name as FileName1 from users where username = '" + Session["admin"] + "'";
        }
        else
        {
            sqluser = @"select *,pkey +'_'+ Cv_name as FileName1 from users where username = '" + drpAssignedTo.SelectedItem.Text + "'";

        }
        DataSet dsforfilename = DatabaseHelper.getDataset(sqluser);
        if (dsforfilename != null && dsforfilename.Tables.Count > 0 && dsforfilename.Tables[0].Rows.Count > 0)
        {
            Fullfilename = dsforfilename.Tables[0].Rows[0]["FileName1"].ToString();
            originalfilename = dsforfilename.Tables[0].Rows[0]["Cv_name"].ToString().Replace(" ", "_");
            string path = Server.MapPath(Request.ApplicationPath + "\\Attachment\\backup");

            path += "\\" + Fullfilename;
            System.IO.FileInfo file = new System.IO.FileInfo(path);

            if (file.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + originalfilename);
                Response.AddHeader("Content-Length", file.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(file.FullName);
                Response.End();
            }
        }

    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        string sqlstring = "";
        if (Session["admin"].ToString().ToLower().Trim() == drpAssignedTo.SelectedItem.Text.ToString().ToLower().Trim())
        {
             sqlstring = "Select * from users where username = '" + Session["admin"] + "'";
        }
        else
        {
            sqlstring = "Select * from users where username = '" + drpAssignedTo.SelectedItem.Text + "'";
        }

        DataSet dsuserdetails = DatabaseHelper.getDataset(sqlstring);
        if (dsuserdetails != null && dsuserdetails.Tables.Count > 0 && dsuserdetails.Tables[0].Rows.Count > 0)
        {
            string email = dsuserdetails.Tables[0].Rows[0]["email"].ToString();
            //string email = "avinashj@esolutionsgroup.co.uk"; 
            string Subject = "Tasks have been re-ordered";
            string Body = "";
            if (Session["admin"].ToString().ToLower().Trim() == drpAssignedTo.SelectedItem.Text.ToString().ToLower().Trim())
            {
                Body = Session["admin"] + ", <br /><br />Please note that the tasks in ESG Desk assigned to you have been re-ordered and the person who re-ordered them would like you to take a look at them and change tasks if appropriate.<br /><br />http://www.esgdesk.com/admin/tasks.aspx <br />";

            }
            else
            {
                Body = drpAssignedTo.SelectedItem.Text + ", <br /><br />Please note that the tasks in ESG Desk assigned to you have been re-ordered and the person who re-ordered them would like you to take a look at them and change tasks if appropriate.<br /><br />http://www.esgdesk.com/admin/tasks.aspx <br />";

            }
           // DatabaseHelper.SendEmailList(email, Subject, Body);
            bool b;
            b = DatabaseHelper.SendEmailListEmp(email, Subject, Body);
            if (b == true)
            {
                Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('Mail sent to employee.');</script>");
                return;
            }
            else
            {
                Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('Couldn't sent mail to employee this time. Please try again later!');</script>");
                return;
            }

        }

    }






}