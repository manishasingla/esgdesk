﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Priority_reports20June.aspx.cs" Inherits="Admin_Priority_reports20June" %>

<%@ Register Src="Notifications.ascx" TagName="Notifications" TagPrefix="uc4" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        Priority report
    </title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form2" runat="server">
        <div id="wrapper">
            <uc2:Admin_Header ID="Admin_Header1" runat="server" />
            <div id="Content">
                <div id="Content_98">
                    <div class="titleText" style="margin:10px 0 10px 0;display:none;">
                        Categories&nbsp;|&nbsp;</div>
                    <div>
                        <%--<uc4:Notifications ID="Notifications" runat="server"  />--%>
                    </div>
                    <%--<br/>
                    <br/>--%>
                    <div style="height:100px">
                        <div id="dvemp" runat="server">
                            All tasks priority
                        </div>
                       <%-- <br />--%>
                        <div>
                            <%--==================0-Immediate task========================================================--%>
                            <asp:LinkButton ID="lnkPr0Task"  runat="server" CausesValidation="False"
                                OnClick="lnkPr0Task_Click"></asp:LinkButton>
                            <asp:Label ID="lbl_Immediate" runat="server"></asp:Label>
                            <%--==========================================================================--%>
                            &nbsp;|&nbsp;
                            <%--======================1-a Do-NOW task====================================================--%>
                            <asp:LinkButton ID="lnkHighestTasks"  runat="server" CausesValidation="False"
                                OnClick="lnkHighestTasks_Click"></asp:LinkButton>
                            <asp:Label ID="lbl_1adonow" runat="server"></asp:Label>
                            <%--==========================================================================--%>
                            &nbsp;|&nbsp;
                            <%--=====================1-b high task=====================================================--%>
                            <asp:LinkButton ID="lnkHighTasks"  runat="server" CausesValidation="False"
                                OnClick="lnkHighTasks_Click"></asp:LinkButton>
                            <asp:Label ID="lbl_1bhigh" runat="server"></asp:Label>
                            <%--==========================================================================--%>
                            &nbsp;|&nbsp;
                            <%--======================1-c normal task====================================================--%>
                            <asp:LinkButton ID="Lnk1cTasks"  runat="server" CausesValidation="False"
                                OnClick="Lnk1cTasks_Click"></asp:LinkButton>
                            <asp:Label ID="lbl_1cnormal" runat="server"></asp:Label>
                            <%--==========================================================================--%>
                            &nbsp;|&nbsp;
                            <%--======================2-not urgent task================================================--%>
                            <asp:LinkButton ID="Link2notTasks"  runat="server" CausesValidation="False"
                                OnClick="Link2notTasks_Click"></asp:LinkButton>
                            <asp:Label ID="lbl_2noturgent" runat="server"></asp:Label>
                            <%--==========================================================================--%>
                            &nbsp;|&nbsp;
                            <%--======================3 Low task====================================================--%>
                            <asp:LinkButton ID="Link3lowTasks"  runat="server" CausesValidation="False"
                                OnClick="Link3lowTasks_Click1"></asp:LinkButton>
                            <asp:Label ID="lbl_3low" runat="server"></asp:Label>
                            <%--==========================================================================--%>
                            &nbsp;|&nbsp;
                            <%--======================4 very low prioriity====================================================--%>
                            <asp:LinkButton ID="Link4lowTasks"  runat="server" CausesValidation="False"
                                OnClick="Link4lowTasks_Click"></asp:LinkButton>
                            <asp:Label ID="lbl4verylow" runat="server"></asp:Label>
                            <%--==========================================================================--%>
                            &nbsp;|&nbsp;
                            <%--=======================5-parked===================================================--%>
                            <asp:LinkButton ID="Link5parkedTasks" runat="server" CausesValidation="False"
                                OnClick="Link5parkedTasks_Click"></asp:LinkButton>
                            <asp:Label ID="lbl5parked" runat="server"></asp:Label><br />
                            <%--==========================================================================--%>
                        </div>
                        <br />
                        
                        <div id="dvadmin" runat="server">
                            Select employee name
                            <asp:DropDownList ID="drpAssignedTo" runat="server" OnSelectedIndexChanged="drpAssignedTo_SelectedIndexChanged"
                                AutoPostBack="True">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <asp:GridView ID="DataGrid1" runat="server" Width="100%" BorderColor="#cccccc" AutoGenerateColumns="False"
                CellPadding="5" ShowFooter="True" OnRowDataBound="DataGrid1_RowDataBound" OnRowCommand="DataGrid1_RowCommand">
                <RowStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Top" Font-Size="12px" />
                <Columns>
                    <asp:BoundField DataField="task_id" HeaderText="ID" Visible="False" SortExpression="task_id" />
                    <asp:BoundField DataField="short_desc" HeaderText="ShrtDesc" Visible="False" SortExpression="short_desc" />
                    <asp:TemplateField HeaderText="Short description" SortExpression="short_desc">
                        <ItemTemplate>
                            <asp:Label Font-Size="12px" ID="lblshrtDesc" runat="server" Width="250px" Text='<%# bind("short_desc") %>'></asp:Label>
                            <div id="lblshrtDesc1" runat="server" style="display: none; position: absolute; background-color: #FEFFB3;
                                border: solid 1px #333333; width: 730px; margin-top: 5px; padding-left: 3px">
                                <asp:Label ID="LblLastcomment" runat="server" Text='<%# bind("ShowAllComment") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:HyperLinkField Text="(+)" Visible="False" DataNavigateUrlFields="task_id" NavigateUrl="edit_task.aspx"
                        DataNavigateUrlFormatString="edit_task.aspx?id={0}" SortExpression="short_desc">
                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                            Font-Underline="False" Font-Size="13px" />
                    </asp:HyperLinkField>
                    <asp:TemplateField HeaderText="Read/Unread">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkReadMark" runat="server" Width="130px" CommandName="ReadMark">(Unread. Mark as read)</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Project" SortExpression="project">
                        <ItemTemplate>
                               <%# Eval("project")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="status" HeaderText="Status" SortExpression="status" />
                    <asp:BoundField DataField="priority" HeaderText="Priority" SortExpression="priority" />
                    <asp:TemplateField HeaderText="Assigned to" SortExpression="assigned_to_user">
                        <ItemTemplate>
                               <%# Eval("assigned_to_user") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Last Updated By" SortExpression="last_updated_user">
                        <ItemTemplate>
                               <%# Eval("last_updated_user")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Last Updated" SortExpression="last_updated_date">
                        <ItemTemplate>
                            <asp:Label ID="lblLastUpdatedOn" runat="server" Text='<%# bind("last_updated_date") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="category" HeaderText="Category" SortExpression="category" />
                    <asp:BoundField HeaderText="ETC" />
                    <asp:BoundField HeaderText="Time to date" />
                    <asp:BoundField HeaderText="Balance" />
                    <asp:BoundField HeaderText="Reported by" SortExpression="reported_user" Visible="False"
                        DataField="reported_user" />
                    <asp:TemplateField HeaderText="Reported on" SortExpression="reported_date" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lblReportedOn" runat="server" Text='<%# Bind("reported_date") %>'></asp:Label>
                            <asp:HiddenField ID="hd" runat="server" Value='<%# Eval("ETC")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="ETC" HeaderText="ETC" Visible="False" />
                    <asp:BoundField DataField="task_id" HeaderText="ID" SortExpression="task_id" />
                </Columns>
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Top" BackColor="#282C5F"
                    CssClass="tblTitle1" />
            </asp:GridView>
        </div>
        <uc1:footer ID="Footer1" runat="server" />
    </form>
</body>
</html>
