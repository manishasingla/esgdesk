﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;

public partial class Admin_Priority_reports20June : System.Web.UI.Page
{
    StringBuilder sql = new StringBuilder();
    string newsql;
    string assigned_user = "";
    bool blnadmin;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["admin"] == null || Session["admin"].ToString() == "")
            {
                Response.Redirect("login.aspx");
                return;
            }
            blnadmin = DatabaseHelper.isAdmin(Session["admin"].ToString());
            if (!blnadmin)
            {
                dvemp.Visible = true;
                dvadmin.Visible = false;
            }
            else
            {
                dvemp.Visible = false;
                dvadmin.Visible = true;
            }
            if (!IsPostBack)
            {
                assigned_user = Session["admin"].ToString();
                loaddropdown();
                loadtask();
                //=====================================================================================
                DataSet ds = new DataSet();
                string sql = @"select * from tasks where  status <> 'closed' and status <> 'Checked' and status <>'parked' and  deleted <> 1  and assigned_to_user = '" + assigned_user + "'   ORDER BY last_updated_date desc ";
                BindData(sql);
                //=========================================================================================
            }
        }
        catch (Exception ex)
        {

        }

    }

    private void loadtask()
    {
        try
        {
            sql.Append(@"select tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '1a - DO NOW' ");

            if (!blnadmin)
            {
                sql.Append(" and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + assigned_user + "'; ");
            }
            else
            {
                sql.Append(" and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + assigned_user + "'; ");
            }

            sql.Append(@"select tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '0 - IMMEDIATE' ");

            if (!blnadmin)
            {
                sql.Append(" and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + assigned_user + "'; ");
            }
            else
            {
                sql.Append(" and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + assigned_user + "'; ");
            }
            sql.Append(@"select tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '1b - high' ");

            if (!blnadmin)
            {
                sql.Append(" and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + assigned_user + "'; ");
            }
            else
            {
                sql.Append(" and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + assigned_user + "'; ");
            }


            sql.Append(@"select tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '1c - normal' ");
            if (!blnadmin)
            {
                sql.Append(" and  status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + assigned_user + "'; ");
            }
            else
            {
                sql.Append(" and  status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + assigned_user + "'; ");
            }

            //        sql.Append(@"select tasks.* from tasks 
            //                where tasks.deleted <> 1 and tasks.priority = '1c - normal' ");
            //        if (!blnadmin)
            //        {
            //            sql.Append(" and  status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + assigned_user + "'; ");
            //        }
            //        else
            //        {
            //            sql.Append(" and  status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + assigned_user + "'; ");
            //        }

            sql.Append(@"select tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '2 - not urgent' ");
            if (!blnadmin)
            {
                sql.Append(" and  status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + assigned_user + "'; ");
            }
            else
            {
                sql.Append(" and  status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + assigned_user + "'; ");
            }

            sql.Append(@"select tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '3 - low priority' ");
            if (!blnadmin)
            {
                sql.Append(" and  status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + assigned_user + "'; ");
            }
            else
            {
                sql.Append(" and  status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + assigned_user + "'; ");
            }

            sql.Append(@"select tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '4 - very low priority' ");
            if (!blnadmin)
            {
                sql.Append(" and  status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + assigned_user + "'; ");
            }
            else
            {
                sql.Append(" and  status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + assigned_user + "'; ");
            }

            sql.Append(@"select tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '5 - parked' ");
            if (!blnadmin)
            {
                sql.Append(" and  status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + assigned_user + "'; ");
            }
            else
            {
                sql.Append(" and  status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + assigned_user + "'; ");
            }

            DataSet ds = DatabaseHelper.getDataset(sql.ToString());

            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {

                //======================================================================================
                //lbl_1adonow.Text = "(" + ds.Tables[0].Rows.Count + ")Tasks with 1a DO NOW priority ";
                //lbl_1adonow.Visible = true;
                //lbl_1adonow.Style.Add(HtmlTextWriterStyle.Color, "RED");

                lnkHighestTasks.Text = "(" + ds.Tables[0].Rows.Count + ")";
                lnkHighestTasks.Style.Add(HtmlTextWriterStyle.Color, "RED");  
                lbl_1adonow.Text = "Tasks with 1a DO NOW priority";
                lbl_1adonow.Visible = true;
                lbl_1adonow.Style.Add(HtmlTextWriterStyle.Color, "RED");
                //=======================================================================================
            }
            else
            {
                
                lbl_1adonow.Text = "(0) Tasks with 1a DO NOW priority";
                lbl_1adonow.Style.Add(HtmlTextWriterStyle.Color, "BLACK");
                //if (!blnadmin)
                //{
                //    lbl_1adonow.Visible = true;
                //}
                //else
                //{
                //    lbl_1adonow.Visible = false;
                //}
            }

            if (ds != null && ds.Tables[1].Rows.Count > 0)
            {
                //=========================================================================================
                //lbl_Immediate.Text = "(" + ds.Tables[1].Rows.Count + ") Tasks with 0 Immediate priority";
                //lbl_Immediate.Visible = true;
                //lbl_Immediate.Style.Add(HtmlTextWriterStyle.Color, "RED");
                lnkPr0Task.Text = "(" + ds.Tables[1].Rows.Count + ")";
                lnkPr0Task.Style.Add(HtmlTextWriterStyle.Color, "#FF1493");  
                lbl_Immediate.Text = "Tasks with 0 Immediate priority";
                lbl_Immediate.Visible = true;
                lbl_Immediate.Style.Add(HtmlTextWriterStyle.Color, "#FF1493");
                //===========================================================================================
            }
            else
            {   
                lbl_Immediate.Text = "(0) Tasks with 0 Immediate priority";
                //lbl_Immediate.Visible = false;
                lbl_Immediate.Style.Add(HtmlTextWriterStyle.Color, "BLACK");
            }
            if (ds != null && ds.Tables[2].Rows.Count > 0)
            {
                //========================================================================================
                //lbl_1bhigh.Text = "(" + ds.Tables[2].Rows.Count + ") Tasks with 1b - high priority";
                //lbl_1bhigh.Visible = true;
                //lbl_1bhigh.Style.Add(HtmlTextWriterStyle.Color, "RED");
                lnkHighTasks.Text = "(" + ds.Tables[2].Rows.Count + ")";
                lnkHighTasks.Style.Add(HtmlTextWriterStyle.Color, "GREEN");  
                lbl_1bhigh.Text = "Tasks with 1b - high priority";
                lbl_1bhigh.Visible = true;
                lbl_1bhigh.Style.Add(HtmlTextWriterStyle.Color, "GREEN");
                //========================================================================================
            }
            else
            {
                
                lbl_1bhigh.Text = "(0) Tasks with 1b - high priority";
                // highTasks.Visible = false;
                lbl_1bhigh.Style.Add(HtmlTextWriterStyle.Color, "BLACK");
            }

            if (ds != null && ds.Tables[3].Rows.Count > 0)
            {
                //================================================================================
                //lbl_1cnormal.Text = "(" + ds.Tables[3].Rows.Count + ") Tasks with 1c - normal priority";
                //lbl_1cnormal.Visible = true;
                //lbl_1cnormal.Style.Add(HtmlTextWriterStyle.Color, "RED");
                Lnk1cTasks.Text = "(" + ds.Tables[3].Rows.Count + ")";
                Lnk1cTasks.Style.Add(HtmlTextWriterStyle.Color, "#FF1493");  
                lbl_1cnormal.Text = "Tasks with 1c - normal priority";
                lbl_1cnormal.Visible = true;
                lbl_1cnormal.Style.Add(HtmlTextWriterStyle.Color, "#FF1493");
                //===================================================================================
            }
            else
            {
                lbl_1cnormal.Text = "(0) Tasks with 1c - normal priority";
                lbl_1cnormal.Style.Add(HtmlTextWriterStyle.Color, "BLACK");
            }

            if (ds != null && ds.Tables[4].Rows.Count > 0)
            {
                //===========================================================================================
                //lbl_2noturgent.Text = "(" + ds.Tables[4].Rows.Count + ") Tasks with 2 - not urgent priority";
                //lbl_2noturgent.Visible = true;
                //lbl_2noturgent.Style.Add(HtmlTextWriterStyle.Color, "RED");
                Link2notTasks.Text = "(" + ds.Tables[4].Rows.Count + ")";
                lbl_2noturgent.Text = "Tasks with 2 - not urgent priority";
                lbl_2noturgent.Visible = true;
                lbl_2noturgent.Style.Add(HtmlTextWriterStyle.Color, "RED");
                //==============================================================================================

            }
            else
            {
                
                lbl_2noturgent.Text = "(0)Tasks with 2 - not urgent priority";
                lbl_2noturgent.Style.Add(HtmlTextWriterStyle.Color, "BLACK");
            }

            if (ds != null && ds.Tables[5].Rows.Count > 0)
            {
                //===============================================================================
                //lbl_3low.Text = "(" + ds.Tables[5].Rows.Count + ") Tasks with 3 - low priority";
                //lbl_3low.Visible = true;
                //lbl_3low.Style.Add(HtmlTextWriterStyle.Color, "RED");
                Link3lowTasks.Text = "(" + ds.Tables[5].Rows.Count + ")";
                Link3lowTasks.Style.Add(HtmlTextWriterStyle.Color, "GREEN");    
                lbl_3low.Text = "Tasks with 3 - low priority";
                lbl_3low.Visible = true;
                lbl_3low.Style.Add(HtmlTextWriterStyle.Color, "GREEN");
                //================================================================================
            }
            else
            {
                
                lbl_3low.Text = "(0) Tasks with 3 - low priority";
                lbl_3low.Style.Add(HtmlTextWriterStyle.Color, "BLACK");

            }

            if (ds != null && ds.Tables[6].Rows.Count > 0)
            {
                //======================================================================================
                //lbl4verylow.Text = "(" + ds.Tables[6].Rows.Count + ") Tasks with 4 - very low priority";
                //lbl4verylow.Visible = true;
                //lbl4verylow.Style.Add(HtmlTextWriterStyle.Color, "RED");
                Link4lowTasks.Text = "(" + ds.Tables[6].Rows.Count + ")";
                Link4lowTasks.Style.Add(HtmlTextWriterStyle.Color, "#FF1493");  
                lbl4verylow.Text = "Tasks with 4 - very low priority";
                lbl4verylow.Visible = true;
                lbl4verylow.Style.Add(HtmlTextWriterStyle.Color, "#FF1493");
                //=======================================================================================
            }
            else
            {
                
                lbl4verylow.Text = "(0) Tasks with 4 - very low priority";
                lbl4verylow.Style.Add(HtmlTextWriterStyle.Color, "BLACK");
            }

            if (ds != null && ds.Tables[7].Rows.Count > 0)
            {
                //====================================================================================
                //lbl5parked.Text = "(" + ds.Tables[7].Rows.Count + ") Tasks with 5 - parked priority";
                //lbl5parked.Visible = true;
                //lbl5parked.Style.Add(HtmlTextWriterStyle.Color, "RED");
                Link5parkedTasks.Text = "(" + ds.Tables[7].Rows.Count + ")";
                lbl5parked.Text = "Tasks with 5 - parked priority";
                lbl5parked.Visible = true;
                lbl5parked.Style.Add(HtmlTextWriterStyle.Color, "RED");
                //====================================================================================
            }
            else
            {
                
                lbl5parked.Text = "(0) Tasks with 5 - parked priority";
                lbl5parked.Style.Add(HtmlTextWriterStyle.Color, "BLACK");

            }
        }
        catch (Exception ex)
        {
        }
    }

    private void loaddropdown()
    { 
        //=========Bind drop down list by User Name========================================

        try
        {
            string sql1 = "select username from users where active = 'Y' order by username";

            DataSet ds_dropdowns = DatabaseHelper.getDataset(sql1);

            drpAssignedTo.DataSource = ds_dropdowns.Tables[0];
            drpAssignedTo.DataTextField = "username";
            drpAssignedTo.DataValueField = "username";
            drpAssignedTo.DataBind();
            string strfu = assigned_user.ToUpper().ToCharArray()[0].ToString();
            string strlu = assigned_user;
            strlu = strlu.Remove(0, 1);

            drpAssignedTo.SelectedValue = strfu + strlu;



            //////  string strc = assigned_user.ToUpper().ToCharArray()[0].ToString();
            //////  string stru=assigned_user;
            ////// stru= stru.Remove(0,1);
            //////  //drpAssignedTo.SelectedValue.Equals(assigned_user);
            //////  //drpAssignedTo.SelectedItem.Value=assigned_user;
            //////  //drpAssignedTo.Items.FindByText(assigned_user).Selected = true;
            //////  //drpAssignedTo.Text = assigned_user;
            //////  //drpAssignedTo.SelectedItem.Value.Equals(assigned_user);
            ////////  drpAssignedTo.Text = assigned_user;
            //////  //drpAssignedTo.SelectedValue = assigned_user;
            ////////  drpAssignedTo.SelectedItem.Value = strc + stru;
            ////// //drpAssignedTo.Items.FindByText(strc + stru).Selected = true;
            ////// drpAssignedTo.SelectedItem.Value = strc + stru;
        }
        catch (Exception ex)
        {
        }
    }
    protected void drpAssignedTo_SelectedIndexChanged(object sender, EventArgs e)
    {
        //==============show selected user======================================
        try
        {
            lnkPr0Task.Text = "";
            lnkHighestTasks.Text = "";
            lnkHighTasks.Text = "";
            Lnk1cTasks.Text = "";
            Link2notTasks.Text = "";
            Link3lowTasks.Text = "";
            Link4lowTasks.Text = "";
            Link5parkedTasks.Text = "";
            assigned_user = drpAssignedTo.SelectedItem.Text;
            DataGrid1.DataSource = null;
            DataGrid1.DataBind();
            string sqlforall = @"select * from tasks where  status <> 'closed' and status <> 'Checked' and status <>'parked' and  deleted <> 1  and assigned_to_user = '" + assigned_user + "'   ORDER BY last_updated_date desc ";
            BindData(sqlforall);
            loadtask();
        }
        catch (Exception ex)
        {
        }
    }

    protected void lnkPr0Task_Click(object sender, EventArgs e)
    {
        //==============Show 0-Immediate level task=============
        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            assigned_user = Session["admin"].ToString();
        }
        else
        {
            assigned_user = drpAssignedTo.SelectedItem.Text;
        }
        DataSet ds = new DataSet();
        string sql = @"select * from tasks where ";
        sql += "status <> 'closed' and status <> 'Checked' and status <>'parked' and   deleted <> 1  and ";
        sql += "assigned_to_user ='" + assigned_user.ToString() + "'  and priority = '0 - IMMEDIATE'   ORDER BY last_updated_date desc";
        BindData(sql);


    }
    protected void lnkHighestTasks_Click(object sender, EventArgs e)
    {
        //=========show 1a-do now level task========================
        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            assigned_user = Session["admin"].ToString();
        }
        else
        {
            assigned_user = drpAssignedTo.SelectedItem.Text;
        }
        DataSet ds = new DataSet();
        string sql = @"select * from tasks where";
        sql += " status <> 'closed' and status <> 'Checked' and status <>'parked' and   deleted <> 1  and";
        sql += " assigned_to_user = '" + assigned_user.ToString() + "'  and priority = '1a - DO NOW'   ORDER BY last_updated_date desc";
        BindData(sql);
    }
    protected void lnkHighTasks_Click(object sender, EventArgs e)
    {
        //=========show 1b-high level task========================
        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            assigned_user = Session["admin"].ToString();
        }
        else
        {
            assigned_user = drpAssignedTo.SelectedItem.Text;
        }
        DataSet ds = new DataSet();
        string sql = @"select * from tasks where";
        sql += " status <> 'closed' and status <> 'Checked' and status <>'parked' and   deleted <> 1  and";
        sql += " assigned_to_user = '" + assigned_user.ToString() + "'  and priority = '1b - high'   ORDER BY last_updated_date desc";
        BindData(sql);
    }
    protected void Lnk1cTasks_Click(object sender, EventArgs e)
    {
        //==========Show 1-c level task==========================
        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            assigned_user = Session["admin"].ToString();
        }
        else
        {
            assigned_user = drpAssignedTo.SelectedItem.Text;
        }
        DataSet ds = new DataSet();
        string sql = @"select * from tasks where";
        sql += " status <> 'closed' and status <> 'Checked' and status <>'parked' and   deleted <> 1  and";
        sql += " assigned_to_user = '" + assigned_user.ToString() + "'  and priority = '1c - normal'   ORDER BY last_updated_date desc";
        BindData(sql);
    }
    protected void Link2notTasks_Click(object sender, EventArgs e)
    {
        //===============Show 2-Not level task=================== 
        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            assigned_user = Session["admin"].ToString();
        }
        else
        {
            assigned_user = drpAssignedTo.SelectedItem.Text;
        }
        DataSet ds = new DataSet();
        string sql = @"select * from tasks where";
        sql += " status <> 'closed' and status <> 'Checked' and status <>'parked' and   deleted <> 1  and";
        sql += " assigned_to_user = '" + assigned_user.ToString() + "'  and priority = '2 - not urgent'   ORDER BY last_updated_date desc";
        BindData(sql);
    }
    protected void Link5parkedTasks_Click(object sender, EventArgs e)
    {
        //=========Show 5-Parked level task=====================
        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            assigned_user = Session["admin"].ToString();
        }
        else
        {
            assigned_user = drpAssignedTo.SelectedItem.Text;
        }
        DataSet ds = new DataSet();
        string sql = @"select * from tasks where";
        sql += " status <> 'closed' and status <> 'Checked' and status <>'parked' and   deleted <> 1  and";
        sql += " assigned_to_user = '" + assigned_user.ToString() + "'  and priority = '5 - parked'   ORDER BY last_updated_date desc";
        BindData(sql);
    }
    private void BindData(string strsql)
    {
        DataSet ds = DatabaseHelper.getDataset(strsql);
        ds = formatTaskDataSet(ds);
        PagedDataSource objds = new PagedDataSource();
        objds.DataSource = ds.Tables[0].DefaultView;

        DataGrid1.DataSource = null;
        DataGrid1.DataBind();

        DataGrid1.DataSource = objds;
        DataGrid1.DataBind();
    }

    private DataSet formatTaskDataSet(DataSet ds)
    {
        try
        {

            ds.Tables[0].Columns.Add("ShowAllComment");
            /// ds.Tables[0].Columns.Add("ItemPriority", typeof(string));

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {

                    if (ds.Tables[0].Rows[i]["short_desc"] != null && ds.Tables[0].Rows[i]["short_desc"].ToString().Trim() != "")
                    {
                        ds.Tables[0].Rows[i]["short_desc"] = "<a  target=\"_blank\"  href=\"edit_task.aspx?id=" + ds.Tables[0].Rows[i]["task_id"].ToString() + "  \">" + ds.Tables[0].Rows[i]["short_desc"].ToString() + "</a>";

                    }
                    else
                    {
                        ds.Tables[0].Rows[i]["short_desc"] = "";

                    }
                    try
                    {
                        string LastComment = "";

                        newsql = @"select top 1.* from task_comments where task_comments.deleted <> 1  and ";
                        newsql += " task_id =" + ds.Tables[0].Rows[i]["task_id"].ToString() + " order by tc_id desc";
                        //testing by rohit
                        DataSet dslastcomment = DatabaseHelper.getDataset(newsql);

                        if (dslastcomment != null && dslastcomment.Tables[0].Rows.Count > 0)
                        {

                            // for (int j = 0; j < dslastcomment.Tables[0].Rows.Count; j++)
                            for (int j = 0; j < 1; j++)
                            {

                                if (dslastcomment.Tables[0].Rows[j]["qflag"].ToString() == "1")
                                {

                                    LastComment += "<span style='color:Red'>" + "Question posted by " + dslastcomment.Tables[0].Rows[j]["username"].ToString() + " for  " + dslastcomment.Tables[0].Rows[j]["QuesTo"].ToString() + " on " + DateTime.Parse(dslastcomment.Tables[0].Rows[j]["post_date"].ToString()).ToString("dd MMM yyyy h:mm tt") + "</span>";

                                    LastComment += "<br/><span>" + dslastcomment.Tables[0].Rows[j]["comment"].ToString() + "</span>";

                                    LastComment += "<span style=\"border-bottom: solid 1px green; margin-bottom: 2px; height:1px\"></span><br/><hr />";

                                }
                                else
                                {

                                    LastComment += "<span style='color:Green'>" + "Comment " + dslastcomment.Tables[0].Rows[j]["tc_id"].ToString() + " posted by " + dslastcomment.Tables[0].Rows[j]["username"].ToString() + " on " + DateTime.Parse(dslastcomment.Tables[0].Rows[j]["post_date"].ToString()).ToString("dd MMM yyyy h:mm tt") + "</span>";
                                    LastComment += "<br/><span>" + dslastcomment.Tables[0].Rows[j]["comment"].ToString() + "</span>";

                                    LastComment += "<span style=\"border-bottom: solid 1px green; margin-bottom: 2px; height:1px\"></span><br/><hr />";

                                }

                            }
                        }

                        ds.Tables[0].Rows[i]["ShowAllComment"] = LastComment.ToString();
                    }
                    catch
                    {

                    }

                }
                catch { }
            }
        }
        catch
        {
            Server.ClearError();
            ///Response.Redirect("fhs.aspx");
        }
        return ds;
    }

    protected void Link3lowTasks_Click1(object sender, EventArgs e)
    {
        //============Show 3-low level task=====================
        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            assigned_user = Session["admin"].ToString();
        }
        else
        {
            assigned_user = drpAssignedTo.SelectedItem.Text;
        }
        DataSet ds = new DataSet();
        string sql = @"select * from tasks where";
        sql += " status <> 'closed' and status <> 'Checked' and status <>'parked' and   deleted <> 1  and";
        sql += " assigned_to_user = '" + assigned_user.ToString() + "'  and priority = '3 - low priority'   ORDER BY last_updated_date desc";
        BindData(sql);
    }

    protected void Link4lowTasks_Click(object sender, EventArgs e)
    {
        //==============Show 4-low level task ==================
        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            assigned_user = Session["admin"].ToString();
        }
        else
        {
            assigned_user = drpAssignedTo.SelectedItem.Text;
        }
        DataSet ds = new DataSet();
        string sql = @"select * from tasks where";
        sql += " status <> 'closed' and status <> 'Checked' and status <>'parked' and   deleted <> 1  and";
        sql += " assigned_to_user = '" + assigned_user.ToString() + "'  and priority = '4 - very low priority'   ORDER BY last_updated_date desc";
        BindData(sql);
    }

    protected void DataGrid1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.DataItemIndex != -1)
        {
            //==================set the colour of row According to there status ===========================
            string htmlColor = (string)DatabaseHelper.executeScalar("select bg_color from priorities where priority_name='" + e.Row.Cells[7].Text + "'");
            e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml(htmlColor);
            //=====================only show read And unread ==============================================
            LinkButton readMarkButton = (LinkButton)e.Row.FindControl("lnkReadMark");
            //=========================================================================================================
            int ETC = Convert.ToInt32(((HiddenField)e.Row.Cells[16].Controls[3]).Value);

            e.Row.Cells[12].Text = "" + (ETC / 60) + "' " + (ETC % 60) + '"';

            int hrsTaken = getHoursTakenSoFar(e.Row.Cells[0].Text);

            e.Row.Cells[13].Text = "" + (hrsTaken / 60) + "' " + (hrsTaken % 60) + '"';

            int hrsLeft = ETC - hrsTaken;

            if (hrsLeft < 0)
            {
                hrsLeft = -1 * hrsLeft;
                e.Row.Cells[13].Text = "-" + (hrsLeft / 60) + "' " + (hrsLeft % 60) + '"';
            }
            else
            {
                e.Row.Cells[14].Text = "" + (hrsLeft / 60) + "' " + (hrsLeft % 60) + '"';
            }
            //=========================================================================================================
            int objIsRead;
            try
            {
                string strReadUnread;
                strReadUnread = "select * from task_comments where tc_id not in(select tc_id from read_comments where username ='" + Session["admin"] + "')and task_comments.deleted <> 1 and task_id =" + e.Row.Cells[18].Text;
                DataSet ds = DatabaseHelper.getDataset(strReadUnread);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    readMarkButton.Text = "[<span style='color:red'>Unread.</span>  Mark as read]";
                }
                else
                {
                    readMarkButton.Text = "[<span style='color:green'>Read.</span> Mark as unread]";
                }
            }
            catch
            {

            }
        }
    }

    protected int getHoursTakenSoFar(string task_id)
    {
        string sqlqry;
        sqlqry = "select sum(datediff(minute,started_date,finished_date)) from hours_reporting where finished_date is not null and task_id=" + task_id;

        int hrsTaken = 0;
        object objHrsTaken = DatabaseHelper.executeScalar(sqlqry);
        if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
        {
            hrsTaken += (int)objHrsTaken;
        }
        else
        {
            hrsTaken = 0;
        }
        string qryfornew;
        qryfornew = "select sum(datediff(minute,started_date,getdate())) from hours_reporting where finished_date is null and task_id=" + task_id;

        objHrsTaken = DatabaseHelper.executeScalar(qryfornew);

        if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
        {
            hrsTaken += (int)objHrsTaken;
        }

        return hrsTaken;
    }
    protected void DataGrid1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
    }


}
