﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Admin_ProjectNoteList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Response.Redirect("login.aspx");
            return;
        }
        
        if (HttpUtility.UrlDecode(Request.QueryString["projectname"]) != null && Request.QueryString["folderid"] != null)
        {
            try
            {
                getcategory(HttpUtility.UrlDecode(Request.QueryString["projectname"].ToString()), Request.QueryString["folderid"].ToString());
                getcategory();
            }
            catch (Exception ex)
            {
                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at Query string 21: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Sql Query :->" + Request.QueryString["projectname"].ToString());
            }

        }
        else if (HttpUtility.UrlDecode(Request.QueryString["projectname"]) != null)
        {
            try
            {
                getcategory(HttpUtility.UrlDecode(Request.QueryString["projectname"].ToString()));
                getcategory();
            }
            catch (Exception ex)
            {
             DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at Query string 22: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Sql Query :->" + Request.QueryString["projectname"].ToString());
            }
        }
        else
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Blank " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"),"" );  
        }
    }

    void GetsolutionCategory()
    {

        string sqlcategory = @"select  distinct category_name from Solution_categories order by category_name";
        DataSet dscategory = null;
        try
        {
             dscategory = DatabaseHelper.getallstatus(sqlcategory);
        }catch(Exception ex)
        {
          DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectNoteList.aspx:1 " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "GetsolutionCategory()"+ sqlcategory);
        }
        if (dscategory != null)
        {
            if (dscategory.Tables.Count > 0 && dscategory.Tables[0].Rows.Count > 0)
            {
                lstcategory.DataSource = dscategory.Tables[0];
                lstcategory.DataBind();
            }

        }

    }


    void getcategory()
    {

        DataSet dscount = new DataSet();
        DataSet dssub = new DataSet();
        string sqlsub = "";


        string sqlcount = @"select category_id,category_name from Solution_categories order by category_id";
        try
        {
            dscount = DatabaseHelper.getallstatus(sqlcount);
        }catch(Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectNoteList.aspx:1 " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "getcategory()" + sqlcount);
        }

        if (dscount != null)
        {
            if (dscount.Tables.Count > 0 && dscount.Tables[0].Rows.Count > 0)
            {

                sqlsub = @"select  Solution_categories.category_id,Solution_categories.category_name,COUNT(*)as counts  from Solution_categories inner join Solution_categoriesnotes on Solution_categories.category_id = Solution_categoriesnotes.Folder_Id
  group by Solution_categories.category_id,Solution_categories.category_name  
   order by category_id ";

                try
                {
                    dssub = DatabaseHelper.getallstatus(sqlsub);
                }
                catch (Exception ex)
                {
                    DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectNoteList.aspx:2 " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "getcategory()" + sqlsub);
                }
                if (dscount.Tables[0].Rows.Count == dssub.Tables[0].Rows.Count)
                {

                    if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0)
                    {

                        lstcategory.DataSource = dssub.Tables[0];
                        lstcategory.DataBind();


                    }
                }
                else
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("category_id");
                    dt.Columns.Add("category_name");
                    dt.Columns.Add("counts");
                    DataSet a = new DataSet();
                    for (int j = 0; j < dscount.Tables[0].Rows.Count; j++)
                    {

                        sqlsub = @"select Solution_categories.category_id,Solution_categories.category_name,COUNT(*)as counts  
 from Solution_categories inner join Solution_categoriesnotes on Solution_categories.category_id = Solution_categoriesnotes.folder_id
where Solution_categoriesnotes.folder_id  = " + dscount.Tables[0].Rows[j]["category_id"] +
 " group by Solution_categories.category_id,Solution_categories.category_name order by category_id   ";

                        try
                        {
                        dssub = DatabaseHelper.getallstatus(sqlsub);
                        }catch(Exception ex)
                        {
                            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectNoteList.aspx:3 " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "getcategory()" + sqlsub);
                        }
                        if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0)
                        {
                            DataRow dr = dssub.Tables[0].Rows[0];
                            dt.ImportRow(dssub.Tables[0].Rows[0]);
                        }
                        else
                        {
 
                            sqlsub = @"select category_id,category_name from Solution_categories where  category_id  = " + dscount.Tables[0].Rows[j]["category_id"];
                            try
                            {
                                dssub = DatabaseHelper.getallstatus(sqlsub);
                            }
                            catch(Exception ex)
                            {
                                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectNoteList.aspx:4 " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "getcategory()" + sqlsub);
                            }
                            
                            if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0)
                            {
                                DataColumn dc = new DataColumn("Counts");
                                dc.DefaultValue = 0;
                                dssub.Tables[0].Columns.Add(dc);
                                DataRow dr = dssub.Tables[0].Rows[0];

                                dt.ImportRow(dssub.Tables[0].Rows[0]);

                            }

                        }
                    }

                    lstcategory.DataSource = dt;
                    lstcategory.DataBind();



                }
            }
        }

    }

    void checkcategory(string projectname)
    {
        string sqlfoldername = @"select Project_Name from Project_Folder where IsProject='True' and  Project_Name = '" + projectname + "' ";
        object checkcategory=null;
        try
        {
            checkcategory = DatabaseHelper.executeScalar(sqlfoldername);
        }catch(Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectNoteList.aspx:1 " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "checkcategory()" + sqlfoldername);
        }
        if (checkcategory == null)
        {
            int result = DatabaseHelper.InsertDefaultprojectcategory(projectname);
        }
    }

    void getcategory(string category_id, string folder_id)
    {
        divaction.Visible = true;
        addcategory.HRef = "addProjectcategory.aspx?projectname=" + Server.UrlEncode(category_id);

        DataSet dscount = new DataSet();
        DataSet dssub = new DataSet();
        string sqlsub = "";

        //======================================================
        string categoryDesc = "";
        DataSet dsCatdesc = null;
        categoryDesc = @" select * from Project_Folder where IsProject='True' and Project_Name  = '" + category_id + "' and Folder_Id='" + folder_id + "'";
        try
        {
            dsCatdesc = DatabaseHelper.getDataset(categoryDesc);
        }
        catch(Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectNoteList.aspx:1 " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "getcategory()" + categoryDesc);
        }
        if (dsCatdesc != null && dsCatdesc.Tables.Count > 0 && dsCatdesc.Tables[0].Rows.Count > 0)
        {
            lblFN.Text = dsCatdesc.Tables[0].Rows[0]["FolderDescription"].ToString().Replace("|@", "'"); ;
        }
        //======================================================


        string sqlfoldername = @"select foldername from Project_Folder where IsProject='True' and Project_Name = '" + category_id + "'  and folder_id  =" + folder_id;
        object FN = null;
        try
        {
            FN = DatabaseHelper.executeScalar(sqlfoldername);
        }catch(Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectNoteList.aspx:2 " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "getcategory()" + sqlfoldername);
        }
        
        
        if (FN != null)
        {
            lblPfoldername.Text = FN.ToString();
        }

        string sqlcount = @"select foldername,folder_id,Project_Name from Project_Folder where IsProject='True' and Project_Name = '" + category_id + "' order by Folder_Id";
        try
        {
            dscount = DatabaseHelper.getallstatus(sqlcount);
        }catch(Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectNoteList.aspx:3 " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "getcategory()" + sqlcount);
        }

        
        if (dscount != null)
        {
            if (dscount.Tables.Count > 0 && dscount.Tables[0].Rows.Count > 0)
            {
                addsolu.HRef = "AddProject.aspx?projectname=" + Server.UrlEncode(category_id) + "&folderid=" + folder_id;
                btnaddsolution.HRef = "AddProject.aspx?projectname=" + Server.UrlEncode(category_id) + "&folderid=" + folder_id;
                editfolder.HRef = "addProjectcategory.aspx?projectname=" + Server.UrlEncode(category_id) + "&folderid=" + folder_id;

                sqlsub = @"select  Project_Folder.Foldername,Project_Folder.folder_id,Project_Folder.Project_Name,COUNT(*)as counts  from Project_Folder inner join ProjectNotes on Project_Folder.folder_id = ProjectNotes.folder_id
 where Project_Folder.IsProject='True' and ProjectNotes.IsProject='True' and ProjectNotes.Project_Name  = '" + category_id + "' and ProjectNotes.folder_id  =" + folder_id + " group by Project_Folder.folder_id,Project_Folder.Foldername ,Project_Folder.Project_Name order by Folder_Id ; select Row_Number()OVER(order by Note_Id) as RowNumber,* from ProjectNotes where folder_id= " + folder_id;
                try
                {

                    dssub = DatabaseHelper.getallstatus(sqlsub);
                }
                catch (Exception ex)
                {
                    DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectNoteList.aspx:4 " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "getcategory()" + sqlsub);
                }
                if (dscount.Tables[0].Rows.Count == dssub.Tables[0].Rows.Count)
                {


                    if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0)
                    {

                        //======================================================================
                        try
                        {
                            string DsAddstatus = "", Dsallstatus = "";
                            DataSet NewDSfol = null, NewDSalstatus = null;
                            dssub.Tables[0].Columns.Add("AddCategoryStatus");

                            DsAddstatus = "Select * from NotesStatus where IsProject='True' and Folder_Id='" + Convert.ToInt32(dssub.Tables[0].Rows[0]["Folder_Id"].ToString()) + "'  and Project_Name='" + dssub.Tables[0].Rows[0]["Project_Name"].ToString() + "' and UserName='" + Session["admin"] + "'";
                            NewDSfol = DatabaseHelper.getDataset(DsAddstatus);

                            Dsallstatus = "Select * from NotesStatus where IsProject='True' and Folder_Id='" + Convert.ToInt32(dssub.Tables[0].Rows[0]["Folder_Id"].ToString()) + "'  and Project_Name='" + dssub.Tables[0].Rows[0]["Project_Name"].ToString() + "' and UserName='" + Session["admin"] + "' and NoteStatus='True'";
                            NewDSalstatus = DatabaseHelper.getDataset(Dsallstatus);
                            if ((NewDSfol.Tables[0].Rows.Count == NewDSalstatus.Tables[0].Rows.Count))
                            {
                                if (NewDSalstatus.Tables[0].Rows.Count >= 1)
                                {
                                    dssub.Tables[0].Rows[0]["AddCategoryStatus"] = Convert.ToBoolean(true);
                                }
                                else
                                {
                                    dssub.Tables[0].Rows[0]["AddCategoryStatus"] = Convert.ToBoolean(false);
                                }
                            }
                            else
                            {
                                dssub.Tables[0].Rows[0]["AddCategoryStatus"] = Convert.ToBoolean(false);
                            }
                        }
                        catch (Exception ex)
                        {
                            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectNoteList.aspx:Only one Folder in Project " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "getcategory()" + sqlsub);
                        }
                        //======================================================================
                        //*********************************************************************
                        for (int changeI = 0; changeI < dssub.Tables[0].Rows.Count; changeI++)
                        {
                            dssub.Tables[0].Rows[changeI]["Foldername"] = dssub.Tables[0].Rows[changeI]["Foldername"].ToString().Replace("|@", "'");
                        }
                        //*********************************************************************


                        Folderlstview.DataSource = dssub.Tables[0];
                        Folderlstview.DataBind();
                        if (dssub.Tables.Count == 2)
                        {
                            if (dssub.Tables[1].Rows.Count > 0)
                            {
                                //==========================================
                                dssub = setuserstatus(dssub);
                                //===========================================
                                //********************************************
                                for (int Ik = 0; Ik < dssub.Tables[1].Rows.Count; Ik++)
                                {
                                    dssub.Tables[1].Rows[Ik]["NoteName"] = dssub.Tables[1].Rows[Ik]["NoteName"].ToString().Replace("|@", "'");
                                }
                                //********************************************
                                gridlist.DataSource = dssub.Tables[1];
                                gridlist.DataBind();
                            }
                            else
                            {
                                gridlist.Visible = false;
                                addsolution.Visible = true;
                            }
                        }

                    }
                }
                else
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Foldername");
                    dt.Columns.Add("folder_id");
                    dt.Columns.Add("Project_Name");
                    dt.Columns.Add("counts");
                    DataSet a = new DataSet();
                    for (int j = 0; j < dscount.Tables[0].Rows.Count; j++)
                    {
                        
                        sqlsub = @"select  Project_Folder.Foldername,Project_Folder.folder_id, Project_Folder.Project_Name,COUNT(*)as counts  from Project_Folder inner join ProjectNotes on Project_Folder.folder_id = ProjectNotes.folder_id
 where Project_Folder.IsProject='True' and ProjectNotes.IsProject='True' and ProjectNotes.Project_Name  = '" + dscount.Tables[0].Rows[j]["Project_Name"] + "' and ProjectNotes.folder_id =" + dscount.Tables[0].Rows[j]["folder_id"] + " group by Project_Folder.folder_id,Project_Folder.Foldername ,Project_Folder.Project_Name order by Folder_Id  ;  select Row_Number()OVER(order by Note_Id) as RowNumber,* from ProjectNotes where folder_id= " + folder_id;
                        try
                        {
                            dssub = DatabaseHelper.getallstatus(sqlsub);
                        }catch(Exception ex)
                        {
                            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectNoteList.aspx:5 " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "getcategory()" + sqlsub);
                        }
                        if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0)
                        {


                            DataRow dr = dssub.Tables[0].Rows[0];

                            dt.ImportRow(dssub.Tables[0].Rows[0]);
                            if (dssub.Tables.Count == 2)
                            {
                                if (dssub.Tables[1].Rows.Count > 0)
                                {
                                    //=============================================
                                    dssub = setuserstatus(dssub);
                                    //=============================================
                                    //*********************************************************************
                                    for (int cI = 0; cI < dssub.Tables[1].Rows.Count; cI++)
                                    {
                                        dssub.Tables[1].Rows[cI]["NoteName"] = dssub.Tables[1].Rows[cI]["NoteName"].ToString().Replace("|@", "'");
                                    }

                                    //*************************************************
                                    gridlist.DataSource = dssub.Tables[1];
                                    gridlist.DataBind();
                                }
                                else
                                {
                                    gridlist.Visible = false;
                                    addsolution.Visible = true;
                                }
                            }
                        }
                        else
                        {
                            
                            sqlsub = @"select foldername,folder_id,Project_Name from Project_Folder
                                 where IsProject='True' and Project_Name  = '" + dscount.Tables[0].Rows[j]["Project_Name"] + "' and folder_id =" + dscount.Tables[0].Rows[j]["folder_id"] + " ; select * from ProjectNotes where folder_id= " + dscount.Tables[0].Rows[j]["folder_id"] + " order by Folder_Id";
                            try
                            {
                                dssub = DatabaseHelper.getallstatus(sqlsub);
                            }catch(Exception ex)
                            {
                                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectNoteList.aspx:6 " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "getcategory()" + sqlsub);
                            }
                            
                            if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0)
                            {
                                DataColumn dc = new DataColumn("Counts");
                                dc.DefaultValue = 0;
                                dssub.Tables[0].Columns.Add(dc);
                                DataRow dr = dssub.Tables[0].Rows[0];
                                dt.ImportRow(dssub.Tables[0].Rows[0]);

                            }

                        }
                    }
                    //======================================
                    dt = setFolderStatusdt(dt);
                    //========================================
                    //**************************************************
                    for (int ik = 0; dt.Rows.Count > ik; ik++)
                    {
                        dt.Rows[ik]["Foldername"] = dt.Rows[ik]["Foldername"].ToString().Replace("|@", "'");
                    }
                    //***************************************************
                    Folderlstview.DataSource = dt;
                    Folderlstview.DataBind();
                    


                }
            }
        }
        
    }
    public DataSet setuserstatus(DataSet dsstatus)
    {
        try
        {
            string insertnotedata = "";
            string Addstatus = "";
            DataSet dsSetstatus = null;
            dsstatus.Tables[1].Columns.Add("AddNoteStatus");
            for (int ichk = 0; ichk < dsstatus.Tables[1].Rows.Count; ichk++)
            {
                Addstatus = "Select NoteStatus from NotesStatus where IsProject='True' and Note_Id='" + Convert.ToInt32(dsstatus.Tables[1].Rows[ichk]["Note_Id"].ToString()) + "' and Folder_Id='" + Convert.ToInt32(dsstatus.Tables[1].Rows[ichk]["Folder_Id"].ToString()) + "'  and Project_Name='" + dsstatus.Tables[1].Rows[ichk]["Project_Name"].ToString() + "' and UserName='" + Session["admin"] + "'";
                try
                {
                    dsSetstatus = DatabaseHelper.getDataset(Addstatus);
                }catch(Exception ex)
                {
                    DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectNoteList.aspx:1 " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "setuserstatus()" + Addstatus);
                }

                if (dsSetstatus != null && dsSetstatus.Tables.Count > 0 && dsSetstatus.Tables[0].Rows.Count > 0)
                {
                    dsstatus.Tables[1].Rows[ichk]["AddNoteStatus"] = dsSetstatus.Tables[0].Rows[0]["NoteStatus"];
                }
                else
                {
                    int insertval = 0;
                    insertnotedata = "insert into NotesStatus(Note_Id,Folder_Id,Project_Name,UserName,NoteStatus,IsProject)values('" + Convert.ToInt32(dsstatus.Tables[1].Rows[ichk]["Note_Id"].ToString()) + "','" + Convert.ToInt32(dsstatus.Tables[1].Rows[ichk]["Folder_Id"].ToString()) + "','" + dsstatus.Tables[1].Rows[ichk]["Project_Name"].ToString() + "','" + Session["admin"] + "','False','True')";
                    try
                    {
                        insertval = DatabaseHelper.executeNonQuery(insertnotedata);
                    }
                    catch (Exception ex)
                    {
                        DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectNoteList.aspx:1 " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "setuserstatus()" + insertnotedata);
                    }
                    dsstatus.Tables[1].Rows[ichk]["AddNoteStatus"] = Convert.ToBoolean(false);  
                }
            }
            return dsstatus;
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectNoteList.aspx:1 " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "setuserstatus" );
        }
        return dsstatus;
    }
    void getcategory(string category_id)
    {

        divaction.Visible = false;
        DataSet dscount = new DataSet();
        DataSet dssub = new DataSet();
        string sqlsub = "";
        btnaddsolution.HRef = "AddProject.aspx?projectname=" + Server.UrlEncode(category_id);
        addcategory.HRef = "addProjectcategory.aspx?projectname=" + Server.UrlEncode(category_id);
        string sqlcount = @" select foldername,folder_id,Project_Name from Project_Folder
                                 where Project_Name  = '" + category_id + "' order by Folder_Id";
        try
        {
            dscount = DatabaseHelper.getallstatus(sqlcount);
        }catch(Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectNoteList.aspx:1 " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "getcategory(string category_id)" + sqlcount);
        }

        sqlsub = @"select  Project_Folder.FolderName,Project_Folder.Folder_Id, Project_Folder.Project_Name,COUNT(*)as counts 
 from Project_Folder inner join ProjectNotes on Project_Folder.Folder_Id = ProjectNotes.Folder_Id
 where Project_Folder.IsProject='True' and ProjectNotes.IsProject='True' and Project_Folder.Project_Name  = '" + category_id + "' group by Project_Folder.Folder_Id,Project_Folder.FolderName ,Project_Folder.Project_Name order by Folder_Id";

        try
        {
            dssub = DatabaseHelper.getallstatus(sqlsub);
        }catch(Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectNoteList.aspx:2 " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "getcategory(string category_id)" + sqlsub);
        }
        if (dscount.Tables[0].Rows.Count == dssub.Tables[0].Rows.Count)
        {
            if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0)
            {
                //===================================================
                dssub = setFolderStatus(dssub);
                //===================================================
                //*********************************************************************
                for (int changeI = 0; changeI < dssub.Tables[0].Rows.Count; changeI++)
                {
                    dssub.Tables[0].Rows[changeI]["Foldername"] = dssub.Tables[0].Rows[changeI]["Foldername"].ToString().Replace("|@", "'");
                }
                //*********************************************************************
                Folderlstview.DataSource = dssub.Tables[0];
                Folderlstview.DataBind();
                gridlist.Visible = false;
               
            }

        }
        else
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Foldername");
            dt.Columns.Add("folder_id");
            dt.Columns.Add("Project_Name");
            dt.Columns.Add("counts");
            DataSet a = new DataSet();
            for (int j = 0; j < dscount.Tables[0].Rows.Count; j++)
            {

                sqlsub = @"select  COUNT(*)as counts,Project_Folder.Foldername,Project_Folder.folder_id, Project_Folder.Project_Name from Project_Folder inner join ProjectNotes on Project_Folder.folder_id = ProjectNotes.folder_id
 where Project_Folder.IsProject='True' and ProjectNotes.IsProject='True' and ProjectNotes.Project_Name ='" + dscount.Tables[0].Rows[j]["Project_Name"] + "' and ProjectNotes.folder_id  =" + dscount.Tables[0].Rows[j]["folder_id"] + " group by Project_Folder.folder_id,Project_Folder.Foldername ,Project_Folder.Project_Name  order by Folder_Id";
                try
                {
                    dssub = DatabaseHelper.getallstatus(sqlsub);
                }catch(Exception ex)
                {
                    DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectNoteList.aspx:3 " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "getcategory(string category_id)" + sqlsub);
                }
                if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0)
                {

                    DataRow dr = dssub.Tables[0].Rows[0];
                    dt.ImportRow(dssub.Tables[0].Rows[0]);

                }
                else
                {
                  
                    sqlsub = @" select foldername,folder_id,Project_Name from Project_Folder
where IsProject='True' and Project_Name = '" + dscount.Tables[0].Rows[j]["Project_Name"] + "' and folder_id =" + dscount.Tables[0].Rows[j]["folder_id"] + "order by Folder_Id";
                    try
                    {
                        dssub = DatabaseHelper.getallstatus(sqlsub);
                    }
                    catch (Exception ex)
                    {
                        DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectNoteList.aspx:4 " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "getcategory(string category_id)" + sqlsub);
                    }
                    if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0)
                    {
                        DataColumn dc = new DataColumn("Counts");
                        dc.DefaultValue = 0;
                        dssub.Tables[0].Columns.Add(dc);
                        DataRow dr = dssub.Tables[0].Rows[0];
                        dt.ImportRow(dssub.Tables[0].Rows[0]);
                    }

                }
            }
            //======================================
            dt = setFolderStatusdt(dt);
            //======================================
            //**************************************************
            for (int ik = 0; dt.Rows.Count > ik; ik++)
            {
                dt.Rows[ik]["Foldername"] = dt.Rows[ik]["Foldername"].ToString().Replace("|@", "'");
            }
            //***************************************************
            Folderlstview.DataSource = dt;
            Folderlstview.DataBind();
            gridlist.Visible = false;
            
        }
    }

    public DataTable setFolderStatusdt(DataTable dtFolderStatus)
    {
        try
        {
            string Addstatusdt = "", allstatusdt = "";
            DataTable dtFolderstatus = null, Allstatusdt = null;
            DataSet dsfol = null, dsalstatus = null;
            dtFolderStatus.Columns.Add("AddCategoryStatus");
            for (int ichk = 0; ichk < dtFolderStatus.Rows.Count; ichk++)
            {
                Addstatusdt = "Select * from NotesStatus where IsProject='True' and Folder_Id='" + Convert.ToInt32(dtFolderStatus.Rows[ichk]["Folder_Id"].ToString()) + "'  and Project_Name='" + dtFolderStatus.Rows[ichk]["Project_Name"].ToString() + "' and UserName='" + Session["admin"] + "'";
                dsfol = DatabaseHelper.getDataset(Addstatusdt);
                dtFolderstatus = dsfol.Tables[0];

                allstatusdt = "Select * from NotesStatus where IsProject='True' and Folder_Id='" + Convert.ToInt32(dtFolderStatus.Rows[ichk]["Folder_Id"].ToString()) + "'  and Project_Name='" + dtFolderStatus.Rows[ichk]["Project_Name"].ToString() + "' and UserName='" + Session["admin"] + "' and NoteStatus='True'";
                dsalstatus = DatabaseHelper.getDataset(allstatusdt);
                Allstatusdt = dsalstatus.Tables[0];


                if ((dtFolderstatus.Rows.Count == Allstatusdt.Rows.Count))
                {
                    if (dtFolderstatus.Rows.Count >= 1)
                    {
                        dtFolderStatus.Rows[ichk]["AddCategoryStatus"] = Convert.ToBoolean(true);
                    }
                    else
                    {
                        dtFolderStatus.Rows[ichk]["AddCategoryStatus"] = Convert.ToBoolean(false);
                    }
                }
                else
                {
                    dtFolderStatus.Rows[ichk]["AddCategoryStatus"] = Convert.ToBoolean(false);
                }

            }
            return dtFolderStatus;
        }
        catch (Exception ex)
        {

        }
        return dtFolderStatus;
    }

    public DataSet setFolderStatus(DataSet dsFolderStatus)
    {
        try
        {
            string Addstatus = "", allstatus = "";
            DataSet dsFolderstatus = null, Allstatusds = null;
            dsFolderStatus.Tables[0].Columns.Add("AddCategoryStatus");
            for (int ichk = 0; ichk < dsFolderStatus.Tables[0].Rows.Count; ichk++)
            {
                Addstatus = "Select * from NotesStatus where IsProject='True' and Folder_Id='" + Convert.ToInt32(dsFolderStatus.Tables[0].Rows[ichk]["Folder_Id"].ToString()) + "'  and Project_Name='" + dsFolderStatus.Tables[0].Rows[ichk]["Project_Name"].ToString() + "' and UserName='" + Session["admin"] + "'";
                dsFolderstatus = DatabaseHelper.getDataset(Addstatus);

                allstatus = "Select * from NotesStatus where IsProject='True' and Folder_Id='" + Convert.ToInt32(dsFolderStatus.Tables[0].Rows[ichk]["Folder_Id"].ToString()) + "'  and Project_Name='" + dsFolderStatus.Tables[0].Rows[ichk]["Project_Name"].ToString() + "' and UserName='" + Session["admin"] + "' and NoteStatus='True'";
                Allstatusds = DatabaseHelper.getDataset(allstatus);
                if (dsFolderstatus.Tables[0].Rows.Count == Allstatusds.Tables[0].Rows.Count)
                {
                    if (dsFolderstatus.Tables[0].Rows.Count >= 1)
                    {
                        dsFolderStatus.Tables[0].Rows[ichk]["AddCategoryStatus"] = Convert.ToBoolean(true);
                    }
                    else
                    {
                        dsFolderStatus.Tables[0].Rows[ichk]["AddCategoryStatus"] = Convert.ToBoolean(false);
                    }
                }
                else
                {
                    dsFolderStatus.Tables[0].Rows[ichk]["AddCategoryStatus"] = Convert.ToBoolean(false);
                }

            }
            return dsFolderStatus;
        }
        catch (Exception ex)
        {

        }
        return dsFolderStatus;
    }

    protected void btninsert(object sender, EventArgs e)
    {
        LinkButton clickbtn = (LinkButton)sender;
        string a = clickbtn.Text;
        FolderInsert(a);

    }
    private void FolderInsert(string Flodername)
    {

        string sqlsub = @"select * from Project_Folder where IsProject='True' and FolderName='" + Flodername + "' and Project_Name='" + Server.UrlDecode(Request.QueryString["projectname"].ToString()) + "'";
        int Result=0;
        try
        {
             Result= DatabaseHelper.checkuser(sqlsub);
        }catch(Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectNoteList.aspx:1 " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "FolderInsert()" + sqlsub);
        }

        if (Result <= 0)
        {


            string sql = @"insert into Project_Folder (FolderName,FolderDescription,Project_Name,IsProject)values
      ('" + Flodername + "','','" + Server.UrlDecode(Request.QueryString["projectname"].ToString()) + "','True')";
            int intResult = 0;
            try
            {
                 intResult = DatabaseHelper.executeSQLquery(sql);
            }catch(Exception ex)
            {
                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectNoteList.aspx:2 " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "FolderInsert()" + sqlsub);
            }

            if (intResult == 0)
            {
                lblmsg.Text = "Try again.";

            }
            else
            {


                string sqll = @"select * from Solution_categoriesnotes where Folder_Id in (select category_id from Solution_categories where category_name='" + Flodername + "')";
                DataSet dsnotes = null;
                try
                {
                    dsnotes = DatabaseHelper.getallstatus(sqll);
                }catch(Exception ex)
                {
                    DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectNoteList.aspx:3 " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "FolderInsert()" + sqll);
                }
                
                if (dsnotes.Tables != null && dsnotes.Tables[0].Rows.Count > 0)
                {
                    string folder_id = getfolderid( Server.UrlDecode(Request.QueryString["projectname"].ToString()), Flodername);

                    for (int i = 0; i < dsnotes.Tables[0].Rows.Count; i++)
                    {
                        sql = @"insert into ProjectNotes (Folder_Id,Project_Name,NoteName,NoteDescription,reported_date,IsProject)values
      (" + folder_id + ",'" + Server.UrlDecode(Request.QueryString["projectname"].ToString()) + "','" + dsnotes.Tables[0].Rows[i]["NoteName"].ToString() + "','" + dsnotes.Tables[0].Rows[i]["NoteDescription"].ToString() + "',getdate(),'True')";
                        int intnoteResult = 0;
                        try
                        {
                             intnoteResult = DatabaseHelper.executeSQLquery(sql);
                        }catch(Exception ex)
                        {
                            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectNoteList.aspx:4 " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "FolderInsert()" + sql);
                        }

                                        }
                }
                lblmsg.Text = "Added sucessfully.";
                getcategory(Server.UrlDecode(Request.QueryString["projectname"].ToString()));
            }
        }
        else
        {
            lblmsg.Text = "Already added.";
           
        }

    }

    public string getfolderid(string ProjectName, string foldername)
    {
        string Fid = "";
        string sql = @" select Folder_Id from Project_Folder where IsProject='True' and Project_Name='" + ProjectName + "' and  FolderName='" + foldername + "'";
        DataSet dsfolderid = null;
        try
        {
             dsfolderid = DatabaseHelper.getallstatus(sql);
        }catch(Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectNoteList.aspx:1 " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "getfolderid()" + sql);
        }
        if (dsfolderid != null && dsfolderid.Tables.Count > 0 && dsfolderid.Tables[0].Rows.Count > 0)
        {
            Fid = dsfolderid.Tables[0].Rows[0]["Folder_Id"].ToString();
        }
        return Fid;
    }
    protected void OnItemDataBound_Folderlstview(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            try
            {
                ListViewDataItem dataitem = (ListViewDataItem)e.Item;
                int policyid = Convert.ToInt32(DataBinder.Eval(dataitem.DataItem, "counts").ToString());
                if (policyid > 0)
                {
                    System.Web.UI.HtmlControls.HtmlControl cell = (System.Web.UI.HtmlControls.HtmlControl)e.Item.FindControl("ChangeColor");

                    cell.Style.Add("color", "Green");

                }
            }
            catch (Exception ex)
            {
                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectNoteList.aspx:1 " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "OnItemDataBound_Folderlstview()");
            }
        }
    }

}