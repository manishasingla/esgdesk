﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Projectdetail.aspx.cs" Inherits="Admin_Solutiondetail" %>

<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Project detail</title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../DashStyle.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        function showhideonLoad() {
            document.getElementById("idfileupload").style.display = "none";
            document.getElementById("divAttachment").style.display = "none";
        }

        function showhideDiv() {

            if ("Show upload" == document.getElementById("fileupload").innerHTML) {
                document.getElementById("idfileupload").style.display = "block";
                document.getElementById("fileupload").innerHTML = "Hide upload"
            }
            else if ("Hide upload" == document.getElementById("fileupload").innerHTML) {
                document.getElementById("idfileupload").style.display = "none";
                document.getElementById("fileupload").innerHTML = "Show upload"
            }
        }
        function showhideDownload() {

            if ("Show download" == document.getElementById("filedownload").innerHTML) {
                document.getElementById("divAttachment").style.display = "block";
                document.getElementById("filedownload").innerHTML = "Hide download"
            }
            else if ("Hide download" == document.getElementById("filedownload").innerHTML) {
                document.getElementById("divAttachment").style.display = "none";
                document.getElementById("filedownload").innerHTML = "Show download"
            }
        }
    </script>

</head>
<body onload="showhideonLoad()">
    <form id="form1" runat="server">
    <%-- <uc2:Admin_Header ID="Admin_Header1" runat="server" />--%>
    <div>
        <div id="inner">
            <div class="pagearea" style="width: 80%; float: left">
                <div id="TicketSummary">
                    <div style="width: 20%; float: left">
                        <asp:Label ID="lblcategoryname" runat="server" Style="font-weight: bolder" Text="Category"></asp:Label>
                        <hr />
                        <asp:ListView ID="Folderlstview" runat="server" OnItemDataBound="OnItemDataBound_Folderlstview">
                            <LayoutTemplate>
                                <div>
                                    <ul style="height: auto; list-style: disc outside none">
                                        <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                    </ul>
                                </div>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <li>
                                    <asp:CheckBox ID="chkboxCategory" Enabled="false" runat="server" Checked='<%#bool.Parse(Eval("AddCategoryStatus").ToString())%>'
                                        AutoPostBack="true" />&nbsp; <a id="ChangeColor" runat="server" href='<%# "ProjectNoteList.aspx?projectname="+HttpUtility.UrlEncode(Eval("Project_Name").ToString())+ "&folderid=" + Eval("folder_id")%>'
                                            style="margin-left: 10px;" target="_self">
                                            <%#Eval("foldername")%>
                                            (<%# Eval("counts")%>)</a> </li>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                    <div style="width: 79%; float: right;">
                        <div style="padding: 4px; background: #EBEBEB;">
                            <%--Solution →
                            <asp:Label ID="lblPcatrgoryname" runat="server"></asp:Label>
                            →--%>
                            <a id="list" runat="server" style="text-decoration: none">Category List</a> → <a
                                id="lblPcatrgoryname" runat="server" target="_self"></a>
                            <asp:Label ID="lblPfoldername" runat="server"></asp:Label>
                        </div>
                        <hr />
                         <div style="width:100%;float: right;border-top:1px solid black;border-right: 1px solid black;border-bottom: 1px solid black;border-left: 1px solid black;">
                        <div id="hideForEmp" runat="server">
                            <table width="100%" border="0">
                                <tr>
                                    <td>
                                        <a id="fileupload" href="#" onclick="showhideDiv();">Show upload</a>
                                        
                                    </td>
                                    <td>
                                    <div id="showdownload" runat="server">
                                      <%--<a id="filedownload" href="#" onclick="showhideDownload();">Show download</a>--%><%--<span id="Showmge" runat="server">File not found</span> --%>
                                    <%--<a id="filedownload" href="#" >Show download</a>--%>
                                    </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="idfileupload" runat="server">
                            <table width="100%" border="0">
                                <tr>
                                    <td>
                                      Description
                                      <asp:TextBox id="txtdesc" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:FileUpload ID="idupload" runat="server" />
                                        <asp:Button ID="btnsave" runat="server" Text="Save" OnClick="btnsave_Click" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                       <%-- <div id="divAttachment" runat="server" style="text-align: left">
                        </div>--%>
                        <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" CellPadding="5"
                            AllowPaging="True" AllowSorting="True" PageSize="100" Width="100%">
                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Top" />
                            <HeaderStyle BackColor="#282C5F" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                ForeColor="#ffffff" Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                            <Columns>
                                <asp:BoundColumn DataField="UploadedId" HeaderText="UploadedId" Visible="false" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="Project_Name" HeaderText="Project_Name" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="FileName1" HeaderText="FileName1" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Description" HeaderText="Description" SortExpression="Description">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="Uploaded_by" HeaderText="Uploaded by" SortExpression="Uploaded_by">
                                </asp:BoundColumn>
                                <%--===================================================================================================--%>
                                <asp:BoundColumn DataField="UploadedOn" HeaderText="Uploaded on" SortExpression="UploadedOn">
                                </asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="Download">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="idlnk" runat="server" Text="download" OnClick="Linkedit_click"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                
                                <asp:TemplateColumn HeaderText="Delete">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="idlnkdelete" runat="server" Text="Delete" OnClick="LinkDelete_click"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                            <PagerStyle NextPageText="Next" PrevPageText="Prev" />
                        </asp:DataGrid>
                    
                    </div>
                    
                        <br />
                        <div style="height: 50px; background: white; padding: 4px">
                            <div style="float: left">
                                <asp:Label ID="lblFN" runat="server" Style="font-weight: bolder;"></asp:Label><br />
                                By Support |
                                <asp:Label ID="lbldate" runat="server"></asp:Label>
                            </div>
                            <div id="edit" runat="server" style="float: right">
                                <a class="submit" id="editsolution" runat="server" target="_self">Edit</a>
                            </div>
                        </div>
                        <div id="solutiondesc" runat="server" style="background: white; padding: 4px">
                        </div>
                        <div>
                            <b>Completed </b>
                            <asp:CheckBox ID="idreadNote" runat="server" AutoPostBack="true" OnCheckedChanged="idreadNote_CheckedChanged" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
