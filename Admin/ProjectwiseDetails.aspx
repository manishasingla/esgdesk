<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ProjectwiseDetails.aspx.cs"
    Inherits="Admin_ProjectwiseDetails" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login credentials</title>
</head>
<body>
    <form id="form1" runat="server">
        <div align="center" id="hidegridError" runat="server">
            <table>
                <tr>
                    <td colspan="2" align="center">
                        <asp:Label ID="lblerror" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
         <div align="center">
              <strong> Login credentials </strong>
         </div>
        <div>
            <table>
                <tr>
                  <td>
                       User login url
                  </td>
                    <td align="center">
                        <asp:Label ID="lbluserloginurl" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        User name</td>
                    <td>
                        <asp:Label ID="LblUserUsername" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Password</td>
                    <td>
                        <asp:Label ID="lblUseerPassword" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                   <td>
                       Admin login url
                   </td>
                    <td align="center">
                        <asp:Label ID="lblAdminloginUrl" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        UserName</td>
                    <td>
                        <asp:Label ID="lbladminusername" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Password
                    </td>
                    <td>
                        <asp:Label ID="lblAdminpassword" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                <td>
                       Control panel url
                </td>
                    <td align="center">
                        <asp:Label ID="lblcontrolpabelurl" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Username</td>
                    <td>
                        <asp:Label ID="lblcontrolusername" runat="server">
                
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Password</td>
                    <td>
                        <asp:Label ID="lblcontrolpassword" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
