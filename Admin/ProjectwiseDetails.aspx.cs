using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Admin_ProjectwiseDetails : System.Web.UI.Page
{
    int id;
    protected void Page_Load(object sender, EventArgs e)
    {
        string var = Request.QueryString["id"];
        try
        {
            id = Convert.ToInt32(var);
        }
        catch
        {
            lblerror.Text = "Project ID must be an integer.";
            return;
        }
        GetProjectdata(id);

    }
    private void GetProjectdata(int id)
    {
        string strSql = "select * from projects where project_id=" + id;
        DataSet ds = DatabaseHelper.getDataset(strSql);
        //********************************************************************************************

        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            lbluserloginurl.Text = ds.Tables[0].Rows[0]["UserLoginurl"].ToString().Replace("''", "'");
            LblUserUsername.Text = ds.Tables[0].Rows[0]["Userusername"].ToString().Replace("''", "'");
            lblUseerPassword.Text = ds.Tables[0].Rows[0]["UserPassword"].ToString().Replace("''", "'");
            lblAdminloginUrl.Text = ds.Tables[0].Rows[0]["Adminloginurl"].ToString().Replace("''", "'");
            lbladminusername.Text = ds.Tables[0].Rows[0]["Adminusername"].ToString().Replace("''", "'");
            lblAdminpassword.Text = ds.Tables[0].Rows[0]["AdminPassword"].ToString().Replace("''", "'");
            lblcontrolpabelurl.Text = ds.Tables[0].Rows[0]["Controlpanelurl"].ToString().Replace("''", "'");
            lblcontrolusername.Text = ds.Tables[0].Rows[0]["Controlusername"].ToString().Replace("''", "'");
            lblcontrolpassword.Text = ds.Tables[0].Rows[0]["ControlPassword"].ToString().Replace("''", "'");

        }
        else
        {
            lblerror.Text = "Project was not found.";
        }

    }





}
