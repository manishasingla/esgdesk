using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Telerik.Web.UI;



public partial class Admin_RADGrid : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    //protected void UseDragColumnCheckBox_CheckedChanged(object sender, EventArgs e)
    //{
    //    grdPendingOrders.MasterTableView.GetColumn("DragDropColumn").Visible = UseDragColumnCheckBox.Checked;
    //    grdPendingOrders.Rebind();

    //    //grdShippedOrders.MasterTableView.GetColumn("DragDropColumn").Visible = UseDragColumnCheckBox.Checked;
    //    //grdShippedOrders.Rebind();
    //}

    protected IList<Order> PendingOrders
    {
        get
        {
            try
            {
                object obj = Session["PendingOrders"];
                if (obj == null)
                {
                    obj = GetOrders();
                    if (obj != null)
                    {
                        Session["PendingOrders"] = obj;
                    }
                    else
                    {
                        obj = new List<Order>();
                    }
                }
                return (IList<Order>)obj;
            }
            catch
            {
                Session["PendingOrders"] = null;
            }
            return new List<Order>();
        }
        set { Session["PendingOrders"] = value; }
    }

    //protected IList<Order> ShippedOrders
    //{
    //    get
    //    {
    //        try
    //        {
    //            object obj = Session["ShippedOrders"];
    //            if (obj == null)
    //            {
    //                Session["ShippedOrders"] = obj = new List<Order>();
    //            }
    //            return (IList<Order>)obj;
    //        }
    //        catch
    //        {
    //            Session["ShippedOrders"] = null;
    //        }
    //        return new List<Order>();
    //    }
    //    set { Session["ShippedOrders"] = value; }
    //}

    protected void grdPendingOrders_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
    {
        
        
        grdPendingOrders.DataSource = PendingOrders;
    }


    protected IList<Order> GetOrders()
    {
        DataTable tbl = new DataTable();
        
        tbl.Columns.Add("TaskID");
        tbl.Columns.Add("CompanyName");
        tbl.Columns.Add("OrderId");
        tbl.Rows.Add(4001, "Test1",'1');
        tbl.Rows.Add(5002, "Test2", '2');
        tbl.Rows.Add(6003, "Test3", '3');
        tbl.Rows.Add(7004, "Test4", '4');
        tbl.Rows.Add(8005, "Test5", '5');
        tbl.Rows.Add(9006, "Test6", '6');
        DataSet dsforStatus = new DataSet();
        dsforStatus.Tables.Add(tbl);
        IList<Order> results = new List<Order>();
        for (int i = 0; i < dsforStatus.Tables[0].Rows.Count; i++)
        {
            int id = Convert.ToInt32(dsforStatus.Tables[0].Rows[i]["OrderId"].ToString());
            string companyName = dsforStatus.Tables[0].Rows[i]["CompanyName"].ToString();
            int TaskIDv1 = Convert.ToInt32(dsforStatus.Tables[0].Rows[i]["TaskID"].ToString());
            results.Add(new Order(id, companyName, TaskIDv1));
        }
    //    using (IDbConnection connection = DbProviderFactories.GetFactory("System.Data.SqlClient").CreateConnection())
    //    {
    //        connection.ConnectionString =
    //            ConfigurationManager.ConnectionStrings["NorthwindConnectionString35"].ConnectionString;

    //        using (IDbCommand command = connection.CreateCommand())
    //        {
    //            command.CommandText =
    //                "SELECT o.OrderID, o.CustomerID, o.RequiredDate, c.CompanyName FROM orders o INNER JOIN customers c on o.customerID = c.customerID";

    //            connection.Open();
    //            try
    //            {
    //                IDataReader reader = command.ExecuteReader();
    //                while (reader.Read())
    //                {
    //                    int id = (int)reader.GetValue(reader.GetOrdinal("OrderID"));
    //                    string customerID = (!reader.IsDBNull(reader.GetOrdinal("CustomerID")))
    //                                            ? (string)reader.GetValue(reader.GetOrdinal("CustomerID"))
    //                                            : string.Empty;
    //                    DateTime requiredDate = (!reader.IsDBNull(reader.GetOrdinal("RequiredDate")))
    //                                                ? (DateTime)reader.GetValue(reader.GetOrdinal("RequiredDate"))
    //                                                : DateTime.MinValue;
    //                    string companyName = (!reader.IsDBNull(reader.GetOrdinal("CompanyName")))
    //                                             ? Server.HtmlEncode((string)reader.GetValue(reader.GetOrdinal("CompanyName")))
    //                                             : string.Empty;
    //                    results.Add(new Order(id, customerID, companyName, requiredDate.ToShortDateString()));
    //                }
    //            }
    //            catch
    //            {
    //                results.Clear();
    //            }
    //        }
    //    }
        return results;
    }
    //protected void grdShippedOrders_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
    //{
    //    grdShippedOrders.DataSource = ShippedOrders;
    //}

    protected void grdPendingOrders_RowDrop(object sender, GridDragDropEventArgs e)
    {
        if (string.IsNullOrEmpty(e.HtmlElement))
        {
            //if (e.DraggedItems[0].OwnerGridID == grdPendingOrders.ClientID)
            //{
            //    // items are drag from pending to shipped grid
            //    if ((e.DestDataItem == null && ShippedOrders.Count == 0) ||
            //        e.DestDataItem != null && e.DestDataItem.OwnerGridID == grdShippedOrders.ClientID)
            //    {
            //        IList<Order> shippedOrders = ShippedOrders;
            //        IList<Order> pendingOrders = PendingOrders;
            //        int destinationIndex = -1;
            //        if (e.DestDataItem != null)
            //        {
            //            Order order = GetOrder(shippedOrders, (int)e.DestDataItem.GetDataKeyValue("OrderId"));
            //            destinationIndex = (order != null) ? shippedOrders.IndexOf(order) : -1;
            //        }


            //        foreach (GridDataItem draggedItem in e.DraggedItems)
            //        {
            //            Order tmpOrder = GetOrder(pendingOrders, (int)draggedItem.GetDataKeyValue("OrderId"));

            //            if (tmpOrder != null)
            //            {
            //                if (destinationIndex > -1)
            //                {
            //                    if (e.DropPosition == GridItemDropPosition.Below)
            //                    {
            //                        destinationIndex += 1;
            //                    }
            //                    shippedOrders.Insert(destinationIndex, tmpOrder);
            //                }
            //                else
            //                {
            //                    shippedOrders.Add(tmpOrder);
            //                }

            //                pendingOrders.Remove(tmpOrder);
            //            }
            //        }

            //        ShippedOrders = shippedOrders;
            //        PendingOrders = pendingOrders;
            //        grdPendingOrders.Rebind();
            //        grdShippedOrders.Rebind();
            //    }
                //else if (e.DestDataItem != null && e.DestDataItem.OwnerGridID == grdPendingOrders.ClientID)
            if (e.DestDataItem != null && e.DestDataItem.OwnerGridID == grdPendingOrders.ClientID)
                {
                    //reorder items in pending grid
                    IList<Order> pendingOrders = PendingOrders;
                    Order order = GetOrder(pendingOrders, (int)e.DestDataItem.GetDataKeyValue("TaskID"));
                    int destinationIndex = pendingOrders.IndexOf(order);
                   //************************************
                      pendingOrders.Remove(order);
                      Order tmpOrder1;
                      int iOrderid=0;
                      foreach (GridDataItem draggedItem in e.DraggedItems)
                      {
                          tmpOrder1 = GetOrder(pendingOrders, (int)draggedItem.GetDataKeyValue("TaskID"));
                         iOrderid = tmpOrder1.OrderID;
                      }
                     int medium=order.OrderID ;
                     int TaskIDv = order.TaskID;
                      string  companyv = order.Company;
                      order = new Order(iOrderid, companyv, TaskIDv); 
                      //order.CustomerID=tmpOrder.CustomerID;
                      pendingOrders.Insert(destinationIndex, order);
                  //************************************
                    if (e.DropPosition == GridItemDropPosition.Above && e.DestDataItem.ItemIndex > e.DraggedItems[0].ItemIndex)
                    {
                        destinationIndex -= 1;
                    }
                    if (e.DropPosition == GridItemDropPosition.Below && e.DestDataItem.ItemIndex < e.DraggedItems[0].ItemIndex)
                    {
                        destinationIndex += 1;
                    }

                    List<Order> ordersToMove = new List<Order>(); 
                    Order DragOrder=null;
                    foreach (GridDataItem draggedItem in e.DraggedItems)
                    {
                         DragOrder = GetOrder(pendingOrders, (int)draggedItem.GetDataKeyValue("TaskID"));
                        Order tmpOrder = GetOrder(pendingOrders, (int)draggedItem.GetDataKeyValue("TaskID"));
                        if (tmpOrder != null)
                        {
                            string icompany=tmpOrder.Company;
                            int iTaskID = tmpOrder.TaskID;
                            tmpOrder = new Order(medium, icompany, iTaskID); 
                            ordersToMove.Add(tmpOrder);
                            
                        }
                    }
                      
                    foreach (Order orderToMove in ordersToMove)
                    {
                        pendingOrders.Remove(DragOrder);
                        pendingOrders.Insert(destinationIndex, orderToMove);
                    }
                    PendingOrders = pendingOrders;
                    grdPendingOrders.Rebind();

                    int destinationItemIndex = destinationIndex - (grdPendingOrders.PageSize * grdPendingOrders.CurrentPageIndex);
                    e.DestinationTableView.Items[destinationItemIndex].Selected = true;
                }
            }
        }


    private static Order GetOrder(IEnumerable<Order> ordersToSearchIn, int TaskID)
    {
        foreach (Order order in ordersToSearchIn)
        {
            if (order.TaskID == TaskID)
            {
                return order;
            }
        }
        return null;
    }
    //protected void grdShippedOrders_RowDrop(object sender, GridDragDropEventArgs e)
    //{
    //    if (!string.IsNullOrEmpty(e.HtmlElement) && e.HtmlElement == "trashCan")
    //    {
    //        IList<Order> shippedOrders = ShippedOrders;
    //        bool deleted = false;
    //        foreach (GridDataItem draggedItem in e.DraggedItems)
    //        {
    //            Order tmpOrder = GetOrder(shippedOrders, (int)draggedItem.GetDataKeyValue("OrderId"));

    //            if (tmpOrder != null)
    //            {
    //                shippedOrders.Remove(tmpOrder);
    //                deleted = true;
    //            }
    //        }
    //        if (deleted)
    //        {
    //            msg.Visible = true;
    //        }
    //        ShippedOrders = shippedOrders;
    //        grdShippedOrders.Rebind();
    //    }
    //}

    //#region Nested type: Order

    protected class Order
    {
        private string _companyName;
        private int _TaskID;
        private int _orderId;
    //    private string _date;

        //public Order(int orderId, string customerId, string companyName, string requiredDate)
        public Order(int orderId, string companyName, int TaskID)
        {
            _orderId = orderId;
            _TaskID = TaskID;
            _companyName = companyName;
            //_date = requiredDate;
        }

        public int OrderID
        {
            get { return _orderId; }
            
        }

        public int TaskID
        {
            get { return _TaskID; }
            
        }

        public string Company
        {
            get { return _companyName; }
           
        }

    //    public string Date
    //    {
    //        get { return _date; }
    //    }
    }

    //#endregion









}
