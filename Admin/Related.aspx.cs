using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Admin_Related : System.Web.UI.Page
{
    string sql="";
    
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Response.Redirect("login.aspx");
            return;
        }
        if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
        {
            Response.Redirect("tasks.aspx");
            return;
        }
          TopFilterLable.InnerHtml="";
          divMessage.InnerHtml = "";

        if (!Page.IsPostBack)
        {
            if (!DatabaseHelper.can_Show_Deleted_Tasks(Session["admin"].ToString()))
            {
                chkViewDeleted.Visible = false;
            }
            else
            {
                chkViewDeleted.Visible = true;
            }


            LnkBtnToChngPR.Enabled = false;
            LnkChngAssingedTo.Enabled = false;
            LnkChngStatus.Enabled = false;
            LnkChngAssingedTo.Enabled = false;
             bindDefault();             
            
        }
    }



    private void BindData()
    {
        string filter = "where  ";
        string filter2 = "and";
        string topFilterLable = "";
        string strFilter = " where ";
        if (chkViewDeleted.Checked == true)
        {
            strFilter += " ClientRequest.deleted = 1 ";
        }
        else
        {
            strFilter += " ClientRequest.deleted <> 1 ";
        }

        if (chkHideClosed.Checked == true)
        {
            strFilter += " and ClientRequest.status = 'closed' ";
        }
        else
        {
            strFilter += " and ClientRequest.status <> 'closed' ";
        }
        if (drpStatuses.SelectedIndex != 0)
        {
            strFilter += " and ClientRequest.Status = '" + drpStatuses.SelectedItem.Value + "' ";

            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Status: " + drpStatuses.SelectedItem.Text;
            }
            else
            {
                topFilterLable += " / Status: " + drpStatuses.SelectedItem.Text;
            }
        }

        if (drpPriorities.SelectedIndex != 0)
        {
            strFilter += " and ClientRequest.Priority = " + drpPriorities.SelectedItem.Value + " ";

            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Priority: " + drpPriorities.SelectedItem.Text;
            }
            else
            {
                topFilterLable += " / Priority: " + drpPriorities.SelectedItem.Text;
            }
        }
        if (!DatabaseHelper.can_Show_Closed_Tasks(Session["admin"].ToString()))
        {
            filter += " status <> 'closed' and ";
            filter2 += " status <> 'closed' and ";

            chkHideClosed.Visible = false;
        }
        else
        {
            if (chkHideClosed.Checked == true)
            {
                filter += " status = 'closed' and ";
                filter2 += " status = 'closed' and ";
            }
            else
            {
                filter += " status <> 'closed' and status <> 'checked' and ";
                filter2 += " status <> 'closed' and status <> 'checked' and ";

            }
        }
        if (chkViewDeleted.Checked == true)
        {
            filter += " deleted = 1 ";
            filter2 += " deleted = 1 ";
        }
        else
        {
            filter += " deleted <> 1 ";
            filter2 += " deleted <> 1 ";
        }
        string drpValue = drpFilter.SelectedValue;
        string username = "";

        if (drpValue.Contains("Assigned by me"))
        {
            filter += " and reported_user = '" + username + "' ";
            filter2 += " and reported_user = '" + username + "' ";
        }
        else if (drpValue.Contains("Assigned by "))
        {
            username = drpValue.Replace("Assigned by ","");

            filter += " and reported_user = '" + username + "' ";
            filter2 += " and reported_user = '" + username + "' ";
        }
        else if (drpValue.Contains("All emps CWO"))
        {
            filter += " and status = 'CWO' ";
            filter2 += " and status = 'CWO' ";
        }
        else if (drpValue.Contains("Show my open tasks"))
        { }
        if (drpProjects.SelectedIndex != 0)
        {
            filter += " and project = '"+ drpProjects.SelectedValue +"' ";
            filter2 += " and project = '" + drpProjects.SelectedValue + "' ";
            
            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Project: " + drpProjects.SelectedValue;
            }
            else
            {
                topFilterLable += " / Project: " + drpProjects.SelectedValue;
            }
        }

        
        if (drpPriorities.SelectedIndex != 0)
        {
            filter += " and priority = '" + drpPriorities.SelectedValue + "' ";
            filter2 += " and priority = '" + drpPriorities.SelectedValue + "' ";

            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Priority: " + drpPriorities.SelectedValue;
            }
            else
            {
                topFilterLable += " / Priority:" + drpPriorities.SelectedValue;
            }
        }
       
       
        if (drpStatuses.SelectedIndex != 0)
        {
            filter += " and status = '" + drpStatuses.SelectedValue + "' ";
            filter2 += " and status = '" + drpStatuses.SelectedValue + "' ";

            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Status: " + drpStatuses.SelectedValue;
            }
            else
            {
                topFilterLable += " / Status: " + drpStatuses.SelectedValue;
            }
        }

        if (chkViewDeleted.Checked == true)
        {
            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by View deleted";
            }
            else
            {
                topFilterLable += " / View deleted";
            }
        }
        if (chkHideClosed.Checked == true)
        {
            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by view closed";
            }
            else
            {
                topFilterLable += " / View closed";
            }
        }

        if (Session["ProjectRelated"] == null || Session["ProjectRelated"].ToString() == "")
        {


        }
        else
        {
            filter += "and project = '" + Session["ProjectRelated"].ToString() + "'";
            strFilter += "and ProjectName = '" + Session["ProjectRelated"].ToString() + "'";
        }
        TopFilterLable.InnerHtml = topFilterLable;
        sql = "select task_id as ID,short_desc as ShortDescr,project as companyname,Priority,last_updated_user as LastUpdatedBy,Status,last_updated_date as LastUpdatedOn from tasks " + filter;
        string sql2 = "select RequestId as ID,ShortDescr,ProjectName as companyname,Priority as ItemPriority, LastUpdatedBy,Status, LastUpdatedOn from ClientRequest " + strFilter;
        DataSet ds = DatabaseHelper.getDataset(sql);
        DataSet dsCRs = DatabaseHelper.getDataset(sql2);
        ds = formatTaskDataSet(ds);
        dsCRs = formatCrsDataSet(dsCRs);
        try
        {
            ds.Merge(dsCRs);
            DataView dv = ds.Tables[0].DefaultView;
            dv.Sort = lblOrderBy.Text;//// "LastUpdatedOn Desc";
            ds.Tables.Clear();
            ds.Tables.Add(dv.ToTable());
        }
    catch { }
        if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "No related yet.";
            DataGrid1.DataSource = null;
            DataGrid1.DataBind();
            LblRecordsno.Text = "0";
            divTotalHrs.Visible = false;
        }
        else
        {
         if (ds.Tables[0].Rows.Count <= 100)
            {
                DataGrid1.PagerStyle.Visible = false;
                DataGrid1.CurrentPageIndex = 0;
            }
            else
            {
                DataGrid1.PagerStyle.Visible = true;
            }
            PagedDataSource objds = new PagedDataSource();
            objds.DataSource = ds.Tables[0].DefaultView;
            LblRecordsno.Text = objds.DataSourceCount.ToString(); 
            divMessage.InnerHtml = "";
            DataGrid1.DataSource = ds.Tables[0];
            DataGrid1.DataBind();
            getTotalHrsReport("select task_id from tasks " + filter);
            divTotalHrs.Visible = true;
        }
    }
    private DataSet formatCrsDataSet(DataSet ds)
    {
        try
        {
           ds.Tables[0].Columns.Add("editrelates");
           ds.Tables[0].Columns.Add("ITEMID");
           ds.Tables[0].Columns.Add("Priority");
           ds.Tables[0].Columns.Add("ShowAllComment");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {
                    if (ds.Tables[0].Rows[i]["ID"] != null && ds.Tables[0].Rows[i]["ID"].ToString().Trim() != "")
                    {

                        string taskId = "<a href=\"edit_request.aspx?reqid=" + ds.Tables[0].Rows[i]["ID"].ToString() + "\">" + "CR" + "</a>";
                        ds.Tables[0].Rows[i]["ITEMID"] = taskId.ToString();
                        ds.Tables[0].Rows[i]["editrelates"] = "<a href=\"edit_request.aspx?reqid=" + ds.Tables[0].Rows[i]["ID"].ToString() + "\">" + "Edit" + "</a>";
                    }
                    else
                    {
                        ds.Tables[0].Rows[i]["ID"] = "";
                    }

                    if (ds.Tables[0].Rows[i]["ShortDescr"] != null && ds.Tables[0].Rows[i]["ShortDescr"].ToString().Trim() != "")
                    {
                     ds.Tables[0].Rows[i]["ShortDescr"] = "<a  href=\"edit_request.aspx?reqid=" + ds.Tables[0].Rows[i]["ID"].ToString() + "  \">" + ds.Tables[0].Rows[i]["ShortDescr"].ToString() + "</a>";
                    }
                    else
                    {

                        ds.Tables[0].Rows[i]["ShortDescr"] = "";
                    }
                    if (ds.Tables[0].Rows[i]["ItemPriority"] != null && ds.Tables[0].Rows[i]["ItemPriority"].ToString().Trim() != "")
                    {
                        try
                        {
                            string priority = ds.Tables[0].Rows[i]["ItemPriority"].ToString();
                            if (ds.Tables[0].Rows[i]["ItemPriority"].ToString() == "0")
                            {
                                priority = "6";

                            }

                            ds.Tables[0].Rows[i]["Priority"] = (string)DatabaseHelper.executeScalar("select priority_name from priorities where priority_id='" + priority + "'");
                        }
                        catch
                        {
                            ds.Tables[0].Rows[i]["Priority"] = "0 - IMMEDIATE";
                        }
                    }
                    else
                    {
                        string priority = "";
                        ds.Tables[0].Rows[i]["Priority"] = priority.ToString();
                    }
                    try
                    {

                       string LastComment = "";
                       sql = "Select * from ClientRequest_Details where deleted <> 1 and RequestId=" + ds.Tables[0].Rows[i]["ID"].ToString() + " order by CommentId desc";
                       DataSet dslastcomment = DatabaseHelper.getDataset(sql);
                       if (dslastcomment != null && dslastcomment.Tables[0].Rows.Count > 0)
                        {
                          for (int j = 0; j < dslastcomment.Tables[0].Rows.Count; j++)
                            {
                              LastComment += "<span style='color:Green'>" + "Comment " + dslastcomment.Tables[0].Rows[j]["CommentId"].ToString() + " posted by " + dslastcomment.Tables[0].Rows[j]["PostedBy"].ToString() + " on " + DateTime.Parse(dslastcomment.Tables[0].Rows[j]["PostedOn"].ToString()).ToString("dd MMM h:mm tt") + "</span>";
                              LastComment += "<br/><span>" + dslastcomment.Tables[0].Rows[j]["Comment"].ToString() + "</span>";
                              LastComment += "<span style=\"border-bottom: solid 1px green; margin-bottom: 2px; height:1px\"></span><br/><hr />";
                            }
                                                     
                        }
                        ds.Tables[0].Rows[i]["ShowAllComment"] = LastComment.ToString();
                    }
                    catch
                    {}
                }
                catch { }
            }
        }
        catch
        {
            Server.ClearError();
        }
        return ds;
    }

    private DataSet formatTaskDataSet(DataSet ds)
    {
        try
        {
            ds.Tables[0].Columns.Add("editrelates");
            ds.Tables[0].Columns.Add("ITEMID");
            ds.Tables[0].Columns.Add("ShowAllComment");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {
                    if (ds.Tables[0].Rows[i]["ID"] != null && ds.Tables[0].Rows[i]["ID"].ToString().Trim() != "")
                    {
                        string taskId = "<a href=\"edit_task.aspx?id=" + ds.Tables[0].Rows[i]["ID"].ToString() + "\">" + "Task" + "</a>";
                        ds.Tables[0].Rows[i]["ITEMID"] = taskId.ToString();
                        ds.Tables[0].Rows[i]["editrelates"] = "<a href=\"edit_task.aspx?id=" + ds.Tables[0].Rows[i]["ID"].ToString() + "\">" + "Edit" + "</a>";
                     }
                    else
                    {
                        ds.Tables[0].Rows[i]["ID"] = "";
                    }
                    if (ds.Tables[0].Rows[i]["ShortDescr"] != null && ds.Tables[0].Rows[i]["ShortDescr"].ToString().Trim() != "")
                    {
                        ds.Tables[0].Rows[i]["ShortDescr"] = "<a  href=\"edit_task.aspx?id=" + ds.Tables[0].Rows[i]["ID"].ToString() + "  \">" + ds.Tables[0].Rows[i]["ShortDescr"].ToString() + "</a>";
                    }
                    else
                    {
                     ds.Tables[0].Rows[i]["ShortDescr"] = "";
                    }
                    if (ds.Tables[0].Rows[i]["Priority"] != null && ds.Tables[0].Rows[i]["Priority"].ToString().Trim() != "")
                    {
                        ds.Tables[0].Rows[i]["Priority"] = ds.Tables[0].Rows[i]["Priority"].ToString();
                    }
                    try
                    {
                       string LastComment = "";
                       sql = "select *  from task_comments where task_comments.deleted <> 1  and task_id =" + ds.Tables[0].Rows[i]["ID"].ToString() + " order by tc_id desc";
                       DataSet dslastcomment = DatabaseHelper.getDataset(sql);
                       if (dslastcomment != null && dslastcomment.Tables[0].Rows.Count > 0)
                       {
                        for (int j = 0; j < dslastcomment.Tables[0].Rows.Count; j++)
                           {
                              if (dslastcomment.Tables[0].Rows[j]["qflag"].ToString() == "1")
                              {
                                 LastComment += "<span style='color:Red'>" + "Question posted by " + dslastcomment.Tables[0].Rows[j]["username"].ToString() + " for  " + dslastcomment.Tables[0].Rows[j]["QuesTo"].ToString() + " on " + DateTime.Parse(dslastcomment.Tables[0].Rows[j]["post_date"].ToString()).ToString("dd MMM h:mm tt") + "</span>";
                                 LastComment += "<br/><span>" + dslastcomment.Tables[0].Rows[j]["comment"].ToString() + "</span>";
                                 LastComment += "<span style=\"border-bottom: solid 1px green; margin-bottom: 2px; height:1px\"></span><br/><hr />";
                               }
                               else
                               {
                                  LastComment += "<span style='color:Green'>" + "Comment " + dslastcomment.Tables[0].Rows[j]["tc_id"].ToString() + " posted by " + dslastcomment.Tables[0].Rows[j]["username"].ToString() + " on " + DateTime.Parse(dslastcomment.Tables[0].Rows[j]["post_date"].ToString()).ToString("dd MMM h:mm tt") + "</span>";
                                  LastComment += "<br/><span>" + dslastcomment.Tables[0].Rows[j]["comment"].ToString() + "</span>";
                                  LastComment += "<span style=\"border-bottom: solid 1px green; margin-bottom: 2px; height:1px\"></span><br/><hr />";
                               }
                            
                            }


                            
                        }

                        ds.Tables[0].Rows[i]["ShowAllComment"] = LastComment.ToString();
                    }
                    catch
                    {

                    }






                }
                catch { }
            }
        }
        catch
        {
            Server.ClearError();
            ///Response.Redirect("fhs.aspx");
        }
        return ds;
    }


    void load_dropdowns()
    {

        // projects
        sql = @"select project_name
		from projects
		where active = 'Y' order by project_name;";

        // categories
        sql += "\nselect category_name from categories order by sort_seq, category_name;";

        // priorities
        sql += "\nselect priority_name,priority_id from priorities order by sort_seq, priority_name;";

        // statuses
        sql += "\nselect status_name from statuses order by sort_seq, status_name;";

        // users
        sql += "\nselect username from users where active = 'Y' order by username;";

        // do a batch of sql statements
        DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);

        drpProjects.DataSource = ds_dropdowns.Tables[0];
        drpProjects.DataTextField = "project_name";
        drpProjects.DataValueField = "project_name";
        drpProjects.DataBind();
        drpProjects.Items.Insert(0, new ListItem("[no filter]", "0"));
        
        DrpMultipleTaskPR.DataSource = ds_dropdowns.Tables[2];
        DrpMultipleTaskPR.DataTextField = "priority_name";
        DrpMultipleTaskPR.DataValueField = "priority_id";
        DrpMultipleTaskPR.DataBind();
        DrpMultipleTaskPR.Items.Insert(0, new ListItem("[no filter]", ""));
       
        drpPriorities.DataSource = ds_dropdowns.Tables[2];
        drpPriorities.DataTextField = "priority_name";
        drpPriorities.DataValueField = "priority_name";
        drpPriorities.DataBind();
        drpPriorities.Items.Insert(0, new ListItem("[no filter]", "0"));


        drpStatuses.DataSource = ds_dropdowns.Tables[3];
        drpStatuses.DataTextField = "status_name";
        drpStatuses.DataValueField = "status_name";
        drpStatuses.DataBind();
        drpStatuses.Items.Insert(0, new ListItem("[no filter]", "0"));
        DrpMultipleTaskStatus.DataSource = ds_dropdowns.Tables[3];
        DrpMultipleTaskStatus.DataTextField = "status_name";
        DrpMultipleTaskStatus.DataValueField = "status_name";
        DrpMultipleTaskStatus.DataBind();
        DrpMultipleTaskStatus.Items.Insert(0, new ListItem("[no filter]", "0"));
        
    }

    void load_filterDropDown()
    {
        drpFilter.Items.Clear();
        if (DatabaseHelper.isAdmin(Session["Admin"].ToString()))
        {
            sql = "select username from users where active='Y' and admin='Y' and username <> '" + Session["Admin"] + "' order by username";

            DataSet dsAdmin = DatabaseHelper.getDataset(sql);

            if (dsAdmin != null)
            {
                if (dsAdmin.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsAdmin.Tables[0].Rows.Count; i++)
                    {
                        drpFilter.Items.Add("Assigned by " + dsAdmin.Tables[0].Rows[i]["username"]);
                    }
                }
            }
            drpFilter.Items.Add("Assigned by " + Session["Admin"]);
            drpFilter.Items.Add("Assigned by me");
            drpFilter.Items.Add("All tasks by last updated");
            drpFilter.Items.Add("All emps CWO");
            drpFilter.Items.Add("Show my open tasks");
            drpFilter.SelectedValue = (string)DatabaseHelper.executeScalar("select default_selected_item from users where username='" + Session["admin"] + "'");
        }
        else
        {
            drpFilter.Items.Add("Show my open tasks");
        }
    }

    protected void drpFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["filter"] = null;
        load_dropdowns();
        setFileterSession();
        DataGrid1.CurrentPageIndex = 0;
        BindData();

    }
   
    protected void drpProjects_SelectedIndexChanged(object sender, EventArgs e)
    {
        setFileterSession();
        DataGrid1.CurrentPageIndex = 0;
        BindData();
    }

    protected void drpCategories_SelectedIndexChanged(object sender, EventArgs e)
    {
        setFileterSession();
        DataGrid1.CurrentPageIndex = 0;
        BindData();
    }

    protected void drpReportedBy_SelectedIndexChanged(object sender, EventArgs e)
    {
        setFileterSession();
        DataGrid1.CurrentPageIndex = 0;
        BindData();
    }

    protected void drpPriorities_SelectedIndexChanged(object sender, EventArgs e)
    {
        setFileterSession();
        DataGrid1.CurrentPageIndex = 0;
        BindData();
    }

    protected void drpAssignedTo_SelectedIndexChanged(object sender, EventArgs e)
    {
        setFileterSession();
        DataGrid1.CurrentPageIndex = 0;
        BindData();
    }

    protected void drpStatuses_SelectedIndexChanged(object sender, EventArgs e)
    {
        setFileterSession();
        DataGrid1.CurrentPageIndex = 0;
        BindData();
    }

    private void setFileterSession()
    {
        Session["HideClosed2"] = chkHideClosed.Checked;
        Session["IncludeDeleted2"] = chkViewDeleted.Checked;
        Session["Filter2"] = drpFilter.SelectedValue;
        Session["Project2"] = drpProjects.SelectedValue;
        Session["ProjectRelated"] = drpProjects.SelectedValue;
        Session["Priority2"] = drpPriorities.SelectedValue;
        Session["Status2"] = drpStatuses.SelectedValue;
        Session["OrderBy2"] = lblOrderBy.Text;
    }
    private void getFilterSession()
    {
        try
        {
            if (Session["HideClosed2"] != null)
                chkHideClosed.Checked = (bool)Session["HideClosed2"];
            if (Session["IncludeDeleted2"] != null)
                chkViewDeleted.Checked = (bool) Session["IncludeDeleted2"];
            if (Session["Filter2"] != null)
                drpFilter.SelectedValue = Session["Filter2"].ToString();
            if (Session["Project2"] != null)
                drpProjects.SelectedValue = Session["Project2"].ToString();
            if (Session["ProjectRelated"] != null)
            {
                drpProjects.SelectedValue = Session["ProjectRelated"].ToString();
            }
            if (Session["Priority2"] != null)
                drpPriorities.SelectedValue = Session["Priority2"].ToString();
            if (Session["Status2"] != null)
                drpStatuses.SelectedValue = Session["Status2"].ToString();
            if (Session["OrderBy2"] != null)
                lblOrderBy.Text = Session["OrderBy2"].ToString();
        }
        catch
        {

        }
    }

    protected void DataGrid1_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        if (e.SortExpression.ToString() == Session["Column"])
        {
            //Reverse the sort order
            if (Session["Order2"] == "ASC")
            {
                lblOrderBy.Text =  e.SortExpression.ToString() + " DESC";
                Session["Order2"] = "DESC";
            }
            else
            {
                lblOrderBy.Text =  e.SortExpression.ToString() + " ASC";
                Session["Order2"] = "ASC";
            }
        }
        else
        {
            //Different column selected, so default to ascending order
            lblOrderBy.Text = e.SortExpression.ToString() + " ASC";
            Session["Order2"] = "ASC";
        }

        Session["Column"] = e.SortExpression.ToString();
        Session["OrderBy2"] = lblOrderBy.Text;
        BindData();
    }

    protected void DataGrid1_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemIndex != -1)
        {
           ((Label)e.Item.FindControl("lblLastUpdatedOn")).Text = DateTime.Parse(((Label)e.Item.FindControl("lblLastUpdatedOn")).Text).ToString("dd MMM h:mm tt").ToString();
            string htmlColor = (string)DatabaseHelper.executeScalar("select bg_color from priorities where priority_name='" + e.Item.Cells[5].Text + "'");
            e.Item.BackColor = System.Drawing.ColorTranslator.FromHtml(htmlColor);
            Label LblAllComment = (Label)e.Item.FindControl("lblshrtDesc");
            LblAllComment.Attributes.Add("onmouseover", "get_(this);");
            LblAllComment.Attributes.Add("onmouseout", "get_1(this);");
        }
    }

    protected void DataGrid1_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        DataGrid1.CurrentPageIndex = e.NewPageIndex;
        BindData();
    }

    protected void btnClearFilters_Click(object sender, EventArgs e)
    {
        load_dropdowns();
        load_filterDropDown();
        chkViewDeleted.Checked = false;
        chkHideClosed.Checked = false;
        setFileterSession();
        BindData();
    }

    void bindDefault()
    {
        load_dropdowns();
        load_filterDropDown();
        lblOrderBy.Text = "LastUpdatedOn desc";
        chkViewDeleted.Checked = false;
        getFilterSession();
        BindData();  
    }

    protected void chkViewDeleted_CheckedChanged(object sender, EventArgs e)
    {
        
        lblOrderBy.Text = " LastUpdatedOn desc";
        setFileterSession();
        BindData();
    }
    protected void chkHideClosed_CheckedChanged(object sender, EventArgs e)
    {
        setFileterSession();
        BindData();
    }

    protected void getTotalHrsReport(string strSql)
    {
        sql = @"select sum(ETC)
                from tasks
                where task_id in (" + strSql + ")";

        int ETC = 0;

        object objETC = DatabaseHelper.executeScalar(sql);

        if (objETC != null && objETC.ToString() != "" && objETC.ToString() != "0")
        {
            ETC += (int)objETC;
        }
        else
        {
            ETC = 0;
        }

        sql = @"select sum(datediff(minute,started_date,finished_date))
                from hours_reporting
                where finished_date is not null and task_id in (" + strSql + ")";

        int hrsTaken = 0;
        object objHrsTaken = DatabaseHelper.executeScalar(sql);
        if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
        {
            hrsTaken += (int)objHrsTaken;
        }
        else
        {
            hrsTaken = 0;
        }

        sql = @"select sum(datediff(minute,started_date,getdate()))
                from hours_reporting
                where finished_date is null and task_id in (" + strSql + ")";

        objHrsTaken = DatabaseHelper.executeScalar(sql);

        if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
        {
            hrsTaken += (int)objHrsTaken;
        }


        lblETC.Text = "" + (ETC / 60) + " hrs " + (ETC % 60) + " mins";

        lblTotalHrsTaken.Text = "" + (hrsTaken / 60) + " hrs " + (hrsTaken % 60) + " mins";

        int hrsLeft = ETC - hrsTaken;

        if (hrsLeft < 0)
        {
            hrsLeft = -1 * hrsLeft;
            lblExpectedHrsLeft.Text = "-" + (hrsLeft / 60) + " hrs " + (hrsLeft % 60) + " mins";
        }
        else
        {
            lblExpectedHrsLeft.Text = "" + (hrsLeft / 60) + " hrs " + (hrsLeft % 60) + " mins";
        }

    }
    protected void LnkBtnToDeleteAll_Click(object sender, EventArgs e)
     {

        int i = 0;
        foreach (DataGridItem row in DataGrid1.Items)
        {
            try
            {
                CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                Label GetItemID = (Label)row.FindControl("lblID");
                if (cb != null && cb.Checked)
                {
                    i = i + 1;

                    if (GetItemID.Text.Contains("task"))
                    {

                        sql = "Update tasks set deleted=1,last_updated_date = getdate(),last_updated_user='" + Session["admin"].ToString().Replace("'", "''") + "' where task_id=" + row.Cells[0].Text;

                        int intResult = DatabaseHelper.executeNonQuery(sql);

                        if (intResult != 0)
                        {

                            sql = @" insert into task_updates([task_id],[username],[post_date],[comment]) 
                            values(" + row.Cells[0].Text + ",'" + Session["admin"].ToString().Replace("'", "''") + "',getdate(),'Task was deleted.'); ";

                            intResult = DatabaseHelper.executeNonQuery(sql);

                            try
                            {

                                sql = @"select *  from task_comments where task_id =" + row.Cells[0].Text + " and  tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "')";
                                /// sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

                                DataSet dscomment = DatabaseHelper.getDataset(sql);

                                if (dscomment != null && dscomment.Tables[0].Rows.Count > 0)
                                {
                                    for (int j = 0; j < dscomment.Tables[0].Rows.Count; j++)
                                    {
                                        try
                                        {


                                            string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                                            sqlinsert += " values(" + dscomment.Tables[0].Rows[j]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                                            int intCommentResult = DatabaseHelper.executeNonQuery(sqlinsert);

                                        }
                                        catch
                                        {
                                        }
                                    }

                                }
                            }
                            catch { }
                        }

                    }
                    else
                    {
                        sql = "update ClientRequest set deleted=1 where [RequestId] =" + row.Cells[0].Text;

                        int intResult = DatabaseHelper.executeNonQuery(sql);

                        if (intResult != 0)
                        {


                            try
                            {

                                string sqlcomment = "select *  from ClientRequest_Details where [RequestId] = " + row.Cells[0].Text + " and [CommentId] not in (select  CommentId  from read_CR_comments where read_CR_comments.username='" + Session["admin"].ToString() + "')";
                                DataSet dscomment = DatabaseHelper.getDataset(sqlcomment);

                                if (dscomment != null && dscomment.Tables[0].Rows.Count > 0)
                                {
                                    for (int j = 0; j < dscomment.Tables[0].Rows.Count; j++)
                                    {
                                        try
                                        {
                                            string sqlinsert = "insert into read_CR_comments([CommentId],[UserName]) ";
                                            sqlinsert += " values(" + dscomment.Tables[0].Rows[j]["CommentId"].ToString() + ",'" + Session["admin"].ToString() + "')";

                                            int intCommentResult = DatabaseHelper.executeNonQuery(sqlinsert);
                                        }
                                        catch
                                        {
                                        }
                                    }


                                }
                            }
                            catch { }

                        }


                    }

                }
            }
            catch { }
        }
        if (i > 0)
        {
            Response.Redirect("Related.aspx", false);
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('Please select item to delete.');</script>");
            return;
        }

    }
    protected void LnkBtnToMarkAllRead_Click(object sender, EventArgs e)
    {

        int i = 0;
        foreach (DataGridItem row in DataGrid1.Items)
        {
            try
            {
                CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                Label GetItemID = (Label)row.FindControl("lblID");
                if (cb != null && cb.Checked)
                {
                    i = i + 1;

                    if (GetItemID.Text.Contains("task"))
                    {
                        sql = "insert into read_task([task_id],[username])values(" + row.Cells[0].Text + ",'" + Session["admin"].ToString() + "')";
                        DatabaseHelper.executeNonQuery(sql);

                        sql = @"select *  from task_comments where task_id =" + row.Cells[0].Text + " and  tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "')";
                        DataSet ds = DatabaseHelper.getDataset(sql);

                        if (ds != null && ds.Tables[0].Rows.Count > 0)
                        {
                            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)
                            {
                                try
                                {


                                    string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                                    sqlinsert += " values(" + ds.Tables[0].Rows[j]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                                    int intResult = DatabaseHelper.executeNonQuery(sqlinsert);

                                }
                                catch
                                {
                                }
                            }

                        }
                    }

                    else
                    {

                        sql = @"select *  from ClientRequest_Details where RequestId =" + row.Cells[0].Text + " and  CommentId  not in (select  CommentId from read_CR_comments where read_CR_comments.UserName='" + Session["admin"].ToString() + "')";
                        DataSet ds = DatabaseHelper.getDataset(sql);

                        if (ds != null && ds.Tables[0].Rows.Count > 0)
                        {
                            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)
                            {
                                try
                                {


                                    string sqlinsert = "insert into read_CR_comments([CommentId],[UserName]) ";
                                    sqlinsert += " values(" + ds.Tables[0].Rows[j]["CommentId"].ToString() + ",'" + Session["admin"].ToString() + "')";

                                    int intResult = DatabaseHelper.executeNonQuery(sqlinsert);

                                }
                                catch
                                {
                                }
                            }

                        }

                    }
                }
            }

            catch { }
        }
        if (i > 0)
        {
            Response.Redirect("Related.aspx", false);
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('Please select task to mark as read.');</script>");

            return;

        }
    }
    protected void LnkBtnToChngPR_Click(object sender, EventArgs e)
    {

        int i = 0;
        foreach (DataGridItem row in DataGrid1.Items)
        {
            try
            {
                CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                Label GetItemID = (Label)row.FindControl("lblID");
                if (cb != null && cb.Checked)
                {
                    i = i + 1;
                    if (GetItemID.Text.Contains("task"))
                    {

                        sql = "Update tasks set priority='" + DrpMultipleTaskPR.SelectedItem.Text.Replace("'", "''") + "',last_updated_date =getdate(),last_updated_user='" + Session["admin"].ToString().Replace("'", "''") + "' where task_id=" + row.Cells[0].Text;

                        int intResult = DatabaseHelper.executeNonQuery(sql);

                        if (intResult != 0)
                        {
                            string cmnt = "";
                            cmnt = "changed priority from " + row.Cells[8].Text + " to " + "1c";


                            sql = @" insert into task_updates([task_id],[username],[post_date],[comment]) 
                            values(" + row.Cells[0].Text + ",'" + Session["admin"].ToString().Replace("'", "''") + "',getdate(),'" + cmnt.ToString() + "'); ";

                            intResult = DatabaseHelper.executeNonQuery(sql);

                        }
                    }
                    else
                    {
                        string setPR = "";
                        if (DrpMultipleTaskPR.SelectedValue.ToString() == "6")
                        {
                            setPR = "0";

                        }
                        else
                        {
                            setPR = DrpMultipleTaskPR.SelectedValue.ToString();
                        }
                        sql = "update ClientRequest set Priority='" + setPR + "',LastUpdatedOn = getdate() where [RequestId] =" + row.Cells[0].Text;

                        int intResult = DatabaseHelper.executeNonQuery(sql);
                    }

                }
            }
            catch { }
        }
        if (i > 0)
        {
            Response.Redirect("Related.aspx", false);
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('Please select task to change priority.');</script>");

            return;

        }
    }
    protected void LnkChngAssingedTo_Click(object sender, EventArgs e)
    {
        int i = 0;
        foreach (DataGridItem row in DataGrid1.Items)
        {
            try
            {
                CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                Label GetItemID = (Label)row.FindControl("lblID");
                if (cb != null && cb.Checked)
                {
                    i = i + 1;
                    if (GetItemID.Text.Contains("task"))
                    {

                        sql = "Update tasks set assigned_to_user='" + DrpMultipleTaskAssing.SelectedItem.Text.Replace("'", "''") + "',last_updated_date =getdate(),last_updated_user='" + Session["admin"].ToString().Replace("'", "''") + "' where task_id=" + row.Cells[0].Text;

                        int intResult = DatabaseHelper.executeNonQuery(sql);

                        if (intResult != 0)
                        {
                            string cmnt = "";
                            cmnt = "changed assinged to from " + row.Cells[9].Text + " to  " + DrpMultipleTaskAssing.SelectedItem.Text.Replace("'", "''");


                            sql = @" insert into task_updates([task_id],[username],[post_date],[comment]) 
                            values(" + row.Cells[0].Text + ",'" + Session["admin"].ToString().Replace("'", "''") + "',getdate(),'" + cmnt.ToString() + "'); ";

                            intResult = DatabaseHelper.executeNonQuery(sql);

                        }
                    }

                }
            }
            catch { }
        }
        if (i > 0)
        {
            Response.Redirect("Related.aspx", false);
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('Please select task to change assigned to.');</script>");
            return;

        }

    }
    protected void LnkChngStatus_Click(object sender, EventArgs e)
    {

        int i = 0;
        foreach (DataGridItem row in DataGrid1.Items)
        {
            // Access the CheckBox 
            try
            {
                CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                Label GetItemID = (Label)row.FindControl("lblID");
                if (cb != null && cb.Checked)
                {
                    i = i + 1;
                    if (GetItemID.Text.Contains("task"))
                    {

                        sql = "Update tasks set status='" + DrpMultipleTaskStatus.SelectedItem.Text.Replace("'", "''") + "',last_updated_date =getdate(),last_updated_user='" + Session["admin"].ToString().Replace("'", "''") + "' where task_id=" + row.Cells[0].Text;

                        int intResult = DatabaseHelper.executeNonQuery(sql);

                        if (intResult != 0)
                        {
                            string cmnt = "";
                            cmnt = "changed status to from " + row.Cells[6].Text + " to  " + DrpMultipleTaskAssing.SelectedItem.Text.Replace("'", "''");


                            sql = @" insert into task_updates([task_id],[username],[post_date],[comment]) 
                            values(" + row.Cells[0].Text + ",'" + Session["admin"].ToString().Replace("'", "''") + "',getdate(),'" + cmnt.ToString() + "'); ";

                            intResult = DatabaseHelper.executeNonQuery(sql);

                        }
                    }
                    else
                    {

                        sql = "update ClientRequest set Status = '" + DrpMultipleTaskStatus.SelectedValue.ToString() + "',LastUpdatedOn = getdate() where [RequestId] =" + row.Cells[0].Text;

                        int intResult = DatabaseHelper.executeNonQuery(sql);
                    }

                }
            }
            catch { }
        }
        if (i > 0)
        {
            Response.Redirect("Related.aspx", false);
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('Please select task/CR to change status.');</script>");

            return;

        }


    }
}
