<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Resources.aspx.cs" Inherits="Admin_Resources" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        Resources</title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
    function checkProject()
    {
    var project =  document.getElementById("txtName").value;
    var drpprjct = document.getElementById("drpProjects").value;
    var rlevanUrl =  document.getElementById("txtRelevanUrl").value;
    if(drpprjct == "other")
    {
    
    if(project =="")
    {
    alert("Please enter project name");
    return false;
    }
       
    }
    
 /*else if(document.getElementById("chkRelevanturl").checked)
  
    {
     alert("start");
    if(rlevanUrl == "")
    {
    alert("start2");
    alert("Please enter relevant url");
    return false;
    }
    
    }*/
   else
    {
    var btnsend = document.getElementById("btnAddProject");
btnsend.click(); 
    }
    
    }
    
    </script>

    <script language="javascript" type="text/javascript">
function showrelevanturl()
{


if(document.getElementById("chkRelevanturl").checked)
 {

 document.getElementById("TrRelevantUrl").style.display = "Block";
 }
  

  else
  {
   document.getElementById("TrRelevantUrl").style.display = "none";
   return false;
  }

}
  
  

    </script>

    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <uc2:Admin_Header ID="Admin_Header1" runat="server" />
        <div id="Content">
            <div class="titleText" style="margin: 10px 0 10px 0; display: none;">
                Resources<span class="text_default" style="display: none;"><a href="admin.aspx">&nbsp;|&nbsp;Back
                    to admin</a> </span>
            </div>
            <div id="DivEntry" runat="server">
                <table cellpadding="5" width="69%">
                    <tr id="TrRelevantUrl">
                        <td align="left" valign="top" style="font-weight: bold; font-size: 14" colspan="4">
                            Content
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" colspan="4" style="height: 478px">
                        <%--*************************************************************************************************************--%>
                            <%--<FTB:FreeTextBox ID="FtbResources" runat="server" Height="450px" Width="100%"></FTB:FreeTextBox>--%>
                            <telerik:RadEditor ID="FtbResources" runat="server" Height="300px" Width="100%">
                                        <Snippets>
                                            <telerik:EditorSnippet Name="Product Template" Value="<table><tr><td><div style='width:300px;border:2px outset #D8D2BD;padding:34px;font-family:Arial, Verdana; font-size:11px;background- color:#F1EFE6;'>Dear ____________________,<br>Thank you for inquiring about ____________. Your request will be processed in 48 hours and shipped at the address you have provided.<b>Please, contact us if you have any problems.</b></div></td></tr></table>">            
                                            </telerik:EditorSnippet>
                                            <telerik:EditorSnippet Name="Signature" Value="<strong>John Smith</strong><br>Sales Manager<br>">
                                            </telerik:EditorSnippet>
                                        </Snippets>
                                        <Links>
                                            <telerik:EditorLink Name="Telerik" Href="http://www.telerik.com">
                                                <telerik:EditorLink Name="Telerik Products" Href="http://www.telerik.com/products"
                                                    Target="_blank" />
                                                <telerik:EditorLink Name="Telerik Support Center" Href="http://www.telerik.com/support/home.aspx"
                                                    Target="_blank" />
                                                <telerik:EditorLink Name="Telerik Community" Href="http://www.telerik.com/community/home.aspx"
                                                    Target="_blank" ToolTip="Telerik Community" />
                                            </telerik:EditorLink>
                                        </Links>
                                        <Tools>
                                        </Tools>
                                        <ImageManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                        <MediaManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                        <FlashManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                        <TemplateManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                        <DocumentManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                        <Modules>
                                            <telerik:EditorModule Name="RadEditorStatistics" Visible="False" Enabled="true" />
                                            <telerik:EditorModule Name="RadEditorDomInspector" Visible="False" Enabled="true" />
                                            <telerik:EditorModule Name="RadEditorNodeInspector" Visible="False" Enabled="true" />
                                            <telerik:EditorModule Name="RadEditorHtmlInspector" Visible="False" Enabled="true" />
                                        </Modules>
                                        <Content>
                                        </Content>
                                    </telerik:RadEditor>
                            
                        <%--*************************************************************************************************************--%>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="width: 114px">
                        </td>
                        <td>
                        </td>
                        <td align="right" valign="top" colspan="2">
                            <asp:Button ID="btnCreate" runat="server" Text="Create" OnClick="btnCreate_Click"
                                CssClass="blueBtns" />
                        </td>
                    </tr>
                </table>
            </div>
            <div id="divMessage" runat="server" style="color: Red; font-weight: bold">
            </div>
            <br />
            <div>
            </div>
        </div>
    </div>
    <uc1:footer ID="Footer1" runat="server" />
    </form>
</body>
</html>
