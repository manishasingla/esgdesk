using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Admin_Resources : System.Web.UI.Page
{
    int id;
    string sql = "";
    protected void Page_Load(object sender, EventArgs e)
    {

    
        ////Page.RegisterClientScriptBlock("Onload", "showrelevanturl();");

        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = "Resources.aspx";
            Response.Redirect("login.aspx");
            return;
        }

        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            Response.Redirect("tasks.aspx");
            return;
        }

       

        if (!IsPostBack)
        {
            string sqlResources = "select [Resources] from [Resources]  where status = 'to check'";
            object objResources = DatabaseHelper.executeScalar(sqlResources);

            try
            {
                if (objResources.ToString() != "")
                {
                    //*************************************************************
                    //FtbResources.Text = objResources.ToString();
                    FtbResources.Content = objResources.ToString();
                    //*************************************************************
                }
                else
                {
                    //*************************************************************
                    //FtbResources.Text = "";
                      FtbResources.Content  = "";
                    //*************************************************************
                }
            }
            catch
            {
                //*************************************************************
                //FtbResources.Text = "";
                  FtbResources.Content = "";
                //*************************************************************
            }

          
           
        }
        else
        {
           
        }
    }
    

    

    protected void btnCreate_Click(object sender, EventArgs e)
    {


        object objResult = DatabaseHelper.executeScalar("select count(*) from Resources where [status]='to check'");

        if (objResult.ToString() != "0")
        {
            string strSql = " update Resources ";
            //*************************************************************
            //strSql += " set [Resources]= '" + FtbResources.Text.Trim().Replace("'", "''") + "'";
            strSql += " set [Resources]= '" + FtbResources.Content.Trim().Replace("'", "''") + "'";
            //*************************************************************
            strSql += " where status = 'to check'";
                 int intResult = DatabaseHelper.executeNonQuery(strSql);

                 if (intResult != 0)
                 {

                     divMessage.InnerHtml = "Resources  updated.";
                 }
                 else
                 {
                     divMessage.InnerHtml = "Resources  not updated.";
                 }
                 Response.Redirect("Resources.aspx", false);
        }
        else
        {
            //*************************************************************
             //string strSql = "insert into Resources (status,Resources)values ('to check','" + FtbResources.Text.Trim().Replace("'", "''") + "')";
               string strSql = "insert into Resources (status,Resources)values ('to check','" + FtbResources.Content.Trim().Replace("'", "''") + "')";
            //*************************************************************
           int intResult = DatabaseHelper.executeNonQuery(strSql);

           if (intResult != 0)
           {

               divMessage.InnerHtml = "Resources  saved.";
           }
           else
           {
               divMessage.InnerHtml = "Resources  not saved.";
           }

        }


        

           
       
    }
    
  
   
}
