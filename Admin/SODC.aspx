﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SODC.aspx.cs" Inherits="Admin_SODC" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Src="Notifications.ascx" TagName="Notifications" TagPrefix="uc4" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        SODC form</title>
        <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
    
</head>
<body>
    <form id="form1" runat="server">
    
    <div>
    <uc2:Admin_Header ID="Admin_Header1" runat="server" />
        <br />
        <div>
            <uc4:Notifications ID="Notifications1" runat="server" />
            <br />
            <br />
            
        </div>
        <table>
            <tr>
                <td align="left" valign="middle" style="width: 197px; height: 29px;">
                    Project:
                </td>
                <td align="left" valign="middle" >
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <asp:DropDownList ID="drppro0" runat="server" Width="205px" AutoPostBack="True" OnSelectedIndexChanged="drppro0_SelectedIndexChanged">
                                   
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqdrppro0" runat="server" ErrorMessage="*" InitialText="Select" ControlToValidate="drppro0"></asp:RequiredFieldValidator>
                            </td>
                           
                            <td>
                                Task id:
                            </td>
                            <td>
                                <asp:DropDownList ID="drpTask0" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpTask0_SelectedIndexChanged">
                                
                                </asp:DropDownList>
                           </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    Comment no.
                </td>
                <td>
                    <asp:DropDownList ID="drpcommentno" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpcommentno_SelectedIndexChanged">
                    
                    </asp:DropDownList>
                   
                </td>
            </tr>
             <tr>
                <td align="left" height="28" style="width: 198px" valign="middle">
                    Comment description:
                </td>
                <td align="left" valign="middle">
                    <%--<asp:Label ID="txtdesc0" runat="server" ForeColor="black" Rows="2" 
                        Width="557px" Height="104px"></asp:Label>--%>
                    <div style="border: 1px solid #000; height: 100px;overflow-y: scroll;padding:5px;" id="txtdesc0"
                        runat="server">
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Brief description
                </td>
                <td>
                    <telerik:RadEditor ID="ftpcomment" runat="server">
                    </telerik:RadEditor>
                </td>
            </tr>
            <tr>
            <td>Any doubt</td>
            <td>
            <asp:DropDownList ID="drpdoubt" runat="server" AutoPostBack="true">
            <asp:ListItem Value="No">No</asp:ListItem> 
            <asp:ListItem Value="Yes">Yes</asp:ListItem>   
            </asp:DropDownList>
            </td>
            </tr>
            <tr>
                <td>
                    Time remaining to complete
                </td>
                <td>
                    <asp:DropDownList ID="drophour" runat="server" AutoPostBack="true">
                        <asp:ListItem Value="0">Select hour</asp:ListItem>
                        <asp:ListItem Value="1">1</asp:ListItem>
                        <asp:ListItem Value="2">2</asp:ListItem>
                        <asp:ListItem Value="3">3</asp:ListItem>
                        <asp:ListItem Value="4">4</asp:ListItem>
                        <asp:ListItem Value="5">5</asp:ListItem>
                        <asp:ListItem Value="6">6</asp:ListItem>
                        <asp:ListItem Value="7">7</asp:ListItem>
                        <asp:ListItem Value="8">8</asp:ListItem>
                        <asp:ListItem Value="9">9</asp:ListItem>
                    </asp:DropDownList>
                    <asp:DropDownList ID="droptime" runat="server">
                        <asp:ListItem Value="0">Select minute </asp:ListItem>
                        <asp:ListItem Value="5">5</asp:ListItem>
                        <asp:ListItem Value="10">10</asp:ListItem>
                        <asp:ListItem Value="15">15</asp:ListItem>
                        <asp:ListItem Value="20">20</asp:ListItem>
                        <asp:ListItem Value="25">25</asp:ListItem>
                        <asp:ListItem Value="30">30</asp:ListItem>
                        <asp:ListItem Value="35">35</asp:ListItem>
                        <asp:ListItem Value="40">40</asp:ListItem>
                        <asp:ListItem Value="45">45</asp:ListItem>
                        <asp:ListItem Value="50">50</asp:ListItem>
                        <asp:ListItem Value="55">55</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="left" valign="middle" style="width: 197px; height: 29px;">
                    Next project:
                </td>
                <td align="left" valign="middle" style="height: 29px; width: 563px;">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="width: 208px; height: 20px">
                                <asp:DropDownList ID="Drpproject1" runat="server" Width="205px" AutoPostBack="True"
                                    OnSelectedIndexChanged="Drpproject1_SelectedIndexChanged">
                                    
                                </asp:DropDownList>
                            </td>
                            <td style="width: 10px">
                            </td>
                            <td>
                              Next Task id:
                            </td>
                            <td style="height: 20px">
                                <asp:DropDownList ID="drpTask11" runat="server" AutoPostBack="True" onselectedindexchanged="drpTask11_SelectedIndexChanged">
                                
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
            <td>
             Next task short description
            </td>
            <td>
                    <div style="border: 1px solid #000; height: 100px;overflow-y: scroll;padding:5px;" id="divforshortdesc" runat="server">
                    </div>
            </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Button ID="btnsave" runat="server" Text="Save" OnClick="btnsave_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="lbldesc" runat="server" />
                </td>
            </tr>
        </table>
    </div>
    <uc1:footer ID="Footer1" runat="server" />
    </form>
</body>
</html>
