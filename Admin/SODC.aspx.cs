﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Admin_SODC : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = "SODC.aspx";
            Response.Redirect("login.aspx");
            return;
        }

        //===================================================
        if (!Page.IsPostBack)
        {
            try
            {
            BindallProject();
            BindallNextProject();
            }
            catch(Exception ex)
            {
                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at Query SODC PageLoad: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Sql Query :Nothing" );
            }
        }
        //===================================================

    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        //string strSODC="select max(Sodc_date) as MaxdateOfUser from SODC where Username='"+ Session["admin"]+"'";
         DataSet dschkcount=null;
         string strquery = "select * from SODC where Username='" + Session["admin"].ToString() + "'";
        try
        {
            dschkcount = DatabaseHelper.getDataset(strquery);
        }
        catch(Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at SODC : btnsave_Click  " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Sql Query :" + strquery);
        }
        if (dschkcount != null && dschkcount.Tables.Count > 0 && dschkcount.Tables[0].Rows.Count > 0)
        {
            insertEodc();
        }
        else
        {
            string insstring = "";
            insstring = "Insert into SODC(task_Id,Projectname,Comment_no,Short_description,Brief_description,Any_doubts,Time_remaining_hour,Time_remaining_minute,Next_task_id,Next_projectname,Sodc_date,Username)";
            //insstring+="values('" + drpTask0.SelectedItem.Text  + "','" + drppro0.SelectedItem.Text  + "','" + drpcommentno.SelectedItem.Text + "','"+txtdesc0.InnerHtml+"','"+ ftpcomment.Content +"','"+drpdoubt.SelectedItem.Text+"','"+drophour.SelectedItem.Text+"','"+droptime.SelectedItem.Text +"','"+drpTask11+"','"+Drpproject1.SelectedItem.Text+"',getdate(),'"+Session["admin"].ToString() +"')";
            insstring += "values('" + drpTask0.SelectedValue + "','" + drppro0.SelectedValue + "','" + drpcommentno.SelectedValue + "','" + txtdesc0.InnerHtml.ToString().Replace("'", "''") + "','" + ftpcomment.Content.ToString().Replace("'", "''") + "','" + drpdoubt.SelectedValue + "','" + drophour.SelectedValue + "','" + droptime.SelectedValue + "','" + drpTask11.SelectedValue + "','" + Drpproject1.SelectedValue + "',getdate(),'" + Session["admin"].ToString()  + "')";
            int result = 0;
            try
            {
                result = DatabaseHelper.executeNonQuery(insstring);
                if (result > 0)
                {
                    lbldesc.Text = "For today your SODC successfully send";
                }
            }catch(Exception ex)
            {
                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at SODC : btnsave_Click  " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Sql Query :" + insstring);
            }
        }
    }
    public void insertEodc()
    {
        string strSODC = "select max(Sodc_date) as MaxdateOfUser from SODC where Username='" + Session["admin"].ToString() + "'";
         DataSet dsdate=null;
        try
        {
            dsdate = DatabaseHelper.getDataset(strSODC);
        }catch(Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at SODC : insertEodc()  " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Sql Query :" + strSODC);
        }
        if (dsdate != null && dsdate.Tables.Count > 0 && dsdate.Tables[0].Rows.Count > 0)
        {
            DateTime dt = Convert.ToDateTime(dsdate.Tables[0].Rows[0]["MaxdateOfUser"]);
            int dtday = 0, dtmonth = 0, dtyear = 0, Tday = 0, Tmonth = 0, Tyear = 0;
            dtday = dt.Day;
            dtmonth = dt.Month;
            dtyear = dt.Year;
            DateTime Todaydt = DateTime.Now;
            Tday = Todaydt.Day;
            Tmonth = Todaydt.Month;
            Tyear = Todaydt.Year;

            if (dtday == Tday && dtmonth == Tmonth && dtyear == Tyear)
            {

                lbldesc.Text = "For today your SODC already send";
            }
            else
            {

                string insstring = "";
                insstring = "Insert into SODC(task_Id,Projectname,Comment_no,Short_description,Brief_description,Any_doubts,Time_remaining_hour,Time_remaining_minute,Next_task_id,Next_projectname,Sodc_date,Username)";
                //insstring+="values('" + drpTask0.SelectedItem.Text  + "','" + drppro0.SelectedItem.Text  + "','" + drpcommentno.SelectedItem.Text + "','"+txtdesc0.InnerHtml+"','"+ ftpcomment.Content +"','"+drpdoubt.SelectedItem.Text+"','"+drophour.SelectedItem.Text+"','"+droptime.SelectedItem.Text +"','"+drpTask11+"','"+Drpproject1.SelectedItem.Text+"',getdate(),'"+Session["admin"].ToString() +"')";
                insstring += "values('" + drpTask0.SelectedValue + "','" + drppro0.SelectedValue + "','" + drpcommentno.SelectedValue + "','" + txtdesc0.InnerHtml.ToString().Replace("'", "''") + "','" + ftpcomment.Content.ToString().Replace("'", "''") + "','" + drpdoubt.SelectedValue + "','" + drophour.SelectedValue + "','" + droptime.SelectedValue + "','" + drpTask11.SelectedValue + "','" + Drpproject1.SelectedValue + "',getdate(),'" + Session["admin"].ToString() + "')";
                try
                {
                    int result = 0;
                    result = DatabaseHelper.executeNonQuery(insstring);
                    if (result > 0)
                    {
                    lbldesc.Text = "For today your SODC successfully send";
                    
                    }
                }catch(Exception ex)
                {
                    DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at SODC : insertEodc() on insert value  " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Sql Query :" + insstring);
                }
            }
        }


    }

    public void BindallProject()
    {
        DataSet dsProject = new DataSet();
        try
        {
            dsProject = DatabaseHelper.getProjectDs();
        }catch(Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at SODC : BindallProject()  " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Sql Query:Nothing");
        }
        if (dsProject != null && dsProject.Tables.Count > 0 && dsProject.Tables[0].Rows.Count > 0)
        {
            try
            {
                drppro0.DataSource = dsProject.Tables[0];
                drppro0.DataTextField = "project_name";
                drppro0.DataValueField = "project_name";
                drppro0.DataBind();
            }catch(Exception ex)
            {
                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at SODC : BindallProject()  " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Sql Query:Nothing");
            }
        }
    }
    public void BindallNextProject()
    {
        DataSet dsProject = new DataSet();
        try
        {
            dsProject = DatabaseHelper.getProjectDs();
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at SODC : BindallNextProject()  " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Sql Query:Nothing");
        }
        if (dsProject != null && dsProject.Tables.Count > 0 && dsProject.Tables[0].Rows.Count > 0)
        {
            try
            {
                Drpproject1.DataSource = dsProject.Tables[0];
                Drpproject1.DataTextField = "project_name";
                Drpproject1.DataValueField = "project_name";
                Drpproject1.DataBind();
            }catch(Exception ex)
            {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at SODC : BindallNextProject() on Binding()  " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Sql Query:Nothing");
            }
            
        }
    }

    protected void drpTask0_SelectedIndexChanged(object sender, EventArgs e)
    {
        string commentid = @"select * from task_comments where deleted<>1 and qflag<>1 and task_id='" + drpTask0.SelectedItem.Text + "'";
        DataSet dscomment=null;
        try
        {
            dscomment = DatabaseHelper.getDataset(commentid);
        }catch( Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at SODC : drpTask0_SelectedIndexChanged()  " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Sql Query:" + commentid);
        }
        if (dscomment != null && dscomment.Tables.Count > 0 && dscomment.Tables[0].Rows.Count > 0)
        {
            try
            {
                drpcommentno.DataSource = dscomment.Tables[0];
                drpcommentno.DataTextField = "tc_id";
                drpcommentno.DataValueField = "tc_id";
                drpcommentno.DataBind();
                drpcommentno.Items.Insert(0, new ListItem("Select", "0"));
            }catch(Exception ex)
            {
                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at SODC : drpTask0_SelectedIndexChanged() on Databind() " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Sql Query:" + commentid);
            }
        }

    }
    protected void drppro0_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataSet dsfortaskid=null;
        string strtask = "";
        try
        {
            //dsfortaskid = DatabaseHelper.getTaskdataset(drppro0.SelectedItem.Text);
            strtask = "select cast(task_id as Varchar) as task_id from tasks where deleted <>'1' and project ='" + drppro0.SelectedItem.Text + "' order by tasks.task_id";
            dsfortaskid = DatabaseHelper.getDataset(strtask);
        }catch(Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at SODC : drppro0_SelectedIndexChanged()  " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Sql Query:" + strtask);
        }
        if (dsfortaskid != null && dsfortaskid.Tables.Count > 0 && dsfortaskid.Tables[0].Rows.Count > 0)
        {
            try
            {
                drpTask0.DataSource = dsfortaskid.Tables[0];
                drpTask0.DataTextField = "task_id";
                drpTask0.DataValueField = "task_id";
                drpTask0.DataBind();
                drpTask0.Items.Insert(0, new ListItem("Select", "0"));
            }catch( Exception ex)
            {
                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at SODC : drppro0_SelectedIndexChanged()  on Databind()  " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Sql Query:" + drppro0.SelectedItem.Text);
            }
            
        }
    }
    protected void drpcommentno_SelectedIndexChanged(object sender, EventArgs e)
    {
        string commentDesc = @"select * from task_comments where deleted<>1 and qflag<>1 and tc_id='" + drpcommentno.SelectedItem.Text + "'";
        DataSet dscommentdesc=null;
        try
        {
            dscommentdesc = DatabaseHelper.getDataset(commentDesc);
        }catch(Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at SODC : drppro0_SelectedIndexChanged()  " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Sql Query:" + commentDesc);
        }
        
        if (dscommentdesc != null && dscommentdesc.Tables.Count > 0 && dscommentdesc.Tables[0].Rows.Count > 0)
        {
            try
            {
                txtdesc0.InnerHtml = Server.HtmlDecode(dscommentdesc.Tables[0].Rows[0]["comment"].ToString());
            }catch(Exception ex)
            {
                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at SODC : drpcommentno_SelectedIndexChanged() on assign comment " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Sql Query:" + commentDesc);
            }
        }
    }
    protected void Drpproject1_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataSet dsfortaskid =null;
        string strassigntaskid = "";

        try
        {
            strassigntaskid="select cast(task_id as Varchar) as task_id from tasks where deleted <>'1' and project ='" + Drpproject1.SelectedItem.Text + "' order by tasks.task_id";
            dsfortaskid=DatabaseHelper.getDataset(strassigntaskid) ;
            //dsfortaskid= DatabaseHelper.getTaskdataset(Drpproject1.SelectedItem.Text);
        }catch(Exception ex)
        {
        DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at SODC : Drpproject1_SelectedIndexChanged()  " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Sql Query:" + Drpproject1.SelectedItem.Text);
        }
        if (dsfortaskid != null && dsfortaskid.Tables.Count > 0 && dsfortaskid.Tables[0].Rows.Count > 0)
        {
            try
            {
                drpTask11.DataSource = dsfortaskid.Tables[0];
                drpTask11.DataTextField = "task_id";
                drpTask11.DataValueField = "task_id";
                drpTask11.DataBind();
                drpTask11.Items.Insert(0, new ListItem("Select", "0"));
            }catch(Exception ex)
            {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at SODC : Drpproject1_SelectedIndexChanged() on Bind()  " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Sql Query:" + Drpproject1.SelectedItem.Text);
            }
        }
    }
    protected void drpTask11_SelectedIndexChanged(object sender, EventArgs e)
    {
        try{
        DataSet dsnexrtask = DatabaseHelper.getDataset("Select * from tasks where task_id='" + drpTask11.SelectedValue.ToString() + "'");
        if (dsnexrtask != null && dsnexrtask.Tables.Count > 0 && dsnexrtask.Tables[0].Rows.Count > 0)
        {
            divforshortdesc.InnerHtml = dsnexrtask.Tables[0].Rows[0]["short_desc"].ToString();
        }
        }catch(Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at SODC : drpTask11_SelectedIndexChanged()  " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Sql Query:" + drpTask11.SelectedValue.ToString());
        }
    }
}
