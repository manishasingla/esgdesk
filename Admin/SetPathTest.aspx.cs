﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
using System.Web.Security;
using System.Globalization;
using System.Net;
using System.Net.NetworkInformation;
using System.IO;
using Telerik.Web.UI;
using Telerik.Charting;
//using System.Web.UI;
//using System.Data;
//using System.Data.SqlClient;
//using System.Drawing;
//using System.Web;
//using System.Web.SessionState;

//using System.Web.UI.WebControls;
//using System.Web.UI.HtmlControls;

public partial class Admin_SetPathTest : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //string test = "Avinash Joshi";
        //string str = HttpContext.Current.Server.MapPath("//Attachment")+"\\ErrorMessage.txt";
        //TraceServiceWrite(test, str);
        DataTable tbl = new DataTable();
        tbl.Columns.Add("Year");
        tbl.Columns.Add("Price");
        tbl.Rows.Add("2007", 33);
        tbl.Rows.Add("2008", 42);
        tbl.Rows.Add("2009", 6);
        tbl.Rows.Add("2010", 15);
        tbl.Rows.Add("2011", 24);
        tbl.Rows.Add("2012", 33);
        DataSet dsforStatus = new DataSet();
        dsforStatus.Tables.Add(tbl);

        ChartSeries secondSerie3 = new ChartSeries();
        secondSerie3.Name = "Price";
        secondSerie3.Type = ChartSeriesType.Line;
        secondSerie3.DataYColumn = "Price";
        RadChart4.Series.Add(secondSerie3);
        RadChart4.PlotArea.XAxis.DataLabelsColumn = "Year";
        RadChart4.DataSource = dsforStatus;
        RadChart4.DataBind();
        RadChart4.ChartTitle.TextBlock.Text = "Yearly Report (Line Chart)";
        
        
    }
    //public void TraceServiceWrite(string content,string str)
    //{
    //    //set up a filestream
    //    try
    //    {

    //        FileStream fs = new FileStream(str, FileMode.OpenOrCreate, FileAccess.Write);

    //        lblpath.Text = "Not Error Path";
    //        //set up a streamwriter for adding text
    //        StreamWriter sw = new StreamWriter(fs);

    //        //find the end of the underlying filestream
    //        sw.BaseStream.Seek(0, SeekOrigin.End);

    //        //add the text
    //        sw.WriteLine(content);
    //        //add the text to the underlying filestream

    //        sw.Flush();
    //        //close the writer
    //        sw.Close();
    //    }
    //    catch (Exception ex)
    //    {
    //        string strmsg = "";
    //        strmsg = ex.Message;
    //        lblpath.Text = ex.Message;
    //    }

    //}
}
