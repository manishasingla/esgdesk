﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ShowSODC.aspx.cs" Inherits="Admin_ShowSODC" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Src="Notifications.ascx" TagName="Notifications" TagPrefix="uc4" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        Show SODC</title>
        <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <uc2:Admin_Header ID="Admin_Header1" runat="server" />
        <br />
        <div>
            <uc4:Notifications ID="Notifications1" runat="server" />
            <br />
            <br />
            
        </div>
        <table width="100%" border="0">
        <tr>
        <td>
              
        
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Select date: 
                    <asp:TextBox ID="GMDStartDate" runat="server"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="GMDStartDate"
                        Format="dd/MM/yyyy" PopupButtonID="GMDStartDate">
                    </ajaxToolkit:CalendarExtender>
                    <br />
                    <br />
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnshow" runat="server" Text="Show SODC" 
                        onclick="btnshow_Click"  />
                    <br />
                    <br />
                    <br />
               
        
         </td>
         </tr>
            <tr>
                
                <td style="width: 600px" align="left">
                    <table>
                    <tr>
                    <td align="left" >
                    <asp:Label id="lbldate1" runat="server"  Font-Bold="true"></asp:Label>
                    </td>
                    </tr>
                        <tr align="left">
                            <td align="left">
                                <asp:Repeater ID="RepDetails" runat="server">
                                    <HeaderTemplate>
                                        
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <table style="border: solid #df5015; width: 650px">
                                            <tr>
                                                <td>
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr style="background-color: #EBEFF0">
                                                <td>
                                                    <table style="background-color: #EBEFF0; border-top: 1px dotted #df5015; width: 500px">
                                                        <tr>
                                                            <td align="center">
                                                                Employee Name:
                                                                <asp:Label ID="lblSubject" runat="server" Text='<%#Eval("username") %>' Font-Bold="true" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table style="border-top: 1px dotted #df5015; width: 500px">
                                                        <tr>
                                                            <td>
                                                                <span style="font-weight: bold">Task URL:</span> <a id="lbltaskUrl" runat="server"
                                                                    target="_blank" href='<%# "edit_task.aspx?Id="+ HttpUtility.UrlEncode(Eval("task_Id").ToString()) %>'>
                                                                    edit_task.aspx?Id=<%# Eval("task_Id").ToString()%> </a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table style="border-top: 1px dotted #df5015; width: 500px">
                                                        <tr>
                                                            <td>
                                                                <span style="font-weight: bold">Project Name:</span>
                                                                <asp:Label ID="Label1" runat="server" Text='<%#Eval("Projectname") %>' />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table style="border-top: 1px dotted #df5015; width: 500px">
                                                        <tr>
                                                            <td>
                                                                <span style="font-weight: bold">Comment no.:</span>
                                                                <asp:Label ID="Label2" runat="server" Text='<%#Eval("Comment_no") %>' />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <span style="font-weight: bold">Short Description:</span>
                                                                <asp:Label ID="Label3" runat="server" Text='<%#Eval("Short_description") %>' />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table style="border-top: 1px dotted #df5015; width: 500px">
                                                        <tr>
                                                            <td>
                                                                <span style="font-weight: bold">Any doubts:</span>
                                                                <asp:Label ID="Label4" runat="server" Text='<%#Eval("Any_doubts") %>' />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table style="border-top: 1px dotted #df5015; width: 500px">
                                                        <tr>
                                                            <td>
                                                                <span style="font-weight: bold">Time remaining to complete the task:</span>
                                                                <asp:Label ID="Label5" runat="server" Text='<%#Eval("Time_remaining_hour") %>' />
                                                                Hour
                                                                <asp:Label ID="Label6" runat="server" Text='<%#Eval("Time_remaining_minute") %>' />
                                                                Minute
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <span style="font-weight: bold">Brief description of current task:</span>
                                                                <asp:Label ID="Label7" runat="server" Text='<%#Eval("Brief_description") %>'  />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table style="border-top: 1px dotted #df5015; width: 500px">
                                                        <tr>
                                                            <td>
                                                                <span style="font-weight: bold">Next task details:</span> <a id="A1" runat="server"
                                                                    target="_blank" href='<%# "edit_task.Aspx?Id="+ HttpUtility.UrlEncode(Eval("Next_task_id").ToString()) %>'>
                                                                    "edit_task.Aspx?Id=<%# Eval("Next_task_id")%> </a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                    
                   <%--=====================================================================================================--%>         
                    
                    
                    
                    <table>
                        <tr align="center">
                            <td align="center">
                                <asp:Repeater ID="Repeater1" runat="server">
                                    <HeaderTemplate>
                                        <b>Not send SODC</b>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <table style="border: solid #df5015; width: 650px">
                                            <tr>
                                                <td>
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr style="background-color: #EBEFF0">
                                                <td>
                                                    <table style="background-color: #EBEFF0; border-top: 1px dotted #df5015; width: 500px">
                                                        <tr>
                                                            <td align="center">
                                                                Employee Name:
                                                                <asp:Label ID="lblSubject" runat="server" Text='<%#Eval("Employee_Name") %>' Font-Bold="true" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                               
                                        </table>
                                        <br />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                    
                    
                    <%--=====================================================================================================--%>    
                    
                    
                </td>
               
            </tr>
        </table>
    </div>
    <uc1:footer ID="Footer1" runat="server" />
    </form>
</body>
</html>
