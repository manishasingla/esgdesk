﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Admin_ShowSODC : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = "ShowSODC.aspx";
            Response.Redirect("login.aspx");
            return;
        }
        if (!Page.IsPostBack)
        {


            DataSet ds = null, DsnotSODC = null;
            string todaysodc = "Select * from SODC where Day(Sodc_date)=Day(GETDATE()) and  MONTH(Sodc_date)=MONTH(GETDATE()) and YEAR(Sodc_date)=YEAR(GETDATE()) order by Username";
            ds = DatabaseHelper.getDataset(todaysodc);
            RepDetails.DataSource = ds;
            RepDetails.DataBind();

            string todaynotSend = @"select firstname +' '+ lastname  as Employee_Name from users  where IsCW='Y' and active='Y' and username not in 
               (
                 select username from SODC
                 where Day(Sodc_date)=Day(GETDATE()) and 
                 MONTH(Sodc_date)=MONTH(GETDATE()) and YEAR(Sodc_date)=YEAR(GETDATE()) 
               )";

            DsnotSODC = DatabaseHelper.getDataset(todaynotSend);
            Repeater1.DataSource = DsnotSODC;
            Repeater1.DataBind();
        }
        lbldate1.Text = "Today ( " + System.DateTime.Now.ToShortDateString() + " ) SODC";
    }
    protected void btnshow_Click(object sender, EventArgs e)
    {

        if (GMDStartDate.Text.ToString() != "" && GMDStartDate.Text.ToString() != null)
        {
            DataSet ds = null, DsnotSODC = null;
            string todaysodc1 = "Select * from SODC where Day(Sodc_date)=Day(convert(Datetime,'" + GMDStartDate.Text + "',103)) and  MONTH(Sodc_date)=MONTH(convert(Datetime,'" + GMDStartDate.Text + "',103)) and YEAR(Sodc_date)=YEAR(convert(Datetime,'" + GMDStartDate.Text + "',103)) order by Username";
            ds = DatabaseHelper.getDataset(todaysodc1);
            RepDetails.DataSource = ds;
            RepDetails.DataBind();

            string todaynotSend = @"select firstname +' '+ lastname  as Employee_Name from users  where IsCW='Y' and active='Y' and username not in 
               (select username from SODC where Day(Sodc_date)=Day(convert(Datetime,'" + GMDStartDate.Text + "',103)) and MONTH(Sodc_date)=MONTH(convert(Datetime,'" + GMDStartDate.Text + "',103)) and YEAR(Sodc_date)=YEAR(convert(Datetime,'" + GMDStartDate.Text + "',103)))";

            DsnotSODC = DatabaseHelper.getDataset(todaynotSend);
            Repeater1.DataSource = DsnotSODC;
            Repeater1.DataBind();
        }
        else 
        {

            DataSet ds = null, DsnotSODC = null;
            string todaysodc1 = "Select * from SODC where Day(Sodc_date)=Day(convert(Datetime,getdate(),103)) and  MONTH(Sodc_date)=MONTH(convert(Datetime,getdate(),103)) and YEAR(Sodc_date)=YEAR(convert(Datetime,getdate(),103)) order by Username";
            ds = DatabaseHelper.getDataset(todaysodc1);
            RepDetails.DataSource = ds;
            RepDetails.DataBind();

            string todaynotSend = @"select firstname +' '+ lastname  as Employee_Name from users  where IsCW='Y' and active='Y' and username not in 
               (select username from SODC where Day(Sodc_date)=Day(convert(Datetime,getdate(),103)) and MONTH(Sodc_date)=MONTH(convert(Datetime,getdate(),103)) and YEAR(Sodc_date)=YEAR(convert(Datetime,getdate(),103)))";

            DsnotSODC = DatabaseHelper.getDataset(todaynotSend);
            Repeater1.DataSource = DsnotSODC;
            Repeater1.DataBind();
        
        
        
        
        }
    }
}
