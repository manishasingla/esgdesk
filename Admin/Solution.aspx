﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Solution.aspx.cs" Inherits="Admin_Solution" %>

<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Solution</title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../DashStyle.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <uc2:Admin_Header ID="Admin_Header1" runat="server" />
    <div>
        <center>
            <div id="divmsg" runat="server" class="pagearea" style="width: 500px; padding: 100px;"
                visible="false">
                This user don't have rights for private solutios
            </div>
        </center>
        <div id="inner" runat="server">
            <div class="pagearea" style="width: 80%; float: left">
                <div id="TicketSummary">
                    <%--<h3 style="font-size: 16px; font-weight: bold; padding: 5px 0">--%>
                    <%--<h3 style="font-size: 16px; font-weight: bold; padding: 5px 0px 5px 6px; background-color: rgb(62, 63, 61);
                        color: white; border-top: 4px solid rgb(0, 166, 81);">
                        Solutions</h3>--%>
                    <div style="text-align: right">
                        <a id="addnewsolution" runat="server" class="submit" href="AddCategory.aspx">New solution
                            category</a>
                    </div>
                    <table>
                        <tr>
                            <td style="padding: 4px">
                                <asp:ListView ID="ListView1" runat="server" GroupItemCount="2" Style="width: 100%">
                                    <ItemTemplate>
                                        <td valign="top">
                                            <table border="0" width="100%" cellspacing="0" cellpadding="0" class="infoBoxContents">
                                                <tr>
                                                    <td align="left" class="smallText" valign="top">
                                                        <div class="even">
                                                            <table style="width: 100%; border-bottom: 1px solid">
                                                                <tr>
                                                                    <td>
                                                                        <div>
                                                                            <h4 class="border_title" style="float: left">
                                                                                <%--<a href="#" class="tooltiptrans">--%>
                                                                                <%--<asp:Label ID="lblCN" runat="server" Text='<%# Eval ("CategoryName") %>'></asp:Label>--%>
                                                                                <a id="lblCN" runat="server" class="tooltiptrans">
                                                                                    <%# Eval ("CategoryName") %>
                                                                                    » <span>
                                                                                        <%# Eval("categorydescription")%>
                                                                                    </span></a>
                                                                            </h4>
                                                                            <asp:HiddenField ID="Hndid" runat="server" Value='<%# Eval ("Category_id") %>' />
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="actionbuttons" style="float: right">
                                                                            <a class="first" href="addcategoryfolder.aspx?categoryid=<%# Eval ("Category_id") %>">
                                                                                <img  src="images/add-icon.png" /></a>|<a class="last" href="AddCategory.aspx?categoryid=<%# Eval ("Category_id") %>"><img   src="images/edit-icon.png" /></a>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <asp:ListView ID="Folderlstview" runat="server">
                                                            <LayoutTemplate>
                                                                <div>
                                                                    <ul style="height: auto; list-style: disc outside none">
                                                                        <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                                                    </ul>
                                                                </div>
                                                            </LayoutTemplate>
                                                            <ItemTemplate>
                                                                <li><a href="Solutionslist.aspx?categoryid=<%#Eval("Category_id")%>&folderid=<%#Eval("folder_id")%>"
                                                                    style="margin-left: 10px;">
                                                                    <%#Eval("foldername")%>
                                                                    (<%# Eval("counts")%>)</a> </li>
                                                            </ItemTemplate>
                                                        </asp:ListView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </ItemTemplate>
                                    <LayoutTemplate>
                                        <table id="groupPlaceholderContainer" runat="server" border="0" style="width: 780px">
                                            <tr id="groupPlaceholder" runat="server">
                                            </tr>
                                        </table>
                                    </LayoutTemplate>
                                    <GroupTemplate>
                                        <tr id="itemPlaceholderContainer" runat="server">
                                            <td id="itemPlaceholder" runat="server">
                                            </td>
                                        </tr>
                                    </GroupTemplate>
                                </asp:ListView>
                                <asp:ListView ID="ListView2" runat="server" GroupItemCount="2" Style="width: 100%">
                                    <ItemTemplate>
                                        <td valign="top">
                                            <table border="0" width="100%" cellspacing="0" cellpadding="0" class="infoBoxContents">
                                                <tr>
                                                    <td align="left" class="smallText" valign="top">
                                                        <div class="even">
                                                            <table style="width: 100%; border-bottom: 1px solid">
                                                                <tr>
                                                                    <td>
                                                                        <div>
                                                                            <h4 class="border_title" style="float: left">
                                                                                <a>
                                                                                    <%--<asp:Label ID="lblCN" runat="server" Text='<%# Eval ("CategoryName") %>'></asp:Label>--%>
                                                                                    <a id="lblCN" runat="server" class="tooltiptrans">
                                                                                        <%# Eval ("CategoryName") %>
                                                                                        » <span>
                                                                                            <%# Eval("categorydescription")%>
                                                                                        </span></a>
                                                                            </h4>
                                                                            <asp:HiddenField ID="Hndid" runat="server" Value='<%# Eval ("Category_id") %>' />
                                                                        </div>
                                                                    </td>
                                                                    <%--<td>
                                                                        <div class="actionbuttons" style="float: right">
                                                                            <a class="first" href="addcategoryfolder.aspx?categoryid=<%# Eval ("Category_id") %>">
                                                                                Add folder</a>|<a class="last" href="AddCategory.aspx?categoryid=<%# Eval ("Category_id") %>">Edit</a>
                                                                        </div>
                                                                    </td>--%>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <asp:ListView ID="Folderlstview" runat="server">
                                                            <LayoutTemplate>
                                                                <div>
                                                                    <ul style="height: auto; list-style: disc outside none">
                                                                        <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                                                    </ul>
                                                                </div>
                                                            </LayoutTemplate>
                                                            <ItemTemplate>
                                                                <li><a href="Solutionslist.aspx?categoryid=<%#Eval("Category_id")%>&folderid=<%#Eval("folder_id")%>"
                                                                    style="margin-left: 10px;">
                                                                    <%#Eval("foldername")%>
                                                                    (<%# Eval("counts")%>)</a> </li>
                                                            </ItemTemplate>
                                                        </asp:ListView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </ItemTemplate>
                                    <LayoutTemplate>
                                        <table id="groupPlaceholderContainer" runat="server" border="0" style="width: 780px">
                                            <tr id="groupPlaceholder" runat="server">
                                            </tr>
                                        </table>
                                    </LayoutTemplate>
                                    <GroupTemplate>
                                        <tr id="itemPlaceholderContainer" runat="server">
                                            <td id="itemPlaceholder" runat="server">
                                            </td>
                                        </tr>
                                    </GroupTemplate>
                                </asp:ListView>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
