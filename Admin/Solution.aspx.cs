﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Admin_Solution : System.Web.UI.Page
{
    string selectedval = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Response.Redirect("login.aspx");
            return;
        }
        else
        {
            if (!DatabaseHelper.isPrivateSolutions(Session["admin"].ToString()))
            {
                //inner.Visible = false;
                //divmsg.Visible = true;
                //return;
                selectedval = "Public";

            }
            else
            {
                selectedval = "Private";

            }
        }
        getcategory();
    }
    void getcategory()
    {
        string sql = "";
        if (selectedval == "Private")
        {
            sql = @"select Category_id,CategoryName,categorydescription from Category ";
        }
        else
        {
            sql = @"select Category_id,CategoryName,categorydescription from Category where VisibleTo='" + selectedval + "'";
        }

        DataSet ds = DatabaseHelper.getallstatus(sql);
        if (ds != null)
        {
            if (ds.Tables != null && ds.Tables[0].Rows.Count > 0)
            {
                if (selectedval == "Private")
                {
                    ListView1.DataSource = ds;
                    ListView1.DataBind();
                    listview1loop();
                }
                else
                {
                    addnewsolution.Visible = false;
                    ListView2.DataSource = ds;
                    ListView2.DataBind();
                    listview2loop();
                }
            }
            else
            {
                inner.Visible = false;
                divmsg.Visible = true;
                return;
            }
        }


    }

    private void listview1loop()
    {
        for (int i = 0; i < ListView1.Items.Count; i++)
        {
            //            string sqlsub = @"select foldername from Category_Folder
            //                       where Category_id  = '" + ((HiddenField)ListView1.Items[i].FindControl("Hndid")).Value.ToString() + "'";

            //            string sqlsub = @"select  COUNT(*)as counts,category_Folder.Foldername  from category_Folder inner join solutions on category_Folder.folder_id = solutions.folder_id
            // where category_Folder.Category_id  = '" + ((HiddenField)ListView1.Items[i].FindControl("Hndid")).Value.ToString() + "' group by category_Folder.folder_id,category_Folder.Foldername";

            string sqlcount = @"select foldername,folder_id,Category_id from Category_Folder
                                 where Category_id  = '" + ((HiddenField)ListView1.Items[i].FindControl("Hndid")).Value.ToString() + "'";
            DataSet dscount = DatabaseHelper.getallstatus(sqlcount);

            string sqlsub = @"select  COUNT(*)as counts,category_Folder.Foldername,category_Folder.folder_id, category_Folder.Category_id  from category_Folder inner join solutions on category_Folder.folder_id = solutions.folder_id
 where category_Folder.Category_id  = '" + ((HiddenField)ListView1.Items[i].FindControl("Hndid")).Value.ToString() + "' group by category_Folder.folder_id,category_Folder.Foldername ,Category_Folder.Category_id  ";

            DataSet dssub = DatabaseHelper.getallstatus(sqlsub);

            if (dscount.Tables[0].Rows.Count == dssub.Tables[0].Rows.Count)
            {

                if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0)
                {
                    ((ListView)ListView1.Items[i].FindControl("Folderlstview")).DataSource = dssub;
                    ((ListView)ListView1.Items[i].FindControl("Folderlstview")).DataBind();
                }

            }
            else
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("Foldername");
                dt.Columns.Add("folder_id");
                dt.Columns.Add("Category_id");
                dt.Columns.Add("counts");
                DataSet a = new DataSet();
                for (int j = 0; j < dscount.Tables[0].Rows.Count; j++)
                {

                    sqlsub = @"select  COUNT(*)as counts,category_Folder.Foldername,category_Folder.folder_id, category_Folder.Category_id  from category_Folder inner join solutions on category_Folder.folder_id = solutions.folder_id
 where solutions.Category_id = " + dscount.Tables[0].Rows[j]["Category_id"] + "and solutions.folder_id  =" + dscount.Tables[0].Rows[j]["folder_id"] + " group by category_Folder.folder_id,category_Folder.Foldername ,Category_Folder.Category_id  ";
                    dssub = DatabaseHelper.getallstatus(sqlsub);
                    if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0)
                    {

                        DataRow dr = dssub.Tables[0].Rows[0];

                        //a.Tables[0].Rows.Add(dr);
                        dt.ImportRow(dssub.Tables[0].Rows[0]);

                    }
                    else
                    {
                        sqlsub = @"select foldername,folder_id,Category_id from Category_Folder
                                 where Category_id  = " + dscount.Tables[0].Rows[j]["Category_id"] + " and folder_id =" + dscount.Tables[0].Rows[j]["folder_id"];
                        dssub = DatabaseHelper.getallstatus(sqlsub);
                        if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0)
                        {
                            DataColumn dc = new DataColumn("Counts");
                            dc.DefaultValue = 0;
                            dssub.Tables[0].Columns.Add(dc);
                            DataRow dr = dssub.Tables[0].Rows[0];
                            //a.Tables[0].Rows.Add(dr);
                            dt.ImportRow(dssub.Tables[0].Rows[0]);
                        }

                    }
                }
                ((ListView)ListView1.Items[i].FindControl("Folderlstview")).DataSource = dt;
                ((ListView)ListView1.Items[i].FindControl("Folderlstview")).DataBind();

            }
        }
    }

    private void listview2loop()
    {
        for (int i = 0; i < ListView2.Items.Count; i++)
        {
            //            string sqlsub = @"select foldername from Category_Folder
            //                       where Category_id  = '" + ((HiddenField)ListView1.Items[i].FindControl("Hndid")).Value.ToString() + "'";

            //            string sqlsub = @"select  COUNT(*)as counts,category_Folder.Foldername  from category_Folder inner join solutions on category_Folder.folder_id = solutions.folder_id
            // where category_Folder.Category_id  = '" + ((HiddenField)ListView1.Items[i].FindControl("Hndid")).Value.ToString() + "' group by category_Folder.folder_id,category_Folder.Foldername";

            string sqlcount = @"select foldername,folder_id,Category_id from Category_Folder
                                 where Category_id  = '" + ((HiddenField)ListView2.Items[i].FindControl("Hndid")).Value.ToString() + "' and category_Folder.VisibleTo= '"+selectedval+"'";
            DataSet dscount = DatabaseHelper.getallstatus(sqlcount);

            string sqlsub = @"select  COUNT(*)as counts,category_Folder.Foldername,category_Folder.folder_id, category_Folder.Category_id  from category_Folder inner join solutions on category_Folder.folder_id = solutions.folder_id
 where category_Folder.Category_id  = '" + ((HiddenField)ListView2.Items[i].FindControl("Hndid")).Value.ToString() + "' and category_Folder.VisibleTo='" + selectedval + "' and  solutions.solutiontype='" + selectedval + "' group by category_Folder.folder_id,category_Folder.Foldername ,Category_Folder.Category_id  ";

            DataSet dssub = DatabaseHelper.getallstatus(sqlsub);

            if (dscount.Tables[0].Rows.Count == dssub.Tables[0].Rows.Count)
            {

                if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0)
                {
                    ((ListView)ListView2.Items[i].FindControl("Folderlstview")).DataSource = dssub;
                    ((ListView)ListView2.Items[i].FindControl("Folderlstview")).DataBind();
                }

            }
            else
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("Foldername");
                dt.Columns.Add("folder_id");
                dt.Columns.Add("Category_id");
                dt.Columns.Add("counts");
                DataSet a = new DataSet();
                for (int j = 0; j < dscount.Tables[0].Rows.Count; j++)
                {

                    sqlsub = @"select  COUNT(*)as counts,category_Folder.Foldername,category_Folder.folder_id, category_Folder.Category_id  from category_Folder inner join solutions on category_Folder.folder_id = solutions.folder_id
 where solutions.Category_id = " + dscount.Tables[0].Rows[j]["Category_id"] + "and solutions.folder_id  =" + dscount.Tables[0].Rows[j]["folder_id"] + " and solutions.solutiontype='"+selectedval+"' group by category_Folder.folder_id,category_Folder.Foldername ,Category_Folder.Category_id  ";
                    dssub = DatabaseHelper.getallstatus(sqlsub);
                    if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0)
                    {

                        DataRow dr = dssub.Tables[0].Rows[0];

                        //a.Tables[0].Rows.Add(dr);
                        dt.ImportRow(dssub.Tables[0].Rows[0]);

                    }
                    else
                    {
                        sqlsub = @"select foldername,folder_id,Category_id from Category_Folder
                                 where Category_id  = " + dscount.Tables[0].Rows[j]["Category_id"] + " and folder_id =" + dscount.Tables[0].Rows[j]["folder_id"] + " and category_Folder.VisibleTo='" + selectedval + "'";
                        dssub = DatabaseHelper.getallstatus(sqlsub);
                        if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0)
                        {
                            DataColumn dc = new DataColumn("Counts");
                            dc.DefaultValue = 0;
                            dssub.Tables[0].Columns.Add(dc);
                            DataRow dr = dssub.Tables[0].Rows[0];
                            //a.Tables[0].Rows.Add(dr);
                            dt.ImportRow(dssub.Tables[0].Rows[0]);
                        }

                    }
                }
                ((ListView)ListView2.Items[i].FindControl("Folderlstview")).DataSource = dt;
                ((ListView)ListView2.Items[i].FindControl("Folderlstview")).DataBind();

            }
        }
    }
}