﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Solutiondetail.aspx.cs" Inherits="Admin_Solutiondetail" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Solution detail</title></title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../DashStyle.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <uc2:Admin_Header ID="Admin_Header1" runat="server" />
    <div>
        <div id="inner">
            <div class="pagearea" style="width: 80%; float: left">
                <div id="TicketSummary">
                    <div style="width: 20%; float: left">
                        <asp:Label ID="lblcategoryname" runat="server" Style="font-weight: bolder"></asp:Label>
                        <hr />
                        <asp:ListView ID="Folderlstview" runat="server">
                            <LayoutTemplate>
                                <div>
                                    <ul style="height: auto; list-style: disc outside none">
                                        <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                    </ul>
                                </div>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <li><a href="Solutionslist.aspx?categoryid=<%#Eval("Category_id")%>&folderid=<%#Eval("folder_id")%>"
                                    style="margin-left: 10px;">
                                    <%#Eval("foldername")%>
                                    (<%# Eval("counts")%>)</a> </li>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                    <div style="width: 79%; float: right;">
                        <div style="padding: 4px; background: #EBEBEB;">
                            <%--Solution →
                            <asp:Label ID="lblPcatrgoryname" runat="server"></asp:Label>
                            →--%>
                            <a href="Solution.aspx" style="text-decoration: none">Solution</a> → <a id="lblPcatrgoryname"
                                runat="server"></a>
                            <asp:Label ID="lblPfoldername" runat="server"></asp:Label>
                        </div>
                        <hr />
                        <br />
                        <div style="height: 50px; background: white; padding: 4px">
                            <div style="float: left">
                                <asp:Label ID="lblFN" runat="server" Style="font-weight: bolder;"></asp:Label><br />
                                By Support |
                                <asp:Label ID="lbldate" runat="server"></asp:Label>
                            </div>
                            <div id="edit" runat="server" style="float: right">
                                <a class="submit" id="editsolution" runat="server">Edit</a>
                            </div>
                        </div>
                        <div id="solutiondesc" runat="server" style="background: white; padding: 4px">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
