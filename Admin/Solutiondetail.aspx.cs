﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Admin_Solutiondetail : System.Web.UI.Page
{
    string selectedval = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Response.Redirect("login.aspx");
            return;
        }
        else
        {
            if (!DatabaseHelper.isPrivateSolutions(Session["admin"].ToString()))
            {
                selectedval = "Public";
                if (Request.QueryString["categoryid"] != null)
                {
                    if (DatabaseHelper.isPrivatecategory(Request.QueryString["categoryid"]))
                    {

                        Response.Redirect("Solution.aspx");
                    }
                    else
                    {
                        edit.Visible = false;
                    }

                }
                else
                {
                    Response.Redirect("Solution.aspx");
                }
            }
            else
            {
                selectedval = "Private";
            }

        }
        if (Request.QueryString["categoryid"] != null && Request.QueryString["folderid"] != null && Request.QueryString["solutionid"] != null)
        {
            if (selectedval == "Private")
            {
                getcategory(Request.QueryString["categoryid"].ToString(), Request.QueryString["folderid"].ToString(), Request.QueryString["solutionid"].ToString());
            }
            else
            {
                pgetcategory(Request.QueryString["categoryid"].ToString(), Request.QueryString["folderid"].ToString(), Request.QueryString["solutionid"].ToString());
            }
        }
        else
        {
            Response.Redirect("Solution.aspx");
        }
    }
    void getcategory(string category_id, string folder_id, string solution_id)
    {

        string sql = @"select Category.Category_id,Category.CategoryName,Category_Folder.FolderName from Category left join Category_Folder
 on Category.Category_Id=Category_Folder.Category_Id
  where Category.Category_id=" + category_id + " and category_Folder.folder_id=" + folder_id;

        DataSet ds = DatabaseHelper.getallstatus(sql);

        if (ds != null)
        {
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                lblcategoryname.Text = ds.Tables[0].Rows[0]["CategoryName"].ToString();
                //lblPcatrgoryname.Text = ds.Tables[0].Rows[0]["CategoryName"].ToString();
                lblPcatrgoryname.InnerText = ds.Tables[0].Rows[0]["CategoryName"].ToString() + " →";
                lblPcatrgoryname.HRef = "Solutionslist.aspx?categoryid=" + category_id;

                editsolution.HRef = "Addsolution.aspx?categoryid=" + category_id + "&folderid=" + folder_id + "&solutionid=" + solution_id;

                lblPfoldername.Text = ds.Tables[0].Rows[0]["Foldername"].ToString();

                //Page.Title = "Solution : " + lblPcatrgoryname.InnerText.Replace(" →", "") + " : " + lblPfoldername.Text;
                Page.Title = lblPfoldername.Text;

                string sqlcount = @"select foldername,folder_id,Category_id from Category_Folder
                                 where Category_id  = " + category_id;
                DataSet dscount = DatabaseHelper.getallstatus(sqlcount);


                string sqlsub = @"select  COUNT(*)as counts,category_Folder.Foldername,category_Folder.folder_id, category_Folder.Category_id  from category_Folder inner join solutions on category_Folder.folder_id = solutions.folder_id
 where category_Folder.Category_id  = " + category_id + " group by category_Folder.folder_id,category_Folder.Foldername ,Category_Folder.Category_id  ; select Row_Number()OVER(order by solution_id) as RowNumber,* from solutions where folder_id= " + folder_id + " and solution_id=" + solution_id;

                DataSet dssub = DatabaseHelper.getallstatus(sqlsub);

                if (dscount.Tables[0].Rows.Count == dssub.Tables[0].Rows.Count)
                {

                    if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0 && dssub.Tables[1].Rows.Count > 0)
                    {
                        Folderlstview.DataSource = dssub.Tables[0];
                        Folderlstview.DataBind();
                        lblFN.Text = dssub.Tables[1].Rows[0]["SolutionName"].ToString();
                        lbldate.Text = dssub.Tables[1].Rows[0]["reported_date"].ToString();
                        solutiondesc.InnerHtml = dssub.Tables[1].Rows[0]["SolutionDescription"].ToString();

                    }
                    else
                    {
                        Response.Redirect("Solution.aspx");
                    }
                }
                else
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Foldername");
                    dt.Columns.Add("folder_id");
                    dt.Columns.Add("Category_id");
                    dt.Columns.Add("counts");
                    DataSet a = new DataSet();
                    for (int j = 0; j < dscount.Tables[0].Rows.Count; j++)
                    {
                        sqlsub = @"select  COUNT(*)as counts,category_Folder.Foldername,category_Folder.folder_id, category_Folder.Category_id  from category_Folder inner join solutions on category_Folder.folder_id = solutions.folder_id
 where solutions.Category_id  = " + dscount.Tables[0].Rows[j]["Category_id"] + " and solutions.folder_id  =" + dscount.Tables[0].Rows[j]["folder_id"] + " group by category_Folder.folder_id,category_Folder.Foldername ,Category_Folder.Category_id  ; select Row_Number()OVER(order by solution_id) as RowNumber,* from solutions where folder_id= " + folder_id + " and solution_id=" + solution_id;
                        dssub = DatabaseHelper.getallstatus(sqlsub);
                        if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0)
                        {

                            DataRow dr = dssub.Tables[0].Rows[0];

                            //a.Tables[0].Rows.Add(dr);
                            dt.ImportRow(dssub.Tables[0].Rows[0]);
                            if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0 && dssub.Tables[1].Rows.Count > 0)
                            {

                                lblFN.Text = dssub.Tables[1].Rows[0]["SolutionName"].ToString();
                                lbldate.Text = dssub.Tables[1].Rows[0]["reported_date"].ToString();
                                solutiondesc.InnerHtml = dssub.Tables[1].Rows[0]["SolutionDescription"].ToString();

                            }
                            else
                            {
                                Response.Redirect("Solution.aspx");
                            }
                        }
                        else
                        {
                            sqlsub = @"select foldername,folder_id,Category_id from Category_Folder
                                 where Category_id  = " + dscount.Tables[0].Rows[j]["Category_id"] + " and folder_id =" + dscount.Tables[0].Rows[j]["folder_id"];
                            dssub = DatabaseHelper.getallstatus(sqlsub);
                            if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0)
                            {
                                DataColumn dc = new DataColumn("Counts");
                                dc.DefaultValue = 0;
                                dssub.Tables[0].Columns.Add(dc);
                                DataRow dr = dssub.Tables[0].Rows[0];
                                //a.Tables[0].Rows.Add(dr);
                                dt.ImportRow(dssub.Tables[0].Rows[0]);
                            }

                        }
                    }

                    Folderlstview.DataSource = dt;
                    Folderlstview.DataBind();
                }
            }
            else
            {
                Page.Title = "Solution details";
            }

        }



    }


    void pgetcategory(string category_id, string folder_id, string solution_id)
    {

        string sql = @"select Category.Category_id,Category.CategoryName,Category_Folder.FolderName from Category left join Category_Folder
 on Category.Category_Id=Category_Folder.Category_Id
  where Category.Category_id=" + category_id + " and category_Folder.folder_id=" + folder_id + " and category_Folder.VisibleTo='public'";

        DataSet ds = DatabaseHelper.getallstatus(sql);

        if (ds != null)
        {
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                lblcategoryname.Text = ds.Tables[0].Rows[0]["CategoryName"].ToString();
                //lblPcatrgoryname.Text = ds.Tables[0].Rows[0]["CategoryName"].ToString();
                lblPcatrgoryname.InnerText = ds.Tables[0].Rows[0]["CategoryName"].ToString() + " →";
                lblPcatrgoryname.HRef = "Solutionslist.aspx?categoryid=" + category_id;

                editsolution.HRef = "Addsolution.aspx?categoryid=" + category_id + "&folderid=" + folder_id + "&solutionid=" + solution_id;

                lblPfoldername.Text = ds.Tables[0].Rows[0]["Foldername"].ToString();

                //Page.Title = "Solution : " + lblPcatrgoryname.InnerText.Replace(" →", "") + " : " + lblPfoldername.Text;
                Page.Title = lblPfoldername.Text;

                string sqlcount = @"select foldername,folder_id,Category_id from Category_Folder
                                 where Category_id  = " + category_id + " and category_Folder.VisibleTo='public'";
                DataSet dscount = DatabaseHelper.getallstatus(sqlcount);


                string sqlsub = @"select  COUNT(*)as counts,category_Folder.Foldername,category_Folder.folder_id, category_Folder.Category_id  from category_Folder inner join solutions on category_Folder.folder_id = solutions.folder_id
 where category_Folder.Category_id  = " + category_id + " and category_Folder.VisibleTo='public' and  solutions.solutiontype='public' group by category_Folder.folder_id,category_Folder.Foldername ,Category_Folder.Category_id  ; select Row_Number()OVER(order by solution_id) as RowNumber,* from solutions where folder_id= " + folder_id + " and solution_id=" + solution_id + " and  solutions.solutiontype='public'";

                DataSet dssub = DatabaseHelper.getallstatus(sqlsub);

                if (dscount.Tables[0].Rows.Count == dssub.Tables[0].Rows.Count)
                {

                    if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0 && dssub.Tables[1].Rows.Count > 0)
                    {
                        Folderlstview.DataSource = dssub.Tables[0];
                        Folderlstview.DataBind();
                        lblFN.Text = dssub.Tables[1].Rows[0]["SolutionName"].ToString();
                        lbldate.Text = dssub.Tables[1].Rows[0]["reported_date"].ToString();
                        solutiondesc.InnerHtml = dssub.Tables[1].Rows[0]["SolutionDescription"].ToString();

                    }
                    else
                    {
                        Response.Redirect("Solution.aspx");
                    }
                }
                else
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Foldername");
                    dt.Columns.Add("folder_id");
                    dt.Columns.Add("Category_id");
                    dt.Columns.Add("counts");
                    DataSet a = new DataSet();
                    for (int j = 0; j < dscount.Tables[0].Rows.Count; j++)
                    {
                        sqlsub = @"select  COUNT(*)as counts,category_Folder.Foldername,category_Folder.folder_id, category_Folder.Category_id  from category_Folder inner join solutions on category_Folder.folder_id = solutions.folder_id
 where solutions.Category_id  = " + dscount.Tables[0].Rows[j]["Category_id"] + " and solutions.folder_id  =" + dscount.Tables[0].Rows[j]["folder_id"] + " and  solutions.solutiontype='public' group by category_Folder.folder_id,category_Folder.Foldername ,Category_Folder.Category_id  ; select Row_Number()OVER(order by solution_id) as RowNumber,* from solutions where folder_id= " + folder_id + " and solution_id=" + solution_id + " and  solutions.solutiontype='public'";
                        dssub = DatabaseHelper.getallstatus(sqlsub);
                        if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0)
                        {

                            DataRow dr = dssub.Tables[0].Rows[0];

                            //a.Tables[0].Rows.Add(dr);
                            dt.ImportRow(dssub.Tables[0].Rows[0]);
                            if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0 && dssub.Tables[1].Rows.Count > 0)
                            {

                                lblFN.Text = dssub.Tables[1].Rows[0]["SolutionName"].ToString();
                                lbldate.Text = dssub.Tables[1].Rows[0]["reported_date"].ToString();
                                solutiondesc.InnerHtml = dssub.Tables[1].Rows[0]["SolutionDescription"].ToString();

                            }
                            else
                            {
                                Response.Redirect("Solution.aspx");
                            }
                        }
                        else
                        {
                            sqlsub = @"select foldername,folder_id,Category_id from Category_Folder
                                 where Category_id  = " + dscount.Tables[0].Rows[j]["Category_id"] + " and folder_id =" + dscount.Tables[0].Rows[j]["folder_id"] + " and category_Folder.VisibleTo='public'";
                            dssub = DatabaseHelper.getallstatus(sqlsub);
                            if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0)
                            {
                                DataColumn dc = new DataColumn("Counts");
                                dc.DefaultValue = 0;
                                dssub.Tables[0].Columns.Add(dc);
                                DataRow dr = dssub.Tables[0].Rows[0];
                                //a.Tables[0].Rows.Add(dr);
                                dt.ImportRow(dssub.Tables[0].Rows[0]);
                            }

                        }
                    }

                    Folderlstview.DataSource = dt;
                    Folderlstview.DataBind();
                }
            }
            else
            {
                Page.Title = "Solution details";
            }

        }



    }
}