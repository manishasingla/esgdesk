﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Solutionslist.aspx.cs" Inherits="Admin_Solutionslist" %>

<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Solution list</title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../DashStyle.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <uc2:Admin_Header ID="Admin_Header1" runat="server" />
    <div>
        <div id="inner">
            <div class="pagearea" style="width: 80%; float: left">
                <div id="TicketSummary">
                    <div style="width: 20%; float: left">
                        <asp:Label ID="lblcategoryname" runat="server" Style="font-weight: bolder"></asp:Label>
                        <hr />
                        <asp:ListView ID="Folderlstview" runat="server">
                            <LayoutTemplate>
                                <div>
                                    <ul style="height: auto; list-style: disc outside none">
                                        <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                    </ul>
                                </div>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <li><a href="Solutionslist.aspx?categoryid=<%#Eval("Category_id")%>&folderid=<%#Eval("folder_id")%>"
                                    style="margin-left: 10px;">
                                    <%#Eval("foldername")%>
                                    (<%# Eval("counts")%>)</a> </li>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                    <div style="width: 79%; float: right; background: #EBEBEB;">
                        <div style="padding: 4px; border-bottom: 1px solid;">
                            <a href="Solution.aspx" style="text-decoration: none">Solution</a> →
                            <%-- <asp:Label ID="lblPcatrgoryname" runat="server"></asp:Label>--%>
                            <a id="lblPcatrgoryname" runat="server"></a>
                            <asp:Label ID="lblPfoldername" runat="server"></asp:Label>
                        </div>
                        <%--   <hr />--%>
                        <br />
                        <div id="divaction" runat="server" style="height: 50px; padding: 4px">
                            <div style="float: left">
                                <asp:Label ID="lblFN" runat="server" Style="font-weight: bolder;"></asp:Label></div>
                            <div id="addedit" runat="server" style="float: right">
                                <a class="submit" id="addsolu" runat="server">Add solution</a> <a class="submit"
                                    id="editfolder" runat="server">Edit</a>
                            </div>
                        </div>
                        <div style="background: white">
                            <br />
                        </div>
                        <div style="background: white; padding: 4px">
                            <asp:GridView ID="gridlist" runat="server" AutoGenerateColumns="false" ShowHeader="false"
                                Width="100%">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <div style="padding: 2px;">
                                                <asp:Label ID="lblRcount" runat="server" Text='<%#Eval("RowNumber") %>'>' ></asp:Label>.&nbsp;
                                                <%--<asp:Label ID="lblsolutionsname" runat="server" Text='<%# Eval("Solutionname") %>'> </asp:Label>--%>
                                                <a href="Solutiondetail.aspx?categoryid=<%#Eval("Category_id")%>&folderid=<%#Eval("folder_id")%>&solutionid=<%#Eval("solution_id")%>">
                                                    <%# Eval("Solutionname") %>
                                                </a>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                        <div id="addsolution" runat="server" style="text-align: center" visible="false">
                            <div class="no-info-text">
                                No solutions in this folder
                            </div>
                            <a class="submit" id="btnaddsolution" runat="server">Add solution</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
