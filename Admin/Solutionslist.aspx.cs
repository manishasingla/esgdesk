﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Admin_Solutionslist : System.Web.UI.Page
{
    string selectedval = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Response.Redirect("login.aspx");
            return;
        }
        else
        {
            if (!DatabaseHelper.isPrivateSolutions(Session["admin"].ToString()))
            {
                selectedval = "Public";
                if (Request.QueryString["categoryid"] != null)
                {
                    if (DatabaseHelper.isPrivatecategory(Request.QueryString["categoryid"]))
                    {

                        Response.Redirect("Solution.aspx");
                    }
                    else
                    {
                        addedit.Visible = false;
                        btnaddsolution.Visible = false;

                    }

                }
                else
                {

                    Response.Redirect("Solution.aspx");
                }
            }
            else
            {
                selectedval = "Private";
            }

        }
        if (Request.QueryString["categoryid"] != null && Request.QueryString["folderid"] != null)
        {
            if (selectedval == "Private")
            {
                getcategory(Request.QueryString["categoryid"].ToString(), Request.QueryString["folderid"].ToString());
            }
            else
            {
                Pgetcategory(Request.QueryString["categoryid"].ToString(), Request.QueryString["folderid"].ToString());
            }
        }
        else if (Request.QueryString["categoryid"] != null)
        {
            if (selectedval == "Private")
            {
                getcategory(Request.QueryString["categoryid"].ToString());
            }
            else
            {
                Pgetcategory(Request.QueryString["categoryid"].ToString());
            }
        }
        else
        {
            Response.Redirect("Solution.aspx");
        }
    }
    void getcategory(string category_id, string folder_id)
    {
        divaction.Visible = true;
        //string sql = @"select Category_id,CategoryName from Category where Category_id=" + category_id;
        string sql = @"select Category.Category_id,Category.CategoryName,Category_Folder.FolderName from Category left join Category_Folder
 on Category.Category_Id=Category_Folder.Category_Id
  where Category.Category_id=" + category_id + " and category_Folder.folder_id=" + folder_id;

        DataSet ds = DatabaseHelper.getallstatus(sql);
        if (ds != null)
        {
            DataSet dscount = new DataSet();
            DataSet dssub = new DataSet();
            string sqlsub = "";

            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                lblcategoryname.Text = ds.Tables[0].Rows[0]["CategoryName"].ToString();
                lblPcatrgoryname.InnerText = ds.Tables[0].Rows[0]["CategoryName"].ToString() + " →";
                lblPcatrgoryname.HRef = "Solutionslist.aspx?categoryid=" + category_id;
                lblPfoldername.Text = ds.Tables[0].Rows[0]["Foldername"].ToString();
                lblFN.Text = ds.Tables[0].Rows[0]["Foldername"].ToString();
                addsolu.HRef = "Addsolution.aspx?categoryid=" + category_id + "&folderid=" + folder_id;
                editfolder.HRef = "addcategoryfolder.aspx?categoryid=" + category_id + "&folderid=" + folder_id;
                btnaddsolution.HRef = "Addsolution.aspx?categoryid=" + category_id + "&folderid=" + folder_id;
                Page.Title = lblPfoldername.Text;

                string sqlcount = @"select foldername,folder_id,Category_id from Category_Folder
                                 where Category_id  = " + category_id;
                dscount = DatabaseHelper.getallstatus(sqlcount);
                //                string sqlsub = @"select  COUNT(*)as counts,category_Folder.Foldername,category_Folder.folder_id, category_Folder.Category_id  from category_Folder inner join solutions on category_Folder.folder_id = solutions.folder_id
                // where category_Folder.Category_id  = " + category_id + " group by category_Folder.folder_id,category_Folder.Foldername ,Category_Folder.Category_id  ;  select * from solutions where folder_id= " + folder_id;
                sqlsub = @"select  category_Folder.Foldername,category_Folder.folder_id, category_Folder.Category_id,COUNT(*)as counts  from category_Folder inner join solutions on category_Folder.folder_id = solutions.folder_id
 where solutions.Category_id  = " + category_id + "and solutions.folder_id  =" + folder_id + " group by category_Folder.folder_id,category_Folder.Foldername ,Category_Folder.Category_id  ;  select Row_Number()OVER(order by solution_id) as RowNumber,* from solutions where folder_id= " + folder_id;


                dssub = DatabaseHelper.getallstatus(sqlsub);
                if (dscount.Tables[0].Rows.Count == dssub.Tables[0].Rows.Count)
                {
                    if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0)
                    {

                        Folderlstview.DataSource = dssub.Tables[0];
                        Folderlstview.DataBind();
                        if (dssub.Tables.Count == 2)
                        {
                            if (dssub.Tables[1].Rows.Count > 0)
                            {
                                gridlist.DataSource = dssub.Tables[1];
                                gridlist.DataBind();
                            }
                            else
                            {
                                gridlist.Visible = false;
                                addsolution.Visible = true;
                            }
                        }

                    }
                }
                else
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Foldername");
                    dt.Columns.Add("folder_id");
                    dt.Columns.Add("Category_id");
                    dt.Columns.Add("counts");
                    DataSet a = new DataSet();
                    for (int j = 0; j < dscount.Tables[0].Rows.Count; j++)
                    {
                        sqlsub = @"select  category_Folder.Foldername,category_Folder.folder_id, category_Folder.Category_id,COUNT(*)as counts  from category_Folder inner join solutions on category_Folder.folder_id = solutions.folder_id
 where solutions.Category_id  = " + dscount.Tables[0].Rows[j]["Category_id"] + "and solutions.folder_id  =" + dscount.Tables[0].Rows[j]["folder_id"] + " group by category_Folder.folder_id,category_Folder.Foldername ,Category_Folder.Category_id  ;  select Row_Number()OVER(order by solution_id) as RowNumber,* from solutions where folder_id= " + folder_id;

                        dssub = DatabaseHelper.getallstatus(sqlsub);
                        if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0)
                        {


                            DataRow dr = dssub.Tables[0].Rows[0];

                            //a.Tables[0].Rows.Add(dr);
                            dt.ImportRow(dssub.Tables[0].Rows[0]);
                            if (dssub.Tables.Count == 2)
                            {
                                if (dssub.Tables[1].Rows.Count > 0)
                                {
                                    gridlist.DataSource = dssub.Tables[1];
                                    gridlist.DataBind();
                                }
                                else
                                {
                                    gridlist.Visible = false;
                                    addsolution.Visible = true;
                                }
                            }
                        }
                        else
                        {
                            sqlsub = @"select foldername,folder_id,Category_id from Category_Folder
                                 where Category_id  = " + dscount.Tables[0].Rows[j]["category_id"] + " and folder_id =" + dscount.Tables[0].Rows[j]["folder_id"] + ";  select * from solutions where folder_id= " + dscount.Tables[0].Rows[j]["folder_id"];
                            dssub = DatabaseHelper.getallstatus(sqlsub);
                            if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0)
                            {
                                DataColumn dc = new DataColumn("Counts");
                                dc.DefaultValue = 0;
                                dssub.Tables[0].Columns.Add(dc);
                                DataRow dr = dssub.Tables[0].Rows[0];
                                //a.Tables[0].Rows.Add(dr);
                                dt.ImportRow(dssub.Tables[0].Rows[0]);

                            }

                        }
                    }
                    // lblPfoldername.Text = dssub.Tables[0].Rows[0]["Foldername"].ToString();
                    Folderlstview.DataSource = dt;
                    Folderlstview.DataBind();
                    //gridlist.Visible = false;
                    //addsolution.Visible = true;


                }
            }
            else
            {
                divaction.Visible = false;
                Page.Title = "Solution";
            }

        }
    }

    void getcategory(string category_id)
    {

        divaction.Visible = false;
        string sql = @"select Category.Category_id,Category.CategoryName,Category_Folder.FolderName from Category left join Category_Folder
 on Category.Category_Id=Category_Folder.Category_Id
  where Category.Category_id=" + category_id;

        DataSet ds = DatabaseHelper.getallstatus(sql);
        if (ds != null)
        {
            DataSet dscount = new DataSet();
            DataSet dssub = new DataSet();
            string sqlsub = "";
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                lblcategoryname.Text = ds.Tables[0].Rows[0]["CategoryName"].ToString();
                //lblPcatrgoryname.InnerText = ds.Tables[0].Rows[0]["CategoryName"].ToString() + " →";
                lblPcatrgoryname.InnerText = ds.Tables[0].Rows[0]["CategoryName"].ToString();
                lblPcatrgoryname.HRef = "Solutionslist.aspx?categoryid=" + category_id;
                Page.Title = lblPcatrgoryname.InnerText;
                //lblPfoldername.Text = ds.Tables[0].Rows[0]["Foldername"].ToString();
                //lblFN.Text = ds.Tables[0].Rows[0]["Foldername"].ToString();
                btnaddsolution.HRef = "Addsolution.aspx?categoryid=" + category_id;
                string sqlcount = @"select foldername,folder_id,Category_id from Category_Folder
                                 where Category_id  = " + category_id;
                dscount = DatabaseHelper.getallstatus(sqlcount);

                sqlsub = @"select  category_Folder.Foldername,category_Folder.folder_id, category_Folder.Category_id,COUNT(*)as counts  from category_Folder inner join solutions on category_Folder.folder_id = solutions.folder_id
 where category_Folder.Category_id  = " + category_id + " group by category_Folder.folder_id,category_Folder.Foldername ,Category_Folder.Category_id";


                dssub = DatabaseHelper.getallstatus(sqlsub);
                if (dscount.Tables[0].Rows.Count == dssub.Tables[0].Rows.Count)
                {
                    if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0)
                    {

                        Folderlstview.DataSource = dssub.Tables[0];
                        Folderlstview.DataBind();
                        gridlist.Visible = false;
                        addsolution.Visible = false;


                    }

                }
                else
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Foldername");
                    dt.Columns.Add("folder_id");
                    dt.Columns.Add("Category_id");
                    dt.Columns.Add("counts");
                    DataSet a = new DataSet();
                    for (int j = 0; j < dscount.Tables[0].Rows.Count; j++)
                    {

                        sqlsub = @"select  COUNT(*)as counts,category_Folder.Foldername,category_Folder.folder_id, category_Folder.Category_id  from category_Folder inner join solutions on category_Folder.folder_id = solutions.folder_id
 where solutions.Category_id = " + dscount.Tables[0].Rows[j]["Category_id"] + "and solutions.folder_id  =" + dscount.Tables[0].Rows[j]["folder_id"] + " group by category_Folder.folder_id,category_Folder.Foldername ,Category_Folder.Category_id  ";
                        dssub = DatabaseHelper.getallstatus(sqlsub);
                        if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0)
                        {

                            DataRow dr = dssub.Tables[0].Rows[0];

                            //a.Tables[0].Rows.Add(dr);
                            dt.ImportRow(dssub.Tables[0].Rows[0]);

                        }
                        else
                        {
                            sqlsub = @"select foldername,folder_id,Category_id from Category_Folder
                                 where Category_id  = " + dscount.Tables[0].Rows[j]["Category_id"] + " and folder_id =" + dscount.Tables[0].Rows[j]["folder_id"];
                            dssub = DatabaseHelper.getallstatus(sqlsub);
                            if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0)
                            {
                                DataColumn dc = new DataColumn("Counts");
                                dc.DefaultValue = 0;
                                dssub.Tables[0].Columns.Add(dc);
                                DataRow dr = dssub.Tables[0].Rows[0];
                                //a.Tables[0].Rows.Add(dr);
                                dt.ImportRow(dssub.Tables[0].Rows[0]);
                            }

                        }
                    }
                    Folderlstview.DataSource = dt;
                    Folderlstview.DataBind();
                    // lblPfoldername.Text = dssub.Tables[0].Rows[0]["Foldername"].ToString();
                    gridlist.Visible = false;
                    addsolution.Visible = true;

                }
            }
            else
            {
                addsolution.Visible = false;
                Page.Title = "Solution";
            }

        }
    }


    void Pgetcategory(string category_id, string folder_id)
    {
        divaction.Visible = true;

        string sql = @"select Category.Category_id,Category.CategoryName,Category_Folder.FolderName from Category left join Category_Folder 
  on Category.Category_Id=Category_Folder.Category_Id
  where Category.Category_id=" + category_id + " and category_Folder.folder_id=" + folder_id + " and category_Folder.VisibleTo='public'";

        DataSet ds = DatabaseHelper.getallstatus(sql);
        if (ds != null)
        {
            DataSet dscount = new DataSet();
            DataSet dssub = new DataSet();
            string sqlsub = "";

            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                lblcategoryname.Text = ds.Tables[0].Rows[0]["CategoryName"].ToString();
                lblPcatrgoryname.InnerText = ds.Tables[0].Rows[0]["CategoryName"].ToString() + " →";
                lblPcatrgoryname.HRef = "Solutionslist.aspx?categoryid=" + category_id;
                lblPfoldername.Text = ds.Tables[0].Rows[0]["Foldername"].ToString();
                lblFN.Text = ds.Tables[0].Rows[0]["Foldername"].ToString();
                addsolu.HRef = "Addsolution.aspx?categoryid=" + category_id + "&folderid=" + folder_id;
                editfolder.HRef = "addcategoryfolder.aspx?categoryid=" + category_id + "&folderid=" + folder_id;
                btnaddsolution.HRef = "Addsolution.aspx?categoryid=" + category_id + "&folderid=" + folder_id;
                Page.Title = lblPfoldername.Text;

                string sqlcount = @"select foldername,folder_id,Category_id from Category_Folder
                                 where Category_id  = " + category_id + " and category_Folder.VisibleTo='public'";
                dscount = DatabaseHelper.getallstatus(sqlcount);
                //                string sqlsub = @"select  COUNT(*)as counts,category_Folder.Foldername,category_Folder.folder_id, category_Folder.Category_id  from category_Folder inner join solutions on category_Folder.folder_id = solutions.folder_id
                // where category_Folder.Category_id  = " + category_id + " group by category_Folder.folder_id,category_Folder.Foldername ,Category_Folder.Category_id  ;  select * from solutions where folder_id= " + folder_id;
                sqlsub = @"select  category_Folder.Foldername,category_Folder.folder_id, category_Folder.Category_id,COUNT(*)as counts  from category_Folder inner join solutions on category_Folder.folder_id = solutions.folder_id
 where solutions.Category_id  = " + category_id + "and solutions.folder_id  =" + folder_id + " and  solutions.solutiontype='public' group by category_Folder.folder_id,category_Folder.Foldername ,Category_Folder.Category_id  ;  select Row_Number()OVER(order by solution_id) as RowNumber,* from solutions where folder_id= " + folder_id + " and  solutions.solutiontype='public'";


                dssub = DatabaseHelper.getallstatus(sqlsub);
                if (dscount.Tables[0].Rows.Count == dssub.Tables[0].Rows.Count)
                {
                    if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0)
                    {

                        Folderlstview.DataSource = dssub.Tables[0];
                        Folderlstview.DataBind();
                        if (dssub.Tables.Count == 2)
                        {
                            if (dssub.Tables[1].Rows.Count > 0)
                            {
                                gridlist.DataSource = dssub.Tables[1];
                                gridlist.DataBind();
                            }
                            else
                            {
                                gridlist.Visible = false;
                                addsolution.Visible = true;
                            }
                        }

                    }
                }
                else
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Foldername");
                    dt.Columns.Add("folder_id");
                    dt.Columns.Add("Category_id");
                    dt.Columns.Add("counts");
                    DataSet a = new DataSet();
                    for (int j = 0; j < dscount.Tables[0].Rows.Count; j++)
                    {
                        sqlsub = @"select  category_Folder.Foldername,category_Folder.folder_id, category_Folder.Category_id,COUNT(*)as counts  from category_Folder inner join solutions on category_Folder.folder_id = solutions.folder_id
 where solutions.Category_id  = " + dscount.Tables[0].Rows[j]["Category_id"] + "and solutions.folder_id  =" + dscount.Tables[0].Rows[j]["folder_id"] + " and  solutions.solutiontype='public'  group by category_Folder.folder_id,category_Folder.Foldername ,Category_Folder.Category_id  ;  select Row_Number()OVER(order by solution_id) as RowNumber,* from solutions where folder_id= " + folder_id + " and  solutions.solutiontype='public'";

                        dssub = DatabaseHelper.getallstatus(sqlsub);
                        if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0)
                        {


                            DataRow dr = dssub.Tables[0].Rows[0];

                            //a.Tables[0].Rows.Add(dr);
                            dt.ImportRow(dssub.Tables[0].Rows[0]);
                            if (dssub.Tables.Count == 2)
                            {
                                if (dssub.Tables[1].Rows.Count > 0)
                                {
                                    gridlist.DataSource = dssub.Tables[1];
                                    gridlist.DataBind();
                                }
                                else
                                {
                                    gridlist.Visible = false;
                                    addsolution.Visible = true;
                                }
                            }
                        }
                        else
                        {
                            sqlsub = @"select foldername,folder_id,Category_id from Category_Folder
                                 where Category_id  = " + dscount.Tables[0].Rows[j]["category_id"] + " and folder_id =" + dscount.Tables[0].Rows[j]["folder_id"] + " and category_Folder.VisibleTo='public' ;  select * from solutions where folder_id= " + dscount.Tables[0].Rows[j]["folder_id"] + " and  solutions.solutiontype='public' ";
                            dssub = DatabaseHelper.getallstatus(sqlsub);
                            if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0)
                            {
                                DataColumn dc = new DataColumn("Counts");
                                dc.DefaultValue = 0;
                                dssub.Tables[0].Columns.Add(dc);
                                DataRow dr = dssub.Tables[0].Rows[0];
                                //a.Tables[0].Rows.Add(dr);
                                dt.ImportRow(dssub.Tables[0].Rows[0]);
                                //if (dssub.Tables.Count == 2)
                                //{
                                //    if (dssub.Tables[1].Rows.Count > 0)
                                //    {
                                //        gridlist.DataSource = dssub.Tables[1];
                                //        gridlist.DataBind();
                                //    }
                                //    else
                                //    {
                                //       // gridlist.Visible = false;
                                //      //  addsolution.Visible = true;
                                //    }
                                //}
                            }

                        }
                    }
                    // lblPfoldername.Text = dssub.Tables[0].Rows[0]["Foldername"].ToString();
                    Folderlstview.DataSource = dt;
                    Folderlstview.DataBind();
                    //gridlist.Visible = false;
                    //addsolution.Visible = true;


                }
            }
            else
            {
                divaction.Visible = false;
                Page.Title = "Solution";
            }

        }
    }

    void Pgetcategory(string category_id)
    {

        divaction.Visible = false;
        string sql = @"select Category.Category_id,Category.CategoryName,Category_Folder.FolderName from Category left join Category_Folder
 on Category.Category_Id=Category_Folder.Category_Id
  where Category.Category_id=" + category_id + " and category_Folder.VisibleTo='public'";

        DataSet ds = DatabaseHelper.getallstatus(sql);
        if (ds != null)
        {
            DataSet dscount = new DataSet();
            DataSet dssub = new DataSet();
            string sqlsub = "";
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                lblcategoryname.Text = ds.Tables[0].Rows[0]["CategoryName"].ToString();
                //lblPcatrgoryname.InnerText = ds.Tables[0].Rows[0]["CategoryName"].ToString() + " →";
                lblPcatrgoryname.InnerText = ds.Tables[0].Rows[0]["CategoryName"].ToString();
                lblPcatrgoryname.HRef = "Solutionslist.aspx?categoryid=" + category_id;
                Page.Title = lblPcatrgoryname.InnerText;
                //lblPfoldername.Text = ds.Tables[0].Rows[0]["Foldername"].ToString();
                //lblFN.Text = ds.Tables[0].Rows[0]["Foldername"].ToString();
                btnaddsolution.HRef = "Addsolution.aspx?categoryid=" + category_id;
                string sqlcount = @"select foldername,folder_id,Category_id from Category_Folder
                                 where Category_id  = " + category_id;
                dscount = DatabaseHelper.getallstatus(sqlcount);

                sqlsub = @"select  category_Folder.Foldername,category_Folder.folder_id, category_Folder.Category_id,COUNT(*)as counts  from category_Folder inner join solutions on category_Folder.folder_id = solutions.folder_id
 where category_Folder.Category_id  = " + category_id + " and category_Folder.VisibleTo='public' group by category_Folder.folder_id,category_Folder.Foldername ,Category_Folder.Category_id";


                dssub = DatabaseHelper.getallstatus(sqlsub);
                if (dscount.Tables[0].Rows.Count == dssub.Tables[0].Rows.Count)
                {
                    if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0)
                    {

                        Folderlstview.DataSource = dssub.Tables[0];
                        Folderlstview.DataBind();
                        gridlist.Visible = false;
                        addsolution.Visible = false;


                    }

                }
                else
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Foldername");
                    dt.Columns.Add("folder_id");
                    dt.Columns.Add("Category_id");
                    dt.Columns.Add("counts");
                    DataSet a = new DataSet();
                    for (int j = 0; j < dscount.Tables[0].Rows.Count; j++)
                    {

                        sqlsub = @"select  COUNT(*)as counts,category_Folder.Foldername,category_Folder.folder_id, category_Folder.Category_id  from category_Folder inner join solutions on category_Folder.folder_id = solutions.folder_id
 where solutions.Category_id = " + dscount.Tables[0].Rows[j]["Category_id"] + "and solutions.folder_id  =" + dscount.Tables[0].Rows[j]["folder_id"] + " and  solutions.solutiontype='public' group by category_Folder.folder_id,category_Folder.Foldername ,Category_Folder.Category_id  ";
                        dssub = DatabaseHelper.getallstatus(sqlsub);
                        if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0)
                        {

                            DataRow dr = dssub.Tables[0].Rows[0];

                            //a.Tables[0].Rows.Add(dr);
                            dt.ImportRow(dssub.Tables[0].Rows[0]);

                        }
                        else
                        {
                            sqlsub = @"select foldername,folder_id,Category_id from Category_Folder
                                 where Category_id  = " + dscount.Tables[0].Rows[j]["Category_id"] + " and folder_id =" + dscount.Tables[0].Rows[j]["folder_id"] + " and category_Folder.VisibleTo='public'";
                            dssub = DatabaseHelper.getallstatus(sqlsub);
                            if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0)
                            {
                                DataColumn dc = new DataColumn("Counts");
                                dc.DefaultValue = 0;
                                dssub.Tables[0].Columns.Add(dc);
                                DataRow dr = dssub.Tables[0].Rows[0];
                                //a.Tables[0].Rows.Add(dr);
                                dt.ImportRow(dssub.Tables[0].Rows[0]);
                            }

                        }
                    }
                    Folderlstview.DataSource = dt;
                    Folderlstview.DataBind();
                    // lblPfoldername.Text = dssub.Tables[0].Rows[0]["Foldername"].ToString();
                    gridlist.Visible = false;
                    addsolution.Visible = true;

                }
            }
            else
            {
                addsolution.Visible = false;
                Page.Title = "Solution";
            }

        }
    }


}