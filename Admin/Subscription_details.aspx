<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Subscription_details.aspx.cs"
    Inherits="Subscription_details" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="Notifications.ascx" TagName="Notifications" TagPrefix="uc4" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        Subscription details</title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
<style type="text/css">
     table#DataGrid1_ctl00 tr th a
     {
      display:block;
      text-decoration:none;
      margin:0;
      padding:0;
      width:100%;
      height:100%;
     }
</style>


</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <div>
            <uc2:Admin_Header ID="Admin_Header1" runat="server" />
            <div id="Content">
                <div id="Content_98">
                    <div class="titleText" style="margin: 10px 0 10px 0; display: none;">
                        Added subscriptions&nbsp;|&nbsp;</div>
                    <div id="divMessage" runat="server" style="color: Red; font-weight: bold;">
                    </div>
                    <div>
                        <uc4:Notifications ID="Notifications" runat="server" />
                    </div>
                    <div style="float: right; width: 100%; padding: 5px 0px; background: #ebebeb;" class="text_default"
                        valign="middle" width="100%" id="divLinks" runat="server">
                        &nbsp;&nbsp;<a style="Border-bottom:1px dotted #000;" href="Client_Subscription.aspx">Add new Subscription</a>
                    </div>
                    <div style="clear: both">
                    </div>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <div>
                            
                               <telerik:RadGrid ID="DataGrid1" runat="server" Width="100%" OnSortCommand="DataGrid1_SortCommand" OnItemCommand="DataGrid1_ItemCommand" OnItemDataBound="DataGrid1_ItemDataBound"  OnPageIndexChanged="DataGrid1_PageIndexChanged"  PageSize="100" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" CellPadding="5" >
                                <PagerStyle Mode="NextPrevAndNumeric" />
                                <MasterTableView PagerStyle-AlwaysVisible="true">
                                <Columns>
                                <%--0--%>
                                
                                <telerik:GridTemplateColumn HeaderText="SubscriptionID" Visible="False">
                                <ItemTemplate>
                                <asp:Label ID="lblsubscriptionidV" runat="server" Text='<%#bind("SubscriptionID")%>' ></asp:Label>
                                </ItemTemplate> 
                                </telerik:GridTemplateColumn> 
                                <%--1--%>
                                
                                <telerik:GridTemplateColumn HeaderText="Subscription ID" SortExpression="SubscriptionID">
                                   <ItemTemplate>
                                      <asp:Label ID="lblsubscriptionID" runat="server"  Text='<%#bind("SubscriptionID")%>' ></asp:Label>
                                   </ItemTemplate> 
                                </telerik:GridTemplateColumn> 
                                 
                                 <%--2--%>
                                 
                                 <telerik:GridTemplateColumn HeaderText="Subscription name" SortExpression="Subscription">
                                 <ItemTemplate>
                                 <asp:Label ID="lblSubscription" runat="server" Text='<%#Bind("Subscription") %>'>
                                 </asp:Label>
                                 </ItemTemplate> 
                                 </telerik:GridTemplateColumn> 
                                 
                                 <%--3--%>
                                 
                                 <telerik:GridHyperLinkColumn DataNavigateUrlFields="SubscriptionID" DataNavigateUrlFormatString="Client_Subscription.aspx?id={0}" NavigateUrl="Client_Subscription.aspx" HeaderText="Edit" Text="edit">
                                 </telerik:GridHyperLinkColumn> 
                                 
                                 <%--4--%>
                                 <telerik:GridTemplateColumn HeaderText="Delete">
                                 <ItemTemplate>
                                  <asp:LinkButton ID="lnkDelete" runat="server" CommandName="delete">delete</asp:LinkButton>
                                 </ItemTemplate>
                                 </telerik:GridTemplateColumn> 
                                 
                                </Columns> 
                                </MasterTableView> 
                                </telerik:RadGrid> 
                                
                                                               
                                </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <uc1:footer ID="Footer1" runat="server" />
    <asp:Label ID="lblOrderBy" runat="server" Text="" Visible="false"></asp:Label>
    </form>
</body>
</html>
