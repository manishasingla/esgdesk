using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
using Telerik.Web;

public partial class Subscription_details : System.Web.UI.Page
{
    string sql = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = "client_details.aspx";
            Response.Redirect("login.aspx");
            return;
        }
        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            Response.Redirect("tasks.aspx");
            return;
        }
        
        if (!Page.IsPostBack)
        {
            lblOrderBy.Text = " ORDER BY SubscriptionID desc";
            BindData();
            
        }   
    }
   private void BindData()
    {
        DataSet ds = DatabaseHelper.getClientSubscription(lblOrderBy.Text);

        if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "No subscription have been added.";
            divMessage.Visible = true;
        }
        else
        {
            divMessage.InnerHtml = "";
            DataGrid1.DataSource = ds.Tables[0];
            DataGrid1.DataBind();
        }
    }
    protected void DataGrid1_SortCommand(object source, Telerik.Web.UI.GridSortCommandEventArgs e)
    {
        if (e.SortExpression.ToString() == Session["Column"])
        {
            //Reverse the sort order
            if (Session["Order"] == "ASC" )
            {
                lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " DESC";
                Session["Order"] = "DESC";
            }
            else
            {
                lblOrderBy.Text = " ORDER BY "+ e.SortExpression.ToString() + " ASC";
                Session["Order"] = "ASC";
            }
        }
        else
        {
           //Different column selected, so default to ascending order
           lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " ASC";
           Session["Order"] = "ASC";
        }
         
        Session["Column"] = e.SortExpression.ToString();
        BindData();
        
    }
    protected void DataGrid1_PageIndexChanged(object source, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        DataGrid1.CurrentPageIndex = e.NewPageIndex;
        BindData();
    }
    protected void DataGrid1_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        foreach (TableCell tc in e.Item.Cells)
        {
            tc.Attributes["style"] = "border:1px solid #00A651;";
        }
        if (e.Item is GridPagerItem)
        {
            GridPagerItem pagerItem = (GridPagerItem)e.Item;
            RadComboBox PageSizeComboBox = (RadComboBox)pagerItem.FindControl("PageSizeComboBox");
            PageSizeComboBox.Visible = false;
            Label changePageSizelbl = (Label)pagerItem.FindControl("ChangePageSizeLabel");
            changePageSizelbl.Visible = false;
        } 
         if (e.Item is GridDataItem) 
        {}
    }
    protected void DataGrid1_ItemCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if(e.CommandName == "delete")
        {
            GridDataItem dataItem = e.Item as GridDataItem;
            object objResult = DatabaseHelper.deleteaddedsubscription(((Label)dataItem.FindControl("lblsubscriptionID")).Text);
            if (objResult.ToString() != "0")
            {
                BindData();
                divMessage.InnerHtml = "Subscription has been deleted successfully.";
                divMessage.Visible = true;
            }
            else
            {
                divMessage.InnerHtml = "There was problem in deleting subscription. Please try again.";
                divMessage.Visible = true;
            }
        }
    }

}
