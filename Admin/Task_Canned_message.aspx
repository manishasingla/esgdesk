<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Task_Canned_message.aspx.cs"
    Inherits="Admin_Task_Canned_message" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        Task canned message
    </title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        function checkChecked() {
            if (document.getElementById("chkShowAllHistory").checked == true) {
                document.getElementById("divNoHistory").style.display = "none";
                document.getElementById("divAllHistory").style.display = "block";
            }
            else {
                document.getElementById("divNoHistory").style.display = "block";
                document.getElementById("divAllHistory").style.display = "none";
            }
        }
    </script>

    <script type="text/javascript" language="javascript">
function confirm()
{

///if (e.keyCode==110)
if (confirm ==true)


var btndel = document.getElementById("BtnDelMsg");
btndel.click();  



else


return false;

}
    </script>

    <script language="javascript" type="text/javascript">
        function checkCat() {
            var cat = document.getElementById("txtCategory").value;
            if (cat == "") {
                alert("Please enter category name");
                return false;
            }
            else {
                var btnsend = document.getElementById("btnAddCat");
                btnsend.click();
            }

        }
    
    </script>

    <script language="javascript" type="text/javascript">
        function chkcatEdit() {
            var cat = document.getElementById("txteditCat").value;
            if (cat == "") {
                alert("Please enter category name");
                return false;
            }
            else {
                var btnsend = document.getElementById("btnUpdateCat");
                btnsend.click();
            }
        }
    
    </script>

    <script language="javascript" type="text/javascript">
        function updateParentWindow() {

            var londesc = document.getElementById('ftbComment').value;
            //// window.opener.updateValues(londesc);   
            window.opener.location.reload(true);
            window.close();


        }
    
    </script>

</head>
<body onload="checkChecked();">
    <form id="form1" runat="server">
        <div id="wrapper">
            <div id="Content">
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
                <div class="titleText" style="margin: 10px 0; display: none;">
                    Canned message</div>
                <div id="divMessage" runat="server" style="color: Red; font-weight: bold">
                </div>
                <div id="DivEntry" runat="server">
                    <div id="divTask" runat="server" class="divBorder">
                        <table width="760" cellpadding="2" border="0">
                            <tr>
                                <td align="left" colspan="4" valign="top">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="height: 31px">
                                                Message category:
                                            </td>
                                            <td style="height: 31px">
                                                <asp:DropDownList ID="DrpMsgCat" runat="server" Width="235px" OnSelectedIndexChanged="DrpMsgCat_SelectedIndexChanged"
                                                    AutoPostBack="True">
                                                </asp:DropDownList>
                                                <asp:Button ID="Button1" runat="server" BorderColor="Transparent" BorderStyle="None"
                                                    Text="Add new" OnClick="Button1_Click" CausesValidation="False" CssClass="blueBtns" />
                                                <asp:Button ID="btnEditCat" runat="server" BackColor="Transparent" BorderColor="Transparent"
                                                    BorderStyle="None" Font-Underline="True" ForeColor="Blue" Text="Edit" OnClick="btnEditCat_Click"
                                                    Width="38px" CausesValidation="False" />
                                                <asp:Button ID="BtnDelcat" runat="server" BackColor="Transparent" BorderColor="Transparent"
                                                    BorderStyle="None" Font-Underline="True" ForeColor="Blue" Text="Delete" OnClick="BtnDelcat_Click"
                                                    Width="49px" CausesValidation="False" />
                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="DrpMsgCat"
                                                ErrorMessage="please select message category">*</asp:RequiredFieldValidator>--%>
                                            </td>
                                            <td style="height: 31px">
                                            </td>
                                            <td style="height: 31px">
                                            </td>
                                            <td style="height: 31px">&nbsp;
                                                
                                            </td>
                                        </tr>
                                        <tr id="TRNewCat" runat="server" style="margin-top: 10px">
                                            <td>
                                            </td>
                                            <td style="border: solid 1px #999999;">
                                                Category name:
                                                <asp:TextBox ID="txtCategory" runat="server" Width="389px"></asp:TextBox>&nbsp;<asp:Button
                                                    ID="btnAddCat" runat="server" Text="Add" CausesValidation="false" OnClick="btnAddCat_Click"
                                                    CssClass="blueBtns" />&nbsp;<asp:Button ID="btncloseCat" runat="server" Text="Close"
                                                        CausesValidation="False" OnClick="btncloseCat_Click" CssClass="blueBtns" /><br />
                                                <span style="padding-right: 100px">
                                                    <asp:Label ID="lblcatmsg" runat="server" ForeColor="Red" Text=""></asp:Label></span>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr id="TREditCat" runat="server" style="margin-top: 10px">
                                            <td style="height: 45px">
                                            </td>
                                            <td style="border: solid 1px #999999; height: 45px;">
                                                Category name:
                                                <asp:TextBox ID="txteditCat" runat="server" Width="366px"></asp:TextBox>&nbsp;<asp:Button
                                                    ID="btnUpdateCat" runat="server" Text="Update" CausesValidation="false" OnClick="btnUpdateCat_Click"
                                                    CssClass="blueBtns" />&nbsp;<asp:Button ID="btnCnclUpdate" runat="server" Text="Close"
                                                        CausesValidation="False" OnClick="btnCnclUpdate_Click" CssClass="blueBtns" /><br />
                                                <asp:Label ID="lbleditmessase" runat="server" ForeColor="Red" Text=""></asp:Label>
                                            </td>
                                            <td style="height: 45px">
                                            </td>
                                            <td style="height: 45px">
                                            </td>
                                            <td style="height: 45px">
                                            </td>
                                        </tr>
                                        <tr id="Tr1" runat="server" style="padding-top: 13px">
                                            <td style="height: 25px">
                                                Select Short description:
                                            </td>
                                            <td style="height: 25px">
                                                <asp:DropDownList ID="DrpShrtDesc" runat="server" Width="577px" OnSelectedIndexChanged="DrpShrtDesc_SelectedIndexChanged"
                                                    AutoPostBack="True">
                                                    <asp:ListItem>[New message]</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td style="height: 25px">
                                            </td>
                                            <td style="height: 25px">
                                            </td>
                                            <td style="height: 25px">
                                            </td>
                                        </tr>
                                        <tr id="TRAddShrtDesc" runat="server" style="padding-top: 13px">
                                            <td style="height: 25px">
                                                Short description:
                                            </td>
                                            <td style="height: 25px">
                                                <asp:TextBox ID="txtshrtDesc" runat="server" Width="574px"></asp:TextBox><asp:RequiredFieldValidator
                                                    ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtshrtDesc" ErrorMessage="please enter short description">*</asp:RequiredFieldValidator>
                                            </td>
                                            <td style="height: 25px">
                                            </td>
                                            <td style="height: 25px">
                                            </td>
                                            <td style="height: 25px">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="4" valign="top">
                                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator2">
                                    </cc1:ValidatorCalloutExtender>
                                    <%--  <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server" TargetControlID="RequiredFieldValidator3">
                                </cc1:ValidatorCalloutExtender>--%>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    Long description:
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ftbComment"
                                        ErrorMessage="please enter long description">*</asp:RequiredFieldValidator>
                                </td>
                                <td align="left" valign="top">&nbsp;
                                    
                                </td>
                                <td align="left" valign="top">
                                </td>
                                <td align="right" valign="top">
                                   <%-- <asp:CheckBox ID="chkdisable" runat="server" AutoPostBack="true" CausesValidation="false" Text="Edit" OnCheckedChanged="chkdisable_CheckedChanged" Visible="false" />--%>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="4" valign="bottom">
                                    <%--********************************************************************************************************--%>
                                    <%--<FTB:FreeTextBox ID="ftbComment" runat="server" Height="200px" Width="100%"></FTB:FreeTextBox>--%>
                                    <telerik:RadEditor ID="ftbComment" runat="server" Height="300px" Width="100%">
                                        <Snippets>
                                            <telerik:EditorSnippet Name="Product Template" Value="<table><tr><td><div style='width:300px;border:2px outset #D8D2BD;padding:34px;font-family:Arial, Verdana; font-size:11px;background- color:#F1EFE6;'>Dear ____________________,<br>Thank you for inquiring about ____________. Your request will be processed in 48 hours and shipped at the address you have provided.<b>Please, contact us if you have any problems.</b></div></td></tr></table>">            
                                            </telerik:EditorSnippet>
                                            <telerik:EditorSnippet Name="Signature" Value="<strong>John Smith</strong><br>Sales Manager<br>">
                                            </telerik:EditorSnippet>
                                        </Snippets>
                                        <Links>
                                            <telerik:EditorLink Name="Telerik" Href="http://www.telerik.com">
                                                <telerik:EditorLink Name="Telerik Products" Href="http://www.telerik.com/products"
                                                    Target="_blank" />
                                                <telerik:EditorLink Name="Telerik Support Center" Href="http://www.telerik.com/support/home.aspx"
                                                    Target="_blank" />
                                                <telerik:EditorLink Name="Telerik Community" Href="http://www.telerik.com/community/home.aspx"
                                                    Target="_blank" ToolTip="Telerik Community" />
                                            </telerik:EditorLink>
                                        </Links>
                                        <Tools>
                                        </Tools>
                                        <ImageManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                        <MediaManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                        <FlashManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                        <TemplateManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                        <DocumentManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                        <Modules>
                                            <telerik:EditorModule Name="RadEditorStatistics" Visible="False" Enabled="true" />
                                            <telerik:EditorModule Name="RadEditorDomInspector" Visible="False" Enabled="true" />
                                            <telerik:EditorModule Name="RadEditorNodeInspector" Visible="False" Enabled="true" />
                                            <telerik:EditorModule Name="RadEditorHtmlInspector" Visible="False" Enabled="true" />
                                        </Modules>
                                        <Content>
                                        </Content>
                                    </telerik:RadEditor>
                                    <%--*****************************************************************************************************************--%>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top" colspan="3" style="height: 28px">
                                    <div id="divMessage1" runat="server" style="color: Red">
                                    </div>
                                </td>
                                <td align="right" valign="top" style="height: 28px">
                                    <%--<asp:Button ID="BtnAddMsg" runat="server" Text="Create" OnClick="BtnAddMsg_Click"
                                    CssClass="blueBtns" />--%>
                                    <%--====================== Change CSS Class=================================================================--%>
                                    <%--<asp:Button ID="BtnEditMsg" runat="server" Text="Insert" OnClick="BtnEditMsg_Click"
                                    Width="53px" CssClass="blueBtns" />--%>
                                    <asp:Button ID="BtnAddMsg" runat="server" Text="Create" OnClick="BtnAddMsg_Click" />
                                    <asp:Button ID="BtnEditMsg" runat="server" Text="Insert" OnClick="BtnEditMsg_Click"
                                        Width="53px" />
                                    <%--<asp:Button ID="BtnDelMsg" runat="server" Text="Delete" OnClick="BtnDelMsg_Click" CssClass="blueBtns" />--%>
                                    <asp:Button ID="BtnDelMsg" runat="server" Text="Delete" OnClick="BtnDelMsg_Click" />
                                    <%--=======================================================================================--%>
                                    <%--===================================================================================================================--%>
                                    <%--<asp:Button id="BtnEdit" runat="server" Text="Edit" CausesValidation="false"  CssClass="blueBtns" OnClick="BtnEdit_Click"/>--%>
                                    <%--===================================================================================================================--%>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
