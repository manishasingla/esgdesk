﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class _Default : System.Web.UI.Page
{
    int RequestId;
    int id;
    string sql = "";
    string strDownload = "";
    string task_action = "";
    string emailSubject = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Session["admin"] == null || Session["admin"].ToString() == "")
        //{
        //    Session["returnUrl"] = (Request.QueryString["reqid"] == null || Request.QueryString["reqid"].ToString() == "" ? "tasks.aspx" : "upload__task_attachment.aspx?id=" + Request.QueryString["id"].ToString());
        //    Response.Redirect("login.aspx");
        //    return;
        //}

        string task_id = Request["id"];
        Session["taskid"] = task_id;
        if (task_id == null || task_id == "0" || task_id == "")
        {
            id = 0;
        }
        else
        {
            if (DatabaseHelper.is_int(task_id))
            {
                id = Convert.ToInt32(task_id);
            }
            else
            {
                // Display an error because the bugid must be an integer
                divMessage.InnerHtml = "<br>Task ID must be an integer.";
                divMessage.InnerHtml += "<p><a href='tasks.aspx'>View tasks</a></p>";
                return;
            }
        }

        divMessage.InnerHtml = "";

        {
            if (!Page.IsPostBack)
            {
                divAttachment.Visible = true;
                divBackLink.InnerHtml = "<a href=\"edit_task.aspx?id=" + id.ToString() + "\">back to task</a>";

              //  string jscript = "function UploadComplete(){" + ClientScript.GetPostBackEventReference(btnSend, "") + "};";
             //   Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "FileCompleteUpload", jscript, true);
               // FlashUpload1.QueryParameters = string.Format("UserName={0}", Session["admin"].ToString());
                BindData();
            }
        }
    }


    public void BindData()
    {
        sql = "select * from tasks where deleted <>1 and task_id = " + id.ToString();

        DataSet dsRequests = DatabaseHelper.getDataset(sql);

        if (dsRequests == null || dsRequests.Tables.Count <= 0 || dsRequests.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "Task not found.<br><br><a href='tasks.aspx'>back to tasks</a>";
            divMessage.Visible = true;
            divBackLink.Visible = false;
            divFlash.Visible = false;
        }
    }

}
