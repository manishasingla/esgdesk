<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TypeList.aspx.cs" Inherits="Admin_TypeList"
    EnableEventValidation="false" %>
    
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="Notifications.ascx" TagName="Notifications" TagPrefix="uc4" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        Support types</title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
     <style type="text/css">
     table#DataGrid1_ctl00 tr th a
     {
      display:block;
      text-decoration:none;
      margin:0;
      padding:0;
      width:100%;
      height:100%;
     }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <uc2:Admin_Header ID="Admin_Header1" runat="server" />
        <div id="Content">
            <div id="Content_98">
                <div class="titleText" style="margin: 10px 0 10px 0; display: none;">
                    Categories&nbsp;|&nbsp;</div>
                    
                <div>
                  <uc4:Notifications ID="Notifications" runat="server" />
                </div>
                
                <div style="float: right; width: 100%; background-color: #ebebeb; padding: 5px 0;"
                    align="right">
                    <span class="text_default" style="float: left;">&nbsp;&nbsp;<asp:LinkButton ID="lnkAddNew"
                        runat="server" OnClick="lnkAddNew_Click" CausesValidation="False">Add new type</asp:LinkButton>
                        <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="False">&nbsp;|&nbsp;Back to type</asp:LinkButton>
                    </span><b>Records: </b>
                    <asp:Label ID="LblRecordsno" runat="server" Text=""></asp:Label>&nbsp;&nbsp;</div>
                <div style="clear: both">
                </div>
                <div id="DivEntry" runat="server" visible="false" style="float: left;">
                    <table>
                        <tr>
                            <td align="left" valign="top">
                                <table style="width: 424px">
                                    <tr>
                                        <td style="width: 424px">
                                            <table width="424" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="left" valign="top">
                                                        <img height="16" src="../images/loginBox_top.png" width="420" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="background: url(../images/loginBox_mid.png) repeat-y left top;">
                                                        <div style="text-align: center">
                                                            <div id="Div1" runat="server">
                                                                <table width="100%" cellpadding="8" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td align="left" valign="top" class="whitetext1">
                                                                            Type ID:
                                                                        </td>
                                                                        <td align="left" valign="top">
                                                                            <asp:Label CssClass="whitetext2" ID="lblId" runat="server">New</asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td align="left" valign="top" class="whitetext1">
                                                                            Type name:
                                                                        </td>
                                                                        <td align="left" valign="top">
                                                                            <asp:TextBox ID="txtName" runat="server" MaxLength="50" Width="220"></asp:TextBox>
                                                                            <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator1">
                                                                            </cc1:ValidatorCalloutExtender>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName"
                                                                                ErrorMessage="Please enter type name.">*</asp:RequiredFieldValidator>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td align="left" valign="top" class="whitetext1">
                                                                            Support URL:
                                                                        </td>
                                                                        <td align="left" valign="top">
                                                                            <asp:TextBox ID="txtSortSequece" runat="server" Width="220"></asp:TextBox>
                                                                            <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" TargetControlID="RequiredFieldValidator2">
                                                                            </cc1:ValidatorCalloutExtender>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtSortSequece"
                                                                                ErrorMessage="Please enter support url.">*</asp:RequiredFieldValidator>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <asp:HiddenField ID="HiddenField1" runat="server" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 100px">
                                                        <img height="16" src="../images/loginBox_btm.png" width="420" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 424px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td align="left" valign="middle">
                                                        <div id="divMessage" runat="server" style="color: Red; font-weight: bold">
                                                        </div>
                                                    </td>
                                                    <td align="right" valign="middle">
                                                        <asp:Button ID="btnCreate" runat="server" Text="Create" OnClick="btnCreate_Click"
                                                            CssClass="blueBtns" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div>
                <%--***********************************************************************************************************--%>
                    <telerik:RadGrid ID="DataGrid1" runat="server" OnItemCommand="DataGrid1_ItemCommand" OnItemDataBound="DataGrid1_ItemDataBound" AutoGenerateColumns="False" CellPadding="5" Width="100%">
                    <MasterTableView PagerStyle-AlwaysVisible="true" >
                    <Columns>
                    <%--0--%>
                    <telerik:GridTemplateColumn HeaderText="Type id">
                    <ItemTemplate>
                       <asp:Label  ID="lbltypeid" runat="server"  Text='<%#bind("Type_id")%>'></asp:Label>
                    </ItemTemplate> 
                    </telerik:GridTemplateColumn> 
                    <%--1--%>
                    <telerik:GridTemplateColumn HeaderText="Type name">
                    <ItemTemplate>
                    <asp:Label ID="lblTypename" runat="server" Text='<%#bind("Type_name") %>'></asp:Label>
                    </ItemTemplate> 
                    </telerik:GridTemplateColumn> 
                    <%--2--%>
                    <telerik:GridTemplateColumn HeaderText="Support Url">
                    <ItemTemplate>
                    <asp:Label id="lblSupportUrl" runat="server" Text='<%#bind("Support_Url") %>'></asp:Label>
                    </ItemTemplate> 
                    </telerik:GridTemplateColumn> 
                    <%--3--%>
                    <telerik:GridTemplateColumn HeaderText="Edit">
                    <ItemTemplate>
                    <asp:LinkButton id="lnkedit" CommandName="edit" runat="server"  Text="edit"></asp:LinkButton>
                    </ItemTemplate> 
                    </telerik:GridTemplateColumn> 
                    <%--4--%>
                    <telerik:GridTemplateColumn HeaderText="Delete">
                    <ItemTemplate>
                    <asp:LinkButton  ID="lnkdelete" CommandName="delete"  runat="server"  Text="delete" ></asp:LinkButton>
                    </ItemTemplate> 
                    </telerik:GridTemplateColumn> 
                    
                    </Columns> 
                    </MasterTableView> 
                    </telerik:RadGrid> 
                    
                    
                    
                    &nbsp;
                <%--***********************************************************************************************************--%>
                    <asp:HiddenField ID="prevName" runat="server" />
                </div>
            </div>
        </div>
    </div>
    <uc1:footer ID="Footer1" runat="server" />
    </form>
</body>
</html>
