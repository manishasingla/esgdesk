﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UnreadTask.aspx.cs" Inherits="Admin_UnreadTask" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Src="Notifications.ascx" TagName="Notifications" TagPrefix="uc4" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
       Unread task</title>

    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="slider.css" />

    <script type="text/javascript" src="slider.js"></script>
    <script type="text/javascript">
        function get_(div_) {
            div_ = div_.id + "1";
            document.getElementById(div_).style.display = "block";
        }
        function get_1(div_) {
            div_ = div_.id + "1";
            document.getElementById(div_).style.display = "none";
        }
    </script>

    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <uc2:Admin_Header ID="Admin_Header1" runat="server" />
    <div>
        <table width="100%">
            <tr>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:GridView Width="100%" ID="unreadgrd" runat="server" CellPadding="8" AutoGenerateColumns="false"
                        BorderColor="#cccccc" AllowPaging="true" PageSize="20" 
                        OnPageIndexChanging="unreadgrd_PageIndexChanging" 
                        onrowdatabound="unreadgrd_RowDataBound">
                        <HeaderStyle Font-Bold="false" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Top" BackColor="#282c5f"
                            CssClass="tblTitle1" />
                        <PagerSettings Mode="NextPrevious" FirstPageText="First" PreviousPageText="Prev"
                            NextPageText="Next" LastPageText="Last" />
                        <Columns>
                            <asp:BoundField DataField="task_id" HeaderText="Task Id" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                   <asp:Label Font-Size="12px" ID="lblshrtDesc" runat="server" Width="450px" Text='<%# bind("short_desc") %>'></asp:Label>
                                    <div id="lblshrtDesc1" runat="server" style="display: none; position: absolute; background-color: #FEFFB3;
                                        border: solid 1px #333333; width: 730px; margin-top: 5px; padding-left: 3px">
                                        <asp:Label ID="LblLastcomment" runat="server" Text='<%# bind("ShowAllComment") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="project" HeaderText="Project" />
                            <asp:BoundField DataField="status" HeaderText="Status" />
                            <asp:BoundField DataField="priority" HeaderText="Priority" />
                            <asp:BoundField DataField="assigned_to_user" HeaderText="Assign To" />
                            <asp:HyperLinkField HeaderText="Edit" Text="edit" DataNavigateUrlFields="task_id"
                                DataNavigateUrlFormatString="edit_task.aspx?id={0}" NavigateUrl="edit_task.aspx">
                                <HeaderStyle Width="25px" />
                            </asp:HyperLinkField>
                        </Columns>
                        <RowStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                            Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Top" Font-Size="12px" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    <uc1:footer ID="Footer1" runat="server" />
    </form>
</body>
</html>
