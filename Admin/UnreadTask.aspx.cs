﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
 

public partial class Admin_UnreadTask : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["admin"] == null || Session["admin"].ToString() == "")
            {
                Response.Redirect("login.aspx");
                return;
            }
            else
            {
                BindData();
            }
        }
    }
    private void BindData()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = DatabaseHelper.getUnreadTaskbyAdmin();
            ds.Tables[0].Columns.Add("ShowAllComment");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                ds.Tables[0].Rows[i]["short_desc"] = "<a  href=\"edit_task.aspx?id=" + ds.Tables[0].Rows[i]["task_id"].ToString() + "  \">" + ds.Tables[0].Rows[i]["short_desc"].ToString() + "</a>";

                try
                {
                    string sql;
                    string LastComment = "";

                    sql = "select *  from task_comments where task_comments.deleted <> 1  and task_id =" + ds.Tables[0].Rows[i]["task_id"].ToString() + " order by tc_id desc";


                    DataSet dslastcomment = DatabaseHelper.getDataset(sql);

                    if (dslastcomment != null && dslastcomment.Tables[0].Rows.Count > 0)
                    {

                        for (int j = 0; j < dslastcomment.Tables[0].Rows.Count; j++)
                        {

                            if (dslastcomment.Tables[0].Rows[j]["qflag"].ToString() == "1")
                            {

                                LastComment += "<span style='color:Red'>" + "Question posted by " + dslastcomment.Tables[0].Rows[j]["username"].ToString() + " for  " + dslastcomment.Tables[0].Rows[j]["QuesTo"].ToString() + " on " + DateTime.Parse(dslastcomment.Tables[0].Rows[j]["post_date"].ToString()).ToString("dd MMM h:mm tt") + "</span>";

                                LastComment += "<br/><span>" + dslastcomment.Tables[0].Rows[j]["comment"].ToString() + "</span>";

                                LastComment += "<span style=\"border-bottom: solid 1px green; margin-bottom: 2px; height:1px\"></span><br/><hr />";

                            }
                            else
                            {

                                LastComment += "<span style='color:Green'>" + "Comment " + dslastcomment.Tables[0].Rows[j]["tc_id"].ToString() + " posted by " + dslastcomment.Tables[0].Rows[j]["username"].ToString() + " on " + DateTime.Parse(dslastcomment.Tables[0].Rows[j]["post_date"].ToString()).ToString("dd MMM h:mm tt") + "</span>";

                                LastComment += "<br/><span>" + dslastcomment.Tables[0].Rows[j]["comment"].ToString() + "</span>";

                                LastComment += "<span style=\"border-bottom: solid 1px green; margin-bottom: 2px; height:1px\"></span><br/><hr />";

                            }

                        }
                    }

                    ds.Tables[0].Rows[i]["ShowAllComment"] = LastComment.ToString();
                }
                catch(Exception ex)
                {

                }
                //-----------------------------------------------------------------------------------------------------------------------------
               
            }

            unreadgrd.DataSource = ds;
            unreadgrd.DataBind();
        }catch(Exception ex)
        {}
        
    }
    protected void unreadgrd_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        unreadgrd.PageIndex= e.NewPageIndex;
        BindData();
    }
    protected void unreadgrd_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.DataItemIndex != -1)
        {
            Label LblAllComment = (Label)e.Row.FindControl("lblshrtDesc");
            LblAllComment.Attributes.Add("onmouseover", "get_(this);");
            LblAllComment.Attributes.Add("onmouseout", "get_1(this);");
        }
    }
}
