﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UnreadTask1.aspx.cs" Inherits="UnreadTask1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Src="Notifications.ascx" TagName="Notifications" TagPrefix="uc4" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <%= ConfigurationManager.AppSettings["CompanyName"].ToString()%></title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="slider.css" />

    <script type="text/javascript" src="slider.js"></script>
    <script type="text/javascript">
        function get_(div_) {
            div_ = div_.id + "1";
            document.getElementById(div_).style.display = "block";
        }
        function get_1(div_) {
            div_ = div_.id + "1";
            document.getElementById(div_).style.display = "none";
        }
    </script>

    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <uc2:Admin_Header ID="Admin_Header1" runat="server" />
    <div>
        <table width="100%">
            <tr>
               <%-- <td>
                </td>
                <td>
                </td>--%>
                <td align="left" valign="top" colspan="2">
                                    <div style="padding: 0px 0 0px 5px; margin: 2px  0 -2px 0;">
                                        <table >
                                            <tr>
                                                <td id="s1">
                                                    <span class="function-menu" onmousedown="slideContent('section-1')">Filter</span>
                                                </td>
                                                <td id="s2">
                                                    <span class="function-menu" onmousedown="slideContent('section-2')">Action</span>
                                                </td>
                                                <td id="s3">
                                                    <span class="function-menu-1" onmousedown="slideContent('section-3')">Quick Search</span>
                                                </td>
                                                <td>
                                                    <span id="TopFilterLable" runat="server" class="topfilter"></span>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div>
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" onclick="return TABLE1_onclick()"
                                            id="TABLE1">
                                            <tr>
                                                <td align="right" valign="bottom">
                                                    <img src="../images/filter_corner_l.png" width="11" height="59" />
                                                </td>
                                                <td bgcolor="#282C5F" width="100%">
                                                    <div class="slider">
                                                        <div class="slidercontent" id="slider">
                                                            <div id="section-1" class="section upper" style="margin-top: 0px;">
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td width="45" valign="top" style="padding-top: 20px;">
                                                                            <b style="color: #e1ac10;">Filter:</b>
                                                                        </td>
                                                                        <td valign="top" class="whitetext2" width="9%">
                                                                            <br />
                                                                            <span class="whitetext2">
                                                                                <asp:DropDownList ID="drpFilter" runat="server" AutoPostBack="True" Width="96%" OnSelectedIndexChanged="drpFilter_SelectedIndexChanged"
                                                                                    CssClass="filerDrpodown">
                                                                                </asp:DropDownList>
                                                                            </span>
                                                                        </td>
                                                                        <td width="9%" valign="top" style="color: White; font-size: 14px">
                                                                            Project<br />
                                                                            <span class="whitetext2"><span class="whitetext2"><span class="whitetext2">
                                                                                <asp:DropDownList ID="drpProjects" runat="server" CssClass="filerDrpodown" AutoPostBack="True"
                                                                                    OnSelectedIndexChanged="drpProjects_SelectedIndexChanged" Width="96%">
                                                                                </asp:DropDownList>
                                                                            </span></span></span>
                                                                        </td>
                                                                        <td width="9%" valign="top" style="color: White; font-size: 14px">
                                                                            Category<br />
                                                                            <asp:DropDownList ID="drpCategories" runat="server" CssClass="filerDrpodown" AutoPostBack="True"
                                                                                OnSelectedIndexChanged="drpCategories_SelectedIndexChanged" Width="96%">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td width="9%" valign="top" style="color: White; font-size: 14px">
                                                                            Reported by<br />
                                                                            <asp:DropDownList ID="drpReportedBy" runat="server" CssClass="filerDrpodown" AutoPostBack="True"
                                                                                OnSelectedIndexChanged="drpReportedBy_SelectedIndexChanged" Width="96%">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td width="9%" valign="top" style="color: White; font-size: 14px">
                                                                            Priority<br />
                                                                            <asp:DropDownList ID="drpPriorities" runat="server" CssClass="filerDrpodown" AutoPostBack="True"
                                                                                OnSelectedIndexChanged="drpPriorities_SelectedIndexChanged" Width="96%">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td id="tdAssignedTo" runat="server" width="9%" valign="top" style="color: White;
                                                                            font-size: 14px">
                                                                            Assigned to<br />
                                                                            <span class="whitetext2">
                                                                                <asp:DropDownList ID="drpAssignedTo" runat="server" CssClass="filerDrpodown" AutoPostBack="True"
                                                                                    OnSelectedIndexChanged="drpAssignedTo_SelectedIndexChanged" Width="96%">
                                                                                </asp:DropDownList>
                                                                            </span>
                                                                        </td>
                                                                        <td width="9%" valign="top" style="color: White; font-size: 14px">
                                                                            Status<br />
                                                                            <asp:DropDownList ID="drpStatuses" runat="server" CssClass="filerDrpodown" AutoPostBack="True"
                                                                                OnSelectedIndexChanged="drpStatuses_SelectedIndexChanged" Width="96%">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td valign="top" width="85" style="padding-top: 20px">
                                                                            <asp:Button ID="btnClearFilters" runat="server" OnClick="btnClearFilters_Click" Text="Clear filters"
                                                                                CssClass="clearfilter" />
                                                                        </td>
                                                                        <td valign="top" class="whitetext2">&nbsp;
                                                                            
                                                                        </td>
                                                                        <td valign="top" class="whitetext2" style="min-width: 140px; color: White; font-size: 14px">
                                                                            <asp:CheckBox ID="chkViewDeleted" runat="server" Text="View only deleted" AutoPostBack="True"
                                                                                OnCheckedChanged="chkViewDeleted_CheckedChanged" /><br />
                                                                            <asp:CheckBox ID="chkHideClosed" runat="server" Text="View closed" AutoPostBack="True" Visible="false"
                                                                                OnCheckedChanged="chkHideClosed_CheckedChanged" />
                                                                        </td>
                                                                        <td valign="top" class="whitetext2" style="min-width: 170px; color: White; font-size: 14px">
                                                                            <asp:CheckBox ID="ChkReadTask" runat="server" Text="Include read&nbsp;&nbsp;" Checked="true" Visible="false"
                                                                                AutoPostBack="True" OnCheckedChanged="ChkReadTask_CheckedChanged" /><br />
                                                                            <span style="color: #E1AC10; float: right;"><b>Records: </b>
                                                                                <asp:Label ID="LblRecordsno" runat="server" Text=""></asp:Label></span>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                           <div id="section-2" class="section upper" style="padding-top: 5px;">
                                         
                                         
                                          <%--<div id="DivMiltiselection" runat="server" style="color: White; font-size: 14px">
                                                                    <b style="color: #e1ac10;">Action:</b>
                                                                    <asp:LinkButton ID="LnkBtnToDeleteAll" runat="server" OnClick="LnkBtnToDeleteAll_Click"
                                                                        CssClass="whitetext2">Delete selected</asp:LinkButton>&nbsp;&nbsp;
                                                                    <asp:LinkButton ID="LnkBtnToMarkAllRead" runat="server" OnClick="LnkBtnToMarkAllRead_Click"
                                                                        CssClass="whitetext2">Mark read</asp:LinkButton><br />
                                                                    <span style="float: left;"><span style="color: Red;">&nbsp;</span></span>
                                                                    
                                                                       <asp:Button ID="LnkBtnToChngPR" runat="server" BackColor="transparent" BorderStyle="none"
                                                                        Text="Change priority to" OnClick="LnkBtnToChngPR_Click" Style="color: White;
                                                                        font-size: 14px; margin-left: -15px" />
                                                                    <asp:DropDownList ID="DrpMultipleTaskPR" runat="server" CssClass="filerDrpodown"
                                                                        AutoPostBack="true" Width="180px">
                                                                    </asp:DropDownList>
                                                                    &nbsp;&nbsp;<asp:Button ID="LnkChngAssingedTo" runat="server" BackColor="transparent"
                                                                        BorderStyle="none" Text="Change assigned to" OnClick="LnkChngAssingedTo_Click"
                                                                        Style="color: White; font-size: 14px" />
                                                                    <asp:DropDownList ID="DrpMultipleTaskAssing" runat="server" CssClass="filerDrpodown"
                                                                        AutoPostBack="false" Width="180px">
                                                                    </asp:DropDownList>
                                                                    &nbsp;&nbsp;<asp:Button ID="LnkChngStatus" runat="server" BackColor="transparent"
                                                                        BorderStyle="none" Text="Change status to" OnClick="LnkChngStatus_Click" Style="color: White;
                                                                        font-size: 14px" />
                                                                    <asp:DropDownList ID="DrpMultipleTaskStatus" runat="server" CssClass="filerDrpodown"
                                                                        AutoPostBack="false" Width="180px">
                                                                    </asp:DropDownList>
                                                                </div>--%>
                                                            </div>
                                                            
                                                            <div id="section-3" class="section upper" >
                                                            Test
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td valign="bottom">
                                                    <img src="../images/filter_corner_r.png" width="11" height="59" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:GridView Width="100%" ID="unreadgrd" runat="server" CellPadding="8" AutoGenerateColumns="false"
                        BorderColor="#cccccc" AllowPaging="true" PageSize="20" 
                        OnPageIndexChanging="unreadgrd_PageIndexChanging" 
                        onrowdatabound="unreadgrd_RowDataBound">
                        <HeaderStyle Font-Bold="false" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Top" BackColor="#282c5f"
                            CssClass="tblTitle1" />
                        <PagerSettings Mode="NextPrevious" FirstPageText="First" PreviousPageText="Prev"
                            NextPageText="Next" LastPageText="Last" />
                        <Columns>
                            <asp:BoundField DataField="task_id" HeaderText="Task Id" />
                            <asp:TemplateField HeaderText="Short description">
                                <ItemTemplate>
                                   <asp:Label Font-Size="12px" ID="lblshrtDesc" runat="server" Width="450px" Text='<%# bind("short_desc") %>'></asp:Label>
                                    <div id="lblshrtDesc1" runat="server" style="display: none; position: absolute; background-color: #FEFFB3;
                                        border: solid 1px #333333; width: 730px; margin-top: 5px; padding-left: 3px">
                                        <asp:Label ID="LblLastcomment" runat="server" Text='<%# bind("ShowAllComment") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="project" HeaderText="Project" />
                            <asp:BoundField DataField="status" HeaderText="Status" />
                            <asp:BoundField DataField="priority" HeaderText="Priority" />
                            <asp:BoundField DataField="assigned_to_user" HeaderText="Assign To" />
                            <asp:HyperLinkField HeaderText="Edit" Text="edit" DataNavigateUrlFields="task_id"
                                DataNavigateUrlFormatString="edit_task.aspx?id={0}" NavigateUrl="edit_task.aspx">
                                <HeaderStyle Width="25px" />
                            </asp:HyperLinkField>
                        </Columns>
                        <RowStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                            Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Top" Font-Size="12px" />
                    </asp:GridView>
                    
                </td>
            </tr>
            <tr>
            <td colspan="2">
                 <asp:Panel ID="Panel7" runat="server" Width="100%">
                   <asp:LinkButton ID="PreviousPage1" runat="server" OnClick="PreviousPage_Click" Text="<< Prev"></asp:LinkButton>
                    <asp:Label ID="lblPageDetails1" runat="server"></asp:Label>
                    <asp:LinkButton ID="NextPage1" runat="server" OnClick="NextPage_Click" Text="Next >>"></asp:LinkButton>
                  </asp:Panel>
            </td>
            </tr>
        </table>
    </div>
    <uc1:footer ID="Footer1" runat="server" />
    </form>
</body>
</html>
