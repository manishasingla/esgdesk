﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text;


public partial class UnreadTask1 : System.Web.UI.Page
{

    string sql = "";
    int page = 0;
    DataSet ds = new DataSet();
    static DataSet ds_dropdowns;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ViewState["VarPagesize"] = 0;
            if (Session["admin"] == null || Session["admin"].ToString() == "")
            {
                Response.Redirect("login.aspx");
                return;
            }
            else
            {
                if (!DatabaseHelper.can_Show_Deleted_Tasks(Session["admin"].ToString()))
                {
                    chkViewDeleted.Visible = false;
                }
                else
                {
                    chkViewDeleted.Visible = true;
                }
                if (Session["filter"] != null)
                {
                    load_dropdowns();
                    load_filterDropDown();
                }
                else {
                    load_dropdowns();
                    load_filterDropDown();
                }
                BindData();
            }
        }
    }
    private void BindData()
    {
        try
        {
            page = 0;
            //filter criteria--------------------------------------
            string filter1 = "";
            if (drpProjects.SelectedIndex != 0)
            {
                filter1 += " and project='" + drpProjects.SelectedValue + "'";
            }
            if (drpCategories.SelectedIndex != 0)
            {
                filter1 += " and category='"+ drpCategories.SelectedValue +"'";
            }
            if (drpReportedBy.SelectedIndex != 0)
            {
                filter1 += " and reported_user='" + drpReportedBy.SelectedValue + "'";
            }
            if (drpPriorities.SelectedIndex != 0)
            {
                filter1 += " and priority='" + drpPriorities.SelectedValue + "'";
            }
            if (drpAssignedTo.SelectedIndex != 0)
            {
                filter1 += " and assigned_to_user='" + drpAssignedTo.SelectedValue + "'";
            }
            if (drpStatuses.SelectedIndex != 0)
            {
                filter1 += " and status='" + drpStatuses.SelectedValue + "'";
            }
            //------------------------------------------------------
            int totalCount = DatabaseHelper.getCountUnreadTaskbyAdmin1(filter1);
            ViewState["totalRec"] = totalCount;
            ds = DatabaseHelper.getUnreadTaskbyAdmin1(filter1, ViewState["VarPagesize"].ToString());
            ds.Tables[0].Columns.Add("ShowAllComment");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                ds.Tables[0].Rows[i]["short_desc"] = "<a  href=\"edit_task.aspx?id=" + ds.Tables[0].Rows[i]["task_id"].ToString() + "  \">" + ds.Tables[0].Rows[i]["short_desc"].ToString() + "</a>";

                try
                {
                    string sql;
                    string LastComment = "";
                    //StringBuilder LastComment1 = new StringBuilder(LastComment);
                    sql = "select top 1 * from task_comments where task_comments.deleted <> 1  and task_id =" + ds.Tables[0].Rows[i]["task_id"].ToString() + " order by tc_id desc";

                   //sql = @"select "; //tc_id,task_id,username,post_date,comment,deleted,qflag,QuesTo,
                   //sql += "'ShowAllComment' = case qflag ";
                   //sql += "when 1 then '<span style=''color:Red''>Question posted by '+ username  + 'for ' + QuesTo + ' on ' + CONVERT(varchar(40), post_date, 113) + '</span><br/><span>' + comment +'</span><span style=\"border-bottom: solid 1px green; margin-bottom: 2px; height:1px\"></span><br/><hr />'";
                   //sql += "when 0 then '<span style=''color:Green''>Comment ' + cast(tc_id as varchar(30)) + ' posted by ' + username + ' on ' + CONVERT(varchar(40), post_date, 113) + '</span><br/><span>' + comment + '</span><span style=\"border-bottom: solid 1px green; margin-bottom: 2px; height:1px\"></span><br/><hr />'";
                   //sql += "end from task_comments where task_comments.deleted <> 1  and task_id =5453 order by tc_id desc";


                    DataSet dslastcomment = DatabaseHelper.getDataset(sql);

                    if (dslastcomment != null && dslastcomment.Tables[0].Rows.Count > 0)
                    {
                        for (int j = 0; j < dslastcomment.Tables[0].Rows.Count; j++)
                        {
                            if (dslastcomment.Tables[0].Rows[j]["qflag"].ToString() == "1")
                            {
                                LastComment += "<span style='color:Red'>" + "Question posted by " + dslastcomment.Tables[0].Rows[j]["username"].ToString() + " for  " + dslastcomment.Tables[0].Rows[j]["QuesTo"].ToString() + " on " + DateTime.Parse(dslastcomment.Tables[0].Rows[j]["post_date"].ToString()).ToString("dd MMM h:mm tt") + "</span>";

                                LastComment += "<br/><span>" + dslastcomment.Tables[0].Rows[j]["comment"].ToString() + "</span>";

                                LastComment += "<span style=\"border-bottom: solid 1px green; margin-bottom: 2px; height:1px\"></span><br/><hr />";
                            }
                            else
                            {
                                LastComment += "<span style='color:Green'>" + "Comment " + dslastcomment.Tables[0].Rows[j]["tc_id"].ToString() + " posted by " + dslastcomment.Tables[0].Rows[j]["username"].ToString() + " on " + DateTime.Parse(dslastcomment.Tables[0].Rows[j]["post_date"].ToString()).ToString("dd MMM h:mm tt") + "</span>";

                                LastComment += "<br/><span>" + dslastcomment.Tables[0].Rows[j]["comment"].ToString() + "</span>";

                                LastComment += "<span style=\"border-bottom: solid 1px green; margin-bottom: 2px; height:1px\"></span><br/><hr />";
                            }
                            //ds.Tables[0].Rows[i]["ShowAllComment"] += LastComment.ToString();
                            //LastComment = "";
                        }
                    }

                    ds.Tables[0].Rows[i]["ShowAllComment"] = LastComment.ToString();
                    //ds.Tables[0].Rows[i]["ShowAllComment"] = "";
                }
                catch(Exception ex)
                {

                }
                //-----------------------------------------------------------------------------------------------------------------------------
            }

            unreadgrd.DataSource = ds;
            unreadgrd.DataBind();

            LblRecordsno.Text = ds.Tables[0].Rows.Count.ToString();

            // for paging----------------------
            try
            {
                PagedDataSource objPds = new PagedDataSource();
                objPds.DataSource = ds.Tables[0].DefaultView;
                objPds.AllowPaging = true;
                objPds.PageSize = 20;

                if (page < 0)
                {
                    page++;
                }
                else if (page >= objPds.PageCount)
                {
                    page = objPds.PageCount - 1;
                }

                objPds.CurrentPageIndex = page;

                int intFrom = ((int)ViewState["VarPagesize"] + 1);

                int intTo = ((intFrom + 19) > (int)ViewState["totalRec"] ? (intFrom + ((int)ViewState["totalRec"] - intFrom)) : (intFrom + 19));

                intTo = intTo >= (int)ViewState["totalRec"] ? (int)ViewState["totalRec"] : intTo;

                //lblPageDetails.Visible = lblPageDetails1.Visible = true;
                lblPageDetails1.Visible = true;

                //lblPageDetails.Text = " " + intFrom + " - " + intTo + " of " + (int)ViewState["totalRec"] + " ";
                lblPageDetails1.Text = " " + intFrom + " - " + intTo + " of " + (int)ViewState["totalRec"] + " ";
                

                //NextPage.Visible = NextPage1.Visible = intTo < (int)ViewState["totalRec"];
                NextPage1.Visible = intTo < (int)ViewState["totalRec"];
                //PreviousPage.Visible = PreviousPage1.Visible = intFrom > 9;
                PreviousPage1.Visible = intFrom > 19;

                try
                {
                    unreadgrd.DataSource = objPds;
                    unreadgrd.DataBind();
                }
                catch { }

                ViewState["pageindex"] = page;
                //RegisterArrays();
            }
            catch { }
            //-----------------------------------

        }catch(Exception ex)
        {}
    }
   private void SetPrevNextMode()
   {

   }
    protected void unreadgrd_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        unreadgrd.PageIndex= e.NewPageIndex;
        ViewState["VarPagesize"] = e.NewPageIndex * 20;
        BindData();
    }
    protected void unreadgrd_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.DataItemIndex != -1)
        {
            Label LblAllComment = (Label)e.Row.FindControl("lblshrtDesc");
            LblAllComment.Attributes.Add("onmouseover", "get_(this);");
            LblAllComment.Attributes.Add("onmouseout", "get_1(this);");
        }
    }

    void load_dropdowns()
    {

        // projects
        try
        {
            sql = @"select project_name
		            from projects
		            where active = 'Y' order by project_name;";

            // categories
            sql += "\nselect category_name from categories order by sort_seq, category_name;";

            // priorities
            sql += "\nselect priority_name from priorities order by sort_seq, priority_name;";

            // statuses
            sql += "\nselect status_name from statuses order by sort_seq, status_name;";

            // users
            sql += "\nselect username from users where active = 'Y' order by username;";

            // do a batch of sql statements
            if(ds_dropdowns == null)
                ds_dropdowns = DatabaseHelper.getDataset(sql);

            drpProjects.DataSource = ds_dropdowns.Tables[0];
            drpProjects.DataTextField = "project_name";
            drpProjects.DataValueField = "project_name";
            drpProjects.DataBind();
            drpProjects.Items.Insert(0, new ListItem("[no filter]", "0"));


            drpCategories.DataSource = ds_dropdowns.Tables[1];
            drpCategories.DataTextField = "category_name";
            drpCategories.DataValueField = "category_name";
            drpCategories.DataBind();
            drpCategories.Items.Insert(0, new ListItem("[no filter]", "0"));

            drpPriorities.DataSource = ds_dropdowns.Tables[2];
            drpPriorities.DataTextField = "priority_name";
            drpPriorities.DataValueField = "priority_name";
            drpPriorities.DataBind();
            drpPriorities.Items.Insert(0, new ListItem("[no filter]", "0"));
            //DrpMultipleTaskPR.DataSource = ds_dropdowns.Tables[2];
            //DrpMultipleTaskPR.DataTextField = "priority_name";
            //DrpMultipleTaskPR.DataValueField = "priority_name";
            //DrpMultipleTaskPR.DataBind();
            //DrpMultipleTaskPR.Items.Insert(0, new ListItem("[no filter]", "0"));

            drpStatuses.DataSource = ds_dropdowns.Tables[3];
            drpStatuses.DataTextField = "status_name";
            drpStatuses.DataValueField = "status_name";
            drpStatuses.DataBind();
            drpStatuses.Items.Insert(0, new ListItem("[no filter]", "0"));
            //DrpMultipleTaskStatus.DataSource = ds_dropdowns.Tables[3];
            //DrpMultipleTaskStatus.DataTextField = "status_name";
            //DrpMultipleTaskStatus.DataValueField = "status_name";
            //DrpMultipleTaskStatus.DataBind();
            //DrpMultipleTaskStatus.Items.Insert(0, new ListItem("[no filter]", "0"));

            drpReportedBy.DataSource = ds_dropdowns.Tables[4];
            drpReportedBy.DataTextField = "username";
            drpReportedBy.DataValueField = "username";
            drpReportedBy.DataBind();
            drpReportedBy.Items.Insert(0, new ListItem("[no filter]", "0"));

            drpAssignedTo.DataSource = ds_dropdowns.Tables[4];
            drpAssignedTo.DataTextField = "username";
            drpAssignedTo.DataValueField = "username";
            drpAssignedTo.DataBind();
            drpAssignedTo.Items.Insert(0, new ListItem("[no filter]", "0"));
            //DrpMultipleTaskAssing.DataSource = ds_dropdowns.Tables[4];
            //DrpMultipleTaskAssing.DataTextField = "username";
            //DrpMultipleTaskAssing.DataValueField = "username";
            //DrpMultipleTaskAssing.DataBind();
            //DrpMultipleTaskAssing.Items.Insert(0, new ListItem("[no filter]", "0"));
        }
        catch { }
    }
    void load_filterDropDown()
    {
        drpFilter.Items.Clear();
        if (DatabaseHelper.isAdmin(Session["Admin"].ToString()))
        {
            sql = "select username from users where active='Y' and admin='Y' and username <> '" + Session["Admin"] + "' order by username";

            DataSet dsAdmin = DatabaseHelper.getDataset(sql);

            if (dsAdmin != null)
            {
                if (dsAdmin.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsAdmin.Tables[0].Rows.Count; i++)
                    {
                        drpFilter.Items.Add("Assigned by " + dsAdmin.Tables[0].Rows[i]["username"]);
                    }
                }
            }

            drpFilter.Items.Add("Assigned by " + Session["Admin"]);
            drpFilter.Items.Add("Assigned by me");
            drpFilter.Items.Add("All tasks by last updated");
            drpFilter.Items.Add("All emps CWO");

            drpFilter.Items.Add("Show my open tasks");

            drpFilter.SelectedValue = (string)DatabaseHelper.executeScalar("select default_selected_item from users where username='" + Session["admin"] + "'");
        }
        else
        {
            drpFilter.Items.Add("Show my open tasks");
        }
    }
    protected void drpFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["filter"] = null;
        Session["filterunread"] = null;
        load_dropdowns();
        setFileterSession();
        //unreadgrd.CurrentPageIndex = 0;
        unreadgrd.PageIndex = 0;
        BindData();
    }
    protected void drpProjects_SelectedIndexChanged(object sender, EventArgs e)
    {
        setFileterSession();
        unreadgrd.PageIndex = 0;
        ViewState["VarPagesize"] = 0;
        BindData();
    }
    protected void drpCategories_SelectedIndexChanged(object sender, EventArgs e)
    {
        setFileterSession();
        //unreadgrd.CurrentPageIndex = 0;
        unreadgrd.PageIndex = 0;
        ViewState["VarPagesize"] = 0;
        BindData();
    }

    protected void drpReportedBy_SelectedIndexChanged(object sender, EventArgs e)
    {
        ///Session["filterunread"] = null;
        setFileterSession();
        // unreadgrd.CurrentPageIndex = 0;
        unreadgrd.PageIndex = 0;
        ViewState["VarPagesize"] = 0;
        BindData();
    }

    protected void drpPriorities_SelectedIndexChanged(object sender, EventArgs e)
    {
        setFileterSession();
        //unreadgrd.CurrentPageIndex = 0;
        unreadgrd.PageIndex = 0;
        ViewState["VarPagesize"] = 0;
        BindData();
    }

    protected void drpAssignedTo_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["filterunread"] = null;
        setFileterSession();
        // unreadgrd.CurrentPageIndex = 0;
        unreadgrd.PageIndex = 0;
        ViewState["VarPagesize"] = 0;
        BindData();
    }

    protected void drpStatuses_SelectedIndexChanged(object sender, EventArgs e)
    {
        setFileterSession();
        //  unreadgrd.CurrentPageIndex = 0;
        unreadgrd.PageIndex = 0;
        ViewState["VarPagesize"] = 0;
        BindData();
    }
    protected void btnClearFilters_Click(object sender, EventArgs e)
    {
        Session["filter"] = null;
        Session["filterunread"] = null;
        load_dropdowns();
        load_filterDropDown();
        chkViewDeleted.Checked = false;
        chkHideClosed.Checked = false;
        ChkReadTask.Checked = true;
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        setFileterSession();
        BindData();
    }
    private void setFileterSession()
    {
        Session["HideClosed"] = chkHideClosed.Checked;
        Session["IncludeDeleted"] = chkViewDeleted.Checked;
        Session["Filter"] = drpFilter.SelectedValue;
        Session["Project"] = drpProjects.SelectedValue;
        Session["Category"] = drpCategories.SelectedValue;
        Session["ReportedBy"] = drpReportedBy.SelectedValue;
        Session["Priority"] = drpPriorities.SelectedValue;
        Session["AssignedTo"] = drpAssignedTo.SelectedValue;
        Session["Status"] = drpStatuses.SelectedValue;
        //Session["OrderBy"] = lblOrderBy.Text;
        Session["ReadTask"] = ChkReadTask.Checked;
    }
    protected void chkViewDeleted_CheckedChanged(object sender, EventArgs e)
    {
        load_dropdowns();
        load_filterDropDown();
        //lblOrderBy.Text = " ORDER BY last_updated_date desc";
        setFileterSession();
        BindData();
    }
    protected void chkHideClosed_CheckedChanged(object sender, EventArgs e)
    {
        //setFileterSession();
        //BindData();
    }
    protected void ChkReadTask_CheckedChanged(object sender, EventArgs e)
    {

        //setFileterSession();
        //BindData();

    }
    protected void PreviousPage_Click(object sender, EventArgs e)
    {
        try
        {
            page = Convert.ToInt32(ViewState["pageindex"]) - 1;
            ViewState["VarPagesize"] = (int)ViewState["VarPagesize"] - 20;
            BindData();
        }
        catch (Exception ex)
        {
            //Response.Redirect("index");
        }
    }
    protected void NextPage_Click(object sender, EventArgs e)
    {
        try
        {
            page = Convert.ToInt32(ViewState["pageindex"]) + 1;
            ViewState["VarPagesize"] = (int)ViewState["VarPagesize"] + 20;
            BindData();
        }
        catch (Exception ex)
        {
            //Response.Redirect("index");
        }
    }
    protected void LnkBtnToDeleteAll_Click(object sender, EventArgs e)
    {

        int i = 0;
        foreach (GridViewRow row in unreadgrd.Rows)
        {
            // Access the CheckBox 
            try
            {
                CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                ///Label GetItemID = (Label)row.FindControl("lblID");
                if (cb != null && cb.Checked)
                {
                    i = i + 1;


                    sql = "Update tasks set deleted=1,last_updated_date = getdate(),last_updated_user='" + Session["admin"].ToString().Replace("'", "''") + "' where task_id=" + row.Cells[19].Text;

                    int intResult = DatabaseHelper.executeNonQuery(sql);

                    if (intResult != 0)
                    {

                        sql = @" insert into task_updates([task_id],[username],[post_date],[comment]) 
                            values(" + row.Cells[19].Text + ",'" + Session["admin"].ToString().Replace("'", "''") + "',getdate(),'Task was deleted.'); ";

                        intResult = DatabaseHelper.executeNonQuery(sql);

                        try
                        {

                            sql = @"select *  from task_comments where task_id =" + row.Cells[19].Text + " and  tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "')";
                            /// sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

                            DataSet dscomment = DatabaseHelper.getDataset(sql);

                            if (dscomment != null && dscomment.Tables[0].Rows.Count > 0)
                            {
                                for (int j = 0; j < dscomment.Tables[0].Rows.Count; j++)
                                {
                                    try
                                    {


                                        string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                                        sqlinsert += " values(" + dscomment.Tables[0].Rows[j]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                                        int intCommentResult = DatabaseHelper.executeNonQuery(sqlinsert);

                                    }
                                    catch
                                    {
                                    }
                                }

                            }
                        }
                        catch { }




                    }

                }
            }
            catch { }
        }
        if (i > 0)
        {
            Response.Redirect("tasks.aspx", false);
        }
        else
        {

            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('Please select task to delete.');</script>");
            /// lblMessage.Text = "Please enter date of birth.";
            return;

        }



    }
}
