<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Upload_News.aspx.cs" Inherits="Admin_Upload_News" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        Upload news
    </title>
    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" EnablePageMethods="true" runat="server">
        </asp:ScriptManager>
        <table width="100%" border="0">
            <tr>
                <td align="center">
                    <table border="0">
                        <tr>
                            <td align="left" valign="top" colspan="2">
                                <%--====================================================================--%>
                                <span style="font-size:medium; font-weight:bold;"  >--------- First Part( Marquee )---------- </span>
                                <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                                <span style="font-size:medium; font-weight:bold;" >--------- Second Part(News Edit,Update and Hide section)---------- </span>
                                <%--=====================================================================--%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                News Name</td>
                            <td>
                                <asp:TextBox ID="txtnewsname" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqnewsname" runat="server" ControlToValidate="txtnewsname"
                                    Display="None" ErrorMessage="Please enter newsname"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                News Heading</td>
                            <td>
                                <asp:TextBox ID="txtnewheading" runat="server" Width="500px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqnewsheading" runat="server" ControlToValidate="txtnewheading"
                                    Display="None" ErrorMessage="Please enter news heading"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                News
                            </td>
                            <td>
                                <%--<FTB:FreeTextBox ID="ftbComment" runat="server" Height="200px" Width="500px">
                                </FTB:FreeTextBox>
                                <asp:RequiredFieldValidator ID="reqftbComment" runat="server" ControlToValidate="ftbComment"
                                    Display="None" ErrorMessage="Please enter news"></asp:RequiredFieldValidator>--%>
                                <telerik:RadEditor ID="ftbComment" runat="server" Height="300px" Width="100%">
                                    <Snippets>
                                        <telerik:EditorSnippet Name="Product Template" Value="<table><tr><td><div style='width:300px;border:2px outset #D8D2BD;padding:34px;font-family:Arial, Verdana; font-size:11px;background- color:#F1EFE6;'>Dear ____________________,<br>Thank you for inquiring about ____________. Your request will be processed in 48 hours and shipped at the address you have provided.<b>Please, contact us if you have any problems.</b></div></td></tr></table>">            
                                        </telerik:EditorSnippet>
                                        <telerik:EditorSnippet Name="Signature" Value="<strong>John Smith</strong><br>Sales Manager<br>">
                                        </telerik:EditorSnippet>
                                    </Snippets>
                                    <Links>
                                        <telerik:EditorLink Name="Telerik" Href="http://www.telerik.com">
                                            <telerik:EditorLink Name="Telerik Products" Href="http://www.telerik.com/products"
                                                Target="_blank" />
                                            <telerik:EditorLink Name="Telerik Support Center" Href="http://www.telerik.com/support/home.aspx"
                                                Target="_blank" />
                                            <telerik:EditorLink Name="Telerik Community" Href="http://www.telerik.com/community/home.aspx"
                                                Target="_blank" ToolTip="Telerik Community" />
                                        </telerik:EditorLink>
                                    </Links>
                                    <Tools>
                                    </Tools>
                                    <ImageManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                    <MediaManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                    <FlashManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                    <TemplateManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                    <DocumentManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                    <Modules>
                                        <telerik:EditorModule Name="RadEditorStatistics" Visible="False" Enabled="true" />
                                        <telerik:EditorModule Name="RadEditorDomInspector" Visible="False" Enabled="true" />
                                        <telerik:EditorModule Name="RadEditorNodeInspector" Visible="False" Enabled="true" />
                                        <telerik:EditorModule Name="RadEditorHtmlInspector" Visible="False" Enabled="true" />
                                    </Modules>
                                    <Content>
                                    </Content>
                                </telerik:RadEditor>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <asp:Button ID="btnsubmit" runat="server" Text="Create" OnClick="btnsubmit_Click" />
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                                    ShowSummary="False" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <div id="divid" runat="server">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="left">
                                <table>
                                    <tr>
                                        <td colspan="2" align="center">
                                            <asp:GridView ID="gvnews" runat="server" AutoGenerateColumns="false">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="News ID">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblid" runat="server" Text='<%# bind("Id")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="News Name">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblnewsname" runat="server" Text='<%# bind("NewsName")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="News Heading">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblheading" runat="server" Text='<%# bind("Newsheding")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Show">
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkshow" runat="server" Checked='<%# Bind("IsShow")%>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:HyperLinkField HeaderText="Edit" Text="edit" DataNavigateUrlFields="Id" DataNavigateUrlFormatString="Upload_News.aspx?id={0}"
                                                        NavigateUrl="Upload_News.aspx">
                                                        <HeaderStyle Width="25px" />
                                                    </asp:HyperLinkField>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center">
                                            <asp:Button ID="Btndelete" runat="server" CausesValidation="false" Text="Show News"
                                                OnClick="Btndelete_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
