using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Admin_Upload_News : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Session["admin"] == null || Session["admin"].ToString() == "")
        //{
        //    Response.Redirect("login.aspx");
        //    return;
        //}
        //lblmessage.Text = "";
        //if (!Page.IsPostBack)
        //{
            
        //    BindCompany();
        //    BindGrid();s

        //}
        if(!IsPostBack)
        {
        Bindgrid();
        //if (Session["admin"] != null || Session["admin"].ToString() != "")
        //{
            if (Request.QueryString["id"] != null && Request.QueryString["id"] != "")
            {
                btnsubmit.Text = "Update";
                int newsid = Convert.ToInt32(Request.QueryString["id"]);
                string strqry;
                DataSet dsnews = new DataSet();
                strqry = "Select * from News where Id='" + newsid + "'";
                dsnews = DatabaseHelper.getDataset(strqry);
                if (dsnews != null && dsnews.Tables.Count > 0 && dsnews.Tables[0].Rows.Count > 0)
                {
                    txtnewsname.Text = dsnews.Tables[0].Rows[0]["NewsName"].ToString();
                    txtnewheading.Text = dsnews.Tables[0].Rows[0]["Newsheding"].ToString();
                    ftbComment.Content  = dsnews.Tables[0].Rows[0]["News"].ToString();
                }
            }
       // }
        }
        ShownewsinMorque();
        
    }

    public void Bindgrid()
    {
        string strqry;
        DataSet dsnews = new DataSet();
        strqry = "Select * from News";
        dsnews = DatabaseHelper.getDataset(strqry);
        if (dsnews != null && dsnews.Tables.Count > 0 && dsnews.Tables[0].Rows.Count > 0)
        {
            gvnews.DataSource = dsnews;
            gvnews.DataBind();
            Btndelete.Visible = true; 
        }
        else
        {
            gvnews.DataSource = null;
            gvnews.DataBind();
            Btndelete.Visible = false;
        }

    }
    public void ShownewsinMorque()
    {
        //===========================================================
        //===========================================================
        string sqlnews, str = "";
        sqlnews = "Select * from News where IsShow='True'";

        DataSet showdsnews = DatabaseHelper.getDataset(sqlnews);
        for (int inews = 0; inews < showdsnews.Tables[0].Rows.Count; inews++)
        {
            str += "<a href='news.aspx?id=" + showdsnews.Tables[0].Rows[inews]["Id"].ToString() + "' target='_blank' style='font-family: fantasy; font-size: medium; font-weight:bold; font-style: normal; '>" + showdsnews.Tables[0].Rows[inews]["Newsheding"].ToString() + "</a>";
            str += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        }
        string strforMarquee = "<MARQUEE onmouseover='this.stop();' behavior='scroll' loop='True' scrollamount='8' scrolldelay='100'  Onmouseout='this.start();'>" + str + "</MARQUEE>";
        Literal1.Text = strforMarquee;
        //===========================================================
        //===========================================================
    }

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        object obj;
        string strqry;
        if (btnsubmit.Text == "Create")
        {
            strqry = "Insert into News(NewsName,News,Newsheding,IsShow)values ('" + txtnewsname.Text.ToString().Replace("'", "''").Trim() + "','" + ftbComment.Content.Replace("'", "''").Trim() + "','" + txtnewheading.Text.ToString().Replace("'", "''").Trim() + "','True')";
            obj = DatabaseHelper.insertNews(strqry);
            if (obj.ToString() == "1")
            {
                divid.InnerText = "News created";
            }
            else
            {
                divid.InnerText = "News not created";
            }
            btnsubmit.Text = "Create";
        }
        else
        {

            strqry = "Update News  set NewsName='" + txtnewsname.Text.ToString().Replace("'", "''").Trim() + "',News='" + ftbComment.Content.Replace("'", "''").Trim() + "',Newsheding='" + txtnewheading.Text.ToString().Replace("'", "''").Trim() + "' where Id='" + Request.QueryString["id"] + "'";
            obj = DatabaseHelper.insertNews(strqry);
            if (obj.ToString() == "1")
            {
                divid.InnerText = "News updated";
            }
            else
            {
                divid.InnerText = "News not updated";
            }
            btnsubmit.Text = "Create";
        }
        Bindgrid();
        ShownewsinMorque();
        clear();
    }
    protected void Btndelete_Click(object sender, EventArgs e)
    {
        bool isShow = true;
        for (int i = 0; i < gvnews.Rows.Count; i++)
        {
            string strqry;
            object obj;
            Label strid = (Label)gvnews.Rows[i].Cells[0].FindControl("lblid");
            CheckBox chkvalue = (CheckBox)gvnews.Rows[i].Cells[0].FindControl("chkshow");
            strqry = "Update News set IsShow='" + chkvalue.Checked + "' where id='" + strid.Text + "'";
            obj = DatabaseHelper.insertNews(strqry);
            if (obj.ToString() != "1")
            {
                isShow = false;
                break;
            }
        }
        if (isShow == false)
        {
            divid.InnerText = "Not updated";
        }
        else
        {
            divid.InnerText = "Successfully updated";
        }
        Bindgrid();
        ShownewsinMorque();
    }
    public void clear()
    {
        txtnewsname.Text="";
        ftbComment.Content = "";
        txtnewheading.Text="";
    }
}