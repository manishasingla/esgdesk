﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewEODC.aspx.cs" Inherits="Admin_SODC" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Src="Notifications.ascx" TagName="Notifications" TagPrefix="uc4" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   
    <title>
       View EODC</title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />


  </head>
<body>
    <form id="form1" runat="server">
    <div>
        <uc2:Admin_Header ID="Admin_Header1" runat="server" />
        <br />
        <div>
            <uc4:Notifications ID="Notifications1" runat="server" />
        </div>
        
        <table width="100%">
            <tr>
                <td align="center" colspan="2">
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Select date: 
                    <asp:TextBox ID="GMDStartDate" runat="server"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="GMDStartDate"
                        Format="dd/MM/yyyy" PopupButtonID="GMDStartDate">
                    </ajaxToolkit:CalendarExtender>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please select a start date"
                        ControlToValidate="GMDStartDate"></asp:RequiredFieldValidator>
                    <br />
                    <br />
                    <br />
                    Select employee:
                    <asp:DropDownList ID="drpAssignedTo" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drpAssignedTo_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:Button ID="btnshow" runat="server" Text="Show Eodc" OnClick="btnshow_Click" />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
            
        </table>
        <table  width="90%" align="left" border="0">
        <tr>
            <td>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <div id="ShowEodc" runat="server" align="left">
                </div>
            </td>
        </tr>
    </table>
    </div>
     <uc1:footer ID="Footer1" runat="server" />
    </form>
</body>
</html>
