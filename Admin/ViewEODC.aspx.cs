﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;  

public partial class Admin_SODC : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = "ViewEODC.aspx";
            Response.Redirect("login.aspx");
            return;
        }
        if (!Page.IsPostBack)
        {
            binduser();
            loaddropdown();
        }

    }

    private void binduser()
    {
       
        
        string strmaxEodc = "";
        DataSet dseodc = null;
//        strmaxEodc=@"SELECT tt.* FROM tblEmp tt
//        INNER JOIN
//        (
//            SELECT username, MAX(EodcDate) AS MaxDateTime
//            FROM tblEmp
//            where username='" + Session["admin"] + @"'
//            GROUP BY username
//         )  groupedtt ON tt.username = groupedtt.username AND tt.EodcDate = '"+ DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy")+"'";



         strmaxEodc= @"SELECT tt.* FROM tblEmp tt
        INNER JOIN
        (
            SELECT username, Convert(varchar(10),max(convert(Datetime,EodcDate,103)),103) AS MaxDateTime
            FROM tblEmp
            where username='" + Session["admin"] + @"'
            GROUP BY username
         )  groupedtt ON tt.username = groupedtt.username AND tt.EodcDate = groupedtt.MaxDateTime";

         dseodc = DatabaseHelper.getDataset(strmaxEodc);
        if (dseodc != null && dseodc.Tables.Count > 0 && dseodc.Tables[0].Rows.Count > 0)
        {
            //ftbComment.Content = dseodc.Tables[0].Rows[0]["EodcDesc"].ToString();
            ShowEodc.InnerHtml = dseodc.Tables[0].Rows[0]["EodcDesc"].ToString();
            //lbldate.Text = dseodc.Tables[0].Rows[0]["EodcDate"].ToString();
        }
        else
        {
            //ftbComment.Content = "EODC not found";
            //lbldate.Text = "";
        }
    }
                
     private void loaddropdown()
    {
        
            string sql1 = "select username from users where active = 'Y' order by username";
            DataSet ds_dropdowns = DatabaseHelper.getDataset(sql1);
            drpAssignedTo.DataSource = ds_dropdowns.Tables[0];
            drpAssignedTo.DataTextField = "username";
            drpAssignedTo.DataValueField = "username";
            drpAssignedTo.DataBind();
            drpAssignedTo.SelectedValue = Session["admin"].ToString();  
            
        
    }
     protected void drpAssignedTo_SelectedIndexChanged(object sender, EventArgs e)
     {

         string strmaxEodc = "";
         DataSet dseodc = null;
//         strmaxEodc = @"SELECT tt.* FROM tblEmp tt
//        INNER JOIN
//        (
//            SELECT username, MAX(EodcDate) AS MaxDateTime
//            FROM tblEmp
//            where username='" + drpAssignedTo.SelectedItem.Text + @"'
//            GROUP BY username
//         )  groupedtt ON tt.username = groupedtt.username AND tt.EodcDate = '" + DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy") + "'";


         strmaxEodc = @"SELECT tt.* FROM tblEmp tt
                 INNER JOIN
                 (
                     SELECT username, Convert(varchar(10),max(convert(Datetime,EodcDate,103)),103) AS MaxDateTime
                     FROM tblEmp
                     where username='" + drpAssignedTo.SelectedItem.Text + @"'
                     GROUP BY username
                  )  groupedtt ON tt.username = groupedtt.username AND tt.EodcDate = groupedtt.MaxDateTime";


         dseodc = DatabaseHelper.getDataset(strmaxEodc);
         if (dseodc != null && dseodc.Tables.Count > 0 && dseodc.Tables[0].Rows.Count > 0)
         {
             //ftbComment.Content = dseodc.Tables[0].Rows[0]["EodcDesc"].ToString();
             ShowEodc.InnerHtml = dseodc.Tables[0].Rows[0]["EodcDesc"].ToString();
             //lbldate.Text = dseodc.Tables[0].Rows[0]["EodcDate"].ToString();
         }
         else
         {
             //ftbComment.Content = "EODC not found";
             //lbldate.Text = "";

         }
     }
     protected void btnshow_Click(object sender, EventArgs e)
     {
         string strmaxEodc = "";
         DataSet dseodc = null;

         if (drpAssignedTo.SelectedItem.Text.ToString().ToUpper() == Session["admin"].ToString().ToUpper())
         {
             strmaxEodc = @"SELECT tt.* FROM tblEmp tt
        INNER JOIN
        (
            SELECT username, MAX(EodcDate) AS MaxDateTime
            FROM tblEmp
            where username='" + Session["admin"].ToString().ToUpper() + @"'
            GROUP BY username
         )  groupedtt ON tt.username = groupedtt.username AND tt.EodcDate = '" + GMDStartDate.Text + "'";
         }
         else
         {
             strmaxEodc = @"SELECT tt.* FROM tblEmp tt
        INNER JOIN
        (
            SELECT username, MAX(EodcDate) AS MaxDateTime
            FROM tblEmp
            where username='" + drpAssignedTo.SelectedItem.Text + @"'
            GROUP BY username
         )  groupedtt ON tt.username = groupedtt.username AND tt.EodcDate = '" + GMDStartDate.Text + "'";
         
         
         }
        dseodc = DatabaseHelper.getDataset(strmaxEodc);
        if (dseodc != null && dseodc.Tables.Count > 0 && dseodc.Tables[0].Rows.Count > 0)
        {
            //ftbComment.Content = dseodc.Tables[0].Rows[0]["EodcDesc"].ToString();
            ShowEodc.InnerHtml = dseodc.Tables[0].Rows[0]["EodcDesc"].ToString();
            //lbldate.Text = dseodc.Tables[0].Rows[0]["EodcDate"].ToString();
        }
        else 
        {
            //ftbComment.Content = "EODC not found";
            //lbldate.Text = "";
        
        }
     }

     protected void showpdf_Click(object sender, EventArgs e)
     {
         //ftbComment.ExportToPdf();  
     }
}
