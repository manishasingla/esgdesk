<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WordSearchResult.aspx.cs"
    Inherits="Admin_WordSearchResult" %>

<%@ Register Src="Notifications.ascx" TagName="Notifications" TagPrefix="uc4" %>

<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        <%= ConfigurationManager.AppSettings["CompanyName"].ToString()%> - word search result</title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script language="javascript" type="text/javascript">
        function window.confirm(str)
        {
            execScript('n = msgbox("'+str+'","4132")', "vbscript");
            return(n == 6);
        }
    </script>

    <script type="text/javascript">
function get_(div_)
{

div_=div_.id+"1";

document.getElementById(div_).style.display="block";
}
function get_1(div_)
{

div_=div_.id+"1";

document.getElementById(div_).style.display="none";
}
    </script>
</telerik:RadCodeBlock>
    <%--<script src="../js/popupscript.js" type="text/javascript" language="javascript"/>--%>
    <style type="text/css">
     table#DataGrid1_ctl00 tr th a
     {
      display:block;
      text-decoration:none;
      margin:0;
      padding:0;
      width:100%;
      height:100%;
     }
    </style>
    <style type="text/css">
    div.RadToolTip_Default table.rtWrapper td.rtWrapperContent
    {
       background-color: #FEFFB3 !important;
    }
</style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
    
        <uc2:Admin_Header ID="Admin_Header1" runat="server" />
            <asp:UpdateProgress ID="updateprogress1" runat="server">
                <ProgressTemplate>
                    <div id="processmessage" class="processMessage">
                        <img alt="loading" src="images/ajax-loader.gif" style="width: 80px; height: 80px" />
                        <span style="color: black; font-weight: bold; font-size: 18px; vertical-align: super;">
                            Please wait... </span>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div id="Content">
                    <div id="Content_98">
                        <div>
                            <div style="padding: 0px 0 0px 5px; margin: 2px  0 -2px 0;">
                                <table>
                                    <tr>
                                        <td style="background: url(../images/yellowbar_filter.png) no-repeat left top; width: 101px;
                                            height: 27px; text-align: center;">
                                            Filter
                                        </td>
                                        <td>
                                            <span id="TopFilterLable" runat="server" class="topfilter"></span>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" onclick="return TABLE1_onclick()"
                                id="TABLE1">
                                <tr>
                                    <td align="right" valign="bottom">
                                        <img src="../images/filter_corner_l.png" width="11" height="59" />
                                    </td>
                                    <td bgcolor="#282C5F" width="100%">
                                        <table width="100%" border="0" cellpadding="5" cellspacing="0" onclick="return TABLE1_onclick()"
                                            id="TABLE2">
                                            <tr>
                                                <td align="left" valign="bottom" style="width: 50px; color: #e1ac10;">
                                                    <b>Filter:</b>
                                                </td>
                                                <td align="left" valign="bottom" class="whitetext2">
                                                    <br />
                                                    <asp:DropDownList ID="drpFilter" runat="server" AutoPostBack="True" Width="150px"
                                                        OnSelectedIndexChanged="drpFilter_SelectedIndexChanged" CssClass="filerDrpodown">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" valign="bottom" class="whitetext2">
                                                    Company<br />
                                                    <asp:DropDownList ID="drpProjects" runat="server" CssClass="filerDrpodown" AutoPostBack="True"
                                                        OnSelectedIndexChanged="drpProjects_SelectedIndexChanged" Width="150px">
                                                    </asp:DropDownList>
                                                </td>
                                                <td colspan="3" align="left" valign="bottom" class="whitetext2">
                                                    Priority<br />
                                                    <asp:DropDownList ID="drpPriorities" runat="server" CssClass="filerDrpodown" AutoPostBack="True"
                                                        OnSelectedIndexChanged="drpPriorities_SelectedIndexChanged" Width="100px">
                                                    </asp:DropDownList>
                                                </td>
                                                <td colspan="2" align="left" valign="bottom" class="whitetext2">
                                                    Status<br />
                                                    <asp:DropDownList ID="drpStatuses" runat="server" CssClass="filerDrpodown" AutoPostBack="True"
                                                        OnSelectedIndexChanged="drpStatuses_SelectedIndexChanged" Width="100px">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" valign="bottom" class="whitetext2">
                                                    <asp:Button ID="btnClearFilters" runat="server" OnClick="btnClearFilters_Click" Text="Clear filters"
                                                        CssClass="clearfilter" />
                                                </td>
                                                <td align="left" valign="bottom" class="whitetext2" style="width: 132px">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="chkViewDeleted" runat="server" Text="View only deleted" AutoPostBack="True"
                                                                    OnCheckedChanged="chkViewDeleted_CheckedChanged" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="chkHideClosed" runat="server" Text="View closed" AutoPostBack="True"
                                                                    OnCheckedChanged="chkHideClosed_CheckedChanged" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td align="left" valign="bottom" class="whitetext2" style="width: 46px">
                                                    <asp:CheckBox ID="ChkTask" runat="server" Text="Task" AutoPostBack="True" OnCheckedChanged="ChkTask_CheckedChanged" />
                                                </td>
                                                <td align="left" valign="bottom" class="whitetext2" style="width: 44px;">
                                                    <asp:CheckBox ID="ChkCrs" runat="server" Text="CRs" AutoPostBack="True" OnCheckedChanged="ChkCrs_CheckedChanged" />
                                                </td>
                                                <td align="left" valign="bottom" class="whitetext2">
                                                    <asp:CheckBox ID="ChkIncludecomment" runat="server" Text="Include comments" AutoPostBack="True"
                                                        Width="131px" OnCheckedChanged="ChkIncludecomment_CheckedChanged" />
                                                </td>
                                                <td align="left" valign="bottom" style="width: 156px; color: #e1ac10;">
                                                    <b>Records: </b>
                                                    <asp:Label ID="LblRecordsno" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td valign="bottom">
                                        <img src="../images/filter_corner_r.png" width="11" height="59" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div style="float: left">
                           <uc4:Notifications ID="Notifications" runat="server" />
                        </div>
                         <div class="titleText" style="margin: 10px 0 10px 0; display: none;">
                            Company related&nbsp;|&nbsp;</div>
                        <div style="float: left; width: 100%; background: #ebebeb; padding: 5px 0px;">
                            &nbsp;&nbsp;<span class="text_default"><a href="edit_task.aspx">Add new task</a></span>
                        </div>
                        <div style="clear: both">
                        </div>
                        <div id="divMessage" runat="server" style="color: Red; font-weight: bold;">
                        </div>
                        <div>
                        
                          <telerik:RadGrid ID="DataGrid1" runat="server" Width="100%" AutoGenerateColumns="False"
                                AllowPaging="True" AllowSorting="True" CellPadding="3" PageSize="100" OnSortCommand="DataGrid1_SortCommand"
                                OnItemDataBound="DataGrid1_ItemDataBound" OnPageIndexChanged="DataGrid1_PageIndexChanged" >
                            <PagerStyle Mode="NextPrevAndNumeric" /> 
                            <MasterTableView PagerStyle-AlwaysVisible="true">
                            <Columns>
                            
                            <%--0--%>
                            <telerik:GridTemplateColumn HeaderText="ID" Visible="False" SortExpression="ID">
                            <ItemTemplate>
                            <asp:Label id="lblidV" runat="server"></asp:Label>
                            </ItemTemplate> 
                            </telerik:GridTemplateColumn>
                            
                            <%--1--%> 
                            <telerik:GridTemplateColumn HeaderText="ID" SortExpression="ID">
                            <ItemTemplate>
                            <asp:Label Font-Size="13px" ID="lblID" runat="server" Text='<%# bind("ITEMID") %>'></asp:Label>
                            </ItemTemplate> 
                            </telerik:GridTemplateColumn> 
                            
                            <%--2--%> 
                            <telerik:GridTemplateColumn HeaderText="Short Description" SortExpression="ShortDescr">
                            <ItemTemplate>
                            <asp:Label Font-Size="13px" ID="lblshrtDesc" runat="server" Width="350px" Text='<%# bind("ShortDescr") %>'></asp:Label>
                                 <%--<div id="lblshrtDesc1" runat="server" style="display: none; position: absolute; background-color: #FEFFB3;
                                                width: 730px; border: solid 1px #333333; margin-top: 5px; padding-left: 3px">
                                    <asp:Label ID="LblLastcomment" runat="server" Text='<%# bind("ShowAllComment") %>'></asp:Label>
                                  </div>--%>
                                  <telerik:RadToolTip ID="RadToolTip1" BackColor="#FEFFB3" runat="server" OffsetX ="-100" AutoCloseDelay="60000" HideEvent="LeaveTargetAndToolTip" OffsetY="10" TargetControlID="lblshrtDesc" Width="500px" RelativeTo="Element" Position="BottomLeft" ShowEvent="OnMouseOver"><%# DataBinder.Eval(Container, "DataItem.ShowAllComment")%></telerik:RadToolTip>
                            </ItemTemplate> 
                            </telerik:GridTemplateColumn> 
                             
                             <%--3--%>
                            <telerik:GridTemplateColumn HeaderText="Company name" SortExpression="companyname">
                            <ItemTemplate>
                            <asp:Label id="lblcompanyname" runat="server"  Text='<%#bind("companyname")%>' ></asp:Label>
                            </ItemTemplate> 
                            </telerik:GridTemplateColumn> 
                            
                            <%--4--%>
                            <telerik:GridTemplateColumn HeaderText="Project name" SortExpression="ProjectName">
                             <ItemTemplate>
                                <asp:Label id="lblprojectname" runat="server" Text='<%#bind("ProjectName")%>' ></asp:Label>
                             </ItemTemplate> 
                            </telerik:GridTemplateColumn> 
                            
                            <%--5--%>
                            <telerik:GridTemplateColumn HeaderText="Priority" SortExpression="Priority">
                            <ItemTemplate>
                            <asp:Label id="lblpriority" runat="server" Text='<%#bind("Priority")%>'></asp:Label>
                            </ItemTemplate> 
                            </telerik:GridTemplateColumn> 
                            
                            <%--6--%>
                            <telerik:GridTemplateColumn HeaderText="Status" SortExpression="status">
                            <ItemTemplate>
                            <asp:Label id="lblStatus" runat="server" Text='<%#bind("Status")%>'></asp:Label>
                            </ItemTemplate> 
                            </telerik:GridTemplateColumn> 
                            
                            <%--7--%>
                            <telerik:GridTemplateColumn HeaderText="Last Updated By" SortExpression="LastUpdatedBy">
                            <ItemTemplate>
                            <asp:Label id="lbllastupdatedby" runat="server" Text='<%#bind("LastUpdatedBy")%>'></asp:Label>
                            </ItemTemplate> 
                            </telerik:GridTemplateColumn> 
                            
                            <%--8--%>
                            <telerik:GridTemplateColumn HeaderText="Last Updated" SortExpression="LastUpdatedOn">
                            <ItemTemplate>
                            <asp:Label ID="lblLastUpdatedOn" Font-Size="13px" runat="server" Text='<%# bind("LastUpdatedOn") %>'></asp:Label>
                            </ItemTemplate> 
                            </telerik:GridTemplateColumn> 
                            
                            <%--9--%>
                            <telerik:GridTemplateColumn HeaderText="Edit" SortExpression="ID">
                               <ItemTemplate>
                                 <asp:Label ID="lbleditRelats" Font-Size="13px" runat="server" Text='<%# bind("editrelates") %>'></asp:Label>
                               </ItemTemplate> 
                            </telerik:GridTemplateColumn> 
                            
                            </Columns> 
                            </MasterTableView> 
                            </telerik:RadGrid> 
                            <%--*************************************************************************************************************************************--%>
                            
                            
                            
                        </div>
                        <div style="text-align: right" id="divTotalHrs" runat="server">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td align="right">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 85px;" align="right" valign="middle">
                                                    <b>Total Hrs:</b>
                                                </td>
                                                <td>
                                                    <table cellpadding="3" cellspacing="0" border="1">
                                                        <tr>
                                                            <td style="width: 85px" align="left" valign="top">
                                                                <asp:Label ID="lblETC" runat="server" Font-Bold="True" Font-Size="12px"></asp:Label>
                                                            </td>
                                                            <td style="width: 85px" align="left" valign="top">
                                                                <asp:Label ID="lblTotalHrsTaken" runat="server" Font-Bold="True" Font-Size="12px"></asp:Label>
                                                            </td>
                                                            <td style="width: 90px" align="left" valign="top">
                                                                <asp:Label ID="lblExpectedHrsLeft" runat="server" Font-Bold="True" Font-Size="12px"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="padding-right: 3px; padding-left: 3px; padding-bottom: 3px; width: 25px;
                                                    padding-top: 3px">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <uc1:footer ID="Footer1" runat="server" />
    <asp:Label ID="lblOrderBy" runat="server" Text="" Visible="false"></asp:Label>
    <asp:Label ID="lblword" runat="server" Text="" Visible="false"></asp:Label>
    <asp:Label ID="LblCROredrBy" runat="server" Text="" Visible="false"></asp:Label>
    <div id="popup" class="popup" style="background-color: #e9e9e9; width: 650px; border: solid 1px #000000;
        border-bottom: solid 2px #000000; border-right: solid 2px #000000; padding: 10px 10px 10px 10px;
        position: absolute; display: none">
    </div>
    </form>
</body>
</html>
