using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
using Telerik.Web;

public partial class Admin_WordSearchResult : System.Web.UI.Page
{
    string sql = "";
    string sql2 = "";
    string sqlinclude = "";
    string sql2include = "";

    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Response.Redirect("login.aspx");
            return;
        }

        if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
        {
            ChkCrs.Visible = false;
        }

        TopFilterLable.InnerHtml = "";
        divMessage.InnerHtml = "";
        if (!Page.IsPostBack)
        {
            if (!DatabaseHelper.can_Show_Deleted_Tasks(Session["admin"].ToString()))
            {
                chkViewDeleted.Visible = false;
            }
            else
            {
                chkViewDeleted.Visible = true;
            }

            setsession();
            bindDefault();
           
        }
    }
    private void setsession()
    {
        try
        {

            if (Session["TaskByWord"].ToString() == "true")
            {
                ChkTask.Checked = true;
            }
            else
            {
                ChkTask.Checked = false;
            }
            if (Session["CRsByWord"].ToString() == "true")
            {
                ChkCrs.Checked = true;
            }
            else
            {
                ChkCrs.Checked = false;
            }
            if (Session["Includecomment"].ToString() == "true")
            {
                ChkIncludecomment.Checked = true;
            }
            else
            {
                ChkIncludecomment.Checked = false;
            }
        }
        catch
        {
            ChkTask.Checked = true;
        }
    }
    private void BindData()
    {
        //******************************************************
        string filter = "where Isprivate=0 and ";
        string filterIncludeCmment = "where Isprivate=0 and ";
        sql = "";
        sqlinclude = "";
        sql2 = "";
        sql2include = "";
        //******************************************************
        string filter2 = "and";
        string topFilterLable = "";
        string strFilter = " where  ";
        string strFilterIncludeComment = " where  ";
        string strtaskid = "";
        string strrequestid = "";
        if (ChkIncludecomment.Checked)
        {
            //******************************************************************************************************************************************************************************
            if (ChkTask.Checked)
            {
                strtaskid = DatabaseHelper.SearchStringtaskId("select task_id from task_comments where comment like N'%" + lblword.Text.Replace("'", "''") + "%' and deleted <> 1");

                if (strtaskid.ToString() != "")
                {
                    filterIncludeCmment += " task_id in(" + strtaskid + ") and ";
                }
                else
                {
                    filterIncludeCmment += " task_id in('" + strtaskid + "') and ";
                }
            }
            if (ChkCrs.Checked)
            {
                strFilterIncludeComment += "  RequestId in (select RequestId from ClientRequest_Details where Comment  like N'%" + lblword.Text.Replace("'", "''") + "%' and deleted <> 1) and ";

            }
            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by word including comments: " + lblword.Text;
            }
            else
            {
                topFilterLable += " / Word including comments: " + lblword.Text;
            }

        }

        if (chkViewDeleted.Checked)
        {
            filter += " deleted = 1 ";
            filterIncludeCmment += "deleted = 1 ";
            strFilter += " ClientRequest.deleted = 1 ";
            strFilterIncludeComment += "  ClientRequest.deleted = 1 ";

        }
        else
        {
            filter += " deleted <> 1 ";
            filterIncludeCmment += " deleted <> 1 ";

            strFilter += " ClientRequest.deleted <> 1 ";
            strFilterIncludeComment += " ClientRequest.deleted <> 1 ";
        }

        if (drpStatuses.SelectedIndex != 0)
        {
            filter += " and status = '" + drpStatuses.SelectedItem.Value + "' ";
            filterIncludeCmment += " and Status ='" + drpStatuses.SelectedItem.Value + "' ";
            filter2 += " and status = '" + drpStatuses.SelectedValue + "' ";
            strFilter += " and ClientRequest.Status = '" + drpStatuses.SelectedItem.Value + "' ";
            strFilterIncludeComment += " and ClientRequest.Status = '" + drpStatuses.SelectedItem.Value + "' ";

            if (topFilterLable == "")
            {
                topFilterLable += "Filtered by Status: " + drpStatuses.SelectedItem.Text;
            }
            else
            {
                topFilterLable += " / Status: " + drpStatuses.SelectedItem.Text;
            }
        }

        if (drpPriorities.SelectedIndex != 0)
        {
            filter += " and Priority = '" + drpPriorities.SelectedItem.Value + "' ";
            filterIncludeCmment += " and Priority ='" + drpPriorities.SelectedItem.Value + "' ";
            filter2 += " and priority = '" + drpPriorities.SelectedValue + "' ";

            strFilter += " and ClientRequest.Priority = " + drpPriorities.SelectedItem.Value + "' ";
            strFilterIncludeComment += " and ClientRequest.Priority = " + drpPriorities.SelectedItem.Value + "' ";

            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Priority: " + drpPriorities.SelectedItem.Text;
            }
            else
            {
                topFilterLable += " / Priority: " + drpPriorities.SelectedItem.Text;
            }
        }

        if (!DatabaseHelper.can_Show_Closed_Tasks(Session["admin"].ToString()))
        {
            chkHideClosed.Visible = false;
            filter += " and status <> 'closed'";
            filter2 += " and status <> 'closed'";
            filterIncludeCmment += " and status <> 'closed'";
            strFilter += " and ClientRequest.status <> 'closed' ";
            strFilterIncludeComment += " and ClientRequest.status <> 'closed' ";
        }
        else
        {
            if (chkHideClosed.Checked)
            {
                filter += " and status = 'closed' ";
                strFilter += " and ClientRequest.status = 'closed' ";
                strFilterIncludeComment += " and ClientRequest.status = 'closed' ";
                strFilterIncludeComment += " and ClientRequest.status = 'closed' ";
            }
            else
            {
                filter += " and status <> 'closed'";
                filter2 += " and status <> 'closed'";
                filterIncludeCmment += " and status <> 'closed'";
                strFilter += " and ClientRequest.status <> 'closed' ";
                strFilterIncludeComment += " and ClientRequest.status <> 'closed' ";

            }
        }

        string drpValue = drpFilter.SelectedValue;
        string username = "";

        if (drpValue.Contains("Assigned by me"))
        {
            filter += " and reported_user = '" + username + "' ";
            filter2 += " and reported_user = '" + username + "' ";
            filterIncludeCmment += " and reported_user = '" + username + "' ";
        }
        else if (drpValue.Contains("Assigned by "))
        {
            username = drpValue.Replace("Assigned by ", "");

            filter += " and reported_user = '" + username + "' ";
            filter2 += " and reported_user = '" + username + "' ";
            filterIncludeCmment += " and reported_user = '" + username + "' ";
        }
        else if (drpValue.Contains("All emps CWO"))
        {
            filter += " and status = 'CWO' ";
            filter2 += " and status = 'CWO' ";
            filterIncludeCmment += " and status = 'CWO' ";
        }
        else if (drpValue.Contains("Show my open tasks"))
        {}
        if (drpProjects.SelectedIndex != 0)
        {
            filter += " and Company = '" + drpProjects.SelectedValue + "' ";
            filterIncludeCmment += " and Company = '" + drpProjects.SelectedValue + "' ";
            filter2 += " and Company = '" + drpProjects.SelectedValue + "' ";
            strFilter += " and CompanyName = '" + drpProjects.SelectedValue + "' ";
            strFilterIncludeComment += " and CompanyName = '" + drpProjects.SelectedValue + "' ";
            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by company: " + drpProjects.SelectedValue;
            }
            else
            {
                topFilterLable += " / company: " + drpProjects.SelectedValue;
            }
        }
        else
        {
            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by word: " + lblword.Text;
            }
            else
            {
                topFilterLable += " / Word: " + lblword.Text;
            }
            filter += " and (Company like N'%" + lblword.Text.Replace("'", "''") + "%' or project like N'%" + lblword.Text.Replace("'", "''") + "%' or short_desc like N'%" + lblword.Text.Replace("'", "''") + "%' )";
            strFilter += " and (CompanyName like N'%" + lblword.Text.Replace("'", "''") + "%' or ProjectName like N'%" + lblword.Text.Replace("'", "''") + "%' or ShortDescr like N'%" + lblword.Text.Replace("'", "''") + "%')";
            }
           if (chkViewDeleted.Checked == true)
           {
            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by View only deleted";
            }
            else
            {
                topFilterLable += " / View only deleted";
            }
          }
        if (chkHideClosed.Checked)
        {
            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by View closed";
            }
            else
            {
                topFilterLable += " / View closed";
            }
        }

        TopFilterLable.InnerHtml = topFilterLable;

        if (ChkTask.Checked)
        {

            sql = "select task_id as ID,short_desc as ShortDescr,Company as companyname,project as ProjectName,Priority,last_updated_user as LastUpdatedBy,Status,last_updated_date as LastUpdatedOn from tasks " + filter;

            sqlinclude = "select task_id as ID,short_desc as ShortDescr,Company as companyname,project as ProjectName,Priority,last_updated_user as LastUpdatedBy,Status,last_updated_date as LastUpdatedOn from tasks " + filterIncludeCmment;
        }
        if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
        {

        }
        else
        {
            if (ChkCrs.Checked)
            {
                sql2 = "select RequestId as ID,ShortDescr,CompanyName as companyname,ProjectName,Priority as ItemPriority, LastUpdatedBy,Status, LastUpdatedOn from ClientRequest " + strFilter;
                sql2include = "select RequestId as ID,ShortDescr,CompanyName as companyname,ProjectName,Priority as ItemPriority, LastUpdatedBy,Status, LastUpdatedOn from ClientRequest " + strFilterIncludeComment;
                
            }
        }
        DataSet dsCRsinclude = null, dsV = null, dsCRs = null, ds = null;
        DataSet dsinclude = new DataSet();

        if (sqlinclude.ToString() != "")
        {
            dsinclude = DatabaseHelper.getDatasetDuplicate(sqlinclude); //include Task Comments
        }
        if (sql2include.ToString() != "")
        {
            dsCRsinclude = DatabaseHelper.getDatasetDuplicate(sql2include); //include CR Comments

        }
        if (sql.ToString() != "")
        {
            ds = DatabaseHelper.getDatasetDuplicate(sql);        //Task
        }
        if (sql2.ToString() != "")
        {
            dsCRs = DatabaseHelper.getDatasetDuplicate(sql2);    //CRs
        }

        if (ChkIncludecomment.Checked)
        {
            if (dsinclude != null && ds != null)
            {
                ds.Merge(dsinclude);
                ds = RemoveDuplicateRows(ds, "ID");
            }
            if (dsCRsinclude != null && dsCRs != null)
            {
                dsCRs.Merge(dsCRsinclude);
                dsCRs = RemoveDuplicateRows(dsCRs, "ID");
            }
        }

        if (ChkTask.Checked)
            ds = formatTaskDataSet(ds);

        if (ChkCrs.Checked)
            dsCRs = formatCrsDataSet(dsCRs);
        try
        {
            if (ChkTask.Checked)
            {
                if (dsCRs != null)
                {
                    ds.Merge(dsCRs);
                }
            }
            else
            {
                ds = dsCRs;
            }

            DataView dv = ds.Tables[0].DefaultView;
            dv.Sort = lblOrderBy.Text;//// "LastUpdatedOn Desc";
            ds.Tables.Clear();
            ds.Tables.Add(dv.ToTable());

        }
        catch (Exception ex)
        { }

        if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "Not related yet.";
            DataGrid1.DataSource = null;
            DataGrid1.DataBind();
            LblRecordsno.Text = "0";

            divTotalHrs.Visible = false;
        }
        else
        {
            if (ds.Tables[0].Rows.Count <= 100)
            {
                DataGrid1.PagerStyle.Visible = false;
                DataGrid1.CurrentPageIndex = 0;
            }
            else
            {
                DataGrid1.PagerStyle.Visible = true;
            }
            //**************************************************
            PagedDataSource objds = new PagedDataSource();
            objds.DataSource = ds.Tables[0].DefaultView;
            LblRecordsno.Text = objds.DataSourceCount.ToString();
            //****************************************************
            divMessage.InnerHtml = "";

            DataGrid1.DataSource = null;
            DataGrid1.DataBind();

            DataGrid1.DataSource = ds.Tables[0];
            DataGrid1.DataBind();

            getTotalHrsReport("select task_id from tasks " + filter);
            divTotalHrs.Visible = true;
        }
    }
    public DataSet RemoveDuplicateRows(DataSet dDataSet, string colName)
    {
        Hashtable hTable = new Hashtable();
        ArrayList duplicateList = new ArrayList();
        foreach (DataRow dtRow in dDataSet.Tables[0].Rows)
        {
            if (hTable.Contains(dtRow[colName]))
                duplicateList.Add(dtRow);
            else
                hTable.Add(dtRow[colName], string.Empty);
        }
        foreach (DataRow dtRow in duplicateList)
            dDataSet.Tables[0].Rows.Remove(dtRow);
        return dDataSet;
    }


    private DataSet formatCrsDataSet(DataSet ds)
    {
        try
        {
            ds.Tables[0].Columns.Add("editrelates");
            ds.Tables[0].Columns.Add("ITEMID");
            ds.Tables[0].Columns.Add("Priority");

            ds.Tables[0].Columns.Add("ShowAllComment");

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {
                    if (ds.Tables[0].Rows[i]["ID"] != null && ds.Tables[0].Rows[i]["ID"].ToString().Trim() != "")
                    {

                        string taskId = "<a href=\"edit_request.aspx?reqid=" + ds.Tables[0].Rows[i]["ID"].ToString() + "\">" + "CR" + "</a>";
                        ds.Tables[0].Rows[i]["ITEMID"] = taskId.ToString();
                        ds.Tables[0].Rows[i]["editrelates"] = "<a href=\"edit_request.aspx?reqid=" + ds.Tables[0].Rows[i]["ID"].ToString() + "\">" + "Edit" + "</a>";
                    }
                    else
                    {
                        ds.Tables[0].Rows[i]["ID"] = "";
                    }

                    if (ds.Tables[0].Rows[i]["ShortDescr"] != null && ds.Tables[0].Rows[i]["ShortDescr"].ToString().Trim() != "")
                    {
                        ds.Tables[0].Rows[i]["ShortDescr"] = "<a href=\"edit_request.aspx?reqid=" + ds.Tables[0].Rows[i]["ID"].ToString() + "\">" + ds.Tables[0].Rows[i]["ShortDescr"].ToString() + "</a>";
                    }
                    else
                    {
                        ds.Tables[0].Rows[i]["ShortDescr"] = "";
                    }

                    if (ds.Tables[0].Rows[i]["ItemPriority"] != null && ds.Tables[0].Rows[i]["ItemPriority"].ToString().Trim() != "")
                    {
                        try
                        {
                            string priority = ds.Tables[0].Rows[i]["ItemPriority"].ToString();
                            if (ds.Tables[0].Rows[i]["ItemPriority"].ToString() == "0")
                            {
                                priority = "6";
                            }

                            ds.Tables[0].Rows[i]["Priority"] = (string)DatabaseHelper.executeScalar("select priority_name from priorities where priority_id='" + priority + "'");
                        }
                        catch
                        {
                            ds.Tables[0].Rows[i]["Priority"] = "0 - IMMEDIATE";
                        }
                    }
                    else
                    {
                        string priority = "";
                        ds.Tables[0].Rows[i]["Priority"] = priority.ToString();
                    }

                    try
                    {
                        string LastComment = "";
                        sql = "Select top 1* from ClientRequest_Details where deleted <> 1 and RequestId=" + ds.Tables[0].Rows[i]["ID"].ToString() + " order by CommentId desc";
                        DataSet dslastcomment = DatabaseHelper.getDataset(sql);
                        if (dslastcomment != null && dslastcomment.Tables[0].Rows.Count > 0)
                        {

                            for (int j = 0; j < dslastcomment.Tables[0].Rows.Count; j++)
                            {

                                LastComment += "<span style='color:Green'>" + "Comment " + dslastcomment.Tables[0].Rows[j]["CommentId"].ToString() + " posted by " + dslastcomment.Tables[0].Rows[j]["PostedBy"].ToString() + " on " + DateTime.Parse(dslastcomment.Tables[0].Rows[j]["PostedOn"].ToString()).ToString("dd MMM h:mm tt") + "</span>";

                                LastComment += "<br/><span>" + dslastcomment.Tables[0].Rows[j]["Comment"].ToString() + "</span>";

                                LastComment += "<span style=\"border-bottom: solid 1px green; margin-bottom: 2px; height:1px\"></span><br/><hr />";
                            }
                        }
                        ds.Tables[0].Rows[i]["ShowAllComment"] = LastComment.ToString();
                    }
                    catch
                    {

                    }
                }
                catch { }
            }
        }
        catch
        {
            Server.ClearError();
        }
        return ds;
    }

    private DataSet formatTaskDataSet(DataSet ds)
    {
        try
        {
            string strTaskId = "";
            ds.Tables[0].Columns.Add("editrelates");
            ds.Tables[0].Columns.Add("ITEMID");
            ds.Tables[0].Columns.Add("ShowAllComment");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {
                    if (ds.Tables[0].Rows[i]["ID"] != null && ds.Tables[0].Rows[i]["ID"].ToString().Trim() != "")
                    {
                        string taskId = "<a href=\"edit_task.aspx?id=" + ds.Tables[0].Rows[i]["ID"].ToString() + "\">" + "Task" + "</a>";
                        ds.Tables[0].Rows[i]["ITEMID"] = taskId.ToString();
                        ds.Tables[0].Rows[i]["editrelates"] = "<a href=\"edit_task.aspx?id=" + ds.Tables[0].Rows[i]["ID"].ToString() + "\">" + "Edit" + "</a>";
                    }
                    else
                    {
                        ds.Tables[0].Rows[i]["ID"] = "";
                    }

                    if (ds.Tables[0].Rows[i]["ShortDescr"] != null && ds.Tables[0].Rows[i]["ShortDescr"].ToString().Trim() != "")
                    {
                        ds.Tables[0].Rows[i]["ShortDescr"] = "<a href=\"edit_task.aspx?id=" + ds.Tables[0].Rows[i]["ID"].ToString() + "\">" + ds.Tables[0].Rows[i]["ShortDescr"].ToString() + "</a>";

                    }
                    else
                    {
                        ds.Tables[0].Rows[i]["ShortDescr"] = "";

                    }
                    if (ds.Tables[0].Rows[i]["Priority"] != null && ds.Tables[0].Rows[i]["Priority"].ToString().Trim() != "")
                    {
                        ds.Tables[0].Rows[i]["Priority"] = ds.Tables[0].Rows[i]["Priority"].ToString();
                    }

                    if (strTaskId == "")
                    {
                        strTaskId = ds.Tables[0].Rows[i]["ID"].ToString().Trim();
                    }
                    else
                    {
                        strTaskId += "," + ds.Tables[0].Rows[i]["ID"].ToString().Trim();
                    }
                   
                }
                catch { }
            }
            try
            {
                string LastComment = "";
                string strlistofTaskid = "";
                strlistofTaskid = strTaskId.TrimEnd(new char[] { ',' });
                sql = "select a.* from task_comments a where a.task_id in(" + strlistofTaskid + ") and  a.tc_id IN (select top 1 b.tc_id from task_comments b where b.task_id = a.task_id order by b.post_date desc)";
                //testing by rohit
                DataSet dslastcomment = DatabaseHelper.getDataset(sql);
                DataView dv = new DataView(dslastcomment.Tables[0]);
                for (int icount = 0; icount < ds.Tables[0].Rows.Count; icount++)
                {
                    LastComment = "";
                    if (dslastcomment != null && dslastcomment.Tables[0].Rows.Count > 0)
                    {
                        dv.RowFilter = "task_id='" + ds.Tables[0].Rows[icount]["ID"].ToString() + "'";
                        if (dv[0]["qflag"].ToString() == "1")
                        {

                            LastComment += "<span style='color:Red'>" + "Question posted by " + dv[0]["username"].ToString() + " for  " + dv[0]["QuesTo"].ToString() + " on " + DateTime.Parse(dv[0]["post_date"].ToString()).ToString("dd MMM yyyy h:mm tt") + "</span>";

                            LastComment += "<br/><span>" + dv[0]["comment"].ToString() + "</span>";

                            LastComment += "<span style=\"border-bottom: solid 1px green; margin-bottom: 2px; height:1px\"></span><br/><hr />";

                        }
                        else
                        {

                            LastComment += "<span style='color:Green'>" + "Comment " + dv[0]["tc_id"].ToString() + " posted by " + dv[0]["username"].ToString() + " on " + DateTime.Parse(dv[0]["post_date"].ToString()).ToString("dd MMM yyyy h:mm tt") + "</span>";

                            LastComment += "<br/><span>" + dv[0]["comment"].ToString() + "</span>";

                            LastComment += "<span style=\"border-bottom: solid 1px green; margin-bottom: 2px; height:1px\"></span><br/><hr />";
                        }
                    }
                    ds.Tables[0].Rows[icount]["ShowAllComment"] = LastComment.ToString();
                }
                               
             }
            catch(Exception ex)
            { }
          }
        catch
        {
            Server.ClearError();
        }
        return ds;
    }

    void load_dropdowns()
    {

        // projects
        sql = @"select distinct Company_Name
		from Company
		where active = 'Y' order by Company_Name;";
        // categories
        sql += "\nselect category_name from categories order by sort_seq, category_name;";

        // priorities
        sql += "\nselect priority_name from priorities order by sort_seq, priority_name;";

        // statuses
        sql += "\nselect status_name from statuses order by sort_seq, status_name;";

        // users
        sql += "\nselect username from users where active = 'Y' order by username;";

        // do a batch of sql statements
        DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);

        drpProjects.DataSource = ds_dropdowns.Tables[0];
        drpProjects.DataTextField = "Company_Name";
        drpProjects.DataValueField = "Company_Name";
        drpProjects.DataBind();
        drpProjects.Items.Insert(0, new ListItem("[no filter]", "0"));

        drpPriorities.DataSource = ds_dropdowns.Tables[2];
        drpPriorities.DataTextField = "priority_name";
        drpPriorities.DataValueField = "priority_name";
        drpPriorities.DataBind();
        drpPriorities.Items.Insert(0, new ListItem("[no filter]", "0"));

        drpStatuses.DataSource = ds_dropdowns.Tables[3];
        drpStatuses.DataTextField = "status_name";
        drpStatuses.DataValueField = "status_name";
        drpStatuses.DataBind();
        drpStatuses.Items.Insert(0, new ListItem("[no filter]", "0"));

    }

    void load_filterDropDown()
    {
        drpFilter.Items.Clear();
        if (DatabaseHelper.isAdmin(Session["Admin"].ToString()))
        {
            sql = "select username from users where active='Y' and admin='Y' and username <> '" + Session["Admin"] + "' order by username";

            DataSet dsAdmin = DatabaseHelper.getDataset(sql);

            if (dsAdmin != null)
            {
                if (dsAdmin.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsAdmin.Tables[0].Rows.Count; i++)
                    {
                        drpFilter.Items.Add("Assigned by " + dsAdmin.Tables[0].Rows[i]["username"]);
                    }
                }
            }

            drpFilter.Items.Add("Assigned by " + Session["Admin"]);
            drpFilter.Items.Add("Assigned by me");
            drpFilter.Items.Add("All tasks by last updated");
            drpFilter.Items.Add("All emps CWO");

            drpFilter.Items.Add("Show my open tasks");

            drpFilter.SelectedValue = (string)DatabaseHelper.executeScalar("select default_selected_item from users where username='" + Session["admin"] + "'");
        }
        else
        {
            drpFilter.Items.Add("Show my open tasks");
        }
    }

    protected void drpFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["filter"] = null;
        load_dropdowns();
        setFileterSession();
        DataGrid1.CurrentPageIndex = 0;
        BindData();

    }

    protected void drpProjects_SelectedIndexChanged(object sender, EventArgs e)
    {
        setFileterSession();
        DataGrid1.CurrentPageIndex = 0;
        BindData();
    }

    protected void drpCategories_SelectedIndexChanged(object sender, EventArgs e)
    {
        setFileterSession();
        DataGrid1.CurrentPageIndex = 0;
        BindData();
    }

    protected void drpReportedBy_SelectedIndexChanged(object sender, EventArgs e)
    {
        setFileterSession();
        DataGrid1.CurrentPageIndex = 0;
        BindData();
    }

    protected void drpPriorities_SelectedIndexChanged(object sender, EventArgs e)
    {
        setFileterSession();
        DataGrid1.CurrentPageIndex = 0;
        BindData();
    }

    protected void drpAssignedTo_SelectedIndexChanged(object sender, EventArgs e)
    {
        setFileterSession();
        DataGrid1.CurrentPageIndex = 0;
        BindData();
    }

    protected void drpStatuses_SelectedIndexChanged(object sender, EventArgs e)
    {
        setFileterSession();
        DataGrid1.CurrentPageIndex = 0;
        BindData();
    }

    private void setFileterSession()
    {
        Session["HideClosed2"] = chkHideClosed.Checked;
        Session["IncludeDeleted2"] = chkViewDeleted.Checked;
        Session["Filter2"] = drpFilter.SelectedValue;
        Session["Project2"] = drpProjects.SelectedValue;
        Session["CompanyRelated"] = drpProjects.SelectedValue;
        Session["Priority2"] = drpPriorities.SelectedValue;
        Session["Status2"] = drpStatuses.SelectedValue;
        Session["OrderBy2"] = lblOrderBy.Text;
        if (ChkIncludecomment.Checked)
        {
            Session["Includecomment"] = "true";
        }
        else
        {
            Session["Includecomment"] = "false";
        }

    }

    private void getFilterSession()
    {
        try
        {
            if (Session["HideClosed2"] != null)
                chkHideClosed.Checked = (bool)Session["HideClosed2"];

            if (Session["IncludeDeleted2"] != null)
                chkViewDeleted.Checked = (bool)Session["IncludeDeleted2"];

            if (Session["Filter2"] != null)
                drpFilter.SelectedValue = Session["Filter2"].ToString();


            if (Session["CompanyRelated"] != null)
            {
                drpProjects.SelectedValue = Session["CompanyRelated"].ToString();
            }

            if (Session["Includecomment"] != null)
            {
                if (Session["Includecomment"].ToString() == "true")
                {
                    ChkIncludecomment.Checked = true;
                }
                else
                {
                    ChkIncludecomment.Checked = false;
                }

            }

            if (Session["Priority2"] != null)
                drpPriorities.SelectedValue = Session["Priority2"].ToString();

            if (Session["Status2"] != null)
                drpStatuses.SelectedValue = Session["Status2"].ToString();

            if (Session["OrderBy2"] != null)
                lblOrderBy.Text = Session["OrderBy2"].ToString();

            if (Session["SearchWord"] != null)
                lblword.Text = Session["SearchWord"].ToString();

            if (Session["TaskByWord"] != null)
            {
                if (Session["TaskByWord"].ToString() == "true")
                {
                    ChkTask.Checked = true;
                }
                else
                {
                    ChkTask.Checked = false;
                }

            }
            if (Session["CRsByWord"] != null)
            {
                if (Session["CRsByWord"].ToString() == "true")
                {
                    ChkCrs.Checked = true;
                }
                else
                {
                    ChkCrs.Checked = false;
                }
            }
        }
        catch
        {

        }
    }

    protected void DataGrid1_SortCommand(object source, Telerik.Web.UI.GridSortCommandEventArgs e)
    {
        if (e.SortExpression.ToString() == Session["Column"])
        {
            if (Session["Order2"] == "ASC")
            {
                lblOrderBy.Text = e.SortExpression.ToString() + " DESC";
                Session["Order2"] = "DESC";
            }
            else
            {
                lblOrderBy.Text = e.SortExpression.ToString() + " ASC";
                Session["Order2"] = "ASC";
            }
        }
        else
        {
            
            lblOrderBy.Text = e.SortExpression.ToString() + " ASC";
            Session["Order2"] = "ASC";
        }

        Session["Column"] = e.SortExpression.ToString();
        Session["OrderBy2"] = lblOrderBy.Text;
        BindData();
    }

    protected void DataGrid1_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        foreach (TableCell tc in e.Item.Cells)
        {
            tc.Attributes["style"] = "border:1px solid #00A651;";
        }
        if (e.Item is GridPagerItem)
        {
            GridPagerItem pagerItem = (GridPagerItem)e.Item;
            RadComboBox PageSizeComboBox = (RadComboBox)pagerItem.FindControl("PageSizeComboBox");
            PageSizeComboBox.Visible = false;
            Label changePageSizelbl = (Label)pagerItem.FindControl("ChangePageSizeLabel");
            changePageSizelbl.Visible = false;
        }
        if (e.Item is GridDataItem)
        //**********************************
        {
            GridDataItem dataItem = e.Item as GridDataItem;
            try
            {
              ((Label)dataItem.FindControl("lblLastUpdatedOn")).Text = DateTime.Parse(((Label)dataItem.FindControl("lblLastUpdatedOn")).Text).ToString("dd MMM h:mm tt").ToString();
            }
            catch
            {
            }
            string htmlColor = (string)DatabaseHelper.executeScalar("select bg_color from priorities where priority_name='" + ((Label)dataItem.FindControl("lblpriority")).Text + "'");
            dataItem.BackColor = System.Drawing.ColorTranslator.FromHtml(htmlColor);
            }
    }

    protected void DataGrid1_PageIndexChanged(object source, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        DataGrid1.CurrentPageIndex = e.NewPageIndex;
        BindData();
    }

    protected void btnClearFilters_Click(object sender, EventArgs e)
    {
        Session["filter"] = null;
        load_dropdowns();
        load_filterDropDown();
        chkViewDeleted.Checked = false;
        chkHideClosed.Checked = false;
        setFileterSession();
        BindData();
    }
    void bindDefault()
    {
        load_dropdowns();
        load_filterDropDown();
        lblOrderBy.Text = "LastUpdatedOn desc";
        chkViewDeleted.Checked = false;
        chkHideClosed.Checked = false;
        getFilterSession();
        BindData();
    }

    protected void chkViewDeleted_CheckedChanged(object sender, EventArgs e)
    {
        lblOrderBy.Text = " LastUpdatedOn desc";
        setFileterSession();
        BindData();
    }
    protected void chkHideClosed_CheckedChanged(object sender, EventArgs e)
    {
        setFileterSession();
        BindData();
    }
    protected void getTotalHrsReport(string strSql)
    {
        sql = @"select sum(ETC)
                from tasks
                where task_id in (" + strSql + ")";

        int ETC = 0;

        object objETC = DatabaseHelper.executeScalar(sql);

        if (objETC != null && objETC.ToString() != "" && objETC.ToString() != "0")
        {
            ETC += (int)objETC;
        }
        else
        {
            ETC = 0;
        }

        sql = @"select sum(datediff(minute,started_date,finished_date))
                from hours_reporting
                where finished_date is not null and task_id in (" + strSql + ")";

        int hrsTaken = 0;
        object objHrsTaken = DatabaseHelper.executeScalar(sql);
        if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
        {
            hrsTaken += (int)objHrsTaken;
        }
        else
        {
            hrsTaken = 0;
        }

        sql = @"select sum(datediff(minute,started_date,getdate()))
                from hours_reporting
                where finished_date is null and task_id in (" + strSql + ")";

        objHrsTaken = DatabaseHelper.executeScalar(sql);

        if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
        {
            hrsTaken += (int)objHrsTaken;
        }


        lblETC.Text = "" + (ETC / 60) + " hrs " + (ETC % 60) + " mins";

        lblTotalHrsTaken.Text = "" + (hrsTaken / 60) + " hrs " + (hrsTaken % 60) + " mins";

        int hrsLeft = ETC - hrsTaken;

        if (hrsLeft < 0)
        {
            hrsLeft = -1 * hrsLeft;
            lblExpectedHrsLeft.Text = "-" + (hrsLeft / 60) + " hrs " + (hrsLeft % 60) + " mins";
        }
        else
        {
            lblExpectedHrsLeft.Text = "" + (hrsLeft / 60) + " hrs " + (hrsLeft % 60) + " mins";
        }

    }
    
    
    

    protected void ChkTask_CheckedChanged(object sender, EventArgs e)
    {
        setFileterSession();
        BindData();
    }
    protected void ChkCrs_CheckedChanged(object sender, EventArgs e)
    {
        setFileterSession();
        BindData();

    }
    protected void ChkIncludecomment_CheckedChanged(object sender, EventArgs e)
    {
        setFileterSession();
        BindData();
    }
}
