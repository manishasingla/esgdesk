﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="addProjectcategory.aspx.cs"
    Inherits="Admin_addcategoryfolder" %>

    <%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Add project category</title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../DashStyle.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
   <%-- <uc2:Admin_Header ID="Admin_Header1" runat="server" />--%>
    <div>
        <div id="inner">
            <div class="pagearea" style="width: 80%; float: left">
                <div id="TicketSummary">
                  
                    <div id="errorExplanation" class="errorExplanation" runat="server" visible="false">
                        <p>
                            There were problems with the following fields:</p>
                        <ul id="ulalready" runat="server" visible="false">
                            <li>Category folder name has already been taken</li></ul>
                        <ul id="ulerror" runat="server" visible="false">
                            <li>Some error while adding category name </li>
                        </ul>
                    </div>
                    <table width="100%" style="background: #EBEBEB; padding: 6px">
                        <tr>
                            <td>
                                Category name *
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtFoldername" runat="server" Style="width: 99%;"> </asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This field is required."
                                    ControlToValidate="txtFoldername" ValidationGroup="error"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Description
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" Style="width: 99%;"> </asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <table width="100%">
                        <tr>
                            <td style="text-align: right">
                                <asp:Button ID="bntdel" runat="server" Text="Delete" Visible="false" Style="float: left"
                                    OnClick="bntdel_Click" />
                                <asp:Button ID="btnupdate" runat="server" Text="Update" Visible="false" ValidationGroup="error" OnClick="btnupdate_Click" />
                                <asp:Button ID="btnsave" runat="server" Text="Save" OnClick="btnsave_Click" ValidationGroup="error" />
                                <asp:Button ID="btnSaveAnother" runat="server" Text="Save and Create Another" OnClick="btnSaveAnother_Click"
                                    ValidationGroup="error" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
