using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net;
using System.Net.NetworkInformation;

public partial class Admin_add_changeRequest : System.Web.UI.Page
{
    int id;
    string sql = "";
    string task_action = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Response.Redirect("login.aspx");
            return;
        }

        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            Response.Redirect("tasks.aspx");
            return;
        }


        string tskid = Request["id"];
        if (tskid == null || tskid == "0" || tskid == "")
        {
            ///id = Convert.ToInt32(id);
            id = 0;
            lnkBackToTask.Visible = false;

        }
        else
        {
            if (DatabaseHelper.is_int(tskid))
            {
                id = Convert.ToInt32(tskid);
                lnkBackToTask.HRef = "edit_task.aspx?id=" + id.ToString();
                lnkBackToTask.Visible = true;
            }
            else
            {
                // Display an error because the bugid must be an integer

                divMessage.InnerHtml = "<br>Task ID must be an integer.";
                divMessage.InnerHtml += "<p><a href='tasks.aspx'>View tasks</a></p>";
                /// DivEntry.Visible = false;
                return;
            }
        }

        if (!Page.IsPostBack)
        {

            if (Session["Canned_msg"] == null)
            {
                //===================================
                //ftbComment.Text = "";
                ftbComment.Content = "";
                //===================================
            }
            else
            {
                if (Session["CannedShortDesc_msg"] == null || Session["CannedShortDesc_msg"].ToString() == "")
                {
                    txtShortDescr.Text = "";
                }
                else
                {
                    txtShortDescr.Text = Session["CannedShortDesc_msg"].ToString();
                }
                //=======================================================================
                ftbComment.Content = "Note to client: " + Session["Canned_msg"].ToString();
                //ftbComment.Text = "Note to client: " + Session["Canned_msg"].ToString();
                //=======================================================================
            }
            ShowSMP.Visible = false;

            load_Compdropdowns();
            load_Userdropdowns();
            Loadprogect_drp();
            id = Convert.ToInt32(Request["id"]);
            /// id = Convert.ToInt32(req_id);
            if (id != 0)
            {
                ///sql = "\nSelect * from tasks where task_id=" + id.ToString() + ";";

                sql = "\nselect * from tasks where task_id = " + id.ToString() + ";";
                sql += "\nSelect * from task_comments where task_id=" + id.ToString() + " order by tc_id desc";

                DataSet dsTask = DatabaseHelper.getDataset(sql);



                //Task details
                if (dsTask.Tables[0].Rows.Count > 0)
                {
                    txtShortDescr.Text = dsTask.Tables[0].Rows[0]["short_desc"].ToString();
                    txtWebsiteURL.Text = dsTask.Tables[0].Rows[0]["Relevant_URL"].ToString();
                    drpCompanyName.SelectedValue = dsTask.Tables[0].Rows[0]["Company"].ToString();
                    Loadprogect_drp();
                    try
                    {
                        drpProjects.SelectedValue = dsTask.Tables[0].Rows[0]["project"].ToString();//// dsTask.Items.FindByText(dsTask.Tables[0].Rows[0]["project"].ToString()).Value;
                    }
                    catch { }
                    try
                    {
                        load_Userdropdowns();
                    }
                    catch { }


                }
                ////else
                ////{
                ////    divMessage.InnerHtml = "<br>Task ID not found.";
                ////    divMessage.InnerHtml += "<p><a href='tasks.aspx'>View tasks</a></p>";

                ////    return;
                ////}

                // merge Task comments
                if (dsTask.Tables[1].Rows.Count > 0)
                {
                    /// ftbComment.Text = "<span style=\"font-style:italic;\">" + "The following has been created from a Change Request added by a client. The words or some of the words are those of the client and therefore may not have been checked or amended for clarity." + "</span>" + "<br/><br/>";

                    for (int i = 0; i < dsTask.Tables[1].Rows.Count; i++)
                    {
                        //=======================================================================================
                        ftbComment.Content += dsTask.Tables[1].Rows[i]["Comment"].ToString() + "<br/><hr/><br/>";
                        //ftbComment.Text += dsTask.Tables[1].Rows[i]["Comment"].ToString() + "<br/><hr/><br/>";
                        //=======================================================================================
                    }
                }


                //////lnkSendAttachment.HRef = "upload_task_attachment.aspx?id=" + id.ToString();
                //////lnkViewAttachment.HRef = "task_attachments.aspx?id=" + id.ToString();
                //////DataSet dsTotalAttachments = DatabaseHelper.getTaskAttachments(id, "");
                //////lnkViewAttachment.InnerHtml = "View attachments (" + dsTotalAttachments.Tables[0].Rows.Count + ")";

            }


        }

    }

    void Loadprogect_drp()
    {
        drpProjects.Items.Clear();

        if (drpCompanyName.SelectedValue.ToString() != "")
        {
            sql = @"select project_name
		from projects
		where archive!='Y' and active = 'Y' and CompanyName='" + drpCompanyName.SelectedValue.ToString() + "' order by project_name;";
        }
        else
        {
            sql = @"select project_name
		from projects
		where archive!='Y' and active = 'Y' order by project_name;";
        }
        DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);
        if (ds_dropdowns.Tables[0].Rows.Count > 0)
        {
            drpProjects.DataSource = ds_dropdowns.Tables[0];
            drpProjects.DataTextField = "project_name";
            drpProjects.DataValueField = "project_name";
            drpProjects.DataBind();
            drpProjects.Items.Insert(0, new ListItem("[no project]", ""));
            ////drpProjects.Items.Insert(1, new ListItem("[Other]", "other"));
        }
        else
        {
            drpProjects.Items.Insert(0, new ListItem("[no project]", ""));
            /// drpProjects.Items.Insert(1, new ListItem("[Other]", "other"));
        }

    }

    void load_Compdropdowns()
    {
        // projects
        sql = @"select Company_Name
		from Company
		where active = 'Y' order by Company_Name;";



        // do a batch of sql statements
        DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);

        drpCompanyName.DataSource = ds_dropdowns.Tables[0];
        drpCompanyName.DataTextField = "Company_Name";
        drpCompanyName.DataValueField = "Company_Name";
        drpCompanyName.DataBind();
        drpCompanyName.Items.Insert(0, new ListItem("[No company]", ""));
    }


    void load_Userdropdowns()
    {
        // projects
        DrpUserName.Items.Clear();

        if (drpCompanyName.SelectedValue.ToString() != "")
        {
            sql = @"select *
		from NonesCRMusers where CompanyName = '" + drpCompanyName.SelectedValue + "' order by UserName;";
        }
        else
        {
            sql = @"select *
		from NonesCRMusers order by UserName;";
        }

        //////        sql = @"select *
        //////		from NonesCRMusers where CompanyName = '" + drpCompanyName.SelectedValue + "' order by UserName;";

        // do a batch of sql statements
        DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);
        if (ds_dropdowns.Tables[0].Rows.Count > 0)
        {
            DrpUserName.DataSource = ds_dropdowns.Tables[0];
            DrpUserName.DataTextField = "UserName";
            DrpUserName.DataValueField = "RegId";
            DrpUserName.DataBind();
            DrpUserName.Items.Insert(0, new ListItem("[No User]", ""));
        }
        else
        {
            DrpUserName.Items.Insert(0, new ListItem("[No User]", ""));
        }

        if (Session["name"] != null)
        {
            DrpUserName.SelectedValue = Session["name"].ToString();
            Session["name"] = null;
        }

    }


    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string CRaddedby;
        string CRPostedby;
        //========================================
        //if (ftbComment.Text.Trim() == "")
        if (ftbComment.Content.Trim() == "")
        //========================================
        {
            divMessage.InnerHtml = "Enter your change request.";
            divMessage.Visible = true;
        }
        else
        {
            //////if (DrpSupport.SelectedItem.ToString() == "[None]")
            //////{
            //////    ftbComment.Text = ftbComment.Text + "<br/><br/><span style=\"color: red;\">" + "Added by support<br/> Note to client: to optimise the request process please add requests directly into the change request system." + "</span>";
            //////}
            //////else
            //////{
            //////    ftbComment.Text = ftbComment.Text + "<br/><br/><span style=\"color: red;\">" + "Added by support (emailed to '" + DrpSupport.SelectedItem.ToString() + "')<br/> Note to client: to optimise the request process please add requests directly into the change request system." + "</span>";
            //////}




            if (DrpSupport.SelectedItem.ToString() == "[None]")
            {
                if (Chkalias.Checked)
                {
                    //===================================================================================================
                    //ftbComment.Text = ftbComment.Text + "<br/><br/><span style=\"color: red;\">" + "Added by support<br/> Note to client: to optimise the request process please add requests directly into the change request system." + "</span>";
                    ftbComment.Content = ftbComment.Content + "<br/><br/><span style=\"color: red;\">" + "Added by support<br/> Note to client: to optimise the request process please add requests directly into the change request system." + "</span>";
                    //===================================================================================================
                }
                else
                {
                    //====================================================================================================
                    //ftbComment.Text = ftbComment.Text;
                    ftbComment.Content = ftbComment.Content;
                    //====================================================================================================
                }
                ///ftbComment.Text = ftbComment.Text + "<br/><br/><span style=\"color: red;\">" + "Added by support.</span>";
            }
            else
            {
                if (Chkalias.Checked)
                {
                    //====================================================================================================
                    //ftbComment.Text = ftbComment.Text + "<br/><br/><span style=\"color: red;\">" + "Added by support (emailed to '" + DrpSupport.SelectedItem.ToString() + "')<br/> Note to client: to optimise the request process please add requests directly into the change request system." + "</span>";
                    ftbComment.Content = ftbComment.Text + "<br/><br/><span style=\"color: red;\">" + "Added by support (emailed to '" + DrpSupport.SelectedItem.ToString() + "')<br/> Note to client: to optimise the request process please add requests directly into the change request system." + "</span>";
                    //====================================================================================================
                }
                else
                {
                    //====================================================================================================
                    //ftbComment.Text = ftbComment.Text;
                    ftbComment.Content = ftbComment.Content;
                    //====================================================================================================
                }
                ///ftbComment.Text = ftbComment.Text + "<br/><br/><span style=\"color: red;\">" + "Added by support (emailed to '" + DrpSupport.SelectedItem.ToString() + "').</span>";
            }

            if (Chkalias.Checked)
            {
                CRaddedby = "Support on behalf of  " + DrpUserName.SelectedItem.ToString();
                CRPostedby = DrpUserName.SelectedItem.ToString();

            }
            else
            {
                CRaddedby = "Support";
                CRPostedby = "Support";
            }

            string interbalref = "";
            if (id == null || id.ToString() == "0" || id.ToString() == "")
            {
                interbalref = "0";

            }
            else
            {
                interbalref = id.ToString();



            }
            string commentaddedby = "";
            try
            {

                commentaddedby = Session["admin"].ToString();
            }
            catch
            {
                commentaddedby = "Support";
            }

            //================================================
            string[] strArrayMacIDAndIP = new string[2];
            string strMacId = "";
            string strIpAddress = "";

            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (nic.OperationalStatus == OperationalStatus.Up)
                {
                    strMacId += nic.GetPhysicalAddress().ToString();
                    break;
                }
            }

            strArrayMacIDAndIP[0] = strMacId;

            strIpAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (strIpAddress == null)

                strIpAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

            strArrayMacIDAndIP[1] = strIpAddress;
            //================================================



            Session["name"] = DrpUserName.SelectedItem.ToString();
            //=====================================================================================================================================================================================
            //object objResult = DatabaseHelper.insertClientRequest("0", DrpUserName.SelectedItem.ToString(), txtWebsiteURL.Text.Trim(), txtShortDescr.Text.Trim(), ftbComment.Text.Trim(), drpCompanyName.SelectedValue.ToString().Replace("'", "''"), drpProjects.SelectedValue.ToString().Replace("'", "''"), CRaddedby, CRPostedby, interbalref.ToString(), commentaddedby);
            object objResult = DatabaseHelper.insertClientRequest("0", DrpUserName.SelectedItem.ToString(), txtWebsiteURL.Text.Trim(), txtShortDescr.Text.Trim(), ftbComment.Content.Trim(), drpCompanyName.SelectedValue.ToString().Replace("'", "''"), drpProjects.SelectedValue.ToString().Replace("'", "''"), CRaddedby, CRPostedby, interbalref.ToString(), commentaddedby, strArrayMacIDAndIP[0], strArrayMacIDAndIP[1], Session["admin"].ToString());
            //=====================================================================================================================================================================================
            if (objResult.ToString() != "0")
            {
                if (id == null || id.ToString() == "0" || id.ToString() == "")
                {

                }
                else
                {
                    sql = "Update tasks set RequestId = " + objResult.ToString() + " where task_id = " + id.ToString() + "";

                    int intResult = DatabaseHelper.executeNonQuery(sql);
                }


                string strBody = generateAdminEmail(Convert.ToInt32(objResult));
                string strBodyclient = generateClientEmail(Convert.ToInt32(objResult));

                DataSet dsUserDetails = DatabaseHelper.getUserDetails2(drpCompanyName.SelectedItem.ToString(), DrpUserName.SelectedItem.ToString());

                bool flag = false;
                //**************************
                if (DatabaseHelper.CanApprove(Session["admin"].ToString()))
                {
                    //**************************

                    if (dsUserDetails == null || dsUserDetails.Tables.Count <= 0 || dsUserDetails.Tables[0].Rows.Count <= 0)
                    {
                    }
                    else
                    {
                           //================================================================================================
                            if (chkEmailnotify.Checked)
                            {
                                string strusername = "";
                                int i = 0;
                                foreach (ListItem boxItem in chkboxclient.Items)
                                {
                                    if (boxItem.Selected == true)
                                    {
                                        if (i == 0)
                                        {
                                            strusername = boxItem.Value + ";";
                                            i++;
                                        }
                                        else
                                        {
                                            strusername += boxItem.Value + ";";
                                        }
                                    }
                                }
                                strusername = strusername.TrimEnd(';');
                                flag = DatabaseHelper.sendEmailChangeRequestNorply(strusername, "Request ID:" + objResult.ToString() + " was updated - Change request form  (Request ID:" + objResult.ToString() + ")", strBodyclient);
                            }

                            //flag = DatabaseHelper.sendEmailChangeRequestNorply(dsUserDetails.Tables[0].Rows[0]["Email"].ToString(), "Request ID:" + objResult.ToString() + " was updated - Change request form  (Request ID:" + objResult.ToString() + ")", strBodyclient);
                        
                    }

                    if (chkEmailnotify.Checked)
                    {
                        flag = DatabaseHelper.sendEmailChangeRequestNorply(ConfigurationManager.AppSettings["ToEmail1"].ToString(), "Request ID:" + objResult.ToString() + " was updated - Change request form  (Request ID:" + objResult.ToString() + ")", strBody);
                    }
                    ///flag = DatabaseHelper.sendEmailChangeRequestNorply(ConfigurationManager.AppSettings["ToEmail2"].ToString(), "Request ID:" + objResult.ToString() + " was updated - Change request form  (Request ID:" + objResult.ToString() + ")", strBody);
                    //*********
                }
                //*********
                divMessage.InnerHtml = "Your change request has been sent.";
                divMessage.Visible = true;
                txtShortDescr.Text = "";
                txtWebsiteURL.Text = "";
                //=======================================================================================================
                //ftbComment.Text = "";
                ftbComment.Content = "";
                //======================================================================================================

                Response.Redirect("edit_request.aspx?reqid=" + objResult.ToString(), false);
            }
            else
            {
                divMessage.InnerHtml = "There was problem in saving you change request. Please try again.";
                divMessage.Visible = true;
            }
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        clearControls();
    }
    void BindusercheckBox()
    {
        if (drpCompanyName.SelectedValue.ToString() != "")
        {
            sql = @"select * from NonesCRMusers where CompanyName = '" + drpCompanyName.SelectedValue + "' order by UserName;";
        }
        else
        {
            sql = @"select * from NonesCRMusers order by UserName;";
        }
        DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);
        if (ds_dropdowns.Tables[0].Rows.Count > 0)
        {
            chkboxclient.Visible = true;
            chkboxclient.DataSource = ds_dropdowns.Tables[0];
            chkboxclient.DataTextField = "UserName";
            chkboxclient.DataValueField = "Email";
            chkboxclient.DataBind();

        }
        else
        {
            chkboxclient.Visible = false;

        }
    }
    private void clearControls()
    {
        txtWebsiteURL.Text = "";
        txtShortDescr.Text = "";
        //=================================================================================================
        //ftbComment.Text = "";
        ftbComment.Content = "";
        //================================================================================================
        divMessage.InnerHtml = "";
        load_Compdropdowns();
        load_Userdropdowns();
    }


    private string generateAdminEmail(int RequestId)
    {
        DataSet dsRequests = DatabaseHelper.getClientRequest(RequestId);
        string strBody = "";
        if (dsRequests == null || dsRequests.Tables.Count <= 0 || dsRequests.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "Request not found.<br><br><a href='requests.aspx'>View requests</a>";
            divMessage.Visible = true;
        }
        else
        {



            strBody += "<b>" + DrpUserName.SelectedItem.ToString() + ":</b> <span style=\"color:red\">";

            //strBody += task_action;

            strBody += "Our support team has added your Change Request " + "<span style=\"color:red\"> (number  </span>" + "<a style=\"color:red\" href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_request.aspx?reqid=" + RequestId + "'>" + RequestId + "</a>) </span> on  " + DateTime.Parse(dsRequests.Tables[0].Rows[0]["LastUpdatedOn"].ToString()).ToString("dd MMM h:mm tt") + "<br><br>";

            strBody += "<span style=\'font-size:11.0pt;font-family:Calibri,sans-serif;color:red\'>Click on the following link to access or amend the change request: " + "  " + "  <a href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_request.aspx?reqid=" + RequestId + "'>" + txtShortDescr.Text.Trim() + " . " + "</a>" + "   Do not start a new change request as this will slow the resolution" + "</span><br><br>";
            strBody += "Client: " + dsRequests.Tables[0].Rows[0]["UserName"].ToString() + "<br>";
            strBody += "Company: " + drpCompanyName.SelectedValue.ToString() + "<br>";
            strBody += "Reported on: " + DateTime.Parse(dsRequests.Tables[0].Rows[0]["RequestDate"].ToString()).ToString("dd MMM h:mm tt") + "<br>";
            strBody += "Title or preferably URL: <a href='" + dsRequests.Tables[0].Rows[0]["WebsiteURL"].ToString() + "' target='_blank'>" + dsRequests.Tables[0].Rows[0]["WebsiteURL"].ToString() + "</a><br>";
            strBody += "Status: (" + dsRequests.Tables[0].Rows[0]["priority"].ToString() + " : " + dsRequests.Tables[0].Rows[0]["status"].ToString() + ")";

            strBody += "<br><br>";

            DataSet dsReqDetails = DatabaseHelper.getClientRequestDetails(RequestId);
            strBody += "The progress of the change request can be seen below:" + "<br><br>";

            strBody += "<table cellspacing=\"0\" border=\"1\" style=\"width: 99%;border-collapse: collapse;\">";
            strBody += "<tbody>";

            for (int i = 0; i < dsReqDetails.Tables[0].Rows.Count; i++)
            {
                strBody += "<tr>";
                strBody += "<td>";
                strBody += "<div style=\"border: 1px none Green;\">";
                strBody += "<table width=\"100%\">";
                strBody += "<tbody>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span style=\"color: Green;\">comment " + dsReqDetails.Tables[0].Rows[i]["CommentId"].ToString() + " posted by " + dsReqDetails.Tables[0].Rows[i]["PostedBy"].ToString() + " on " + DateTime.Parse(dsReqDetails.Tables[0].Rows[i]["PostedOn"].ToString()).ToString("dd MMM h:mm tt") + "</span>";
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\"> </td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span>" + dsReqDetails.Tables[0].Rows[i]["comment"].ToString() + "</span>"; ////+ "<br/><br/><span style=\"color: red;\">" + "Added by support (emailed to 'michaelr@estatesolutions.co.uk') Note to client: to optimise the request process please add requests directly into the change request system." + "</span>"; 
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "</tbody>";
                strBody += "</table>";
                strBody += "</div>";
                strBody += "</td>";
                strBody += "</tr>";
            }
            strBody += "</tbody>";
            strBody += "</table>";
            strBody += "<br><span style=\'font-size:11.0pt;font-family:Calibri,sans-serif;color:red\'>Please do not reply to this email as it does not take replies. Please use the Change Request System as this is the quickest way to process your requests and also keeps a record of communications and progress for you to review at any time.</span><br/>";
            strBody += "<br>";
            strBody += "<br/><br/>" + "Thanks & Regards," + "<br/>Support.";
        }
        return strBody;
    }


    private string generateClientEmail(int RequestId)
    {
        DataSet dsRequests = DatabaseHelper.getClientRequest(RequestId);
        string strBody = "";
        if (dsRequests == null || dsRequests.Tables.Count <= 0 || dsRequests.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "Request not found.<br><br><a href='requests.aspx'>View requests</a>";
            divMessage.Visible = true;
        }
        else
        {



            strBody += "<b>" + DrpUserName.SelectedItem.ToString() + ":</b> <span style=\"color:red\">";

            //strBody += task_action;

            strBody += "Our support team has added your Change Request " + "<span style=\"color:red\"> (number  </span>" + "<a style=\"color:red\" href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/edit_request.aspx?reqid=" + RequestId + "'>" + RequestId + "</a>) </span> on  " + DateTime.Parse(dsRequests.Tables[0].Rows[0]["LastUpdatedOn"].ToString()).ToString("dd MMM h:mm tt") + "<br><br>";

            strBody += "<span style=\'font-size:11.0pt;font-family:Calibri,sans-serif;color:red\'>Click on the following link to access or amend the change request: " + "  " + "  <a href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/edit_request.aspx?reqid=" + RequestId + "'>" + txtShortDescr.Text.Trim() + " . " + "</a>" + "   Do not start a new change request as this will slow the resolution" + "</span><br><br>";
            strBody += "Client: " + dsRequests.Tables[0].Rows[0]["UserName"].ToString() + "<br>";
            strBody += "Company: " + drpCompanyName.SelectedValue.ToString() + "<br>";
            strBody += "Reported on: " + DateTime.Parse(dsRequests.Tables[0].Rows[0]["RequestDate"].ToString()).ToString("dd MMM h:mm tt") + "<br>";
            strBody += "Title or preferably URL: <a href='" + dsRequests.Tables[0].Rows[0]["WebsiteURL"].ToString() + "' target='_blank'>" + dsRequests.Tables[0].Rows[0]["WebsiteURL"].ToString() + "</a><br>";
            strBody += "Status: (" + dsRequests.Tables[0].Rows[0]["priority"].ToString() + " : " + dsRequests.Tables[0].Rows[0]["status"].ToString() + ")";

            strBody += "<br><br>";

            DataSet dsReqDetails = DatabaseHelper.getClientRequestDetails(RequestId);
            strBody += "The progress of the change request can be seen below:" + "<br><br>";

            strBody += "<table cellspacing=\"0\" border=\"1\" style=\"width: 99%;border-collapse: collapse;\">";
            strBody += "<tbody>";

            for (int i = 0; i < dsReqDetails.Tables[0].Rows.Count; i++)
            {
                strBody += "<tr>";
                strBody += "<td>";
                strBody += "<div style=\"border: 1px none Green;\">";
                strBody += "<table width=\"100%\">";
                strBody += "<tbody>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span style=\"color: Green;\">comment " + dsReqDetails.Tables[0].Rows[i]["CommentId"].ToString() + " posted by " + dsReqDetails.Tables[0].Rows[i]["PostedBy"].ToString() + " on " + DateTime.Parse(dsReqDetails.Tables[0].Rows[i]["PostedOn"].ToString()).ToString("dd MMM h:mm tt") + "</span>";
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\"> </td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span>" + dsReqDetails.Tables[0].Rows[i]["comment"].ToString() + "</span>"; ////+ "<br/><br/><span style=\"color: red;\">" + "Added by support (emailed to 'michaelr@estatesolutions.co.uk') Note to client: to optimise the request process please add requests directly into the change request system." + "</span>"; 
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "</tbody>";
                strBody += "</table>";
                strBody += "</div>";
                strBody += "</td>";
                strBody += "</tr>";
            }
            strBody += "</tbody>";
            strBody += "</table>";
            strBody += "<br><span style=\'font-size:11.0pt;font-family:Calibri,sans-serif;color:red\'>Please do not reply to this email as it does not take replies. Please use the Change Request System as this is the quickest way to process your requests and also keeps a record of communications and progress for you to review at any time.</span><br/>";
            strBody += "<br>";
            strBody += "<br/><br/>" + "Thanks & Regards," + "<br/>Support.";
        }
        return strBody;
    }
    protected void drpCompanyName_SelectedIndexChanged(object sender, EventArgs e)
    {
        load_Userdropdowns();
        Loadprogect_drp();
        //=======================
          BindusercheckBox();
        //=======================
    }


    protected void DrpUserName_SelectedIndexChanged(object sender, EventArgs e)
    {
        object objUserId = DatabaseHelper.executeScalar("select ShowSMP from  NonesCRMusers where UserName = '" + DrpUserName.SelectedItem.Text + "'");
        if (objUserId != null)
        {
            if (objUserId.ToString() == "True")
            {
                ShowSMP.Visible = true;
            }
            else
            {
                ShowSMP.Visible = false;
            }
        }
        else
        {
            ShowSMP.Visible = false;
        }
    }

}
