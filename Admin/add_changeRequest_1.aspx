<%@ Page Language="C#" AutoEventWireup="true" CodeFile="add_changeRequest.aspx.cs"
    Inherits="Admin_add_changeRequest" MaintainScrollPositionOnPostback="true" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="Header.ascx" TagName="Header" TagPrefix="uc3" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Src="footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <%= ConfigurationManager.AppSettings["CompanyName"].ToString()%></title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="javascript">

        function hide() {
            //  alert("hide");

            var l = document.getElementById("lnk_h_s");
            var h = document.getElementById("d_h_s");
            var i = document.getElementById("img1");


            if (l.innerHTML == "Add new comment") {
                l.innerHTML = "Close comment box"
                i.src = "../images/Minus.png"
                h.style.display = "block";
            }
            else {
                l.innerHTML = "Add new comment"
                i.src = "../images/plus.png"
                h.style.display = "none";
            }
        }
 
    </script>

    <script language="javascript" type="text/javascript">
        function enableBOB() {
            if (document.getElementById("Chkalias").checked) {
                var drpdwnStates = document.getElementById("DrpSupport");
                drpdwnStates.disabled = !drpdwnStates.disabled;
            }
            else {
                var drpdwnStates = document.getElementById("DrpSupport");
                drpdwnStates.disabled = !drpdwnStates.disabled;
                // document.getElementById("DrpSupport").enabled = false;
            }

        }
    </script>

</head>
<body onload="enableBOB();">
    <form id="form1" runat="server">
    <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>
    <div id="wrapper">
        <div>
            <div id="pageTitle" style="height: 30px" runat="server">
                Request form</div>
            <uc2:Admin_Header ID="Admin_Header1" runat="server" />
            <div id="Content" style="margin-top: 10px;">
                <div id="Content_98">
                    <div id="Div1" class="titleText" style="margin: 10px 0 20px 0; display: none;" runat="server">
                        Request form</div>
                    <div style="padding: 5px; background: #ebebeb;">
                        <a href="client_requests.aspx">Back to client requests</a>&nbsp;|&nbsp; <a id="lnkBackToTask"
                            runat="server" visible="false" href="edit_task.aspx">Back to task</a></div>
                    <div class="divBorder" style="border: solid 1px #cdcdcd; padding: 10px;">
                        <table width="100%">
                            <tr>
                                <td colspan="3">
                                    <p>
                                        The change request system is here for you to <span style="font-weight: bold">make requests
                                            for changes </span>or additions to a project or <span style="font-weight: bold">request
                                                support.</span> Please note these are only requests and may or may not be
                                        actioned dependent on the agreement(s)/subscription(s) in place (*).</p>
                                    <p>
                                        Be as clear and concise as possible.<span style="color: red">The easier the request
                                            is to understand the quicker the change will be for us to complete.If you want the
                                            change request to be reviewed more quickly then it is very important to read the
                                            tips at the foot of this page.</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <hr />
                                    <span style="color: Red">*</span> Note: <span style="color: Green">If you need to upload
                                        any attachment please save the change request first then edit the change request.</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <hr />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <table width="80%">
                                        <tbody>
                                            <tr>
                                                <td valign="top" align="left" colspan="4">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td width="50%" align="left" valign="top">
                                                                Short description:<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                                                    ErrorMessage="Please enter short description." ControlToValidate="txtShortDescr">*</asp:RequiredFieldValidator>
                                                            </td>
                                                            <td valign="top" align="left">
                                                                URL if relevant (Website address i.e. www.ourwebsite.com/contactus.html):
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" align="left" width="50%">
                                                                <asp:TextBox ID="txtShortDescr" runat="server" Width="97%"></asp:TextBox>
                                                            </td>
                                                            <td valign="top" align="left" width="50%">
                                                                <asp:TextBox ID="txtWebsiteURL" runat="server" Width="100%"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td valign="top" align="left" colspan="2">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                            </tr>
                                            <tr>
                                            </tr>
                                            <tr>
                                                <td valign="top" align="left" width="25%">
                                                    <span></span>
                                                </td>
                                                <td valign="top" align="left" width="25%">
                                                </td>
                                                <td valign="top" align="left" width="25%">
                                                </td>
                                                <td valign="top" align="left" width="25%">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" align="left" colspan="4">
                                                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    Company name:
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please select company name."
                                                                        ControlToValidate="drpCompanyName">*</asp:RequiredFieldValidator>
                                                                </td>
                                                                <td>
                                                                    Project name:<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server"
                                                                        ErrorMessage="Please select project name." ControlToValidate="drpProjects">*</asp:RequiredFieldValidator>
                                                                </td>
                                                                <td>
                                                                    User name:<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
                                                                        ErrorMessage="Please user name." ControlToValidate="DrpUserName">*</asp:RequiredFieldValidator>
                                                                </td>
                                                                <td>
                                                                    Types:
                                                                </td>
                                                                <td>
                                                                    OBO:
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="20%" align="left" valign="top">
                                                                    <asp:DropDownList ID="drpCompanyName" runat="server" Width="97%" OnSelectedIndexChanged="drpCompanyName_SelectedIndexChanged"
                                                                        AutoPostBack="True" CssClass="filerDrpodown">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td width="22%" align="left" valign="top">
                                                                    <asp:DropDownList ID="drpProjects" runat="server" Width="97%" CssClass="filerDrpodown">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td width="22%" align="left" valign="top">
                                                                    <asp:DropDownList ID="DrpUserName" runat="server" Width="97%" OnSelectedIndexChanged="DrpUserName_SelectedIndexChanged"
                                                                        AutoPostBack="True" CssClass="filerDrpodown">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="DrpTypes" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td width="10%" align="left" valign="top">
                                                                    <asp:CheckBox ID="Chkalias" onclick="enableBOB();" runat="server" Width="101px" Font-Bold="True"
                                                                        Text="Add as alias"></asp:CheckBox>
                                                                </td>
                                                                <td width="20%" align="left" valign="top">
                                                                    <asp:DropDownList ID="DrpSupport" runat="server" Width="100%" CssClass="filerDrpodown">
                                                                        <asp:ListItem Value="">[None]</asp:ListItem>
                                                                        <asp:ListItem Value="michaelr@estatesolutions.co.uk">michaelr@estatesolutions.co.uk</asp:ListItem>
                                                                        <asp:ListItem Value="michaelr@123sitesolutions.com">michaelr@123sitesolutions.com</asp:ListItem>
                                                                        <asp:ListItem Value="mail@estatesolutions.co.uk">mail@estatesolutions.co.uk</asp:ListItem>
                                                                        <asp:ListItem Value="michaelr@emailsetc.co.uk">michaelr@emailsetc.co.uk</asp:ListItem>
                                                                        <asp:ListItem Value="simonl@emailsetc.co.uk">simonl@emailsetc.co.uk</asp:ListItem>
                                                                        <asp:ListItem Value="support@emailsetc.co.uk">support@emailsetc.co.uk</asp:ListItem>
                                                                        <asp:ListItem Value="support@123sitesolutions.com">support@123sitesolutions.com</asp:ListItem>
                                                                        <asp:ListItem Value="support@estatesolutions.co.uk">support@estatesolutions.co.uk</asp:ListItem>
                                                                        <asp:ListItem Value="support@4saleorlet.co.uk">support@4saleorlet.co.uk</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="4" valign="top">
                                                    <br />
                                                    <div style="height: 22px; background-color: rgb(40, 44, 95); padding-top: 5px; padding-left: 6px;">
                                                        <asp:ImageButton ID="img1" runat="server" src="../images/plus.png" Style="border-width: 0px;
                                                            margin-top: -2px;" OnClientClick="hide();return false;" />
                                                        <asp:LinkButton ID="lnk_h_s" runat="server" OnClientClick="hide();return false;"
                                                            CssClass="whitetext2" Style="text-decoration: none; vertical-align: top;">Close comment box</asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton2" runat="server" OnClientClick="hide();return false;"
                                                            Style="text-decoration: none; vertical-align: top; padding-left: 85%; color: rgb(40, 44, 95)"></asp:LinkButton>
                                                    </div>
                                                    <div id="d_h_s" runat="server" style="display: block">
                                                        <div align="left">
                                                            Change requested: <a id="A1" onclick="window.open('Customer_Support.aspx','','status=yes,width=760,height=560')"
                                                                href="#">Canned message </a><span>&nbsp;</span>
                                                        </div>
                                                        <div style="margin-top: 4px;">
                                                            <%--**********************************************RadEditor*************************************************--%>
                                                            <%--<FTB:FreeTextBox ID="ftbComment" runat="server" Width="100%" Height="200px">
                                                            </FTB:FreeTextBox>--%>
                                                            <telerik:RadEditor ID="ftbComment" runat="server" Height="300px" Width="100%">
                                                                <Snippets>
                                                                    <telerik:EditorSnippet Name="Product Template" Value="<table><tr><td><div style='width:300px;border:2px outset #D8D2BD;padding:34px;font-family:Arial, Verdana; font-size:11px;background- color:#F1EFE6;'>Dear ____________________,<br>Thank you for inquiring about ____________. Your request will be processed in 48 hours and shipped at the address you have provided.<b>Please, contact us if you have any problems.</b></div></td></tr></table>">            
                                                                    </telerik:EditorSnippet>
                                                                    <telerik:EditorSnippet Name="Signature" Value="<strong>John Smith</strong><br>Sales Manager<br>">
                                                                    </telerik:EditorSnippet>
                                                                </Snippets>
                                                                <Links>
                                                                    <telerik:EditorLink Name="Telerik" Href="http://www.telerik.com">
                                                                        <telerik:EditorLink Name="Telerik Products" Href="http://www.telerik.com/products"
                                                                            Target="_blank" />
                                                                        <telerik:EditorLink Name="Telerik Support Center" Href="http://www.telerik.com/support/home.aspx"
                                                                            Target="_blank" />
                                                                        <telerik:EditorLink Name="Telerik Community" Href="http://www.telerik.com/community/home.aspx"
                                                                            Target="_blank" ToolTip="Telerik Community" />
                                                                    </telerik:EditorLink>
                                                                </Links>
                                                                <Tools>
                                                                </Tools>
                                                                <ImageManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                                                <MediaManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                                                <FlashManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                                                <TemplateManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                                                <DocumentManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                                                <Modules>
                                                                    <telerik:EditorModule Name="RadEditorStatistics" Visible="False" Enabled="true" />
                                                                    <telerik:EditorModule Name="RadEditorDomInspector" Visible="False" Enabled="true" />
                                                                    <telerik:EditorModule Name="RadEditorNodeInspector" Visible="False" Enabled="true" />
                                                                    <telerik:EditorModule Name="RadEditorHtmlInspector" Visible="False" Enabled="true" />
                                                                </Modules>
                                                                <Content>
                                                                </Content>
                                                            </telerik:RadEditor>
                                                            <%--********************************************************************************************************--%>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" align="left" colspan="4">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" align="left" colspan="4">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" align="left">
                                                </td>
                                                <td valign="top" align="left">
                                                    <asp:ValidationSummary ID="ValidationSummary3" runat="server" ShowSummary="False"
                                                        ShowMessageBox="True"></asp:ValidationSummary>
                                                </td>
                                                <td valign="top" align="left">
                                                </td>
                                                <td valign="top" align="left">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" align="left">
                                                    <div id="divMessage" runat="server" visible="false">
                                                    </div>
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                <asp:CheckBoxList id="chkboxclient" runat="server" AutoPostBack="true" RepeatDirection="Horizontal" ></asp:CheckBoxList>
                                                </td>
                                                <td valign="top" align="right">
                                                  <div style="float: right">
                                                  <asp:CheckBox ID="chkEmailnotify" runat="server" Font-Bold="True" Text="Email notify&nbsp;&nbsp;" Checked="true" Style="float: left;">
                                                  </asp:CheckBox>&nbsp;<asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" CssClass="blueBtns" Text="Submit" />
                                                   <asp:Button ID="btnClear" runat="server" CausesValidation="False" OnClick="btnClear_Click" CssClass="blueBtns" Text="Clear" /></div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <hr />
                                    <p style="font-weight: bold">
                                        <span style="color: Red; font-size: 18px">What are the benefits to you?</span> Why
                                        use the Change Request system?</p>
                                    <p>
                                    </p>
                                    <div style="padding-left: 15px">
                                        <li>The support team will receive the request<span style="color: Red"> immediately.</span></li>
                                        <li>You will also be able to login at any time and <span style="color: Red">check the
                                            status.</span></li>
                                        <li>The ongoing progress will be <span style="color: Red">logged in one place</span>
                                            rather than through a stream of unrelated email communications.</li>
                                        <li>If the support team require to speak directly with you they can initiate an <span
                                            style="color: Red">online chat</span> with you or <span style="color: Red">remote desktop
                                                login. </span></li>
                                    </div>
                                    <p style="font-weight: bold">
                                        <span style="color: Red; font-size: 18px">How to get your request looked at quickly?</span>
                                        Tips on ensuring that the change request is reviewed and/or processed in the least
                                        amount of time.</p>
                                    <p>
                                    </p>
                                    <div style="padding-left: 15px">
                                        <li>Be as<span style="color: Red"> clear</span> and concise as possible</li>
                                        <li>When logging in ensure that you <span style="color: red">check the 'Save details'
                                            check box</span>. You will then never have to login again from that PC (if cookies
                                            are enabled).</li>
                                        <li><span style="color: red">Dont reply to the emails which are sent automatically </span>
                                            when any change request is updated. Login to your account and add any comments directly
                                            into the system</li>
                                        <li>If you have multiple changes for a certain page enter the changes in the same change
                                            request. <span style="color: Red">If you create more than one change request for one
                                                issue as it will slow the procedure.</span></li>
                                        <li>Ensure that any reference to a part of the site is referred to by page name or preferably
                                            URL (i.e. www.acmeco.com/contactus.htm) and the paragraph or element</li>
                                        <li>If you need to upload any attachment please save the change request first then edit
                                            the change request.</li>
                                        <li>If you are using Windows 7 you may find the inbuilt 'snipping tool' useful for pasting
                                            screenshots into yout change request.</li>
                                        <li><span>Save this website as a bookmark so you can quickly access it.</span><span
                                            style="margin-top: 20px"><a class="addthis_button" href="http://www.addthis.com/bookmark.php?v=250&amp;username=mrscottjames">
                                                <img src="http://s7.addthis.com/static/btn/v2/lg-share-en.gif" width="125" height="16"
                                                    alt="Bookmark and Share" style="border: 0" /></a>

                                            <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#username=mrscottjames"></script>

                                        </span></li>
                                    </div>
                                    <p>
                                        To most efficiently explain a text change please look at the following as example<br />
                                        In www.acmeco.com/aboutus.html, please change:<br />
                                        Acme Co is a great company in London which produces fantastic widgets for the discerning
                                        widget consumer.<br />
                                        to<br />
                                        Acme Co is a great company in the UK which produces fantastic widgets for the discerning
                                        widget consumer.
                                    </p>
                                    <div id="ShowSMP" runat="server">
                                        <p>
                                            <span style="color: Red; font-weight: bold; font-size: 18px">What support and/or maintenance
                                                packages are available?</span></p>
                                        <p>
                                            There are several packages available for premier support (immediate response) and
                                            to cut the costs of website maintenance. Please <a href="http://www.request.estatesolutions.eu/Support_and_maintenance_contracts.pdf"
                                                target="_blank" style="text-decoration: underline">download</a> the following
                                            PDF which outlines all the options including non related IT support.</p>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <uc1:footer ID="Footer2" runat="server" />
    </div>
    <%--</ContentTemplate>
    </asp:UpdatePanel>--%>
    </form>
</body>
</html>
