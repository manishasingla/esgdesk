﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Admin_addcategoryfolder : System.Web.UI.Page
{
    string categroy_id = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Response.Redirect("login.aspx");
            return;
        }
        else
        {
            if (!DatabaseHelper.isPrivateSolutions(Session["admin"].ToString()))
            {
                Response.Redirect("Solution.aspx");
            }
        }
        bntdel.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete this category folder?')");
        if (!IsPostBack)
        {
            if (Request.QueryString["categoryid"] != null && Request.QueryString["folderid"] != null)
            {

                GetFolderCategory(Request.QueryString["categoryid"].ToString(), Request.QueryString["folderid"].ToString());
                //  categroy_id = Request.QueryString["cid"].ToString();
            }
            else if (Request.QueryString["categoryid"] != null)
            {

                // categroy_id = Request.QueryString["cid"].ToString();
            }
            else
            {
                Response.Redirect("Solution.aspx");
            }

        }



    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        FolderInsert("");
    }

    private void FolderInsert(string next)
    {
        string sqlsub = @"select * from Category_Folder where FolderName='" + txtFoldername.Text.Replace("'", "|@").Trim() + "' and Category_Id=" + Request.QueryString["categoryid"].ToString();


        int Result = DatabaseHelper.checkuser(sqlsub);

        if (Result <= 0)
        {
            string sql = @"insert into Category_Folder (FolderName,FolderDescription,Category_Id,VisibleTo)values
                     ('" + txtFoldername.Text.Replace("'", "|@").Trim() + "','" + txtDescription.Text.Replace("'", "|@") + "'," + Request.QueryString["categoryid"].ToString() + ",'" + ddlallow.SelectedItem.Value + "')";

            int intResult = DatabaseHelper.executeSQLquery(sql);

            if (intResult == 0)
            {
                errorExplanation.Visible = true;
                ulerror.Visible = true;
            }
            else
            {
                if (next == "")
                {
                    Response.Redirect("Solution.aspx");
                }
                else
                {
                    clear();
                }
            }
        }
        else
        {
            errorExplanation.Visible = true;
            ulalready.Visible = true;
        }

    }

    private void Folderdelete(string Categoryid, string folderid)
    {
        string sqlsub = @"delete from solutions where Category_id=" + Categoryid + " and Folder_Id=" + folderid;

        int Result = DatabaseHelper.executeSQLquery(sqlsub);
        if (Result == 0)
        {
            sqlsub = @"delete from category_folder where Category_id=" + Categoryid + " and Folder_Id=" + folderid;
            Result = DatabaseHelper.executeSQLquery(sqlsub);
            if (Result == 0)
            {
                errorExplanation.Visible = true;
                ulerror.Visible = true;
            }
            else
            {
                //Response.Redirect("Solutionslist.aspx?categoryid=" + Request.QueryString["categoryid"].ToString() + "&folderid=" + Request.QueryString["folderid"].ToString());
                Response.Redirect("Solutionslist.aspx?categoryid=" + Request.QueryString["categoryid"].ToString());
                //Response.Redirect("Solution.aspx");
            }
        }

    }
    protected void btnSaveAnother_Click(object sender, EventArgs e)
    {
        FolderInsert("Yes");
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if (Request.QueryString["categoryid"] != null && Request.QueryString["folderid"] != null)
        {
            Response.Redirect("Solutionslist.aspx?categoryid=" + Request.QueryString["categoryid"].ToString() + "&folderid=" + Request.QueryString["folderid"].ToString());
        }
        //else if (Request.QueryString["categoryid"] != null)
        //{
        //    Response.Redirect("Solutionslist.aspx?categoryid=" + Request.QueryString["categoryid"].ToString());
        //}
        else
        {
            Response.Redirect("Solution.aspx");
        }
        // Response.Redirect("Solutionslist.aspx?cid=" + Request.QueryString["cid"].ToString());
    }

    void clear()
    {
        txtFoldername.Text = string.Empty;
        txtDescription.Text = string.Empty;
        errorExplanation.Visible = false;
        ulalready.Visible = false;
        ulerror.Visible = false;
    }
    private void GetFolderCategory(string Categoryid, string folderid)
    {
        string sql = @"select * from Category_Folder where Category_id=" + Categoryid + " and Folder_Id=" + folderid;


        DataSet ds = DatabaseHelper.getallstatus(sql);

        if (ds != null)
        {
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                txtFoldername.Text = ds.Tables[0].Rows[0]["FolderName"].ToString().Replace("|@", "'");
                txtDescription.Text = ds.Tables[0].Rows[0]["FolderDescription"].ToString().Replace("|@", "'");
               // ddlallow.SelectedItem.Value = ds.Tables[0].Rows[0]["VisibleTo"].ToString();

                ddlallow.SelectedValue = ds.Tables[0].Rows[0]["VisibleTo"].ToString();
                
                bntdel.Visible = true;
                btnupdate.Visible = true;
                btnsave.Visible = false;
                btnSaveAnother.Visible = false;
            }
        }
        else
        {
            errorExplanation.Visible = true;
            ulalready.Visible = true;
        }

    }
    protected void bntdel_Click(object sender, EventArgs e)
    {
        Folderdelete(Request.QueryString["categoryid"].ToString(), Request.QueryString["folderid"].ToString());
    }

    private void FolderUpdate(string cid, string fid)
    {
        string sqlsub = @"select * from Category_Folder where FolderName='" + txtFoldername.Text.Replace("'", "|@").Trim() + "' and category_id<>" + cid + " and Folder_Id<>" + fid;


        int Result = DatabaseHelper.checkuser(sqlsub);

        if (Result <= 0)
        {
            string sql = @"update Category_Folder set FolderName='" + txtFoldername.Text.Replace("'", "|@").Trim() + "',FolderDescription='" + txtDescription.Text.Replace("'", "|@") + "' ,VisibleTo='" + ddlallow.SelectedItem.Value + "' where Category_Id=" + cid + " and Folder_Id=" + fid;


            int intResult = DatabaseHelper.executeSQLquery(sql);

            if (intResult == 0)
            {
                errorExplanation.Visible = true;
                ulerror.Visible = true;
            }
            else
            {
                Response.Redirect("Solutionslist.aspx?categoryid=" + Request.QueryString["categoryid"].ToString() + "&folderid=" + Request.QueryString["folderid"].ToString());
                //Response.Redirect("Solution.aspx");

            }
        }
        else
        {
            errorExplanation.Visible = true;
            ulalready.Visible = true;
        }

    }

    protected void btnupdate_Click(object sender, EventArgs e)
    {
        FolderUpdate(Request.QueryString["categoryid"], Request.QueryString["folderid"]);
    }
}