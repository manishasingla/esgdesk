<%@ Page Language="C#" AutoEventWireup="true" CodeFile="admin.aspx.cs" Inherits="Admin_admin" %>

<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Admin dashboard</title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <uc2:Admin_Header ID="Admin_Header1" runat="server" />
        <div id="Content">
        <div id="pageTitle" style="height:30px;font-weight:bold;text-align:left">Control Panel</div>
         <div style="float:right">
        <span style="color:Black; font-size:16px" id="Notification" runat="server">Notifications:<asp:LinkButton id="LnkNotification" runat="server" ForeColor="red" OnClick="LnkNotification_Click" ></asp:LinkButton></span>&nbsp;
             <span style="color:green;" id="NewCR" runat="server">New CRs<asp:LinkButton id="LnkNewCR" runat="server" OnClick="LnkNewCR_Click"></asp:LinkButton></span>&nbsp;
              <span style="color:green;" id="CRwocr" runat="server">CR(wocr)<asp:LinkButton id="LnkCRWocr" runat="server" OnClick="LnkCRWocr_Click"></asp:LinkButton></span>&nbsp;
             <span style="color:green;" id="CrNewComment" runat="server">CRs with new comment<asp:LinkButton id="LnkCrNewComment" runat="server" OnClick="LnkCrNewComment_Click"></asp:LinkButton></span>
              <span style="color:green;" id="MakeCommentRead" runat="server"><asp:LinkButton id="LnkMakecommentRead" runat="server" OnClick="LnkMakecommentRead_Click"></asp:LinkButton></span>&nbsp;
              <span style="color:Red;" id="unansweredque" runat="server">My unanswered questions<asp:LinkButton id="lnkUnansweredQuestions" runat="server" OnClick="lnkUnansweredQuestions_Click" CausesValidation="False"></asp:LinkButton></span>&nbsp;
          <span style="color:Red;" id="immediatetask" runat="server">My immediate tasks<asp:LinkButton id="lnkPr0Task" runat="server" OnClick="lnkPr0Task_Click" CausesValidation="False"></asp:LinkButton></span>&nbsp;
           <span style="color:Red;" id="OverDuetsk" runat="server">Overdue tasks <asp:Button id="BtnOverdue" runat="server"  BackColor="transparent"  ForeColor="blue" BorderStyle="none" style="cursor:pointer;width:auto" CausesValidation="False" OnClick="BtnOverdue_Click" /></span>
          <span style="color:#EE82EE;" id="highestTasks" runat="server">My 1a  tasks<asp:LinkButton id="lnkHighestTasks" runat="server" OnClick="lnkHighestTasks_Click" CausesValidation="False"></asp:LinkButton></span>&nbsp;
          <span style="color:Black;" id="highTasks" runat="server">My high tasks<asp:LinkButton id="lnkHighTasks" runat="server" OnClick="lnkHighTasks_Click" CausesValidation="False"></asp:LinkButton></span>&nbsp;
          <span style="color:Black;" id="PR1CTask" runat="server">My 1c tasks<asp:LinkButton id="Lnk1cTasks" runat="server"  CausesValidation="False" OnClick="Lnk1cTasks_Click"></asp:LinkButton></span>&nbsp;
           <span style="color:Red;" id="TskNewComment" runat="server">My tasks with new comment<asp:LinkButton id="LnkTskNewcomment" runat="server" OnClick="LnkTskNewcomment_Click"></asp:LinkButton></span>
           <span style="color:green;" id="spnMakeTskcmntRead" runat="server"><asp:LinkButton id="LnkMakeTaskcommentRead" runat="server" OnClick="LnkMakeTaskcommentRead_Click"></asp:LinkButton></span>&nbsp;
           <span style="color:Red;" id="SpnAllTaskNewComment" runat="server">Tasks with new comment<asp:LinkButton id="LnkAllTsknewComment" runat="server" OnClick="LnkAllTsknewComment_Click"></asp:LinkButton></span>
           <span style="color:green;" id="Span2" runat="server"><asp:LinkButton id="LnkMakeAllTaskCmmntRead" runat="server" OnClick="LnkMakeAllTaskCmmntRead_Click"></asp:LinkButton></span>&nbsp;
           
                                </div>
        <div id="divMessage" runat="server" style="color:Red;font-weight:bold"></div>
        <div class="divBorder">
        <ul style="line-height:25px">
            <li><a href="categories.aspx">Categories</a></li>
            <li><a href="priorities.aspx">Priorities</a></li>
            <li><a href="Company.aspx">Companies</a></li>
            <li><a href="projects.aspx">Projects</a></li>
             <li><a href="Resources.aspx">Resources</a></li>
            <li><a href="client_details.aspx">Registered clients</a></li>
            <li><a href="users.aspx">Registered emps</a></li>
            <li><a href="statuses.aspx">Statuses</a></li>
            <li><a href="Subscription_details.aspx">Client subscription</a></li>
             <li><a href="GoogleAcList.aspx"> Google Analytics </a></li>
              <li><a href="TypeList.aspx">Support type </a></li>
               <li><a href="Notes_categories.aspx">Notes categories</a></li>
               
        </ul>
        </div>          
        </div>    
        
        </div><uc1:footer ID="Footer1" runat="server" />
    </form>
</body>
</html>
