<%@ Page Language="C#" AutoEventWireup="true" CodeFile="call_taken.aspx.cs" Inherits="Admin_call_taken" %>

<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Call taken</title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        function checkChecked()
        {
            if(document.getElementById("chkShowAllHistory").checked==true)
            {
                document.getElementById("divNoHistory").style.display="none";
                document.getElementById("divAllHistory").style.display="block";       
            }
            else
            {
                document.getElementById("divNoHistory").style.display="block";
                document.getElementById("divAllHistory").style.display="none";       
            }
        }
    </script>

</head>
<body onload="checkChecked();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
     
            <div id="Content">
                <div class="titleText" style="margin-bottom: 10px;">
                    Call taken</div>
                <div id="divMessage" runat="server" style="color: Red; font-weight: bold">
                </div>
                <div id="DivEntry" runat="server">
                    <div id="divTask" runat="server" class="divBorder">
                        <table width="100%" cellpadding="5" style="padding-top: 20px;">
                            <tr>
                                <td align="left" colspan="4" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                Project:&nbsp;
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="drpProjects" runat="server" Width="175px">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10px">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="drpProjects"
                                                    ErrorMessage="Project is required.">*</asp:RequiredFieldValidator>
                                            </td>
                                            <td style="width: 10px">
                                            </td>
                                            <td>
                                                Re-assign:&nbsp;
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="drpUsers" runat="server" Width="175px">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10px">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="drpUsers"
                                                    ErrorMessage="Re-assign to employee is required.">*</asp:RequiredFieldValidator>
                                            </td>
                                            <td style="width: 10px">
                                            </td>
                                            <td>
                                                Priority:
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="drpPriorities" runat="server" Width="175px">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                            </td>
                                            <td style="width: 10px">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="drpPriorities"
                                                    ErrorMessage="Priority is required.">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td style="width: 10px">
                                            </td>
                                            <td style="width: 10px">
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td style="width: 10px">
                                            </td>
                                        </tr>
                                        <tr style="display: none;">
                                            <td>
                                                Project type:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="drpProjectType" runat="server" Width="175px">
                                                    <asp:ListItem></asp:ListItem>
                                                    <asp:ListItem>es</asp:ListItem>
                                                    <asp:ListItem Selected="True">other</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10px">
                                            </td>
                                            <td style="width: 10px">
                                            </td>
                                            <td>
                                                Status:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="drpStatuses" runat="server" Width="175px">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10px">
                                            </td>
                                            <td style="width: 10px">
                                            </td>
                                            <td>
                                                Category:
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="drpCategories" runat="server" Width="175px">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                            </td>
                                            <td style="width: 10px">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="4" valign="top">
                                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator1">
                                    </cc1:ValidatorCalloutExtender>
                                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server" TargetControlID="RequiredFieldValidator3">
                                    </cc1:ValidatorCalloutExtender>
                                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="server" TargetControlID="RequiredFieldValidator6">
                                    </cc1:ValidatorCalloutExtender>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="4" valign="top">
                                    <table>
                                        <tr style="display: none;">
                                            <td align="left" valign="top">
                                                Short description:
                                            </td>
                                            <td align="left" valign="top">
                                                <asp:TextBox ID="txtShortDescr" runat="server" Width="650px"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="4" valign="top">
                                    <div id="Divsessionmsg" runat="server" style="color: Red; font-weight: bold">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="4" valign="top">
                                    <span style="font-size: 16px; font-weight: bold">Caller details (please complete as
                                        much as possible)</span>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="4" valign="top">
                                    <table cellpadding="3" cellspacing="0">
                                        <tr>
                                            <td valign="top">
                                                Name:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtName" runat="server" Width="525px" MaxLength="60"></asp:TextBox>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                Company:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtCompany" runat="server" Width="525px" MaxLength="100"></asp:TextBox>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                Tel:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtTel" runat="server" Width="525px" MaxLength="20"></asp:TextBox>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                Email Address:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtEmail" runat="server" Width="525px" MaxLength="100"></asp:TextBox>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                Product/service enquiring about:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtProduct" runat="server" Width="525px"></asp:TextBox>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                Description of enquiry:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtDescription" runat="server" Rows="5" TextMode="MultiLine" Width="525px"></asp:TextBox>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                What action to take:
                                            </td>
                                            <td>
                                                <asp:CheckBoxList ID="chkAction" runat="server" RepeatColumns="4">
                                                    <asp:ListItem>call back</asp:ListItem>
                                                    <asp:ListItem>email more details</asp:ListItem>
                                                    <asp:ListItem>nothing</asp:ListItem>
                                                    <asp:ListItem>caller will call again</asp:ListItem>
                                                </asp:CheckBoxList>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                Any relevant website URLs:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtRelevantURL" runat="server" Width="525px" Rows="2" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" colspan="2">
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td>
                                                            <div id="divMessage1" runat="server" style="color: Red">
                                                            </div>
                                                        </td>
                                                        <td align="right">
                                                            <asp:Button ID="btnUpdate" runat="server" Text="Submit" OnClick="btnUpdate_Click"
                                                                CssClass="blueBtns" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
       
    </form>
</body>
</html>
