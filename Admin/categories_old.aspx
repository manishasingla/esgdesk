<%@ Page Language="C#" AutoEventWireup="true" CodeFile="categories.aspx.cs" Inherits="Admin_categories" EnableEventValidation="false" %>

<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="Notifications.ascx" TagName="Notifications" TagPrefix="uc4" %>

<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= ConfigurationManager.AppSettings["CompanyName"].ToString()%></title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <uc2:Admin_Header ID="Admin_Header1" runat="server" />
        <div id="Content">
        <div id="pageTitle" style="height:30px;font-weight:bold;text-align:left">Categories</div>
          <div>
          <uc4:Notifications ID="Notifications" runat="server" />
          </div>
        <div >
            <asp:LinkButton ID="lnkAddNew" runat="server" OnClick="lnkAddNew_Click" CausesValidation="False">add new category</asp:LinkButton>
            &nbsp;&nbsp;
            <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="False">back to categories</asp:LinkButton>
             
            
            </div>
            <br />
        <div id="DivEntry" runat="server" visible="false" >
            <table>
                <tr>
                    <td>
                        Type id:</td>
                    <td>
                    </td>
                    <td>
                        <asp:Label ID="lblId" runat="server">New</asp:Label></td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        Type name:
                    </td>
                    <td>
                    </td>
                    <td>
                        <asp:TextBox ID="txtName" runat="server" MaxLength="50"></asp:TextBox><cc1:ValidatorCalloutExtender
                            ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator1">
                        </cc1:ValidatorCalloutExtender>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName"
                            ErrorMessage="Please enter type name.">*</asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td>
                        Support URL:
                    </td>
                    <td>
                    </td>
                    <td valign="top">
                        <asp:TextBox ID="txtSortSequece" runat="server" ></asp:TextBox>
                        <cc1:ValidatorCalloutExtender
                            ID="ValidatorCalloutExtender2" runat="server" TargetControlID="RequiredFieldValidator2">
                        </cc1:ValidatorCalloutExtender>
                    </td>
                    <td>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtSortSequece"
                            ErrorMessage="Please enter support url.">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
        
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                        <asp:Button ID="btnCreate" runat="server" Text="Create" OnClick="btnCreate_Click" /></td>
                    <td>
                    </td>
                </tr>
            </table>
        </div> 
        <div style="float:left; padding-left:416px"><b>Records: </b> <asp:Label ID="LblRecordsno" runat="server" Text="" ></asp:Label></div>
        <div id="divMessage" runat="server" style="color:Red;font-weight:bold"></div>
        <br />
        <div> 
            <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" CellPadding="5" OnItemCommand="DataGrid1_ItemCommand" OnItemDataBound="DataGrid1_ItemDataBound">
                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" HorizontalAlign="Center" />
                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" HorizontalAlign="Center" BackColor="Gainsboro" />
                <Columns>
                    <asp:BoundColumn DataField="category_id" HeaderText="Category id"></asp:BoundColumn>
                    <asp:BoundColumn DataField="category_name" HeaderText="Category name">
                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                            Font-Underline="False" HorizontalAlign="Left" />
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="sort_seq" HeaderText="Sort sequence"></asp:BoundColumn>
                    <asp:BoundColumn DataField="default_selection" HeaderText="Default selection"></asp:BoundColumn>
                    <asp:ButtonColumn CommandName="edit" HeaderText="edit" Text="edit"></asp:ButtonColumn>
                    <asp:ButtonColumn CommandName="delete" HeaderText="delete" Text="delete" ></asp:ButtonColumn>
                </Columns>
            </asp:DataGrid>&nbsp;
            <asp:HiddenField ID="prevName" runat="server" />
        </div>
        </div>    
        
        </div>       <uc1:footer ID="Footer1" runat="server" />
    </form>
</body>
</html>
