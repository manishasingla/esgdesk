using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Admin_categories : System.Web.UI.Page
{
    string sql = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        LnkMakecommentRead.Attributes.Add("onclick", "return confirm('Sure you want to mark every comment in every CR as read by you?');");
        LnkMakeTaskcommentRead.Attributes.Add("onclick", "javascript:return confirm('Sure you want to mark every comment in each of your tasks as read by you (this means you must have read them all and acted on all that require action already)?');");
        LnkMakeAllTaskCmmntRead.Attributes.Add("onclick", "javascript:return confirm('Sure you want to mark every comment in each of  tasks as read by you (this means you must have read them all and acted on all that require action already)?');");
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = "categories.aspx";
            Response.Redirect("login.aspx");
            return;
        }

        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
           Response.Redirect("tasks.aspx");
           return;
        } 

        divMessage.InnerHtml = "";

        if (!Page.IsPostBack)
        {
            getNewCR();
            getCRNewComment();
            getCRwaitingfrmClient();
            getUnasweredQuestions();
            getPR0Tasks();
            getPR1aTasks();
            getPR1bTasks();
            getTskNewComment();
            getAllTskNewComment();
            bindData();

        }
    }
    void getCRwaitingfrmClient()
    {
        sql = @"select ClientRequest.* from ClientRequest 
                where ClientRequest.Status = 'awaiting client response- required' and status <> 'closed' and deleted <> 1 ";



        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {

            if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
            {

                CRwocr.Visible = false;
            }
            else
            {
                LnkCRWocr.Text = "(" + ds.Tables[0].Rows.Count + ")";
                CRwocr.Visible = true;
            }

        }
        else
        {


            if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
            {

                CRwocr.Visible = false;
            }
            else
            {
                LnkCRWocr.Text = "(0)";
                CRwocr.Visible = false;
            }


        }
    }
    void getCRNewComment()
    {


        //// sql = @"select ClientRequest.* from ClientRequest where ClientRequest.[RequestId] in (select [RequestId] from ClientRequest_Details where [CommentId] not in (select  CommentId from read_CR_comments)) and ClientRequest.deleted <> 1";
        ////  sql = @"select ClientRequest_Details.* from ClientRequest_Details where [CommentId] not in (select  CommentId from read_CR_comments where [UserName]='Support') and ClientRequest_Details.deleted <> 1";
        sql = "select ClientRequest.* from ClientRequest where RequestId in(select ClientRequest_Details.RequestId from ClientRequest_Details where [CommentId] not in (select  CommentId from read_CR_comments where read_CR_comments.username='" + Session["admin"].ToString() + "') and ClientRequest_Details.deleted <> 1)";

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
            {
                /// LnkNewCR.Text = "(" + ds.Tables[0].Rows.Count + ")";
                CrNewComment.Visible = false;
                LnkMakecommentRead.Visible = false;
            }
            else
            {
                LnkCrNewComment.Text = "(" + ds.Tables[0].Rows.Count + ")";
                LnkMakecommentRead.Text = "(" + "-" + ")";
                LnkMakecommentRead.Visible = true;
                CrNewComment.Visible = true;
            }

        }
        else
        {

            if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
            {
                /// LnkNewCR.Text = "(" + ds.Tables[0].Rows.Count + ")";
                CrNewComment.Visible = false;
                LnkMakecommentRead.Visible = false;
            }
            else
            {
                LnkCrNewComment.Text = "(0)";
                CrNewComment.Visible = false;
                LnkMakecommentRead.Visible = false;
            }




        }
    }
    protected void LnkCRWocr_Click(object sender, EventArgs e)
    {

        Session["filter"] = "CRwocr";
        Response.Redirect("client_requests.aspx", false);


    }
    protected void LnkNewCR_Click(object sender, EventArgs e)
    {

        Session["filter"] = "NewCR";
        Response.Redirect("client_requests.aspx", false);
        /// bindPR1bTasks();

    }
    protected void LnkCrNewComment_Click(object sender, EventArgs e)
    {
        Session["filter"] = "CRNewComment";
        Response.Redirect("client_requests.aspx", false);

    }

    void getNewCR()
    {
        sql = @"select ClientRequest.* from ClientRequest 
                where ClientRequest.Status = 'new' and status <> 'closed' and deleted <> 1 ";

        ////if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        ////{
        ////    sql += " and status <> 'closed' and assigned_to_user = '" + Session["admin"] + "' ";
        ////}
        ////else
        ////{
        ////    sql += " and assigned_to_user = '" + Session["admin"] + "' ";
        ////}

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            ///  LnkNewCR.Text = "(" + ds.Tables[0].Rows.Count + ")";
            /// NewCR.Visible = true;
            if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
            {
                /// LnkNewCR.Text = "(" + ds.Tables[0].Rows.Count + ")";
                NewCR.Visible = false;
            }
            else
            {
                LnkNewCR.Text = "(" + ds.Tables[0].Rows.Count + ")";
                NewCR.Visible = true;
            }

        }
        else
        {


            if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
            {
                /// LnkNewCR.Text = "(" + ds.Tables[0].Rows.Count + ")";
                NewCR.Visible = false;
            }
            else
            {
                LnkNewCR.Text = "(0)";
                NewCR.Visible = false;
            }


        }
    }

    protected void LnkMakecommentRead_Click(object sender, EventArgs e)
    {
        string sql = "select *  from ClientRequest_Details where [CommentId] not in (select  CommentId  from read_CR_comments where read_CR_comments.username='" + Session["admin"].ToString() + "') and ClientRequest_Details.deleted <> 1";
        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {
                    string sqlinsert = "insert into read_CR_comments([CommentId],[UserName]) ";
                    sqlinsert += " values(" + ds.Tables[0].Rows[i]["CommentId"].ToString() + ",'" + Session["admin"].ToString() + "')";

                    int intResult = DatabaseHelper.executeNonQuery(sqlinsert);
                }
                catch
                {
                }
            }


        }

        getCRNewComment();

    }

    protected void lnkUnansweredQuestions_Click(object sender, EventArgs e)
    {
        Session["filter"] = "Unanswered";
        Session["filterunread"] = "";
        Response.Redirect("tasks.aspx");
    }

    protected void lnkPr0Task_Click(object sender, EventArgs e)
    {

        Session["filter"] = "Immediate";
        Session["filterunread"] = "";
        Response.Redirect("tasks.aspx");
    }

    protected void lnkHighestTasks_Click(object sender, EventArgs e)
    {
        Session["filter"] = "HighestTasks";
        Session["filterunread"] = "";
        Response.Redirect("tasks.aspx");
    }

    protected void lnkHighTasks_Click(object sender, EventArgs e)
    {
        Session["filter"] = "HighTasks";
        Session["filterunread"] = "";
        Response.Redirect("tasks.aspx");
    }

    void getUnasweredQuestions()
    {
//////        sql = @"select task_comments.* from task_comments,tasks 
//////                where tasks.task_id = task_comments.task_id and tasks.deleted <> 1 and tasks.status <> 'closed' and task_comments.deleted <> 1 and qflag=1 ";

//////        sql += " and assigned_to_user = '" + Session["admin"] + "' ";

        sql = @"select task_comments.* from task_comments,tasks 
                where tasks.task_id = task_comments.task_id and tasks.deleted <> 1 and tasks.status <> 'closed' and task_comments.deleted <> 1 and qflag=1 ";

        sql += " and task_comments.QuesTo = '" + Session["admin"] + "' ";


        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            lnkUnansweredQuestions.Text = "(" + ds.Tables[0].Rows.Count + ")";
            unansweredque.Visible = true;
        }
        else
        {
            lnkUnansweredQuestions.Text = "(0)";
            unansweredque.Visible = false;
        }
    }

    void getPR0Tasks()
    {
        sql = @"select tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '0 - IMMEDIATE' ";

        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            sql += " and status <> 'closed' and assigned_to_user = '" + Session["admin"] + "' ";
        }
        else
        {
            sql += " and assigned_to_user = '" + Session["admin"] + "' ";
        }

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            lnkPr0Task.Text = "(" + ds.Tables[0].Rows.Count + ")";
            immediatetask.Visible = true;
        }
        else
        {
            lnkPr0Task.Text = "(0)";
            immediatetask.Visible = false;
        }
    }

    void getPR1aTasks()
    {
        sql = @"select tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '1a - DO NOW' ";

        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            sql += " and status <> 'closed' and assigned_to_user = '" + Session["admin"] + "' ";
        }
        else
        {
            sql += " and assigned_to_user = '" + Session["admin"] + "' ";
        }

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            lnkHighestTasks.Text = "(" + ds.Tables[0].Rows.Count + ")";
            highestTasks.Visible = true;
        }
        else
        {
            lnkHighestTasks.Text = "(0)";
            highestTasks.Visible = false;
        }
    }

    void getPR1bTasks()
    {
        sql = @"select tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '1b - high' ";

        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            sql += " and status <> 'closed' and assigned_to_user = '" + Session["admin"] + "' ";
        }
        else
        {
            sql += " and assigned_to_user = '" + Session["admin"] + "' ";
        }

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            lnkHighTasks.Text = "(" + ds.Tables[0].Rows.Count + ")";
            highTasks.Visible = true;
        }
        else
        {
            lnkHighTasks.Text = "(0)";
            highTasks.Visible = false;
        }
    }
    void getTskNewComment()
    {

       


        sql = "select tasks.* from tasks where task_id in(select task_comments.task_id from task_comments where tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 )";
      

       sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            LnkTskNewcomment.Text = "(" + ds.Tables[0].Rows.Count + ")";
            LnkMakeTaskcommentRead.Text = "(" + "-" + ")";
            LnkMakeTaskcommentRead.Visible = true;
            TskNewComment.Visible = true;
        }
        else
        {
            LnkTskNewcomment.Text = "(0)";
            TskNewComment.Visible = false;
            LnkMakeTaskcommentRead.Visible = false;
        }
    }
    protected void LnkTskNewcomment_Click(object sender, EventArgs e)
    {

        Session["filterunread"] = "TaskNewComment";
        ////Session["filter"] = "Immediate";
        Response.Redirect("tasks.aspx");


    }
    protected void LnkMakeTaskcommentRead_Click(object sender, EventArgs e)
    {
        sql = @"select *  from task_comments where task_id in (select task_id  from tasks where tasks.task_id = task_comments.task_id and  tasks.assigned_to_user = '" + Session["admin"].ToString() + "'  and tasks.deleted <> 1 and tasks.status <> 'closed') and task_comments.tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 ";
        /// sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {

                    //////string sqlgetcmntId = "select tc_id from task_comments where task_id ='" + ds.Tables[0].Rows[i]["task_id"].ToString() + "'";
                    //////object TskcmntID = DatabaseHelper.executeScalar(sqlgetcmntId);


                    //////if (TskcmntID!=null)
                    //////{
                    string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                    sqlinsert += " values(" + ds.Tables[0].Rows[i]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                    int intResult = DatabaseHelper.executeNonQuery(sqlinsert);
                    string sqlinsert2 = "insert into read_task([task_id],[username]) ";
                    sqlinsert2 += " values(" + ds.Tables[0].Rows[i]["task_id"].ToString() + ",'" + Session["admin"].ToString() + "')";
                    int intResult2 = DatabaseHelper.executeNonQuery(sqlinsert2);
                    //////}
                }
                catch
                {
                }
            }


        }

        // getTskNewComment();
        Response.Redirect("tasks.aspx", false);
    }
    void getAllTskNewComment()
    {

        sql = "select tasks.* from tasks where task_id in(select task_comments.task_id from task_comments where tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 )";

        ///sql += "and tasks.deleted <> 1 and tasks.status <> 'closed'";

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
            {
                LnkAllTsknewComment.Text = "(0)";
                SpnAllTaskNewComment.Visible = false;
                LnkMakeAllTaskCmmntRead.Visible = false;
                LnkAllTsknewComment.Visible = false;

            }
            else
            {


                LnkAllTsknewComment.Text = "(" + ds.Tables[0].Rows.Count + ")";
                LnkMakeAllTaskCmmntRead.Text = "(" + "-" + ")";
                LnkMakeAllTaskCmmntRead.Visible = true;
                SpnAllTaskNewComment.Visible = true;

            }
        }
        else
        {
            LnkAllTsknewComment.Text = "(0)";
            SpnAllTaskNewComment.Visible = false;
            LnkMakeAllTaskCmmntRead.Visible = false;
            LnkAllTsknewComment.Visible = false;
        }


    }
    protected void LnkAllTsknewComment_Click(object sender, EventArgs e)
    {
        Session["filterunread"] = "AllTaskNewComment";
        ////Session["filter"] = "Immediate";
        Response.Redirect("tasks.aspx");
    }
    protected void LnkMakeAllTaskCmmntRead_Click(object sender, EventArgs e)
    {


        sql = @"select *  from task_comments where task_id in (select task_id  from tasks where tasks.task_id = task_comments.task_id) and task_comments.tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 ";
        /// sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {

                    string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                    sqlinsert += " values(" + ds.Tables[0].Rows[i]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                    int intResult = DatabaseHelper.executeNonQuery(sqlinsert);
                    string sqlinsert2 = "insert into read_task([task_id],[username]) ";
                    sqlinsert2 += " values(" + ds.Tables[0].Rows[i]["task_id"].ToString() + ",'" + Session["admin"].ToString() + "')";
                    int intResult2 = DatabaseHelper.executeNonQuery(sqlinsert2);
                    //////}
                }
                catch
                {
                }
            }


        }

        getAllTskNewComment();


    }
    protected void btnCreate_Click(object sender, EventArgs e)
    {
        if (txtName.Text.Trim() == "")
        {
            divMessage.InnerHtml = "Please enter category name.";
            return;
        }

        if (btnCreate.Text == "Create")
        {
            object objResult = DatabaseHelper.executeScalar("select count(*) from categories where [category_name]='" + txtName.Text.Trim() + "'");

            if (objResult.ToString() != "0")
            {
                divMessage.InnerHtml = "Category name alredy exist.";
                return;
            }


            string strSql = " insert into categories([category_name],[sort_seq],[default_selection]) ";
            strSql += " values('" + txtName.Text.Trim().Replace("'", "''") + "'," + ((txtSortSequece.Text.Trim()!="")? txtSortSequece.Text.Trim() : "0" ) + ",'" + (chkDefault.Checked ? "Y" : "N") +"')";

            int intResult = DatabaseHelper.executeNonQuery(strSql);

            if (intResult != 0)
            {
                divMessage.InnerHtml = "Category was created.";
                clearControls();
            }
            else
            {
                divMessage.InnerHtml = "Category was not created.";
            }
        }
        else if (btnCreate.Text == "Edit")
        {
            object objResult = DatabaseHelper.executeScalar("select count(*) from categories where [category_name]='" + txtName.Text.Trim() + "' and [category_id]!=" + lblId.Text);

            if (objResult.ToString() != "0")
            {
                divMessage.InnerHtml = "Category name alredy exist.";
                return;
            }

            string strSql = " update categories ";
            strSql += " set [category_name]= '" + txtName.Text.Trim().Replace("'", "''") + "',";
            strSql += " [sort_seq] = "+ txtSortSequece.Text.Trim() + ",";
            strSql += " [default_selection] = '" + (chkDefault.Checked ? "Y" : "N") +"'" ;
            strSql += " where category_id = " + lblId.Text.Trim();

            int intResult = DatabaseHelper.executeNonQuery(strSql);

            if (intResult != 0)
            {
                if (prevName.Value != txtName.Text.Trim().Replace("'", "''"))
                {
                    strSql = "update tasks set category = '" + txtName.Text.Trim().Replace("'", "''") +"' ";
                    strSql+= " where category ='"+ prevName.Value.Replace("'","''") +"'";
                    intResult = DatabaseHelper.executeNonQuery(strSql);
                }

                divMessage.InnerHtml = "Category was updated.";
            }
            else
            {
                divMessage.InnerHtml = "Category was not updated.";
            }
        }
    }

    private void bindData()
    {
        string strSql = "select * from categories";

        DataSet ds = DatabaseHelper.getDataset(strSql);

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                DataGrid1.DataSource = ds.Tables[0];
                PagedDataSource objds = new PagedDataSource();
                objds.DataSource = ds.Tables[0].DefaultView;

                LblRecordsno.Text = objds.DataSourceCount.ToString();
                DataGrid1.DataBind();
                DataGrid1.Visible = true;
                lnkBack.Visible = false;
            }
            else
            {
                LblRecordsno.Text = "0";
                DataGrid1.DataSource = null;
                DataGrid1.DataBind();
                DataGrid1.Visible = false;
                lnkBack.Visible = false;
                divMessage.InnerHtml = "No category in the database.";
            }
        }
        else
        {
           


            DataGrid1.DataSource = null;
            DataGrid1.DataBind();
            DataGrid1.Visible = false;
            lnkBack.Visible = false;
            divMessage.InnerHtml = "No category in the database..";
        }
    }
    protected void lnkAddNew_Click(object sender, EventArgs e)
    {
        clearControls();
        DivEntry.Visible=true;
        DataGrid1.Visible=false;
        lnkBack.Visible = true;
        divMessage.InnerHtml = "";
    }

    protected void DataGrid1_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "edit")
        {
            string strSql = "select * from categories where category_id=" + e.Item.Cells[0].Text;
            DataSet ds = DatabaseHelper.getDataset(strSql);

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    lblId.Text = ds.Tables[0].Rows[0]["category_id"].ToString();
                    txtName.Text = ds.Tables[0].Rows[0]["category_name"].ToString();
                    prevName.Value = ds.Tables[0].Rows[0]["category_name"].ToString();
                    txtSortSequece.Text = ds.Tables[0].Rows[0]["sort_seq"].ToString();
                    chkDefault.Checked = ds.Tables[0].Rows[0]["default_selection"].ToString() == "Y" ? true : false;
                    btnCreate.Text = "Edit";
                    DivEntry.Visible = true;
                    DataGrid1.Visible = false;
                    lnkBack.Visible = true;
                    divMessage.InnerHtml = "";
                }
            }            
        }
        else if (e.CommandName == "delete")
        {
            object objResult = DatabaseHelper.executeScalar("select count(*) from tasks where category=" + e.Item.Cells[0].Text);

            if (objResult.ToString() == "0")
            {
                string strSql = "delete from categories where category_id=" + e.Item.Cells[0].Text;
                int intResult = DatabaseHelper.executeNonQuery(strSql);

                if (intResult != 0)
                {
                    divMessage.InnerHtml = "Category was deleted.";
                    bindData();
                }
                else
                {
                    divMessage.InnerHtml = "Category was not deleted.";
                    bindData();
                }
            }
            else
            {
                divMessage.InnerHtml = "You can not delete this category. It is been refered by another table.";
            }
        }
    }

    protected void DataGrid1_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton deleteButton = (LinkButton)e.Item.Cells[5].Controls[0];
            
            //We can now add the onclick event handler
            deleteButton.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete this category?')");
        }
    }

    private void clearControls()
    {
        lblId.Text = "New";
        txtName.Text = "";
        txtSortSequece.Text = "";
        chkDefault.Checked = false;
        btnCreate.Text = "Create";
    }

    protected void lnkBack_Click(object sender, EventArgs e)
    {
        clearControls();
        DivEntry.Visible = false;
        bindData();
    }
}
