<%@ Page Language="C#" AutoEventWireup="true" CodeFile="change_password.aspx.cs"
    Inherits="Admin_change_password" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc2" %>
<%--<%@ Register Src="Header.ascx" TagName="Header" TagPrefix="uc1" %>--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Change password</title>
    <link href= "CSS/stylesheet1.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <form id="form1" runat="server">
      <asp:ScriptManager ID="ScriptManager1" EnablePageMethods="true" runat="server">
</asp:ScriptManager>
    <div id="wrapper">
  <div class="centralizer">
    <div class="authentication">
      <div>
        <div class="content" style="width:729px; height:300px;">
          <div class="logo_login"> 
            <!-- <a href="#" class="logo_text">estateCRM</a>--><img src="../images/esg-desklogo.png" width="160" height="35" style="padding-top:10px;" />
            </div>
            <div style="float:right; margin-top:-75px; font-size:13px;"><a href="../login.aspx"> Client login</a>&nbsp;&nbsp;<a href="login.aspx">Support login</a></div>
          <h4 class="ctx-clr"> Change request system </h4>
          <div class="message warning"> </div>
          <div class="form">
            <div class="field"> <span style="color: Red"> <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtCurrentPassword"
                                                                    ErrorMessage="Please enter old password.">*</asp:RequiredFieldValidator></span>Current password: <asp:TextBox ID="txtCurrentPassword" runat="server" Width="315px" TextMode="Password"></asp:TextBox>
                                                                     <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" TargetControlID="RequiredFieldValidator2">
                                                                </cc1:ValidatorCalloutExtender>
                                                                </div>
              
              
            <div class="field" style="float:right;"> <span style="color: Red"><asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtNewPassword"
                                                                    ErrorMessage="Please enter new password.">*</asp:RequiredFieldValidator></span> New password:
                 <asp:TextBox ID="txtNewPassword" runat="server" Width="315px" TextMode="Password"></asp:TextBox>
                                                               
                                                                <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server" TargetControlID="RequiredFieldValidator3">
                                                                </cc1:ValidatorCalloutExtender> </div>
              
            <div class="field inline" style="width:50%; font-size:20px; margin-top: 20px;" >
              <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtConfirmPassword"
                                                                    ErrorMessage="Please enter password to confirm new password.">*</asp:RequiredFieldValidator>  
<asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="The confirm password entered does not match the new password entered."
                                                                    ControlToCompare="txtNewPassword" ControlToValidate="txtConfirmPassword">*</asp:CompareValidator> 
              Confirm new password:                                 <asp:TextBox ID="txtConfirmPassword" runat="server"  style="margin-top:-20px; Width:315px;" TextMode="Password"></asp:TextBox>
                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator1">
                                                                </cc1:ValidatorCalloutExtender>
                                                                <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="server" TargetControlID="CompareValidator1">
                                                                </cc1:ValidatorCalloutExtender>                       
            </div>
            
            
           <%-- <div class="field"> <span style="color: Red">*</span> Client ID:
              <input name="txtClientID" id="txtClientID" onkeypress="return isNumberKey(event);" style="width: 315px;" type="text">
              <span id="Span2" title="Enter client ID" style="color:Red;display:none;"></span> </div>--%>
              
              
            <div class="buttons" style="float:right; margin-top:50px;">
              <ul>
                <li>
                  <asp:Button ID="btnChangePassword" runat="server" Text="Change password" OnClick="btnChangePassword_Click"
                                        CssClass="loginbutton" />
              
                <!--  <input name="btnlogin" value="Login" onclick='javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("btnlogin", "", true, "", "", false, false))' id="btnlogin" class="loginbutton" type="submit">-->
                </li>
              </ul>
            </div>
            
            
            <footer>
             
               <div id="divMessage" style="color: Red" runat="server" visible="true">
                                    </div>
            </footer>
            
           
            
          </div>
        </div>
        
      </div>
    </div>
  </div>
</div>


</div>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    <div id="wrapper" style="display:none;">
       
        <div id="Content">
            <table>
                <tr>
                    <td align="left" valign="top" style="width: 434px">
                        <table style="width: 424px">
                            <tr>
                                <td align="left" class="titleText" valign="top" style="width: 424px; display: none;">
                                    Change password
                                </td>
                            </tr>
                            <tr>
                                <td height="24" align="left" valign="middle" style="width: 424px">
                                    <span style="height: 10px">To reset your password, provide your current password.
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 424px">
                                    <table width="424" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="left" valign="top">
                                                <img height="16" src="../images/loginBox_top.png" width="420" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="background: url(../images/loginBox_mid.png) repeat-y left top;">
                                                <div style="text-align: center">
                                                    <table width="100%" border="0" align="center" cellpadding="8" cellspacing="0">
                                                        <tr>
                                                            <td width="5" align="left" valign="top" class="whitetext1">&nbsp;
                                                                
                                                            </td>
                                                            <td width="165" align="left" valign="top" class="whitetext1">
                                                                Current password:
                                                            </td>
                                                            <td align="left" valign="top">
                                                               
                                                                &nbsp;&nbsp;&nbsp;
                                                               
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" class="whitetext1">&nbsp;
                                                                
                                                            </td>
                                                            <td align="left" valign="top" class="whitetext1">
                                                                New password:
                                                            </td>
                                                            <td align="left" valign="top">
                                                              
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" class="whitetext1">&nbsp;
                                                                
                                                            </td>
                                                            <td align="left" valign="top" class="whitetext1">
                                                                Confirm new password:
                                                            </td>
                                                            <td align="left" valign="top">
                                                  
                                                               
                                                               
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 100px">
                                                <img height="16" src="../images/loginBox_btm.png" width="420" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <hr />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 424px">
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                   
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 424px">
                                   
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
   
    </form>
</body>
</html>
