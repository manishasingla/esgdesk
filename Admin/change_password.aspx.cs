using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Admin_change_password : System.Web.UI.Page
{
    string sql = "";
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = "change_password.aspx";
            Response.Redirect("login.aspx");
            return;
        }
       
        divMessage.InnerHtml = "";
    }
    protected void btnChangePassword_Click(object sender, EventArgs e)
    {
        DataSet ds = DatabaseHelper.getDataset("select * from users where username='" + Session["admin"].ToString().Replace("'", "''") + "'");

        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            if (txtCurrentPassword.Text.ToString() != ds.Tables[0].Rows[0]["password"].ToString())
            {
                divMessage.InnerHtml = "The current password you entered is incorrect.";
                txtCurrentPassword.Focus();
                return;
            }
            else
            {
                int result = 0;

                sql = @"Update users Set [Password]='" + txtNewPassword.Text.Trim().Replace("'", "''") +
                       "' Where [username]='" + Session["admin"].ToString().Replace("'", "''") + "'";

                result = DatabaseHelper.executeNonQuery(sql);

                if (result > 0)
                {
                    bool flag = DatabaseHelper.sendEmailTasks(ds.Tables[0].Rows[0]["email"].ToString(), "Password details from " + ConfigurationManager.AppSettings["CompanyName"].ToString(), generateEmail(ds));
                    
                    //flag = DatabaseHelper.sendEmailChangeRequest(ConfigurationManager.AppSettings["ToEmail1"].ToString(), "Password details from " + ConfigurationManager.AppSettings["CompanyName"].ToString(), generateEmail(ds));
                    //flag = DatabaseHelper.sendEmailChangeRequest(ConfigurationManager.AppSettings["ToEmail1"].ToString(), "Password details from " + ConfigurationManager.AppSettings["CompanyName"].ToString(), generateEmail(ds));

                    clearControls();
                    divMessage.InnerHtml = "Your new password has been saved.";
                }
                else
                {
                    divMessage.InnerHtml = "Your new password has not been saved at this time. Please try again...";
                }
            }
        }
    }

    private void clearControls()
    {
        txtConfirmPassword.Text = "";
        txtNewPassword.Text = "";
        txtConfirmPassword.Text = "";
    }

    private string generateEmail(DataSet ds)
    {
        string strBody = "";

        strBody += "<br>";
        strBody += "Dear " + ds.Tables[0].Rows[0]["firstname"].ToString() + " " + ds.Tables[0].Rows[0]["lastname"].ToString() + ",<br><br>";
        strBody += "You have changed your password on \"" + ConfigurationManager.AppSettings["CompanyName"].ToString() + "\".<br><br>";
        strBody += "Your login details are as follows:<br>";
        strBody += "Username: " + Session["admin"].ToString() + "<br>";
        strBody += "Password: " + txtNewPassword.Text.Trim() + "<br><br>";
        strBody += "<a href=\"" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/login.aspx\">Click here to login</a><br><br>";
        strBody += "-------------------------------------------<br><br>";
        strBody += ConfigurationManager.AppSettings["CompanyName"].ToString() + "<br>";
        strBody += ConfigurationManager.AppSettings["WebAddress"].ToString() + "<br>";

        return strBody;
    }
}
