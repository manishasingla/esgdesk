<%@ Page Language="C#" AutoEventWireup="true" CodeFile="client_details.aspx.cs" Inherits="Admin_client_details" %>

<%@ Register Src="Notifications.ascx" TagName="Notifications" TagPrefix="uc4" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Client details</title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
     table#DataGrid1_ctl00 tr th a
     {
      display:block;
      text-decoration:none;
      margin:0;
      padding:0;
      width:100%;
      height:100%;
     }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="wrapper">
            <div>
                <uc2:Admin_Header ID="Admin_Header1" runat="server" />
                <div id="Content">
                    <div id="Content_98">
                        <div class="titleText" style="margin: 10px 0 10px 0; display: none;">
                            Registered clients&nbsp;|&nbsp;</div>
                        <div>
                            <uc4:Notifications ID="Notifications" runat="server" />
                        </div>
                        <div style="float: right; width: 100%; padding: 5px 0px; background: #ebebeb;" align="right">
                            <span class="text_default" id="divLinks" runat="server" style="float: left;"><a href="edit_client.aspx">
                                &nbsp;Register new client</a> </span><b>Records: </b>
                            <asp:Label ID="LblRecordsno" runat="server" Text=""></asp:Label>&nbsp;</div>
                        <div style="clear: both">
                        </div>
                        <div id="divMessage" runat="server" style="color: Red; font-weight: bold;">
                        </div>
                        <div>
                            <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>--%>
                                    <%--<asp:DataGrid ID="DataGrid1" runat="server" Width="100%" AutoGenerateColumns="False"
                                        AllowPaging="True" AllowSorting="True" CellPadding="3" OnPageIndexChanged="DataGrid1_PageIndexChanged"
                                        PageSize="100" OnSortCommand="DataGrid1_SortCommand" OnItemDataBound="DataGrid1_ItemDataBound"
                                        OnItemCommand="DataGrid1_ItemCommand">
                                        <PagerStyle NextPageText="Next" PrevPageText="Prev" />
                                        <HeaderStyle Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False"
                                            HorizontalAlign="Center" VerticalAlign="Top" BackColor="#282C5F" CssClass="tblTitle1" />
                                        <Columns>
                                            <asp:BoundColumn DataField="RegId" HeaderText="RegId" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Email" HeaderText="Email" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="FirstName" HeaderText="Firstname" SortExpression="FirstName">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="Surname" HeaderText="Surname" SortExpression="Surname"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="UserName" HeaderText="Username" SortExpression="UserName">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="Password" HeaderText="Password" SortExpression="Password">
                                                <HeaderStyle />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="CompanyName" HeaderText="Company name" SortExpression="CompanyName">
                                                <HeaderStyle />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="Phone" HeaderText="Phone" SortExpression="Phone"></asp:BoundColumn>
                                            <asp:TemplateColumn HeaderText="Email" SortExpression="Email">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEmail" runat="server" Width="100px" CssClass="wordwrap"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="WebSite" HeaderText="Website" SortExpression="WebSite">
                                                <HeaderStyle Width="50px" Wrap="true" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="Subscription" HeaderText="Subscription" SortExpression="Subscription">
                                                <HeaderStyle />
                                            </asp:BoundColumn>
                                            <asp:TemplateColumn HeaderText="Show SMP" HeaderStyle-Width="50px">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkShowSmp" Enabled="false" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Allow chat" HeaderStyle-Width="50px">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkAllowChat" Enabled="false" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Allow remote desktop" HeaderStyle-Width="50px">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkAllowRemoteDesktop" Enabled="false" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="RegDate" HeaderText="Registered Date" SortExpression="RegDate">
                                            </asp:BoundColumn>
                                            <asp:HyperLinkColumn DataNavigateUrlField="RegId" DataNavigateUrlFormatString="edit_client.aspx?id={0}"
                                                HeaderText="Edit" NavigateUrl="edit_client.aspx" Text="edit"></asp:HyperLinkColumn>
                                            <asp:TemplateColumn HeaderText="Delete">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkDelete" runat="server" CommandName="delete">delete</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Top" />
                                    </asp:DataGrid>--%>
                                    
                                    
                                   
                                    
                                    <telerik:RadGrid ID="DataGrid1" runat="server" Width="100%" AutoGenerateColumns="False"
                                        AllowPaging="True" AllowSorting="True" CellPadding="3" OnPageIndexChanged="DataGrid1_PageIndexChanged"
                                        OnSortCommand="DataGrid1_SortCommand" PageSize="100" OnItemCommand="DataGrid1_ItemCommand" OnItemDataBound="DataGrid1_ItemDataBound">
                                        <PagerStyle Mode="NextPrevAndNumeric" />
                                        <MasterTableView PagerStyle-AlwaysVisible="true">
                                       
                                            <Columns>
                                                <%--0--%>
                                                <telerik:GridTemplateColumn HeaderText="RegId" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblregid" runat="server" Text='<% #Bind("RegId")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <%--1--%>
                                                <telerik:GridTemplateColumn HeaderText="Email" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblemailN" runat="server" Text='<%#bind("Email") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <%--2--%>
                                                <telerik:GridTemplateColumn HeaderText="Firstname" SortExpression="FirstName">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblfirstname" runat="server" Text='<%#Bind("FirstName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <%--3--%>
                                                <telerik:GridTemplateColumn HeaderText="Surname" SortExpression="Surname">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblsurname" runat="server" Text='<%#bind("Surname") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <%--4--%>
                                                <telerik:GridTemplateColumn HeaderText="Username" SortExpression="UserName">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblusername" runat="server" Text='<%#Bind("UserName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <%--5--%>
                                                <%--<telerik:GridTemplateColumn HeaderText="Password" SortExpression="Password">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblpassword" runat="server" Text='<%#bind("Password") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>--%>
                                                <%--6--%>
                                                <telerik:GridTemplateColumn HeaderText="Company name" SortExpression="CompanyName">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblcompanyname" runat="server" Text='<%#Bind("CompanyName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <%--7--%>
                                                <telerik:GridTemplateColumn HeaderText="Phone" SortExpression="Phone">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblphone" runat="server" Text='<%#Bind("Phone") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <%--8--%>
                                                <telerik:GridTemplateColumn HeaderText="Email" SortExpression="Email">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEmail" runat="server" Width="100px" CssClass="wordwrap"></asp:Label>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <%--9--%>
                                                <telerik:GridTemplateColumn HeaderText="Website" SortExpression="WebSite">
                                                    <HeaderStyle Width="50px" Wrap="true" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblwebsite" runat="server" Text='<%#Bind("WebSite") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <%--10--%>
                                                <telerik:GridTemplateColumn HeaderText="Subscription" SortExpression="Subscription">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblsubscription" runat="server" Text='<%#Bind("Subscription") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <%--11--%>
                                                <telerik:GridTemplateColumn HeaderText="Show SMP" HeaderStyle-Width="50px">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkShowSmp" Enabled="false" runat="server" />
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <%--12--%>
                                                <telerik:GridTemplateColumn HeaderText="Allow chat" HeaderStyle-Width="50px">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkAllowChat" Enabled="false" runat="server" />
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <%--13--%>
                                                <telerik:GridTemplateColumn HeaderText="Allow remote desktop" HeaderStyle-Width="50px">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkAllowRemoteDesktop" Enabled="false" runat="server" />
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <%--14--%>
                                                <telerik:GridTemplateColumn HeaderText="Registered Date" SortExpression="RegDate">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblregdate" runat="server" Text='<%#Bind("RegDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <%--15--%>
                                                 <telerik:GridTemplateColumn HeaderText="Password" SortExpression="Password">
                                                    <ItemTemplate>
                                                        
                                                        <asp:Button id="btnsendPass" runat="server"  Text="Email password" OnClick="btnsendPass_OnClick"   />
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                
                                                <%--<telerik:GridButtonColumn  id="btnsendPass1" runat="server"  Text="Password" OnClick="btnsendPass1_OnClick"></telerik:GridButtonColumn>--%>
                                                <%--15--%>
                                                <telerik:GridHyperLinkColumn DataNavigateUrlFields="RegId" DataNavigateUrlFormatString="edit_client.aspx?id={0}"
                                                    HeaderText="Edit" NavigateUrl="edit_client.aspx" Text="edit">
                                                </telerik:GridHyperLinkColumn>
                                                <%--16--%>
                                                <telerik:GridTemplateColumn HeaderText="Delete">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkDelete" runat="server" CommandName="delete">delete</asp:LinkButton>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                            </Columns>
                                        </MasterTableView>
                                    </telerik:RadGrid>
                                    
                                <%--</ContentTemplate>
                            </asp:UpdatePanel>--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       
      <%--   <asp:Button id="Test" Text="example" runat="server" onclick="Test_Click"/>--%>
        <uc1:footer ID="Footer1" runat="server" />
        <asp:Label ID="lblOrderBy" runat="server" Text="" Visible="false"></asp:Label>
    </form>
</body>
</html>
