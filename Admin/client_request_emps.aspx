<%@ Page Language="C#" AutoEventWireup="true" CodeFile="client_request_emps.aspx.cs" Inherits="Admin_client_request_emps" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="Notifications.ascx" TagName="Notifications" TagPrefix="uc4" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Src="Header.ascx" TagName="Header" TagPrefix="uc3" %>
<%@ Register Src="footer.ascx" TagName="footer" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    
    <title>Client request</title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../css/MenuMatic.css" type="text/css" media="screen"
        charset="utf-8" />
    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
    <link href="SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
   <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript" language="javascript">
function confirm()
{
  if (confirm ==true)
   var btnmark = document.getElementById("LnkMakecommentRead");
   btnmark.click();  
   else
   return false;
}
    </script>

    <script type="text/javascript">
        function get_(div_) {

            div_ = div_.id + "1";

            document.getElementById(div_).style.display = "block";
        }
        function get_1(div_) {

            div_ = div_.id + "1";

            document.getElementById(div_).style.display = "none";
        }
    </script>

    <script type="text/javascript" language="javascript">
        function EnablePRlink() {
            var objPR = document.getElementById("DrpMultipleTaskPR").value;

            if (objPR != "6") {

                document.getElementById("LnkBtnToChngPR").disabled = false;

                // document.getElementById("LnkChngAssingedTo").disabled = "disabled";
                document.getElementById("LnkChngStatus").disabled = "disabled";


            }
            else {

                document.getElementById("LnkBtnToChngPR").disabled = "disabled";

                document.getElementById("LnkChngStatus").disabled = "disabled";

            }

            return;

        }

        function EnableStatuslink() {
            var objStatus = document.getElementById("DrpMultipleTaskStatus").value;

            if (objStatus != "0") {

                document.getElementById("LnkBtnToChngPR").disabled = "disabled";

                document.getElementById("LnkChngStatus").disabled = false;

            }
            else {

                document.getElementById("LnkBtnToChngPR").disabled = "disabled";

                document.getElementById("LnkChngStatus").disabled = "disabled";

            }
            return;

        }

        //function to select all checkbox of DataGrid1
        function setCheckAll(obj) {
            var chkGridcontrols = document.getElementById("<%=DataGrid1.ClientID%>").getElementsByTagName("input");
            for (var i = 0; i < chkGridcontrols.length; i++) {
                chkGridcontrols[i].checked = obj.checked;
            }

            return true;
        }

        //function to uncheck 'select all' when any checkbox in DataGrid is unchecked
        function validate(obj) {

            var chkGridcontrols = document.getElementById("<%=DataGrid1.ClientID%>").getElementsByTagName("input");
            var chkheaderSelect = document.getElementById("chkheaderSelect");

                if (!obj.checked)
                    chkheaderSelect.checked = false;
            }

        //Validate Mark Button & Delete Button present inside Action area
        function validateonbtn(a) {
            var chkGridcontrols = document.getElementById("<%=DataGrid1.ClientID%>").getElementsByTagName("input");
            var count = 0;
            for (var i = 0; i < chkGridcontrols.length; i++) {
                if (chkGridcontrols[i].checked)
                    count = 1;
            }

            if (count > 0) {
                if (a == 0) {
                    if (!confirm("Are you sure you want to delete the selected CRs?"))
                        return false;
                    else
                        return true;
                }
                else if (a == 1) { }

            }
            else {
                alert("Please select at least one CR");
                return false;
            }
        }

    </script>
    </telerik:RadCodeBlock>
    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="slider.css" />

    <script type="text/javascript" src="slider.js"></script>
<style type="text/css">
    table#DataGrid1_ctl00 tr th a
     {
      display:block;
      text-decoration:none;
      margin:0;
      padding:0;
      width:100%;
      height:100%;
      back-color:red
      }
  </style>
  <style type="text/css">
  div.RadToolTip_Default table.rtWrapper td.rtWrapperContent
   {
    background-color: #FEFFB3 !important;
   }
   
   table#DataGrid1_ctl00 tr td 
     {
      border: 1px solid #00A651;
     }
     table#DataGrid1_ctl00 tr th 
     {
      border: 1px solid #00A651;
     }
   </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="wrapper">
            <uc2:Admin_Header ID="Admin_Header1" runat="server" />
            <div id="Content">
                <asp:UpdateProgress ID="updateprogress1" runat="server">
                    <ProgressTemplate>
                        <div id="processmessage" class="processMessage">
                            <img alt="loading" src="images/ajax-loader.gif" style="width: 80px; height: 80px" />
                            <span style="color: black; font-weight: bold; font-size: 18px; vertical-align: super;">
                                Please wait... </span>
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <div id="Content_98">
                            <div class="titleText" style="margin: 10px 0 10px 0; display: none;">
                                Change requests</div>
                            <div style="text-align: left">
                                <div style="padding: 0px 0 0px 5px; margin: 2px  0 -2px 0;">
                                    <table>
                                        <tr>
                                            <td class="function-menu">
                                                <span style="cursor:pointer;" onmousedown="slideContent('section-1')">Filter</span>
                                            </td>
                                            <td style="margin-left: 5px;" class="function-menu">
                                                <span style="cursor:pointer;" onmousedown="slideContent('section-2')">Action</span>
                                            </td>
                                            <td style="text-align: right;">
                                                <span id="TopFilterLable" runat="server" class="topfilter"></span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div>
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" onclick="return TABLE1_onclick()"
                                        id="TABLE1">
                                        <tr>
                                            <td style="border-radius: 9px 9px 9px 9px; height: 62px;" bgcolor="#3E3F3D" width="100%">
                                                <div class="slider">
                                                    <div class="slidercontent" id="slider">
                                                        <div id="section-1" class="section upper" style="margin-top: 0px;">
                                                            <table border="0" cellpadding="0" width="100%" cellspacing="0">
                                                                <tr>
                                                                    <td width="45" valign="top" style="padding-top: 20px;">
                                                                        <b style="color: #ffffff;">Filter:</b>
                                                                    </td>
                                                                    <td valign="top" style="color: White; font-size: 14px">
                                                                        Change added by<br />
                                                                        <asp:DropDownList ID="drpChangeBy" runat="server" Width="170px" AutoPostBack="True"
                                                                            OnSelectedIndexChanged="drpChangeBy_SelectedIndexChanged" CssClass="filerDrpodown">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td valign="top" style="color: White; font-size: 14px">
                                                                        Status<br />
                                                                        <asp:DropDownList ID="drpStatus" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpStatus_SelectedIndexChanged"
                                                                            Width="170px" CssClass="filerDrpodown">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td valign="top" style="color: White; font-size: 14px">
                                                                        Priority<br />
                                                                        <asp:DropDownList ID="drpPriority" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpPriority_SelectedIndexChanged"
                                                                            Width="170px" CssClass="filerDrpodown">
                                                                            <asp:ListItem Value="6">[no priority]</asp:ListItem>
                                                                            <asp:ListItem Value="0">0 - IMMEDIATE</asp:ListItem>
                                                                            <asp:ListItem Value="1">1 - highest</asp:ListItem>
                                                                            <asp:ListItem Value="2">2</asp:ListItem>
                                                                            <asp:ListItem Value="3">3</asp:ListItem>
                                                                            <asp:ListItem Value="4">4</asp:ListItem>
                                                                            <asp:ListItem Value="5">5 - lowest</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td valign="top" style="color: White; font-size: 14px" style="width: 200px">
                                                                        Company name<br />
                                                                        <asp:DropDownList ID="drpWebsiteUrl" runat="server" Width="200px" AutoPostBack="True"
                                                                            OnSelectedIndexChanged="drpWebsiteUrl_SelectedIndexChanged" CssClass="filerDrpodown">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td valign="top" style="color: White; font-size: 14px" style="width: 200px">
                                                                        Items<br />
                                                                        <asp:DropDownList ID="drpPerPage1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpPerPage1_SelectedIndexChanged">
                                                                            <asp:ListItem Value="20" Selected="True">20</asp:ListItem>
                                                                            <asp:ListItem Value="50">50</asp:ListItem>
                                                                            <asp:ListItem Value="100">100</asp:ListItem>
                                                                            <asp:ListItem Value="00">All</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td valign="bottom" class="whitetext2">
                                                                        &nbsp;&nbsp;<asp:Button ID="btnClearFilters" OnClick="btnClearFilters_Click" runat="server"
                                                                            CssClass="clearfilter" Text="Clear filters"></asp:Button>
                                                                    </td>
                                                                    <td valign="bottom" style="color: White; font-size: 14px">
                                                                        <asp:CheckBox ID="chkViewDeleted" runat="server" AutoPostBack="True" OnCheckedChanged="chkViewDeleted_CheckedChanged"
                                                                            Text="View deleted" />
                                                                    </td>
                                                                    <td valign="bottom" style="color: White; font-size: 14px">
                                                                        <asp:CheckBox ID="chkHideClosed" runat="server" AutoPostBack="True" OnCheckedChanged="chkHideClosed_CheckedChanged"
                                                                            Text="View closed" />
                                                                    </td>
                                                                    <td valign="bottom" style="color: #ffffff;">
                                                                        <b>Records: </b>
                                                                        <asp:Label ID="LblRecordsno" runat="server" Text=""></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div id="section-2" class="section upper" style="padding-top: 20px;">
                                                            <div id="DivMiltiselection" runat="server" style="padding-bottom: 7px" class="whitetext2">
                                                                <b style="color: #ffffff;">Action:</b>
                                                                <asp:LinkButton ID="LnkBtnToDeleteAll" runat="server" OnClick="LnkBtnToDeleteAll_Click"
                                                                    CssClass="whitetext2" OnClientClick="return validateonbtn(0);">Delete Selected</asp:LinkButton>&nbsp;&nbsp;
                                                                <asp:LinkButton ID="LnkBtnToMarkAsRead" runat="server" OnClick="LnkBtnToMarkAsRead_Click"
                                                                    CssClass="whitetext2" OnClientClick="return validateonbtn(1);">Mark read</asp:LinkButton>&nbsp;&nbsp;
                                                                <asp:Button ID="LnkBtnToChngPR" runat="server" BackColor="transparent" BorderStyle="none"
                                                                    ForeColor="#ffffff" Text="Change priorirty to" OnClick="LnkBtnToChngPR_Click" />
                                                                <asp:DropDownList ID="DrpMultipleTaskPR" runat="server" CssClass="filerDrpodown"
                                                                    AutoPostBack="true" Width="180px" OnSelectedIndexChanged="DrpMultipleTaskPR_SelectedIndexChanged">
                                                                    <asp:ListItem Value="6">[no priority]</asp:ListItem>
                                                                    <asp:ListItem Value="0">0 - IMMEDIATE</asp:ListItem>
                                                                    <asp:ListItem Value="1">1 - highest</asp:ListItem>
                                                                    <asp:ListItem Value="8">1c</asp:ListItem>
                                                                    <asp:ListItem Value="2">2</asp:ListItem>
                                                                    <asp:ListItem Value="3">3</asp:ListItem>
                                                                    <asp:ListItem Value="4">4</asp:ListItem>
                                                                    <asp:ListItem Value="5">5 - lowest</asp:ListItem>
                                                                </asp:DropDownList>
                                                                &nbsp;&nbsp;<asp:Button ID="LnkChngStatus" runat="server" BackColor="transparent"
                                                                    ForeColor="#ffffff" BorderStyle="none" Text="Change status to" OnClick="LnkChngStatus_Click" />
                                                                <asp:DropDownList ID="DrpMultipleTaskStatus" runat="server" CssClass="filerDrpodown"
                                                                    AutoPostBack="true" Width="180px" OnSelectedIndexChanged="DrpMultipleTaskStatus_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div style="float: left;">
                                <uc4:Notifications ID="Notifications" runat="server" />
                            </div>
                            <div style="clear: both">
                            </div>
                            <div style="float: left; width: 100%; padding: 5px 0px;">
                                <%--<a href="add_changeRequest.aspx">&nbsp Add change request</a>--%>
                            </div>
                            <div style="clear: both">
                            </div>
                            <div id="divMessage" runat="server" style="color: Red; font-weight: bold;">
                            </div>
                            <%--                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>--%>
                            <div style="font-size: 12px">
                                <%--*************************************************************************************************************************************************--%>
                                   <telerik:RadGrid ID="DataGrid1" runat="server" Width="100%" CssClass="tasktbl" AutoGenerateColumns="False"
                                    AllowPaging="false" AllowSorting="True" CellPadding="5" OnPageIndexChanged="DataGrid1_PageIndexChanged"  
                                    OnSortCommand="DataGrid1_SortCommand" OnItemCommand="DataGrid1_ItemCommand" OnItemDataBound="DataGrid1_ItemDataBound">
                                    <MasterTableView PagerStyle-AlwaysVisible="true">
                                    <columns>
                                    <%--0--%>
                                    <telerik:GridTemplateColumn HeaderText="ID" Visible="False" SortExpression="RequestId">
                                    <ItemTemplate>
                                    <asp:Label ID="lblRequestIdF" runat="server" Text='<%# bind("RequestId") %>' ></asp:Label>
                                    </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <%--<telerik:GridBoundColumn DataField="RequestId" HeaderText="ID" Visible="False" SortExpression="RequestId"></telerik:GridBoundColumn>--%>
                                    <%--1--%>
                                    <telerik:GridTemplateColumn>
                                       <HeaderTemplate>
                                        <%--Select all--%>
                                            <input id="chkheaderSelect" type="checkbox" onclick="return setCheckAll(this);" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" onclick ="return validate(this);" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <%--2--%>
                                    <telerik:GridTemplateColumn HeaderText="Short description" SortExpression="ShortDescr" >
                                    <HeaderStyle Width="300px" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblshrtDesc" runat="server" CssClass="wordwrap" Text='<%# bind("ShortDescr") %>'></asp:Label>
                                            <%--<div id="lblshrtDesc1" runat="server" style="display: none; position: absolute; background-color: #FEFFB3;
                                                width: 730px; border: solid 1px #333333; margin-top: 5px; padding-left: 3px">
                                                <asp:Label ID="LblLastcomment" runat="server" Text='<%# bind("ShowAllComment") %>'></asp:Label>
                                            </div>--%>
                                            <telerik:RadToolTip ID="RadToolTip1" BackColor="#FEFFB3" runat="server" OffsetX ="150" AutoCloseDelay="60000" HideEvent="LeaveTargetAndToolTip" OffsetY="10" TargetControlID="lblshrtDesc" Width="500px" RelativeTo="Element" Position="BottomLeft" ShowEvent="OnMouseOver"><%# DataBinder.Eval(Container, "DataItem.ShowAllComment")%></telerik:RadToolTip>
                                            <%--<ajax:HoverMenuExtender ID="hme2" runat="Server"  OffsetX ="100"  OffsetY="20" PopupControlID="lblshrtDesc1" PopupPosition="Bottom" TargetControlID="lblshrtDesc" />--%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <%--3--%>
                                    
                                    <telerik:GridTemplateColumn HeaderText="Read/Unread" >
                                    <HeaderStyle Width="128px"/>
                                    <ItemTemplate>
                                    <asp:LinkButton ID="lnkReadMark" runat="server" CssClass="wordwrap" CommandName="ReadMark">(Unread. Mark as read)</asp:LinkButton>
                                    </ItemTemplate> 
                                    </telerik:GridTemplateColumn>
                                    <%--4--%>
                                    <telerik:GridTemplateColumn HeaderText="Project name" SortExpression="ProjectName">
                                    <ItemTemplate>
                                    <a id="lblprojectname" runat="server" target="_blank" href='<%# "projectrequests.aspx?projectname="+ Eval("ProjectName") %>'><%# Eval("ProjectName")%></a>
                                    </ItemTemplate>
                                    </telerik:GridTemplateColumn> 
                                     <%--5--%>
                                    <telerik:GridTemplateColumn HeaderText="Status">
                                    <ItemTemplate>
                                    <asp:Label ID="lblstatus" runat="server" Text='<%# bind("Status") %>' ></asp:Label>
                                    </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <%--6--%>
                                    <telerik:GridTemplateColumn HeaderText="Priority" SortExpression="Priority">
                                     <ItemTemplate>
                                        <asp:Label ID="lblPriority" runat="server" Text='<%# bind("Priority") %>' ></asp:Label>
                                     </ItemTemplate> 
                                    </telerik:GridTemplateColumn>
                                    <%--7--%>
                                    <telerik:GridTemplateColumn HeaderText="Priority" SortExpression="ClientRequest" Visible="False">
                                       <ItemTemplate>
                                          <asp:Label ID="lblPriorityV" runat="server" Text='<%# bind("Priority") %>' ></asp:Label>
                                       </ItemTemplate> 
                                     </telerik:GridTemplateColumn>
                                   <%-- 8--%>
                                     <telerik:GridTemplateColumn HeaderText="Company name" SortExpression="CompanyName">
                                      <ItemTemplate>
                                          <asp:Label ID="lblCompanyName" runat="server" Text='<%# bind("CompanyName") %>' ></asp:Label>
                                       </ItemTemplate> 
                                     </telerik:GridTemplateColumn>
                                    <%--9--%>
                                    <%--<telerik:GridTemplateColumn HeaderText="Last updated by" SortExpression="LastUpdatedBy">--%>
                                     <telerik:GridTemplateColumn HeaderText="Updated by" SortExpression="LastUpdatedBy">
                                    <ItemTemplate>
                                          <asp:Label ID="lblLastUpdatedBy" runat="server" Text='<%# bind("LastUpdatedBy") %>' ></asp:Label>
                                       </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <%--10--%>
                                    <telerik:GridTemplateColumn HeaderText="Last updated" SortExpression="LastUpdatedOn">
                                    <ItemTemplate>
                                          <asp:Label ID="lblLastUpdatedOn" runat="server" Text='<%# bind("LastUpdatedOn") %>' ></asp:Label>
                                       </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <%--11--%>
                                    <telerik:GridTemplateColumn HeaderText="Change added by" SortExpression="AddedBy">
                                     <ItemTemplate>
                                          <asp:Label ID="lblAddedBy" runat="server" Text='<%# bind("AddedBy") %>' ></asp:Label>
                                     </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    
                                    <%--12--%>
                                    <%--<telerik:GridTemplateColumn HeaderText="Change added on" SortExpression="RequestDate">--%>
                                    <telerik:GridTemplateColumn HeaderText="Added on" SortExpression="RequestDate">
                                    <ItemTemplate>
                                            <asp:Label ID="lblChangeAddedOn" runat="server" Text='<%# bind("RequestDate") %>'></asp:Label>
                                    </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                     <%--13--%>
                                    <telerik:GridTemplateColumn HeaderText="Type">
                                    <HeaderStyle Width="66px" />
                                    <ItemTemplate>
                                            <asp:LinkButton ID="Supportype" runat="server" CommandName="SupprtLink">Support type</asp:LinkButton>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                     <%--14--%>
                                    <telerik:GridTemplateColumn HeaderText="ID" SortExpression="RequestId">
                                    <ItemTemplate>
                                            <asp:Label ID="lblRequestId" runat="server" Text='<%# bind("RequestId") %>'></asp:Label>
                                    </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <%--15--%>
                                   <%-- <telerik:GridHyperLinkColumn HeaderText="Edit" Text="edit" DataNavigateUrlFields="RequestId" DataNavigateUrlFormatString="edit_request.aspx?reqid={0}" NavigateUrl="edit_request.aspx" >
                                    
                                    </telerik:GridHyperLinkColumn>--%>
                                     </columns>
                                   </MasterTableView>
                                </telerik:RadGrid>
                                <%--*************************************************************************************************************************************************--%>
                            </div>
                           
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <uc1:footer ID="Footer1" runat="server" />
        </div>
        <asp:Label ID="lblOrderBy" runat="server" Text="" Visible="false"></asp:Label>
    </form>
</body>
</html>
