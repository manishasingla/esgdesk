using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.IO;

public partial class companyNotes : System.Web.UI.Page
{
    string company;
    string sql = "";
    string emailSubject = "";
    static ArrayList arrAllFHSCAtegories = new ArrayList();
    protected void Page_Load(object sender, EventArgs e)
    {
       /// btnote.Attributes.Add("onclick", "checkAnyChecked();");
        btnRemove.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete FHS notes of selected category?');");
        Page.RegisterClientScriptBlock("onload", "<script> top.window.resizeBy(screen.availWidth,screen.availHeight);</script>");
        LnkShowAllCat.Attributes.Add("onclick", "ShowAllCat();");
        LnkHideTabArea.Attributes.Add("onclick", "HideAllCat();");
        if (!Page.IsPostBack)
        {
          

           
            //if (Session["TaskComp"] == null || Session["TaskComp"].ToString().Trim() == "")
            //{

            //}
            if (Request.QueryString["CompName"] == null || Request.QueryString["CompName"].ToString() == "")
            {


            }
            else
            {

                hdnCategories.Value = "";
                //hdnProjectCompany.Value = Session["TaskComp"].ToString();
                hdnProjectCompany.Value = Request.QueryString["CompName"].ToString();
                try
                {
                   

                        sql = "select * from Notes_Categories where Category_Type='FHS'";


                        DataSet dsNCategories = DatabaseHelper.getDataset(sql);


                        if (dsNCategories != null && dsNCategories.Tables.Count > 0 && dsNCategories.Tables[0].Rows.Count > 0)
                        {
                            arrAllFHSCAtegories.Clear();


                            for (int i = 0; i < dsNCategories.Tables[0].Rows.Count; i++)
                            {

                                arrAllFHSCAtegories.Add(dsNCategories.Tables[0].Rows[i]["Categoy_name"].ToString());
                            }


                            Repeater1.DataSource = dsNCategories.Tables[0];
                            Repeater1.DataBind();


                        }
                  

                }
                catch { }

                
                if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
                {
                    FtbCompNotes.ReadOnly = true;
                    btnote.Enabled = false;
                    chkEmailnotify.Enabled = false;
                    btnRemove.Enabled = false;
                    LnkHideTabArea.Visible = false;
                    LnkShowAllCat.Visible = false;
                    LnkHideTabArea.Enabled = false;
                    LnkShowAllCat.Enabled = false;
                }
                else
                {
                    
                    btnRemove.Enabled = false;
                    LnkHideTabArea.Visible = true;
                    LnkShowAllCat.Visible = true;
                    LnkHideTabArea.Enabled = true;
                    LnkShowAllCat.Enabled = true;
                    FtbCompNotes.ReadOnly = false;
                    btnote.Enabled = true;
                    chkEmailnotify.Enabled = true;
                }
                try
                {
                    TabContainer1.ActiveTabIndex = 0;
                    hdnCategories.Value = "0";
                    TabContainer1_ActiveTabChanged(TabContainer1, null);
                    
                }
                catch { }

            }
        }

    }

    public void Page_Init(object sender, EventArgs e)
    {

        try
        {
            

                createTab();
           
        }
        catch
        {
        }
    }

    private void createTab()
    {



        //if (Session["TaskComp"] == null || Session["TaskComp"].ToString().Trim() == "")
        //{

        //}
        if (Request.QueryString["CompName"] == null || Request.QueryString["CompName"].ToString() == "")
        {


        }
        else
        {
            //hdnProjectCompany.Value = Session["TaskComp"].ToString();
            hdnProjectCompany.Value = Request.QueryString["CompName"].ToString();
            hdnFHSCategories.Value = "";

            sql = "select * from Company_notes where Company_name ='" + hdnProjectCompany.Value + "' and Note_Type='FHS' and Allow_Notes ='True'";


            DataSet ds = DatabaseHelper.getDataset(sql);


            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
               
                AjaxControlToolkit.TabPanel tp = new AjaxControlToolkit.TabPanel();


                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {

                    /// i = i + 1;
                    tp = new AjaxControlToolkit.TabPanel();
                    tp.ID = ds.Tables[0].Rows[i]["Category_name"].ToString().Replace(" ", "").Replace("-", "").Replace(" ", "");
                    
                    FreeTextBoxControls.FreeTextBox b = new FreeTextBoxControls.FreeTextBox();
                    /// Button b = new Button();
                    string hdfgh = ds.Tables[0].Rows[i]["Category_name"].ToString().Replace(" ", "_").Replace("-", "").Replace(" ", "");
                    b.ID = ds.Tables[0].Rows[i]["Category_name"].ToString().Replace(" ", "").Replace("-", "").Replace(" ", "");
                    b.Text = ds.Tables[0].Rows[i]["Notes"].ToString();
                    b.Height = 500;//TabContainer1.Height;

                   b.Width = TabContainer1.Width;
                   
                    tp.Controls.Add(b);
                    tp.HeaderText = ds.Tables[0].Rows[i]["Category_name"].ToString();
                    TabContainer1.Controls.Add(tp);

                    hdnFHSCategories.Value += ds.Tables[0].Rows[i]["Category_name"].ToString() + ";";
                   

                }


            }
           
        }
    }
    protected void btnote_Click(object sender, EventArgs e)
    {

        string r = TabContainer1.ActiveTabIndex.ToString();
        TabContainer1.ActiveTabIndex = int.Parse(hdnCategories.Value.ToString());

        FreeTextBoxControls.FreeTextBox FTBCompNotes = (FreeTextBoxControls.FreeTextBox)(TabContainer1.ActiveTab.FindControl(TabContainer1.ActiveTab.HeaderText.ToString().Replace(" ", "").Replace("-", "").Replace(" ", "")));

        if (FTBCompNotes.Text.Trim() == "")
        {
            div1.InnerHtml = "Please enter notes .";
            return;
        }

        ////string sqlcompanynote = "select [Notes] from [Company_notes]  where Company_name = '" + hdnProjectCompany.Value + "' and Note_Type ='FHS' and Category_name='" + TabContainer1.ActiveTab.HeaderText.ToString() + "'";
        ////object objNoteResult = DatabaseHelper.executeScalar(sqlcompanynote);
        ////if (objNoteResult != null)
        ////{
            string strSql = " update Company_notes ";
            strSql += " set [Notes]= '" + FTBCompNotes.Text.Trim().Replace("'", "''") + "'";
            strSql += " where Company_Name = '" + hdnProjectCompany.Value + "' and  Note_Type ='FHS' and Category_name='" + TabContainer1.ActiveTab.HeaderText.ToString() + "' ";

            int intResult = DatabaseHelper.executeNonQuery(strSql);
            if (intResult != 0)
            {
               sendNotification();
                if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
                {
                   
                    btnRemove.Enabled = false;
                    LnkHideTabArea.Enabled = false;
                    LnkShowAllCat.Enabled = false;
                }
                else
                {
                    btnRemove.Enabled = true;
                    LnkHideTabArea.Enabled = true;
                    LnkShowAllCat.Enabled = true;
                }
                div1.InnerHtml = "FHS notes Updated";
            }
            else
            {
                div1.InnerHtml = "FHS notes not updated. please try again.";
            }
       // }

    

    }

    private string generateRemovednoteAdminEmail()
    {
        string strBody = "";
        strBody += "<br/>";
        strBody += "FHS notes have been removed for  "+ hdnProjectCompany.Value + "  at  "+ DateTime.Now.ToString("dd MMM h:mm tt")+"<br/><br>";
        ///strBody += FtbCompNotes.Text +"<br/><br/>";
        strBody += "<span style=\'font-size:11.0pt;font-family:Calibri,sans-serif;color:red\'>Please do not reply to this email as it does not take replies.</span><br/>";
        strBody += "<br/>" + "Regards," + "<br/>Support.";
        return strBody;
    }
    private string generateAdminEmail()
    {
        TabContainer1.ActiveTabIndex = int.Parse(hdnCategories.Value.ToString());

        FreeTextBoxControls.FreeTextBox FTBCompNotes = (FreeTextBoxControls.FreeTextBox)(TabContainer1.ActiveTab.FindControl(TabContainer1.ActiveTab.HeaderText.ToString().Replace(" ", "").Replace("-", "").Replace(" ", "")));
        string strBody = "";
        strBody += "<br/>";
        strBody += "Following FHS notes have been updated for  "+ hdnProjectCompany.Value + "  at  "+ DateTime.Now.ToString("dd MMM h:mm tt")+"<br/><br>";
        strBody += FTBCompNotes.Text + "<br/><br/>";
        strBody += "<span style=\'font-size:11.0pt;font-family:Calibri,sans-serif;color:red\'>Please do not reply to this email as it does not take replies.</span><br/>";
        strBody += "<br/>" + "Regards," + "<br/>Support.";
        return strBody;
    }
    void sendNotification()
    {
        sql = "select email, EmailNotification";
        sql += " from users";
        sql += " where admin = 'Y'";
        sql += " and EmailNotification ='Y' ";

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            string strHtml = generateAdminEmail();

            string toEmails = "";


            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (ds.Tables[0].Rows[i]["email"].ToString() != "")
                {

                   
                toEmails += ds.Tables[0].Rows[i]["email"].ToString() + ";";
                        

                }
              

            }

            toEmails = toEmails.TrimEnd(';');

            if (emailSubject == "")
            {

                emailSubject = "FHS notes have been added for   " + hdnProjectCompany.Value;

               /// emailSubject = "Task ID:" + strTaskId.ToString() + " was updated - " + txtShortDescr.Text.Trim() + " (Task ID:" + strTaskId.ToString() + ")";
            }
            if (chkEmailnotify.Checked)
            {
                bool flag = DatabaseHelper.sendEmailTasks(toEmails, emailSubject, strHtml);
            }
            else
            {
            }
        }
    }
    void sendRemovedNotification()
    {
        sql = "select email, EmailNotification";
        sql += " from users";
        sql += " where admin = 'Y'";
        sql += " and EmailNotification ='Y' ";

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            string strHtml = generateRemovednoteAdminEmail();

            string toEmails = "";


            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (ds.Tables[0].Rows[i]["email"].ToString() != "")
                {


                    toEmails += ds.Tables[0].Rows[i]["email"].ToString() + ";";


                }


            }

            toEmails = toEmails.TrimEnd(';');

            if (emailSubject == "")
            {

                emailSubject = "FHS notes have been removed for   " + hdnProjectCompany.Value;

                /// emailSubject = "Task ID:" + strTaskId.ToString() + " was updated - " + txtShortDescr.Text.Trim() + " (Task ID:" + strTaskId.ToString() + ")";
            }
            if (chkEmailnotify.Checked)
            {
                bool flag = DatabaseHelper.sendEmailTasks(toEmails, emailSubject, strHtml);
            }
            else
            {
            }
        }
    }
    protected void btnRemove_Click(object sender, EventArgs e)
    {
        string r = TabContainer1.ActiveTabIndex.ToString();
        TabContainer1.ActiveTabIndex = int.Parse(hdnCategories.Value.ToString());

        FreeTextBoxControls.FreeTextBox FTBCompNotes = (FreeTextBoxControls.FreeTextBox)(TabContainer1.ActiveTab.FindControl(TabContainer1.ActiveTab.HeaderText.ToString().Replace(" ", "").Replace("-", "").Replace(" ", "")));

      
        string strSql = "update Company_notes ";
        strSql += " set Allow_Notes= 'False'";
        strSql += " where Company_Name = '" + hdnProjectCompany.Value + "' and  Note_Type ='FHS' and Category_name='" + TabContainer1.ActiveTab.HeaderText.ToString() + "' ";

        int intResult = DatabaseHelper.executeNonQuery(strSql);
        if (intResult != 0)
        {
        sendRemovedNotification();
            btnRemove.Enabled = false;
            //div1.InnerHtml = "FHS notes removed";
            Response.Redirect("companyNotes.aspx", false);
        }
        else
        {
            ///div1.InnerHtml = "FHS notes not removed. please try again.";
            Response.Redirect("companyNotes.aspx", false);
        }

    }
    protected void BtnRdbCat_Click(object sender, EventArgs e)
    {
       

        string r = TabContainer1.ActiveTabIndex.ToString();
        TabContainer1.ActiveTabIndex = int.Parse(hdnCategories.Value.ToString());

        string sqlcompanynote = "select [Notes] from [Company_notes]  where Company_name = '" + hdnProjectCompany.Value + "' and Note_Type ='FHS' and Category_name='" + TabContainer1.ActiveTab.HeaderText.ToString() + "'";
        object objNoteResult = DatabaseHelper.executeScalar(sqlcompanynote);

        if (objNoteResult != null)
        {
            FreeTextBoxControls.FreeTextBox CompNotes = (FreeTextBoxControls.FreeTextBox)(TabContainer1.ActiveTab.FindControl(TabContainer1.ActiveTab.HeaderText.ToString().Replace(" ", "").Replace("-", "").Replace(" ", "")));
            CompNotes.Text = objNoteResult.ToString();
            ///hdnProjectCompany.Value = objResult.ToString();
            if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
            {
                btnRemove.Enabled = false;
                LnkHideTabArea.Enabled = false;
                LnkShowAllCat.Enabled = false;
            }
            else
            {
                btnRemove.Enabled = true;
                LnkHideTabArea.Enabled = true;
                LnkShowAllCat.Enabled = true;

            }
        }
        else
        {
            btnRemove.Enabled = false;
            FtbCompNotes.Text = "";
            LnkHideTabArea.Enabled = false;
            LnkShowAllCat.Enabled = false;

        }




    }
    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        try
        {
            string r = TabContainer1.ActiveTabIndex.ToString();
            TabContainer1.ActiveTabIndex = int.Parse(hdnCategories.Value.ToString());
            string sqlcompanynote = "select [Notes] from [Company_notes]  where Company_name = '" + hdnProjectCompany.Value + "' and Note_Type ='FHS' and Category_name='" + TabContainer1.ActiveTab.HeaderText.ToString() + "'";
            object objNoteResult = DatabaseHelper.executeScalar(sqlcompanynote);

            if (objNoteResult != null)
            {
                FreeTextBoxControls.FreeTextBox CompNotes = (FreeTextBoxControls.FreeTextBox)(TabContainer1.ActiveTab.FindControl(TabContainer1.ActiveTab.HeaderText.ToString().Replace(" ", "").Replace("-", "").Replace(" ", "")));
                CompNotes.Text = objNoteResult.ToString();
                ///hdnProjectCompany.Value = objResult.ToString();
                if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
                {
                    btnRemove.Enabled = false;
                    LnkHideTabArea.Enabled = false;
                    LnkShowAllCat.Enabled = false;
                }
                else
                {
                    btnRemove.Enabled = true;
                    LnkHideTabArea.Enabled = true;
                    LnkShowAllCat.Enabled = true;
                }
            }
            else
            {
                btnRemove.Enabled = false;
                FtbCompNotes.Text = "";
                LnkHideTabArea.Enabled = false;
                LnkShowAllCat.Enabled = false;
            }
        }
        catch { }

    }
    protected void btnAddCategory_Click(object sender, EventArgs e)
    {


        string strCkdCategories = hdnFHSCategories.Value.Trim();


        string[] strFhsCategory = strCkdCategories.Split(new char[] { ';' });

        ArrayList ChkdFHSCategories = new ArrayList();
        try
        {
            for (int k = 0; k < strFhsCategory.Length - 1; k++)
            {
                ChkdFHSCategories.Add(strFhsCategory[k].ToString());

            }
        }
        catch { }


        try
        {
            string FHSNotes = "";
            string strSqlCat = "";
            int intResultCat = 0;
            for (int j = 0; j < arrAllFHSCAtegories.Count; j++)
            {
                try
                {
                    object FHSCatname = arrAllFHSCAtegories[j].ToString();
                    if (ChkdFHSCategories.Contains(FHSCatname.ToString()) == false)
                    {
                        object objResultFcat = DatabaseHelper.executeScalar("select count(*) from Company_notes where [Company_Name]='" + hdnProjectCompany.Value + "' and Note_Type ='FHS'and Category_name ='" + FHSCatname.ToString() + "'");
                        if (objResultFcat.ToString() != "0")
                        {
                            strSqlCat = "Update Company_notes set Allow_Notes ='False' where Company_name = '" + hdnProjectCompany.Value.Replace("'", "''") + "' and Note_Type ='FHS'and Category_name ='" + FHSCatname.ToString() + "'";


                            intResultCat = DatabaseHelper.executeNonQuery(strSqlCat);
                        }
                        else
                        {
                            strSqlCat = " insert into Company_notes ([Company_name],[Note_Type],[Category_name],[Notes],[Allow_Notes]) ";
                            strSqlCat += " values('" + hdnProjectCompany.Value.Replace("'", "''") + "','FHS','" + FHSCatname.ToString() + "','" + FHSNotes + "','False')";

                            intResultCat = DatabaseHelper.executeNonQuery(strSqlCat);
                        }

                    }
                    else
                    {

                        object objResultFcat2 = DatabaseHelper.executeScalar("select count(*) from Company_notes where [Company_Name]='" + hdnProjectCompany.Value + "' and Note_Type ='FHS'and Category_name ='" + FHSCatname.ToString() + "'");
                        if (objResultFcat2.ToString() != "0")
                        {
                            strSqlCat = "Update Company_notes set Allow_Notes ='True' where Company_name = '" + hdnProjectCompany.Value.Replace("'", "''") + "' and Note_Type ='FHS'and Category_name ='" + FHSCatname.ToString() + "'";

                            intResultCat = DatabaseHelper.executeNonQuery(strSqlCat);
                        }
                        else
                        {
                            strSqlCat = " insert into Company_notes ([Company_name],[Note_Type],[Category_name],[Notes],[Allow_Notes]) ";
                            strSqlCat += " values('" + hdnProjectCompany.Value.Replace("'", "''") + "','FHS','" + FHSCatname.ToString() + "','" + FHSNotes + "','True')";

                            intResultCat = DatabaseHelper.executeNonQuery(strSqlCat);
                        }


                    }
                }
                catch
                {


                }
            }

            Response.Redirect("companyNotes.aspx", false);
        }
        catch { }

    }

  
}
