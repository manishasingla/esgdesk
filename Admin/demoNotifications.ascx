﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="demoNotifications.ascx.cs"
    Inherits="Admin_demoNotifications" %>

<script language="javascript" type="text/javascript">
    function window.confirm(str) {
        execScript('n = msgbox("' + str + '","4132")', "vbscript");
        return (n == 6);
    }
</script>

<table cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <%--<td style="padding: 0 0 0 10px;">--%>
        <td style="padding: 0 0 0 0px;">
            <span id="Notification" runat="server" style="font-size: 16px; color: #282C5F;">
                <%--Notifications:--%>
            </span>
            
            <%---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            MINE:<span id="immediatetask" runat="server" style="color: red">Immediate<asp:Button
                ID="lnkPr0Task" runat="server"  ForeColor="red" Style="background: none repeat scroll 0% 0% transparent;
                border: 0px; cursor: pointer; width: auto; padding: 0;Border-bottom:1px dotted #000;" CausesValidation="False"
                OnClick="lnkPr0Task_Click" />
            </span>
             <%---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            <span id="highestTasks" runat="server" style="color: #FF1493">Do now<asp:Button ID="lnkHighestTasks"
                runat="server" Style="background: none repeat scroll 0% 0% transparent; border: medium none;
                cursor: pointer; color: #FF1493; width: auto; padding: 0;Border-bottom:1px dotted #000;" CausesValidation="False"
                OnClick="lnkHighestTasks_Click" /></span>
            <%---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            <span id="highTasks" runat="server" style="color: black">High<asp:Button ID="lnkHighTasks"
                runat="server" CausesValidation="False" Style="background: none repeat scroll 0% 0% transparent;
                border: medium none; cursor: pointer; width: auto; padding: 0;Border-bottom:1px dotted #000;" OnClick="lnkHighTasks_Click" /></span>
            <%---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            <span id="PR1CTask" runat="server" style="color: black">Normal<asp:Button ID="Lnk1cTasks"
                runat="server" CausesValidation="False" Style="background: none repeat scroll 0% 0% transparent;
                border: medium none; cursor: pointer; width: auto; padding: 0;Border-bottom:1px dotted #000;" OnClick="Lnk1cTasks_Click" /></span>
            <%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            <span id="PRlowTask" runat="server" style="color: black">Low<asp:Button ID="LinklowTasks"
                Style="background: none repeat scroll 0% 0% transparent; border: medium none;
                cursor: pointer; width: auto; padding: 0;Border-bottom:1px dotted #000;" runat="server" CausesValidation="False"
                OnClick="LinklowTasks_Click" /></span>
            <%---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            
            <span id="TskNewComment" runat="server" style="color: #FF1493;">Unread comments<asp:Button
                ID="LnkTskNewcomment" runat="server" CausesValidation="false" Style="background: none repeat scroll 0% 0% transparent;
                border: medium none; cursor: pointer; color: #FF1493; width: auto; padding: 0;Border-bottom:1px dotted #000;"
                OnClick="LnkTskNewcomment_Click" /><asp:Button ID="LnkMakeTaskcommentRead" runat="server"
                    CausesValidation="false" Style="background: none repeat scroll 0% 0% transparent;
                    border: medium none; cursor: pointer; color: #FF1493; width: auto; padding: 0;
                    margin-left: -5px" OnClick="LnkMakeTaskcommentRead_Click" /></span>
            <%---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            <span id="unansweredque" runat="server" style="color: black">To answer<asp:Button
                ID="lnkUnansweredQuestions" runat="server" Style="background: none repeat scroll 0% 0% transparent;
                border: medium none; cursor: pointer; width: auto; padding: 0;Border-bottom:1px dotted #000;" CausesValidation="False"
                OnClick="lnkUnansweredQuestions_Click" /><%--<span style="color:#333333;font-weight:bold;">&nbsp;|&nbsp;</span>--%>
            </span><span id="firstpipe" runat="server" style="color: #333333; font-weight: bold;">
                &nbsp;|&nbsp;</span>
            <%---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            <span id="OverDuetsk" runat="server" style="color: red">Overdue<asp:Button ID="BtnOverdue"
                runat="server" BackColor="transparent" CausesValidation="False" Style="background: none repeat scroll 0% 0% transparent;
                border: medium none; cursor: pointer; color: Red; width: auto; padding: 0;Border-bottom:1px dotted #000;" OnClick="BtnOverdue_Click" /></span>
            <%-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            <span id="allsign" runat="server" style="color:Black;">ALL:&nbsp;</span> <span id="SpnAllTaskNewComment" runat="server" style="color: #FF1493">
                <%--<span style="color:Black">&nbsp;|&nbsp;</span>Tasks with new comment--%>
                Unread comments<asp:Button ID="LnkAllTsknewComment" runat="server" Style="background: none repeat scroll 0% 0% transparent;
                    border: medium none; cursor: pointer; color: #FF1493; width: auto; padding: 0;Border-bottom:1px dotted #000;" OnClick="LnkAllTsknewComment_Click" /></span>
            <span id="Span1" runat="server" style="color: #FF1493; ">
                <asp:Button ID="LnkMakeAllTaskCmmntRead" runat="server" CausesValidation="false" Style="background: none repeat scroll 0% 0% transparent;
                    border: medium none; cursor: pointer; color: #FF1493; width: auto; padding: 0;margin-left:-8px" OnClick="LnkMakeAllTaskCmmntRead_Click" /></span>
            <%-------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            <span id="AllOverDuetsk" runat="server" style="color: red">
                <%--<span style="color:Black;">&nbsp;|&nbsp;</span>--%>
                <span style="color: Black;"></span>Overdue<asp:Button ID="BtnallOverdue" runat="server"
                    BackColor="transparent" BorderStyle="none" CausesValidation="False" Style="background: none repeat scroll 0% 0% transparent;
                    border: medium none; cursor: pointer; color: Red; width: auto; padding: 0;Border-bottom:1px dotted #000;" OnClick="BtnallOverdue_Click" /></span>
            <%---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            <span id="tobeanswered" runat="server" style="color: black">To be answered<asp:Button
                ID="lnktobeanswered" runat="server" Style="background: none repeat scroll 0% 0% transparent;
                border: medium none; cursor: pointer; width: auto; padding: 0;Border-bottom:1px dotted #000;" CausesValidation="False"
                OnClick="lnktobeanswered_Click" /></span> 
            <%---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------%> 
            <span id="UnApprovedCR" runat="server" style="color:red">
             UnApproved<asp:Button ForeColor="red" ID="Btnunapproved" runat="server" Style="background: none repeat scroll 0% 0% transparent;
                    border:medium none;cursor:pointer;width:auto; padding:0Border-bottom:1px dotted #000;" OnClick="Btnunapproved_Click" />
                    </span>
              
            
             <%---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            <span id="NewCrComment" runat="server" style="color:red">
             UnApproved Comment<asp:Button ForeColor="red" ID="NewCRCommentinRed" runat="server" Style="background: none repeat scroll 0% 0% transparent;
                    border:medium none;cursor:pointer;width:auto; padding:0;Border-bottom:1px dotted #000;" OnClick="NewCRCommentinRed_Click"   />
                    </span>
            <span id="secondpipe" runat="server" style="color:#333333;font-weight: bold;">&nbsp;|&nbsp;</span>
            <%---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------%>
              <span id="CRsign" runat="server" style="color:Green;">CR:&nbsp;</span> <span id="NewCR" runat="server" style="color: Green">
                <%--<span style="color:Black ">&nbsp;|&nbsp;</span>--%>
                New<asp:Button ForeColor="green" ID="LnkNewCR" runat="server" Style="background: none repeat scroll 0% 0% transparent;
                    border: medium none; cursor: pointer; width: auto; padding: 0;Border-bottom:1px dotted #000;" OnClick="LnkNewCR_Click" /></span><span
                        style="color: #333333"> </span>
            <%----------------------------------------------------------------------------------------------------------------------------------------------------%>
            
            <span id="CrNewComment" runat="server">
                    <span style="color:Black">
                        Unread comments
                    </span>
                    <asp:Button ID="LnkCrNewComment" runat="server" Style="background: none repeat scroll 0% 0% transparent; border: medium none; cursor: pointer;width:auto;padding:0;Border-bottom:1px dotted #000;" OnClick="LnkCrNewComment_Click"/>
                        </span>
                        <span id="MakeCommentRead" runat="server" style="color:Black;margin-left:-5px">
                        <asp:Button ID="LnkMakecommentRead" runat="server" Style="background:none repeat scroll 0% 0% transparent; border:medium none; cursor:pointer;width:auto;padding:0;" OnClick="LnkMakecommentRead_Click"/>
                    </span>
            <%---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------%> 
            
            <%---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------%>           
        </td>
    </tr>
</table>
