﻿using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System;
using System.Data;

public partial class Admin_demoNotifications : System.Web.UI.UserControl
{

    int id;
    int reqid;
    string sql = "";
    protected static string TskID;
    bool chkAdmin;
    protected void Page_Load(object sender, EventArgs e)
    {
        chkAdmin = DatabaseHelper.isAdmin(Session["admin"].ToString());
        if (!Page.IsPostBack)
        {
            if (!chkAdmin)
            {
                firstpipe.Visible = false;
                secondpipe.Visible = false;
                allsign.Visible = false;
                CRsign.Visible = false;
            }
            else
            {
                firstpipe.Visible = true;
                secondpipe.Visible = true;
                allsign.Visible =true ;
                CRsign.Visible = true;
            }

            
            LnkMakeTaskcommentRead.Attributes.Add("onclick", "javascript:return confirm('Sure you want to mark every comment in each of your tasks as read by you (this means you must have read them all and acted on all that require action already)?');");
            LnkMakeAllTaskCmmntRead.Attributes.Add("onclick", "javascript:return confirm('Sure you want to mark every comment in each of tasks as read by you (this means you must have read them all and acted on all that require action already)?');");


            if (Session["admin"] == null)
            {
                return;
            }
            else
            {
                getUnasweredQuestions();
                getTobeAnswered();
                //=====Not Required=====================
                //getCRwaitingfrmClient();
                //======================================
                getPR0Tasks();
                getPR1aTasks();
                getPR1bTasks();
                getPR1cTasks();
                getPRLowTasks();
                getNewCR();
                getOverduetasks();
                getAllOverduetasks();
                getCRNewComment();
                getTskNewComment();
                getAllTskNewComment();
                //************************
                getUnApprovedCRbyM();
                getUnApprovedCRCommentbyM();
                //************************

            }

        }
    }




    void getPR1bTasks()
    {
        sql = @"select tasks.task_id from tasks 
                where tasks.deleted <> 1 and tasks.priority = '1b - high' ";
        sql += " and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + Session["admin"] + "' ";
       
        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables.Count >0  && ds.Tables[0].Rows.Count > 0)
        {
            lnkHighTasks.Text = "(" + ds.Tables[0].Rows.Count + ")";
            highTasks.Visible = true;
        }
        else
        {
            lnkHighTasks.Text = "(0)";
            highTasks.Visible = false;
        }
    }
    protected void lnkHighTasks_Click(object sender, EventArgs e)
    {
        //Session["boolclose"] = "true";
        Session["filter"] = "HighTasks";
        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        //===============================
        Session["AllOverdueTask"] = "";
        //===============================
        Response.Redirect("tasks.aspx");
    }

    void getNewCR()
    {
        sql = @"select ClientRequest.RequestId from ClientRequest 
                where ClientRequest.Status = 'new' and status <> 'closed' and deleted <> 1 ";


        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {

            if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
            {
                NewCR.Visible = false;
            }
            else
            {
                LnkNewCR.Text = "(" + ds.Tables[0].Rows.Count + ")";
                NewCR.Visible = true;
            }

        }
        else
        {


            if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
            {
                NewCR.Visible = false;
            }
            else
            {
                LnkNewCR.Text = "(0)";
                NewCR.Visible = false;
            }


        }
    }

    protected void LnkNewCR_Click(object sender, EventArgs e)
    {
        Session["boolclose"] = true;
        Session["boolcount"] = "false";
        Session["filter"] = "NewCR";
        //===============================
        Session["AllOverdueTask"] = "";
        //===============================
        Response.Redirect("client_requests.aspx", false);
        /// bindPR1bTasks();

    }
    void getUnasweredQuestions()
    {
        sql = @"select distinct tasks.task_id from task_comments,tasks 
                where tasks.task_id = task_comments.task_id and tasks.deleted <> 1 and task_comments.deleted <> 1 and qflag=1 ";
      
        sql += " and status <> 'closed' ";
        sql += " and task_comments.QuesTo = '" + Session["admin"] + "' ";
        //=============================================================================================
        
        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            lnkUnansweredQuestions.Text = "(" + ds.Tables[0].Rows.Count + ")";
            unansweredque.Visible = true;
        }
        else
        {
            lnkUnansweredQuestions.Text = "(0)";
            unansweredque.Visible = false;
        }
    }
    protected void lnkUnansweredQuestions_Click(object sender, EventArgs e)
    {
        
        Session["filter"] = "Unanswered";
        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        //===============================
        Session["AllOverdueTask"] = "";
        //===============================
        Response.Redirect("tasks.aspx");
    }



    protected void LnkCRWocr_Click(object sender, EventArgs e)
    {
      
        Session["filter"] = "CRwocr";
        //===============================
        Session["AllOverdueTask"] = "";
        //===============================
        Response.Redirect("client_requests.aspx", false);


    }

    void getPR0Tasks()
    {
        sql = @"select tasks.task_id from tasks 
                where tasks.deleted <> 1 and tasks.priority = '0 - IMMEDIATE' ";
        //***********************************************************************************
        
           sql += " and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + Session["admin"] + "' ";
        
        //***********************************************************************************
        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            lnkPr0Task.Text="("+ds.Tables[0].Rows.Count+")";
            immediatetask.Visible = true;
        }
        else
        {
            lnkPr0Task.Text = "(0)";
            immediatetask.Visible = false;
        }
    }

    protected void lnkPr0Task_Click(object sender, EventArgs e)
    {
        
        Session["filter"] = "Immediate";
        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        //===============================
        Session["AllOverdueTask"] = "";
        //===============================
        Response.Redirect("tasks.aspx");
    }

    void getPR1aTasks()
    {
        sql = @"select tasks.task_id from tasks 
                where tasks.deleted <> 1 and tasks.priority = '1a - DO NOW' ";
        sql += " and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + Session["admin"] + "' ";
        
        
        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            lnkHighestTasks.Text = "(" + ds.Tables[0].Rows.Count + ")";
            highestTasks.Visible = true;
        }
        else
        {
            lnkHighestTasks.Text = "(0)";
            highestTasks.Visible = false;
        }
    }
    protected void lnkHighestTasks_Click(object sender, EventArgs e)
    {


        Session["fil"] = "1a - DO NOW";
        Session["filter"] = "1a - DO NOW";
        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        //===============================
        Session["AllOverdueTask"] = "";
        //===============================
        Response.Redirect("tasks.aspx");
    }
    void getPR1cTasks()
    {

        sql = @"select tasks.task_id from tasks 
         where tasks.deleted <> 1 and tasks.priority = '1c - normal' ";
        sql += " and  status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + Session["admin"] + "' ";
        
        
        DataSet ds = DatabaseHelper.getDataset(sql);
        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            Lnk1cTasks.Text = "(" + ds.Tables[0].Rows.Count + ")";
            PR1CTask.Visible = true;
        }
        else
        {
            Lnk1cTasks.Text = "(0)";
            PR1CTask.Visible = false;
        }
    }

    protected void Lnk1cTasks_Click(object sender, EventArgs e)
    {

        Session["filter"] = "1c - normal";
        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        //===============================
        Session["AllOverdueTask"] = "";
        //===============================
        Response.Redirect("tasks.aspx");
    }

    void getOverduetasks()
    {

        try
        {

            int GvnETC = 0;
            int j = 0;
            DateTime crrntdate = DateTime.Now.Date;
            DateTime duedate;
            sql = @"select task_id from tasks where DATEDIFF(day,SUBSTRING(DueDate,4,Len(DueDate)),REPLACE(CONVERT(VARCHAR(11), GETDATE(), 106), ' ', '/')) > 0  and DueDate!='' and tasks.deleted <> 1 and  status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + Session["admin"] + "' ";
            DataSet ds = DatabaseHelper.getDataset(sql);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                
                if (ds.Tables[0].Rows.Count >0)
                  {
                     BtnOverdue.Text = "(" + ds.Tables[0].Rows.Count.ToString() + ")";
                     OverDuetsk.Visible = true;
                }
                else
                {
                    BtnOverdue.Visible = false;
                    OverDuetsk.Visible = false;
                }


            }
            else
            {
                BtnOverdue.Visible = false;
                OverDuetsk.Visible = false;
            }

        }
        catch { }

    }

    protected void BtnOverdue_Click(object sender, EventArgs e)
    {

        Session["filter"] = " ";
        Session["filterunread"] = "";
        Session["OverdueTask"] = "True";
        //===============================
        Session["AllOverdueTask"] = "";
        //===============================
        Response.Redirect("tasks.aspx");


    }

    void getCRNewComment()
    {
        sql = "select ClientRequest.RequestId from ClientRequest where RequestId in(select ClientRequest_Details.RequestId from ClientRequest_Details where [CommentId] not in (select  CommentId from read_CR_comments where read_CR_comments.username='" + Session["admin"].ToString() + "') and ClientRequest_Details.deleted <> 1)";
        DataSet ds = DatabaseHelper.getDataset(sql);
        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
            {
                CrNewComment.Visible = false;
                LnkMakecommentRead.Visible = false;
            }
            else
            {
                LnkCrNewComment.Text = "(" + ds.Tables[0].Rows.Count + ")";
                LnkMakecommentRead.Text = "(" + "-" + ")";
                CrNewComment.Visible = true;
                LnkMakecommentRead.Visible = true;
            }

        }
        else  
        { 

            if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
            {
                CrNewComment.Visible = false;
                LnkMakecommentRead.Visible = false;
            }
            else
            {
                LnkCrNewComment.Text = "(0)";
                CrNewComment.Visible = false;
                LnkMakecommentRead.Visible = false;
            }




        }
    }

    protected void LnkCrNewComment_Click(object sender, EventArgs e)
    {
        Session["boolclose"] = "true";
        Session["boolcount"] = "false";
        Session["filter"] = "CRNewComment";
        //===============================
        Session["AllOverdueTask"] = "";
        //===============================
        Response.Redirect("client_requests.aspx", false);
    }

    protected void LnkMakecommentRead_Click(object sender, EventArgs e)
    {
        Session["boolclose"] = "true";
        Session["boolcount"] = "false";
        string sql = "select *  from ClientRequest_Details where [CommentId] not in (select  CommentId  from read_CR_comments where read_CR_comments.username='" + Session["admin"].ToString() + "') and ClientRequest_Details.deleted <> 1";
        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {
                    string sqlinsert = "insert into read_CR_comments([CommentId],[UserName]) ";
                    sqlinsert += " values(" + ds.Tables[0].Rows[i]["CommentId"].ToString() + ",'" + Session["admin"].ToString() + "')";
                    int intResult = DatabaseHelper.executeNonQuery(sqlinsert);
                }
                catch
                {
                }
            }

        }

        getCRNewComment();
    }

    void getTskNewComment()
    {
        sql = "";
        LnkTskNewcomment.Text = "";
        
          if (!chkAdmin)
       {

            //sql = "select tasks.* from tasks where task_id in(select task_comments.task_id from task_comments where tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 )";
            //sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

            sql = " select tc_id into #tmpread from read_comments where username ='" + Session["admin"].ToString() + "'";
            sql += " select tasks.Task_Id from tasks where task_id in ";
            sql += " (select Distinct task_comments.task_id from task_comments where tc_id  not in (select tc_id from #tmpread) and task_comments.deleted <> 1)";
            sql += " and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "'";
            sql += " drop table #tmpread ";

        }
        else
        {
            //sql = "select tasks.* from tasks where task_id in(select task_comments.task_id from task_comments where tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 )";
            //sql += "and tasks.deleted <> 1 and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

            sql = " select tc_id into #tmpread from read_comments where username ='" + Session["admin"].ToString() + "'";
            sql += " select tasks.Task_Id from tasks where task_id in ";
            sql += " (select Distinct task_comments.task_id from task_comments where tc_id  not in (select tc_id from #tmpread) and task_comments.deleted <> 1)";
            sql += " and tasks.deleted <> 1  and tasks.assigned_to_user = '" + Session["admin"].ToString() + "'";
            sql += " drop table #tmpread ";


        }
        DataSet dsNewTskCmnt = DatabaseHelper.getDataset(sql);

        if (dsNewTskCmnt != null && dsNewTskCmnt.Tables.Count > 0 && dsNewTskCmnt.Tables[0].Rows.Count > 0)
        {
            LnkTskNewcomment.Text = "(" + dsNewTskCmnt.Tables[0].Rows.Count + ")";
            LnkMakeTaskcommentRead.Text = "(" + "-" + ")";
            LnkMakeTaskcommentRead.Visible = true;
            TskNewComment.Visible = true;
        }
        else
        {
            LnkTskNewcomment.Text = "(0)";
            TskNewComment.Visible = false;
            LnkMakeTaskcommentRead.Visible = false;
        }
    }

    protected void LnkTskNewcomment_Click(object sender, EventArgs e)
    {
        Session["boolclose"] = "true";
        Session["filter"] = "";
        Session["filterunread"] = "TaskNewComment";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        //===============================
        Session["AllOverdueTask"] = "";
        //===============================
        Session["boolcount"] = "false";
        Response.Redirect("tasks.aspx");


    }
    protected void LnkMakeTaskcommentRead_Click(object sender, EventArgs e)
    {

        sql = @"select *  from task_comments where task_id in (select task_id  from tasks where tasks.task_id = task_comments.task_id and  tasks.assigned_to_user = '" + Session["admin"].ToString() + "'  and tasks.deleted <> 1 and tasks.status <> 'closed') and task_comments.tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 ";
        DataSet ds = DatabaseHelper.getDataset(sql);
        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {

                   string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                    sqlinsert += " values(" + ds.Tables[0].Rows[i]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                    int intResult = DatabaseHelper.executeNonQuery(sqlinsert);

                    string sqlinsert2 = "insert into read_task([task_id],[username]) ";
                    sqlinsert2 += " values(" + ds.Tables[0].Rows[i]["task_id"].ToString() + ",'" + Session["admin"].ToString() + "')";
                    int intResult2 = DatabaseHelper.executeNonQuery(sqlinsert2);

                }
                catch
                {
                }
            }


        }

        sql = @"select *  from task_comments where task_id in (select task_id  from tasks where tasks.task_id = task_comments.task_id and  tasks.assigned_to_user = '" + Session["admin"].ToString() + "'  and tasks.deleted <> 1 and tasks.status = 'closed') and task_comments.tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 ";
        DataSet ds1 = DatabaseHelper.getDataset(sql);
        if (ds1 != null && ds1.Tables.Count > 0 && ds1.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds1.Tables[0].Rows.Count; i++)
            {
                try
                {


                    string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                    sqlinsert += " values(" + ds1.Tables[0].Rows[i]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                    int intResult = DatabaseHelper.executeNonQuery(sqlinsert);

                    string sqlinsert2 = "insert into read_task([task_id],[username]) ";
                    sqlinsert2 += " values(" + ds1.Tables[0].Rows[i]["task_id"].ToString() + ",'" + Session["admin"].ToString() + "')";
                    int intResult2 = DatabaseHelper.executeNonQuery(sqlinsert2);

                }
                catch
                {
                }
            }


        }


        
        Response.Redirect("tasks.aspx", false);
    }


    void getAllTskNewComment()
    {

        //*********************************************************************************************************************************************



        //sql = "Select t.task_Id from tasks t,task_comments tc where t.task_id in (select task_comments.task_id from task_comments where tc_id not in";
        //sql += " (select tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "' )and task_comments.deleted <> 1) and t.task_id = tc.task_id and tc.post_date =";
        //sql += " (select top 1 post_date from task_comments where tc.task_id = task_id order by post_date desc)";

        DataSet ds1 = null;
        string stask_id = "";
        string sqkqry = "select distinct task_comments.task_id  as tk_Id from task_comments where tc_id not in(select tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "')and task_comments.deleted <> 1 and task_comments.task_id is NOT NULL and task_comments.task_id<>'' order by task_comments.task_id";
        ds1 = DatabaseHelper.getDataset(sqkqry);
        if (ds1 != null && ds1.Tables.Count > 0 && ds1.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds1.Tables[0].Rows.Count; i++)
            {
                if (stask_id == "")
                {
                    stask_id = ds1.Tables[0].Rows[i]["tk_Id"].ToString() + ",";
                }
                else
                {
                    stask_id += ds1.Tables[0].Rows[i]["tk_Id"].ToString() + ",";
                }
            }
        }
        stask_id = stask_id.TrimEnd(new char[] { ',' });

        //sql = "Select t.task_Id from tasks t,task_comments tc where t.task_id in (" + stask_id + " ) and t.task_id = tc.task_id and tc.post_date =";
        //sql += " (select top 1 post_date from task_comments where tc.task_id = task_id order by post_date desc)";
        string sqlall = "";

        sqlall = "Select t.task_Id from tasks t,task_comments tc where t.task_id in (" + stask_id + ") and t.task_id = tc.task_id and tc.post_date = (select top 1 post_date from task_comments where tc.task_id = task_id order by post_date desc)";



        //sql = "Select t.task_Id from tasks t,task_comments tc where t.task_id in (select task_comments.task_id from task_comments where tc_id not in";
        //sql += " (select tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "' )and task_comments.deleted <> 1) and t.task_id = tc.task_id and tc.post_date =";
        //sql += " (select top 1 post_date from task_comments where tc.task_id = task_id order by post_date desc)";

        //*************************************************************************************************************************************
        DataSet ds = DatabaseHelper.getDataset(sqlall);

        //*********************************************************************************************************************************************
        //DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            
              if (!chkAdmin)
             {
                
                LnkAllTsknewComment.Text = "(0)";
                SpnAllTaskNewComment.Visible = false;
                LnkMakeAllTaskCmmntRead.Visible = false;
                LnkAllTsknewComment.Visible = false;
             }
            else
            {

                

                LnkAllTsknewComment.Text = "(" + ds.Tables[0].Rows.Count + ")";
                LnkMakeAllTaskCmmntRead.Text = "(" + "-" + ")";
                LnkMakeAllTaskCmmntRead.Visible = true;
                SpnAllTaskNewComment.Visible = true;
                //********************************************
                
            }
        }
        else
        {
            //********************************************
            LnkAllTsknewComment.Text = "(0)";
            SpnAllTaskNewComment.Visible = false;
            LnkMakeAllTaskCmmntRead.Visible = false;
            LnkAllTsknewComment.Visible = false;
            //********************************************
            
        }


    }

    protected void LnkAllTsknewComment_Click(object sender, EventArgs e)
    {
        Session["boolclose"] = "true";
        Session["filter"] = "";
        Session["filterunread"] = "AllTaskNewComment";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        //===============================
        Session["AllOverdueTask"] = "";
        //===============================
        Session["boolcount"] = "true";
        Response.Redirect("tasks.aspx");
    }
    protected void LnkMakeAllTaskCmmntRead_Click(object sender, EventArgs e)
    {
    }
    void getAllOverduetasks()
    {

        try
        {

            int GvnETC = 0;
            int j = 0;
            DateTime crrntdate = DateTime.Now.Date;
            DateTime duedate;
            sql = @"select tasks.* from tasks where tasks.deleted <> 1 and  status <> 'closed' and  status <> 'checked' and status <>'parked'";
            DataSet ds = DatabaseHelper.getDataset(sql);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
             for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                     string strcrrntdate = string.Format("{0:dd/MM/yyyy}", DateTime.Now.Date.ToString()).ToString();
                    //=============================================================================================
                    string djgfjs = ds.Tables[0].Rows[i]["DueDate"].ToString();
                    if (ds.Tables[0].Rows[i]["DueDate"].ToString() != null && ds.Tables[0].Rows[i]["DueDate"].ToString() != "")
                    {
                       string strduedate = string.Format("{0:dd/MM/yyyy}", ds.Tables[0].Rows[i]["DueDate"].ToString()).ToString();
                        duedate = DateTime.Parse(strduedate.ToString());
                        crrntdate = DateTime.Parse(strcrrntdate.ToString());
                        //=====================================================================================================
                        if (DateTime.Compare(duedate, crrntdate) < 0)
                        {
                            j++;
                        }
                    }
                }
                if (j > 0)
                 {
                    
                    if (DatabaseHelper.isAdmin(Session["admin"].ToString()))
                      {
                      BtnallOverdue.Text = "(" + j.ToString() + ")";
                      AllOverDuetsk.Visible = true;
                       }
                    else
                    {
                        AllOverDuetsk.Visible = false;
                    }
                }
                else
                {
                    BtnallOverdue.Visible = false;
                    AllOverDuetsk.Visible = false;
                 }


            }
            else
            {

                BtnallOverdue.Visible = false;
                AllOverDuetsk.Visible = true;
            }

        }
        catch(Exception ex)
        {
           // DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error:In Query", " Input Parameters :" + " Message:->" + ex.Message.ToString());
        }
    }

    void getPRLowTasks()
    {
        sql = @"select tasks.task_id from tasks 
                where tasks.deleted <> 1 and tasks.priority = '2 - not urgent'";

         sql += " and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + Session["admin"] + "' ";
         DataSet ds = DatabaseHelper.getDataset(sql);

         if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            LinklowTasks.Text = "(" + ds.Tables[0].Rows.Count + ")";
            PRlowTask.Visible = true;
        }
        else
        {
            LinklowTasks.Text = "(0)";
            PRlowTask.Visible = false;
        }
    }
    protected void BtnallOverdue_Click(object sender, EventArgs e)
    {
        Session["filter"] = " ";
        Session["filterunread"] = "";
        Session["OverdueTask"] = "True";
        Session["AllOverdueTask"] = "True";
        Response.Redirect("tasks.aspx");

    }
    protected void LinklowTasks_Click(object sender, EventArgs e)
    {
        Session["filter"] = "2 - not urgent";
        Session["filterunread"] = "";
        //===============================
        Session["AllOverdueTask"] = "";
        //===============================
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        Response.Redirect("tasks.aspx");
    }
    void getTobeAnswered()
    {
        sql = @"select distinct tasks.task_id from task_comments,tasks 
              where tasks.task_id = task_comments.task_id and tasks.deleted <> 1 and task_comments.deleted <> 1 and qflag=1 ";
             sql += " and status <> 'closed' and task_comments.username = '" + Session["admin"] + "'";


        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
                if (chkAdmin)
            {
                lnktobeanswered.Text = "(" + ds.Tables[0].Rows.Count + ")";
                tobeanswered.Visible = true;
            }
            else 
            {
                tobeanswered.Visible = false;
            }
        }
        else
        {
            tobeanswered.Visible = false;
        }
    }
    protected void lnktobeanswered_Click(object sender, EventArgs e)
    {
        Session["filter"] = "ToBeAnswered";
        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        Session["AllOverdueTask"] = "";
        Response.Redirect("tasks.aspx");
    }
    protected void Btnunapproved_Click(object sender, EventArgs e)
    {
        Session["boolclose"] = true;
        Session["boolcount"] = "false";
        Session["filter"] = "NewUnApprovedCR";
        Session["AllOverdueTask"] = "";
        Response.Redirect("client_requests.aspx", false);

    }
    void getUnApprovedCRbyM()
    {
        sql = @"select ClientRequest.RequestId from ClientRequest where ClientRequest.CRApprove='False' and ClientRequest.Status = 'new' and status <> 'closed' and deleted <> 1 ";
        DataSet ds = DatabaseHelper.getDataset(sql);
        if (ds != null && ds.Tables.Count > 0 && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
           if (DatabaseHelper.CanApprove(Session["admin"].ToString()))
            {
                Btnunapproved.Text = "(" + ds.Tables[0].Rows.Count + ")";
                UnApprovedCR.Visible = true;
            }
            else
            {

                UnApprovedCR.Visible = false;
            }

        }
        else
        {
            UnApprovedCR.Visible = false;
        }
    }
    protected void NewCRCommentinRed_Click(object sender, EventArgs e)
    {
        Session["boolclose"] = true;
        Session["boolcount"] = "false";
        Session["filter"] = "NewUnApprovedCRComment";
        //===============================
        Session["AllOverdueTask"] = "";
        //===============================
        Response.Redirect("client_requests.aspx", false);
    }
    void getUnApprovedCRCommentbyM()
    {

        sql = @"select ClientRequest.* from ClientRequest where  ClientRequest.Status<>'closed' and ClientRequest.deleted <> 1 and RequestId in
                (select Distinct Requestid from ClientRequest_Details where ApprovUnApprov='Approve' and deleted<>1)";

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables.Count > 0 && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {

            if (DatabaseHelper.CanApprove(Session["admin"].ToString()))
            {
                NewCRCommentinRed.Text = "(" + ds.Tables[0].Rows.Count + ")";
                NewCrComment.Visible = true;
            }
            else
            {

                NewCrComment.Visible = false;
            }
        }
        else
        {
            NewCrComment.Visible = false;
        }
    }
}
