<%@ Page Language="C#" AutoEventWireup="true" CodeFile="edit_client.aspx.cs" Inherits="edit_client" %>

<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        Edit client - <%=System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(company_name)%>
    </title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
    function checkCompany()
    {
    var Company =  document.getElementById("txtCompanyName").value;
    var drpComapny = document.getElementById("drpProjects").value;
    if(drpComapny == "other")
    {
    if(Company =="")
    {
    alert("Please enter company name");
    return false;
    }
    else
    {
    var btnsend = document.getElementById("btnAddCompany");
      btnsend.click();  
    }
    }
    else
    {
     var btnsend = document.getElementById("btnAddCompany");
      btnsend.click();  
    }
    
    }
    
    </script>

    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div id="wrapper">
            <uc2:Admin_Header ID="Admin_Header1" runat="server" />
            <div id="Content">
                <%--<div style="float: left">--%>
                <%--==========================Notification====================================================================================--%>
                <%--<span style="color: Black; font-size: 16px" id="Notification" runat="server">Notifications:<asp:LinkButton
                    ID="LnkNotification" runat="server" ForeColor="red" OnClick="LnkNotification_Click"></asp:LinkButton></span>--%>
                <%--==============================================================================================================--%>
                <%--===================================New CRs===========================================================================--%>
                <%--<span style="color: green;" id="NewCR" runat="server">New CRs<asp:LinkButton ID="LnkNewCR"
                    runat="server" OnClick="LnkNewCR_Click"></asp:LinkButton></span>--%>
                <%--==============================================================================================================--%>
                <%--===============================CR(wocr)===============================================================================--%>
                <%--<span style="color: green;"
                        id="CRwocr" runat="server">CR(wocr)<asp:LinkButton ID="LnkCRWocr" runat="server"
                            OnClick="LnkCRWocr_Click"></asp:LinkButton></span>--%>
                <%--==============================================================================================================--%>
                <%--===========================CRs with new comment===================================================================================--%>
                <%--<span style="color: green;"
                                id="CrNewComment" runat="server">CRs with new comment<asp:LinkButton ID="LnkCrNewComment"
                                    runat="server" OnClick="LnkCrNewComment_Click"></asp:LinkButton></span>
                                    <span style="color: green;" id="MakeCommentRead" runat="server">
                    <asp:LinkButton ID="LnkMakecommentRead" runat="server" OnClick="LnkMakecommentRead_Click"></asp:LinkButton></span>--%>
                <%--==============================================================================================================--%>
                <%--==============================================================================================================--%>
                <%--==============================My unanswered questions================================================================================--%>
                <%-- <span style="color: Red;" id="unansweredque" runat="server">My unanswered questions<asp:LinkButton
                    ID="lnkUnansweredQuestions" runat="server" OnClick="lnkUnansweredQuestions_Click"
                    CausesValidation="False"></asp:LinkButton></span>--%>
                <%--==============================================================================================================--%>
                <%--===============================My immediate tasks===============================================================================--%>
                <%--<span style="color: Red;"
                        id="immediatetask" runat="server">My immediate tasks<asp:LinkButton ID="lnkPr0Task"
                            runat="server" OnClick="lnkPr0Task_Click" CausesValidation="False"></asp:LinkButton></span>--%>
                <%--==============================================================================================================--%>
                <%--====================================Overdue tasks==========================================================================--%>
                <%--<span style="color: Red;" id="OverDuetsk" runat="server">Overdue tasks
                        <asp:Button ID="BtnOverdue" runat="server" BackColor="transparent" ForeColor="blue"
                        BorderStyle="none" Style="cursor: pointer; width: auto" CausesValidation="False"
                        OnClick="BtnOverdue_Click" /></span> --%>
                <%--=====================My 1a tasks=========================================================================================--%>
                <%-- <span style="color: #EE82EE;" id="highestTasks"
                            runat="server">My 1a tasks<asp:LinkButton ID="lnkHighestTasks" runat="server" OnClick="lnkHighestTasks_Click"
                                CausesValidation="False"></asp:LinkButton></span>--%>
                <%--============================My high tasks==================================================================================--%>
                <%--&nbsp;<span style="color: Black;"
                                    id="highTasks" runat="server">My high tasks<asp:LinkButton ID="lnkHighTasks" runat="server"  OnClick="lnkHighTasks_Click" CausesValidation="False"></asp:LinkButton></span>--%>
                <%--==============================================================================================================--%>
                <%--======================My 1c tasks========================================================================================--%>
                <%-- <span style="color: Black;" id="PR1CTask" runat="server">My 1c tasks<asp:LinkButton
                    ID="Lnk1cTasks" runat="server" CausesValidation="False" OnClick="Lnk1cTasks_Click"></asp:LinkButton></span>--%>
                <%--==============================================================================================================--%>
                <%--=========================My tasks with new comment=====================================================================================--%>
                <%-- <span style="color:Red;" id="TskNewComment" runat="server">My tasks with new comment<asp:LinkButton
                    ID="LnkTskNewcomment" runat="server" OnClick="LnkTskNewcomment_Click"></asp:LinkButton></span>
                    <span style="color: green;" id="spnMakeTskcmntRead" runat="server">
                    <asp:LinkButton ID="LnkMakeTaskcommentRead" runat="server" OnClick="LnkMakeTaskcommentRead_Click"></asp:LinkButton></span>--%>
                <%--==============================================================================================================--%>
                <%--=======================Tasks with new comment=======================================================================================--%>
                <%--<span style="color: Red;" id="SpnAllTaskNewComment" runat="server">Tasks with new comment<asp:LinkButton
                    ID="LnkAllTsknewComment" runat="server" OnClick="LnkAllTsknewComment_Click"></asp:LinkButton></span>
                <span style="color: green;" id="Span2" runat="server">
                    <asp:LinkButton ID="LnkMakeAllTaskCmmntRead" runat="server" OnClick="LnkMakeAllTaskCmmntRead_Click"></asp:LinkButton></span>--%>
                <%--==============================================================================================================--%>
                <%--&nbsp;--%>
                <%--==============================================================================================================--%>
                <%--</div>--%>
                <%--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--%>
                <div style="float: left">
                    <%--================================Start Without change name code =========================================================================--%>
                    <%--New CRs|CR(wocr)|CRs with new comment|My unanswered questions|My immediate tasks|Overdue tasks|All overdue|My 1a tasks|My high tasks|My 1c tasks|My tasks with new comment|Tasks with new comment
                 
                 Immediate(1) Do now(1) High(3) Normal(4) Low(21) Questions(3) Unread comments(3) Overdue (1)  |  All overdue (2)  |  CR: New(2) Unread comments(4)
                 
                 |CR(wocr)|Tasks with new comment
                 
                 My immediate tasks|My 1a tasks|My high tasks|My 1c tasks|My unanswered questions|My tasks with new comment|Overdue tasks|All overdue|New CRs|CRs with new comment|
                  --%>
                    <%--===============================My immediate tasks===============================================================================--%>
                    <span style="color: Red;" id="immediatetask" runat="server">immediate<asp:LinkButton
                        ID="lnkPr0Task" ForeColor="red" runat="server" OnClick="lnkPr0Task_Click" CausesValidation="False"></asp:LinkButton></span>
                    <%--==============================================================================================================--%>
                    <%--=====================My 1a tasks=========================================================================================--%>
                    <span style="color: #FF1493;" id="highestTasks" runat="server">Do now<asp:LinkButton
                        ID="lnkHighestTasks" ForeColor="#FF1493" runat="server" OnClick="lnkHighestTasks_Click"
                        CausesValidation="False"></asp:LinkButton></span>
                    <%--==================================================================================================================================--%>
                    <%--=====================My high tasks==================================================================================--%>
                    <span style="color: black" id="highTasks" runat="server">High<asp:LinkButton ID="lnkHighTasks"
                        runat="server" OnClick="lnkHighTasks_Click" CausesValidation="False"></asp:LinkButton></span>
                    <%--==============================================================================================================--%>
                    <%--======================My 1c tasks========================================================================================--%>
                    <span style="color: black;" id="PR1CTask" runat="server">Normal<asp:LinkButton ID="Lnk1cTasks"
                        runat="server" CausesValidation="False" OnClick="Lnk1cTasks_Click"></asp:LinkButton></span>
                    <%--==============================================================================================================--%>
                    <%--=======================Low=================================================================================================================--%>
                    <span id="PRlowTask" runat="server" style="color: black">Low<asp:LinkButton ID="LinklowTasks"
                        runat="server" CausesValidation="False" OnClick="LinklowTasks_Click"></asp:LinkButton></span>
                    <%--==============================================================================================================--%>
                    <%--==============================My unanswered questions================================================================================--%>
                    <span style="color: Red;" id="unansweredque" runat="server">Questions<asp:LinkButton
                        ID="lnkUnansweredQuestions" runat="server" OnClick="lnkUnansweredQuestions_Click"
                        CausesValidation="False"></asp:LinkButton></span>
                    <%--==============================================================================================================--%>
                    <%--=========================My tasks with new comment=====================================================================================--%>
                    <span style="color: #FF1493;" id="TskNewComment" runat="server">Unread comments<asp:LinkButton
                        ID="LnkTskNewcomment" CausesValidation="false" ForeColor="#FF1493" runat="server"
                        OnClick="LnkTskNewcomment_Click"></asp:LinkButton></span> <span style="color: #FF1493"
                            id="spnMakeTskcmntRead" runat="server">
                            <asp:LinkButton ID="LnkMakeTaskcommentRead" CausesValidation="false" ForeColor="#FF1493"
                                runat="server" OnClick="LnkMakeTaskcommentRead_Click"></asp:LinkButton></span>
                    <%--==============================================================================================================--%>
                    <%--===================================Overdue tasks===========================================================================--%>
                    <span style="color: red" id="OverDuetsk" runat="server">Overdue<asp:Button ID="BtnOverdue"
                        runat="server" BackColor="transparent" ForeColor="blue" BorderStyle="none" Style="color: red;
                        cursor: pointer; width: auto; padding: 0;" CausesValidation="False" OnClick="BtnOverdue_Click" /></span>
                    <%--==============================================================================================================--%>
                    <%--===================================AllOverDue=============================================================================================--%>
                    <span id="AllOverDuetsk" runat="server" style="color: red"><span style="color: Black;">
                        &nbsp;|&nbsp;</span>All overdue<asp:LinkButton ID="BtnallOverdue" runat="server"
                            BackColor="transparent" BorderStyle="none" CausesValidation="False" Style="width: auto;
                            cursor: pointer; color: red" OnClick="BtnallOverdue_Click"></asp:LinkButton></span>
                    <%--<asp:Button ID="Button1" runat="server" BackColor="transparent" BorderStyle="none" CausesValidation="False" Style="width:auto;cursor:pointer;color:red" onclick="BtnallOverdue_Click"/>--%>
                    <%--==============================================================================================================================--%>
                    <%--===================================New CRs===========================================================================--%>
                    <span style="color: green;" id="NewCR" runat="server"><span style="color: Black">&nbsp;|&nbsp;</span>CR:New<asp:LinkButton
                        ID="LnkNewCR" ForeColor="green" runat="server" OnClick="LnkNewCR_Click"></asp:LinkButton></span>
                    <%--==============================================================================================================--%>
                    <%--============================CRs with new comment===================================================================================--%>
                    <span style="color: black;" id="CrNewComment" runat="server">Unread comments<asp:LinkButton
                        ID="LnkCrNewComment" runat="server" OnClick="LnkCrNewComment_Click"></asp:LinkButton></span>
                    <span style="color: black" id="MakeCommentRead" runat="server">
                        <asp:LinkButton ID="LnkMakecommentRead" runat="server" OnClick="LnkMakecommentRead_Click"></asp:LinkButton></span>
                    <%--==============================================================================================================--%>
                    <%--===============================CR(wocr)===============================================================================--%>
                    <%--<span style="color:green;" id="CRwocr" runat="server">CR(wocr)<asp:LinkButton ID="LnkCRWocr" runat="server" OnClick="LnkCRWocr_Click"></asp:LinkButton></span>--%>
                    <%--==============================================================================================================--%>
                    <%--=======================Tasks with new comment================================================================================================================--%>
                    <span style="color: red;" id="SpnAllTaskNewComment" runat="server"><span style="color: Black">
                        &nbsp;|&nbsp;</span>Tasks with new comment<asp:LinkButton ID="LnkAllTsknewComment"
                            runat="server" ForeColor="red" OnClick="LnkAllTsknewComment_Click"></asp:LinkButton></span>
                    <span style="color: red;" id="Span2" runat="server">
                        <asp:LinkButton ID="LnkMakeAllTaskCmmntRead" ForeColor="red" runat="server" OnClick="LnkMakeAllTaskCmmntRead_Click"></asp:LinkButton></span>
                    <%--==============================================================================================================================================================--%>
                </div>
                <%--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--%>
                <div style="width: 100%; float: left; padding: 5px 0; background: #ebebeb;">
                    &nbsp;<span class="text_default"><a  style="Border-bottom:1px dotted #000;" href="edit_client.aspx">Register new client</a><a
                      style="Border-bottom:1px dotted #000;"  href="client_details.aspx" >&nbsp;|&nbsp;Back to client details</a>&nbsp; <a  style="Border-bottom:1px dotted #000;" href="Company.aspx"
                            id="FrmCompLnk" runat="server">|&nbsp;Back to companies</a></span></div>
                <div style="clear: both">
                </div>
                <div id="DivEntry" runat="server">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="left" valign="top">
                                <table style="width: 424px">
                                    <tr>
                                        <td align="left" class="titleText" valign="top" style="width: 424px; display: none;">
                                            Users&nbsp;|&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="middle" style="width: 424px">
                                            <div id="divLinks" runat="server">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 424px">
                                            <table width="424" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="left" valign="top">
                                                        <img height="16" src="../images/loginBox_top.png" width="420" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="background: url(../images/loginBox_mid.png) repeat-y left top;">
                                                        <div style="text-align: center">
                                                            <table cellpadding="6">
                                                                <tr>
                                                                    <td align="left" class="whitetext1" valign="top">
                                                                    </td>
                                                                    <td width="120" align="left" valign="top" class="whitetext1">
                                                                        Reg. ID:
                                                                    </td>
                                                                    <td align="left" valign="bottom" class="whitetext1">
                                                                        <asp:Label ID="lblId" runat="server">New</asp:Label>
                                                                        <br />
                                                                        <asp:CheckBox ID="chkShwSmp" runat="server" Text="Show SMP" />
                                                                        &nbsp;
                                                                        <asp:CheckBox ID="chkChat" runat="server" Text="Allow Chat" />
                                                                        <br />
                                                                        <asp:CheckBox ID="chkRmtDsktp" runat="server" Text="Allow Remote Desktop" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" class="whitetext1" valign="top">
                                                                    </td>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        Subscription:
                                                                    </td>
                                                                    <td align="left" valign="top">
                                                                        <asp:DropDownList ID="drpsubscription" Width="225px" runat="server">
                                                                        </asp:DropDownList>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="drpsubscription"
                                                                            ErrorMessage="Select subscription.">*</asp:RequiredFieldValidator>
                                                                        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="server" TargetControlID="RequiredFieldValidator8">
                                                                        </cc1:ValidatorCalloutExtender>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" class="whitetext1" valign="top">
                                                                    </td>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        Username:
                                                                    </td>
                                                                    <td align="left" valign="top">
                                                                        <asp:TextBox ID="txtUserName" runat="server" Width="220px" MaxLength="50"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Enter username."
                                                                            ControlToValidate="txtUserName">*</asp:RequiredFieldValidator>
                                                                        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator1">
                                                                        </cc1:ValidatorCalloutExtender>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" class="whitetext1" valign="top">
                                                                    </td>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        First name:
                                                                    </td>
                                                                    <td align="left" valign="top">
                                                                        <asp:TextBox ID="txtFirstName" runat="server" Width="220px" MaxLength="50"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Enter first name."
                                                                            ControlToValidate="txtFirstName">*</asp:RequiredFieldValidator>
                                                                        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="server" TargetControlID="RequiredFieldValidator2">
                                                                        </cc1:ValidatorCalloutExtender>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" class="whitetext1" valign="top">
                                                                    </td>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        Surname:
                                                                    </td>
                                                                    <td align="left" valign="top">
                                                                        <asp:TextBox ID="txtSurname" runat="server" Width="220px" MaxLength="50"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Enter surname."
                                                                            ControlToValidate="txtSurname">*</asp:RequiredFieldValidator>
                                                                        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="server" TargetControlID="RequiredFieldValidator3">
                                                                        </cc1:ValidatorCalloutExtender>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" class="whitetext1" valign="top">
                                                                    </td>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        Company name:
                                                                    </td>
                                                                    <td align="left" valign="top">
                                                                        <asp:DropDownList ID="drpProjects" runat="server" CssClass="filerDrpodown" AutoPostBack="True"
                                                                            OnSelectedIndexChanged="drpProjects_SelectedIndexChanged" Width="225px">
                                                                        </asp:DropDownList>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="drpProjects"
                                                                            ErrorMessage="Please select company name.">*</asp:RequiredFieldValidator>
                                                                        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="server" TargetControlID="RequiredFieldValidator7">
                                                                        </cc1:ValidatorCalloutExtender>
                                                                        <table id="TDAddNewComapny" runat="server" style="border: solid 1px #999999; padding-top: 10px">
                                                                            <tr style="padding-top: 7px">
                                                                                <td>
                                                                                    <asp:TextBox ID="txtCompanyName" runat="server" Width="290px" MaxLength="200"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="right">
                                                                                    <asp:Button ID="btnAddCompany" runat="server" Text="Add" CausesValidation="false"
                                                                                        OnClick="btnAddCompany_Click" />
                                                                                    <asp:Button ID="btnclosecompany" runat="server" Text="Close" CausesValidation="False"
                                                                                        OnClick="btnclosecompany_Click" />
                                                                                    <br />
                                                                                    <span style="padding-right: 100px">
                                                                                        <asp:Label ID="lblcompanymsg" runat="server" ForeColor="Red" Text=""></asp:Label>
                                                                                    </span>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <%--==============================================================--%>
                                                                <tr>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                    </td>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        Project name:
                                                                    </td>
                                                                    <td align="left" valign="top" class="whitetext2">
                                                                        <asp:DropDownList ID="drpNewProjects" runat="server" CssClass="filerDrpodown" AutoPostBack="True"
                                                                            OnSelectedIndexChanged="drpNewProjects_SelectedIndexChanged" Width="222px">
                                                                        </asp:DropDownList>
                                                                        <asp:RequiredFieldValidator ID="ReqdrpNewProjects" runat="server" ControlToValidate="drpNewProjects"
                                                                            ErrorMessage="Please enter project name.">*</asp:RequiredFieldValidator>
                                                                        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="server" TargetControlID="ReqdrpNewProjects">
                                                                        </cc1:ValidatorCalloutExtender>
                                                                        <table id="TDAddNewproject" runat="server" style="border: solid 1px #999999; padding-top: 10px">
                                                                            <tr style="padding-top: 7px">
                                                                                <td>
                                                                                    <asp:TextBox ID="txtprojectname" runat="server" Width="290px" MaxLength="200"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="right">
                                                                                    <asp:Button ID="btnaddProject" runat="server" Text="Add" CausesValidation="false"
                                                                                        OnClick="btnaddProject_Click" />
                                                                                    <asp:Button ID="btncloseProject" runat="server" Text="Close" CausesValidation="False"
                                                                                        OnClick="btncloseProject_Click" />
                                                                                    <br />
                                                                                    <span style="padding-right: 100px">
                                                                                        <asp:Label ID="lblprojectmsg" runat="server" ForeColor="Red" Text=""></asp:Label>
                                                                                    </span>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <%--===============================================================--%>
                                                                <tr>
                                                                    <td align="left" class="whitetext1" valign="top">
                                                                    </td>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        Support type:
                                                                    </td>
                                                                    <td align="left" valign="top">
                                                                        <asp:DropDownList ID="DrpSupportType" Width="225px" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" class="whitetext1" valign="top">
                                                                    </td>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        Phone:
                                                                    </td>
                                                                    <td align="left" valign="top">
                                                                        <asp:TextBox ID="txtPhone" runat="server" Width="220px" MaxLength="16"></asp:TextBox>
                                                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Enter phone number." ControlToValidate="txtPhone">*</asp:RequiredFieldValidator>
                                                                        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="server" TargetControlID="RequiredFieldValidator5"></cc1:ValidatorCalloutExtender>--%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" class="whitetext1" valign="top">
                                                                    </td>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        Email address:
                                                                    </td>
                                                                    <td align="left" valign="top">
                                                                        <asp:TextBox ID="txtEmail" runat="server" Width="220px" MaxLength="50"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="Dynamic"
                                                                            ErrorMessage="Enter valid email address" ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Enter email address."
                                                                            ControlToValidate="txtEmail" Display="Dynamic">*</asp:RequiredFieldValidator>
                                                                        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="server" TargetControlID="RequiredFieldValidator6">
                                                                        </cc1:ValidatorCalloutExtender>
                                                                        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="server" TargetControlID="RegularExpressionValidator1">
                                                                        </cc1:ValidatorCalloutExtender>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" class="whitetext1" valign="top">
                                                                    </td>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        Website:
                                                                    </td>
                                                                    <td align="left" valign="top">
                                                                        <asp:TextBox ID="txtWebsite" runat="server" Width="220px" MaxLength="50"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" class="whitetext1" valign="top">
                                                                    </td>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        Password:
                                                                    </td>
                                                                    <td align="left" valign="top">
                                                                        <asp:TextBox ID="txtPassword" runat="server" MaxLength="50" TextMode="Password" Width="220px"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtPassword"
                                                                            ErrorMessage="Enter password.">*</asp:RequiredFieldValidator>
                                                                        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" TargetControlID="RequiredFieldValidator4">
                                                                        </cc1:ValidatorCalloutExtender>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" class="whitetext1" valign="top">
                                                                    </td>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        Confirm password:
                                                                    </td>
                                                                    <td align="left" valign="top">
                                                                        <asp:TextBox ID="txtConfirmPassword" runat="server" MaxLength="50" TextMode="Password"
                                                                            Width="220px"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtConfirmPassword"
                                                                            Display="Dynamic" ErrorMessage="Enter password to confirm.">*</asp:RequiredFieldValidator>
                                                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtPassword"
                                                                            ControlToValidate="txtConfirmPassword" Display="Dynamic" ErrorMessage="Password entered does not match.">*</asp:CompareValidator>
                                                                        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server" TargetControlID="RequiredFieldValidator9">
                                                                        </cc1:ValidatorCalloutExtender>
                                                                        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="server" TargetControlID="CompareValidator1">
                                                                        </cc1:ValidatorCalloutExtender>
                                                                    </td>
                                                                </tr>
                                                                <%--============================================================--%>
                                                                <tr>
                                                                    <td align="left" class="whitetext1" valign="top">
                                                                    </td>
                                                                    <td width="120" align="left" valign="top" class="whitetext1">
                                                                        Email notify:
                                                                    </td>
                                                                    <td align="left" valign="bottom" class="whitetext1">
                                                                        <asp:CheckBox ID="Chkemailnotify" runat="server"  />
                                                                    </td>
                                                                </tr>
                                                                <%--========================================--%>
                                                                <tr>
                                                                <td align="left" class="whitetext1" valign="top">
                                                                    </td>
                                                                <td width="120" align="left" valign="top" class="whitetext1">
                                                                        Auto migrate comments to tasks:
                                                                </td>
                                                                <td align="left" valign="bottom" class="whitetext1">
                                                                        <asp:CheckBox ID="ChkmigratetoTask"  runat="server" />
                                                                </td>
                                                                </tr>
                                                                <%--========================================--%>
                                                                <tr>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                    </td>
                                                                    <td width="120" align="left" valign="top" class="whitetext1">
                                                                    </td>
                                                                    <td align="left" valign="bottom" class="whitetext1">
                                                                        Contactable by:
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" class="whitetext1" valign="top">
                                                                    </td>
                                                                    <td width="120" align="left" valign="top" class="whitetext1">
                                                                        Anyone
                                                                    </td>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        <asp:DropDownList ID="dranyone" runat="server">
                                                                            <asp:ListItem Selected="True" Value="not contactable">not contactable</asp:ListItem>
                                                                            <asp:ListItem Value="Change request">Change request</asp:ListItem>
                                                                            <asp:ListItem Value="telephone and email">telephone and email</asp:ListItem>
                                                                            <asp:ListItem Value="telephone only">telephone only</asp:ListItem>
                                                                            <asp:ListItem Value="e-mail only">e-mail only</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                    </td>
                                                                    <td width="120" align="left" valign="top" class="whitetext1">
                                                                        Emp task is assigned to
                                                                    </td>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        <asp:DropDownList ID="dremp" runat="server">
                                                                            <asp:ListItem Selected="True" Value="not contactable">not contactable</asp:ListItem>
                                                                            <asp:ListItem Value="Change request">Change request</asp:ListItem>
                                                                            <asp:ListItem Value="telephone and email">telephone and email</asp:ListItem>
                                                                            <asp:ListItem Value="telephone only">telephone only</asp:ListItem>
                                                                            <asp:ListItem Value="e-mail only">e-mail only</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                    </td>
                                                                    <td width="120" align="left" valign="top" class="whitetext1">
                                                                        FHS Lead
                                                                    </td>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        <asp:DropDownList ID="drfhslead" runat="server">
                                                                            <asp:ListItem Selected="True" Value="not contactable">not contactable</asp:ListItem>
                                                                            <asp:ListItem Value="Change request">Change request</asp:ListItem>
                                                                            <asp:ListItem Value="telephone and email">telephone and email</asp:ListItem>
                                                                            <asp:ListItem Value="telephone only">telephone only</asp:ListItem>
                                                                            <asp:ListItem Value="e-mail only">e-mail only</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                    </td>
                                                                    <td width="120" align="left" valign="top" class="whitetext1">
                                                                        UK</td>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        <asp:DropDownList ID="druk" runat="server">
                                                                            <asp:ListItem Selected="True" Value="telephone and email">telephone and email</asp:ListItem>
                                                                            <asp:ListItem Value="Change request">Change request</asp:ListItem>
                                                                            <asp:ListItem Value="telephone only">telephone only</asp:ListItem>
                                                                            <asp:ListItem Value="e-mail only">e-mail only</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                    </td>
                                                                    <td width="120" align="left" valign="top" class="whitetext1">
                                                                        Show client requests</td>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        <asp:CheckBoxList ID="chklist" runat="server" TextAlign="Right"   RepeatColumns="4" RepeatDirection="Horizontal">
                                                                        </asp:CheckBoxList>
                                                                    </td>
                                                                </tr>
                                                                <%--=============================================================--%>
                                                                <tr>
                                                                    <td align="left" valign="top">
                                                                    </td>
                                                                    <td align="left" valign="top">
                                                                    </td>
                                                                    <td align="right" valign="top">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 100px">
                                                        <img height="16" src="../images/loginBox_btm.png" width="420" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <div id="divMessage" runat="server" style="color: Red; font-weight: bold">
                                            </div>
                                            <div align="right" style="float: right;">
                                                <asp:Button ID="btnSubmit" runat="server" Text="Create" OnClick="btnSubmit_Click"
                                                    CssClass="blueBtns" /></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 424px">
                                            <div id="div1" runat="server" style="color: Red; font-weight: bold;">
                                                <asp:HiddenField ID="ClientUsername" runat="server" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <br />
                <div>
                </div>
            </div>
        </div>
        <uc1:footer ID="Footer1" runat="server" />
    </form>
</body>
</html>
