using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class edit_client : System.Web.UI.Page
{
    int id;
    string sql = "";
    public string company_name = "";
    
    protected void Page_Load(object sender, EventArgs e)
    {
     btnAddCompany.Attributes.Add("Onclick", "return checkCompany();");
      ///  btnSubmit.Attributes.Add("Onclick", "return checkCompany();");

        LnkMakecommentRead.Attributes.Add("onclick", "return confirm('Sure you want to mark every comment in every CR as read by you?');");
        LnkMakeTaskcommentRead.Attributes.Add("onclick", "javascript:return confirm('Sure you want to mark every comment in each of your tasks as read by you (this means you must have read them all and acted on all that require action already)?');");
        LnkMakeAllTaskCmmntRead.Attributes.Add("onclick", "javascript:return confirm('Sure you want to mark every comment in each of  tasks as read by you (this means you must have read them all and acted on all that require action already)?');");
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
           //Session["returnUrl"] = (Request.QueryString["id"] == null || Request.QueryString["id"].ToString() == "" ? "tasks.aspx" : "edit_user.aspx?id=" + Request.QueryString["id"].ToString());
            Session["returnUrl"] = "add_client.aspx";
            Response.Redirect("login.aspx");
            return;
        }
        try
        {
            if (Request.QueryString["Comp"] == null || Request.QueryString["Comp"].ToString() == "")
            {
                FrmCompLnk.Visible = false;
            }
            else if (Request.QueryString["Comp"].ToString() == "True")
            {
                FrmCompLnk.Visible = true;
            }
        }
        catch { }

        divMessage.InnerHtml = "";

        string var = Request.QueryString["id"];
        if (var == null)
        {
            id = 0;
            this.Title = "Add client";
        }
        else
        {
            try
            {
                id = Convert.ToInt32(var);
                this.Title = "Edit client";
            }
            catch
            {
                divMessage.InnerHtml = "User ID must be an integer.";
                DivEntry.Visible = false;
                return;
            }
        }

        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            object objUserId = DatabaseHelper.executeScalar("select user_id from users where username = '" + Session["admin"].ToString() + "'");

            if (id.ToString() != objUserId.ToString())
            {
                Response.Redirect("tasks.aspx");
                return;
            }
            divLinks.Visible = false;
        }

        if (!IsPostBack)
        {

            //=========================================
            Loadprogect_drp();
            TDAddNewproject.Visible = false;
            Bindcheckboxlist();
           
            //=========================================

            load_Userdropdowns();
            loadCompany_dropdowns();
            LoadSupportType_Dropdown();
            TDAddNewComapny.Visible = false;
            ////txtCompanyName.Visible = false;


            if (id == 0)
            {
                clearControls();
                drpsubscription.SelectedValue = "ND";  
                
            }
            else
            {

                string strSql = "select * from NonesCRMusers where RegId=" + id;
                DataSet ds = DatabaseHelper.getDataset(strSql);

                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        lblId.Text = ds.Tables[0].Rows[0]["RegId"].ToString();
                        txtUserName.Text = ds.Tables[0].Rows[0]["UserName"].ToString();
                        ClientUsername.Value = ds.Tables[0].Rows[0]["UserName"].ToString();
                        txtPassword.Text = ds.Tables[0].Rows[0]["Password"].ToString();
                        string pwd = ds.Tables[0].Rows[0]["Password"].ToString();
                        txtPassword.Attributes.Add("value", pwd);
                        txtConfirmPassword.Attributes.Add("value", pwd);
                        ///txtPassword.Attributes.Add("value","ghg") = ds.Tables[0].Rows[0]["Password"].ToString();
                        ////txtPassword.Attributes("value") = ds.Tables[0].Rows[0]["Password"].ToString();
                        txtFirstName.Text = ds.Tables[0].Rows[0]["FirstName"].ToString();
                        txtSurname.Text = ds.Tables[0].Rows[0]["Surname"].ToString();
                        txtCompanyName.Text = ds.Tables[0].Rows[0]["CompanyName"].ToString();
                        company_name = ds.Tables[0].Rows[0]["CompanyName"].ToString();
                        drpProjects.SelectedValue = ds.Tables[0].Rows[0]["CompanyName"].ToString();
                        txtPhone.Text = ds.Tables[0].Rows[0]["Phone"].ToString();
                        txtEmail.Text = ds.Tables[0].Rows[0]["Email"].ToString();
                        txtWebsite.Text = ds.Tables[0].Rows[0]["WebSite"].ToString();
                        //=======================================================================
                        //***********************************************************************
                        //=======================================================================
                        if (ds.Tables[0].Rows[0]["Email_Notify"].ToString() != "")
                        {
                            if (ds.Tables[0].Rows[0]["Email_Notify"].ToString() == "True")
                            {
                                Chkemailnotify.Checked = true;
                            }
                            else
                            {
                                Chkemailnotify.Checked = false;
                            }
                        }
                        if (ds.Tables[0].Rows[0]["Migrate_CR_comments_Tasks"].ToString() != "")
                        {
                            if (ds.Tables[0].Rows[0]["Migrate_CR_comments_Tasks"].ToString() == "True")
                            {
                                ChkmigratetoTask.Checked = true;
                            }
                            else
                            {
                                ChkmigratetoTask.Checked = false;
                            }
                        }
                        try
                        {
                            LoadNewProject(ds.Tables[0].Rows[0]["CompanyName"].ToString());
                            dranyone.SelectedValue = ds.Tables[0].Rows[0]["Anyone"].ToString();
                            dremp.SelectedValue = ds.Tables[0].Rows[0]["Emptask_assign"].ToString();
                            drfhslead.SelectedValue = ds.Tables[0].Rows[0]["FHSLead"].ToString();
                            druk.SelectedValue = ds.Tables[0].Rows[0]["UK"].ToString();
                            drpNewProjects.SelectedValue = ds.Tables[0].Rows[0]["ProjectName"].ToString();
                            string struser = "";
                            string[] strToUser = ds.Tables[0].Rows[0]["ShowRequest"].ToString().Split(',');
                            for (int icount = 0; icount <= chklist.Items.Count - 1; icount++)
                            {

                                for (int inum = 0; inum < strToUser.Length; inum++)
                                {
                                    if (chklist.Items[icount].Text.ToString().Trim() == strToUser[inum].ToString().Trim())
                                    {
                                        chklist.Items[icount].Selected = true;

                                    }
                                }
                            }
                            
                            
                       }
                        catch (Exception ex)
                        { }

                        //=========================================================================
                        //***********************************************************************
                        //=========================================================================

                        try
                        {
                            DrpSupportType.SelectedValue = ds.Tables[0].Rows[0]["SupportUrl"].ToString();
                        }
                        catch { }
                        if (ds.Tables[0].Rows[0]["Subscription"].ToString() !="")
                        {
                            drpsubscription.SelectedValue = ds.Tables[0].Rows[0]["Subscription"].ToString();
                        }
                        if (ds.Tables[0].Rows[0]["ShowSMP"].ToString() != "")
                        {
                            if (ds.Tables[0].Rows[0]["ShowSMP"].ToString() == "True")
                            {
                                chkShwSmp.Checked = true;
                            }
                            else
                            {
                                chkShwSmp.Checked = false;
                            }

                        }
                        else
                        {
                            chkShwSmp.Checked = false;
                        }

                        if (ds.Tables[0].Rows[0]["AllowChat"].ToString() != "")
                        {
                            if (ds.Tables[0].Rows[0]["AllowChat"].ToString() == "True")
                            {
                                chkChat.Checked = true;
                            }
                            else
                            {
                                chkChat.Checked = false;
                            }

                        }
                        else
                        {
                            chkChat.Checked = false;
                        }

                        if (ds.Tables[0].Rows[0]["AllowRemotDesktop"].ToString() != "")
                        {
                            if (ds.Tables[0].Rows[0]["AllowRemotDesktop"].ToString() == "True")
                            {
                                chkRmtDsktp.Checked = true;
                            }
                            else
                            {
                                chkRmtDsktp.Checked = false;
                            }

                        }
                        else
                        {
                            chkRmtDsktp.Checked = false;
                        }

                        btnSubmit.Text = "Update";

                        txtUserName.Enabled = true;
                    }
                    else
                    {
                        divMessage.InnerHtml = "Client details was not found.";
                        DivEntry.Visible = false;
                    }
                }
                else
                {
                    divMessage.InnerHtml = "Client details was not found.";
                    DivEntry.Visible = false;
                }
            }

            getNewCR();
            getCRNewComment();
            //getCRwaitingfrmClient();
            getUnasweredQuestions();
            getPR0Tasks();
            getPR1aTasks();
            getPR1bTasks();
            getPR1cTasks();
            //====================
             getPRLowTasks();
             getAllOverduetasks();
            //====================
             getTskNewComment();
             getAllTskNewComment();
             //getAllNotification();
             getOverduetasks();
             

        }
           
    }
    protected int getHoursTakenSoFar(string task_id)
    {
        sql = @"select sum(datediff(minute,started_date,finished_date))
                from hours_reporting
                where finished_date is not null and task_id=" + task_id;

        int hrsTaken = 0;
        object objHrsTaken = DatabaseHelper.executeScalar(sql);
        if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
        {
            hrsTaken += (int)objHrsTaken;
        }
        else
        {
            hrsTaken = 0;
        }

        sql = @"select sum(datediff(minute,started_date,getdate()))
                from hours_reporting
                where finished_date is null and task_id=" + task_id;

        objHrsTaken = DatabaseHelper.executeScalar(sql);

        if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
        {
            hrsTaken += (int)objHrsTaken;
        }

        return hrsTaken;
    }
    void getOverduetasks()
    {

        try
        {

            int GvnETC = 0;
            int j = 0;
            DateTime crrntdate = DateTime.Now.Date;
            DateTime duedate;
            sql = @"select tasks.* from tasks 
                        where tasks.deleted <> 1 and  status <> 'closed' and  status <> 'checked' and status <>'parked'";

            //if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
            //{
            sql += " and assigned_to_user = '" + Session["admin"] + "' ";
            //}

            DataSet ds = DatabaseHelper.getDataset(sql);

            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //=========================================================================================
                    //string strcrrntdate = DateTime.Parse(DateTime.Now.Date.ToString()).ToString("dd/MM/yyyy");
                    string strcrrntdate = string.Format("{0:dd/MM/yyyy}", DateTime.Now.Date.ToString()).ToString();
                    //=========================================================================================
                    string djgfjs = ds.Tables[0].Rows[i]["DueDate"].ToString();
                    if (ds.Tables[0].Rows[i]["DueDate"].ToString() != null && ds.Tables[0].Rows[i]["DueDate"].ToString() != "")
                    {

                        //=====================================================================================================
                        string strduedate = string.Format("{0:dd/MM/yyyy}", ds.Tables[0].Rows[i]["DueDate"].ToString()).ToString();
                        duedate = DateTime.Parse(strduedate.ToString());
                        crrntdate = DateTime.Parse(strcrrntdate.ToString());

                        //string strduedate = DateTime.Parse(ds.Tables[0].Rows[i]["DueDate"].ToString()).ToString("dd/MM/yyyy");
                        //duedate = Convert.ToDateTime(strduedate.ToString());
                        //crrntdate = Convert.ToDateTime(strcrrntdate.ToString());
                        //======================================================================================================                        
                        if (DateTime.Compare(duedate, crrntdate) < 0)
                        {

                            j++;

                        }
                    }

                }


                if (j > 0)
                {
                    BtnOverdue.Text = "(" + j.ToString() + ")";
                    OverDuetsk.Visible = true;
                }
                else
                {
                    BtnOverdue.Visible = false;
                    OverDuetsk.Visible = false;
                }


            }
            else
            {
                BtnOverdue.Visible = false;
                OverDuetsk.Visible = false;
            }

        }
        catch { }

    }

//    void getOverduetasks()
//    {

//        try
//        {

//            int GvnETC = 0;
//            int j = 0;
//            DateTime crrntdate = DateTime.Now.Date;
//            DateTime duedate;
//            sql = @"select tasks.* from tasks 
//                        where tasks.deleted <> 1 and  status <> 'closed' and  status <> 'checked' and status <>'parked'";

//            if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
//            {
//                sql += " and assigned_to_user = '" + Session["admin"] + "' ";
//            }

//            DataSet ds = DatabaseHelper.getDataset(sql);

//            if (ds != null && ds.Tables[0].Rows.Count > 0)
//            {


//                ////BtnOverdue.Text = "(" + ds.Tables[0].Rows.Count + ")";
//                ////immediatetask.Visible = true;

//                ////for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
//                ////{
//                ////    int ETC = Convert.ToInt32(ds.Tables[0].Rows[i]["ETC"].ToString());

//                ////    ///GvnETC = "" + (ETC / 60) + " hrs " + (ETC % 60) + " mins";

//                ////    int hrsTaken = getHoursTakenSoFar(ds.Tables[0].Rows[i]["task_id"].ToString());

//                ////   //// e.Item.Cells[14].Text = "" + (hrsTaken / 60) + " hrs " + (hrsTaken % 60) + " mins";

//                ////    ///int hrsLeft = ETC - hrsTaken;

//                ////    if(hrsTaken > ETC)
//                ////    {
//                ////        j++;
//                ////    }
//                ////}



//                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
//                {
//                    string strcrrntdate = DateTime.Parse(DateTime.Now.Date.ToString()).ToString("dd/MM/yyyy");
//                    string djgfjs = ds.Tables[0].Rows[i]["DueDate"].ToString();
//                    if (ds.Tables[0].Rows[i]["DueDate"].ToString() != null && ds.Tables[0].Rows[i]["DueDate"].ToString() != "")
//                    {
//                        string strduedate = DateTime.Parse(ds.Tables[0].Rows[i]["DueDate"].ToString()).ToString("dd/MM/yyyy");
//                        duedate = DateTime.Parse(strduedate.ToString());
//                        crrntdate = DateTime.Parse(strcrrntdate.ToString());
//                        if (DateTime.Compare(duedate, crrntdate) < 0)
//                        {

//                            j++;

//                        }


//                    }

//                }


//                if (j > 0)
//                {
//                    BtnOverdue.Text = "(" + j.ToString() + ")";
//                    OverDuetsk.Visible = true;
//                }
//                else
//                {
//                    BtnOverdue.Visible = false;
//                    OverDuetsk.Visible = false;
//                }


//            }
//            else
//            {
//                BtnOverdue.Visible = false;
//                OverDuetsk.Visible = false;
//            }

//        }
//        catch { }

//    }


    protected void BtnOverdue_Click(object sender, EventArgs e)
    {

        Session["filter"] = " ";
        Session["filterunread"] = "";
        Session["OverdueTask"] = "True";
        Response.Redirect("tasks.aspx");


    }



    void getPR1cTasks()
    {

        sql = @"select tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '1c - normal' ";
        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            sql += " and  status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + Session["admin"] + "' ";
        }
        else
        {
            sql += " and  status <> 'closed' and  status <> 'checked' and status <>'parked'  and assigned_to_user = '" + Session["admin"] + "' ";
        }

        DataSet ds = DatabaseHelper.getDataset(sql);
        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            Lnk1cTasks.Text = "(" + ds.Tables[0].Rows.Count + ")";
            PR1CTask.Visible = true;
        }
        else
        {
            Lnk1cTasks.Text = "(0)";
            PR1CTask.Visible = false;
        }
    }

    protected void Lnk1cTasks_Click(object sender, EventArgs e)
    {
        Session["filter"] = "1c - normal";
        Session["filterunread"] = "";
        Session["filterunread"] = "";
        Session["OverdueTask"] = null;
        Response.Redirect("tasks.aspx");
    }

//    void getAllNotification()
//    {


//        sql = @"select distinct Company_name from Company_notes 
//                where Allow_Notes ='True' and Notes !=''";



//        DataSet ds = DatabaseHelper.getDataset(sql);

//        if (ds != null && ds.Tables[0].Rows.Count > 0)
//        {


//            ////LnkNotification.Text = "(" + ds.Tables[0].Rows.Count + ")";
//            Notification.Visible = true;

//        }
//        else
//        {

//            Notification.Visible = true;
//            LnkNotification.Text = "";


//        }


//    }

    protected void LnkNotification_Click(object sender, EventArgs e)
    {
        Response.Redirect("Notification.aspx", false);
    }

    void LoadSupportType_Dropdown()
    {


        sql = @"select * from Type  order by Type_name";
        // do a batch of sql statements
        DataSet ds_Supportdropdowns = DatabaseHelper.getDataset(sql);
        try
        {
            DrpSupportType.DataSource = ds_Supportdropdowns.Tables[0];
            DrpSupportType.DataTextField = "Type_name";
            DrpSupportType.DataValueField = "Support_Url";
            DrpSupportType.DataBind();
            DrpSupportType.Items.Insert(0, new ListItem("[Select]", ""));
            ///drpProjects.Items.Insert(1, new ListItem("[Other]", "other"));
        }
        catch { }

       
    }

    void loadCompany_dropdowns()
    {

        // projects
////////////        sql = @"select project_name
////////////		from projects
////////////		where active = 'Y' order by project_name;";

        sql = @"select Company_Name
		from Company
		where active = 'Y' order by Company_Name;";
        // do a batch of sql statements
        DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);
        try
        {
            drpProjects.DataSource = ds_dropdowns.Tables[0];
            drpProjects.DataTextField = "Company_Name";
            drpProjects.DataValueField = "Company_Name";
            drpProjects.DataBind();
            drpProjects.Items.Insert(0, new ListItem("[no filter]", ""));
            drpProjects.Items.Insert(1, new ListItem("[Other]", "other"));
        }
        catch { }

       
    }


    void load_Userdropdowns()
    {
        // projects
        drpsubscription.Items.Clear();
        sql = @"select *
		from Client_subscription  order by Subscription;";

        // do a batch of sql statements
        DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);
        if (ds_dropdowns.Tables[0].Rows.Count > 0)
        {
            drpsubscription.DataSource = ds_dropdowns.Tables[0];
            drpsubscription.DataTextField = "Subscription";
            drpsubscription.DataValueField = "Subscription";
            drpsubscription.DataBind();
            drpsubscription.Items.Insert(0, new ListItem("[No subscription]", ""));
        }
        else
        {
            drpsubscription.Items.Insert(0, new ListItem("[No subscription]", ""));
        }
    }


    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
                       
            bool flag = false;
            string strMsg = "";
            if (txtUserName.Text.Trim() == "")
            {
                strMsg += "- Username is required.<br>";
                flag = true;
            }
            if (txtPassword.Text.Trim() == "")
            {
                strMsg += "- Password is required.<br>";
                flag = true;
            }
            if (txtConfirmPassword.Text.Trim() == "")
            {
                strMsg += "- Confirm password is required.<br>";
                flag = true;
            }
            else
            {
                if (txtPassword.Text.Trim() != txtConfirmPassword.Text.Trim())
                {
                    strMsg += "- Confirm password must match password.<br>";
                    flag = true;
                }
            }
            if (txtFirstName.Text.Trim() == "")
            {
                strMsg += "- First name is required.<br>";
                flag = true;
            }
            if (txtSurname.Text.Trim() == "")
            {
                strMsg += "- Surname is required.<br>";
                flag = true;
            }
           
            //if (txtPhone.Text.Trim() == "")
            //{
            //    strMsg += "- Phone name is required.<br>";
            //    flag = true;
            //}

            if (txtEmail.Text.Trim() == "")
            {
                strMsg += "- Email is required.<br>";
                flag = true;
            }
            else
            {
                if (!DatabaseHelper.validate_email(txtEmail.Text.Trim()))
                {
                    strMsg += "- Format of email address is invalid.<br>";
                    flag = true;
                }
            }

            if (flag)
            {
                divMessage.InnerHtml = strMsg;
                return;
            }

            if (btnSubmit.Text == "Create")
            {
                sql = "select count(*) from NonesCRMusers where UserName ='" + txtUserName.Text.Trim().Replace("'", "''") + "'";

                object objResult = DatabaseHelper.executeScalar(sql);


                if (objResult.ToString() == "0")
                {
                    string supportname = "";
                    string supportUrl = "";
                    if(DrpSupportType.SelectedValue != "")
                    {
                        supportname = DrpSupportType.SelectedItem.ToString();
                        supportUrl = DrpSupportType.SelectedValue.ToString();
                    }

                  
                    
                    //===============================================================
                    sql = "insert into NonesCRMusers(UserName,Password,FirstName,Surname,CompanyName,ProjectName,Phone,Email,ClientId,WebSite,RegDate,Email_Notify,Migrate_CR_comments_Tasks,Anyone,Emptask_assign,FHSLead,UK,ShowRequest,ShowSMP,Subscription,AllowChat,AllowRemotDesktop,supportType,SupportUrl) ";
                    //===============================================================
                    
                    sql += "values('" + txtUserName.Text.Trim().Replace("'", "''") + "',";
                    sql += "'" + txtPassword.Text.Trim().Replace("'", "''") + "',";
                    sql += "'" + txtFirstName.Text.Trim().Replace("'", "''") + "',";
                    sql += "'" + txtSurname.Text.Trim().Replace("'", "''") + "',";
                    sql += "'" + txtCompanyName.Text.Trim().Replace("'", "''") + "',";
                    //===============================================================
                    sql += "'" + drpNewProjects.Text.Trim().Replace("'", "''") + "',";
                    //===============================================================
                    sql += "'" + txtPhone.Text.Trim().Replace("'", "''") + "',";
                    sql += "'" + txtEmail.Text.Trim().Replace("'", "''") + "',";
                    sql += "0,";
                    sql += "'" + txtWebsite.Text.Trim().Replace("'", "''") + "',";
                    sql += "getdate(),";
                    if (Chkemailnotify.Checked)
                    {
                        sql += "'True',";
                    }
                    else
                    {
                        sql += "'False',";
                    }
                    if (ChkmigratetoTask.Checked)
                    {
                        sql += "'True',";
                    }
                    else
                    {
                        sql += "'False',";
                    }
                    //===============================================================
                    //===============================================================
                    sql += "'" + dranyone.SelectedItem.Text.Trim() + "',";
                    sql += "'" + dremp.SelectedItem.Text.Trim() + "',";
                    sql += "'" + drfhslead.SelectedItem.Text.Trim() + "',";
                    sql += "'" + druk.SelectedItem.Text.Trim() + "',";
                    string struser = "";
                    for (int icount = 0; icount <= chklist.Items.Count - 1; icount++)
                    {
                        if (chklist.Items[icount].Selected)
                        {
                            struser += chklist.Items[icount].Text.ToString() + ",";
                        }
                    }
                    if (struser.ToString() != "")
                    {
                        sql += "'" + struser.TrimEnd(new char[] { ',' }) + "',";
                    }
                    else
                    {
                        sql += "'" + struser + "',";
                    }
                    //===============================================================
                    //===============================================================
                    if (chkShwSmp.Checked)
                    {
                        sql += "'True',";
                    }
                    else
                    {
                        sql += "'False',";
                    }

                    sql += "'" + drpsubscription.SelectedItem.Text.Trim().Replace("'", "''") + "',";
                    if (chkChat.Checked)
                    {
                        sql += "'True',";
                    }
                    else
                    {
                        sql += "'False',";
                    }
                     if (chkRmtDsktp.Checked)
                    {
                        sql += "'True')";
                    }
                    else
                    {
                        sql += "'False','" + supportname.ToString() + "','" + supportUrl.ToString() + "')";
                    }
                   
                    int intResult = DatabaseHelper.executeNonQuery(sql);

                    if (intResult != 0)
                    {
                        string strBody = generateEmail();
                        //==========================================================================================================================================
                        flag = DatabaseHelper.sendEmailChangeRequestNorply(ConfigurationManager.AppSettings["ToEmail1"].ToString(), "Registration details", strBody);
                        //=========================================================================================================================================== 
                       
                       if (Chkemailnotify.Checked)
                       {
                          flag = DatabaseHelper.sendEmailChangeRequestNorply(txtEmail.Text.Trim(), "Registration details", strBody);
                          
                       }
                        Session["username"] = txtUserName.Text.Trim();
                        Session["ClientId"] = "0";
                        Session["ESCRMuser"] = "false";
                        divMessage.InnerHtml = "Client details was saved.";
                        //Response.Redirect("confirmation.aspx", false);
                    }
                    else
                    {
                        divMessage.InnerHtml = "There was problem in saving client details. Please try again.";
                        divMessage.Visible = true;
                    }
                }
                else
                {
                    divMessage.InnerHtml = "User already exists.   Choose another username.";
                    divMessage.Visible = true;
                }
            }
            else if (btnSubmit.Text == "Update")
            {

                sql = " update NonesCRMusers set";
                sql += " UserName = '" + txtUserName.Text.Trim().Replace("'", "''") + "',";
                sql += " Password = '" + txtPassword.Text.Trim().Replace("'", "''") + "',";
                sql += " FirstName = '" + txtFirstName.Text.Trim().Replace("'", "''") + "',";
                sql += " Surname = '" + txtSurname.Text.Trim().Replace("'", "''") + "',";
                sql += " CompanyName = '" + txtCompanyName.Text.Trim().Replace("'", "''") + "',";
                sql += " Phone = '" + txtPhone.Text.Trim().Replace("'", "''") + "',";
                sql += " Email = '" + txtEmail.Text.Trim().Replace("'", "''") + "',";
                sql += " WebSite = '" + txtWebsite.Text.Trim().Replace("'", "''") + "',";

                if (chkShwSmp.Checked)
                {
                    sql += " ShowSMP = 'True',";
                }
                else
                {
                    sql += " ShowSMP = 'False',";
                }
                sql += "Subscription = '" + drpsubscription.SelectedItem.Text.Trim().Replace("'", "''") + "',";
                //=================================================================================
                //*********************************************************************************
                //=================================================================================
                if (Chkemailnotify.Checked)
                {
                    sql += " Email_Notify = 'True',";
                }
                else
                {
                    sql += " Email_Notify = 'False',";
                }
                if(ChkmigratetoTask.Checked)
                {
                    sql += " Migrate_CR_comments_Tasks = 'True',";
                }
                else
                {
                    sql += " Migrate_CR_comments_Tasks = 'False',";
                }
                sql += "Anyone = '" + dranyone.SelectedItem.Text.Trim().Replace("'", "''") + "',";
                sql += "Emptask_assign = '" + dremp.SelectedItem.Text.Trim().Replace("'", "''") + "',";
                sql += "FHSLead = '" + drfhslead.SelectedItem.Text.Trim().Replace("'", "''") + "',";
                sql += "UK = '" + druk.SelectedItem.Text.Trim().Replace("'", "''") + "',";
                sql += "ProjectName = '" + drpNewProjects.SelectedItem.Text.Trim().Replace("'", "''") + "',";
                string struser = "";
                for (int icount = 0; icount <= chklist.Items.Count - 1; icount++)
                {
                    if (chklist.Items[icount].Selected)
                    {
                        struser += chklist.Items[icount].Text.ToString() + ",";
                    }
                }
                if (struser.ToString() != "")
                {
                    sql += " ShowRequest = '" + struser.TrimEnd(new char[] { ',' }) + "',";
                }
                else
                {
                    //sql += "'" + struser + "',";
                    sql += " ShowRequest = '" + struser + "',";
                }
                //=================================================================================
                //*********************************************************************************
                //=================================================================================
                if (chkChat.Checked)
                {
                    sql += " AllowChat = 'True',";
                }
                else
                {
                    sql += " AllowChat = 'False',";
                }
                if (chkRmtDsktp.Checked)
                {
                    sql += " AllowRemotDesktop = 'True'";
                }
                else
                {
                    sql += " AllowRemotDesktop = 'False'";
                }
                if (DrpSupportType.SelectedValue != "")
                {
                    sql += ",  supportType = '" + DrpSupportType.SelectedItem.ToString() + "',";
                    sql += "  SupportUrl = '" + DrpSupportType.SelectedValue.ToString() + "'";
                }
                else 
                {
                    sql += ",  supportType = '" + DrpSupportType.SelectedItem.ToString() + "',";
                    sql += "  SupportUrl = '" + DrpSupportType.SelectedValue.ToString() + "'";
                
                }
                sql += " where RegId = " + lblId.Text.Trim();

                int intResult = DatabaseHelper.executeNonQuery(sql);

                if (intResult != 0)
                {

                    string SqlCRlastupdated = "";
                    SqlCRlastupdated = "Update ClientRequest set LastUpdatedBy ='" + txtUserName.Text.Trim().Replace("'", "''") + "' where LastUpdatedBy = '" + ClientUsername.Value.ToString().Trim().Replace("'", "''") + "' and   UserName='" + ClientUsername.Value.ToString().Trim().Replace("'", "''") + "' and CompanyName = '" + txtCompanyName.Text.Trim().Replace("'", "''") + "'";
                    int intResultCRlastupdate = DatabaseHelper.executeNonQuery(SqlCRlastupdated);

                    string SqlCRaddedby = "";
                    SqlCRaddedby = "Update ClientRequest set AddedBy ='" + txtUserName.Text.Trim().Replace("'", "''") + "' where AddedBy = '" + ClientUsername.Value.ToString().Trim().Replace("'", "''") + "' and   UserName='" + ClientUsername.Value.ToString().Trim().Replace("'", "''") + "' and CompanyName = '" + txtCompanyName.Text.Trim().Replace("'", "''") + "'";
                    int intResultCRaddedby = DatabaseHelper.executeNonQuery(SqlCRaddedby);

                    string SqlCRTB = "";
                    SqlCRTB = "Update ClientRequest set UserName ='" + txtUserName.Text.Trim().Replace("'", "''") + "' where  UserName='" + ClientUsername.Value.ToString().Trim().Replace("'", "''") + "' and CompanyName = '" + txtCompanyName.Text.Trim().Replace("'", "''") + "'";
                    int intResultCRtbl = DatabaseHelper.executeNonQuery(SqlCRTB);
                    string SqlCRDetails = "";
                    SqlCRDetails = "Update ClientRequest_Details set PostedBy ='" + txtUserName.Text.Trim().Replace("'", "''") + "' where  PostedBy='" + ClientUsername.Value.ToString().Trim().Replace("'", "''") + "'";
                    int intResultCRtbl2 = DatabaseHelper.executeNonQuery(SqlCRDetails);
                    string SqlCRcommnet= "";
                    SqlCRcommnet = "Update read_CR_comments set UserName ='" + txtUserName.Text.Trim().Replace("'", "''") + "' where  UserName='" + ClientUsername.Value.ToString().Trim().Replace("'", "''") + "'";
                    int intResultCRtbl3 = DatabaseHelper.executeNonQuery(SqlCRcommnet);
                    string strBody = generateUpdateEmail();
                 
                    flag = DatabaseHelper.sendEmailChangeRequestNorply(ConfigurationManager.AppSettings["ToEmail1"].ToString(), "Registration details have been changed.", strBody);
                   //// flag = DatabaseHelper.sendEmailChangeRequest(ConfigurationManager.AppSettings["ToEmail2"].ToString(), "Registration details", strBody);
                    if (Chkemailnotify.Checked)
                    {
                        flag = DatabaseHelper.sendEmailChangeRequestNorply(txtEmail.Text.Trim(), "Registration details have been changed.", strBody);
                    }
                    Session["username"] = txtUserName.Text.Trim();
                    Session["ClientId"] = "0";
                    Session["ESCRMuser"] = "false";

                    divMessage.InnerHtml = "Client details was updated.";
                }
                else
                {
                    divMessage.InnerHtml = "Client details was not updated.";
                }
            }
            //======================================================================================================================================================
        }
    }

    private string generateEmail()
    {
        string strBody = "";
        strBody += "<br>" + txtFirstName.Text.Trim() + " " + txtSurname.Text.Trim() + ",<br><br>";
        strBody += "We have registered an account for you to add any change requests.To make any change requests please go to: " + ConfigurationManager.AppSettings["WebAddress2"].ToString() + " and use the following login details for any changes you might request.<br><br>";
        strBody += "Username: " + txtUserName.Text.Trim() + "<br>";
        strBody += "Password: " + txtPassword.Text.Trim() + "<br><br>";
        strBody += "You can change the password to something more memorable within the system. Please ensure you bookmark the website so you can easily find it next time. If you check the check box on login which allows you to save the password you will not have to login again if you open the website on this PC (requires you have cookies enabled)." + "<br><br>";
        strBody += "<span style=\'font-size:11.0pt;font-family:Calibri,sans-serif;color:red\'>Please do not reply to this email as it does not take replies. Please use the Change Request System as this is the quickest way to process your requests and also keeps a record of communications and progress for you to review at any time.</span><br/>";
        strBody += "<br>";
        strBody += "Your registration details are:" + "<br><br>";
       
        ////strBody += "Your registered details follow. If any are incorrect please let us know:<br>";
        strBody += "First name: " + txtFirstName.Text.Trim() + "<br>";
        strBody += "Surname: " + txtSurname.Text.Trim() + "<br>";
        strBody += "Company name: " + txtCompanyName.Text.Trim() + "<br>";
        strBody += "Phone: " + txtPhone.Text.Trim() + "<br>";
        strBody += "Email address: " + txtEmail.Text.Trim() + "<br>";
        strBody += "Website: " + txtWebsite.Text.Trim() + "<br><br>";
       
       /// strBody += "To make any change requests please go to: " + ConfigurationManager.AppSettings["WebAddress2"].ToString() + "<br><br>";
        strBody += "Why use the Change Request system rather than email or call:" + "<br><br>";
        strBody += "<li style='margin-left:13px'>If the email recipient is not available to receive or action your email your request will still get actioned by the support team which monitor the Change Request system.</li><br>";
        strBody += "<li style='margin-left:13px'>The support team will receive the request immediately.</li><br>";
        strBody += "<li style='margin-left:13px'>You will also be able to login at any time and check the status.</li><br>";
        strBody += "<li style='margin-left:13px'>The ongoing progress will be logged in one place rather than through a stream of disparate email communications.</li><br>";
        strBody += "<li style='margin-left:13px'>If the support team require to speak directly with you they can initiate an on-line chat with you.</li><br><br>";
        strBody += "<br/><br/>" + "Regards," + "<br/>Support.";
        return strBody;

    }


    private string generateUpdateEmail()
    {
        
        string strBody = "";
        strBody += "<br>" + txtFirstName.Text.Trim() + " " + txtSurname.Text.Trim() + ",<br><br>";
        strBody += "Your registration details for the Change Request System have changed.<br> Your login details for any changes you might request are<br>";
        strBody += "Username: " + txtUserName.Text.Trim() + "<br>";
        strBody += "Password: " + txtPassword.Text.Trim() + "<br><br>";
        strBody += "You can change the password to something more memorable." + "<br><br>";
        strBody += "<span style=\'font-size:11.0pt;font-family:Calibri,sans-serif;color:red\'>Please do not reply to this email as it does not take replies. Please use the Change Request System as this is the quickest way to process your requests and also keeps a record of communications and progress for you to review at any time.</span><br/>";
        strBody += "<br>";
        strBody += "Your registered details follow. If any are incorrect please let us know:<br>";
        strBody += "First name: " + txtFirstName.Text.Trim() + "<br>";
        strBody += "Surname: " + txtSurname.Text.Trim() + "<br>";
        strBody += "Company name: " + txtCompanyName.Text.Trim() + "<br>";
        strBody += "Phone: " + txtPhone.Text.Trim() + "<br>";
        strBody += "Email address: " + txtEmail.Text.Trim() + "<br>";
        strBody += "Website: " + txtWebsite.Text.Trim() + "<br>";
        strBody += "To make any change requests please go to: " + ConfigurationManager.AppSettings["WebAddress2"].ToString() + "<br><br>";
        strBody += "Why use the Change Request system rather than email or call:" + "<br><br>";
        strBody += "<li style='margin-left:13px'>If the email recipient is not available to receive or action your email your request will still get actioned by the support team which monitor the Change Request system.</li><br>";
        strBody += "<li style='margin-left:13px'>The support team will receive the request immediately.</li><br>";
        strBody += "<li style='margin-left:13px'>You will also be able to login at any time and check the status.</li><br>";
        strBody += "<li style='margin-left:13px'>The ongoing progress will be logged in one place rather than through a stream of unrelated email communications.</li><br>";
        strBody += "<li style='margin-left:13px'>If the support team require to speak directly with you they can initiate an online chat with you or remote desktop login.</li><br><br>";
        strBody += "<br/><br/>" + "Regards," + "<br/>Support.";
        return strBody;

    }

    private void clearControls()
    {
        lblId.Text = "New";
        txtUserName.Text = "";
        txtPassword.Text = "";
        txtFirstName.Text = "";
        txtSurname.Text = "";
        txtCompanyName.Text = "";
        txtPhone.Text = "";
        txtEmail.Text = "";
        txtWebsite.Text = "";
        btnSubmit.Text = "Create";
    }

//    void getCRwaitingfrmClient()
//    {
//        sql = @"select ClientRequest.* from ClientRequest 
//                where ClientRequest.Status = 'awaiting client response- required' and status <> 'closed' and deleted <> 1 ";



//        DataSet ds = DatabaseHelper.getDataset(sql);

//        if (ds != null && ds.Tables[0].Rows.Count > 0)
//        {

//            if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
//            {

//                CRwocr.Visible = false;
//            }
//            else
//            {
//                LnkCRWocr.Text = "(" + ds.Tables[0].Rows.Count + ")";
//                CRwocr.Visible = true;
//            }

//        }
//        else
//        {


//            if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
//            {

//                CRwocr.Visible = false;
//            }
//            else
//            {
//                LnkCRWocr.Text = "(0)";
//                CRwocr.Visible = false;
//            }


//        }
//    }
    //protected void LnkCRWocr_Click(object sender, EventArgs e)
    //{
    //    Session["filter"] = "CRwocr";
    //    Response.Redirect("client_requests.aspx", false);
    //}
    void getCRNewComment()
    {


        //// sql = @"select ClientRequest.* from ClientRequest where ClientRequest.[RequestId] in (select [RequestId] from ClientRequest_Details where [CommentId] not in (select  CommentId from read_CR_comments)) and ClientRequest.deleted <> 1";
        ////  sql = @"select ClientRequest_Details.* from ClientRequest_Details where [CommentId] not in (select  CommentId from read_CR_comments where [UserName]='Support') and ClientRequest_Details.deleted <> 1";
        sql = "select ClientRequest.* from ClientRequest where RequestId in(select ClientRequest_Details.RequestId from ClientRequest_Details where [CommentId] not in (select  CommentId from read_CR_comments where read_CR_comments.username='" + Session["admin"].ToString() + "') and ClientRequest_Details.deleted <> 1)";

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
            {
                /// LnkNewCR.Text = "(" + ds.Tables[0].Rows.Count + ")";
                CrNewComment.Visible = false;
                
                LnkMakecommentRead.Visible = false;
                
            }
            else
            {
                LnkCrNewComment.Text = "(" + ds.Tables[0].Rows.Count + ")";
                LnkMakecommentRead.Text = "(" + "-" + ")";
                LnkMakecommentRead.Visible = true;
                CrNewComment.Visible = true;
            }

        }
        else
        {

            if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
            {
                /// LnkNewCR.Text = "(" + ds.Tables[0].Rows.Count + ")";
                CrNewComment.Visible = false;
                LnkMakecommentRead.Visible = false;
               
            }
            else
            {
                LnkCrNewComment.Text = "(0)";
                CrNewComment.Visible = false;
                LnkMakecommentRead.Visible = false;
            }




        }
    }

    protected void LnkNewCR_Click(object sender, EventArgs e)
    {

        Session["filter"] = "NewCR";
        Response.Redirect("client_requests.aspx", false);
        /// bindPR1bTasks();

    }
    protected void LnkCrNewComment_Click(object sender, EventArgs e)
    {
        Session["filter"] = "CRNewComment";
        Response.Redirect("client_requests.aspx", false);

    }

    void getNewCR()
    {
        sql = @"select ClientRequest.* from ClientRequest 
                where ClientRequest.Status = 'new' and status <> 'closed' and deleted <> 1 ";

        ////if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        ////{
        ////    sql += " and status <> 'closed' and assigned_to_user = '" + Session["admin"] + "' ";
        ////}
        ////else
        ////{
        ////    sql += " and assigned_to_user = '" + Session["admin"] + "' ";
        ////}

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            ///  LnkNewCR.Text = "(" + ds.Tables[0].Rows.Count + ")";
            /// NewCR.Visible = true;
            if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
            {
                /// LnkNewCR.Text = "(" + ds.Tables[0].Rows.Count + ")";
                NewCR.Visible = false;
            }
            else
            {
                LnkNewCR.Text = "(" + ds.Tables[0].Rows.Count + ")";
                NewCR.Visible = true;
            }

        }
        else
        {


            if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
            {
                /// LnkNewCR.Text = "(" + ds.Tables[0].Rows.Count + ")";
                NewCR.Visible = false;
            }
            else
            {
                LnkNewCR.Text = "(0)";
                NewCR.Visible = false;
            }


        }
    }

    protected void LnkMakecommentRead_Click(object sender, EventArgs e)
    {
        string sql = "select *  from ClientRequest_Details where [CommentId] not in (select  CommentId  from read_CR_comments where read_CR_comments.username='" + Session["admin"].ToString() + "') and ClientRequest_Details.deleted <> 1";
        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {
                    string sqlinsert = "insert into read_CR_comments([CommentId],[UserName]) ";
                    sqlinsert += " values(" + ds.Tables[0].Rows[i]["CommentId"].ToString() + ",'" + Session["admin"].ToString() + "')";

                    int intResult = DatabaseHelper.executeNonQuery(sqlinsert);
                }
                catch
                {
                }
            }


        }

        getCRNewComment();

    }

    protected void lnkUnansweredQuestions_Click(object sender, EventArgs e)
    {
        Session["filter"] = "Unanswered";
        Session["filterunread"] = "";
        Session["filterunread"] = "";
        Session["OverdueTask"] = null;
        Response.Redirect("tasks.aspx");
    }

    protected void lnkPr0Task_Click(object sender, EventArgs e)
    {

        Session["filter"] = "Immediate";
        Session["filterunread"] = "";
        Session["filterunread"] = "";
        Session["OverdueTask"] = null;
        Response.Redirect("tasks.aspx");
    }

    protected void lnkHighestTasks_Click(object sender, EventArgs e)
    {
        Session["filter"] = "HighestTasks";
        Session["filterunread"] = "";
        Session["filterunread"] = "";
        Session["OverdueTask"] = null;
        Response.Redirect("tasks.aspx");
    }

    protected void lnkHighTasks_Click(object sender, EventArgs e)
    {
        Session["filter"] = "HighTasks";
        Session["filterunread"] = "";
        Session["filterunread"] = "";
        Session["OverdueTask"] = null;
        Response.Redirect("tasks.aspx");
    }

    void getUnasweredQuestions()
    {
//////        sql = @"select task_comments.* from task_comments,tasks 
//////                where tasks.task_id = task_comments.task_id and tasks.deleted <> 1 and tasks.status <> 'closed' and task_comments.deleted <> 1 and qflag=1 ";
     

//////        sql += " and assigned_to_user = '" + Session["admin"] + "' ";

        sql = @"select distinct tasks.* from task_comments,tasks 
                where tasks.task_id = task_comments.task_id and  tasks.deleted <> 1 and task_comments.deleted <> 1 and qflag=1 ";

        if (!DatabaseHelper.can_Show_Closed_Tasks(Session["admin"].ToString()))
        {
            sql += " and status <> 'closed' ";
        }

        sql += " and task_comments.QuesTo = '" + Session["admin"] + "' ";
        


        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            lnkUnansweredQuestions.Text = "(" + ds.Tables[0].Rows.Count + ")";
            unansweredque.Visible = true;
        }
        else
        {
            lnkUnansweredQuestions.Text = "(0)";
            unansweredque.Visible = false;
        }
    }

    void getPR0Tasks()
    {
        sql = @"select tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '0 - IMMEDIATE' ";

        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            sql += " and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + Session["admin"] + "' ";
        }
        else
        {
            sql += " and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + Session["admin"] + "' ";
        }

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            lnkPr0Task.Text = "(" + ds.Tables[0].Rows.Count + ")";
            immediatetask.Visible = true;
        }
        else
        {
            lnkPr0Task.Text = "(0)";
            immediatetask.Visible = false;
        }
    }

    void getPR1aTasks()
    {
        sql = @"select tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '1a - DO NOW' ";

        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            sql += " and status <> 'closed' and status <> 'checked' and status <>'parked' and assigned_to_user = '" + Session["admin"] + "' ";
        }
        else
        {
            sql += " and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + Session["admin"] + "' ";
        }

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            lnkHighestTasks.Text = "(" + ds.Tables[0].Rows.Count + ")";
            highestTasks.Visible = true;
        }
        else
        {
            lnkHighestTasks.Text = "(0)";
            highestTasks.Visible = false;
        }
    }

    void getPR1bTasks()
    {
        sql = @"select tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '1b - high' ";

        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            sql += " and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + Session["admin"] + "' ";
        }
        else
        {
            sql += " and status <> 'closed' and  status <> 'checked' and status <>'parked'  and assigned_to_user = '" + Session["admin"] + "' ";
        }

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            lnkHighTasks.Text = "(" + ds.Tables[0].Rows.Count + ")";
            highTasks.Visible = true;
        }
        else
        {
            lnkHighTasks.Text = "(0)";
            highTasks.Visible = false;
        }
    }
    void getTskNewComment()
    {


        sql = "select tasks.* from tasks where task_id in(select task_comments.task_id from task_comments where tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 )";
        
        sql += "and tasks.deleted <> 1 and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            LnkTskNewcomment.Text = "(" + ds.Tables[0].Rows.Count + ")";
            LnkMakeTaskcommentRead.Text = "(" + "-" + ")";
            LnkMakeTaskcommentRead.Visible = true;
            TskNewComment.Visible = true;
        }
        else
        {
            LnkTskNewcomment.Text = "(0)";
            TskNewComment.Visible = false;
            LnkMakeTaskcommentRead.Visible = false;
        }
    }
    protected void LnkTskNewcomment_Click(object sender, EventArgs e)
    {

        Session["filterunread"] = "TaskNewComment";
        Session["filterunread"] = "";
        Session["OverdueTask"] = null;
        Response.Redirect("tasks.aspx");


    }
    protected void LnkMakeTaskcommentRead_Click(object sender, EventArgs e)
    {
        sql = @"select *  from task_comments where task_id in (select task_id  from tasks where tasks.task_id = task_comments.task_id and  tasks.assigned_to_user = '" + Session["admin"].ToString() + "'  and tasks.deleted <> 1 and tasks.status <> 'closed') and task_comments.tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 ";
        /// sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {

                    //////string sqlgetcmntId = "select tc_id from task_comments where task_id ='" + ds.Tables[0].Rows[i]["task_id"].ToString() + "'";
                    //////object TskcmntID = DatabaseHelper.executeScalar(sqlgetcmntId);


                    //////if (TskcmntID!=null)
                    //////{
                    string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                    sqlinsert += " values(" + ds.Tables[0].Rows[i]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                    int intResult = DatabaseHelper.executeNonQuery(sqlinsert);
                    string sqlinsert2 = "insert into read_task([task_id],[username]) ";
                    sqlinsert2 += " values(" + ds.Tables[0].Rows[i]["task_id"].ToString() + ",'" + Session["admin"].ToString() + "')";
                    int intResult2 = DatabaseHelper.executeNonQuery(sqlinsert2);
                    //////}
                }
                catch
                {
                }
            }


        }

        Session["filterunread"] = "";
        Session["OverdueTask"] = null;
        Response.Redirect("tasks.aspx", false);
    }


    void getAllTskNewComment()
    {

        sql = "select tasks.* from tasks where task_id in(select task_comments.task_id from task_comments where tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 )";

       /// sql += "and tasks.deleted <> 1 and tasks.status <> 'closed'";

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
            {
                LnkAllTsknewComment.Text = "(0)";
                SpnAllTaskNewComment.Visible = false;
                LnkMakeAllTaskCmmntRead.Visible = false;
                LnkAllTsknewComment.Visible = false;

            }
            else
            {


                LnkAllTsknewComment.Text = "(" + ds.Tables[0].Rows.Count + ")";
                LnkMakeAllTaskCmmntRead.Text = "(" + "-" + ")";
                LnkMakeAllTaskCmmntRead.Visible = true;
                SpnAllTaskNewComment.Visible = true;

            }
        }
        else
        {
            LnkAllTsknewComment.Text = "(0)";
            SpnAllTaskNewComment.Visible = false;
            LnkMakeAllTaskCmmntRead.Visible = false;
            LnkAllTsknewComment.Visible = false;
        }


    }
    protected void LnkAllTsknewComment_Click(object sender, EventArgs e)
    {
        Session["filterunread"] = "AllTaskNewComment";
        Session["filterunread"] = "";
        Session["OverdueTask"] = null;
        Response.Redirect("tasks.aspx");
    }
    protected void LnkMakeAllTaskCmmntRead_Click(object sender, EventArgs e)
    {


        sql = @"select *  from task_comments where task_id in (select task_id  from tasks where tasks.task_id = task_comments.task_id) and task_comments.tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 ";
        /// sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {

                    string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                    sqlinsert += " values(" + ds.Tables[0].Rows[i]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                    int intResult = DatabaseHelper.executeNonQuery(sqlinsert);
                    string sqlinsert2 = "insert into read_task([task_id],[username]) ";
                    sqlinsert2 += " values(" + ds.Tables[0].Rows[i]["task_id"].ToString() + ",'" + Session["admin"].ToString() + "')";
                    int intResult2 = DatabaseHelper.executeNonQuery(sqlinsert2);
                    //////}
                }
                catch
                {
                }
            }


        }

        getAllTskNewComment();


    }
    protected void drpProjects_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (drpProjects.SelectedValue == "other")
        {
            
            TDAddNewComapny.Visible = true;
            txtCompanyName.Text = "";
            ///txtCompanyName.Visible = true;
        }
        else
        {
            //======================================
            Loadprogect_drp();
            //======================================
            TDAddNewComapny.Visible = false;
            txtCompanyName.Text = drpProjects.SelectedValue;
           // txtCompanyName.Visible = false;
        }
    }

    protected void btnAddCompany_Click(object sender, EventArgs e)
    {

        object objResult = DatabaseHelper.executeScalar("select count(*) from Company where [Company_Name]='" + txtCompanyName.Text.Trim() + "'");

        if (objResult.ToString() != "0")
        {
            lblcompanymsg.Text = "Company name alredy exist.";
            return;
        }

        string desc = "";
        string dfltselection = "N";
        string stractive = "Y";

        string strSql = " insert into Company ([Company_Name],[description],[default_selection],[active]) ";
        strSql += " values('" + txtCompanyName.Text.Trim().Replace("'", "''") + "','" + desc.Trim().Replace("'", "''") + "','" + dfltselection + "','" + stractive + "')";

        int intResult = DatabaseHelper.executeNonQuery(strSql);

        if (intResult != 0)
        {
            loadCompany_dropdowns();
            lblcompanymsg.Text = "Company added successfully.";
          //========================================================
            Loadprogect_drp();
          //========================================================
            ///clearControls();
        }
        else
        {
            lblcompanymsg.Text = "Company was not created.";
        }

    }
    protected void btnclosecompany_Click(object sender, EventArgs e)
    {
        txtCompanyName.Text = "";
        lblcompanymsg.Text = "";
        loadCompany_dropdowns();
        TDAddNewComapny.Visible = false;
    }
    protected void drpNewProjects_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (drpNewProjects.SelectedValue == "other")
        {
            TDAddNewproject.Visible = true;
            txtprojectname.Text = "";
        }
        else
        {
            TDAddNewproject.Visible = false;
            txtprojectname.Text = drpNewProjects.SelectedValue;
        }
    }
    protected void btnaddProject_Click(object sender, EventArgs e)
    {
        object objResult = DatabaseHelper.executeScalar("select count(*) from projects where active = 'Y' and CompanyName='" + drpProjects.SelectedValue.ToString() + "' and project_name='" + txtprojectname.Text.Trim().Replace("'", "''") + "'");

        if (objResult.ToString() != "0")
        {
            lblprojectmsg.Text = "Project name already exist.";
            return;
        }

        string desc = "";
        string dfltselection = "N";
        string stractive = "Y";
        string strarchive = "N";

        string strSql = " insert into projects ([project_name],[description],[default_selection],[active],[CompanyName],[archive]) ";
        strSql += " values('" + txtprojectname.Text.Trim().Replace("'", "''") + "','" + desc.Trim().Replace("'", "''") + "','" + dfltselection + "','" + stractive + "','" + drpProjects.SelectedValue.ToString() + "','" + strarchive + "')";

        //project_name,description,default_selection,active,CompanyName

        int intResult = DatabaseHelper.executeNonQuery(strSql);

        if (intResult !=0)
        {

            lblprojectmsg.Text = "Project added successfully.";
            //========================================================
            Loadprogect_drp();
            //========================================================
            //clearControls();
        }
        else
        {
            lblprojectmsg.Text = "Project was not created.";
        }
        insertArchivecache();
    }
    private void insertArchivecache()
    {
        string sqlArchive = "", sqlAll = "";
        DataSet ds_dropdowns, ds_dropdownsAll, ds_forProjectRelated;


        Cache.Remove("Dropdowns_For_NotArchive");
        sqlArchive = "select project_name from projects where archive!='Y' and active = 'Y' order by project_name;";
        ds_dropdowns = DatabaseHelper.getDataset(sqlArchive);
        Cache.Insert("Dropdowns_For_NotArchive", ds_dropdowns, null, DateTime.Now.AddHours(6), TimeSpan.Zero);

        DataSet ds_Alldropdowns;
        Cache.Remove("Dropdowns_For_AllProject");
        sqlAll = "select project_name from projects where  active = 'Y' order by project_name;";
        ds_Alldropdowns = DatabaseHelper.getDataset(sqlAll);
        Cache.Insert("Dropdowns_For_AllProject", ds_Alldropdowns, null, DateTime.Now.AddHours(6), TimeSpan.Zero);


        string SqlallQry = "";
        Cache.Remove("dropdowns");
        SqlallQry = @"select project_name	from projects where archive!='Y' and  active = 'Y' order by project_name;";
        SqlallQry += "\nselect category_name from categories order by sort_seq, category_name;";
        SqlallQry += "\nselect priority_name from priorities order by sort_seq, priority_name;";
        SqlallQry += "\nselect status_name from statuses order by sort_seq, status_name;";
        SqlallQry += "\nselect username from users where active = 'Y' order by username;";
        ds_dropdownsAll = DatabaseHelper.getDataset(SqlallQry);
        Cache.Insert("dropdowns", ds_dropdownsAll, null, DateTime.Now.AddHours(6), TimeSpan.Zero);


        string SqlProjectRequest = "";
        Cache.Remove("dropdownsNew");
        SqlProjectRequest = @"select project_name from projects where active = 'Y' order by project_name;";
        SqlProjectRequest += "\nselect category_name from categories order by sort_seq, category_name;";
        SqlProjectRequest += "\nselect priority_name from priorities order by sort_seq, priority_name;";
        SqlProjectRequest += "\nselect status_name from statuses order by sort_seq, status_name;";
        SqlProjectRequest += "\nselect username from users where active = 'Y' order by username;";
        ds_forProjectRelated = DatabaseHelper.getDataset(SqlProjectRequest);
        Cache.Insert("dropdownsNew", ds_forProjectRelated, null, DateTime.Now.AddHours(6), TimeSpan.Zero);


    }
    protected void btncloseProject_Click(object sender, EventArgs e)
    {
        txtprojectname.Text = "";
        lblprojectmsg.Text = "";
        Loadprogect_drp();
        TDAddNewproject.Visible = false;
    }
    void Loadprogect_drp()
    {
        drpNewProjects.Items.Clear();
        sql = @"select project_name
		from projects
		where active = 'Y' and Archive ='N' and CompanyName='" + drpProjects.SelectedValue.ToString() + "' order by project_name;";
        DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);
        if (ds_dropdowns.Tables[0].Rows.Count > 0)
        {
            try
            {
                drpNewProjects.DataSource = ds_dropdowns.Tables[0];
                drpNewProjects.DataTextField = "project_name";
                drpNewProjects.DataValueField = "project_name";
                drpNewProjects.DataBind();
                drpNewProjects.Items.Insert(0, new ListItem("[no project]", ""));
                drpNewProjects.Items.Insert(1, new ListItem("[Other]", "other"));
            }
            catch { }
        }
        else
        {
            drpNewProjects.Items.Insert(0, new ListItem("[no project]", ""));
            drpNewProjects.Items.Insert(1, new ListItem("[Other]", "other"));
        }
    }
    //=======================================================================================================
    void LoadNewProject(string strcompany)
    {
        drpNewProjects.Items.Clear();
        sql = @"select project_name
		from projects
		where active = 'Y' and Archive ='N' and CompanyName='" + strcompany.ToString() + "' order by project_name;";
        DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);
        if (ds_dropdowns.Tables[0].Rows.Count > 0)
        {
            try
            {
                drpNewProjects.DataSource = ds_dropdowns.Tables[0];
                drpNewProjects.DataTextField = "project_name";
                drpNewProjects.DataValueField = "project_name";
                drpNewProjects.DataBind();
                drpNewProjects.Items.Insert(0, new ListItem("[no project]", ""));
                drpNewProjects.Items.Insert(1, new ListItem("[Other]", "other"));
            }
            catch { }
        }
        else
        {
            drpNewProjects.Items.Insert(0, new ListItem("[no project]", ""));
            drpNewProjects.Items.Insert(1, new ListItem("[Other]", "other"));
        }
    }
    
    private void Bindcheckboxlist()
    {
        string strchkbox = "Select * from users where Admin='N' order by username Asc ";
     DataSet dschkuser = DatabaseHelper.getDataset(strchkbox);
     if (dschkuser != null && dschkuser.Tables.Count > 0 && dschkuser.Tables[0].Rows.Count > 0)
     {
         chklist.DataSource = dschkuser.Tables[0];
         chklist.DataTextField = "username";
         chklist.DataValueField = "username";
         chklist.DataBind();
     }    
    }
  //=============================================================================================================
    void getPRLowTasks()
    {
        sql = @"select tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '2 - not urgent'";

        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            sql += " and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + Session["admin"] + "' ";
        }
        else
        {
            sql += " and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + Session["admin"] + "' ";
        }

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            LinklowTasks.Text = "(" + ds.Tables[0].Rows.Count + ")";
            PRlowTask.Visible = true;
        }
        else
        {
            LinklowTasks.Text = "(0)";
            PRlowTask.Visible = false;
        }
    }

    protected void LinklowTasks_Click(object sender, EventArgs e)
    {
        Session["filter"] = "2 - not urgent";
        Session["filterunread"] = "";
        //===============================
        Session["AllOverdueTask"] = "";
        //===============================
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        Response.Redirect("tasks.aspx");
    }
    protected void BtnallOverdue_Click(object sender, EventArgs e)
    {
        Session["filter"] = " ";
        Session["filterunread"] = "";
        Session["OverdueTask"] = "True";
        //===============================
        Session["AllOverdueTask"] = "True";
        //===============================
        Response.Redirect("tasks.aspx");
    }
    void getAllOverduetasks()
    {
        try
        {
            int GvnETC = 0;
            int j = 0;
            DateTime crrntdate = DateTime.Now.Date;
            DateTime duedate;
            sql = @"select tasks.* from tasks 
                        where tasks.deleted <> 1 and  status <> 'closed' and  status <> 'checked' and status <>'parked'";

            //if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
            //{
            //sql += " and assigned_to_user = '" + Session["admin"] + "' ";
            //}

            DataSet ds = DatabaseHelper.getDataset(sql);

            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //=========================================================================================
                    //string strcrrntdate = DateTime.Parse(DateTime.Now.Date.ToString()).ToString("dd/MM/yyyy");
                    string strcrrntdate = string.Format("{0:dd/MM/yyyy}", DateTime.Now.Date.ToString()).ToString();
                    //=========================================================================================
                    string djgfjs = ds.Tables[0].Rows[i]["DueDate"].ToString();
                    if (ds.Tables[0].Rows[i]["DueDate"].ToString() != null && ds.Tables[0].Rows[i]["DueDate"].ToString() != "")
                    {
                        string strduedate = string.Format("{0:dd/MM/yyyy}", ds.Tables[0].Rows[i]["DueDate"].ToString()).ToString();
                        duedate = DateTime.Parse(strduedate.ToString());
                        crrntdate = DateTime.Parse(strcrrntdate.ToString());
                        //======================================================================================================
                        //string strduedate = DateTime.Parse(ds.Tables[0].Rows[i]["DueDate"].ToString()).ToString("dd/MM/yyyy");
                        //duedate = Convert.ToDateTime(strduedate.ToString());
                        //crrntdate = Convert.ToDateTime(strcrrntdate.ToString());
                        //======================================================================================================
                        if (DateTime.Compare(duedate, crrntdate) < 0)
                        {
                            j++;
                        }
                    }
                }


                if (j > 0)
                {
                    if (DatabaseHelper.isAdmin(Session["admin"].ToString()))
                    {
                        BtnallOverdue.Text = "(" + j.ToString() + ")";
                        AllOverDuetsk.Visible = true;
                    }
                    else
                    {
                        AllOverDuetsk.Visible = false;
                    }
                }
                else
                {
                    BtnallOverdue.Visible = false;
                    AllOverDuetsk.Visible = true;
                }


            }
            else
            {

                BtnallOverdue.Visible = false;
                AllOverDuetsk.Visible = true;
            }

        }
        catch { }
    }
}


