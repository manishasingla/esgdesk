<%@ Page Language="C#" AutoEventWireup="true" CodeFile="edit_comment.aspx.cs" Inherits="Admin_edit_comment" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit.HTMLEditor" tagprefix="cc2" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        Edit comment
    </title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div id="wrapper">
            <uc2:Admin_Header ID="Admin_Header1" runat="server" />
            <div id="Content">
                <div style="width: 100%; float: left; padding: 5px 0; background: #ebebeb;">
                    <a id="lnkBack" runat="server" class="text_default">&nbsp;Back to task</a>
                </div>
                <div style="clear: both">
                </div>
                <div class="titleText" style="margin: 10px 0 20px 0; display: none;">
                    Edit comment</div>
                <div id="divMessage" runat="server" style="color: Red; font-weight: bold">
                </div>
                <div id="DivEntry" runat="server">
                    <div id="divTask" runat="server">
                        <table width="70%" cellpadding="2">
                            <tr>
                                <td align="left" valign="top">
                                    Comment:
                                </td>
                                <td align="left" valign="top">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top">
                                </td>
                                <td align="left" valign="top">
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="4" valign="top">
                                  <cc2:Editor ID="ftbComment" runat="server" Height="300px" Width="100%" />
                                    <%--***********************************************************************************************************--%>
                                    <%--<FTB:FreeTextBox ID="ftbComment" runat="server" Height="300px" ToolbarStyleConfiguration="Office2003" Width="100%"></FTB:FreeTextBox>--%>
                                    <%--<telerik:RadEditor ID="ftbComment" runat="server" Height="300px" Width="100%">
                                        <Snippets>
                                            <telerik:EditorSnippet Name="Product Template" Value="<table><tr><td><div style='width:300px;border:2px outset #D8D2BD;padding:34px;font-family:Arial, Verdana; font-size:11px;background- color:#F1EFE6;'>Dear ____________________,<br>Thank you for inquiring about ____________. Your request will be processed in 48 hours and shipped at the address you have provided.<b>Please, contact us if you have any problems.</b></div></td></tr></table>">            
                                            </telerik:EditorSnippet>
                                            <telerik:EditorSnippet Name="Signature" Value="<strong>John Smith</strong><br>Sales Manager<br>">
                                            </telerik:EditorSnippet>
                                        </Snippets>
                                        <Links>
                                            <telerik:EditorLink Name="Telerik" Href="http://www.telerik.com">
                                                <telerik:EditorLink Name="Telerik Products" Href="http://www.telerik.com/products"
                                                    Target="_blank" />
                                                <telerik:EditorLink Name="Telerik Support Center" Href="http://www.telerik.com/support/home.aspx"
                                                    Target="_blank" />
                                                <telerik:EditorLink Name="Telerik Community" Href="http://www.telerik.com/community/home.aspx"
                                                    Target="_blank" ToolTip="Telerik Community" />
                                            </telerik:EditorLink>
                                        </Links>
                                        <Tools>
                                        </Tools>
                                        <ImageManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                        <MediaManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                        <FlashManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                        <TemplateManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                        <DocumentManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                        <Modules>
                                            <telerik:EditorModule Name="RadEditorStatistics" Visible="False" Enabled="true" />
                                            <telerik:EditorModule Name="RadEditorDomInspector" Visible="False" Enabled="true" />
                                            <telerik:EditorModule Name="RadEditorNodeInspector" Visible="False" Enabled="true" />
                                            <telerik:EditorModule Name="RadEditorHtmlInspector" Visible="False" Enabled="true" />
                                        </Modules>
                                        <Content>
                                        </Content>
                                    </telerik:RadEditor>--%>
                                    <%--***********************************************************************************************************--%>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top" colspan="3">
                                    <div id="divMessage1" runat="server" style="color: Red">
                                    </div>
                                </td>
                                <td align="right" valign="top">
                                    <asp:CheckBox ID="chkEmailnotify" runat="server" Checked="true" Font-Bold="True"
                                        Text="Email notify" />&nbsp; &nbsp;<asp:CheckBox ID="chkAnswer" runat="server" Font-Bold="True"
                                            Text="as an answer" />&nbsp; &nbsp;<asp:Button ID="btnUpdate" runat="server" Text="Update"
                                                OnClick="btnUpdate_Click" CssClass="blueBtns" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <uc1:footer ID="Footer1" runat="server" />
            <asp:HiddenField ID="hdnShortDescr" runat="server" />
        </div>
    </form>
</body>
</html>
