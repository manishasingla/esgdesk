<%@ Page Language="C#" AutoEventWireup="true" CodeFile="edit_company.aspx.cs" Inherits="Admin_edit_company" %>

<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        Edit company - <%=System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(company_name)%></title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
    function checkProject()
    {
    var project =  document.getElementById("txtName").value;
    var drpprjct = document.getElementById("drpProjects").value;
    if(drpprjct == "other")
    {
    
    if(project =="")
    {
    alert("Please enter project name");
    return false;
    }
    else
    {
    var btnsend = document.getElementById("btnAddProject");
btnsend.click();  
    }
    }
    else
    {
    var btnsend = document.getElementById("btnAddProject");
btnsend.click(); 
    }
    
    }
    
    </script>

    <script type="text/javascript" language="javascript">
function setFHSCategories() {

	var hiddenFhsCat = document.getElementById("hdnFHSCategories");
	var chkbxs = document.getElementById("Repeater1").getElementsByTagName("input");
	hiddenFhsCat.value = "";
	for(var i=0;i<chkbxs.length;i++)
	{
		if(chkbxs[i].checked==true) { hiddenFhsCat.value += chkbxs[i].title + ";"; }
	}
	
}

function setClientCategories() {
	var hiddenClientCat = document.getElementById("hdnClientCategories");
	var chkbxs = document.getElementById("Repeater2").getElementsByTagName("input");
	hiddenClientCat.value = "";
	for(var i=0;i<chkbxs.length;i++)
	{
		if(chkbxs[i].checked==true) { hiddenClientCat.value += chkbxs[i].title + ";"; }
	}
}

function setAllChecked(obj) {
	var chkbxs = document.getElementById("Repeater1").getElementsByTagName("input");
	for(var i=0;i<chkbxs.length;i++)
	{
		chkbxs[i].checked=obj.checked;
	}
	chkbxs = document.getElementById("Repeater2").getElementsByTagName("input");
	for(var i=0;i<chkbxs.length;i++)
	{
		chkbxs[i].checked=obj.checked;
	}
	setFHSCategories();
	setClientCategories();
}


function SetFHSCatOnload()
{
var hiddenFhsCat = document.getElementById("hdnFHSCategories");
var chkbxs = document.getElementById("Repeater1").getElementsByTagName("input");
if(hiddenFhsCat.value !="")
{
var items = hiddenFhsCat.value.split(";");
for(var j=0;j<chkbxs.length;j++)
	{

       for(var i=0;i<items.length-1;i++)
		{
		
			if(items[i] == chkbxs[j].title)
			{
		
			chkbxs[j].checked = true;
			
			}
			
			
			}
			
	}
			
			
			
}

}
function SetClientCatOnload()
{

var hiddenClientCat = document.getElementById("hdnClientCategories");
var chkbxs = document.getElementById("Repeater2").getElementsByTagName("input");
if(hiddenClientCat.value !="")
{
var items = hiddenClientCat.value.split(";");

for(var j=0;j<chkbxs.length;j++)
{

            for(var i=0;i<items.length-1;i++)
			{
			if(items[i] == chkbxs[j].title)
			{
			chkbxs[j].checked = true;
			
			}
			
			}
			
}
			
			
}

}


function checkAnyChecked() {
	var blnchecked = false;
	if(document.getElementById("rdbArea").checked == true) {
		var hidden =  document.getElementById("hdnLocations");
		if(hidden.value == "" || hidden.value == "all" || hidden.value == "All")
		{
			alert("Please check any of the locations first.");
			return;
		}
	}
	else if(document.getElementById("rdbPostcode").checked == true) {
		var hidden =  document.getElementById("hdnPostcodes");
		if(hidden.value == "" || hidden.value == "all" || hidden.value == "All")
		{
			alert("Please check any of the postcodes first.");
			return;
		}
	}
	document.getElementById("btnSearch").click();
}
    </script>

    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
</head>
<body onload="SetFHSCatOnload(); SetClientCatOnload();">
    <form id="form1" runat="server">
    <div id="wrapper">
        <uc2:Admin_Header ID="Admin_Header1" runat="server" />
        <div id="Content">
            <div style="width: 100%; background: #ebebeb; padding: 5px 0; margin: 5px 0 1px 0;">
                &nbsp;<a href="edit_company.aspx">Add new company</a> |&nbsp;<a href="Company.aspx">Back
                    to companies</a>&nbsp;|&nbsp;
                    
                    <%--==============================================================================================================================--%>
                    <a href="#" id="CompanyNotes" runat="server" class="text_button_trans">FHS notes</a>                              
                <%--<asp:LinkButton ID="CompanyNotes" runat="server" CausesValidation="False" OnClick="CompanyNotes_Click" Text="FHS notes"></asp:LinkButton>--%>
                &nbsp;|&nbsp;
                <a href="#" id="CRNotes" runat="server" class="text_button_trans">Client notes</a>                              
                <%--<asp:LinkButton ID="CRNotes" runat="server" CausesValidation="False" OnClick="CRNotes_Click"  Text="Client notes"></asp:LinkButton>--%>
                    <%--=============================================================================================================================--%>
                    </div>
            <div id="DivEntry" runat="server">
                <table>
                    <tr>
                        <td align="left" valign="top">
                            <table style="width: 524px" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="left" class="titleText" valign="top" style="display: none;">
                                        Companies&nbsp;|&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 424px">
                                        <table width="424" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="left" valign="top">
                                                    <img height="16" src="../images/loginBox_top.png" width="420" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="background: url(../images/loginBox_mid.png) repeat-y left top;">
                                                    <div style="text-align: center">
                                                        <table cellpadding="8">
                                                            <tr>
                                                                <td align="left" valign="top" style="width: 2px;">
                                                                </td>
                                                                <td width="130" align="left" valign="top" class="whitetext1">
                                                                    Company ID:
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext2">
                                                                    <asp:Label ID="lblId" runat="server">New</asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" valign="top" style="width: 2px;">
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext1">
                                                                    Company name:
                                                                </td>
                                                                <td align="left" valign="top">
                                                                    <asp:TextBox ID="TxtCompany" runat="server" MaxLength="50" Width="200px"></asp:TextBox>
                                                                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator1">
                                                                    </cc1:ValidatorCalloutExtender>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TxtCompany"
                                                                        ErrorMessage="Please enter company name.">*</asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" valign="top" style="width: 2px;">
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext1">
                                                                    Active:
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext2">
                                                                    <asp:CheckBox ID="chkActive" runat="server" Checked="True" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" valign="top" style="width: 2px;">
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext1">
                                                                    Default selection:
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext2">
                                                                    <asp:CheckBox ID="chkDefault" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" valign="top" style="width: 2px;">
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext1" colspan="2">
                                                                    FHS Notes:<br>
                                                                    <asp:DataList ID="Repeater1" runat="server" RepeatDirection="Horizontal" RepeatColumns="3"
                                                                        CssClass="whitetext2">
                                                                        <ItemTemplate>
                                                                            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                                                                                <tr align="left">
                                                                                    <td align="left">
                                                                                        <input type="checkbox" id="chkLocation2" runat="server" title='<%# DataBinder.Eval(Container.DataItem,"Categoy_name") %>'
                                                                                            onclick="javascript:setFHSCategories();" />
                                                                                        <%# DataBinder.Eval(Container.DataItem, "Categoy_name")%>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ItemTemplate>
                                                                    </asp:DataList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" valign="top" style="width: 2px;">
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext1" colspan="4">
                                                                    Client Notes:<br>
                                                                    <asp:DataList ID="Repeater2" runat="server" RepeatDirection="Horizontal" RepeatColumns="3"
                                                                        CssClass="whitetext2">
                                                                        <ItemTemplate>
                                                                            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                                                                                <tr align="left">
                                                                                    <td align="left">
                                                                                        <input type="checkbox" id="chkLocation" runat="server" title='<%# DataBinder.Eval(Container.DataItem,"Categoy_name") %>'
                                                                                            onclick="javascript:setClientCategories();" />
                                                                                        <%# DataBinder.Eval(Container.DataItem, "Categoy_name")%>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ItemTemplate>
                                                                    </asp:DataList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" valign="top" style="width: 2px;">
                                                                </td>
                                                                <td width="135" align="left" valign="top" class="whitetext1">
                                                                    Description:
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext2">
                                                                    <asp:TextBox ID="txtDescription" runat="server" MaxLength="180" Rows="10" TextMode="MultiLine"
                                                                        Width="200px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" valign="top">
                                                                </td>
                                                                <td align="left" valign="top">
                                                                </td>
                                                                <td align="left" valign="top">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <img height="16" src="../images/loginBox_btm.png" width="420" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top">
                                        <table width="80%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="height: 34px">
                                                    <hr />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <asp:Button ID="btnCreate" runat="server" Text="Create" OnClick="btnCreate_Click"
                                                        CssClass="blueBtns" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 424px">
                                        <div id="divMessage" style="color: Red" runat="server" visible="true">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="prevName" runat="server" />
            </div>
            <asp:HiddenField ID="hdnFHSCategories" runat="server" Value="All" />
            <asp:HiddenField ID="hdnClientCategories" runat="server" Value="All" />
            <div>
            </div>
        </div>
    </div>
    <uc1:footer ID="Footer1" runat="server" />
    </form>
</body>
</html>
