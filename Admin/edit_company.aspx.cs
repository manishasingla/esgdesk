using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.Drawing.Drawing2D;
using System.IO;
public partial class Admin_edit_company : System.Web.UI.Page
{
    int id;
    string sql = "";
    static ArrayList arrAllFHSCAtegories = new ArrayList();
    static ArrayList arrAllClientCategories = new ArrayList();
    public string company_name = "";
    protected void Page_Load(object sender, EventArgs e)
    {

       // btnCreate.Attributes.Add("Onclick", "return checkProject();");

        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = "edit_company.aspx";
            Response.Redirect("login.aspx");
            return;
        }

        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            Response.Redirect("tasks.aspx");
            return;
        }

        string var = Request.QueryString["id"];
        if (var == null)
        {
            id = 0;
            this.Title = "Add company";
        }
        else
        {
            try
            {
                id = Convert.ToInt32(var);
                this.Title = "Edit company";
            }
            catch
            {
                divMessage.InnerHtml = "Company ID must be an integer.";
                DivEntry.Visible = false;
                return;
            }
        }

        if (!IsPostBack)
        {
            // add or edit?
            
            if (id == 0)
            {
                clearControls();
            }
            else
            {


                try
                {

                    sql = "select * from Notes_Categories where Category_Type='FHS'";


                    DataSet dsNCategories = DatabaseHelper.getDataset(sql);

                    arrAllFHSCAtegories.Clear();
                    if (dsNCategories != null && dsNCategories.Tables.Count > 0 && dsNCategories.Tables[0].Rows.Count > 0)
                    {

                     

                        for (int i = 0; i < dsNCategories.Tables[0].Rows.Count; i++)
                        {
                           
                            arrAllFHSCAtegories.Add(dsNCategories.Tables[0].Rows[i]["Categoy_name"].ToString());
                        }


                        Repeater1.DataSource = dsNCategories.Tables[0];
                        Repeater1.DataBind();


                    }
                }
                catch { }

                try
                {

                    sql = "select * from Notes_Categories where Category_Type='Client'";


                    DataSet dsClientCategories = DatabaseHelper.getDataset(sql);

                    arrAllClientCategories.Clear();
                    if (dsClientCategories != null && dsClientCategories.Tables.Count > 0 && dsClientCategories.Tables[0].Rows.Count > 0)
                    {



                        for (int i = 0; i < dsClientCategories.Tables[0].Rows.Count; i++)
                        {

                            arrAllClientCategories.Add(dsClientCategories.Tables[0].Rows[i]["Categoy_name"].ToString());
                        }


                        Repeater2.DataSource = dsClientCategories.Tables[0];
                        Repeater2.DataBind();


                    }
                }
                catch { }




                string strSql = "select * from Company where Company_id=" + id;
                DataSet ds = DatabaseHelper.getDataset(strSql);

                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        lblId.Text = ds.Tables[0].Rows[0]["Company_id"].ToString();
                        TxtCompany.Text = ds.Tables[0].Rows[0]["Company_Name"].ToString();
                        prevName.Value = ds.Tables[0].Rows[0]["Company_Name"].ToString();
                        company_name = ds.Tables[0].Rows[0]["Company_Name"].ToString();
                        txtDescription.Text = ds.Tables[0].Rows[0]["description"].ToString();
                        chkDefault.Checked = ds.Tables[0].Rows[0]["default_selection"].ToString() == "Y" ? true : false;
                        chkActive.Checked = ds.Tables[0].Rows[0]["active"].ToString() == "Y" ? true : false;
                        btnCreate.Text = "Update";
                        DivEntry.Visible = true;
                        DivEntry.Visible = true;
                        divMessage.InnerHtml = "";
                      
                        //============================================================================
                            string strCNotes = "window.open('companyNotes.aspx?CompName=" + prevName.Value +"','_blank');";
                            CompanyNotes.Attributes.Add("onClick", strCNotes);
                            string strCRNotes = "window.open('CRNotes.aspx?CompName=" + prevName.Value +"','_blank');";
                            CRNotes.Attributes.Add("onClick", strCRNotes);
                       //===============================================================================






                        string sqlcompanynote = "select count(*) from [Company_notes]  where Company_Name = '" + prevName.Value.ToString() + "'and Note_Type ='FHS' and Allow_Notes='True'";
                        int objNoteResult = int.Parse(DatabaseHelper.executeScalar(sqlcompanynote).ToString());
                        try
                        {
                            if (objNoteResult > 0)
                            {
                                CompanyNotes.Visible = true;
                                //CompanyNotes.ForeColor = Color.Red;
                                CompanyNotes.Style.Add("color", "#FF0000");

                            }
                            else
                            {
                                CompanyNotes.Visible = true;
                                //CompanyNotes.ForeColor = Color.Blue;
                                CompanyNotes.Style.Add("color", "#0033ff");
                            }
                        }
                        catch { }
                        string sqlCRnote = "select count(*) from [Company_notes]  where Company_Name = '" + prevName.Value.ToString() + "' and Note_Type ='Client'  and Allow_Notes='True'";
                        int objCRNoteResult = int.Parse(DatabaseHelper.executeScalar(sqlCRnote).ToString());

                        try
                        {
                            if (objCRNoteResult > 0)
                            {
                                CRNotes.Visible = true;
                                //CRNotes.ForeColor = Color.Red;
                                CRNotes.Style.Add("color", "#FF0000");


                            }
                            else
                            {
                                CRNotes.Visible = true;
                                //CRNotes.ForeColor = Color.Blue;
                                CRNotes.Style.Add("color", "#0033ff");
                            }
                        }
                        catch { }


                        string strSql2 = "select * from Company_notes where Company_name='" + prevName.Value + "' and Note_Type='FHS' and Allow_Notes = 'True'";
                        DataSet dsaddedFhsCat = DatabaseHelper.getDataset(strSql2);
                        if (dsaddedFhsCat.Tables[0].Rows.Count > 0)
                        {
                            hdnFHSCategories.Value ="";
                            for (int i = 0; i < dsaddedFhsCat.Tables[0].Rows.Count; i++)
                            {
                                hdnFHSCategories.Value += dsaddedFhsCat.Tables[0].Rows[i]["Category_name"].ToString() + ";";
                            }
                        }


                        string strSql3 = "select * from Company_notes where Company_name='" + prevName.Value + "' and Note_Type='Client' and Allow_Notes = 'True'";
                        DataSet dsaddedClientCat = DatabaseHelper.getDataset(strSql3);
                        if (dsaddedClientCat.Tables[0].Rows.Count > 0)
                        {
                            hdnClientCategories.Value = "";
                            for (int i = 0; i < dsaddedClientCat.Tables[0].Rows.Count; i++)
                            {
                                hdnClientCategories.Value += dsaddedClientCat.Tables[0].Rows[i]["Category_name"].ToString() + ";";
                            }
                        }
                      
                    }
                    else
                    {
                        divMessage.InnerHtml = "Company was not found.";
                        hdnFHSCategories.Value = "";
                        DivEntry.Visible = false;
                    }
                }
                else
                {
                    hdnFHSCategories.Value = "";
                    divMessage.InnerHtml = "Company was not found.";
                    DivEntry.Visible = false;
                }
            }
 
        }
        else
        {
           
        }

        
    }

    

   

    protected void btnCreate_Click(object sender, EventArgs e)
    {
        if (TxtCompany.Text.Trim() == "")
        {
            divMessage.InnerHtml = "Please enter company name.";
            return;
        }

        if (btnCreate.Text == "Create")
        {
            object objResult = DatabaseHelper.executeScalar("select count(*) from Company where [Company_Name]='" + TxtCompany.Text.Trim() + "'");

            if (objResult.ToString() != "0")
            {
                divMessage.InnerHtml = "Company name alredy exist.";
                return;
            }


            string strSql = " insert into Company ([Company_Name],[description],[default_selection],[active]) ";
            strSql += " values('" + TxtCompany.Text.Trim().Replace("'", "''") + "','" + txtDescription.Text.Trim().Replace("'", "''") + "','" + (chkDefault.Checked ? "Y" : "N") + "','" + (chkActive.Checked ? "Y" : "N") + "')";

            int intResult = DatabaseHelper.executeNonQuery(strSql);

            if (intResult != 0)
            {


                try
                {

                    string strCkdCategories = hdnFHSCategories.Value.Trim();

                    strCkdCategories = strCkdCategories.ToLower() == "" || strCkdCategories.ToLower() == "select" ? "All" : strCkdCategories;

                  
                     string[] strFhsCategory = strCkdCategories.Split(new char[] { ';' });

                     ArrayList ChkdFHSCategories = new ArrayList();
                     try
                     {
                         for (int k = 0; k < strFhsCategory.Length - 1; k++)
                         {
                             ChkdFHSCategories.Add(strFhsCategory[k].ToString());

                         }
                     }
                     catch { }


                     try
                     {
                         string FHSNotes = "";
                         int intResultFhsCat = 0;
                         for (int j = 0; j < arrAllFHSCAtegories.Count; j++)
                         {
                             try
                             {
                                 object FHSCatname = arrAllFHSCAtegories[j].ToString();
                                 if (ChkdFHSCategories.Contains(FHSCatname.ToString()) == false)
                                 {

                                     string strSql2 = " insert into Company_notes ([Company_name],[Note_Type],[Category_name],[Notes],[Allow_Notes]) ";
                                     strSql2 += " values('" + TxtCompany.Text.Trim().Replace("'", "''") + "','FHS','" + FHSCatname.ToString() + "','" + FHSNotes + "','False')";

                                     intResultFhsCat = DatabaseHelper.executeNonQuery(strSql2);



                                 }
                                 else
                                 {
                                     string strSql3 = " insert into Company_notes ([Company_name],[Note_Type],[Category_name],[Notes],[Allow_Notes]) ";
                                     strSql3 += " values('" + TxtCompany.Text.Trim().Replace("'", "''") + "','FHS','" + FHSCatname.ToString() + "','" + FHSNotes + "','True')";

                                     intResultFhsCat = DatabaseHelper.executeNonQuery(strSql3);
                                 }
                             }
                             catch 
                             {
                                 divMessage.InnerHtml = "Company was created.Some problem while adding FHS notes category.";
                             
                             }


                         }
                     }
                     catch
                     {
                         divMessage.InnerHtml = "Company was created.Some problem while adding FHS notes category.";
                     }

                    //int introw2 = DatabaseHelper.insertArea(agencyid, strReqAreas);

                }
                catch { }



                try
                {

                    string strClientCategories = hdnClientCategories.Value.Trim();




                    string[] strClientCategory = strClientCategories.Split(new char[] { ';' });

                    ArrayList ChkdClientCategories = new ArrayList();
                    try
                    {
                        for (int k = 0; k < strClientCategory.Length - 1; k++)
                        {
                            ChkdClientCategories.Add(strClientCategory[k].ToString());

                        }
                    }
                    catch { }


                    try
                    {
                        string ClientNotes = "";
                        int intResultClientCat = 0;
                        for (int j = 0; j < arrAllClientCategories.Count; j++)
                        {
                            try
                            {
                                object ClientCatname = arrAllClientCategories[j].ToString();
                                if (ChkdClientCategories.Contains(ClientCatname.ToString()) == false)
                                {

                                    string strSql2 = " insert into Company_notes ([Company_name],[Note_Type],[Category_name],[Notes],[Allow_Notes]) ";
                                    strSql2 += " values('" + TxtCompany.Text.Trim().Replace("'", "''") + "','Client','" + ClientCatname.ToString() + "','" + ClientNotes + "','False')";

                                    intResultClientCat = DatabaseHelper.executeNonQuery(strSql2);



                                }
                                else
                                {
                                    string strSql3 = " insert into Company_notes ([Company_name],[Note_Type],[Category_name],[Notes],[Allow_Notes]) ";
                                    strSql3 += " values('" + TxtCompany.Text.Trim().Replace("'", "''") + "','Client','" + ClientCatname.ToString() + "','" + ClientNotes + "','True')";

                                    intResultClientCat = DatabaseHelper.executeNonQuery(strSql3);
                                }
                            }
                            catch
                            {
                                divMessage.InnerHtml = "Company was created.Some problem while adding client notes category.";

                            }


                        }
                    }
                    catch
                    {
                        divMessage.InnerHtml = "Company was created.Some problem while adding client notes category.";
                    }

                    //int introw2 = DatabaseHelper.insertArea(agencyid, strReqAreas);

                }
                catch { }



                divMessage.InnerHtml = "Company was created.";

                clearControls();
                ///Response.Redirect("edit_company.aspx?id="+lblId.Text, false);

              
            }
            else
            {
                divMessage.InnerHtml = "Company was not created.";
            }
        }
        else if (btnCreate.Text == "Update")
        {
            object objResult = DatabaseHelper.executeScalar("select count(*) from Company where [Company_Name]='" + TxtCompany.Text.Trim() + "' and [Company_id]!=" + lblId.Text);

            if (objResult.ToString() != "0")
            {
                divMessage.InnerHtml = "Company name alredy exist.";
                return;
            }

            string strSql = " update Company ";
            strSql += " set [Company_Name]= '" + TxtCompany.Text.Trim().Replace("'", "''") + "',";
            strSql += " [description] = '" + txtDescription.Text.Trim().Replace("'", "''")  + "',";
            strSql += " [default_selection] = '" + (chkDefault.Checked ? "Y" : "N") + "',";
            strSql += " [active] = '" + (chkActive.Checked ? "Y" : "N") +"'";

            strSql += " where Company_id = " + lblId.Text.Trim();

            int intResult = DatabaseHelper.executeNonQuery(strSql);

            if (intResult != 0)
            {
               


                strSql = "update NonesCRMusers set CompanyName = '" + TxtCompany.Text.Trim().Replace("'", "''") + "' ";
                strSql += " where CompanyName ='" + prevName.Value.Replace("'", "''") + "'";
                intResult = DatabaseHelper.executeNonQuery(strSql);
                strSql = "update ClientRequest set CompanyName = '" + TxtCompany.Text.Trim().Replace("'", "''") + "' ";
                strSql += " where CompanyName ='" + prevName.Value.Replace("'", "''") + "'";
                intResult = DatabaseHelper.executeNonQuery(strSql);
                strSql = "update projects set CompanyName = '" + TxtCompany.Text.Trim().Replace("'", "''") + "' ";
                strSql += " where CompanyName ='" + prevName.Value.Replace("'", "''") + "'";
                intResult = DatabaseHelper.executeNonQuery(strSql);
                strSql = "update tasks set Company = '" + TxtCompany.Text.Trim().Replace("'", "''") + "' ";
                strSql += " where Company ='" + prevName.Value.Replace("'", "''") + "'";
                intResult = DatabaseHelper.executeNonQuery(strSql);
                strSql = "update Company_notes set Company_name = '" + TxtCompany.Text.Trim().Replace("'", "''") + "' ";
                strSql += " where Company_name ='" + prevName.Value.Replace("'", "''") + "'";
                intResult = DatabaseHelper.executeNonQuery(strSql);
                
                try
                {

                    string strCkdCategories = hdnFHSCategories.Value.Trim();

                    ///strCkdCategories = strCkdCategories.ToLower() == "" || strCkdCategories.ToLower() == "select" ? "All" : strCkdCategories;


                    string[] strFhsCategory = strCkdCategories.Split(new char[] { ';' });

                    ArrayList ChkdFHSCategories = new ArrayList();
                    try
                    {
                        for (int k = 0; k < strFhsCategory.Length - 1; k++)
                        {
                            ChkdFHSCategories.Add(strFhsCategory[k].ToString());

                        }
                    }
                    catch { }


                    try
                    {
                        string FHSNotes = "";
                        string strSqlCat = "";
                        int intResultCat = 0;
                        for (int j = 0; j < arrAllFHSCAtegories.Count; j++)
                        {
                            try
                            {
                                object FHSCatname = arrAllFHSCAtegories[j].ToString();
                                if (ChkdFHSCategories.Contains(FHSCatname.ToString()) == false)
                                {
                                    object objResultFcat = DatabaseHelper.executeScalar("select count(*) from Company_notes where [Company_Name]='" + TxtCompany.Text.Trim() + "' and Note_Type ='FHS'and Category_name ='" + FHSCatname.ToString() + "'");
                                    if (objResultFcat.ToString() != "0")
                                    {
                                        strSqlCat = "Update Company_notes set Allow_Notes ='False' where Company_name = '" + TxtCompany.Text.Trim().Replace("'", "''") + "' and Note_Type ='FHS'and Category_name ='" + FHSCatname.ToString() + "'";


                                        intResultCat = DatabaseHelper.executeNonQuery(strSqlCat);
                                    }
                                    else
                                    {
                                       strSqlCat = " insert into Company_notes ([Company_name],[Note_Type],[Category_name],[Notes],[Allow_Notes]) ";
                                        strSqlCat += " values('" + TxtCompany.Text.Trim().Replace("'", "''") + "','FHS','" + FHSCatname.ToString() + "','" + FHSNotes + "','False')";

                                        intResultCat = DatabaseHelper.executeNonQuery(strSqlCat);
                                    }

                                }
                                else
                                {

                                    object objResultFcat2 = DatabaseHelper.executeScalar("select count(*) from Company_notes where [Company_Name]='" + TxtCompany.Text.Trim() + "' and Note_Type ='FHS'and Category_name ='" + FHSCatname.ToString() + "'");
                                    if (objResultFcat2.ToString() != "0")
                                    {
                                        strSqlCat = "Update Company_notes set Allow_Notes ='True' where Company_name = '" + TxtCompany.Text.Trim().Replace("'", "''") + "' and Note_Type ='FHS'and Category_name ='" + FHSCatname.ToString() + "'";

                                        intResultCat = DatabaseHelper.executeNonQuery(strSqlCat);
                                    }
                                    else
                                    {
                                        strSqlCat = " insert into Company_notes ([Company_name],[Note_Type],[Category_name],[Notes],[Allow_Notes]) ";
                                        strSqlCat += " values('" + TxtCompany.Text.Trim().Replace("'", "''") + "','FHS','" + FHSCatname.ToString() + "','" + FHSNotes + "','True')";

                                        intResultCat = DatabaseHelper.executeNonQuery(strSqlCat);
                                    }

                                   
                                }
                            }
                            catch
                            {
                                divMessage.InnerHtml = "Company was created.Some problem while adding FHS notes category.";

                            }


                        }
                    }
                    catch
                    {
                        divMessage.InnerHtml = "Company was created.Some problem while adding FHS notes category.";
                    }

                    //int introw2 = DatabaseHelper.insertArea(agencyid, strReqAreas);

                }
                catch { }


                try
                {

                    string strClientCategories = hdnClientCategories.Value.Trim();

                    ///strCkdCategories = strCkdCategories.ToLower() == "" || strCkdCategories.ToLower() == "select" ? "All" : strCkdCategories;


                    string[] strClientCategory = strClientCategories.Split(new char[] { ';' });

                    ArrayList ChkdClientCategories = new ArrayList();
                    try
                    {
                        for (int k = 0; k < strClientCategory.Length - 1; k++)
                        {
                            ChkdClientCategories.Add(strClientCategory[k].ToString());

                        }
                    }
                    catch { }


                    try
                    {
                        string ClientNotes = "";
                        string strSqlCat = "";
                        int intResultClientCat = 0;
                        for (int j = 0; j < arrAllClientCategories.Count; j++)
                        {
                            try
                            {
                                object ClientCatname = arrAllClientCategories[j].ToString();
                                if (ChkdClientCategories.Contains(ClientCatname.ToString()) == false)
                                {
                                    object objResultCcat = DatabaseHelper.executeScalar("select count(*) from Company_notes where [Company_Name]='" + TxtCompany.Text.Trim() + "' and Note_Type ='Client'and Category_name ='" + ClientCatname.ToString() + "'");
                                    if (objResultCcat.ToString() != "0")
                                    {
                                        strSqlCat = "Update Company_notes set Allow_Notes ='False' where Company_name = '" + TxtCompany.Text.Trim().Replace("'", "''") + "' and Note_Type ='Client'and Category_name ='" + ClientCatname.ToString() + "'";


                                        intResultClientCat = DatabaseHelper.executeNonQuery(strSqlCat);
                                    }
                                    else
                                    {
                                        strSqlCat = " insert into Company_notes ([Company_name],[Note_Type],[Category_name],[Notes],[Allow_Notes]) ";
                                        strSqlCat += " values('" + TxtCompany.Text.Trim().Replace("'", "''") + "','Client','" + ClientCatname.ToString() + "','" + ClientNotes + "','False')";

                                        intResultClientCat = DatabaseHelper.executeNonQuery(strSqlCat);
                                    }

                                }
                                else
                                {

                                    object objResultCcat2 = DatabaseHelper.executeScalar("select count(*) from Company_notes where [Company_Name]='" + TxtCompany.Text.Trim() + "' and Note_Type ='Client'and Category_name ='" + ClientCatname.ToString() + "'");
                                    if (objResultCcat2.ToString() != "0")
                                    {
                                        strSqlCat = "Update Company_notes set Allow_Notes ='True' where Company_name = '" + TxtCompany.Text.Trim().Replace("'", "''") + "' and Note_Type ='Client'and Category_name ='" + ClientCatname.ToString() + "'";

                                        intResultClientCat = DatabaseHelper.executeNonQuery(strSqlCat);
                                    }
                                    else
                                    {
                                        strSqlCat = " insert into Company_notes ([Company_name],[Note_Type],[Category_name],[Notes],[Allow_Notes]) ";
                                        strSqlCat += " values('" + TxtCompany.Text.Trim().Replace("'", "''") + "','Client','" + ClientCatname.ToString() + "','" + ClientNotes + "','True')";

                                        intResultClientCat = DatabaseHelper.executeNonQuery(strSqlCat);
                                    }


                                }
                            }
                            catch
                            {
                                divMessage.InnerHtml = "Company was created.Some problem while adding FHS notes category.";

                            }


                        }
                    }
                    catch
                    {
                        divMessage.InnerHtml = "Company was created.Some problem while adding FHS notes category.";
                    }

                    //int introw2 = DatabaseHelper.insertArea(agencyid, strReqAreas);

                }
                catch { }




                divMessage.InnerHtml = "Company was updated.";
                Response.Redirect("edit_company.aspx?id=" + lblId.Text, false);

            }
            else
            {
                divMessage.InnerHtml = "Company was not updated.";
            }
        }
    }
    
    private void clearControls()
    {
        lblId.Text = "New";
        TxtCompany.Text = "";
        chkActive.Checked = true;
        txtDescription.Text = "";
        btnCreate.Text = "Create";
    }



    //protected void CompanyNotes_Click(object sender, EventArgs e)
    //{
    //    Session["TaskComp"] = prevName.Value;
    //    Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> window.open('companyNotes.aspx','','status=no,scrollbars=yes,position=center,resizable=yes,maximize=yes'); </script>");
    //}
    //protected void CRNotes_Click(object sender, EventArgs e)
    //{
    //    Session["TaskComp"] = prevName.Value;
    //    Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> window.open('CRNotes.aspx','','status=no,scrollbars=yes,position=center,resizable=yes,maximize=yes'); </script>");
    //}
}
