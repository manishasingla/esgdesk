<%@ Page Language="C#" AutoEventWireup="true" CodeFile="edit_project.aspx.cs" Inherits="Admin_edit_project" %>

<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        Add/Edit project
    </title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        function checkProject() {
            var project = document.getElementById("txtName").value;
            var drpprjct = document.getElementById("drpProjects").value;
            var rlevanUrl = document.getElementById("txtRelevanUrl").value;
            if (drpprjct == "other") {

                if (project == "") {
                    alert("Please enter project name");
                    return false;
                }

            }

            /*else if(document.getElementById("chkRelevanturl").checked)
  
            {
            alert("start");
            if(rlevanUrl == "")
            {
            alert("start2");
            alert("Please enter relevant url");
            return false;
            }
    
            }*/
            else {
                var btnsend = document.getElementById("btnAddProject");
                btnsend.click();
            }

        }
    
    </script>

    <script language="javascript" type="text/javascript">
        function showrelevanturl() {


            if (document.getElementById("chkRelevanturl").checked) {

                document.getElementById("TrRelevantUrl").style.display = "Block";
            }


            else {
                document.getElementById("TrRelevantUrl").style.display = "none";
                // return false;
            }

        }

        function editprojectname() {
            document.getElementById("TDEditProjectName").style.display = "Block";
            document.getElementById("TDAddProjectName").style.display = "none";
            var txt = document.getElementById("txtProjectName");
            var e = document.getElementById("drpProjects");
            var strproj = e.options[e.selectedIndex].value;
            txt.value = strproj;
            document.getElementById("<%= lblAddProjectMsg.ClientID %>").innerHTML="";
            document.getElementById("<%= lblProjectMsg.ClientID %>").innerHTML="";
            document.title = "Edit project - " + txt.value.toString();
        }
        function closeeditprojectname() {
            document.getElementById("TDEditProjectName").style.display = "none";
        }
        function updateeditprojectname() {
            document.getElementById("<%= btnUpdateProject.ClientID %>").click();
        }
        
        //****************Add new Project*****************************************
        
        function newaddprojectname()
         {
             document.title = "Add project";
            document.getElementById("TDAddProjectName").style.display = "Block";
            document.getElementById("TDEditProjectName").style.display = "none";
            
            document.getElementById("<%= lblAddProjectMsg.ClientID %>").innerHTML="";
            document.getElementById("<%= lblProjectMsg.ClientID %>").innerHTML="";
            
            document.getElementById("<%= txtAddProjectName.ClientID %>").value=""; 
         }
        function cancelprojectname() {
            document.getElementById("TDAddProjectName").style.display = "none";
        }
        function addprojectname() {
            document.getElementById("<%= btnAddProject.ClientID %>").click();
        }
        
        //*********************************************************
        
        
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div id="wrapper">
            <uc2:Admin_Header ID="Admin_Header1" runat="server" />
            <div id="Content">
                <div style="width: 100%; background: #ebebeb; padding: 5px 0;">
                    &nbsp;&nbsp;<a href="edit_project.aspx">To add a project go to Edit Project area</a>
                    &nbsp;|&nbsp; <a href="projects.aspx">Back to projects</a> <a href="Company.aspx"
                        id="FrmCompLnk" runat="server">&nbsp;|&nbsp;Back to companies</a>&nbsp;
                </div>
                <table border="0">
                    <tr>
                        <td align="left" valign="top">
                            <table style="width: 424px">
                                <tr>
                                    <td align="left" class="titleText" valign="top" style="width: 424px; display: none;">
                                        Projects
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 424px">
                                        <table width="424" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="left" valign="top">
                                                    <img height="16" src="../images/loginBox_top.png" width="420" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="background: url(../images/loginBox_mid.png) repeat-y left top;">
                                                    <div style="text-align: center">
                                                        <div id="DivEntry" runat="server">
                                                            <table cellpadding="5" border="0">
                                                                <tr>
                                                                    <%--<td align="left" valign="top">
                                                                    </td>--%>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        Project ID:
                                                                    </td>
                                                                    <td align="left" valign="top" class="whitetext2">
                                                                        <asp:Label ID="lblId" runat="server">New</asp:Label>
                                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                        <a href="" id="LnkGoogleAnalytics" runat="server">Add google analytics</a>&nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <%--<td align="left" valign="top" class="whitetext1">
                                                                    </td>--%>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        Company name:
                                                                    </td>
                                                                    <td align="left" valign="top" class="whitetext2">
                                                                        <asp:DropDownList ID="drpCompany" runat="server" CssClass="filerDrpodown" AutoPostBack="True"
                                                                            Width="226px" OnSelectedIndexChanged="drpCompany_SelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="drpCompany"
                                                                        ErrorMessage="Please select company name." InitialValue=" ">*</asp:RequiredFieldValidator>
                                                                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" TargetControlID="RequiredFieldValidator2">
                                                                    </cc1:ValidatorCalloutExtender>--%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <%--<td align="left" valign="top" class="whitetext1">
                                                                    </td>--%>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        Project name:
                                                                    </td>
                                                                    <td align="left" valign="top" class="whitetext2">
                                                                        <asp:DropDownList ID="drpProjects" runat="server" CssClass="filerDrpodown" AutoPostBack="true"
                                                                            Width="226px" OnSelectedIndexChanged="drpProjects_SelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                                        <asp:RequiredFieldValidator ID="ReqdrpProjects" runat="server" ControlToValidate="drpProjects"
                                                                            InitialValue="" ErrorMessage="Please enter project name" Display="None"></asp:RequiredFieldValidator>
                                                                        <a id="btnadd" runat="server" href="#" onclick="newaddprojectname();">Add</a> <a
                                                                            id="btnedit" runat="server" href="#" onclick="editprojectname();">Edit</a>
                                                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="drpProjects"
                                                                        ErrorMessage="Please select project name." InitialValue=" ">*</asp:RequiredFieldValidator>
                                                                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator1">
                                                                    </cc1:ValidatorCalloutExtender>--%>
                                                                        <table id="TDEditProjectName" border="0" runat="server" style="border: solid 1px #999999;
                                                                            padding-top: 10px; display: none">
                                                                            <tr style="padding-top: 7px">
                                                                                <td>
                                                                                    <asp:TextBox ID="txtProjectName" runat="server" Width="290px" MaxLength="200"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="right">
                                                                                    <asp:Button ID="btnUpdateProject" runat="server" Text="Add" CausesValidation="false"
                                                                                        OnClick="btnUpdateProject_Click" Style="display: none" />
                                                                                    <a id="btnUpdateProj" runat="server" href="#" onclick="updateeditprojectname();">Update</a>
                                                                                    <a id="btnCloseProject" runat="server" href="#" onclick="closeeditprojectname();">Cancel</a>
                                                                                    <br />
                                                                                    <span style="padding-right: 100px">
                                                                                        <asp:Label ID="lblProjectMsg" runat="server" ForeColor="Red" Text=""></asp:Label>
                                                                                    </span>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <%--***************************************************************************************************--%>
                                                                        <table id="TDAddProjectName" runat="server" style="border: solid 1px #999999; padding-top: 10px;
                                                                            display: none" border="2">
                                                                            <tr style="padding-top: 7px">
                                                                                <td>
                                                                                    <asp:TextBox ID="txtAddProjectName" runat="server" Width="290px" MaxLength="200"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="right">
                                                                                    <asp:Button ID="btnAddProject" runat="server" Text="AddProject" CausesValidation="false"
                                                                                        Style="display: none" OnClick="btnAddProject_Click" />
                                                                                    <a id="btnAddProj" runat="server" href="#" onclick="addprojectname();">Add</a> <a
                                                                                        id="btnCancelProject" runat="server" href="#" onclick="cancelprojectname();">Cancel</a>
                                                                                    <br />
                                                                                    <span style="padding-right: 100px">
                                                                                        <asp:Label ID="lblAddProjectMsg" runat="server" ForeColor="Red" Text=""></asp:Label>
                                                                                    </span>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <%--***************************************************************************************************--%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <%--<td align="left" valign="top" class="whitetext1">
                                                                    </td>--%>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        Website url:
                                                                    </td>
                                                                    <td align="left" valign="top" class="whitetext2">
                                                                        <asp:TextBox ID="txtwebsite" runat="server" MaxLength="50" Width="220px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <%--<td align="left" valign="top" class="whitetext1">
                                                                    </td>--%>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        <span id="DataGrid1_ctl02_lblComment">Relevant url:</span>
                                                                    </td>
                                                                    <td align="left" valign="top" class="whitetext2">
                                                                        <asp:CheckBox ID="chkRelevanturl" Checked="true" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr id="TrRelevantUrl">
                                                                    <%--<td align="left" valign="top" class="whitetext1">
                                                                    </td>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                    </td>--%>
                                                                      <td align="left" valign="top" class="whitetext1">
                                                                    </td>
                                                                    <td valign="top" class="whitetext2" style="text-align: left;">
                                                                        <asp:TextBox ID="txtRelevanUrl" runat="server" MaxLength="1000" Rows="7" TextMode="MultiLine"
                                                                            Width="220px"></asp:TextBox>
                                                                        <%--<asp:RequiredFieldValidator ID="reqRelevanUrl" runat="server" ControlToValidate="txtRelevanUrl"
                                                                            ErrorMessage="Please enter relevant url" Display="None"></asp:RequiredFieldValidator>--%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <%--***********************************************************************--%>
                                                                    <td colspan="2" align="left">
                                                                        <asp:Panel ID="websitePanel" runat="server">
                                                                            <table cellpadding="2" border="0">
                                                                                <tr>
                                                                                    <td align="left" valign="top" class="whitetext1">
                                                                                    </td>
                                                                                    <td align="left" valign="top" class="whitetext1">
                                                                                        Host address/IP:
                                                                                    </td>
                                                                                    <td align="left" valign="top" class="whitetext2">
                                                                                        <asp:TextBox ID="txtFTPIP" runat="server" MaxLength="50" Width="220px"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr><td colspan="3" height="3"></td></tr>
                                                                                <tr>
                                                                                    <td align="left" valign="top" class="whitetext1">
                                                                                    </td>
                                                                                    <td align="left" valign="top" class="whitetext1">
                                                                                        User name:
                                                                                    </td>
                                                                                    <td align="left" valign="top" class="whitetext2">
                                                                                        <asp:TextBox ID="txtFTPUsername" runat="server" MaxLength="50" Width="220px"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                 <tr><td colspan="3" height="3"></td></tr>
                                                                                <tr>
                                                                                    <td align="left" valign="top" class="whitetext1">
                                                                                    </td>
                                                                                    <td align="left" valign="top" class="whitetext1">
                                                                                        Password:
                                                                                    </td>
                                                                                    <td align="left" valign="top" class="whitetext2">
                                                                                        <asp:TextBox ID="txtFTPPassword" runat="server" MaxLength="50" Width="220px"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                                <%--***********************************************************************--%>
                                                                <%--*****************************************************--%>
                                                                <tr>
                                                                    <%--<td align="left" valign="top" class="whitetext1">
                                                                    </td>--%>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        Test Domain Url
                                                                    </td>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        <asp:TextBox ID="txttestdomainUrl" runat="server" MaxLength="50" Width="220px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" align="left">
                                                                        <asp:Panel ID="testdomainPanel" runat="server">
                                                                            <table cellpadding="2" border="0">
                                                                                <tr>
                                                                                    <td align="left" valign="top" class="whitetext1">
                                                                                    </td>
                                                                                    <td align="left" valign="top" class="whitetext1">
                                                                                        Host address/IP:
                                                                                    </td>
                                                                                    <td align="left" valign="top" class="whitetext1">
                                                                                        <asp:TextBox ID="txtTestFTPIP" runat="server" MaxLength="50" Width="220px"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                 <tr><td colspan="3" height="3"></td></tr>
                                                                                <tr>
                                                                                    <td align="left" valign="top" class="whitetext1">
                                                                                    </td>
                                                                                    <td align="left" valign="top" class="whitetext1">
                                                                                        User name:
                                                                                    </td>
                                                                                    <td align="left" valign="top" class="whitetext1">
                                                                                        <asp:TextBox ID="txtTestFTPUsername" runat="server" MaxLength="50" Width="220px"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                 <tr><td colspan="3" height="3"></td></tr>
                                                                                <tr>
                                                                                    <td align="left" valign="top" class="whitetext1">
                                                                                    </td>
                                                                                    <td align="left" valign="top" class="whitetext1">
                                                                                        Password:
                                                                                    </td>
                                                                                    <td align="left" valign="top" class="whitetext1">
                                                                                        <asp:TextBox ID="txtTestFTPPassword" runat="server" MaxLength="50" Width="220px"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                                <%--*****************************************************--%>
                                                                <%--==========================================================--%>
                                                                <tr>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        Client id:
                                                                    </td>
                                                                    <td align="left" valign="top" class="whitetext2">
                                                                        <asp:TextBox ID="txtclientid" runat="server" Width="220px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        User name 1:
                                                                    </td>
                                                                    <td align="left" valign="top" class="whitetext2">
                                                                        <asp:TextBox ID="txtusername1" runat="server" Width="220px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        Password 1:
                                                                    </td>
                                                                    <td align="left" valign="top" class="whitetext2">
                                                                        <asp:TextBox ID="txtPassword1" runat="server" Width="220px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        User name 2:
                                                                    </td>
                                                                    <td align="left" valign="top" class="whitetext2">
                                                                        <asp:TextBox ID="txtusername2" runat="server" Width="220px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        Password 2:
                                                                    </td>
                                                                    <td align="left" valign="top" class="whitetext2">
                                                                        <asp:TextBox ID="txtPassword2" runat="server" Width="220px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <%--==========================================================--%>
                                                                <tr>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        Active:
                                                                    </td>
                                                                    <td align="left" valign="top" class="whitetext2">
                                                                        <asp:CheckBox ID="chkActive" runat="server" Checked="True" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        Archive:
                                                                    </td>
                                                                    <td align="left" valign="top" class="whitetext2">
                                                                        <asp:CheckBox ID="chkArchive" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <%--==========================================================--%>
                                                                <tr>
                                                                    <%--<td align="left" valign="top" class="whitetext1">
                                                                    </td>--%>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        Default selection:
                                                                    </td>
                                                                    <td align="left" valign="top" class="whitetext2">
                                                                        <asp:CheckBox ID="chkDefault" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <%--<td align="left" valign="top" class="whitetext1">
                                                                    </td>--%>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        Description:
                                                                    </td>
                                                                    <td align="left" valign="top" class="whitetext2">
                                                                        <asp:TextBox ID="txtDescription" runat="server" MaxLength="1000" Rows="3" TextMode="MultiLine"
                                                                            Width="220px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <%--===========================================================================--%>
                                                                <tr>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        User Login url:
                                                                    </td>
                                                                    <td align="left" valign="top" class="whitetext2">
                                                                        <asp:TextBox ID="txtuserloginUrl" runat="server" TextMode="MultiLine" MaxLength="1000" Rows="3" Width="220px" ></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        username:
                                                                    </td>
                                                                    <td align="left" valign="top" class="whitetext2">
                                                                        <asp:TextBox ID="txtusername" runat="server" MaxLength="50" Width="220px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        password:
                                                                    </td>
                                                                    <td align="left" valign="top" class="whitetext2">
                                                                        <asp:TextBox ID="txtuserpasseord" runat="server"  MaxLength="50" Width="220px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        admin login url:
                                                                    </td>
                                                                    <td align="left" valign="top" class="whitetext2">
                                                                        <asp:TextBox ID="txtadminloginurl" runat="server" TextMode="MultiLine" MaxLength="1000" Rows="3" Width="220px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        username:
                                                                    </td>
                                                                    <td align="left" valign="top" class="whitetext2">
                                                                        <asp:TextBox ID="txtuserAdminname" runat="server" MaxLength="50" Width="220px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        password:</td>
                                                                    <td align="left" valign="top" class="whitetext2">
                                                                        <asp:TextBox ID="txtAdminpassword" runat="server"  MaxLength="50" Width="220px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        control panel url:</td>
                                                                    <td align="left" valign="top" class="whitetext2">
                                                                        <asp:TextBox ID="txtcontrolpanelurl" runat="server"  TextMode="MultiLine" MaxLength="1000" Rows="3" Width="220px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        username:</td>
                                                                    <td align="left" valign="top" class="whitetext2">
                                                                        <asp:TextBox ID="txtControlname" runat="server" MaxLength="50" Width="220px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                        Password</td>
                                                                    <td align="left" valign="top" class="whitetext2">
                                                                        <asp:TextBox ID="txtControlpassword" runat="server"  MaxLength="50" Width="220px"></asp:TextBox></td>
                                                                </tr>
                                                                <%--===========================================================================--%>
                                                                <tr>
                                                                    <%--<td align="left" valign="top" class="whitetext1">
                                                                    </td>--%>
                                                                    <td align="left" valign="top" class="whitetext1">
                                                                    </td>
                                                                    <td align="left" valign="top">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:HiddenField ID="prevName" runat="server" />
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100px">
                                                    <img height="16" src="../images/loginBox_btm.png" width="420" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top">
                                        <hr />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 424px">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td align="left" valign="middle">
                                                    <div id="divMessage" style="color: Red" runat="server" visible="true">
                                                    </div>
                                                </td>
                                                <td align="right" valign="middle">
                                                    <div align="right" style="float: right;">
                                                        <asp:Button ID="btnCreate" runat="server" Text="Update" OnClick="btnCreate_Click"
                                                            CssClass="blueBtns" />
                                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                                                            ShowSummary="False" />
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 424px">&nbsp;
                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <div>
                </div>
            </div>
        </div>
        <uc1:footer ID="Footer1" runat="server" />
    </form>
</body>
</html>
