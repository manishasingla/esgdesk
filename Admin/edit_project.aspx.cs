using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Admin_edit_project : System.Web.UI.Page
{
    int id;
    string sql = "";
    protected void Page_Load(object sender, EventArgs e)
    {

        //btnCreate.Attributes.Add("Onclick", "return checkProject();");
        chkRelevanturl.Attributes.Add("OnClick", "showrelevanturl();");
        ////Page.RegisterClientScriptBlock("Onload", "showrelevanturl();");

        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = "edit_project.aspx";
            Response.Redirect("login.aspx");
            return;
        }

        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            Response.Redirect("tasks.aspx");
            return;
        }
        try
        {
            if (Request.QueryString["Comp"] == null || Request.QueryString["Comp"].ToString() == "")
            {
                FrmCompLnk.Visible = false;
            }
            else if (Request.QueryString["Comp"].ToString() == "True")
            {
                FrmCompLnk.Visible = true;
            }
        }
        catch { }
        string var = Request.QueryString["id"];
        if (var == null)
        {
            //id = 0;
            Response.Redirect("projects.aspx");
        }
        else
        {
            try
            {
                id = Convert.ToInt32(var);
            }
            catch
            {
                divMessage.InnerHtml = "Project ID must be an integer.";
                DivEntry.Visible = false;
                return;
            }
        }



        if (!IsPostBack)
        {

            // add or edit?
            // TDAddNewProject.Visible = false;
            loadCompany_dropdowns();
            Loadprogect_drp();
            //if (id == 0)
            //{
            //    clearControls();
            //}
            //else
            //{


            GetProjectdata(id);
        }



        //}
        //else
        //{

        //}
    }
    private void insertArchivecache()
    {
        string sqlArchive = "", sqlAll = "";
        DataSet ds_dropdowns, ds_dropdownsAll, ds_forProjectRelated;


        Cache.Remove("Dropdowns_For_NotArchive");
        sqlArchive = "select project_name from projects where archive!='Y' and active = 'Y' order by project_name;";
        ds_dropdowns = DatabaseHelper.getDataset(sqlArchive);
        Cache.Insert("Dropdowns_For_NotArchive", ds_dropdowns, null, DateTime.Now.AddHours(6), TimeSpan.Zero);

        DataSet ds_Alldropdowns;
        Cache.Remove("Dropdowns_For_AllProject");
        sqlAll = "select project_name from projects where  active = 'Y' order by project_name;";
        ds_Alldropdowns = DatabaseHelper.getDataset(sqlAll);
        Cache.Insert("Dropdowns_For_AllProject", ds_Alldropdowns, null, DateTime.Now.AddHours(6), TimeSpan.Zero);


        string SqlallQry = "";
        Cache.Remove("dropdowns");
        SqlallQry = @"select project_name	from projects where archive!='Y' and  active = 'Y' order by project_name;";
        SqlallQry += "\nselect category_name from categories order by sort_seq, category_name;";
        SqlallQry += "\nselect priority_name from priorities order by sort_seq, priority_name;";
        SqlallQry += "\nselect status_name from statuses order by sort_seq, status_name;";
        SqlallQry += "\nselect username from users where active = 'Y' order by username;";
        ds_dropdownsAll = DatabaseHelper.getDataset(SqlallQry);
        Cache.Insert("dropdowns", ds_dropdownsAll, null, DateTime.Now.AddHours(6), TimeSpan.Zero);


        string SqlProjectRequest = "";
        Cache.Remove("dropdownsNew");
        SqlProjectRequest = @"select project_name from projects where archive!='Y' and active = 'Y' order by project_name;";
        SqlProjectRequest += "\nselect category_name from categories order by sort_seq, category_name;";
        SqlProjectRequest += "\nselect priority_name from priorities order by sort_seq, priority_name;";
        SqlProjectRequest += "\nselect status_name from statuses order by sort_seq, status_name;";
        SqlProjectRequest += "\nselect username from users where active = 'Y' order by username;";
        ds_forProjectRelated = DatabaseHelper.getDataset(SqlProjectRequest);
        Cache.Insert("dropdownsNew", ds_forProjectRelated, null, DateTime.Now.AddHours(6), TimeSpan.Zero);


    }
    private void GetProjectdata(int id)
    {
        string strSql = "select * from projects where project_id=" + id;
        DataSet ds = DatabaseHelper.getDataset(strSql);

        if (ds != null)
        {
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                lblId.Text = ds.Tables[0].Rows[0]["project_id"].ToString();
                //drpCompany.SelectedValue = ds.Tables[0].Rows[0]["CompanyName"].ToString();
                drpCompany.SelectedItem.Text = ds.Tables[0].Rows[0]["CompanyName"].ToString();
                Loadprogect_drp();
                // txtName.Text = ds.Tables[0].Rows[0]["project_name"].ToString();
                //drpProjects.SelectedValue = ds.Tables[0].Rows[0]["project_name"].ToString();
                drpProjects.SelectedItem.Text  = ds.Tables[0].Rows[0]["project_name"].ToString();
                prevName.Value = ds.Tables[0].Rows[0]["project_name"].ToString();
                txtDescription.Text = ds.Tables[0].Rows[0]["description"].ToString();
                txtRelevanUrl.Text = ds.Tables[0].Rows[0]["Relevanturl"].ToString();

                txtwebsite.Text = ds.Tables[0].Rows[0]["WebsiteUrl"].ToString();
                txtFTPIP.Text = ds.Tables[0].Rows[0]["HostAddress_IP"].ToString();
                txtFTPUsername.Text = ds.Tables[0].Rows[0]["UserName"].ToString();
                txtFTPPassword.Text = ds.Tables[0].Rows[0]["Password"].ToString();

                //****************************************************************************
                txttestdomainUrl.Text = ds.Tables[0].Rows[0]["TestDomainUrl"].ToString().Replace("''", "'");
                txtTestFTPIP.Text = ds.Tables[0].Rows[0]["TestHostAddress_IP"].ToString().Replace("''", "'");
                txtTestFTPUsername.Text = ds.Tables[0].Rows[0]["TestUserName"].ToString().Replace("''", "'");
                txtTestFTPPassword.Text = ds.Tables[0].Rows[0]["TestPassword"].ToString().Replace("''", "'");
                //==================
                txtclientid.Text = ds.Tables[0].Rows[0]["Client_Id"].ToString().Replace("''", "'");
                txtusername1.Text = ds.Tables[0].Rows[0]["UserName1"].ToString().Replace("''", "'");
                txtPassword1.Text = ds.Tables[0].Rows[0]["Password1"].ToString().Replace("''", "'");
                txtusername2.Text = ds.Tables[0].Rows[0]["UserName2"].ToString().Replace("''", "'");
                txtPassword2.Text = ds.Tables[0].Rows[0]["Password2"].ToString().Replace("''", "'");
                //=================
                chkArchive.Checked = ds.Tables[0].Rows[0]["Archive"].ToString() == "Y" ? true : false;
                //******************************ProjectLogin**********************************************

                txtuserloginUrl.Text = ds.Tables[0].Rows[0]["UserLoginurl"].ToString().Replace("''", "'");
                txtusername.Text = ds.Tables[0].Rows[0]["Userusername"].ToString().Replace("''", "'");
                txtuserpasseord.Text = ds.Tables[0].Rows[0]["UserPassword"].ToString().Replace("''", "'");
                txtadminloginurl.Text = ds.Tables[0].Rows[0]["Adminloginurl"].ToString().Replace("''", "'");
                txtuserAdminname.Text = ds.Tables[0].Rows[0]["Adminusername"].ToString().Replace("''", "'");
                txtAdminpassword.Text = ds.Tables[0].Rows[0]["AdminPassword"].ToString().Replace("''", "'");
                txtcontrolpanelurl.Text = ds.Tables[0].Rows[0]["Controlpanelurl"].ToString().Replace("''", "'");
                txtControlname.Text = ds.Tables[0].Rows[0]["Controlusername"].ToString().Replace("''", "'");
                txtControlpassword.Text = ds.Tables[0].Rows[0]["ControlPassword"].ToString().Replace("''", "'");
                                                              
                //*******************************************************************************************
                chkDefault.Checked = ds.Tables[0].Rows[0]["default_selection"].ToString() == "Y" ? true : false;
                chkActive.Checked = ds.Tables[0].Rows[0]["active"].ToString() == "Y" ? true : false;
                //btnCreate.Text = "Update";
                DivEntry.Visible = true;
                DivEntry.Visible = true;
                divMessage.InnerHtml = "";
                //TDAddNewProject.Visible = true;
                LnkGoogleAnalytics.Visible = true;
                LnkGoogleAnalytics.HRef = "AddGoogleAc.aspx?id=" + lblId.Text + "&Project=True";
            }
            else
            {
                divMessage.InnerHtml = "Project was not found.";
                DivEntry.Visible = false;
            }
        }
        else
        {
            divMessage.InnerHtml = "Project was not found.";
            DivEntry.Visible = false;
        }
    }

    void Loadprogect_drp()
    {
        drpProjects.Items.Clear();
        sql = @"select project_name
		from projects
		where active = 'Y' and CompanyName='" + drpCompany.SelectedItem.Text.ToString() + "' order by project_name;";
        DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);
        if (ds_dropdowns.Tables.Count > 0 && ds_dropdowns.Tables[0].Rows.Count > 0)
        {
            try
            {
                drpProjects.DataSource = ds_dropdowns.Tables[0];
                drpProjects.DataTextField = "project_name";
                drpProjects.DataValueField = "project_name";
                drpProjects.DataBind();
                // drpProjects.Items.Insert(0, new ListItem("[no project]", ""));
                // drpProjects.Items.Insert(1, new ListItem("[Other]", "other"));
            }
            catch { }
            //******************************
            //btnedit.Visible = true;
            //btnadd.Visible = false;
            //******************************
        }
        else
        {
            // drpProjects.Items.Insert(0, new ListItem("[no project]", ""));
            //drpProjects.Items.Insert(1, new ListItem("[Other]", "other"));
            //******************************
            //btnedit.Visible = false ;
            //btnadd.Visible = true ;
            //******************************
        }

    }

    void loadCompany_dropdowns()
    {

        // projects
        ////////////        sql = @"select project_name
        ////////////		from projects
        ////////////		where active = 'Y' order by project_name;";

        sql = @"select distinct Company_Name
		from Company where Company_Name !='' order by Company_Name;";
        /// DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);
        // do a batch of sql statements

        DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);
        if (ds_dropdowns.Tables.Count > 0 && ds_dropdowns.Tables[0].Rows.Count > 0)
        {
            try
            {
                drpCompany.DataSource = ds_dropdowns.Tables[0];
                drpCompany.DataTextField = "Company_Name";
                drpCompany.DataValueField = "Company_Name";
                drpCompany.DataBind();
                //drpCompany.Items.Insert(0, new ListItem("[no company]", ""));
            }

            catch { }
        }
        // drpProjects.Items.Insert(1, new ListItem("[Other]", "other"));


    }

    protected void btnCreate_Click(object sender, EventArgs e)
    {
        if (drpProjects.SelectedValue == "")
        {
            divMessage.InnerHtml = "Please enter project name.";
            return;
        }

        //if (chkRelevanturl.Checked)
        //{
        //    if (txtRelevanUrl.Text == "")
        //    {
        //        divMessage.InnerHtml = "Please enter relevant url.";
        //        return;

        //    }
        //}

        TDEditProjectName.Style.Add(HtmlTextWriterStyle.Display, "none");
        TDAddProjectName.Style.Add(HtmlTextWriterStyle.Display, "none");
        lblAddProjectMsg.Text = "";
        lblProjectMsg.Text = "";

        string strSql = " update projects ";
        strSql += " set [project_name]= '" + drpProjects.SelectedItem.Text.ToString()  + "',";
        strSql += " [description] = '" + txtDescription.Text.Trim().Replace("'", "''") + "',";
        strSql += " [default_selection] = '" + (chkDefault.Checked ? "Y" : "N") + "',";
        strSql += " [active] = '" + (chkActive.Checked ? "Y" : "N") + "',";
        strSql += " [CompanyName] = '" + drpCompany.SelectedItem.Text.ToString().Replace("'", "''") + "',";
        strSql += " [WebsiteUrl] = '" + txtwebsite.Text.Replace("'", "''") + "',";

        strSql += " [HostAddress_IP] = '" + txtFTPIP.Text.Replace("'", "''") + "',";

        strSql += " [UserName] = '" + txtFTPUsername.Text.Replace("'", "''") + "',";

        strSql += " [Password] = '" + txtFTPPassword.Text.Replace("'", "''") + "',";

        //****************************************************************************
        strSql += " [TestDomainUrl] = '" + txttestdomainUrl.Text.Replace("'", "''") + "',";
        strSql += " [TestHostAddress_IP] = '" + txtTestFTPIP.Text.Replace("'", "''") + "',";
        strSql += " [TestUserName] = '" + txtTestFTPUsername.Text.Replace("'", "''") + "',";
        strSql += " [TestPassword] = '" + txtTestFTPPassword.Text.Replace("'", "''") + "',";
        strSql += " [Client_Id] = '" + txtclientid.Text.Replace("'", "''") + "',";
        strSql += " [UserName1] = '" + txtusername1.Text.Replace("'", "''") + "',";
        strSql += " [Password1] = '" + txtPassword1.Text.Replace("'", "''") + "',";
        strSql += " [UserName2] = '" + txtusername2.Text.Replace("'", "''") + "',";
        strSql += " [Password2] = '" + txtPassword2.Text.Replace("'", "''") + "',";
        strSql += " [Archive] = '" + (chkArchive.Checked ? "Y" : "N") + "',";
        //****************************************************************************
        strSql += " [UserLoginurl]='"+ txtuserloginUrl.Text.Replace ("'","''")+"',";
        strSql += " [Userusername]='" + txtusername.Text.Replace("'", "''") + "',";
        strSql += " [UserPassword]='" + txtuserpasseord.Text.Replace("'", "''") + "',";
        strSql += " [Adminloginurl]='" + txtadminloginurl.Text.Replace("'", "''") + "',";
        strSql += " [Adminusername]='" + txtuserAdminname.Text.Replace("'", "''") + "',";
        strSql += " [AdminPassword]='" + txtAdminpassword.Text.Replace("'", "''") + "',";
        strSql += " [Controlpanelurl]='" + txtcontrolpanelurl.Text.Replace("'", "''") + "',";
        strSql += " [Controlusername]='" + txtControlname.Text.Replace("'", "''") + "',";
        strSql += " [ControlPassword]='" + txtControlpassword.Text.Replace("'", "''") + "',";
                                                                      
        //***************************************************************************
        strSql += " [Relevanturl] = '" + txtRelevanUrl.Text.Replace("'", "''") + "'";

        strSql += " where project_id = " + lblId.Text.Trim();

        int intResult = DatabaseHelper.executeNonQuery(strSql);

        if (intResult != 0)
        {
            //strSql = "update tasks set project = '" + drpProjects.SelectedValue + "' ";
            //strSql += " where project ='" + prevName.Value.Replace("'", "''") + "'";
            //intResult = DatabaseHelper.executeNonQuery(strSql);
            //strSql = "update ClientRequest set ProjectName = '" + drpProjects.SelectedValue + "' ";
            //strSql += " where ProjectName ='" + prevName.Value.Replace("'", "''") + "'";
            //intResult = DatabaseHelper.executeNonQuery(strSql);

            divMessage.InnerHtml = "Project was updated.";
        }
        else
        {
            divMessage.InnerHtml = "Project was not updated.";
        }
        insertArchivecache();
    }

    //private void clearControls()
    //{
    //    lblId.Text = "New";
    //    LnkGoogleAnalytics.Visible = false;

    //    chkActive.Checked = true;
    //    txtDescription.Text = "";
    //    //btnCreate.Text = "Create";
    //}
    protected void drpCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtwebsite.Text ="";
        txtRelevanUrl.Text ="";
        txtFTPIP.Text ="";
        txtFTPUsername.Text ="";
        txtFTPPassword.Text ="";
        txttestdomainUrl.Text ="";
        txtTestFTPIP.Text ="";
        txtTestFTPUsername.Text ="";
        txtTestFTPPassword.Text ="";
        txtDescription.Text = "";
        divMessage.InnerHtml = "";


        Loadprogect_drp();
        setDefaultproject();
    }
    void setDefaultproject()
    {
        string strsql = "";
        if (drpProjects.SelectedItem != null)
        {
            strsql = @"select distinct project_id from projects where project_name='" + drpProjects.SelectedItem.Text + "'";

            DataSet default_project_id = DatabaseHelper.getDataset(strsql);
            if (default_project_id.Tables.Count > 0 && default_project_id.Tables[0].Rows.Count > 0)
            {
                id = Convert.ToInt32(default_project_id.Tables[0].Rows[0]["project_id"].ToString());
                lblId.Text = default_project_id.Tables[0].Rows[0]["project_id"].ToString();
                GetDefaultProjectdata(id);
            }
            btnedit.Visible = true;
            btnadd.Visible = true;
        }
        else
        {
            btnedit.Visible = false;
            btnadd.Visible = true;
        }
        TDEditProjectName.Style.Add(HtmlTextWriterStyle.Display, "none");
        TDAddProjectName.Style.Add(HtmlTextWriterStyle.Display, "none");
    }
    private void GetDefaultProjectdata(int id)
    {
        string strSql = "select * from projects where project_id=" + id;
        DataSet ds = DatabaseHelper.getDataset(strSql);

        if (ds != null)
        {
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                lblId.Text = ds.Tables[0].Rows[0]["project_id"].ToString();
                if (ds.Tables[0].Rows[0]["CompanyName"].ToString() != "")
                {
                    drpCompany.SelectedItem.Text = ds.Tables[0].Rows[0]["CompanyName"].ToString();
                }
                drpProjects.SelectedItem.Text = ds.Tables[0].Rows[0]["project_name"].ToString();
                prevName.Value = ds.Tables[0].Rows[0]["project_name"].ToString();
                txtDescription.Text = ds.Tables[0].Rows[0]["description"].ToString();
                txtRelevanUrl.Text = ds.Tables[0].Rows[0]["Relevanturl"].ToString();

                txtwebsite.Text = ds.Tables[0].Rows[0]["WebsiteUrl"].ToString();
                txtFTPIP.Text = ds.Tables[0].Rows[0]["HostAddress_IP"].ToString();
                txtFTPUsername.Text = ds.Tables[0].Rows[0]["UserName"].ToString();
                txtFTPPassword.Text = ds.Tables[0].Rows[0]["Password"].ToString();

                //****************************************************************************
                txttestdomainUrl.Text = ds.Tables[0].Rows[0]["TestDomainUrl"].ToString().Replace("''", "'");
                txtTestFTPIP.Text = ds.Tables[0].Rows[0]["TestHostAddress_IP"].ToString().Replace("''", "'");
                txtTestFTPUsername.Text = ds.Tables[0].Rows[0]["TestUserName"].ToString().Replace("''", "'");
                txtTestFTPPassword.Text = ds.Tables[0].Rows[0]["TestPassword"].ToString().Replace("''", "'");

                txtclientid.Text = ds.Tables[0].Rows[0]["Client_Id"].ToString().Replace("''", "'");
                txtusername1.Text = ds.Tables[0].Rows[0]["UserName1"].ToString().Replace("''", "'");
                txtPassword1.Text = ds.Tables[0].Rows[0]["Password1"].ToString().Replace("''", "'");
                txtusername2.Text = ds.Tables[0].Rows[0]["UserName2"].ToString().Replace("''", "'");
                txtPassword2.Text = ds.Tables[0].Rows[0]["Password2"].ToString().Replace("''", "'");
                //*************************************************************************************


                txtuserloginUrl.Text = ds.Tables[0].Rows[0]["UserLoginurl"].ToString().Replace("''", "'");
                txtusername.Text = ds.Tables[0].Rows[0]["Userusername"].ToString().Replace("''", "'");
                txtuserpasseord.Text = ds.Tables[0].Rows[0]["UserPassword"].ToString().Replace("''", "'");
                txtadminloginurl.Text = ds.Tables[0].Rows[0]["Adminloginurl"].ToString().Replace("''", "'");
                txtuserAdminname.Text = ds.Tables[0].Rows[0]["Adminusername"].ToString().Replace("''", "'");
                txtAdminpassword.Text = ds.Tables[0].Rows[0]["AdminPassword"].ToString().Replace("''", "'");
                txtcontrolpanelurl.Text = ds.Tables[0].Rows[0]["Controlpanelurl"].ToString().Replace("''", "'");
                txtControlname.Text = ds.Tables[0].Rows[0]["Controlusername"].ToString().Replace("''", "'");
                txtControlpassword.Text = ds.Tables[0].Rows[0]["ControlPassword"].ToString().Replace("''", "'");
              
                
                //*************************************************************************************
                chkDefault.Checked = ds.Tables[0].Rows[0]["default_selection"].ToString() == "Y" ? true : false;
                chkActive.Checked = ds.Tables[0].Rows[0]["active"].ToString() == "Y" ? true : false;
                //btnCreate.Text = "Update";
                DivEntry.Visible = true;
                DivEntry.Visible = true;
                divMessage.InnerHtml = "";
                //TDAddNewProject.Visible = true;
                LnkGoogleAnalytics.Visible = true;
                LnkGoogleAnalytics.HRef = "AddGoogleAc.aspx?id=" + lblId.Text + "&Project=True";
            }
            else
            {
                divMessage.InnerHtml = "Project was not found.";
                DivEntry.Visible = false;
            }
        }
        else
        {
            divMessage.InnerHtml = "Project was not found.";
            DivEntry.Visible = false;
        }
    }



    protected void drpProjects_SelectedIndexChanged(object sender, EventArgs e)
    {
        TDAddProjectName.Style.Add(HtmlTextWriterStyle.Display, "none");
        TDEditProjectName.Style.Add(HtmlTextWriterStyle.Display, "none");
        sql = @"select distinct project_id from projects where project_name='" + drpProjects.SelectedValue + "'";

        DataSet ds_project_id = DatabaseHelper.getDataset(sql);
        if (ds_project_id.Tables.Count > 0 && ds_project_id.Tables[0].Rows.Count > 0)
        {
            id = Convert.ToInt32(ds_project_id.Tables[0].Rows[0]["project_id"].ToString());
            GetProjectdata(id);
        }
    }
    protected void btnUpdateProject_Click(object sender, EventArgs e)
    {
        //Response.Write("hi");
        TDEditProjectName.Style.Add(HtmlTextWriterStyle.Display, "block");
        TDAddProjectName.Style.Add(HtmlTextWriterStyle.Display, "none");

        object objResult = DatabaseHelper.executeScalar("select count(*) from projects where active = 'Y' and CompanyName='" + drpCompany.SelectedItem.Text + "' and project_name='" + txtProjectName.Text.Trim().Replace("'", "''") + "'");

        if (objResult.ToString() != "0")
        {
            lblProjectMsg.Text = "Project name already exist.";
            return;
        }


        string strSql = " update projects ";
        strSql += " set [project_name]= '" + txtProjectName.Text + "'";

        strSql += " where project_id = " + lblId.Text.Trim();

        int intResult = DatabaseHelper.executeNonQuery(strSql);

        if (intResult != 0)
        {
            lblProjectMsg.Text = "Project name was updated.";
        }
        else
        {
            lblProjectMsg.Text = "Project name was not updated.";
        }
        GetProjectdata(Convert.ToInt32(lblId.Text));
    }
    protected void btnAddProject_Click(object sender, EventArgs e)
    {
        TDAddProjectName.Style.Add(HtmlTextWriterStyle.Display, "block");
        TDEditProjectName.Style.Add(HtmlTextWriterStyle.Display, "none");

        object objResult = DatabaseHelper.executeScalar("select count(*) from projects where active = 'Y' and CompanyName='" + drpCompany.SelectedItem.Text + "' and project_name='" + txtAddProjectName.Text.Trim().Replace("'", "''") + "'");

        if (objResult.ToString() != "0")
        {
            lblAddProjectMsg.Text = "Project name already exist.";
            return;
        }
        string strAddSql = "insert into projects (CompanyName,project_name,active,default_selection,Archive)Values('" + drpCompany.SelectedItem.Text + "','" + txtAddProjectName.Text + "','Y','N','N');select SCOPE_IDENTITY()";
        int id = Convert.ToInt32(DatabaseHelper.checkProjectId(strAddSql));
        lblId.Text = id.ToString();
        if (id != 0)
        {
            lblAddProjectMsg.Text = "Project name was inserted.";
            btnedit.Visible = true;
            btnadd.Visible = true;
        }
        else
        {
            lblAddProjectMsg.Text = "Project name was not inserted.";
        }
        GetProjectdata(Convert.ToInt32(lblId.Text));
        insertArchivecache();
    }
   
}
