<%@ Page Language="C#" AutoEventWireup="true" CodeFile="edit_project1.aspx.cs" Inherits="Admin_edit_project" %>

<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        <%= ConfigurationManager.AppSettings["CompanyName"].ToString()%></title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        function checkProject() {
            var project = document.getElementById("txtName").value;
            var drpprjct = document.getElementById("drpProjects").value;
            var rlevanUrl = document.getElementById("txtRelevanUrl").value;
            if (drpprjct == "other") {

                if (project == "") {
                    alert("Please enter project name");
                    return false;
                }

            }

            /*else if(document.getElementById("chkRelevanturl").checked)
  
            {
            alert("start");
            if(rlevanUrl == "")
            {
            alert("start2");
            alert("Please enter relevant url");
            return false;
            }
    
            }*/
            else {
                var btnsend = document.getElementById("btnAddProject");
                btnsend.click();
            }

        }
    
    </script>
    <script language="javascript" type="text/javascript">
        function showrelevanturl() {


            if (document.getElementById("chkRelevanturl").checked) {

                document.getElementById("TrRelevantUrl").style.display = "Block";
            }


            else {
                document.getElementById("TrRelevantUrl").style.display = "none";
                // return false;
            }

        }

        function editprojectname() {
            document.getElementById("TDEditProjectName").style.display = "Block";
            var txt = document.getElementById("txtProjectName");
            var e = document.getElementById("drpProjects");
            var strproj = e.options[e.selectedIndex].value;
            txt.value = strproj;

        }
        function closeeditprojectname() {
            document.getElementById("TDEditProjectName").style.display = "none";
        }
        function updateeditprojectname() {
            document.getElementById("<%= btnUpdateProject.ClientID %>").click();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <uc2:Admin_Header ID="Admin_Header1" runat="server" />
        <div id="Content">
            <div style="width: 100%; background: #ebebeb; padding: 5px 0;">
                &nbsp;&nbsp;<a href="edit_project.aspx">Add new project</a> &nbsp;|&nbsp; <a href="projects.aspx">
                    Back to projects</a> <a href="Company.aspx" id="FrmCompLnk" runat="server">&nbsp;|&nbsp;Back
                        to companies</a>&nbsp;
            </div>
            <table>
                <tr>
                    <td align="left" valign="top">
                        <table style="width: 424px">
                            <tr>
                                <td align="left" class="titleText" valign="top" style="width: 424px; display: none;">
                                    Projects
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 424px">
                                    <table width="424" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="left" valign="top">
                                                <img height="16" src="../images/loginBox_top.png" width="420" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="background: url(../images/loginBox_mid.png) repeat-y left top;">
                                                <div style="text-align: center">
                                                    <div id="DivEntry" runat="server">
                                                        <table cellpadding="5">
                                                            <tr>
                                                                <td align="left" valign="top">
                                                                </td>
                                                                <td width="130" align="left" valign="top" class="whitetext1">
                                                                    Project ID:
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext2">
                                                                    <asp:Label ID="lblId" runat="server">New</asp:Label>
                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                    <a href="" id="LnkGoogleAnalytics" runat="server">Add google analytics</a>&nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" valign="top" class="whitetext1">
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext1">
                                                                    Company name:
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext2">
                                                                    <asp:DropDownList ID="drpCompany" runat="server" CssClass="filerDrpodown" AutoPostBack="True"
                                                                        Width="222px" OnSelectedIndexChanged="drpCompany_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="drpCompany"
                                                                        ErrorMessage="Please select company name." InitialValue=" ">*</asp:RequiredFieldValidator>
                                                                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" TargetControlID="RequiredFieldValidator2">
                                                                    </cc1:ValidatorCalloutExtender>--%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" valign="top" class="whitetext1">
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext1">
                                                                    Project name:
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext2">
                                                                    <asp:DropDownList ID="drpProjects" runat="server" CssClass="filerDrpodown" AutoPostBack="true"
                                                                        Width="222px" OnSelectedIndexChanged="drpProjects_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                    <a id="btnedit" runat="server" href="#" onclick="editprojectname();">Edit</a>
                                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="drpProjects"
                                                                        ErrorMessage="Please select project name." InitialValue=" ">*</asp:RequiredFieldValidator>
                                                                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator1">
                                                                    </cc1:ValidatorCalloutExtender>--%>
                                                                    <table id="TDEditProjectName" runat="server" style="border: solid 1px #999999; padding-top: 10px;
                                                                        display: none">
                                                                        <tr style="padding-top: 7px">
                                                                            <td>
                                                                                <asp:TextBox ID="txtProjectName" runat="server" Width="290px" MaxLength="200"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="right">
                                                                                <asp:Button ID="btnUpdateProject" runat="server" Text="Add" CausesValidation="false"
                                                                                    OnClick="btnUpdateProject_Click" Style="display: none" />
                                                                                <a id="btnUpdateProj" runat="server" href="#" onclick="updateeditprojectname();">Update</a>
                                                                                <a id="btnCloseProject" runat="server" href="#" onclick="closeeditprojectname();">Cancel</a>
                                                                                <br />
                                                                                <span style="padding-right: 100px">
                                                                                    <asp:Label ID="lblProjectMsg" runat="server" ForeColor="Red" Text=""></asp:Label>
                                                                                </span>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" valign="top" class="whitetext1">
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext1">
                                                                    Website url:
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext2">
                                                                    <asp:TextBox ID="txtwebsite" runat="server" MaxLength="50" Width="220px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" valign="top" class="whitetext1">
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext1">
                                                                    <span id="DataGrid1_ctl02_lblComment">Relevant url:</span>
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext2">
                                                                    <asp:CheckBox ID="chkRelevanturl" Checked="true" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr id="TrRelevantUrl">
                                                                <td align="left" valign="top" class="whitetext1">
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext1">
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext2">
                                                                    <asp:TextBox ID="txtRelevanUrl" runat="server" MaxLength="1000" Rows="7" TextMode="MultiLine"
                                                                        Width="220px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" valign="top" class="whitetext1">
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext1">
                                                                    Host address/IP:
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext2">
                                                                    <asp:TextBox ID="txtFTPIP" runat="server" MaxLength="50" Width="220px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" valign="top" class="whitetext1">
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext1">
                                                                    User name:
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext2">
                                                                    <asp:TextBox ID="txtFTPUsername" runat="server" MaxLength="50" Width="220px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" valign="top" class="whitetext1">
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext1">
                                                                    Password:
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext2">
                                                                    <asp:TextBox ID="txtFTPPassword" runat="server" MaxLength="50" Width="220px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" valign="top" class="whitetext1">
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext1">
                                                                    Active:
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext2">
                                                                    <asp:CheckBox ID="chkActive" runat="server" Checked="True" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" valign="top" class="whitetext1">
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext1">
                                                                    Default selection:
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext2">
                                                                    <asp:CheckBox ID="chkDefault" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" valign="top" class="whitetext1">
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext1">
                                                                    Description:
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext2">
                                                                    <asp:TextBox ID="txtDescription" runat="server" MaxLength="1000" Rows="3" TextMode="MultiLine"
                                                                        Width="220px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" valign="top" class="whitetext1">
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext1">
                                                                </td>
                                                                <td align="left" valign="top">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <asp:HiddenField ID="prevName" runat="server" />
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 100px">
                                                <img height="16" src="../images/loginBox_btm.png" width="420" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <hr />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 424px">
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="left" valign="middle">
                                                <div id="divMessage" style="color: Red" runat="server" visible="true">
                                                </div>
                                            </td>
                                            <td align="right" valign="middle">
                                                <div align="right" style="float: right;">
                                                    <asp:Button ID="btnCreate" runat="server" Text="Update" OnClick="btnCreate_Click"
                                                        CssClass="blueBtns" /></div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 424px">
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <div>
            </div>
        </div>
    </div>
    <uc1:footer ID="Footer1" runat="server" />
    </form>
</body>
</html>
