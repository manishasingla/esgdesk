using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Admin_edit_project : System.Web.UI.Page
{
    int id;
    string sql = "";
    protected void Page_Load(object sender, EventArgs e)
    {

        //btnCreate.Attributes.Add("Onclick", "return checkProject();");
        chkRelevanturl.Attributes.Add("OnClick", "showrelevanturl();");
        ////Page.RegisterClientScriptBlock("Onload", "showrelevanturl();");

        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = "edit_project.aspx";
            Response.Redirect("login.aspx");
            return;
        }

        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            Response.Redirect("tasks.aspx");
            return;
        }
        try
        {
            if (Request.QueryString["Comp"] == null || Request.QueryString["Comp"].ToString() == "")
            {
                FrmCompLnk.Visible = false;
            }
            else if (Request.QueryString["Comp"].ToString() == "True")
            {
                FrmCompLnk.Visible = true;
            }
        }
        catch { }
        string var = Request.QueryString["id"];
        if (var == null)
        {
            //id = 0;
            Response.Redirect("projects.aspx");
        }
        else
        {
            try
            {
                id = Convert.ToInt32(var);
            }
            catch
            {
                divMessage.InnerHtml = "Project ID must be an integer.";
                DivEntry.Visible = false;
                return;
            }
        }



        if (!IsPostBack)
        {

            // add or edit?
            // TDAddNewProject.Visible = false;
            loadCompany_dropdowns();
            Loadprogect_drp();
            //if (id == 0)
            //{
            //    clearControls();
            //}
            //else
            //{


            GetProjectdata(id);
        }



        //}
        //else
        //{

        //}
    }

    private void GetProjectdata(int id)
    {
        string strSql = "select * from projects where project_id=" + id;
        DataSet ds = DatabaseHelper.getDataset(strSql);

        if (ds != null)
        {
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                lblId.Text = ds.Tables[0].Rows[0]["project_id"].ToString();
                drpCompany.SelectedValue = ds.Tables[0].Rows[0]["CompanyName"].ToString();
                Loadprogect_drp();
                // txtName.Text = ds.Tables[0].Rows[0]["project_name"].ToString();
                drpProjects.SelectedValue = ds.Tables[0].Rows[0]["project_name"].ToString();
                prevName.Value = ds.Tables[0].Rows[0]["project_name"].ToString();
                txtDescription.Text = ds.Tables[0].Rows[0]["description"].ToString();
                txtRelevanUrl.Text = ds.Tables[0].Rows[0]["Relevanturl"].ToString();

                txtwebsite.Text = ds.Tables[0].Rows[0]["WebsiteUrl"].ToString();
                txtFTPIP.Text = ds.Tables[0].Rows[0]["HostAddress_IP"].ToString();
                txtFTPUsername.Text = ds.Tables[0].Rows[0]["UserName"].ToString();
                txtFTPPassword.Text = ds.Tables[0].Rows[0]["Password"].ToString();

                chkDefault.Checked = ds.Tables[0].Rows[0]["default_selection"].ToString() == "Y" ? true : false;
                chkActive.Checked = ds.Tables[0].Rows[0]["active"].ToString() == "Y" ? true : false;
                //btnCreate.Text = "Update";
                DivEntry.Visible = true;
                DivEntry.Visible = true;
                divMessage.InnerHtml = "";
                //TDAddNewProject.Visible = true;
                LnkGoogleAnalytics.Visible = true;
                LnkGoogleAnalytics.HRef = "AddGoogleAc.aspx?id=" + lblId.Text + "&Project=True";
            }
            else
            {
                divMessage.InnerHtml = "Project was not found.";
                DivEntry.Visible = false;
            }
        }
        else
        {
            divMessage.InnerHtml = "Project was not found.";
            DivEntry.Visible = false;
        }
    }

    void Loadprogect_drp()
    {
        drpProjects.Items.Clear();
        sql = @"select project_name
		from projects
		where active = 'Y' and CompanyName='" + drpCompany.SelectedValue.ToString() + "' order by project_name;";
        DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);
        if (ds_dropdowns.Tables.Count > 0 && ds_dropdowns.Tables[0].Rows.Count > 0)
        {
            try
            {
                drpProjects.DataSource = ds_dropdowns.Tables[0];
                drpProjects.DataTextField = "project_name";
                drpProjects.DataValueField = "project_name";
                drpProjects.DataBind();
                // drpProjects.Items.Insert(0, new ListItem("[no project]", ""));
                // drpProjects.Items.Insert(1, new ListItem("[Other]", "other"));
            }
            catch { }
        }
        else
        {
            // drpProjects.Items.Insert(0, new ListItem("[no project]", ""));
            //drpProjects.Items.Insert(1, new ListItem("[Other]", "other"));
        }

    }

    void loadCompany_dropdowns()
    {

        // projects
        ////////////        sql = @"select project_name
        ////////////		from projects
        ////////////		where active = 'Y' order by project_name;";

        sql = @"select distinct Company_Name
		from Company where Company_Name !='' order by Company_Name;";
        /// DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);
        // do a batch of sql statements

        DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);
        if (ds_dropdowns.Tables.Count > 0 && ds_dropdowns.Tables[0].Rows.Count > 0)
        {
            try
            {
                drpCompany.DataSource = ds_dropdowns.Tables[0];
                drpCompany.DataTextField = "Company_Name";
                drpCompany.DataValueField = "Company_Name";
                drpCompany.DataBind();
                //drpCompany.Items.Insert(0, new ListItem("[no company]", ""));
            }

            catch { }
        }
        // drpProjects.Items.Insert(1, new ListItem("[Other]", "other"));


    }

    protected void btnCreate_Click(object sender, EventArgs e)
    {
        if (drpProjects.SelectedValue == "")
        {
            divMessage.InnerHtml = "Please enter project name.";
            return;
        }

        if (chkRelevanturl.Checked)
        {
            if (txtRelevanUrl.Text == "")
            {
                divMessage.InnerHtml = "Please enter relevant url.";
                return;

            }

        } TDEditProjectName.Style.Add(HtmlTextWriterStyle.Display, "none");


        string strSql = " update projects ";
        strSql += " set [project_name]= '" + drpProjects.SelectedValue + "',";
        strSql += " [description] = '" + txtDescription.Text.Trim().Replace("'", "''") + "',";
        strSql += " [default_selection] = '" + (chkDefault.Checked ? "Y" : "N") + "',";
        strSql += " [active] = '" + (chkActive.Checked ? "Y" : "N") + "',";
        strSql += " [CompanyName] = '" + drpCompany.SelectedValue.ToString().Replace("'", "''") + "',";
        strSql += " [WebsiteUrl] = '" + txtwebsite.Text.Replace("'", "''") + "',";

        strSql += " [HostAddress_IP] = '" + txtFTPIP.Text.Replace("'", "''") + "',";

        strSql += " [UserName] = '" + txtFTPUsername.Text.Replace("'", "''") + "',";

        strSql += " [Password] = '" + txtFTPPassword.Text.Replace("'", "''") + "',";

        strSql += " [Relevanturl] = '" + txtRelevanUrl.Text.Replace("'", "''") + "'";

        strSql += " where project_id = " + lblId.Text.Trim();

        int intResult = DatabaseHelper.executeNonQuery(strSql);

        if (intResult != 0)
        {
            //strSql = "update tasks set project = '" + drpProjects.SelectedValue + "' ";
            //strSql += " where project ='" + prevName.Value.Replace("'", "''") + "'";
            //intResult = DatabaseHelper.executeNonQuery(strSql);
            //strSql = "update ClientRequest set ProjectName = '" + drpProjects.SelectedValue + "' ";
            //strSql += " where ProjectName ='" + prevName.Value.Replace("'", "''") + "'";
            //intResult = DatabaseHelper.executeNonQuery(strSql);

            divMessage.InnerHtml = "Project was updated.";
        }
        else
        {
            divMessage.InnerHtml = "Project was not updated.";
        }

    }

    //private void clearControls()
    //{
    //    lblId.Text = "New";
    //    LnkGoogleAnalytics.Visible = false;

    //    chkActive.Checked = true;
    //    txtDescription.Text = "";
    //    //btnCreate.Text = "Create";
    //}
    protected void drpCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        Loadprogect_drp();

    }

    protected void drpProjects_SelectedIndexChanged(object sender, EventArgs e)
    {
        sql = @"select distinct project_id from projects where project_name='" + drpProjects.SelectedValue + "'";

        DataSet ds_project_id = DatabaseHelper.getDataset(sql);
        if (ds_project_id.Tables.Count > 0 && ds_project_id.Tables[0].Rows.Count > 0)
        {
            id = Convert.ToInt32(ds_project_id.Tables[0].Rows[0]["project_id"].ToString());
            GetProjectdata(id);
        }
    }
    protected void btnUpdateProject_Click(object sender, EventArgs e)
    {
        //Response.Write("hi");
        TDEditProjectName.Style.Add(HtmlTextWriterStyle.Display, "block");
        string strSql = " update projects ";
        strSql += " set [project_name]= '" + txtProjectName.Text + "'";

        strSql += " where project_id = " + lblId.Text.Trim();

        int intResult = DatabaseHelper.executeNonQuery(strSql);

        if (intResult != 0)
        {
            lblProjectMsg.Text = "Project name was updated.";
        }
        else
        {
            lblProjectMsg.Text = "Project name was not updated.";
        }
        GetProjectdata(Convert.ToInt32(lblId.Text));
    }
}
