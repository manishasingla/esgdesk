using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.Drawing.Drawing2D;
using System.Net;
using System.Net.NetworkInformation;

public partial class Admin_edit_request : System.Web.UI.Page
{
    private static int RequestId;
    string sql = "";
    int id;
    string task_action = "";
    string emailSubject = "";
    bool deletedFlag = false;
   
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = (Request.QueryString["reqid"] == null || Request.QueryString["reqid"].ToString() == "" ? "client_requests.aspx" : "edit_request.aspx?reqid=" + Request.QueryString["reqid"].ToString());
            if (ftbComment.Content.Trim() != "")
            {
                Session["comment"] = ftbComment.Content.ToString();
            }
                Response.Redirect("login.aspx");
            return;
        }
        
        //  Approve button will enable show only 'M'
        if (DatabaseHelper.CanApprove(Session["admin"].ToString()))
        {
            btnapprove.Enabled = true;
            btnUpdate.Enabled = false;
            btnUpdate.Visible = false;
            btnUpdate.CssClass = btnUpdate.CssClass.Replace("blueBtns", "");
            btnUpdate.CssClass = "BtnsNotHover";
        }
        else
        {
            btnapprove.Enabled = false;
            btnapprove.Visible = false;
            btnUpdate.Enabled = true;
            btnUpdate.Visible = true;
            btnapprove.CssClass = btnapprove.CssClass.Replace("blueBtns", "");
            btnapprove.CssClass = "BtnsNotHover";
        }
        //*******************************************************************
        //*******************************************************************
        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            string sqlchkuser1 = "select * from dbo.NonesCRMusers  where ShowRequest like '%" + Session["admin"] + "%'";
            DataSet dsuser1 = DatabaseHelper.getDataset(sqlchkuser1);
            if (dsuser1 != null && dsuser1.Tables.Count > 0 && dsuser1.Tables[0].Rows.Count > 0)
            { }
            else
            {
                Response.Redirect("tasks.aspx");
                return;

            }
        }
        //if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
        //{
        //    Response.Redirect("tasks.aspx");
        //    return;
        //}
        //=====================================================================
        divMessage.InnerHtml = "";
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["reqid"] != null && Request.QueryString["reqid"] != "0" && Request.QueryString["reqid"] != "")
            {
               BtnDelAllCmnt.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete selected comments?')");
                if (Session["Canned_msg"] == null || Session["Canned_msg"].ToString() == "")
                {
                    ftbComment.Content = "";
                    
                }
                else
                {
                    if (Session["CannedShortDesc_msg"] == null || Session["CannedShortDesc_msg"].ToString() == "")
                    {
                        txtShortDescr.Text = "";
                    }
                    else
                    {
                        txtShortDescr.Text = Session["CannedShortDesc_msg"].ToString();
                    }
                   
                    d_h_s.Style["display"] = "block";
                    ftbComment.Content = "Note to client: " + Session["Canned_msg"].ToString();
                    Session["Canned_msg"] = null;
                 }

                load_Compdropdowns();
                load_Userdropdowns();
               
                load_statuses();
                try
                {
                    RequestId = Convert.ToInt16(Request.QueryString["reqid"].ToString());
                    lblRequestId.Text = RequestId.ToString();
                    lnkCreateTask.HRef = "edit_task.aspx?reqid=" + lblRequestId.Text;
                    lnkSendAttachment.HRef = "upload_attachment.aspx?reqid=" + RequestId.ToString();
                    lnkViewAttachment.HRef = "view_attachments.aspx?reqid=" + RequestId.ToString();
                    DataSet dsTotalAttachments = DatabaseHelper.getAttachments(RequestId, "");
                    lnkViewAttachment.InnerHtml = "View attachments (" + dsTotalAttachments.Tables[0].Rows.Count + ")";
                    BindData();
                    //================================
                    BindusercheckBox();
                    //================================
                }
                catch (Exception ex)
                {
                    DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error:In ESGDESK when status is changed by Admin in CR " + "DateTime : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:" + ex.Message + "\n" + " Source:" + ex.Source + "\n" + " InnerException:" + ex.InnerException + " Stack Trace: " + ex.StackTrace + "\n");
                    divMessage.InnerHtml = "Request Id must be Integer.<br><br><a href='client_requests.aspx'>View requests</a>";
                    divRequest.Visible = false;
                    divRequestDetails.Visible = false;
                }

                if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
                {
                    CompanyNotes.Visible = false;
                    CRNotes.Visible = false;
                 }
                else
                {
                    CompanyNotes.Visible = true;
                    CRNotes.Visible = true;
                 }

               if (Session["message"] != null)
               {
                    divMessage1.InnerHtml = Session["message"].ToString();
                    divMessage1.Visible = true;
                    Session.Remove("message");
               }
                if (Session["comment"] != null)
                {
                    ftbComment.Content = Session["comment"].ToString();
                    Session.Remove("comment");
                }

                sql = "Select * from ClientRequest where RequestId=" + RequestId.ToString();
                DataSet dsRequests = DatabaseHelper.getDataset(sql);
                if (dsRequests != null && dsRequests.Tables.Count > 0 && dsRequests.Tables[0].Rows.Count > 0)
                {
                    string projectname = dsRequests.Tables[0].Rows[0]["ProjectName"].ToString();
                    if (projectname != null && projectname != "")
                    {
                        this.Title = "Edit request - " + projectname;
                    }

                }
                else
                {
                    this.Title = "Edit request";
                }
            }

        }
        else
        {
            if (Session["message"] != null)
            {
                divMessage1.InnerHtml = Session["message"].ToString();
                divMessage1.Visible = true;
                Session.Remove("message");
            }

            if (Session["comment"] != null)
            {
               ftbComment.Content = Session["comment"].ToString();
               Session.Remove("comment");
            }


        }
    }
    protected int getHoursTakenSoFar(string task_id)
    {
        sql = @"select sum(datediff(minute,started_date,finished_date))
                from hours_reporting
                where finished_date is not null and task_id=" + task_id;

        int hrsTaken = 0;
        object objHrsTaken = DatabaseHelper.executeScalar(sql);
        if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
        {
            hrsTaken += (int)objHrsTaken;
        }
        else
        {
            hrsTaken = 0;
        }

        sql = @"select sum(datediff(minute,started_date,getdate()))
                from hours_reporting
                where finished_date is null and task_id=" + task_id;

        objHrsTaken = DatabaseHelper.executeScalar(sql);

        if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
        {
            hrsTaken += (int)objHrsTaken;
        }

        return hrsTaken;
    }

    protected void BtnOverdue_Click(object sender, EventArgs e)
    {

        Session["filter"] = " ";
        Session["filterunread"] = "";
        Session["OverdueTask"] = "True";
        Response.Redirect("tasks.aspx");
    }

    protected void Lnk1cTasks_Click(object sender, EventArgs e)
    {
        Session["filter"] = "1c - normal";
        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        Response.Redirect("tasks.aspx");
    }
    protected void LnkNotification_Click(object sender, EventArgs e)
    {
        Response.Redirect("Notification.aspx", false);
    }

    protected void LnkCRWocr_Click(object sender, EventArgs e)
    {   
        Session["filter"] = "CRwocr";
        Response.Redirect("client_requests.aspx", false);
    }
    void load_Compdropdowns()
    {
        sql = @"select distinct Company_Name
		from Company
		where active = 'Y' order by Company_Name;";
        DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);
        try
        {
            drpCompanyName.DataSource = ds_dropdowns.Tables[0];
            drpCompanyName.DataTextField = "Company_Name";
            drpCompanyName.DataValueField = "Company_Name";
            drpCompanyName.DataBind();
            drpCompanyName.Items.Insert(0, new ListItem("[No company]", ""));
        }
        catch (Exception ex)
        { }
    }
    protected void LnkRelated_Click(object sender, EventArgs e)
    {
        Session["ProjectRelated"] = drpProjects.SelectedValue;
        Response.Redirect("Related.aspx", false);

    }

    protected void Lnkcomprelated_Click(object sender, EventArgs e)
    {
        Session["CompanyRelated"] = drpCompanyName.SelectedValue.ToString();
        Response.Redirect("CompanyRelated.aspx", false);
    }
    protected void LnkNewCR_Click(object sender, EventArgs e)
    {
        Session["filter"] = "NewCR";
        Response.Redirect("client_requests.aspx", false);
    }
    protected void LnkCrNewComment_Click(object sender, EventArgs e)
    {
        Session["filter"] = "CRNewComment";
        Response.Redirect("client_requests.aspx", false);

    }
    
    void load_Userdropdowns()
    {
        DrpUserName.Items.Clear();
        if (drpCompanyName.SelectedValue.ToString() != "")
        {
            sql = @"select *
		from NonesCRMusers where CompanyName = '" + drpCompanyName.SelectedValue + "' order by UserName;";
        }
        else
        {
            sql = @"select *
		from NonesCRMusers order by UserName;";
        }
        DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);
        if (ds_dropdowns.Tables[0].Rows.Count > 0)
        {
            DrpUserName.DataSource = null;
            DrpUserName.DataBind();

            DrpUserName.DataSource = ds_dropdowns.Tables[0];
            DrpUserName.DataTextField = "UserName";
            DrpUserName.DataValueField = "UserName";

            DrpUserName.DataBind();
            DrpUserName.Items.Insert(0, new ListItem("[No User]", ""));
         }
        else
        {
            DrpUserName.Items.Insert(0, new ListItem("[No User]", ""));
        }
        if (Session["name"] != null)
        {
            DrpUserName.SelectedItem.Value = Session["name"].ToString();
            Session["name"] = null;
        }
     }
    void load_Priority()
    {
        sql += "\nselect priority_name from priorities order by sort_seq, priority_name;";
        DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);

        drpPriority.DataSource = ds_dropdowns.Tables[2];
        drpPriority.DataTextField = "priority_name";
        drpPriority.DataValueField = "priority_name";
        drpPriority.DataBind();
        drpPriority.Items.Insert(0, new ListItem("[no priority]", ""));
    }
    void load_statuses()
    {
        sql = "select status_name from statuses order by sort_seq, status_name;";
        DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);
        drpOurstatus.DataSource = ds_dropdowns.Tables[0];
        drpOurstatus.DataTextField = "status_name";
        drpOurstatus.DataValueField = "status_name";
        drpOurstatus.DataBind();
        drpOurstatus.Items.Insert(0, new ListItem("[no status]", ""));
        drpStatus.DataSource = ds_dropdowns.Tables[0];
        drpStatus.DataTextField = "status_name";
        drpStatus.DataValueField = "status_name";
        drpStatus.DataBind();
        drpStatus.Items.Insert(0, new ListItem("[no status]", ""));
    }

    public void BindData()
    {
        int RequestId = Convert.ToInt16(lblRequestId.Text);
        sql = "Select * from ClientRequest where RequestId=" + RequestId.ToString();
        DataSet dsRequests = DatabaseHelper.getDataset(sql);

        if (dsRequests == null || dsRequests.Tables.Count <= 0 || dsRequests.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "Request not found.<br><br><a href='client_requests.aspx'>View requests</a>";
            divRequest.Visible = false;
            divRequestDetails.Visible = false;
        }
        else
        {
            if (dsRequests.Tables[0].Rows[0]["deleted"].ToString() == "1")
            {
                if (!DatabaseHelper.can_Show_Deleted_Tasks(Session["admin"].ToString()))
                {
                    divMessage.InnerHtml = "Request not found.<br><br><a href='client_requests.aspx'>View requests</a>";
                    divRequest.Visible = false;
                    divRequestDetails.Visible = false;
                    return;
                }
                else
                {
                    btnUpdate.Text = "Mark as undeleted";
                    btnDelete.Text = "Delete permanently";
                }

                deletedFlag = true;
            }
            else
            {
                deletedFlag = false;
            }
            lblRequestId.Text = dsRequests.Tables[0].Rows[0]["RequestId"].ToString();
            lblLastUpdated.Text = " Last changed by <b>" + dsRequests.Tables[0].Rows[0]["LastUpdatedBy"].ToString() + "</b> on <b>" + DateTime.Parse(dsRequests.Tables[0].Rows[0]["LastUpdatedOn"].ToString()).ToString("dd MMM yyyy h:mm tt") + "</b>";
            txtWebsiteURL.Text = dsRequests.Tables[0].Rows[0]["WebsiteURL"].ToString();
            txtShortDescr.Text = dsRequests.Tables[0].Rows[0]["ShortDescr"].ToString();
            drpStatus.SelectedValue = dsRequests.Tables[0].Rows[0]["Status"].ToString();
            try
            {
                drpOurstatus.SelectedValue = dsRequests.Tables[0].Rows[0]["OurStatus"].ToString(); /////drpOurstatus.Items.FindByText(dsRequests.Tables[0].Rows[0]["OurStatus"].ToString()).Value;
            }
            catch { }
            drpPriority.SelectedValue = dsRequests.Tables[0].Rows[0]["Priority"].ToString();
            lblClientId.Text = dsRequests.Tables[0].Rows[0]["ClientId"].ToString();
            try
            {
                DrpUserName.SelectedValue.Equals(dsRequests.Tables[0].Rows[0]["UserName"].ToString());
            }
            catch { }
            lblUserName.Text = dsRequests.Tables[0].Rows[0]["UserName"].ToString();
            prevWebsiteURL.Value = dsRequests.Tables[0].Rows[0]["WebsiteURL"].ToString();
            prevShortDescr.Value = dsRequests.Tables[0].Rows[0]["ShortDescr"].ToString();
            prevStatus.Value = dsRequests.Tables[0].Rows[0]["Status"].ToString();
            prevPriority.Value = dsRequests.Tables[0].Rows[0]["Priority"].ToString();
            hdnProjectCompany.Value = dsRequests.Tables[0].Rows[0]["CompanyName"].ToString();
            try
            {
                string sqlSEO = "select [SEO] from [projects]  where [project_name] = '" + dsRequests.Tables[0].Rows[0]["ProjectName"].ToString() + "'";
                object objResultSEO = DatabaseHelper.executeScalar(sqlSEO);

                if (objResultSEO.ToString() == "True")
                {
                    BtnAnalytics.ForeColor = Color.Red;
                }
                else
                {
                    BtnAnalytics.ForeColor = Color.Blue;
                    BtnAnalytics.Attributes.Add("onclick", "return alert('Please contact support as your subscription level does not allow to view analytics.');");
                }
            }
            catch (Exception ex)
            { }
            string sqlcompanynote = "select [Notes] from [Company]  where Company_Name = '" + dsRequests.Tables[0].Rows[0]["CompanyName"].ToString() + "'";
            object objNoteResult = DatabaseHelper.executeScalar(sqlcompanynote);
            if (objNoteResult != "" && objNoteResult != null)
            {
                CompanyNotes.ForeColor = Color.Red;
            }
            else
            {
                CompanyNotes.ForeColor = Color.Blue;
            }
            string sqlCRnote = "select [ClientNotes] from [Company]  where Company_Name = '" + dsRequests.Tables[0].Rows[0]["CompanyName"].ToString() + "'";
            object objCRNoteResult = DatabaseHelper.executeScalar(sqlCRnote);
            if (objCRNoteResult != "")
            {
                CRNotes.ForeColor = Color.Red;
            }
            else
            {
                CRNotes.ForeColor = Color.Blue;
            }
            DataSet dsUserDetails = DatabaseHelper.getUserDetails(lblClientId.Text, lblUserName.Text);
            if (dsUserDetails == null || dsUserDetails.Tables.Count <= 0 || dsUserDetails.Tables[0].Rows.Count <= 0)
            {
                TDSubscription.InnerHtml = "ND";
            }
            else
            {
                load_Compdropdowns();
                drpCompanyName.SelectedValue = dsUserDetails.Tables[0].Rows[0]["CompanyName"].ToString();
                load_Userdropdowns();
                Loadprogect_drp();
                DrpUserName.SelectedValue = dsRequests.Tables[0].Rows[0]["UserName"].ToString();
                if (dsUserDetails.Tables[0].Rows[0]["Subscription"].ToString() != "")
                {
                    TDSubscription.InnerHtml = dsUserDetails.Tables[0].Rows[0]["Subscription"].ToString();
                }
                else
                {
                    TDSubscription.InnerHtml = "ND";
                }
             }
            drpCompanyName.SelectedValue = dsRequests.Tables[0].Rows[0]["CompanyName"].ToString();
            Loadprogect_drp();
            try
            {
                drpProjects.SelectedValue = dsRequests.Tables[0].Rows[0]["ProjectName"].ToString();
                TaskProject.Value = dsRequests.Tables[0].Rows[0]["ProjectName"].ToString();
            }
            catch (Exception ex)
            { }
            try
            {
                lblIRN.Text = "";
                sql = "select task_id from tasks where RequestId=" + RequestId.ToString() + " order by task_id";
                DataSet dsTaskId = DatabaseHelper.getDataset(sql);
                if (dsTaskId == null || dsTaskId.Tables.Count <= 0 || dsTaskId.Tables[0].Rows.Count <= 0)
                {
                    lblIRN.Text = "";
                }
                else
                {
                    string strIRN = "";
                    for (int i = 0; i < dsTaskId.Tables[0].Rows.Count; i++)
                    {
                        strIRN += "<a target='_blank' href=edit_task.aspx?id=" + dsTaskId.Tables[0].Rows[i][0] + ">" + dsTaskId.Tables[0].Rows[i][0] + "</a>&nbsp;&nbsp;";
                    }
                    lblIRN.Text = strIRN;
                }

            }
            catch (Exception ex)
            { }
            string refnumber = dsRequests.Tables[0].Rows[0]["Internalref"].ToString();
            try
            {
               if (lblIRN.Text == "")
                {
                   if (refnumber == null || refnumber == "0" || refnumber == "")
                    {
                      lblIRN.Text = "";
                    }
                    else
                    {
                       string strIRN2 = "";
                        strIRN2 += "<a target='_blank' href=edit_task.aspx?id=" + dsRequests.Tables[0].Rows[0]["Internalref"].ToString() + ">" + dsRequests.Tables[0].Rows[0]["Internalref"].ToString() + "</a>&nbsp;&nbsp;";

                        lblIRN.Text = strIRN2;
                    }

                }
                else
                {
                    if (refnumber == null || refnumber == "0" || refnumber == "")
                    {
                        lblIRN.Text = lblIRN.Text;
                    }
                    else
                    {
                      lblIRN.Text = lblIRN.Text + "<a target='_blank' href=edit_task.aspx?id=" + dsRequests.Tables[0].Rows[0]["Internalref"].ToString() + ">";
                    }

                }
            }
            catch { }
            sql = "Select * from ClientRequest_Details where deleted <> 1 and RequestId=" + RequestId.ToString() + " order by CommentId desc";
            DataSet dsReqDetails = DatabaseHelper.getDataset(sql);
            if (dsReqDetails == null || dsReqDetails.Tables.Count <= 0 || dsReqDetails.Tables[0].Rows.Count <= 0)
            {
                BtnDelAllCmnt.Visible = false;
                tblreadDel.Visible = false;
             }
            else
            {
                BtnDelAllCmnt.Visible = true;
                tblreadDel.Visible = true;
            }
            DataGrid1.DataSource = dsReqDetails.Tables[0];
            DataGrid1.DataBind();
        }
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        int RequestId = Convert.ToInt16(lblRequestId.Text);
        if (ftbComment.Content != "")
        {
            if (DrpSupport.SelectedItem.ToString() == "[None]")
            {
                if (Chkalias.Checked)
                {
                    ftbComment.Content = ftbComment.Content + "<br/><br/><span style=\"color: red;\">" + "Added by support<br/> Note to client: to optimise the request process please add requests directly into the change request system." + "</span>";
                }
                else
                {
                 ftbComment.Content = ftbComment.Content;
                }
                
            }
            else
            {
                if (Chkalias.Checked)
                {
                    ftbComment.Content = ftbComment.Content + "<br/><br/><span style=\"color: red;\">" + "Added by support (emailed to '" + DrpSupport.SelectedItem.ToString() + "')<br/> Note to client: to optimise the request process please add requests directly into the change request system." + "</span>";
                }
                else
                {
                    ftbComment.Content = ftbComment.Content;
                }
           }
        }
        //================================================
        string[] strArrayMacIDAndIP = new string[2];
        string strMacId = "";
        string strIpAddress = "";

        foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
        {
            if (nic.OperationalStatus == OperationalStatus.Up)
            {
                strMacId += nic.GetPhysicalAddress().ToString();
                break;
            }
        }

        strArrayMacIDAndIP[0] = strMacId;

        strIpAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (strIpAddress == null)

            strIpAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

        strArrayMacIDAndIP[1] = strIpAddress;
        //================================================
        string sql = "";
        if (ftbComment.Content.Trim() != "")
        {
            sql = " Update ClientRequest ";
            sql += " set WebsiteURL='" + txtWebsiteURL.Text.Trim().Replace("'", "''") + "', ";
            sql += " ShortDescr='" + txtShortDescr.Text.Trim().Replace("'", "''") + "', ";
            sql += " Status='" + drpStatus.SelectedValue.Replace("'", "''") + "', ";
            sql += " Priority=" + drpPriority.SelectedValue + ", ";
            if (Chkalias.Checked)
            {
                sql += "LastUpdatedBy='" + lblUserName.Text.Replace("'", "''") + "', ";
            }
            else
            {
                sql += "LastUpdatedBy='Support', ";
            }

            sql += " LastUpdatedOn=getdate(), ";
            sql += " CompanyName ='" + drpCompanyName.SelectedValue + "' ,";
            sql += " ProjectName ='" + drpProjects.SelectedValue + "',";
            sql += " OurStatus ='" + drpOurstatus.SelectedItem.Text.Replace("'", "''") + "' ";
            sql += " where RequestId=" + RequestId.ToString() + "; ";
            sql += "DECLARE @CommentId INT;";
            sql += " insert into ClientRequest_Details(RequestId,Comment,Ip,Macid,Approve,ApprovUnApprov,PostedBy,PostedOn) ";
            sql += " values(" + RequestId.ToString() + ",";
            sql += "'" + ftbComment.Content.ToString().Replace("'", "''") + "',";
            sql += "'" + strArrayMacIDAndIP[1] + "',";
            sql += "'" + strArrayMacIDAndIP[0] + "',";
            sql += "'False',";
            sql += "'Approve',";
            if (Chkalias.Checked)
            {
                sql += "'" + "Support on behalf of  " + lblUserName.Text.Replace("'", "''") + "',";
            }
            else
            {
                sql += "'Support',";
            }
            sql += "getdate()); ";
            sql += " SET @CommentId = SCOPE_IDENTITY(); ";
            sql += " insert into read_CR_comments([CommentId],[UserName]) ";
            sql += " values(@CommentId,'" + Session["admin"].ToString() + "')";
        }
        else
        {
            sql = " Update ClientRequest ";
            sql += " set WebsiteURL='" + txtWebsiteURL.Text.ToString().Trim().Replace("'", "''") + "', ";
            sql += "ShortDescr='" + txtShortDescr.Text.Trim().Replace("'", "''") + "', ";
            sql += "Status='" + drpStatus.SelectedValue.Replace("'", "''") + "', ";
            sql += "Priority=" + drpPriority.SelectedValue + ", ";
            sql += "UserName='" + DrpUserName.SelectedItem.Text + "', ";
            if (Chkalias.Checked)
            {
                sql += "LastUpdatedBy='" + lblUserName.Text.Replace("'", "''") + "', ";
            }
            else
            {
                sql += "LastUpdatedBy='Support', ";
            }
            sql += "LastUpdatedOn=getdate() ,";
            sql += "CompanyName ='" + drpCompanyName.SelectedValue + "',";
            sql += " ProjectName ='" + drpProjects.SelectedValue + "',";
            sql += " OurStatus ='" + drpOurstatus.SelectedItem.Text.Replace("'", "''") + "' ";
            sql += "where RequestId=" + RequestId.ToString() + "; ";
        }

        int intResult = DatabaseHelper.executeNonQuery(sql);

        if (intResult != 0)
        {
            if (btnUpdate.Text == "Mark as undeleted")
            {
                intResult = DatabaseHelper.executeNonQuery("update ClientRequest set deleted = 0 where RequestId=" + RequestId.ToString());
                task_action = "Support has restored deleted your Change Request  ";
                emailSubject = "CR: " + RequestId.ToString() + " was restored - " + txtShortDescr.Text.Trim() + " (CR ID:" + RequestId.ToString() + ")";
            }
            else
            {
                task_action = "Support has updated your Change Request  ";
                emailSubject = "CR: " + RequestId.ToString() + " was updated - " + txtShortDescr.Text.Trim() + " (CR ID:" + RequestId.ToString() + ")";
                DataSet dsUserDetails = DatabaseHelper.getUserDetails(lblClientId.Text, lblUserName.Text);
                Session["name"] = lblUserName.Text;
                bool flag = false;
                if (dsUserDetails == null || dsUserDetails.Tables.Count <= 0 || dsUserDetails.Tables[0].Rows.Count <= 0)
                {}
                else
                {
                    if (chkEmailnotify.Checked)
                    {}
                }
                if (chkEmailnotify.Checked)
                {}
                
            }
            Session["message"] = "CR was updated.";
            Session.Remove("Canned_msg");
            try
            {

                string sqlcomment = "select *  from ClientRequest_Details where [RequestId] = " + lblRequestId.Text + " and [CommentId] not in (select  CommentId  from read_CR_comments where read_CR_comments.username='" + Session["admin"].ToString() + "')";
                DataSet dscomment = DatabaseHelper.getDataset(sqlcomment);
                if (dscomment != null && dscomment.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dscomment.Tables[0].Rows.Count; i++)
                    {
                        try
                        {
                            string sqlinsert = "insert into read_CR_comments([CommentId],[UserName]) ";
                            sqlinsert += " values(" + dscomment.Tables[0].Rows[i]["CommentId"].ToString() + ",'" + Session["admin"].ToString() + "')";
                            int intCommentResult = DatabaseHelper.executeNonQuery(sqlinsert);
                        }
                        catch
                        {
                        }
                    }


                }
            }
            catch { }
            Response.Redirect("edit_request.aspx?reqid=" + RequestId.ToString());
             }
        else
        {
            divMessage1.InnerHtml = "Comment was not updated. Please try again.";
            divMessage1.Visible = true;
        }
    }

    private string record_changes_for_email(bool isAdmin)
    {
        string strSql = "";

        if (prevStatus.Value != drpStatus.SelectedValue)
        {
            strSql += "Status has been changed from \""
                + prevStatus.Value.Replace("'", "''") + "\" to \""
                + drpStatus.SelectedItem.Text.Replace("'", "''") + "\"<br>";
        }
        if (isAdmin)
        {
            if (prevPriority.Value != drpPriority.SelectedValue)
            {
                strSql += "Priority has been changed from \""
                    + prevPriority.Value.Replace("'", "''") + "\" to \""
                    + drpPriority.SelectedItem.Text.Replace("'", "''") + "\"<br>";
            }
        }

        if (prevWebsiteURL.Value != txtWebsiteURL.Text.Trim())
        {
            strSql += "Relevant URL has been changed from  \""
                + prevWebsiteURL.Value.Replace("'", "''") + "\" to \""
               + txtWebsiteURL.Text.Trim().Replace("'", "''") + "\"<br>";
        }

        if (prevShortDescr.Value != txtShortDescr.Text.Trim())
        {
            strSql += "Short description has been changed from \""
                + prevShortDescr.Value.Replace("'", "''") + "\" to \""
                + txtShortDescr.Text.Trim().Replace("'", "''") + "\"<br>";
        }
        
        if (ftbComment.Content.Trim() != "")
        {
            strSql += "Comment has been added.<br>";
        }
        return strSql;
    }

    private string generateEmail(int RequestId)
    {
        DataSet dsRequests = DatabaseHelper.getClientRequest(RequestId);
        string strBody = "";
        if (dsRequests == null || dsRequests.Tables.Count <= 0 || dsRequests.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "Request not found.<br><br><a href='requests.aspx'>View requests</a>";
        }
        else
        {

            strBody += "<b>" + lblUserName.Text + " :</b> <span style=\"color:red\">";
            strBody += task_action + "<span style=\"color:red\"> (number  </span>" + "<a style=\"color:red\" href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/edit_request.aspx?reqid=" + RequestId + "'>" + RequestId + "</a>) </span> on " + DateTime.Parse(dsRequests.Tables[0].Rows[0]["LastUpdatedOn"].ToString()).ToString("dd MMM h:mm tt") + "<br><br>";
            string attribures = record_changes_for_email(false);
            if (attribures.ToString().Trim() != "")
            {
                strBody += "<span style=\"color:red\">" + attribures + "</span><br>";
            }
            strBody += "<span style=\'font-size:11.0pt;font-family:Calibri,sans-serif;color:red\'>Click on the following link to access or amend the change request: " + "  " + "<a href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/edit_request.aspx?reqid=" + RequestId + "'>" + txtShortDescr.Text.Trim() + " . " + "</a>" + "  Do not start a new change request as this will slow the resolution." + "</span><br><br>";
            strBody += "Client: " + dsRequests.Tables[0].Rows[0]["UserName"].ToString() + "<br>";
            strBody += "Company: " + drpCompanyName.SelectedValue.ToString() + "<br>";
            strBody += "Reported on: " + DateTime.Parse(dsRequests.Tables[0].Rows[0]["RequestDate"].ToString()).ToString("dd MMM h:mm tt") + "<br>";
            strBody += "Title or preferably URL: <a href='" + dsRequests.Tables[0].Rows[0]["WebsiteURL"].ToString() + "' target='_blank'>" + dsRequests.Tables[0].Rows[0]["WebsiteURL"].ToString() + "</a><br>";
            strBody += "Status: (" + drpPriority.SelectedItem.Text + " : " + dsRequests.Tables[0].Rows[0]["status"].ToString() + ")";
            strBody += "<br><br>";

            DataSet dsReqDetails = DatabaseHelper.getClientRequestDetails(RequestId);
            strBody += "The progress of the change request can be seen below:" + "<br><br>";
            strBody += "<table cellspacing=\"0\" border=\"1\" style=\"width: 99%;border-collapse: collapse;\">";
            strBody += "<tbody>";
            for (int i = 0; i < dsReqDetails.Tables[0].Rows.Count; i++)
            {
                strBody += "<tr>";
                strBody += "<td>";
                strBody += "<div style=\"border: 1px none Green;\">";
                strBody += "<table width=\"100%\">";
                strBody += "<tbody>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span style=\"color: Green;\">comment " + dsReqDetails.Tables[0].Rows[i]["CommentId"].ToString() + " posted by " + dsReqDetails.Tables[0].Rows[i]["PostedBy"].ToString() + " on " + DateTime.Parse(dsReqDetails.Tables[0].Rows[i]["PostedOn"].ToString()).ToString("dd MMM yyyy h:mm tt") + "</span>";
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\"> </td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span>" + dsReqDetails.Tables[0].Rows[i]["comment"].ToString() + "</span>";
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "</tbody>";
                strBody += "</table>";
                strBody += "</div>";
                strBody += "</td>";
                strBody += "</tr>";
            }
            strBody += "</tbody>";
            strBody += "</table>";
            strBody += "<br><span style=\'font-size:11.0pt;font-family:Calibri,sans-serif;color:red\'>Please do not reply to this email as it does not take replies. Please use the Change Request System as this is the quickest way to process your requests and also keeps a record of communications and progress for you to review at any time.</span><br/>";
            strBody += "<br>";
            strBody += "<br/><br/>" + "Thanks & Regards," + "<br/>Support.";
        }
        return strBody;
    }

    private string generateAdminEmail(int RequestId)
    {
        DataSet dsRequests = DatabaseHelper.getClientRequest(RequestId);
        string strBody = "";
        if (dsRequests == null || dsRequests.Tables.Count <= 0 || dsRequests.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "Request not found.<br><br><a href='requests.aspx'>View requests</a>";
        }
        else
        {
            strBody += "<b>" + lblUserName.Text + " :</b> <span style=\"color:red\">";
            strBody += task_action + "<span style=\"color:red\"> (number  </span>" + " <a style=\"color:red\" href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_request.aspx?reqid=" + RequestId + "'>" + RequestId + "</a>) </span> on " + DateTime.Parse(dsRequests.Tables[0].Rows[0]["LastUpdatedOn"].ToString()).ToString("dd MMM h:mm tt") + "<br><br>";
            string attribures = record_changes_for_email(true);
            if (attribures.ToString().Trim() != "")
            {
                 strBody += "<span style=\"color:red\">" + attribures + "</span><br>";
            }

            strBody += "<span style=\'font-size:11.0pt;font-family:Calibri,sans-serif;color:red\'>Click on the following link to access or amend the change request:  " + " " + " <a href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_request.aspx?reqid=" + RequestId + "'>" + txtShortDescr.Text.Trim() + " . " + "</a>" + "  Do not start a new change request as this will slow the resolution." + "</span><br><br>";
            strBody += "Client: " + dsRequests.Tables[0].Rows[0]["UserName"].ToString() + "<br>";
            strBody += "Company: " + drpCompanyName.SelectedValue.ToString() + "<br>";
            strBody += "Reported on: " + DateTime.Parse(dsRequests.Tables[0].Rows[0]["RequestDate"].ToString()).ToString("dd MMM h:mm tt") + "<br>";
            strBody += "Title or preferably URL: <a href='" + dsRequests.Tables[0].Rows[0]["WebsiteURL"].ToString() + "' target='_blank'>" + dsRequests.Tables[0].Rows[0]["WebsiteURL"].ToString() + "</a><br>";
            strBody += "Status: (" + drpPriority.SelectedItem.Text + " : " + dsRequests.Tables[0].Rows[0]["status"].ToString() + ")";
            strBody += "<br><br>";
            DataSet dsReqDetails = DatabaseHelper.getClientRequestDetails(RequestId);
            strBody += "The progress of the change request can be seen below:" + "<br><br>";
            strBody += "<table cellspacing=\"0\" border=\"1\" style=\"width: 99%;border-collapse: collapse;\">";
            strBody += "<tbody>";

            for (int i = 0; i < dsReqDetails.Tables[0].Rows.Count; i++)
            {
                strBody += "<tr>";
                strBody += "<td>";
                strBody += "<div style=\"border: 1px none Green;\">";
                strBody += "<table width=\"100%\">";
                strBody += "<tbody>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span style=\"color: Green;\">comment " + dsReqDetails.Tables[0].Rows[i]["CommentId"].ToString() + " posted by " + dsReqDetails.Tables[0].Rows[i]["PostedBy"].ToString() + " on " + DateTime.Parse(dsReqDetails.Tables[0].Rows[i]["PostedOn"].ToString()).ToString("dd MMM yyyy h:mm tt") + "</span>";
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\"> </td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span>" + dsReqDetails.Tables[0].Rows[i]["comment"].ToString() + "</span>";
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "</tbody>";
                strBody += "</table>";
                strBody += "</div>";
                strBody += "</td>";
                strBody += "</tr>";
            }
            strBody += "</tbody>";
            strBody += "</table>";
            strBody += "<br><span style=\'font-size:11.0pt;font-family:Calibri,sans-serif;color:red\'>Please do not reply to this email as it does not take replies. Please use the Change Request System as this is the quickest way to process your requests and also keeps a record of communications and progress for you to review at any time.</span><br/>";
            strBody += "<br>";
            strBody += "<br/><br/>" + "Thanks & Regards," + "<br/>Support.";
        }
        return strBody;
    }

    protected void DataGrid1_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemIndex != -1)
        {

            //=================================================================
            Label lblid = (Label)e.Item.FindControl("lblid");
            Label lblmacid = (Label)e.Item.FindControl("lblmacid");
            Label lblipAd = (Label)e.Item.FindControl("lblipAdd");
            Label lblMacipAd = (Label)e.Item.FindControl("lblMacipAdd");

            if (DatabaseHelper.isAdmin(Session["admin"].ToString()))
            {
                if (lblid.Text == "")
                {
                    lblid.Visible = false;
                    lblipAd.Visible = false;
                }
                else
                {
                    lblid.Visible = true;
                    lblipAd.Visible = true;
                }
                if (lblmacid.Text == "")
                {
                    lblmacid.Visible = false;
                    lblMacipAd.Visible = false;
                }
                else
                {
                    lblmacid.Visible = true;
                    lblMacipAd.Visible = true;
                }
            }
            else
            {
                lblid.Visible = false;
                lblmacid.Visible = false;
                lblipAd.Visible = false;
                lblMacipAd.Visible = false;
            }
            //=================================================================
            Button ApprovUnapprovButton = (Button)e.Item.FindControl("ApprovUnapprov");
            if (!DatabaseHelper.CanApprove(Session["admin"].ToString()))
            {
                ApprovUnapprovButton.Visible = false;

            }
            if (DatabaseHelper.CanApprove(Session["admin"].ToString()))
            {
                if (ApprovUnapprovButton.Text == "Approve")
                {
                    ApprovUnapprovButton.ForeColor = System.Drawing.Color.Red;
                    ApprovUnapprovButton.Font.Bold = true;
                    ApprovUnapprovButton.Font.Underline = true;
                }
            }
            //=================================================================
            Label lblPosted = (Label)e.Item.FindControl("lblPosted");
            Button readMarkButton = (Button)e.Item.FindControl("lnkReadMark");
            lblPosted.Text = "comment " + e.Item.Cells[1].Text + " posted by " + e.Item.Cells[2].Text + " on " + DateTime.Parse(e.Item.Cells[3].Text).ToString("dd MMM yyyy h:mm tt");
            sql = "select count(*) from read_CR_comments where CommentId=" + e.Item.Cells[1].Text + " and username='" + Session["admin"].ToString() + "'";
            object objIsRead = DatabaseHelper.executeScalar(sql);
            if (objIsRead.ToString() == "0")
            {
                lblPosted.ForeColor = System.Drawing.Color.Red;
                readMarkButton.Text = "[Unread. Mark as read]";
                readMarkButton.Style.Add(HtmlTextWriterStyle.Color, "red");
            }
            else
            {
                lblPosted.ForeColor = System.Drawing.Color.Green;
                readMarkButton.Text = "[Read. Mark as unread]";
                readMarkButton.Style.Add(HtmlTextWriterStyle.Color, "green");
            }
            readMarkButton.Visible = true;
        }
    }

    protected void DataGrid1_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "delete")
        {
            sql = "update ClientRequest_Details set deleted=1 Where [RequestId]=" + e.Item.Cells[0].Text + " and [CommentId]=" + e.Item.Cells[1].Text;

            int intResult = DatabaseHelper.executeNonQuery(sql);

            if (intResult != 0)
            {
                divMessage1.InnerHtml = "Comment " + e.Item.Cells[1].Text + "  was deleted.";

                divMessage1.Visible = true;
                ftbComment.Content = "";
                BindData();
            }
            else
            {
                divMessage1.InnerHtml = "Comment " + e.Item.Cells[1].Text + "  was not deleted.";
                divMessage1.Visible = true;
            }
        }
        else if (e.CommandName == "ReadMark")
        {
            Button readMarkButton = (Button)e.Item.FindControl("lnkReadMark");

            if (readMarkButton.Text == "[Unread. Mark as read]")
            {
                sql = @"insert into read_CR_comments([CommentId],[UserName]) 
                        values(" + e.Item.Cells[1].Text + ",'" + Session["admin"].ToString() + "')";
                DatabaseHelper.executeNonQuery(sql);
                BindData();
            }
            else
            {
                sql = @"delete from read_CR_comments where [CommentId]=" + e.Item.Cells[1].Text + " and [UserName]='" + Session["admin"].ToString() + "'";
                DatabaseHelper.executeNonQuery(sql);
                BindData();
            }
        }
        else if (e.CommandName == "Create_new_task")
        {
            Response.Redirect("edit_task.aspx?reqid=" + lblRequestId.Text + "&cid=" + ((HiddenField)e.Item.FindControl("hdn_commandId")).Value);
        }
        else if (e.CommandName == "ApprovMark")
        {
            
            //===============================================================================
            //This Code will Run by 'M'
            //===============================================================================
            string strheader = ((Button)e.Item.FindControl("ApprovUnapprov")).Text;
            if (strheader == "Approve")
            {
                string strBodyAdmin = "", strBodyclient = "";
                int intApprovCr = 0, intRetResult;
                int RequestId = Convert.ToInt16(lblRequestId.Text);
                string sqlchkApp;
                sqlchkApp = "select * from ClientRequest_Details where Deleted<>1 and Approve='False' and RequestId=" + RequestId.ToString() + " and CommentId=" + e.Item.Cells[1].Text + "";
                DataSet dschkApp = DatabaseHelper.getDataset(sqlchkApp);

                if (DatabaseHelper.CanApprove(Session["admin"].ToString()) && ftbComment.Content == "" && dschkApp != null && dschkApp.Tables.Count > 0 && dschkApp.Tables[0].Rows.Count > 0)
                {
                    string sqlselect;
                    sqlselect = "select * from ClientRequest_Details where Deleted<>1 and Approve='False' and RequestId=" + RequestId.ToString() + " and  CommentId=" + e.Item.Cells[1].Text + "";
                    DataSet dsselect = DatabaseHelper.getDataset(sqlselect);
                    if (dsselect != null && dsselect.Tables.Count > 0 && dsselect.Tables[0].Rows.Count > 0)
                    {
                        //**********for Approve the Change Request******************************************* 
                        string chgrequest, ApprovCr;
                        chgrequest = "Select * from ClientRequest where CRApprove='False' and Deleted<>1 and RequestId=" + RequestId.ToString() + "";
                        DataSet dschgrequest = DatabaseHelper.getDataset(chgrequest);
                        if (dschgrequest != null && dschgrequest.Tables.Count > 0 && dschgrequest.Tables[0].Rows.Count > 0)
                        {
                            
                            ApprovCr = "Update ClientRequest set CRApprove='True' where CRApprove='False' and Deleted<>1 and RequestId=" + RequestId.ToString() + ";";
                            intApprovCr = DatabaseHelper.executeNonQuery(ApprovCr);
                        }
                        //************************************************************************************ 
                        string sqlapproveCmt, UpdateApp;
                       
                        UpdateApp = " Update ClientRequest_Details set ApprovUnApprov='UnApprove' where ApprovUnApprov='Approve' and Deleted<>1 and RequestId=" + RequestId.ToString() + " and CommentId=" + e.Item.Cells[1].Text + ";";
                        int intAppResult = DatabaseHelper.executeNonQuery(UpdateApp);
                        sqlapproveCmt = " Update ClientRequest_Details  set Approve='True' where Approve='False' and Deleted<>1 and RequestId=" + RequestId.ToString() + " and CommentId=" + e.Item.Cells[1].Text + ";";
                        intRetResult = DatabaseHelper.executeNonQuery(sqlapproveCmt);
                        //===============================================================================================================
                        //***************************************************************************************************************
                        //if (intApprovCr != 0)
                        //{
                            //--------------Send Email after approving CR by Highest admin------------------------------------------------


                            if (chkEmailnotify.Checked)
                            {
                                bool flag = false;
                                strBodyAdmin = generateAdminEmailNew(Convert.ToInt32(RequestId.ToString()));
                                flag = DatabaseHelper.sendEmailChangeRequestNorply(ConfigurationManager.AppSettings["ToEmail1"].ToString(), "Request ID:" + RequestId.ToString() + " was updated - Change request form  (Request ID:" + RequestId.ToString() + ")", strBodyAdmin);
                            }

                            //if (chkEmailnotify.Checked)
                            //{
                                //string strusername = "";
                                //int i = 0;
                                //foreach (ListItem boxItem in chkboxclient.Items)
                                //{
                                //    if (boxItem.Selected == true)
                                //    {
                                //        if (i == 0)
                                //        {
                                //            bool flag = false;
                                //            strusername = boxItem.Value + ";";
                                //            strBodyclient = generateClientEmailNew(Convert.ToInt32(RequestId.ToString()), boxItem.Text);
                                //            flag = DatabaseHelper.sendEmailChangeRequestNorply(boxItem.Value, "Request ID:" + RequestId.ToString() + " was updated - Change request form  (Request ID:" + RequestId.ToString() + ")", strBodyclient);
                                //            i++;
                                //        }
                                //        else
                                //        {
                                //            bool flag = false;
                                //            strusername += boxItem.Value + ";";
                                //            strBodyclient = generateClientEmailNew(Convert.ToInt32(RequestId.ToString()), boxItem.Text);
                                //            flag = DatabaseHelper.sendEmailChangeRequestNorply(boxItem.Value, "Request ID:" + RequestId.ToString() + " was updated - Change request form  (Request ID:" + RequestId.ToString() + ")", strBodyclient);
                                //        }
                                //    }
                                //}
                                //strusername = strusername.TrimEnd(';');
                                
                            //}


                            //======================================================
                            if (chkEmailnotify.Checked)
                            {
                            bool flag = false;
                            string strusername = "";
                            int i = 0;
                            foreach (ListItem boxItem in chkboxclient.Items)
                            {
                                if (boxItem.Selected == true)
                                {
                                    if (i == 0)
                                    {
                                        strusername = boxItem.Value + ";";
                                        i++;
                                    }
                                    else
                                    {
                                        strusername += boxItem.Value + ";";
                                    }
                                }
                            }
                            strusername = strusername.TrimEnd(';');
                            strBodyclient = generateClientEmailNew(Convert.ToInt32(RequestId.ToString()));
                            flag = DatabaseHelper.sendEmailChangeRequestNorply(strusername, "Request ID:" + RequestId.ToString() + " was updated - Change request form  (Request ID:" + RequestId.ToString() + ")", strBodyclient);
                    }
                            ////=====================================================







                            //DataSet dsUserDetails = DatabaseHelper.getUserDetails2(drpCompanyName.SelectedItem.ToString(), lblUserName.Text);

                            //bool flag = false;


                            
                            //    if (dsUserDetails == null || dsUserDetails.Tables.Count <= 0 || dsUserDetails.Tables[0].Rows.Count <= 0)
                            //    {
                            //    }
                            //    else
                            //    {
                            //        if (chkEmailnotify.Checked)
                            //        {
                            //        flag = DatabaseHelper.sendEmailChangeRequestNorply(dsUserDetails.Tables[0].Rows[0]["Email"].ToString(), "Request ID:" + RequestId.ToString() + " was updated - Change request form  (Request ID:" + RequestId.ToString() + ")", strBodyclient);
                            //         }

                            //    }

                            //    if (chkEmailnotify.Checked)
                            //    {
                            //    flag = DatabaseHelper.sendEmailChangeRequestNorply(ConfigurationManager.AppSettings["ToEmail1"].ToString(), "Request ID:" + RequestId.ToString() + " was updated - Change request form  (Request ID:" + RequestId.ToString() + ")", strBody);
                            //    }
                           
                            Response.Redirect("edit_request.aspx?reqid=" + RequestId.ToString());


                        }
                        else
                        {
                            //=============================================================================================================
                            //*************************************************************************************************************

                            if (btnUpdate.Text == "Mark as undeleted")
                            {
                                intRetResult = DatabaseHelper.executeNonQuery("update ClientRequest set deleted = 0 where RequestId=" + RequestId.ToString());
                                task_action = "Support has restored deleted your Change Request  ";
                                emailSubject = "CR: " + RequestId.ToString() + " was restored - " + txtShortDescr.Text.Trim() + " (CR ID:" + RequestId.ToString() + ")";

                            }
                            else
                            {
                                task_action = "Support has updated your Change Request  ";
                                emailSubject = "CR: " + RequestId.ToString() + " was updated - " + txtShortDescr.Text.Trim() + " (CR ID:" + RequestId.ToString() + ")";
                                DataSet dsUserDetails = DatabaseHelper.getUserDetails(lblClientId.Text, lblUserName.Text);
                                Session["name"] = lblUserName.Text;
                                bool flag = false;
                                if (dsUserDetails == null || dsUserDetails.Tables.Count <= 0 || dsUserDetails.Tables[0].Rows.Count <= 0)
                                { }
                                else
                                {
                                    if (chkEmailnotify.Checked)
                                    {
                                        flag = DatabaseHelper.sendEmailChangeRequestNorply(dsUserDetails.Tables[0].Rows[0]["Email"].ToString(), emailSubject, generateEmail(RequestId));
                                    }
                                }
                                if (chkEmailnotify.Checked)
                                {
                                    flag = DatabaseHelper.sendEmailChangeRequestNorply(ConfigurationManager.AppSettings["ToEmail1"].ToString(), emailSubject, generateAdminEmail(RequestId));
                                }
                            }
                            Session["message"] = "CR was updated.";
                            Session.Remove("Canned_msg");
                            try
                            {
                                string sqlcomment = "select *  from ClientRequest_Details where [RequestId] = " + lblRequestId.Text + " and [CommentId] not in (select  CommentId  from read_CR_comments where read_CR_comments.username='" + Session["admin"].ToString() + "')";
                                DataSet dscomment = DatabaseHelper.getDataset(sqlcomment);

                                if (dscomment != null && dscomment.Tables[0].Rows.Count > 0)
                                {
                                    for (int i = 0; i < dscomment.Tables[0].Rows.Count; i++)
                                    {
                                        try
                                        {
                                            string sqlinsert = "insert into read_CR_comments([CommentId],[UserName]) ";
                                            sqlinsert += " values(" + dscomment.Tables[0].Rows[i]["CommentId"].ToString() + ",'" + Session["admin"].ToString() + "')";

                                            int intCommentResult = DatabaseHelper.executeNonQuery(sqlinsert);
                                        }
                                        catch
                                        {
                                        }
                                    }
                                }
                            }
                            catch { }
                            Response.Redirect("edit_request.aspx?reqid=" + RequestId.ToString());
                       }
                        
                    }

                    else
                    {
                        divMessage1.Visible = true;
                        divMessage1.InnerHtml = "You have no comment's for approved";

                    }
                }
                else if (DatabaseHelper.CanApprove(Session["admin"].ToString()) && ftbComment.Content != "")
                {
                    insertbyAdmin();
                }
                
            }
        }
   

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        int RequestId = Convert.ToInt16(lblRequestId.Text);
        if (btnDelete.Text == "Delete permanently")
        {
            task_action = "Support has permanently deleted your Change Request  ";
            emailSubject = "CR:" + RequestId.ToString() + " was permanently deleted  - " + txtShortDescr.Text.Trim() + " (CR ID:" + RequestId.ToString() + ")";
            string strBody = generateEmail(RequestId);
            string strBody1 = generateAdminEmail(RequestId);
            sql = "delete from read_CR_comments where CommentId in (select CommentId from ClientRequest_Details where RequestId =" + RequestId.ToString() + "); ";
            sql += "delete from ClientRequest_Details where RequestId =" + RequestId.ToString() + "; ";
            sql += "delete from ClientRequest_Attachment where RequestId =" + RequestId.ToString() + "; ";
            sql += "delete from ClientRequest where RequestId =" + RequestId.ToString() + "; ";
            int intResult = DatabaseHelper.executeNonQuery(sql);
            if (intResult != 0)
            {
                try
                {
                    string sqlcomment = "select *  from ClientRequest_Details where [RequestId] = " + lblRequestId.Text + " and [CommentId] not in (select  CommentId  from read_CR_comments where read_CR_comments.username='" + Session["admin"].ToString() + "')";
                    DataSet dscomment = DatabaseHelper.getDataset(sqlcomment);

                    if (dscomment != null && dscomment.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < dscomment.Tables[0].Rows.Count; i++)
                        {
                            try
                            {
                                string sqlinsert = "insert into read_CR_comments([CommentId],[UserName]) ";
                                sqlinsert += " values(" + dscomment.Tables[0].Rows[i]["CommentId"].ToString() + ",'" + Session["admin"].ToString() + "')";

                                int intCommentResult = DatabaseHelper.executeNonQuery(sqlinsert);
                            }
                            catch
                            {
                            }
                        }
                    }
                }
                catch { }

                Response.Redirect("client_requests.aspx", false);
            }
            else
            {
                divMessage1.InnerHtml = "There was problem in deleting change request. Please try again.";
                divMessage1.Visible = true;
            }
        }
        else
        {
            task_action = "Support has deleted your Change Request  ";
            emailSubject = "CR:" + RequestId.ToString() + " was deleted  - " + txtShortDescr.Text.Trim() + " (CR ID:" + RequestId.ToString() + ")";
            string strBody = generateEmail(RequestId);
            string strBody1 = generateAdminEmail(RequestId);
            sql = "update ClientRequest set deleted=1 where [RequestId] = " + RequestId + "; ";

            int intResult = DatabaseHelper.executeNonQuery(sql);

            if (intResult != 0)
            {
                try
                {

                    string sqlcomment = "select *  from ClientRequest_Details where [RequestId] = " + lblRequestId.Text + " and [CommentId] not in (select  CommentId  from read_CR_comments where read_CR_comments.username='" + Session["admin"].ToString() + "')";
                    DataSet dscomment = DatabaseHelper.getDataset(sqlcomment);

                    if (dscomment != null && dscomment.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < dscomment.Tables[0].Rows.Count; i++)
                        {
                            try
                            {
                                string sqlinsert = "insert into read_CR_comments([CommentId],[UserName]) ";
                                sqlinsert += " values(" + dscomment.Tables[0].Rows[i]["CommentId"].ToString() + ",'" + Session["admin"].ToString() + "')";

                                int intCommentResult = DatabaseHelper.executeNonQuery(sqlinsert);
                            }
                            catch
                            {
                            }
                        }
                    }
                }
                catch { }

                Response.Redirect("client_requests.aspx", false);
            }
            else
            {
                divMessage1.InnerHtml = "There was problem in deleting change request. Please try again.";
                divMessage1.Visible = true;
            }
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        sql = "update tasks set RequestId = " + lblRequestId.Text + " where deleted <> 1 and task_id = " + txtIRN.Text.Trim();
        int intResult = DatabaseHelper.executeNonQuery(sql);
        if (intResult != 0)
        {
            txtIRN.Text = "";
            BindData();
            divMessage1.InnerHtml = "Internal reference number was updated.";
            divMessage1.Visible = true;
        }
        else
        {
            divMessage1.InnerHtml = "Error while updating internal reference number. Please ensure the task with internal reference number is exist.";
            divMessage1.Visible = true;
        }
    }

    protected void btnRemove_Click(object sender, EventArgs e)
    {
        sql = "update tasks set RequestId = 0 where task_id = " + txtIRN.Text.Trim();

        int intResult = DatabaseHelper.executeNonQuery(sql);

        if (intResult != 0)
        {
            BindData();
            divMessage1.InnerHtml = "Internal reference number was removed.";
            divMessage1.Visible = true;
        }
        else
        {
            divMessage1.InnerHtml = "Error while removing internal reference number. Please ensure the task with internal reference number is exist.";
            divMessage1.Visible = true;
        }
    }

    protected void lnkClose2_Click(object sender, EventArgs e)
    {
        ftbComment.Content = Session["Canned_msg"].ToString();
    }
    protected void drpCompanyName_SelectedIndexChanged(object sender, EventArgs e)
    {
        load_Userdropdowns();
        Loadprogect_drp();
    }

    void Loadprogect_drp()
    {
        try
        {
            drpProjects.Items.Clear();


            if (drpCompanyName.SelectedValue.ToString() != "")
            {
                sql = @"select project_name
		from projects
		where active = 'Y' and CompanyName='" + drpCompanyName.SelectedValue.ToString() + "' order by project_name;";
            }
            else
            {
                sql = @"select project_name
		from projects
		where active = 'Y' order by project_name;";
            }
            DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);
            if (ds_dropdowns.Tables[0].Rows.Count > 0)
            {
                drpProjects.DataSource = ds_dropdowns.Tables[0];
                drpProjects.DataTextField = "project_name";
                drpProjects.DataValueField = "project_name";
                drpProjects.DataBind();
                drpProjects.Items.Insert(0, new ListItem("[no project]", ""));
             }
            else
            {
                drpProjects.Items.Insert(0, new ListItem("[no project]", ""));
                
            }
        }
        catch { }
    }

    protected void LnkMakecommentRead_Click(object sender, EventArgs e)
    {
        string sql = "select *  from ClientRequest_Details where [CommentId] not in (select  CommentId  from read_CR_comments where read_CR_comments.username='" + Session["admin"].ToString() + "') and ClientRequest_Details.deleted <> 1";
        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {
                    string sqlinsert = "insert into read_CR_comments([CommentId],[UserName]) ";
                    sqlinsert += " values(" + ds.Tables[0].Rows[i]["CommentId"].ToString() + ",'" + Session["admin"].ToString() + "')";

                    int intResult = DatabaseHelper.executeNonQuery(sqlinsert);
                }
                catch
                {
                }
            }
        }
    }

    protected void lnkUnansweredQuestions_Click(object sender, EventArgs e)
    {
        Session["filter"] = "Unanswered";
        Session["filterunread"] = "";
        Response.Redirect("tasks.aspx");
    }

    protected void lnkPr0Task_Click(object sender, EventArgs e)
    {

        Session["filter"] = "Immediate";
        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        Response.Redirect("tasks.aspx");
    }

    protected void lnkHighestTasks_Click(object sender, EventArgs e)
    {
        Session["filter"] = "1a - DO NOW";
        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        Response.Redirect("tasks.aspx");
    }

    protected void lnkHighTasks_Click(object sender, EventArgs e)
    {
        Session["filter"] = "HighTasks";
        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        Response.Redirect("tasks.aspx");
    }

    protected void LnkTskNewcomment_Click(object sender, EventArgs e)
    {

        Session["filterunread"] = "TaskNewComment";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        Response.Redirect("tasks.aspx");


    }
    protected void LnkMakeTaskcommentRead_Click(object sender, EventArgs e)
    {
        sql = @"select *  from task_comments where task_id in (select task_id  from tasks where tasks.task_id = task_comments.task_id and  tasks.assigned_to_user = '" + Session["admin"].ToString() + "'  and tasks.deleted <> 1 and tasks.status <> 'closed') and task_comments.tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 ";
        DataSet ds = DatabaseHelper.getDataset(sql);
        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {
                    string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                    sqlinsert += " values(" + ds.Tables[0].Rows[i]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                    int intResult = DatabaseHelper.executeNonQuery(sqlinsert);
                    string sqlinsert2 = "insert into read_task([task_id],[username]) ";
                    sqlinsert2 += " values(" + ds.Tables[0].Rows[i]["task_id"].ToString() + ",'" + Session["admin"].ToString() + "')";
                    int intResult2 = DatabaseHelper.executeNonQuery(sqlinsert2);
                }
                catch
                {
                }
            }


        }

        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        Response.Redirect("tasks.aspx", false);
    }

   
    protected void LnkAllTsknewComment_Click(object sender, EventArgs e)
    {
        Session["filterunread"] = "AllTaskNewComment";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        Response.Redirect("tasks.aspx");
    }
    protected void LnkMakeAllTaskCmmntRead_Click(object sender, EventArgs e)
    {


        sql = @"select *  from task_comments where task_id in (select task_id  from tasks where tasks.task_id = task_comments.task_id ) and task_comments.tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 ";
        DataSet ds = DatabaseHelper.getDataset(sql);
        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {

                    string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                    sqlinsert += " values(" + ds.Tables[0].Rows[i]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                    int intResult = DatabaseHelper.executeNonQuery(sqlinsert);
                    string sqlinsert2 = "insert into read_task([task_id],[username]) ";
                    sqlinsert2 += " values(" + ds.Tables[0].Rows[i]["task_id"].ToString() + ",'" + Session["admin"].ToString() + "')";
                    int intResult2 = DatabaseHelper.executeNonQuery(sqlinsert2);

                }
                catch
                {
                }
            }

        }
    }
        protected void CompanyNotes_Click(object sender, EventArgs e)
    {
        Session["TaskComp"] = hdnProjectCompany.Value;
        Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> window.open('companyNotes.aspx','','status=no,position=center,resizable=no,scrollbars=yes,maximize=yes'); </script>");
    }

    protected void CRNotes_Click(object sender, EventArgs e)
    {
        Session["TaskComp"] = hdnProjectCompany.Value;
        Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> window.open('CRNotes.aspx','','status=no,position=center,resizable=no,scrollbars=yes,maximize=yes'); </script>");
    }

    protected void BtnDelAllCmnt_Click(object sender, EventArgs e)
    {
        int i = 0;

        foreach (DataGridItem row in DataGrid1.Items)
        {
            try
            {
                CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                if (cb != null && cb.Checked)
                {
                    i = i + 1;
                    sql = "update ClientRequest_Details set deleted=1 Where [RequestId]=" + row.Cells[0].Text + " and [CommentId]=" + row.Cells[1].Text;

                    int intResult = DatabaseHelper.executeNonQuery(sql);


                }
            }
            catch { }
        }
        if (i > 0)
        {
            divMessage1.InnerHtml = i + " Comment(s) deleted.";
            divMessage1.Visible = true;
        }
        else
        {
            divMessage1.InnerHtml = "Please select comment to delete.";
            divMessage1.Visible = true;
        }

        BindData();
    }

    protected void BtnAnalytics_Click(object sender, EventArgs e)
    {
        sql = @"select *  from projects where project_name ='" + TaskProject.Value + "'";
        DataSet ds = DatabaseHelper.getDataset(sql);
        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            if (ds.Tables[0].Rows[0]["SEO"].ToString() == "True")
            {
                Session["GoogleUser"] = ds.Tables[0].Rows[0]["GoogleUserId"].ToString();
                Session["GooglePwd"] = ds.Tables[0].Rows[0]["GooglePwd"].ToString();
                Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> window.open('Analytics.aspx','','scrollbars=yes,width=470,height=500'); </script>");
             }
            else
            {
                BtnAnalytics.Attributes.Add("onclick", "return alert('Analytics  not added for this project.');");
            }

        }


    }

    protected void btnAllasRead_Click(object sender, EventArgs e)
    {

        int i = 0;
        foreach (DataGridItem row in DataGrid1.Items)
        {
            try
            {
                CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                if (cb != null && cb.Checked)
                {
                    i = i + 1;

                    sql = @"insert into read_CR_comments([CommentId],[UserName]) 
                        values(" + row.Cells[1].Text + ",'" + Session["admin"].ToString() + "')";
                    DatabaseHelper.executeNonQuery(sql);

                }
            }
            catch { }
        }

        chkSelectAll.Checked = false;


        if (i == 0)
        {
            divMessage1.InnerHtml = "There is no comment which is unread";
            divMessage1.Visible = true;
        }

        BindData();

    }
    protected void btnapprove_Click(object sender, EventArgs e)
    {
        insertbyAdmin();
    }


    void insertbyAdmin()
    {
        int RequestId = Convert.ToInt16(lblRequestId.Text);
        if (ftbComment.Content != "")
        {
            if (DrpSupport.SelectedItem.ToString() == "[None]")
            {
                if (Chkalias.Checked)
                {
                    ftbComment.Content = ftbComment.Content + "<br/><br/><span style=\"color: red;\">" + "Added by support<br/> Note to client: to optimise the request process please add requests directly into the change request system." + "</span>";
                }
                else
                {
                     ftbComment.Content = ftbComment.Content;
                }
            }
            else
            {
                if (Chkalias.Checked)
                {
                    ftbComment.Content = ftbComment.Content + "<br/><br/><span style=\"color: red;\">" + "Added by support (emailed to '" + DrpSupport.SelectedItem.ToString() + "')<br/> Note to client: to optimise the request process please add requests directly into the change request system." + "</span>";
                    
                }
                else
                {
                    ftbComment.Content = ftbComment.Content;
                    
                }
            }

        }
       
        string[] strArrayMacIDAndIP = new string[2];
        string strMacId = "";
        string strIpAddress = "";

        foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
        {
            if (nic.OperationalStatus == OperationalStatus.Up)
            {
                strMacId += nic.GetPhysicalAddress().ToString();
                break;
            }
        }

        strArrayMacIDAndIP[0] = strMacId;

        strIpAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (strIpAddress == null)

            strIpAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

        strArrayMacIDAndIP[1] = strIpAddress;
        
        string sql = "";
        
        if (ftbComment.Content.Trim() != "")
        
        {
            sql = " Update ClientRequest ";
            sql += " set WebsiteURL='" + txtWebsiteURL.Text.Trim().Replace("'", "''") + "', ";
            sql += " ShortDescr='" + txtShortDescr.Text.Trim().Replace("'", "''") + "', ";
            sql += " Status='" + drpStatus.SelectedValue.Replace("'", "''") + "', ";
            sql += " Priority=" + drpPriority.SelectedValue + ", ";
            
            if (Chkalias.Checked)
            {
                sql += "LastUpdatedBy='" + lblUserName.Text.Replace("'", "''") + "', ";
            }
            else
            {
                sql += "LastUpdatedBy='Support', ";
            }

            sql += " LastUpdatedOn=getdate(), ";
            sql += " CompanyName ='" + drpCompanyName.SelectedValue + "' ,";
            sql += " ProjectName ='" + drpProjects.SelectedValue + "',";
            sql += " OurStatus ='" + drpOurstatus.SelectedItem.Text.Replace("'", "''") + "' ";
            sql += " where RequestId=" + RequestId.ToString() + "; ";
            sql += "DECLARE @CommentId INT;";
            sql += " insert into ClientRequest_Details(RequestId,Comment,Ip,Macid,Approve,ApprovUnApprov,PostedBy,PostedOn) ";
            sql += " values(" + RequestId.ToString() + ",";
            
            sql += "'" + ftbComment.Content.ToString().Replace("'", "''") + "',";
            sql += "'" + strArrayMacIDAndIP[1] + "',";
            sql += "'" + strArrayMacIDAndIP[0] + "',";
            sql += "'True',";
            sql += "'UnApprove',";
            if (Chkalias.Checked)
            {
                sql += "'" + "Support on behalf of  " + lblUserName.Text.Replace("'", "''") + "',";
            }
            else
            {
                sql += "'Support',";
            }
            sql += "getdate()); ";
            sql += " SET @CommentId = SCOPE_IDENTITY(); ";
            sql += " insert into read_CR_comments([CommentId],[UserName]) ";
            sql += " values(@CommentId,'" + Session["admin"].ToString() + "')";
        }
        else
        {
            sql = " Update ClientRequest ";
            sql += " set WebsiteURL='" + txtWebsiteURL.Text.ToString().Trim().Replace("'", "''") + "', ";
            sql += "ShortDescr='" + txtShortDescr.Text.Trim().Replace("'", "''") + "', ";
            sql += "Status='" + drpStatus.SelectedValue.Replace("'", "''") + "', ";
            sql += "Priority=" + drpPriority.SelectedValue + ", ";
            sql += "UserName='" + DrpUserName.SelectedItem.Text + "', ";
            if (Chkalias.Checked)
            {
                sql += "LastUpdatedBy='" + lblUserName.Text.Replace("'", "''") + "', ";
            }
            else
            {
                sql += "LastUpdatedBy='Support', ";
            }
            sql += "LastUpdatedOn=getdate() ,";
            sql += "CompanyName ='" + drpCompanyName.SelectedValue + "',";
            sql += " ProjectName ='" + drpProjects.SelectedValue + "',";
            sql += " OurStatus ='" + drpOurstatus.SelectedItem.Text.Replace("'", "''") + "' ";
            sql += "where RequestId=" + RequestId.ToString() + "; ";
        }

        int intResult = DatabaseHelper.executeNonQuery(sql);

        if (intResult != 0)
        {
            if (btnUpdate.Text == "Mark as undeleted")
            {
                intResult = DatabaseHelper.executeNonQuery("update ClientRequest set deleted = 0 where RequestId=" + RequestId.ToString());
                task_action = "Support has restored deleted your Change Request  ";
                emailSubject = "CR: " + RequestId.ToString() + " was restored - " + txtShortDescr.Text.Trim() + " (CR ID:" + RequestId.ToString() + ")";
            }
            else
            {
                task_action = "Support has updated your Change Request  ";
                emailSubject = "CR: " + RequestId.ToString() + " was updated - " + txtShortDescr.Text.Trim() + " (CR ID:" + RequestId.ToString() + ")";
                DataSet dsUserDetails = DatabaseHelper.getUserDetails(lblClientId.Text, lblUserName.Text);
                Session["name"] = lblUserName.Text;
                bool flag = false;
                if (dsUserDetails == null || dsUserDetails.Tables.Count <= 0 || dsUserDetails.Tables[0].Rows.Count <= 0)
                {
                }
                else
                {
                    if (chkEmailnotify.Checked)
                    {
                       flag = DatabaseHelper.sendEmailChangeRequestNorply(dsUserDetails.Tables[0].Rows[0]["Email"].ToString(), emailSubject, generateEmail(RequestId));
                    }
                }
                if (chkEmailnotify.Checked)
                {
                    flag = DatabaseHelper.sendEmailChangeRequestNorply(ConfigurationManager.AppSettings["ToEmail1"].ToString(), emailSubject, generateAdminEmail(RequestId));
                }
                
            }
             Session["message"] = "CR was updated.";
             Session.Remove("Canned_msg");

            try
            {

                string sqlcomment = "select *  from ClientRequest_Details where [RequestId] = " + lblRequestId.Text + " and [CommentId] not in (select  CommentId  from read_CR_comments where read_CR_comments.username='" + Session["admin"].ToString() + "')";
                DataSet dscomment = DatabaseHelper.getDataset(sqlcomment);

                if (dscomment != null && dscomment.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dscomment.Tables[0].Rows.Count; i++)
                    {
                        try
                        {
                            string sqlinsert = "insert into read_CR_comments([CommentId],[UserName]) ";
                            sqlinsert += " values(" + dscomment.Tables[0].Rows[i]["CommentId"].ToString() + ",'" + Session["admin"].ToString() + "')";

                            int intCommentResult = DatabaseHelper.executeNonQuery(sqlinsert);
                        }
                        catch
                        {
                        }
                    }


                }
            }
            catch { }
            Response.Redirect("edit_request.aspx?reqid=" + RequestId.ToString());
            }
        else
        {
            divMessage1.InnerHtml = "Comment was not updated. Please try again.";
            divMessage1.Visible = true;
        }

    }



    private string generateAdminEmailNew(int RequestId)
    {
        DataSet dsRequests = DatabaseHelper.getClientRequest(RequestId);
        string strBody = "";
        if (dsRequests == null || dsRequests.Tables.Count <= 0 || dsRequests.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "Request not found.<br><br><a href='requests.aspx'>View requests</a>";
            divMessage.Visible = true;
        }
        else
        {
            strBody += "<b>" + DrpUserName.SelectedItem.ToString() + ":</b> <span style=\"color:red\">";
            strBody += "Our support team has added your Change Request " + "<span style=\"color:red\"> (number  </span>" + "<a style=\"color:red\" href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_request.aspx?reqid=" + RequestId + "'>" + RequestId + "</a>) </span> on  " + DateTime.Parse(dsRequests.Tables[0].Rows[0]["LastUpdatedOn"].ToString()).ToString("dd MMM h:mm tt") + "<br><br>";
            strBody += "<span style=\'font-size:11.0pt;font-family:Calibri,sans-serif;color:red\'>Click on the following link to access or amend the change request: " + "  " + "  <a href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_request.aspx?reqid=" + RequestId + "'>" + txtShortDescr.Text.Trim() + " . " + "</a>" + "   Do not start a new change request as this will slow the resolution" + "</span><br><br>";
            strBody += "Client: " + dsRequests.Tables[0].Rows[0]["UserName"].ToString() + "<br>";
            strBody += "Company: " + drpCompanyName.SelectedValue.ToString() + "<br>";
            strBody += "Reported on: " + DateTime.Parse(dsRequests.Tables[0].Rows[0]["RequestDate"].ToString()).ToString("dd MMM h:mm tt") + "<br>";
            strBody += "Title or preferably URL: <a href='" + dsRequests.Tables[0].Rows[0]["WebsiteURL"].ToString() + "' target='_blank'>" + dsRequests.Tables[0].Rows[0]["WebsiteURL"].ToString() + "</a><br>";
            strBody += "Status: (" + dsRequests.Tables[0].Rows[0]["priority"].ToString() + " : " + dsRequests.Tables[0].Rows[0]["status"].ToString() + ")";
            strBody += "<br><br>";
            DataSet dsReqDetails = DatabaseHelper.getClientRequestDetails(RequestId);
            strBody += "The progress of the change request can be seen below:" + "<br><br>";
            strBody += "<table cellspacing=\"0\" border=\"1\" style=\"width: 99%;border-collapse: collapse;\">";
            strBody += "<tbody>";
            for (int i = 0; i < dsReqDetails.Tables[0].Rows.Count; i++)
            {
                strBody += "<tr>";
                strBody += "<td>";
                strBody += "<div style=\"border: 1px none Green;\">";
                strBody += "<table width=\"100%\">";
                strBody += "<tbody>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span style=\"color: Green;\">comment " + dsReqDetails.Tables[0].Rows[i]["CommentId"].ToString() + " posted by " + dsReqDetails.Tables[0].Rows[i]["PostedBy"].ToString() + " on " + DateTime.Parse(dsReqDetails.Tables[0].Rows[i]["PostedOn"].ToString()).ToString("dd MMM h:mm tt") + "</span>";
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\"> </td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span>" + dsReqDetails.Tables[0].Rows[i]["comment"].ToString() + "</span>"; ////+ "<br/><br/><span style=\"color: red;\">" + "Added by support (emailed to 'michaelr@estatesolutions.co.uk') Note to client: to optimise the request process please add requests directly into the change request system." + "</span>"; 
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "</tbody>";
                strBody += "</table>";
                strBody += "</div>";
                strBody += "</td>";
                strBody += "</tr>";
            }
            strBody += "</tbody>";
            strBody += "</table>";
            strBody += "<br><span style=\'font-size:11.0pt;font-family:Calibri,sans-serif;color:red\'>Please do not reply to this email as it does not take replies. Please use the Change Request System as this is the quickest way to process your requests and also keeps a record of communications and progress for you to review at any time.</span><br/>";
            strBody += "<br>";
            strBody += "<br/><br/>" + "Thanks & Regards," + "<br/>Support.";
        }
        return strBody;
    }

    //private string generateClientEmailNew(int RequestId,string streUsername)
    //{
    //    DataSet dsRequests = DatabaseHelper.getClientRequest(RequestId);
    //    string strBody = "";
    //    if (dsRequests == null || dsRequests.Tables.Count <= 0 || dsRequests.Tables[0].Rows.Count <= 0)
    //    {
    //        divMessage.InnerHtml = "Request not found.<br><br><a href='requests.aspx'>View requests</a>";
    //        divMessage.Visible = true;
    //    }
    //    else
    //    {

    //        //strBody += "<b>" + DrpUserName.SelectedItem.ToString() + ":</b> <span style=\"color:red\">";
    //        strBody += "<b>" + streUsername + ":</b> <span style=\"color:red\">";
    //        strBody += "Our support team has added your Change Request " + "<span style=\"color:red\"> (number  </span>" + "<a style=\"color:red\" href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/edit_request.aspx?reqid=" + RequestId + "'>" + RequestId + "</a>) </span> on  " + DateTime.Parse(dsRequests.Tables[0].Rows[0]["LastUpdatedOn"].ToString()).ToString("dd MMM h:mm tt") + "<br><br>";
    //        strBody += "<span style=\'font-size:11.0pt;font-family:Calibri,sans-serif;color:red\'>Click on the following link to access or amend the change request: " + "  " + "  <a href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/edit_request.aspx?reqid=" + RequestId + "'>" + txtShortDescr.Text.Trim() + " . " + "</a>" + "   Do not start a new change request as this will slow the resolution" + "</span><br><br>";
    //        strBody += "Client: " + dsRequests.Tables[0].Rows[0]["UserName"].ToString() + "<br>";
    //        strBody += "Company: " + drpCompanyName.SelectedValue.ToString() + "<br>";
    //        strBody += "Reported on: " + DateTime.Parse(dsRequests.Tables[0].Rows[0]["RequestDate"].ToString()).ToString("dd MMM h:mm tt") + "<br>";
    //        strBody += "Title or preferably URL: <a href='" + dsRequests.Tables[0].Rows[0]["WebsiteURL"].ToString() + "' target='_blank'>" + dsRequests.Tables[0].Rows[0]["WebsiteURL"].ToString() + "</a><br>";
    //        strBody += "Status: (" + dsRequests.Tables[0].Rows[0]["priority"].ToString() + " : " + dsRequests.Tables[0].Rows[0]["status"].ToString() + ")";
    //        strBody += "<br><br>";
    //        DataSet dsReqDetails = DatabaseHelper.getClientRequestDetails(RequestId);
    //        strBody += "The progress of the change request can be seen below:" + "<br><br>";
    //        strBody += "<table cellspacing=\"0\" border=\"1\" style=\"width: 99%;border-collapse: collapse;\">";
    //        strBody += "<tbody>";
    //        for (int i = 0; i < dsReqDetails.Tables[0].Rows.Count; i++)
    //        {
    //            strBody += "<tr>";
    //            strBody += "<td>";
    //            strBody += "<div style=\"border: 1px none Green;\">";
    //            strBody += "<table width=\"100%\">";
    //            strBody += "<tbody>";
    //            strBody += "<tr>";
    //            strBody += "<td align=\"left\">";
    //            strBody += "<span style=\"color: Green;\">comment " + dsReqDetails.Tables[0].Rows[i]["CommentId"].ToString() + " posted by " + dsReqDetails.Tables[0].Rows[i]["PostedBy"].ToString() + " on " + DateTime.Parse(dsReqDetails.Tables[0].Rows[i]["PostedOn"].ToString()).ToString("dd MMM h:mm tt") + "</span>";
    //            strBody += "</td>";
    //            strBody += "</tr>";
    //            strBody += "<tr>";
    //            strBody += "<td align=\"left\"> </td>";
    //            strBody += "</tr>";
    //            strBody += "<tr>";
    //            strBody += "<td align=\"left\">";
    //            strBody += "<span>" + dsReqDetails.Tables[0].Rows[i]["comment"].ToString() + "</span>"; ////+ "<br/><br/><span style=\"color: red;\">" + "Added by support (emailed to 'michaelr@estatesolutions.co.uk') Note to client: to optimise the request process please add requests directly into the change request system." + "</span>"; 
    //            strBody += "</td>";
    //            strBody += "</tr>";
    //            strBody += "</tbody>";
    //            strBody += "</table>";
    //            strBody += "</div>";
    //            strBody += "</td>";
    //            strBody += "</tr>";
    //        }
    //        strBody += "</tbody>";
    //        strBody += "</table>";
    //        strBody += "<br><span style=\'font-size:11.0pt;font-family:Calibri,sans-serif;color:red\'>Please do not reply to this email as it does not take replies. Please use the Change Request System as this is the quickest way to process your requests and also keeps a record of communications and progress for you to review at any time.</span><br/>";
    //        strBody += "<br>";
    //        strBody += "<br/><br/>" + "Thanks & Regards," + "<br/>Support.";
    //    }
    //    return strBody;
    //}
    private string generateClientEmailNew(int RequestId)
    {
        DataSet dsRequests = DatabaseHelper.getClientRequest(RequestId);
        string strBody = "";
        if (dsRequests == null || dsRequests.Tables.Count <= 0 || dsRequests.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "Request not found.<br><br><a href='requests.aspx'>View requests</a>";
            divMessage.Visible = true;
        }
        else
        {



            strBody += "<b>" + DrpUserName.SelectedItem.ToString() + ":</b> <span style=\"color:red\">";

            //strBody += task_action;

            strBody += "Our support team has added your Change Request " + "<span style=\"color:red\"> (number  </span>" + "<a style=\"color:red\" href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/edit_request.aspx?reqid=" + RequestId + "'>" + RequestId + "</a>) </span> on  " + DateTime.Parse(dsRequests.Tables[0].Rows[0]["LastUpdatedOn"].ToString()).ToString("dd MMM h:mm tt") + "<br><br>";

            strBody += "<span style=\'font-size:11.0pt;font-family:Calibri,sans-serif;color:red\'>Click on the following link to access or amend the change request: " + "  " + "  <a href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/edit_request.aspx?reqid=" + RequestId + "'>" + txtShortDescr.Text.Trim() + " . " + "</a>" + "   Do not start a new change request as this will slow the resolution" + "</span><br><br>";
            strBody += "Client: " + dsRequests.Tables[0].Rows[0]["UserName"].ToString() + "<br>";
            strBody += "Company: " + drpCompanyName.SelectedValue.ToString() + "<br>";
            strBody += "Reported on: " + DateTime.Parse(dsRequests.Tables[0].Rows[0]["RequestDate"].ToString()).ToString("dd MMM h:mm tt") + "<br>";
            strBody += "Title or preferably URL: <a href='" + dsRequests.Tables[0].Rows[0]["WebsiteURL"].ToString() + "' target='_blank'>" + dsRequests.Tables[0].Rows[0]["WebsiteURL"].ToString() + "</a><br>";
            strBody += "Status: (" + dsRequests.Tables[0].Rows[0]["priority"].ToString() + " : " + dsRequests.Tables[0].Rows[0]["status"].ToString() + ")";

            strBody += "<br><br>";

            DataSet dsReqDetails = DatabaseHelper.getClientRequestDetails(RequestId);
            strBody += "The progress of the change request can be seen below:" + "<br><br>";

            strBody += "<table cellspacing=\"0\" border=\"1\" style=\"width: 99%;border-collapse: collapse;\">";
            strBody += "<tbody>";

            for (int i = 0; i < dsReqDetails.Tables[0].Rows.Count; i++)
            {
                strBody += "<tr>";
                strBody += "<td>";
                strBody += "<div style=\"border: 1px none Green;\">";
                strBody += "<table width=\"100%\">";
                strBody += "<tbody>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span style=\"color: Green;\">comment " + dsReqDetails.Tables[0].Rows[i]["CommentId"].ToString() + " posted by " + dsReqDetails.Tables[0].Rows[i]["PostedBy"].ToString() + " on " + DateTime.Parse(dsReqDetails.Tables[0].Rows[i]["PostedOn"].ToString()).ToString("dd MMM h:mm tt") + "</span>";
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\"> </td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span>" + dsReqDetails.Tables[0].Rows[i]["comment"].ToString() + "</span>"; ////+ "<br/><br/><span style=\"color: red;\">" + "Added by support (emailed to 'michaelr@estatesolutions.co.uk') Note to client: to optimise the request process please add requests directly into the change request system." + "</span>"; 
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "</tbody>";
                strBody += "</table>";
                strBody += "</div>";
                strBody += "</td>";
                strBody += "</tr>";
            }
            strBody += "</tbody>";
            strBody += "</table>";
            strBody += "<br><span style=\'font-size:11.0pt;font-family:Calibri,sans-serif;color:red\'>Please do not reply to this email as it does not take replies. Please use the Change Request System as this is the quickest way to process your requests and also keeps a record of communications and progress for you to review at any time.</span><br/>";
            strBody += "<br>";
            strBody += "<br/><br/>" + "Thanks & Regards," + "<br/>Support.";
        }
        return strBody;
    }


    void BindusercheckBox()
    {
        if (drpCompanyName.SelectedValue.ToString() != "")
        {
            sql = @"select * from NonesCRMusers where CompanyName = '" + drpCompanyName.SelectedValue + "' order by UserName;";
        }
        else
        {
            sql = @"select * from NonesCRMusers order by UserName;";
        }
        DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);
        if (ds_dropdowns.Tables[0].Rows.Count > 0)
        {
            chkboxclient.Visible = true;
            chkboxclient.DataSource = ds_dropdowns.Tables[0];
            chkboxclient.DataTextField = "UserName";
            chkboxclient.DataValueField = "Email";
            chkboxclient.DataBind();
            try{
            chkboxclient.Items.FindByText(DrpUserName.SelectedItem.Text).Selected = true; 
            }catch(Exception ex)
            {}

        }
        else
        {
            chkboxclient.Visible = false;

        }
    }



}
