<%@ Page Language="C#" AutoEventWireup="true" CodeFile="edit_request.aspx.cs" Inherits="Admin_edit_request" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="~/Admin/demoNotifications.ascx" TagName="Notifications" TagPrefix="uc4" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
    <title><%# ConfigurationManager.AppSettings["CompanyName"].ToString()%></title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
      <script type="text/javascript" language="javascript">

        function hide() {
            var l = document.getElementById("lnk_h_s");
            var h = document.getElementById("d_h_s");
            var i = document.getElementById("img1");


            if (l.innerHTML == "Add new comment") {
                l.innerHTML = "Close comment box"
                i.src = "../images/Minus.png"
                 h.style.display = "block";
              
            }
            else {
                l.innerHTML = "Add new comment"
                i.src = "../images/plus.png"
                 h.style.display = "none";
              
            }



        }
    </script>
      <script language="javascript" type="text/javascript">
        function beforeAddorRemove(flag) {
            if (document.getElementById("txtIRN").value == "") {
                alert("Please enter internal reference number.");
                return false;
            }

            if (flag == 1) {
                if (confirm('Are you sure you want to remove internal reference number?')) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }

    </script>
      <script language="javascript" type="text/javascript">

        //to check all comments
        function setCheckAll() {
            var chkboxLet = document.getElementById("chkSelectAll");
            var chkGridcontrols = document.getElementById("<%=DataGrid1.ClientID%>").getElementsByTagName("input");
            for (var i = 0; i < chkGridcontrols.length; i++) {
                chkGridcontrols[i].checked = chkboxLet.checked;
            }
            return true;
        }

        //to check whether any comment is checked to 'Mark Read'
        function ReadSomeComment() {
            var flag = false;
            var count = 0;
            var chkGridcontrols = document.getElementById("<%=DataGrid1.ClientID%>").getElementsByTagName("input");
            for (var i = 0; i < chkGridcontrols.length; i++) {
                if (chkGridcontrols[i].checked) {
                    count = 1;
                    flag = true;
                }
            }
            if (count == 0) {
                alert('Please select at least one comment')
                flag = false;
            }
            else
                flag = true;
            
            return flag;
        }

        //calling btnclick event of BtnDelAllCmnt through linkbtn 'lnkdelete'
        function DeletedBtn() {
            document.getElementById('<%=BtnDelAllCmnt.ClientID %>').click();
        }

        //calling btnclick event of btnAllasRead through 'Readlnk'
        function readBtn() {
            document.getElementById('<%=btnAllasRead.ClientID %>').click();
        }

        //to check whether any comment is checked to Delete
        function validateOnbtn() {
            var count = 0;
            var chkGridcontrols = document.getElementById("<%=DataGrid1.ClientID%>").getElementsByTagName("input");
            for (var i = 0; i < chkGridcontrols.length; i++) {
                if (chkGridcontrols[i].checked) {
                    count = 1;
                }
            }

            if (count > 0) {
                if (!confirm('Are you sure that you want to delete the selected comment(s)?'))
                    return false;
                else
                    return true;
            }
            else if (count == 0) {
                alert('Please select at least one comment')
                return false;
            }
        }

        //to uncheck 'check all' when any comment is unchecked
        function validate(sp){
           var chkGridcontrols = document.getElementById("<%=DataGrid1.ClientID%>").getElementsByTagName("input");
            for (var i = 0; i < chkGridcontrols.length; i++) {
           
                if (!chkGridcontrols[i].checked)
                {
                    document.getElementById("chkSelectAll").checked = false;
                    
                }
            }
        }
  
    </script>
      <script language="javascript" type="text/javascript">
        function enableBOB() {
            if (document.getElementById("Chkalias").checked) {
                var drpdwnStates = document.getElementById("DrpSupport");
                drpdwnStates.disabled = !drpdwnStates.disabled;
            }
            else {
                var drpdwnStates = document.getElementById("DrpSupport");
                drpdwnStates.disabled = !drpdwnStates.disabled;
                
            }

        }
    </script>
      <script language="javascript" type="text/javascript">
        function updateValues(longdesc) 
        {
            document.getElementById("ftbComment").value = longdesc;
        }
    function OnClientSubmit(editor)
        {
        //other possible values are All, ALL_NO_BRAKES, CSS, SPAN, FONT, WORD_ALL and WORD_NO_FONTS
         editor.fire("FormatStripper", { value: "WORD" });
         }
    
    </script>
    </telerik:RadCodeBlock>
    </head>
    <body onload="enableBOB();" style="background:">
    <form id="form1" runat="server">
      <div id="wrapper">
        <uc2:Admin_Header ID="Admin_Header1" runat="server" />
        <div id="Content" style="background:#ffffff;">
          <div id="Content_98">
            <div class="titleText" style="display: none;"> Edit change request</div>
            <div id="divMessage" runat="server" style="color: Red; font-weight: bold"> </div>
            <div>
              <uc4:Notifications ID="Notifications" runat="server" />
            </div>
            <div style="width: 100%; background: #ebebeb; padding: 5px 0; float: left;"> <span style="float: left;">&nbsp;&nbsp;<a href="add_changeRequest.aspx">Add change request</a>&nbsp;|&nbsp;<a
                        id="lnkCreateTask" href="edit_task.aspx" runat="server">Create new task</a></span><span
                            style="float: right;">&nbsp;&nbsp;
              <asp:Button Style="background:none;border:none" ID="LnkRelated" runat="server"
                                Text="Project related" OnClick="LnkRelated_Click" CausesValidation="False" CssClass="text_button_trans" />
              &nbsp;&nbsp;
              <asp:Button Style="background:none;border:none" ID="Lnkcomprelated" runat="server" Text="Company related" OnClick="Lnkcomprelated_Click"
                                    CssClass="text_button_trans"/>
              &nbsp;&nbsp;
              <asp:Button Style="background:none;border:none" ID="CompanyNotes" runat="server" Text="FHS notes" OnClick="CompanyNotes_Click"
                                CausesValidation="False" CssClass="text_button_trans"/>
              &nbsp;&nbsp;
              <asp:Button Style="background:none;border:none" ID="CRNotes" runat="server" Text="Client notes" OnClick="CRNotes_Click" CausesValidation="False"
                                    CssClass="text_button_trans" />
              <asp:Button ID="BtnAnalytics" runat="server"
                                        Text="Analytics" BorderStyle="none" BackColor="transparent" CssClass="text_button_trans"
                                        Style="cursor: pointer" CausesValidation="False" OnClick="BtnAnalytics_Click" />
              </span> </div>
            <div style="clear: both"> </div>
            <div id="divRequest" runat="server" class="divBorder">
              <table width="100%" border="0" cellspacing="0" cellpadding="5" style="border:1px solid #999999;">
                <tr>
                  <td height="22" align="left" valign="top" bgcolor="#282c5f" class="whitetext2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                      <td align="left" bgcolor="#282C5F"><asp:Label ID="lblLastUpdated" runat="server" Style="color: White; font-size: 14px"></asp:Label></td>
                      <td align="right" valign="middle" bgcolor="#282c5f" class="whitetext2"><asp:Label ID="lblIRN" runat="server"></asp:Label>
                          &nbsp;&nbsp;<span style="color: #E0BC14">Request
                        ID:
                        <asp:Label ID="lblRequestId" runat="server" Font-Bold="True"></asp:Label>
                        </span></td>
                    </tr>
                    </table></td>
                </tr>
                <tr>
                  <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                      <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="3">
                      
                          <tr>
                          <td width="16%" align="left" valign="bottom"> Status </td>
                          <td width="17%" align="left" valign="bottom"><span >Our status&nbsp; </span></td>
                           <td width="12%" align="left" valign="bottom"><span style=" width: 175px">Priority</span></td>
                          <td width="15%" align="left" valign="bottom">Internal reference number</td>
                         
                          <td width="12%" align="left" valign="bottom"> Subscription</td>
                          <td align="left" valign="bottom"></td>
                          <td width="318" align="left" valign="bottom"></td>
                        </tr>
                          <tr valign="top">
                          <td width="16%" height="10" align="left"><asp:DropDownList ID="drpStatus" runat="server" Width="228px"> </asp:DropDownList></td>
                          <td width="17%"><asp:DropDownList ID="drpOurstatus" runat="server" Width="228px"> </asp:DropDownList></td>
                          <td><asp:DropDownList ID="drpPriority" runat="server" Width="175px">
                              <asp:ListItem Value="6">[no priority]</asp:ListItem>
                              <asp:ListItem Value="0">0 - IMMEDIATE</asp:ListItem>
                              <asp:ListItem Value="1">1 - highest</asp:ListItem>
                              <asp:ListItem Value="8">1c</asp:ListItem>
                              <asp:ListItem Value="2">2</asp:ListItem>
                              <asp:ListItem Value="3">3</asp:ListItem>
                              <asp:ListItem Value="4">4</asp:ListItem>
                              <asp:ListItem Value="5">5 - lowest</asp:ListItem>
                            </asp:DropDownList></td>
                          <td width="15%"><asp:TextBox ID="txtIRN" runat="server" MaxLength="10" Width="200px"></asp:TextBox></td>
                          
                          <td><span id="TDSubscription" runat="server" style="color: Red; font-weight: bold;"> </span></td>
                          <td></td>
                          <td><table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                              
                                <td align="left" valign="top" width="30%">&nbsp;</td>
                                <td width="15%" align="left" valign="top"></td>
                                <td width="25%" align="left" valign="top"><asp:Button ID="btnAdd" runat="server" Text=" Add " CausesValidation="False" OnClick="btnAdd_Click"
                                                                        OnClientClick="javascript:return beforeAddorRemove(0);" CssClass="blueBtns" />
                                  &nbsp; </td>
                                <td width="10%" align="right" valign="top"><asp:Button ID="btnRemove" runat="server" Text="Remove" CausesValidation="False"
                                                                        OnClick="btnRemove_Click" OnClientClick="javascript:return beforeAddorRemove(1);"
                                                                        CssClass="blueBtns" /></td>
                              </tr>
                            </table></td>
                        </tr>
                          <!--<tr>
                          <td align="left" valign="top"></td>
                          <td></td>
                          <td width="1%" align="left" valign="top"></td>
                          <td align="left" valign="top"></td>
                          <td align="left" valign="top" style="padding-left: 10px; width: 19px;"></td>
                          <td align="left" valign="top">&nbsp;</td>
                        </tr>-->
                          
                        </table></td>
                    </tr>
                      <tr>
                      <td align="left" valign="top"><asp:Label ID="lblUserName" runat="server" Visible="False"></asp:Label>
                          <asp:Label ID="lblClientId" runat="server" Visible="False"></asp:Label></td>
                    </tr>
                      <tr>
                      <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="3">
                          <tr>
                          <td align="left" valign="bottom"> Short description </td>
                          <td align="left" valign="bottom"> URL if relevant (Website address i.e. www.ourwebsite.com/contactus.html)</td>
                        </tr>
                          <tr>
                          <td width="50%" align="left" valign="top"><asp:TextBox ID="txtShortDescr" runat="server" Width="97%"></asp:TextBox>
                              <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtShortDescr"
                                                            ErrorMessage="Please enter short description.">*</asp:RequiredFieldValidator>
                              <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" TargetControlID="RequiredFieldValidator2"> </cc1:ValidatorCalloutExtender></td>
                          <td width="50%" align="left" valign="top"><asp:TextBox ID="txtWebsiteURL" runat="server" Width="99%"></asp:TextBox></td>
                        </tr>
                          <!--<tr>
                          <td align="left" valign="top" height="10"></td>
                          <td align="left" valign="top"></td>
                        </tr>-->
                        </table></td>
                    </tr>
                      <tr>
                      <td align="left" valign="top"><table width="68%" border="0" cellspacing="0" cellpadding="3">
                          <tr>
                          <td width="15%" align="left" valign="bottom"><span>Company name </span></td>
                          <td width="15%" align="left" valign="bottom"><span>Project name </span></td>
                          <td width="15%" align="left" valign="bottom"><span >User name</span></td>
                          <td width="12%" align="left" valign="bottom"><span>OBO</span></td>
                          <td width="12%" align="left" valign="bottom"></td>
                        </tr>
                          <tr>
                          <td width="15%" align="left" valign="top"><asp:DropDownList ID="drpCompanyName" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpCompanyName_SelectedIndexChanged"
                                                            Width="228px"> </asp:DropDownList></td>
                        <td width="15%" align="left" valign="top"><asp:DropDownList ID="drpProjects" runat="server" AutoPostBack="True" CssClass="filerDrpodown"
                                                            Width="228px"> </asp:DropDownList>
                              <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="drpProjects"
                                                            ErrorMessage="Please select project name.">*</asp:RequiredFieldValidator>
                              <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator4"> </cc1:ValidatorCalloutExtender></td>
                          <td width="15%" align="left" valign="top"><asp:DropDownList ID="DrpUserName" runat="server" Width="200px"> </asp:DropDownList></td>
                          <td width="12%" align="left" valign="top"><asp:DropDownList ID="DrpSupport" runat="server" Width="200px">
                              <asp:ListItem>[None]</asp:ListItem>
                              <asp:ListItem Value="michaelr@estatesolutions.co.uk">michaelr@estatesolutions.co.uk</asp:ListItem>
                              <asp:ListItem Value="michaelr@123sitesolutions.com">michaelr@123sitesolutions.com</asp:ListItem>
                              <asp:ListItem Value="mail@estatesolutions.co.uk">mail@estatesolutions.co.uk</asp:ListItem>
                              <asp:ListItem Value="michaelr@emailsetc.co.uk">michaelr@emailsetc.co.uk</asp:ListItem>
                              <asp:ListItem Value="simonl@emailsetc.co.uk">simonl@emailsetc.co.uk</asp:ListItem>
                              <asp:ListItem Value="support@emailsetc.co.uk">support@emailsetc.co.uk</asp:ListItem>
                              <asp:ListItem Value="support@123sitesolutions.com">support@123sitesolutions.com</asp:ListItem>
                              <asp:ListItem Value="support@estatesolutions.co.uk">support@estatesolutions.co.uk</asp:ListItem>
                              <asp:ListItem Value="support@4saleorlet.co.uk">support@4saleorlet.co.uk</asp:ListItem>
                            </asp:DropDownList></td>
                          <td width="10%" align="center" valign="top"><asp:CheckBox ID="Chkalias" runat="server" Text="Add as alias" onclick="enableBOB();"
                                                            Font-Bold="True" Width="101px" /></td>
                        </tr>
                        </table></td>
                    </tr>
                      <tr>
                      <td align="left" valign="top">&nbsp;</td>
                    </tr>
                      <tr>
                      <td align="left" valign="top"><a href="#" id="CannedMessage" runat="server"></a></td>
                    </tr>
                      <tr>
                      <td align="left" valign="middle"><div style="height: 22px; background-color: rgb(40, 44, 95); padding-top: 5px; padding-left: 6px;">
                          <asp:ImageButton ID="img1" runat="server" src="../images/plus.png" Style="border-width: 0px;
                                                    margin-top: -2px;" OnClientClick="hide();return false;" />
                          <asp:LinkButton ID="lnk_h_s" runat="server" OnClientClick="hide();return false;"
                                                    CssClass="whitetext2" Style="text-decoration: none; vertical-align: top;">Add new comment</asp:LinkButton>
                          <asp:LinkButton ID="LinkButton2" runat="server" OnClientClick="hide();return false;"
                                                    Style="text-decoration: none; vertical-align: top; padding-left: 85%; color: rgb(40, 44, 95)"></asp:LinkButton>
                        </div>
                        
                        
                        
                          <div id="d_h_s" runat="server" style="display:none" >
                          <div align="left" valign="top" style="height: 22px; color: Black"> Comment:&nbsp; <a href="#" id="A1" onclick="window.open('Customer_Support.aspx','','status=yes,width=760,height=550')"> Canned message&nbsp;</a> </div>
                          <%--*****************************************************************************************************--%>
                          <telerik:RadEditor ID="ftbComment" OnClientSubmit="OnClientSubmit" runat="server" Height="300px" Width="100%"> <Snippets>
                            <telerik:EditorSnippet Name="Product Template" Value="<table><tr><td><div style='width:300px;border:2px outset #D8D2BD;padding:34px;font-family:Arial, Verdana; font-size:11px;background- color:#F1EFE6;'>Dear ____________________,<br>Thank you for inquiring about ____________. Your request will be processed in 48 hours and shipped at the address you have provided.<b>Please, contact us if you have any problems.</b></div></td></tr></table>"> </telerik:EditorSnippet>
                            <telerik:EditorSnippet Name="Signature" Value="<strong>John Smith</strong><br>Sales Manager<br>"> </telerik:EditorSnippet>
                            </Snippets>
                              <Links>
                              <telerik:EditorLink Name="Telerik" Href="http://www.telerik.com">
                              <telerik:EditorLink Name="Telerik Products" Href="http://www.telerik.com/products"
                                                    Target="_blank" />
                              <telerik:EditorLink Name="Telerik Support Center" Href="http://www.telerik.com/support/home.aspx"
                                                    Target="_blank" />
                              <telerik:EditorLink Name="Telerik Community" Href="http://www.telerik.com/community/home.aspx"
                                                    Target="_blank" ToolTip="Telerik Community" />
                              </telerik:EditorLink>
                            </Links>
                              <Tools> </Tools>
                              <ImageManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                              <MediaManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                              <FlashManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                              <TemplateManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                              <DocumentManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                              <Modules>
                              <telerik:EditorModule Name="RadEditorStatistics" Visible="False" Enabled="true" />
                              <telerik:EditorModule Name="RadEditorDomInspector" Visible="False" Enabled="true" />
                              <telerik:EditorModule Name="RadEditorNodeInspector" Visible="False" Enabled="true" />
                              <telerik:EditorModule Name="RadEditorHtmlInspector" Visible="False" Enabled="true" />
                            </Modules>
                              <Content> </Content>
                            </telerik:RadEditor>
                          <%--*****************************************************************************************************--%>
                        </div>
                        
                          </td>
                          </tr>
                          <tr>
                          <td align="left" valign="top"><br />
                          
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                        
                          
                            <td width="25%"  valign="top">
                              <span style="display:block; margin-top:5px;"><a id="lnkSendAttachment" href="#" runat="server">Upload attachment</a> &nbsp;&nbsp <a id="lnkViewAttachment" href="#" runat="server">View attachments</a></span>
                              <span id="divMessage1" runat="server" style="color: Red" visible="false"> </span></td>
                              
                               <td width="35%" align="left">   <asp:CheckBoxList ID="chkboxclient" runat="server" AutoPostBack="true" RepeatDirection="Horizontal">
                                                        </asp:CheckBoxList></td>
                              
                              <td align="right" valign="top">
                              <span>
                              <asp:CheckBox  Checked="true" ID="chkEmailnotify" runat="server" Font-Bold="True" Text="Email notify" />
                              &nbsp;
                              <asp:Button ID="btnUpdate" runat="server" Text="Update for approval" OnClick="btnUpdate_Click"  CssClass="blueBtns" />
                              &nbsp;
                              
                              <asp:Button id="btnapprove" runat="server" Text="Update as approved" CssClass="blueBtns" OnClick="btnapprove_Click" />
                              &nbsp;
                              <asp:Button ID="btnDelete" runat="server" Text="Delete" OnClick="btnDelete_Click"
                              OnClientClick="javascript:return confirm('Are you sure you want to delele change request?');" CssClass="blueBtns" />
                              </span>
                              </td>
                            </tr>
                        </table>
                        
                        
                     </td>
                    </tr>
                    </table>
                    
                    
                    
                 </td>
                </tr>
              </table>
            </div>
            <br />
            <div id="divRequestDetails" runat="server" style="width: 100%; background:#FFF">
              <table id="tblreadDel" runat="server" width="100%">
                <tr>
                  <td align="left" style="height: 26px">&nbsp;&nbsp;
                    <asp:CheckBox ID="chkSelectAll" runat="server" onclick="return setCheckAll();" Text=" Check All" style="margin-left:-9px;" />
                    &nbsp;&nbsp;
                    <div id="allread" runat="server" style="display:none;">
                      <asp:Button ID="btnAllasRead" runat="server" Text="Mark checked as read" OnClick="btnAllasRead_Click" CssClass="blueBtns" OnClientClick= "return ReadSomeComment();"    />
                    </div>
                    <a id="Readlnk" href="#"  onclick="readBtn();" >Mark checked as read</a> &nbsp;&nbsp;
                    &nbsp;&nbsp;
                    <div id="someDelete" runat="server" style="display:none;">
                      <asp:Button ID="BtnDelAllCmnt" runat="server" Text="Delete checked" OnClick="BtnDelAllCmnt_Click" OnClientClick="return validateOnbtn();"  CssClass="blueBtns"/>
                    </div>
                    <asp:LinkButton ID="lnkdelete" runat="server" CausesValidation="false" Text="Delete checked" OnClientClick="DeletedBtn();" ></asp:LinkButton></td>
                </tr>
              </table>
              <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" Width="100%"
                        ShowHeader="False" OnItemDataBound="DataGrid1_ItemDataBound" OnItemCommand="DataGrid1_ItemCommand"
                        Style="margin: 5px 0 0 0;">
                <Columns>
                <asp:BoundColumn DataField="RequestId" Visible="False"></asp:BoundColumn>
                <asp:BoundColumn DataField="CommentId" Visible="False"></asp:BoundColumn>
                <asp:BoundColumn DataField="PostedBy" Visible="False"></asp:BoundColumn>
                <asp:BoundColumn DataField="PostedOn" Visible="False"></asp:BoundColumn>
                <asp:TemplateColumn>
                  <ItemTemplate>
                    <div style="border-top: 1px; border-left: 1px; border-bottom: 1px; border-right: 1px;
                                        border-color: Green;">
                      <table width="100%">
                        <tr>
                          <td align="left"><table width="100%" cellpadding="0" cellspacing="0" border="0">
                              <tr>
                              <td width="90%"><asp:CheckBox ID="chkSelect" runat="server" onclick="validate(this);" />
                                  &nbsp;
                                  <asp:Label ID="lblPosted" runat="server" ForeColor="Green"></asp:Label>
                                  &nbsp;&nbsp;
                                  <%--<asp:LinkButton ID="lnkReadMark" runat="server" CommandName="ReadMark">(Unread. Mark as read)</asp:LinkButton>--%>
                                  <asp:Button ID="lnkReadMark" runat="server" CommandName="ReadMark" Text="(Unread. Mark as read)"
                                                                                        Style="background: none; border: none" />
                                  <%--************************************************************************************--%>
                                  <asp:Label ID="lblipAdd" runat="server" Text="IP:"></asp:Label>
                                  <asp:Label ID="lblid" runat="server" Text='<%#bind("Ip") %>'></asp:Label>
                                  <asp:Label ID="lblMacipAdd" runat="server" Text="MacId:"></asp:Label>
                                  <asp:Label ID="lblmacid" runat="server" Text='<%#bind("Macid") %>'></asp:Label>
                                  <asp:Button ID="ApprovUnapprov" runat="server" CommandName="ApprovMark" Text='<%#bind("ApprovUnApprov") %>' Style="background: none; border: none" />
                                  <%--************************************************************************************--%></td>
                              <td align="right" width="5%"><asp:HiddenField ID="hdn_commandId" runat="server" Value='<%# Eval("CommentId")%>' />
                                  <asp:Button ID="lnkCreatenewtask" runat="server" CausesValidation="False" CommandName="Create_new_task"
                                                                    BackColor="transparent" BorderStyle="none" ForeColor="#282C5F" Text="Create new task" /></td>
                              <td align="right" width="5%"><asp:Button ID="lnkDelete" runat="server" CausesValidation="False" CommandName="delete"
                               BackColor="transparent" BorderStyle="none" ForeColor="#282C5F" Text="delete"/></td>
                            </tr>
                            </table></td>
                        </tr>
                        <tr>
                          <td align="left"></td>
                        </tr>
                        <tr>
                          <td align="left"><asp:Label ID="lblComment" runat="server" Text='<%# bind("Comment") %>'></asp:Label></td>
                        </tr>
                      </table>
                    </div>
                  </ItemTemplate>
                </asp:TemplateColumn>
                </Columns>
              </asp:DataGrid>
            </div>
          </div>
          <asp:Panel ID="Panel2" runat="server" Width="755px" Style="display: none">
            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="background-color: #ffffff;
                    float: left">
              <tr>
                <td align="right" style="padding: 5px;"><asp:LinkButton ID="LinkButton1" runat="server" Style="color: #811007; font-size: 16px;
                                font-weight: bolder; text-decoration: underline;" OnClick="lnkClose2_Click"><img src="../images/close_icon.png" width="16" height="16" border="0" /></asp:LinkButton></td>
              </tr>
              <tr>
                <td align="center" valign="top"><iframe id="Customersupport" src="Customer_Support.aspx" height="570"; width="760" frameborder="0" scrolling="yes" ></iframe></td>
              </tr>
            </table>
            </asp:Panel>
          <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="CannedMessage"
                PopupControlID="Panel1" DropShadow="true" CancelControlID="lnkClose2" BackgroundCssClass="modalBackground"> </cc1:ModalPopupExtender>
          <asp:Button Style="display: none" ID="Button1" runat="server" Text="Button" CausesValidation="false"> </asp:Button>
        </div>
        <asp:Panel ID="Panel1" runat="server" Width="755px" Style="display: none">
          <table border="0" cellspacing="0" cellpadding="0" width="100%" style="background-color: #ffffff;
                float: left">
            <tr>
              <td align="right" style="padding-right: 10px;"><asp:LinkButton ID="lnkClose2" runat="server" Style="color: #811007; font-size: 16px;
                            font-weight: bolder; text-decoration: underline;" OnClick="lnkClose2_Click">Close</asp:LinkButton></td>
            </tr>
            <tr>
              <td align="center" valign="top"></td>
            </tr>
          </table>
          </asp:Panel>
        <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="CannedMessage"
            PopupControlID="Panel1" DropShadow="true" CancelControlID="lnkClose2" BackgroundCssClass="modalBackground"> </cc1:ModalPopupExtender>
        <asp:Button Style="display: none" ID="blnClick" runat="server" Text="Button" CausesValidation="false"> </asp:Button>
      </div>
      <uc1:footer ID="Footer1" runat="server" />
      <asp:HiddenField ID="prevWebsiteURL" runat="server" />
      <asp:HiddenField ID="TaskProject" runat="server" />
      <asp:HiddenField ID="prevShortDescr" runat="server" />
      <asp:HiddenField ID="prevStatus" runat="server" />
      <asp:HiddenField ID="prevPriority" runat="server" />
      <asp:HiddenField ID="hdnProjectCompany" runat="server" />
    </form>
</body>
</html>
