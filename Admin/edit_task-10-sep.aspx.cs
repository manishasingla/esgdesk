using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.Drawing.Drawing2D;
using System.IO;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Net;
using System.Net.NetworkInformation;
//using System.Windows.Forms;


public partial class Admin_edit_task_ten_sep : System.Web.UI.Page
{
    int id;
    int reqid;
    string sql = "";
    string task_action = "";
    string emailSubject = "";
    bool deletedFlag = false;
    bool flagEmailNotification = false;
    protected static string taskCompany;
    protected static string TskID;
    //public static string FTPid;
    public string FTPid;
    public static string ProjectId_Client;
    protected static string strresource;
    int etcMins = 0;


    static ArrayList arr1aAssignedUsers = new ArrayList();
    static ArrayList arrImmidateUsers = new ArrayList();
    private void Page_Init(object sender, System.EventArgs e)
    {
     //   this.EnableViewState = false;
      //  lblComment.EnableViewState = false;
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Header.DataBind();


        btnOK.Attributes.Add("onClick", "return checkedOkConfirm();");
        btnCancel.Attributes.Add("onClick", "return checkedCancelConfirm();");
        drpStatuses.Attributes.Add("onchange", "return GetStatus();");
        if (Request.QueryString.Get("id") != null && Request.QueryString.Get("id") != null)
        {
            hfurl.Value = Request.QueryString.Get("id").ToString();
            lbl_AssignedTo.Visible = true;
        }
        else
        {
            d_h_s.Style.Add(HtmlTextWriterStyle.Display, "block");
            lnk_h_s.Text = "Close comment box";
            img1.ImageUrl = "../images/Minus.png";

            lhk_h.Text = "Close attributes";
            img.ImageUrl = "../images/Minus.png";
            t_h_s.Style.Add(HtmlTextWriterStyle.Display, "block");
            lbl_AssignedTo.Visible = false;

        }
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = (Request.QueryString["id"] == null || Request.QueryString["id"].ToString() == "" ? "edit_task.aspx" : "edit_task.aspx?id=" + Request.QueryString["id"].ToString());

            if (ftbComment.Content.Trim() != "")
            {
                Session["comment"] = ftbComment.Content;
            }

            Response.Redirect("login.aspx");
            return;
        }

        divMessage1.InnerHtml = "";
        divMessage.InnerHtml = "";

        if (Session["Canned_msg"] == null || Session["Canned_msg"].ToString() == "")
        {
        }
        else
        {
            Session.Remove("Canned_msg");
        }

        string task_id = Request["id"];
        if (task_id == null || task_id == "0" || task_id == "")
        {
            id = 0;
            LnkIRTN.Visible = false;
            LnkAskQuestion.Visible = false;
        }
        else
        {
            if (DatabaseHelper.is_int(task_id))
            {
                id = Convert.ToInt32(task_id);
                LnkAskQuestion.HRef = "task_Question.aspx?id=" + id.ToString();
                LnkIRTN.HRef = "Add_Task.aspx?IrtID=" + id.ToString();
                LnkAskQuestion.Visible = true;
                LnkIRTN.Visible = true;

            }
            else
            {
                // Display an error because the bugid must be an integer
                divMessage.InnerHtml = "Task ID must be an integer.";
                divMessage.InnerHtml += "<p><a href='tasks.aspx'>View tasks</a></p>";
                DivEntry.Visible = false;
                return;
            }
        }

        string req_id = Request["reqid"];
        if (req_id == null || req_id == "0" || req_id == "")
        {
            reqid = 0;
            lnkBackToRequest.Visible = false;

        }
        else
        {
            if (DatabaseHelper.is_int(req_id))
            {
                reqid = Convert.ToInt32(req_id);
                lnkBackToRequest.HRef = "edit_request.aspx?reqid=" + reqid.ToString();
                lnkBackToRequest.Visible = true;
            }
            else
            {
                // Display an error because the bugid must be an integer
                divMessage.InnerHtml = "<br>Change request ID must be an integer.";
                divMessage.InnerHtml += "<p><a href='client_requests.aspx'>View change requests</a></p>";
                DivEntry.Visible = false;
                return;
            }
        }

        if (!Page.IsPostBack)
        {
            //*************Read all Comment*************************************************************************
            string readcmmtsql;
            DataSet dsreadcmmt;
            try
            {

                readcmmtsql = @"select tc_id from task_comments where task_id =" + Convert.ToInt32(task_id) + " and  tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "')";
                /// sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";
                dsreadcmmt = DatabaseHelper.getDataset(readcmmtsql);
                if (dsreadcmmt != null && dsreadcmmt.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsreadcmmt.Tables[0].Rows.Count; i++)
                    {
                        try
                        {
                            string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                            sqlinsert += " values(" + dsreadcmmt.Tables[0].Rows[i]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                            int intCommentResult = DatabaseHelper.executeNonQuery(sqlinsert);

                        }
                        catch
                        {
                        }
                    }

                }
            }
            catch { }
            //******************************************************************************************************

            chkShowAllHistory.Attributes.Add("onclick", "checkChecked();");
            lnkNudge.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want send Nudge to task owner?')");
            //========Add New Code==================================
            load_dropdowns();
            Bindmailcheckboxlist(Convert.ToInt32(task_id));
            //======================================================
            set_default_selection();



            if (Session["Task_Canned_msg"] == null || Session["Task_Canned_msg"].ToString() == "")
            {
                ftbComment.Content = "";
            }
            else
            {

                try
                {
                    if (Session["Task_ShrtDescCanned_msg"] == null || Session["Task_ShrtDescCanned_msg"].ToString() == "")
                    {
                        txtShortDescr.Text = "";

                    }
                    else
                    {
                        txtShortDescr.Text = Session["Task_ShrtDescCanned_msg"].ToString();
                    }
                }

                catch { }
                //===================================================
                //open the blocked of the Canned message
                //===================================================
                d_h_s.Style["display"] = "block";
                //===================================================
                //===================================================
                ftbComment.Content = Session["Task_Canned_msg"].ToString();
                //===================================================
                //Set the Canned message null
                //===================================================
                Session["Task_Canned_msg"] = null;
                //===================================================
                //===================================================
            }

            if (id == 0)
            {
                if (reqid != 0)
                {
                    sql = "\nSelect * from ClientRequest where RequestId=" + reqid.ToString() + ";";

                    sql += "\nSelect * from ClientRequest_Details where RequestId=" + reqid.ToString() + " order by CommentId desc";

                    DataSet dsClientRequest = DatabaseHelper.getDataset(sql);

                    // change request
                    if (dsClientRequest.Tables[0].Rows.Count > 0)
                    {
                        try
                        {
                            txtShortDescr.Text = dsClientRequest.Tables[0].Rows[0]["ShortDescr"].ToString();
                            txtRelevantURL.Text = dsClientRequest.Tables[0].Rows[0]["WebsiteURL"].ToString();
                            lblProject.Text = dsClientRequest.Tables[0].Rows[0]["ProjectName"].ToString();
                            hdnProjectCompany.Value = dsClientRequest.Tables[0].Rows[0]["CompanyName"].ToString();

                            try
                            {
                                drpProjects.SelectedValue = drpProjects.Items.FindByText(dsClientRequest.Tables[0].Rows[0]["ProjectName"].ToString()).Value;
                            }
                            catch { }
                        }
                        catch { }


                    }
                    else
                    {
                        divMessage.InnerHtml = "<br>Request ID not found.";
                        divMessage.InnerHtml += "<p><a href='client_requests.aspx'>View change requests</a></p>";
                        DivEntry.Visible = false;
                        lnkBackToRequest.Visible = false;
                        return;
                    }

                    // merge change request comments
                    if (dsClientRequest.Tables[1].Rows.Count > 0)
                    {
                        ftbComment.Content = "<span style=\"font-style:italic;\">" + "The following has been created from a Change Request added by a client. The words or some of the words are those of the client and therefore may not have been checked or amended for clarity." + "</span>" + "<br/><br/>";
                        if (Request.QueryString.Get("cid") != null && Request.QueryString.Get("cid") != "")
                        {
                            for (int i = 0; i < dsClientRequest.Tables[1].Rows.Count; i++)
                            {
                                if (dsClientRequest.Tables[1].Rows[i]["CommentId"].ToString() == Request.QueryString.Get("cid"))
                                {
                                    ftbComment.Content += dsClientRequest.Tables[1].Rows[i]["Comment"].ToString() + "<br/><hr/>";
                                    ftbComment.Content += "<span " + "style=" + "Color:red" + ">This exchange was created by Support from comments which were added to an existing exchange. To avoid confusion, please ensure you add a new exchange rather than add comments to an existing exchange unless they are directly related.</span>";
                                }
                            }
                        }
                        else
                        {
                            for (int i = 0; i < dsClientRequest.Tables[1].Rows.Count; i++)
                            {
                                ftbComment.Content += dsClientRequest.Tables[1].Rows[i]["Comment"].ToString() + "<br/><hr/><br/>";
                            }
                        }
                    }

                    lnkSendAttachment.HRef = "upload_attachment.aspx?reqid=" + reqid.ToString();
                    lnkViewAttachment.HRef = "view_attachments.aspx?reqid=" + reqid.ToString();
                    DataSet dsTotalAttachments = DatabaseHelper.getAttachments(reqid, "");
                    lnkViewAttachment.InnerHtml = "View attachments (" + dsTotalAttachments.Tables[0].Rows.Count + ")";
                }

                divAttachments.Visible = false;
                chkQuestion.Visible = false;

                //===========================================================================
                BtnDelAllCmnt.Visible = false;
                //===========================================================================
                btnUpdate.Text = "Create";
                btnDelete.Visible = false;
                lblTaskId.Text = "New";
                lblProject.Text = drpProjects.SelectedItem.Text;
                lblAssignedTo.Text = drpUsers.SelectedItem.Text;
                //lbl_AssignedTo.Text = "<a href=EmployeesReports.aspx?id=" + drpUsers.SelectedItem.Text + "target=_blank>" + drpUsers.SelectedItem.Text + "</a>";
                lbl_AssignedTo.Text = "<a href=Priority_reports.aspx?Ename=" + drpUsers.SelectedItem.Text + "target=_blank>" + drpUsers.SelectedItem.Text + "</a>";
                DivStartdateCal.Visible = true;
                DivDuedateCal.Visible = true;
                DivStartdateLbl.Visible = false;
                DivDuedateLbl.Visible = false;

            }
            else
            {
                // Get this entry's data from the db and fill in the form

                divAttachments.Visible = true;
                lnkSubscribers.HRef = "view_subscribers.aspx?id=" + id.ToString();
                lnkSubscribers.Visible = true;
                lnkCreateNewCR.HRef = "add_changeRequest.aspx?id=" + id.ToString();
                lnkHrsReport.HRef = "report_task_hrs.aspx?id=" + id.ToString();
                lnkHrsReport.Visible = true;
                lnkNudge.Visible = true;
                DivStartdateCal.Visible = false;
                DivDuedateCal.Visible = false;
                DivStartdateLbl.Visible = true;
                DivDuedateLbl.Visible = true;
                lblTaskId.Text = id.ToString();
                TskID = id.ToString();
                /// LnkRstStartDate.Attributes.Add()
                LnkRstStartDate.Attributes.Add("onclick", "window.open('Edit_OverdueDate.aspx?id=" + id.ToString() + "','','scrollbars=no,width=922,height=590');");
                LnkDueDate.Attributes.Add("onclick", "window.open('Edit_OverdueDate.aspx?id=" + id.ToString() + "','','scrollbars=no,width=922,height=590');");
                BindData();


            }

            if (!DatabaseHelper.can_Delete_Tasks(Session["admin"].ToString()))
            {
                btnDelete.Visible = false;
            }


            getprojectid(drpProjects.SelectedItem.Text);

            if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
            {
                LnkRelated.Visible = false;
                Lnkcomprelated.Visible = false;
                CompanyNotes.Visible = true;
                CRNotes.Visible = false;
                lnkCreateNewCR.Visible = false;
                BtnAnalytics.Visible = false;
                LnkRstStartDate.Visible = false;
                LnkDueDate.Visible = false;
                lblamend.HRef = "";
                lblamend.Target = "";
            }
            else
            {
                LnkRelated.Visible = true;
                Lnkcomprelated.Visible = true;
                CompanyNotes.Visible = true;
                CRNotes.Visible = true;
                lnkCreateNewCR.Visible = true;
                BtnAnalytics.Visible = true;
                LnkRstStartDate.Visible = true;
                LnkDueDate.Visible = true;

            }
        }

        //===============================
        register1aTaskAllocated();
        registerImmiAllocated();
        //===============================
        ensureCurrentlyWorkingOnStatus();

        if (Session["message"] != null)
        {
            divMessage1.InnerHtml = Session["message"].ToString();
            Session.Remove("message");
        }

        if (Session["comment"] != null)
        {
            ftbComment.Content = Session["comment"].ToString();
            Session.Remove("comment");
        }
        //============================================================================================================
        string strCNotes = "window.open('companyNotes.aspx?CompName=" + hdnProjectCompany.Value + "','_blank');";
        CompanyNotes.Attributes.Add("onClick", strCNotes);
        string strCRNotes = "window.open('CRNotes.aspx?CompName=" + hdnProjectCompany.Value + "','_blank');";
        CRNotes.Attributes.Add("onClick", strCRNotes);
        //============================================================================================================
    }
    void getprojectid(string ProjectName)
    {
        sql = @"select project_id, project_name
		from projects
		where project_name ='" + ProjectName + "'";

        DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);
        if (ds_dropdowns.Tables[0] != null)
        {
            lblamend.HRef = "edit_project.aspx?id=" + ds_dropdowns.Tables[0].Rows[0]["project_id"].ToString();
            FTPid = ds_dropdowns.Tables[0].Rows[0]["project_id"].ToString();
            ProjectId_Client = ds_dropdowns.Tables[0].Rows[0]["project_id"].ToString();
        }
    }

    protected void BtnOverdue_Click(object sender, EventArgs e)
    {

        Session["filter"] = " ";
        Session["filterunread"] = "";
        Session["OverdueTask"] = "True";
        Response.Redirect("tasks.aspx");
    }

    protected void Lnk1cTasks_Click(object sender, EventArgs e)
    {
        Session["filter"] = "1c - normal";
        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        Response.Redirect("tasks.aspx");
    }

    protected void LnkNotification_Click(object sender, EventArgs e)
    {
        Response.Redirect("Notification.aspx", false);
    }
    public bool ShowIsPrivateTask()
    {
        //===============================================================================================================
        //=======================================Show this task to previously assigned user==============================
        bool isPrivate = false;
        string strIsPrivate = " Select distinct Username from task_updates where task_id = " + lblTaskId.Text.Trim();
        strIsPrivate += " Union ";
        strIsPrivate += " Select distinct Username from task_subscriptions where task_id= " + lblTaskId.Text.Trim();
        strIsPrivate += " Union ";
        strIsPrivate += " Select distinct username from users where See_all_private_tasks= 'Y'";
        DataSet dsPrivateUser = DatabaseHelper.getDataset(strIsPrivate);
        if (dsPrivateUser != null && dsPrivateUser.Tables.Count > 0 && dsPrivateUser.Tables[0].Rows.Count > 0)
        {
            for (int iCout = 0; iCout < dsPrivateUser.Tables[0].Rows.Count; iCout++)
            {
                if (Session["admin"].ToString().ToUpper().Trim() == dsPrivateUser.Tables[0].Rows[iCout]["username"].ToString().ToUpper().Trim())
                {
                    isPrivate = true;
                    return isPrivate;
                }
            }
        }
        else
        {
            return isPrivate;
        }
        return isPrivate;
        //=================================================================================================================
        //=================================================================================================================
    }
    public void BindData()
    {
        //======================= Check this task is a Private Task or Not===========================
        //*****************************************************************************************
        //=========================================================================================
        string sqlprivate;
        sqlprivate = "select Isprivate from tasks where task_id = " + lblTaskId.Text.Trim();
        DataSet dsIsuser = DatabaseHelper.getDataset(sqlprivate);
        if (dsIsuser != null && dsIsuser.Tables.Count > 0 && dsIsuser.Tables[0].Rows.Count > 0)
        {
            if (dsIsuser.Tables[0].Rows[0]["Isprivate"] != null && (dsIsuser.Tables[0].Rows[0]["Isprivate"].ToString() != "") && Convert.ToBoolean(dsIsuser.Tables[0].Rows[0]["Isprivate"]) == true)
            {
                if (ShowIsPrivateTask() == false)
                {
                    divMessage.InnerHtml = "<br>You are not authorized to view this task.<br><br><a href='tasks.aspx'>View other tasks</a>";
                    DivEntry.Visible = false;
                    divTaskDetails.Visible = false;
                    return;
                }
            }
        }

        string strsqlpri;
        strsqlpri = "select Isprivate from tasks where task_id = " + lblTaskId.Text.Trim();
        DataSet dsIsuserPri = DatabaseHelper.getDataset(strsqlpri);
        if (dsIsuserPri != null && dsIsuserPri.Tables.Count > 0 && dsIsuserPri.Tables[0].Rows.Count > 0)
        {
            if (dsIsuser.Tables[0].Rows[0]["Isprivate"] != null && (dsIsuser.Tables[0].Rows[0]["Isprivate"].ToString() != "") && Convert.ToBoolean(dsIsuser.Tables[0].Rows[0]["Isprivate"]) == true)
            {
                checkisPrivate.Checked = true;
            }
            else
            {
                checkisPrivate.Checked = false;
            }
        }
        //=========================================================================================
        //*****************************************************************************************
        //=========================================================================================
        sql = "select * from tasks where task_id = " + lblTaskId.Text.Trim();

        DataSet dsTask = DatabaseHelper.getDataset(sql);

        if (dsTask == null || dsTask.Tables.Count <= 0 || dsTask.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "<br>Task not found.<br><br><a href='tasks.aspx'>View tasks</a>";
            DivEntry.Visible = false;
            divTaskDetails.Visible = false;
        }
        else
        {
            if (dsTask.Tables[0].Rows[0]["deleted"].ToString() == "1")
            {
                if (!DatabaseHelper.can_Show_Deleted_Tasks(Session["admin"].ToString()))
                {
                    divMessage.InnerHtml = "<br>Task not found.<br><br><a href='tasks.aspx'>View tasks</a>";
                    DivEntry.Visible = false;
                    divTaskDetails.Visible = false;
                    return;
                }
                else
                {
                    btnUpdate.Text = "Mark as undeleted";
                    btnDelete.Text = "Delete permanently";
                }

                deletedFlag = true;
            }
            else
            {
                deletedFlag = false;
            }

            lblTaskId.Text = dsTask.Tables[0].Rows[0]["task_id"].ToString();



            if (dsTask.Tables[0].Rows[0]["RequestId"].ToString() != "0")
            {
                lblIRN.Text = "Linked ref CR: <a href='edit_request.aspx?reqid=" + dsTask.Tables[0].Rows[0]["RequestId"].ToString() + "'>" + dsTask.Tables[0].Rows[0]["RequestId"].ToString() + "</a>";
                string sqlCR = "select RequestId from ClientRequest where RequestId = " + dsTask.Tables[0].Rows[0]["RequestId"].ToString() + " and Status  <> 'closed' and deleted <> 1 ";
                object ObjCRstatus = DatabaseHelper.executeScalar(sqlCR);
                if (ObjCRstatus != null)
                {
                    btnDelete.Attributes.Add("onclick", "javascript:return confirm('Please note that this task has a RELATED CHANGE REQUEST which is not yet closed or deleted. Should it still be deleted??');");

                }

                else
                {
                    btnDelete.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete task?');");
                }

            }
            else
            {
                lblIRN.Text = "";
                btnDelete.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete task?');");
            }

            if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
            {
                lblIRN.Visible = false;
            }

            try
            {


                lblIRTN.Text = "";

                sql = "select task_id from tasks where Internalref=" + dsTask.Tables[0].Rows[0]["task_id"].ToString() + " and  deleted <> 1 order by task_id";

                DataSet dsTaskId = DatabaseHelper.getDataset(sql);

                if (dsTaskId == null || dsTaskId.Tables.Count <= 0 || dsTaskId.Tables[0].Rows.Count <= 0)
                {
                    lblIRTN.Text = " ";
                }
                else
                {
                    string strIRN = "";
                    for (int i = 0; i < dsTaskId.Tables[0].Rows.Count; i++)
                    {
                        strIRN += "<a target='_blank' href=edit_task.aspx?id=" + dsTaskId.Tables[0].Rows[i][0] + ">" + dsTaskId.Tables[0].Rows[i][0] + "</a>&nbsp;&nbsp;";
                    }

                    lblIRTN.Text = " Linked ref task: " + strIRN;
                }

            }
            catch { }

            string sqlResources = "select Resources from Resources where status = 'to check'";
            object ObjResources = DatabaseHelper.executeScalar(sqlResources);
            if (ObjResources != null)
            {


                FtbResources.Text = ObjResources.ToString();

            }
            else
            {
                strresource = "";
            }

            try
            {


                lblLastUpdated.Text = " Last changed by <b>" + dsTask.Tables[0].Rows[0]["last_updated_user"].ToString() + "</b> on <b>" + DateTime.Parse(dsTask.Tables[0].Rows[0]["last_updated_date"].ToString()).ToString("dd MMM yyyy h:mm tt") + "</b>";
                lblReportedBy.Text = "Reported by <b>" + dsTask.Tables[0].Rows[0]["reported_user"].ToString() + "</b> on <b>" + DateTime.Parse(dsTask.Tables[0].Rows[0]["reported_date"].ToString()).ToString("dd MMM yyyy h:mm tt") + "</b>";
                //==================================================================================================
                //**************************************************************************************************
                //==================================================================================================
                string strsql = "select Anyone,Emptask_assign,FHSLead,UK from NonesCRMusers where ProjectName = '" + dsTask.Tables[0].Rows[0]["project"].ToString() + "'";
                DataSet dsProjectby = DatabaseHelper.getDataset(strsql);
                if (dsProjectby != null && dsProjectby.Tables.Count > 0 && dsProjectby.Tables[0].Rows.Count > 0)
                {
                    indicator.Visible = true;
                    lblanyone.Text = dsProjectby.Tables[0].Rows[0]["Anyone"].ToString();
                    lblemp.Text = dsProjectby.Tables[0].Rows[0]["Emptask_assign"].ToString();
                    lblfhslead.Text = dsProjectby.Tables[0].Rows[0]["FHSLead"].ToString();
                    lbluk.Text = dsProjectby.Tables[0].Rows[0]["UK"].ToString();

                }
                else
                {
                    indicator.Visible = false;

                }


                //==================================================================================================
                //**************************************************************************************************
                //==================================================================================================
                lblProject.Text = "<a href=projectrequests.aspx?projectname=" + Server.UrlEncode(dsTask.Tables[0].Rows[0]["project"].ToString()) + " target=_blank>" + dsTask.Tables[0].Rows[0]["project"].ToString() + "</a>";
                lblAssignedTo.Text = dsTask.Tables[0].Rows[0]["assigned_to_user"].ToString();
                lbl_AssignedTo.Text = "<a href=Priority_reports.aspx?Ename=" + dsTask.Tables[0].Rows[0]["assigned_to_user"].ToString() + " target=_blank>" + dsTask.Tables[0].Rows[0]["assigned_to_user"].ToString() + "</a>";
                lbl_status.Text = dsTask.Tables[0].Rows[0]["status"].ToString();
                lbl_Shortdescription.Text = dsTask.Tables[0].Rows[0]["short_desc"].ToString();

                string[] values1 = dsTask.Tables[0].Rows[0]["Relevant_URL"].ToString().Split('\r');

                string sqlRelevanturlT = "select [Relevanturl] from [projects]  where [project_name] = '" + dsTask.Tables[0].Rows[0]["project"].ToString() + "'";
                object RelevantUrlProject = DatabaseHelper.executeScalar(sqlRelevanturlT);
                string[] values2 = RelevantUrlProject.ToString().Split('\r');
                string[] All = new string[values1.Length + values2.Length];
                string[] R_url = new string[values1.Length + values2.Length];
                Array.Copy(values1, All, values1.Length);
                Array.Copy(values2, 0, All, values1.Length, values2.Length);
                string test = "";
                for (int j = 0; All.Length > j; j++)
                {
                    R_url[j] = All[j].Replace("\n", "");
                }
                R_url = RemoveDuplicatesValue(R_url);
                for (int i = 0; R_url.Length > i; i++)
                {
                    string url = R_url[i];
                    string http = R_url[i].ToString().Replace("http://", "");
                    string https = R_url[i].ToString().Replace("https://", "");
                    if (url != http)
                    {
                        if (R_url[i] != "")
                        {
                            R_url[i] = http;
                            R_url[i] = R_url[i].Trim();
                            R_url[i] = "http://" + R_url[i];
                        }
                    }
                    if (url != https)
                    {
                        if (R_url[i] != "")
                        {
                            R_url[i] = https;
                            R_url[i] = R_url[i].Trim();
                            R_url[i] = "https://" + R_url[i];
                        }
                    }
                    else
                    {
                        if (R_url[i] != "")
                        {
                            R_url[i] = http;
                            R_url[i] = R_url[i].Trim();
                            R_url[i] = "http://" + R_url[i];
                        }
                    }

                    if (i == 0)
                    {

                        lbl_RelevantURL.Text = "<a href=" + R_url[i] + " + target=" + "blank" + ">" + R_url[i] + "</a>";
                    }
                    else
                    {
                        R_url[i].Replace("\n", "");
                        R_url[i] = R_url[i].Trim();

                        lbl_RelevantURL.Text += "<br>" + "<a href=" + R_url[i] + " + target=" + "blank" + ">" + R_url[i] + "</a>";
                    }
                }


                lbl_Projecttype.Text = dsTask.Tables[0].Rows[0]["project_type"].ToString();
                lbl_Priority.Text = dsTask.Tables[0].Rows[0]["priority"].ToString();
                lbl_Category.Text = dsTask.Tables[0].Rows[0]["category"].ToString();
                try
                {
                    if (dsTask.Tables[0].Rows[0]["StartDate"] != null && dsTask.Tables[0].Rows[0]["StartDate"].ToString() != "")
                    {
                        lbl_Startdate.Text = DateTime.Parse(dsTask.Tables[0].Rows[0]["StartDate"].ToString()).ToString("ddd  dd MMM yyyy");

                    }
                    else
                    {
                        lbl_Startdate.Text = "Not set";

                    }
                }
                catch
                {


                    lbl_Startdate.Text = "Not set";

                }
                try
                {
                    if (dsTask.Tables[0].Rows[0]["DueDate"] != null && dsTask.Tables[0].Rows[0]["DueDate"].ToString() != "")
                    {
                        lbl_Duedate.Text = DateTime.Parse(dsTask.Tables[0].Rows[0]["DueDate"].ToString()).ToString("ddd  dd MMM yyyy");

                    }
                    else
                    {
                        lbl_Duedate.Text = "Not set";

                    }
                }
                catch
                {

                    lbl_Duedate.Text = "Not set";
                }
                try
                {
                    drpProjects.SelectedValue = drpProjects.Items.FindByText(dsTask.Tables[0].Rows[0]["project"].ToString()).Value;
                }
                catch { }
                try
                {
                    drpUsers.SelectedValue = drpUsers.Items.FindByText(dsTask.Tables[0].Rows[0]["assigned_to_user"].ToString()).Value;
                }
                catch { }
                drpStatuses.SelectedValue = drpStatuses.Items.FindByText(dsTask.Tables[0].Rows[0]["status"].ToString()).Value;
                drpCategories.SelectedValue = drpCategories.Items.FindByText(dsTask.Tables[0].Rows[0]["category"].ToString()).Value;
                drpPriorities.SelectedValue = drpPriorities.Items.FindByText(dsTask.Tables[0].Rows[0]["priority"].ToString()).Value;
            }
            catch { }

            try
            {
                drpProjectType.SelectedValue = drpProjectType.Items.FindByText(dsTask.Tables[0].Rows[0]["project_type"].ToString()).Value;
            }
            catch { }
            txtShortDescr.Text = dsTask.Tables[0].Rows[0]["short_desc"].ToString();
            txtRelevantURL.Text = dsTask.Tables[0].Rows[0]["Relevant_URL"].ToString();

            try
            {
                prevProject.Value = drpProjects.Items.FindByText(dsTask.Tables[0].Rows[0]["project"].ToString()).Value;
            }
            catch { }
            TaskProject.Value = dsTask.Tables[0].Rows[0]["project"].ToString();
            //==================================================================================================================
            //prevAssignedTo.Value = drpUsers.Items.FindByText(dsTask.Tables[0].Rows[0]["assigned_to_user"].ToString()).Value;
            if (dsTask.Tables[0].Rows[0]["assigned_to_user"] != null && dsTask.Tables[0].Rows[0]["assigned_to_user"].ToString() != "")
            {
                try
                {
                    prevAssignedTo.Value = drpUsers.Items.FindByText(dsTask.Tables[0].Rows[0]["assigned_to_user"].ToString()).Value;
                }
                catch (Exception ex)
                {

                    string sqlAssigntoUser = "update tasks set assigned_to_user='Rohit' where task_id= " + lblTaskId.Text.Trim() + ";";
                    int testAssigntoUser = DatabaseHelper.executeNonQuery(sqlAssigntoUser);
                    prevAssignedTo.Value = drpUsers.Items.FindByText("Rohit").Value;
                    drpUsers.SelectedValue = drpUsers.Items.FindByText("Rohit").Value;
                    DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error:in assigned to user set Rohit Sir " + "DateTime : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:" + ex.Message + "\n" + " Source:" + ex.Source + "\n" + " InnerException:" + ex.InnerException + " Stack Trace: " + ex.StackTrace + "\n" + " Task Id " + lblTaskId.Text);
                    lbl_AssignedTo.Text = "<a href=Priority_reports.aspx?Ename=" + "Rohit" + " target=_blank>" + "Rohit" + "</a>";
                }
            }

            //==================================================================================================================


            prevStatus.Value = drpStatuses.Items.FindByText(dsTask.Tables[0].Rows[0]["status"].ToString()).Value;
            prevCategory.Value = drpCategories.Items.FindByText(dsTask.Tables[0].Rows[0]["category"].ToString()).Value;
            prevPriority.Value = drpPriorities.Items.FindByText(dsTask.Tables[0].Rows[0]["priority"].ToString()).Value;
            prevProjectType.Value = drpProjectType.Items.FindByText(dsTask.Tables[0].Rows[0]["project_type"].ToString()).Value;
            prevShortDescr.Value = dsTask.Tables[0].Rows[0]["short_desc"].ToString();
            prevRelevantUrl.Value = dsTask.Tables[0].Rows[0]["Relevant_URL"].ToString();
            prevETC.Value = dsTask.Tables[0].Rows[0]["ETC"].ToString();
            try
            {
                if (dsTask.Tables[0].Rows[0]["StartDate"] != null && dsTask.Tables[0].Rows[0]["StartDate"].ToString() != "")
                {
                    lblStartdate.Text = DateTime.Parse(dsTask.Tables[0].Rows[0]["StartDate"].ToString()).ToString("ddd  dd MMM yyyy");

                }
                else
                {
                    lblStartdate.Text = "Not set";

                }
            }
            catch
            {


                lblStartdate.Text = "Not set";

            }
            try
            {
                if (dsTask.Tables[0].Rows[0]["DueDate"] != null && dsTask.Tables[0].Rows[0]["DueDate"].ToString() != "")
                {
                    lblDueDate.Text = DateTime.Parse(dsTask.Tables[0].Rows[0]["DueDate"].ToString()).ToString("ddd  dd MMM yyyy");
                }
                else
                {
                    lblDueDate.Text = "Not set";

                }
            }
            catch
            {

                lblDueDate.Text = "Not set";
            }
            hdnProjectCompany.Value = "";
            string sqlcompany = "select [CompanyName] from [projects]  where [project_name] = '" + dsTask.Tables[0].Rows[0]["project"].ToString() + "'";
            object objResult = DatabaseHelper.executeScalar(sqlcompany);

            if (objResult != null)
            {
                hdnProjectCompany.Value = objResult.ToString();
                taskCompany = objResult.ToString();
            }
            else
            {
                hdnProjectCompany.Value = dsTask.Tables[0].Rows[0]["Company"].ToString();
                taskCompany = dsTask.Tables[0].Rows[0]["Company"].ToString();
            }

            try
            {

                string sqlSEO = "select [SEO] from [projects]  where [project_name] = '" + dsTask.Tables[0].Rows[0]["project"].ToString() + "'";
                object objResultSEO = DatabaseHelper.executeScalar(sqlSEO);

                if (objResultSEO.ToString() == "True")
                {
                    BtnAnalytics.ForeColor = Color.Red;
                }
                else
                {
                    BtnAnalytics.ForeColor = Color.Blue;
                    //BtnAnalytics.Attributes.Add("onclick", "return alert('Please contact support as your subscription level does not allow to view analytics.');");
                }


            }
            catch { }


            string sqlcompanynote = "select count(*) from [Company_notes]  where Company_Name = '" + dsTask.Tables[0].Rows[0]["Company"].ToString() + "'and Note_Type ='FHS' and Allow_Notes='True'";
            int objNoteResult = int.Parse(DatabaseHelper.executeScalar(sqlcompanynote).ToString());
            try
            {
                if (objNoteResult > 0)
                {
                    //CompanyNotes.ForeColor = Color.Red;
                    CompanyNotes.Style.Add("color", "#FF0000");

                }
                else
                {
                    //CompanyNotes.ForeColor = Color.Blue;
                    CompanyNotes.Style.Add("color", "#0033ff");

                }
            }
            catch { }
            string sqlCRnote = "select count(*) from [Company_notes]  where Company_Name = '" + dsTask.Tables[0].Rows[0]["Company"].ToString() + "' and Note_Type ='Client'  and Allow_Notes='True'";
            int objCRNoteResult = int.Parse(DatabaseHelper.executeScalar(sqlCRnote).ToString());

            try
            {
                if (objCRNoteResult > 0)
                {
                    //CRNotes.ForeColor = Color.Red;
                    CRNotes.Style.Add("color", "#FF0000");

                }
                else
                {
                    //CRNotes.ForeColor = Color.Blue;
                    CRNotes.Style.Add("color", "#0033ff");
                }
            }
            catch { }


            try
            {
                int etcMins = Convert.ToInt32(prevETC.Value);
                int hrs = etcMins / 60;
                int mins = etcMins % 60;

                drpHrs.SelectedValue = hrs.ToString();
                drpMins.SelectedValue = mins.ToString();
            }
            catch { }

            try
            {
                int ETC = Convert.ToInt32(dsTask.Tables[0].Rows[0]["ETC"].ToString());

                int hrsTaken = getHoursTakenSoFar(lblTaskId.Text.Trim());

                lblTotalHrs.Text = "Total hours so far: <b>" + (hrsTaken / 60) + " hrs " + (hrsTaken % 60) + " mins</b>";

                int hrsLeft = ETC - hrsTaken;

                if (hrsLeft < 0)
                {
                    hrsLeft = -1 * hrsLeft;
                    lblHrsLeft.Text = "Expected hrs left : <b>-" + (hrsLeft / 60) + " hrs " + (hrsLeft % 60) + " mins</b>";
                }
                else
                {
                    lblHrsLeft.Text = "Expected hrs left : <b>" + (hrsLeft / 60) + " hrs " + (hrsLeft % 60) + " mins</b>";
                }
                lblratiotime.Text = "<b>" + (hrsTaken / 60) + "'" + (hrsTaken % 60) + "''" + "(" + ETC / 60 + "'" + (ETC % 60) + "'')</b>";
            }
            catch { }
            //=================================================================================================================
            //lnkSendAttachment.HRef = "TriggerUpload.aspx?id=" + dsTask.Tables[0].Rows[0]["task_id"].ToString();
            lnkSendAttachment.HRef = "upload_task_attachment.aspx?id=" + dsTask.Tables[0].Rows[0]["task_id"].ToString();

            //==================================================================================================================
            lnkViewAttachment.HRef = "task_attachments.aspx?id=" + dsTask.Tables[0].Rows[0]["task_id"].ToString();
            DataSet dsTotalAttachments = DatabaseHelper.getTaskAttachments(Convert.ToInt32(dsTask.Tables[0].Rows[0]["task_id"].ToString()), "");
            lnkViewAttachment.InnerHtml = "View attachments (" + dsTotalAttachments.Tables[0].Rows.Count + ")";
            lnkCreateNewCR.HRef = "add_changeRequest.aspx?id=" + dsTask.Tables[0].Rows[0]["task_id"].ToString();
            if (ChkIncludeRead.Checked)
            {
                sql = "select * from task_comments where deleted <> 1 and  task_id = " + lblTaskId.Text.Trim() + " order by tc_id desc";
            }
            else
            {
                sql = "select * from task_comments where deleted <> 1 and  task_id = " + lblTaskId.Text.Trim() + " and tc_id not in(select tc_id from read_comments where username ='" + Session["admin"].ToString() + "') order by tc_id desc";
            }

            DataSet dsTaskDetails = DatabaseHelper.getDataset(sql);

            if (dsTaskDetails == null || dsTaskDetails.Tables.Count <= 0 || dsTaskDetails.Tables[0].Rows.Count <= 0)
            {
                BtnDelAllCmnt.Visible = false;

            }
            else
            {
                BtnDelAllCmnt.Visible = true;

            }
            DataGrid1.DataSource = dsTaskDetails.Tables[0];
            DataGrid1.DataBind();

            //=======================================================================================================
            //sql = "select * from task_comments where task_id = " + lblTaskId.Text.Trim() + " order by tc_id desc";

            //DataSet dsTaskDetails1 = DatabaseHelper.getDataset(sql);

            //dsTaskDetails1.Tables[0].Columns.Add("temp");

            //for (int i = 0; i < dsTaskDetails1.Tables[0].Rows.Count; i++)
            //{
            //    dsTaskDetails1.Tables[0].Rows[i]["temp"] = "C";
            //}


            sql = "select *,'C' as temp from task_comments where task_id = " + lblTaskId.Text.Trim() + " order by tc_id desc";

            DataSet dsTaskDetails1 = DatabaseHelper.getDataset(sql);
            
            //========================================================================================================
            //sql = "select * from task_updates where task_id = " + lblTaskId.Text.Trim() + " order by tc_id desc";

            //DataSet dsTaskUpdates = DatabaseHelper.getDataset(sql);

            //System.Type myDataType;
            //myDataType = System.Type.GetType("System.Int32");
            //dsTaskUpdates.Tables[0].Columns.Add("deleted", myDataType);
            //dsTaskUpdates.Tables[0].Columns.Add("temp");

            //for (int i = 0; i < dsTaskUpdates.Tables[0].Rows.Count; i++)
            //{
            //    dsTaskUpdates.Tables[0].Rows[i]["deleted"] = 0;
            //    dsTaskUpdates.Tables[0].Rows[i]["temp"] = "U";
            //}

            sql = "select *,Convert(int,0) as deleted,'U' as temp from task_updates where task_id = " + lblTaskId.Text.Trim() + " order by tc_id desc";

            DataSet dsTaskUpdates = DatabaseHelper.getDataset(sql);
            //========================================================================================================
            dsTaskDetails1.Merge(dsTaskUpdates);


            DataView view = new DataView(dsTaskDetails1.Tables[0]);
            view.Sort = "post_date Desc, temp desc ";

            DataGrid2.DataSource = view;
            DataGrid2.DataBind();

            if (chkShowAllHistory.Checked == true)
            {
                divAllHistory.Style.Add("display", "block");
                divNoHistory.Style.Add("display", "none");
            }
            else
            {
                divAllHistory.Style.Add("display", "none");
                divNoHistory.Style.Add("display", "block");
            }
        }
    }

    public string[] RemoveDuplicatesValue(string[] myList)
    {

        System.Collections.ArrayList newList = new System.Collections.ArrayList();

        foreach (string str in myList)

            if (!newList.Contains(str))

                newList.Add(str);

        return (string[])newList.ToArray(typeof(string));

    }

    bool ReturnValue()
    {

        return false;

    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        //***************************************************************
        string[] strArrayMacIDAndIP = new string[2];
        string strMacId = "";
        string strIpAddress = "";

        foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
        {
            if (nic.OperationalStatus == OperationalStatus.Up)
            {
                strMacId += nic.GetPhysicalAddress().ToString();
                break;
            }
        }

        strArrayMacIDAndIP[0] = strMacId;

        strIpAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (strIpAddress == null)

            strIpAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

        strArrayMacIDAndIP[1] = strIpAddress;
        //*****************************************************************

        try
        {
            etcMins = Convert.ToInt32(drpHrs.SelectedValue) * 60 + Convert.ToInt32(drpMins.SelectedValue);
        }
        catch { }

        if (hdnTocheckStatus.Value == "False")
        {
            return;
        }

        //==================================================================
        //Check 
        //==================================================================

        if (drpPriorities.SelectedItem.Text.ToString() == "1a - DO NOW") 
        {
            if (drpStatuses.SelectedItem.Text.ToString() != "to check")
            {
                if (drpStatuses.SelectedItem.Text.ToString() != "Checked")
                {
                    if (drpStatuses.SelectedItem.Text.ToString() != "parked")
                    {
                        if (drpStatuses.SelectedItem.Text.ToString() != "closed")
                        {
                            if (drpStatuses.SelectedItem.Text.ToString() != "re-opened")
                            {
                                if (drpStatuses.SelectedItem.Text.ToString() != "CWO")
                                {
                                    if (drpStatuses.SelectedItem.Text.ToString() != "CWO but away")
                                    {
                                        string sqlchkdonow = "";
                                        sqlchkdonow = "select task_id from tasks where status <> 'closed' and status <> 'Checked' and status <>'parked' and deleted<>1 and(priority='1a - DO NOW') and  assigned_to_user='" + drpUsers.SelectedItem.Text.ToString().Replace("'", "''") + "'";
                                        DataSet dsdoNow = DatabaseHelper.getDataset(sqlchkdonow);
                                        if (dsdoNow != null && dsdoNow.Tables.Count > 0)
                                        {
                                            if (dsdoNow.Tables[0].Rows.Count > 1)
                                            {
                                                drpPriorities.SelectedValue = lbl_Priority.Text;
                                                Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('" + drpUsers.SelectedItem.Text.ToString() + " already have the 1a-DO NOW  task.');</script>");
                                                return;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else if (drpPriorities.SelectedItem.Text.ToString() == "0 - IMMEDIATE") 
        {
            if( drpStatuses.SelectedItem.Text.ToString() != "to check")
            {
                if(drpStatuses.SelectedItem.Text.ToString() != "Checked")
                {
                  if( drpStatuses.SelectedItem.Text.ToString() != "parked")
                    {
                      if(drpStatuses.SelectedItem.Text.ToString() != "closed")
                         {
                             if (drpStatuses.SelectedItem.Text.ToString() != "re-opened")
                             {
                                 if (drpStatuses.SelectedItem.Text.ToString() != "CWO")
                                 {
                                     if (drpStatuses.SelectedItem.Text.ToString() != "CWO but away")
                                     {
                                         string sqlchkImmidiate = "";
                                         sqlchkImmidiate = "select * from tasks where status <> 'closed' and status <> 'Checked' and status <>'parked' and deleted<>1 and(priority='0 - IMMEDIATE') and assigned_to_user='" + drpUsers.SelectedItem.Text.ToString().Replace("'", "''") + "'";
                                         DataSet dsImmidiate = DatabaseHelper.getDataset(sqlchkImmidiate);
                                         if (dsImmidiate != null && dsImmidiate.Tables.Count > 0)
                                         {
                                             if (dsImmidiate.Tables[0].Rows.Count > 1)
                                             {
                                                 drpPriorities.SelectedValue = lbl_Priority.Text;
                                                 Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('" + drpUsers.SelectedItem.Text.ToString() + " already have 0 - IMMEDIATE task.');</script>");
                                                 return;
                                             }
                                         }
                                     }
                                 }
                             }
                         }
                    }
                }
            }
        }
        //==================================================================
        //==================================================================
        if (lblTaskId.Text == "New")
        {
            if (ftbComment.Content.Trim() == "")
            {
                divMessage1.InnerHtml = "Comment is required.";
                return;
            }
            else
            {
                flagEmailNotification = true;
            }

            sql = @"DECLARE @TaskId INT;";
            sql += "DECLARE @CommentId INT;";
            sql += " insert into tasks([RequestId],[short_desc],[reported_user],[reported_date],[status],[priority],[category],[project],[OrderId],[Isprivate],[project_type],[Relevant_URL],[assigned_to_user],[last_updated_user],[last_updated_date],[deleted],[ETC],Company,StartDate,DueDate) ";
            sql += " values(" + reqid.ToString() + ",";
            sql += "'" + txtShortDescr.Text.Trim().Replace("'", "''") + "',";
            sql += "'" + Session["admin"].ToString().Replace("'", "''") + "',";
            sql += "getdate(),";
            sql += "'" + drpStatuses.SelectedItem.Text.Replace("'", "''") + "',";
            sql += "'" + drpPriorities.SelectedItem.Text.Replace("'", "''") + "',";
            sql += "'" + drpCategories.SelectedItem.Text.Replace("'", "''") + "',";
            sql += "'" + drpProjects.SelectedItem.Text.Replace("'", "''") + "',";
            sql += "99999,";
            if (checkisPrivate.Checked)
            {
                sql += "'True',";
            }
            else
            {

                sql += "'False',";
            }
            sql += "'" + drpProjectType.SelectedItem.Text.Replace("'", "''") + "',";
            sql += "'" + txtRelevantURL.Text.Trim().Replace("'", "''") + "',";
            sql += "'" + drpUsers.SelectedItem.Text.Replace("'", "''") + "',";
            sql += "'" + Session["admin"].ToString().Replace("'", "''") + "',";
            sql += "getdate(),";
            sql += "0,";
            sql += "" + etcMins.ToString() + ",'" + hdnProjectCompany.Value.ToString() + "','" + txtStartDate.Text + "','" + txtDueDate.Text + "'); ";
            sql += " SET @TaskId = SCOPE_IDENTITY(); ";

            if (ftbComment.Content.Trim() != "")
            {
                sql += " insert into task_comments([task_id],[username],[post_date],[comment],[Ip],[Macid],[deleted],[qflag],[QuesTo]) ";
                sql += " values(@TaskId,";
                sql += "'" + Session["admin"].ToString().Replace("'", "''") + "',";
                sql += "getdate(),";
                sql += "N'" + ftbComment.Content.Trim().Replace("'", "''") + "',";
                //************************************************
                sql += "'" + strArrayMacIDAndIP[1] + "',";
                sql += "'" + strArrayMacIDAndIP[0] + "',";
                //************************************************
                sql += "0,";
                
                if (drpCategories.SelectedItem.Text == "question")
                {
                    sql += (drpCategories.SelectedItem.Text == "question" ? "1" : "0") + ", ";
                    sql += "'" + drpUsers.SelectedItem.Text.Replace("'", "''") + "' ); ";

                }
                else
                {
                    string strquesto = "";
                    sql += (drpCategories.SelectedItem.Text == "question" ? "1" : "0") + ", ";
                    sql += "'" + strquesto.Replace("'", "''") + "' ); ";

                    
                }
                sql += " SET @CommentId = SCOPE_IDENTITY(); ";
                sql += " insert into read_comments([tc_id],[username]) ";
                sql += " values(@CommentId,'" + Session["admin"] + "'); ";
                sql += " insert into read_task([task_id],[username]) ";
                sql += " values(@TaskId,'" + Session["admin"] + "'); ";
            }
            sql += "select @TaskId;";

            object objResult = DatabaseHelper.executeScalar(sql);
            //==================================================================
            //Insert the add recipient value in task_subscriptions table 
            //==================================================================
            if (drpAddRecipient.SelectedItem.Value != "" && drpAddRecipient.SelectedItem.Text != "[add recipients]")
            {
                string sqlforselect, sqlforinsert;
                sqlforselect = @"select count(*) from task_subscriptions where task_id = " + objResult.ToString() + " and username='" + drpAddRecipient.SelectedItem.Text + "'";
                object countforinsert = DatabaseHelper.executeScalar(sqlforselect);
                if (countforinsert.ToString() != "0")
                { }
                else
                {
                    sqlforinsert = "insert into task_subscriptions([task_id],[username])values(" + objResult.ToString() + ",'" + drpAddRecipient.SelectedItem.Text + "')";
                    DatabaseHelper.executeScalar(sqlforinsert);
                }
            }
            //==================================================================
            //==================================================================


            if (objResult.ToString() != "0")
            {
                
                add_subscription(objResult);

                if (reqid != 0)
                {
                    add_taskAttachments(objResult);
                }
                
                emailSubject = "Task ID:" + objResult.ToString() + " was added - " + txtShortDescr.Text.Trim() + " (Task ID:" + objResult.ToString() + ")";

                task_action = ((string)DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + Session["admin"].ToString() + "'")) + " has added task ";

                sendNotification(objResult.ToString());

                Session["message"] = "Task was created.";

                Response.Redirect("edit_task.aspx?id=" + objResult.ToString());
            }
            else
            {
                divMessage.InnerHtml = "Task was not created.";
            }
        }
        else
        {

            //==================================================================
            //Insert the add recipient value in task_subscriptions table
            //==================================================================
            if (drpAddRecipient.SelectedItem.Value != "" && drpbinduser.SelectedItem.Text != "[add recipients]")
            {
                string sqlforselect, sqlforinsert;
                sqlforselect = @"select count(*) from task_subscriptions where task_id = " + lblTaskId.Text.Trim() + " and username='" + drpbinduser.SelectedItem.Text + "'";
                object countforupdate = DatabaseHelper.executeScalar(sqlforselect);
                if (countforupdate.ToString() != "0")
                { }
                else
                {
                    sqlforinsert = "insert into task_subscriptions([task_id],[username])values(" + lblTaskId.Text.Trim() + ",'" + drpbinduser.SelectedItem.Text + "')";
                    DatabaseHelper.executeScalar(sqlforinsert);
                }
            }
            
            try
            {

                string str_assignto = "", assignedTo = "", strQueryTo = "", assignedTochk = "", Currstatus = "";
                int OrderIdTo = 0, upOrderIdTo = 0;
                str_assignto = "Select * from tasks where task_id = " + lblTaskId.Text.Trim() + ";";
                DataSet dsAssign = DatabaseHelper.getDataset(str_assignto);

                //---------------------------------------------------------------------------------------------
                if (dsAssign != null && dsAssign.Tables.Count > 0 && dsAssign.Tables[0].Rows.Count > 0)
                {
                    assignedTochk = dsAssign.Tables[0].Rows[0]["assigned_to_user"].ToString();
                    Currstatus = dsAssign.Tables[0].Rows[0]["status"].ToString();
                    if (dsAssign.Tables[0].Rows[0]["OrderId"] != null && dsAssign.Tables[0].Rows[0]["OrderId"].ToString() != "")
                    {
                        upOrderIdTo = Convert.ToInt32(dsAssign.Tables[0].Rows[0]["OrderId"].ToString());
                    }
                    if (Convert.ToInt32(dsAssign.Tables[0].Rows[0]["OrderId"].ToString()) != 99999 && dsAssign.Tables[0].Rows[0]["OrderId"].ToString() != "" && dsAssign.Tables[0].Rows[0]["OrderId"] != null)
                    {
                        if ((assignedTochk != drpUsers.SelectedItem.Text.ToString()) && upOrderIdTo != 99999)
                        {
                            //Here we change the order if Task assigned to another user. 
                            int MaxOrderId = 0;
                            string strmaxOrder = "select max(OrderId) as MaxOrderId from tasks where status <> 'closed' and status <> 'Checked' and status <>'parked' and  deleted <> 1  and assigned_to_user = '" + assignedTochk + "'  and OrderId!=99999";
                            DataSet Maxds = DatabaseHelper.getDataset(strmaxOrder);
                            if (Maxds != null & Maxds.Tables.Count > 0 && Maxds.Tables[0].Rows.Count > 0)
                            {
                                MaxOrderId = Convert.ToInt32(Maxds.Tables[0].Rows[0]["MaxOrderId"].ToString());
                            }
                            string AllOrderId = "select Task_id,OrderId from tasks where status <> 'closed' and status <> 'Checked' and status <>'parked' and  deleted <> 1  and assigned_to_user = '" + assignedTochk + "' and (OrderId > '" + upOrderIdTo + "' and OrderId <=  '" + MaxOrderId + "')    ORDER BY OrderId ASC";
                            DataSet Allds = DatabaseHelper.getDataset(AllOrderId);
                            if (Allds != null && Allds.Tables.Count > 0 && Allds.Tables[0].Rows.Count > 0)
                            {
                                for (int i = 0; Allds.Tables[0].Rows.Count > i; i++)
                                {
                                    int vOrderId = Convert.ToInt32(Allds.Tables[0].Rows[i]["OrderId"].ToString()) - 1;
                                    string IncreseQ = "Update tasks set OrderId=" + vOrderId + " where Task_id=" + Convert.ToInt32(Allds.Tables[0].Rows[i]["Task_id"].ToString()) + ";";
                                    int chki = DatabaseHelper.executeNonQuery(IncreseQ);
                                }
                            }
                        }
                        else if (((drpStatuses.SelectedItem.Text.ToString() != Currstatus) && (drpStatuses.SelectedItem.Text.ToString() == "checked" || drpStatuses.SelectedItem.Text.ToString() == "closed" || drpStatuses.SelectedItem.Text.ToString() == "parked")) && upOrderIdTo != 99999)
                        {
                            //Here i will check that task will be closed,parked or checked so changed the order of other Task.
                            int MaxOrderId = 0;
                            string strmaxOrder = "select max(OrderId) as MaxOrderId from tasks where status <> 'closed' and status <> 'Checked' and status <>'parked' and  deleted <> 1  and assigned_to_user = '" + assignedTochk + "'  and OrderId!=99999";
                            DataSet Maxds = DatabaseHelper.getDataset(strmaxOrder);
                            if (Maxds != null & Maxds.Tables.Count > 0 && Maxds.Tables[0].Rows.Count > 0)
                            {
                                MaxOrderId = Convert.ToInt32(Maxds.Tables[0].Rows[0]["MaxOrderId"].ToString());
                            }
                            string AllOrderId = "select Task_id,OrderId from tasks where status <> 'closed' and status <> 'Checked' and status <>'parked' and  deleted <> 1  and assigned_to_user = '" + assignedTochk + "' and (OrderId > '" + upOrderIdTo + "' and OrderId <=  '" + MaxOrderId + "')    ORDER BY OrderId ASC";
                            DataSet Allds = DatabaseHelper.getDataset(AllOrderId);
                            if (Allds != null && Allds.Tables.Count > 0 && Allds.Tables[0].Rows.Count > 0)
                            {
                                for (int i = 0; Allds.Tables[0].Rows.Count > i; i++)
                                {

                                    int vOrderId = Convert.ToInt32(Allds.Tables[0].Rows[i]["OrderId"].ToString()) - 1;
                                    string IncreseQ = "Update tasks set OrderId=" + vOrderId + " where Task_id=" + Convert.ToInt32(Allds.Tables[0].Rows[i]["Task_id"].ToString()) + ";";
                                    int chki = DatabaseHelper.executeNonQuery(IncreseQ);

                                }
                            }

                        }
                        else if (((drpStatuses.SelectedItem.Text.ToString() != Currstatus) && (Currstatus == "checked" || Currstatus == "closed" || Currstatus == "parked")) && upOrderIdTo != 99999)
                        {
                            int MaxOrderId = 0;
                            string strmaxOrder = "select max(OrderId) as MaxOrderId from tasks where status <> 'closed' and status <> 'Checked' and status <>'parked' and  deleted <> 1  and assigned_to_user = '" + assignedTochk + "'  and OrderId!=99999";
                            DataSet Maxds = DatabaseHelper.getDataset(strmaxOrder);
                            if (Maxds != null & Maxds.Tables.Count > 0 && Maxds.Tables[0].Rows.Count > 0)
                            {
                                MaxOrderId = Convert.ToInt32(Maxds.Tables[0].Rows[0]["MaxOrderId"].ToString());
                            }
                            string IncreseQ = "Update tasks set OrderId=" + (Convert.ToInt32(MaxOrderId) + 1) + " where Task_id=" + Convert.ToInt32(lblTaskId.Text.Trim()) + ";";
                            int chki = DatabaseHelper.executeNonQuery(IncreseQ);

                        }
                    }
                }
                //---------------------------------------------------------------------------------------------
                if (dsAssign != null && dsAssign.Tables.Count > 0 && dsAssign.Tables[0].Rows.Count > 0)
                {
                    assignedTo = dsAssign.Tables[0].Rows[0]["assigned_to_user"].ToString();
                    if (dsAssign.Tables[0].Rows[0]["OrderId"] != null && dsAssign.Tables[0].Rows[0]["OrderId"].ToString() != "")
                    {
                        OrderIdTo = Convert.ToInt32(dsAssign.Tables[0].Rows[0]["OrderId"].ToString());
                    }
                    else
                    {
                        try
                        {
                            string sqlQuestion = "update tasks set OrderId=99999 where task_id= " + lblTaskId.Text.Trim() + ";";
                            int test = DatabaseHelper.executeNonQuery(sqlQuestion);
                            OrderIdTo = 99999;
                        }
                        catch (Exception ex)
                        {
                            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error:Edit Task for 99999 " + "DateTime : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:" + ex.Message + "\n" + " Source:" + ex.Source + "\n" + " InnerException:" + ex.InnerException + " Stack Trace: " + ex.StackTrace + "\n" + " Task Id " + lblTaskId.Text);
                        }
                    }
                }
                if (assignedTo != drpUsers.SelectedItem.Text.ToString())
                {
                    if (OrderIdTo != 99999)
                    {
                        strQueryTo = "update tasks set OrderId=99999 where task_id = " + lblTaskId.Text.Trim() + ";";
                        int intResultTo = DatabaseHelper.executeNonQuery(strQueryTo);
                    }
                }
            }
            catch (Exception ex)
            {
                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error:Change Order Id " + "DateTime : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:" + ex.Message + "\n" + " Source:" + ex.Source + "\n" + " InnerException:" + ex.InnerException + " Stack Trace: " + ex.StackTrace + "\n" + " Task Id " + lblTaskId.Text);
            
            }
            //******************************************************************
            //==================================================================

           
            sql = " update tasks set ";
            sql += " [short_desc]='" + txtShortDescr.Text.Trim().Replace("'", "''") + "',";
            sql += " [status]='" + drpStatuses.SelectedItem.Text.Replace("'", "''") + "',";
            sql += " [priority]='" + drpPriorities.SelectedItem.Text.Replace("'", "''") + "',";
            sql += " [category]='" + drpCategories.SelectedItem.Text.Replace("'", "''") + "',";
            sql += " [project]='" + drpProjects.SelectedItem.Text.Replace("'", "''") + "',";
            if (checkisPrivate.Checked)
            {
                sql += " [Isprivate]='True',";
            }
            else
            {
                sql += " [Isprivate]='False',";

            }
            sql += " [project_type]='" + drpProjectType.SelectedItem.Text.Replace("'", "''") + "',";
            sql += " [Relevant_URL]='" + txtRelevantURL.Text.Trim().Replace("'", "''") + "',";
            sql += " [assigned_to_user]='" + drpUsers.SelectedItem.Text.Replace("'", "''") + "',";
            sql += " [last_updated_user]='" + Session["admin"].ToString().Replace("'", "''") + "',";
            sql += " [last_updated_date]=getdate(),";
            sql += " [ETC]=" + etcMins.ToString() + ",";
            sql += " [Company]= '" + hdnProjectCompany.Value.ToString() + "'";
            sql += " where task_id = " + lblTaskId.Text.Trim() + "; ";

            if (ftbComment.Content.Trim() != "")
            {
                sql += "DECLARE @CommentId INT;";
                sql += " insert into task_comments([task_id],[username],[post_date],[comment],[Ip],[Macid],[deleted],[qflag],[QuesTo]) ";
                sql += " values(" + lblTaskId.Text.Trim() + ",";
                sql += "'" + Session["admin"].ToString().Replace("'", "''") + "',";
                sql += "getdate(),";
                sql += "N'" + ftbComment.Content.Trim().Replace("'", "''") + "',";
                //*********************************************************
                sql += "'" + strArrayMacIDAndIP[1] + "',";
                sql += "'" + strArrayMacIDAndIP[0] + "',";
                //*********************************************************
                sql += "0,";
               
                if (chkQuestion.Checked == true)
                {
                    sql += (chkQuestion.Checked ? "1" : "0") + ", ";
                    sql += "'" + drpUsers.SelectedValue.Replace("'", "''") + "' ); ";

                }
                else
                {
                    string strquesto = "";
                    sql += (chkQuestion.Checked ? "1" : "0") + ", ";
                    sql += "'" + strquesto.Replace("'", "''") + "' ); ";
                }
                sql += " SET @CommentId = SCOPE_IDENTITY(); ";
                sql += " insert into read_comments([tc_id],[username]) ";
                sql += " values(@CommentId,'" + Session["admin"] + "')";
                sql += " insert into read_task([task_id],[username]) ";
                sql += " values(" + lblTaskId.Text.Trim() + ",'" + Session["admin"] + "'); ";
            }

            sql += record_changes();

            int intResult = DatabaseHelper.executeNonQuery(sql);

            if (intResult != 0)
            {
                if ((prevStatus.Value != drpStatuses.SelectedValue) && (drpStatuses.SelectedValue == "CWO"))
                {
                    sql = @"select count(*) from hours_reporting 
                        where task_id = " + lblTaskId.Text.Trim() + " and finished_date is null and started_date > convert(datetime,'" + DateTime.Today.ToString() + "',103)";

                    object count = DatabaseHelper.executeScalar(sql);

                    if (count.ToString() == "0")
                    {
                        sql = "insert into hours_reporting ([task_id],[started_date],[username]) ";
                        sql += "values(" + lblTaskId.Text.Trim() + ",getdate(),'" + Session["admin"].ToString().Replace("'", "''") + "')";

                        intResult = DatabaseHelper.executeNonQuery(sql);
                    }
                }
                else if ((prevStatus.Value == "CWO") && (drpStatuses.SelectedValue != "CWO"))
                {
                    sql = @"update hours_reporting set finished_date = getdate(), minutes = datediff(minute, started_date, getdate()) 
			                where task_id = " + lblTaskId.Text.Trim() + "  and finished_date is null";

                    intResult = DatabaseHelper.executeNonQuery(sql);
                }


                if (lblAssignedTo.Text.Trim() != drpUsers.SelectedItem.Text.Trim())
                {
                    object objResult = DatabaseHelper.executeScalar("select count(*) from task_subscriptions where task_id=" + lblTaskId.Text.Trim() + " and username='" + drpUsers.SelectedItem.Text.Trim().Replace("'", "''") + "'");

                    if (objResult.ToString() == "0")
                    {
                        sql = "insert into task_subscriptions ([task_id],[username]) ";
                        sql += "values(" + lblTaskId.Text.Trim() + ",'" + drpUsers.Text.Trim().Replace("'", "''") + "')";

                        intResult = DatabaseHelper.executeNonQuery(sql);
                    }
                }

                if (btnUpdate.Text == "Mark as undeleted")
                {
                    intResult = DatabaseHelper.executeNonQuery("update tasks set deleted = 0 where task_id = " + lblTaskId.Text.Trim());

                    sql = @" insert into task_updates([task_id],[username],[post_date],[comment]) 
                            values(" + lblTaskId.Text.Trim() + ",'" + Session["admin"].ToString().Replace("'", "''") + "',getdate(),'Restored deleted task.'); ";

                    intResult = DatabaseHelper.executeNonQuery(sql);

                    task_action = ((string)DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + Session["admin"].ToString() + "'")) + " has restored deleted task ";

                    if (chkQuestion.Checked == true)
                    {
                        emailSubject = "Task ID:" + lblTaskId.Text.Trim() + " was restored - Question asked - please respond. (Task ID:" + lblTaskId.Text.Trim() + ")";
                    }
                    else
                    {
                        emailSubject = "Task ID:" + lblTaskId.Text.Trim() + " was restored - " + txtShortDescr.Text.Trim() + " (Task ID:" + lblTaskId.Text.Trim() + ")";
                    }
                }
                else
                {
                    task_action = ((string)DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + Session["admin"].ToString() + "'")) + " has updated task ";

                    if (chkQuestion.Checked == true)
                    {
                        emailSubject = "Task ID:" + lblTaskId.Text.Trim() + " was updated - Question asked - please respond. (Task ID:" + lblTaskId.Text.Trim() + ")";
                    }
                    else
                    {
                        emailSubject = "Task ID:" + lblTaskId.Text.Trim() + " was updated - " + txtShortDescr.Text.Trim() + " (Task ID:" + lblTaskId.Text.Trim() + ")";
                    }
                }
                //=======================================
                if (drpPriorities.SelectedItem.Text.ToString() == "0 - IMMEDIATE" || drpPriorities.SelectedItem.Text.ToString() == "1a - DO NOW")
                {
                    sendNotification(lblTaskId.Text.Trim());
                }
                //=======================================
                //sendNotification(lblTaskId.Text.Trim());

                if (lblAssignedTo.Text.Trim() != drpUsers.SelectedItem.Text.Trim())
                {
                    if (!DatabaseHelper.isAdmin(lblAssignedTo.Text.Trim()))
                    {
                        intResult = DatabaseHelper.executeNonQuery("delete from task_subscriptions where task_id=" + lblTaskId.Text.Trim() + " and username='" + lblAssignedTo.Text.Trim().Replace("'", "''") + "'");
                    }
                }

                try
                {

                    string sqlinsert = "insert into read_task([task_id],[username]) ";
                    sqlinsert += " values(" + lblTaskId.Text.Trim() + ",'" + Session["admin"].ToString() + "')";

                    int intCommentResult = DatabaseHelper.executeNonQuery(sqlinsert);
                }
                catch { }



                Session["message"] = "Task was updated.";

                Response.Redirect("edit_task.aspx?id=" + lblTaskId.Text.Trim());


            }
            else
            {
                divMessage1.InnerHtml = "Task was not updated.";
            }
        }
        /// }
    }

    private string record_changes()
    {
        string base_sql = @" insert into task_updates([task_id],[username],[post_date],[comment]) 
                            values($task_id,'$us',getdate(),'$comment'); ";

        base_sql = base_sql.Replace("$task_id", lblTaskId.Text.Trim());
        base_sql = base_sql.Replace("$us", Session["admin"].ToString().Replace("'", "''"));

        string strSql = "";

        if (prevRelevantUrl.Value != txtRelevantURL.Text.Trim())
        {
            strSql += base_sql.Replace(
               "$comment",
               "changed Relevant URL  from \""
               + prevRelevantUrl.Value.Replace("'", "''") + "\" to \""
               + txtRelevantURL.Text.Trim().Replace("'", "''") + "\"");

        }

        if (prevShortDescr.Value != txtShortDescr.Text.Trim())
        {
            strSql += base_sql.Replace(
                "$comment",
                "changed desc from \""
                + prevShortDescr.Value.Replace("'", "''") + "\" to \""
                + txtShortDescr.Text.Trim().Replace("'", "''") + "\"");

        }

        if (etcMins.ToString() != prevETC.Value)
        {
            try
            {
                int prevMins = Convert.ToInt32(prevETC.Value);

                strSql += base_sql.Replace(
                   "$comment",
                   "changed estimated time to completion from \""
                   + (prevMins / 60) + " hrs " + (prevMins % 60) + " mins" + "\" to \""
                   + (etcMins / 60) + " hrs " + (etcMins % 60) + " mins" + "\"");

            }
            catch { }

        }

        if (drpProjectType.SelectedValue != prevProjectType.Value)
        {
            strSql += base_sql.Replace(
               "$comment",
               "changed project type from \""
               + prevProjectType.Value.Replace("'", "''") + "\" to \""
               + drpProjectType.SelectedItem.Text.Replace("'", "''") + "\"");

        }

        if (drpProjects.SelectedValue != prevProject.Value)
        {
            strSql += base_sql.Replace(
                "$comment",
                "changed project from \""
                + prevProject.Value.Replace("'", "''") + "\" to \""
                + drpProjects.SelectedItem.Text.Replace("'", "''") + "\"");


        }

        if (prevCategory.Value != drpCategories.SelectedValue)
        {
            strSql += base_sql.Replace(
                "$comment",
                "changed category from \""
                + prevCategory.Value.Replace("'", "''") + "\" to \""
                + drpCategories.SelectedItem.Text.Replace("'", "''") + "\"");

        }

        if (prevPriority.Value != drpPriorities.SelectedValue)
        {

            strSql += base_sql.Replace(
                "$comment",
                "changed priority from \""
                + prevPriority.Value.Replace("'", "''") + "\" to \""
                + drpPriorities.SelectedItem.Text.Replace("'", "''") + "\"");

        }

        if (prevStatus.Value != drpStatuses.SelectedValue)
        {
            strSql += base_sql.Replace(
                "$comment",
                "changed status from \""
                + prevStatus.Value.Replace("'", "''") + "\" to \""
                + drpStatuses.SelectedItem.Text.Replace("'", "''") + "\"");

        }

        if (prevAssignedTo.Value != drpUsers.SelectedValue)
        {
            strSql += base_sql.Replace(
                "$comment",
                "changed assigned_to from \""
                + prevAssignedTo.Value.Replace("'", "''") + "\" to \""
                + drpUsers.SelectedItem.Text.Replace("'", "''") + "\"");


        }
        return strSql;
    }

    void sendNotification(string strTaskId)
    {

        //===========================New Code=========================================================

        string touser_id = "";

        for (int ichk = 0; ichk <= chkmaillst.Items.Count - 1; ichk++)
        {
            if (chkmaillst.Items[ichk].Selected == true)
            {
                touser_id += chkmaillst.Items[ichk].Value + ",";
            }

        }
        string[] ArrofUser = touser_id.ToString().Split(',');



        sql = "select email, EmailNotification,PR_EmailNotification ";
        sql += " from users, task_subscriptions ";
        sql += " where users.username = task_subscriptions.username ";

        if (ArrofUser.Length > 1)
        {
            sql += " and task_subscriptions.task_id =" + strTaskId.ToString() + " and task_subscriptions.username <> '" + Session["admin"].ToString() + "' and user_id in (" + touser_id.TrimEnd(',') + ")";
        }
        else
        {
            sql += " and task_subscriptions.task_id =" + strTaskId.ToString() + " and task_subscriptions.username <> '" + Session["admin"].ToString() + "'";
        }

        DataSet ds = DatabaseHelper.getDataset(sql);
        if (drpbinduser.SelectedItem.Value != "" && drpbinduser.SelectedItem.Text != "[not assigned]")
        {
            string Sqlqry;
            Sqlqry = "select email,EmailNotification,PR_EmailNotification  from users where active = 'Y'and user_id in (" + drpbinduser.SelectedItem.Value + ")";
            DataSet dsoneqry = DatabaseHelper.getDataset(Sqlqry);
            if (ds.Tables.Count > 0)
            {
                DataRow newRow = ds.Tables[0].NewRow();
                newRow["email"] = dsoneqry.Tables[0].Rows[0]["email"].ToString();
                newRow["EmailNotification"] = dsoneqry.Tables[0].Rows[0]["EmailNotification"].ToString();
                newRow["PR_EmailNotification"] = dsoneqry.Tables[0].Rows[0]["PR_EmailNotification"].ToString();
                ds.Tables[0].Rows.Add(newRow);
            }
            else
            {

                DataRow newRow = ds.Tables[0].NewRow();
                newRow["email"] = dsoneqry.Tables[0].Rows[0]["email"].ToString();
                newRow["EmailNotification"] = dsoneqry.Tables[0].Rows[0]["EmailNotification"].ToString();
                newRow["PR_EmailNotification"] = dsoneqry.Tables[0].Rows[0]["PR_EmailNotification"].ToString();
                ds.Tables[0].Rows.Add(newRow);
            }


        }

        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            string strHtml = generateAdminEmail(strTaskId);

            string toEmails = "";


            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (ds.Tables[0].Rows[i]["EmailNotification"].ToString() == "Y")
                {

                    string reqPR = "";
                    string[] strPR = ds.Tables[0].Rows[i]["PR_EmailNotification"].ToString().Split(new char[] { ';' });
                    for (int j = 0; j < strPR.Length - 1; j++)
                    {
                        reqPR = strPR[j];
                        if (reqPR == "All")
                        {
                            toEmails += ds.Tables[0].Rows[i]["email"].ToString() + ";";
                        }
                        else if (reqPR == drpPriorities.SelectedValue)
                        {
                            toEmails += ds.Tables[0].Rows[i]["email"].ToString() + ";";
                        }

                    }


                }
                else if ((ds.Tables[0].Rows[i]["EmailNotification"].ToString() == "N") && (flagEmailNotification == true))
                {

                    string reqPR = "";
                    string[] strPR = ds.Tables[0].Rows[i]["PR_EmailNotification"].ToString().Split(new char[] { ';' });

                    for (int j = 0; j < strPR.Length; j++)
                    {
                        reqPR = strPR[j];
                        if (reqPR == "All")
                        {
                            toEmails += ds.Tables[0].Rows[i]["email"].ToString() + ";";
                        }
                        else if (reqPR == drpPriorities.SelectedValue)
                        {
                            toEmails += ds.Tables[0].Rows[i]["email"].ToString() + ";";
                        }

                    }

                }

            }
            toEmails = toEmails.TrimEnd(';');

            //===========================================================================================
            sql = "select email,EmailNotification,PR_EmailNotification ,Firstname,user_id ";
            sql += " from users, task_subscriptions ";
            sql += " where users.username = task_subscriptions.username ";
            sql += " and task_subscriptions.task_id =" + strTaskId.ToString() + " and task_subscriptions.username = '" + Session["admin"].ToString() + "'";
            DataSet ds_chkboxlst = DatabaseHelper.getDataset(sql);
            if (ds_chkboxlst != null && ds_chkboxlst.Tables.Count > 0 && ds_chkboxlst.Tables[0].Rows.Count > 0)
            {
                string strmail = ds_chkboxlst.Tables[0].Rows[0]["email"].ToString();
                if (toEmails != "")
                {
                    if (!toEmails.Contains(strmail))
                    {
                        toEmails += ";" + strmail;
                    }
                }
                else
                {
                    toEmails = strmail;
                }
            }
            //==============================================================================================

            if (emailSubject == "")
            {
                emailSubject = "Task ID:" + strTaskId.ToString() + " was updated - " + txtShortDescr.Text.Trim() + " (Task ID:" + strTaskId.ToString() + ")";
            }
            if (chkEmailnotify.Checked)
            {
                bool flag = DatabaseHelper.sendEmailTasks(toEmails, emailSubject, strHtml);
            }
            else
            {
            }
        }
    }

    private string record_changes_for_email()
    {
        string strSql = "";

        if (drpProjects.SelectedValue != prevProject.Value)
        {
            strSql += "Project has been changed from \""
                + prevProject.Value.Replace("'", "''") + "\" to \""
                + drpProjects.SelectedItem.Text.Replace("'", "''") + "\"<br>";

            //prevProject.Value = drpProjects.SelectedItem.Value;
        }

        if (prevAssignedTo.Value != drpUsers.SelectedValue)
        {
            strSql += "Assigned_to has been changed from \""
                + prevAssignedTo.Value.Replace("'", "''") + "\" to \""
                + drpUsers.SelectedItem.Text.Replace("'", "''") + "\"<br>";

            //prevAssignedTo.Value = drpUsers.SelectedValue;		    
        }

        if (prevStatus.Value != drpStatuses.SelectedValue)
        {
            strSql += "Status has been changed from \""
                + prevStatus.Value.Replace("'", "''") + "\" to \""
                + drpStatuses.SelectedItem.Text.Replace("'", "''") + "\"<br>";

            //prevStatus.Value = drpStatuses.SelectedValue;
        }
        try
        {

            if (prevPriority.Value != drpPriorities.SelectedValue)
            {
                strSql += "Priority has been changed from \""
                    + prevPriority.Value.Replace("'", "''") + "\" to \""
                    + drpPriorities.SelectedItem.Text.Replace("'", "''") + "\"<br>";

                //prevPriority.Value = drpPriorities.SelectedValue;
            }
        }
        catch { }

        if (prevCategory.Value != drpCategories.SelectedValue)
        {
            strSql += "Category has been changed from \""
                + prevCategory.Value.Replace("'", "''") + "\" to \""
                + drpCategories.SelectedItem.Text.Replace("'", "''") + "\"<br>";

            //prev_category.Value = drpCategories.SelectedValue;
        }

        if (drpProjectType.SelectedValue != prevProjectType.Value)
        {
            strSql += "Project type has been changed from \""
                + prevProjectType.Value.Replace("'", "''") + "\" to \""
               + drpProjectType.SelectedItem.Text.Replace("'", "''") + "\"<br>";

            //prevProjectType.Value = drpProjectType.SelectedItem.Value;	    
        }

        if (etcMins.ToString() != prevETC.Value)
        {
            try
            {
                int prevMins = Convert.ToInt32(prevETC.Value);

                strSql += "Estimated time to completion has been changed from \""
                   + (prevMins / 60) + " hrs " + (prevMins % 60) + " mins" + "\" to \""
                   + (etcMins / 60) + " hrs " + (etcMins % 60) + " mins" + "\"";
            }
            catch { }

            //prevProjectType.Value = drpProjectType.SelectedItem.Value;	    
        }

        if (prevRelevantUrl.Value != txtRelevantURL.Text.Trim())
        {
            strSql += "Relevant URL has been changed from  \""
                + prevRelevantUrl.Value.Replace("'", "''") + "\" to \""
               + txtRelevantURL.Text.Trim().Replace("'", "''") + "\"<br>";

            //prevRelevantUrl.Value = txtRelevantURL.Text.Trim();
        }

        if (prevShortDescr.Value != txtShortDescr.Text.Trim())
        {
            strSql += "Desc has been changed from \""
                + prevShortDescr.Value.Replace("'", "''") + "\" to \""
                + txtShortDescr.Text.Trim().Replace("'", "''") + "\"<br>";

            //prevShortDescr.Value = txtShortDescr.Text.Trim();
        }



        if (ftbComment.Content.Trim() != "" && lblTaskId.Text != "New")
        {
            if (chkQuestion.Checked == true)
            {
                strSql += "Question has been added.<br>";
            }
            else
            {
                strSql += "Comment has been added.<br>";
            }

            // flagEmailNotification is set to verify the email notification setting according to user account settings.
            flagEmailNotification = true;
        }
        return strSql;
    }

    private string generateAdminEmail(string strTaskId)
    {
        DataSet dsTask = DatabaseHelper.getDataset("select * from tasks where task_id=" + strTaskId);
        string strBody = "";
        if (dsTask == null || dsTask.Tables.Count <= 0 || dsTask.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "Task not found.<br><br><a href='tasks.aspx'>View tasks</a>";
        }
        else
        {
            strBody += "Task: <span style=\"color:red\">";

            strBody += task_action;

            strBody += " (<a style=\"color:red\" href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_task.aspx?id=" + strTaskId + "'>" + strTaskId + "</a>) </span> " + DateTime.Parse(dsTask.Tables[0].Rows[0]["last_updated_date"].ToString()).ToString("dd MMM h:mm tt") + "<br><br>";

            if (lblTaskId.Text != "New")
            {
                string attribures = record_changes_for_email();

                if (attribures.ToString().Trim() != "")
                {
                    strBody += "The attributes below have been amended.<br>";
                    strBody += "<span style=\"color:red\">" + attribures + "</span><br>";
                }
            }

            strBody += "<a href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_task.aspx?id=" + strTaskId + "'>" + txtShortDescr.Text.Trim() + "</a>&nbsp;&nbsp;&nbsp;(" + dsTask.Tables[0].Rows[0]["priority"].ToString() + " : " + dsTask.Tables[0].Rows[0]["status"].ToString() + ")<br><br>";
            strBody += "Emp: " + ((string)DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + dsTask.Tables[0].Rows[0]["assigned_to_user"].ToString() + "'")) + "<br>";
            strBody += "Project: " + dsTask.Tables[0].Rows[0]["project"].ToString() + "<br>";
            strBody += "Relevant URL: <a href='" + dsTask.Tables[0].Rows[0]["Relevant_URL"].ToString() + "' target='_blank'>" + dsTask.Tables[0].Rows[0]["Relevant_URL"].ToString() + "</a><br>";
            strBody += "Start date: " + dsTask.Tables[0].Rows[0]["StartDate"].ToString() + "<br>";
            strBody += "Due date: " + dsTask.Tables[0].Rows[0]["DueDate"].ToString() + "<br>";
            strBody += "<br><br>";

            sql = "Select * from task_comments where task_id=" + strTaskId.ToString() + " and deleted <> 1 order by tc_id desc";

            DataSet dsTaskDetails = DatabaseHelper.getDataset(sql);

            strBody += "<table cellspacing=\"0\" border=\"1\" style=\"width: 99%;border-collapse: collapse;\">";
            strBody += "<tbody>";

            for (int i = 0; i < dsTaskDetails.Tables[0].Rows.Count; i++)
            {
                strBody += "<tr>";
                strBody += "<td>";
                strBody += "<div style=\"border: 1px none Green;\">";
                strBody += "<table width=\"100%\">";
                strBody += "<tbody>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                if (dsTaskDetails.Tables[0].Rows[i]["qflag"].ToString() == "1")
                {
                    strBody += "<span style=\"color:red;\">Question posted by " + dsTaskDetails.Tables[0].Rows[i]["username"].ToString() + " on " + DateTime.Parse(dsTaskDetails.Tables[0].Rows[i]["post_date"].ToString()).ToString("dd MMM h:mm tt") + "</span>";
                }
                else
                {
                    strBody += "<span style=\"color: Green;\">comment " + dsTaskDetails.Tables[0].Rows[i]["tc_id"].ToString() + " posted by " + dsTaskDetails.Tables[0].Rows[i]["username"].ToString() + " on " + DateTime.Parse(dsTaskDetails.Tables[0].Rows[i]["post_date"].ToString()).ToString("dd MMM h:mm tt") + "</span>";
                }
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\"> </td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span>" + dsTaskDetails.Tables[0].Rows[i]["comment"].ToString() + "</span>";
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "</tbody>";
                strBody += "</table>";
                strBody += "</div>";
                strBody += "</td>";
                strBody += "</tr>";
            }
            strBody += "</tbody>";
            strBody += "</table>";
        }
        return strBody;
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {

        if (btnDelete.Text == "Delete permanently")
        {
            task_action = ((string)DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + Session["admin"].ToString() + "'")) + " has permanently deleted task ";

            emailSubject = "Task ID:" + lblTaskId.Text.Trim() + " was permanently deleted  - " + txtShortDescr.Text.Trim() + " (Task ID:" + lblTaskId.Text.Trim() + ")";

            string strHtml = generateAdminEmail(lblTaskId.Text.Trim());
            string toEmails = "";

            sql = "select email,PR_EmailNotification ";
            sql += " from users, task_subscriptions ";
            sql += " where users.username = task_subscriptions.username ";
            sql += " and task_subscriptions.task_id =" + lblTaskId.Text.Trim() + " and task_subscriptions.username <> '" + Session["admin"].ToString() + "'";

            DataSet ds = DatabaseHelper.getDataset(sql);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (toEmails == "")
                    {
                        //toEmails = ds.Tables[0].Rows[i]["email"].ToString();
                        string reqPR = "";
                        string[] strPR = ds.Tables[0].Rows[i]["PR_EmailNotification"].ToString().Split(new char[] { ';' });
                        for (int j = 0; j < strPR.Length - 1; j++)
                        {
                            reqPR = strPR[j];
                            if (reqPR == "All")
                            {
                                toEmails = ds.Tables[0].Rows[i]["email"].ToString() + ";";
                            }
                            else if (reqPR == drpPriorities.SelectedValue)
                            {
                                toEmails = ds.Tables[0].Rows[i]["email"].ToString() + ";";
                            }

                        }

                    }
                    else
                    {

                        string reqPR = "";
                        string[] strPR = ds.Tables[0].Rows[i]["PR_EmailNotification"].ToString().Split(new char[] { ';' });
                        for (int j = 0; j < strPR.Length - 1; j++)
                        {
                            reqPR = strPR[j];
                            if (reqPR == "All")
                            {
                                toEmails += ds.Tables[0].Rows[i]["email"].ToString() + ";";
                            }
                            else if (reqPR == drpPriorities.SelectedValue)
                            {
                                toEmails += ds.Tables[0].Rows[i]["email"].ToString() + ";";
                            }

                        }
                        //toEmails += ";" + ds.Tables[0].Rows[i]["email"].ToString();
                    }
                }
            }

            sql = "delete from read_comments where tc_id in (select tc_id from task_comments where task_id =" + lblTaskId.Text + "); ";
            sql += "delete from hours_reporting where task_id =" + lblTaskId.Text + "; ";
            sql += "delete from task_updates where task_id =" + lblTaskId.Text + "; ";
            sql += "delete from task_subscriptions where task_id =" + lblTaskId.Text + "; ";
            sql += "delete from task_comments where task_id =" + lblTaskId.Text + "; ";
            sql += "delete from task_attachments where task_id =" + lblTaskId.Text + "; ";
            sql += "delete from tasks where task_id =" + lblTaskId.Text + "; ";
            sql += "delete from read_task where task_id =" + lblTaskId.Text + "; ";

            int intResult = DatabaseHelper.executeNonQuery(sql);

            if (intResult != 0)
            {
                bool flag = DatabaseHelper.sendEmailTasks(toEmails, emailSubject, strHtml);

                try
                {

                    sql = @"select tc_id  from task_comments where task_id =" + lblTaskId.Text + " and  tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "')";
                    /// sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

                    DataSet dscomment = DatabaseHelper.getDataset(sql);

                    if (dscomment != null && dscomment.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < dscomment.Tables[0].Rows.Count; i++)
                        {
                            try
                            {


                                string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                                sqlinsert += " values(" + dscomment.Tables[0].Rows[i]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                                int intCommentResult = DatabaseHelper.executeNonQuery(sqlinsert);

                            }
                            catch
                            {
                            }
                        }



                    }
                }
                catch { }

                Response.Redirect("tasks.aspx");
            }
            else
            {
                divMessage1.InnerHtml = "Task " + lblTaskId.Text + "  was not deleted permanently.";
                divMessage1.Visible = true;
            }
        }
        else
        {
            sql = "update tasks set deleted=1 where task_id = " + lblTaskId.Text + "; ";
            int intResult = DatabaseHelper.executeNonQuery(sql);
            if (intResult != 0)
            {
                task_action = ((string)DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + Session["admin"].ToString() + "'")) + " has deleted task ";
                emailSubject = "Task ID:" + lblTaskId.Text.Trim() + " was deleted - " + txtShortDescr.Text.Trim() + " (Task ID:" + lblTaskId.Text.Trim() + ")";
                sendNotification(lblTaskId.Text.Trim());
                sql = @" insert into task_updates([task_id],[username],[post_date],[comment]) 
                           values(" + lblTaskId.Text.Trim() + ",'" + Session["admin"].ToString().Replace("'", "''") + "',getdate(),'Task was deleted.'); ";

                intResult = DatabaseHelper.executeNonQuery(sql);

                try
                {
                    sql = @"select tc_id  from task_comments where task_id =" + lblTaskId.Text + " and  tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "')";
                    DataSet dscomment = DatabaseHelper.getDataset(sql);

                    if (dscomment != null && dscomment.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < dscomment.Tables[0].Rows.Count; i++)
                        {
                            try
                            {


                                string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                                sqlinsert += " values(" + dscomment.Tables[0].Rows[i]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                                int intCommentResult = DatabaseHelper.executeNonQuery(sqlinsert);

                            }
                            catch
                            {
                            }
                        }



                    }
                }
                catch { }



                Response.Redirect("tasks.aspx");
            }
            else
            {
                divMessage1.InnerHtml = "Task " + lblTaskId.Text + "  was not deleted.";
                divMessage1.Visible = true;
            }
        }
    }

    protected void DataGrid1_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "edit")
        {
            Response.Redirect("edit_comment.aspx?id=" + e.Item.Cells[0].Text + "&tcid=" + e.Item.Cells[1].Text);
        }
        else if (e.CommandName == "delete")
        {
            sql = "Update task_comments set deleted=1 where tc_id=" + e.Item.Cells[1].Text;

            int intResult = DatabaseHelper.executeNonQuery(sql);

            if (intResult != 0)
            {
                string base_sql = @" insert into task_updates([task_id],[username],[post_date],[comment]) 
                            values(" + lblTaskId.Text.Trim() + ",'" + Session["admin"].ToString().Replace("'", "''") + "',getdate(),'Comment " + e.Item.Cells[1].Text + "  has been deleted.'); ";

                intResult = DatabaseHelper.executeNonQuery(base_sql);

                divMessage1.InnerHtml = "Comment " + e.Item.Cells[1].Text + "  was deleted.";

                divMessage1.Visible = true;
                ftbComment.Content = "";
                BindData();
            }
            else
            {
                divMessage1.InnerHtml = "Comment " + e.Item.Cells[1].Text + "  was not deleted.";
                divMessage1.Visible = true;
            }
        }
        else if (e.CommandName == "ReadMark")
        {
            Button readMarkButton = (Button)e.Item.FindControl("lnkReadMark");

            if (readMarkButton.Text == "[Unread Mark as read]")
            {
                sql = @"insert into read_comments([tc_id],[username]) 
                        values(" + e.Item.Cells[1].Text + ",'" + Session["admin"].ToString() + "')";
                DatabaseHelper.executeNonQuery(sql);
                BindData();
            }
            else
            {
                sql = @"delete from read_comments where [tc_id]=" + e.Item.Cells[1].Text + " and [username]='" + Session["admin"] + "'";
                DatabaseHelper.executeNonQuery(sql);
                BindData();
            }
        }
    }

    protected void DataGrid1_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemIndex != -1)
        {
            Label lblPosted = (Label)e.Item.FindControl("lblPosted");
            Button readMarkButton = (Button)e.Item.FindControl("lnkReadMark");
            //*****************************************************
            Label lblid = (Label)e.Item.FindControl("lblid");
            Label lblmacid = (Label)e.Item.FindControl("lblmacid");
            Label lblipAd = (Label)e.Item.FindControl("lblipAdd");
            Label lblMacipAd = (Label)e.Item.FindControl("lblMacipAdd");

            if (DatabaseHelper.isAdmin(Session["admin"].ToString()))
            {
                if (lblid.Text == "")
                {
                    lblid.Visible = false;
                    lblipAd.Visible = false;
                }
                else
                {
                    lblid.Visible = true;
                    lblipAd.Visible = true;
                }
                if (lblmacid.Text == "")
                {
                    lblmacid.Visible = false;
                    lblMacipAd.Visible = false;
                }
                else
                {
                    lblmacid.Visible = true;
                    lblMacipAd.Visible = true;
                }
            }
            else
            {
                lblid.Visible = false;
                lblmacid.Visible = false;
                lblipAd.Visible = false;
                lblMacipAd.Visible = false;
            }
            //*****************************************************

            if (e.Item.Cells[6].Text == "1")
            {
                lblPosted.Text = "Question posted by " + e.Item.Cells[2].Text + " for  " + e.Item.Cells[7].Text + " on " + DateTime.Parse(e.Item.Cells[3].Text).ToString("dd MMM yyyy h:mm tt");
                lblPosted.ForeColor = System.Drawing.Color.Red;
                readMarkButton.Visible = false;
            }
            else
            {
                lblPosted.Text = "comment " + e.Item.Cells[1].Text + " posted by " + e.Item.Cells[2].Text + " on " + DateTime.Parse(e.Item.Cells[3].Text).ToString("dd MMM yyyy h:mm tt");

                sql = "select count(*) from read_comments where tc_id=" + e.Item.Cells[1].Text + " and username='" + Session["admin"] + "'";

                object objIsRead = DatabaseHelper.executeScalar(sql);
                if (objIsRead.ToString() == "0")
                {
                    lblPosted.ForeColor = System.Drawing.Color.Red;
                    readMarkButton.Text = "[Unread Mark as read]";
                    readMarkButton.Style.Add(HtmlTextWriterStyle.Color, "red");
                }
                else
                {
                    lblPosted.ForeColor = System.Drawing.Color.Green;
                    readMarkButton.Text = "[Read Mark as unread]";
                    readMarkButton.Style.Add(HtmlTextWriterStyle.Color, "green");
                }
                readMarkButton.Visible = true;
            }

            ImageButton deleteButton = (ImageButton)e.Item.FindControl("lnkDelete");
            ImageButton editButton = (ImageButton)e.Item.FindControl("lnkEdit");

            //We can now add the onclick event handler
            deleteButton.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete comment " + e.Item.Cells[1].Text + "?')");

            if (!DatabaseHelper.can_Delete_Comments_Attachments(Session["admin"].ToString()))
            {
                deleteButton.Visible = false;
                editButton.Visible = false;
            }

            if (deletedFlag == true)
            {
                deleteButton.Visible = false;
                editButton.Visible = false;
            }

        }
    }

    protected void DataGrid2_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "edit")
        {
            Response.Redirect("edit_comment.aspx?id=" + e.Item.Cells[0].Text + "&tcid=" + e.Item.Cells[1].Text);
        }
        else if (e.CommandName == "delete")
        {
            sql = "Update task_comments set deleted=1 where tc_id=" + e.Item.Cells[1].Text;

            int intResult = DatabaseHelper.executeNonQuery(sql);

            if (intResult != 0)
            {
                divMessage1.InnerHtml = "Comment " + e.Item.Cells[1].Text + "  was deleted.";

                divMessage1.Visible = true;
                ftbComment.Content = "";
                BindData();
            }
            else
            {
                divMessage1.InnerHtml = "Comment " + e.Item.Cells[1].Text + "  was not deleted.";
                divMessage1.Visible = true;
            }
        }
        else if (e.CommandName == "ReadMark")
        {
            LinkButton readMarkButton = (LinkButton)e.Item.FindControl("lnkReadMark");

            if (readMarkButton.Text == "[Unread Mark as read]")
            {
                sql = @"insert into read_comments([tc_id],[username]) 
                        values(" + e.Item.Cells[1].Text + ",'" + Session["admin"] + "')";
                DatabaseHelper.executeNonQuery(sql);
                BindData();
            }
            else
            {
                sql = @"delete from read_comments where [tc_id]=" + e.Item.Cells[1].Text + " and [username]='" + Session["admin"] + "'";
                DatabaseHelper.executeNonQuery(sql);
                BindData();
            }
        }
    }

    protected void DataGrid2_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemIndex != -1)
        {
            Label lblPosted = (Label)e.Item.FindControl("lblPosted");
            Button readMarkButton = (Button)e.Item.FindControl("lnkReadMark");


            if (e.Item.Cells[7].Text == "C")
            {
                if (e.Item.Cells[8].Text == "1")
                {
                    lblPosted.Text = "Question posted by " + e.Item.Cells[2].Text + " on " + DateTime.Parse(e.Item.Cells[3].Text).ToString("dd MMM h:mm tt");
                    lblPosted.ForeColor = System.Drawing.Color.Red;
                    readMarkButton.Visible = false;
                }
                else
                {
                    lblPosted.Text = "comment " + e.Item.Cells[1].Text + " posted by " + e.Item.Cells[2].Text + " on " + DateTime.Parse(e.Item.Cells[3].Text).ToString("dd MMM h:mm tt");

                    sql = "select count(*) from read_comments where tc_id=" + e.Item.Cells[1].Text + " and username='" + Session["admin"] + "'";

                    object objIsRead = DatabaseHelper.executeScalar(sql);
                    if (objIsRead.ToString() == "0")
                    {
                        lblPosted.ForeColor = System.Drawing.Color.Red;
                        readMarkButton.Text = "[Unread Mark as read]";
                        readMarkButton.Style.Add(HtmlTextWriterStyle.Color, "red");
                    }
                    else
                    {
                        lblPosted.ForeColor = System.Drawing.Color.Green;
                        readMarkButton.Text = "[Read Mark as unread]";
                        readMarkButton.Style.Add(HtmlTextWriterStyle.Color, "green");
                    }
                    readMarkButton.Visible = true;
                }
            }
            else
            {
                ((Label)e.Item.FindControl("lblPosted")).Text = "changed by " + e.Item.Cells[2].Text + " on " + DateTime.Parse(e.Item.Cells[3].Text).ToString("dd MMM h:mm tt");
                readMarkButton.Visible = false;
            }

            LinkButton deleteButton = (LinkButton)e.Item.FindControl("lnkDelete");
            LinkButton editButton = (LinkButton)e.Item.FindControl("lnkEdit");

            //We can now add the onclick event handler
            deleteButton.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete comment " + e.Item.Cells[1].Text + "?')");

            if (!DatabaseHelper.can_Delete_Comments_Attachments(Session["admin"].ToString()) || e.Item.Cells[7].Text == "U" || e.Item.Cells[6].Text == "1")
            {
                deleteButton.Visible = false;
                editButton.Visible = false;
            }
            if (deletedFlag == true)
            {
                deleteButton.Visible = false;
                editButton.Visible = false;
            }
        }
    }

    void load_dropdowns()
    {
        // projects
        sql = @"select project_name
		from projects
		where archive!='Y' and active = 'Y' order by project_name;";

        // categories
        sql += "\nselect category_name from categories order by sort_seq, category_name;";

        // priorities
        sql += "\nselect priority_name from priorities order by sort_seq, priority_name;";

        // statuses
        sql += "\nselect status_name from statuses order by sort_seq, status_name;";

        // users
        sql += "\nselect username from users where IsCW='Y' and active = 'Y' order by username;";

        //=================================================================================
        //Usermails
        sql += "\nselect username,user_id from users where active = 'Y' order by username;";
        //=================================================================================
        // do a batch of sql statements
        DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);
        if (ds_dropdowns.Tables[0] != null)
        {
            drpProjects.DataSource = ds_dropdowns.Tables[0];
            drpProjects.DataTextField = "project_name";
            drpProjects.DataValueField = "project_name";
            drpProjects.DataBind();
            drpProjects.Items.Insert(0, new ListItem("[no project]", ""));


        }

        if (ds_dropdowns.Tables[1] != null)
        {
            drpCategories.DataSource = ds_dropdowns.Tables[1];
            drpCategories.DataTextField = "category_name";
            drpCategories.DataValueField = "category_name";
            drpCategories.DataBind();
            drpCategories.Items.Insert(0, new ListItem("[no category]", ""));
        }
        if (ds_dropdowns.Tables[2] != null)
        {
            drpPriorities.DataSource = ds_dropdowns.Tables[2];
            drpPriorities.DataTextField = "priority_name";
            drpPriorities.DataValueField = "priority_name";
            drpPriorities.DataBind();
            drpPriorities.Items.Insert(0, new ListItem("[no priority]", ""));
        }
        if (ds_dropdowns.Tables[3] != null)
        {
            drpStatuses.DataSource = ds_dropdowns.Tables[3];
            drpStatuses.DataTextField = "status_name";
            drpStatuses.DataValueField = "status_name";
            drpStatuses.DataBind();
            drpStatuses.Items.Insert(0, new ListItem("[no status]", ""));
        }
        if (ds_dropdowns.Tables[4] != null)
        {
            drpUsers.DataSource = ds_dropdowns.Tables[4];
            drpUsers.DataTextField = "username";
            drpUsers.DataValueField = "username";
            drpUsers.DataBind();
            drpUsers.Items.Insert(0, new ListItem("[not assigned]", ""));
        }
        if (ds_dropdowns.Tables[5] != null)
        {
            drpbinduser.DataSource = ds_dropdowns.Tables[5];
            drpbinduser.DataTextField = "username";
            drpbinduser.DataValueField = "user_id";
            drpbinduser.DataBind();
            drpbinduser.Items.Insert(0, new ListItem("[not assigned]", ""));

            drpAddRecipient.DataSource = ds_dropdowns.Tables[5];
            drpAddRecipient.DataTextField = "username";
            drpAddRecipient.DataValueField = "user_id";
            drpAddRecipient.DataBind();
            drpAddRecipient.Items.Insert(0, new ListItem("[add recipients]", ""));
        }
    }


    void Bindmailcheckboxlist(int taskId)
    {
        //=================================================================
        sql = "select email, EmailNotification,PR_EmailNotification ,Firstname,user_id ";
        sql += " from users, task_subscriptions ";
        sql += " where users.username = task_subscriptions.username ";
        sql += " and task_subscriptions.task_id =" + taskId.ToString() + " and task_subscriptions.username <> '" + Session["admin"].ToString() + "'";
        DataSet ds_chkboxlst = DatabaseHelper.getDataset(sql);
        chkmaillst.DataSource = ds_chkboxlst.Tables[0];
        chkmaillst.DataTextField = "Firstname";
        chkmaillst.DataValueField = "user_id";
        chkmaillst.DataBind();
        for (int ichk = 0; ichk < ds_chkboxlst.Tables[0].Rows.Count; ichk++)
        {
            chkmaillst.Items[ichk].Selected = true;

        }
        //=================================================================

    }

    void set_default_selection()
    {
        sql = "\nselect top 1 project_name from projects where default_selection = 'Y' order by project_name;"; // 0
        sql += "\nselect top 1 category_name from categories where default_selection = 'Y' order by category_name;";  // 1
        sql += "\nselect top 1 priority_name from priorities where default_selection = 'Y' order by priority_name;"; // 2
        sql += "\nselect top 1 status_name from statuses where default_selection = 'Y' order by status_name;"; // 3

        DataSet ds_defaults = DatabaseHelper.getDataset(sql);

        string default_value;

        // projects
        if (ds_defaults.Tables[0].Rows.Count > 0)
        {
            default_value = ds_defaults.Tables[0].Rows[0][0].ToString();
        }
        else
        {
            default_value = "0";
        }
        foreach (ListItem li in drpProjects.Items)
        {
            if (li.Value == default_value)
            {
                li.Selected = true;
            }
            else
            {
                li.Selected = false;
            }
        }

        // categories
        if (ds_defaults.Tables[1].Rows.Count > 0)
        {
            default_value = ds_defaults.Tables[1].Rows[0][0].ToString();
        }
        else
        {
            default_value = "0";
        }
        foreach (ListItem li in drpCategories.Items)
        {
            if (li.Value == default_value)
            {
                li.Selected = true;
            }
            else
            {
                li.Selected = false;
            }
        }
        // priorities
        if (ds_defaults.Tables[2].Rows.Count > 0)
        //if (drpPriorities.Items.Count > 0)
        {

            default_value = ds_defaults.Tables[2].Rows[0][0].ToString();
            // default_value = "1c - normal";
        }
        else
        {
            default_value = "0";
        }
        foreach (ListItem li in drpPriorities.Items)
        {
            if (li.Value == default_value)
            {
                li.Selected = true;
            }
            else
            {
                li.Selected = false;
            }
        }

        // statuses
        if (ds_defaults.Tables[3].Rows.Count > 0)
        {
            default_value = ds_defaults.Tables[3].Rows[0][0].ToString();
        }
        else
        {
            default_value = "0";
        }
        foreach (ListItem li in drpStatuses.Items)
        {
            if (li.Value == default_value)
            {
                li.Selected = true;
            }
            else
            {
                li.Selected = false;
            }
        }
    }

    void generate_task_from_client_request()
    {
        sql = "\nSelect * from ClientRequest where RequestId=" + reqid.ToString() + ";";

        sql += "\nSelect * from ClientRequest_Details where RequestId=" + reqid.ToString() + " order by CommentId desc";

        DataSet dsClientRequest = DatabaseHelper.getDataset(sql);

        // change request
        if (dsClientRequest.Tables[0].Rows.Count > 0)
        {
            txtShortDescr.Text = dsClientRequest.Tables[0].Rows[0]["ShortDescr"].ToString();
            txtRelevantURL.Text = dsClientRequest.Tables[0].Rows[0]["WebsiteURL"].ToString();
        }

        // merge change request comments
        if (dsClientRequest.Tables[1].Rows.Count > 0)
        {
            ftbComment.Content = "";

            for (int i = 0; i < dsClientRequest.Tables[1].Rows.Count; i++)
            {
                ftbComment.Content += dsClientRequest.Tables[1].Rows[i]["Comment"].ToString() + "<br/><hr/><br/>";
            }
        }
    }

    void add_subscription(object objTaskid)
    {
        int intResult;

        sql = "select username from users where subscribe_to_all_tasks='Y' and username != '" + Session["admin"].ToString().Replace("'", "''") + "' ";

        if (drpUsers.SelectedIndex != 0)
        {
            sql += " and username != '" + drpUsers.SelectedValue.Replace("'", "''") + "'";
        }

        DataSet dsSubscr = DatabaseHelper.getDataset(sql);

        if (dsSubscr != null && dsSubscr.Tables.Count > 0 && dsSubscr.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < dsSubscr.Tables[0].Rows.Count; i++)
            {
                sql = "insert into task_subscriptions ([task_id],[username]) ";
                sql += "values(" + objTaskid.ToString() + ",'" + dsSubscr.Tables[0].Rows[i]["username"].ToString() + "')";

                intResult = DatabaseHelper.executeNonQuery(sql);
            }
        }

        sql = "insert into task_subscriptions ([task_id],[username]) ";
        sql += "values(" + objTaskid.ToString() + ",'" + Session["admin"].ToString().Replace("'", "''") + "'); ";

        if (drpUsers.SelectedIndex > 0 && (drpUsers.SelectedValue != Session["admin"].ToString()))
        {
            sql += " insert into task_subscriptions ([task_id],[username]) ";
            sql += "values(" + objTaskid.ToString() + ",'" + drpUsers.SelectedValue.Replace("'", "''") + "'); ";
        }

        intResult = DatabaseHelper.executeNonQuery(sql);
    }

    void add_taskAttachments(object objTaskid)
    {
        int intResult;

        sql = "select * from ClientRequest_Attachment where RequestId=" + reqid.ToString();

        DataSet dsAttachments = DatabaseHelper.getDataset(sql);

        if (dsAttachments != null && dsAttachments.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < dsAttachments.Tables[0].Rows.Count; i++)
            {
                sql = "insert into task_attachments([task_id],[Pkey],[FileName],[Description],[UploadedBy],[UploadedOn])";
                sql += "values(" + objTaskid.ToString() + ",";
                sql += "'" + dsAttachments.Tables[0].Rows[i]["Pkey"].ToString() + "',";
                sql += "'" + dsAttachments.Tables[0].Rows[i]["FileName"].ToString() + "',";
                sql += "'" + dsAttachments.Tables[0].Rows[i]["Description"].ToString() + "',";
                sql += "'" + Session["admin"].ToString().Replace("'", "''") + "',";
                sql += "getdate())";

                intResult = DatabaseHelper.executeNonQuery(sql);
            }
        }
    }

    

    void register1aTaskAllocated()
    {
        arr1aAssignedUsers.Clear();
        sql = @"select distinct assigned_to_user from tasks where tasks.status <> 'Checked' and tasks.status <>'parked' and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.priority = '1a - DO NOW' ";
        DataSet ds = DatabaseHelper.getDataset(sql);
        foreach (DataRow row in ds.Tables[0].Rows)
        {
            try
            {
                if ((row["assigned_to_user"] != null && row["assigned_to_user"].ToString().Trim() != ""))
                {
                    arr1aAssignedUsers.Add(row["assigned_to_user"]);
                }
            }
            catch { }
        }
        ClientScriptManager cs = Page.ClientScript;
        string str1aAssignedUsers = "";
        if (arr1aAssignedUsers.Count > 0)
        {
            for (int j = 0; j < arr1aAssignedUsers.Count; j++)
            {
                try
                {
                    str1aAssignedUsers += "'" + arr1aAssignedUsers[j] + "',";
                }
                catch { }
            }
        }
        str1aAssignedUsers = str1aAssignedUsers.TrimEnd(new char[] { ',' });
        if (cs.IsClientScriptBlockRegistered("MyArr1aAssignedUsers") == false)
        {
            cs.RegisterArrayDeclaration("MyArr1aAssignedUsers", str1aAssignedUsers);
        }
    }
    void registerImmiAllocated()
    {
        arrImmidateUsers.Clear();
        string sqlImmi = "";
        sqlImmi = @"select distinct assigned_to_user from tasks where tasks.status <> 'Checked' and tasks.status <>'parked' and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.priority = '0 - IMMEDIATE' ";

        DataSet dsImmi = DatabaseHelper.getDataset(sqlImmi);

        foreach (DataRow row in dsImmi.Tables[0].Rows)
        {
            try
            {
                if ((row["assigned_to_user"] != null && row["assigned_to_user"].ToString().Trim() != ""))
                {
                    arrImmidateUsers.Add(row["assigned_to_user"]);
                }
            }
            catch { }
        }

        ClientScriptManager csImmi = Page.ClientScript;

        string str1aImmidiateUsers = "";

        if (arrImmidateUsers.Count > 0)
        {
            for (int j = 0; j < arrImmidateUsers.Count; j++)
            {
                try
                {
                    str1aImmidiateUsers += "'" + arrImmidateUsers[j] + "',";
                }
                catch { }
            }
        }

        str1aImmidiateUsers = str1aImmidiateUsers.TrimEnd(new char[] { ',' });

        if (csImmi.IsClientScriptBlockRegistered("MyArrImmidiateUsers") == false)
        {
            csImmi.RegisterArrayDeclaration("MyArrImmidiateUsers", str1aImmidiateUsers);
        }


    }

    protected void lnkNudge_Click(object sender, EventArgs e)
    {
        string FullName = "";
        sql = "select email, firstname +' '+ lastname as name ";
        sql += " from users, tasks";
        sql += " where users.username = tasks.assigned_to_user ";
        sql += " and tasks.task_id =" + lblTaskId.Text.Trim();

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            string toEmails = "";

            toEmails = ds.Tables[0].Rows[0]["email"].ToString();
            FullName = ds.Tables[0].Rows[0]["name"].ToString();

            emailSubject = ((string)DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + Session["admin"].ToString() + "'")) + " has nudged you regarding the task " + lblTaskId.Text.Trim();

            emailSubject = emailSubject.ToUpper();

            task_action = ((string)DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + Session["admin"].ToString() + "'")) + " has nudged you regarding the task ";

            task_action = task_action.ToUpper();

            string strHtml = generateNudgeEmail(lblTaskId.Text.Trim());

            bool flag = DatabaseHelper.sendEmailTasks(toEmails, emailSubject, strHtml);

            Session["message"] = "A nudge was sent to " + FullName + ".";

            Response.Redirect("edit_task.aspx?id=" + lblTaskId.Text.Trim());
        }
        else
        {
            Session["message"] = "Error while sending Nudge to task owner. Please confirm owner email address have been exist in database.";

            Response.Redirect("edit_task.aspx?id=" + lblTaskId.Text.Trim());
        }
    }

    private string generateNudgeEmail(string strTaskId)
    {
        DataSet dsTask = DatabaseHelper.getDataset("select * from tasks where task_id=" + strTaskId);
        string strBody = "";
        if (dsTask == null || dsTask.Tables.Count <= 0 || dsTask.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "Task not found.<br><br><a href='tasks.aspx'>View tasks</a>";
        }
        else
        {
            strBody += "Task: <span style=\"color:red\">";

            strBody += task_action;

            strBody += " (<a style=\"color:red\" href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_task.aspx?id=" + strTaskId + "'>" + strTaskId + "</a>) </span> " + DateTime.Parse(DateTime.Now.ToString()).ToString("dd MMM h:mm tt") + "<br><br>";

            if (lblTaskId.Text != "New")
            {
                string attribures = record_changes_for_email();

                if (attribures.ToString().Trim() != "")
                {
                    strBody += "The attributes below have been amended.<br>";
                    strBody += "<span style=\"color:red\">" + attribures + "</span><br>";
                }
            }

            strBody += "<a href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_task.aspx?id=" + strTaskId + "'>" + txtShortDescr.Text.Trim() + "</a>&nbsp;&nbsp;&nbsp;(" + dsTask.Tables[0].Rows[0]["priority"].ToString() + " : " + dsTask.Tables[0].Rows[0]["status"].ToString() + ")<br><br>";
            strBody += "Emp: " + ((string)DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + dsTask.Tables[0].Rows[0]["assigned_to_user"].ToString() + "'")) + "<br>";
            strBody += "Project: " + dsTask.Tables[0].Rows[0]["project"].ToString() + "<br>";
            strBody += "Relevant URL: <a href='" + dsTask.Tables[0].Rows[0]["Relevant_URL"].ToString() + "' target='_blank'>" + dsTask.Tables[0].Rows[0]["Relevant_URL"].ToString() + "</a><br>";
            strBody += "<br><br>";

            sql = "Select * from task_comments where task_id=" + strTaskId.ToString() + " and deleted <> 1 order by tc_id desc";

            DataSet dsTaskDetails = DatabaseHelper.getDataset(sql);

            strBody += "<table cellspacing=\"0\" border=\"1\" style=\"width: 99%;border-collapse: collapse;\">";
            strBody += "<tbody>";

            for (int i = 0; i < dsTaskDetails.Tables[0].Rows.Count; i++)
            {
                strBody += "<tr>";
                strBody += "<td>";
                strBody += "<div style=\"border: 1px none Green;\">";
                strBody += "<table width=\"100%\">";
                strBody += "<tbody>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                if (dsTaskDetails.Tables[0].Rows[i]["qflag"].ToString() == "1")
                {
                    strBody += "<span style=\"color:red;\">Question posted by " + dsTaskDetails.Tables[0].Rows[i]["username"].ToString() + " on " + DateTime.Parse(dsTaskDetails.Tables[0].Rows[i]["post_date"].ToString()).ToString("dd MMM h:mm tt") + "</span>";
                }
                else
                {
                    strBody += "<span style=\"color: Green;\">comment " + dsTaskDetails.Tables[0].Rows[i]["tc_id"].ToString() + " posted by " + dsTaskDetails.Tables[0].Rows[i]["username"].ToString() + " on " + DateTime.Parse(dsTaskDetails.Tables[0].Rows[i]["post_date"].ToString()).ToString("dd MMM h:mm tt") + "</span>";
                }
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\"> </td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span>" + dsTaskDetails.Tables[0].Rows[i]["comment"].ToString() + "</span>";
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "</tbody>";
                strBody += "</table>";
                strBody += "</div>";
                strBody += "</td>";
                strBody += "</tr>";
            }
            strBody += "</tbody>";
            strBody += "</table>";
        }
        return strBody;
    }

    void ensureCurrentlyWorkingOnStatus()
    {

        sql = "select count(*) from tasks where assigned_to_user ='" + Session["admin"] + "' and deleted <> 1 and status <>'closed' and task_id <> " + id.ToString() + " and (status = 'CWO' or status = 'CWO but away') ";

        object count = DatabaseHelper.executeScalar(sql);

        if (count.ToString() != "0")
        {

            hdnStatusFlag.Value = "true";

        }
        else
        {
            hdnStatusFlag.Value = "false";
        }
    }

    protected int getHoursTakenSoFar(string task_id)
    {
        sql = @"select sum(datediff(minute,started_date,finished_date))
                from hours_reporting
                where finished_date is not null and task_id=" + task_id.ToString();

        int hrsTaken = 0;
        object objHrsTaken = DatabaseHelper.executeScalar(sql);
        if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
        {
            hrsTaken += (int)objHrsTaken;
        }
        else
        {
            hrsTaken = 0;
        }

        sql = @"select sum(datediff(minute,started_date,getdate()))
                from hours_reporting
                where finished_date is null and task_id=" + task_id.ToString();

        objHrsTaken = DatabaseHelper.executeScalar(sql);

        if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
        {
            hrsTaken += (int)objHrsTaken;
        }

        return hrsTaken;
    }


    
    protected void LnkTskNewcomment_Click(object sender, EventArgs e)
    {

        Session["filterunread"] = "TaskNewComment";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        Response.Redirect("tasks.aspx");


    }
   
    protected void Admin_Header1_Load(object sender, EventArgs e)
    {

    }
    protected void LnkRelated_Click(object sender, EventArgs e)
    {
        Session["ProjectRelated"] = drpProjects.SelectedValue;
        //Response.Redirect("Related.aspx", false);
        Response.Redirect("http://www.esgdesk.com/admin/projectrequests.aspx?projectname=" + drpProjects.SelectedValue);

    }
    
    

    protected void Lnkcomprelated_Click(object sender, EventArgs e)
    {
        Session["CompanyRelated"] = hdnProjectCompany.Value.ToString();
        Response.Redirect("CompanyRelated.aspx", false);

    }
    protected void drpProjects_SelectedIndexChanged(object sender, EventArgs e)
    {
        string sqlcompany = "select [CompanyName] from [projects]  where [project_name] = '" + drpProjects.SelectedValue.ToString() + "'";
        object objResult = DatabaseHelper.executeScalar(sqlcompany);
        if (objResult != null)
        {
            hdnProjectCompany.Value = objResult.ToString();
        }

    }
    //================================================================================================================================================
    //protected void CompanyNotes_Click(object sender, EventArgs e)
    //{
    //    Session["TaskComp"] = hdnProjectCompany.Value;
    //    //Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> window.open('companyNotes.aspx','','status=no,scrollbars=yes,position=center,resizable=yes,maximize=yes'); </script>");
    //    Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> window.open('companyNotes.aspx','_blank'); </script>");
    //}
    //protected void CRNotes_Click(object sender, EventArgs e)
    //{
    //    Session["TaskComp"] = hdnProjectCompany.Value;
    //    //Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> window.open('CRNotes.aspx','','status=no,scrollbars=yes,position=center,resizable=yes,maximize=yes'); </script>");
    //    Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> window.open('CRNotes.aspx','_blank'); </script>");
    //}
    //================================================================================================================================================
    protected void BtnDelAllCmnt_Click(object sender, EventArgs e)
    {

        int i = 0;

        try
        {
            //=====================================================================================
            foreach (DataGridItem row in DataGrid1.Items)
            {

                CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                Label lbl = (Label)row.FindControl("lbltc_id");
                if (cb != null)
                {
                    if (cb.Checked)
                    {
                        i = i + 1;
                        string commentId = lbl.Text;
                        //==================================================================
                        sql = "Update task_comments set deleted=1 where tc_id=" + commentId.ToString() + "";
                        int intResult = DatabaseHelper.executeNonQuery(sql);
                        if (intResult != 0)
                        {
                            string base_sql = @" insert into task_updates([task_id],[username],[post_date],[comment]) 
                                     values(" + lblTaskId.Text.Trim() + ",'" + Session["admin"].ToString().Replace("'", "''") + "',getdate(),'Comment " + commentId.ToString() + "  has been deleted.'); ";

                            intResult = DatabaseHelper.executeNonQuery(base_sql);
                        }
                        //===================================================================
                    }
                }
            }
            chkSelectAll.Checked = false;
            //=====================================================================================
        }
        catch { }

        if (i > 0)
        {

            divMessage1.InnerHtml = "Selected comments deleted.";
        }
        else
        {

        }

        BindData();
    }

    protected void btnIncluderead_Click(object sender, EventArgs e)
    {
        BindData();
    }

    protected void btnCheckAllasRead_Click(object sender, EventArgs e)
    {

        sql = @"select *  from task_comments where task_id =" + lblTaskId.Text + " and  tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "')";
        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {


                    string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                    sqlinsert += " values(" + ds.Tables[0].Rows[i]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                    int intResult = DatabaseHelper.executeNonQuery(sqlinsert);

                }
                catch
                {
                }
            }

            BindData();

            // }
        }
    }
    protected void btnCheckAllToDelete_Click(object sender, EventArgs e)
    {
        bool checkState;

    }
    protected void BtnAnalytics_Click(object sender, EventArgs e)
    {

        sql = @"select *  from projects where project_name ='" + TaskProject.Value + "'";

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            if (ds.Tables[0].Rows[0]["SEO"].ToString() == "True")
            {

                Session["GoogleUser"] = ds.Tables[0].Rows[0]["GoogleUserId"].ToString();
                Session["GooglePwd"] = ds.Tables[0].Rows[0]["GooglePwd"].ToString();
                Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> window.open('Analytics.aspx','','scrollbars=yes,width=620,height=580'); </script>");


            }
            else
            {

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Analytics  not added for this project.');", true);
            }
        }


    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {


    }
    protected void btnRemove_Click(object sender, EventArgs e)
    {

    }


    protected void BtnRemoveStrtDate_Click(object sender, EventArgs e)
    {
        string startdate = "";
        sql = " update tasks set ";
        sql += " [StartDate]='" + startdate + "',";
        sql += " [last_updated_user]='" + Session["admin"].ToString().Replace("'", "''") + "',";
        sql += " [last_updated_date]=getdate()";
        sql += " where task_id = " + lblTaskId.Text.Trim() + "; ";

        int intResult = DatabaseHelper.executeNonQuery(sql);
        if (intResult != 0)
        {
            Response.Redirect("edit_task.aspx?id=" + lblTaskId.Text.Trim());
        }


    }
    protected void BtnRmvDueDate_Click(object sender, EventArgs e)
    {
        string duedate = "";
        sql = " update tasks set ";
        sql += " [DueDate]='" + duedate + "',";
        sql += " [last_updated_user]='" + Session["admin"].ToString().Replace("'", "''") + "',";
        sql += " [last_updated_date]=getdate()";
        sql += " where task_id = " + lblTaskId.Text.Trim() + "; ";

        int intResult = DatabaseHelper.executeNonQuery(sql);
        if (intResult != 0)
        {
            Response.Redirect("edit_task.aspx?id=" + lblTaskId.Text.Trim());
        }
    }
    protected void lblAssignedTo_Click(object sender, EventArgs e)
    {

    }

    protected void btnAllasRead_Click(object sender, EventArgs e)
    {
        int i = 0;
        try
        {
            //=====================================================================================
            foreach (DataGridItem row in DataGrid1.Items)
            {

                CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                Label lbl = (Label)row.FindControl("lbltc_id");
                if (cb != null)
                {
                    if (cb.Checked)
                    {
                        i = i + 1;
                        string commentId = lbl.Text;
                        string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                        sqlinsert += " values('" + commentId + "','" + Session["admin"].ToString() + "')";

                        int intResult = DatabaseHelper.executeNonQuery(sqlinsert);


                    }

                }
            }
            //====================================================================================
            chkSelectAll.Checked = false;

        }
        catch { }

        BindData();


        if (i == 0)
        {
            divMessage1.InnerHtml = "There is no task which is unread";

        }

    }

}
