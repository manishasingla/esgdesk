﻿<%@ Page Language="C#" AutoEventWireup="true" ValidateRequest="false" CodeFile="edit_task.aspx.cs"  Inherits="Admin_edit_task" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Src="~/Admin/demoNotifications.ascx" TagName="Notifications" TagPrefix="uc5" %>
<%--<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>--%>
<%@ Register Src="~/Admin/footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit.HTMLEditor" tagprefix="cc2" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
	<title>Edit task</title>
    <style type="text/css">
        .margin-test{ width:auto !important; float:left;}
        .margin-test-1{ width:auto !important; float:none; position:absolute; left:116px;}
    </style>
	<script type="text/javascript" language="javascript">
        function confirmSubmit(mycheckbox) {
            var agree = confirm('Are you sure you want to delete all comments?');
            if (agree) {
                setTimeout('__doPostBack(\'gvQuotes$ctl07$btnDelete\',\'\')', 0); return true;
            } else {
                mycheckbox.checked = false;
                return false;
            }
        }

        function DeletedBtn() {
            document.getElementById('<%#BtnDelAllCmnt.ClientID %>').click();
        }
        function readBtn() {

           document.getElementById('<%#btnAllasRead.ClientID %>').click();
        }


        function validate(sp) {

            var icount = 0;
            if (sp.checked) {
                if (parseInt(document.getElementById("checkboxchecked").value) == 0) {
                    icount = parseInt(document.getElementById("checkboxchecked").value) + 1;
                }
                else {
                    icount = parseInt(document.getElementById("checkboxchecked").value) + 1;
                }
                document.getElementById("checkboxchecked").value = icount;
                
            }
            else
             {
               icount = parseInt(document.getElementById("checkboxchecked").value) - 1;
                document.getElementById("checkboxchecked").value = icount;
             }

            var chkGridcontrols = document.getElementById("<%#DataGrid1.ClientID%>").getElementsByTagName("input");
            for (var i = 0; i < chkGridcontrols.length; i++) {
                if (!chkGridcontrols[i].checked) {
                    document.getElementById("chkSelectAll").checked = false;
                }
            }

            if (icount > 0) {
                //alert(icount);
            }

        }

        function validateOnbtn() {
            var icount = parseInt(document.getElementById("checkboxchecked").value);
            var count = 0;
            var chkGridcontrols = document.getElementById("<%#DataGrid1.ClientID%>").getElementsByTagName("input");
            for (var i = 0; i < chkGridcontrols.length; i++) {
                if (chkGridcontrols[i].checked) {
                    count = 1;
                }
            }

            if (count > 0) {
                if (!confirm('Are you sure that you want to delete the checked comments?')) {
                    return false;
                }
                else {
                    document.getElementById("checkboxchecked").value = 0;
                    return true;
                }

            }
            else if (count == 0) {
                document.getElementById("checkboxchecked").value = 0;
                alert('Please select at least one comment')
                return false;
            }
        }

        
        function ReadSomeComment() {
            var flag = false;
            var count = 0;
            var chkGridcontrols = document.getElementById("<%#DataGrid1.ClientID%>").getElementsByTagName("input");
            for (var i = 0; i < chkGridcontrols.length; i++) {
                if (chkGridcontrols[i].checked) {
                    count = 1;
                    flag = true;
                }
            }
            if (count == 0) {
                //document.getElementById("checkboxchecked").value=0;
                alert('Please select at least one comment')
                flag = false;
            }
            else {
                document.getElementById("checkboxchecked").value = 0;
                flag = true;

            }
            return flag;
        }

        function hide() {
            //  alert("hide");

            var l = document.getElementById("lnk_h_s");
            var h = document.getElementById("d_h_s");
            var i = document.getElementById("img1");


            if (l.innerHTML == "Add new comment") {
                l.innerHTML = "Close comment box"
                i.src = "../images/minus-ico-esg.png"
                h.style.display = "block";
            }
            else {
                l.innerHTML = "Add new comment"
                i.src = "../images/plus-ico-esg.png"
                h.style.display = "none";
            }



        }
        function hide1() {
            var l1 = document.getElementById("lhk_h");
            var i = document.getElementById("img");
            var h1 = document.getElementById("t_h_s");


            if (l1.innerHTML == "Edit attributes") {
                l1.innerHTML = "Close attributes"
                i.src = "../images/minus-ico-esg.png"
                h1.style.display = "block";

            }
            else {
                l1.innerHTML = "Edit attributes"
                i.src = "../images/plus-ico-esg.png"
                h1.style.display = "none";
            }
        }
        function hide2() {
            var l2 = document.getElementById("SPU");
            var i = document.getElementById("img3");
            var h2 = document.getElementById("S_P_U");


            if (l2.innerHTML == "Edit Recipients") {
                l2.innerHTML = "Close Recipients"
                i.src = "../images/minus-ico-esg.png"
                h2.style.display = "block";

            }
            else {
                l2.innerHTML = "Edit Recipients"
                i.src = "../images/plus-ico-esg.png"
                h2.style.display = "none";
            }
        }


        function setCheckAll() {
            var chkboxLet = document.getElementById("chkSelectAll");
            var chkGridcontrols = document.getElementById("<%#DataGrid1.ClientID%>").getElementsByTagName("input");
            for (var i = 0; i < chkGridcontrols.length; i++) {
                chkGridcontrols[i].checked = chkboxLet.checked;
               
            }
            if (document.getElementById("chkSelectAll").checked == true)
                document.getElementById("checkboxchecked").value = 1;
            else
                document.getElementById("checkboxchecked").value = 0;
            return true;
        }
    </script>
	<script type="text/javascript">

        function OpenPopup(w, h) {
            var left = (screen.width / 2) - (w / 2);
            var top = (screen.height / 2) - (h / 2);
            var q = "EmployeesReports.aspx?id=" + document.getElementById("hfurl").value;
            window.open(q, "List", "scrollbars=yes,menubar=no,toolbar=no,location=center,resizable=no,");
            //    if (window.focus) {newwindow.focus()}

            return false;

        }
        function PopupCenter(pageURL, title, w, h) {
            var left = (screen.width / 2) - (w / 2);
            var top = (screen.height / 2) - (h / 2);
            var targetWin = window.open(pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
        } 
    </script>
	<script language="javascript" type="text/javascript" src="calendarDateInput.js"></script>
	<link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
	<script language="javascript" type="text/javascript">
        function beforeAddorRemove(flag) {
            if (document.getElementById("txtIRN").value == "") {
                alert("Please enter internal reference number.");
                return false;
            }

            if (flag == 1) {
                if (confirm('Are you sure you want to remove internal reference number?')) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }
    </script>
	<script type="text/javascript">
        function checkDate(sender, args) {

            if (sender._selectedDate < new Date().format(sender._format)) {
                alert("You cannot select a day earlier than today!");
                sender._selectedDate = new Date();

                document.getElementById("txtStartDate").innerText = "";
            }
        }

        function checkDate1(sender, args) {
            if (sender._selectedDate < new Date().format(sender._format)) {
                alert("You cannot select a day earlier than today!");
                sender._selectedDate = new Date();

                document.getElementById("txtDueDate").innerText = "";
            }
        }


        function Shown(sender, args) {
            alert('start');
            alert(sender);
            alert(args);
            var CalendarExt1 = $find('CalendarExtender1'); // Note: CalendarExtBehavior1 is the BehaviorID I gave to the CalendarExtender for date 1
            var todayDate = new Date();
           
            sender._selectedDate = todayDate;
           
        }

        function showDate(sender, args) {
            alert('start2');
            alert(sender);
            alert(args);
            if (sender._textbox.get_element().value == "") {
                var todayDate = new Date();
                sender._selectedDate = todayDate;
            }
        }
    </script>
	<script language="javascript" type="text/javascript">
        function checkChecked() {
            if (document.getElementById("chkShowAllHistory").checked == true) {
                document.getElementById("divNoHistory").style.display = "none";
                document.getElementById("divAllHistory").style.display = "block";
                document.getElementById("allread").style.display = "none";
                document.getElementById("someDelete").style.display = "none";


            }
            else {
                document.getElementById("divNoHistory").style.display = "block";
                document.getElementById("divAllHistory").style.display = "none";
                document.getElementById("allread").style.display = "none";
                document.getElementById("someDelete").style.display = "none";
            }
        }       
    </script>
	<script language="javascript" type="text/javascript">
        function GotoIncludeRead() {
           document.getElementById('btnIncluderead').click();
            return true;
       }
    
    </script>
	<script language="javascript" type="text/javascript">
        function GotoChaekAllread() {
            document.getElementById('btnCheckAllasRead').click();
            return true;
        }
   </script>
	<script language="javascript" type="text/javascript">
        function GotoChaekAllDel() {
            document.getElementById('btnCheckAllToDelete').click();
            return true;
        }
    </script>
	<script language="javascript" type="text/javascript">
        function window.confirm(str) {
            execScript('n = msgbox("' + str + '","4132")', "vbscript");
            return (n == 6);
        }
    </script>
	<script language="javascript" type="text/javascript">

        function checkedOkConfirm() {
            document.getElementById("hdnTocheckStatus").value = "True";
            document.getElementById('btnUpdate').click();
            return true;
        }
        function checkedCancelConfirm() {

            document.getElementById("hdnTocheckStatus").value = "False";
            document.getElementById('btnUpdate').click();
            return true;
        }
        function GetStatus() {

            if (document.getElementById("drpStatuses").value == "to check") {
                document.getElementById("hdnTocheckStatus").value = "False";
                return true;
            }
            else {

                document.getElementById("hdnTocheckStatus").value = "True";
                return true;
            }
        }
    </script>
    <script>
        function OnClientSelectionChange(editor) {
            if ($telerik.isFirefox) {
                var toolPaste = editor.get_toolByName("Paste");
                toolPaste.setState(0);
            }
        }
</script>
	<script type="text/javascript">

        var MyArr1aAssignedUsers = new Array();
        var MyArrImmidiateUsers = new Array();
        function disableSubmit() {
            if (typeof (Page_ClientValidate) == 'function') {
                if (Page_ClientValidate() == true) {
                    if (Boolean(CheckStatus()) == false) {

                        return false;
                    }
                    if (!CheckETC()) {
                        return false;
                    }

                    if (document.getElementById("drpPriorities").value == "1a - DO NOW" || document.getElementById("drpPriorities").value == "0 - IMMEDIATE") {
                        if (document.getElementById("drpUsers").value == document.getElementById("lblAssignedTo").innerHTML) {
                            if (document.getElementById("prevPriority").value != document.getElementById("drpPriorities").value) {
                                if (!check1aAssigned()) {
                                    return false;
                                }
                            }
                        }
                        else {
                            if (!check1aAssigned()) {
                                return false;
                            }
                        }
                    }
                    if (document.getElementById("btnUpdate").value == "Create" && document.getElementById("drpProjects").value == "general") {
                        return checkSubmit();
                    }

                    if (document.getElementById("drpStatuses").value == "to check") {

                        //alert(document.getElementById("hdnTocheckStatus").value);
                        if (document.getElementById("hdnTocheckStatus").value == "False") {

                            document.getElementById('hiddenButton').click();
                            return false;
                        }
                        else {
                            return true;
                        }

                    }
                }
                else {
                    return true;
                }
            }
            else {
                if (!CheckStatus()) {
                    return false;
                }

                if (!CheckETC()) {
                    return false;
                }
                if (document.getElementById("drpPriorities").value == "1a - DO NOW" || document.getElementById("drpPriorities").value == "0 - IMMEDIATE") {
                    if (document.getElementById("drpUsers").value == document.getElementById("lblAssignedTo").innerHTML) {
                        if (document.getElementById("prevPriority").value != document.getElementById("drpPriorities").value) {
                            if (!check1aAssigned()) {
                                return false;
                            }
                        }
                    }
                    else {
                        if (!check1aAssigned()) {
                            return false;
                        }
                    }
                }

                if (document.getElementById("btnUpdate").value == "Create" && document.getElementById("drpProjects").value == "general") {
                    return checkSubmit();
                }

                if (document.getElementById("drpStatuses").value == "to check") {


                    if (document.getElementById("hdnTocheckStatus").value == "False") {

                        document.getElementById('hiddenButton').click();
                        return false;
                    }
                    else {
                        return true;
                    }
                }

            }
        }
        function check1aAssigned() {
            //            var i = 0;
            //            for (i = 0; i < MyArr1aAssignedUsers.length; i++) {
            //                if (MyArr1aAssignedUsers[i] == document.getElementById("drpUsers").value) {
            //                    if (confirm(document.getElementById("drpUsers").value + " already has a 'Highest' task assigned. Sure you want to add another?"))
            //                        return true;
            //                    else
            //                        return false;
            //                   }
            //               }

            //===========================================================================
            
            if (document.getElementById("drpStatuses").value != "to check") {

                if (document.getElementById("drpPriorities").value == '0 - IMMEDIATE') {
                    
                    var i = 0;
                    for (i = 0; i < MyArrImmidiateUsers.length; i++) {
                        if (MyArrImmidiateUsers[i] == document.getElementById("drpUsers").value) {
                            alert(document.getElementById("drpUsers").value + " already has a '0 - IMMEDIATE' task assigned.")
                            return false;
                        }

                    }
                } else if (document.getElementById("drpPriorities").value == '1a - DO NOW') {
                    
                    var i = 0;
                    for (i = 0; i < MyArr1aAssignedUsers.length; i++) {
                        if (MyArr1aAssignedUsers[i] == document.getElementById("drpUsers").value) {
                            alert(document.getElementById("drpUsers").value + " already has a '1a - DO NOW' task assigned.")
                            return false;
                        }
                    }
                }
            }
            else {
                var i = 0;
                for (i = 0; i < MyArr1aAssignedUsers.length; i++) {
                    if (MyArr1aAssignedUsers[i] == document.getElementById("drpUsers").value) {
                        if (confirm(document.getElementById("drpUsers").value + " already has a 'Highest' task assigned. Sure you want to add another?"))
                            return true;
                        else
                            return false;
                    }
                }

            }
            return true;
            //===========================================================================

        }

        function checkSubmit() {
            if (confirm("Can you be more specific with the Project type?"))
                return false;
            else
                return true;
        }
        function CheckStatus() {
            var blnstatus = false;
            if (document.getElementById("drpStatuses").value == "CWO" && document.getElementById("hdnStatusFlag").value == "true") {
                alert("You already have task assigned status CWO or CWO but away'. Please ensure only one task always has assigned status: 'CWO' or 'CWO but away'.");
                return false;
                blnstatus = true;
            }
            else if (document.getElementById("drpStatuses").value == "CWO but away" && document.getElementById("hdnStatusFlag").value == "true") {
                alert("You already have task assigned status CWO or CWO but away'. Please ensure only one task always has assigned status: 'CWO' or 'CWO but away'.");
                return false;
                blnstatus = true;
            }
            else {
                blnstatus = true;
            }

            return Boolean(blnstatus);

        }
        function CheckETC() {
            if (document.getElementById("drpHrs").value == "0" && document.getElementById("drpMins").value == "0" && document.getElementById("drpStatuses").value == "CWO") {
                alert("Please select estimated time to completion.");
                return false;
            }
            else {
                return true;
            }
        }    
    </script>
	<style type="text/css">
.esg-tabs {
	background:#f7f7f7;
	background:-moz-linear-gradient(top, #f7f7f7 0%, #ededed 100%);
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0%, #f7f7f7), color-stop(100%, #ededed));
	background:-webkit-linear-gradient(top, #f7f7f7 0%, #ededed 100%);
	background:-o-linear-gradient(top, #f7f7f7 0%, #ededed 100%);
	background:-ms-linear-gradient(top, #f7f7f7 0%, #ededed 100%);
	background:linear-gradient(to bottom, #f7f7f7 0%, #ededed 100%);
filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#f7f7f7', endColorstr='#ededed', GradientType=0);
	border:1px solid #ccc;
	-webkit-border-radius:7px;
	-moz-border-radius:7px;
	-khtml-border-radius:7px;
	border-radius:5px;
	margin: 3px 0 5px;
	padding: 3px 10px 7px;
	position:relative;
	cursor:pointer;
	color:#0054A6 !important;
}
.esg-tabs a {
	color:#0054A6 !important;
}
.ie9 .esg-tabs {
	filter:none
}
</style>
	</head>
	<body onload="checkChecked();" style="font-size: 12px">
    <form id="form1" runat="server">
      <div id="wrapper">
        <uc2:Admin_Header ID="Admin_Header1" runat="server" />
        <div id="Content">
          <div id="Content_98">
            <asp:HiddenField ID="hfurl" runat="server" />
            <div>
              <uc5:Notifications ID="Notifications1" runat="server" />
            </div>
            <div style="width: 100%; background: #ebebeb; padding: 5px 0;"> &nbsp;&nbsp; <span> <a id="LnkIRTN" style="Border-bottom:1px dotted #000;" runat="server" href="Add_Task.aspx">Clone task&nbsp;|&nbsp;</a> <a id="lnkBackToRequest" style="Border-bottom:1px dotted #000;" runat="server" visible="false" href="edit_request.aspx">Back
            to change request&nbsp;|&nbsp; </a> <a id="lnkCreateNewCR" style="Border-bottom:1px dotted #000;" href="add_changeRequest.aspx"
                            runat="server">Clone change request</a> </span> <span style="float: right">
              <asp:Label ID="lblIRN" style="Border-bottom:1px dotted #000;" runat="server"> </asp:Label>
              &nbsp;<a href="#" style="border-bottom: 1px dotted #000;"
                                        onclick="window.open('ProjectwiseDetails.aspx?id=<%= ProjectId_Client %>','','status=no,scrollbars=no,width=400,height=270')"
                                        class="text_button_trans">Login credentials</a> &nbsp; <a href="#" style="Border-bottom:1px dotted #000;" onclick="window.open('Client_Detail.aspx?ProjectId=<%= ProjectId_Client %>','','status=no,scrollbars=no,width=400,height=250')"   class="text_button_trans">Client Id details</a> &nbsp; <a href="#" style="Border-bottom:1px dotted #000;" onclick="window.open('FTP_details.aspx?id=<%= FTPid %>','','status=no,scrollbars=no,width=400,height=200')"   class="text_button_trans">FTP credentials</a> &nbsp;
              <asp:Button ID="LnkRelated" style="Border-bottom:1px dotted #000;" runat="server" Text="Project related" OnClick="LnkRelated_Click" CssClass="text_button_trans" />
              &nbsp;
              <asp:Button
                                        ID="Lnkcomprelated" runat="server" style="Border-bottom:1px dotted #000;" Text="Company related" OnClick="Lnkcomprelated_Click"
                                        CssClass="text_button_trans" />
              &nbsp; <a href="#" id="CompanyNotes" runat="server" style="Border-bottom:1px dotted #000;" class="text_button_trans">FHS notes</a> &nbsp; <a href="#" id="CRNotes" runat="server" style="Border-bottom:1px dotted #000;" class="text_button_trans">Client notes</a> &nbsp;
              <%--<asp:Button  ID="CompanyNotes" runat="server" Text="FHS notes" style="Border-bottom:1px dotted #000;" OnClick="CompanyNotes_Click" CausesValidation="False" CssClass="text_button_trans" />&nbsp;
                                        <asp:Button  ID="CRNotes"  style="Border-bottom:1px dotted #000;" runat="server" Text="Client notes" OnClick="CRNotes_Click" CausesValidation="False" CssClass="text_button_trans" />&nbsp;
                                        --%>
              <asp:Button style="Border-bottom:1px dotted #000;" ID="BtnAnalytics" runat="server" Text="Analytics" CausesValidation="False" OnClick="BtnAnalytics_Click" CssClass="text_button_trans" />
              </span></div>
            <div id="divMessage" runat="server" style="color: Red; font-weight: bold"> </div>
            <div id="DivEntry" runat="server">
              <div id="divTask" runat="server">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td align="left" valign="top" bgcolor="#282c5f" style="padding: 5px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                        <td align="left" valign="top" style="padding: 0 0 0 5px; color: White; font-size: 14px"> Task ID:
                            <asp:Label ID="lblTaskId" runat="server" Font-Bold="True"></asp:Label></td>
                        <td align="left" valign="top" class="whitetext2" style="padding: 0 0 0 5px; color: White;
                                                font-size: 14px"><asp:Label ID="lblIRTN" runat="server" CssClass="linke124"></asp:Label>
                            &nbsp; </td>
                        <td align="left" style="color: White; font-size: 14px"><asp:Label ID="lblReportedBy" runat="server"></asp:Label></td>
                        <td align="left" class="whitetext2">&nbsp;</td>
                        <td align="left" valign="top" style="color: White; font-size: 14px"><asp:Label ID="lblLastUpdated" runat="server"></asp:Label></td>
                        <td align="right" valign="top" class="whitetext2"><span class="whitetext2"></span>
                            <asp:LinkButton ID="lnkNudge" style="Border-bottom:1px dotted #fff;" runat="server" OnClick="lnkNudge_Click" Visible="false"
                                                    CausesValidation="False">Nudge</asp:LinkButton>
                            &nbsp; <a id="lnkHrsReport" style="Border-bottom:1px dotted #fff;" runat="server" href="#" target="_blank" visible="false"> Hours report</a> &nbsp; <a style="Border-bottom:1px dotted #fff;" id="lnkSubscribers" runat="server" href="#" target="_blank"
                                                        visible="false">Subscribers</a></td>
                        <td align="right" valign="top" class="whitetext2"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          </table></td>
                      </tr>
                      </table></td>
                  </tr>
                  <tr>
                    <td align="left" valign="top"><table width="100%" style="border: solid 1px #cdcdcd; padding: 5px;" class="divBorder"
                                        cellspacing="0" cellpadding="0">
                        <tr>
                        <td align="left" valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                            <td style="width: 52px;"> Project: </td>
                            <td><asp:Label ID="lblProject" runat="server" ></asp:Label></td>
                            <td  align="right"> | </td>
                            <td style="width: 78px;"> Assigned to: </td>
                            <td><asp:Label ID="lbl_AssignedTo" runat="server" ></asp:Label>
                                <asp:LinkButton ID="lblAssignedTo" runat="server" Style="display: none"
                                                                OnClientClick="OpenPopup(600,500)"></asp:LinkButton></td>
                            <td align="right"> | </td>
                            <td style="width: 46px;"> Status:&nbsp; </td>
                            <td><asp:Label ID="lbl_status" runat="server" ></asp:Label></td>
                              <td  align="right"> | </td>
                            <td style="width: 68px;"> Due date: </td>
                            <td><asp:Label ID="lbl_Duedate" runat="server"></asp:Label></td>
                            <td  align="right"> | </td>
                            <td style="width: 69px;"> Start date:</td>
                            <td><asp:Label ID="lbl_Startdate" runat="server"></asp:Label></td>
                            <td  align="right"> | </td>
                            <td style="width: 81px;"> Project type: </td>
                            <td><asp:Label ID="lbl_Projecttype" runat="server"></asp:Label></td>
                            <td  align="right"> | </td>
                            <td style="width: 68px;"> Category: </td>
                            <td><asp:Label ID="lbl_Category" runat="server"></asp:Label></td>
                            <td  align="right"> | </td>
                            <td style="width: 50px;"> Priority: </td>
                            <td><asp:Label ID="lbl_Priority" runat="server"></asp:Label></td>
                          
                          </tr>
                            <tr>
                            <td height="10"></td>
                          </tr>
                            <%--=================================================================================--%>
                            <tr id="indicator" runat="server">
                            <td colspan="24" align="left">Anyone:&nbsp;
                                <asp:Label ID="lblanyone" runat="server" ></asp:Label>
                                &nbsp;|&nbsp; Emp task is assigned to:&nbsp;
                                <asp:Label ID="lblemp" runat="server"
                                                            ></asp:Label>
                                &nbsp;|&nbsp; FHS Lead:&nbsp;
                                <asp:Label ID="lblfhslead" runat="server" ></asp:Label>
                                &nbsp;|&nbsp; UK:&nbsp;
                                <asp:Label ID="lbluk" runat="server" ></asp:Label></td>
                          </tr>
                            <tr>
                            <td height="10"></td>
                          </tr>
                            <%--=================================================================================--%>
                            <tr>
                            <td colspan="30"><table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                  <td valign="top" style="width: 115px;"> Short description: </td>
                                  <td valign="top" style="width: 500px"><asp:Label ID="lbl_Shortdescription" runat="server"></asp:Label></td>
                                  <td rowspan="2">&nbsp;</td>
                                  <td colspan="2" rowspan="2" valign="top"><a id="lblamend" runat="server"  target="blank" style="text-decoration: none;Border-bottom:1px dotted #000;">Relevant
                                    URL:</a>
                                    <%-- </td>
                                                                    <td style="width: 500px">--%>
                                    <asp:Label ID="lbl_RelevantURL" style="Border-bottom:1px dotted #000;" runat="server"></asp:Label>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                                </tr>
                                <tr>
                                  <td colspan="2" valign="top" style="width: 115px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                      <tr>
                                        <td width="22%" height="10"></td>
                                        <td width="78%" align="left"></td>
                                      </tr>
                                      <tr>
                                        <td width="22%" class="margin-test" align="right">Estimated time:</td>
                                        <td width="78%" class="margin-test-1" align="left"><asp:Label ID="lblratiotime" runat="server" style="margin-right:25px;"></asp:Label></td>
                                      </tr>
                                    </table></td>
                                </tr>
                                <tr>
                                  <td height="10"></td>
                                </tr>
                                <tr>
                                  <td colspan="30"><div class="esg-tabs">
                                      <asp:ImageButton ID="img" runat="server" src="../images/plus-ico-esg.png" Style="border-width: 0;float: right;padding: 3px 10px;" OnClientClick="hide1();return false;" />
                                      <asp:LinkButton ID="lhk_h" runat="server" OnClientClick="hide1();return false;" CssClass="whitetext2" Style="text-decoration: none; vertical-align: top;">Edit attributes</asp:LinkButton>
                                      <asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="hide1();return false;"
                                                                                Style="text-decoration: none; vertical-align: top; padding-left: 86%; color: rgb(40, 44, 95)"></asp:LinkButton>
                                    </div></td>
                                </tr>
                                <tr>
                                  <td colspan="15"><table id="t_h_s" runat="server" style="display: none" width="100%">
                                      <tr>
                                        <td width="11%" valign="top"> Change project&nbsp; </td>
                                        <td width="11%" valign="top"> Project type </td>
                                        <td width="11%" valign="top"> Re-assign&nbsp; </td>
                                        <td width="11%" valign="top"> Status</td>
                                        <td width="11%" valign="top"> Category </td>
                                        <td width="11%" valign="top"> Priority </td>
                                        <td width="30%" valign="top">Estimated time to completion</td>
                                        <td width="1%" align="left" valign="top"></td>
                                      </tr>
                                      
                                      <tr>
                                        <td  valign="top"><asp:DropDownList ID="drpProjects" runat="server" Width="150px" OnSelectedIndexChanged="drpProjects_SelectedIndexChanged"
                                                                                        AutoPostBack="True"> </asp:DropDownList>
                                          <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="drpProjects"
                                                                                        ErrorMessage="Project is required.">*</asp:RequiredFieldValidator></td>
                                        <td valign="top"><asp:DropDownList ID="drpProjectType" runat="server" Width="150px">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>es</asp:ListItem>
                                            <asp:ListItem Selected="True">other</asp:ListItem>
                                          </asp:DropDownList></td>
                                        <td  valign="top"><asp:DropDownList ID="drpUsers" runat="server" Width="150px"> </asp:DropDownList>
                                          <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="drpUsers"
                                                                                        ErrorMessage="Re-assign to employee is required.">*</asp:RequiredFieldValidator></td>
                                        <td valign="top"><asp:DropDownList ID="drpStatuses" runat="server" Width="150px"> </asp:DropDownList>
                                          <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="drpStatuses"
                                                                                        ErrorMessage="Status is required.">*</asp:RequiredFieldValidator></td>
                                        <td valign="top"><asp:DropDownList ID="drpCategories" runat="server" Width="150px"> </asp:DropDownList>
                                          <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="drpCategories"
                                                                                        ErrorMessage="Category is required.">*</asp:RequiredFieldValidator></td>
                                        <td  valign="top"><asp:DropDownList ID="drpPriorities" runat="server" Width="150px"> </asp:DropDownList>
                                          <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="drpPriorities"
                                                                                        ErrorMessage="Priority is required.">*</asp:RequiredFieldValidator></td>
                                        <td valign="top" width="4%">
                                        <table width="70%">
                                        <tr>
                                             <td width="30%" align="left"><asp:DropDownList ID="drpHrs" runat="server">
                                                              <asp:ListItem Value="0">00</asp:ListItem>
                                                              <asp:ListItem Value="1">01</asp:ListItem>
                                                              <asp:ListItem Value="2">02</asp:ListItem>
                                                              <asp:ListItem Value="3">03</asp:ListItem>
                                                              <asp:ListItem Value="4">04</asp:ListItem>
                                                              <asp:ListItem Value="5">05</asp:ListItem>
                                                              <asp:ListItem Value="6">06</asp:ListItem>
                                                              <asp:ListItem Value="7">07</asp:ListItem>
                                                              <asp:ListItem Value="8">08</asp:ListItem>
                                                              <asp:ListItem Value="9">09</asp:ListItem>
                                                              <asp:ListItem Value="10">10</asp:ListItem>
                                                              <asp:ListItem Value="11">11</asp:ListItem>
                                                              <asp:ListItem Value="12">12</asp:ListItem>
                                                              <asp:ListItem Value="13">13</asp:ListItem>
                                                              <asp:ListItem Value="14">14</asp:ListItem>
                                                              <asp:ListItem Value="15">15</asp:ListItem>
                                                              <asp:ListItem Value="16">16</asp:ListItem>
                                                              <asp:ListItem Value="17">17</asp:ListItem>
                                                              <asp:ListItem Value="18">18</asp:ListItem>
                                                              <asp:ListItem Value="19">19</asp:ListItem>
                                                              <asp:ListItem Value="20">20</asp:ListItem>
                                                              <asp:ListItem Value="21">21</asp:ListItem>
                                                              <asp:ListItem Value="22">22</asp:ListItem>
                                                              <asp:ListItem Value="23">23</asp:ListItem>
                                                              <asp:ListItem Value="24">24</asp:ListItem>
                                                              <asp:ListItem Value="25">25</asp:ListItem>
                                                              <asp:ListItem Value="26">26</asp:ListItem>
                                                              <asp:ListItem Value="27">27</asp:ListItem>
                                                              <asp:ListItem Value="28">28</asp:ListItem>
                                                              <asp:ListItem Value="29">29</asp:ListItem>
                                                              <asp:ListItem Value="30">30</asp:ListItem>
                                                              <asp:ListItem Value="31">31</asp:ListItem>
                                                              <asp:ListItem Value="32">32</asp:ListItem>
                                                              <asp:ListItem Value="33">33</asp:ListItem>
                                                              <asp:ListItem Value="34">34</asp:ListItem>
                                                              <asp:ListItem Value="35">35</asp:ListItem>
                                                              <asp:ListItem Value="36">36</asp:ListItem>
                                                              <asp:ListItem Value="37">37</asp:ListItem>
                                                              <asp:ListItem Value="38">38</asp:ListItem>
                                                              <asp:ListItem Value="39">39</asp:ListItem>
                                                              <asp:ListItem Value="40">40</asp:ListItem>
                                                              <asp:ListItem Value="41">41</asp:ListItem>
                                                              <asp:ListItem Value="42">42</asp:ListItem>
                                                              <asp:ListItem Value="43">43</asp:ListItem>
                                                              <asp:ListItem Value="44">44</asp:ListItem>
                                                              <asp:ListItem Value="45">45</asp:ListItem>
                                                              <asp:ListItem Value="46">46</asp:ListItem>
                                                              <asp:ListItem Value="47">47</asp:ListItem>
                                                              <asp:ListItem Value="48">48</asp:ListItem>
                                                              <asp:ListItem Value="49">49</asp:ListItem>
                                                              <asp:ListItem Value="50">50</asp:ListItem>
                                                              <asp:ListItem Value="51">51</asp:ListItem>
                                                              <asp:ListItem Value="52">52</asp:ListItem>
                                                              <asp:ListItem Value="53">53</asp:ListItem>
                                                              <asp:ListItem Value="54">54</asp:ListItem>
                                                              <asp:ListItem Value="55">55</asp:ListItem>
                                                              <asp:ListItem Value="56">56</asp:ListItem>
                                                              <asp:ListItem Value="57">57</asp:ListItem>
                                                              <asp:ListItem Value="58">58</asp:ListItem>
                                                              <asp:ListItem Value="59">59</asp:ListItem>
                                                              <asp:ListItem Value="60">60</asp:ListItem>
                                                            </asp:DropDownList>
                                                            hrs </td>
                                                          <td width="30%" ><asp:DropDownList ID="drpMins" runat="server">
                                                              <asp:ListItem Value="0">00</asp:ListItem>
                                                              <asp:ListItem Value="5">05</asp:ListItem>
                                                              <asp:ListItem Value="10">10</asp:ListItem>
                                                              <asp:ListItem Value="15">15</asp:ListItem>
                                                              <asp:ListItem Value="20">20</asp:ListItem>
                                                              <asp:ListItem Value="25">25</asp:ListItem>
                                                              <asp:ListItem Value="30">30</asp:ListItem>
                                                              <asp:ListItem Value="35">35</asp:ListItem>
                                                              <asp:ListItem Value="40">40</asp:ListItem>
                                                              <asp:ListItem Value="45">45</asp:ListItem>
                                                              <asp:ListItem Value="50">50</asp:ListItem>
                                                              <asp:ListItem Value="55">55</asp:ListItem>
                                                            </asp:DropDownList>
                                                            mins </td>
                   
                   
                                        
                                        
                                        </tr>
                                        </table>
                                         </td>
                                        <td width="10%" valign="top" ></td>
                                      </tr>
                                      
                                      <tr>
                                        <td align="left" valign="middle" colspan="6" ><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                              <td width="46%" valign="top"> Short description : </td>
                                             <td  width="50%" align="left" valign="top">Relevant URL : </td> 
                                             
                                      
                                            </tr>
                                          </table>
                                          
                                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                              
                                              <td width="81%" align="left" valign="top"><asp:TextBox ID="txtShortDescr" runat="server" Width="424px"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtShortDescr" ErrorMessage="Short description is required.">*</asp:RequiredFieldValidator>
                                                <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" TargetControlID="RequiredFieldValidator2"> </cc1:ValidatorCalloutExtender></td>
                                              
                                              <td align="left" valign="top"  width="83%"><asp:TextBox ID="txtRelevantURL" runat="server" Width="475px" Rows="1" TextMode="MultiLine" style="height:18px"></asp:TextBox></td>
                                            </tr>
                                          </table>
                                          
                                          
                                          
                                          </td>
                                        <td colspan="2" align="left" valign="middle">
                                        <table cellpadding="0" cellspacing="0">
                                        
                            <tr><td><asp:Label ID="lblTotalHrs" runat="server" Font-Bold="False"></asp:Label></td></tr>
                        
                          
                                        
                                        </table>
                                        
                                        </td>
                                      </tr>
                                      
                                      <tr><td colspan="8" height="5px"></td></tr>
                                      
                                      <tr>
                                        <td colspan="8">
                                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tr>
                                        
                                        <td width="8%" valign="top"><span>Start date :&nbsp;</span></td>
                                        <td width="27%" valign="top"><div id="DivStartdateCal" runat="server">
                                            <asp:TextBox ID="txtStartDate" runat="server" Width="150px"></asp:TextBox>
                                          </div>
                                          <div id="DivStartdateLbl" runat="server">
                                            <asp:Label ID="lblStartdate" Font-Bold="true" runat="server"></asp:Label>
                                            &nbsp;&nbsp; <a href="#" runat="server" id="LnkRstStartDate">Reset&nbsp;</a>
                                            <asp:Button
                                             ID="BtnRemoveStrtDate" runat="server" Font-Size="12px" BackColor="Transparent"
                                             BorderColor="Transparent" BorderStyle="None" ForeColor="Blue" Text="Remove" OnClick="BtnRemoveStrtDate_Click"
                                             Width="54px" />
                                          </div></td>
                                        <td width="2%" valign="top"><asp:Label ID="Label1" runat="server"></asp:Label></td>
                                        <td width="6%" valign="top">Due date :&nbsp; </td>
                                        <td width="33%" valign="top"> <div id="DivDuedateCal" runat="server">
                                            <asp:TextBox ID="txtDueDate" runat="server" Width="150px"></asp:TextBox>
                                          </div>
                                          <div id="DivDuedateLbl" runat="server">
                                            <asp:Label ID="lblDueDate" Font-Bold="true" runat="server"></asp:Label>
                                            &nbsp;&nbsp; <a href="#" id="LnkDueDate" runat="server">Reset&nbsp;</a>
                                            <asp:Button
                                                                                            ID="BtnRmvDueDate" runat="server" Font-Size="12px" BackColor="Transparent" BorderColor="Transparent"
                                                                                            BorderStyle="None" ForeColor="Blue" Text="Remove" OnClick="BtnRmvDueDate_Click"
                                                                                            Width="53px" />
                                          </div>
                                          <td width="43%">          <asp:Label ID="lblHrsLeft" runat="server" Font-Bold="False"></asp:Label></td>
                                      
                                       <!-- <td width="15%" valign="top">  </td>
                                        <td width="12%" valign="top"></td>
                                        <td width="21%"  align="left" valign="top"></td>
                                        <td width="1%"></td>
                                        <td width="1%" valign="top"></td>
                                        <td width="1%"></td>-->
                                        
                                        </table>
                                        </td>
                                     
                                      </tr>
                                      
                                      <tr>
                                        <td align="left" valign="top" colspan="15"><table border="0" cellpadding="0" cellspacing="0">
                                          </table>
                                          <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator1"> </cc1:ValidatorCalloutExtender>
                                          <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server" TargetControlID="RequiredFieldValidator3"> </cc1:ValidatorCalloutExtender>
                                          <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="server" TargetControlID="RequiredFieldValidator4"> </cc1:ValidatorCalloutExtender>
                                          <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="server" TargetControlID="RequiredFieldValidator5"> </cc1:ValidatorCalloutExtender>
                                          <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="server" TargetControlID="RequiredFieldValidator6"> </cc1:ValidatorCalloutExtender></td>
                                      </tr>
                                      
                                      <tr>
                                        <td align="left" valign="top" class="whitetext2" colspan="15"><asp:Label ID="lblUserName" runat="server" Visible="False"></asp:Label></td>
                                      </tr>
                                      
                                      <tr>
                                        <td align="left" valign="top" colspan="6"><table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                              <td rowspan="2" align="left" valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                  <tr>
                                                    <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                        
                                                        <tr>
                                                          <td height="10"></td>
                                                        </tr>
                                                      </table></td>
                                                  </tr>
                                                </table></td>
                                            </tr>
                                            <tr>
                                              <td  class="whitetext2"></td>
                                              <td class="whitetext2"></td>
                                            </tr>
                                          </table></td>
                                      </tr>
                                    </table></td>
                                </tr>
                              </table></td>
                          </tr>
                          </table></td>
                      </tr>
                        <tr>
                        <td align="left" valign="middle"><div class="esg-tabs">
                            <asp:ImageButton ID="img1" runat="server" src="../images/plus-ico-esg.png" Style="border-width: 0;float: right;padding: 3px 10px;" OnClientClick="hide();return false;" />
                            <asp:LinkButton ID="lnk_h_s" runat="server" OnClientClick="hide();return false;"
                                                        CssClass="whitetext2" Style="text-decoration: none; vertical-align: top;">Add new comment</asp:LinkButton>
                            <asp:LinkButton ID="LinkButton2" runat="server" OnClientClick="hide();return false;"
                                                        Style="text-decoration: none; vertical-align: top; padding-left: 84%; color: rgb(40, 44, 95);"></asp:LinkButton>
                          </div>
                            <div id="d_h_s" runat="server" style="display: none;">
                            <div align="left" valign="top" style=" color: Black"> Comment: <a href="#" id="A1" onclick="window.open('Task_Canned_message.aspx','','status=yes,width=760,height=560')"> Canned message&nbsp;</a> </div>
                            <%--<telerik:RadEditor ID="ftbComment" runat="server" Height="300px" Width="100%" OnClientSelectionChange="OnClientSelectionChange"> <Snippets>
                              <telerik:EditorSnippet Name="Product Template" Value="<table><tr><td><div style='width:300px;border:2px outset #D8D2BD;padding:34px;font-family:Arial, Verdana; font-size:11px;background- color:#F1EFE6;'>Dear ____________________,<br>Thank you for inquiring about ____________. Your request will be processed in 48 hours and shipped at the address you have provided.<b>Please, contact us if you have any problems.</b></div></td></tr></table>"> </telerik:EditorSnippet>
                              <telerik:EditorSnippet Name="Signature" Value="<strong>John Smith</strong><br>Sales Manager<br>"> </telerik:EditorSnippet>
                              </Snippets>
                                <Links>
                                <telerik:EditorLink Name="Telerik" Href="http://www.telerik.com">
                                <telerik:EditorLink Name="Telerik Products" Href="http://www.telerik.com/products"
                                                                    Target="_blank" />
                                <telerik:EditorLink Name="Telerik Support Center" Href="http://www.telerik.com/support/home.aspx"
                                                                    Target="_blank" />
                                <telerik:EditorLink Name="Telerik Community" Href="http://www.telerik.com/community/home.aspx"
                                                                    Target="_blank" ToolTip="Telerik Community" />
                                </telerik:EditorLink>
                              </Links>
                                <Tools> </Tools>
                                <ImageManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                <MediaManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                <FlashManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                <TemplateManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                <DocumentManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                <Modules>
                                <telerik:EditorModule Name="RadEditorStatistics" Visible="False" Enabled="true" />
                                <telerik:EditorModule Name="RadEditorDomInspector" Visible="False" Enabled="true" />
                                <telerik:EditorModule Name="RadEditorNodeInspector" Visible="False" Enabled="true" />
                                <telerik:EditorModule Name="RadEditorHtmlInspector" Visible="False" Enabled="true" />
                              </Modules>
                                <Content> </Content>
                              </telerik:RadEditor>--%>
                                  <cc2:Editor ID="ftbComment" runat="server" Height="300px" Width="100%" />
                            <%--******************************************************************************************--%>
                          </div></td>
                      </tr>
                        <tr>
                        <%--==========================================================================================================================================--%>
                        <td><div class="esg-tabs">
                            <asp:ImageButton ID="img3" runat="server" src="../images/plus-ico-esg.png" Style="border-width: 0;float: right;padding: 3px 10px;" OnClientClick="hide2();return false;" />
                            <asp:LinkButton ID="SPU" runat="server" OnClientClick="hide2();return false;" CssClass="whitetext2"
                                                        Style="text-decoration: none; vertical-align: top;">Edit Recipients</asp:LinkButton>
                            <asp:LinkButton ID="LinkButton3" runat="server" OnClientClick="hide2();return false;"
                                                        Style="text-decoration: none; vertical-align: top; padding-left: 84%; color: rgb(40, 44, 95);"></asp:LinkButton>
                          </div>
                            <div id="S_P_U" runat="server" style="display: none;">
                            <table style="float: left; height: 45px; width: 500px;" border="0" cellpadding="0"
                                                        cellspacing="0">
                                <tr>
                                <td><asp:CheckBoxList ID="chkmaillst" runat="server" RepeatDirection="Horizontal"> </asp:CheckBoxList></td>
                                <td><asp:DropDownList ID="drpbinduser" runat="server" Width="175px"> </asp:DropDownList></td>
                                <td><asp:DropDownList ID="drpAddRecipient" runat="server" Width="175px"> </asp:DropDownList></td>
                              </tr>
                              </table>
                          </div></td>
                        <%--==========================================================================================================================================--%>
                      </tr>
                        <tr>
                        <td align="left" valign="middle" style="height: 22px"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                            <td align="left" valign="bottom"><div id="divAttachments" runat="server"> <a id="lnkSendAttachment" style="Border-bottom:1px dotted #000;" href="#" runat="server">Upload attachment</a> &nbsp;&amp;&nbsp; <a id="lnkViewAttachment" style="Border-bottom:1px dotted #000;" href="#" runat="server">View attachments</a>&nbsp;
                                <asp:CheckBox ID="chkShowAllHistory" runat="server" Text="Show all history" />
                              </div>
                                <div id="divMessage1" runat="server" style="color: Red"> </div></td>
                            <td colspan="-1" align="right" valign="bottom" style="height: 32px"><%--================================================================================--%>
                                <asp:CheckBox ID="checkisPrivate" runat="server"  Text="Private" />
                                &nbsp;&nbsp;
                                <%--================================================================================--%>
                                <asp:CheckBox ID="chkEmailnotify" runat="server" Checked="true"
                                                                Text="Email notify" />
                                &nbsp;&nbsp;
                                <asp:CheckBox ID="chkQuestion" runat="server"
                                                                    Text="As a question" />
                                &nbsp;&nbsp;<a id="LnkAskQuestion" style="Border-bottom:1px dotted #000;" runat="server" href="task_Question.aspx"
                                                                        class="text_button_trans">Ask question</a> &nbsp;&nbsp;
                                <asp:Button ID="btnUpdate"
                                                                            runat="server" Text="Update" OnClientClick="return disableSubmit();" OnClick="btnUpdate_Click"
                                                                            CssClass="blueBtns" />
                                &nbsp;
                                <asp:Button ID="btnDelete" runat="server" Text="Delete"
                                                                                OnClick="btnDelete_Click" CausesValidation="False" CssClass="blueBtns" /></td>
                          </tr>
                          </table></td>
                      </tr>
                      </table></td>
                  </tr>
                </table>
              </div>
              <br />
              <div id="divTaskDetails" runat="server">
                <div id="divNoHistory" runat="server">
                  <table width="100%">
                    <tr>
                      <td align="right"><table width="100%">
                          <tr>
                          <td align="left" style="height: 26px">&nbsp;&nbsp;
                              <asp:CheckBox ID="chkSelectAll" runat="server" onclick="return setCheckAll();" Text="Check All" style="margin-left:-14px;" />
                              &nbsp;&nbsp;
                              <div id="allread" runat="server">
                              <asp:Button ID="btnAllasRead" runat="server"  Text="Mark checked as read" OnClick="btnAllasRead_Click"
                                                            CssClass="blueBtns" OnClientClick="return ReadSomeComment();" />
                            </div>
                              <a id="Readlnk" href="#" style="Border-bottom:1px dotted #000;" onclick="readBtn();">Mark checked as read</a> &nbsp;&nbsp;
                              &nbsp;&nbsp;
                              <div id="someDelete" runat="server">
                              <asp:Button ID="BtnDelAllCmnt" runat="server"  Text="Delete checked" OnClick="BtnDelAllCmnt_Click"
                                                            OnClientClick="return validateOnbtn();" CssClass="blueBtns" />
                            </div>
                              <asp:LinkButton ID="lnkdelete" style="Border-bottom:1px dotted #000;" runat="server" CausesValidation="false" Text="Delete checked"
                                                        OnClientClick="DeletedBtn();"></asp:LinkButton></td>
                          <td align="right" style="height: 26px"><asp:CheckBox ID="ChkIncludeRead" runat="server" Text="Include read comments" Font-Bold="true"
                                                        Checked="true" AutoPostBack="True" /></td>
                        </tr>
                        </table></td>
                    </tr>
                    <tr>
                      <td><asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                                            BorderWidth="0px" GridLines="None" OnItemCommand="DataGrid1_ItemCommand" OnItemDataBound="DataGrid1_ItemDataBound"
                                            ShowHeader="False" Width="100%">
                          <Columns>
                        <asp:BoundColumn DataField="task_id" Visible="False"></asp:BoundColumn>
                        <asp:BoundColumn DataField="tc_id" Visible="False"></asp:BoundColumn>
                        <asp:BoundColumn DataField="username" Visible="False"></asp:BoundColumn>
                        <asp:BoundColumn DataField="post_date" Visible="False"></asp:BoundColumn>
                        <asp:BoundColumn DataField="comment" Visible="False"></asp:BoundColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                            <div style="border: solid 1px green; margin-bottom: 2px">
                                <table width="100%">
                                <tr>
                                    <td align="left"><table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                        <td><asp:Label ID="lblPosted" runat="server"></asp:Label>
                                            &nbsp;
                                            <asp:Button ID="lnkReadMark" runat="server" CommandName="ReadMark" Text="(Unread. Mark as read)"
                                                                                        Style="background: none;border:none;cursor:pointer;Border-bottom:1px dotted #000;" />
                                            &nbsp;
                                            <%--************************************************************************************--%>
                                            <asp:Label ID="lblipAdd" runat="server" Text="IP:"></asp:Label>
                                            <asp:Label ID="lblid" runat="server" Text='<%#bind("Ip") %>'></asp:Label>
                                            <asp:Label ID="lblMacipAdd" runat="server" Text="MacId:"></asp:Label>
                                            <asp:Label ID="lblmacid" runat="server" Text='<%#bind("Macid") %>'></asp:Label>
                                            <%--************************************************************************************--%>
                                            <asp:CheckBox ID="chkSelect" runat="server" onclick="validate(this);" /></td>
                                        <td align="right"><asp:ImageButton ImageUrl="images/edit.png" ToolTip="Edit"  ID="lnkEdit" runat="server" CausesValidation="False" BackColor="transparent"
                                                                                        BorderStyle="none" ForeColor="#282C5F" CommandName="edit"   Style="cursor: pointer"
                                                                                        Text="edit"/>
                                            <asp:ImageButton ImageUrl="images/delete.png" ToolTip="Delete"  ID="lnkDelete" runat="server" CausesValidation="False" BackColor="transparent"
                                                                                        BorderStyle="none" ForeColor="#282C5F" CommandName="delete" Style="cursor: pointer"
                                                                                        Text="delete"/></td>
                                      </tr>
                                      </table></td>
                                  </tr>
                                <tr>
                                    <td align="left"></td>
                                  </tr>
                                <tr>
                                    <td align="left"><asp:Label ID="lblComment" runat="server" Text='<%# bind("comment") %>'></asp:Label></td>
                                  </tr>
                              </table>
                              </div>
                          </ItemTemplate>
                          </asp:TemplateColumn>
                        <asp:BoundColumn DataField="qflag" HeaderText="qflag" Visible="False"></asp:BoundColumn>
                        <asp:BoundColumn DataField="QuesTo" Visible="False"></asp:BoundColumn>
                        <%--==========================================================================--%>
                        <asp:TemplateColumn Visible="false">
                            <ItemTemplate>
                            <asp:Label ID="lbltc_id" runat="server" Text='<%# bind("tc_id") %>'></asp:Label>
                          </ItemTemplate>
                          </asp:TemplateColumn>
                        <%--==========================================================================--%>
                        </Columns>
                        </asp:DataGrid></td>
                    </tr>
                  </table>
                </div>
                <div id="divAllHistory" runat="server">
                  <asp:DataGrid ID="DataGrid2" runat="server" AutoGenerateColumns="False" Width="100%"
                                ShowHeader="False" OnItemDataBound="DataGrid2_ItemDataBound" OnItemCommand="DataGrid2_ItemCommand"
                                BorderStyle="None" GridLines="None">
                    <Columns>
                    <asp:BoundColumn DataField="task_id" Visible="False"></asp:BoundColumn>
                    <asp:BoundColumn DataField="tc_id" Visible="False"></asp:BoundColumn>
                    <asp:BoundColumn DataField="username" Visible="False"></asp:BoundColumn>
                    <asp:BoundColumn DataField="post_date" Visible="False"></asp:BoundColumn>
                    <asp:BoundColumn DataField="comment" Visible="False"></asp:BoundColumn>
                    <asp:TemplateColumn>
                      <ItemTemplate>
                        <div style="border: solid 1px green; margin-bottom: 2px">
                          <table width="100%">
                            <tr>
                              <td align="left"><table width="100%" cellpadding="0" cellspacing="0" border="0">
                                  <tr>
                                  <td><asp:Label ID="lblPosted" runat="server" ForeColor="Green"></asp:Label>
                                      &nbsp;
                                      <asp:Button ID="lnkReadMark" runat="server" CommandName="ReadMark" Text="(Unread. Mark as read)"
                                                                            Style="background: none; border: none" /></td>
                                  <td align="right"><asp:LinkButton ID="lnkEdit" runat="server" CausesValidation="False" CommandName="edit">edit</asp:LinkButton>
                                      &nbsp;
                                      <asp:LinkButton ID="lnkDelete" runat="server" CausesValidation="False" CommandName="delete">delete</asp:LinkButton></td>
                                </tr>
                                </table></td>
                            </tr>
                            <tr>
                              <td align="left"></td>
                            </tr>
                            <tr>
                              <td align="left"><asp:Label ID="lblComment" runat="server" Text='<%# bind("comment") %>'></asp:Label></td>
                            </tr>
                          </table>
                        </div>
                      </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="deleted" Visible="False"></asp:BoundColumn>
                    <asp:BoundColumn DataField="temp" Visible="False"></asp:BoundColumn>
                    <asp:BoundColumn DataField="qflag" HeaderText="qflag" Visible="False"></asp:BoundColumn>
                    </Columns>
                  </asp:DataGrid>
                </div>
              </div>
            </div>
          </div>
        </div>
        <cc1:CalendarExtender ID="CalendarExtender2" runat="server" PopupButtonID="Image2"
            TargetControlID="txtDueDate" Format="ddd, dd/MMM/yyyy"> </cc1:CalendarExtender>
        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="Image1"
            TargetControlID="txtStartDate" Format="ddd, dd/MMM/yyyy"> </cc1:CalendarExtender>
        <asp:HiddenField ID="checkboxchecked" runat="server" Value="0" />
        <asp:HiddenField ID="prevProject" runat="server" />
        <asp:HiddenField ID="TaskProject" runat="server" />
        <asp:HiddenField ID="prevAssignedTo" runat="server" />
        <asp:HiddenField ID="prevStatus" runat="server" />
        <asp:HiddenField ID="prevProjectType" runat="server" />
        <asp:HiddenField ID="prevCategory" runat="server" />
        <asp:HiddenField ID="prevPriority" runat="server" />
        <asp:HiddenField ID="prevRelevantUrl" runat="server" />
        <asp:HiddenField ID="prevShortDescr" runat="server" />
        <asp:HiddenField ID="prevETC" runat="server" />
        <asp:HiddenField ID="hdnStatusFlag" runat="server" Value="false"/>
        <asp:HiddenField ID="hdnProjectCompany" runat="server" />
        <asp:HiddenField ID="hdnTocheckStatus" runat="server" />
        <asp:Button ID="btnIncluderead" OnClick="btnIncluderead_Click" runat="server" CausesValidation="False"
            BorderStyle="none" BackColor="transparent" Text=""></asp:Button>
        <asp:Button ID="btnCheckAllToDelete" OnClick="btnCheckAllToDelete_Click" runat="server"
            CausesValidation="False" BorderStyle="none" BackColor="transparent" Text=""> </asp:Button>
      </div>
      <div id="hiddenDiv" style="display: none; visibility: hidden;">
        <asp:Button runat="server" ID="hiddenButton" />
      </div>
      <asp:Panel ID="Panalconfirm" runat="server" Width="700px" Style="display: none; background-color: White">
        <div>
          <table style="background-color: White">
            <tr style="background-color: White">
              <td align="left" valign="top"><%--****************************************************************************************************************************--%>
                <FTB:FreeTextBox ID="FtbResources" runat="server" Height="300px" Width="700px" AutoGenerateToolbarsFromString="False"> </FTB:FreeTextBox>
                <%--****************************************************************************************************************************--%></td>
            </tr>
            <tr style="background-color: White">
              <td align="center" valign="top" style="background-color: White"><asp:Button ID="btnOK" runat="server" Text="OK" />
                &nbsp;
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" /></td>
            </tr>
          </table>
        </div>
        </asp:Panel>
      <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalBackground"
        DropShadow="true" PopupControlID="Panalconfirm" TargetControlID="hiddenButton"> </cc1:ModalPopupExtender>
      <uc1:footer ID="Footer1" runat="server" />
    </form>
</body>
</html>
