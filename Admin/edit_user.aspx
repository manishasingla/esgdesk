<%@ Page Language="C#" AutoEventWireup="true" CodeFile="edit_user.aspx.cs" Inherits="Admin_edit_user" %>

<%@ Register Src="Notifications.ascx" TagName="Notifications" TagPrefix="uc4" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI" TagPrefix="cc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Edit user - <%=System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(username)%> </title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript">

//        function setReqAreas() {
//            var hiddenPR = document.getElementById("hdnReqPR");
//            var chkbxs = document.getElementById("Repeater2").getElementsByTagName("input");
//            hiddenPR.value = "";
//            for (var i = 0; i < chkbxs.length; i++) {
//                if (chkbxs[i].checked == true) { hiddenPR.value += chkbxs[i].title + ";"; }
//            }
//        }

//        function setAllChecked(obj) {
//            var chkbxs = document.getElementById("Repeater2").getElementsByTagName("input");
//            for (var i = 0; i < chkbxs.length; i++) {
//                chkbxs[i].checked = obj.checked;
//            }
//            setReqAreas();
//        }
    </script>
    <script type="text/javascript" language="javascript">
        function validateImg(sender, args) {

            var cvfilevalue = document.getElementById("fileImgUpload").value;
            args.IsValid = (cvfilevalue != "");
        }

        function validateImgFileFormats(sender, args) {
            var Imgfilevalue = document.getElementById("fileImgUpload").value;
            if (!/(\.(jpeg|gif|png|jpg))$/i.test(Imgfilevalue)) {
                args.IsValid = false;
            }
            else {
                args.IsValid = true;
            }

        }

        function fnSetActiveTabIndex() {
            //            var tabContainer = document.getElementById("TabContainer1");

            //            var index = tabContainer.get_activeTab();
            //            alert(index);
        }
    </script>
    <style type="text/css">
        .TabHeaderCSS
        {
            font-family: Verdana, Arial, Courier New;
            font-size: 10px;
            text-align: center;
            cursor: pointer;
        }
        .BackColorTab
        {
            font-family: Verdana, Arial, Courier New;
            font-size: 10px;
        }
        
        .inputbox005
        {
            width: 200px;
            height: 20px;
            border: 1px solid #dfdfdf;
            background-color: rgb(242, 246, 255);
        }
        
        .messagebox006
        {
            width: 200px;
            height: 50px;
            border: 1px solid #dfdfdf;
            background-color: rgb(242, 246, 255);
            resize: none;
        }
        
        
        @media screen and (-webkit-min-device-pixel-ratio:0)
        {
            .messagebox006
            {
                width: 196px;
            }
        }
        
        
        .inputbox004
        {
            width: 169px;
            height: 20px;
            border: 1px solid #dfdfdf;
            background-color: rgb(242, 246, 255);
        }
        
        .messagebox004
        {
            width: 169px;
            height: 50px;
            border: 1px solid #dfdfdf;
            background-color: rgb(242, 246, 255);
            resize: none;
        }
        .grdi001 td, th
        {
            border: 1px solid #888888;
        }
    </style>
</head>
<body style="overflow-x: hidden;">
    <form id="form1" runat="server">
    <div id="wrapper">
        <uc2:Admin_Header ID="Admin_Header1" runat="server" />
        <div id="Content">
            <div>
                <uc4:Notifications ID="Notifications" runat="server" />
            </div>
            <div style="width: 100%; background: #ebebeb; padding: 5px; float: left;" class="text_default">
                <a href="edit_user.aspx">Add new user</a> &nbsp;|&nbsp; <a href="users.aspx">Back to
                    users</a></div>
            <div style="clear: both">
            </div>
            <cc1:TabContainer ID="TabContainer1" runat="server" Style="width: 60%; padding-left: 4px;"
                ActiveTabIndex="8" OnActiveTabChanged="TabContainer1_ActiveTabChanged" AutoPostBack="false">
                <cc1:TabPanel ID="TabAboutUs" runat="server" HeaderText="General Info">
                    <ContentTemplate>
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="left" valign="top">
                                    <table style="width: 424px; margin-left: -5px; margin-top: -9px;" cellpadding="0">
                                        <tr>
                                            <td align="left" class="titleText" valign="top" style="width: 424px; display: none;">
                                                Users |
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="middle">
                                                <div id="divLinks" runat="server">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 424px">
                                                <table width="424" cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td align="left" valign="top">
                                                            <img height="16" src="../images/loginBox_top.png" width="420" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" style="background: url(../images/loginBox_mid.png) repeat-y left top;">
                                                            <div style="text-align: center">
                                                                <div id="DivEntry" runat="server">
                                                                    <table width="93%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td align="left" valign="top">
                                                                                <table cellpadding="2" cellspacing="2" border="0">
                                                                                    <tr>
                                                                                        <td width="170" align="left" valign="top" class="whitetext1">
                                                                                            &nbsp;User ID:
                                                                                        </td>
                                                                                        <td width="682" align="left" valign="top">
                                                                                            <asp:Label CssClass="whitetext2" ID="lblId" runat="server">New</asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" valign="top" class="whitetext1" width="46%">
                                                                                            &nbsp;Username:
                                                                                        </td>
                                                                                        <td align="left" valign="top">
                                                                                            <asp:TextBox ID="txtUserName" runat="server" CssClass="inputbox005" MaxLength="50"></asp:TextBox>
                                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="firstentry"
                                                                                                runat="server" ControlToValidate="txtUserName" ErrorMessage="Username is required."
                                                                                                Display="Dynamic">*</asp:RequiredFieldValidator>
                                                                                            <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator1"
                                                                                                Enabled="True">
                                                                                            </cc1:ValidatorCalloutExtender>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" valign="top" class="whitetext1">
                                                                                            &nbsp;Password:
                                                                                        </td>
                                                                                        <td align="left" valign="top">
                                                                                            <asp:TextBox ID="txtPassword" runat="server" MaxLength="50" CssClass="inputbox005"
                                                                                                TextMode="Password"></asp:TextBox>
                                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="firstentry"
                                                                                                runat="server" ControlToValidate="txtPassword" ErrorMessage="Password is required."
                                                                                                Display="Dynamic">*</asp:RequiredFieldValidator>
                                                                                            <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" TargetControlID="RequiredFieldValidator2"
                                                                                                Enabled="True">
                                                                                            </cc1:ValidatorCalloutExtender>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" valign="top" class="whitetext1">
                                                                                            &nbsp;Confirm password:
                                                                                        </td>
                                                                                        <td align="left" valign="top">
                                                                                            <asp:TextBox ID="txtConfirmPassword" runat="server" MaxLength="50" CssClass="inputbox005"
                                                                                                TextMode="Password"></asp:TextBox>
                                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ValidationGroup="firstentry"
                                                                                                ControlToValidate="txtConfirmPassword" Display="Dynamic" ErrorMessage="Confirm password is required.">*</asp:RequiredFieldValidator>
                                                                                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtPassword"
                                                                                                ControlToValidate="txtConfirmPassword" ErrorMessage="Confirm password must match password.">*</asp:CompareValidator>
                                                                                            <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="server" TargetControlID="RequiredFieldValidator6"
                                                                                                Enabled="True">
                                                                                            </cc1:ValidatorCalloutExtender>
                                                                                            <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="server" TargetControlID="CompareValidator1"
                                                                                                Enabled="True">
                                                                                            </cc1:ValidatorCalloutExtender>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" valign="top" class="whitetext1">
                                                                                            &nbsp;First name:
                                                                                        </td>
                                                                                        <td align="left" valign="top">
                                                                                            <asp:TextBox ID="txtFirstName" runat="server" MaxLength="50" CssClass="inputbox005"></asp:TextBox>
                                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="firstentry"
                                                                                                runat="server" ControlToValidate="txtFirstName" ErrorMessage="First name is required."
                                                                                                Display="Dynamic">*</asp:RequiredFieldValidator>
                                                                                            <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server" TargetControlID="RequiredFieldValidator3"
                                                                                                Enabled="True">
                                                                                            </cc1:ValidatorCalloutExtender>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" valign="top" class="whitetext1">
                                                                                            &nbsp;Last name:
                                                                                        </td>
                                                                                        <td align="left" valign="top">
                                                                                            <asp:TextBox ID="txtLastName" runat="server" MaxLength="50" CssClass="inputbox005"></asp:TextBox>
                                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="firstentry"
                                                                                                runat="server" ControlToValidate="txtLastName" ErrorMessage="Last name is required."
                                                                                                Display="Dynamic">*</asp:RequiredFieldValidator>
                                                                                            <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="server" TargetControlID="RequiredFieldValidator4"
                                                                                                Enabled="True">
                                                                                            </cc1:ValidatorCalloutExtender>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" valign="top" class="whitetext1">
                                                                                            &nbsp;Email address:
                                                                                        </td>
                                                                                        <td align="left" valign="top">
                                                                                            <asp:TextBox ID="txtEmail" runat="server" MaxLength="50" CssClass="inputbox005"></asp:TextBox>
                                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationGroup="firstentry"
                                                                                                runat="server" ControlToValidate="txtEmail" Display="Dynamic" ErrorMessage="Format of email address is invalid."
                                                                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtEmail"
                                                                                                ErrorMessage="Email is required." Display="Dynamic">*</asp:RequiredFieldValidator>
                                                                                            <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="server" TargetControlID="RequiredFieldValidator5"
                                                                                                Enabled="True">
                                                                                            </cc1:ValidatorCalloutExtender>
                                                                                            <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="server" TargetControlID="RegularExpressionValidator1"
                                                                                                Enabled="True">
                                                                                            </cc1:ValidatorCalloutExtender>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="display: none">
                                                                                        <td align="left" valign="top" class="whitetext1">
                                                                                            &nbsp;Prioirty task:
                                                                                        </td>
                                                                                        <td align="left" valign="top">
                                                                                            <asp:DropDownList Style="width: 200px; height: 20px; border: 1px solid rgb(130, 131, 134);
                                                                                                background-color: rgb(242, 246, 255);" ID="drpPriorities" runat="server" Width="107px">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" valign="top" class="whitetext1">
                                                                                            &nbsp;Default selected item:
                                                                                        </td>
                                                                                        <td align="left" valign="top">
                                                                                            <asp:DropDownList Style="width: 200px; height: 23px; border: 1px solid rgb(130, 131, 134);
                                                                                                background-color: rgb(242, 246, 255);" ID="drpFilter" runat="server" CssClass="filerDrpodown"
                                                                                                Width="205px">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" valign="top" class="whitetext1">
                                                                                            &nbsp;Date Of Joining:
                                                                                        </td>
                                                                                        <td align="left" valign="top">
                                                                                            <asp:TextBox ID="txtDOJ" runat="server" MaxLength="50" CssClass="inputbox005"></asp:TextBox>
                                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="firstentry"
                                                                                                runat="server" ControlToValidate="txtDOJ" ErrorMessage="Date of joining is required."
                                                                                                Display="Dynamic">*</asp:RequiredFieldValidator>
                                                                                            <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="server" TargetControlID="RequiredFieldValidator4"
                                                                                                Enabled="True">
                                                                                            </cc1:ValidatorCalloutExtender>
                                                                                            <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtDOJ"
                                                                                                Format="dd/MM/yyyy" Enabled="True">
                                                                                            </cc1:CalendarExtender>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" valign="top" class="whitetext1">
                                                                                            &nbsp;Image url:
                                                                                        </td>
                                                                                        <td align="left" valign="top">
                                                                                            <asp:FileUpload ID="fileImgUpload" size="16" runat="server" onchange="setTextBoxValue(this);" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" valign="top" class="whitetext1">
                                                                                            &nbsp;Title:
                                                                                        </td>
                                                                                        <td align="left" valign="top" class="whitetext1">
                                                                                            <asp:TextBox ID="txtTitle" runat="server" CssClass="inputbox005"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" valign="top" class="whitetext1">
                                                                                            &nbsp;Skills:
                                                                                        </td>
                                                                                        <td align="left" valign="top" class="whitetext1">
                                                                                            <asp:TextBox ID="txtskills" runat="server" CssClass="inputbox005"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" valign="top" class="whitetext1">
                                                                                            &nbsp;Date Of Birth:
                                                                                        </td>
                                                                                        <td align="left" valign="top">
                                                                                            <asp:TextBox ID="txtdateofBirth" runat="server" MaxLength="50" CssClass="inputbox005"></asp:TextBox>
                                                                                            <asp:RequiredFieldValidator ID="ReqdateofBirth" runat="server" ValidationGroup="firstentry"
                                                                                                ControlToValidate="txtdateofBirth" ErrorMessage="Date of birth is required."
                                                                                                Display="Dynamic">*</asp:RequiredFieldValidator>
                                                                                            <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="server" TargetControlID="ReqdateofBirth"
                                                                                                Enabled="True">
                                                                                            </cc1:ValidatorCalloutExtender>
                                                                                            <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtdateofBirth"
                                                                                                Format="dd/MM/yyyy" Enabled="True">
                                                                                            </cc1:CalendarExtender>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" valign="top" class="whitetext1">
                                                                                            &nbsp;Designation:
                                                                                        </td>
                                                                                        <td align="left" valign="top">
                                                                                            <asp:DropDownList ID="ddlDesg" runat="server" Width="200px" />
                                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="*"
                                                                                                Display="Dynamic" ControlToValidate="ddlDesg" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" valign="top" class="whitetext1">
                                                                                            &nbsp;Company name:
                                                                                        </td>
                                                                                        <td align="left" valign="top">
                                                                                            <asp:DropDownList ID="ddlCompany" runat="server" Width="200px" InitialValue="--Select--" />
                                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="*"
                                                                                                ControlToValidate="ddlCompany" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" valign="top" class="whitetext1">
                                                                                            &nbsp;Resume
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:FileUpload ID="fileupload" size="16" runat="server" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2">
                                                                                            <asp:CustomValidator ID="ImgValid" runat="server" ValidationGroup="firstentry" ErrorMessage="Please select an image"
                                                                                                ClientValidationFunction="validateImg" ControlToValidate="fileImgUpload"></asp:CustomValidator>
                                                                                            <asp:CustomValidator ID="CustomValidator1" runat="server" ValidationGroup="firstentry"
                                                                                                ErrorMessage="Invalid file uploaded. Only jpeg, .jpg, .gif or .png may be submitted."
                                                                                                ClientValidationFunction="validateImgFileFormats" ControlToValidate="fileImgUpload"></asp:CustomValidator>
                                                                                            <asp:Label ID="lblErrorMsg" runat="server"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" valign="top" class="whitetext1" colspan="2">
                                                                                            <input id="chkIsCW" runat="server" onclick="" type="checkbox" checked="checked" />&nbsp;<asp:Label
                                                                                                ID="lblIsEmployee" runat="server" Text="Is Currently Working ?"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <!-- <td align="left" valign="top" class="whitetext1">&nbsp;
                                                                                                
                                                                                            </td>-->
                                                                                        <td align="left" valign="top">
                                                                                            <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="firstentry" runat="server"
                                                                                                ShowMessageBox="True" ShowSummary="False" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" valign="top">
                                                                                <table width="100%" border="0" cellspacing="0" cellpadding="8">
                                                                                    <tr>
                                                                                        <td align="left" valign="top" class="whitetext2">
                                                                                            <span class="whitetext1"><span style="font-size: 14px; font-weight: bold; display: block;
                                                                                                margin-left: 5px; margin-bottom: -12px;">Only email this priority:</span></span>
                                                                                            <br>
                                                                                            <%--<input id="chkReqPR" runat="server" onclick="setAllChecked(this);" type="checkbox" />
                                                                                            &nbsp;All priorities<br />
                                                                                            <asp:DataList ID="Repeater2" runat="server" RepeatColumns="3" RepeatDirection="Horizontal">
                                                                                                <ItemTemplate>
                                                                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <%# DataBinder.Eval(Container.DataItem, "RePRChk")%>
                                                                                                                <span style="float: right\9;">
                                                                                                                    <%# DataBinder.Eval(Container.DataItem, "priority_name")%></span>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </ItemTemplate>
                                                                                            </asp:DataList>--%>
                                                                                            <asp:UpdatePanel ID="up1" runat="server">
                                                                                            <ContentTemplate>
                                                                                            <asp:CheckBox ID="chkReqPR" runat="server" Text="All priorities" OnCheckedChanged="chkReqPR_CheckedChanged" AutoPostBack="true"/>
                                                                                            
                                                                                            <asp:DataList ID="Repeater2" runat="server" RepeatColumns="3" RepeatDirection="Horizontal">
                                                                                                <ItemTemplate>
                                                                                                   <table border="0" cellpadding="0" cellspacing="0">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:CheckBox ID="RePRChk" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "priority_name")%>' />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </ItemTemplate>
                                                                                            </asp:DataList>
                                                                                            </ContentTemplate>
                                                                                            </asp:UpdatePanel>
                                                                                            
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" valign="top" class="whitetext2">
                                                                                            <table width="100%" border="0" cellpadding="3" cellspacing="0" style="margin-left: -3px;">
                                                                                                <tr valign="top">
                                                                                                    <td align="left" width="40%">
                                                                                                        <asp:CheckBox ID="chkAdmin" runat="server" Text="Admin" />
                                                                                                    </td>
                                                                                                    <td align="left">
                                                                                                        <asp:CheckBox ID="chkDeleteTasks" runat="server" Text="Can delete tasks" Style="margin-left: 12px;" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr valign="top">
                                                                                                    <td align="left">
                                                                                                        <asp:CheckBox ID="chkShowDeletedTasks" runat="server" Text="Show deleted tasks" />
                                                                                                    </td>
                                                                                                    <td align="left">
                                                                                                        <asp:CheckBox ID="chkShowClosedTasks" runat="server" Text="Show closed tasks" Style="margin-left: 12px;" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr valign="top">
                                                                                                    <td align="left">
                                                                                                        <asp:CheckBox ID="chkShowClientRequests" runat="server" Text="Show client requests"
                                                                                                            Style="float: right\9; margin-right: -5px\9;" />
                                                                                                    </td>
                                                                                                    <td align="left">
                                                                                                        <asp:CheckBox ID="chkEditDelete" runat="server" Text="Can edit/delete comments &amp; attachments"
                                                                                                            Style="float: left; margin-top: 1px; margin-left: 12px;" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr align="left" valign="top">
                                                                                                    <td align="left">
                                                                                                        <asp:CheckBox ID="chkCanApprove" runat="server" Text="Can Approve" />
                                                                                                    </td>
                                                                                                    <td align="left">
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr align="left" valign="top">
                                                                                                    <td align="left">
                                                                                                        <asp:CheckBox ID="ChkShowPrivate" runat="server" Text="See all private tasks" />
                                                                                                    </td>
                                                                                                    <td align="left">
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" class="whitetext2" valign="top">
                                                                                            <asp:CheckBox ID="chkReqStatus" runat="server" Text="Required CWO status" Checked="true" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" valign="top" class="whitetext2">
                                                                                            <span class="whitetext2">
                                                                                                <asp:CheckBox ID="chkActive" runat="server" Checked="True" Text="Active" />
                                                                                                &nbsp;&nbsp;&nbsp;<asp:CheckBox ID="chkSubscription" runat="server" Text="Subscribe to all tasks" />
                                                                                                &nbsp;&nbsp;&nbsp;<asp:CheckBox ID="chkprivatesoution" runat="server" Text="Private solution" />
                                                                                            </span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" valign="top" class="whitetext2">
                                                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-left: -9px;">
                                                                                                <tr>
                                                                                                    <td class="whitetext1">
                                                                                                        &nbsp;&nbsp;&nbsp;Email notification settings
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:RadioButtonList ID="rbEmailNotification" runat="server" RepeatColumns="2">
                                                                                                            <asp:ListItem Selected="True" Value="3">Email all changes</asp:ListItem>
                                                                                                            <asp:ListItem Value="0">Email comments only</asp:ListItem>
                                                                                                        </asp:RadioButtonList>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:HiddenField ID="hdnReqPR" runat="server" Value="All" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" style="width: 100px">
                                                            <img height="16" src="../images/loginBox_btm.png" width="420" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top">
                                                <hr />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 424px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <div align="right" style="float: right;">
                                                    <asp:Button ID="btnCreate" runat="server" Text="Create" ValidationGroup="firstentry"
                                                        OnClick="btnCreate_Click" CssClass="blueBtns" />
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 424px">
                                                <div id="divMessage2" style="color: Red" runat="server">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td valign="top">
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <img runat="server" id="EmpImage" onerror="this.src='EmpImages/NoImage.png'" onclick=""
                                                    style="margin: 0px 0 0 0px; cursor: pointer" width="142" height="155" />
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </cc1:TabPanel>
                <%--=========================================================================================================--%>
                <cc1:TabPanel ID="TabAddress" runat="server" HeaderText="Address">
                    <ContentTemplate>
                        <table width="424" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td align="left" valign="top">
                                    <img height="16" src="../images/loginBox_top.png" width="420" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background: url(../images/loginBox_mid.png) repeat-y left top;">
                                    <table cellspacing="2" cellpadding="2" border="0" width="93%">
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                &nbsp;Phone 1:
                                            </td>
                                            <td align="left" valign="top">
                                                <asp:TextBox ID="txtphon1" runat="server" CssClass="inputbox005"></asp:TextBox>
                                                <%--  <asp:RequiredFieldValidator ID="Reqtxtphon1" runat="server" ControlToValidate="txtphon1"
                                                        ValidationGroup="Employee2tab" ErrorMessage="Phone no is required." Display="Dynamic">*</asp:RequiredFieldValidator>
                                                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="server" TargetControlID="Reqtxtphon1">
                                                    </cc1:ValidatorCalloutExtender>--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                &nbsp;Phone 2:
                                            </td>
                                            <td align="left" valign="top">
                                                <asp:TextBox ID="txtphon2" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                &nbsp;Phone 3:
                                            </td>
                                            <td align="left" valign="top">
                                                <asp:TextBox ID="txtphone3" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr align="center">
                                            <td colspan="2" align="left" valign="top" class="whitetext1">
                                                &nbsp;Permanent address
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                &nbsp;Address 1:
                                            </td>
                                            <td align="left" valign="top">
                                                <asp:TextBox ID="txtaddress1" runat="server" TextMode="MultiLine" CssClass="messagebox006"></asp:TextBox>
                                                <%-- <asp:RequiredFieldValidator ID="Reqtxtaddress1" runat="server" ControlToValidate="txtaddress1"
                                                        ValidationGroup="Employee2tab" ErrorMessage="Please enter address." Display="Dynamic">*</asp:RequiredFieldValidator>
                                                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="server" TargetControlID="Reqtxtaddress1">
                                                    </cc1:ValidatorCalloutExtender>--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                &nbsp;Address 2:
                                            </td>
                                            <td align="left" valign="top">
                                                <asp:TextBox ID="txtaddress2" runat="server" TextMode="MultiLine" CssClass="messagebox006"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                &nbsp;Landmark
                                            </td>
                                            <td align="left" valign="top">
                                                <asp:TextBox ID="txtlandmark" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                &nbsp;City:
                                            </td>
                                            <td align="left" valign="top">
                                                <asp:TextBox ID="txtcity" runat="server" CssClass="inputbox005"></asp:TextBox>
                                                <%--<asp:RequiredFieldValidator ID="ReqcityId" runat="server" ControlToValidate="txtcity"
                                                        ValidationGroup="Employee2tab" ErrorMessage="Please enter city." Display="Dynamic">*</asp:RequiredFieldValidator>
                                                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="server" TargetControlID="ReqcityId">
                                                    </cc1:ValidatorCalloutExtender>--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                &nbsp;Postcode:
                                            </td>
                                            <td align="left" valign="top">
                                                <asp:TextBox ID="txtpostcode" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                &nbsp;State:
                                            </td>
                                            <td align="left" valign="top">
                                                <asp:TextBox ID="txtstate" runat="server" CssClass="inputbox005"></asp:TextBox>
                                                <%--<asp:RequiredFieldValidator ID="ReqstateId" runat="server" ControlToValidate="txtstate"
                                                        ValidationGroup="Employee2tab" ErrorMessage="Please enter state name." Display="Dynamic">*</asp:RequiredFieldValidator>
                                                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender14" runat="server" TargetControlID="ReqstateId">
                                                    </cc1:ValidatorCalloutExtender>--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                &nbsp;Country
                                            </td>
                                            <td align="left" valign="top">
                                                <asp:TextBox ID="txtCountry" runat="server" CssClass="inputbox005"></asp:TextBox>
                                                <%--<asp:RequiredFieldValidator ID="ReqtxtCountryId" runat="server" ControlToValidate="txtCountry"
                                                        ValidationGroup="Employee2tab" ErrorMessage="Please enter country name" Display="Dynamic">*</asp:RequiredFieldValidator>
                                                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="server" TargetControlID="ReqtxtCountryId">
                                                    </cc1:ValidatorCalloutExtender>--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                &nbsp;Skype/ Hotmail
                                                <br />
                                                &nbsp;Personal email id:
                                            </td>
                                            <td align="left" valign="top">
                                                <asp:TextBox ID="txtemailId" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                &nbsp;Fathers name:
                                            </td>
                                            <td align="left" valign="top">
                                                <asp:TextBox ID="txtfathername" CssClass="inputbox005" runat="server"></asp:TextBox>
                                                <%--<asp:RequiredFieldValidator ID="Reqtxtfathername" runat="server" ControlToValidate="txtfathername"
                                                        ValidationGroup="Employee2tab" ErrorMessage="Please enter father name" Display="Dynamic">*</asp:RequiredFieldValidator>
                                                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="server" TargetControlID="Reqtxtfathername">
                                                    </cc1:ValidatorCalloutExtender>--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                &nbsp;Mothers name:
                                            </td>
                                            <td align="left" valign="top">
                                                <asp:TextBox ID="txtmothername" runat="server" CssClass="inputbox005"></asp:TextBox>
                                                <%--<asp:RequiredFieldValidator ID="Reqtxtmothername" runat="server" ControlToValidate="txtmothername"
                                                        ValidationGroup="Employee2tab" ErrorMessage="Please enter mother name" Display="Dynamic">*</asp:RequiredFieldValidator>
                                                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender17" runat="server" TargetControlID="Reqtxtmothername">
                                                    </cc1:ValidatorCalloutExtender>--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                &nbsp;Relationship Status
                                                <br />
                                                &nbsp;(Single/ Married)
                                            </td>
                                            <td align="left" valign="top">
                                                <asp:TextBox ID="txtmarriedorSingle" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 424px">
                                                <div id="div1" style="color: Red" runat="server" visible="true">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 100px">
                                    <img height="16" src="../images/loginBox_btm.png" width="420" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <hr />
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <div style="float: right;">
                                        <asp:Button ID="btnscond" CausesValidation="false" runat="server" Text="Update" CssClass="blueBtns"
                                            OnClick="btnscond_Click" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 424px">
                                    <div id="AddressMsg" style="color: Red" runat="server" visible="true">
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </cc1:TabPanel>
                <%--=========================================================================================================--%>
                <cc1:TabPanel ID="TabEducation" runat="server" HeaderText="Education">
                    <ContentTemplate>
                        <table width="424" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td align="left" valign="top">
                                    <img height="16" src="../images/loginBox_top.png" width="420" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background: url(../images/loginBox_mid.png) repeat-y left top;">
                                    <table cellspacing="2" cellpadding="2" border="0" width="93%" style="margin-left: 5px;">
                                        <tr valign="top">
                                            <td align="left" valign="top" class="whitetext1" style="color: Red;">
                                                Course (Hign school)
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txt10" CssClass="inputbox005" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Specialization
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtspecialization" runat="server" CssClass="messagebox006" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Intitute/Univercity
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtinstitute" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Address
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Year
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtyear" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Aggregate Mark
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtmarks" Width="200px" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1" style="color: Red">
                                                Course(Higher Secondary)
                                            </td>
                                            <td>
                                                <asp:TextBox ID="lbl12" CssClass="inputbox005" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Specialization
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtspecializationHSC" runat="server" CssClass="messagebox006" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Intitute/Univercity
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtinstituteHSC" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Address
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtadddres12" CssClass="messagebox006" runat="server" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Month/Yr.
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtmonthHSC" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Aggregate Mark
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtmarks12" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1" style="color: Red">
                                                Course(Degree)
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtcourseBsc" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Specialization
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtspecDEG" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Institute/Univercity
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtinsDEG" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Address
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtAddBsc" CssClass="messagebox006" runat="server" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Month/Yr.
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtmonthDEG" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Aggregate Mark
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TxtAggreBsc" CssClass="inputbox005" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1" style="color: Red">
                                                &nbsp;Course (PG)
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtcoursePG" CssClass="inputbox005" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Specialization
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtspedPG" runat="server" CssClass="messagebox006" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Intitute/Univercity
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtinstitutePG" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Address
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtaddressPG" CssClass="messagebox006" runat="server" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Month/Yr.
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtyearPG" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Aggregate Mark
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtAggMarkePG" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Training
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txttraining" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Institute
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtinstituteTraining" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Location
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtlocation" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Date Attended(From)
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtattendedfrom" runat="server" CssClass="inputbox005"></asp:TextBox>
                                                <cc1:CalendarExtender ID="CalendarExtender21" runat="server" TargetControlID="txtattendedfrom"
                                                    Format="dd/MM/yyyy" Enabled="True">
                                                </cc1:CalendarExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Date Attended (To)
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtdateattended" runat="server" CssClass="inputbox005"></asp:TextBox>
                                                <cc1:CalendarExtender ID="Caltxtdateattended2" runat="server" TargetControlID="txtdateattended"
                                                    Format="dd/MM/yyyy" Enabled="True">
                                                </cc1:CalendarExtender>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 100px">
                                    <img height="16" src="../images/loginBox_btm.png" width="420" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <hr />
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <div style="float: right;">
                                        <asp:Button ID="Button1" CausesValidation="False" runat="server" Text="Update" CssClass="blueBtns"
                                            OnClick="Button1_Click" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 424px">
                                    <div id="DivEducation" style="color: Red" runat="server">
                                    </div>
                                </td>
                            </tr>
                        </table>
                        </td> </table>
                    </ContentTemplate>
                </cc1:TabPanel>
                <%--=========================================================================================================--%>
                <cc1:TabPanel ID="TabpreviousEmployment" runat="server" HeaderText="Previous Employment">
                    <ContentTemplate>
                        <table width="424" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td align="left" valign="top">
                                    <img height="16" src="../images/loginBox_top.png" width="420" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background: url(../images/loginBox_mid.png) repeat-y left top;">
                                    <table cellspacing="2" cellpadding="2" border="0" width="93%" style="margin-left: 5px;">
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Name of Employer
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtemployer" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Date Employed(From)
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtdateemployed" runat="server" CssClass="inputbox005"></asp:TextBox>
                                                <cc1:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="txtdateemployed"
                                                    Format="dd/MM/yyyy">
                                                </cc1:CalendarExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Date Employed(To)
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtdateemployedTo" runat="server" CssClass="inputbox005"></asp:TextBox>
                                                <cc1:CalendarExtender ID="CalendarExtender4" runat="server" TargetControlID="txtdateemployedTo"
                                                    Format="dd/MM/yyyy">
                                                </cc1:CalendarExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Address
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtaddresscompany" runat="server" CssClass="messagebox006" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Phone
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtphoneaddress" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Designation
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtdesignation" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Department
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtdepartment" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Employee Code
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtemployeecode" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Responseblities
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtboxres" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Reason for Leaving
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtResleaving" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Monthly Gross
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtmonthlygross" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 100px">
                                    <img height="16" src="../images/loginBox_btm.png" width="420" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <hr />
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <div style="float: right;">
                                        <asp:Button ID="Button2" CausesValidation="false" runat="server" Text="Create" CssClass="blueBtns"
                                            OnClick="OtherEmployer_Click" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 424px">
                                    <div id="DivOtherEmployer" style="color: Red" runat="server" visible="true">
                                    </div>
                                </td>
                            </tr>
                        </table>
                        </td> </table>
                    </ContentTemplate>
                </cc1:TabPanel>
                <%--=========================================================================================================--%>
                <cc1:TabPanel ID="TabotherpreviousEmployment" runat="server" HeaderText="OtherPrevious Employment">
                    <ContentTemplate>
                        <table width="700" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td align="left" valign="top">
                                    <!-- <img height="16" src="../images/loginBox_top.png" width="936" />-->
                                    <div style="width: 934px; height: 16px; background-color: #20235a; border-radius: 20px 20px 0 0;"
                                        class="bgcolor">
                                    </div>
                                </td>
                            </tr>
                            </tr> </tr>
                            <td align="left" style="background: url(../images/loginBox_mid.png) repeat-y left top;">
                            </td>
                            <table border="0" cellpadding="2" cellspacing="2" class="bgcolor" style="background: none repeat scroll 0 0 #20235A;
                                color: #FFF; width: 934px;">
                                <tr>
                                    <td style="margin: 0px; padding: 0px;">
                                        <asp:GridView ID="gvpreviousemployee" runat="server" AutoGenerateColumns="False"
                                            CssClass="grdi001" Font-Overline="True" PageSize="5" ShowFooter="True" Style="border-collapse: collapse;
                                            text-decoration: none;">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Name of Organization">
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtNameofOrganization" runat="server" CssClass="messagebox004" TextMode="MultiLine" />
                                                    </FooterTemplate>
                                                    <ItemTemplate>
                                                        <%# Eval("NameofOrganization")%>
                                                    </ItemTemplate>
                                                    <ControlStyle CssClass="whitetext1" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Location">
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtLocation" runat="server" CssClass="messagebox004" TextMode="MultiLine" />
                                                    </FooterTemplate>
                                                    <ItemTemplate>
                                                        <%# Eval("Location")%>
                                                    </ItemTemplate>
                                                    <ControlStyle CssClass="whitetext1" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Duration (From)">
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtDurationFrom" runat="server" CssClass="inputbox004" />
                                                        <cc1:CalendarExtender ID="CalendarExtfrom" runat="server" Format="dd/MM/yyyy" TargetControlID="txtDurationFrom">
                                                        </cc1:CalendarExtender>
                                                    </FooterTemplate>
                                                    <ItemTemplate>
                                                        <%# Eval("DurationFrom")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtDurationTo" runat="server" CssClass="inputbox004" />
                                                        <cc1:CalendarExtender ID="CalendarExtTo" runat="server" Format="dd/MM/yyyy" TargetControlID="txtDurationTo">
                                                        </cc1:CalendarExtender>
                                                    </FooterTemplate>
                                                    <ItemTemplate>
                                                        <%# Eval("DurationTo")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtReasonforLeaving" runat="server" CssClass="messagebox004" TextMode="MultiLine" />
                                                    </FooterTemplate>
                                                    <ItemTemplate>
                                                        <%# Eval("ReasonforLeaving")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <FooterTemplate>
                                                        <asp:Button ID="btnAdd" runat="server" CommandName="Footer" OnClick="Add" Text="Add" />
                                                    </FooterTemplate>
                                                    <ItemTemplate>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataTemplate>
                                                <tr align="left" class="whitetext1" style="text-decoration: none;" valign="top">
                                                    <th scope="col">
                                                        Name of Oganization
                                                    </th>
                                                    <th scope="col">
                                                        Location
                                                    </th>
                                                    <th scope="col">
                                                        Duration From
                                                    </th>
                                                    <th scope="col">
                                                        Duration To
                                                    </th>
                                                    <th scope="col">
                                                        Reason for leaving
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtNameofOrganization" runat="server" TextMode="MultiLine" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtLocation" runat="server" TextMode="MultiLine" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtDurationFrom" runat="server" />
                                                        <cc1:CalendarExtender ID="CalendarExtfrom1" runat="server" Format="dd/MM/yyyy" TargetControlID="txtDurationFrom">
                                                        </cc1:CalendarExtender>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtDurationTo" runat="server" />
                                                        <cc1:CalendarExtender ID="CalendarExtTo2" runat="server" Format="dd/MM/yyyy" TargetControlID="txtDurationTo">
                                                        </cc1:CalendarExtender>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtReasonforLeaving" runat="server" TextMode="MultiLine" />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnAdd" runat="server" CommandName="EmptyDataTemplate" CssClass="blueBtns"
                                                            OnClick="Add" Text="Create" />
                                                    </td>
                                                </tr>
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                            </td>
                            <tr>
                                <td align="left" style="width: 100px">
                                    <!--<img height="16" src="../images/loginBox_btm.png" width="936" />-->
                                    <div style="width: 934px; height: 16px; background-color: #20235a; border-radius: 0 0 20px 20px;"
                                        class="bgcolor">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <hr />
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <br />
                                    <div style="float: right; padding-bottom: 10px; margin-top: -18px;">
                                        <asp:Button ID="BtnPreviousdetails" runat="server" Text="Create" CssClass="blueBtns"
                                            OnClick="OtherPreviousEmployer_Click" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                        </td> </table>
                    </ContentTemplate>
                </cc1:TabPanel>
                <%--=========================================================================================================--%>
                <cc1:TabPanel ID="Tababoutyourself" runat="server" HeaderText="About yourself">
                    <HeaderTemplate>
                        About yourself
                    </HeaderTemplate>
                    <ContentTemplate>
                        <table width="424" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td align="left" valign="top">
                                    <img height="16" src="../images/loginBox_top.png" width="420" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background: url(../images/loginBox_mid.png) repeat-y left top;
                                    width: 936px; color: #FFF;">
                                    <table cellspacing="2" cellpadding="2" border="0" width="93%" style="margin-left: 5px;">
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Your Personal strengths
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtstrengts" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Area of improvements
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtimprovement" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Your career objectives?
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtobjective" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                expectations from the job
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtexpectations" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Significant achivements
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtachievements" runat="server" CssClass="messagebox006" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Other Interests
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtotherinterests" runat="server" CssClass="messagebox006" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 100px">
                                    <img height="16" src="../images/loginBox_btm.png" width="420" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <hr />
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <div style="float: right;">
                                        <asp:Button ID="btnyourself" CausesValidation="False" runat="server" Text="Create"
                                            CssClass="blueBtns" OnClick="btnProjectDetails_Click" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 424px">
                                    <div id="divyourself" style="color: Red" runat="server">
                                    </div>
                                </td>
                            </tr>
                        </table>
                        </td> </table>
                    </ContentTemplate>
                </cc1:TabPanel>
                <%--=========================================================================================================--%>
                <cc1:TabPanel ID="TabReferences" runat="server" HeaderText="References">
                    <ContentTemplate>
                        <table width="424" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td align="left" valign="top">
                                    <img height="16" src="../images/loginBox_top.png" width="420" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background: url(../images/loginBox_mid.png) repeat-y left top;">
                                    <table cellspacing="2" cellpadding="2" border="0" width="93%" style="margin-left: 5px;">
                                        <tr>
                                            <td colspan="2" valign="top" align="left" class="whitetext1" style="color: Red">
                                                First friend
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Name
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtname" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Nature of Association
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtorga" Width="200px" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Organization & designation
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtdesignationfriend" Width="200px" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                email
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtemailfriend" Width="200px" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Phone
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtphone" Width="200px" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Mobile
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtmobilefriend" Width="200px" runat="server" CssClass="inputbox005"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" valign="top" align="left" class="whitetext1" style="color: Red">
                                                Second friend
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Name
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtname2" Width="200px" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Nature of Association
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtorga2" Width="200px" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Organization & designation
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtdesignationfriend2" Width="200px" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                email
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtemailfriend2" Width="200px" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Phone
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtphone2" Width="200px" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Mobile
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtmobilefriend2" Width="200px" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 100px">
                                    <img height="16" src="../images/loginBox_btm.png" width="420" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <hr />
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <div style="float: right;">
                                        <asp:Button ID="btnReferences" runat="server" CausesValidation="false" Text="Create"
                                            CssClass="blueBtns" OnClick="btnReferences_Click" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 424px">
                                    <div id="DivReferences" style="color: Red" runat="server" visible="true">
                                    </div>
                                </td>
                            </tr>
                        </table>
                        </td> </table>
                    </ContentTemplate>
                </cc1:TabPanel>
                <%--=========================================================================================================--%>
                <cc1:TabPanel ID="TabAddDetails" runat="server" HeaderText="Additional Details">
                    <ContentTemplate>
                        <table width="424" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td align="left" valign="top">
                                    <img height="16" src="../images/loginBox_top.png" width="420" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background: url(../images/loginBox_mid.png) repeat-y left top;">
                                    <table cellspacing="2" cellpadding="2" border="0" width="93%" style="margin-left: 5px;">
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Nationality
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtnationality" Width="200px" runat="server" CssClass="messagebox006"
                                                    TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Do you hold a Valid Passport
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtvalidPasswort" Width="200px" runat="server" CssClass="messagebox006"
                                                    TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Have you denied visa to any country
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtdeniedVisa" Width="200px" runat="server" CssClass="messagebox006"
                                                    TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Have you worked on overseas assignments?
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtoverseasAssignment" Width="200px" runat="server" CssClass="messagebox006"
                                                    TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Have you attended any selection process at ESG before
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtesgprocess" Width="200px" runat="server" CssClass="messagebox006"
                                                    TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Do you any relative /friend employed with ESG
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtfriendEsg" Width="200px" runat="server" CssClass="messagebox006"
                                                    TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Have you suffered from any major illness
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtillness" Width="200px" runat="server" CssClass="messagebox006"
                                                    TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Have you ever been arrested,Prosecuted or convicted for any criminal case
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtarrest" Width="200px" runat="server" CssClass="messagebox006"
                                                    TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Do you have any commitment to another employer or organization that might affect
                                                you employment with us?
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtcommitment" Width="200px" runat="server" CssClass="messagebox006"
                                                    TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Any other information you would like us to knowwhich may have significantbearing
                                                on our hiring decision
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtotherinfo" Width="200px" runat="server" CssClass="messagebox006"
                                                    TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 100px">
                                    <img height="16" src="../images/loginBox_btm.png" width="420" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <hr />
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <div style="float: right;">
                                        <asp:Button ID="btnadditionalDet" runat="server" OnClick="btnAdddetails_Click" Text="Create"
                                            CssClass="blueBtns" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 424px">
                                    <div id="DivaddDetails" style="color: Red" runat="server">
                                    </div>
                                </td>
                            </tr>
                        </table>
                        </td> </table>
                    </ContentTemplate>
                </cc1:TabPanel>
                <%--=========================================================================================================--%>
                <cc1:TabPanel ID="TabProjectDetails" runat="server" HeaderText="Project details">
                    <ContentTemplate>
                        <table width="424" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td align="left" valign="top">
                                    <img height="16" src="../images/loginBox_top.png" width="420" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background: url(../images/loginBox_mid.png) repeat-y left top;
                                    width: 936px; color: #FFF;">
                                    <table cellspacing="2" cellpadding="2" border="0" width="93%" style="margin-left: 5px;">
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Company
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlCompName" runat="server" CssClass="inputbox005" OnSelectedIndexChanged="ddlCompName_SelectedIndexChanged"
                                                    AutoPostBack="True">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Project
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlProject" runat="server" CssClass="inputbox005">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="whitetext1">
                                                Date of association
                                            </td>
                                            <td align="left" valign="top">
                                                <asp:TextBox ID="txtDOA" runat="server" MaxLength="50" CssClass="inputbox005"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ValidationGroup="firstentry"
                                                    ControlToValidate="txtDOA" ErrorMessage="Date of association." Display="Dynamic">*</asp:RequiredFieldValidator>
                                                <cc1:CalendarExtender ID="CalendarExtender5" runat="server" TargetControlID="txtDOA"
                                                    Format="dd/MM/yyyy" Enabled="True">
                                                </cc1:CalendarExtender>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 100px">
                                    <img height="16" src="../images/loginBox_btm.png" width="420" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <hr />
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <div style="float: right;">
                                        <asp:Button ID="btnProjectDetails" runat="server" Text="Create" CssClass="blueBtns"
                                            OnClick="btnProjectDetails_Click" CausesValidation="False" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 424px">
                                    <div id="div2" style="color: Red" runat="server">
                                    </div>
                                </td>
                            </tr>
                        </table>
                        </td> </table>
                    </ContentTemplate>
                </cc1:TabPanel>
                <%--=========================================================================================================--%>
            </cc1:TabContainer>
            <div id="divMessage" runat="server" style="color: Red; font-weight: bold">
            </div>
            <br />
            <div>
            </div>
        </div>
    </div>
    <uc1:footer ID="Footer1" runat="server" />
    </form>
</body>
</html>
