using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using System.Globalization;

public partial class Admin_edit_user : System.Web.UI.Page
{
    int id;
    string sql = "";
    string strDOJ = "";
    DateTime dt = DateTime.Now;
    DateTime dtDOB = DateTime.Now;
    string DesgId = "";
    string CompanyName = "";
    string strDOB = "";
    string strDate = "";
    string strDtDOB = "", strDtDOA="";
    DataSet dsComp;
    DataSet dsDesg,dsProj,dsProjDetails;
    DataSet ds;
    static int index;
    public string username = "";


    clsLeave_Logic objLeave = new clsLeave_Logic();
    bool blnIsTimeOfJoining = false;
    static string sstrEmpImgURL = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = (Request.QueryString["id"] == null || Request.QueryString["id"].ToString() == "" ? "tasks.aspx" : "edit_user.aspx?id=" + Request.QueryString["id"].ToString());
            Response.Redirect("login.aspx");
            return;
        }

        string var = Request.QueryString["id"];
        if (var == null)
        {
            id = 0;
            this.Title = "Add user";
        }
        else
        {
            try
            {
                id = Convert.ToInt32(var);
                this.Title = "Edit user";
            }
            catch
            {
                divMessage.InnerHtml = "User ID must be an integer.";
                DivEntry.Visible = false;
                return;
            }
        }
        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            object objUserId = DatabaseHelper.executeScalar("select user_id from users where username = '" + Session["admin"].ToString() + "'");

            if (id.ToString() != objUserId.ToString())
            {
                Response.Redirect("tasks.aspx");
                return;
            }
            divLinks.Visible = false;
        }
        if (Session["TabIndex"] != null)
            TabContainer1.ActiveTabIndex =Convert.ToInt32(Session["TabIndex"].ToString());

        if (!IsPostBack)
        {
            fnFillCompddl();
            fnFillDesgddl();
            fnFillProjectddl();
            load_filterDropDown();
          
            drpFilter.SelectedValue = "Show my open tasks";
            // add or edit?
            if (id == 0)
            {
                clearControls();
                hdnReqPR.Value = "All";
                ///load_Priority();
            }
            else
            {
                string strSql = "select * from users where user_id=" + id;
                ds = DatabaseHelper.getDataset(strSql);
                dsProjDetails=DatabaseHelper.getDataset("select * from projectdetails where user_id="+id);

                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        Session["Registered"] = id;
                        lblId.Text = ds.Tables[0].Rows[0]["user_id"].ToString();
                        txtUserName.Text = ds.Tables[0].Rows[0]["username"].ToString();
                        
                        txtPassword.Text = ds.Tables[0].Rows[0]["password"].ToString();
                        string pwd = ds.Tables[0].Rows[0]["password"].ToString();
                        txtPassword.Attributes.Add("value", pwd);
                        txtConfirmPassword.Attributes.Add("Value", pwd);
                        txtFirstName.Text = ds.Tables[0].Rows[0]["firstname"].ToString();
                        username = ds.Tables[0].Rows[0]["firstname"].ToString();
                        txtLastName.Text = ds.Tables[0].Rows[0]["lastname"].ToString();
                        txtEmail.Text = ds.Tables[0].Rows[0]["email"].ToString();
                        chkAdmin.Checked = ds.Tables[0].Rows[0]["admin"].ToString() == "Y" ? true : false;
                        chkSubscription.Checked = ds.Tables[0].Rows[0]["subscribe_to_all_tasks"].ToString() == "Y" ? true : false;
                        chkDeleteTasks.Checked = ds.Tables[0].Rows[0]["can_delete_tasks"].ToString() == "Y" ? true : false;
                        chkEditDelete.Checked = ds.Tables[0].Rows[0]["Can_edit_delete_comments_attachments"].ToString() == "Y" ? true : false;
                        chkActive.Checked = ds.Tables[0].Rows[0]["active"].ToString() == "Y" ? true : false;
                        chkCanApprove.Checked = ds.Tables[0].Rows[0]["CanApprove"].ToString() == "Y" ? true : false;
                        ChkShowPrivate.Checked = ds.Tables[0].Rows[0]["See_all_private_tasks"].ToString() == "Y" ? true : false;
                        chkShowClosedTasks.Checked = ds.Tables[0].Rows[0]["show_closed_tasks"].ToString() == "Y" ? true : false;
                        chkShowDeletedTasks.Checked = ds.Tables[0].Rows[0]["show_deleted_tasks"].ToString() == "Y" ? true : false;
                        chkShowClientRequests.Checked = ds.Tables[0].Rows[0]["show_client_requests"].ToString() == "Y" ? true : false;
                        chkReqStatus.Checked = ds.Tables[0].Rows[0]["Req_CWO_status"].ToString() == "Y" ? true : false;
                        rbEmailNotification.SelectedValue = ds.Tables[0].Rows[0]["EmailNotification"].ToString() == "Y" ? "1" : "0";
                        drpFilter.SelectedValue = ds.Tables[0].Rows[0]["default_selected_item"].ToString();
                        //===================================================================================================


                        txtphon1.Text = ds.Tables[0].Rows[0]["phone1"].ToString().Replace("''", "'");
                        txtphon2.Text = ds.Tables[0].Rows[0]["phone2"].ToString().Replace("''", "'");
                        txtphone3.Text = ds.Tables[0].Rows[0]["phone3"].ToString().Replace("''", "'");
                        txtaddress1.Text = ds.Tables[0].Rows[0]["Address1"].ToString().Replace("''", "'");
                        txtaddress2.Text = ds.Tables[0].Rows[0]["Address2"].ToString().Replace("''", "'");
                        txtlandmark.Text = ds.Tables[0].Rows[0]["Landmark"].ToString().Replace("''", "'");
                        txtcity.Text = ds.Tables[0].Rows[0]["City"].ToString().Replace("''", "'");
                        txtpostcode.Text = ds.Tables[0].Rows[0]["Postcode"].ToString().Replace("''", "'");
                        txtstate.Text = ds.Tables[0].Rows[0]["State"].ToString().Replace("''", "'");
                        txtCountry.Text = ds.Tables[0].Rows[0]["Country"].ToString().Replace("''", "'");
                        txtemailId.Text = ds.Tables[0].Rows[0]["Personal_email_id"].ToString().Replace("''", "'");
                        txtfathername.Text = ds.Tables[0].Rows[0]["Fathers_name"].ToString().Replace("''", "'");
                        txtmothername.Text = ds.Tables[0].Rows[0]["Mothers_name"].ToString().Replace("''", "'");
                        txtmarriedorSingle.Text = ds.Tables[0].Rows[0]["Married_status"].ToString().Replace("''", "'");


                        txt10.Text = ds.Tables[0].Rows[0]["Course_HS"].ToString().Replace("''", "'");
                        txtspecialization.Text = ds.Tables[0].Rows[0]["Specialization_HS"].ToString().Replace("''", "'");
                        txtinstitute.Text = ds.Tables[0].Rows[0]["Intitute_HS"].ToString().Replace("''", "'");
                        //txtaddress.Text = ds.Tables[0].Rows[0]["Address_HS"].ToString().Replace("''", "'");
                        txtyear.Text = ds.Tables[0].Rows[0]["Year_HS"].ToString().Replace("''", "'");
                        txtmarks.Text = ds.Tables[0].Rows[0]["Aggregate_Mark_HS"].ToString().Replace("''", "'");
                        lbl12.Text = ds.Tables[0].Rows[0]["Course_SS"].ToString().Replace("''", "'");
                        txtspecializationHSC.Text = ds.Tables[0].Rows[0]["Specialization_SS"].ToString().Replace("''", "'");
                        txtinstituteHSC.Text = ds.Tables[0].Rows[0]["Intitute_SS"].ToString().Replace("''", "'");
                        txtadddres12.Text = ds.Tables[0].Rows[0]["Address_SS"].ToString().Replace("''", "'");
                        txtmonthHSC.Text = ds.Tables[0].Rows[0]["Year_SS"].ToString().Replace("''", "'");
                        txtmarks12.Text = ds.Tables[0].Rows[0]["Aggregate_Mark_SS"].ToString().Replace("''", "'");
                        txtcourseBsc.Text = ds.Tables[0].Rows[0]["Course_G"].ToString().Replace("''", "'");
                        txtspecDEG.Text = ds.Tables[0].Rows[0]["Specialization_G"].ToString().Replace("''", "'");
                        txtinsDEG.Text = ds.Tables[0].Rows[0]["Intitute_G"].ToString().Replace("''", "'");
                        txtAddBsc.Text = ds.Tables[0].Rows[0]["Address_G"].ToString().Replace("''", "'");
                        txtmonthDEG.Text = ds.Tables[0].Rows[0]["Year_G"].ToString().Replace("''", "'");
                        TxtAggreBsc.Text = ds.Tables[0].Rows[0]["Aggregate_Mark_G"].ToString().Replace("''", "'");
                        txtcoursePG.Text = ds.Tables[0].Rows[0]["Course_PG"].ToString().Replace("''", "'");
                        txtspedPG.Text = ds.Tables[0].Rows[0]["Specialization_PG"].ToString().Replace("''", "'");
                        txtinstitutePG.Text = ds.Tables[0].Rows[0]["Intitute_PG"].ToString().Replace("''", "'");
                        txtaddressPG.Text = ds.Tables[0].Rows[0]["Address_PG"].ToString().Replace("''", "'");
                        txtyearPG.Text = ds.Tables[0].Rows[0]["Year_PG"].ToString().Replace("''", "'");
                        txtAggMarkePG.Text = ds.Tables[0].Rows[0]["Aggregate_Mark_PG"].ToString().Replace("''", "'");
                        txttraining.Text = ds.Tables[0].Rows[0]["Training"].ToString().Replace("''", "'");
                        txtinstituteTraining.Text = ds.Tables[0].Rows[0]["Institute"].ToString().Replace("''", "'");
                        txtlocation.Text = ds.Tables[0].Rows[0]["Location"].ToString().Replace("''", "'");
                        txtattendedfrom.Text = ds.Tables[0].Rows[0]["Date_Attended_From"].ToString().Replace("''", "'");
                        txtdateattended.Text = ds.Tables[0].Rows[0]["Date_Attended_To"].ToString().Replace("''", "'");




                        txtemployer.Text = ds.Tables[0].Rows[0]["Name_of_Employer"].ToString().Replace("''", "'");
                        txtdateemployed.Text = ds.Tables[0].Rows[0]["Date_Employed_From"].ToString().Replace("''", "'");
                        txtdateemployedTo.Text = ds.Tables[0].Rows[0]["Date_Employed_To"].ToString().Replace("''", "'");
                        txtaddresscompany.Text = ds.Tables[0].Rows[0]["Employer_Address"].ToString().Replace("''", "'");
                        txtphoneaddress.Text = ds.Tables[0].Rows[0]["Employer_Phone"].ToString().Replace("''", "'");
                        txtdesignation.Text = ds.Tables[0].Rows[0]["Designation"].ToString().Replace("''", "'");
                        txtdepartment.Text = ds.Tables[0].Rows[0]["Department"].ToString().Replace("''", "'");
                        txtemployeecode.Text = ds.Tables[0].Rows[0]["Employee_Code"].ToString().Replace("''", "'");
                        txtboxres.Text = ds.Tables[0].Rows[0]["Responseblities"].ToString().Replace("''", "'");
                        txtResleaving.Text = ds.Tables[0].Rows[0]["Reason_for_Leaving"].ToString().Replace("''", "'");
                        txtmonthlygross.Text = ds.Tables[0].Rows[0]["Monthly_Gross"].ToString().Replace("''", "'");


                        txtstrengts.Text = ds.Tables[0].Rows[0]["Your_Personal_strengts"].ToString().Replace("''", "'");
                        txtimprovement.Text = ds.Tables[0].Rows[0]["Area_of_improvements"].ToString().Replace("''", "'");
                        txtobjective.Text = ds.Tables[0].Rows[0]["Your_career_objectives"].ToString().Replace("''", "'");
                        txtexpectations.Text = ds.Tables[0].Rows[0]["expectations_from_job"].ToString().Replace("''", "'");
                        txtachievements.Text = ds.Tables[0].Rows[0]["Significant_achivements"].ToString().Replace("''", "'");
                        txtotherinterests.Text = ds.Tables[0].Rows[0]["Other_Interests"].ToString().Replace("''", "'");





                        txtname.Text = ds.Tables[0].Rows[0]["Name_1"].ToString().Replace("''", "'");
                        txtorga.Text = ds.Tables[0].Rows[0]["Nature_of_Association_1"].ToString().Replace("''", "'");
                        txtdesignationfriend.Text = ds.Tables[0].Rows[0]["Organization_designation_1"].ToString().Replace("''", "'");
                        txtemailfriend.Text = ds.Tables[0].Rows[0]["email_1"].ToString().Replace("''", "'");
                        txtphone.Text = ds.Tables[0].Rows[0]["Phone_1"].ToString().Replace("''", "'");
                        txtmobilefriend.Text = ds.Tables[0].Rows[0]["Mobile_1"].ToString().Replace("''", "'");
                        txtname2.Text = ds.Tables[0].Rows[0]["Name_2"].ToString().Replace("''", "'");
                        txtorga2.Text = ds.Tables[0].Rows[0]["Nature_of_Association_2"].ToString().Replace("''", "'");
                        txtdesignationfriend2.Text = ds.Tables[0].Rows[0]["Organization_designation_2"].ToString().Replace("''", "'");
                        txtemailfriend2.Text = ds.Tables[0].Rows[0]["email_2"].ToString().Replace("''", "'");
                        txtphone2.Text = ds.Tables[0].Rows[0]["Phone_2"].ToString().Replace("''", "'");
                        txtmobilefriend2.Text = ds.Tables[0].Rows[0]["Mobile_2"].ToString().Replace("''", "'");




                        txtnationality.Text = ds.Tables[0].Rows[0]["Nationality"].ToString().Replace("''", "'");
                        txtvalidPasswort.Text = ds.Tables[0].Rows[0]["validPasswort"].ToString().Replace("''", "'");
                        txtdeniedVisa.Text = ds.Tables[0].Rows[0]["deniedVisa"].ToString().Replace("''", "'");
                        txtoverseasAssignment.Text = ds.Tables[0].Rows[0]["overseasAssignment"].ToString().Replace("''", "'");
                        txtesgprocess.Text = ds.Tables[0].Rows[0]["esgprocess"].ToString().Replace("''", "'");
                        txtfriendEsg.Text = ds.Tables[0].Rows[0]["friendEsg"].ToString().Replace("''", "'");
                        txtillness.Text = ds.Tables[0].Rows[0]["illness"].ToString().Replace("''", "'");
                        txtarrest.Text = ds.Tables[0].Rows[0]["arrest"].ToString().Replace("''", "'");
                        txtcommitment.Text = ds.Tables[0].Rows[0]["commitment"].ToString().Replace("''", "'");
                        txtotherinfo.Text = ds.Tables[0].Rows[0]["otherinfo"].ToString().Replace("''", "'");



                        string strSql1 = "select * from Employee_Previous_Company where user_id=" + id;
                        DataSet ds1 = DatabaseHelper.getDataset(strSql1);

                        if (ds1 != null)
                        {
                            if (ds1.Tables.Count > 0)
                            {
                                gvpreviousemployee.DataSource = ds1;
                                gvpreviousemployee.DataBind();
                            }
                        }



                        //===================================================================================================

                        if (ds.Tables[0].Rows[0]["private_solutions"].ToString() == "True")
                        {
                            chkprivatesoution.Checked = true;
                        }
                        else
                        {
                            chkprivatesoution.Checked = false;
                        }
                        if (ds.Tables[0].Rows[0]["DOJ"].ToString() != "")
                        {
                            strDate = ds.Tables[0].Rows[0]["DOJ"].ToString();
                            DateTime dt = Convert.ToDateTime(strDate);
                            strDate = dt.ToString("dd/MM/yyyy"); //it is on live
                            //strDate = dt.ToString("MM/dd/yyyy"); //17/11/2014 SP changed the DateFormat
                        }

                        txtDOJ.Text = strDate;

                        //======================================================
                        if (ds.Tables[0].Rows[0]["DOB"].ToString() != "")
                        {
                            strDtDOB = ds.Tables[0].Rows[0]["DOB"].ToString();
                            DateTime dtbirth = Convert.ToDateTime(strDtDOB);
                            strDtDOB = dtbirth.ToString("dd/MM/yyyy"); //it is on live
                            //strDtDOB = dtbirth.ToString("MM/dd/yyyy"); //17/11/2014 SP changed the DateFormat
                        }
                        txtTitle.Text = ds.Tables[0].Rows[0]["Title"].ToString().Replace("''", "'");
                        txtskills.Text = ds.Tables[0].Rows[0]["Skills"].ToString().Replace("''", "'");
                        txtdateofBirth.Text = strDtDOB;
                        //RadEditor2.Content = ds.Tables[0].Rows[0]["CV"].ToString().Replace("''", "'");

                        //string Pkey = "";
                        //string uploadPath1 = Server.MapPath(Request.ApplicationPath + "\\Attachment\\EmployeeCV");
                        //if (System.IO.Directory.Exists(uploadPath1))
                        //{
                        //    System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(uploadPath1);
                        //    Pkey = System.Guid.NewGuid().ToString();
                        //    //======================================================================================================================
                        //    //object obj = DatabaseHelper.insertAttachment(RequestId, Pkey, idupload.FileName, "ProjectNote", Session["Admin"].ToString());
                        //    DatabaseHelper.insertNoteAttachment(Server.UrlDecode(Request.QueryString["projectname"]), Convert.ToInt32(Request.QueryString["folderid"]), Convert.ToInt32(Request.QueryString["solutionid"]), Pkey, idupload.FileName.ToString(), "ProjectNote".ToString(), Session["Admin"].ToString());
                        //    //======================================================================================================================
                        //    fileupload.SaveAs(uploadPath1 + "\\" + Pkey + "_" + idupload.FileName);

                        //}
                        //======================================================

                        if (ds.Tables[0].Rows[0]["EmpImgURL"].ToString().Trim() == "")
                        {
                            EmpImage.Src = "EmpImages/NoImage.png";
                            sstrEmpImgURL = "EmpImages/NoImage.png";
                        }
                        else
                        {
                            EmpImage.Src = ds.Tables[0].Rows[0]["EmpImgURL"].ToString().Trim();
                            sstrEmpImgURL = ds.Tables[0].Rows[0]["EmpImgURL"].ToString().Trim();
                        }
                        chkIsCW.Checked = ds.Tables[0].Rows[0]["IsCW"].ToString() == "Y" ? true : false;
                        hdnReqPR.Value = ds.Tables[0].Rows[0]["PR_EmailNotification"].ToString();
                        btnCreate.Text = "Update";
                        DivEntry.Visible = true;
                        divMessage.InnerHtml = "";

                        txtUserName.Enabled = false;
                        //====================== Section project id and Designation ==========================//
                        fnSetDesgddl(dsDesg);

                        ddlDesg.SelectedIndex = index;

                        fnSetCompddl(dsComp);

                        ddlCompany.SelectedIndex = index;

  
                        //==================================================================================//
                    }
                    else
                    {
                        divMessage.InnerHtml = "User was not found.";
                        DivEntry.Visible = false;
                    }
                }
                else
                {
                    divMessage.InnerHtml = "User was not found.";
                    DivEntry.Visible = false;
                }

                //=====================Project Details Region============================================//
                if (dsProjDetails != null)
                {
                    Button btnProjD=null;

                    if (dsProjDetails.Tables.Count > 0)
                    {
                       // AjaxControlToolkit.TabContainer container = (AjaxControlToolkit.TabContainer)TabContainer1;
                       // btnProjD = (Button)container.FindControl("btnProjectDetails");
                        
                       // btnProjD.Text = "Update";
                        if (dsProjDetails.Tables[0].Rows.Count > 0)
                        {
                            btnProjectDetails.Text = "Update";
                            fnSetCompanyNameddl(dsComp);
                            ddlCompName.SelectedIndex = index;
                            fnSetProjddl(dsProj);
                            ddlProject.SelectedIndex = index;
                            fnSetDOA();
                            txtDOA.Text = strDtDOA;
                        }
                        else
                            btnProjectDetails.Text = "Create";   
                    }
                    else
                    {
                        btnProjectDetails.Text = "Create";
                        //btnProjD.Text = "Create";
                    }
                }

                //===========================================================================================//
            }
            load_Priority();

        }

        if (Session["message"] != null)
        {
            divMessage.InnerHtml = Session["message"].ToString();
            Session.Remove("message");
        }
    }
    protected void btnCreate_Click(object sender, EventArgs e)
    {


        bool flag = false;
        string strMsg = "";
        if (txtUserName.Text.Trim() == "")
        {
            strMsg += "- Username is required.<br>";
            flag = true;
        }
        if (txtPassword.Text.Trim() == "")
        {
            strMsg += "- Password is required.<br>";
            flag = true;
        }
        if (txtConfirmPassword.Text.Trim() == "")
        {
            strMsg += "- Confirm password is required.<br>";
            flag = true;
        }
        else
        {
            if (txtPassword.Text.Trim() != txtConfirmPassword.Text.Trim())
            {
                strMsg += "- Confirm password must match password.<br>";
                flag = true;
            }
        }
        if (txtFirstName.Text.Trim() == "")
        {
            strMsg += "- First name is required.<br>";
            flag = true;
        }
        if (txtLastName.Text.Trim() == "")
        {
            strMsg += "- Last name is required.<br>";
            flag = true;
        }
        if (txtEmail.Text.Trim() == "")
        {
            strMsg += "- Email is required.<br>";
            flag = true;
        }
        else
        {
            if (!DatabaseHelper.validate_email(txtEmail.Text.Trim()))
            {
                strMsg += "- Format of email address is invalid.<br>";
                flag = true;
            }
        }
        if (txtDOJ.Text.Trim() == "")
        {
            strMsg += "- Date of joining is required.<br>";
            flag = true;
        }
        if (txtdateofBirth.Text.Trim() == "")
        {
            strMsg += "- Date of birth is required.<br>";
            flag = true;
        }
        if (flag)
        {
            divMessage.InnerHtml = strMsg;
            return;
        }

        string strReqPR = hdnReqPR.Value.Trim();
        strReqPR = strReqPR.ToLower() == "" || strReqPR.ToLower() == "select" ? "All" : strReqPR;


        if (btnCreate.Text == "Create")
        {
            try
            {

                if (fileImgUpload.HasFile)
                {
                    string filename = Path.GetFileName(fileImgUpload.PostedFile.FileName);
                    //Save images into Images folder
                    fileImgUpload.SaveAs(Server.MapPath("EmpImages/" + id.ToString() + filename));
                    sstrEmpImgURL = "EmpImages/" + id.ToString() + filename;
                }
                else
                {
                    sstrEmpImgURL = "EmpImages/NoImages.png";
                }

                dt = Convert.ToDateTime(txtDOJ.Text);
                strDOJ = dt.ToString("yyyy/MM/dd");

                //strDOJ = DateTime.ParseExact(txtDOJ.Text.ToString(), "yyyy/MM/dd", System.Globalization.CultureInfo.InvariantCulture).ToString();
                //==================================
                dtDOB = Convert.ToDateTime(txtdateofBirth.Text);
                strDOB = dtDOB.ToString("yyyy/MM/dd");
                //==================================

                //==================================

                //DesgId = ddlDesg.SelectedValue.ToString(); // As per task 13853,by SP 17/11/2014 if selected value of ddlDesg is --Select--, need to insert 0, else it will give error
                if (ddlDesg.SelectedIndex == 0)
                {
                    DesgId = "0";
                }
                else
                {
                    DesgId = ddlDesg.SelectedValue.ToString();
                }
                CompanyName = ddlCompany.SelectedValue.ToString();

                //===================================




                object objResult = DatabaseHelper.executeScalar("select count(*) from users where [username]='" + txtUserName.Text.Trim() + "'");

                if (objResult.ToString() != "0")
                {
                    divMessage.InnerHtml = "Username already exists.   Choose another username.";
                    return;
                }

                string strSql = "DECLARE @user_id INT; ";
                strSql += " insert into users ([username],[password],[CanApprove],[See_all_private_tasks],[firstname],[lastname],[email],[admin],[subscribe_to_all_tasks],[can_delete_tasks],[Can_edit_delete_comments_attachments],[created_user],[most_recent_login_datetime],[active],[show_closed_tasks],[show_client_requests],[show_deleted_tasks],[EmailNotification],[default_selected_item],[PR_EmailNotification],[DOJ],[EmpImgURL],[IsCW],[Title],[CompanyName],[DesgId],[Skills],[DOB],[Pkey],[CV_name]) ";
                // strSql += " insert into users ([username],[password],[firstname],[lastname],[email],[admin],[subscribe_to_all_tasks],[can_delete_tasks],[Can_edit_delete_comments_attachments],[created_user],[most_recent_login_datetime],[active],[show_closed_tasks],[show_client_requests],[show_deleted_tasks],[EmailNotification],[default_selected_item],[PR_EmailNotification],[DOJ],[EmpImgURL]) ";
                strSql += " values('" + txtUserName.Text.Trim().Replace("'", "''") + "',";
                strSql += "'" + txtPassword.Text.Trim().Replace("'", "''") + "',";
                //***************************************************************
                strSql += "'" + (chkCanApprove.Checked ? "Y" : "N") + "',";
                strSql += "'" + (ChkShowPrivate.Checked ? "Y" : "N") + "',";
                //***************************************************************
                strSql += "'" + txtFirstName.Text.Trim().Replace("'", "''") + "',";
                strSql += "'" + txtLastName.Text.Trim().Replace("'", "''") + "',";
                strSql += "'" + txtEmail.Text.Trim().Replace("'", "''") + "',";
                strSql += "'" + (chkSubscription.Checked ? "Y" : "N") + "',";
                strSql += "'" + (chkDeleteTasks.Checked ? "Y" : "N") + "',";
                strSql += "'" + (chkDeleteTasks.Checked ? "Y" : "N") + "',";
                strSql += "'" + (chkEditDelete.Checked ? "Y" : "N") + "',";
                strSql += "'" + Session["admin"].ToString() + "',";
                strSql += "'',";
                strSql += "'" + (chkActive.Checked ? "Y" : "N") + "',";
                strSql += "'" + (chkShowClosedTasks.Checked ? "Y" : "N") + "',";
                strSql += "'" + (chkShowClientRequests.Checked ? "Y" : "N") + "',";
                strSql += "'" + (chkShowDeletedTasks.Checked ? "Y" : "N") + "',";
                strSql += "'" + (rbEmailNotification.SelectedValue == "1" ? "Y" : "N") + "',";
                strSql += "'" + drpFilter.SelectedValue + "','" + strReqPR.ToString().Replace("'", "''") + "',";
                strSql += "'" + strDOJ + "',";
                strSql += "'" + sstrEmpImgURL + "',";
                strSql += "'" + (chkIsCW.Checked ? "Y" : "N") + "',";
                //==============================================
                strSql += "'" + txtTitle.Text.Trim().Replace("'", "''") + "',";
                //============================================================
                strSql += "'" + CompanyName + "',";
                strSql += "'" + DesgId + "',";


                //============================================================
                strSql += "'" + txtskills.Text.Trim().Replace("'", "''") + "',";
                strSql += "'" + strDOB + "',";
                string Pkey = "";
                string uploadPath1 = Server.MapPath(Request.ApplicationPath + "\\Attachment\\backup");
                if (fileupload.HasFile) //Checked for FileUpload has file or not. //SP 18/11/2014 as was getting Access Denied error, eventhough we are not uploading any files on to the server.
                {
                    if (System.IO.Directory.Exists(uploadPath1))
                    {
                        System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(uploadPath1);
                        Pkey = System.Guid.NewGuid().ToString();
                        fileupload.SaveAs(uploadPath1 + "\\" + Pkey + "_" + fileupload.FileName);
                    }
                }
                strSql += "'" + Pkey + "',";
                strSql += "'" + fileupload.FileName + "')";
                //==============================================
                strSql += " SET @user_id = SCOPE_IDENTITY();";
                strSql += " select @user_id;";


                try
                {
                    objResult = DatabaseHelper.executeScalar(strSql);
               
                    DatabaseHelper.SendEmailList("chaitalip@esolutionsgroup.co.uk", "Insert Query of User : " 
                        + Session["admin"] + "DateTime:-> : " 
                        + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" 
                        + " Message:->" + "No" + "\n" + " Source:->" + "No" + "\n" 
                        + " InnerException:->" + "No" + " Stack Trace:-> " + "No" + "\n" + "Sql Query :->" + strSql);
                }
                catch (Exception ex)
                {
                    DatabaseHelper.SendEmailList("chaitalip@esolutionsgroup.co.uk", "Error at Try Catch " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "getcategory()");

                }
                Session["Registered"] = objResult;

                if (objResult != "0")
                {
                    objLeave.fnInsertLeavesDetail(dt, Session["Registered"].ToString(), true);//for year of joining leaves setting
                    objLeave.fnInsertLeavesDetail(DateTime.Now, Session["Registered"].ToString(), false);// for current year leaves setting
                    Session["message"] = "User was created.";
                    Response.Redirect("edit_user.aspx?id=" + objResult.ToString(),false);
                }
                else
                {
                    divMessage.InnerHtml = "User was not created.";
                }
            }
            catch (Exception ex)
            {
                DatabaseHelper.SendEmailList("chaitalip@esolutionsgroup.co.uk", "Error at All Try Catch " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + " All Method");
            }
        }
        else if (btnCreate.Text == "Update")
        {
            dt = Convert.ToDateTime(txtDOJ.Text);
            strDOJ = dt.ToString("yyyy/MM/dd");
         
            //strDOJ = DateTime.ParseExact(txtDOJ.Text.ToString(), "yyyy/MM/dd", System.Globalization.CultureInfo.InvariantCulture).ToString();

            //==================================
            dtDOB = Convert.ToDateTime(txtdateofBirth.Text);
            strDOB = dtDOB.ToString("yyyy/MM/dd");
            //strDOJ = DateTime.ParseExact(txtdateofBirth.Text.Trim(), "yyyy/MM/dd", CultureInfo.InvariantCulture).ToString();
            //==================================

            // As per task 13853,by SP 17/11/2014 if selected value of ddlDesg is --Select--, need to insert 0, else it will give error
            if (ddlDesg.SelectedIndex == 0)
            {
                DesgId = "0";
            }
            else
            {
                DesgId = ddlDesg.SelectedValue.ToString();
            }
            CompanyName = ddlCompany.SelectedValue.ToString();

            object objResult = DatabaseHelper.executeScalar("select count(*) from users where [username]='" + txtUserName.Text.Trim() + "' and [user_id]!=" + lblId.Text);

            if (objResult.ToString() != "0")
            {
                divMessage.InnerHtml = "Username already exists. Choose another username.";
                return;
            }

            if (fileImgUpload.HasFile)
            {
                string filename = Path.GetFileName(fileImgUpload.PostedFile.FileName);
                //Save images into Images folder
                fileImgUpload.SaveAs(Server.MapPath("EmpImages/" + id.ToString() + filename));
                sstrEmpImgURL = "EmpImages/" + id.ToString() + filename;

            }
            hdnReqPR.Value = "";
            //for all priorities
            if (chkReqPR.Checked == true)
            {
                foreach (DataListItem item in Repeater2.Items)
                {
                    CheckBox chk = (CheckBox)item.FindControl("RePRChk");
                    hdnReqPR.Value += chk.Text.ToString() + ";";
                    
                }
            }
            //for specific priorities
            else
            {
                foreach (DataListItem item in Repeater2.Items)
                {
                    CheckBox chk = (CheckBox)item.FindControl("RePRChk");
                    if (chk.Checked == true)
                    {
                        hdnReqPR.Value += chk.Text.ToString() + ";";
                    }
                }
            }
            strReqPR = hdnReqPR.Value.Trim();
           

            string strSql = " update users set";
            strSql += " username = '" + txtUserName.Text.Trim().Replace("'", "''") + "',";
            strSql += " password = '" + txtPassword.Text.Trim().Replace("'", "''") + "',";
            strSql += " firstname = '" + txtFirstName.Text.Trim().Replace("'", "''") + "',";
            strSql += " lastname = '" + txtLastName.Text.Trim().Replace("'", "''") + "',";
            strSql += " email = '" + txtEmail.Text.Trim().Replace("'", "''") + "',";
            //************************************************
            strSql += " CanApprove = '" + (chkCanApprove.Checked ? "Y" : "N") + "',";
            strSql += " See_all_private_tasks = '" + (ChkShowPrivate.Checked ? "Y" : "N") + "',";
            //************************************************
            strSql += " admin = '" + (chkAdmin.Checked ? "Y" : "N") + "',";
            strSql += " subscribe_to_all_tasks = '" + (chkSubscription.Checked ? "Y" : "N") + "',";
            strSql += " can_delete_tasks = '" + (chkDeleteTasks.Checked ? "Y" : "N") + "',";
            strSql += " Can_edit_delete_comments_attachments = '" + (chkEditDelete.Checked ? "Y" : "N") + "',";
            strSql += " active ='" + (chkActive.Checked ? "Y" : "N") + "',";
            strSql += " show_closed_tasks ='" + (chkShowClosedTasks.Checked ? "Y" : "N") + "',";
            strSql += " show_client_requests ='" + (chkShowClientRequests.Checked ? "Y" : "N") + "',";
            strSql += " show_deleted_tasks ='" + (chkShowDeletedTasks.Checked ? "Y" : "N") + "',";
            strSql += " EmailNotification ='" + (rbEmailNotification.SelectedValue == "1" ? "Y" : "N") + "',";
            strSql += " default_selected_item ='" + drpFilter.SelectedValue + "',PR_EmailNotification ='" + strReqPR.ToString().Replace("'", "''") + "',";
            strSql += "DOJ='" + strDOJ + "',";
            if (chkprivatesoution.Checked == true)
            {
                strSql += "  private_solutions =1 ,";
            }
            else
            { strSql += "  private_solutions =0 ,"; }
            strSql += "EmpImgURL='" + sstrEmpImgURL + "', ";
            strSql += "IsCW='" + (chkIsCW.Checked ? "Y" : "N") + "',";
            strSql += "Req_CWO_status='" + (chkReqStatus.Checked ? "Y" : "N") + "',";
            //======================================================
            strSql += "Title='" + txtTitle.Text.Trim().Replace("'", "''") + "',";
            strSql += "Skills='" + txtskills.Text.Trim().Replace("'", "''") + "',";
            strSql += "CompanyName= '" + CompanyName + "',"; //ddlCompany.SelectedItem.Text 
            strSql += "DesgId='" + DesgId + "',"; //ddlDesg.SelectedValue.ToString() //if selected value of ddlDesg is --Select--, need to insert 0, else it will give error

            if (fileupload.HasFile)
            {
                strSql += "DOB='" + strDOB + "',";
                string Pkey = "";
                string uploadPath1 = Server.MapPath(Request.ApplicationPath + "\\Attachment\\backup");
                if (System.IO.Directory.Exists(uploadPath1))
                {
                    System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(uploadPath1);
                    Pkey = System.Guid.NewGuid().ToString();
                    fileupload.SaveAs(uploadPath1 + "\\" + Pkey + "_" + fileupload.FileName);
                }
                strSql += "Pkey='" + Pkey + "',";
                strSql += "CV_name='" + fileupload.FileName + "'";
            }
            else
            {
                strSql += "DOB='" + strDOB + "'";
            }
            //======================================================

            strSql += " where user_id = " + lblId.Text.Trim();

            int intResult = DatabaseHelper.executeNonQuery(strSql);

            if (intResult != 0)
            {
                //=========================================================================================================

                if (!chkIsCW.Checked)
                {
                    string SelectUpdateTask = "Select Distinct a.Task_id as task_id from Tasks a inner join task_comments b on a.task_id=b.task_id where a.assigned_to_user='" + txtUserName.Text.ToString() + "'";
                    DataSet dsGetdata = DatabaseHelper.getDataset(SelectUpdateTask);
                    string Commentsql = "";
                    if (dsGetdata != null && dsGetdata.Tables.Count > 0 && dsGetdata.Tables[0].Rows.Count > 0)
                    {
                        string strSql2 = "Update tasks set assigned_to_user='Rohit' where assigned_to_user='" + txtUserName.Text.ToString() + "'";
                        int intResult2 = DatabaseHelper.executeNonQuery(strSql2);
                        for (int i = 0; i < dsGetdata.Tables[0].Rows.Count; i++)
                        {
                            string strcomment = "changed assigned_to from \"" + txtUserName.Text.ToString() + "\" to \"Rohit\"";
                            Commentsql += "insert into task_updates([task_id],[username],[post_date],[comment])values('" + dsGetdata.Tables[0].Rows[i]["task_id"].ToString() + "','" + Session["admin"].ToString() + "',getdate(),'" + strcomment + "'); ";

                        }
                        int upDateRes = DatabaseHelper.executeNonQuery(Commentsql);

                    }
                }
                //=========================================================================================================
                if (DateTime.Now.Year == dt.Year)
                {
                    objLeave.fnInsertLeavesDetail(dt, Session["Registered"].ToString(), true);
                }
                else
                {
                    objLeave.fnInsertLeavesDetail(dt, Session["Registered"].ToString(), true);
                    objLeave.fnInsertLeavesDetail(DateTime.Now, Session["Registered"].ToString(), false);
                }


                Session["message"] = "User was updated.";

                Response.Redirect("edit_user.aspx?id=" + lblId.Text.Trim());
            }
            else
            {
                divMessage.InnerHtml = "User was not updated.";
            }
        }
    }

    private void clearControls()
    {
        lblId.Text = "New";
        txtUserName.Text = "";
        txtPassword.Text = "";
        txtFirstName.Text = "";
        txtLastName.Text = "";
        txtEmail.Text = "";
        chkAdmin.Checked = false;
        chkDeleteTasks.Checked = false;
        chkEditDelete.Checked = true;
        btnCreate.Text = "Create";
        /// hdnReqPR.Value = "All";
    }

    void load_Priority()
    {
        sql = "select * from priorities order by sort_seq, priority_name;";
        DataSet ds = DatabaseHelper.getDataset(sql);

        if (hdnReqPR.Value != "")
        {
            int i = 0;
            ArrayList strPRname = new ArrayList();
            string[] strPR = hdnReqPR.Value.ToString().Split(new char[] { ';' });
            //for (int j = 0; j < strPR.Length - 1; j++)
            //{
            //    strPRname.Add(strPR[j].ToString());

            //}

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                ///ds.Tables[0].Columns.Add("item");
                //ds.Tables[0].Columns.Add("RePRChk");

                //foreach (DataRow row in ds.Tables[0].Rows)
                //{
                //    if (row[0] != null && row[0].ToString().Trim() != "")
                //    {

                //        if (strPRname.Contains(row[0].ToString().Trim()))
                //        {
                //            row["priority_name"] = row["priority_name"].ToString().Replace("|@", "'");
                //            row["RePRChk"] = "<input type=\"checkbox\" id=\"chkPR" + i + "\" onclick=\"javascript:setReqAreas();\" Checked=\"True\" title=\"" + row["priority_name"].ToString().Replace("|@", "'") + "\"/>";

                //        }
                //        else
                //        {
                //            row["priority_name"] = row["priority_name"].ToString().Replace("|@", "'");
                //            row["RePRChk"] = "<input type=\"checkbox\" id=\"chkPR" + i + "\" onclick=\"javascript:setReqAreas();\"  title=\"" + row["priority_name"].ToString().Replace("|@", "'") + "\"/>";

                //        }

                //    }
                //    i++;

                //}
                Repeater2.DataSource = ds.Tables[0];
                Repeater2.DataBind();




                foreach (DataListItem item in Repeater2.Items)
                {
                    CheckBox chk = (CheckBox)item.FindControl("RePRChk");
                    for (int j = 0; j < strPR.Length; j++)
                    {
                        if (strPR[j].ToString() == "All")
                        {
                            chkReqPR.Checked = true;
                        }
                        else if (chk.Text.ToString() == strPR[j].ToString())
                        {
                            chk.Checked = true;
                        }
                    }
                }
            }
        }
        else
        {
            int i = 0;
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                /////ds.Tables[0].Columns.Add("item");
                //ds.Tables[0].Columns.Add("RePRChk");

                //foreach (DataRow row in ds.Tables[0].Rows)
                //{
                //    row["RePRChk"] = "";
                //    row["priority_name"] = row["priority_name"].ToString().Replace("|@", "'");
                //    row["RePRChk"] = "<input type=\"checkbox\" id=\"chkPR" + i + "\" onclick=\"javascript:setReqAreas();\"  title=\"" + row["priority_name"].ToString().Replace("|@", "'") + "\"/>";

                //    i++;
                //}
                Repeater2.DataSource = ds.Tables[0];
                Repeater2.DataBind();
            }

        }
    }

    void load_filterDropDown()
    {
        drpFilter.Items.Clear();
        if (DatabaseHelper.isAdmin(Session["Admin"].ToString()))
        {
            sql = "select username from users where active='Y' and admin='Y' and username <> '" + Session["Admin"] + "' order by username";

            DataSet dsAdmin = DatabaseHelper.getDataset(sql);

            if (dsAdmin != null)
            {
                if (dsAdmin.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsAdmin.Tables[0].Rows.Count; i++)
                    {
                        drpFilter.Items.Add("Assigned by " + dsAdmin.Tables[0].Rows[i]["username"]);
                    }
                }
            }

            drpFilter.Items.Add("Assigned by " + Session["Admin"]);
            drpFilter.Items.Add("Assigned by me");
            drpFilter.Items.Add("All tasks by last updated");
            drpFilter.Items.Add("All emps CWO");
            drpFilter.Items.Add("Show my open tasks");
        }
        else
        {
            drpFilter.Items.Add("Show my open tasks");
        }
    }

    protected void btnscond_Click(object sender, EventArgs e)
    {
        bool flag = false;
        string strMsg = "";
        if (lblId.Text.Trim() == "")
        {
            strMsg += "- Please enter first employee infrmation.<br>";
            flag = true;
        }
        if (flag)
        {
            AddressMsg.InnerHtml = strMsg;
            return;
        }
      
        if (btnscond.Text == "Update")
        {
            if (lblId.Text != "")
            {
                string strSql = " update users set";
                strSql += " phone1 = '" + txtphon1.Text.Trim().Replace("'", "''") + "',";
                strSql += " phone2 = '" + txtphon2.Text.Trim().Replace("'", "''") + "',";
                strSql += " phone3 = '" + txtphone3.Text.Trim().Replace("'", "''") + "',";
                strSql += " Address1 = '" + txtaddress1.Text.Trim().Replace("'", "''") + "',";
                strSql += " Address2 = '" + txtaddress2.Text.Trim().Replace("'", "''") + "',";
                strSql += " Landmark = '" + txtlandmark.Text.Trim().Replace("'", "''") + "',";
                strSql += " City = '" + txtcity.Text.Trim().Replace("'", "''") + "',";
                strSql += " Postcode = '" + txtpostcode.Text.Trim().Replace("'", "''") + "',";
                strSql += " State = '" + txtstate.Text.Trim().Replace("'", "''") + "',";
                strSql += " Country = '" + txtCountry.Text.Trim().Replace("'", "''") + "',";
                strSql += " Personal_email_id = '" + txtemailId.Text.Trim().Replace("'", "''") + "',";
                strSql += " Fathers_name = '" + txtfathername.Text.Trim().Replace("'", "''") + "',";
                strSql += " Mothers_name = '" + txtmothername.Text.Trim().Replace("'", "''") + "'";
                strSql += " Married_status = '" + txtmarriedorSingle.Text.Trim().Replace("'", "''") + "'";
                strSql += " where user_id = " + lblId.Text.Trim();
                int intResult = DatabaseHelper.executeNonQuery(strSql);
                if (intResult != 0)
                {
                    AddressMsg.InnerHtml = "Successfully updated records";
                }

            }

        }

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        string strMsg = "";
        bool flag = false;
        if (lblId.Text.Trim() == "")
        {
            strMsg += "- Please enter first employee infrmation.<br>";
            flag = true;
        }
        if (flag)
        {
            DivEducation.InnerHtml = strMsg;
            return;
        }
        if (btnscond.Text == "Update")
        {
            string strSql = " update users set";
            strSql += " Course_HS= '" + txt10.Text.Trim().Replace("'", "''") + "',";
            strSql += " Specialization_HS=  '" + txtspecialization.Text.Trim().Replace("'", "''") + "',";

            strSql += " Intitute_HS= '" + txtinstitute.Text.Trim().Replace("'", "''") + "',";
            //strSql += " Address_HS= '" + txtaddress.Text.Trim().Replace("'", "''") + "',";

            strSql += " Year_HS= '" + txtyear.Text.Trim().Replace("'", "''") + "',";

            strSql += " Aggregate_Mark_HS= '" + txtmarks.Text.Trim().Replace("'", "''") + "',";

            strSql += " Course_SS= '" + lbl12.Text.Trim().Replace("'", "''") + "',";
            strSql += " Specialization_SS= '" + txtspecializationHSC.Text.Trim().Replace("'", "''") + "',";

            strSql += " Intitute_SS= '" + txtinstituteHSC.Text.Trim().Replace("'", "''") + "',";

            strSql += " Address_SS= '" + txtadddres12.Text.Trim().Replace("'", "''") + "',";

            strSql += " Year_SS= '" + txtmonthHSC.Text.Trim().Replace("'", "''") + "',";

            strSql += " Aggregate_Mark_SS= '" + txtmarks12.Text.Trim().Replace("'", "''") + "',";

            strSql += " Course_G= '" + txtcourseBsc.Text.Trim().Replace("'", "''") + "',";

            strSql += " Specialization_G= '" + txtspecDEG.Text.Trim().Replace("'", "''") + "',";

            strSql += " Intitute_G= '" + txtinsDEG.Text.Trim().Replace("'", "''") + "',";

            strSql += " Address_G= '" + txtAddBsc.Text.Trim().Replace("'", "''") + "',";

            strSql += " Year_G= '" + txtmonthDEG.Text.Trim().Replace("'", "''") + "',";

            strSql += " Aggregate_Mark_G= '" + TxtAggreBsc.Text.Trim().Replace("'", "''") + "',";

            strSql += " Course_PG= '" + txtcoursePG.Text.Trim().Replace("'", "''") + "',";

            strSql += " Specialization_PG= '" + txtspedPG.Text.Trim().Replace("'", "''") + "',";

            strSql += " Intitute_PG= '" + txtinstitutePG.Text.Trim().Replace("'", "''") + "',";

            strSql += " Address_PG= '" + txtaddressPG.Text.Trim().Replace("'", "''") + "',";

            strSql += " Year_PG= '" + txtyearPG.Text.Trim().Replace("'", "''") + "',";

            strSql += " Aggregate_Mark_PG= '" + txtAggMarkePG.Text.Trim().Replace("'", "''") + "',";

            strSql += " Training= '" + txttraining.Text.Trim().Replace("'", "''") + "',";

            strSql += " Institute= '" + txtinstituteTraining.Text.Trim().Replace("'", "''") + "',";

            strSql += " Location= '" + txtlocation.Text.Trim().Replace("'", "''") + "',";

            strSql += " Date_Attended_From= '" + txtattendedfrom.Text.Trim().Replace("'", "''") + "',";
            strSql += " Date_Attended_To= '" + txtdateattended.Text.Trim().Replace("'", "''") + "'";
            strSql += " where user_id = " + lblId.Text.Trim();
            int intResult = DatabaseHelper.executeNonQuery(strSql);
            if (intResult != 0)
            {
                DivEducation.InnerHtml = "Successfully updated records";
            }
        }
    }

    protected void OtherEmployer_Click(object sender, EventArgs e)
    {
        string strMsg = "";
        bool flag = false;
        if (lblId.Text.Trim() == "")
        {
            strMsg += "- Please enter first employee infrmation.<br>";
            flag = true;
        }
        if (flag)
        {
            DivOtherEmployer.InnerHtml = strMsg;
            return;
        }
        if (btnscond.Text == "Update")
        {
            string strSql = " update users set ";

            strSql += " Name_of_Employer= '" + txtemployer.Text.Trim().Replace("'", "''") + "',";
            strSql += " Date_Employed_From= '" + txtdateemployed.Text.Trim().Replace("'", "''") + "',";
            strSql += " Date_Employed_To= '" + txtdateemployedTo.Text.Trim().Replace("'", "''") + "',";
            strSql += " Employer_Address= '" + txtaddresscompany.Text.Trim().Replace("'", "''") + "',";
            strSql += " Employer_Phone= '" + txtphoneaddress.Text.Trim().Replace("'", "''") + "',";
            strSql += " Designation= '" + txtdesignation.Text.Trim().Replace("'", "''") + "',";
            strSql += " Department= '" + txtdepartment.Text.Trim().Replace("'", "''") + "',";
            strSql += " Employee_Code=  '" + txtemployeecode.Text.Trim().Replace("'", "''") + "',";
            strSql += " Responseblities= '" + txtboxres.Text.Trim().Replace("'", "''") + "',";
            strSql += " Reason_for_Leaving= '" + txtResleaving.Text.Trim().Replace("'", "''") + "',";
            strSql += " Monthly_Gross= '" + txtmonthlygross.Text.Trim().Replace("'", "''") + "'";
            strSql += " where user_id = " + lblId.Text.Trim();
            int intResult = DatabaseHelper.executeNonQuery(strSql);
            if (intResult != 0)
            {
                DivOtherEmployer.InnerHtml = "Successfully updated records";
            }
        }
    }

    protected void OtherPreviousEmployer_Click(object sender, EventArgs e)
    {
        //Employee_Previous_Company
    }

    protected void btnyourself_Click(object sender, EventArgs e)
    {
        string strMsg = "";
        bool flag = false;
        if (lblId.Text.Trim() == "")
        {
            strMsg += "- Please enter first employee infrmation.<br>";
            flag = true;
        }
        if (flag)
        {
            divyourself.InnerHtml = strMsg;
            return;
        }
        if (btnscond.Text == "Update")
        {
            string strSql = " update users set ";
            strSql += " Your_Personal_strengts= '" + txtstrengts.Text.Trim().Replace("'", "''") + "',";
            strSql += "Area_of_improvements= '" + txtimprovement.Text.Trim().Replace("'", "''") + "',";
            strSql += "Your_career_objectives= '" + txtobjective.Text.Trim().Replace("'", "''") + "',";
            strSql += "expectations_from_job= '" + txtexpectations.Text.Trim().Replace("'", "''") + "',";
            strSql += "Significant_achivements= '" + txtachievements.Text.Trim().Replace("'", "''") + "',";
            strSql += "Other_Interests= '" + txtotherinterests.Text.Trim().Replace("'", "''") + "'";
            strSql += " where user_id = " + lblId.Text.Trim();
            int intResult = DatabaseHelper.executeNonQuery(strSql);
            if (intResult != 0)
            {
                divyourself.InnerHtml = "Successfully updated records";
            }

        }
    }

    protected void btnReferences_Click(object sender, EventArgs e)
    {
        string strMsg = "";
        bool flag = false;
        if (lblId.Text.Trim() == "")
        {
            strMsg += "- Please enter first employee infrmation.<br>";
            flag = true;
        }
        if (flag)
        {
            DivReferences.InnerHtml = strMsg;
            return;
        }
        if (btnscond.Text == "Update")
        {
            string strSql = " update users set ";
            strSql += " Name_1= '" + txtname.Text.Trim().Replace("'", "''") + "',";
            strSql += " Nature_of_Association_1= '" + txtorga.Text.Trim().Replace("'", "''") + "',";
            strSql += " Organization_designation_1= '" + txtdesignationfriend.Text.Trim().Replace("'", "''") + "',";
            strSql += " email_1= '" + txtemailfriend.Text.Trim().Replace("'", "''") + "',";
            strSql += " Phone_1= '" + txtphone.Text.Trim().Replace("'", "''") + "',";
            strSql += " Mobile_1= '" + txtmobilefriend.Text.Trim().Replace("'", "''") + "',";
            strSql += " Name_2=  '" + txtname2.Text.Trim().Replace("'", "''") + "',";
            strSql += " Nature_of_Association_2= '" + txtstrengts.Text.Trim().Replace("'", "''") + "',";
            strSql += " Organization_designation_2= '" + txtorga2.Text.Trim().Replace("'", "''") + "',";
            strSql += " email_2= '" + txtemailfriend2.Text.Trim().Replace("'", "''") + "',";
            strSql += " Phone_2= '" + txtphone2.Text.Trim().Replace("'", "''") + "',";
            strSql += " Mobile_2= '" + txtmobilefriend2.Text.Trim().Replace("'", "''") + "'";
            strSql += " where user_id = " + lblId.Text.Trim();
            int intResult = DatabaseHelper.executeNonQuery(strSql);
            if (intResult != 0)
            {
                DivReferences.InnerHtml = "Successfully updated records";
            }

        }
    }

    protected void btnAdddetails_Click(object sender, EventArgs e)
    {
        string strMsg = "";
        bool flag = false;
        if (lblId.Text.Trim() == "")
        {
            strMsg += "- Please enter first employee infrmation.<br>";
            flag = true;
        }
        if (flag)
        {
            DivaddDetails.InnerHtml = strMsg;
            return;
        }
        if (btnscond.Text == "Update")
        {
            string strSql = " update users set ";
            strSql += " Nationality= '" + txtnationality.Text.Trim().Replace("'", "''") + "',";
            strSql += " validPasswort= '" + txtvalidPasswort.Text.Trim().Replace("'", "''") + "',";
            strSql += " deniedVisa= '" + txtdeniedVisa.Text.Trim().Replace("'", "''") + "',";
            strSql += " overseasAssignment= '" + txtoverseasAssignment.Text.Trim().Replace("'", "''") + "',";
            strSql += " esgprocess= '" + txtesgprocess.Text.Trim().Replace("'", "''") + "',";
            strSql += " friendEsg= '" + txtfriendEsg.Text.Trim().Replace("'", "''") + "',";
            strSql += " illness= '" + txtillness.Text.Trim().Replace("'", "''") + "',";
            strSql += " arrest= '" + txtarrest.Text.Trim().Replace("'", "''") + "',";
            strSql += " commitment= '" + txtcommitment.Text.Trim().Replace("'", "''") + "',";
            strSql += " otherinfo= '" + txtotherinfo.Text.Trim().Replace("'", "''") + "'";
            strSql += " where user_id = " + lblId.Text.Trim();
            int intResult = DatabaseHelper.executeNonQuery(strSql);
            if (intResult != 0)
            {
                DivaddDetails.InnerHtml = "Successfully updated records";
            }


        }


    }

    protected void Add(object sender, EventArgs e)
    {
        Control control = null;
        if (gvpreviousemployee.FooterRow != null)
        {
            control = gvpreviousemployee.FooterRow;
        }
        else
        {
            control = gvpreviousemployee.Controls[0].Controls[0];
        }
        string nameofOrganization = (control.FindControl("txtNameofOrganization") as TextBox).Text;
        string LOcation = (control.FindControl("txtLocation") as TextBox).Text;
        string Datefrom = (control.FindControl("txtDurationFrom") as TextBox).Text;
        string DateTo = (control.FindControl("txtDurationTo") as TextBox).Text;
        string ReasonforLeaving = (control.FindControl("txtReasonforLeaving") as TextBox).Text;
        DatabaseHelper.Employee_Previous_Company(Convert.ToInt16(lblId.Text.Trim()), nameofOrganization, LOcation, Datefrom, DateTo, ReasonforLeaving);
        string strSql1 = "select * from Employee_Previous_Company where user_id=" + id;
        DataSet ds1 = DatabaseHelper.getDataset(strSql1);
        gvpreviousemployee.DataSource = ds1;
        gvpreviousemployee.DataBind();
    }

  
    protected void ddlCompName_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCompName.SelectedValue != null)
        {
            fnFillProjectddl();
        }
    }

    #region FIll Project & Designation DDL

    public void fnFillCompddl()
    {
        try
        {
            dsComp = DatabaseHelper.getDataset("Select distinct Company_Name, Company_id from Company order by Company_Name Asc");
            if (dsComp != null)
            {
                if (dsComp.Tables.Count > 0)
                {
                    ddlCompany.DataSource = dsComp.Tables[0];
                    ddlCompany.DataTextField = "Company_Name";
                    ddlCompany.DataValueField = "Company_id";
                    ddlCompany.DataBind(); 
                    ddlCompany.Items.Insert(0, "--Select--");

                    ddlCompName.DataSource = dsComp.Tables[0];
                    ddlCompName.DataTextField = "Company_Name";
                    ddlCompName.DataValueField = "Company_id";
                    ddlCompName.DataBind();
                    ddlCompName.Items.Insert(0, "--Select--");
                }
            }
        }
        catch
        {
        }

    }

    public void fnFillDesgddl()
    {

        try
        {
            dsDesg = DatabaseHelper.getDataset("Select * from tbl_Desg");

            if (dsDesg != null)
            {
                if (dsDesg.Tables.Count > 0)
                {
                    ddlDesg.DataSource = dsDesg;
                    ddlDesg.DataValueField = "DesgId";
                    ddlDesg.DataTextField = "Desg";
                    ddlDesg.DataBind();
                    ddlDesg.Items.Insert(0, "--Select--");
                }
            }
        }
        catch
        {
        }

    }

    public void fnFillProjectddl()
    {

        try
        {
                dsProj = DatabaseHelper.getDataset("Select * from Projects" );//Where companyName='" + ddlCompName.SelectedItem.Text + "'");
              
                if (dsProj != null)
                {
                    if (dsProj.Tables.Count > 0)
                    {
                   DataView dv = new DataView(dsProj.Tables[0]);

                if (ddlCompName.SelectedItem.Text != "--Select--")
                {
                    dv.RowFilter ="[companyname]='"+ ddlCompName.SelectedItem.Text + "'";
                }

                        
                        ddlProject.DataSource = dv;
                        ddlProject.DataValueField = "project_id";
                        ddlProject.DataTextField = "project_name";
                        ddlProject.DataBind();
                        ddlProject.Items.Insert(0, "--Select--");
                    }
                }
        }
        catch
        {
        }

    }

    public int fnSetCompddl(DataSet dsComp)
    {
        index = 0;
        try
        {

            DataRow[] foundRows;
            if (ds != null)
            {
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows[0]["CompanyName"].ToString().Trim() != "")
                    {
                        string filter = "[Company_Name] ='" + ds.Tables[0].Rows[0]["CompanyName"] + "'";
                        //DataView dv = new DataView(dsComp.Tables[0]);
                        //dv.RowFilter = filter;
                       foundRows = dsComp.Tables[0].Select(filter);

                        foreach (DataRow dr in foundRows)
                        {
                            index = Convert.ToInt32(dr.Table.Rows.IndexOf(dr));
                        }
                       // index = Convert.ToInt32(dv.ToTable().Rows[0]["Company_id"]);
                        index += 1;
                    }
                    else
                    {
                        index = 0;
                    }
                }
            }
        }
        catch
        {
        }
        return index;
    }

    public int fnSetDesgddl(DataSet dsDesg)
    {
        index = 0;
        try
        {
            DataRow[] foundRows;
            if (dsDesg.Tables[0].Rows[0]["DesgId"].ToString().Trim() != "")
            {
                string filter = "[DesgId] =" + ds.Tables[0].Rows[0]["DesgId"];
                foundRows = dsDesg.Tables[0].Select(filter);
                //int index = Convert.ToInt32(ddlCompany.Items.IndexOf(ddlCompany.Items.FindByValue(ds.Tables[0].Rows[0]["CompanyName"].ToString())));//ds.Tables[0].Rows[0]["CompanyName"].ToString());
                //int i = Convert.ToInt32(ddlCompany.Items.FindByValue("Value"));
                //ddlCompany.SelectedIndex = i;
                foreach (DataRow dr in foundRows)
                {
                    index = Convert.ToInt32(dr.Table.Rows.IndexOf(dr));
                }
                index += 1;
            }
            else
            {
                index = 0;
            }
        }
        catch
        {
        }
        return index;
    }

    public int fnSetCompanyNameddl(DataSet dsProj)
    {
        index = 0;
        try
        {

            DataRow[] foundRows;
            if (dsProjDetails != null)
            {
                if (dsProjDetails.Tables.Count > 0)
                {
                    if (dsProjDetails.Tables[0].Rows[0]["CompanyName"].ToString().Trim() != "")
                    {
                        string filter = "[Company_Name] ='" + dsProjDetails.Tables[0].Rows[0]["CompanyName"] + "'";
                        //DataView dv = new DataView(dsComp.Tables[0]);
                        //dv.RowFilter = filter;
                        foundRows = dsProj.Tables[0].Select(filter);

                        foreach (DataRow dr in foundRows)
                        {
                            index = Convert.ToInt32(dr.Table.Rows.IndexOf(dr));
                        }
                        // index = Convert.ToInt32(dv.ToTable().Rows[0]["Company_id"]);
                        index += 1;
                    }
                    else
                    {
                        index = 0;
                    }
                }
            }
        }
        catch
        {
        }
        return index;
    }

    public int fnSetProjddl(DataSet dsProj)
    {
        index = 0;
        try
        {
            DataRow[] foundRows;
            if (dsProj.Tables[0].Rows[0]["Project_id"].ToString().Trim() != "")
            {
                string filter = "[Project_id] =" + dsProjDetails.Tables[0].Rows[0]["Project_id"];
                foundRows = dsProj.Tables[0].Select(filter);
                //int index = Convert.ToInt32(ddlCompany.Items.IndexOf(ddlCompany.Items.FindByValue(ds.Tables[0].Rows[0]["CompanyName"].ToString())));//ds.Tables[0].Rows[0]["CompanyName"].ToString());
                //int i = Convert.ToInt32(ddlCompany.Items.FindByValue("Value"));
                //ddlCompany.SelectedIndex = i;
                foreach (DataRow dr in foundRows)
                {
                    index = Convert.ToInt32(dr.Table.Rows.IndexOf(dr));
                }
                index += 1;
            }
            else
            {
                index = 0;
            }
        }
        catch
        {
        }
        return index;
    }

    public void fnSetDOA()
    {
        try
        {
            if (dsProjDetails != null)
            {
                if (dsProjDetails.Tables.Count > 0)
                {
                    if (dsProjDetails.Tables[0].Rows[0]["DOA"].ToString() != "")
                    {
                        strDtDOA = dsProjDetails.Tables[0].Rows[0]["DOA"].ToString();
                        DateTime dtA = Convert.ToDateTime(strDtDOA);
                        strDtDOA = dtA.ToString("dd/MM/yyyy");
                    }
                }
            }
        }
        catch
        {
        }
    }


    #endregion

    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        int tabId = TabContainer1.TabIndex;
        Session["TabIndex"] = tabId;
    }

    protected void btnProjectDetails_Click(object sender, EventArgs e)
    {
        
        string strMsg = "";
        bool flag = false;
        if (lblId.Text.Trim() == "")
        {
            strMsg += "- Please enter first employee infrmation.<br>";
            flag = true;
        }
        if (flag)
        {
            divyourself.InnerHtml = strMsg;
            return;
        }

        DateTime tempDt = Convert.ToDateTime(txtDOA.Text);
        string date = tempDt.ToString("yyyy/MM/dd");

        if (btnProjectDetails.Text == "Update")
        {

            string strSql = "update ProjectDetails set ";
            strSql += " Project_id= " + ddlProject.SelectedValue + ",";
            strSql += "companyName= '" + ddlCompName.SelectedItem.Text.Trim().Replace("'", "''") + "',";
            //  strSql += "user_id= " + lblId.Text.Trim().Replace("'", "''") + ",'";
            strSql += "DOA= '" + date + "'";// txtDOA.Text.Trim().Replace("'", "''") + "' ";
            strSql += " where user_id = " + lblId.Text.Trim();
            int intResult = DatabaseHelper.executeNonQuery(strSql);
            if (intResult != 0)
            {
                divyourself.InnerHtml = "Successfully updated records";
            }

        }
        else
        {
            string strSql = "insert into ProjectDetails values( ";
            strSql += ddlProject.SelectedValue + " , '";
            strSql += ddlCompName.SelectedItem.Text.Trim().Replace("'", "''") + "',";
            strSql += lblId.Text.Trim().Replace("'", "''") + ",'";
          
            strSql += date+"')"; //txtDOA.Text.Trim().Replace("'", "''") + "' )";

            int intResult = DatabaseHelper.executeNonQuery(strSql);
            if (intResult != 0)
            {
                divyourself.InnerHtml = "Successfully updated records";
                btnProjectDetails.Text = "Update";
            }

        }
    }
    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void chkReqPR_CheckedChanged(object sender, EventArgs e)
    {
        if (chkReqPR.Checked == true)
        {
            foreach (DataListItem item in Repeater2.Items)
            {
                CheckBox chk = (CheckBox)item.FindControl("RePRChk");
                chk.Checked = true;
                chk.Enabled = false;
            }
        }
        else if (chkReqPR.Checked == false)
        {
            foreach (DataListItem item in Repeater2.Items)
            {
                CheckBox chk = (CheckBox)item.FindControl("RePRChk");
                chk.Checked = false;
                chk.Enabled = true;
            }
        }
        TabContainer1.ActiveTabIndex = 0;
    }
}
