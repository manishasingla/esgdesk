﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="esActivityLog.aspx.cs" Inherits="esActivityLog" %>

<%--<%@ Register Assembly="AjaxControls" Namespace="AjaxControls" TagPrefix="asp" %>--%>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
        .g
        {
            text-align:left;
            color: White;
            border: none;
            background: none;
        }
    </style>
    <title>EstateCRM</title>
    <style type="text/css">
        .modalPopup
        {
            text-align: center;
            padding-top: 30px;
            border-style: solid;
            border-width: 1px;
            background-color: White;
            width: 300px;
            height: 50px;
            font-size: 16px;
            color: #000000;
            font-weight: bold;
            vertical-align: middle;
        }
    </style>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        function gotosearch() {
            if (document.getElementById("chkSearch").checked) {
                var gotosearch = document.getElementById("btnsearchbyDate");

                gotosearch.click();

            }

        }

        function gototypesearch() {
            if (document.getElementById("chkSearch").checked) {
                var gotoTypesearch = document.getElementById("btnsearchbyType");

                gotoTypesearch.click();

            }

        }

        function gotoNumbersearch() {

            if (document.getElementById("chkSearch").checked) {
                var gotoTypesearch = document.getElementById("btmSerachbyNumber");

                gotoTypesearch.click();

            }
        }


        function gotoCompanysearch() {

            if (document.getElementById("chkSearch").checked) {
                var gotoCompsearch = document.getElementById("btnSearchbyComp");

                gotoCompsearch.click();

            }


        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <uc2:Admin_Header ID="Admin_Header1" runat="server" />
        <%-- <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>--%>
        <div style="margin: 10px 0 0 0;">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="right" valign="middle">
                        <img height="59" src="../images/filter_corner_l.png" width="11" />
                    </td>
                    <td align="left" valign="middle" bgcolor="#282c5f">
                        <table>
                            <tr style="border: solid 1px black">
                                <td style="color:White;font-size:14px" font-weight="bold">
                                    Filter:
                                </td>
                                <td style="color:White;font-size:14px">
                                    <asp:Label ID="Label1" runat="server" Text="Company name" Width="100px"></asp:Label>
                                </td>
                                <td style="color:White;font-size:14px" width="253px">
                                    <asp:DropDownList ID="ddlCompanyname" runat="server" border="1" DataTextField="companyname"
                                        DataValueField="companyname" Width="250px" AppendDataBoundItems="true">
                                        <asp:ListItem Value="All" Text="All"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td style="color:White;font-size:14px">
                                    &nbsp;
                                </td>
                                <td style="color:White;font-size:14px">
                                    <asp:Label ID="Label9" runat="server" Text="Event type" Width="65px"></asp:Label>
                                </td>
                                <td style="color:White;font-size:14px">
                                    <asp:DropDownList ID="ddlType" runat="server" Width="100px">
                                        <asp:ListItem>All</asp:ListItem>
                                        <asp:ListItem>New</asp:ListItem>
                                        <asp:ListItem>Updated</asp:ListItem>
                                        <asp:ListItem>Deleted</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td style="color:White;font-size:14px">
                                    &nbsp;
                                </td>
                                <td style="color:White;font-size:14px">
                                    <asp:Label ID="Label10" runat="server" Text="Date"></asp:Label>
                                </td>
                                <td style="color:White;font-size:14px">
                                    <asp:DropDownList ID="ddlDate" runat="server" Width="100px">
                                        <asp:ListItem>All</asp:ListItem>
                                        <asp:ListItem Value="0">Today</asp:ListItem>
                                        <asp:ListItem Value="1">Yesterday</asp:ListItem>
                                        <asp:ListItem Value="7">Last 7 days</asp:ListItem>
                                        <asp:ListItem Value="30">Last 30 days</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td style="color:White;font-size:14px">
                                    &nbsp;
                                </td>
                                <td style="color:White;font-size:14px">
                                    <asp:Label ID="Label11" runat="server" Text="Number" Width="50px"></asp:Label>
                                </td>
                                <td style="color:White;font-size:14px">
                                    <asp:DropDownList ID="ddlNumber" runat="server" Width="150px">
                                        <asp:ListItem>All</asp:ListItem>
                                        <asp:ListItem Value="10">Last 10</asp:ListItem>
                                        <asp:ListItem Value="20">Last 20</asp:ListItem>
                                        <asp:ListItem Value="30">Last 30</asp:ListItem>
                                        <asp:ListItem Value="40">Last 40</asp:ListItem>
                                        <asp:ListItem Value="50">Last 50</asp:ListItem>
                                        <asp:ListItem Value="100">Last 100</asp:ListItem>
                                        <asp:ListItem Value="500">Last 500</asp:ListItem>
                                        <asp:ListItem Value="All in last 7 days">All in last 7 days</asp:ListItem>
                                        <asp:ListItem Value="All in last 30 days">All in last 30 days</asp:ListItem>
                                        <asp:ListItem Value="All in last 60 days">All in last 60 days</asp:ListItem>
                                        <asp:ListItem Value="All in last 360 days">All in last 360 days</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td style="color:White;font-size:14px">
                                    &nbsp;
                                </td>
                                <td style="color:White;font-size:14px">
                                    <asp:CheckBox ID="chkSearch" runat="server" AutoPostBack="True" OnCheckedChanged="chkSearch_CheckedChanged"
                                        Text="Auto results" Width="100px" />
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkSearch" runat="server" OnClick="lnkSearch_Click" CssClass="clearfilter"
                                        Width="80px" Style="text-align: center; line-height: 20px;">Search</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td align="left" valign="middle">
                        <img height="59" src="../images/filter_corner_r.png" width="11" />
                    </td>
                </tr>
            </table>
        </div>
        <div style="height: 10px">
        </div>
        <div class="divBorder"">
            <table width="500" align="center">
                <tr>
                    <td colspan="11">
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
            <table align="center" width="100%">
                <tr>
                    <td colspan="3" style="margin-left: 40px">
                        <asp:GridView ID="dgCompany" runat="server" AutoGenerateColumns="False" BorderColor="black"
                            BorderStyle="solid" BorderWidth="1px" CellPadding="4" GridLines="Both" AllowSorting="True"
                            AllowPaging="true" PageSize="25" OnRowCommand="dgCompany_RowCommand" OnRowDeleting="dgCompany_RowDeleting1"
                            OnRowEditing="dgCompany_RowEditing1" Font-Size="12px" OnPageIndexChanging="dgCompany_PageIndexChanging">
                            <PagerSettings Mode="NextPrevious" PreviousPageText="Previous" NextPageText="Next"
                                Position="Bottom"    />
                            <PagerStyle CssClass="g"/>
                            <FooterStyle BackColor="#282C5F" ForeColor="white" Width="100%" />
                            <HeaderStyle Font-Italic="False" Font-Size="12px" ForeColor="black" Font-Overline="False"
                                Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" BackColor="Gainsboro" />
                            <Columns>
                                <asp:TemplateField HeaderText="Company name">
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="LinkButton0" runat="server" Text="Company name" CommandName="sortcompanyname"
                                            Font-Underline="False" ToolTip="Sort by company name">Company name</asp:LinkButton>
                                    </HeaderTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox0" runat="server" Text='<%# Bind("CompanyName") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label0" runat="server" Text='<%# Bind("CompanyName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="User name">
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="LinkButton1" runat="server" Text="User Name" CommandName="sortusername"
                                            Font-Underline="False">User name</asp:LinkButton>
                                    </HeaderTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("UserName") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("UserName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="LinkButton2" runat="server" Text="Client ID" CommandName="sortclientid"
                                            Font-Underline="False">Client ID</asp:LinkButton>
                                    </HeaderTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("ClientId") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("ClientId") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="LinkButton3" runat="server" Text="Date time" CommandName="sortdatetime"
                                            Font-Underline="False">Date time</asp:LinkButton>
                                    </HeaderTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("DateTime22") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("DateTime22") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="LinkButton4" runat="server" Text="Price" CommandName="sortprice"
                                            Font-Underline="False">Price</asp:LinkButton>
                                    </HeaderTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("PriceP") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        0
                                        <asp:Label ID="Label4" runat="server" Text='<%# Bind("PriceP") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="LinkButton5" runat="server" Text="Type" CommandName="sorttype"
                                            Font-Underline="False">Type</asp:LinkButton>
                                    </HeaderTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("DealType22") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label5" runat="server" Text='<%# Bind("DealType22") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="LinkButton6" runat="server" Text="Event type" CommandName="sorteventtype"
                                            Font-Underline="False">Event type</asp:LinkButton>
                                    </HeaderTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("eventtype") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label6" runat="server" Text='<%# Bind("eventtype") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="LinkButton7" runat="server" Text="Description" CommandName="sortdescription"
                                            Font-Underline="False">Description</asp:LinkButton>
                                    </HeaderTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox7" runat="server" Text='<%# Bind("Descreption") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label7" runat="server" Text='<%# Bind("Descreption") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="LinkButton8" runat="server" Text="Address" CommandName="sortaddress"
                                            Font-Underline="False">Address</asp:LinkButton>
                                    </HeaderTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox8" runat="server" Text='<%# Bind("Address22") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label8" runat="server" Text='<%# Bind("Address22") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerStyle BackColor="transparent" ForeColor="Black" HorizontalAlign="Right" />
                        </asp:GridView>
                        <br />
                        <asp:Label ID="lblDisplay" runat="server" ForeColor="Red" Text="Label" Visible="False"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <%--</ContentTemplate>
        </asp:UpdatePanel>--%>
    </div>
    <asp:Button ID="btnsearchbyType" runat="server" BackColor="Transparent" BorderStyle="None"
        OnClick="btnsearchbyType_Click" />
    <asp:Button ID="btmSerachbyNumber" runat="server" BackColor="Transparent" BorderStyle="None"
        OnClick="btmSerachbyNumber_Click" />
    <asp:Button ID="btnsearchbyDate" runat="server" BackColor="Transparent" BorderStyle="None"
        OnClick="btnsearchbyDate_Click" />
    <asp:Button ID="btnSearchbyComp" runat="server" BackColor="Transparent" BorderStyle="None"
        OnClick="btnSearchbyComp_Click" />
    </form>
</body>
</html>
