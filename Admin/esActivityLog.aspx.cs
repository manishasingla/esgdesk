﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Drawing;

public partial class esActivityLog : System.Web.UI.Page
{
    string dealPrice;
    string dealType;
    DataSet cds = new DataSet();
    DataSet cdsRent = new DataSet();
    string strPricePer;
    DataSet dsDeleted = new DataSet();
    string strEventType;
    public static DataSet dsMain = new DataSet();
    string strPostCode;
    bool flagClient = false;
    string strClient;
    string sort = string.Empty;
    string ddlsearch;
    public static DataView dv;
    protected void Page_Load(object sender, EventArgs e)
    {
        ///ddlDate.Attributes.Add("onchange", "gotosearch();");

        ddlDate.Attributes.Add("onchange", "return gotosearch();");
        ddlType.Attributes.Add("onchange", "return gototypesearch();");
        ddlNumber.Attributes.Add("onchange", "return gotoNumbersearch();");
        ddlCompanyname.Attributes.Add("onchange", "return gotoCompanysearch();");

        if (!(IsPostBack))
        {
            if (Session["company"] != null && Session["company"] != "")
            {
                bindCompany();
                if (lblDisplay.Text != "Record for " + ddlsearch + " company was not found")
                {
                    bindData();
                }
                else
                {
                    lblDisplay.Visible = true;
                }
            }
            else
            {
                bindCompany();
            }
        }


    }


    public void bindCompany()
    {
        DataSet ds = new DataSet();
        ds = DatabaseHelper.getCompany();
        try
        {
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlCompanyname.DataSource = ds;
                    ddlCompanyname.DataTextField = "companyname";
                    ddlCompanyname.DataValueField = "companyname";
                    ddlCompanyname.DataBind();
                    //ddlCompanyname.Items.Insert(0, new ListItem("All", "All"));


                    if (Session["company"] != null && Session["company"] != "")
                    {
                        ddlsearch = Session["company"].ToString();

                        ddlCompanyname.SelectedValue = ddlsearch;
                        //ddlCompanyname.SelectedItem.Text = Session["company"].ToString();
                        if (ddlCompanyname.SelectedValue != ddlsearch)
                        {
                            lblDisplay.Text = "Record for " + ddlsearch + " company was not found";
                            ddlCompanyname.SelectedValue = "All";
                        }

                    }
                    //int rcompany = ds.Tables[0].Rows.Count + 1;
                    //ddlCompanyname.Items.Insert(rcompany, new ListItem("All", "All"));
                }
            }
        }
        catch
        {
            lblDisplay.Text = "Record was not found";
        }
    }
    public void bindCompanyAll()
    {
        DataSet ds = new DataSet();
        ds = DatabaseHelper.getCompanyAll();
        try
        {
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {


                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        string[] Split;
                        string tick = dr["clientid"].ToString();
                        Split = tick.Split(',');
                        for (int j = 0; j < Split.Length; j++)
                        {

                            if (flagClient == false)
                            {
                                strClient = "'" + dr["clientid"].ToString() + "',";
                                flagClient = true;
                            }
                            else
                            {
                                strClient = strClient + "'" + dr["clientid"].ToString() + "',";
                            }

                        }
                    }
                    if (strClient == "")
                    {
                        strClient = "No data";
                    }
                    if (strClient.EndsWith(","))
                    {
                        strClient = strClient.TrimEnd(new char[] { ',', ' ' });
                    }


                }
            }
        }
        catch { }
    }

    public void getClientid(string companyname)
    {
        strClient = string.Empty;
        DataSet ds = new DataSet();
        ds = DatabaseHelper.getCompanyClientid(companyname);
        try
        {
            if (ds.Tables[0].Rows.Count > 0)
            {

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    string[] Split;
                    string tick = dr["clientid"].ToString();
                    Split = tick.Split(',');
                    for (int j = 0; j < Split.Length; j++)
                    {

                        if (flagClient == false)
                        {
                            strClient = "'" + dr["clientid"].ToString() + "',";
                            flagClient = true;
                        }
                        else
                        {
                            strClient = strClient + "'" + dr["clientid"].ToString() + "',";
                        }

                    }
                }
                if (strClient == "")
                {
                    strClient = "No data";
                }
                if (strClient.EndsWith(","))
                {
                    strClient = strClient.TrimEnd(new char[] { ',', ' ' });
                }

            }
            else
            {
                strClient = "No data";

            }
        }

        catch { }

    }
    public void bindGrid(string clientid, string strTop)
    {
        dsMain = DatabaseHelper.bindCompanydetails(clientid, strTop);
        try
        {
            if (dsMain.Tables.Count > 0)
            {
                if (dsMain.Tables[0].Rows.Count > 0)
                {
                    dsMain = changedataset(dsMain);
                    if (sort == "")
                    {
                        OrderbyDate("DateTime desc");
                    }
                    dgCompany.DataSource = dv.ToTable();
                    dgCompany.DataBind();
                    lblDisplay.Visible = false;
                }
                else
                {
                    dgCompany.DataSource = dsMain;
                    dgCompany.DataBind();
                    lblDisplay.Visible = true;
                    lblDisplay.Text = "Record was not found";
                }
            }
        }
        catch { }
    }

    private DataSet changedataset(DataSet ds)
    {
        try
        {
            ds.Tables[0].Columns.Add("Dealtype22");
            ds.Tables[0].Columns.Add("PriceP");
            ds.Tables[0].Columns.Add("Address22");
            ds.Tables[0].Columns.Add("DateTime22");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (ds.Tables[0].Rows[i]["PropertyId"].ToString() != "")
                {
                    if (ds.Tables[0].Rows[i]["eventtype"] != null && ds.Tables[0].Rows[i]["eventtype"].ToString().Trim() != "")
                    {
                        strEventType = ds.Tables[0].Rows[i]["eventtype"].ToString();
                    }
                    if (strEventType != "Deleted")
                    {
                        if (ds.Tables[0].Rows[i]["DealType"] != null && ds.Tables[0].Rows[i]["DealType"].ToString().Trim() != "")
                        {
                            string strDeal = ds.Tables[0].Rows[i]["DealType"].ToString();
                            if (strDeal == "1")
                            {
                                ds.Tables[0].Rows[i]["DealType22"] = "Buy";
                                bindSale(ds.Tables[0].Rows[i]["PropertyId"].ToString(), ds.Tables[0].Rows[i]["clientid"].ToString());
                                if (cds.Tables.Count > 0)
                                {
                                    if (cds.Tables[0].Rows.Count > 0)
                                    {
                                        dealPrice = cds.Tables[0].Rows[0]["bindSale"].ToString();
                                    }
                                    else
                                    {
                                        dealPrice = "";
                                    }
                                    ds.Tables[0].Rows[i]["PriceP"] = "£" + dealPrice;
                                }
                            }
                            else if (strDeal == "2")
                            {
                                ds.Tables[0].Rows[i]["DealType22"] = "Let";
                                bindRent(ds.Tables[0].Rows[i]["PropertyId"].ToString(), ds.Tables[0].Rows[i]["clientid"].ToString());
                                if (cdsRent.Tables.Count > 0)
                                {
                                    if (cdsRent.Tables[0].Rows.Count > 0)
                                    {
                                        dealPrice = cdsRent.Tables[0].Rows[0]["bindRent"].ToString();
                                        if (cdsRent.Tables[0].Rows[0]["PricePer"] != null && cdsRent.Tables[0].Rows[0]["PricePer"].ToString().Trim() != "")
                                        {
                                            string strPricep = statusPricePer(cdsRent.Tables[0].Rows[0]["PricePer"].ToString());
                                            dealPrice = dealPrice + " " + strPricep;
                                        }
                                    }
                                    else
                                    {
                                        dealPrice = "";
                                    }
                                    ds.Tables[0].Rows[i]["PriceP"] = "£" + dealPrice;
                                }
                            }
                            else if (strDeal == "4")
                            {

                                bindSale(ds.Tables[0].Rows[i]["PropertyId"].ToString(), ds.Tables[0].Rows[i]["clientid"].ToString());
                                if (cds.Tables.Count > 0)
                                {
                                    if (cds.Tables[0].Rows.Count > 0)
                                    {
                                        dealPrice = cds.Tables[0].Rows[0]["bindSale"].ToString();
                                        dealType = "Buy";
                                    }
                                    else
                                    {
                                        dealPrice = "";
                                        dealType = "";
                                    }
                                }
                                bindRent(ds.Tables[0].Rows[i]["PropertyId"].ToString(), ds.Tables[0].Rows[i]["clientid"].ToString());
                                if (cdsRent.Tables.Count > 0)
                                {
                                    if (cdsRent.Tables[0].Rows.Count > 0)
                                    {
                                        if (dealPrice != "")
                                        {
                                            dealPrice = dealPrice + " / " + cdsRent.Tables[0].Rows[0]["bindRent"].ToString();
                                        }
                                        else
                                        {
                                            dealPrice = cdsRent.Tables[0].Rows[0]["bindRent"].ToString();
                                        }
                                        if (cdsRent.Tables[0].Rows[0]["PricePer"] != null && cdsRent.Tables[0].Rows[0]["PricePer"].ToString().Trim() != "")
                                        {
                                            string priceper = statusPricePer(cdsRent.Tables[0].Rows[0]["PricePer"].ToString());

                                            dealPrice = dealPrice + " " + priceper;
                                        }
                                        if (dealType != "")
                                        {

                                            ds.Tables[0].Rows[i]["DealType22"] = dealType + "/" + "Let";
                                        }
                                        else
                                        {
                                            ds.Tables[0].Rows[i]["DealType22"] = "Let";
                                        }
                                    }
                                }
                                ds.Tables[0].Rows[i]["PriceP"] = "£" + dealPrice;
                            }
                        }
                        if (ds.Tables[0].Rows[i]["DateTime"] != null && ds.Tables[0].Rows[i]["DateTime"].ToString().Trim() != "")
                        {
                            DateTime dt = Convert.ToDateTime(ds.Tables[0].Rows[i]["DateTime"].ToString());
                            ds.Tables[0].Rows[i]["DateTime22"] = dt.ToShortDateString();
                        }
                        if (ds.Tables[0].Rows[i]["PostCode"] != null && ds.Tables[0].Rows[i]["PostCode"].ToString() != "")
                        {
                            strPostCode = "," + ds.Tables[0].Rows[0]["PostCode"].ToString();
                        }
                        //ds.Tables[0].Rows[i]["Address22"] = ds.Tables[0].Rows[i]["Address"].ToString();
                        if (ds.Tables[0].Rows[i]["Address"] != null && ds.Tables[0].Rows[i]["Address"].ToString() != "")
                        {
                            string strAddress = ds.Tables[0].Rows[i]["Address"].ToString();
                            string strRegion = ds.Tables[0].Rows[i]["region"].ToString();
                            string strTown = ds.Tables[0].Rows[i]["town"].ToString();

                            if ((strAddress.ToString() != "") && (strRegion.ToString() != "") && (strTown.ToString() != ""))
                            {
                                ds.Tables[0].Rows[i]["Address22"] = strAddress.ToString() + ", " + strRegion.ToString() + ", " + strTown.ToString() + strPostCode;
                            }
                            else if ((strAddress.ToString() == "") && (strRegion.ToString() != "") && (strTown.ToString() != ""))
                            {
                                ds.Tables[0].Rows[i]["Address22"] = strRegion.ToString() + ", " + strTown.ToString() + strPostCode;
                            }
                            else if ((strAddress.ToString() != "") && (strRegion.ToString() == "") && (strTown.ToString() != ""))
                            {
                                ds.Tables[0].Rows[i]["Address22"] = strAddress.ToString() + ", " + strTown.ToString() + strPostCode;
                            }
                            else if ((strAddress.ToString() != "") && (strRegion.ToString() != "") && (strTown.ToString() == ""))
                            {
                                ds.Tables[0].Rows[i]["Address22"] = strAddress.ToString() + ", " + strRegion.ToString() + strPostCode;
                            }
                            else if ((strAddress.ToString() != "") && (strRegion.ToString() == "") && (strTown.ToString() == ""))
                            {
                                ds.Tables[0].Rows[i]["Address22"] = strAddress.ToString() + strPostCode;
                            }
                            else if ((strAddress.ToString() == "") && (strRegion.ToString() != "") && (strTown.ToString() == ""))
                            {
                                ds.Tables[0].Rows[i]["Address22"] = strRegion.ToString() + strPostCode;
                            }
                            else if ((strAddress.ToString() == "") && (strRegion.ToString() == "") && (strTown.ToString() != ""))
                            {
                                ds.Tables[0].Rows[i]["Address22"] = strTown.ToString() + strPostCode;
                            }
                            //ds.Tables[0].Rows[i]["Address"] = ds.Tables[0].Rows[i]["Address"].ToString().TrimEnd(new char[] { ',', ' ' }).TrimStart(new char[] { ',', ' ' });// +postcode2;



                        }
                        else
                        {
                            ds.Tables[0].Rows[i]["Address22"] = "";
                        }

                    }
                    else
                    {
                        if (ds.Tables[0].Rows[i]["Price"] != null && ds.Tables[0].Rows[i]["Price"].ToString().Trim() != "")
                        {
                            dealPrice = ds.Tables[0].Rows[i]["Price"].ToString();
                        }
                        if (ds.Tables[0].Rows[i]["PricePer"] != null && ds.Tables[0].Rows[i]["PricePer"].ToString().Trim() != "")
                        {
                            dealPrice = dealPrice + " " + ds.Tables[0].Rows[i]["PricePer"].ToString();
                            ds.Tables[0].Rows[i]["DealType22"] = "Let";
                        }
                        else
                        {
                            ds.Tables[0].Rows[i]["DealType22"] = "Buy";
                        }
                        ds.Tables[0].Rows[i]["PriceP"] = "£" + dealPrice;

                        if (ds.Tables[0].Rows[i]["PropertyId"] != null && ds.Tables[0].Rows[i]["PropertyId"].ToString().Trim() != "")
                        {
                            ds.Tables[0].Rows[i]["Address22"] = ds.Tables[0].Rows[i]["PropertyId"].ToString();
                        }
                        if (ds.Tables[0].Rows[i]["DateTime"] != null && ds.Tables[0].Rows[i]["DateTime"].ToString().Trim() != "")
                        {
                            DateTime dt = Convert.ToDateTime(ds.Tables[0].Rows[i]["DateTime"].ToString());
                            ds.Tables[0].Rows[i]["DateTime22"] = dt.ToShortDateString();
                        }
                    }
                }



            }


        }
        catch { }
        return ds;
    }
    public DataSet bindSale(string propertyid, string clientid)
    {

        cds = DatabaseHelper.bindSale(propertyid, clientid);
        cds.Tables[0].Columns.Add("bindSale");
        if (cds.Tables.Count > 0)
        {
            if (cds.Tables[0].Rows.Count > 0)
            {
                if (cds.Tables[0].Rows[0]["AskingPrice"] != null && cds.Tables[0].Rows[0]["AskingPrice"].ToString().Trim() != "")
                {
                    cds.Tables[0].Rows[0]["bindSale"] = cds.Tables[0].Rows[0]["AskingPrice"].ToString();
                }
            }
        }
        return cds;
    }

    public DataSet bindRent(string propertyid, string clientid)
    {
        cdsRent = DatabaseHelper.bindRent(propertyid, clientid);
        cdsRent.Tables[0].Columns.Add("bindRent");
        if (cdsRent.Tables.Count > 0)
        {
            if (cdsRent.Tables[0].Rows.Count > 0)
            {
                if (cdsRent.Tables[0].Rows[0]["Price"] != null && cdsRent.Tables[0].Rows[0]["Price"].ToString().Trim() != "")
                {
                    cdsRent.Tables[0].Rows[0]["bindRent"] = cdsRent.Tables[0].Rows[0]["Price"].ToString();
                }
            }
        }
        return cds;
    }

    public string statusPricePer(string priceper)
    {
        switch (priceper)
        {
            case "1":
                strPricePer = "Month";
                break;
            case "2":
                strPricePer = "Quarter";
                break;
            case "3":
                strPricePer = "Year";
                break;
            default:
                strPricePer = "Week";
                break;
        }
        return strPricePer;
    }


    public void OrderbyDate(string sort)
    {
        try
        {
            if (dsMain.Tables.Count > 0)
            {
                if (dsMain.Tables[0].Rows.Count > 0)
                {

                    dv = dsMain.Tables[0].DefaultView;
                    dv.Sort = sort;
                    //dv.Table.Clear();
                    //dsMain.Tables.Clear();
                    //dsMain.Tables.Add(dv.ToTable());
                    //dgCompany.DataSource = dsMain.Tables[0].DefaultView;
                    //dgCompany.DataBind();
                    //dsMain.AcceptChanges();

                    dgCompany.DataSource = dv.ToTable();
                    dgCompany.DataBind();
                }
            }

        }
        catch { }
    }
    public void sortDS(string typeName)
    {
        try
        {
            if (typeName != "All")
            {
                if (dsMain.Tables.Count > 0)
                {
                    if (dsMain.Tables[0].Rows.Count > 0)
                    {

                        if (ddlDate.SelectedValue != "All")
                        {
                            bindByDateandEventtype();
                        }
                        else if (ddlType.SelectedValue != "All" && ddlNumber.SelectedValue != "All")
                        {
                            //bindNumberCombo();

                            if (ddlNumber.SelectedValue == "All in last 7 days" || ddlNumber.SelectedValue == "All in last 30 days" || ddlNumber.SelectedValue == "All in last 60 days" || ddlNumber.SelectedValue == "All in last 360 days")
                            {
                                bindNumberCombo();
                            }
                            else
                            {
                                dsMain.Tables[0].DefaultView.Sort = sort;
                                dsMain.Tables[0].DefaultView.RowFilter = "eventtype='" + typeName + "'";
                                if (dsMain.Tables[0].DefaultView.Count > 0)
                                {
                                    dgCompany.DataSource = dsMain.Tables[0].DefaultView;
                                    dgCompany.DataBind();
                                    lblDisplay.Visible = false;
                                }
                                else
                                {
                                    dgCompany.DataSource = dsMain.Tables[0].DefaultView;
                                    dgCompany.DataBind();
                                    lblDisplay.Visible = true;
                                    lblDisplay.Text = "Record not found";
                                }
                            }
                        }

                        else
                        {
                            if (ddlNumber.SelectedValue == "All" && ddlType.SelectedValue != "All")
                            {
                                getClientid(ddlCompanyname.SelectedItem.Text);
                                bindGrid(strClient, "");
                            }
                            dsMain.Tables[0].DefaultView.Sort = sort;
                            dsMain.Tables[0].DefaultView.RowFilter = "eventtype='" + typeName + "'";
                            if (dsMain.Tables[0].DefaultView.Count > 0)
                            {
                                dgCompany.DataSource = dsMain.Tables[0].DefaultView;

                                dgCompany.DataBind();
                                lblDisplay.Visible = false;
                            }
                            else
                            {
                                dgCompany.DataSource = dsMain.Tables[0].DefaultView;
                                dgCompany.DataBind();
                                lblDisplay.Visible = true;
                                lblDisplay.Text = "Record not found";
                            }

                        }
                    }

                }
            }
            else if ((typeName == "All") && (ddlDate.SelectedValue != "All"))
            {
                bindByDateandEventtype();
            }
            else if ((typeName == "All") && (ddlDate.SelectedValue == "All") && (ddlNumber.SelectedValue != "All"))
            {
                bindNumberCombo();
            }
            else
            {
                dgCompany.DataSource = dsMain;
                dgCompany.DataBind();
                lblDisplay.Visible = false;
            }
        }
        catch { }
    }
    public void bindByDateandEventtype()
    {
        try
        {
            if (ddlDate.SelectedValue != "All")
            {
                if (dsMain.Tables.Count > 0)
                {
                    if (dsMain.Tables[0].Rows.Count > 0)
                    {
                        if (ddlType.SelectedValue != "All")
                        {
                            strEventType = ddlType.SelectedValue;
                        }
                        if (ddlDate.SelectedValue == "1")
                        {
                            DataTable dt;
                            if (ddlCompanyname.SelectedItem.Text == "All" && ddlType.SelectedValue == "All" && ddlDate.SelectedValue != "All" && ddlNumber.SelectedValue == "All")
                            {
                                dt = dsMain.Tables[0];
                            }
                            else
                            {
                                dt = dv.ToTable();
                            }
                            DataView dv2 = new DataView(dt);
                            string EndDate, StartDate = "";
                            DateTime myDateTime1 = DateTime.Parse(System.DateTime.Now.ToShortDateString(), System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat);

                            StartDate = myDateTime1.ToString("MM/dd/yyyy");
                            DateTime myDateTime2 = DateTime.Parse(System.DateTime.Now.ToShortDateString(), System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat);
                            myDateTime2 = myDateTime2.AddDays(-1);

                            EndDate = myDateTime2.ToString("MM/dd/yyyy");
                            if (ddlType.SelectedValue != "All")
                            {
                                dv2.RowFilter = "eventtype='" + strEventType + "' and DateTime <=#" + StartDate + "# and DateTime>=#" + EndDate + "#";
                            }
                            else
                            {
                                dv2.RowFilter = "DateTime <=#" + StartDate + "# and DateTime>=#" + EndDate + "#";
                            }
                            if (dv2.Count >= 1)
                            {
                                lblDisplay.Visible = false;
                                dv2.Sort = sort;
                                dgCompany.DataSource = dv2.ToTable();
                                dgCompany.DataBind();
                            }
                            else
                            {
                                lblDisplay.Visible = true;
                                lblDisplay.Text = "Record was not found";
                                dgCompany.DataSource = dv2.ToTable();
                                dgCompany.DataBind();
                            }
                        }
                        else if (ddlDate.SelectedValue == "7" || ddlDate.SelectedValue == "30")
                        {
                            double ddays = Convert.ToDouble(ddlDate.SelectedValue.ToString());
                            DataTable dt;
                            if (ddlCompanyname.SelectedItem.Text == "All" && ddlType.SelectedValue == "All" && ddlDate.SelectedValue != "All" && ddlNumber.SelectedValue == "All")
                            {
                                dt = dsMain.Tables[0];
                            }
                            else
                            {
                                dt = dv.ToTable();
                            }
                            DataView dv2 = new DataView(dt);
                            string EndDate, StartDate = "";
                            DateTime myDateTime1 = DateTime.Parse(System.DateTime.Now.ToShortDateString(), System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat);
                            myDateTime1 = myDateTime1.AddDays(1);
                            StartDate = myDateTime1.ToString("MM/dd/yyyy");
                            DateTime myDateTime2 = DateTime.Parse(System.DateTime.Now.ToShortDateString(), System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat);
                            myDateTime2 = myDateTime2.AddDays(-ddays);

                            EndDate = myDateTime2.ToString("MM/dd/yyyy");
                            if (ddlType.SelectedValue != "All")
                            {
                                dv2.RowFilter = "eventtype='" + strEventType + "' and DateTime <=#" + StartDate + "# and DateTime>=#" + EndDate + "#";
                            }
                            else
                            {
                                dv2.RowFilter = "DateTime <=#" + StartDate + "# and DateTime>=#" + EndDate + "#";
                            }
                            if (dv2.Count >= 1)
                            {
                                lblDisplay.Visible = false;
                                dv2.Sort = sort;
                                dgCompany.DataSource = dv2.ToTable();
                                dgCompany.DataBind();
                            }
                            else
                            {
                                lblDisplay.Visible = true;
                                lblDisplay.Text = "Record was not found";
                                dgCompany.DataSource = dv2.ToTable();
                                dgCompany.DataBind();
                            }
                        }

                        else
                        {
                            DataTable dt;
                            if (ddlCompanyname.SelectedItem.Text == "All" && ddlType.SelectedValue == "All" && ddlDate.SelectedValue != "All" && ddlNumber.SelectedValue == "All")
                            {
                                dt = dsMain.Tables[0];

                            }
                            else
                            {
                                dt = dv.ToTable();
                            }
                            DataView dv2 = new DataView(dt);
                            string EndDate, StartDate = "";
                            DateTime myDateTime1 = DateTime.Parse(System.DateTime.Now.ToShortDateString(), System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat);
                            //  myDateTime1 = myDateTime1.AddDays(1);
                            StartDate = myDateTime1.ToString("MM/dd/yyyy");

                            DateTime myDateTime2 = DateTime.Parse(System.DateTime.Now.ToShortDateString(), System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat);
                            myDateTime2 = myDateTime2.AddDays(1);
                            EndDate = myDateTime2.ToString("MM/dd/yyyy");
                            if (ddlType.SelectedValue != "All")
                            {
                                dv2.RowFilter = "eventtype='" + strEventType + "' and DateTime >#" + StartDate + "# and DateTime<=#" + EndDate + "#";
                            }
                            else
                            {
                                dv2.RowFilter = "DateTime >#" + StartDate + "# and DateTime<#" + EndDate + "#";
                            }
                            if (dv2.Count >= 1)
                            {
                                lblDisplay.Visible = false;
                                dv2.Sort = sort;
                                dgCompany.DataSource = dv2.ToTable();
                                dgCompany.DataBind();
                            }
                            else
                            {
                                lblDisplay.Visible = true;
                                lblDisplay.Text = "Record was not found";
                                dgCompany.DataSource = dv2.ToTable();
                                dgCompany.DataBind();
                            }

                        }

                    }
                }
            }
            else if (ddlType.SelectedValue != "All" && (ddlNumber.SelectedValue == "All"))
            {
                sortDS(ddlType.SelectedValue);
            }
            else if ((ddlNumber.SelectedValue != "All" && ddlType.SelectedValue == "All") || (ddlNumber.SelectedValue != "All" && ddlType.SelectedValue != "All"))
            {
                bindNumberCombo();
            }
            else if (ddlNumber.SelectedValue == "All" && ddlType.SelectedValue == "All" && ddlDate.SelectedValue == "All" && ddlCompanyname.SelectedItem.Text == "All")
            {
                dgCompany.DataSource = dv.ToTable();
                dgCompany.DataBind();
                lblDisplay.Visible = false;
            }
            else
            {
                ddlType.SelectedIndex = 0;
                dgCompany.DataSource = dsMain;
                dgCompany.DataBind();
                lblDisplay.Visible = false;
            }

        }
        catch { }
    }


    public void bindNumberCombo()
    {
        if (ddlNumber.SelectedValue != "All")
        {
            if (ddlNumber.SelectedValue != "All in last 7 days" && ddlNumber.SelectedValue != "All in last 30 days" && ddlNumber.SelectedValue != "All in last 60 days" && ddlNumber.SelectedValue != "All in last 360 days")
            {
                // bindNumberCombowithselectedvalue(ddlNumber.SelectedValue);
                if (ddlCompanyname.SelectedItem.Text != "All")
                {
                    getClientid(ddlCompanyname.SelectedItem.Text);
                    if (ddlNumber.SelectedValue != "All")
                    {
                        bindGrid(strClient, ddlNumber.SelectedValue);
                    }
                    else
                    {
                        bindGrid(strClient, "");
                    }
                    if (ddlType.SelectedValue != "All" && ddlDate.SelectedValue != "All")
                    {
                        sortDS(ddlType.SelectedValue);
                    }
                    else if (ddlType.SelectedValue != "All" && ddlDate.SelectedValue == "All")
                    {
                        sortDS(ddlType.SelectedValue);
                    }
                    else if (ddlNumber.SelectedValue != "All" && ddlDate.SelectedValue != "All")
                    {
                        bindByDateandEventtype();
                    }

                }
                else if (ddlCompanyname.SelectedItem.Text != "All" && ddlType.SelectedValue == "All" && ddlNumber.SelectedValue == "All" && ddlDate.SelectedValue == "All")
                {
                    getClientid(ddlCompanyname.SelectedItem.Text);
                    bindGrid(strClient, "");
                    if (ddlType.SelectedValue != "All" && ddlDate.SelectedValue != "All")
                    {
                        sortDS(ddlType.SelectedValue);
                    }
                    else if (ddlType.SelectedValue != "All" && ddlDate.SelectedValue == "All")
                    {
                        sortDS(ddlType.SelectedValue);
                    }

                }
                else if (ddlCompanyname.SelectedItem.Text == "All" && ddlType.SelectedValue != "All" && ddlNumber.SelectedValue != "All" && ddlDate.SelectedValue != "All")
                {
                    //getClientid(ddlCompanyname.SelectedValue);
                    //bindGrid(strClient, "10");
                    sortDS(ddlType.SelectedValue);


                }
                else if (ddlCompanyname.SelectedItem.Text == "All" && ddlType.SelectedValue != "All" && ddlNumber.SelectedValue != "All" && ddlDate.SelectedValue == "All")
                {
                    //getClientid(ddlCompanyname.SelectedValue);
                    //bindGrid(strClient, "10");
                    sortDS(ddlType.SelectedValue);


                }
                else
                {
                    bindCompanyAll();
                    bindGrid(strClient, "");
                }
            }
            else
            {
                sort = "";
                bindCompanyAll();
                ddlCompanyname.SelectedItem.Text = "All";
                bindGrid(strClient, "");


                //ddlType.SelectedIndex = 0;
                // ddlDate.SelectedIndex = 0;
                //ddlNumber.SelectedIndex = 0;
                if (ddlNumber.SelectedValue == "All in last 7 days")
                {
                    bindNumberCombowithselectedvalue("7");
                }
                else if (ddlNumber.SelectedValue == "All in last 30 days")
                {
                    bindNumberCombowithselectedvalue("30");
                }
                else if (ddlNumber.SelectedValue == "All in last 60 days")
                {
                    bindNumberCombowithselectedvalue("60");
                }
                else if (ddlNumber.SelectedValue == "All in last 360 days")
                {
                    bindNumberCombowithselectedvalue("360");
                }
            }
        }
        else if (ddlNumber.SelectedValue == "All" && ddlType.SelectedValue != "All")
        {
            getClientid(ddlCompanyname.SelectedItem.Text);
            bindGrid(strClient, "");
            sortDS(ddlType.SelectedValue);
        }
        else if (ddlNumber.SelectedValue == "All" && ddlType.SelectedValue == "All" && ddlCompanyname.SelectedItem.Text != "All")
        {
            getClientid(ddlCompanyname.SelectedItem.Text);
            bindGrid(strClient, "");
        }
        else if ((ddlNumber.SelectedValue == "All" && ddlType.SelectedValue == "All" && ddlCompanyname.SelectedItem.Text == "All") || (ddlNumber.SelectedValue == "All" && ddlType.SelectedValue != "All" && ddlCompanyname.SelectedItem.Text == "All"))
        {

            lblDisplay.Visible = false;
            dv.Sort = sort;
            dgCompany.DataSource = dv.ToTable();
            dgCompany.DataBind();

        }
        else
        {
            lblDisplay.Visible = true;
            lblDisplay.Text = "Record was not found";
            dgCompany.DataSource = dsMain;
            dgCompany.DataBind();
        }
    }
    public void bindNumberCombowithselectedvalue(string strValue)
    {
        double ddays = Convert.ToDouble(strValue);
        DataTable dt = dv.ToTable();
        DataView dv2 = new DataView(dt);
        string EndDate, StartDate = "";
        DateTime myDateTime1 = DateTime.Parse(System.DateTime.Now.ToShortDateString(), System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat);
        myDateTime1 = myDateTime1.AddDays(1);
        StartDate = myDateTime1.ToString("MM/dd/yyyy");
        DateTime myDateTime2 = DateTime.Parse(System.DateTime.Now.ToShortDateString(), System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat);
        myDateTime2 = myDateTime2.AddDays(-ddays);

        EndDate = myDateTime2.ToString("MM/dd/yyyy");
        if (ddlType.SelectedValue != "All")
        {
            dv2.RowFilter = "eventtype='" + ddlType.SelectedValue + "' and DateTime <=#" + StartDate + "# and DateTime>=#" + EndDate + "#";
        }
        else
        {
            dv2.RowFilter = "DateTime <=#" + StartDate + "# and DateTime>=#" + EndDate + "#";
        }
        if (dv2.Count >= 1)
        {
            lblDisplay.Visible = false;
            if (ViewState["sort"] != null)
            {
                dv2.Sort = ViewState["sort"].ToString();
            }
            dgCompany.DataSource = dv2.ToTable();
            dgCompany.DataBind();
        }
        else
        {
            lblDisplay.Visible = true;
            lblDisplay.Text = "Record was not found";
            dgCompany.DataSource = dv2.ToTable();
            dgCompany.DataBind();
        }
    }
    protected void dgCompany_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        if (e.CommandName == "sortcompanyname")
        {
            if (ViewState["sortCompanyName"] == null)
            {
                ViewState["sortCompanyName"] = "asc";
            }
            if (ViewState["sortCompanyName"].ToString() == "asc")
            {
                sort = "CompanyName asc";
                ViewState["sortCompanyName"] = "desc";

            }
            else
            {
                sort = "CompanyName desc";
                ViewState["sortCompanyName"] = "asc";
            }

        }
        if (e.CommandName == "sortclientid")
        {
            if (ViewState["sortclientid"] == null)
            {
                ViewState["sortclientid"] = "asc";
            }
            if (ViewState["sortclientid"].ToString() == "asc")
            {
                sort = "clientid asc";
                ViewState["sortclientid"] = "desc";
            }
            else
            {
                sort = "clientid desc";
                ViewState["sortclientid"] = "asc";
            }



        }
        if (e.CommandName == "sortusername")
        {
            if (ViewState["sortUserName"] == null)
            {
                ViewState["sortUserName"] = "asc";
            }
            if (ViewState["sortUserName"].ToString() == "asc")
            {
                sort = "UserName asc";
                ViewState["sortUserName"] = "desc";
            }
            else
            {
                sort = "UserName desc";
                ViewState["sortUserName"] = "asc";
            }

        }
        if (e.CommandName == "sortdatetime")
        {
            if (ViewState["sortDateTime"] == null)
            {
                ViewState["sortDateTime"] = "asc";
            }
            if (ViewState["sortDateTime"].ToString() == "asc")
            {
                sort = "DateTime asc";
                ViewState["sortDateTime"] = "desc";
            }
            else
            {
                sort = "DateTime desc";
                ViewState["sortDateTime"] = "asc";
            }

        }
        if (e.CommandName == "sortprice")
        {
            if (ViewState["sortPrice"] == null)
            {
                ViewState["sortPrice"] = "asc";
            }
            if (ViewState["sortPrice"].ToString() == "asc")
            {
                sort = "PriceP asc";
                ViewState["sortPrice"] = "desc";
            }
            else
            {
                sort = "PriceP desc";
                ViewState["sortPrice"] = "asc";
            }


        }
        if (e.CommandName == "sorttype")
        {
            if (ViewState["sortDealtype22"] == null)
            {
                ViewState["sortDealtype22"] = "asc";
            }
            if (ViewState["sortDealtype22"].ToString() == "asc")
            {
                sort = "Dealtype22 asc";
                ViewState["sortDealtype22"] = "desc";
            }
            else
            {
                sort = "Dealtype22 desc";
                ViewState["sortDealtype22"] = "asc";
            }

        }
        if (e.CommandName == "sorteventtype")
        {
            if (ViewState["sorteventtype"] == null)
            {
                ViewState["sorteventtype"] = "asc";
            }
            if (ViewState["sorteventtype"].ToString() == "asc")
            {
                sort = "eventtype asc";
                ViewState["sorteventtype"] = "desc";
            }
            else
            {
                sort = "eventtype desc";
                ViewState["sorteventtype"] = "asc";
            }



        }
        if (e.CommandName == "sortdescription")
        {

            if (ViewState["sortdescreption"] == null)
            {
                ViewState["sortdescreption"] = "asc";
            }
            if (ViewState["sortdescreption"].ToString() == "asc")
            {
                sort = "descreption asc";
                ViewState["sortdescreption"] = "desc";
            }
            else
            {
                sort = "descreption desc";
                ViewState["sortdescreption"] = "asc";
            }



        }
        if (e.CommandName == "sortaddress")
        {
            if (ViewState["sortAddress"] == null)
            {
                ViewState["sortAddress"] = "asc";
            }
            if (ViewState["sortAddress"].ToString() == "asc")
            {
                sort = "Address asc";
                ViewState["sortAddress"] = "desc";
            }
            else
            {
                sort = "Address desc";
                ViewState["sortAddress"] = "asc";
            }

        }

        if (sort != "")
        {
            ViewState["sort"] = sort;
            OrderbyDate(ViewState["sort"].ToString());
        }
        if ((ddlType.SelectedValue != "All" || ddlDate.SelectedValue != "All") && ddlCompanyname.SelectedItem.Text != "All")
        {
            sortDS(ddlType.SelectedValue);
        }
        else if (ddlType.SelectedValue != "All" && ddlDate.SelectedValue == "All" && ddlCompanyname.SelectedItem.Text == "All" && ddlNumber.SelectedValue == "All")
        {
            sortDS(ddlType.SelectedValue);
        }
        else if (ddlType.SelectedValue == "All" && ddlDate.SelectedValue != "All" && ddlCompanyname.SelectedItem.Text == "All" && ddlNumber.SelectedValue == "All")
        {
            bindByDateandEventtype();
        }

        else if ((ddlNumber.SelectedValue != "All" && ddlType.SelectedValue == "All") || (ddlNumber.SelectedValue != "All" && ddlType.SelectedValue != "All"))
        {
            bindNumberCombo();
        }
        else if ((ddlNumber.SelectedValue == "All" && ddlType.SelectedValue == "All" && ddlDate.SelectedValue == "All" && ddlCompanyname.SelectedItem.Text != "All"))
        {
            if (dv.Count > 0)
            {
                lblDisplay.Visible = false;
                dgCompany.DataSource = dv.ToTable();
                dgCompany.DataBind();
            }
            else
            {
                lblDisplay.Visible = false;
                dgCompany.DataSource = dsMain;
                dgCompany.DataBind();
            }
        }
        else if ((ddlNumber.SelectedValue == "All" && ddlType.SelectedValue == "All" && ddlDate.SelectedValue == "All" && ddlCompanyname.SelectedItem.Text == "All"))
        {
            lblDisplay.Visible = false;
            dgCompany.DataSource = dv.ToTable();
            dgCompany.DataBind();
        }
        else
        {
            if (dsMain.Tables.Count > 0)
            {
                if (dsMain.Tables[0].Rows.Count > 0)
                {
                    lblDisplay.Visible = false;
                    dgCompany.DataSource = dsMain;
                    dgCompany.DataBind();
                }
            }
            else if (dsMain.Tables.Count > 0)
            {
                if (dsMain.Tables[0].Rows.Count > 0)
                {
                    lblDisplay.Visible = false;
                    dgCompany.DataSource = dsMain;
                    dgCompany.DataBind();
                }
            }
            else
            {
                lblDisplay.Visible = true;
                lblDisplay.Text = "Record was not found";
                dgCompany.DataSource = dsMain;
                dgCompany.DataBind();
            }
        }

    }
    protected void dgCompany_RowDeleting1(object sender, GridViewDeleteEventArgs e)
    {

    }
    protected void dgCompany_RowEditing1(object sender, GridViewEditEventArgs e)
    {

    }

    protected void ddlNumber_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if ((ddlNumber.SelectedValue == "All in last 7 days" || ddlNumber.SelectedValue == "All in last 30 days" || ddlNumber.SelectedValue == "All in last 60 days" || ddlNumber.SelectedValue == "All in last 360 days"))

        //{
        if (chkSearch.Checked == true)
        {
            ViewState["sort"] = null;
            //sort = "DateTime desc";               
            bindNumberCombo();
        }
        ///}
    }
    protected void chkSearch_CheckedChanged(object sender, EventArgs e)
    {

        if (chkSearch.Checked == true)
        {
            ///  ModalUpdateProgress1.Visible = true;
            lnkSearch.Visible = false;

            //// ddlCompanyname.Enabled = true;

            //     //ddlCompanyname.BorderColor = System.Drawing.Color.Blue;

            //     if ((ddlType.SelectedValue != "All" || ddlDate.SelectedValue != "All") && ddlCompanyname.SelectedValue != "All")
            //     {
            //         sortDS(ddlType.SelectedValue);
            //     }
            //     else if (ddlType.SelectedValue != "All" && ddlDate.SelectedValue == "All" && ddlCompanyname.SelectedValue == "All" && ddlNumber.SelectedValue == "All")
            //     {
            //         sortDS(ddlType.SelectedValue);
            //     }
            //     else if (ddlType.SelectedValue == "All" && ddlDate.SelectedValue != "All" && ddlCompanyname.SelectedValue == "All" && ddlNumber.SelectedValue == "All")
            //     {
            //         bindByDateandEventtype();
            //     }
            //     else if ((ddlNumber.SelectedValue != "All" && ddlType.SelectedValue == "All") || (ddlNumber.SelectedValue != "All" && ddlType.SelectedValue != "All"))
            //     {
            //         bindNumberCombo();
            //     }
            //     else if ((ddlNumber.SelectedValue == "All" && ddlType.SelectedValue == "All" && ddlDate.SelectedValue == "All" && ddlCompanyname.SelectedValue != "All"))
            //     {
            //         if (dv.Count > 0)
            //         {
            //             lblDisplay.Visible = false;
            //             dgCompany.DataSource = dv.ToTable();
            //             dgCompany.DataBind();
            //         }
            //         else
            //         {
            //             lblDisplay.Visible = false;
            //             dgCompany.DataSource = dsMain;
            //             dgCompany.DataBind();
            //         }
            //     }
            //     else if ((ddlNumber.SelectedValue == "All" && ddlType.SelectedValue == "All" && ddlDate.SelectedValue == "All" && ddlCompanyname.SelectedValue == "All"))
            //     {
            //         lblDisplay.Visible = false;
            //         dgCompany.DataSource = dv.ToTable();
            //         dgCompany.DataBind();
            //     }
            //     else
            //     {
            //         if (dsMain.Tables.Count > 0)
            //         {
            //             if (dsMain.Tables[0].Rows.Count > 0)
            //             {
            //                 lblDisplay.Visible = false;
            //                 dgCompany.DataSource = dsMain;
            //                 dgCompany.DataBind();
            //             }
            //         }
            //         else if (dsMain.Tables.Count > 0)
            //         {
            //             if (dsMain.Tables[0].Rows.Count > 0)
            //             {
            //                 lblDisplay.Visible = false;
            //                 dgCompany.DataSource = dsMain;
            //                 dgCompany.DataBind();
            //             }
            //         }
            //         else
            //         {
            //             lblDisplay.Visible = true;
            //             lblDisplay.Text = "Record was not found";
            //             dgCompany.DataSource = dsMain;
            //             dgCompany.DataBind();
            //         }
            //     }
            bindData();

        }
        else
        {
            /// ModalUpdateProgress1.Visible = false;
            lnkSearch.Visible = true;
            //ddlCompanyname.BackColor = System.Drawing.Color.White;
            //ddlCompanyname.Enabled = false;
        }
    }


    protected void lnkSearch_Click(object sender, EventArgs e)
    {
        if (ddlCompanyname.SelectedItem.Text != "Select company")
        {
            if (chkSearch.Checked == false)
            {

                bindData();

            }
        }
        else
        {
            if (ddlNumber.SelectedValue == "All in last 7 days" || ddlNumber.SelectedValue == "All in last 30 days" || ddlNumber.SelectedValue == "All in last 60 days" || ddlNumber.SelectedValue == "All in last 360 days")
            {
                if (chkSearch.Checked == false)
                {
                    ViewState["sort"] = null;
                    sort = "DateTime desc";
                    //ddlDate.SelectedIndex=0;
                    bindNumberCombo();
                }
            }
        }
    }
    public void bindData()
    {
        if (ddlCompanyname.SelectedItem.Text != "Select company")
        {
            //ddlCompanyname.BorderColor = System.Drawing.Color.Blue;
            if (dsMain.Tables.Count > 0)
            {

                if (dsMain.Tables[0].Rows.Count > 0)
                {
                    ViewState["sort"] = null;
                    string strCompanyname = dsMain.Tables[0].Rows[0]["CompanyName"].ToString();
                    if (strCompanyname == ddlCompanyname.SelectedItem.Text)
                    {
                        if (ddlType.SelectedValue != "All" && ddlDate.SelectedValue == "All" && ddlCompanyname.SelectedItem.Text != "All" && ddlNumber.SelectedValue == "All")
                        {
                            sortDS(ddlType.SelectedValue);
                        }
                        else if (ddlType.SelectedValue != "All" && ddlDate.SelectedValue != "All" && ddlCompanyname.SelectedItem.Text != "All" && ddlNumber.SelectedValue == "All")
                        {
                            sortDS(ddlType.SelectedValue);
                        }
                        else if (ddlType.SelectedValue == "All" && ddlDate.SelectedValue != "All" && ddlCompanyname.SelectedItem.Text != "All" && ddlNumber.SelectedValue == "All")
                        {
                            sortDS(ddlType.SelectedValue);
                        }
                        else if (ddlType.SelectedValue != "All" && ddlDate.SelectedValue == "All" && ddlCompanyname.SelectedItem.Text == "All" && ddlNumber.SelectedValue == "All")
                        {
                            sortDS(ddlType.SelectedValue);
                        }
                        else if (ddlType.SelectedValue == "All" && ddlDate.SelectedValue != "All" && ddlCompanyname.SelectedItem.Text == "All" && ddlNumber.SelectedValue == "All")
                        {
                            bindByDateandEventtype();
                        }
                        else if ((ddlNumber.SelectedValue != "All" && ddlType.SelectedValue == "All") || (ddlNumber.SelectedValue != "All" && ddlType.SelectedValue != "All"))
                        {
                            sort = "";
                            bindNumberCombo();
                        }
                        else if (ddlNumber.SelectedValue == "All" && ddlType.SelectedValue == "All" && ddlCompanyname.SelectedItem.Text != "All")
                        {
                            bindGrid(dsMain.Tables[0].Rows[0]["ClientId"].ToString(), "");
                        }
                        else if ((ddlNumber.SelectedValue == "All" && ddlType.SelectedValue == "All" && ddlDate.SelectedValue == "All" && ddlCompanyname.SelectedItem.Text != "All"))
                        {
                            if (dv.Count > 0)
                            {
                                lblDisplay.Visible = false;
                                dgCompany.DataSource = dv.ToTable();
                                dgCompany.DataBind();
                            }
                            else
                            {
                                lblDisplay.Visible = false;
                                dgCompany.DataSource = dsMain;
                                dgCompany.DataBind();
                            }
                        }
                        else if ((ddlNumber.SelectedValue == "All" && ddlType.SelectedValue == "All" && ddlDate.SelectedValue == "All" && ddlCompanyname.SelectedItem.Text == "All"))
                        {
                            lblDisplay.Visible = false;
                            dgCompany.DataSource = dv.ToTable();
                            dgCompany.DataBind();
                        }
                        else
                        {
                            if (dsMain.Tables.Count > 0)
                            {
                                if (dsMain.Tables[0].Rows.Count > 0)
                                {
                                    lblDisplay.Visible = false;
                                    dgCompany.DataSource = dsMain;
                                    dgCompany.DataBind();
                                }
                            }
                            else if (dsMain.Tables.Count > 0)
                            {
                                if (dsMain.Tables[0].Rows.Count > 0)
                                {
                                    lblDisplay.Visible = false;
                                    dgCompany.DataSource = dsMain;
                                    dgCompany.DataBind();
                                }
                            }
                            else
                            {
                                lblDisplay.Visible = true;
                                lblDisplay.Text = "Record was not found";
                                dgCompany.DataSource = dsMain;
                                dgCompany.DataBind();
                            }
                        }
                    }
                    else
                    {
                        if (ddlCompanyname.SelectedItem.Text != "All")
                        {
                            ViewState["sort"] = null;
                            lblDisplay.Visible = false;
                            sort = "";
                            getClientid(ddlCompanyname.SelectedItem.Text);

                            if (ddlNumber.SelectedValue != "All")
                            {
                                bindNumberCombo();
                            }
                            else
                            {
                                bindGrid(strClient, "");
                                if (ddlType.SelectedValue != "All" && ddlDate.SelectedValue != "All")
                                {
                                    sortDS(ddlType.SelectedValue);
                                }
                                else if (ddlType.SelectedValue != "All" && ddlDate.SelectedValue == "All")
                                {
                                    sortDS(ddlType.SelectedValue);
                                }
                            }


                        }
                        else if (ddlCompanyname.SelectedItem.Text == "All")
                        {
                            ViewState["sort"] = null;
                            lblDisplay.Visible = false;
                            sort = "";
                            bindCompanyAll();
                            if (ddlNumber.SelectedValue != "All" && ddlDate.SelectedValue == "All")
                            {
                                bindNumberCombo();
                            }
                            else if (ddlNumber.SelectedValue == "All" && ddlDate.SelectedValue != "All")
                            {
                                bindByDateandEventtype();
                            }
                            else
                            {
                                bindGrid(strClient, "");
                                if (ddlType.SelectedValue != "All" && ddlDate.SelectedValue != "All")
                                {
                                    sortDS(ddlType.SelectedValue);
                                }
                                else if (ddlType.SelectedValue != "All" && ddlDate.SelectedValue == "All")
                                {
                                    sortDS(ddlType.SelectedValue);
                                }
                            }
                        }
                    }
                }
                else
                {
                    ViewState["sort"] = null;
                    if (ddlCompanyname.SelectedItem.Text != "All")
                    {

                        getClientid(ddlCompanyname.SelectedItem.Text);
                        if (ddlNumber.SelectedValue != "All")
                        {
                            bindNumberCombo();
                        }
                        else if (ddlDate.SelectedValue != "All")
                        {
                            bindByDateandEventtype();
                        }
                        else
                        {
                            bindGrid(strClient, "");
                            if (ddlType.SelectedValue != "All" && ddlDate.SelectedValue != "All")
                            {
                                sortDS(ddlType.SelectedValue);
                            }
                            else if (ddlType.SelectedValue != "All" && ddlDate.SelectedValue == "All")
                            {
                                sortDS(ddlType.SelectedValue);
                            }
                        }

                    }
                    else if (ddlCompanyname.SelectedItem.Text == "All")
                    {

                        sort = "";
                        ///  bindCompanyAll();

                        bindCompanyAll();
                        //getClientid(ddlCompanyname.SelectedValue);
                        if (ddlNumber.SelectedValue != "All")
                        {
                            bindNumberCombo();
                        }

                        else if (ddlDate.SelectedValue != "All")
                        {
                            bindByDateandEventtype();
                        }

                        else
                        {
                            bindGrid(strClient, "");
                            if (ddlType.SelectedValue != "All" && ddlDate.SelectedValue != "All")
                            {
                                sortDS(ddlType.SelectedValue);
                            }
                            else if (ddlType.SelectedValue != "All" && ddlDate.SelectedValue == "All")
                            {
                                sortDS(ddlType.SelectedValue);
                            }
                        }
                    }
                }
            }
            else
            {
                ViewState["sort"] = null;
                if (ddlCompanyname.SelectedItem.Text != "All")
                {
                    getClientid(ddlCompanyname.SelectedItem.Text);
                    if (ddlNumber.SelectedValue != "All")
                    {
                        bindNumberCombo();
                    }

                    else if (ddlDate.SelectedValue != "All")
                    {
                        bindGrid(strClient, "");
                        bindByDateandEventtype();
                    }

                    else
                    {
                        bindGrid(strClient, "");
                        if (ddlType.SelectedValue != "All" && ddlDate.SelectedValue != "All")
                        {
                            sortDS(ddlType.SelectedValue);
                        }
                        else if (ddlType.SelectedValue != "All" && ddlDate.SelectedValue == "All")
                        {
                            sortDS(ddlType.SelectedValue);
                        }
                    }

                }
                else if (ddlCompanyname.SelectedItem.Text == "All")
                {
                    bindCompanyAll();
                    // getClientid(ddlCompanyname.SelectedValue);
                    if (ddlNumber.SelectedValue != "All")
                    {
                        bindNumberCombo();
                    }
                    else if (ddlDate.SelectedValue != "All")
                    {
                        bindGrid(strClient, "");
                        bindByDateandEventtype();
                    }
                    else
                    {
                        bindGrid(strClient, "");
                        if (ddlType.SelectedValue != "All" && ddlDate.SelectedValue != "All")
                        {
                            sortDS(ddlType.SelectedValue);
                        }
                        else if (ddlType.SelectedValue != "All" && ddlDate.SelectedValue == "All")
                        {
                            sortDS(ddlType.SelectedValue);
                        }
                    }
                }
            }
        }

    }
    protected void btnsearchbyDate_Click(object sender, EventArgs e)
    {

        if (chkSearch.Checked == true)
        {
            ViewState["sort"] = null;
            sort = "DateTime desc";
            //ddlNumber.SelectedIndex = 0;
            bindByDateandEventtype();
        }
    }
    protected void btnsearchbyType_Click(object sender, EventArgs e)
    {
        if (chkSearch.Checked == true)
        {
            ViewState["sort"] = null;
            sort = "DateTime desc";
            sortDS(ddlType.SelectedValue);
        }



    }
    protected void btmSerachbyNumber_Click(object sender, EventArgs e)
    {
        if (chkSearch.Checked == true)
        {
            ViewState["sort"] = null;
            //sort = "DateTime desc";               
            bindNumberCombo();
        }
    }
    protected void btnSearchbyComp_Click(object sender, EventArgs e)
    {

        if (chkSearch.Checked == true)
        {
            if (ddlCompanyname.SelectedItem.Text != "All")
            {

                lblDisplay.Visible = false;
                sort = "";
                getClientid(ddlCompanyname.SelectedItem.Text);
                bindGrid(strClient, "");
                ddlType.SelectedIndex = 0;
                ddlDate.SelectedIndex = 0;
                ddlNumber.SelectedIndex = 0;
                chkSearch.Checked = true;
                lnkSearch.Visible = false;
            }
            else if (ddlCompanyname.SelectedItem.Text == "All")
            {
                lblDisplay.Visible = false;
                sort = "";
                bindCompanyAll();
                bindGrid(strClient, "");
                ddlType.SelectedIndex = 0;
                ddlDate.SelectedIndex = 0;
                ddlNumber.SelectedIndex = 0;
                chkSearch.Checked = true;
                lnkSearch.Visible = false;
            }
        }

    }


    protected void dgCompany_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        dgCompany.PageIndex = e.NewPageIndex;
        bindData();
    }
}
