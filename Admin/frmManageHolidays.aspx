<%@ Page Language="C#" AutoEventWireup="true" Inherits="Admin_frmManageHoliDays"
    CodeFile="frmManageHolidays.aspx.cs" Title="Holiday's list" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Src="Notifications.ascx" TagName="Notifications" TagPrefix="uc4" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<%@ Register Assembly="GMDatePicker" Namespace="GrayMatterSoft" TagPrefix="cc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <script type="text/javascript" language="javascript">


    </script>
<%--<link type="text/css" rel="stylesheet" href="../StyleSheet.css">--%>
    <title>Holiday's list</title>
    <style type="text/css">
        .style1
        {
            height: 24px;
        }
    </style>
</head>
<body style="margin:0px;">
    <form id="form1" runat="server">
    <div id="wrapper">
        <uc2:Admin_Header ID="Admin_Header1" runat="server" />
        <div class="publicloginTable" style="height: 83%; background-color: #f1f1f1;">
            <div style="float: left; width: 47%; height: 100%" class="publicloginTable" id="leftdiv"
                runat="server">
                <table style="height: 295px; width: 864px">
                
                    <tr>
                        <td colspan="6" class="LoginTitle"><h1 style="color:Black">
                             Holiday's list</h1>
                        </td>
                          
                           
                    </tr> 
                    <tr>
                        <td valign="top">
                            <asp:Panel ID="pnlAdmin" runat="server" Visible="False"  GroupingText="Manage holidays">
                                <table width="100%">
                                    <tr>
                                        <td align="left" valign="top" colspan="3" class="style1">
                                            <asp:RadioButton ID="rdbtnNew" runat="server" Text="Add New Holiday"
                                                AutoPostBack="True" Checked="True" OnCheckedChanged="rdbtnNew_CheckedChanged" />
                                        </td>
                                    </tr>
                    <tr>
                        <td align="left" valign="top" colspan="3" class="style1">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Select a date
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <asp:TextBox ID="txtDate" runat="server" Width="289px"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtDate"
                                Format="dd/MM/yyyy" PopupButtonID="txtDate">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Holiday's name
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <asp:TextBox ID="txtHolidayName" runat="server" Width="289px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                        </td>
                    </tr>
                    <tr>
                        <td height="50px">
                            Holiday's Type
                        </td>
                        <td>
                            :
                        </td>
                        <td height="30px">
                            <asp:DropDownList ID="ddlHolidayType" runat="server" Height="25px" Width="145px">
                                <asp:ListItem>-------Select a type------</asp:ListItem>
                                <asp:ListItem Value="1">National</asp:ListItem>
                                <asp:ListItem Value="2">Fixed</asp:ListItem>
                                <asp:ListItem Value="3">Restricted</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                            
                        </td>
                        <td>&nbsp;
                            
                        </td>
                        <td>
                            <asp:Button ID="Button1" runat="server" Text="Add Now" OnClick="Button1_Click" />
                            <asp:Button ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" Text="Update Now"
                                Visible="False" />
                        </td>
                    </tr>
                </table>
                <%-- </ajaxToolkit:UpdatePanelAnimationExtender>--%>
                </asp:Panel> </td>
                <td style="width: 40px">
                </td>
                <td style="width: 372px">
                <div><span>Select a year :</span> <span> <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="True" 
                        onselectedindexchanged="ddlYear_SelectedIndexChanged">
                        </asp:DropDownList></span></div>
                        <br />
                    <div align="right" valign="top">
                       <asp:Label runat="server" ID="lblRecordsdetails" Visible="false"> 
                       </asp:Label>
                        <asp:GridView ID="gvHolidaysDetails" runat="server" AutoGenerateColumns="False" Height="100%"
                            PageSize="20" Style="margin-left: 0px; vertical-align: top" Width="101%" OnRowCommand="gvHolidaysDetails_RowCommand"
                            OnRowDataBound="gvHolidaysDetails_RowDataBound" CellPadding="5">
                            <Columns>
                                <asp:TemplateField HeaderText=" Date" SortExpression="LDate">
                                    <HeaderStyle Width="15%" />
                                    <ItemTemplate>
                                        <%#Eval("LDate")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Holiday name" SortExpression="LName">
                                    <HeaderStyle Width="45%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <%#Eval("LName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Holiday Type" SortExpression="LType">
                                    <HeaderStyle Width="30%" />
                                    <ItemTemplate>
                                        <%#Eval("LType")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Delete" SortExpression="LDate" Visible="false">
                                    <HeaderStyle Width="10%" />
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkBtnDelete" Text="Delete" runat="server" CommandArgument='<%#Eval("LId")%>'
                                            CommandName="Del"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Update" SortExpression="LName" Visible="false">
                                    <HeaderStyle Width="10%" />
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkBtnUpdate" Text="Update" runat="server" CommandArgument='<%#string.Format("{0}|{1}|{2}|{3}",Eval("LId"),Eval("LDate"),Eval("LName"),Eval("LType"))%>'
                                            CommandName="Upd"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div>
                        <asp:Label ID="lblSuccessMsg" runat="server"></asp:Label>
                    </div>
                </td>
                </tr> </table> <span>&nbsp;&nbsp;&nbsp; </span>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
