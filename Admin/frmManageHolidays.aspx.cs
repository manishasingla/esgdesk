using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Globalization;
public partial class Admin_frmManageHoliDays : System.Web.UI.Page
{
    clsEmployee_Logic1 objEmployee = new clsEmployee_Logic1();
    clsLeave_Logic objLeave = new clsLeave_Logic();
    String dtDayOfLeave = "";
    static DateTime dtDayOfLeave1 = System.DateTime.Now;
    static DataView dv = null;
    static string strLeaveType = "";
    static string strPONP = "";
    private string strError = "No records found.";
    static string strCmd = "";
    static string strColText = "";
    static string strId = "";
    static string strCmdArg = "";
    static int iStaticColumnCount = 0;
    static string strDate = "";
    DataSet ds = new DataSet();
    string strLId = "";
    bool blnIsUpdating = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ddlHolidayType.SelectedIndex = -1;
            fillddl();
        }
        else
        {
           
        }

        if (Session["admin"] != null && Session["admin"].ToString() != "")
        {
            if (DatabaseHelper.isAdmin(Session["admin"].ToString()))
            {
                pnlAdmin.Visible = true;
                gvHolidaysDetails.Columns[3].Visible = true;
                gvHolidaysDetails.Columns[4].Visible = true;
                
            }
        }
        else
        {
            Response.Redirect("login.aspx");
            return;
        }
        //if (Session["admin"] != null)
  

        if (!IsPostBack)
        {
            //fillddl();
            fnFillGV(Convert.ToInt32(ddlYear.SelectedItem.Text));
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {

        string strHolidayName = txtHolidayName.Text.Replace("'", "@").Trim();

        if (txtDate.Text != null && txtDate.Text.Trim() != "" && txtHolidayName.Text.Trim() != "" && ddlHolidayType.SelectedIndex!=0)
        //if(CalendarExtender1.SelectedDate
        {
            string strSql = "select count(*) from tbl_Holidays where convert (varchar, LDate, 103)=Convert(varchar," + "'" + txtDate.Text + "'" + ",103)";
            bool blnintIsExist = objLeave.CheckDateInHolidays(strSql);
            if (!blnintIsExist)
            {
                DateTime dt = Convert.ToDateTime(txtDate.Text);
                string strDate1 = dt.ToString("yyyy/MM/dd");

                string strsql = "insert into tbl_Holidays values('" + strDate1 + "','" + strHolidayName + "','"+ddlHolidayType.SelectedItem.ToString()+"')";
                int intIsSuccess = objLeave.setHoliday(strsql);
                if (intIsSuccess >= 1)
                {
                    lblSuccessMsg.Text = "Operation performed successfully.";
                    txtDate.Text = "";
                    ddlHolidayType.SelectedIndex = 0;
                    txtHolidayName.Text = "";

                }
                else
                    lblSuccessMsg.Text = "Please try again , process could not be completed successfully at this moment.";
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "hwa", "alert(' A Holiday is already existing on selected date.');", true);

            }
        }
        else
            ClientScript.RegisterStartupScript(GetType(), "hwa", "alert('Please select a Date , Holiday name and Holiday type.');", true);
         fnFillGV(Convert.ToInt32(ddlYear.SelectedItem.Text));

    }



    public void fnFillGV(int year)
    {
        lblSuccessMsg.Text = "";
        // string strsql = "select convert (varchar,LDate,103 ) as LDate, LName from tbl_Holidays ";
        string strsql = "select * from tbl_Holidays where DATEPART(yy, Ldate) = " + year + " order by LDate Asc";
        DataSet ds = objLeave.getHoliday(strsql);
        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvHolidaysDetails.Visible = true;
                lblRecordsdetails.Visible = false;
                gvHolidaysDetails.DataSource = ds.Tables[0].DefaultView;
                gvHolidaysDetails.DataBind();
            }

            else
            {
                gvHolidaysDetails.Visible = false;
                lblRecordsdetails.Visible = true;
                lblRecordsdetails.Text = "Records are not founded for selected year.";
            }
        }
    }

    protected void gvHolidaysDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToUpper() == "UPD")
        {
            rdbtnNew.Checked = false;
            string sortByAndArrangeBy = (e.CommandArgument).ToString();
            // string strName =
            lblSuccessMsg.Text = "";
            char[] separator = { '|' };
            string[] sortByAndArrangeByArray = sortByAndArrangeBy.Split(separator);
            strLId = sortByAndArrangeByArray[0];
            Session["LId"] = strLId;
            dtDayOfLeave1 = Convert.ToDateTime(sortByAndArrangeByArray[1]);
            //CalendarExtender1.SelectedDate = dtDayOfLeave1;
            // DateTime dt = CalendarExtender1.SelectedDate; //Convert.ToDateTime(CalendarExtender1.DateString);
            //  strDate = CalendarExtender1.SelectedDate.ToString();
            DateTime dt = Convert.ToDateTime(sortByAndArrangeByArray[1]);
            txtDate.Text = dt.ToString("dd/MM/yyyy");
            txtHolidayName.Text = sortByAndArrangeByArray[2].Replace("@", "'");
            string strHolidayType = sortByAndArrangeByArray[3].Replace("@", "'");
            if(strHolidayType=="National")
            ddlHolidayType.SelectedIndex = 1;
            else if(strHolidayType=="Fixed")
                ddlHolidayType.SelectedIndex = 2;
            else if(strHolidayType == "Restricted")
                ddlHolidayType.SelectedIndex = 3;
            btnUpdate.Visible = true;
             fnFillGV(Convert.ToInt32(ddlYear.SelectedItem.Text));
            rdbtnNew.Checked = false;
            blnIsUpdating = true;
            if (blnIsUpdating) 
            //txtDate.AutoPostBack = false;
            Button1.Visible = false;

        }

        else if (e.CommandName.ToUpper() == "DEL")
        {

            string strLDate = (e.CommandArgument).ToString();

            try
            {
                clsLeave_Logic objclsll = new clsLeave_Logic();
                //DateTime dt = Convert.ToDateTime(strLDate);
                //strLDate = dt.ToString("dd/MM/yyyy");
                strLId = e.CommandArgument.ToString();
                string strSql = "Delete from tbl_Holidays where LId="+strLId;//convert(varchar,LDate,103) =" + "convert(varchar,'" + strLDate + "',103)";
                int intNoOfRowDeleted = objclsll.DeleteHoliday(strSql);

                if (intNoOfRowDeleted > 0)
                {

                    // gvHolidaysDetails.DataBind();
                    fnFillGV(Convert.ToInt32(ddlYear.SelectedItem.Text)) ;
                    //GMDDate.DateString = null;
                    txtDate.Text = "";
                    txtHolidayName.Text = "";
                    ddlHolidayType.SelectedIndex = 0;

                }
                else
                {                 
                    lblSuccessMsg.Text = "Operation was not successful please try again.";
                }

            }
            catch (Exception ex)
            {
                lblSuccessMsg.Text = ex.Message;
            }
           

        }
    }

    protected void gvHolidaysDetails_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //if (Session["adm"] == "yes")
        //{
        //    gvHolidaysDetails.Columns[2].Visible = true;
        //    gvHolidaysDetails.Columns[3].Visible = true;
        //}
        //else
        //{
        //    gvHolidaysDetails.Columns[2].Visible = false;
        //    gvHolidaysDetails.Columns[3].Visible = false;
        //}


        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            string str = DataBinder.Eval(e.Row.DataItem, "LName").ToString();
            e.Row.Cells[1].Text = str.Replace("@", "'").Trim();


            DateTime dt = Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "LDate"));
            string strcellText = dt.ToString("dd/MM/yyyy");
            e.Row.Cells[0].Text = strcellText;

        }

    }



    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        if (txtDate.Text != null && txtDate.Text.ToString().Trim() != "" && txtHolidayName.Text.Trim() != "")
        {
            DateTime dt = Convert.ToDateTime(txtDate.Text);
            string strDate1 = dt.ToString("yyyy/MM/dd");

            string strSql = "update tbl_Holidays set LDate ='" + strDate1 + "', LName='" + txtHolidayName.Text.Replace("'", "@") + "', LType='" + ddlHolidayType.SelectedItem.ToString() + "' Where LId=" + Session["LId"].ToString();
            objLeave.updateHolidays(strSql);
             fnFillGV(Convert.ToInt32(ddlYear.SelectedItem.Text));
           
        }
        else
        {
            ClientScript.RegisterStartupScript(GetType(), "hwa", "alert('Please select a Date and Holiday name.');", true);
        }
    }




    protected void txtDate_TextChanged(object sender, EventArgs e)
    {
    //    //if (blnIsUpdating == false)
    //    //{
    //    //    txtHolidayName.Text = "";
    //    //    ddlHolidayType.SelectedIndex = 0;
    //    //    fnFillGV();
    //    //}
    }


  
  
    protected void rdbtnNew_CheckedChanged(object sender, EventArgs e)
    {
        Button1.Visible = true;
        btnUpdate.Visible = false;
        txtDate.Text = "";
        txtHolidayName.Text = "";
        ddlHolidayType.SelectedIndex = 0;
        fnFillGV(Convert.ToInt32(ddlYear.SelectedItem.Text));
    }

    void fillddl()
    {
        try
        {
            int year;
            year = DateTime.Now.Year;
            int YearAtMid = year - 6;
            int j = 1;
            for (int i = 0; i <= 12; i++)
            {
                if (i <= 6)
                {
                    ddlYear.Items.Add(Convert.ToString(YearAtMid + i));

                }
                else
                {
                    ddlYear.Items.Add(Convert.ToString(year + j));
                    j++;
                }
                ddlYear.SelectedIndex = 6;
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, objLeave.strErrorSubject, "frmManageLeave : fillddl Method");

        }
    }


    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        fnFillGV(Convert.ToInt32(ddlYear.SelectedItem.Text));
    }
}




















