﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
//using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
//using System.Xml.Linq;

public partial class Admin_grid : System.Web.UI.Page
{
    string sql = "";
    string qureystring = "";
    DataSet ds;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataSet empname = DatabaseHelper.getEmpsName();
            if (empname != null && empname.Tables[0].Rows.Count > 0)
            {
                ddlemp.DataSource = empname.Tables[0];
                ddlemp.DataTextField = "assigned_to_user";
                ddlemp.DataValueField = "assigned_to_user";
                ddlemp.DataBind();
            }
            if (Request.QueryString.Get("id") != null && Request.QueryString.Get("id") != null)
            {
                qureystring = Request.QueryString.Get("id");
                sql = @"select assigned_to_user,last_updated_user,last_updated_date,ETC,a.task_id from  tasks a join  hours_reporting b ON a.task_id = b.task_id where a.task_id ='" + qureystring + "'";
                ds = DatabaseHelper.getDataset(sql);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    ddlemp.SelectedItem.Text = ds.Tables[0].Rows[0]["assigned_to_user"].ToString();
                    DataGrid1.DataSource = ds.Tables[0];
                    DataGrid1.DataBind();
                }
            }


        }
    }
    protected int getHoursTakenSoFar(string task_id)
    {
        sql = @"select sum(datediff(minute,started_date,finished_date))
                from hours_reporting
                where finished_date is not null and task_id=" + task_id;

        int hrsTaken = 0;
        object objHrsTaken = DatabaseHelper.executeScalar(sql);
        if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
        {
            hrsTaken += (int)objHrsTaken;
        }
        else
        {
            hrsTaken = 0;
        }

        sql = @"select sum(datediff(minute,started_date,getdate()))
                from hours_reporting
                where finished_date is null and task_id=" + task_id;

        objHrsTaken = DatabaseHelper.executeScalar(sql);

        if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
        {
            hrsTaken += (int)objHrsTaken;
        }

        return hrsTaken;
    }

    protected void DataGrid1_ItemDataBound(object sender, DataGridItemEventArgs e)
    {

        if (e.Item.ItemIndex != -1)
        {
            int ETC = Convert.ToInt32(e.Item.Cells[3].Text);

            e.Item.Cells[3].Text = "" + (ETC / 60) + " hrs " + (ETC % 60) + " mins";

            int hrsTaken = getHoursTakenSoFar(e.Item.Cells[6].Text);

            e.Item.Cells[4].Text = "" + (hrsTaken / 60) + " hrs " + (hrsTaken % 60) + " mins";

            int hrsLeft = ETC - hrsTaken;

            if (hrsLeft < 0)
            {
                hrsLeft = -1 * hrsLeft;
                e.Item.Cells[5].Text = "-" + (hrsLeft / 60) + " hrs " + (hrsLeft % 60) + " mins";
            }
            else
            {
                e.Item.Cells[5].Text = "" + (hrsLeft / 60) + " hrs " + (hrsLeft % 60) + " mins";
            }
            //if (e.Item.Cells[7].Text != null && e.Item.Cells[7].Text != "")
            //{
            //    string dt = e.Item.Cells[7].Text;
            //    string[] sp = dt.Split(' ');
            //    e.Item.Cells[7].Text = sp[0];
            //}
        }
    }
    protected void ddldays_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (ddldays.SelectedItem.Text == "today")
        //{
        //    string d = DateTime.Now.ToShortDateString().ToString();

        //    for (int i = 0; i < DataGrid1.Items.Count; i++)
        //    {
        //        if (DataGrid1.Items[i].Cells[7].Text.ToString() == d)
        //        {
        //            Response.Write("Record found");
        //            DataGrid1.Items[i].Style.Add(HtmlTextWriterStyle.Display, "block");
        //        }
        //        else
        //        {
        //            Response.Write("Record not found");
        //            DataGrid1.Items[i].Style.Add(HtmlTextWriterStyle.Display, "none");
        //        }

        //    }
        //}
        //else if (ddldays.SelectedItem.Text == "yesterday")
        //{
        //    bind();
        //    string d = DateTime.Now.ToShortDateString().ToString();
        //    string[] sp = d.Split('/');
        //    int dd = int.Parse(sp[1].ToString()) - 1;
        //    sp[1] = dd.ToString();
        //    d = "";
        //    d += sp[0] + "/";
        //    d += sp[1] + "/";
        //    d += sp[2];
        //    for (int i = 0; i < DataGrid1.Items.Count; i++)
        //    {
        //        if (DataGrid1.Items[i].Cells[7].Text.ToString() == d)
        //        {
        //            Response.Write("Record found");
        //            //DataGrid1.Items[i].Style.Add(HtmlTextWriterStyle.Display, "block");
        //            DataGrid1.Items[i].Visible = true;
        //        }
        //        else
        //        {
        //            Response.Write("Record not found");
        //            //DataGrid1.Items[i].Style.Add(HtmlTextWriterStyle.Display, "none");
        //            DataGrid1.Items[i].Visible = false;
        //        }

        //    }
        //}
        //else if (ddldays.SelectedItem.Text == "Last 7 days")
        //{

        //    string d = DateTime.Now.ToShortDateString().ToString();
        //    string[] sp = d.Split('/');
        //    for (int j = 0; j < 7; j++)
        //    {
        //        int dd = int.Parse(sp[1].ToString()) - 1;
        //        sp[1] = dd.ToString();
        //        d = "";
        //        d += sp[0] + "/";
        //        d += sp[1] + "/";
        //        d += sp[2];
        //        for (int i = 0; i < DataGrid1.Items.Count; i++)
        //        {
        //            if (DataGrid1.Items[i].Cells[7].Text.ToString() == d)
        //            {
        //                Response.Write("Record found");

        //                DataGrid1.Items[i].Visible = true;
        //            }
        //            else
        //            {
        //                //Response.Write("Record not found");

        //                DataGrid1.Items[i].Visible = false;
        //            }

        //        }
        //    }
        //}
        //else
        if (ddldays.SelectedItem.Value == "1" || ddldays.SelectedItem.Value == "2" || ddldays.SelectedItem.Value == "3" || ddldays.SelectedItem.Value == "4")
        {
            string dcount = "";

            if (ddldays.SelectedItem.Value == "1")
            {
                dcount = "0";
            }
            else if (ddldays.SelectedItem.Value == "2")
            {
                dcount = "1";
            }
            else if (ddldays.SelectedItem.Value == "3")
            {
                dcount = "7";
            }
            else
            {
                dcount = "31";
            }
            bind();
            // DataTable dt;
            //string d = DateTime.Now.ToShortDateString().ToString();
            //string[] sp = d.Split('/');
            //for (int j = 0; j < 31; j++)
            //{
            //    int dd = int.Parse(sp[1].ToString()) - 1;
            //    sp[1] = dd.ToString();
            //    d = "";
            //    d += sp[0] + "/";
            //    d += sp[1] + "/";
            //    d += sp[2];
            //    for (int i = 0; i < DataGrid1.Items.Count; i++)
            //    {
            //        if (DataGrid1.Items[i].Cells[7].Text.ToString() == d)
            //        {
            //            Response.Write("Record found");
            //            DataGrid1.Items[i].Style.Add(HtmlTextWriterStyle.BackgroundColor, "red");
            //           // DataGrid1.Items[i].Visible = true;
            //        }
            //        else
            //        {
            //            //   Response.Write("Record not found");
            //            DataGrid1.Items[i].Style.Add(HtmlTextWriterStyle.BackgroundColor, "yellow");
            //           // DataGrid1.Items[i].Visible = false;
            //        }

            //    }
            //}
            double dday = Convert.ToDouble(dcount);
            DateTime sd = DateTime.Parse(System.DateTime.Now.ToShortDateString(), System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat);

            DateTime ed = sd;
            ed = ed.AddDays(-dday);
            DataView dv1 = new DataView(ds.Tables[0]);
            DataTable dt = dv1.ToTable();
            DataView dv = new DataView(dt);
            string ss = sd.ToString("MMMM/dd/yyyy");
            string ee = ed.ToString("MMMM/dd/yyyy");
            if (sd != null && ed != null)
            {
                if (ddldays.SelectedItem.Value == "1")
                {
                    //string ss = sd.ToString("MM/dd/yyyy");
                    dv.RowFilter = "assigned_to_user='" + ddlemp.SelectedItem.Text + "' and Date =#" + ss + "#";
                }
                else if (ddldays.SelectedItem.Value == "2")
                {
                    //string ee = ed.ToString("MM/dd/yyyy");
                    dv.RowFilter = "assigned_to_user='" + ddlemp.SelectedItem.Text + "' and Date =#" + ee + "#";
                }
                else
                {
                    //string ss = sd.ToString("MM/dd/yyyy");
                    //string ee = ed.ToString("MM/dd/yyyy");
                    dv.RowFilter = "assigned_to_user='" + ddlemp.SelectedItem.Text + "' and Date <=#" + ss + "# and Date>=#" + ee + "#";
                }
            }
            if (dv.Count >= 1)
            {
                DataGrid1.DataSource = null;
                DataGrid1.DataBind();
                DataGrid1.DataSource = dv.ToTable();
                DataGrid1.DataBind();
                Response.Write("Record found");
            }
            else
            {
                DataGrid1.DataSource = null;
                DataGrid1.DataBind();
                DataGrid1.DataSource = dv.ToTable();
                DataGrid1.DataBind();
                Response.Write("Record not found");
            }
            //sql = @"Select * from  tasks a join  hours_reporting b ON a.task_id = b.task_id where assigned_to_user='" + ddlemp.SelectedItem.Text + "'and convert(varchar,last_updated_date,101)='" + d + "'";
            //DataSet ds = DatabaseHelper.getDataset(sql);
            //if (ds != null && ds.Tables[0].Rows.Count > 0)
            //{
            //    ddlemp.SelectedItem.Text = ds.Tables[0].Rows[0]["assigned_to_user"].ToString();
            //    DataGrid1.DataSource = ds.Tables[0];
            //    DataGrid1.DataBind();
            //    Response.Write(DataGrid1.Items.Count);
            //}
        }
    }
    protected void ddlemp_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddldays.SelectedIndex = 0;
        bind();

    }

    private void bind()
    {
        if (DataGrid1.Items.Count > 0)
        {
            DataGrid1.DataSource = null;
            DataGrid1.DataBind();
        }
        sql = @"select assigned_to_user,last_updated_user,last_updated_date,ETC,a.task_id from  tasks a join  hours_reporting b ON a.task_id = b.task_id where a.assigned_to_user ='" + ddlemp.SelectedItem.Text + "'";
        ds = DatabaseHelper.getDataset(sql);
        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            ds.Tables[0].Columns.Add("Date");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (ds.Tables[0].Rows[i]["last_updated_date"] != null && ds.Tables[0].Rows[i]["last_updated_date"].ToString().Trim() != "")
                {
                    DateTime dt = Convert.ToDateTime(ds.Tables[0].Rows[i]["last_updated_date"].ToString());
                    string date = dt.ToString("MMMM/dd/yyyy");
                    ds.Tables[0].Rows[i]["Date"] = date;
                    //ds.Tables[0].Rows[i]["Date"] = dt.ToString("MM/dd/yyyy");
                }

            }

            ddlemp.SelectedItem.Text = ds.Tables[0].Rows[0]["assigned_to_user"].ToString();
            DataGrid1.DataSource = ds.Tables[0];
            DataGrid1.DataBind();
            //Response.Write(DataGrid1.Items.Count);
        }
    }
    protected void DataGrid1_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        if (e.SortExpression.ToString() == Session["Column"])
        {
            //Reverse the sort order
            if (Session["Order"] == "ASC")
            {
                lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " DESC";
                Session["Order"] = "DESC";
            }
            else
            {
                lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " ASC";
                Session["Order"] = "ASC";
            }
        }
        else
        {
            //Different column selected, so default to ascending order
            lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " ASC";
            Session["Order"] = "ASC";
        }

        Session["Column"] = e.SortExpression.ToString();
        Session["OrderBy"] = lblOrderBy.Text;
        bind();
    }
}
