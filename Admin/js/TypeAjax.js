﻿
//==================================================*****First Task Start***********========================================
// JScript File
var obj;

function GetDataViaAJAXforDate() {
    if (window.XMLHttpRequest) {
        // Mozilla, Safari, ...
        obj = new XMLHttpRequest();
        if (obj.overrideMimeType) {
            obj.overrideMimeType('text/xml');
        }

    }
    else if (window.ActiveXObject) { // IE
        try {
            obj = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                obj = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e1) {
                obj = null;
            }
        }


    } //end else if

    if (obj == null) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }
    if (obj != null) {
        //====================================================
        obj.onreadystatechange = ProcessResponseDate;
        //============================================================
        var url = window.location.href.split("//")[1].split("/")[0];
        //==========================================================================================================
        obj.open("GET", "http://" + url + "/Admin/ChangeType.aspx?Date=" + document.getElementById("txtDate").value, true);
        //============================================================================================================
        obj.send(null);
    }
    return false;
}

function ProcessResponseDate() {
    if (obj.readyState == 4) {
        if (obj.status == 200) {
            //=======================================================================
            var txtDate = document.getElementById("txtDate");
            //=======================================================================
            var dsRoot = obj.responseXML.documentElement;
            //==========================================================
            var lblIntime = document.getElementById("lblIntime");
            var lblOutTime = document.getElementById("lblOutTime");

            lblIntime = "";
            lblOutTime = "";
            //==========================================================
            var lastintime = dsRoot.getElementsByTagName('lastintime');
            var outtime = dsRoot.getElementsByTagName('updatedate');

            if (document.all) {
                lblIntime = lastintime[0].text;
                lblOutTime = outtime[0].text;
            }
            else {
                lblIntime = lastintime[0].textContent;
                lblOutTime = outtime[0].textContent;
            }

            document.getElementById("lblIntime").innerHTML = lblIntime;
            document.getElementById("lblOutTime").innerHTML = lblOutTime;

            document.getElementById("hdnIntime").value = lblIntime;
            document.getElementById("hdnOutTime").value = lblOutTime;


        }
        else {
            alert("Error retrieving data!");
        }
    }
}

function GetDataViaAJAXA()
 {
  if (window.XMLHttpRequest) {
        // Mozilla, Safari, ...
        obj = new XMLHttpRequest();
        if (obj.overrideMimeType) {
            obj.overrideMimeType('text/xml');
        }
     
    }
    else if (window.ActiveXObject) { // IE
        try {
            obj = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                obj = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e1) {
                obj = null;
            }
        }
        

    } //end else if

    if (obj == null) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }
    if (obj != null) {
    
         //====================================================
        obj.onreadystatechange = ProcessResponseA;
        //============================================================
        var url = window.location.href.split("//")[1].split("/")[0];
        //==========================================================================================================
        obj.open("GET", "http://" + url + "/Admin/ChangeType.aspx?ProjectName=" + encodeURIComponent(document.getElementById("drppro0").value), true);
    
     // obj.open("GET", "http://localhost:52663/httpdocs/Admin/ChangeType.aspx?ProjectName=" + encodeURIComponent(document.getElementById("drppro0").value), true);
      
      
      
       //============================================================================================================
        obj.send(null);
    }
    return false;
}


function ProcessResponseA()
 {
   if (obj.readyState == 4) {
        if (obj.status == 200) {
             //======================================================================
               var ProjectId = document.getElementById("drppro0");
             //=======================================================================
               var dsRoot = obj.responseXML.documentElement;
             //==========================================================
               var TaskId = document.getElementById("drpTask0");
             //var TaskId = TaskControlId
            
           //==========================================================
            var TaskV = dsRoot.getElementsByTagName('task_id');
            var TaskT = dsRoot.getElementsByTagName('task_id');
            var text;
            var text1;
            var listItem;
              
            //============================================================================
           for (var count = TaskId.options.length - 1; count > -1; count--)
             {
                TaskId.options[count] = null;
             }
             //==============================================================================
            
            for (var count = 0; count < TaskV.length; count++)
            {
                text = (TaskV[count].textContent || TaskV[count].innerText || TaskV[count].text);
                text1 = (TaskT[count].textContent || TaskT[count].innerText || TaskT[count].text);
                listItem = new Option(text, text1, false, false);
                //=============================================================
                 TaskId.options[TaskId.options.length] = listItem;
                   //=============================================================
            }

         
        }
        else {
            alert("Error retrieving data!");
        }
    }
}

function GetDataViaAJAXforDescA() {
    //alert("enter");
    
     if (window.XMLHttpRequest) {
        // Mozilla, Safari, ...
        obj = new XMLHttpRequest();
        if (obj.overrideMimeType) {
            obj.overrideMimeType('text/xml');
        }

    }
    else if (window.ActiveXObject) { // IE
        try {
            obj = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                obj = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e1) {
                obj = null;
            }
        }


    } //end else if

    if (obj == null) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }

    if (obj != null) {
   
        //=====================================================
        obj.onreadystatechange = ProcessResponseForDescAA;
        //============================================================
         var url = window.location.href.split("//")[1].split("/")[0];
        //==========================================================================================================
    obj.open("GET", "http://" + url + "/Admin/ChangeType.aspx?TaskId=" + encodeURIComponent(document.getElementById("drpTask0").value), true);
    
    
       //  obj.open("GET", "http://localhost:52663/httpdocs/Admin/ChangeType.aspx?TaskId=" + encodeURIComponent(document.getElementById("drpTask0").value), true);
         
         
        //============================================================================================================
        
        obj.send(null);
    }
    return false;
}


function ProcessResponseForDescAA() {
     if (obj.readyState == 4) {
        if (obj.status == 200)
        {
            var Desc = document.getElementById("txtdesc0");
            var Url = document.getElementById("txtproUrl0");

            var dsRoot = obj.responseXML.documentElement;
            
            var TaskV = dsRoot.getElementsByTagName('Relevant_URL');
            var TaskT = dsRoot.getElementsByTagName('short_desc');
           
           for (var count = 0; count < TaskV.length; count++) 
            {
                text = (TaskV[count].textContent || TaskV[count].innerText || TaskV[count].text);
                //alert(text);
                text1 = (TaskT[count].textContent || TaskT[count].innerText || TaskT[count].text);
                //alert(text1);
                document.getElementById("txtdesc0").innerHTML = text1;
                document.getElementById("txtproUrl0").innerHTML = text;
             }
          }
        else
        {
            alert("Error retrieving data!");
        }
    }
}



//==================================================*****Second Task Start***********========================================

// JScript File
var obj;
function GetDataViaAJAXB() {
    if (window.XMLHttpRequest) {
        // Mozilla, Safari, ...
        obj = new XMLHttpRequest();
        if (obj.overrideMimeType) {
            obj.overrideMimeType('text/xml');
        }

    }
    else if (window.ActiveXObject) { // IE
        try {
            obj = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                obj = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e1) {
                obj = null;
            }
        }


    } //end else if

    if (obj == null) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }
    if (obj != null) {

        //====================================================
        obj.onreadystatechange = ProcessResponseB;
        //============================================================
        var url = window.location.href.split("//")[1].split("/")[0];
        //========================================================================================================== 
        obj.open("GET", "http://" + url + "/Admin/ChangeType.aspx?ProjectName=" + encodeURIComponent(document.getElementById("drppro1").value), true);
        //============================================================================================================
        obj.send(null);
    }
    return false;
}


function ProcessResponseB() {
    if (obj.readyState == 4) {
        if (obj.status == 200) {
            //======================================================================
            var ProjectId = document.getElementById("drppro1");
            //=======================================================================
            var dsRoot = obj.responseXML.documentElement;
            //==========================================================
            var TaskId = document.getElementById("drpTask1");
            //var TaskId = TaskControlId

            //==========================================================
            var TaskV = dsRoot.getElementsByTagName('task_id');
            var TaskT = dsRoot.getElementsByTagName('task_id');
            var text;
            var text1;
            var listItem;

            //============================================================================
            for (var count = TaskId.options.length - 1; count > -1; count--) {
                TaskId.options[count] = null;
            }
            //==============================================================================

            for (var count = 0; count < TaskV.length; count++) {
                text = (TaskV[count].textContent || TaskV[count].innerText || TaskV[count].text);
                text1 = (TaskT[count].textContent || TaskT[count].innerText || TaskT[count].text);
                listItem = new Option(text, text1, false, false);
                //=============================================================
                TaskId.options[TaskId.options.length] = listItem;
                //=============================================================
            }


        }
        else {
            alert("Error retrieving data!");
        }
    }
}

function GetDataViaAJAXforDescB() {
    //alert("enter");

    if (window.XMLHttpRequest) {
        // Mozilla, Safari, ...
        obj = new XMLHttpRequest();
        if (obj.overrideMimeType) {
            obj.overrideMimeType('text/xml');
        }

    }
    else if (window.ActiveXObject) { // IE
        try {
            obj = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                obj = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e1) {
                obj = null;
            }
        }


    } //end else if

    if (obj == null) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }

    if (obj != null) {

        //=====================================================
        obj.onreadystatechange = ProcessResponseForDescBB;
        //============================================================
        var url = window.location.href.split("//")[1].split("/")[0];
        //========================================================================================================== 
        obj.open("GET", "http://" + url + "/Admin/ChangeType.aspx?TaskId=" + encodeURIComponent(document.getElementById("drpTask1").value), true);
        //============================================================================================================

        obj.send(null);
    }
    return false;
}


function ProcessResponseForDescBB() {
    if (obj.readyState == 4) {
        if (obj.status == 200) {
            var Desc = document.getElementById("txtdesc1");
            var Url = document.getElementById("txtproUrl1");

            var dsRoot = obj.responseXML.documentElement;

            var TaskV = dsRoot.getElementsByTagName('Relevant_URL');
            var TaskT = dsRoot.getElementsByTagName('short_desc');

            for (var count = 0; count < TaskV.length; count++) {
                text = (TaskV[count].textContent || TaskV[count].innerText || TaskV[count].text);
                //alert(text);
                text1 = (TaskT[count].textContent || TaskT[count].innerText || TaskT[count].text);
                //alert(text1);
                document.getElementById("txtdesc1").innerHTML = text1;
                document.getElementById("txtproUrl1").innerHTML = text;
            }
        }
        else {
            alert("Error retrieving data!");
        }
    }
}







//==================================================*****Third Task Start***********========================================
// JScript File
var obj;
function GetDataViaAJAXC() {
    if (window.XMLHttpRequest) {
        // Mozilla, Safari, ...
        obj = new XMLHttpRequest();
        if (obj.overrideMimeType) {
            obj.overrideMimeType('text/xml');
        }

    }
    else if (window.ActiveXObject) { // IE
        try {
            obj = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                obj = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e1) {
                obj = null;
            }
        }


    } //end else if

    if (obj == null) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }
    if (obj != null) {

        //====================================================
        obj.onreadystatechange = ProcessResponseC;
        //============================================================
        var url = window.location.href.split("//")[1].split("/")[0];
        //========================================================================================================== 
        obj.open("GET", "http://" + url + "/Admin/ChangeType.aspx?ProjectName=" + encodeURIComponent(document.getElementById("drppro2").value), true);
        //============================================================================================================
        obj.send(null);
    }
    return false;
}


function ProcessResponseC() {
    if (obj.readyState == 4) {
        if (obj.status == 200) {
            //======================================================================
            var ProjectId = document.getElementById("drppro2");
            //=======================================================================
            var dsRoot = obj.responseXML.documentElement;
            //==========================================================
            var TaskId = document.getElementById("drpTask2");
            //var TaskId = TaskControlId

            //==========================================================
            var TaskV = dsRoot.getElementsByTagName('task_id');
            var TaskT = dsRoot.getElementsByTagName('task_id');
            var text;
            var text1;
            var listItem;

            //============================================================================
            for (var count = TaskId.options.length - 1; count > -1; count--) {
                TaskId.options[count] = null;
            }
            //==============================================================================

            for (var count = 0; count < TaskV.length; count++) {
                text = (TaskV[count].textContent || TaskV[count].innerText || TaskV[count].text);
                text1 = (TaskT[count].textContent || TaskT[count].innerText || TaskT[count].text);
                listItem = new Option(text, text1, false, false);
                //=============================================================
                TaskId.options[TaskId.options.length] = listItem;
                //=============================================================
            }


        }
        else {
            alert("Error retrieving data!");
        }
    }
}

function GetDataViaAJAXforDescC() {
    //alert("enter");

    if (window.XMLHttpRequest) {
        // Mozilla, Safari, ...
        obj = new XMLHttpRequest();
        if (obj.overrideMimeType) {
            obj.overrideMimeType('text/xml');
        }

    }
    else if (window.ActiveXObject) { // IE
        try {
            obj = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                obj = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e1) {
                obj = null;
            }
        }


    } //end else if

    if (obj == null) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }

    if (obj != null) {

        //=====================================================
        obj.onreadystatechange = ProcessResponseForDescCC;
        //============================================================
        var url = window.location.href.split("//")[1].split("/")[0];
        //========================================================================================================== 
        obj.open("GET", "http://" + url + "/Admin/ChangeType.aspx?TaskId=" + encodeURIComponent(document.getElementById("drpTask2").value), true);
        //============================================================================================================

        obj.send(null);
    }
    return false;
}


function ProcessResponseForDescCC() {
    if (obj.readyState == 4) {
        if (obj.status == 200) {
            var Desc = document.getElementById("txtdesc2");
            var Url = document.getElementById("txtproUrl2");

            var dsRoot = obj.responseXML.documentElement;

            var TaskV = dsRoot.getElementsByTagName('Relevant_URL');
            var TaskT = dsRoot.getElementsByTagName('short_desc');

            for (var count = 0; count < TaskV.length; count++) {
                text = (TaskV[count].textContent || TaskV[count].innerText || TaskV[count].text);
                //alert(text);
                text1 = (TaskT[count].textContent || TaskT[count].innerText || TaskT[count].text);
                //alert(text1);
                document.getElementById("txtdesc2").innerHTML = text1;
                document.getElementById("txtproUrl2").innerHTML = text;
            }
        }
        else {
            alert("Error retrieving data!");
        }
    }
}








//==================================================*****Four Task Start***********========================================
// JScript File
var obj;
function GetDataViaAJAXD() {
    if (window.XMLHttpRequest) {
        // Mozilla, Safari, ...
        obj = new XMLHttpRequest();
        if (obj.overrideMimeType) {
            obj.overrideMimeType('text/xml');
        }

    }
    else if (window.ActiveXObject) { // IE
        try {
            obj = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                obj = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e1) {
                obj = null;
            }
        }


    } //end else if

    if (obj == null) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }
    if (obj != null) {

        //====================================================
        obj.onreadystatechange = ProcessResponseD;
        //============================================================
        var url = window.location.href.split("//")[1].split("/")[0];
        //========================================================================================================== 
        obj.open("GET", "http://" + url + "/Admin/ChangeType.aspx?ProjectName=" + encodeURIComponent(document.getElementById("drppro3").value), true);
        //============================================================================================================
        obj.send(null);
    }
    return false;
}


function ProcessResponseD() {
    if (obj.readyState == 4) {
        if (obj.status == 200) {
            //======================================================================
            var ProjectId = document.getElementById("drppro3");
            //=======================================================================
            var dsRoot = obj.responseXML.documentElement;
            //==========================================================
            var TaskId = document.getElementById("drpTask3");
            //var TaskId = TaskControlId

            //==========================================================
            var TaskV = dsRoot.getElementsByTagName('task_id');
            var TaskT = dsRoot.getElementsByTagName('task_id');
            var text;
            var text1;
            var listItem;

            //============================================================================
            for (var count = TaskId.options.length - 1; count > -1; count--) {
                TaskId.options[count] = null;
            }
            //==============================================================================

            for (var count = 0; count < TaskV.length; count++) {
                text = (TaskV[count].textContent || TaskV[count].innerText || TaskV[count].text);
                text1 = (TaskT[count].textContent || TaskT[count].innerText || TaskT[count].text);
                listItem = new Option(text, text1, false, false);
                //=============================================================
                TaskId.options[TaskId.options.length] = listItem;
                //=============================================================
            }


        }
        else {
            alert("Error retrieving data!");
        }
    }
}

function GetDataViaAJAXforDescD() {
    //alert("enter");

    if (window.XMLHttpRequest) {
        // Mozilla, Safari, ...
        obj = new XMLHttpRequest();
        if (obj.overrideMimeType) {
            obj.overrideMimeType('text/xml');
        }

    }
    else if (window.ActiveXObject) { // IE
        try {
            obj = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                obj = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e1) {
                obj = null;
            }
        }


    } //end else if

    if (obj == null) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }

    if (obj != null) {

        //=====================================================
        obj.onreadystatechange = ProcessResponseForDescDD;
        //============================================================
        var url = window.location.href.split("//")[1].split("/")[0];
        //========================================================================================================== 
        obj.open("GET", "http://" + url + "/Admin/ChangeType.aspx?TaskId=" + encodeURIComponent(document.getElementById("drpTask3").value), true);
        //============================================================================================================

        obj.send(null);
    }
    return false;
}


function ProcessResponseForDescDD() {
    if (obj.readyState == 4) {
        if (obj.status == 200) {
            var Desc = document.getElementById("txtdesc3");
            var Url = document.getElementById("txtproUrl3");

            var dsRoot = obj.responseXML.documentElement;

            var TaskV = dsRoot.getElementsByTagName('Relevant_URL');
            var TaskT = dsRoot.getElementsByTagName('short_desc');

            for (var count = 0; count < TaskV.length; count++) {
                text = (TaskV[count].textContent || TaskV[count].innerText || TaskV[count].text);
                //alert(text);
                text1 = (TaskT[count].textContent || TaskT[count].innerText || TaskT[count].text);
                //alert(text1);
                document.getElementById("txtdesc3").innerHTML = text1;
                document.getElementById("txtproUrl3").innerHTML = text;
            }
        }
        else {
            alert("Error retrieving data!");
        }
    }
}
//==================================================*****FiFth Task Start***********========================================
// JScript File
var obj;
function GetDataViaAJAXE() {
    if (window.XMLHttpRequest) {
        // Mozilla, Safari, ...
        obj = new XMLHttpRequest();
        if (obj.overrideMimeType) {
            obj.overrideMimeType('text/xml');
        }

    }
    else if (window.ActiveXObject) { // IE
        try {
            obj = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                obj = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e1) {
                obj = null;
            }
        }


    } //end else if

    if (obj == null) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }
    if (obj != null) {

        //====================================================
        obj.onreadystatechange = ProcessResponseE;
        //============================================================
        var url = window.location.href.split("//")[1].split("/")[0];
        //========================================================================================================== 
        obj.open("GET", "http://" + url + "/Admin/ChangeType.aspx?ProjectName=" + encodeURIComponent(document.getElementById("drppro4").value), true);
        //============================================================================================================
        obj.send(null);
    }
    return false;
}


function ProcessResponseE() {
    if (obj.readyState == 4) {
        if (obj.status == 200) {
            //======================================================================
            var ProjectId = document.getElementById("drppro4");
            //=======================================================================
            var dsRoot = obj.responseXML.documentElement;
            //==========================================================
            var TaskId = document.getElementById("drpTask4");
            //var TaskId = TaskControlId

            //==========================================================
            var TaskV = dsRoot.getElementsByTagName('task_id');
            var TaskT = dsRoot.getElementsByTagName('task_id');
            var text;
            var text1;
            var listItem;

            //============================================================================
            for (var count = TaskId.options.length - 1; count > -1; count--) {
                TaskId.options[count] = null;
            }
            //==============================================================================

            for (var count = 0; count < TaskV.length; count++) {
                text = (TaskV[count].textContent || TaskV[count].innerText || TaskV[count].text);
                text1 = (TaskT[count].textContent || TaskT[count].innerText || TaskT[count].text);
                listItem = new Option(text, text1, false, false);
                //=============================================================
                TaskId.options[TaskId.options.length] = listItem;
                //=============================================================
            }


        }
        else {
            alert("Error retrieving data!");
        }
    }
}

function GetDataViaAJAXforDescE() {
    //alert("enter");

    if (window.XMLHttpRequest) {
        // Mozilla, Safari, ...
        obj = new XMLHttpRequest();
        if (obj.overrideMimeType) {
            obj.overrideMimeType('text/xml');
        }

    }
    else if (window.ActiveXObject) { // IE
        try {
            obj = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                obj = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e1) {
                obj = null;
            }
        }


    } //end else if

    if (obj == null) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }

    if (obj != null) {

        //=====================================================
        obj.onreadystatechange = ProcessResponseForDescEE;
        //============================================================
        var url = window.location.href.split("//")[1].split("/")[0];
        //========================================================================================================== 
        obj.open("GET", "http://" + url + "/Admin/ChangeType.aspx?TaskId=" + encodeURIComponent(document.getElementById("drpTask4").value), true);
        //============================================================================================================

        obj.send(null);
    }
    return false;
}


function ProcessResponseForDescEE() {
    if (obj.readyState == 4) {
        if (obj.status == 200) {
            var Desc = document.getElementById("txtdesc4");
            var Url = document.getElementById("txtproUrl4");

            var dsRoot = obj.responseXML.documentElement;

            var TaskV = dsRoot.getElementsByTagName('Relevant_URL');
            var TaskT = dsRoot.getElementsByTagName('short_desc');

            for (var count = 0; count < TaskV.length; count++) {
                text = (TaskV[count].textContent || TaskV[count].innerText || TaskV[count].text);
                //alert(text);
                text1 = (TaskT[count].textContent || TaskT[count].innerText || TaskT[count].text);
                //alert(text1);
                document.getElementById("txtdesc4").innerHTML = text1;
                document.getElementById("txtproUrl4").innerHTML = text;
            }
        }
        else {
            alert("Error retrieving data!");
        }
    }
}
//==================================================*****Sixth Task Start***********========================================
// JScript File
var obj;
function GetDataViaAJAXF() {
   
    if (window.XMLHttpRequest) {
        // Mozilla, Safari, ...
        obj = new XMLHttpRequest();
        if (obj.overrideMimeType) {
            obj.overrideMimeType('text/xml');
        }

    }
    else if (window.ActiveXObject) { // IE
        try {
            obj = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                obj = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e1) {
                obj = null;
            }
        }


    } //end else if

    if (obj == null) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }
    if (obj != null) {

        //====================================================
        obj.onreadystatechange = ProcessResponseF;
        //============================================================
        var url = window.location.href.split("//")[1].split("/")[0];
        //========================================================================================================== 
        obj.open("GET", "http://" + url + "/Admin/ChangeType.aspx?ProjectName=" + encodeURIComponent(document.getElementById("drppro5").value), true);
        //============================================================================================================
        obj.send(null);
    }
    return false;
}


function ProcessResponseF() {
    if (obj.readyState == 4) {
        if (obj.status == 200) {
            //======================================================================
            var ProjectId = document.getElementById("drppro5");
            //=======================================================================
            var dsRoot = obj.responseXML.documentElement;
            //==========================================================
            var TaskId = document.getElementById("drpTask5");
            //var TaskId = TaskControlId

            //==========================================================
            var TaskV = dsRoot.getElementsByTagName('task_id');
            var TaskT = dsRoot.getElementsByTagName('task_id');
            var text;
            var text1;
            var listItem;

            //============================================================================
            for (var count = TaskId.options.length - 1; count > -1; count--) {
                TaskId.options[count] = null;
            }
            //==============================================================================

            for (var count = 0; count < TaskV.length; count++) {
                text = (TaskV[count].textContent || TaskV[count].innerText || TaskV[count].text);
                text1 = (TaskT[count].textContent || TaskT[count].innerText || TaskT[count].text);
                listItem = new Option(text, text1, false, false);
                //=============================================================
                TaskId.options[TaskId.options.length] = listItem;
                //=============================================================
            }


        }
        else {
            alert("Error retrieving data!");
        }
    }
}

function GetDataViaAJAXforDescF() {
    

    if (window.XMLHttpRequest) {
        // Mozilla, Safari, ...
        obj = new XMLHttpRequest();
        if (obj.overrideMimeType) {
            obj.overrideMimeType('text/xml');
        }

    }
    else if (window.ActiveXObject) { // IE
        try {
            obj = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                obj = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e1) {
                obj = null;
            }
        }


    } //end else if

    if (obj == null) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }

    if (obj != null) {

        //=====================================================
        obj.onreadystatechange = ProcessResponseForDescFF;
        //============================================================
        var url = window.location.href.split("//")[1].split("/")[0];
        //========================================================================================================== 
        obj.open("GET", "http://" + url + "/Admin/ChangeType.aspx?TaskId=" + encodeURIComponent(document.getElementById("drpTask5").value), true);
        //============================================================================================================

        obj.send(null);
    }
    return false;
}


function ProcessResponseForDescFF() {

    if (obj.readyState == 4) {
        if (obj.status == 200) {
            var Desc = document.getElementById("txtdesc5");
            var Url = document.getElementById("txtproUrl5");

            var dsRoot = obj.responseXML.documentElement;

            var TaskV = dsRoot.getElementsByTagName('Relevant_URL');
            var TaskT = dsRoot.getElementsByTagName('short_desc');

            for (var count = 0; count < TaskV.length; count++) {
                text = (TaskV[count].textContent || TaskV[count].innerText || TaskV[count].text);
                //alert(text);
                text1 = (TaskT[count].textContent || TaskT[count].innerText || TaskT[count].text);
                //alert(text1);
                document.getElementById("txtdesc5").innerHTML = text1;
                document.getElementById("txtproUrl5").innerHTML = text;
            }
        }
        else {
            alert("Error retrieving data!");
        }
    }
}
//==================================================*****Seven Task Start***********========================================
// JScript File
var obj;
function GetDataViaAJAXG() {
    if (window.XMLHttpRequest) {
        // Mozilla, Safari, ...
        obj = new XMLHttpRequest();
        if (obj.overrideMimeType) {
            obj.overrideMimeType('text/xml');
        }

    }
    else if (window.ActiveXObject) { // IE
        try {
            obj = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                obj = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e1) {
                obj = null;
            }
        }


    } //end else if

    if (obj == null) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }
    if (obj != null) {

        //====================================================
        obj.onreadystatechange = ProcessResponseG;
        //============================================================
        var url = window.location.href.split("//")[1].split("/")[0];
        //========================================================================================================== 
        obj.open("GET", "http://" + url + "/Admin/ChangeType.aspx?ProjectName=" + encodeURIComponent(document.getElementById("drppro6").value), true);
        //============================================================================================================
        obj.send(null);
    }
    return false;
}


function ProcessResponseG() {
    if (obj.readyState == 4) {
        if (obj.status == 200) {
            //======================================================================
            var ProjectId = document.getElementById("drppro6");
            //=======================================================================
            var dsRoot = obj.responseXML.documentElement;
            //==========================================================
            var TaskId = document.getElementById("drpTask6");
            //var TaskId = TaskControlId

            //==========================================================
            var TaskV = dsRoot.getElementsByTagName('task_id');
            var TaskT = dsRoot.getElementsByTagName('task_id');
            var text;
            var text1;
            var listItem;

            //============================================================================
            for (var count = TaskId.options.length - 1; count > -1; count--) {
                TaskId.options[count] = null;
            }
            //==============================================================================

            for (var count = 0; count < TaskV.length; count++) {
                text = (TaskV[count].textContent || TaskV[count].innerText || TaskV[count].text);
                text1 = (TaskT[count].textContent || TaskT[count].innerText || TaskT[count].text);
                listItem = new Option(text, text1, false, false);
                //=============================================================
                TaskId.options[TaskId.options.length] = listItem;
                //=============================================================
            }


        }
        else {
            alert("Error retrieving data!");
        }
    }
}

function GetDataViaAJAXforDescG() {
    //alert("enter");

    if (window.XMLHttpRequest) {
        // Mozilla, Safari, ...
        obj = new XMLHttpRequest();
        if (obj.overrideMimeType) {
            obj.overrideMimeType('text/xml');
        }

    }
    else if (window.ActiveXObject) { // IE
        try {
            obj = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                obj = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e1) {
                obj = null;
            }
        }


    } //end else if

    if (obj == null) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }

    if (obj != null) {

        //=====================================================
        obj.onreadystatechange = ProcessResponseForDescGG;
        //============================================================
        var url = window.location.href.split("//")[1].split("/")[0];
        //========================================================================================================== 
        obj.open("GET", "http://" + url + "/Admin/ChangeType.aspx?TaskId=" + encodeURIComponent(document.getElementById("drpTask6").value), true);
        //============================================================================================================

        obj.send(null);
    }
    return false;
}


function ProcessResponseForDescGG() {
    if (obj.readyState == 4) {
        if (obj.status == 200) {
            var Desc = document.getElementById("txtdesc6");
            var Url = document.getElementById("txtproUrl6");

            var dsRoot = obj.responseXML.documentElement;

            var TaskV = dsRoot.getElementsByTagName('Relevant_URL');
            var TaskT = dsRoot.getElementsByTagName('short_desc');

            for (var count = 0; count < TaskV.length; count++) {
                text = (TaskV[count].textContent || TaskV[count].innerText || TaskV[count].text);
                //alert(text);
                text1 = (TaskT[count].textContent || TaskT[count].innerText || TaskT[count].text);
                //alert(text1);
                document.getElementById("txtdesc6").innerHTML = text1;
                document.getElementById("txtproUrl6").innerHTML = text;
            }
        }
        else {
            alert("Error retrieving data!");
        }
    }
}
//==================================================*****Eight Task Start***********========================================
// JScript File
var obj;
function GetDataViaAJAXH() {
    if (window.XMLHttpRequest) {
        // Mozilla, Safari, ...
        obj = new XMLHttpRequest();
        if (obj.overrideMimeType) {
            obj.overrideMimeType('text/xml');
        }

    }
    else if (window.ActiveXObject) { // IE
        try {
            obj = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                obj = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e1) {
                obj = null;
            }
        }


    } //end else if

    if (obj == null) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }
    if (obj != null) {

        //====================================================
        obj.onreadystatechange = ProcessResponseH;
        //============================================================
        var url = window.location.href.split("//")[1].split("/")[0];
        //========================================================================================================== 
        obj.open("GET", "http://" + url + "/Admin/ChangeType.aspx?ProjectName=" + encodeURIComponent(document.getElementById("drppro7").value), true);
        //============================================================================================================
        obj.send(null);
    }
    return false;
}


function ProcessResponseH() {
    if (obj.readyState == 4) {
        if (obj.status == 200) {
            //======================================================================
            var ProjectId = document.getElementById("drppro7");
            //=======================================================================
            var dsRoot = obj.responseXML.documentElement;
            //==========================================================
            var TaskId = document.getElementById("drpTask7");
            //var TaskId = TaskControlId

            //==========================================================
            var TaskV = dsRoot.getElementsByTagName('task_id');
            var TaskT = dsRoot.getElementsByTagName('task_id');
            var text;
            var text1;
            var listItem;

            //============================================================================
            for (var count = TaskId.options.length - 1; count > -1; count--) {
                TaskId.options[count] = null;
            }
            //==============================================================================

            for (var count = 0; count < TaskV.length; count++) {
                text = (TaskV[count].textContent || TaskV[count].innerText || TaskV[count].text);
                text1 = (TaskT[count].textContent || TaskT[count].innerText || TaskT[count].text);
                listItem = new Option(text, text1, false, false);
                //=============================================================
                TaskId.options[TaskId.options.length] = listItem;
                //=============================================================
            }


        }
        else {
            alert("Error retrieving data!");
        }
    }
}

function GetDataViaAJAXforDescH() {
    //alert("enter");

    if (window.XMLHttpRequest) {
        // Mozilla, Safari, ...
        obj = new XMLHttpRequest();
        if (obj.overrideMimeType) {
            obj.overrideMimeType('text/xml');
        }

    }
    else if (window.ActiveXObject) { // IE
        try {
            obj = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                obj = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e1) {
                obj = null;
            }
        }


    } //end else if

    if (obj == null) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }

    if (obj != null) {

        //=====================================================
        obj.onreadystatechange = ProcessResponseForDescHH;
        //============================================================
        var url = window.location.href.split("//")[1].split("/")[0];
        //========================================================================================================== 
        obj.open("GET", "http://" + url + "/Admin/ChangeType.aspx?TaskId=" + encodeURIComponent(document.getElementById("drpTask7").value), true);
        //============================================================================================================

        obj.send(null);
    }
    return false;
}


function ProcessResponseForDescHH() {
    if (obj.readyState == 4) {
        if (obj.status == 200) {
            var Desc = document.getElementById("txtdesc7");
            var Url = document.getElementById("txtproUrl7");

            var dsRoot = obj.responseXML.documentElement;

            var TaskV = dsRoot.getElementsByTagName('Relevant_URL');
            var TaskT = dsRoot.getElementsByTagName('short_desc');

            for (var count = 0; count < TaskV.length; count++) {
                text = (TaskV[count].textContent || TaskV[count].innerText || TaskV[count].text);
                //alert(text);
                text1 = (TaskT[count].textContent || TaskT[count].innerText || TaskT[count].text);
                //alert(text1);
                document.getElementById("txtdesc7").innerHTML = text1;
                document.getElementById("txtproUrl7").innerHTML = text;
            }
        }
        else {
            alert("Error retrieving data!");
        }
    }
}
//==================================================*****Nine Task Start***********========================================
// JScript File
var obj;
function GetDataViaAJAXI() {
    if (window.XMLHttpRequest) {
        // Mozilla, Safari, ...
        obj = new XMLHttpRequest();
        if (obj.overrideMimeType) {
            obj.overrideMimeType('text/xml');
        }

    }
    else if (window.ActiveXObject) { // IE
        try {
            obj = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                obj = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e1) {
                obj = null;
            }
        }


    } //end else if

    if (obj == null) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }
    if (obj != null) {

        //====================================================
        obj.onreadystatechange = ProcessResponseI;
        //============================================================
        var url = window.location.href.split("//")[1].split("/")[0];
        //========================================================================================================== 
        obj.open("GET", "http://" + url + "/Admin/ChangeType.aspx?ProjectName=" + encodeURIComponent(document.getElementById("drppro8").value), true);
        //============================================================================================================
        obj.send(null);
    }
    return false;
}


function ProcessResponseI() {
    if (obj.readyState == 4) {
        if (obj.status == 200) {
            //======================================================================
            var ProjectId = document.getElementById("drppro8");
            //=======================================================================
            var dsRoot = obj.responseXML.documentElement;
            //==========================================================
            var TaskId = document.getElementById("drpTask8");
            //var TaskId = TaskControlId

            //==========================================================
            var TaskV = dsRoot.getElementsByTagName('task_id');
            var TaskT = dsRoot.getElementsByTagName('task_id');
            var text;
            var text1;
            var listItem;

            //============================================================================
            for (var count = TaskId.options.length - 1; count > -1; count--) {
                TaskId.options[count] = null;
            }
            //==============================================================================

            for (var count = 0; count < TaskV.length; count++) {
                text = (TaskV[count].textContent || TaskV[count].innerText || TaskV[count].text);
                text1 = (TaskT[count].textContent || TaskT[count].innerText || TaskT[count].text);
                listItem = new Option(text, text1, false, false);
                //=============================================================
                TaskId.options[TaskId.options.length] = listItem;
                //=============================================================
            }


        }
        else {
            alert("Error retrieving data!");
        }
    }
}

function GetDataViaAJAXforDescI() {
    //alert("enter");

    if (window.XMLHttpRequest) {
        // Mozilla, Safari, ...
        obj = new XMLHttpRequest();
        if (obj.overrideMimeType) {
            obj.overrideMimeType('text/xml');
        }

    }
    else if (window.ActiveXObject) { // IE
        try {
            obj = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                obj = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e1) {
                obj = null;
            }
        }


    } //end else if

    if (obj == null) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }

    if (obj != null) {

        //=====================================================
        obj.onreadystatechange = ProcessResponseForDescII;
        //============================================================
        var url = window.location.href.split("//")[1].split("/")[0];
        //========================================================================================================== 
        obj.open("GET", "http://" + url + "/Admin/ChangeType.aspx?TaskId=" + encodeURIComponent(document.getElementById("drpTask8").value), true);
        //============================================================================================================

        obj.send(null);
    }
    return false;
}


function ProcessResponseForDescII() {
    if (obj.readyState == 4) {
        if (obj.status == 200) {
            var Desc = document.getElementById("txtdesc8");
            var Url = document.getElementById("txtproUrl8");

            var dsRoot = obj.responseXML.documentElement;

            var TaskV = dsRoot.getElementsByTagName('Relevant_URL');
            var TaskT = dsRoot.getElementsByTagName('short_desc');

            for (var count = 0; count < TaskV.length; count++) {
                text = (TaskV[count].textContent || TaskV[count].innerText || TaskV[count].text);
                //alert(text);
                text1 = (TaskT[count].textContent || TaskT[count].innerText || TaskT[count].text);
                //alert(text1);
                document.getElementById("txtdesc8").innerHTML = text1;
                document.getElementById("txtproUrl8").innerHTML = text;
            }
        }
        else {
            alert("Error retrieving data!");
        }
    }
}
//==================================================*****Ten Task Start***********========================================

// JScript File
var obj;
function GetDataViaAJAXJ() {
    if (window.XMLHttpRequest) {
        // Mozilla, Safari, ...
        obj = new XMLHttpRequest();
        if (obj.overrideMimeType) {
            obj.overrideMimeType('text/xml');
        }

    }
    else if (window.ActiveXObject) { // IE
        try {
            obj = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                obj = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e1) {
                obj = null;
            }
        }


    } //end else if

    if (obj == null) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }
    if (obj != null) {

        //====================================================
        obj.onreadystatechange = ProcessResponseJ;
        //============================================================
        var url = window.location.href.split("//")[1].split("/")[0];
        //========================================================================================================== 
        obj.open("GET", "http://" + url + "/Admin/ChangeType.aspx?ProjectName=" + encodeURIComponent(document.getElementById("drppro9").value), true);
        //============================================================================================================
        obj.send(null);
    }
    return false;
}


function ProcessResponseJ() {
    if (obj.readyState == 4) {
        if (obj.status == 200) {
            //======================================================================
            var ProjectId = document.getElementById("drppro9");
            //=======================================================================
            var dsRoot = obj.responseXML.documentElement;
            //==========================================================
            var TaskId = document.getElementById("drpTask9");
            //var TaskId = TaskControlId

            //==========================================================
            var TaskV = dsRoot.getElementsByTagName('task_id');
            var TaskT = dsRoot.getElementsByTagName('task_id');
            var text;
            var text1;
            var listItem;

            //============================================================================
            for (var count = TaskId.options.length - 1; count > -1; count--) {
                TaskId.options[count] = null;
            }
            //==============================================================================

            for (var count = 0; count < TaskV.length; count++) {
                text = (TaskV[count].textContent || TaskV[count].innerText || TaskV[count].text);
                text1 = (TaskT[count].textContent || TaskT[count].innerText || TaskT[count].text);
                listItem = new Option(text, text1, false, false);
                //=============================================================
                TaskId.options[TaskId.options.length] = listItem;
                //=============================================================
            }


        }
        else {
            alert("Error retrieving data!");
        }
    }
}

function GetDataViaAJAXforDescJ() {
    //alert("enter");

    if (window.XMLHttpRequest) {
        // Mozilla, Safari, ...
        obj = new XMLHttpRequest();
        if (obj.overrideMimeType) {
            obj.overrideMimeType('text/xml');
        }

    }
    else if (window.ActiveXObject) { // IE
        try {
            obj = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                obj = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e1) {
                obj = null;
            }
        }


    } //end else if

    if (obj == null) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }

    if (obj != null) {

        //=====================================================
        obj.onreadystatechange = ProcessResponseForDescJJ;
        //============================================================
        var url = window.location.href.split("//")[1].split("/")[0];
        //========================================================================================================== 
        obj.open("GET", "http://" + url + "/Admin/ChangeType.aspx?TaskId=" + encodeURIComponent(document.getElementById("drpTask9").value), true);
        //============================================================================================================

        obj.send(null);
    }
    return false;
}


function ProcessResponseForDescJJ() {
    if (obj.readyState == 4) {
        if (obj.status == 200) {
            var Desc = document.getElementById("txtdesc9");
            var Url = document.getElementById("txtproUrl9");

            var dsRoot = obj.responseXML.documentElement;

            var TaskV = dsRoot.getElementsByTagName('Relevant_URL');
            var TaskT = dsRoot.getElementsByTagName('short_desc');

            for (var count = 0; count < TaskV.length; count++) {
                text = (TaskV[count].textContent || TaskV[count].innerText || TaskV[count].text);
                //alert(text);
                text1 = (TaskT[count].textContent || TaskT[count].innerText || TaskT[count].text);
                //alert(text1);
                document.getElementById("txtdesc9").innerHTML = text1;
                document.getElementById("txtproUrl9").innerHTML = text;
            }
        }
        else {
            alert("Error retrieving data!");
        }
    }
}

//==================================================*****Eleven Task Start***********========================================

// JScript File
var obj;
function GetDataViaAJAXK() {
    if (window.XMLHttpRequest) {
        // Mozilla, Safari, ...
        obj = new XMLHttpRequest();
        if (obj.overrideMimeType) {
            obj.overrideMimeType('text/xml');
        }

    }
    else if (window.ActiveXObject) { // IE
        try {
            obj = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                obj = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e1) {
                obj = null;
            }
        }


    } //end else if

    if (obj == null) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }
    if (obj != null) {

        //====================================================
        obj.onreadystatechange = ProcessResponseK;
        //============================================================
        var url = window.location.href.split("//")[1].split("/")[0];
        //==========================================================================================================
        obj.open("GET", "http://" + url + "/Admin/ChangeType.aspx?ProjectName=" + encodeURIComponent(document.getElementById("drpNxtpro").value), true);
        //============================================================================================================
        obj.send(null);
    }
    return false;
}


function ProcessResponseK() {
    if (obj.readyState == 4) {
        if (obj.status == 200) {
            //======================================================================
            var ProjectId = document.getElementById("drpNxtpro");
            //=======================================================================
            var dsRoot = obj.responseXML.documentElement;
            //==========================================================
            var TaskId = document.getElementById("drpNxtTask");
            //var TaskId = TaskControlId

            //==========================================================
            var TaskV = dsRoot.getElementsByTagName('task_id');
            var TaskT = dsRoot.getElementsByTagName('task_id');
            var text;
            var text1;
            var listItem;

            //============================================================================
            for (var count = TaskId.options.length - 1; count > -1; count--) {
                TaskId.options[count] = null;
            }
            //==============================================================================

            for (var count = 0; count < TaskV.length; count++) {
                text = (TaskV[count].textContent || TaskV[count].innerText || TaskV[count].text);
                text1 = (TaskT[count].textContent || TaskT[count].innerText || TaskT[count].text);
                listItem = new Option(text, text1, false, false);
                //=============================================================
                TaskId.options[TaskId.options.length] = listItem;
                //=============================================================
            }


        }
        else {
            alert("Error retrieving data!");
        }
    }
}

function GetDataViaAJAXforDescK() {
    
    if (window.XMLHttpRequest) {
        // Mozilla, Safari, ...
        obj = new XMLHttpRequest();
        if (obj.overrideMimeType) {
            obj.overrideMimeType('text/xml');
        }

    }
    else if (window.ActiveXObject) { // IE
        try {
            obj = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                obj = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e1) {
                obj = null;
            }
        }


    } //end else if

    if (obj == null) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }

    if (obj != null) {

        //=====================================================
        obj.onreadystatechange = ProcessResponseForDescKK;
        //============================================================
        var url = window.location.href.split("//")[1].split("/")[0];
        //==========================================================================================================
        obj.open("GET", "http://" + url + "/Admin/ChangeType.aspx?TaskId=" + encodeURIComponent(document.getElementById("drpNxtTask").value), true);
        //============================================================================================================

        obj.send(null);
    }
    return false;
}


function ProcessResponseForDescKK() {
    if (obj.readyState == 4) {
        if (obj.status == 200) {
            var Desc = document.getElementById("TxtNextDesc");
            var Url = document.getElementById("txtProjecturl");

            var dsRoot = obj.responseXML.documentElement;

            var TaskV = dsRoot.getElementsByTagName('Relevant_URL');
            var TaskT = dsRoot.getElementsByTagName('short_desc');

            for (var count = 0; count < TaskV.length; count++) {
                text = (TaskV[count].textContent || TaskV[count].innerText || TaskV[count].text);
                //alert(text);
                text1 = (TaskT[count].textContent || TaskT[count].innerText || TaskT[count].text);
                //alert(text1);
                document.getElementById("TxtNextDesc").innerHTML = text1;
                document.getElementById("txtProjecturl").innerHTML = text;
            }
        }
        else {
            alert("Error retrieving data!");
        }
    }
}



