<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="Admin_login" %>

<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%--<script runat="server">

    protected void btnLogin_Click(object sender, EventArgs e)
    {

    }
</script>--%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title> <%= ConfigurationManager.AppSettings["CompanyName"].ToString()%></title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        //<![CDATA[
        function checkUrl() {
            try {
                //   alert(document.URL);
                if (document.URL.indexOf("http://esgdesk.com") !== -1) //indexOf returns the position of the string in the other string. If not found, it will return -1.
                // if (document.URL.contains("http://data"))
                    window.location.href = correctUrl();
            }
            catch (e)
            { alert("Error: " + e); }
        }

        function correctUrl() {
            //   alert("correcturl");
            return (document.URL.replace("http://esgdesk.com", "http://www.esgdesk.com"));
            // return (document.URL.replace("http://localhost:49349/estatecrmwebservice1", "http://www.data.estatecrm.co.uk"));
        }
        
    </script>

</head>
<body>
    <form id="form1" runat="server" defaultbutton="btnLogin">
    <div id="wrapper">
        <uc2:Admin_Header ID="Admin_Header1" runat="server"></uc2:Admin_Header>
        <div id="Content">
            <div id="pageTitle">
                &nbsp;</div>
            <div style="text-align: left">
                <table>
                    <tr>
                        <td align="left" valign="top">
                            <table style="width: 420px">
                                <tr>
                                    <td align="left" class="titleText" valign="top" style="width: 424px; display: none;">
                                        Support login
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 424px">
                                        <table width="420" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="left" valign="top">
                                                    <img height="16" src="../images/loginBox_top.png" width="420" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="background: url(../images/loginBox_mid.png) repeat-y left top;">
                                                    <div style="text-align: center">
                                                        <table align="center" cellpadding="8" cellspacing="0" style="width: 90%">
                                                            <tr>
                                                                <td align="left" class="whitetext1" style="width: 96px" valign="top">
                                                                    Username<span style="color: red">*</span>
                                                                </td>
                                                                <td align="right" valign="top" style="margin-left: 80px">
                                                                    <asp:TextBox ID="txtUserName" runat="server" Width="220px" TabIndex="1"></asp:TextBox><asp:RequiredFieldValidator
                                                                        ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtUserName" Display="Dynamic"
                                                                        ErrorMessage="Please enter username.">*</asp:RequiredFieldValidator>
                                                                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" TargetControlID="RequiredFieldValidator2">
                                                                    </cc1:ValidatorCalloutExtender>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" class="whitetext1" style="width: 96px;" valign="top">
                                                                    Password<span style="color: red">*</span>
                                                                </td>
                                                                <td align="right" valign="top">
                                                                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" Width="220px" 
                                                                        TabIndex="2"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtPassword"
                                                                        Display="Dynamic" ErrorMessage="Please enter password.">*</asp:RequiredFieldValidator>
                                                                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server" TargetControlID="RequiredFieldValidator3">
                                                                    </cc1:ValidatorCalloutExtender>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100px">
                                                    <img height="16" src="../images/loginBox_btm.png" width="420" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top">
                                        <hr />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 424px">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 424px">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="left" style="height: 38px;" valign="top">
                                                    <asp:CheckBox ID="chkSaveDetails" runat="server" Checked="True" TabIndex="2" />
                                                </td>
                                                <td align="left" style="height: 38px" valign="top">
                                                    Save details (check this if you want to be able to go straight into the change request
                                                    site without having to login. You will need to have cookies enabled in your browser.)
                                                </td>
                                            </tr>
                                        </table>
                                        <table border="0" cellpadding="0" cellspacing="0" width="420">
                                            <tr>
                                                <td style="height: 24px">
                                                </td>
                                                <td align="right" style="height: 24px">
                                                    <asp:Button ID="btnLogin" runat="server" OnClick="btnLogin_Click" Text="Login" 
                                                        CssClass="blueBtns" TabIndex="3" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 424px">
                                        <div id="divMessage" runat="server" style="color: red" visible="true">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <uc1:footer ID="Footer1" runat="server" />
    </form>
</body>
</html>
