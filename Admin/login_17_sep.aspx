<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login_17_sep.aspx.cs" Inherits="Admin_login_sev" %>
<%--
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>--%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%--<script runat="server">

    protected void btnLogin_Click(object sender, EventArgs e)
    {

    }
</script>--%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title> <%--<%= ConfigurationManager.AppSettings["CompanyName"].ToString()%>--%></title>
  <%--  <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />--%>
    <link href="CSS/stylesheet1.css" rel="Stylesheet" type="text/css" />
    <script type="text/javascript">
        //<![CDATA[
        function checkUrl() {
            try {
                //   alert(document.URL);
                if (document.URL.indexOf("http://esgdesk.com") !== -1) //indexOf returns the position of the string in the other string. If not found, it will return -1.
                // if (document.URL.contains("http://data"))
                    window.location.href = correctUrl();
            }
            catch (e)
            { alert("Error: " + e); }
        }

        function correctUrl() {
            //   alert("correcturl");
            return (document.URL.replace("http://esgdesk.com", "http://www.esgdesk.com"));
            // return (document.URL.replace("http://localhost:49349/estatecrmwebservice1", "http://www.data.estatecrm.co.uk"));
        }
        
    </script>

</head>
<body>
    <form id="form1" runat="server" defaultbutton="btnLogin">
  <asp:ScriptManager ID="ScriptManager1" EnablePageMethods="true" runat="server">
</asp:ScriptManager>

    <div id="wrapper">
  <div class="centralizer">
    <div class="authentication">
      <div>
        <div class="content" style="width:729px; height:350px;">
          <div class="logo_login"> 
            <!-- <a href="#" class="logo_text">estateCRM</a>--><img src="../images/esg-desklogo.png" width="160" height="35" style="padding-top:10px;" />
            </div>
            <div style="float:right; margin-top:-75px; font-size:13px;"><a href="../login.aspx"> Client login</a>&nbsp;|&nbsp;<a href="login.aspx">Support login</a></div>
          <h4 class="ctx-clr"> Change request system </h4>
          <div class="message warning"> </div>
          <div class="form">
            <div class="field"><span style="color: Red"><asp:RequiredFieldValidator
                                                                        ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtUserName" Display="Dynamic"
                                                                        ErrorMessage="Please enter username.">*</asp:RequiredFieldValidator>
            </span>User name
               <asp:TextBox ID="txtUserName" runat="server" Width="315px" TabIndex="1"></asp:TextBox>
                                                                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" TargetControlID="RequiredFieldValidator2">
                                                                    </cc1:ValidatorCalloutExtender>
              <span id="RequiredFieldValidator1" title="Enter user name" style="color:Red;display:none;"></span> </div>
              
            <div class="field" style="float:right;"> <span style="color: Red"> <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtPassword"
                                                                        Display="Dynamic" ErrorMessage="Please enter password.">*</asp:RequiredFieldValidator>
            </span>Password
               <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" Width="315px" 
                                                                        TabIndex="2"></asp:TextBox>
                                                                   
                                                                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server" TargetControlID="RequiredFieldValidator3">
                                                                    </cc1:ValidatorCalloutExtender>
              <span id="Span1" title="Enter password" style="color:Red;display:none;"></span> </div>
              
            <div class="field inline" style="width:95%" >
              <div> <span style="font-size:14px;">
                 <asp:CheckBox ID="chkSaveDetails" runat="server" Checked="True" TabIndex="2" />
                &nbsp; Save details (check this if you want to be
                able to go straight into the change request site without having to login. You will need to have cookies enabled in your browser.) </span> </div>
            </div>
            
            
           <%-- <div class="field"> <span style="color: Red">*</span> Client ID:
              <input name="txtClientID" id="txtClientID" onkeypress="return isNumberKey(event);" style="width: 315px;" type="text">
              <span id="Span2" title="Enter client ID" style="color:Red;display:none;"></span> </div>--%>
              
              
            <div class="buttons" style="float:right;">
              <ul>
                <li>
                 <asp:Button ID="btnLogin" runat="server" OnClick="btnLogin_Click" Text="Support login" 
                                                        CssClass="loginbutton" TabIndex="3" />
              
                <!--  <input name="btnlogin" value="Login" onclick='javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("btnlogin", "", true, "", "", false, false))' id="btnlogin" class="loginbutton" type="submit">-->
                </li>
              </ul>
            </div>
            
            
            <footer>
              <ul class="flags">
                <li><span id="lblMessage" style="color:Red;"></span> </li>
              </ul>
              <div id="ValidationSummary1" style="color:Red;width:300px;display:none;"> </div>
            </footer>
            
            <div id="divMessage" runat="server" style="color: red" visible="true">
            
          </div>
        </div>
        
      </div>
    </div>
  </div>
</div>


</div>




<%--
    
    <uc1:footer ID="Footer1" runat="server" style="display:none;" />--%>
    </form>
</body>
</html>
