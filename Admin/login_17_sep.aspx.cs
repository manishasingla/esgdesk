using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Globalization;
using System.Threading;
using System.Net;
using System.Net.NetworkInformation;


public partial class Admin_login_sev : System.Web.UI.Page
{
    string sql = "";


    string Intime;
    //  DateTime d = DateTime.Now;

    //= DateTime.Parse(System.DateTime.Now.ToString(), System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat).ToShortTimeString();
    //DateTime.Now.ToShortTimeString();
    //DateTime.Parse(System.DateTime.Now.ToShortTimeString().ToString(), System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat);
    //DateTime.Now.ToShortTimeString();
    string Indate;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //CultureInfo MyCulture = new CultureInfo("IN");
            //DateTime MyDate = System.DateTime.Now;

            //Thread.CurrentThread.CurrentCulture = MyCulture;

            //DateTimeFormatInfo dtf = MyCulture.DateTimeFormat;
            ////Change date and time separator
            ////dtf.DateSeparator = "\\";
            ////dtf.TimeSeparator = "&";
            //Intime = MyDate.ToShortTimeString();
            ////object d = TimeZoneInfo.GetSystemTimeZones();
            ////object t = TimeZoneInfo.GetSystemTimeZones();

            ////TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
            ////// May 7, 08:04:00
            ////DateTime userDateTime = DateTime.Now;
            ////DateTime utcDateTime = userDateTime.Subtract(tzi.BaseUtcOffset);
            string zoneId = "India Standard Time";
            TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById(zoneId);
            DateTime result = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, tzi);
            //Intime = result.ToShortTimeString().ToString();
            Intime = result.ToString("hh:mm tt");
            // Intime = d.ToLocalTime().ToShortTimeString();
        }
        catch
        { }

        divMessage.InnerHtml = "";

        if (!Page.IsPostBack)
        {
            if (Request.QueryString["un"] != null && Request.QueryString["pwd"] != null)
            {
                txtUserName.Text = Request.QueryString["un"].ToString();
                txtPassword.Text = Request.QueryString["pwd"].ToString();
                //Pass the user name and password to the VerifyLogin method
                VerifyLogin(Request.QueryString["un"].ToString(), Request.QueryString["pwd"].ToString());
            }
            else  if (Request.Browser.Cookies)
            {
                //Check if the cookies with name PBLOGIN exist on user's machine
                if (Request.Cookies["ADMINLOGIN"] != null)
                {
                    txtUserName.Text = Request.Cookies["ADMINLOGIN"]["USERNAME"].ToString();
                    txtPassword.Text = Request.Cookies["ADMINLOGIN"]["PASSWORD"].ToString();
                    
                    //Pass the user name and password to the VerifyLogin method
                    VerifyLogin(Request.Cookies["ADMINLOGIN"]["USERNAME"].ToString(), Request.Cookies["ADMINLOGIN"]["PASSWORD"].ToString());
                }
            }
        }
    }

    private string IOtimeGenerateHTML4HR()
    {
        object objUserName = DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + Session["admin"].ToString() + "'");

        string strHtml = "";
        strHtml += "<br>" + "Hi," + "<br>";
        strHtml += "Employee name: " + objUserName.ToString() + "<br>";
        strHtml += "Date: " + Indate + "<br>";
        strHtml += "<b>" + "In time: " + "</b>" + Intime + "<br/><br/>";
        strHtml += "Thanks & regards" + "<br/>";
        strHtml += objUserName.ToString();
        return strHtml;
    }
    private string IOtimeGenerateHTML4HR_With_MacId()
    {
        //***************************************************************
        string[] strArrayMacIDAndIP = new string[2];
        string strMacId = "";
        string strIpAddress = "";

        foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
        {
            if (nic.OperationalStatus == OperationalStatus.Up)
            {
                strMacId += nic.GetPhysicalAddress().ToString();
                break;
            }
        }

        strArrayMacIDAndIP[0] = strMacId;

        strIpAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (strIpAddress == null)

            strIpAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

        strArrayMacIDAndIP[1] = strIpAddress;
        //*****************************************************************




        object objUserName = DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + Session["admin"].ToString() + "'");

        string strHtml = "";
        strHtml += "<br>" + "Hi," + "<br>";
        strHtml += "Employee name: " + objUserName.ToString() + "<br>";
        strHtml += "Date: " + Indate + "<br>";
        strHtml += "<b>" + "In time: " + "</b>" + Intime + "<br/><br/>";
        strHtml += "Thanks & regards" + "<br/>";
        strHtml += objUserName.ToString() + "<br/>";
        strHtml += "Ip: " + strArrayMacIDAndIP[1].ToString() + "<br/>";
        strHtml += "MacId: " + strArrayMacIDAndIP[0].ToString() + "<br/>";
        return strHtml;
    }
    protected void VerifyLogin(string UserName, string Password)
    {
        DataSet ds = DatabaseHelper.getDataset("select * from users where username='" + UserName.Replace("'", "''") + "' and password = '" + Password.Replace("'", "''") + "'");

        if (ds != null && ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["active"].ToString() != "N")
                {
                    Session["admin"] = UserName;
                    Session["Show_Closed_Tasks"] = ds.Tables[0].Rows[0]["Show_Closed_Tasks"].ToString();

                    string userid = ds.Tables[0].Rows[0]["user_id"].ToString();
                    Indate = DateTime.Parse(System.DateTime.Now.ToString(), System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat).ToString("dd/MM/yyyy");

                    // Indate = DateTime.Now.ToString("dd/MM/yyyy");

                    sql = "SELECT count(*) from tblIntime where [username]='" + Session["admin"].ToString().Replace("'", "''")
                                      + "' And [Indate]= '" + Indate + "'";
                    object objintime = DatabaseHelper.executeScalar(sql);

                    if (objintime.ToString() != "0")
                    {
                    }
                    else
                    {
                        sql = "";
                        sql += " INSERT INTO [tblIntime] (username,user_id,Indate,Intime)";
                        sql += "VALUES ('" + Session["admin"].ToString().Replace("'", "''") + "',";
                        sql += "'" + int.Parse(userid) + "',";
                        sql += "'" + Indate + "',";
                        sql += "'" + Intime + "'";
                        sql += "); ";

                        object objResult = DatabaseHelper.executeScalar(sql);

                        if (objResult != "0")
                        {
                            bool RetMail1 = true;
                            object email = DatabaseHelper.executeScalar("select email from users where username='" + Session["admin"].ToString() + "'");
                            string strBody4HR = "";
                            string strBody4HRMacIP = "";

                            strBody4HR = IOtimeGenerateHTML4HR();
                            strBody4HRMacIP=IOtimeGenerateHTML4HR_With_MacId();
                            RetMail1 = DatabaseHelper.sendEmailEODC(email.ToString(), email.ToString(), "In time and out time from   " + Session["admin"].ToString(), strBody4HR, 0);
                            //RetMail1 = DatabaseHelper.sendEmailEODC("ritub@esolutionsgroup.co.uk", email.ToString(), "In time from  " + Session["admin"].ToString(), strBody4HRMacIP, 0);
                            RetMail1 = DatabaseHelper.sendEmailEODC("juhis@esolutionsgroup.co.uk", email.ToString(), "In time from  " + Session["admin"].ToString(), strBody4HRMacIP, 0);
                            RetMail1 = DatabaseHelper.sendEmailEODC("rohitd@esolutionsgroup.co.uk", email.ToString(), "In time from  " + Session["admin"].ToString(), strBody4HRMacIP, 0);
                            //RetMail1 = DatabaseHelper.sendEmailEODC("rohitd@123sitesolutions.com", email.ToString(), "In time from  " + Session["admin"].ToString(), strBody4HRMacIP, 0);
                        }
                    }

                    try
                    {
                        sql = @"select top(1) task_id from tasks where deleted <> 1  and status = 'CWO'
                                    and  assigned_to_user = '" + Session["admin"].ToString().Replace("'", "''") + "' order by last_updated_user";

                        object objTaskId = DatabaseHelper.executeScalar(sql);

                        if (objTaskId != null)
                        {
                            sql = @"select count(*) from hours_reporting 
                                where task_id = " + objTaskId.ToString() + " and finished_date is null and started_date > convert(datetime,'" + DateTime.Today.ToString() + "',103)";

                            object count = DatabaseHelper.executeScalar(sql);

                            if (count.ToString() == "0")
                            {
                                sql = "insert into hours_reporting ([task_id],[started_date],[username]) ";
                                sql += "values(" + objTaskId.ToString() + ",getdate(),'" + Session["admin"].ToString().Replace("'", "''") + "')";
                                int intResult = DatabaseHelper.executeNonQuery(sql);
                            }
                        }
                    }
                    catch { }

                    int intLogin = DatabaseHelper.executeNonQuery("update users set most_recent_login_datetime = getdate() where username='" + UserName.Replace("'", "''") + "'");

                    if (Session["returnUrl"] != null)
                    {
                        Response.Redirect(Session["returnUrl"].ToString(), false);
                        Session.Remove("returnUrl");
                    }
                    else
                    {
                        Response.Redirect("tasks.aspx", false);
                    }
                }
                else
                    divMessage.InnerHtml = "Your account is inactived. You can't login at this time.";
            }
            else
                divMessage.InnerHtml = "Incorrect user name and/or password. Please try again with 'Caps' off.";
        }
        else
            divMessage.InnerHtml = "Incorrect user name and/or password. Please try again with 'Caps' off.";
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        string UserName = txtUserName.Text.Trim();
        string Password = txtPassword.Text.Trim();

        if (chkSaveDetails.Checked)
        {
            //Check if the browser support cookies
            if ((Request.Browser.Cookies))
            {
                //Check if the cookie with name PBLOGIN exist on user's machine
                if ((Request.Cookies["ADMINLOGIN"] == null))
                {
                    //Create a cookie with expiry of 30 days
                    Response.Cookies["ADMINLOGIN"].Expires = DateTime.Now.AddDays(30);

                    //Write username to the cookie
                    Response.Cookies["ADMINLOGIN"]["USERNAME"] = UserName;
                    //Write password to the cookie
                    Response.Cookies["ADMINLOGIN"]["PASSWORD"] = Password;
                }
                //If the cookie already exist then wirte the user name and password on the cookie
                else
                {
                    Response.Cookies["ADMINLOGIN"]["USERNAME"] = UserName;
                    Response.Cookies["ADMINLOGIN"]["PASSWORD"] = Password;
                }
            }
        }

        VerifyLogin(UserName, Password);
    }

}
