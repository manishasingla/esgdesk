using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Admin_loginparameter : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string sql = "";

        string ClientId = "0";
        string UserName = "";
        string Password = "";
        string savedetails = "";
        

        if (Request.QueryString["username"] != null && Request.QueryString["username"] != "0")
        {

            try
            {

                UserName = Request.QueryString["username"];

                if (Request.QueryString["password"] != null && Request.QueryString["password"] != "0")
                {
                    Password = Request.QueryString["password"];
                }


                if (Request.QueryString["savedetails"] != null && Request.QueryString["savedetails"] != "false")
                {
                    savedetails = Request.QueryString["savedetails"].ToString();
                   /// Request.Cookies["ESLOGIN"] = ESlogin;
                }




                if (savedetails == "true")
                {
                    //Check if the browser support cookies

                   //// Request.Cookies["ADMINLOGIN"] = null;

                    ////if ((Request.Browser.Cookies))
                    ////{
                        //Check if the cookie with name PBLOGIN exist on user's machine

                        //////if ((Request.Cookies["ADMINLOGIN"] == null))
                        //////{
                        //////    //Create a cookie with expiry of 30 days
                        //////    Response.Cookies["ADMINLOGIN"].Expires = DateTime.Now.AddDays(30);

                        //////    //Write username to the cookie
                        //////    Response.Cookies["ADMINLOGIN"]["USERNAME"] = UserName;
                        //////    //Write password to the cookie
                        //////    Response.Cookies["ADMINLOGIN"]["PASSWORD"] = Password;
                        //////}
                        ////////If the cookie already exist then wirte the user name and password on the cookie
                        //////else
                        //////{
                            Response.Cookies["ADMINLOGIN"].Expires = DateTime.Now.AddDays(30);

                            Response.Cookies["ADMINLOGIN"]["USERNAME"] = UserName;
                            Response.Cookies["ADMINLOGIN"]["PASSWORD"] = Password;
                        //////}
                   /// }
                }



                VerifyLogin(UserName, Password);



            }
            catch
            
            {
                Response.Redirect("login.aspx", false);
            
            }
        }

    }

    protected void VerifyLogin(string UserName, string Password)
    {

        string sql = "";

        DataSet ds = DatabaseHelper.getDataset("select * from users where username='" + UserName.Replace("'", "''") + "' and password = '" + Password.Replace("'", "''") + "'");

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["active"].ToString() != "N")
                {
                    Session["admin"] = UserName;

                    try
                    {

                        sql = @"select top(1) task_id from tasks where deleted <> 1  and status = 'CWO'
                                    and  assigned_to_user = '" + Session["admin"].ToString().Replace("'", "''") + "' order by last_updated_user";

                        object objTaskId = DatabaseHelper.executeScalar(sql);

                        if (objTaskId != null)
                        {

                            sql = @"select count(*) from hours_reporting 
                        where task_id = " + objTaskId.ToString() + " and finished_date is null and started_date > convert(datetime,'" + DateTime.Today.ToString() + "',103)";

                            object count = DatabaseHelper.executeScalar(sql);

                            if (count.ToString() == "0")
                            {
                                sql = "insert into hours_reporting ([task_id],[started_date],[username]) ";
                                sql += "values(" + objTaskId.ToString() + ",getdate(),'" + Session["admin"].ToString().Replace("'", "''") + "')";

                                int intResult = DatabaseHelper.executeNonQuery(sql);
                            }
                        }
                    }
                    catch { }

                    int intLogin = DatabaseHelper.executeNonQuery("update users set most_recent_login_datetime = getdate() where username='" + UserName.Replace("'", "''") + "'");

                    ////if (Session["returnUrl"] != null)
                    ////{
                    ////    Response.Redirect(Session["returnUrl"].ToString(), false);
                    ////    Session.Remove("returnUrl");
                    ////}
                    ////else
                    ////{
                        Response.Redirect("tasks.aspx", false);
                    ///}
                }
                else
                {
                    Response.Redirect("login.aspx", false);
                }
            }
            else
            {
                Response.Redirect("login.aspx", false);
               // divMessage.InnerHtml = "Incorrect user name and/or password. Please try again with 'Caps' off.";
            }
        }
        else
        {
            Response.Redirect("login.aspx", false);
            ///divMessage.InnerHtml = "Incorrect user name and/or password. Please try again with 'Caps' off.";
        }
    }
}
