﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Admin_newsdetails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Response.Redirect("login.aspx");
            return;
        }
        if (Request.QueryString["id"] != null)
        {
            getcategory(Request.QueryString["id"].ToString());
        }
        else
        {
            Response.Redirect("Dashboard.aspx");
        }


    }
    void getcategory(string id)
    {

        string sql = @"select * from Newstbl 
  where id=" + id;

        DataSet ds = DatabaseHelper.getallstatus(sql);

        if (ds != null)
        {
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                lblNH.Text = ds.Tables[0].Rows[0]["Heading"].ToString();
                newsdesc.InnerHtml = ds.Tables[0].Rows[0]["Comment"].ToString();
                lbldate.Text = ds.Tables[0].Rows[0]["Addeddate"].ToString();
                editnews.HRef = "AddNews.aspx?id=" + Request.QueryString["id"].ToString();
            }
            else
            {
                Response.Redirect("Dashboard.aspx");
            }

        }
        else
        {
            Response.Redirect("Dashboard.aspx");
        }



    }
}