﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Admin_newslist : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Response.Redirect("login.aspx");
            return;
        }
        getcategory();
    }
    void getcategory()
    {

        string sqlsub = @" select Row_Number()OVER(order by id) as RowNumber,* from newstbl ";


        DataSet dssub = DatabaseHelper.getallstatus(sqlsub);
        if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0)
        {

            gridlist.DataSource = dssub.Tables[0];
            gridlist.DataBind();

        }
        else
        {
            gridlist.Visible = false;
            addsolution.Visible = true;
        }

    }
}



