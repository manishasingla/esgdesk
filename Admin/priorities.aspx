<%@ Page Language="C#" AutoEventWireup="true" CodeFile="priorities.aspx.cs" Inherits="Admin_priorities" %>

<%@ Register Src="Notifications.ascx" TagName="Notifications" TagPrefix="uc4" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        Priorities
    </title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
    
    <script type="text/JavaScript">
function confirmDelete()
{
var  agree=confirm("Are you sure you want to delete this file?");
if (agree)
     return true;
else
     return false;
}
</script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="wrapper">
            <uc2:Admin_Header ID="Admin_Header1" runat="server" />
            <div id="Content">
                <div id="Content_98">
                    <div class="titleText" style="margin: 10px 0 10px 0; display: none;">
                        Priorities&nbsp;|&nbsp;</div>
                    <div>
                        <uc4:Notifications ID="Notifications" runat="server" />
                    </div>
                    <div align="right" style="float: right; width: 100%; background-color: #ebebeb; padding: 5px 0;">
                        <span class="text_default" style="float: left">&nbsp;&nbsp;<asp:LinkButton ID="lnkAddNew"
                            runat="server" OnClick="lnkAddNew_Click" CausesValidation="False">Add new priority</asp:LinkButton>
                            <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="False">&nbsp;|&nbsp;Back to priorities</asp:LinkButton></span><b>Records:
                            </b>
                        <asp:Label ID="LblRecordsno" runat="server" Text=""></asp:Label>&nbsp;&nbsp;</div>
                    <div style="clear: both">
                    </div>
                    <div id="DivEntry" runat="server" visible="false">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="left" valign="top" style="width: 434px">
                                    <table cellpadding="0" style="width: 424px">
                                        <tr>
                                            <td style="width: 424px">
                                                <table width="424" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td align="left" valign="top">
                                                            <img height="16" src="../images/loginBox_top.png" width="420" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" style="background: url(../images/loginBox_mid.png) repeat-y left top;">
                                                            <div style="text-align: center">
                                                                <table width="100%" cellpadding="8" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" valign="top" class="whitetext1">
                                                                        </td>
                                                                        <td align="left" valign="top" class="whitetext1">
                                                                            Priority ID:
                                                                        </td>
                                                                        <td align="left" valign="top">
                                                                            <asp:Label CssClass="whitetext2" ID="lblId" runat="server">New</asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" valign="top" class="whitetext1">
                                                                        </td>
                                                                        <td align="left" valign="top" class="whitetext1">
                                                                            Priority name:
                                                                        </td>
                                                                        <td align="left" valign="top">
                                                                            <asp:TextBox ID="txtName" runat="server" MaxLength="50" Width="220"></asp:TextBox>
                                                                            <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator1">
                                                                            </cc1:ValidatorCalloutExtender>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName"
                                                                                ErrorMessage="Please enter priority name.">*</asp:RequiredFieldValidator>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" valign="top" class="whitetext1">
                                                                        </td>
                                                                        <td align="left" valign="top" class="whitetext1">
                                                                            Sort sequence:
                                                                        </td>
                                                                        <td valign="top" align="left">
                                                                            <asp:TextBox ID="txtSortSequece" runat="server" Width="220px" MaxLength="2"></asp:TextBox>
                                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"
                                                                                TargetControlID="txtSortSequece">
                                                                            </cc1:FilteredTextBoxExtender>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" valign="top" class="whitetext1">
                                                                        </td>
                                                                        <td align="left" valign="top" class="whitetext1">
                                                                            Background color:
                                                                        </td>
                                                                        <td align="left" valign="top">
                                                                            <asp:TextBox ID="txtBgColor" runat="server" MaxLength="50" Width="220px">#ffffff</asp:TextBox>
                                                                            <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" TargetControlID="RequiredFieldValidator2">
                                                                            </cc1:ValidatorCalloutExtender>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtBgColor"
                                                                                ErrorMessage="Please enter background color value.">*</asp:RequiredFieldValidator>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" valign="top" class="whitetext1">
                                                                        </td>
                                                                        <td align="left" valign="top" class="whitetext1">
                                                                            Default selection:
                                                                        </td>
                                                                        <td align="left" valign="top">
                                                                            <asp:CheckBox ID="chkDefault" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" style="width: 100px">
                                                            <img height="16" src="../images/loginBox_btm.png" width="420" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top">
                                                <hr />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 424px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td align="left" valign="middle">
                                                            <div id="divMessage" runat="server" style="color: Red; font-weight: bold">
                                                            </div>
                                                        </td>
                                                        <td align="right" valign="middle">
                                                            <asp:Button ID="btnCreate" runat="server" Text="Create" OnClick="btnCreate_Click"
                                                                CssClass="blueBtns" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div>
                        <div style="clear: both;">
                        </div>
                        <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" CellPadding="5"
                            OnItemCommand="DataGrid1_ItemCommand" OnItemDataBound="DataGrid1_ItemDataBound"
                            Width="100%">
                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                Font-Underline="False" HorizontalAlign="Center" />
                            <HeaderStyle Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False"
                                HorizontalAlign="Center" BackColor="#282C5F" ForeColor="#FFFFFF" />
                            <Columns>
                                <asp:BoundColumn DataField="priority_id" HeaderText="Priority id"></asp:BoundColumn>
                                <asp:BoundColumn DataField="priority_name" HeaderText="Priority name">
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" />
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="sort_seq" HeaderText="Sort sequence"></asp:BoundColumn>
                                <asp:BoundColumn DataField="bg_color" HeaderText="Background color"></asp:BoundColumn>
                                <asp:BoundColumn DataField="default_selection" HeaderText="Default selection"></asp:BoundColumn>
                                <%--         <asp:ButtonColumn CommandName="edit" HeaderText="Edit" Text="edit"></asp:ButtonColumn>
                            <asp:ButtonColumn CommandName="delete" HeaderText="Delete" Text="delete"></asp:ButtonColumn>
                           --%>
                                <asp:TemplateColumn HeaderText="Edit">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="lnkEdit" runat="server" ImageUrl="images/edit-icon.png" CommandName="Edit">
                                        </asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Delete">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="lnkdelete" runat="server" ImageUrl="images/delete-icon.png"
                                            OnClientClick="return confirmDelete()" CommandName="delete"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                
                                
                                
                                
                                
                            </Columns>
                        </asp:DataGrid>
                        <asp:HiddenField ID="prevName" runat="server" />
                        &nbsp;
                    </div>
                </div>
            </div>
        </div>
        <uc1:footer ID="Footer1" runat="server" />
    </form>
</body>
</html>
