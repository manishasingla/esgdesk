using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Admin_priorities : System.Web.UI.Page
{
    string sql = "";
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = "priorities.aspx";
            Response.Redirect("login.aspx");
            return;
        }

        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            Response.Redirect("tasks.aspx");
            return;
        }


        divMessage.InnerHtml = "";

        if (!Page.IsPostBack)
        {
            bindData();
        }
    }

    protected void btnCreate_Click(object sender, EventArgs e)
    {
        if (txtName.Text.Trim() == "")
        {
            divMessage.InnerHtml = "Please enter priority name.";
            return;
        }
        if (txtBgColor.Text.Trim() == "")
        {
            divMessage.InnerHtml = "Please enter background color.";
            return;
        }

        if (btnCreate.Text == "Create")
        {
            object objResult = DatabaseHelper.executeScalar("select count(*) from priorities where [priority_name]='" + txtName.Text.Trim() + "'");

            if (objResult.ToString() != "0")
            {
                divMessage.InnerHtml = "Priority name alredy exist.";
                return;
            }


            string strSql = " insert into priorities ([priority_name],[sort_seq],[bg_color],[default_selection]) ";
            strSql += " values('" + txtName.Text.Trim().Replace("'", "''") + "'," + ((txtSortSequece.Text.Trim() != "") ? txtSortSequece.Text.Trim() : "0") + ",'" + txtBgColor.Text.Trim().Replace("'", "''") + "','" + (chkDefault.Checked ? 'Y' : 'N') + "')";

            int intResult = DatabaseHelper.executeNonQuery(strSql);

            if (intResult != 0)
            {
                divMessage.InnerHtml = "Priority was created.";
                clearControls();
            }
            else
            {
                divMessage.InnerHtml = "Priority was not created.";
            }
        }
        else if (btnCreate.Text == "Edit")
        {
            object objResult = DatabaseHelper.executeScalar("select count(*) from priorities where [priority_name]='" + txtName.Text.Trim() + "' and [priority_id]!=" + lblId.Text);

            if (objResult.ToString() != "0")
            {
                divMessage.InnerHtml = "Priority name alredy exist.";
                return;
            }

            string strSql = " update priorities ";
            strSql += " set [priority_name]= '" + txtName.Text.Trim().Replace("'", "''") + "',";
            strSql += " [sort_seq] = " + txtSortSequece.Text.Trim() + ",";
            strSql += " [bg_color]= '" + txtBgColor.Text.Trim().Replace("'", "''") + "',";
            strSql += " [default_selection] = '" + (chkDefault.Checked ? 'Y' : 'N') + "'";
            strSql += " where priority_id = " + lblId.Text.Trim();

            int intResult = DatabaseHelper.executeNonQuery(strSql);

            if (intResult != 0)
            {
                strSql = "update tasks set priority = '" + txtName.Text.Trim().Replace("'", "''") + "' ";
                strSql += " where priority ='" + prevName.Value.Replace("'", "''") + "'";
                intResult = DatabaseHelper.executeNonQuery(strSql);

                divMessage.InnerHtml = "Priority was updated.";
            }
            else
            {
                divMessage.InnerHtml = "Priority was not updated.";
            }
        }
    }

    private void bindData()
    {
        string strSql = "select * from priorities";

        DataSet ds = DatabaseHelper.getDataset(strSql);

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                DataGrid1.DataSource = ds.Tables[0];
                DataGrid1.DataBind();
                PagedDataSource objds = new PagedDataSource();
                objds.DataSource = ds.Tables[0].DefaultView;

                LblRecordsno.Text = objds.DataSourceCount.ToString();
                DataGrid1.Visible = true;
                lnkBack.Visible = false;
            }
            else
            {
                LblRecordsno.Text = "0";
                DataGrid1.DataSource = null;
                DataGrid1.DataBind();
                DataGrid1.Visible = false;
                lnkBack.Visible = false;
                divMessage.InnerHtml = "No priority found.";
            }
        }
        else
        {
            DataGrid1.DataSource = null;
            DataGrid1.DataBind();
            DataGrid1.Visible = false;
            lnkBack.Visible = false;
            divMessage.InnerHtml = "No priority found.";
        }
    }
    protected void lnkAddNew_Click(object sender, EventArgs e)
    {
        clearControls();
        DivEntry.Visible = true;
        DataGrid1.Visible = false;
        lnkBack.Visible = true;
        divMessage.InnerHtml = "";
    }

    protected void DataGrid1_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "edit")
        {
            string strSql = "select * from priorities where priority_id=" + e.Item.Cells[0].Text;
            DataSet ds = DatabaseHelper.getDataset(strSql);

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    lblId.Text = ds.Tables[0].Rows[0]["priority_id"].ToString();
                    txtName.Text = ds.Tables[0].Rows[0]["priority_name"].ToString();
                    prevName.Value = ds.Tables[0].Rows[0]["priority_name"].ToString();
                    txtBgColor.Text = ds.Tables[0].Rows[0]["bg_color"].ToString();
                    txtSortSequece.Text = ds.Tables[0].Rows[0]["sort_seq"].ToString();
                    chkDefault.Checked = ds.Tables[0].Rows[0]["default_selection"].ToString() == "Y" ? true : false;
                    btnCreate.Text = "Edit";
                    DivEntry.Visible = true;
                    DataGrid1.Visible = false;
                    lnkBack.Visible = true;
                    divMessage.InnerHtml = "";
                }
            }
        }
        else if (e.CommandName == "delete")
        {
            object objResult = DatabaseHelper.executeScalar("select count(*) from tasks where priority =" + e.Item.Cells[0].Text);

            if (objResult.ToString() == "0")
            {
                string strSql = "delete from priorities where priority_id=" + e.Item.Cells[0].Text;
                int intResult = DatabaseHelper.executeNonQuery(strSql);

                if (intResult != 0)
                {
                    divMessage.InnerHtml = "Priority was deleted.";
                    bindData();
                }
                else
                {
                    divMessage.InnerHtml = "Priority was not deleted.";
                    bindData();
                }
            }
            else
            {
                divMessage.InnerHtml = "You can not delete this priority. It is been refered by another table.";
            }
        }
    }

    protected void DataGrid1_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //ImageButton deleteButton = (ImageButton)e.Item.Cells[6].Controls[0];

            //We can now add the onclick event handler
            //deleteButton.Attributes.("onclick", "javascript:return confirm('Are you sure you want to delete this priority?')");
        }

        if (e.Item.ItemIndex != -1)
        {
            e.Item.BackColor = System.Drawing.ColorTranslator.FromHtml(e.Item.Cells[3].Text);
        }
    }

    private void clearControls()
    {
        lblId.Text = "New";
        txtName.Text = "";
        txtSortSequece.Text = "";
        txtBgColor.Text = "#ffffff";
        chkDefault.Checked = false;
        btnCreate.Text = "Create";
    }

    protected void lnkBack_Click(object sender, EventArgs e)
    {
        clearControls();
        DivEntry.Visible = false;
        bindData();
    }
}
