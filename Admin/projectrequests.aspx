﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="projectrequests.aspx.cs"
    Inherits="Admin_projectrequests" %>

<%@ Register Src="Notifications.ascx" TagName="Notifications" TagPrefix="uc4" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Src="Header.ascx" TagName="Header" TagPrefix="uc3" %>
<%@ Register Src="footer.ascx" TagName="footer" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        Project <%=projectname %></title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../css/MenuMatic.css" type="text/css" media="screen"
        charset="utf-8" />
    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
    <link href="SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript">
        function confirm() {

            ///if (e.keyCode==110)
            if (confirm == true)


                var btnmark = document.getElementById("LnkMakecommentRead");
            btnmark.click();



            //else


            return false;

        }
    </script>
    <script type="text/javascript">
        function get_(div_) {

            div_ = div_.id + "1";

            document.getElementById(div_).style.display = "block";
        }
        function get_1(div_) {

            div_ = div_.id + "1";

            document.getElementById(div_).style.display = "none";
        }
    </script>
    <script type="text/javascript" language="javascript">
        function EnablePRlink() {
            var objPR = document.getElementById("DrpMultipleTaskPR").value;

            if (objPR != "6") {

                document.getElementById("LnkBtnToChngPR").disabled = false;


                document.getElementById("LnkChngStatus").disabled = "disabled";


            }
            else {

                document.getElementById("LnkBtnToChngPR").disabled = "disabled";

                document.getElementById("LnkChngStatus").disabled = "disabled";

            }

            return;

        }

        function EnableStatuslink() {
            var objStatus = document.getElementById("DrpMultipleTaskStatus").value;

            if (objStatus != "0") {

                document.getElementById("LnkBtnToChngPR").disabled = "disabled";

                document.getElementById("LnkChngStatus").disabled = false;

            }
            else {

                document.getElementById("LnkBtnToChngPR").disabled = "disabled";

                document.getElementById("LnkChngStatus").disabled = "disabled";

            }
            return;

        }
    </script>
    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="slider.css" />
    <script type="text/javascript" src="slider.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <uc2:Admin_Header ID="Admin_Header1" runat="server" />
        <div id="Content">
            <asp:UpdateProgress ID="updateprogress1" runat="server">
                <ProgressTemplate>
                    <div id="processmessage" class="processMessage">
                        <img alt="loading" src="images/ajax-loader.gif" style="width: 80px; height: 80px" />
                        <span style="color: black; font-weight: bold; font-size: 18px; vertical-align: super;">
                            Please wait... </span>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <div id="Content_98">
                        <div class="titleText" style="margin: 10px 0 10px 0; display: none;">
                            Change requests</div>
                        <div style="text-align: left">
                            <div style="padding: 0px 0 0px 5px; margin: 2px  0 -2px 0;">
                                <table width="100%">
                                    <tr>
                                        <td style="background: url(../images/yellowbar_filter.png) no-repeat left top; width: 101px;
                                            height: 27px; text-align: center;">
                                            <span class="link" onmousedown="slideContent('section-1')">Filter</span>
                                        </td>
                                        <td style="background: url(../images/yellowbar_filter.png) no-repeat left top; width: 101px;
                                            height: 27px; text-align: center;">
                                            <span class="link" onmousedown="slideContent('section-2')">Action</span>
                                        </td>
                                        <td style="text-align: right;">
                                            <span id="TopFilterLable" runat="server" class="topfilter"></span>
                                        </td>
                                        <td style="text-align:right;" >
                                            <div style="text-align:right;">
                                                <a href="#" style="border-bottom: 1px dotted #000;color:Red;" onclick="window.open('Client_Detail.aspx?ProjectId=<%= ProjectId_Client %>','','status=no,scrollbars=no,width=400,height=250')"
                                                    class="text_button_trans">Client Id details</a> &nbsp; <a href="#" style="border-bottom: 1px dotted #000;color:Red;"
                                                        onclick="window.open('FTP_details.aspx?id=<%= FTPid %>','','status=no,scrollbars=no,width=400,height=200')"
                                                        class="text_button_trans">FTP credentials</a>
                                                        &nbsp;
                                                <asp:Button ID="LnkRelated" Style="border-bottom: 1px dotted #000;" runat="server"
                                                    Text="Project related" OnClick="LnkRelated_Click" CssClass="text_button_trans" />&nbsp;
                                                <asp:Button ID="Lnkcomprelated" runat="server" Style="border-bottom: 1px dotted #000;"
                                                    Text="Company related" OnClick="Lnkcomprelated_Click" CssClass="text_button_trans" />&nbsp;
                                               
                                                <a href="#" id="CompanyNotes" runat="server" style="border-bottom: 1px dotted #000;" class="text_button_trans">FHS notes</a> &nbsp;
                                                <a href="#" id="CRNotes" runat="server" style="border-bottom: 1px dotted #000;" class="text_button_trans">Client notes</a>
                                                &nbsp;
                                                <asp:Button Style="border-bottom: 1px dotted #000;" ID="BtnAnalytics" runat="server"
                                                    Text="Analytics" CausesValidation="False" OnClick="BtnAnalytics_Click" CssClass="text_button_trans" /></span>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div>
                                <table width="100%" border="0" cellpadding="0" cellspacing="0" onclick="return TABLE1_onclick()"
                                    id="TABLE1">
                                    <tr>
                                        <td align="right" valign="bottom">
                                            <img src="../images/filter_corner_l.png" width="11" height="59" />
                                        </td>
                                        <td bgcolor="#282C5F" width="100%">
                                            <div class="slider">
                                                <div class="slidercontent" id="slider">
                                                    <div id="section-1" class="section upper" style="margin-top: 0px;">
                                                        <table border="0" cellpadding="0" width="100%" cellspacing="0">
                                                            <tr>
                                                                <td valign="top">
                                                                    <span class="style1"></span><b style="color: #e1ac10;">Filter:</b>
                                                                </td>
                                                                <td valign="top" style="color: White; font-size: 14px">
                                                                    Change added by<br />
                                                                    <asp:DropDownList ID="drpChangeBy" runat="server" Width="170px" AutoPostBack="True"
                                                                        OnSelectedIndexChanged="drpChangeBy_SelectedIndexChanged" CssClass="filerDrpodown">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td valign="top" style="color: White; font-size: 14px">
                                                                    Status<br />
                                                                    <asp:DropDownList ID="drpStatus" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpStatus_SelectedIndexChanged"
                                                                        Width="170px" CssClass="filerDrpodown">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td valign="top" style="color: White; font-size: 14px">
                                                                    Priority<br />
                                                                    <asp:DropDownList ID="drpPriority" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpPriority_SelectedIndexChanged"
                                                                        Width="170px" CssClass="filerDrpodown">
                                                                        <asp:ListItem Value="0">[no priority]</asp:ListItem>
                                                                        <asp:ListItem Value="00">0 - IMMEDIATE</asp:ListItem>
                                                                        <asp:ListItem Value="1">1 - highest</asp:ListItem>
                                                                        <asp:ListItem Value="2">2</asp:ListItem>
                                                                        <asp:ListItem Value="3">3</asp:ListItem>
                                                                        <asp:ListItem Value="4">4</asp:ListItem>
                                                                        <asp:ListItem Value="5">5 - lowest</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td valign="top" style="color: White; font-size: 14px" style="width: 200px">
                                                                    Company name<br />
                                                                    <asp:DropDownList ID="drpWebsiteUrl" runat="server" Width="200px" AutoPostBack="True"
                                                                        OnSelectedIndexChanged="drpWebsiteUrl_SelectedIndexChanged" CssClass="filerDrpodown">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td valign="bottom" class="whitetext2">
                                                                    &nbsp;&nbsp;<asp:Button ID="btnClearFilters" OnClick="btnClearFilters_Click" runat="server"
                                                                        CssClass="clearfilter" Text="Clear filters"></asp:Button>
                                                                </td>
                                                                <td valign="bottom" style="color: White; font-size: 14px">
                                                                    <asp:CheckBox ID="chkViewDeleted" runat="server" AutoPostBack="True" OnCheckedChanged="chkViewDeleted_CheckedChanged"
                                                                        Text="View only deleted" />
                                                                </td>
                                                                <td valign="bottom" style="color: White; font-size: 14px">
                                                                    <asp:CheckBox ID="chkHideClosed" runat="server" AutoPostBack="True" OnCheckedChanged="chkHideClosed_CheckedChanged"
                                                                        Text="View closed" />
                                                                </td>
                                                                <%--                                                                <td valign="bottom" style="color: #e1ac10; display:none;">
                                                                    <b>Records: </b>
                                                                    <asp:Label ID="LblRecordsno" runat="server" Text=""></asp:Label>
                                                                </td>--%>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div id="section-2" class="section upper" style="padding-top: 20px;">
                                                        <div id="DivMiltiselection" runat="server" style="padding-bottom: 7px" class="whitetext2">
                                                            <b style="color: #e1ac10;">Action:</b>
                                                            <asp:LinkButton ID="LnkBtnToDeleteAll" runat="server" OnClick="LnkBtnToDeleteAll_Click"
                                                                CssClass="whitetext2">Delete Selected</asp:LinkButton>&nbsp;&nbsp;
                                                            <asp:LinkButton ID="LnkBtnToMarkAsRead" runat="server" OnClick="LnkBtnToMarkAsRead_Click"
                                                                CssClass="whitetext2">Mark read</asp:LinkButton>&nbsp;&nbsp;
                                                            <asp:Button ID="LnkBtnToChngPR" runat="server" BackColor="transparent" BorderStyle="none"
                                                                ForeColor="#ffffff" Text="Change priorirty to" OnClick="LnkBtnToChngPR_Click" />
                                                            <asp:DropDownList ID="DrpMultipleTaskPR" runat="server" CssClass="filerDrpodown"
                                                                AutoPostBack="true" Width="180px" OnSelectedIndexChanged="DrpMultipleTaskPR_SelectedIndexChanged">
                                                                <asp:ListItem Value="6">[no priority]</asp:ListItem>
                                                                <asp:ListItem Value="0">0 - IMMEDIATE</asp:ListItem>
                                                                <asp:ListItem Value="1">1 - highest</asp:ListItem>
                                                                <asp:ListItem Value="8">1c</asp:ListItem>
                                                                <asp:ListItem Value="2">2</asp:ListItem>
                                                                <asp:ListItem Value="3">3</asp:ListItem>
                                                                <asp:ListItem Value="4">4</asp:ListItem>
                                                                <asp:ListItem Value="5">5 - lowest</asp:ListItem>
                                                            </asp:DropDownList>
                                                            &nbsp;&nbsp;<asp:Button ID="LnkChngStatus" runat="server" BackColor="transparent"
                                                                ForeColor="#ffffff" BorderStyle="none" Text="Change status to" OnClick="LnkChngStatus_Click" />
                                                            <asp:DropDownList ID="DrpMultipleTaskStatus" runat="server" CssClass="filerDrpodown"
                                                                AutoPostBack="true" Width="180px" OnSelectedIndexChanged="DrpMultipleTaskStatus_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td valign="bottom">
                                            <img src="../images/filter_corner_r.png" width="11" height="59" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div style="float:left;width:100%;">
                            <uc4:Notifications ID="Notifications" runat="server" />
                        </div>
                        <div style="text-align:right;">
                         <span>Relevent Url:</span>
                         <asp:Label id="lblReleventUrl1" style="Border-bottom:1px dotted #000;" Font-Bold="True" runat="server" ></asp:Label>
                        </div>
                        <br />
                        <div style="clear: both">
                        </div>
                        <br />
                        <%--<div style="float: left; width: 100%; background: #ebebeb; padding: 5px 0px;">
                            <a href="add_changeRequest.aspx">&nbsp Add change request</a>
                        </div>--%>
                        <div style="clear: both">
                        </div>
                        <div id="divMessage" runat="server" style="color: Red; font-weight: bold;">
                        </div>
                        <table cellpadding="0" cellspacing="0" border="0" height="59">
                            <tr>
                                <td align="right" valign="bottom">
                                    <img src="../images/filter_corner_l.png" width="11" height="59">
                                </td>
                                <td bgcolor="#282C5F" width="97%">
                                    <div id="dvadmin" runat="server" style="color: #ffffff!important;">
                                        <asp:Label ID="lblProject" runat="server" Text="Project"></asp:Label>
                                        <asp:DropDownList ID="drpProjects" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drpProjects_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        &nbsp;Arch.
                                        <asp:CheckBox  ID="chkArchive" runat="server" AutoPostBack="True" OnCheckedChanged="chkArchive_CheckedChanged" />
                                        &nbsp;&nbsp;
                                        <asp:Label ID="lblType" runat="server" Text="Type: "></asp:Label>
                                        <asp:DropDownList ID="drpType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpType_SelectedIndexChanged">
                                            <%--<asp:ListItem Text="All" Value="All"></asp:ListItem>
                                            <asp:ListItem Text="Tasks" Selected="True" Value="Tasks"></asp:ListItem>
                                            <asp:ListItem Text="CR's" Value="CR"></asp:ListItem>--%>
                                        </asp:DropDownList>
                                        &nbsp;&nbsp;
                                        <%-- <asp:Label ID="lblhrspent" runat="server" Text="Hours spent: "></asp:Label>
                            <asp:DropDownList ID="drpHoursspent" runat = "server" 
                                onselectedindexchanged="drpHoursspent_SelectedIndexChanged" 
                                AutoPostBack="True">
                            <asp:ListItem Text="Today" Value="0"></asp:ListItem>
                            <asp:ListItem Text ="Last 7 days" Value="7" Selected="True"></asp:ListItem>
                            <asp:ListItem Text ="Last 28 days" Value="28"></asp:ListItem>
                            <asp:ListItem Text ="All time" Value=""></asp:ListItem>
                            </asp:DropDownList>--%>
                                    </div>
                                </td>
                                <td valign="bottom">
                                    <img src="../images/filter_corner_r.png" width="11" height="59">
                                </td>
                            </tr>
                        </table>
                        <div id="dvCrHeader" runat="server" visible="false">
                            <p>
                            </p>
                            <asp:Label ID="lblCR" runat="server" Text="Client Requests" CssClass="titleText"></asp:Label>
                            <span style="color: black; float: right;"><b>Records: </b>
                                <asp:Label ID="lblCrRecords" runat="server"></asp:Label></span>
                        </div>
                        <%--                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>--%>
                        <div id="dvCR" runat="server" style="font-size: 12px">
                            <asp:DataGrid ID="DataGrid1" runat="server" Width="100%" AutoGenerateColumns="False"
                                AllowPaging="True" AllowSorting="True" CellPadding="5" OnPageIndexChanged="DataGrid1_PageIndexChanged"
                                PageSize="25" OnSortCommand="DataGrid1_SortCommand" OnItemCommand="DataGrid1_ItemCommand"
                                OnItemDataBound="DataGrid1_ItemDataBound">
                                <PagerStyle NextPageText="Next" PrevPageText="Prev" />
                                <HeaderStyle Font-Bold="false" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Top" BackColor="#282C5F"
                                    CssClass="tblTitle1" />
                                <Columns>
                                    <asp:BoundColumn DataField="RequestId" Visible="false" HeaderText="ID" SortExpression="RequestId">
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" /></ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Short description" SortExpression="ShortDescr" HeaderStyle-CssClass="wordwrap">
                                        <HeaderStyle Width="300px" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblshrtDesc" runat="server" CssClass="wordwrap" Text='<%# bind("ShortDescr") %>'></asp:Label>
                                            <div id="lblshrtDesc1" runat="server" style="display: none; position: absolute; background-color: #FEFFB3;
                                                width: 730px; border: solid 1px #333333; margin-top: 5px; padding-left: 3px">
                                                <asp:Label ID="LblLastcomment" runat="server" Text='<%# bind("ShowAllComment") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Read/Unread" SortExpression="" HeaderStyle-CssClass="wordwrap">
                                        <HeaderStyle Width="128px" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkReadMark" runat="server" CssClass="wordwrap" CommandName="ReadMark">(Unread. Mark as read)</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Project name" SortExpression="ProjectName">
                                        <ItemTemplate>
                                            <a id="lblprojectname" runat="server" target="_blank" href='<%# "projectrequests.aspx?projectname="+ Eval("ProjectName") %>'>
                                                <%# Eval("ProjectName")%></a>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="Status" HeaderText="Status" SortExpression="ClientRequest.Status">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Priority" SortExpression="ClientRequest.Priority"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Priority" HeaderText="Priority" SortExpression="ClientRequest.Priority"
                                        Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="CompanyName" HeaderText="Company name" SortExpression="CompanyName">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="LastUpdatedBy" HeaderText="Last updated by" SortExpression="ClientRequest.LastUpdatedBy">
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Last updated" SortExpression="ClientRequest.LastUpdatedOn">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLastUpdatedOn" runat="server" Text='<%# bind("LastUpdatedOn") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="AddedBy" HeaderText="Change added by" SortExpression="ClientRequest.AddedBy">
                                        <HeaderStyle Width="60px" />
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Change added on" SortExpression="ClientRequest.RequestDate">
                                        <ItemTemplate>
                                            <asp:Label ID="lblChangeAddedOn" runat="server" Text='<%# bind("RequestDate") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Type" SortExpression="">
                                        <HeaderStyle Width="66px" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Supportype" runat="server" CommandName="SupprtLink">Support type</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="RequestId" HeaderText="ID" SortExpression="RequestId">
                                    </asp:BoundColumn>
                                    <asp:HyperLinkColumn HeaderText="Edit" Text="edit" DataNavigateUrlField="RequestId"
                                        DataNavigateUrlFormatString="edit_request.aspx?reqid={0}" NavigateUrl="edit_request.aspx">
                                    </asp:HyperLinkColumn>
                                </Columns>
                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Top" />
                            </asp:DataGrid>
                        </div>
                        <div id="dvheader" runat="server">
                            <p>
                            </p>
                            <%--======================================================================================--%>
                             <asp:Label ID="lblTasks" runat="server" Text="Tasks" CssClass="titleText"></asp:Label>
                            <asp:Label Id="lbltodaytimetxt" runat="server" Text="Today-"></asp:Label>
                            <asp:Label Id="lbltodaytime" runat="server"  ></asp:Label>
                            <asp:Label Id="lblyesterday" runat="server" Text="Yesterday-"></asp:Label>
                            <asp:Label Id="lblyesterdaytime" runat="server"  ></asp:Label>
                            <asp:Label Id="lblsevendaytxt" runat="server" Text="last 7 days-" ></asp:Label>
                            <asp:Label Id="lblsevenminute" runat="server"  ></asp:Label>
                             <asp:Label Id="lblmonthtxt" runat="server" Text="last 30 days-" ></asp:Label>
                            <asp:Label Id="lblTotalMonthMinute" runat="server"></asp:Label>
                             <asp:Label Id="lbltotalTime" runat="server" Text="all time-" ></asp:Label>
                             <asp:Label Id="lbltotalspendMinute" runat="server"></asp:Label>
                             <br/>
                             <asp:DropDownList ID="drpclientname" runat="server" Width="170px" AutoPostBack="True"></asp:DropDownList>
                            <%--======================================================================================--%>
                            
                            <span style="color: black; float: right;"><b>Records: </b>
                                <asp:Label ID="lblTaskRecords" runat="server"></asp:Label></span>
                        </div>
                        <br/>
                        <div id="dvTask" runat="server">
                            <asp:GridView ID="gvTask" runat="server" Width="100%" AutoGenerateColumns="False"
                                AllowPaging="True" AllowSorting="True" CellPadding="5" PageSize="50" BorderColor="#cccccc"
                                OnPageIndexChanging="gvTask_PageIndexChanging" OnRowDataBound="gvTask_RowDataBound"
                                OnRowCommand="gvTask_RowCommand" OnSorting="gvTask_Sorting" ShowFooter="True">
                                <PagerSettings Mode="NextPrevious" FirstPageText="First" PreviousPageText="Prev"
                                    NextPageText="Next" LastPageText="Last" />
                                <RowStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Top" Font-Size="12px" />
                                <Columns>
                                    <asp:BoundField DataField="task_id" HeaderText="ID" Visible="False" SortExpression="task_id" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" /></ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="short_desc" HeaderText="ShrtDesc" Visible="False" SortExpression="short_desc" />
                                    <asp:TemplateField HeaderText="Short description" SortExpression="short_desc">
                                        <ItemTemplate>
                                            <asp:Label Font-Size="12px" ID="lblshrtDesc" runat="server" Width="250px" Text='<%# bind("short_desc") %>'></asp:Label>
                                            <div id="lblshrtDesc1" runat="server" style="display: none; position: absolute; background-color: #FEFFB3;
                                                border: solid 1px #333333; width: 730px; margin-top: 5px; padding-left: 3px">
                                                <asp:Label ID="LblLastcomment" runat="server" Text='<%# bind("ShowAllComment") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:HyperLinkField Text="(+)" Visible="False" DataNavigateUrlFields="task_id" NavigateUrl="edit_task.aspx"
                                        DataNavigateUrlFormatString="edit_task.aspx?id={0}" SortExpression="short_desc">
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" Font-Size="13px" />
                                    </asp:HyperLinkField>
                                    <asp:TemplateField HeaderText="Read/Unread">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkReadMark" runat="server" Width="130px" CommandName="ReadMark">(Unread. Mark as read)</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Project" SortExpression="project">
                                        <ItemTemplate>
                                            <a id="lblprojectname" runat="server" target="_blank" href='<%# "projectrequests.aspx?projectname="+ Eval("project") %>'>
                                                <%# Eval("project")%></a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="status" HeaderText="Status" SortExpression="status" />
                                    <asp:BoundField DataField="priority" HeaderText="Priority" SortExpression="priority" />
                                    <asp:TemplateField HeaderText="Assigned to" SortExpression="assigned_to_user">
                                        <ItemTemplate>
                                            <a id="lblassignedtouser" runat="server" target="_blank" href='<%# "Priority_reports.aspx?Ename="+ Eval("assigned_to_user") %>'>
                                                <%# Eval("assigned_to_user") %></a><%--</a> --%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Last Updated By" SortExpression="last_updated_user">
                                        <ItemTemplate>
                                            <a id="lbllast_updated_user" runat="server" target="_blank" href='<%# "Priority_reports.aspx?Ename="+ Eval("last_updated_user") %>'>
                                                <%# Eval("last_updated_user")%></a><%--</a> --%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Last Updated" SortExpression="last_updated_date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLastUpdatedOn" runat="server" Text='<%# bind("last_updated_date") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="category" HeaderText="Category" SortExpression="category" />
                                    <asp:BoundField HeaderText="ETC" />
                                    <asp:BoundField HeaderText="Time to date" />
                                    <asp:BoundField HeaderText="Balance" />
                                    <asp:BoundField HeaderText="Reported by" SortExpression="reported_user" Visible="False"
                                        DataField="reported_user" />
                                    <asp:TemplateField HeaderText="Reported on" SortExpression="reported_date" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblReportedOn" runat="server" Text='<%# Bind("reported_date") %>'></asp:Label>
                                            <asp:HiddenField ID="hd" runat="server" Value='<%# Eval("ETC")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ETC" HeaderText="ETC" Visible="False" />
                                    <asp:BoundField DataField="task_id" HeaderText="ID" SortExpression="task_id" />
                                    <asp:HyperLinkField HeaderText="Edit" Text="edit" DataNavigateUrlFields="task_id"
                                        DataNavigateUrlFormatString="edit_task.aspx?id={0}" NavigateUrl="edit_task.aspx">
                                        <HeaderStyle Width="25px" />
                                    </asp:HyperLinkField>
                                    <%--                                            <asp:TemplateField HeaderText="Delete">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkdel" runat="server" CommandName="delete" Text="delete">delete</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                </Columns>
                                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Top" BackColor="#282C5F"
                                    CssClass="tblTitle1" />
                            </asp:GridView>
                            <asp:Panel ID="Panel1" runat="server">
                            </asp:Panel>
                        </div>
                        <div style="text-align: right" id="divTotalHrs" runat="server">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td align="right">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 85px;" align="right" valign="middle">
                                                    <b>Total Hrs:</b>
                                                </td>
                                                <td>
                                                    <table cellpadding="3" cellspacing="0" border="1">
                                                        <tr>
                                                            <td style="width: 95px; background-color: #282c5f; color: #ffffff; font-weight: bold;"
                                                                align="left" valign="top">
                                                                Estimated time
                                                            </td>
                                                            <td style="width: 130px; background-color: #282c5f; color: #ffffff; font-weight: bold;"
                                                                align="left" valign="top">
                                                                Total hours Taken
                                                            </td>
                                                            <td style="width: 90px; background-color: #282c5f; color: #ffffff; font-weight: bold;"
                                                                align="left" valign="top">
                                                                Elapsed time
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 95px" align="left" valign="top">
                                                                <asp:Label ID="lblETC" runat="server" Font-Bold="True" Font-Size="12px"></asp:Label>
                                                            </td>
                                                            <td style="width: 125px" align="left" valign="top">
                                                                <asp:Label ID="lblTotalHrsTaken" runat="server" Font-Bold="True" Font-Size="12px"></asp:Label>
                                                            </td>
                                                            <td style="width: 90px" align="left" valign="top">
                                                                <asp:Label ID="lblExpectedHrsLeft" runat="server" Font-Bold="True" Font-Size="12px"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="padding-right: 3px; padding-left: 3px; padding-bottom: 3px; width: 25px;
                                                    padding-top: 3px">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <%--                            </ContentTemplate>
                        </asp:UpdatePanel>--%>
                    </div>
                </ContentTemplate>
                <Triggers>
                <asp:PostBackTrigger ControlID="drpProjects" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <%---------------------------------------------------------------------xxx--------------------------------------------------------------%>
        <br />
        <span style="font-size: large">Projects notes </span>
        <hr />
        <center>
            <iframe id="frmaddnotes" runat="server" width="99%" height="500px" style="border:none;" name="frmaddnotes">
            </iframe>
        </center>
        <br />
        <div id="clientnoteSection" runat="server">
        <span style="font-size: large">Client notes </span>
        <hr />
        <center>
            <iframe id="IframeClieNote" runat="server" width="99%" height="500px" style="border:none;" name="frmaddnotes">
            </iframe>
        </center>
        </div>
        <uc1:footer ID="Footer1" runat="server" />
    </div>
    <asp:Label ID="lblOrderBy" runat="server" Text="" Visible="false"></asp:Label>
    </form>
</body>
</html>
