﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Sql;
using System.Configuration;

public partial class Admin_projectrequests : System.Web.UI.Page
{
    string sql = "";
    public string projectname = "";
    int closecount = 0;
    bool hidecheck;
    string task_action = "";
    string emailSubject = "";
    public static string FTPid;
    public static string ProjectId_Client;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            //DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Check Session is null or Blank:-> DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), Session["admin"].ToString() );
            Session["returnUrl"] = "client_requests.aspx";
            Response.Redirect("login.aspx");
            return;
        }
        //*******************************************************
        // if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
        //{
        //     Response.Redirect("tasks.aspx");
        //     return;
        // }
        if (DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            clientnoteSection.Visible = true;
        }
        else
        {
            clientnoteSection.Visible = false;
        }
        ////**********************************************************************************
        string strtrace = "";
        try
        {
            strtrace = "ProjectNoteList.aspx?projectname=" + HttpUtility.UrlEncode(HttpUtility.UrlDecode(Request.QueryString["projectname"].ToString()));
            frmaddnotes.Attributes["src"] = "ProjectNoteList.aspx?projectname=" + HttpUtility.UrlEncode(HttpUtility.UrlDecode(Request.QueryString["projectname"].ToString()));
            IframeClieNote.Attributes["src"] = "AdminProjectNoteList.aspx?projectname=" + HttpUtility.UrlEncode(HttpUtility.UrlDecode(Request.QueryString["projectname"].ToString()));
            //DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Selected Project Iframe Binding 7:-> " + Session["admin"] + "  DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), strtrace);
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at Iframe Binding 6:-> " + Session["admin"] + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :->" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Sql Query:-> " + strtrace);
        }

        ////**********************************************************************************


        //*******************************************************
        try
        {
            if (Session["boolclose"] == "true")
            {
                chkHideClosed.Checked = true;
                Session["boolclose"] = "false";
            }
        }catch(Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error At Session[boolclose] 1: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Session['boolclose']" );
        }
        if (!Page.IsPostBack)
        {
           
            try
            {
                //==============================================================================================
                if (Request.QueryString["archive"] == null)
                {
                    chkArchive.Checked = false;
                    load_notArchive();

                }
                else if (Convert.ToBoolean(Request.QueryString["archive"]) == false)
                {
                    chkArchive.Checked = false;
                    load_notArchive();

                }
                else if (Convert.ToBoolean(Request.QueryString["archive"]) == true)
                {
                    load_AllProject();
                    chkArchive.Checked = true;

                }
                drpProjects.SelectedValue = Request.QueryString["projectname"].ToString();
                //==============================================================================================

                load_statuses();
            }
            catch (Exception ex)
            {
                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at Query string 1: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Page LOad 2" + Request.QueryString["projectname"].ToString());
            }
            //*******************************************************
            DataTable tabCrTasks = new DataTable();
            try
            {
                if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
                {
                    //Response.Redirect("tasks.aspx");
                    // return;
                    tabCrTasks.Clear();
                    tabCrTasks.Columns.Add("Text", typeof(string));
                    tabCrTasks.Columns.Add("Value", typeof(string));
                    tabCrTasks.Rows.Add("Tasks", "Tasks");
                    drpType.DataSource = tabCrTasks;
                    drpType.DataTextField = "Text";
                    drpType.DataValueField = "Value";
                    drpType.DataBind();
                }
                else
                {
                    tabCrTasks.Clear();
                    tabCrTasks.Columns.Add("Text", typeof(string));
                    tabCrTasks.Columns.Add("Value", typeof(string));
                    tabCrTasks.Rows.Add("All", "All");
                    tabCrTasks.Rows.Add("Tasks", "Tasks");
                    tabCrTasks.Rows.Add("CR's", "CR");
                    drpType.DataSource = tabCrTasks;
                    drpType.DataTextField = "Text";
                    drpType.DataValueField = "Value";
                    drpType.DataBind();

                }
            }
            catch (Exception ex)
            {
                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Bind All Task CR: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Bind All Task CR ");
            }
            //*******************************************************

            try
            {
                drpType.SelectedValue = "Tasks";

                if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
                {
                    LnkBtnToDeleteAll.Visible = false;
                    LnkBtnToChngPR.Visible = false;
                    DivMiltiselection.Visible = false;
                }
                else
                {
                    LnkBtnToDeleteAll.Visible = true;
                    LnkBtnToChngPR.Visible = true;
                    DivMiltiselection.Visible = true;
                }
                LnkChngStatus.Enabled = false;
                LnkBtnToChngPR.Enabled = false;
            }
            catch (Exception ex)
            {
                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at Query string 1: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "PageLoad 1 :");
            }
            if (Session["filter"] == null || Session["filter"].ToString() == "")
            {
                if (HttpUtility.UrlDecode(Request.QueryString["projectname"].ToString()) != null)
                {
                    try
                    {
                        projectname = HttpUtility.UrlDecode(Request.QueryString["projectname"].ToString());
                    }
                    catch (Exception ex)
                    {
                        DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at Query string 1: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Sql Query :->" + projectname);
                    }
                    //========================================
                    try
                    {
                        getprojectid(projectname);
                    }
                    catch (Exception ex)
                    {
                        DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at getprojectid() 2: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Sql Query :->" + projectname);

                    }
                    //**************************************************************************************************************

                    string sqlRelevanturlT = "";
                    try
                    {

                        sqlRelevanturlT = "select [Relevanturl] from [projects]  where [project_name] = '" + projectname.ToString().Trim() + "'";
                        string[] values1 = new string[] { "" };
                        object RelevantUrlProject = DatabaseHelper.executeScalar(sqlRelevanturlT);
                        string[] values2 = RelevantUrlProject.ToString().Split('\r');
                        string[] All = new string[values1.Length + values2.Length];
                        string[] R_url = new string[values1.Length + values2.Length];
                        Array.Copy(values1, All, values1.Length);
                        Array.Copy(values2, 0, All, values1.Length, values2.Length);
                        for (int j = 0; All.Length > j; j++)
                        {
                            R_url[j] = All[j].Replace("\n", "");
                        }
                        R_url = RemoveDuplicatesValue(R_url);
                        for (int i = 0; R_url.Length > i; i++)
                        {
                            string url = R_url[i];
                            string http = R_url[i].ToString().Replace("http://", "");
                            string https = R_url[i].ToString().Replace("https://", "");
                            if (url != http)
                            {
                                if (R_url[i] != "")
                                {
                                    R_url[i] = http;
                                    R_url[i] = R_url[i].Trim();
                                    R_url[i] = "http://" + R_url[i];
                                }
                            }
                            if (url != https)
                            {
                                if (R_url[i] != "")
                                {
                                    R_url[i] = https;
                                    R_url[i] = R_url[i].Trim();
                                    R_url[i] = "https://" + R_url[i];
                                }
                            }
                            else
                            {
                                if (R_url[i] != "")
                                {
                                    R_url[i] = http;
                                    R_url[i] = R_url[i].Trim();
                                    R_url[i] = "http://" + R_url[i];
                                }
                            }

                            if (i == 0)
                            {
                                lblReleventUrl1.Text = "<a href=" + R_url[i].ToString() + " + target=" + "blank" + ">" + R_url[i].ToString() + "</a>";
                                //lbl_RelevantURL.Text = "<a href=" + R_url[i] + " + target=" + "blank" + ">" + R_url[i] + "</a>";
                            }
                            else
                            {
                                R_url[i].Replace("\n", "");
                                R_url[i] = R_url[i].Trim();
                                lblReleventUrl1.Text += "<br>" + "<a href=" + R_url[i].ToString() + " + target=" + "blank" + ">" + R_url[i].ToString() + "</a>";
                                //lbl_RelevantURL.Text += "<br>" + "<a href=" + R_url[i] + " + target=" + "blank" + ">" + R_url[i] + "</a>";
                            }


                        }
                    }
                    catch (Exception ex)
                    {
                        DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at Page Load sqlRelevanturlT Query: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Sql Query :->" + sqlRelevanturlT);
                    }
                    //}

                    //-------------------------------------------------------------------------------------------
                    string SqlclientName = "";
                    try
                    {

                        SqlclientName = @"select RegId,UserName	from NonesCRMusers where ProjectName='" + projectname.ToString().Trim() + "'";
                        DataSet dsclient_dropdowns = DatabaseHelper.getDataset(SqlclientName);
                        if (dsclient_dropdowns != null && dsclient_dropdowns.Tables.Count > 0 && dsclient_dropdowns.Tables[0].Rows.Count > 0)
                        {
                            try
                            {
                                drpclientname.DataSource = dsclient_dropdowns.Tables[0];
                                drpclientname.DataTextField = "UserName";
                                drpclientname.DataValueField = "UserName";
                                drpclientname.DataBind();
                                drpclientname.Items.Insert(0, new ListItem("[Clients]", ""));
                            }
                            catch (Exception ex)
                            {
                                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at Dropdown binding 3: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Sql Query :->" + SqlclientName);
                            }
                        }
                        else
                        {

                            drpclientname.Items.Insert(0, new ListItem("[Clients]", ""));
                            drpclientname.Enabled = false;
                        }
                    }
                    catch (Exception ex)
                    {

                        DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at Dropdown binding 4:-> " + Session["admin"] + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :->" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Sql Query:->" + SqlclientName);

                    }
                    //--------------------------------------------------------------------------------------------------------------
                    //***************************************************************************************************************

                }

                string sqlcompany = "select [CompanyName] from [projects]  where [project_name] = '" + projectname + "'";
                try
                {

                    object objResult = DatabaseHelper.executeScalar(sqlcompany);

                    if (objResult != null)
                    {
                        string strCNotes = "window.open('companyNotes.aspx?CompName=" + objResult.ToString() + "','_blank');";
                        CompanyNotes.Attributes.Add("onClick", strCNotes);
                        string strCRNotes = "window.open('CRNotes.aspx?CompName=" + objResult.ToString() + "','_blank');";
                        CRNotes.Attributes.Add("onClick", strCRNotes);
                    }
                }
                catch (Exception ex)
                {
                    DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at JavaScript 5: " + Session["admin"] + "DateTime : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:" + ex.Message + "\n" + " Source:" + ex.Source + "\n" + " InnerException:" + ex.InnerException + " Stack Trace: " + ex.StackTrace + "\n" + "Sql Query:-> " + sqlcompany);
                }
                ////**********************************************************************************
                //string strtrace = "";
                //try
                //{
                //    strtrace = "ProjectNoteList.aspx?projectname=" + HttpUtility.UrlEncode(HttpUtility.UrlDecode(Request.QueryString["projectname"].ToString()));
                //    frmaddnotes.Attributes["src"] = "ProjectNoteList.aspx?projectname=" + HttpUtility.UrlEncode(HttpUtility.UrlDecode(Request.QueryString["projectname"].ToString()));
                //    DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at Iframe Binding 7:-> " + Session["admin"] + "  DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), strtrace);
                //}
                //catch (Exception ex)
                //{
                //    DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at Iframe Binding 6:-> " + Session["admin"] + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :->" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Sql Query:-> " + strtrace);
                //}

                ////**********************************************************************************



                //*********************************************
                //drpHoursspent.SelectedValue = "last7days";
                //************************************************

                try
                {
                    BindWebsiteURL();
                    BindPostedBy();
                    getFilterSession();
                    drpType.SelectedValue = "Tasks";
                    //**********************************
                    //drpHoursspent.Visible = true;
                    //*********************************
                    divTotalHrs.Visible = true;
                    BindTaskData();
                    dvCR.Visible = false;
                    dvTask.Visible = true;
                }
                catch (Exception ex)
                {
                    DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at Page Load -Tasks-> " + Session["admin"] + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :->" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "PageLoad:-> ");
                }
            }
            else
            {
                if (Session["filter"].ToString() == "NewCR")
                {
                    try
                    {

                        drpStatus.SelectedValue = "new";
                        BindWebsiteURL();
                        BindPostedBy();
                        getFilterSession();
                        drpType.SelectedValue = "CR";
                        //***********************************
                        //drpHoursspent.Visible = false;
                        //**********************************
                        divTotalHrs.Visible = false;
                        BindData();
                    }
                    catch (Exception ex)
                    {
                        DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at Page Load -NewCR-> " + Session["admin"] + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :->" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "PageLoad:-> ");
                    }
                }
                else if (Session["filter"].ToString() == "CRwocr")
                {
                    try
                    {
                        drpStatus.SelectedValue = "awaiting client response- required";
                        lblOrderBy.Text = " ORDER BY LastUpdatedOn desc";
                        BindWebsiteURL();
                        BindPostedBy();
                        getFilterSession();
                        drpType.SelectedValue = "CR";
                        //****************************************
                        //drpHoursspent.Visible = false;
                        //*****************************************
                        divTotalHrs.Visible = false;
                        BindData();
                    }
                    catch (Exception ex)
                    {
                        DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at Page Load -CRwocr-> " + Session["admin"] + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :->" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "PageLoad:-> ");
                    }

                }
                else if (Session["filter"].ToString() == "CRNewComment")
                {
                    try
                    {
                        lblOrderBy.Text = " ORDER BY LastUpdatedOn desc";
                        BindWebsiteURL();
                        BindPostedBy();
                        getFilterSession();
                        drpType.SelectedValue = "CR";
                        //**************************************
                        // drpHoursspent.Visible = false;
                        //****************************************
                        divTotalHrs.Visible = false;
                        BindData2();
                    }
                    catch (Exception ex)
                    {
                        DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at Page Load -CRNewComment-> " + Session["admin"] + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :->" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "PageLoad:-> ");
                    }

                }
                else
                {
                    try
                    {
                        lblOrderBy.Text = " ORDER BY LastUpdatedOn desc";
                        BindWebsiteURL();
                        BindPostedBy();
                        getFilterSession();
                        drpType.SelectedValue = "Tasks";
                        //****************************************
                        //drpHoursspent.Visible = true;
                        //**************************************
                        BindTaskData();
                        dvCR.Visible = false;
                        dvTask.Visible = true;
                    }
                    catch (Exception ex)
                    {
                        DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at Page Load -Tasks with else part-> " + Session["admin"] + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :->" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "PageLoad:-> ");
                    }

                }

            }

        }



        //===========================================================================================
        //b.deleted<>1 and  b.status <> 'checked' and b.status <>'parked' and b.status <>'closed' and
        //-------------Todays Time Spend------------------
        try
        {
            string T_fini_null = "", T_both = "", T_start_null = "", T_both_blank = "";
            DataSet ds_fini_null = null, ds_T_both = null, ds_start_null = null, ds_both_blank = null;
            int Total_Minute = 0, intF = 0, intS = 0, intT = 0, intL = 0;
            string result = "";

            //*********************when task is today start and finished date is Null means task is CWO*************************
            T_fini_null = @"Select sum(DATEDIFF(Minute,a.started_date,GETDATE())) as TF_Minute from dbo.hours_reporting  a inner Join tasks b on
              a.task_id = b.task_id 
              where b.deleted<>1 and status <> 'checked' and b.status <> 'closed' and b.status <> 'Checked' and b.status <>'parked' and a.finished_date is Null and b.project='" + Request.QueryString["projectname"].ToString() + @"' and 
              (a.started_date > CONVERT(datetime, CONVERT(varchar,DATEADD(day, 0, GETDATE()), 102)) and a.started_date < CONVERT(datetime, CONVERT(varchar,DATEADD(day, 1, GETDATE()), 102)))";
            ds_fini_null = DatabaseHelper.getDataset(T_fini_null);

            //*********************when task started and completed on same day*************************************************************
            T_both = @"Select SUM(a.minutes) as TS_Minute from dbo.hours_reporting  a
         inner Join tasks b on
         a.task_id = b.task_id 
         where b.deleted<>1 and status <> 'checked' and b.status <> 'closed' and b.status <> 'Checked' and b.status <>'parked' and a.finished_date is Not Null and b.project='" + Request.QueryString["projectname"].ToString() + @"' and 
         (a.started_date > CONVERT(datetime, CONVERT(varchar,DATEADD(day, 0, GETDATE()), 102)) and a.started_date < CONVERT(datetime, CONVERT(varchar,DATEADD(day, 1, GETDATE()), 102))) 
         and (a.finished_date > CONVERT(datetime, CONVERT(varchar,DATEADD(day, 0, GETDATE()), 102)) and a.finished_date < CONVERT(datetime, CONVERT(varchar,DATEADD(day, 1, GETDATE()), 102))) ";
            ds_T_both = DatabaseHelper.getDataset(T_both);


            //*********************when task started on before today but completed on today **********************************
            T_start_null = @"Select Sum(Datediff(Minute,CONVERT(datetime, CONVERT(varchar,DATEADD(day, 0, GETDATE()), 102)),a.finished_date)) as TT_Minute from dbo.hours_reporting  a 
              inner Join tasks b on
              a.task_id = b.task_id 
              where b.deleted<>1 and status <> 'checked' and b.status <> 'closed' and b.status <> 'Checked' and b.status <>'parked' and a.finished_date is Not Null and b.project='" + Request.QueryString["projectname"].ToString() + @"' and 
              a.started_date < CONVERT(datetime, CONVERT(varchar,DATEADD(day, 0, GETDATE()), 102)) and
              (a.finished_date > CONVERT(datetime, CONVERT(varchar,DATEADD(day, 0, GETDATE()), 102))   and a.finished_date < CONVERT(datetime, CONVERT(varchar,DATEADD(day, 1, GETDATE()), 102))) ";
            ds_start_null = DatabaseHelper.getDataset(T_start_null);

            //*********************when task is created before today and not completed today means start date and completed date is not today *****************************************

            //T_both_blank = @"Select Sum(Datediff(Minute,CONVERT(datetime, CONVERT(varchar,DATEADD(day, 0, GETDATE()), 102)),CONVERT(datetime, CONVERT(varchar,DATEADD(day, 1, GETDATE()), 102)))) as Today_Minute from dbo.hours_reporting  a 
            //inner Join tasks b on a.task_id = b.task_id 
            //where a.finished_date is Null and b.project='" + Request.QueryString["projectname"].ToString() + @"' and 
            //a.started_date < CONVERT(datetime, CONVERT(varchar,DATEADD(day, 0, GETDATE()), 102))";


            T_both_blank = @"Select Sum(Datediff(Minute,CONVERT(datetime, CONVERT(varchar,DATEADD(day, 0, GETDATE()), 102)),GETDATE())) as Today_Minute from dbo.hours_reporting  a 
        inner Join tasks b on a.task_id = b.task_id 
        where b.deleted<>1 and status <> 'checked' and b.status <> 'closed' and b.status <> 'Checked' and b.status <>'parked' and a.finished_date is Null and b.project='" + Request.QueryString["projectname"].ToString() + @"' and 
        a.started_date < CONVERT(datetime, CONVERT(varchar,DATEADD(day, 0, GETDATE()), 102))";


            ds_both_blank = DatabaseHelper.getDataset(T_both_blank);





            if (ds_fini_null != null && ds_fini_null.Tables.Count > 0 && ds_fini_null.Tables[0].Rows.Count > 0)
            {
                if (ds_fini_null.Tables[0].Rows[0]["TF_Minute"].ToString() != "")
                {
                    intF = Convert.ToInt32(ds_fini_null.Tables[0].Rows[0]["TF_Minute"].ToString());
                }
            }

            if (ds_T_both != null && ds_T_both.Tables.Count > 0 && ds_T_both.Tables[0].Rows.Count > 0)
            {
                if (ds_T_both.Tables[0].Rows[0]["TS_Minute"].ToString() != "")
                {
                    intS = Convert.ToInt32(ds_T_both.Tables[0].Rows[0]["TS_Minute"].ToString());
                }
            }

            if (ds_start_null != null && ds_start_null.Tables.Count > 0 && ds_start_null.Tables[0].Rows.Count > 0)
            {
                if (ds_start_null.Tables[0].Rows[0]["TT_Minute"].ToString() != "")
                {
                    intT = Convert.ToInt32(ds_start_null.Tables[0].Rows[0]["TT_Minute"].ToString());
                }
            }
            if (ds_both_blank != null && ds_both_blank.Tables.Count > 0 && ds_both_blank.Tables[0].Rows.Count > 0)
            {
                if (ds_both_blank.Tables[0].Rows[0]["Today_Minute"].ToString() != "")
                {
                    intL = Convert.ToInt32(ds_both_blank.Tables[0].Rows[0]["Today_Minute"].ToString());
                }
            }

            Total_Minute = intF + intS + intT + intL;

            int MouH = 0;
            MouH = Total_Minute % 480;

            if (Total_Minute / 480 != 0)
            {
                result = +Total_Minute / 480 + " Days ";
            }
            if (MouH / 60 != 0)
            {
                result += (MouH / 60) + " Hours ";
            }
            if (MouH % 60 != 0)
            {
                result += (MouH % 60) + " Mins ";
            }

            lbltodaytime.Text = " " + result + " | ";
            //============Yesterday Task ==============================================================================

            string Y_fini_null = "", Y_both = "", Y_start_null = "", Y_both_blank = "", Yresult = "";
            DataSet dsY_fini_null = null, dsY_T_both = null, dsY_start_null = null, dsY_both_blank = null;
            int YTotal_Minute = 0, YintF = 0, YintS = 0, YintT = 0, YintL = 0;

            //**********When Yesterday task is start on  but not completed and it is CWO********************


            Y_fini_null = @"Select sum(DATEDIFF(Minute,a.started_date,CONVERT(datetime, CONVERT(varchar,DATEADD(day, 0, GETDATE()), 102)))) as YF_Minute from dbo.hours_reporting  a
                            inner Join tasks b on
                            a.task_id = b.task_id 
                            where b.deleted<>1 and status <> 'checked' and b.status <> 'closed' and b.status <> 'Checked' and b.status <>'parked' and a.finished_date is Null and b.project='" + Request.QueryString["projectname"].ToString() + @"' and 
                            (a.started_date > CONVERT(datetime, CONVERT(varchar,DATEADD(day, -1, GETDATE()), 102)) and a.started_date <  CONVERT(datetime, CONVERT(varchar,DATEADD(day, 0, GETDATE()), 102)))";
            dsY_fini_null = DatabaseHelper.getDataset(Y_fini_null);

            //**********When Yesterday task is start and completed ***************************************
            Y_both = @"Select sum(a.minutes) as YS_Minute from dbo.hours_reporting  a
                    inner Join tasks b on
                    a.task_id = b.task_id 
                    where b.deleted<>1 and status <> 'checked' and b.status <> 'closed' and b.status <> 'Checked' and b.status <>'parked' and a.finished_date is Not Null and b.project='" + Request.QueryString["projectname"].ToString() + @"' and 
                    (a.started_date > CONVERT(datetime, CONVERT(varchar,DATEADD(day, -1, GETDATE()), 102)) and a.started_date < CONVERT(datetime, CONVERT(varchar,DATEADD(day, 0, GETDATE()), 102)))
                    and (a.finished_date < CONVERT(datetime,CONVERT(varchar,DATEADD(day, 0, GETDATE()), 102)) and a.finished_date > CONVERT(datetime,CONVERT(varchar,DATEADD(day, -1, GETDATE()), 102)))";

            dsY_T_both = DatabaseHelper.getDataset(Y_both);

            //**********When  before Yesterday task is start and completed Yesterday***********************
            Y_start_null = @"Select Sum(DATEDIFF(MINUTE,CONVERT(datetime, CONVERT(varchar,DATEADD(day, -1, GETDATE()), 102)),a.finished_date)) as TT_Minute from dbo.hours_reporting a  
                    inner Join tasks b on a.task_id = b.task_id 
                                where b.deleted<>1 and status <> 'checked' and b.status <> 'closed' and b.status <> 'Checked' and b.status <>'parked' and a.finished_date is Not Null and b.project='" + Request.QueryString["projectname"].ToString() + @"' and 
                    a.started_date < CONVERT(datetime, CONVERT(varchar,DATEADD(day, -1, GETDATE()), 102)) and
                   (a.finished_date < CONVERT(datetime, CONVERT(varchar,DATEADD(day, 0, GETDATE()), 102)) and  a.finished_date > CONVERT(datetime, CONVERT(varchar,DATEADD(day, -1, GETDATE()), 102)))";

            dsY_start_null = DatabaseHelper.getDataset(Y_start_null);
            //***********when task start before Yestarday but not completed Yesterday***********************

            Y_both_blank = @"Select Sum(Datediff(Minute,CONVERT(datetime, CONVERT(varchar,DATEADD(day, -1, GETDATE()), 102)),CONVERT(datetime, CONVERT(varchar,DATEADD(day, 0, GETDATE()), 102)))) as Yesterday_Minute from dbo.hours_reporting  a 
                            inner Join tasks b on a.task_id = b.task_id 
                                        where b.deleted<>1 and status <> 'checked' and b.status <> 'closed' and b.status <> 'Checked' and b.status <>'parked' and a.finished_date is Null and b.project='" + Request.QueryString["projectname"].ToString() + @"' and 
                            a.started_date < CONVERT(datetime, CONVERT(varchar,DATEADD(day, -1, GETDATE()), 102))";

            dsY_both_blank = DatabaseHelper.getDataset(Y_both_blank);


            if (dsY_fini_null != null && dsY_fini_null.Tables.Count > 0 && dsY_fini_null.Tables[0].Rows.Count > 0)
            {
                if (dsY_fini_null.Tables[0].Rows[0]["YF_Minute"].ToString() != "")
                {
                    YintF = Convert.ToInt32(dsY_fini_null.Tables[0].Rows[0]["YF_Minute"].ToString());
                }
            }
            if (dsY_T_both != null && dsY_T_both.Tables.Count > 0 && dsY_T_both.Tables[0].Rows.Count > 0)
            {
                if (dsY_T_both.Tables[0].Rows[0]["YS_Minute"].ToString() != "")
                {
                    YintS = Convert.ToInt32(dsY_T_both.Tables[0].Rows[0]["YS_Minute"].ToString());
                }
            }
            if (dsY_start_null != null && dsY_start_null.Tables.Count > 0 && dsY_start_null.Tables[0].Rows.Count > 0)
            {
                if (dsY_start_null.Tables[0].Rows[0]["TT_Minute"].ToString() != "")
                {
                    YintT = Convert.ToInt32(dsY_start_null.Tables[0].Rows[0]["TT_Minute"].ToString());
                }
            }
            if (dsY_both_blank != null && dsY_both_blank.Tables.Count > 0 && dsY_both_blank.Tables[0].Rows.Count > 0)
            {
                if (dsY_both_blank.Tables[0].Rows[0]["Yesterday_Minute"].ToString() != "")
                {
                    YintL = Convert.ToInt32(dsY_both_blank.Tables[0].Rows[0]["Yesterday_Minute"].ToString());
                }
            }

            YTotal_Minute = YintF + YintS + YintT + YintL;
            int YMouH = 0;
            YMouH = YTotal_Minute % 480;
            if (YTotal_Minute / 480 != 0)
            {
                Yresult = YTotal_Minute / 480 + " Days ";
            }
            if (YMouH / 60 != 0)
            {
                Yresult += (YMouH / 60) + " Hours ";
            }
            if (YMouH % 60 != 0)
            {
                Yresult += (YMouH % 60) + " Mins ";
            }
            lblyesterdaytime.Text = " " + Yresult + "|";
            //============ End Yesterday Task ==============================================================================

            //==================Week Day time===============================================================================
            string TotalseventimeQry = "";
            DataSet dssevendaytime = null;
            int TotalsevenMinute = 0;
            int SumAllSeven = 0;

            TotalseventimeQry = @"Select sum(a.minutes) as sevenday_Minute from dbo.hours_reporting  a
                    inner Join tasks b on
                    a.task_id = b.task_id 
                    where b.deleted<>1 and status <> 'checked' and b.status <> 'closed' and b.status <> 'Checked' and b.status <>'parked' and b.project='" + Request.QueryString["projectname"].ToString() + @"' and 
                    (a.started_date > CONVERT(datetime, CONVERT(varchar,DATEADD(day, -7, GETDATE()), 102)) and a.finished_date < CONVERT(datetime,CONVERT(varchar,DATEADD(day, -1, GETDATE()), 102))) ";

            dssevendaytime = DatabaseHelper.getDataset(TotalseventimeQry);
            if (dssevendaytime != null && dssevendaytime.Tables.Count > 0 && dssevendaytime.Tables[0].Rows.Count > 0)
            {
                if (dssevendaytime.Tables[0].Rows[0]["sevenday_Minute"].ToString() != "")
                {
                    TotalsevenMinute = Convert.ToInt32(dssevendaytime.Tables[0].Rows[0]["sevenday_Minute"].ToString());
                }
            }
            SumAllSeven = TotalsevenMinute + YTotal_Minute + Total_Minute;
            int TotalsevenMOD = 0;
            string WeekDay = "";
            TotalsevenMOD = SumAllSeven % 480;
            if ((SumAllSeven / 480) != 0)
            {
                WeekDay += (SumAllSeven / 480) + " Days ";

            }
            if ((TotalsevenMOD / 60) != 0)
            {
                WeekDay += (TotalsevenMOD / 60) + " Hours ";
            }
            if ((TotalsevenMOD % 60) != 0)
            {
                WeekDay += (TotalsevenMOD % 60) + " Mins ";
            }
            lblsevenminute.Text = " " + WeekDay + " | ";


            //=======================End week day Time=============================================================================

            //=======================start one Month time==========================================================================
            string TotalMonthtimeQry = "";
            DataSet dsMonthdaytime = null;
            int TotalMonthMinute = 0;
            int allMonthTime = 0;
            TotalMonthtimeQry = @"Select sum(a.minutes) as monthday_Minute from dbo.hours_reporting  a
                           inner Join tasks b on
                            a.task_id = b.task_id 
                           where b.deleted<>1 and status <> 'checked' and b.status <> 'closed' and b.status <> 'Checked' and b.status <>'parked' and b.project='" + Request.QueryString["projectname"].ToString() + @"' and 
                            (a.started_date > CONVERT(datetime, CONVERT(varchar,DATEADD(day, -30, GETDATE()), 102)) and a.finished_date < CONVERT(datetime,CONVERT(varchar,DATEADD(day, -7, GETDATE()), 102)))";

            dsMonthdaytime = DatabaseHelper.getDataset(TotalMonthtimeQry);
            if (dsMonthdaytime != null && dsMonthdaytime.Tables.Count > 0 && dsMonthdaytime.Tables[0].Rows.Count > 0)
            {
                if (dsMonthdaytime.Tables[0].Rows[0]["monthday_Minute"].ToString() != "")
                {
                    TotalMonthMinute = Convert.ToInt32(dsMonthdaytime.Tables[0].Rows[0]["monthday_Minute"].ToString());
                }
            }
            allMonthTime = TotalMonthMinute + SumAllSeven;
            int TotalMonthMinuteMOD = 0;
            TotalMonthMinuteMOD = allMonthTime % 480;
            string MonthDay = "";

            if ((allMonthTime / 480) != 0)
            {
                MonthDay += (allMonthTime / 480) + " Days ";
            }
            if ((TotalMonthMinuteMOD / 60) != 0)
            {
                MonthDay += (TotalMonthMinuteMOD / 60) + " Hours ";
            }
            if ((TotalMonthMinuteMOD % 60) != 0)
            {
                MonthDay += (TotalMonthMinuteMOD % 60) + " Mins ";
            }
            //lblTotalMonthMinute.Text = " " + allMonthTime / 480 + " Days " + TotalMonthMinuteMOD / 60 + " Hours " + TotalMonthMinuteMOD % 60 + " Mins |";
            lblTotalMonthMinute.Text = " " + MonthDay + "|";
            //=======================End one Month time============================================================================

            //===============Start spend total time ===============================================================================
            string Totalspendtime = "";
            DataSet dsspendtime = null;
            int TotalMinute = 0;
            Totalspendtime = @"select sum(minutes) as Total_Minute from hours_reporting  a
                            inner Join tasks b on
                            a.task_id = b.task_id 
                          where  b.project='" + Request.QueryString["projectname"].ToString() + @"'";
            dsspendtime = DatabaseHelper.getDataset(Totalspendtime);
            if (dsspendtime != null && dsspendtime.Tables.Count > 0 && dsspendtime.Tables[0].Rows.Count > 0)
            {
                if (dsspendtime.Tables[0].Rows[0]["Total_Minute"].ToString() != "")
                {
                    TotalMinute = Convert.ToInt32(dsspendtime.Tables[0].Rows[0]["Total_Minute"].ToString());
                }
            }
            int TotalMOD = 0;
            TotalMOD = TotalMinute % 480;
            string totalTime = "";
            if (TotalMinute / 480 != 0)
            {
                totalTime += (TotalMinute / 480) + " Days ";
            }
            if ((TotalMOD / 60) != 0)
            {
                totalTime += (TotalMOD / 60) + " Hours ";
            }
            if ((TotalMOD % 60) != 0)
            {
                totalTime += 60 + " Mins";
            }
            lbltotalspendMinute.Text = " " + totalTime + "|";
            //===============End spend total time ===============================================================================
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at DateTime Calculation in Project Request:-> " + Session["admin"] + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :->" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "DateTime Calculation: ");
        }
    }
    public string[] RemoveDuplicatesValue(string[] myList)
    {

        System.Collections.ArrayList newList = new System.Collections.ArrayList();

        foreach (string str in myList)

            if (!newList.Contains(str))

                newList.Add(str);

        return (string[])newList.ToArray(typeof(string));

    }
    private void BindTaskData()
    {
        //************************************
        //lblhrspent.Visible = true;
        //drpHoursspent.Visible = true;
        string CreateQueryInfo = "";
        //**************************************
        try
        {
            dvheader.Visible = true;

            lblOrderBy.Text = " ORDER BY last_updated_date desc";
            string filter = " where ";

            string filter2 = "";

            string topFilterLable = "";

            if (!DatabaseHelper.can_Show_Closed_Tasks(Session["admin"].ToString()))
            {
                if (Session["filterunread"] == null || Session["filterunread"].ToString() == "")
                {

                    filter += " status <> 'closed' and status <> 'Checked' and status <>'parked' and  ";
                }
                else
                {
                    filter += " status <> 'closed' and ";
                }

                chkHideClosed.Visible = false;
            }
            else
            {
                if (chkHideClosed.Checked == true)
                {
                    if (Session["filterunread"] == null || Session["filterunread"].ToString() == "")
                    {
                        filter += " status <> 'checked' and status <>'parked' and status = 'closed' and ";
                    }

                }
                else
                {
                    if (Session["filterunread"] == null || Session["filterunread"].ToString() == "")
                    {

                        filter += " status <> 'closed' and status <> 'Checked' and status <>'parked' and ";

                    }
                    else
                    {

                        filter += " status <> 'closed' and  ";

                    }
                }
            }


            if (chkViewDeleted.Checked == true)
            {
                filter += " deleted = 1 ";
            }
            else
            {

                filter += " deleted <> 1 ";

            }

            if (drpWebsiteUrl.SelectedIndex != 0)
            {
                filter += " and (project = '" + drpWebsiteUrl.SelectedValue + "' or company = '" + drpWebsiteUrl.SelectedValue + "' )";

                if (topFilterLable == "")
                {
                    topFilterLable = "Filtered by Company Name: " + drpWebsiteUrl.SelectedItem.Text;
                }
                else
                {
                    topFilterLable += " / Company Name: " + drpWebsiteUrl.SelectedItem.Text;
                }
            }
            else if (projectname != "")
            {
                filter += " and (project = '" + projectname + "' )";

            }
            else
                filter += " and (project = '" + drpProjects.SelectedValue + "' )";

            if (drpChangeBy.SelectedIndex != 0)
            {
                filter += " and reported_user = '" + drpChangeBy.SelectedValue + "' ";


                if (topFilterLable == "")
                {
                    topFilterLable = "Filtered by Reported by: " + drpChangeBy.SelectedItem.Text;
                }
                else
                {
                    topFilterLable += " / Reported by:" + drpChangeBy.SelectedItem.Text;
                }
            }

            if (hidecheck == false)
            {
                if (drpPriority.SelectedIndex != 0)
                {
                    if (drpPriority.SelectedValue == "00")
                    {
                        filter += " and priority = '" + drpPriority.SelectedItem.Text + "' ";

                    }
                    else if (drpPriority.SelectedValue == "1")
                    {
                        filter += " and (priority = '1a - DO NOW' or priority = '1b - high' or priority = '1c - normal') ";
                    }
                    else if (drpPriority.SelectedValue == "2")
                    {
                        filter += " and priority = '2 - not urgent' ";
                    }
                    else if (drpPriority.SelectedValue == "3")
                    {
                        filter += " and priority = '3 - low priority' ";
                    }
                    else if (drpPriority.SelectedValue == "4")
                    {
                        filter += " and priority = '4 - very low priority' ";
                    }
                    else
                    {
                        filter += " and priority = '5 - parked' ";
                    }

                    if (topFilterLable == "")
                    {
                        topFilterLable = "Filtered by Priority: " + drpPriority.SelectedItem.Text;
                    }
                    else
                    {
                        topFilterLable += " / Priority:" + drpPriority.SelectedItem.Text;
                    }
                }
            }

            if (drpStatus.SelectedIndex != 0)
            {
                if (drpStatus.SelectedValue == "CWO")
                {
                    filter = filter.Replace("and status <> 'Checked'", "").Replace("and status <>'parked'", "");
                    filter += " and status = '" + drpStatus.SelectedValue + "' ";

                    if (topFilterLable == "")
                    {
                        topFilterLable = "Filtered by Status: " + drpStatus.SelectedItem.Text;
                    }
                    else
                    {
                        topFilterLable += " / Status: " + drpStatus.SelectedItem.Text;
                    }
                }
                else if (drpStatus.SelectedValue == "CWO but away")
                {
                    filter = filter.Replace("and status <> 'Checked'", "").Replace("and status <>'parked'", "");
                    filter += " and status =  '" + drpStatus.SelectedValue + "' ";

                    if (topFilterLable == "")
                    {
                        topFilterLable = "Filtered by Status: " + drpStatus.SelectedItem.Text;
                    }
                    else
                    {
                        topFilterLable += " / Status: " + drpStatus.SelectedItem.Text;
                    }
                }
                else
                {
                    filter = filter.Replace("and status <> 'Checked'", "").Replace("and status <>'parked'", "");
                    filter += " and status =  '" + drpStatus.SelectedValue + "' ";

                    if (topFilterLable == "")
                    {
                        topFilterLable = "Filtered by Status: " + drpStatus.SelectedItem.Text;
                    }
                    else
                    {
                        topFilterLable += " / Status: " + drpStatus.SelectedItem.Text;
                    }
                }

            }

            if (chkViewDeleted.Checked == true)
            {
                if (topFilterLable == "")
                {
                    topFilterLable = "Filtered by View only deleted";
                }
                else
                {
                    topFilterLable += " / View only deleted";
                }
            }

            if (chkHideClosed.Checked == true)
            {
                if (topFilterLable == "")
                {
                    topFilterLable = "Filtered by view closed";
                }
                else
                {
                    topFilterLable += " / view closed";
                }
            }

            TopFilterLable.InnerHtml = topFilterLable;
            if (Session["filterunread"] == null || Session["filterunread"].ToString() == "")
            {
                //***********************************************************
                //if (drpHoursspent.SelectedValue != "")
                //{
                //    sql = @"declare @Date datetime " +
                //           "set @Date= dateadd(dd, datediff(dd, " + drpHoursspent.SelectedValue + ", getDate()), 0) " +
                //           "select * from tasks where task_id in (select distinct task_id from hours_reporting where finished_date is not null and task_id in " +
                //           "(select task_id from tasks " + filter +
                //            " )" + filter2 +
                //           " and (convert(varchar(10),started_date,120) >= @Date and convert(varchar(10),started_date,120) <= GETDATE())) " +
                //           lblOrderBy.Text;
                //}
                //else
                //{
                sql = "select * from tasks " + filter + " " + lblOrderBy.Text;

                //}
                CreateQueryInfo = "Session[filterunread] == null || Session[filterunread].ToString()";
                //***********************************************************
            }

            else
            {
                if (Session["filterunread"].ToString() == "TaskNewComment")
                {

                    string striFilter = "";
                    striFilter = filter;
                    sql = "select * from tasks where task_id in(select task_comments.task_id from task_comments where tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 )" +
                            " and project ='" + Request.QueryString["projectname"].ToString() + "'";
                    sql += striFilter.Replace("where", "and") + lblOrderBy.Text;
                    CreateQueryInfo = "TaskNewComment";
                }

                else if (Session["filterunread"].ToString() == "AllTaskNewComment")
                {
                    sql = "select  tasks.* from tasks where task_id in(select task_comments.task_id from task_comments where tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 )" +
                            " and project ='" + Request.QueryString["projectname"].ToString() + "'";
                    if (Session["boolcount"] == "true")
                    {
                        sql += lblOrderBy.Text;
                    }
                    else
                    {

                        if (hidecheck == false)
                        {
                            sql += filter + " " + lblOrderBy.Text;
                        }
                        else
                        {
                            sql += lblOrderBy.Text;
                        }

                    }
                    CreateQueryInfo = "AllTaskNewComment";

                }
                else
                {
                    sql = "select * from tasks " + filter + " " + lblOrderBy.Text;
                }
            }

            if (Session["OverdueTask"] == null || Session["OverdueTask"].ToString() == "")
            { }
            else
            {
                CreateQueryInfo = "Session[OverdueTask] == null || Session[OverdueTask].ToString()";
                string strduedate = "";
                sql = "select * from tasks " + filter + " and deleted <> 1 and status <> 'checked' and status <> 'parked' and DueDate <> '" + strduedate + "'  " + lblOrderBy.Text;

            }
            DataSet ds = null;
            try
            {
                ds = DatabaseHelper.getDataset(sql);
                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Selected Query from BindTaskData():-> " + Session["admin"] + "  DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm") +" From which condition data is enter:- " + CreateQueryInfo, sql);
            }
            catch (Exception ex)
            {
                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at BindTaskData:-> " + Session["admin"] + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :->" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "BindTaskData: " + sql);
            }

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                ds = formatTaskDataSet(ds);
            }

            if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0].Rows.Count <= 0)
            {
                divMessage.InnerHtml = "No task yet.";
                gvTask.DataSource = null;
                gvTask.DataBind();
                //LblRecordsno.Text = "0";
                lblTaskRecords.Text = "0";
                divTotalHrs.Visible = false;
                dvheader.Visible = false;

                if (closecount != 0)
                {
                    chkHideClosed.Checked = true;
                    closecount = 0;
                }
                else
                {
                    chkHideClosed.Checked = false;
                }

                hidecheck = false;


            }
            else
            {
                dvTask.Visible = true;
                //LblRecordsno.Text = ds.Tables[0].Rows.Count.ToString();
                //gvTask.PageIndex = 0;
                divMessage.InnerHtml = "";
                gvTask.Visible = true;
                gvTask.DataSource = null;
                gvTask.DataBind();
                gvTask.DataSource = ds.Tables[0];
                gvTask.DataBind();
                lblTaskRecords.Text = ds.Tables[0].Rows.Count.ToString();
                //==============================
                setRowValue(ds);
                //==============================
                if (closecount != 0)
                {
                    chkHideClosed.Checked = true;
                    closecount = 0;
                }
                else
                {
                    chkHideClosed.Checked = false;
                }

                string taskid = "";

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        taskid += ds.Tables[0].Rows[i]["task_id"].ToString() + ", ";
                    }

                }

                int length = taskid.Length;
                taskid = taskid.Remove(length - 2, 1);
                getTotalHrsReport(taskid);
                divTotalHrs.Visible = true;

            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at Iframe Binding 6:-> " + Session["admin"] + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :->" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "BindTaskData: ");

        }
    }

    private void setRowValue(DataSet ds)
    {
        //===================================================================================
        try
        {
            bool chkbool;
            chkbool = DatabaseHelper.can_Delete_Tasks(Session["admin"].ToString());

            //***************************************************
            //for (int i = 0; i < DataGrid1.Rows.Count; i++)
            for (int i = 0; i < gvTask.Rows.Count; i++)
            //***************************************************
            {
                //********************************************************************
                string countvalue = ds.Tables[0].Rows[i]["ShowReadUnread"].ToString();
                //********************************************************************
                LinkButton readMarkButton = (LinkButton)gvTask.Rows[i].FindControl("lnkReadMark");
                if (countvalue == "False")
                {
                    readMarkButton.Text = "[<span style='color:red'>Unread.</span>  Mark as read]";
                }
                else
                {
                    readMarkButton.Text = "[<span style='color:green'>Read.</span> Mark as unread]";
                }
           }
        }
        catch (Exception Ex)
        { }
        //=================================================================================

    }
    protected void getTotalHrsReport(string strSql)
    {

        try
        {
            sql = @"select sum(ETC)
                from tasks
                where task_id in (" + strSql + ")";

            int ETC = 0;

            object objETC = DatabaseHelper.executeScalar(sql);

            if (objETC != null && objETC.ToString() != "" && objETC.ToString() != "0")
            {
                ETC += (int)objETC;
            }
            else
            {
                ETC = 0;
            }

            sql = @"select sum(datediff(minute,started_date,finished_date))
                from hours_reporting
                where finished_date is not null and task_id in (" + strSql + ")";

            int hrsTaken = 0;
            object objHrsTaken = DatabaseHelper.executeScalar(sql);
            if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
            {
                hrsTaken += (int)objHrsTaken;
            }
            else
            {
                hrsTaken = 0;
            }

            sql = @"select sum(datediff(minute,started_date,getdate()))
                from hours_reporting
                where finished_date is null and task_id in (" + strSql + ")";

            objHrsTaken = DatabaseHelper.executeScalar(sql);

            if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
            {
                hrsTaken += (int)objHrsTaken;
            }


            lblETC.Text = "" + (ETC / 60) + " hrs " + (ETC % 60) + " mins";

            lblTotalHrsTaken.Text = "" + (hrsTaken / 60) + " hrs " + (hrsTaken % 60) + " mins";

            int hrsLeft = ETC - hrsTaken;

            if (hrsLeft < 0)
            {
                hrsLeft = -1 * hrsLeft;
                lblExpectedHrsLeft.Text = "-" + (hrsLeft / 60) + " hrs " + (hrsLeft % 60) + " mins";
            }
            else
            {
                lblExpectedHrsLeft.Text = "" + (hrsLeft / 60) + " hrs " + (hrsLeft % 60) + " mins";
            }

            if (lblETC.Text.ToString() == "" && lblTotalHrsTaken.Text.ToString() == "" && lblExpectedHrsLeft.Text.ToString() == "")
                divTotalHrs.Visible = false;

        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at getTotalHrsReport:-> " + Session["admin"] + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :->" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "getTotalHrsReport: " + sql);
        }
    }


    protected int getHoursTakenSoFar(string task_id)
    {
        int hrsTaken = 0;
        try
        {

            sql = @"select sum(datediff(minute,started_date,finished_date))
                from hours_reporting
                where finished_date is not null and task_id=" + task_id.ToString();


            object objHrsTaken = DatabaseHelper.executeScalar(sql);
            if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
            {
                hrsTaken += (int)objHrsTaken;
            }
            else
            {
                hrsTaken = 0;
            }

            sql = @"select sum(datediff(minute,started_date,getdate()))
                from hours_reporting
                where finished_date is null and task_id=" + task_id.ToString();

            objHrsTaken = DatabaseHelper.executeScalar(sql);

            if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
            {
                hrsTaken += (int)objHrsTaken;
            }

            return hrsTaken;
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at getHoursTakenSoFar 1:-> " + Session["admin"] + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :->" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "getTotalHrsReport: " + sql);
        }
        return hrsTaken;
    }

    void load_statuses()
    {

        try
        {
            string sql = "";
            DataSet ds_dropdowns;

            if (Cache["dropdownsNew"] == null)
            {
                //projects


                sql = @"select project_name from projects where archive!='Y' and active = 'Y' order by project_name;";


                // categories
                sql += "\nselect category_name from categories order by sort_seq, category_name;";

                // priorities
                sql += "\nselect priority_name from priorities order by sort_seq, priority_name;";

                // statuses
                sql += "\nselect status_name from statuses order by sort_seq, status_name;";

                // users
                sql += "\nselect username from users where active = 'Y' order by username;";

                ds_dropdowns = DatabaseHelper.getDataset(sql);
                //  Cache["Company_notes"] = ds;
                Cache.Insert("dropdownsNew", ds_dropdowns, null, DateTime.Now.AddHours(6), TimeSpan.Zero);
            }
            else
            {
                ds_dropdowns = (DataSet)Cache["dropdownsNew"];
            }

            if (ds_dropdowns.Tables.Count > 0)
            {
                if (ds_dropdowns.Tables[0].Rows.Count > 0)
                {
                    //drpProjects.DataSource = ds_dropdowns.Tables[0];
                    //drpProjects.DataTextField = "project_name";
                    //drpProjects.DataValueField = "project_name";
                    //drpProjects.DataBind();
                    //drpProjects.SelectedValue = Request.QueryString["projectname"].ToString();
                }

                if (ds_dropdowns.Tables[3].Rows.Count > 0)
                {
                    drpStatus.DataSource = ds_dropdowns.Tables[3];
                    drpStatus.DataTextField = "status_name";
                    drpStatus.DataValueField = "status_name";
                    drpStatus.DataBind();

                    DrpMultipleTaskStatus.DataSource = ds_dropdowns.Tables[3];
                    DrpMultipleTaskStatus.DataTextField = "status_name";
                    DrpMultipleTaskStatus.DataValueField = "status_name";
                    DrpMultipleTaskStatus.DataBind();
                }

                drpStatus.Items.Insert(0, new ListItem("[no status]", ""));
                DrpMultipleTaskStatus.Items.Insert(0, new ListItem("[no status]", "0"));
            }

        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at load_statuses 1:-> " + Session["admin"] + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :->" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "load_statuses: " + sql);
        }

    }

    private void BindData()
    {
        //*********************************
        // lblhrspent.Visible = false;
        //drpHoursspent.Visible = false;
        //************************************
        try
        {
            lblOrderBy.Text = " ORDER BY LastUpdatedOn desc";

            string strFilter = " where ";

            string topFilterLable = "";

            if (chkViewDeleted.Checked == true)
            {
                strFilter += " ClientRequest.deleted = 1 ";
            }
            else
            {
                strFilter += " ClientRequest.deleted <> 1 ";
            }

            if (chkHideClosed.Checked == true)
            {
                strFilter += " and ClientRequest.status = 'closed' ";
            }
            else
            {
                strFilter += " and ClientRequest.status <> 'closed' ";
            }

            if (drpChangeBy.SelectedIndex != 0)
            {
                char[] separator = new char[] { '+' };

                string[] strCid = drpChangeBy.SelectedItem.Value.Split(separator);

                strFilter += " and ClientRequest.ClientId = " + strCid[0] + " and ClientRequest.UserName='" + drpChangeBy.SelectedItem.Text + "' ";

                if (topFilterLable == "")
                {
                    topFilterLable = "Filtered by Change added by: " + drpChangeBy.SelectedItem.Text;
                }
                else
                {
                    topFilterLable += " / Change added by: " + drpChangeBy.SelectedItem.Text;
                }
            }

            if (drpStatus.SelectedIndex != 0)
            {
                strFilter += " and ClientRequest.Status = '" + drpStatus.SelectedItem.Value + "' ";

                if (topFilterLable == "")
                {
                    topFilterLable = "Filtered by Status: " + drpStatus.SelectedItem.Text;
                }
                else
                {
                    topFilterLable += " / Status: " + drpStatus.SelectedItem.Text;
                }
            }

            if (drpPriority.SelectedIndex != 0)
            {

                if (drpPriority.SelectedValue == "00")
                {
                    strFilter += " and ClientRequest.Priority = 0 '" + drpPriority.SelectedItem.Text + "' ";

                }
                else
                    strFilter += " and ClientRequest.Priority = " + drpPriority.SelectedItem.Value + " ";

                if (topFilterLable == "")
                {
                    topFilterLable = "Filtered by Priority: " + drpPriority.SelectedItem.Text;
                }
                else
                {
                    topFilterLable += " / Priority: " + drpPriority.SelectedItem.Text;
                }
            }

            if (drpWebsiteUrl.SelectedIndex != 0)
            {
                strFilter += " and CompanyName = '" + drpWebsiteUrl.SelectedItem.Value + "' ";

                if (topFilterLable == "")
                {
                    topFilterLable = "Filtered by Company Name: " + drpWebsiteUrl.SelectedItem.Text;
                }
                else
                {
                    topFilterLable += " / Company Name: " + drpWebsiteUrl.SelectedItem.Text;
                }
            }
            else if (projectname != "")
            {
               // strFilter += " and (projectName = '" + projectname + "') ";
                strFilter += " and project = '" + drpProjects.SelectedValue + "' ";
                //filter2 += " and project = '" + drpProjects.SelectedValue + "' ";
                //filter3 += " and project = '" + drpProjects.SelectedValue + "' ";
                if (topFilterLable == "")
                {
                    topFilterLable = "Filtered by Project: " + drpProjects.SelectedValue;
                }
                else
                {
                    topFilterLable += " / Project: " + drpProjects.SelectedValue;
                }
            }
            else
            {
                strFilter += "and (projectName = '" + Request.QueryString["projectname"].ToString() + "') ";
            }


            if (chkViewDeleted.Checked == true)
            {
                if (topFilterLable == "")
                {
                    topFilterLable = "Filtered by View only deleted";
                }
                else
                {
                    topFilterLable += " / View only deleted";
                }
            }

            if (chkHideClosed.Checked == true)
            {
                if (topFilterLable == "")
                {
                    topFilterLable = "Filtered by view closed";
                }
                else
                {
                    topFilterLable += " / View closed";
                }
            }

            TopFilterLable.InnerHtml = topFilterLable;
            DataSet ds = null;
            try
            {
                sql = "Select * from ClientRequest" + strFilter + " " + lblOrderBy.Text;
                ds = DatabaseHelper.getDataset(sql);
            }
            catch (Exception ex)
            {
                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at BindData 1:-> " + Session["admin"] + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :->" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "BindData: " + sql);
            }

            ds = formatCrsDataSet(ds);
            try
            {
                if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0].Rows.Count <= 0)
                {
                    divMessage.InnerHtml = "No request found.";
                    divMessage.Visible = true;
                    dvCrHeader.InnerHtml = "No requests found";
                    DataGrid1.DataSource = null;
                    DataGrid1.DataBind();
                    lblCrRecords.Text = "0";
                }
                else
                {

                    //========================================================
                    //if (ds.Tables[0].Rows.Count <= 100)
                    if (ds.Tables[0].Rows.Count <= 25)
                    //========================================================
                    {
                        DataGrid1.PagerStyle.Visible = false;
                        DataGrid1.CurrentPageIndex = 0;
                    }
                    else
                    {
                        DataGrid1.PagerStyle.Visible = true;
                    }

                    //PagedDataSource objds = new PagedDataSource();
                    //objds.DataSource = ds.Tables[0].DefaultView;
                    //LblRecordsno.Text = objds.DataSourceCount.ToString();

                    divMessage.InnerHtml = "";
                    divMessage.Visible = false;

                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        if (ds.Tables[0].Rows[i]["AddedBy"] != null && ds.Tables[0].Rows[i]["AddedBy"].ToString().Trim() != "")
                        {
                            ds.Tables[0].Rows[i]["AddedBy"] = ds.Tables[0].Rows[i]["AddedBy"].ToString();
                        }
                        else
                        {
                            ds.Tables[0].Rows[i]["AddedBy"] = ds.Tables[0].Rows[i]["UserName"].ToString();
                        }
                    }

                    dvCR.Visible = true;
                    DataGrid1.DataSource = null;
                    DataGrid1.DataBind();

                    DataGrid1.DataSource = ds.Tables[0];
                    DataGrid1.DataBind();
                    lblCrRecords.Text = ds.Tables[0].Rows.Count.ToString();
                    divTotalHrs.Visible = false;
                    dvheader.Visible = false;
                }
            }
            catch (Exception ex)
            {
                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "BindData 2:-> " + Session["admin"] + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :->" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "BindData 1: ");
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at BindData 3:-> " + Session["admin"] + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :->" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "BindData 2: ");
        }
    }
    private void BindData2()
    {
        try
        {
            string strFilter = "";


            string topFilterLable = "";

            if (chkViewDeleted.Checked == true)
            {
                strFilter += " and ClientRequest_Details.deleted = 1 ";
            }
            else
            {
                strFilter += " and ClientRequest_Details.deleted <> 1 ";
            }

            if (chkHideClosed.Checked == true)
            {

            }
            else
            {
                strFilter += " and ClientRequest.status <> 'closed' ";
            }


            if (drpChangeBy.SelectedIndex != 0)
            {
                char[] separator = new char[] { '+' };

                string[] strCid = drpChangeBy.SelectedItem.Value.Split(separator);

                strFilter += " and ClientRequest.ClientId = " + strCid[0] + " and ClientRequest.UserName='" + drpChangeBy.SelectedItem.Text + "' ";

                if (topFilterLable == "")
                {
                    topFilterLable = "Filtered by Change added by: " + drpChangeBy.SelectedItem.Text;
                }
                else
                {
                    topFilterLable += " / Change added by: " + drpChangeBy.SelectedItem.Text;
                }
            }

            if (drpStatus.SelectedIndex != 0)
            {
                strFilter += " and ClientRequest.Status = '" + drpStatus.SelectedItem.Value + "' ";

                if (topFilterLable == "")
                {
                    topFilterLable = "Filtered by Status: " + drpStatus.SelectedItem.Text;
                }
                else
                {
                    topFilterLable += " / Status: " + drpStatus.SelectedItem.Text;
                }
            }
            else
            {
                // strFilter += " and ClientRequest.status <> 'closed' ";
            }

            if (drpPriority.SelectedIndex != 0)
            {
                strFilter += " and ClientRequest.Priority = " + drpPriority.SelectedItem.Value + " ";

                if (topFilterLable == "")
                {
                    topFilterLable = "Filtered by Priority: " + drpPriority.SelectedItem.Text;
                }
                else
                {
                    topFilterLable += " / Priority: " + drpPriority.SelectedItem.Text;
                }
            }

            if (drpWebsiteUrl.SelectedIndex != 0)
            {
                strFilter += " and CompanyName = '" + drpWebsiteUrl.SelectedItem.Value + "' ";

                if (topFilterLable == "")
                {
                    topFilterLable = "Filtered by Company Name: " + drpWebsiteUrl.SelectedItem.Text;
                }
                else
                {
                    topFilterLable += " / Company Name: " + drpWebsiteUrl.SelectedItem.Text;
                }
            }


            if (chkViewDeleted.Checked == true)
            {
                if (topFilterLable == "")
                {
                    topFilterLable = "Filtered by View only deleted";
                }
                else
                {
                    topFilterLable += " / View only deleted";
                }
            }

            if (chkHideClosed.Checked == true)
            {
                if (topFilterLable == "")
                {
                    topFilterLable = "Filtered by view closed";
                }
                else
                {
                    topFilterLable += " / View closed";
                }
            }

            TopFilterLable.InnerHtml = topFilterLable;
            DataSet ds = null;
            try
            {
                sql = "select * from ClientRequest where RequestId in(select ClientRequest_Details.RequestId from ClientRequest_Details where [CommentId] not in (select  CommentId from read_CR_comments where read_CR_comments.username='" + Session["admin"].ToString() + "') " + strFilter + ")" + lblOrderBy.Text;

                ds = DatabaseHelper.getDataset(sql);
            }
            catch (Exception ex)
            {
                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at BindData2:-> " + Session["admin"] + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :->" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "BindData2: " + sql);
            }
            ds = formatCrsDataSet(ds);

            try
            {

                if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0].Rows.Count <= 0)
                {
                    divMessage.InnerHtml = "No request found.";
                    divMessage.Visible = true;
                    DataGrid1.DataSource = null;
                    DataGrid1.DataBind();
                    //LblRecordsno.Text = "0";
                }
                else
                {

                    if (ds.Tables[0].Rows.Count <= 25)
                    {
                        DataGrid1.PagerStyle.Visible = false;
                        DataGrid1.CurrentPageIndex = 0;
                    }
                    else
                    {
                        DataGrid1.PagerStyle.Visible = true;
                    }

                    PagedDataSource objds = new PagedDataSource();
                    objds.DataSource = ds.Tables[0].DefaultView;

                    //LblRecordsno.Text = objds.DataSourceCount.ToString();
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        if (ds.Tables[0].Rows[i]["AddedBy"] != null && ds.Tables[0].Rows[i]["AddedBy"].ToString().Trim() != "")
                        {
                            ds.Tables[0].Rows[i]["AddedBy"] = ds.Tables[0].Rows[i]["AddedBy"].ToString();
                        }
                        else
                        {
                            ds.Tables[0].Rows[i]["AddedBy"] = ds.Tables[0].Rows[i]["UserName"].ToString();
                        }
                    }

                    divMessage.InnerHtml = "";
                    divMessage.Visible = false;
                    DataGrid1.DataSource = null;
                    DataGrid1.DataBind();

                    DataGrid1.DataSource = ds.Tables[0];
                    DataGrid1.DataBind();
                }
            }
            catch (Exception ex)
            {
                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at BindData2:-> " + Session["admin"] + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :->" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "BindData2: ");
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at BindData2:-> " + Session["admin"] + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :->" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "BindData2:2 ");
        }
    }

    private DataSet formatTaskDataSet(DataSet ds)
    {
        string strtaskid = "";
        try
        {

            ds.Tables[0].Columns.Add("ShowAllComment");
            ds.Tables[0].Columns.Add("ShowReadUnread");

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {

                    if (strtaskid == "")
                    {
                        strtaskid = ds.Tables[0].Rows[i]["task_id"].ToString() + ",";
                    }
                    else
                    {
                        strtaskid = strtaskid + ds.Tables[0].Rows[i]["task_id"].ToString() + ",";
                    }

                    if (ds.Tables[0].Rows[i]["short_desc"] != null && ds.Tables[0].Rows[i]["short_desc"].ToString().Trim() != "")
                    {
                        ds.Tables[0].Rows[i]["short_desc"] = "<a  href=\"edit_task.aspx?id=" + ds.Tables[0].Rows[i]["task_id"].ToString() + "  \">" + ds.Tables[0].Rows[i]["short_desc"].ToString() + "</a>";
                    }
                    else
                    {
                        ds.Tables[0].Rows[i]["short_desc"] = "";
                    }


                    //try
                    //{
                    //    DataSet dslastcomment = null;
                    //    string LastComment = "";
                    //    try
                    //    {
                    //        sql = "select top 1.* from task_comments where task_comments.deleted <> 1  and task_id =" + ds.Tables[0].Rows[i]["task_id"].ToString() + " order by tc_id desc";

                    //        dslastcomment = DatabaseHelper.getDataset(sql);
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at formatTaskDataSet 1:-> " + Session["admin"] + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :->" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "formatTaskDataSet: ");
                    //    }

                    //    if (dslastcomment != null && dslastcomment.Tables[0].Rows.Count > 0)
                    //    {

                    //        for (int j = 0; j < 1; j++)
                    //        {

                    //            if (dslastcomment.Tables[0].Rows[j]["qflag"].ToString() == "1")
                    //            {

                    //                LastComment += "<span style='color:Red'>" + "Question posted by " + dslastcomment.Tables[0].Rows[j]["username"].ToString() + " for  " + dslastcomment.Tables[0].Rows[j]["QuesTo"].ToString() + " on " + DateTime.Parse(dslastcomment.Tables[0].Rows[j]["post_date"].ToString()).ToString("dd MMM yyyy h:mm tt") + "</span>";

                    //                LastComment += "<br/><span>" + dslastcomment.Tables[0].Rows[j]["comment"].ToString() + "</span>";

                    //                LastComment += "<span style=\"border-bottom: solid 1px green; margin-bottom: 2px; height:1px\"></span><br/><hr />";

                    //            }
                    //            else
                    //            {

                    //                LastComment += "<span style='color:Green'>" + "Comment " + dslastcomment.Tables[0].Rows[j]["tc_id"].ToString() + " posted by " + dslastcomment.Tables[0].Rows[j]["username"].ToString() + " on " + DateTime.Parse(dslastcomment.Tables[0].Rows[j]["post_date"].ToString()).ToString("dd MMM yyyy h:mm tt") + "</span>";

                    //                LastComment += "<br/><span>" + dslastcomment.Tables[0].Rows[j]["comment"].ToString() + "</span>";

                    //                LastComment += "<span style=\"border-bottom: solid 1px green; margin-bottom: 2px; height:1px\"></span><br/><hr />";

                    //            }

                    //        }
                    //    }

                    //    ds.Tables[0].Rows[i]["ShowAllComment"] = LastComment.ToString();
                    //  }
                    
                    //catch (Exception ex)
                    //{
                        //DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at formatTaskDataSet 2:-> " + Session["admin"] + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :->" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "formatTaskDataSet: ");
                    //}

               
            
            
            }
                catch (Exception ex)
                {
                    DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at formatTaskDataSet 3:-> " + Session["admin"] + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :->" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "formatTaskDataSet: ");
                }
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at formatTaskDataSet 4:-> " + Session["admin"] + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :->" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "formatTaskDataSet: ");
            Server.ClearError();
        }




        //*******************************************************************************************************************
        strtaskid = strtaskid.TrimEnd(new char[] { ',' });
        DataView dv=null;
        try
        {
            DataSet dslastcomment = null;
            
            try
            {


                sql = "select a.* from task_comments a where a.task_id in(" + strtaskid + ") and a.tc_id IN (select top 1 b.tc_id from task_comments b where b.task_id = a.task_id order by b.post_date desc)";

                //sql = "select top 1.* from task_comments where task_comments.deleted <> 1  and task_id in (" + strtaskid + ") order by tc_id desc";

                dslastcomment = DatabaseHelper.getDataset(sql);
                 dv = new DataView(dslastcomment.Tables[0]);
            }
            catch (Exception ex)
            {
                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at formatTaskDataSet 1:-> " + Session["admin"] + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :->" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "formatTaskDataSet: ");
            }

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                string LastComment = "";
                if (dslastcomment != null && dslastcomment.Tables[0].Rows.Count > 0)
                {
                    
                    dv.RowFilter = "task_id='" + ds.Tables[0].Rows[i]["task_id"].ToString() + "'";

                    if (dv[0]["qflag"].ToString() == "1")
                    {

                        LastComment += "<span style='color:Red'>" + "Question posted by " + dv[0]["username"].ToString() + " for  " + dv[0]["QuesTo"].ToString() + " on " + DateTime.Parse(dv[0]["post_date"].ToString()).ToString("dd MMM yyyy h:mm tt") + "</span>";

                        LastComment += "<br/><span>" + dv[0]["comment"].ToString() + "</span>";

                        LastComment += "<span style=\"border-bottom: solid 1px green; margin-bottom: 2px; height:1px\"></span><br/><hr />";

                    }
                    else
                    {

                        LastComment += "<span style='color:Green'>" + "Comment " + dv[0]["tc_id"].ToString() + " posted by " + dv[0]["username"].ToString() + " on " + DateTime.Parse(dv[0]["post_date"].ToString()).ToString("dd MMM yyyy h:mm tt") + "</span>";

                        LastComment += "<br/><span>" + dv[0]["comment"].ToString() + "</span>";

                        LastComment += "<span style=\"border-bottom: solid 1px green; margin-bottom: 2px; height:1px\"></span><br/><hr />";
                    }
                }
                     
                ds.Tables[0].Rows[i]["ShowAllComment"] = LastComment.ToString();
            }
        }

        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at formatTaskDataSet 2:-> " + Session["admin"] + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :->" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "formatTaskDataSet: ");
        }

        //******************************************************************************************************************
        //********************************************************************************************************************
        try
        {
           string  sqlreadUnread = "select distinct task_id from task_comments where tc_id not in(select tc_id from read_comments where username ='" + Session["admin"] + "')and task_comments.deleted <> 1 and task_id in (" + strtaskid + ")";

           DataSet dsReadUnread = DatabaseHelper.getDataset(sqlreadUnread);

            
            for (int icount = 0; icount < ds.Tables[0].Rows.Count; icount++)
            {
                DataView dv1 = new DataView(dsReadUnread.Tables[0]);
                dv1.RowFilter = "task_id='" + ds.Tables[0].Rows[icount]["task_id"].ToString() + "'";
                if (dv1.Count >= 1)
                {
                    ds.Tables[0].Rows[icount]["ShowReadUnread"] = false;

                }
                else
                {
                    ds.Tables[0].Rows[icount]["ShowReadUnread"] = true;
                }
            }



            //if (ds != null && ds.Tables[0].Rows.Count > 0)
            //{
            //    readMarkButton.Text = "[<span style='color:red'>Unread.</span>  Mark as read]";
            //}
            //else
            //{

            //    readMarkButton.Text = "[<span style='color:green'>Read.</span> Mark as unread]";
            //}
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at gvTask_RowDataBound: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "gvTask_RowDataBound :" + sql);
        }
        



        //strtaskid = strtaskid.TrimEnd(new char[] { ',' });
        //string strReadUnread;
        //strReadUnread = "select distinct task_id from task_comments where tc_id not in(select tc_id from read_comments where username ='" + Session["admin"] + "')and task_comments.deleted <> 1 and task_id in (" + strtaskid + ")";
        //DataSet dsreadUnread = DatabaseHelper.getDataset(strReadUnread);

        //for (int icount = 0; icount < ds.Tables[0].Rows.Count; icount++)
        //{
        //    DataView dv = new DataView(dsreadUnread.Tables[0]);
        //    dv.RowFilter = "task_id='" + ds.Tables[0].Rows[icount]["task_id"].ToString() + "'";
        //    if (dv.Count >= 1)
        //    {
        //        ds.Tables[0].Rows[icount]["ShowReadUnread"] = false;

        //    }
        //    else
        //    {
        //        ds.Tables[0].Rows[icount]["ShowReadUnread"] = true;
        //    }
        //}
        
        
        //******************************************************************************************************************























        return ds;
    }

    private DataSet formatCrsDataSet(DataSet ds)
    {
        try
        {

            ds.Tables[0].Columns.Add("ShowAllComment");

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {
                    if (ds.Tables[0].Rows[i]["ShortDescr"] != null && ds.Tables[0].Rows[i]["ShortDescr"].ToString().Trim() != "")
                    {
                        ds.Tables[0].Rows[i]["ShortDescr"] = "<a  href=\"edit_request.aspx?reqid=" + ds.Tables[0].Rows[i]["RequestId"].ToString() + "  \">" + ds.Tables[0].Rows[i]["ShortDescr"].ToString() + "</a>";
                    }
                    else
                    {
                        ds.Tables[0].Rows[i]["ShortDescr"] = "";
                    }

                    try
                    {
                        DataSet dslastcomment = null;
                        string LastComment = "";
                        try
                        {
                            sql = "Select top 1.* from ClientRequest_Details where deleted <> 1 and RequestId=" + ds.Tables[0].Rows[i]["RequestId"].ToString() + " order by CommentId desc";

                            dslastcomment = DatabaseHelper.getDataset(sql);
                        }
                        catch (Exception ex)
                        {
                            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at formatCrsDataSet: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "formatCrsDataSet :" + sql);
                        }
                        if (dslastcomment != null && dslastcomment.Tables[0].Rows.Count > 0)
                        {

                            for (int j = 0; j < dslastcomment.Tables[0].Rows.Count; j++)
                            {

                                LastComment += "<span style='color:Green'>" + "Comment " + dslastcomment.Tables[0].Rows[j]["CommentId"].ToString() + " posted by " + dslastcomment.Tables[0].Rows[j]["PostedBy"].ToString() + " on " + DateTime.Parse(dslastcomment.Tables[0].Rows[j]["PostedOn"].ToString()).ToString("dd MMM yyyy h:mm tt") + "</span>";

                                LastComment += "<br/><span>" + dslastcomment.Tables[0].Rows[j]["Comment"].ToString() + "</span>";

                                LastComment += "<span style=\"border-bottom: solid 1px green; margin-bottom: 2px; height:1px\"></span><br/><hr />";

                            }

                        }
                        ds.Tables[0].Rows[i]["ShowAllComment"] = LastComment.ToString();
                    }
                    catch (Exception ex)
                    {
                        DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at formatCrsDataSet 1: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "formatCrsDataSet :");
                    }

                }
                catch (Exception ex)
                {
                    DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at formatCrsDataSet 2: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "formatCrsDataSet :");
                }
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at Query formatCrsDataSet 3: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "formatCrsDataSet :");
            Server.ClearError();
        }
        return ds;
    }
    protected void BindWebsiteURL()
    {
        try
        {
            DataSet ds = DatabaseHelper.getCompanyNameCR();

            if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0].Rows.Count <= 0)
            {
                ds = new DataSet();
                ds.Tables.Add();
                ds.Tables[0].Columns.Add("Company_Name");
            }
            DataRow row = ds.Tables[0].NewRow();
            row["Company_Name"] = "[no filter]";
            ds.Tables[0].Rows.InsertAt(row, 0);

            drpWebsiteUrl.DataSource = ds.Tables[0];
            drpWebsiteUrl.DataTextField = "Company_Name";
            drpWebsiteUrl.DataValueField = "Company_Name";
            drpWebsiteUrl.DataBind();
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at Query string 1: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "BindWebsiteURL :");
        }
    }

    protected void BindPostedBy()
    {
        try
        {
            sql = "select distinct username from " +
                   "( " +
                   " select distinct username from  users  where active = 'Y' " +
                   " union all " +
                   " select distinct username from ClientRequest " +
                   " )a ";

            //DataSet ds = DatabaseHelper.getRequestPostedBy();

            DataSet ds = DatabaseHelper.getDataset(sql);

            if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0].Rows.Count <= 0)
            {
                ds = new DataSet();
                ds.Tables.Add();
                //ds.Tables[0].Columns.Add("Cid");
                ds.Tables[0].Columns.Add("UserName");
            }
            DataRow row = ds.Tables[0].NewRow();
            row["UserName"] = "[no filter]";
            //row["Cid"] = "0";
            ds.Tables[0].Rows.InsertAt(row, 0);

            drpChangeBy.DataSource = ds.Tables[0];
            drpChangeBy.DataTextField = "UserName";
            //drpChangeBy.DataValueField = "Cid";
            drpChangeBy.DataValueField = "UserName";
            drpChangeBy.DataBind();
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at BindPostedBy: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "DataGrid1_PageIndexChanged :" + sql);

        }
    }

    protected void DataGrid1_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        try
        {
            DataGrid1.CurrentPageIndex = e.NewPageIndex;

            if (Session["filter"] == null || Session["admin"].ToString() == "")
            {
                BindData();
            }
            else
            {
                if (Session["filter"].ToString() == "NewCR")
                {
                    BindData();
                }
                else if (Session["filter"].ToString() == "CRNewComment")
                {
                    BindData2();
                }
                else
                {
                    BindData();
                }
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at DataGrid1_PageIndexChanged: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "DataGrid1_PageIndexChanged :");
        }

    }

    protected void DataGrid1_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        try
        {
            if (Session["reqColumn"] != null)
            {
                if (e.SortExpression.ToString() == Session["reqColumn"].ToString())
                {
                    //Reverse the sort order
                    if (Session["reqColumn"].ToString() == "CompanyName")
                    {
                        if (Session["filter"] == null || Session["filter"].ToString() == "")
                        {

                            if (Session["reqOrder"].ToString() == "ASC")
                            {
                                lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " DESC";
                                Session["reqOrder"] = "DESC";
                            }
                            else
                            {
                                lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " ASC";
                                Session["reqOrder"] = "ASC";
                            }
                        }
                        else
                        {
                            if (Session["filter"].ToString() == "CRNewComment")
                            {

                                if (Session["reqOrder"].ToString() == "ASC")
                                {
                                    lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " DESC";
                                    Session["reqOrder"] = "DESC";
                                }
                                else
                                {
                                    lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " ASC";
                                    Session["reqOrder"] = "ASC";
                                }

                            }
                        }
                    }
                    else
                    {
                        if (Session["reqOrder"] != null)
                        {
                            if (Session["reqOrder"].ToString() == "ASC")
                            {
                                lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " DESC";
                                Session["reqOrder"] = "DESC";
                            }
                            else
                            {
                                lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " ASC";
                                Session["reqOrder"] = "ASC";
                            }
                        }


                    }
                }
                else
                {
                    if (e.SortExpression.ToString() == "CompanyName")
                    {
                        //Different column selected, so default to ascending order
                        if (Session["filter"] == null || Session["filter"].ToString() == "")
                        {
                            lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " ASC";
                            Session["reqOrder"] = "ASC";
                        }
                        else
                        {
                            if (Session["filter"].ToString() == "CRNewComment")
                            {
                                lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " ASC";
                                Session["reqOrder"] = "ASC";

                            }

                        }

                    }
                    else
                    {
                        lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " ASC";
                        Session["reqOrder"] = "ASC";
                    }
                }
            }

            Session["reqColumn"] = e.SortExpression.ToString();
            Session["reqOrderBy"] = lblOrderBy.Text;

            if (Session["filter"] == null || Session["filter"].ToString() == "")
            {
                BindData();
            }
            else
            {
                if (Session["filter"].ToString() == "NewCR")
                {

                    BindData();
                }
                else if (Session["filter"].ToString() == "CRNewComment")
                {

                    BindData2();

                }
                else
                {

                    BindData();
                }
            }

        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at DataGrid1_SortCommand: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "drpChangeBy_SelectedIndexChanged :");
        }
    }
    protected void drpChangeBy_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            setFileterSession();
            dvTask.Visible = false;

            if (drpType.SelectedValue == "CR" || drpType.SelectedValue == "All")
            {
                if (Session["filter"] == null || Session["filter"].ToString() == "")
                {
                    BindData();
                }
                else
                {
                    if (Session["filter"].ToString() == "NewCR")
                    {

                        BindData();
                    }
                    else if (Session["filter"].ToString() == "CRNewComment")
                    {

                        BindData2();

                    }
                    else
                    {

                        BindData();
                    }
                }
            }

            if (drpType.SelectedValue != "CR")
            {
                BindTaskData();
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at drpChangeBy_SelectedIndexChanged: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "drpChangeBy_SelectedIndexChanged :");
        }

    }

    protected void drpWebsiteUrl_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            setFileterSession();
            dvTask.Visible = false;

            if (drpWebsiteUrl.SelectedValue != "0")
                projectname = drpWebsiteUrl.SelectedValue;

            if (drpType.SelectedValue == "CR" || drpType.SelectedValue == "All")
            {
                if (Session["filter"] == null || Session["filter"].ToString() == "")
                {
                    BindData();
                }
                else
                {
                    if (Session["filter"].ToString() == "NewCR")
                    {

                        BindData();

                    }
                    else if (Session["filter"].ToString() == "CRNewComment")
                    {

                        BindData2();
                    }
                    else
                    {
                        BindData();
                    }
                }
            }

            if (drpType.SelectedValue != "CR")
            {
                BindTaskData();
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at drpWebsiteUrl_SelectedIndexChanged: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "drpWebsiteUrl_SelectedIndexChanged :");
        }

    }

    protected void drpStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            setFileterSession();
            dvTask.Visible = false;

            if (drpType.SelectedValue == "CR" || drpType.SelectedValue == "All")
            {

                if (Session["filter"] == null || Session["filter"].ToString() == "")
                {
                    BindData();
                }
                else
                {
                    if (Session["filter"].ToString() == "NewCR")
                    {

                        BindData();
                    }
                    else if (Session["filter"].ToString() == "CRNewComment")
                    {

                        BindData2();

                    }
                    else
                    {

                        BindData();
                    }
                }
            }

            if (drpType.SelectedValue != "CR")
            {
                BindTaskData();
            }

        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at drpStatus_SelectedIndexChanged: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "drpStatus_SelectedIndexChanged :");
        }
    }

    protected void drpPriority_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            setFileterSession();
            dvTask.Visible = false;

            if (drpType.SelectedValue == "CR" || drpType.SelectedValue == "All")
            {

                if (Session["filter"] == null || Session["filter"].ToString() == "")
                {
                    BindData();
                }
                else
                {
                    if (Session["filter"].ToString() == "NewCR")
                    {

                        BindData();
                    }
                    else if (Session["filter"].ToString() == "CRNewComment")
                    {
                        BindData2();
                    }
                    else
                    {
                        BindData();
                    }
                }
            }

            if (drpType.SelectedValue != "CR")
            {
                BindTaskData();
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at drpPriority_SelectedIndexChanged: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "drpPriority_SelectedIndexChanged :");
        }


    }

    protected void DataGrid1_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemIndex != -1)
        {
            if (e.Item.Cells[7].Text == "0")
            {
                e.Item.Cells[6].Text = "0 - IMMEDIATE";
                e.Item.BackColor = System.Drawing.Color.Pink;
            }
            else if (e.Item.Cells[7].Text == "1")
            {
                e.Item.Cells[6].Text = "1 - highest";
            }
            else if (e.Item.Cells[7].Text == "5")
            {
                e.Item.Cells[6].Text = "5 - lowest";
            }
            else if (e.Item.Cells[7].Text == "6")
            {
                e.Item.Cells[6].Text = "[no priority]";
            }
            else if (e.Item.Cells[7].Text == "8")
            {
                e.Item.Cells[6].Text = "1c";
                string htmlColor = "#ffeeee";
                e.Item.BackColor = System.Drawing.ColorTranslator.FromHtml(htmlColor);

            }
            else
            {
                e.Item.Cells[6].Text = e.Item.Cells[7].Text;
            }

            ((Label)e.Item.FindControl("lblChangeAddedOn")).Text = DateTime.Parse(((Label)e.Item.FindControl("lblChangeAddedOn")).Text).ToString("dd MMM yyyy h:mm tt").ToString();
            ((Label)e.Item.FindControl("lblLastUpdatedOn")).Text = DateTime.Parse(((Label)e.Item.FindControl("lblLastUpdatedOn")).Text).ToString("dd MMM yyyy h:mm tt").ToString();
            LinkButton readMarkButton = (LinkButton)e.Item.FindControl("lnkReadMark");
            LinkButton LinksupportType = (LinkButton)e.Item.FindControl("Supportype");

            Label LblAllComment = (Label)e.Item.FindControl("lblshrtDesc");
            LblAllComment.Attributes.Add("onmouseover", "get_(this);");
            LblAllComment.Attributes.Add("onmouseout", "get_1(this);");

            try
            {
                sql = "Select supportType,SupportUrl from NonesCRMusers where UserName in (select UserName from ClientRequest where RequestId = " + e.Item.Cells[0].Text + ")";
                DataSet dsSupport = DatabaseHelper.getDataset(sql);

                if (dsSupport != null && dsSupport.Tables[0].Rows.Count > 0)
                {
                    if (dsSupport.Tables[0].Rows[0]["SupportUrl"].ToString() != "")
                    {
                        LinksupportType.Text = "<span style='color:red'>" + dsSupport.Tables[0].Rows[0]["supportType"].ToString() + "</span>";
                        if (dsSupport.Tables[0].Rows[0]["SupportUrl"].ToString().Contains("http://") == true)
                        {
                            LinksupportType.Attributes.Add("onclick", "window.open('" + dsSupport.Tables[0].Rows[0]["SupportUrl"].ToString() + "','',',,');");
                        }
                        else
                        {
                            LinksupportType.Attributes.Add("onclick", "window.open('" + "http://" + dsSupport.Tables[0].Rows[0]["SupportUrl"].ToString() + "','',',,');");
                        }
                        LinksupportType.Enabled = true;
                    }
                    else
                    {
                        LinksupportType.Text = "[<span style='color:Blue'>Not added</span>]";
                        LinksupportType.Enabled = false;

                    }


                }
            }
            catch (Exception ex)
            {
                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at DataGrid1_ItemDataBound: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "DataGrid1_ItemDataBound :" + sql);
            }

            try
            {
                sql = "select *  from ClientRequest_Details where [CommentId] not in (select CommentId from read_CR_comments where read_CR_comments.username='" + Session["admin"].ToString() + "') and ClientRequest_Details.deleted <> 1 and ClientRequest_Details.RequestId= " + e.Item.Cells[0].Text;

                DataSet ds = DatabaseHelper.getDataset(sql);

                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    readMarkButton.Text = "[<span style='color:red'>Unread.</span>  Mark as read]";

                }
                else
                {
                    readMarkButton.Text = "[<span style='color:green'>Read.</span> Mark as unread]";
                }
            }
            catch (Exception ex)
            {
                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at DataGrid1_ItemDataBound: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "DataGrid1_ItemDataBound :" + sql);
            }
        }
    }

    protected void DataGrid1_ItemCommand(object source, DataGridCommandEventArgs e)
    {

        if (e.CommandName == "ReadMark")
        {
            LinkButton readMarkButton = (LinkButton)e.Item.FindControl("lnkReadMark");

            if (readMarkButton.Text == "[<span style='color:red'>Unread.</span>  Mark as read]")
            {
                DataSet ds = null;
                try
                {
                    sql = @"select *  from ClientRequest_Details where RequestId =" + e.Item.Cells[0].Text + " and  CommentId  not in (select  CommentId from read_CR_comments where read_CR_comments.UserName='" + Session["admin"].ToString() + "')";
                    ds = DatabaseHelper.getDataset(sql);
                }
                catch (Exception ex)
                {
                    DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at DataGrid1_ItemCommand: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "DataGrid1_ItemCommand :" + sql);
                }

                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        string sqlinsert = "";
                        try
                        {


                            sqlinsert = "insert into read_CR_comments([CommentId],[UserName]) ";
                            sqlinsert += " values(" + ds.Tables[0].Rows[i]["CommentId"].ToString() + ",'" + Session["admin"].ToString() + "')";

                            int intResult = DatabaseHelper.executeNonQuery(sqlinsert);

                        }
                        catch (Exception ex)
                        {
                            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at DataGrid1_ItemCommand: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "DataGrid1_ItemCommand :" + sqlinsert);
                        }
                    }

                }

                Response.Redirect("client_requests.aspx", false);
            }
            else
            {
                DataSet ds = null;
                try
                {
                    sql = @"select *  from ClientRequest_Details where RequestId =" + e.Item.Cells[0].Text + " and  CommentId  in (select  CommentId from read_CR_comments where read_CR_comments.UserName='" + Session["admin"].ToString() + "')";
                    ds = DatabaseHelper.getDataset(sql);
                }
                catch (Exception ex)
                {

                    DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at DataGrid1_ItemCommand: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "DataGrid1_ItemCommand :" + sql);
                }


                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        string sqlinsert = "";
                        try
                        {

                            sqlinsert = "Delete from read_CR_comments where CommentId =" + ds.Tables[0].Rows[i]["CommentId"].ToString() + " and UserName = '" + Session["admin"].ToString() + "'";
                            int intResult = DatabaseHelper.executeNonQuery(sqlinsert);

                        }
                        catch (Exception ex)
                        {
                            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at Query string 1: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "DataGrid1_ItemCommand :" + sqlinsert);
                        }
                    }

                }

                Response.Redirect("client_requests.aspx", false);
            }
        }
    }

    private void setFileterSession()
    {
        try
        {
            if (Session["filter"] == null || Session["filter"].ToString() == "")
            {
                Session["reqHideClosed"] = chkHideClosed.Checked;
                Session["reqViewDeleted"] = chkViewDeleted.Checked;
                Session["reqChangedBy"] = drpChangeBy.SelectedValue;
                Session["reqStatus"] = drpStatus.SelectedValue;
                Session["reqPriority"] = drpPriority.SelectedValue;
                Session["reqWebsiteURL"] = drpWebsiteUrl.SelectedValue;
                Session["reqOrderBy"] = lblOrderBy.Text;
                Session.Remove("filter");
            }
            else
            {
                if (Session["filter"].ToString() == "CRNewComment")
                {

                    Session["reqHideClosed"] = chkHideClosed.Checked;
                    Session["reqViewDeleted"] = chkViewDeleted.Checked;
                    Session["reqChangedBy"] = drpChangeBy.SelectedValue;
                    Session["reqStatus"] = drpStatus.SelectedValue;
                    Session["reqPriority"] = drpPriority.SelectedValue;
                    Session["reqWebsiteURL"] = drpWebsiteUrl.SelectedValue;
                    Session["reqOrderBy"] = lblOrderBy.Text;

                }
                if (Session["filter"].ToString() == "CRwocr")
                {

                    Session["reqHideClosed"] = chkHideClosed.Checked;
                    Session["reqViewDeleted"] = chkViewDeleted.Checked;
                    Session["reqChangedBy"] = drpChangeBy.SelectedValue;
                    Session["reqStatus"] = "awaiting client response- required";
                    Session["reqPriority"] = drpPriority.SelectedValue;
                    Session["reqWebsiteURL"] = drpWebsiteUrl.SelectedValue;
                    Session["reqOrderBy"] = lblOrderBy.Text;

                }

            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at setFileterSession: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "setFileterSession :");
        }


    }

    private void getFilterSession()
    {
        try
        {
            if (Session["filter"] != null && Session["filter"].ToString() != "")
            {
                if (Session["filter"].ToString() == "CRwocr")
                {
                    Session["reqStatus"] = "awaiting client response- required";
                }
            }

            if (Session["reqHideClosed"] != null)
                chkHideClosed.Checked = (bool)Session["reqHideClosed"];

            if (Session["reqViewDeleted"] != null)
                chkViewDeleted.Checked = (bool)Session["reqViewDeleted"];

            if (Session["reqChangedBy"] != null)
                drpChangeBy.SelectedValue = Session["reqChangedBy"].ToString();

            if (Session["reqChangedBy"] != null)
                drpChangeBy.SelectedValue = Session["reqChangedBy"].ToString();


            if (Session["reqStatus"] != null)
                drpStatus.SelectedValue = Session["reqStatus"].ToString();

            if (Session["reqPriority"] != null)
                drpPriority.SelectedValue = Session["reqPriority"].ToString();

            if (Session["reqWebsiteURL"] != null)
                drpWebsiteUrl.SelectedValue = Session["reqWebsiteURL"].ToString();

            if (Session["reqOrderBy"] != null)
                lblOrderBy.Text = Session["reqOrderBy"].ToString();
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at getFilterSession: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "getFilterSession :");
        }
    }

    protected void btnClearFilters_Click(object sender, EventArgs e)
    {
        try
        {
            lblOrderBy.Text = " ORDER BY LastUpdatedOn desc";
            BindWebsiteURL();
            BindPostedBy();
            drpChangeBy.SelectedIndex = 0;
            drpStatus.SelectedIndex = 0;
            drpPriority.SelectedIndex = 0;
            drpWebsiteUrl.SelectedIndex = 0;
            drpType.SelectedIndex = 1;
            //Default Selection: Tasks
            //**************************************
            //drpHoursspent.SelectedIndex = 1;   
            //***************************************
            //Default Selection: Last 7 days
            dvCR.Visible = false;
            setFileterSession();
            chkViewDeleted.Checked = false;
            chkHideClosed.Checked = false;
            Session.Remove("filter");

            BindTaskData();
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at btnClearFilters_Click: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "btnClearFilters_Click :");
        }
    }

    protected void chkViewDeleted_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            setFileterSession();
            dvTask.Visible = false;

            if (drpType.SelectedValue == "CR" || drpType.SelectedValue == "All")
            {
                if (Session["filter"] == null || Session["filter"].ToString() == "")
                {
                    BindData();
                }
                else
                {
                    if (Session["filter"].ToString() == "NewCR")
                    {

                        BindData();
                    }
                    else if (Session["filter"].ToString() == "CRNewComment")
                    {

                        BindData2();

                    }
                    else
                    {

                        BindData();
                    }
                }
            }

            if (drpType.SelectedValue != "CR")
            {
                BindTaskData();
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at chkViewDeleted_CheckedChanged: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "chkViewDeleted_CheckedChanged :");
        }
    }
    protected void chkHideClosed_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            setFileterSession();
            dvTask.Visible = false;

            if (drpType.SelectedValue == "CR" || drpType.SelectedValue == "All")
            {
                if (Session["filter"] == null || Session["filter"].ToString() == "")
                {
                    BindData();
                }
                else
                {
                    if (Session["filter"].ToString() == "NewCR")
                    {

                        BindData();
                    }
                    else if (Session["filter"].ToString() == "CRNewComment")
                    {

                        BindData2();

                    }
                    else
                    {

                        BindData();
                    }
                }
            }

            if (drpType.SelectedValue != "CR")
            {
                BindTaskData();
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at chkHideClosed_CheckedChanged: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "chkHideClosed_CheckedChanged :");
        }

    }

    protected void LnkBtnToDeleteAll_Click(object sender, EventArgs e)
    {

        try
        {

            int i = 0;
            foreach (DataGridItem row in DataGrid1.Items)
            {
                // Access the CheckBox 
                try
                {
                    CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                    if (cb != null && cb.Checked)
                    {
                        i = i + 1;
                        sql = "update ClientRequest set deleted=1 where [RequestId] =" + row.Cells[0].Text;

                        int intResult = DatabaseHelper.executeNonQuery(sql);

                        if (intResult != 0)
                        {
                            string sqlcomment = "";
                            try
                            {
                                sqlcomment = "select *  from ClientRequest_Details where [RequestId] = " + row.Cells[0].Text + " and [CommentId] not in (select  CommentId  from read_CR_comments where read_CR_comments.username='" + Session["admin"].ToString() + "')";
                                DataSet dscomment = DatabaseHelper.getDataset(sqlcomment);

                                if (dscomment != null && dscomment.Tables[0].Rows.Count > 0)
                                {
                                    for (int j = 0; j < dscomment.Tables[0].Rows.Count; j++)
                                    {
                                        string sqlinsert = "";
                                        try
                                        {
                                            sqlinsert = "insert into read_CR_comments([CommentId],[UserName]) ";
                                            sqlinsert += " values(" + dscomment.Tables[0].Rows[j]["CommentId"].ToString() + ",'" + Session["admin"].ToString() + "')";

                                            int intCommentResult = DatabaseHelper.executeNonQuery(sqlinsert);
                                        }
                                        catch (Exception ex)
                                        {
                                            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at Query string 1: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "LnkBtnToDeleteAll_Click :" + sqlinsert);
                                        }
                                    }


                                }
                            }
                            catch (Exception ex)
                            {
                                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at LnkBtnToDeleteAll_Click: 2" + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "LnkBtnToDeleteAll_Click :" + sqlcomment);
                            }

                        }

                    }
                }
                catch (Exception ex)
                {
                    DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at LnkBtnToDeleteAll_Click: 3" + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "LnkBtnToDeleteAll_Click :" + sql);
                }
            }
            if (i > 0)
            {
                Response.Redirect("client_requests.aspx", false);
            }
            else
            {
                Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('Please select CR to delete.');</script>");

                return;

            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at Query string 1: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "LnkBtnToDeleteAll_Click :");
            Response.Redirect("client_requests.aspx", false);

        }


    }
    protected void LnkBtnToChngPR_Click(object sender, EventArgs e)
    {

        try
        {

            int i = 0;
            foreach (DataGridItem row in DataGrid1.Items)
            {
                // Access the CheckBox 
                try
                {
                    CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                    if (cb != null && cb.Checked)
                    {
                        i = i + 1;
                        sql = "update ClientRequest set Priority='" + DrpMultipleTaskPR.SelectedValue.ToString() + "' where [RequestId] =" + row.Cells[0].Text;

                        int intResult = DatabaseHelper.executeNonQuery(sql);

                    }
                }
                catch (Exception ex)
                {
                    DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at LnkBtnToChngPR_Click: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "LnkBtnToChngPR_Click :" + sql);
                }
            }
            if (i > 0)
            {
                Response.Redirect("client_requests.aspx", false);
            }
            else
            {
                Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('Please select CR to change priority.');</script>");

                return;

            }
        }
        catch (Exception ex) { Response.Redirect("client_requests.aspx", false); }



    }
    protected void LnkChngStatus_Click(object sender, EventArgs e)
    {

        try
        {

            int i = 0;
            foreach (DataGridItem row in DataGrid1.Items)
            {
                // Access the CheckBox 
                try
                {
                    CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                    if (cb != null && cb.Checked)
                    {
                        i = i + 1;
                        sql = "update ClientRequest set Status = '" + DrpMultipleTaskStatus.SelectedValue.ToString() + "' where [RequestId] =" + row.Cells[0].Text;

                        int intResult = DatabaseHelper.executeNonQuery(sql);

                    }
                }
                catch (Exception ex)
                {
                    DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at LnkChngStatus_Click: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "LnkChngStatus_Click :" + sql);
                }
            }
            if (i > 0)
            {
                Response.Redirect("client_requests.aspx", false);
            }
            else
            {
                Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('Please select CR to change status.');</script>");

                return;

            }
        }
        catch (Exception ex) { Response.Redirect("client_requests.aspx", false); }

    }
    protected void LnkBtnToMarkAsRead_Click(object sender, EventArgs e)
    {
        int i = 0;
        foreach (DataGridItem row in DataGrid1.Items)
        {
            try
            {
                CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                if (cb != null && cb.Checked)
                {
                    i = i + 1;

                    sql = @"select *  from ClientRequest_Details where RequestId =" + row.Cells[0].Text + " and  CommentId  not in (select  CommentId from read_CR_comments where read_CR_comments.UserName='" + Session["admin"].ToString() + "')";

                    DataSet ds = DatabaseHelper.getDataset(sql);

                    if (ds != null && ds.Tables[0].Rows.Count > 0)
                    {
                        for (int j = 0; j < ds.Tables[0].Rows.Count; j++)
                        {
                            string sqlinsert = "";
                            try
                            {


                                sqlinsert = "insert into read_CR_comments([CommentId],[UserName]) ";
                                sqlinsert += " values(" + ds.Tables[0].Rows[j]["CommentId"].ToString() + ",'" + Session["admin"].ToString() + "')";

                                int intResult = DatabaseHelper.executeNonQuery(sqlinsert);

                            }
                            catch (Exception ex)
                            {
                                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at LnkBtnToMarkAsRead_Click: 1" + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "DrpMultipleTaskPR_SelectedIndexChanged :" + sqlinsert);
                            }
                        }

                    }

                }


            }

            catch (Exception ex)
            {
                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at LnkBtnToMarkAsRead_Click: 3 " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "LnkBtnToMarkAsRead_Click :" + sql);
            }
        }
        if (i > 0)
        {
            Response.Redirect("client_requests.aspx", false);
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('Please select CR to mark as read.');</script>");

            return;

        }

    }
    protected void DrpMultipleTaskPR_SelectedIndexChanged(object sender, EventArgs e)
    {
        int i = 0;
        foreach (DataGridItem row in DataGrid1.Items)
        {
            try
            {
                CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                if (cb != null && cb.Checked)
                {
                    i = i + 1;


                    sql = @"update ClientRequest set priority='" + DrpMultipleTaskPR.SelectedItem.Value + "' where RequestId =" + row.Cells[0].Text;
                    DatabaseHelper.executeNonQuery(sql);

                }


            }

            catch (Exception ex)
            {
                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at DrpMultipleTaskPR_SelectedIndexChanged: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "DrpMultipleTaskPR_SelectedIndexChanged :" + sql);
            }
        }
        if (i > 0)
        {
            Response.Redirect("client_requests.aspx", false);
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('Please select CR to mark as read.');</script>");

            return;

        }

    }

    protected void DrpMultipleTaskStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        int i = 0;
        foreach (DataGridItem row in DataGrid1.Items)
        {
            try
            {
                CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                if (cb != null && cb.Checked)
                {
                    i = i + 1;


                    sql = @"update ClientRequest set status='" + DrpMultipleTaskStatus.SelectedItem.Text + "' where RequestId =" + row.Cells[0].Text;
                    DatabaseHelper.executeNonQuery(sql);

                }


            }

            catch (Exception ex)
            {
                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at DrpMultipleTaskStatus_SelectedIndexChanged: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "DrpMultipleTaskStatus_SelectedIndexChanged :" + sql);

            }
        }
        if (i > 0)
        {
            Response.Redirect("client_requests.aspx", false);
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('Please select CR to mark as read.');</script>");

            return;

        }


    }
    protected void drpType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (drpType.SelectedValue == "Tasks")
            {
                if (drpWebsiteUrl.SelectedIndex == 0)
                    projectname = Request.QueryString["projectname"].ToString();
                else
                    projectname = drpWebsiteUrl.SelectedValue;
                //drpHoursspent.SelectedValue = "7";
                lblOrderBy.Text = " ORDER BY last_updated_date desc";
                BindTaskData();
                dvheader.Visible = true;
                dvTask.Visible = true;
                dvCR.Visible = false;
                dvCrHeader.Visible = false;
                divTotalHrs.Visible = true;
            }
            else if (drpType.SelectedValue == "CR")
            {
                if (drpWebsiteUrl.SelectedIndex == 0)
                    projectname = Request.QueryString["projectname"].ToString();
                else
                    projectname = drpWebsiteUrl.SelectedValue;

                lblOrderBy.Text = " ORDER BY LastUpdatedOn desc";
                BindData();
                dvheader.Visible = false;
                dvTask.Visible = false;
                dvCR.Visible = true;
                dvCrHeader.Visible = true;
                divTotalHrs.Visible = false;
            }
            else
            {
                if (drpWebsiteUrl.SelectedIndex == 0)
                    projectname = Request.QueryString["projectname"].ToString();
                else
                    projectname = drpWebsiteUrl.SelectedValue;

                lblOrderBy.Text = " ORDER BY LastUpdatedOn desc";
                BindData();
                lblOrderBy.Text = " ORDER BY last_updated_date desc";
                BindTaskData();
                dvheader.Visible = true;
                dvTask.Visible = true;
                dvCR.Visible = true;
                dvCrHeader.Visible = true;

            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at drpType_SelectedIndexChanged : " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "drpType_SelectedIndexChanged :");
        }
    }

    protected void gvTask_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        try
        {
            BindTaskData();
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at gvTask_PageIndexChanged : " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "gvTask_PageIndexChanged :");
        }
    }

    protected void gvTask_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvTask.PageIndex = e.NewPageIndex;
            BindTaskData();
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at gvTask_PageIndexChanging: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "gvTask_PageIndexChanging :");
        }
    }

    protected void gvTask_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.DataItemIndex != -1)
        {
            ((Label)e.Row.FindControl("lblReportedOn")).Text = DateTime.Parse(((Label)e.Row.FindControl("lblReportedOn")).Text).ToString("dd MMM yyyy h:mm tt").ToString();
            ((Label)e.Row.FindControl("lblLastUpdatedOn")).Text = DateTime.Parse(((Label)e.Row.FindControl("lblLastUpdatedOn")).Text).ToString("dd MMM yyyy h:mm tt").ToString();
            
            

            //***********************************************************************************************************************
            //string htmlColor = (string)DatabaseHelper.executeScalar("select bg_color from priorities where priority_name='" + e.Row.Cells[8].Text + "'");
            //e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml(htmlColor);

            try
            {
                DataSet dsbackground;
                if (HttpContext.Current.Cache["BackgroundColor"] == null)
                {
                    //put in a cache
                    string sqlBackgrdcolor = @"select * from priorities";
                    dsbackground = DatabaseHelper.getDataset(sqlBackgrdcolor);
                    HttpContext.Current.Cache.Insert("BackgroundColor", dsbackground, null, DateTime.Now.AddMinutes(60), System.Web.Caching.Cache.NoSlidingExpiration);
                    DataView dv = new DataView(dsbackground.Tables[0]);
                    dv.RowFilter = "priority_name='" + e.Row.Cells[8].Text + "'";
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml(dv[0]["bg_color"].ToString());
                }
                else
                {
                    dsbackground = (DataSet)HttpContext.Current.Cache["BackgroundColor"];
                    DataView dvget = new DataView(dsbackground.Tables[0]);
                    dvget.RowFilter = "priority_name='" + e.Row.Cells[8].Text + "'";
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml(dvget[0]["bg_color"].ToString());

                }
            }
            catch (Exception ex)
            { }
            //***********************************************************************************************************************
            
            
            int ETC = Convert.ToInt32(((HiddenField)e.Row.Cells[17].Controls[3]).Value);

            e.Row.Cells[13].Text = "" + (ETC / 60) + "' " + (ETC % 60) + '"';
            //=============================================================
            //=============================================================
            //int hrsTaken = getHoursTakenSoFar(e.Row.Cells[0].Text);
            int hrsTaken = getHoursTakenSoFar(e.Row.Cells[19].Text);
            //=============================================================
            //=============================================================

            e.Row.Cells[14].Text = "" + (hrsTaken / 60) + "' " + (hrsTaken % 60) + '"';

            int hrsLeft = ETC - hrsTaken;

            if (hrsLeft < 0)
            {
                hrsLeft = -1 * hrsLeft;
                e.Row.Cells[15].Text = "-" + (hrsLeft / 60) + "' " + (hrsLeft % 60) + '"';
            }
            else
            {
                e.Row.Cells[15].Text = "" + (hrsLeft / 60) + "' " + (hrsLeft % 60) + '"';
            }
            
            
            //LinkButton readMarkButton = (LinkButton)e.Row.FindControl("lnkReadMark");

            //try
            //{
            //    sql = "select * from task_comments where tc_id not in(select tc_id from read_comments where username ='" + Session["admin"] + "')and task_comments.deleted <> 1 and task_id =" + e.Row.Cells[19].Text;

            //    DataSet ds = DatabaseHelper.getDataset(sql);

            //    if (ds != null && ds.Tables[0].Rows.Count > 0)
            //    {
            //        readMarkButton.Text = "[<span style='color:red'>Unread.</span>  Mark as read]";
            //    }
            //    else
            //    {

            //        readMarkButton.Text = "[<span style='color:green'>Read.</span> Mark as unread]";
            //    }
            //}
            //catch (Exception ex)
            //{
            //    DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at gvTask_RowDataBound: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "gvTask_RowDataBound :" + sql);
            //}

            Label LblAllComment = (Label)e.Row.FindControl("lblshrtDesc");
            LblAllComment.Attributes.Add("onmouseover", "get_(this);");
            LblAllComment.Attributes.Add("onmouseout", "get_1(this);");
            CheckBox ChkTasks = (CheckBox)e.Row.FindControl("chkSelect");

            try
            {
                if (!DatabaseHelper.can_Delete_Tasks(Session["admin"].ToString()))
                {
                    //ME deleteButton.Visible = false;
                    ChkTasks.Visible = false;
                }
                else
                {
                    //ME  deleteButton.Visible = true;
                    ChkTasks.Visible = true;

                }
            }
            catch { }

            try
            {

                if (e.Row.Cells[7].Text == "closed")
                {
                    closecount++;
                }

            }
            catch { }
        }

    }
    protected void gvTask_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        GridViewRow gvr;
        int rows;
        GridViewRow row;

        if (e.CommandName == "ReadMark")
        {
            gvr = (GridViewRow)((LinkButton)e.CommandSource).NamingContainer;
            rows = gvr.RowIndex;

            row = gvTask.Rows[rows];
            LinkButton readMarkButton = (LinkButton)row.FindControl("lnkReadMark");

            if (readMarkButton.Text == "[<span style='color:red'>Unread.</span>  Mark as read]")
            {
                DataSet ds = null;
                try
                {
                    sql = @"insert into read_task([task_id],[username]) 
                        values(" + row.Cells[19].Text + ",'" + Session["admin"].ToString() + "')";
                    DatabaseHelper.executeNonQuery(sql);

                    sql = @"select *  from task_comments where task_id =" + row.Cells[19].Text + " and  tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "')";

                    ds = DatabaseHelper.getDataset(sql);
                }
                catch (Exception ex)
                {
                    DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at gvTask_RowCommand: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "gvTask_RowCommand :" + sql);
                }

                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        string sqlinsert = "";
                        try
                        {
                            sqlinsert = "insert into read_comments([tc_id],[username]) ";
                            sqlinsert += " values(" + ds.Tables[0].Rows[i]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                            int intResult = DatabaseHelper.executeNonQuery(sqlinsert);

                        }
                        catch (Exception ex)
                        {
                            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at gvTask_RowCommand: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "gvTask_RowCommand :" + sqlinsert);
                        }
                    }

                }

                Response.Redirect("projectrequests.aspx?projectname=" + Request.QueryString["projectname"].ToString(), false);
            }
            else
            {
                DataSet ds = null;
                try
                {
                    sql = @"delete from read_task where [task_id]=" + row.Cells[19].Text + " and [username]='" + Session["admin"] + "'";
                    DatabaseHelper.executeNonQuery(sql);

                    sql = @"select *  from task_comments where task_id =" + row.Cells[19].Text + " and  tc_id  in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "')";
                    ds = DatabaseHelper.getDataset(sql);
                }
                catch (Exception ex)
                {
                    DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at gvTask_RowCommand: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "gvTask_RowCommand :" + sql);
                }
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        string sqlinsert = "";
                        try
                        {

                            sqlinsert = "Delete from read_comments where tc_id =" + ds.Tables[0].Rows[i]["tc_id"].ToString() + " and username = '" + Session["admin"].ToString() + "'";
                            int intResult = DatabaseHelper.executeNonQuery(sqlinsert);

                        }
                        catch (Exception ex)
                        {
                            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at gvTask_RowCommand: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "gvTask_RowCommand :" + sqlinsert);
                        }
                    }

                }

                Response.Redirect("projectrequests.aspx?projectname=" + Request.QueryString["projectname"].ToString(), false);
            }
        }

        else if (e.CommandName == "delete")
        {
            gvr = (GridViewRow)((LinkButton)e.CommandSource).NamingContainer;
            rows = gvr.RowIndex;

            row = gvTask.Rows[rows];
            int intResult = 0;
            try
            {
                sql = "update tasks set deleted=1 where task_id = " + row.Cells[19].Text + "; ";

                intResult = DatabaseHelper.executeNonQuery(sql);
            }
            catch (Exception ex)
            {
                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at gvTask_RowCommand: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "gvTask_RowCommand :" + sql);
            }
            if (intResult != 0)
            {
                try
                {
                    task_action = ((string)DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + Session["admin"].ToString() + "'")) + " has deleted task ";

                    emailSubject = "Task ID:" + row.Cells[19].Text + " was deleted - " + ((Label)row.FindControl("lblshrtDesc")).Text + " (Task ID:" + row.Cells[19].Text + ")";

                    sendNotification(row.Cells[19].Text, ((Label)row.FindControl("lblshrtDesc")).Text);
                    try
                    {
                        sql = @" insert into task_updates([task_id],[username],[post_date],[comment]) 
                            values(" + row.Cells[19].Text + ",'" + Session["admin"].ToString().Replace("'", "''") + "',getdate(),'Task was deleted.'); ";

                        intResult = DatabaseHelper.executeNonQuery(sql);
                    }
                    catch (Exception ex)
                    {
                        DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at gvTask_RowCommand: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "gvTask_RowCommand :" + sql);
                    }

                }
                catch (Exception ex)
                {
                    DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at gvTask_RowCommand: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "gvTask_RowCommand :" + sql);
                }

                try
                {
                    sql = @"select *  from task_comments where task_id =" + row.Cells[19].Text + " and  tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "')";

                    DataSet dscomment = DatabaseHelper.getDataset(sql);

                    if (dscomment != null && dscomment.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < dscomment.Tables[0].Rows.Count; i++)
                        {
                            string sqlinsert = "";
                            try
                            {
                                sqlinsert = "insert into read_comments([tc_id],[username]) ";
                                sqlinsert += " values(" + dscomment.Tables[0].Rows[i]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                                int intCommentResult = DatabaseHelper.executeNonQuery(sqlinsert);
                            }
                            catch (Exception ex)
                            {
                                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at gvTask_RowCommand : " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "gvTask_RowCommand :" + sqlinsert);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at gvTask_RowCommand : " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "gvTask_RowCommand :" + sql);
                }

                Response.Redirect("projectrequests.aspx?projectname=" + Request.QueryString["projectname"].ToString(), false);
            }

        }

    }
    protected void gvTask_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
    {
        try
        {

            if (Session["Column"] != null)
            {
                if (e.SortExpression.ToString() == Session["Column"].ToString())
                {
                    //Reverse the sort order
                    if (Session["Order"] == "ASC")
                    {
                        lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " DESC";
                        Session["Order"] = "DESC";
                    }
                    else
                    {
                        lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " ASC";
                        Session["Order"] = "ASC";
                    }
                }
            }
            else
            {
                //Different column selected, so default to ascending order
                lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " ASC";
                Session["Order"] = "ASC";
            }

            Session["Column"] = e.SortExpression.ToString();
            Session["OrderBy"] = lblOrderBy.Text;
            BindTaskData();
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at gvTask_Sorting: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "gvTask_Sorting :");
        }

    }

    void sendNotification(string strTaskId, string StrShrtDesc)
    {
        try
        {
            sql = "select email, EmailNotification,PR_EmailNotification ";
            sql += " from users, task_subscriptions ";
            sql += " where users.username = task_subscriptions.username ";
            sql += " and task_subscriptions.task_id =" + strTaskId.ToString() + " and task_subscriptions.username <> '" + Session["admin"].ToString() + "'";

            DataSet ds = DatabaseHelper.getDataset(sql);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                string strHtml = generateAdminEmail(strTaskId, StrShrtDesc);

                string toEmails = "";


                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (ds.Tables[0].Rows[i]["EmailNotification"].ToString() == "Y")
                    {

                        string reqPR = "";
                        string[] strPR = ds.Tables[0].Rows[i]["PR_EmailNotification"].ToString().Split(new char[] { ';' });
                        for (int j = 0; j < strPR.Length - 1; j++)
                        {
                            reqPR = strPR[j];
                            if (reqPR == "All")
                            {
                                toEmails += ds.Tables[0].Rows[i]["email"].ToString() + ";";
                            }
                            else if (reqPR == drpPriority.SelectedValue)
                            {
                                toEmails += ds.Tables[0].Rows[i]["email"].ToString() + ";";
                            }

                        }


                    }


                }

                toEmails = toEmails.TrimEnd(';');

                if (emailSubject == "")
                {
                    emailSubject = "Task ID:" + strTaskId.ToString() + " was updated - " + StrShrtDesc.ToString().Trim() + " (Task ID:" + strTaskId.ToString() + ")";
                }

                bool flag = DatabaseHelper.sendEmailTasks(toEmails, emailSubject, strHtml);

            }
        }
        catch (Exception ex)
        {

            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at sendNotification : " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "sendNotification :" + sql);
        }

    }

    private string generateAdminEmail(string strTaskId, string StrShrtDesc)
    {
        DataSet dsTask = DatabaseHelper.getDataset("select * from tasks where task_id=" + strTaskId);
        string strBody = "";
        if (dsTask == null || dsTask.Tables.Count <= 0 || dsTask.Tables[0].Rows.Count <= 0)
        {
        }
        else
        {
            strBody += "Task: <span style=\"color:red\">";

            strBody += task_action;

            strBody += " (<a style=\"color:red\" href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_task.aspx?id=" + strTaskId + "'>" + strTaskId + "</a>) </span> " + DateTime.Parse(dsTask.Tables[0].Rows[0]["last_updated_date"].ToString()).ToString("dd MMM h:mm tt") + "<br><br>";
            strBody += "<a href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_task.aspx?id=" + strTaskId + "'>" + StrShrtDesc.ToString().Trim() + "</a>&nbsp;&nbsp;&nbsp;(" + dsTask.Tables[0].Rows[0]["priority"].ToString() + " : " + dsTask.Tables[0].Rows[0]["status"].ToString() + ")<br><br>";
            strBody += "Emp: " + ((string)DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + dsTask.Tables[0].Rows[0]["assigned_to_user"].ToString() + "'")) + "<br>";
            strBody += "Project: " + dsTask.Tables[0].Rows[0]["project"].ToString() + "<br>";
            strBody += "Relevant URL: <a href='" + dsTask.Tables[0].Rows[0]["Relevant_URL"].ToString() + "' target='_blank'>" + dsTask.Tables[0].Rows[0]["Relevant_URL"].ToString() + "</a><br>";
            strBody += "<br><br>";

            sql = "Select * from task_comments where task_id=" + strTaskId.ToString() + " and deleted <> 1 order by tc_id desc";

            DataSet dsTaskDetails = DatabaseHelper.getDataset(sql);

            strBody += "<table cellspacing=\"0\" border=\"1\" style=\"width: 99%;border-collapse: collapse;\">";
            strBody += "<tbody>";

            for (int i = 0; i < dsTaskDetails.Tables[0].Rows.Count; i++)
            {
                strBody += "<tr>";
                strBody += "<td>";
                strBody += "<div style=\"border: 1px none Green;\">";
                strBody += "<table width=\"100%\">";
                strBody += "<tbody>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                if (dsTaskDetails.Tables[0].Rows[i]["qflag"].ToString() == "1")
                {
                    strBody += "<span style=\"color:red;\">Question posted by " + dsTaskDetails.Tables[0].Rows[i]["username"].ToString() + " on " + DateTime.Parse(dsTaskDetails.Tables[0].Rows[i]["post_date"].ToString()).ToString("dd MMM h:mm tt") + "</span>";
                }
                else
                {
                    strBody += "<span style=\"color: Green;\">comment " + dsTaskDetails.Tables[0].Rows[i]["tc_id"].ToString() + " posted by " + dsTaskDetails.Tables[0].Rows[i]["username"].ToString() + " on " + DateTime.Parse(dsTaskDetails.Tables[0].Rows[i]["post_date"].ToString()).ToString("dd MMM h:mm tt") + "</span>";
                }
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\"> </td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span>" + dsTaskDetails.Tables[0].Rows[i]["comment"].ToString() + "</span>";
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "</tbody>";
                strBody += "</table>";
                strBody += "</div>";
                strBody += "</td>";
                strBody += "</tr>";
            }
            strBody += "</tbody>";
            strBody += "</table>";
        }
        return strBody;
    }

    //*******************************************************************************
    //protected void drpHoursspent_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    BindTaskData();
    //}
    //****************************************************************************
    void getprojectid(string ProjectName)
    {
        try
        {
            sql = @"select project_id, project_name
		from projects
		where project_name ='" + ProjectName + "'";

            DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);
            if (ds_dropdowns.Tables[0] != null)
            {
                //lblamend.HRef = "edit_project.aspx?id=" + ds_dropdowns.Tables[0].Rows[0]["project_id"].ToString();
                FTPid = ds_dropdowns.Tables[0].Rows[0]["project_id"].ToString();
                ProjectId_Client = ds_dropdowns.Tables[0].Rows[0]["project_id"].ToString();
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at Query string 1: " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "getprojectid :" + sql);
        }
    }
    protected void drpProjects_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            bool archive = false;
            if (chkArchive.Checked)
            {
                archive = true;
            }
           // BindTaskData();
            //Response.Redirect("projectrequests.aspx?projectname=" + Server.UrlEncode(drpProjects.SelectedValue) + "&archive=" + archive, false);
            Response.Redirect("projectrequests.aspx?projectname=" + Server.UrlEncode(drpProjects.SelectedValue), false);
            
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at drpProjects_SelectedIndexChanged : " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "drpProjects_SelectedIndexChanged :");
        }
    }
    protected void BtnAnalytics_Click(object sender, EventArgs e)
    {
        string sqlanalyts = "";
        try
        {
            sqlanalyts = @"select *  from projects where project_name ='" + drpProjects.SelectedValue + "'";

            DataSet ds = DatabaseHelper.getDataset(sqlanalyts);

            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["SEO"].ToString() == "True")
                {

                    Session["GoogleUser"] = ds.Tables[0].Rows[0]["GoogleUserId"].ToString();
                    Session["GooglePwd"] = ds.Tables[0].Rows[0]["GooglePwd"].ToString();
                    Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> window.open('Analytics.aspx','','scrollbars=yes,width=620,height=580'); </script>");
                }
                else
                {

                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Analytics  not added for this project.');", true);
                }
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at BtnAnalytics_Click : " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "BtnAnalytics_Click :" + sqlanalyts);
        }
    }
    protected void Lnkcomprelated_Click(object sender, EventArgs e)
    {
        try
        {
            string sqlanalyts = @"select *  from projects where project_name ='" + drpProjects.SelectedValue + "'";
            DataSet ds = DatabaseHelper.getDataset(sqlanalyts);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                Session["CompanyRelated"] = ds.Tables[0].Rows[0]["CompanyName"].ToString();
            }
            Response.Redirect("CompanyRelated.aspx", false);
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at Lnkcomprelated_Click : " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Lnkcomprelated_Click :");
        }
    }
    protected void LnkRelated_Click(object sender, EventArgs e)
    {
        try
        {
            Session["ProjectRelated"] = drpProjects.SelectedValue;
            //Response.Redirect("Related.aspx", false);
            Response.Redirect("http://www.esgdesk.com/admin/projectrequests.aspx?projectname=" + drpProjects.SelectedValue);
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at LnkRelated_Click : " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "LnkRelated_Click :");
        }
    }
    protected void chkArchive_CheckedChanged(object sender, EventArgs e)
    {
        if (chkArchive.Checked)
        {
            load_AllProject();

        }
        else
        {
            load_notArchive();
        }
        //====================================
        //DataGrid1.DataSource = null;
        //DataGrid1.DataBind();
        //gvTask.DataSource = null;
        //gvTask.DataBind();
        //lblCrRecords.Text = "0";
        //getTotalHrsReport("0");
        //getHoursTakenSoFar("0");
        //====================================
    }
    void load_notArchive()
    {
        string strcomboArc = "";

        try
        {
            DataSet ds_Archive;
            if (Cache["Dropdowns_For_NotArchive"] == null)
            {
                strcomboArc = @"select project_name from projects where archive!='Y' and active = 'Y' order by project_name;";
                ds_Archive = DatabaseHelper.getDataset(strcomboArc);
                Cache.Insert("Dropdowns_For_NotArchive", ds_Archive, null, DateTime.Now.AddHours(6), TimeSpan.Zero);
            }
            else
            {
                ds_Archive = (DataSet)Cache["Dropdowns_For_NotArchive"];
            }
            drpProjects.DataSource = ds_Archive.Tables[0];
            drpProjects.DataTextField = "project_name";
            drpProjects.DataValueField = "project_name";
            drpProjects.DataBind();
            drpProjects.Items.Insert(0, new ListItem("[no filter]", "0"));

        }
        catch(Exception ex) 
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at LnkRelated_Click : " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "load_notArchive :" + strcomboArc);
        }
    }
    void load_AllProject()
    {
        string strNonArc = "";

        try
        {
            DataSet ds_NonArc;
            if (Cache["Dropdowns_For_AllProject"] == null)
            {
                strNonArc = @"select project_name from projects where  active = 'Y' order by project_name;";
                ds_NonArc = DatabaseHelper.getDataset(strNonArc);
                Cache.Insert("Dropdowns_For_AllProject", ds_NonArc, null, DateTime.Now.AddHours(6), TimeSpan.Zero);
            }
            else
            {
                ds_NonArc = (DataSet)Cache["Dropdowns_For_AllProject"];
            }
            drpProjects.DataSource = ds_NonArc.Tables[0];
            drpProjects.DataTextField = "project_name";
            drpProjects.DataValueField = "project_name";
            drpProjects.DataBind();
            drpProjects.Items.Insert(0, new ListItem("[no filter]", "0"));

        }
        catch(Exception ex) 
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at LnkRelated_Click : " + Session["admin"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "load_AllProject :" + strNonArc);
        }
    }


}