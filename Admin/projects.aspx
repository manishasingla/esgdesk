<%@ Page Language="C#" AutoEventWireup="true" CodeFile="projects.aspx.cs" Inherits="Admin_projects" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="Notifications.ascx" TagName="Notifications" TagPrefix="uc4" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        Projects
    </title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
     table#DataGrid1_ctl00 tr th a
     {
      display:block;
      text-decoration:none;
      margin:0;
      padding:0;
      width:100%;
      height:100%;
     }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="wrapper">
            <uc2:Admin_Header ID="Admin_Header1" runat="server" />
            <div id="Content">
                <div id="Content_98">
                    <div class="titleText" style="margin: 10px 0 10px 0; display: none;">
                        Projects&nbsp;|&nbsp;</div>
                    <div>
                        <uc4:Notifications ID="Notifications" runat="server" />
                    </div>
                    <div id="divMessage" runat="server" style="color: Red; font-weight: bold">
                    </div>
                    <div id="progresscircle">
                        <asp:UpdateProgress ID="updateprogress1" runat="server">
                            <ProgressTemplate>
                                <div id="processmessage" class="processMessage">
                                    <img alt="loading" src="images/ajax-loader.gif" style="width: 80px; height: 80px" />
                                    <span style="color: black; font-weight: bold; font-size: 18px; vertical-align: super;">
                                        Please wait... </span>
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <div class="divBorder" style="float: left; width: 100%">
                                    <div style="width: 100%; background: #ebebeb; padding: 5px 0; float: left;">
                                        <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                                            <tr>
                                                <td width="25%" align="left" valign="middle">
                                                  <span>To add a project go to Edit Project area</span>
                                                    <%--<a style="Border-bottom:1px dotted #000;" href="edit_project.aspx">To add a project go to Edit Project area</a>--%>
                                                </td>
                                                <td width="50%" align="center" valign="middle">
                                                    <div>
                                                        <asp:LinkButton ID="lnkAll" runat="server" style="Border-bottom:1px dotted #000;" Font-Bold="true" OnClick="lnkAll_Click">All</asp:LinkButton>&nbsp;
                                                        &nbsp; &nbsp;&nbsp; &nbsp;&nbsp;
                                                        <asp:LinkButton ID="lnkA" runat="server" style="Border-bottom:1px dotted #000;" Font-Bold="true" OnClick="lnkA_Click">A</asp:LinkButton>
                                                        <asp:LinkButton ID="lnkB" runat="server" style="Border-bottom:1px dotted #000;" Font-Bold="true" OnClick="lnkB_Click">B</asp:LinkButton>
                                                        <asp:LinkButton ID="lnkC" runat="server" style="Border-bottom:1px dotted #000;" Font-Bold="true" OnClick="lnkC_Click">C</asp:LinkButton>
                                                        <asp:LinkButton ID="lnkD" runat="server" style="Border-bottom:1px dotted #000;" Font-Bold="true" OnClick="lnkD_Click">D</asp:LinkButton>
                                                        <asp:LinkButton ID="lnkE" runat="server" style="Border-bottom:1px dotted #000;" Font-Bold="true" OnClick="lnkE_Click">E</asp:LinkButton>
                                                        <asp:LinkButton ID="lnkF" runat="server" style="Border-bottom:1px dotted #000;" Font-Bold="true" OnClick="lnkF_Click">F</asp:LinkButton>
                                                        <asp:LinkButton ID="lnkG" runat="server" style="Border-bottom:1px dotted #000;" Font-Bold="true" OnClick="lnkG_Click">G</asp:LinkButton>
                                                        <asp:LinkButton ID="lnkH" runat="server" style="Border-bottom:1px dotted #000;" Font-Bold="true" OnClick="lnkH_Click">H</asp:LinkButton>
                                                        <asp:LinkButton ID="lnkI" runat="server" style="Border-bottom:1px dotted #000;" Font-Bold="true" OnClick="lnkI_Click">I</asp:LinkButton>
                                                        <asp:LinkButton ID="lnkJ" runat="server" style="Border-bottom:1px dotted #000;" Font-Bold="true" OnClick="lnkJ_Click">J</asp:LinkButton>
                                                        <asp:LinkButton ID="lnkK" runat="server" style="Border-bottom:1px dotted #000;" Font-Bold="true" OnClick="lnkK_Click">K</asp:LinkButton>
                                                        <asp:LinkButton ID="lnkL" runat="server" style="Border-bottom:1px dotted #000;" Font-Bold="true" OnClick="lnkL_Click">L</asp:LinkButton>
                                                        <asp:LinkButton ID="lnkM" runat="server" style="Border-bottom:1px dotted #000;" Font-Bold="true" OnClick="lnkM_Click">M</asp:LinkButton>
                                                        <asp:LinkButton ID="lnkN" runat="server" style="Border-bottom:1px dotted #000;" Font-Bold="true" OnClick="lnkN_Click">N</asp:LinkButton>
                                                        <asp:LinkButton ID="lnkO" runat="server" style="Border-bottom:1px dotted #000;" Font-Bold="true" OnClick="lnkO_Click">O</asp:LinkButton>
                                                        <asp:LinkButton ID="lnkP" runat="server" style="Border-bottom:1px dotted #000;" Font-Bold="true" OnClick="lnkP_Click">P</asp:LinkButton>
                                                        <asp:LinkButton ID="lnkQ" runat="server" style="Border-bottom:1px dotted #000;" Font-Bold="true" OnClick="lnkQ_Click">Q</asp:LinkButton>
                                                        <asp:LinkButton ID="lnkR" runat="server" style="Border-bottom:1px dotted #000;" Font-Bold="true" OnClick="lnkR_Click">R</asp:LinkButton>
                                                        <asp:LinkButton ID="lnkS" runat="server" style="Border-bottom:1px dotted #000;" Font-Bold="true" OnClick="lnkS_Click">S</asp:LinkButton>
                                                        <asp:LinkButton ID="lnkT" runat="server" style="Border-bottom:1px dotted #000;" Font-Bold="true" OnClick="lnkT_Click">T</asp:LinkButton>
                                                        <asp:LinkButton ID="lnkU" runat="server" style="Border-bottom:1px dotted #000;" Font-Bold="true" OnClick="lnkU_Click">U</asp:LinkButton>
                                                        <asp:LinkButton ID="lnkV" runat="server" style="Border-bottom:1px dotted #000;" Font-Bold="true" OnClick="lnkV_Click">V</asp:LinkButton>
                                                        <asp:LinkButton ID="lnkW" runat="server" style="Border-bottom:1px dotted #000;" Font-Bold="true" OnClick="lnkW_Click">W</asp:LinkButton>
                                                        <asp:LinkButton ID="lnkX" runat="server" style="Border-bottom:1px dotted #000;" Font-Bold="true" OnClick="lnkX_Click">X</asp:LinkButton>
                                                        <asp:LinkButton ID="lnkY" runat="server" style="Border-bottom:1px dotted #000;"  Font-Bold="true" OnClick="lnkY_Click">Y</asp:LinkButton>
                                                        <asp:LinkButton ID="lnkZ" runat="server" style="Border-bottom:1px dotted #000;" Font-Bold="true" OnClick="lnkZ_Click">Z</asp:LinkButton>
                                                    </div>
                                                </td>
                                                <td align="right" valign="middle">
                                                    <b>Records: </b>
                                                    <asp:Label ID="LblRecordsno" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div style="clear: both">
                                    </div>
                                    <%--******************************************************************************************************************--%>
                                    
                                  
                                    <telerik:RadGrid ID="DataGrid1" runat="server" AutoGenerateColumns="false" Width="100%"
                                        CellPadding="5" AllowPaging="true" PageSize="20" AllowSorting="true" OnItemCommand="DataGrid1_ItemCommand"
                                        OnItemDataBound="DataGrid1_ItemDataBound" OnPageIndexChanged="DataGrid1_PageIndexChanged"
                                        OnSortCommand="DataGrid1_SortCommand">
                                        <PagerStyle Mode="NextPrevAndNumeric" />
                                        <HeaderStyle Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False"
                                            HorizontalAlign="Center" />
                                        <MasterTableView PagerStyle-AlwaysVisible="true">
                                            <Columns>
                                                <%--0--%>
                                                <telerik:GridTemplateColumn HeaderText="Project id" SortExpression="project_id">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblprojectid" runat="server" Text='<%# bind("project_id") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <%--1--%>
                                                <telerik:GridTemplateColumn HeaderText="Project dashboard" SortExpression="project_name">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                    <asp:HyperLink runat="server" Target="_blank"  Text='<%# bind("project_name") %>' NavigateUrl='<%# "projectrequests.aspx?projectname="+ Server.UrlEncode(DataBinder.Eval(Container.DataItem,"project_name").ToString())%>' ID="lblprojectname"></asp:HyperLink>  
                                                    
                                                <%-- <asp:Label ID="lblprojectname" runat="server" Text='<%# bind("project_name") %>'></asp:Label>--%>
                                                     </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <%--2--%>
                                                <telerik:GridTemplateColumn HeaderText="Company name" SortExpression="CompanyName">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblcompanyname" runat="server" Text='<%# bind("CompanyName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <%--3--%>
                                                <%--===================================================--%>
                                                <telerik:GridTemplateColumn HeaderText="Register clients">
                                                   <ItemTemplate>
                                                       <asp:DropDownList runat="server" Width="150px" ID="DrpClients" AutoPostBack="true"/>
                                                   </ItemTemplate>
                                               </telerik:GridTemplateColumn>
                                            <%--===================================================--%>
                                                <telerik:GridTemplateColumn HeaderText="Website url" SortExpression="WebsiteUrl">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblwebsiteurl" runat="server" Text='<%# bind("WebsiteUrl") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridButtonColumn CommandName="Related" HeaderText="Company related tasks/CRs" Text="Related tasks/CRs">
                                    </telerik:GridButtonColumn>
                                                <telerik:GridTemplateColumn HeaderText="Relevent url" SortExpression="Relevanturl">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRelevanturl" runat="server" Text='<%# bind("Relevanturl") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <%--4--%>
                                                <telerik:GridTemplateColumn HeaderText="Default selection" SortExpression="default_selection">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbldefaultselection" runat="server" Text='<%# bind("default_selection") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <%--5--%>
                                                <telerik:GridTemplateColumn HeaderText="Active" SortExpression="active">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblactive" runat="server" Text='<%# bind("active") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <%--6--%>
                                                <telerik:GridTemplateColumn HeaderText="Analytics">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkAnalytics" CommandName="Analytics" runat="server" Text="Analytics"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <%--7--%>
                                                <%--<telerik:GridHyperLinkColumn DataNavigateUrlFields="project_id" DataNavigateUrlFormatString="edit_project.aspx?id={0}"
                                                    HeaderText="Edit" NavigateUrl="edit_project.aspx" Text="edit">
                                                </telerik:GridHyperLinkColumn>
                                               --%>
                                               
                                               
                                                    <telerik:GridTemplateColumn HeaderText="Edit">
                                                        <ItemTemplate>
                                                           <a href="edit_project.aspx?id=<%# Eval("project_id")%>">
                                                              <%--<asp:ImageButton ID="editid" runat="server" ImageUrl="images/edit-icon.png" />--%>
                                                              <img   src="images/edit-icon.png" /> 
                                                           </a>   
                                                         </ItemTemplate> 
                                                    </telerik:GridTemplateColumn>
                            
                            
                                               
                                               
                                          <%--8--%>
                                             <telerik:GridTemplateColumn HeaderText="Delete">
                                               <ItemTemplate>
                                                <%--<asp:ImageButton ID="lnkdel" runat="server" CommandName="delete" ImageUrl="~/images/delete-icon.png" />--%>
                                                 <asp:ImageButton ID="lnkdelete" runat="server" ImageUrl="images/delete-icon.png" CommandName="delete"/>
                                                <%-- <asp:LinkButton ID="lnkdelete" runat="server" CommandName="delete"><%--<image imageurl="~/images/delete-icon.png"></image></asp:LinkButton>--%>
                                               </ItemTemplate>
                                           </telerik:GridTemplateColumn>
                                                
                                                
                                                
                                                
                                                <%--<telerik:GridTemplateColumn HeaderText="Delete">
                                                    <ItemTemplate>
                                                        <asp:LinkButton CommandName="delete" ID="lnkdelete" runat="server" Text="delete"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                
                                                
                                                --%>
                                                
                                                
                                                
                                                
                                                
                                                
                                                <%--9--%>
                                                
                                                
                                                
                                                
                                                
                                                <telerik:GridTemplateColumn HeaderText="Google userid" SortExpression="GoogleUserId"
                                                    Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblgoogleid" runat="server" Text='<%# bind("GoogleUserId") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                <%--10--%>
                                                <telerik:GridTemplateColumn HeaderText="Google Pwd" SortExpression="GooglePwd" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblgooglepwd" runat="server" Text='<%# bind("GooglePwd") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                            </Columns>
                                        </MasterTableView>
                                    </telerik:RadGrid>
                                    <%--******************************************************************************************************************--%>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
            <uc1:footer ID="Footer1" runat="server" />
            <asp:Label ID="lblOrderBy" runat="server" Text="" Visible="false"></asp:Label></div>
    </form>
</body>
</html>
