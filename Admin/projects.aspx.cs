using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
using Telerik.Web;

public partial class Admin_projects : System.Web.UI.Page
{
    string sql = "";
    string strfilter = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = "projects.aspx";
            Response.Redirect("login.aspx");
            return;
        }
        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            Response.Redirect("tasks.aspx");
            return;
        }
        divMessage.InnerHtml = "";
        if (!Page.IsPostBack)
        {
            Session["Current"] = "";
            lblOrderBy.Text = " ORDER BY project_name ASC";
            getFilterSession();
            bindData();
         }
    }
    private void setFileterSession()
    {
        Session["projectOrderBy"] = lblOrderBy.Text;
    }

    private void getFilterSession()
    {
        try
        {
            if (Session["projectOrderBy"] != null)
                lblOrderBy.Text = Session["projectOrderBy"].ToString();
        }
        catch
        {

        }
    }
    private void bindData()
    {
        string strSql;
        if (Session["Current"] == "ONE")
        {
          strSql = "select * from projects where  project_name like '" + Session["Word"] + "%'" + lblOrderBy.Text;
        }
        else if (Session["Current"] == "ALL" && Session["Word"] == "ALL")
        {
            strSql = "select * from projects " + lblOrderBy.Text;
        }
        else
        {
            strSql = "select * from projects " + lblOrderBy.Text;
        }
        DataSet ds = DatabaseHelper.getDataset(strSql);
        if (ds != null)
        {
            try
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    PagedDataSource objds = new PagedDataSource();
                    objds.DataSource = ds.Tables[0].DefaultView;
                    LblRecordsno.Text = objds.DataSourceCount.ToString();
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 20)
                        {
                            DataGrid1.PagerStyle.Visible = true;
                        }
                        else
                        {
                            DataGrid1.PagerStyle.Visible = false;
                        }
                        DataGrid1.DataSource = ds.Tables[0];
                        DataGrid1.DataBind();
                    }
                }
                else
                {
                    LblRecordsno.Text = "0";
                    DataGrid1.DataSource = null;
                    DataGrid1.DataBind();
                    divMessage.InnerHtml = "No projects in the database..";
                }
            }
            catch {

                DataGrid1.DataSource = null;
                DataGrid1.DataBind();
                divMessage.InnerHtml = "No projects in the database..";
            }
        }
        else
        {
            try
            {
                DataGrid1.DataSource = null;
                DataGrid1.DataBind();
                divMessage.InnerHtml = "No projects in the database..";
            }
            catch
            {
                DataGrid1.DataSource = null;
                DataGrid1.DataBind();
                divMessage.InnerHtml = "No projects in the database..";
            }
        }
    }
    protected void DataGrid1_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        foreach (TableCell tc in e.Item.Cells)
        {
         tc.Attributes["style"] = "border:1px solid #00A651;";
        }
            if (e.Item is GridPagerItem)
        {
            GridPagerItem pagerItem = (GridPagerItem)e.Item;
            RadComboBox PageSizeComboBox = (RadComboBox)pagerItem.FindControl("PageSizeComboBox");
            PageSizeComboBox.Visible = false;
            Label changePageSizelbl = (Label)pagerItem.FindControl("ChangePageSizeLabel");
            changePageSizelbl.Visible = false;
        } 
        if (e.Item.ItemType == GridItemType.Item || e.Item.ItemType == GridItemType.AlternatingItem)
        
        {
            GridDataItem dataItem = e.Item as GridDataItem;
            ImageButton deleteButton = (ImageButton)dataItem.FindControl("lnkdelete");
            LinkButton btnAnalytics = (LinkButton)dataItem.FindControl("lnkAnalytics");
            //==========================================================================
            DropDownList CompClients = (DropDownList)dataItem.FindControl("DrpClients");
            //==========================================================================
            deleteButton.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete this project?')");
            string sqlSEO = "select [SEO] from [projects]  where [project_name] = '" + ((HyperLink)dataItem.FindControl("lblprojectname")).Text + "'";
             object objResultSEO = DatabaseHelper.executeScalar(sqlSEO);
            try
            {

                 if (objResultSEO.ToString() == "True")
                 {
                    string htmlColor = "red";
                    ((LinkButton)dataItem.FindControl("lnkAnalytics")).ForeColor = System.Drawing.ColorTranslator.FromHtml(htmlColor);
                 }
                 else
                 {
                        string htmlColor = "blue";
                        ((LinkButton)dataItem.FindControl("lnkAnalytics")).ForeColor = System.Drawing.ColorTranslator.FromHtml(htmlColor);
                        btnAnalytics.Attributes.Add("onclick", "javascript:alert('Analytics  not added for this project.')");
                 }

            }
            catch { }
            try
            {
                sql = @"select RegId,UserName	from NonesCRMusers where ProjectName='" + ((HyperLink)dataItem.FindControl("lblprojectname")).Text + "'";
                DataSet dsclient_dropdowns = DatabaseHelper.getDataset(sql);
                if (dsclient_dropdowns.Tables[0].Rows.Count > 0)
                {
                    try
                    {
                        CompClients.DataSource = dsclient_dropdowns.Tables[0];
                        CompClients.DataTextField = "UserName";
                        CompClients.DataValueField = "RegId";
                        CompClients.DataBind();
                        CompClients.Items.Insert(0, new ListItem("[Clients]", ""));
                    }
                    catch { }
                }
                else
                {

                    CompClients.Items.Insert(0, new ListItem("[Clients]", ""));
                    CompClients.Enabled = false;
                }
            }
            catch { }
        }
    }
    protected void DataGrid1_ItemCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
    {
        GridDataItem dataItem = e.Item as GridDataItem;
        if (e.CommandName == "delete")
            {
                object objResult = DatabaseHelper.executeScalar("select count(*) from tasks where project =" + ((HyperLink)dataItem.FindControl("lblprojectname")).Text);
                object objResult2 = DatabaseHelper.executeScalar("select count(*) from ClientRequest where ProjectName =" + ((HyperLink)dataItem.FindControl("lblprojectname")).Text);
            if (objResult.ToString() == "0" && objResult2.ToString() == "0")
            {
                string strSql = "delete from projects where project_id=" + ((Label)dataItem.FindControl("lblprojectid")).Text;
                int intResult = DatabaseHelper.executeNonQuery(strSql);
                if (intResult != 0)
                {
                    divMessage.InnerHtml = "project was deleted.";
                    bindData();
                }
                else
                {
                    divMessage.InnerHtml = "Project was not deleted.";
                    bindData();
                }
            }
            else
            {
                divMessage.InnerHtml = "You can't delete project \"" + ((HyperLink)dataItem.FindControl("lblprojectname")).Text + "\"  because some tasks/CRs still reference it...";
                
            }
        }
        else if (e.CommandName == "Analytics")
        {
           if (((Label)dataItem.FindControl("lblgoogleid")).Text.ToString() != "")
              {
                if (((Label)dataItem.FindControl("lblgoogleid")).Text.ToString() != "&nbsp;")
                {
                    Session["GoogleUser"] = ((Label)dataItem.FindControl("lblgoogleid")).Text;
                    Session["GooglePwd"] = ((Label)dataItem.FindControl("lblgooglepwd")).Text;
                     Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> window.open('Analytics.aspx','','scrollbars=yes,width=630,height=580'); </script>");
                }
                else
                {
                    Session["GoogleUser"] = null;//e.Item.Cells[9].Text;
                    Session["GooglePwd"] = null;//e.Item.Cells[10].Text;
                    Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> alert('Analytics  not added for this project.')</script>");
                }
                 
            }
            else
            {
                Session["GoogleUser"] = null;//e.Item.Cells[9].Text;
                Session["GooglePwd"] = null;//e.Item.Cells[10].Text;
                Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> alert('Analytics  not added for this project.')</script>");
            }

        }
        else if (e.CommandName == "Related")
        {
            
            try
            {
                Session["CompanyRelated"] = ((Label)dataItem.FindControl("lblcompanyname")).Text;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "window.open('CompanyRelated.aspx','_blank');", true);
                //Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> window.open('CompanyRelated.aspx','_blank'); </script>");
                //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(),"<script> window.open('CompanyRelated.aspx','_blank'); </script>", true);
            }
            catch (Exception ex)
            { }
        }
    }
    protected void DataGrid1_PageIndexChanged(object source, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        DataGrid1.CurrentPageIndex = e.NewPageIndex;
        bindData();
    }
    protected void DataGrid1_SortCommand(object source, Telerik.Web.UI.GridSortCommandEventArgs e)
    {
        if (e.SortExpression.ToString() == Session["projectColumn"])
        {
            if (Session["projectOrder"] == "DESC")
            {
                lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " ASC";
                Session["projectOrder"] = "ASC";
            }
            else
            {
                lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " DESC";
                Session["projectOrder"] = "DESC";                
            }
        }
        else
        {
            lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " DESC";
            Session["projectOrder"] = "DESC";
        }

        Session["projectColumn"] = e.SortExpression.ToString();
        Session["projectOrderBy"] = lblOrderBy.Text;
        DataGrid1.CurrentPageIndex = 0;
        bindData();
    }

    protected void lnkAll_Click(object sender, EventArgs e)
    {
        strfilter = "";
        Session["Current"] = "ALL";
        Session["Word"] = "ALL";
        DataGrid1.CurrentPageIndex = 0;
        bindData();

    }
    protected void lnkA_Click(object sender, EventArgs e)
    {
        string strchar = "A";
        Session["Current"] = "ONE";
        Session["Word"] = strchar;
        DataGrid1.CurrentPageIndex = 0;
        bindData();
    }
    protected void lnkB_Click(object sender, EventArgs e)
    {
        string strchar = "B";
        Session["Current"] = "ONE";
        Session["Word"] = strchar;
        DataGrid1.CurrentPageIndex = 0;
        bindData();
    }
    protected void lnkC_Click(object sender, EventArgs e)
    {
        string strchar = "C";
        Session["Current"] = "ONE";
        Session["Word"] = strchar;
        DataGrid1.CurrentPageIndex = 0;
        bindData();
    }
    protected void lnkD_Click(object sender, EventArgs e)
    {
        string strchar = "D";
        Session["Current"] = "ONE";
        Session["Word"] = strchar;
        DataGrid1.CurrentPageIndex = 0;
        bindData();
    }
    protected void lnkE_Click(object sender, EventArgs e)
    {
        string strchar = "E";
        Session["Current"] = "ONE";
        Session["Word"] = strchar;
        DataGrid1.CurrentPageIndex = 0;
        bindData();
    }
    protected void lnkF_Click(object sender, EventArgs e)
    {
        string strchar = "F";
        Session["Current"] = "ONE";
        Session["Word"] = strchar;
        DataGrid1.CurrentPageIndex = 0;
        bindData();
    }
    protected void lnkG_Click(object sender, EventArgs e)
    {
        string strchar = "G";
        Session["Current"] = "ONE";
        Session["Word"] = strchar;
        DataGrid1.CurrentPageIndex = 0;
        bindData();
    }
    protected void lnkH_Click(object sender, EventArgs e)
    {
        string strchar = "H";
        Session["Current"] = "ONE";
        Session["Word"] = strchar;
        DataGrid1.CurrentPageIndex = 0;
        bindData();
    }
    protected void lnkI_Click(object sender, EventArgs e)
    {
        string strchar = "I";
        Session["Current"] = "ONE";
        Session["Word"] = strchar;
        DataGrid1.CurrentPageIndex = 0;
        bindData();
    }
    protected void lnkJ_Click(object sender, EventArgs e)
    {
        string strchar = "J";
        Session["Current"] = "ONE";
        Session["Word"] = strchar;
        DataGrid1.CurrentPageIndex = 0;
        bindData();
    }
    protected void lnkK_Click(object sender, EventArgs e)
    {
        string strchar = "K";
        Session["Current"] = "ONE";
        Session["Word"] = strchar;
        DataGrid1.CurrentPageIndex = 0;
        bindData();
    }
    protected void lnkL_Click(object sender, EventArgs e)
    {
        string strchar = "L";
        Session["Current"] = "ONE";
        Session["Word"] = strchar;
        DataGrid1.CurrentPageIndex = 0;
        strfilter = "where project_name like '" + strchar + "%'";
        bindData();

    }
    protected void lnkM_Click(object sender, EventArgs e)
    {
        string strchar = "M";
        Session["Current"] = "ONE";
        Session["Word"] = strchar;
        DataGrid1.CurrentPageIndex = 0;
        bindData();
    }
    protected void lnkN_Click(object sender, EventArgs e)
    {
        string strchar = "N";
        Session["Current"] = "ONE";
        Session["Word"] = strchar;
        DataGrid1.CurrentPageIndex = 0;
        bindData();
    }
    protected void lnkO_Click(object sender, EventArgs e)
    {
        string strchar = "O";
        Session["Current"] = "ONE";
        Session["Word"] = strchar;
        DataGrid1.CurrentPageIndex = 0;
        bindData();

    }
    protected void lnkP_Click(object sender, EventArgs e)
    {
        string strchar = "P";
        Session["Current"] = "ONE";
        Session["Word"] = strchar;
        DataGrid1.CurrentPageIndex = 0;
        bindData();

    }
    protected void lnkQ_Click(object sender, EventArgs e)
    {
        string strchar = "Q";
        Session["Current"] = "ONE";
        Session["Word"] = strchar;
        DataGrid1.CurrentPageIndex = 0;
        bindData();
    }
    protected void lnkR_Click(object sender, EventArgs e)
    {
        string strchar = "R";
        Session["Current"] = "ONE";
        Session["Word"] = strchar;
        DataGrid1.CurrentPageIndex = 0;
        bindData();

    }
    protected void lnkS_Click(object sender, EventArgs e)
    {
        string strchar = "S";
        Session["Current"] = "ONE";
        Session["Word"] = strchar;
        DataGrid1.CurrentPageIndex = 0;
        bindData();

    }
    protected void lnkT_Click(object sender, EventArgs e)
    {
        string strchar = "T";
        Session["Current"] = "ONE";
        Session["Word"] = strchar;
        DataGrid1.CurrentPageIndex = 0;
        bindData();

    }
    protected void lnkU_Click(object sender, EventArgs e)
    {

        string strchar = "U";
        Session["Current"] = "ONE";
        Session["Word"] = strchar;
        DataGrid1.CurrentPageIndex = 0;
        bindData();
    }
    protected void lnkV_Click(object sender, EventArgs e)
    {
        string strchar = "V";
        Session["Current"] = "ONE";
        Session["Word"] = strchar;
        DataGrid1.CurrentPageIndex = 0;
        bindData();
    }
    protected void lnkW_Click(object sender, EventArgs e)
    {
        string strchar = "W";
        Session["Current"] = "ONE";
        Session["Word"] = strchar;
        DataGrid1.CurrentPageIndex = 0;
        bindData();
    }
    protected void lnkX_Click(object sender, EventArgs e)
    {
        string strchar = "X";
        Session["Current"] = "ONE";
        Session["Word"] = strchar;
        DataGrid1.CurrentPageIndex = 0;
        bindData();
    }
    protected void lnkY_Click(object sender, EventArgs e)
    {
        string strchar = "Y";
        Session["Current"] = "ONE";
        Session["Word"] = strchar;
        DataGrid1.CurrentPageIndex = 0;
        bindData();
    }
    protected void lnkZ_Click(object sender, EventArgs e)
    {
        string strchar = "Z";
        Session["Current"] = "ONE";
        Session["Word"] = strchar;
        DataGrid1.CurrentPageIndex = 0;
        bindData();
    }
}
