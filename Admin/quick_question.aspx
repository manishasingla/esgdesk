<%@ Page Language="C#" AutoEventWireup="true" CodeFile="quick_question.aspx.cs" Inherits="Admin_quick_question" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        Quick question
    </title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        function checkChecked()
        {
            if(document.getElementById("chkShowAllHistory").checked==true)
            {
                document.getElementById("divNoHistory").style.display="none";
                document.getElementById("divAllHistory").style.display="block";       
            }
            else
            {
                document.getElementById("divNoHistory").style.display="block";
                document.getElementById("divAllHistory").style.display="none";       
            }
        }
    </script>

</head>
<body onload="checkChecked();">
    <form id="form1" runat="server">
        <div id="wrapper">
            <div id="Content">
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
                <div class="titleText" style="margin: 10px 0 10px 0;">
                    Quick Question</div>
                <div id="divMessage" runat="server" style="color: Red; font-weight: bold">
                </div>
                <div id="DivEntry" runat="server">
                    <div id="divTask" runat="server" class="divBorder">
                        <table width="750" cellpadding="2">
                            <tr>
                                <td align="left" colspan="4" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                Re-assign:&nbsp;
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="drpUsers" runat="server" Width="175px">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10px">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="drpUsers"
                                                    ErrorMessage="Re-assign to employee is required.">*</asp:RequiredFieldValidator>
                                            </td>
                                            <td style="width: 10px">
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td style="width: 10px">
                                            </td>
                                            <td style="width: 10px">
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 10px">
                                            </td>
                                            <td style="height: 10px">
                                            </td>
                                            <td style="height: 10px;">
                                            </td>
                                            <td style="height: 10px">
                                            </td>
                                            <td style="height: 10px">
                                            </td>
                                            <td style="height: 10px">
                                            </td>
                                            <td style="height: 10px;">
                                            </td>
                                            <td style="height: 10px">
                                            </td>
                                            <td style="height: 10px">
                                            </td>
                                            <td style="width: 10px">
                                            </td>
                                            <td style="height: 10px">
                                            </td>
                                            <td style="width: 10px; height: 10px">
                                            </td>
                                        </tr>
                                        <tr style="display: none">
                                            <td>
                                                Change project:&nbsp;
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="drpProjects" runat="server" Width="175px">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10px">
                                            </td>
                                            <td style="width: 10px">
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td style="width: 10px">
                                            </td>
                                            <td style="width: 10px">
                                            </td>
                                            <td>
                                                Status:
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="drpStatuses" runat="server" Width="175px">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr style="display: none">
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td style="width: 10px">
                                            </td>
                                            <td style="width: 10px">
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td style="width: 10px">
                                            </td>
                                            <td style="width: 10px">
                                            </td>
                                            <td style="height: 10px">
                                            </td>
                                            <td style="height: 10px">
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr style="display: none">
                                            <td>
                                                Project type:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="drpProjectType" runat="server" Width="175px">
                                                    <asp:ListItem></asp:ListItem>
                                                    <asp:ListItem>es</asp:ListItem>
                                                    <asp:ListItem Selected="True">other</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10px">
                                            </td>
                                            <td style="width: 10px">
                                            </td>
                                            <td>
                                                Category:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="drpCategories" runat="server" Width="175px">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10px">
                                            </td>
                                            <td style="width: 10px">
                                            </td>
                                            <td>
                                                Priority:
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="drpPriorities" runat="server" Width="175px">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="4" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                    </table>
                                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server" TargetControlID="RequiredFieldValidator3">
                                    </cc1:ValidatorCalloutExtender>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    Quick question:
                                </td>
                                <td align="left" valign="top">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top">
                                </td>
                                <td align="left" valign="top">
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="4" valign="top">
                                    <%--**************************************************************************************************************--%>
                                    <%--<FTB:FreeTextBox ID="ftbComment" runat="server" Height="200px" Width="100%"></FTB:FreeTextBox>--%>
                                    <telerik:RadEditor ID="ftbComment" runat="server" Height="300px" Width="100%">
                                        <Snippets>
                                            <telerik:EditorSnippet Name="Product Template" Value="<table><tr><td><div style='width:300px;border:2px outset #D8D2BD;padding:34px;font-family:Arial, Verdana; font-size:11px;background- color:#F1EFE6;'>Dear ____________________,<br>Thank you for inquiring about ____________. Your request will be processed in 48 hours and shipped at the address you have provided.<b>Please, contact us if you have any problems.</b></div></td></tr></table>">            
                                            </telerik:EditorSnippet>
                                            <telerik:EditorSnippet Name="Signature" Value="<strong>John Smith</strong><br>Sales Manager<br>">
                                            </telerik:EditorSnippet>
                                        </Snippets>
                                        <Links>
                                            <telerik:EditorLink Name="Telerik" Href="http://www.telerik.com">
                                                <telerik:EditorLink Name="Telerik Products" Href="http://www.telerik.com/products"
                                                    Target="_blank" />
                                                <telerik:EditorLink Name="Telerik Support Center" Href="http://www.telerik.com/support/home.aspx"
                                                    Target="_blank" />
                                                <telerik:EditorLink Name="Telerik Community" Href="http://www.telerik.com/community/home.aspx"
                                                    Target="_blank" ToolTip="Telerik Community" />
                                            </telerik:EditorLink>
                                        </Links>
                                        <Tools>
                                        </Tools>
                                        <ImageManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                        <MediaManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                        <FlashManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                        <TemplateManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                        <DocumentManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                        <Modules>
                                            <telerik:EditorModule Name="RadEditorStatistics" Visible="False" Enabled="true" />
                                            <telerik:EditorModule Name="RadEditorDomInspector" Visible="False" Enabled="true" />
                                            <telerik:EditorModule Name="RadEditorNodeInspector" Visible="False" Enabled="true" />
                                            <telerik:EditorModule Name="RadEditorHtmlInspector" Visible="False" Enabled="true" />
                                        </Modules>
                                        <Content>
                                        </Content>
                                    </telerik:RadEditor>
                                    <%--**************************************************************************************************************--%>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top" colspan="3">
                                    <div id="divMessage1" runat="server" style="color: Red">
                                    </div>
                                </td>
                                <td align="right" valign="top">
                                    <asp:Button ID="btnUpdate" runat="server" Text="Create" OnClick="btnUpdate_Click"
                                        CssClass="blueBtns" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
