using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Admin_quick_question : System.Web.UI.Page
{
    int id;    
    string sql = "";
    string task_action = "";
    string emailSubject = "";
    string shortDescription = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
           Session["returnUrl"] = "quick_question.aspx";
            Response.Redirect("login.aspx");
            return;
        }

        divMessage1.InnerHtml = "";
        divMessage.InnerHtml = "";

        string task_id = Request["id"];
        if (task_id == null || task_id == "0" || task_id == "")
        {
            id = 0;
        }
        else
        {
            if (DatabaseHelper.is_int(task_id))
            {
                id = Convert.ToInt32(task_id);
            }
            else
            {
                // Display an error because the bugid must be an integer
                divMessage.InnerHtml = "Task ID must be an integer.";
                divMessage.InnerHtml += "<p><a href='tasks.aspx'>View tasks</a></p>";
                DivEntry.Visible = false;
                return;
            }
        }

        if (!Page.IsPostBack)
        {
            load_dropdowns();
            set_default_selection();            
        }
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        if (drpUsers.SelectedIndex == 0)
        {
            divMessage1.InnerHtml = "Re-assigned to emplyee is required.";
            return;
        }       
        //*****************************************************
        //if (ftbComment.Text.Trim() == "")
        if (ftbComment.Content.Trim() == "")
        //*****************************************************
        {
            divMessage1.InnerHtml = "Quick question is required.";
            return;
        }
        else
        {

            shortDescription = "Question asked - please respond.";
            
            shortDescription = shortDescription.Replace("<p>", " ");
            shortDescription = shortDescription.Replace("</p>", " ");
            shortDescription = shortDescription.Replace("<br/>", " ");
            shortDescription = shortDescription.Replace("<br>", " ");
            shortDescription = shortDescription.Replace("<div>", " ");
            shortDescription = shortDescription.Replace("</div>", " ");
            shortDescription = shortDescription.Replace("<P>", " ");
            shortDescription = shortDescription.Replace("</P>", " ");
            shortDescription = shortDescription.Replace("<BR/>", " ");
            shortDescription = shortDescription.Replace("<BR>", " ");
            shortDescription = shortDescription.Replace("<DIV>", " ");
            shortDescription = shortDescription.Replace("</DIV>", " ");
            shortDescription = shortDescription.Replace("<B>", " ");
            shortDescription = shortDescription.Replace("</B>", " ");
            shortDescription = shortDescription.Replace("<b>", " ");
            shortDescription = shortDescription.Replace("</b>", " ");

            sql = @"DECLARE @TaskId INT;";
            sql += " insert into tasks([RequestId],[short_desc],[reported_user],[reported_date],[OrderId],[status],[priority],[category],[project],[project_type],[Relevant_URL],[assigned_to_user],[last_updated_user],[last_updated_date],[deleted]) ";
            sql += " values(0,";
            sql += "'" + shortDescription.Trim().Replace("'", "''") + "',";
            sql += "'" + Session["admin"].ToString().Replace("'", "''") + "',";
            sql += "getdate(),";
            sql += "99999,";
            sql += "'" + drpStatuses.SelectedItem.Text.Replace("'", "''") + "',";
            sql += "'" + drpPriorities.SelectedItem.Text.Replace("'", "''") + "',";
            sql += "'" + drpCategories.SelectedItem.Text.Replace("'", "''") + "',";
            sql += "'" + drpProjects.SelectedItem.Text.Replace("'", "''") + "',";
            sql += "'" + drpProjectType.SelectedItem.Text.Replace("'", "''") + "',";
            sql += "'',";
            sql += "'" + drpUsers.SelectedItem.Text.Replace("'", "''") + "',";
            sql += "'" + Session["admin"].ToString().Replace("'", "''") + "',";
            sql += "getdate(),";
            sql += "0); ";
            sql += " SET @TaskId = SCOPE_IDENTITY(); ";
            //***************************************************
            //if (ftbComment.Text.Trim() != "")
              if (ftbComment.Content.Trim() != "")
            //***************************************************
            {
                sql += " insert into task_comments([task_id],[username],[post_date],[comment],[deleted],[qflag]) ";
                sql += " values(@TaskId,";
                sql += "'" + Session["admin"].ToString().Replace("'", "''") + "',";
                sql += "getdate(),";
                //**************************************************************************
                //sql += "'" + ftbComment.Text.Trim().Replace("'", "''") + "',";
                sql += "'" + ftbComment.Content.Trim().Replace("'", "''") + "',";
                //**************************************************************************
                sql += "0,1); ";
            }
            sql += "select @TaskId;";

            object objResult = DatabaseHelper.executeScalar(sql);

            if (objResult.ToString() != "0")
            {
                // add subscribers for the task
                add_subscription(objResult);

                emailSubject = "Task ID:" + objResult.ToString() + " was added - " + shortDescription.Trim() + " (Task ID:" + objResult.ToString() + ")";

                task_action = ((string)DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + Session["admin"].ToString() + "'")) + " has added quick question ";
                
                sendNotification(objResult.ToString());

                divMessage1.InnerHtml = "Quick question was created.";
            }
            else
            {
                divMessage1.InnerHtml = "Quick question was not created.";
            }
        }       
    }

    void sendNotification(string strTaskId)
    {
        sql = "select email ";
        sql += " from users, task_subscriptions ";
        sql += " where users.username = task_subscriptions.username ";
        sql += " and task_subscriptions.task_id =" + strTaskId.ToString();

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            string toEmails = "";

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (toEmails == "")
                {
                    toEmails = ds.Tables[0].Rows[i]["email"].ToString();
                }
                else
                {
                    toEmails += ";" + ds.Tables[0].Rows[i]["email"].ToString();
                }
            }
            string strHtml = generateAdminEmail(strTaskId);

            if (emailSubject == "")
            {
                emailSubject = "Task ID:" + strTaskId.ToString() + " was updated - " + shortDescription.Trim() + " (Task ID:" + strTaskId.ToString() + ")";
            }

            bool flag = DatabaseHelper.sendEmailTasks(toEmails, emailSubject, strHtml);
        }
        else
        {

        }
    }

    private string generateAdminEmail(string strTaskId)
    {
        DataSet dsTask = DatabaseHelper.getDataset("select * from tasks where task_id=" + strTaskId);
        string strBody = "";
        if (dsTask == null || dsTask.Tables.Count <= 0 || dsTask.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "Task not found.<br><br><a href='tasks.aspx'>View tasks</a>";
        }
        else
        {
            strBody += "Task: <span style=\"color:red\">";

            strBody += task_action;

            strBody += " (<a style=\"color:red\" href='"+ ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_task.aspx?id=" + strTaskId + "'>" + strTaskId + "</a>) </span> " + DateTime.Parse(dsTask.Tables[0].Rows[0]["last_updated_date"].ToString()).ToString("dd MMM h:mm tt") + "<br><br>";

            strBody += "<a href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_task.aspx?id=" + strTaskId + "'>" + shortDescription.Trim() + "</a>&nbsp;&nbsp;&nbsp;(" + dsTask.Tables[0].Rows[0]["priority"].ToString() + " : " + dsTask.Tables[0].Rows[0]["status"].ToString() + ")<br><br>";
            strBody += "Emp: " + ((string)DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + dsTask.Tables[0].Rows[0]["assigned_to_user"].ToString() + "'")) + "<br>";
            strBody += "Project: " + dsTask.Tables[0].Rows[0]["project"].ToString() + "<br>";
            strBody += "Relevant URL: <a href='"  + dsTask.Tables[0].Rows[0]["Relevant_URL"].ToString() + "' target='_blank'>" + dsTask.Tables[0].Rows[0]["Relevant_URL"].ToString() + "</a><br>";
            strBody += "<br><br>";

            sql = "Select * from task_comments where task_id=" + strTaskId.ToString() + " and deleted <> 1 order by tc_id desc";

            DataSet dsTaskDetails = DatabaseHelper.getDataset(sql);

            strBody += "<table cellspacing=\"0\" border=\"1\" style=\"width: 99%;border-collapse: collapse;\">";
            strBody += "<tbody>";

            for (int i = 0; i < dsTaskDetails.Tables[0].Rows.Count; i++)
            {
                strBody += "<tr>";
                strBody += "<td>";
                strBody += "<div style=\"border: 1px none Green;\">";
                strBody += "<table width=\"100%\">";
                strBody += "<tbody>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                if (dsTaskDetails.Tables[0].Rows[i]["qflag"].ToString() == "1")
                {
                    strBody += "<span style=\"color: red;\">Question posted by " + dsTaskDetails.Tables[0].Rows[i]["username"].ToString() + " on " + DateTime.Parse(dsTaskDetails.Tables[0].Rows[i]["post_date"].ToString()).ToString("dd MMM h:mm tt") + "</span>";
                }
                else
                {
                    strBody += "<span style=\"color: Green;\">comment " + dsTaskDetails.Tables[0].Rows[i]["tc_id"].ToString() + " posted by " + dsTaskDetails.Tables[0].Rows[i]["username"].ToString() + " on " + DateTime.Parse(dsTaskDetails.Tables[0].Rows[i]["post_date"].ToString()).ToString("dd MMM h:mm tt") + "</span>";
                }
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\"> </td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span>" + dsTaskDetails.Tables[0].Rows[i]["comment"].ToString() + "</span>";
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "</tbody>";
                strBody += "</table>";
                strBody += "</div>";
                strBody += "</td>";
                strBody += "</tr>";
            }
            strBody += "</tbody>";
            strBody += "</table>";
        }
        return strBody;
    }
    
    void load_dropdowns()
    {
        // projects
        sql = @"select project_name
		from projects
		where active = 'Y' order by project_name;";

        // categories
        sql += "\nselect category_name from categories order by sort_seq, category_name;";

        // priorities
        sql += "\nselect priority_name from priorities order by sort_seq, priority_name;";

        // statuses
        sql += "\nselect status_name from statuses order by sort_seq, status_name;";

        // users
        sql += "\nselect username from users where active = 'Y' order by username;";

        // do a batch of sql statements
        DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);

        drpProjects.DataSource = ds_dropdowns.Tables[0];
        drpProjects.DataTextField = "project_name";
        drpProjects.DataValueField = "project_name";
        drpProjects.DataBind();
        drpProjects.Items.Insert(0, new ListItem("[no project]", ""));


        drpCategories.DataSource = ds_dropdowns.Tables[1];
        drpCategories.DataTextField = "category_name";
        drpCategories.DataValueField = "category_name";
        drpCategories.DataBind();
        drpCategories.Items.Insert(0, new ListItem("[no category]", ""));

        drpPriorities.DataSource = ds_dropdowns.Tables[2];
        drpPriorities.DataTextField = "priority_name";
        drpPriorities.DataValueField = "priority_name";
        drpPriorities.DataBind();
        drpPriorities.Items.Insert(0, new ListItem("[no priority]", ""));

        drpStatuses.DataSource = ds_dropdowns.Tables[3];
        drpStatuses.DataTextField = "status_name";
        drpStatuses.DataValueField = "status_name";
        drpStatuses.DataBind();
        drpStatuses.Items.Insert(0, new ListItem("[no status]", ""));

        drpUsers.DataSource = ds_dropdowns.Tables[4];
        drpUsers.DataTextField = "username";
        drpUsers.DataValueField = "username";
        drpUsers.DataBind();
        drpUsers.Items.Insert(0, new ListItem("[not assigned]", ""));
    }
    void set_default_selection()
    {
        sql = "\nselect top 1 project_name from projects where default_selection = 'Y' order by project_name;"; // 0
        sql += "\nselect top 1 category_name from categories where default_selection = 'Y' order by category_name;";  // 1
        sql += "\nselect top 1 priority_name from priorities where default_selection = 'Y' order by priority_name;"; // 2
        sql += "\nselect top 1 status_name from statuses where default_selection = 'Y' order by status_name;"; // 3

        DataSet ds_defaults = DatabaseHelper.getDataset(sql);

        string default_value;

        // projects
        if (ds_defaults.Tables[0].Rows.Count > 0)
        {
            default_value = ds_defaults.Tables[0].Rows[0][0].ToString();
        }
        else
        {
            default_value = "0";
        }
        foreach (ListItem li in drpProjects.Items)
        {
            if (li.Value == default_value)
            {
                li.Selected = true;
            }
            else
            {
                li.Selected = false;
            }
        }

        // categories
        if (ds_defaults.Tables[1].Rows.Count > 0)
        {
            default_value = "question";
        }
        else
        {
            default_value = "0";
        }
        foreach (ListItem li in drpCategories.Items)
        {
            if (li.Value == default_value)
            {
                li.Selected = true;
            }
            else
            {
                li.Selected = false;
            }
        }
        // priorities
        if (ds_defaults.Tables[2].Rows.Count > 0)
        {
            default_value = "0 - IMMEDIATE";
        }
        else
        {
            default_value = "0";
        }
        foreach (ListItem li in drpPriorities.Items)
        {
            if (li.Value == default_value)
            {
                li.Selected = true;
            }
            else
            {
                li.Selected = false;
            }
        }

        // statuses
        if (ds_defaults.Tables[3].Rows.Count > 0)
        {
            default_value = ds_defaults.Tables[3].Rows[0][0].ToString();
        }
        else
        {
            default_value = "0";
        }
        foreach (ListItem li in drpStatuses.Items)
        {
            if (li.Value == default_value)
            {
                li.Selected = true;
            }
            else
            {
                li.Selected = false;
            }
        }
    }
    
    void add_subscription(object objTaskid)
    {
        int intResult;

        sql = "insert into task_subscriptions ([task_id],[username]) ";
        sql += "values(" + objTaskid.ToString() + ",'" + Session["admin"].ToString().Replace("'", "''") + "'); ";

        if (drpUsers.SelectedIndex > 0 && (drpUsers.SelectedValue != Session["admin"].ToString()))
        {
            sql += " insert into task_subscriptions ([task_id],[username]) ";
            sql += "values(" + objTaskid.ToString() + ",'" + drpUsers.SelectedValue.Replace("'", "''") + "'); ";
        }

        intResult = DatabaseHelper.executeNonQuery(sql);
    }
}
