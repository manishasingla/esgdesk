using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Admin_report : System.Web.UI.Page
{
    string sql = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        LnkMakecommentRead.Attributes.Add("onclick", "return confirm('Sure you want to mark every comment in every CR as read by you?');");
        LnkMakeTaskcommentRead.Attributes.Add("onclick", "javascript:return confirm('Sure you want to mark every comment in each of your tasks as read by you (this means you must have read them all and acted on all that require action already)?');");
        LnkMakeAllTaskCmmntRead.Attributes.Add("onclick", "javascript:return confirm('Sure you want to mark every comment in each of  tasks as read by you (this means you must have read them all and acted on all that require action already)?');");
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = "report.aspx";
            Response.Redirect("login.aspx");
            return;
        }

        TopFilterLable.InnerHtml = "";
        divMessage.InnerHtml = "";

        if (!Page.IsPostBack)
        {
            if (!DatabaseHelper.can_Show_Deleted_Tasks(Session["admin"].ToString()))
            {
                chkViewDeleted.Visible = false;
            }
            else
            {
                chkViewDeleted.Visible = true;
            }

            if (Session["filter"] != null)
            {
                if (Session["filter"].ToString() == "Immediate")
                {
                    bindPR0Tasks();
                }
                else if (Session["filter"].ToString() == "Unanswered")
                {
                    bindUnansweredQuestions();
                }
                else if (Session["filter"].ToString() == "HighestTasks")
                {
                    bindPR1aTasks();
                }
                else if (Session["filter"].ToString() == "HighTasks")
                {
                    bindPR1bTasks();
                }
                else
                {
                    bindDefault();
                }
            }
            else
            {
                bindDefault();
            }

            getNewCR();
            getCRNewComment();
            getUnasweredQuestions();
            getPR0Tasks();
            getPR1aTasks();
            getPR1bTasks();
            getTskNewComment();
            getAllTskNewComment();
        }
    }

    private void BindData()
    {
        string filter = "where ";

        string topFilterLable = "";

        if (!DatabaseHelper.can_Show_Closed_Tasks(Session["admin"].ToString()))
        {
            filter += " status <> 'closed' and ";

            chkHideClosed.Visible = false;
        }
        else
        {
            if (chkHideClosed.Checked == true)
            {
                filter += " status <> 'closed' and status <> 'checked' and ";
            }
        }

        if (chkViewDeleted.Checked == true)
        {
            filter += " deleted = 1 ";
        }
        else
        {
            filter += " deleted <> 1 ";
        }

        string drpValue = drpFilter.SelectedValue;
        string username = "";

        if (drpValue.Contains("Assigned by me"))
        {
            filter += " and reported_user = '" + username + "' ";
        }
        else if (drpValue.Contains("Assigned by "))
        {
            username = drpValue.Replace("Assigned by ", "");

            filter += " and reported_user = '" + username + "' ";
        }
        else if (drpValue.Contains("All emps CWO"))
        {
            filter += " and status = 'CWO' ";
        }
        else if (drpValue.Contains("Show my open tasks"))
        {
            filter += " and assigned_to_user = '" + Session["admin"] + "' ";
        }

        if (drpProjects.SelectedIndex != 0)
        {
            filter += " and project = '" + drpProjects.SelectedValue + "' ";

            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Project: " + drpProjects.SelectedValue;
            }
            else
            {
                topFilterLable += " / Project: " + drpProjects.SelectedValue;
            }
        }

        if (drpCategories.SelectedIndex != 0)
        {
            filter += " and category = '" + drpCategories.SelectedValue + "' ";

            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Category: " + drpCategories.SelectedValue;
            }
            else
            {
                topFilterLable += " / Category: " + drpCategories.SelectedValue;
            }
        }

        if (drpReportedBy.SelectedIndex != 0)
        {
            filter += " and reported_user = '" + drpReportedBy.SelectedValue + "' ";

            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Reported by: " + drpReportedBy.SelectedValue;
            }
            else
            {
                topFilterLable += " / Reported by:" + drpReportedBy.SelectedValue;
            }
        }

        if (drpPriorities.SelectedIndex != 0)
        {
            filter += " and priority = '" + drpPriorities.SelectedValue + "' ";

            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Priority: " + drpPriorities.SelectedValue;
            }
            else
            {
                topFilterLable += " / Priority:" + drpPriorities.SelectedValue;
            }
        }

        if (drpAssignedTo.SelectedIndex != 0)
        {
            filter += " and assigned_to_user = '" + drpAssignedTo.SelectedValue + "' ";

            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Assigned to: " + drpAssignedTo.SelectedValue;
            }
            else
            {
                topFilterLable += " / Assigned to: " + drpAssignedTo.SelectedValue;
            }
        }
        if (drpStatuses.SelectedIndex != 0)
        {
            filter += " and status = '" + drpStatuses.SelectedValue + "' ";

            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Status: " + drpStatuses.SelectedValue;
            }
            else
            {
                topFilterLable += " / Status: " + drpStatuses.SelectedValue;
            }
        }

        if (chkViewDeleted.Checked == true)
        {
            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by View deleted";
            }
            else
            {
                topFilterLable += " / View deleted";
            }
        }

        if (chkHideClosed.Checked == true)
        {
          using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Admin_report : System.Web.UI.Page
{
    string sql = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        LnkMakecommentRead.Attributes.Add("onclick", "return confirm('Sure you want to mark every comment in every CR as read by you?');");
        LnkMakeTaskcommentRead.Attributes.Add("onclick", "javascript:return confirm('Sure you want to mark every comment in each of your tasks as read by you (this means you must have read them all and acted on all that require action already)?');");
        LnkMakeAllTaskCmmntRead.Attributes.Add("onclick", "javascript:return confirm('Sure you want to mark every comment in each of  tasks as read by you (this means you must have read them all and acted on all that require action already)?');");
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = "report.aspx";
            Response.Redirect("login.aspx");
            return;
        }

        TopFilterLable.InnerHtml = "";
        divMessage.InnerHtml = "";

        if (!Page.IsPostBack)
        {
            if (!DatabaseHelper.can_Show_Deleted_Tasks(Session["admin"].ToString()))
            {
                chkViewDeleted.Visible = false;
            }
            else
            {
                chkViewDeleted.Visible = true;
            }

            if (Session["filter"] != null)
            {
                if (Session["filter"].ToString() == "Immediate")
                {
                    bindPR0Tasks();
                }
                else if (Session["filter"].ToString() == "Unanswered")
                {
                    bindUnansweredQuestions();
                }
                else if (Session["filter"].ToString() == "HighestTasks")
                {
                    bindPR1aTasks();
                }
                else if (Session["filter"].ToString() == "HighTasks")
                {
                    bindPR1bTasks();
                }
                else
                {
                    bindDefault();
                }
            }
            else
            {
                bindDefault();
            }

            getNewCR();
            getCRNewComment();
            getUnasweredQuestions();
            getPR0Tasks();
            getPR1aTasks();
            getPR1bTasks();
            getTskNewComment();
            getAllTskNewComment();
        }
    }

    private void BindData()
    {
        string filter = "where ";

        string topFilterLable = "";

        if (!DatabaseHelper.can_Show_Closed_Tasks(Session["admin"].ToString()))
        {
            filter += " status <> 'closed' and ";

            chkHideClosed.Visible = false;
        }
        else
        {
            if (chkHideClosed.Checked == true)
            {
                filter += " status <> 'closed' and status <> 'checked' and ";
            }
        }

        if (chkViewDeleted.Checked == true)
        {
            filter += " deleted = 1 ";
        }
        else
        {
            filter += " deleted <> 1 ";
        }

        string drpValue = drpFilter.SelectedValue;
        string username = "";

        if (drpValue.Contains("Assigned by me"))
        {
            filter += " and reported_user = '" + username + "' ";
        }
        else if (drpValue.Contains("Assigned by "))
        {
            username = drpValue.Replace("Assigned by ", "");

            filter += " and reported_user = '" + username + "' ";
        }
        else if (drpValue.Contains("All emps CWO"))
        {
            filter += " and status = 'CWO' ";
        }
        else if (drpValue.Contains("Show my open tasks"))
        {
            filter += " and assigned_to_user = '" + Session["admin"] + "' ";
        }

        if (drpProjects.SelectedIndex != 0)
        {
            filter += " and project = '" + drpProjects.SelectedValue + "' ";

            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Project: " + drpProjects.SelectedValue;
            }
            else
            {
                topFilterLable += " / Project: " + drpProjects.SelectedValue;
            }
        }

        if (drpCategories.SelectedIndex != 0)
        {
            filter += " and category = '" + drpCategories.SelectedValue + "' ";

            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Category: " + drpCategories.SelectedValue;
            }
            else
            {
                topFilterLable += " / Category: " + drpCategories.SelectedValue;
            }
        }

        if (drpReportedBy.SelectedIndex != 0)
        {
            filter += " and reported_user = '" + drpReportedBy.SelectedValue + "' ";

            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Reported by: " + drpReportedBy.SelectedValue;
            }
            else
            {
                topFilterLable += " / Reported by:" + drpReportedBy.SelectedValue;
            }
        }

        if (drpPriorities.SelectedIndex != 0)
        {
            filter += " and priority = '" + drpPriorities.SelectedValue + "' ";

            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Priority: " + drpPriorities.SelectedValue;
            }
            else
            {
                topFilterLable += " / Priority:" + drpPriorities.SelectedValue;
            }
        }

        if (drpAssignedTo.SelectedIndex != 0)
        {
            filter += " and assigned_to_user = '" + drpAssignedTo.SelectedValue + "' ";

            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Assigned to: " + drpAssignedTo.SelectedValue;
            }
            else
            {
                topFilterLable += " / Assigned to: " + drpAssignedTo.SelectedValue;
            }
        }
        if (drpStatuses.SelectedIndex != 0)
        {
            filter += " and status = '" + drpStatuses.SelectedValue + "' ";

            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Status: " + drpStatuses.SelectedValue;
            }
            else
            {
                topFilterLable += " / Status: " + drpStatuses.SelectedValue;
            }
        }

        if (chkViewDeleted.Checked == true)
        {
            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by View deleted";
            }
            else
            {
                topFilterLable += " / View deleted";
            }
        }

        if (chkHideClosed.Checked == true)
        {
            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Hide closed";
            }
            else
            {
                topFilterLable += " / Hide closed";
            }
        }

        TopFilterLable.InnerHtml = topFilterLable;

        sql = "select * from tasks " + filter + " " + lblOrderBy.Text;

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "No task yet.";
            DataGrid1.DataSource = null;
            DataGrid1.DataBind();
        }
        else
        {
            if (ds.Tables[0].Rows.Count <= 100)
            {
                DataGrid1.PagerStyle.Visible = false;
                DataGrid1.CurrentPageIndex = 0;
            }
            else
            {
                DataGrid1.PagerStyle.Visible = true;
            }

            divMessage.InnerHtml = "";
            DataGrid1.DataSource = ds.Tables[0];
            DataGrid1.DataBind();

            getTotalHrsReport("select task_id from tasks " + filter);

        }
    }

    void load_dropdowns()
    {

        // projects
        sql = @"select project_name
		from projects
		where active = 'Y' order by project_name;";

        // categories
        sql += "\nselect category_name from categories order by sort_seq, category_name;";

        // priorities
        sql += "\nselect priority_name from priorities order by sort_seq, priority_name;";

        // statuses
        sql += "\nselect status_name from statuses order by sort_seq, status_name;";

        // users
        sql += "\nselect username from users where active = 'Y' order by username;";

        // do a batch of sql statements
        DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);

        drpProjects.DataSource = ds_dropdowns.Tables[0];
        drpProjects.DataTextField = "project_name";
        drpProjects.DataValueField = "project_name";
        drpProjects.DataBind();
        drpProjects.Items.Insert(0, new ListItem("[no filter]", "0"));


        drpCategories.DataSource = ds_dropdowns.Tables[1];
        drpCategories.DataTextField = "category_name";
        drpCategories.DataValueField = "category_name";
        drpCategories.DataBind();
        drpCategories.Items.Insert(0, new ListItem("[no filter]", "0"));

        drpPriorities.DataSource = ds_dropdowns.Tables[2];
        drpPriorities.DataTextField = "priority_name";
        drpPriorities.DataValueField = "priority_name";
        drpPriorities.DataBind();
        drpPriorities.Items.Insert(0, new ListItem("[no filter]", "0"));

        drpStatuses.DataSource = ds_dropdowns.Tables[3];
        drpStatuses.DataTextField = "status_name";
        drpStatuses.DataValueField = "status_name";
        drpStatuses.DataBind();
        drpStatuses.Items.Insert(0, new ListItem("[no filter]", "0"));

        drpReportedBy.DataSource = ds_dropdowns.Tables[4];
        drpReportedBy.DataTextField = "username";
        drpReportedBy.DataValueField = "username";
        drpReportedBy.DataBind();
        drpReportedBy.Items.Insert(0, new ListItem("[no filter]", "0"));

        drpAssignedTo.DataSource = ds_dropdowns.Tables[4];
        drpAssignedTo.DataTextField = "username";
        drpAssignedTo.DataValueField = "username";
        drpAssignedTo.DataBind();
        drpAssignedTo.Items.Insert(0, new ListItem("[no filter]", "0"));
    }

    void load_filterDropDown()
    {
        drpFilter.Items.Clear();
        if (DatabaseHelper.isAdmin(Session["Admin"].ToString()))
        {
            sql = "select username from users where active='Y' and admin='Y' and username <> '" + Session["Admin"] + "' order by username";

            DataSet dsAdmin = DatabaseHelper.getDataset(sql);

            if (dsAdmin != null)
            {
                if (dsAdmin.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsAdmin.Tables[0].Rows.Count; i++)
                    {
                        drpFilter.Items.Add("Assigned by " + dsAdmin.Tables[0].Rows[i]["username"]);
                    }
                }
            }

            drpFilter.Items.Add("Assigned by " + Session["Admin"]);
            drpFilter.Items.Add("Assigned by me");
            drpFilter.Items.Add("All tasks by last updated");
            drpFilter.Items.Add("All emps CWO");

            drpFilter.Items.Add("Show my open tasks");

            drpFilter.SelectedValue = (string)DatabaseHelper.executeScalar("select default_selected_item from users where username='" + Session["admin"] + "'");
        }
        else
        {
            drpFilter.Items.Add("Show my open tasks");
        }
    }

    protected void drpFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["filter"] = null;
        load_dropdowns();
        setFileterSession();
        DataGrid1.CurrentPageIndex = 0;
        BindData();

    }

    protected void drpProjects_SelectedIndexChanged(object sender, EventArgs e)
    {
        setFileterSession();
        DataGrid1.CurrentPageIndex = 0;
        BindData();
    }

    protected void drpCategories_SelectedIndexChanged(object sender, EventArgs e)
    {
        setFileterSession();
        DataGrid1.CurrentPageIndex = 0;
        BindData();
    }

    protected void drpReportedBy_SelectedIndexChanged(object sender, EventArgs e)
    {
        setFileterSession();
        DataGrid1.CurrentPageIndex = 0;
        BindData();
    }

    protected void drpPriorities_SelectedIndexChanged(object sender, EventArgs e)
    {
        setFileterSession();
        DataGrid1.CurrentPageIndex = 0;
        BindData();
    }

    protected void drpAssignedTo_SelectedIndexChanged(object sender, EventArgs e)
    {
        setFileterSession();
        DataGrid1.CurrentPageIndex = 0;
        BindData();
    }

    protected void drpStatuses_SelectedIndexChanged(object sender, EventArgs e)
    {
        setFileterSession();
        DataGrid1.CurrentPageIndex = 0;
        BindData();
    }

    private void setFileterSession()
    {
        Session["HideClosed"] = chkHideClosed.Checked;
        Session["IncludeDeleted"] = chkViewDeleted.Checked;
        Session["Filter"] = drpFilter.SelectedValue;
        Session["Project"] = drpProjects.SelectedValue;
        Session["Category"] = drpCategories.SelectedValue;
        Session["ReportedBy"] = drpReportedBy.SelectedValue;
        Session["Priority"] = drpPriorities.SelectedValue;
        Session["AssignedTo"] = drpAssignedTo.SelectedValue;
        Session["Status"] = drpStatuses.SelectedValue;
        Session["OrderBy"] = lblOrderBy.Text;
    }

    private void getFilterSession()
    {
        try
        {
            if (Session["HideClosed"] != null)
                chkHideClosed.Checked = (bool)Session["HideClosed"];

            if (Session["IncludeDeleted"] != null)
                chkViewDeleted.Checked = (bool)Session["IncludeDeleted"];

            if (Session["Filter"] != null)
                drpFilter.SelectedValue = Session["Filter"].ToString();

            if (Session["Project"] != null)
                drpProjects.SelectedValue = Session["Project"].ToString();

            if (Session["Category"] != null)
                drpCategories.SelectedValue = Session["Category"].ToString();

            if (Session["ReportedBy"] != null)
                drpReportedBy.SelectedValue = Session["ReportedBy"].ToString();

            if (Session["Priority"] != null)
                drpPriorities.SelectedValue = Session["Priority"].ToString();

            if (Session["AssignedTo"] != null)
                drpAssignedTo.SelectedValue = Session["AssignedTo"].ToString();

            if (Session["Status"] != null)
                drpStatuses.SelectedValue = Session["Status"].ToString();

            if (Session["OrderBy"] != null)
                lblOrderBy.Text = Session["OrderBy"].ToString();
        }
        catch
        {

        }
    }

    protected void DataGrid1_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        if (e.SortExpression.ToString() == Session["Column"])
        {
            //Reverse the sort order
            if (Session["Order"] == "ASC")
            {
                lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " DESC";
                Session["Order"] = "DESC";
            }
            else
            {
                lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " ASC";
                Session["Order"] = "ASC";
            }
        }
        else
        {
            //Different column selected, so default to ascending order
            lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " ASC";
            Session["Order"] = "ASC";
        }

        Session["Column"] = e.SortExpression.ToString();
        Session["OrderBy"] = lblOrderBy.Text;
        BindData();
    }

    protected void DataGrid1_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemIndex != -1)
        {
            //((Label)e.Item.FindControl("lblReportedOn")).Text = DateTime.Parse(((Label)e.Item.FindControl("lblReportedOn")).Text).ToString("dd MMM h:mm tt").ToString();
            //((Label)e.Item.FindControl("lblLastUpdatedOn")).Text = DateTime.Parse(((Label)e.Item.FindControl("lblLastUpdatedOn")).Text).ToString("dd MMM h:mm tt").ToString();

            string htmlColor = (string)DatabaseHelper.executeScalar("select bg_color from priorities where priority_name='" + e.Item.Cells[3].Text + "'");
            e.Item.BackColor = System.Drawing.ColorTranslator.FromHtml(htmlColor);

            int ETC = Convert.ToInt32(e.Item.Cells[9].Text);

            e.Item.Cells[6].Text = ""+ (ETC / 60) + " hrs " + (ETC % 60) + " mins";

            int hrsTaken = getHoursTakenSoFar(e.Item.Cells[0].Text);

            e.Item.Cells[7].Text = "" + (hrsTaken / 60) + " hrs " + (hrsTaken % 60) + " mins";

            int hrsLeft = ETC - hrsTaken;

            if (hrsLeft < 0)
            {
                hrsLeft = -1 * hrsLeft;
                e.Item.Cells[8].Text = "- " + (hrsLeft / 60) + " hrs " + (hrsLeft % 60) + " mins";
            }
            else
            {
                e.Item.Cells[8].Text = "" + (hrsLeft / 60) + " hrs " + (hrsLeft % 60) + " mins";
            }
        }
    }

    protected void DataGrid1_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        DataGrid1.CurrentPageIndex = e.NewPageIndex;
        BindData();
    }

    protected void btnClearFilters_Click(object sender, EventArgs e)
    {
        Session["filter"] = null;
        load_dropdowns();
        load_filterDropDown();
        chkViewDeleted.Checked = false;
        chkHideClosed.Checked = false;
        setFileterSession();
        BindData();
    }

    void getUnasweredQuestions()
    {
////        sql = @"select task_comments.* from task_comments,tasks 
////                where tasks.task_id = task_comments.task_id and tasks.deleted <> 1 and tasks.status <> 'closed' and task_comments.deleted <> 1 and qflag=1 ";

////       sql += " and assigned_to_user = '" + Session["admin"] + "' ";
        sql = @"select task_comments.* from task_comments,tasks 
                where tasks.task_id = task_comments.task_id and tasks.deleted <> 1 and tasks.status <> 'closed' and task_comments.deleted <> 1 and qflag=1 ";

        sql += " and task_comments.QuesTo = '" + Session["admin"] + "' ";


        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            lnkUnansweredQuestions.Text = "(" + ds.Tables[0].Rows.Count + ")";
            unansweredque.Visible = true;
        }
        else
        {
            lnkUnansweredQuestions.Text = "(0)";
            unansweredque.Visible = false;
        }
    }

    void getPR0Tasks()
    {
        sql = @"select tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '0 - IMMEDIATE' ";

        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            sql += " and status <> 'closed' and assigned_to_user = '" + Session["admin"] + "' ";
        }
        else
        {
            sql += " and assigned_to_user = '" + Session["admin"] + "' ";
        }

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            lnkPr0Task.Text = "(" + ds.Tables[0].Rows.Count + ")";
            immediatetask.Visible = true;
        }
        else
        {
            lnkPr0Task.Text = "(0)";
            immediatetask.Visible = false;
        }
    }

    void getPR1aTasks()
    {
        sql = @"select tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '1a - DO NOW' ";

        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            sql += " and status <> 'closed' and assigned_to_user = '" + Session["admin"] + "' ";
        }
        else
        {
            sql += " and assigned_to_user = '" + Session["admin"] + "' ";
        }

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            lnkHighestTasks.Text = "(" + ds.Tables[0].Rows.Count + ")";
            highestTasks.Visible = true;
        }
        else
        {
            lnkHighestTasks.Text = "(0)";
            highestTasks.Visible = false;
        }
    }

    void getPR1bTasks()
    {
        sql = @"select tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '1b - high' ";

        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            sql += " and status <> 'closed' and assigned_to_user = '" + Session["admin"] + "' ";
        }
        else
        {
            sql += " and assigned_to_user = '" + Session["admin"] + "' ";
        }

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            lnkHighTasks.Text = "(" + ds.Tables[0].Rows.Count + ")";
            highTasks.Visible = true;
        }
        else
        {
            lnkHighTasks.Text = "(0)";
            highTasks.Visible = false;
        }
    }

    protected void lnkPr0Task_Click(object sender, EventArgs e)
    {
        Session["filter"] = "Immediate";
        Session["filterunread"] = "";

        Response.Redirect("tasks.aspx");
    }

    protected void lnkHighestTasks_Click(object sender, EventArgs e)
    {
        Session["filter"] = "HighestTasks";
        Session["filterunread"] = "";

        Response.Redirect("tasks.aspx");
    }

    protected void lnkHighTasks_Click(object sender, EventArgs e)
    {
        Session["filter"] = "HighTasks";
        Session["filterunread"] = "";

        Response.Redirect("tasks.aspx");
    }

    protected void lnkUnansweredQuestions_Click(object sender, EventArgs e)
    {
        Session["filter"] = "Unanswered";
        Session["filterunread"] = "";

        Response.Redirect("tasks.aspx");
    }

    void bindPR0Tasks()
    {
        load_dropdowns();

        load_filterDropDown();

        chkViewDeleted.Checked = false;

        lblOrderBy.Text = " ORDER BY last_updated_date desc";

        drpFilter.SelectedValue = "Show my open tasks";

        drpPriorities.SelectedIndex = 1;
        setFileterSession();
        DataGrid1.CurrentPageIndex = 0;
        BindData();
    }

    void bindPR1aTasks()
    {
        load_dropdowns();

        load_filterDropDown();

        chkViewDeleted.Checked = false;

        lblOrderBy.Text = " ORDER BY last_updated_date desc";

        drpFilter.SelectedValue = "Show my open tasks";

        drpPriorities.SelectedIndex = 2;
        setFileterSession();
        DataGrid1.CurrentPageIndex = 0;
        BindData();
    }

    void bindPR1bTasks()
    {
        load_dropdowns();

        load_filterDropDown();

        chkViewDeleted.Checked = false;

        lblOrderBy.Text = " ORDER BY last_updated_date desc";

        drpFilter.SelectedValue = "Show my open tasks";

        drpPriorities.SelectedIndex = 3;
        setFileterSession();
        DataGrid1.CurrentPageIndex = 0;
        BindData();
    }

    void bindUnansweredQuestions()
    {
        load_dropdowns();

        load_filterDropDown();

        drpFilter.SelectedValue = "Show my open tasks";

        chkViewDeleted.Checked = false;

        sql = @"select distinct tasks.* from task_comments,tasks 
                where tasks.task_id = task_comments.task_id and tasks.deleted <> 1 and task_comments.deleted <> 1 and qflag=1 ";

        if (!DatabaseHelper.can_Show_Closed_Tasks(Session["admin"].ToString()))
        {
            sql += " and status <> 'closed' ";
        }

        sql += " and assigned_to_user = '" + Session["admin"] + "' ";

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            TopFilterLable.InnerHtml = "Filtered by Unanswered questions";

            DataGrid1.CurrentPageIndex = 0;
            DataGrid1.DataSource = ds.Tables[0];
            DataGrid1.DataBind();
        }
        else
        {
            lnkUnansweredQuestions.Text = "(0)";
            unansweredque.Visible = false;
        }
    }

    void bindDefault()
    {
        load_dropdowns();

        load_filterDropDown();

        lblOrderBy.Text = " ORDER BY last_updated_date desc";

        chkViewDeleted.Checked = false;

        if (DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            drpFilter.SelectedValue = (string)DatabaseHelper.executeScalar("select default_selected_item from users where username='" + Session["admin"] + "'");
        }
        else
        {
            drpFilter.SelectedValue = "Show my open tasks";
        }

        getFilterSession();

        BindData();
    }

    protected void chkViewDeleted_CheckedChanged(object sender, EventArgs e)
    {
        load_dropdowns();
        load_filterDropDown();
        lblOrderBy.Text = " ORDER BY last_updated_date desc";
        setFileterSession();
        BindData();
    }
    protected void chkHideClosed_CheckedChanged(object sender, EventArgs e)
    {
        setFileterSession();
        BindData();
    }

    protected int getHoursTakenSoFar(string task_id)
    {
        sql = @"select sum(datediff(minute,started_date,finished_date))
                from hours_reporting
                where finished_date is not null and task_id=" + task_id;

        int hrsTaken = 0;
        object objHrsTaken =  DatabaseHelper.executeScalar(sql);
        if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
        {
            hrsTaken += (int)objHrsTaken;
        }
        else
        {
            hrsTaken = 0;
        }
        
        sql = @"select sum(datediff(minute,started_date,getdate()))
                from hours_reporting
                where finished_date is null and task_id=" + task_id;

        objHrsTaken = DatabaseHelper.executeScalar(sql);

        if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
        {
            hrsTaken += (int) objHrsTaken;
        }        

        return hrsTaken;        
    }

    protected void getTotalHrsReport(string strSql)
    {
        sql= @"select sum(ETC)
                from tasks
                where task_id in (" + strSql + ")";

        int ETC = 0;
        
        object objETC = DatabaseHelper.executeScalar(sql);

        if (objETC != null && objETC.ToString() != "" && objETC.ToString() != "0")
        {
            ETC += (int)objETC;
        }
        else
        {
            ETC = 0;
        }
        
        sql = @"select sum(datediff(minute,started_date,finished_date))
                from hours_reporting
                where finished_date is not null and task_id in (" + strSql + ")";

        int hrsTaken = 0;
        object objHrsTaken = DatabaseHelper.executeScalar(sql);
        if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
        {
            hrsTaken += (int)objHrsTaken;
        }
        else
        {
            hrsTaken = 0;
        }

        sql = @"select sum(datediff(minute,started_date,getdate()))
                from hours_reporting
                where finished_date is null and task_id in (" + strSql + ")";

        objHrsTaken = DatabaseHelper.executeScalar(sql);

        if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
        {
            hrsTaken += (int)objHrsTaken;
        }


        lblETC.Text = "" + (ETC / 60) + " hrs " + (ETC % 60) + " mins";

        lblTotalHrsTaken.Text = "" + (hrsTaken / 60) + " hrs " + (hrsTaken % 60) + " mins";

        int hrsLeft = ETC - hrsTaken;

        if (hrsLeft < 0)
        {
            hrsLeft = -1 * hrsLeft;
            lblExpectedHrsLeft.Text = "- " + (hrsLeft / 60) + " hrs " + (hrsLeft % 60) + " mins";
        }
        else
        {
            lblExpectedHrsLeft.Text = "" + (hrsLeft / 60) + " hrs " + (hrsLeft % 60) + " mins";
        }

    }

    void getTskNewComment()
    {

        //////sql = "select tasks.* from tasks where task_id in(select task_comments.task_id from task_comments where tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 )";
        ////////////        sql = @"select tasks.* from tasks 
        ////////////                where tasks.deleted <> 1 and tasks.priority = '1b - high' ";

        //////if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        //////{
        //////    sql += "and deleted <> 1 and status <> 'closed' and assigned_to_user = '" + Session["admin"] + "' ";
        //////}
        //////else
        //////{
        //////    sql += "and deleted <> 1 and assigned_to_user = '" + Session["admin"] + "' ";
        //////}


        sql = "select tasks.* from tasks where task_id in(select task_comments.task_id from task_comments where tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 )";
        //////        sql = @"select tasks.* from tasks 
        //////                where tasks.deleted <> 1 and tasks.priority = '1b - high' ";

        ////if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        ////{
        sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            LnkTskNewcomment.Text = "(" + ds.Tables[0].Rows.Count + ")";
            LnkMakeTaskcommentRead.Text = "(" + "-" + ")";
            LnkMakeTaskcommentRead.Visible = true;
            TskNewComment.Visible = true;
        }
        else
        {
            LnkTskNewcomment.Text = "(0)";
            TskNewComment.Visible = false;
            LnkMakeTaskcommentRead.Visible = false;
        }
    }
    protected void LnkTskNewcomment_Click(object sender, EventArgs e)
    {

        Session["filterunread"] = "TaskNewComment";
        ////Session["filter"] = "Immediate";
        Response.Redirect("tasks.aspx");


    }
    protected void LnkMakeTaskcommentRead_Click(object sender, EventArgs e)
    {
        sql = @"select *  from task_comments where task_id in (select task_id  from tasks where tasks.task_id = task_comments.task_id and  tasks.assigned_to_user = '" + Session["admin"].ToString() + "'  and tasks.deleted <> 1 and tasks.status <> 'closed') and task_comments.tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 ";
        /// sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {

                    //////string sqlgetcmntId = "select tc_id from task_comments where task_id ='" + ds.Tables[0].Rows[i]["task_id"].ToString() + "'";
                    //////object TskcmntID = DatabaseHelper.executeScalar(sqlgetcmntId);


                    //////if (TskcmntID!=null)
                    //////{
                    string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                    sqlinsert += " values(" + ds.Tables[0].Rows[i]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                    int intResult = DatabaseHelper.executeNonQuery(sqlinsert);
                    string sqlinsert2 = "insert into read_task([task_id],[username]) ";
                    sqlinsert2 += " values(" + ds.Tables[0].Rows[i]["task_id"].ToString() + ",'" + Session["admin"].ToString() + "')";
                    int intResult2 = DatabaseHelper.executeNonQuery(sqlinsert2);
                    //////}
                }
                catch
                {
                }
            }


        }

        // getTskNewComment();
        Response.Redirect("tasks.aspx", false);
    }


    void getCRNewComment()
    {


        //// sql = @"select ClientRequest.* from ClientRequest where ClientRequest.[RequestId] in (select [RequestId] from ClientRequest_Details where [CommentId] not in (select  CommentId from read_CR_comments)) and ClientRequest.deleted <> 1";
        ////  sql = @"select ClientRequest_Details.* from ClientRequest_Details where [CommentId] not in (select  CommentId from read_CR_comments where [UserName]='Support') and ClientRequest_Details.deleted <> 1";
        sql = "select ClientRequest.* from ClientRequest where RequestId in(select ClientRequest_Details.RequestId from ClientRequest_Details where [CommentId] not in (select  CommentId from read_CR_comments where read_CR_comments.username='" + Session["admin"].ToString() + "') and ClientRequest_Details.deleted <> 1)";

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
            {
                /// LnkNewCR.Text = "(" + ds.Tables[0].Rows.Count + ")";
                CrNewComment.Visible = 