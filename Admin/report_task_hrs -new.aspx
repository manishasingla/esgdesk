<%@ Page Language="C#" AutoEventWireup="true" CodeFile="report_task_hrs.aspx.cs"
    Inherits="Admin_report_task_hrs" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc3" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc4" %>
<%@ Register Src="Notifications.ascx" TagName="Notifications" TagPrefix="uc5" %>
<%@ Register Assembly="FlashUpload" Namespace="FlashUpload" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        Task hours
    </title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        function disableSendButton()
        {            
            document.getElementById("btnSend1").disabled=true;
            document.getElementById("divMessage").innerHTML ="Please wait: your file is being sent...";
            document.getElementById("btnSend").click();
            return false;
        }
    </script>

</head>
<body style="background:#fff;">
    <form id="form1" runat="server">
        <div id="wrapper">
        <uc3:Admin_Header ID="Admin_Header1" runat="server" />
        <div>
                    <uc5:Notifications ID="Notifications1" runat="server" />
                </div>
            <div id="Content">
                <div id="pageTitle" style="height: 22px !important; padding:3px; line-height:22px !important; font-weight: bold; text-align: left">
                    Task Hours Report</div>
                <div id="divMessage" runat="server" style="color: Red; font-weight: bold;">
                </div>
                <div id="divDetails" runat="server" style="text-align:left">
                <%--=======================================================--%>
                <table border="0" width="100%">
                <tr>
                <td>
                 <%--=======================================================--%>
                
                
                   <div id="divTask" runat="server" class="divBorder">
                        <table width="100%" cellpadding="2" border="0">
                            <tr>
                                <td align="left" valign="top">
                                    Task id:
                                    <asp:Label ID="lblTaskId" runat="server" Font-Bold="True"></asp:Label></td>
                                <td align="left" valign="top">
                                    <asp:Label ID="lblLastUpdated" runat="server" ForeColor="Green"></asp:Label></td>
                                <td align="left" valign="top">
                                </td>
                                <td align="right" valign="top">
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="4" valign="top">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" >
                                        <tr>
                                            <td align="left">
                                                <asp:Label ID="lblReportedBy" runat="server"></asp:Label></td>
                                            <td align="right" style="padding-right: 10px">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="4" valign="top">
                                    <table cellpadding="5" cellspacing="0" border="0">
                                        <tr>
                                            <td>
                                                Project:&nbsp;
                                                <asp:Label ID="lblProject" runat="server" Font-Bold="True"></asp:Label></td>
                                            <td>
                                                /</td>
                                            <td>
                                                Assigned to:&nbsp;
                                                <asp:Label ID="lblAssignedTo" runat="server" Font-Bold="True"></asp:Label></td>
                                            <td>
                                                /</td>
                                            <td>
                                                Status:&nbsp;
                                                <asp:Label ID="lblStatus" runat="server" Font-Bold="True"></asp:Label></td>
                                            <td>
                                                /</td>
                                            <td>
                                                &nbsp;Priority:&nbsp;
                                                <asp:Label ID="lblPriority" runat="server" Font-Bold="True"></asp:Label></td>
                                            <td>
                                                /</td>
                                            <td>
                                                Category:&nbsp;
                                                <asp:Label ID="lblCategory" runat="server" Font-Bold="True"></asp:Label></td>
                                            <td>
                                                /</td>
                                            <td>
                                                Project type:&nbsp;
                                                <asp:Label ID="lblProjectType" runat="server" Font-Bold="True"></asp:Label></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="4" valign="top">
                                    <table border="0" cellpadding="5" cellspacing="0">
                                        <tr>
                                            <td align="left" valign="top">
                                                Short description:
                                            </td>
                                            <td align="left" valign="top">
                                                <asp:Label ID="lblShortDescription" runat="server"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top">
                                                Relevant URL:</td>
                                            <td align="left" valign="top">
                                                <asp:Label ID="lblRelevantURL" runat="server"></asp:Label></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="4" valign="top">
                                    <table border="0" cellpadding="5" cellspacing="0" >
                                        <tr>
                                            <td align="left" valign="top">
                                                Estimated time to completion:
                                                <asp:Label ID="lblETC" runat="server" Font-Bold="True"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top">
                                                Total hours so far:
                                                <asp:Label ID="lblTotalHrs" runat="server" Font-Bold="True"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top">
                                                Expected hrs left :
                                                <asp:Label ID="lblHrsLeft" runat="server" Font-Bold="True"></asp:Label></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                
                  <%--=======================================================--%>
                </td>
                 </tr>
                 <tr>
                 <td>
                 <div>
                  <%--=======================================================--%>
                  
                   <telerik:RadChart ID="RadChart4" runat="server" Height="600px" Width="850px" Style="float:left; margin-left:10px; margin-bottom:10px;" >
                    </telerik:RadChart>
                    <%--<br />--%>
                    <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" CellPadding="6" Width="38%"
                        AllowPaging="True" AllowSorting="True" OnItemDataBound="DataGrid1_ItemDataBound"
                        PageSize="100" Style="float:left; margin-left:10px;">
                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                            Font-Underline="False" HorizontalAlign="center" VerticalAlign="Top" />
                        <HeaderStyle BackColor="LightGray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                        <Columns>
                            <asp:BoundColumn DataField="task_id" HeaderText="task_id" Visible="False"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Started">
                                <ItemTemplate>
                                    <asp:Label ID="lblStarted" runat="server" Text='<%# bind("started_date") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Finished">
                                <ItemTemplate>
                                    <asp:Label ID="lblFinished" runat="server" Text='<%# bind("Finished_date") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Time taken">
                                <ItemTemplate>
                                    <asp:Label ID="lblTimeTaken" runat="server" Text='<%# bind("TimeTaken") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="username" HeaderText="Assigned to"></asp:BoundColumn>
                        </Columns>
                        <PagerStyle NextPageText="Next" PrevPageText="Prev" />
                    </asp:DataGrid>
                         
                   <%--=======================================================--%>
                   </div>
                 </td>
                 </tr>
                 </table>
                <%--=======================================================--%>
                   
                </div>
            </div>
            <asp:Label ID="lblOrderBy" runat="server" Visible="false" Text=""></asp:Label>
        </div>
        <uc4:footer ID="Footer2" runat="server" />
    </form>
</body>
</html>
