using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
using Telerik.Charting;
using System.Configuration;

public partial class Admin_report_task_hrs : System.Web.UI.Page
{
    int id;
    string sql = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = (Request.QueryString["id"] == null || Request.QueryString["id"].ToString() == "" ? "tasks.aspx" : "report_task_hrs.aspx?id=" + Request.QueryString["id"].ToString());
            Response.Redirect("login.aspx");
            return;
        }

        string task_id = Request["id"];
        if (task_id == null || task_id == "0" || task_id == "")
        {
            id = 0;
        }
        else
        {
            if (DatabaseHelper.is_int(task_id))
            {
                id = Convert.ToInt32(task_id);
            }
            else
            {
                // Display an error because the bugid must be an integer
                divMessage.InnerHtml = "<br>Task ID must be an integer.";
                divMessage.InnerHtml += "<p><a href='tasks.aspx'>View tasks</a></p>";
                return;
            }
        }
        Bindchart(id);
        divMessage.InnerHtml = "";
        if (!Page.IsPostBack)
        {
            sql = "select * from tasks where task_id = " + id.ToString();

            DataSet dsTask = DatabaseHelper.getDataset(sql);

            if (dsTask == null || dsTask.Tables.Count <= 0 || dsTask.Tables[0].Rows.Count <= 0)
            {
                divMessage.InnerHtml = "<br>Task not found.<br><br><a href='tasks.aspx'>View tasks</a>";
                divDetails.Visible = false;
            }
            else
            {
                
                divDetails.Visible = true;
                BindData(id);
            }

            if (!DatabaseHelper.can_Delete_Comments_Attachments(Session["admin"].ToString()))
            {
                DataGrid1.Columns[8].Visible = false;
            }
        }
    }
    public void Bindchart(int id)
    {
        //sql = "select Cast(DAY(Convert(varchar(20),Started_date,107))as int) as Date ,Round(Convert(Float,(Sum(Minutes))/60.0),1) as TIME ";
        //sql += " from hours_reporting where task_Id=" + id + " and Month(Started_date)=Month(Getdate()) and ";
        //sql +=" Year(Started_date)=Year(Getdate())  group by (Convert(varchar(20),Started_date,107)) ";

        //sql = "select SUBSTRING(Convert(varchar(20),Started_date,107),0,Charindex(',',Convert(varchar(20),Started_date,107))) as Date ,Round(Convert(Float,(Sum(Minutes))/60.0),1) as TIME ";
        //sql += " from hours_reporting where task_Id=" + 3164 + " and Month(Started_date)=Month(Getdate()) and ";
        //sql += " Year(Started_date)=Year(Getdate())  group by (Convert(varchar(20),Started_date,107)) ";

        sql = "select SUBSTRING(Convert(varchar(20),Started_date,107),0,Charindex(',',Convert(varchar(20),Started_date,107))) as Date ,Round(Convert(Float,(Sum(Minutes))/60.0),1) as TIME ";
        sql += " from hours_reporting where task_Id=" + id + " ";
        sql += "  group by (Convert(varchar(20),Started_date,107)) ";

        DataSet dsTask = DatabaseHelper.getDataset(sql);

        ChartSeries secondSerie3 = new ChartSeries();
        secondSerie3.Name = "TIME IN HOUR";
        secondSerie3.Type = ChartSeriesType.Bar;
        secondSerie3.DataYColumn = "TIME";
        RadChart4.Series.Add(secondSerie3);
        RadChart4.PlotArea.XAxis.DataLabelsColumn = "DATE";

        //===============================
        RadChart4.PlotArea.XAxis.Appearance.TextAppearance.TextProperties.Font = new System.Drawing.Font("Verdana", 8);
        RadChart4.PlotArea.XAxis.AxisLabel.TextBlock.Text = "<----------Date---------->";
        RadChart4.PlotArea.XAxis.AxisLabel.Visible = true;
        RadChart4.PlotArea.XAxis.AxisLabel.TextBlock.Appearance.TextProperties.Color = System.Drawing.Color.Red;
        RadChart4.PlotArea.XAxis.Appearance.Width = 4;
        RadChart4.PlotArea.XAxis.Appearance.Color = System.Drawing.Color.Red;

        RadChart4.PlotArea.YAxis.Appearance.TextAppearance.TextProperties.Font = new System.Drawing.Font("Verdana", 8);
        RadChart4.PlotArea.YAxis.AxisLabel.TextBlock.Text = "<----------Hours (Spend in task)---------->";
        RadChart4.PlotArea.YAxis.AxisLabel.Visible = true;
        RadChart4.PlotArea.YAxis.AxisLabel.TextBlock.Appearance.TextProperties.Color = System.Drawing.Color.Red;
        RadChart4.PlotArea.YAxis.Appearance.Width = 4;
        RadChart4.PlotArea.YAxis.Appearance.Color = System.Drawing.Color.Red;

        //===============================
        RadChart4.DataSource = dsTask;
        RadChart4.DataBind();
        RadChart4.ChartTitle.TextBlock.Text = "Hour spend in a task(Bar Chart)";
    }
    public void BindData(int task_id)
    {

        sql = "select * from tasks where task_id=" + task_id;

        DataSet dsTask = DatabaseHelper.getDataset(sql);

        if (dsTask == null || dsTask.Tables.Count <= 0 || dsTask.Tables[0].Rows.Count <= 0)
        {

        }
        else
        {
            lblLastUpdated.Text = " Last changed by <b>" + dsTask.Tables[0].Rows[0]["last_updated_user"].ToString() + "</b> on <b>" + DateTime.Parse(dsTask.Tables[0].Rows[0]["last_updated_date"].ToString()).ToString("dd MMM h:mm tt") + "</b>";
            lblReportedBy.Text = "Reported by <b>" + dsTask.Tables[0].Rows[0]["reported_user"].ToString() + "</b> on <b>" + DateTime.Parse(dsTask.Tables[0].Rows[0]["reported_date"].ToString()).ToString("dd MMM h:mm tt") + "</b>";
            lblTaskId.Text = dsTask.Tables[0].Rows[0]["task_id"].ToString();
            lblProject.Text = dsTask.Tables[0].Rows[0]["project"].ToString();
            lblPriority.Text = dsTask.Tables[0].Rows[0]["priority"].ToString();
            lblStatus.Text = dsTask.Tables[0].Rows[0]["status"].ToString();
            lblAssignedTo.Text = dsTask.Tables[0].Rows[0]["assigned_to_user"].ToString();
            lblProjectType.Text = dsTask.Tables[0].Rows[0]["project_type"].ToString();
            lblCategory.Text = dsTask.Tables[0].Rows[0]["category"].ToString();
            lblShortDescription.Text = dsTask.Tables[0].Rows[0]["short_desc"].ToString();
            lblRelevantURL.Text = dsTask.Tables[0].Rows[0]["Relevant_URL"].ToString();
            dsTask.Tables[0].Rows[0]["ETC"].ToString();

            int ETC = Convert.ToInt32(dsTask.Tables[0].Rows[0]["ETC"].ToString());

            lblETC.Text = "" + (ETC / 60) + " hrs " + (ETC % 60) + " mins";

            int hrsTaken = getHoursTakenSoFar(task_id);

            lblTotalHrs.Text = "" + (hrsTaken / 60) + " hrs " + (hrsTaken % 60) + " mins";

            int hrsLeft = ETC - hrsTaken;

            if (hrsLeft < 0)
            {
                hrsLeft = -1 * hrsLeft;
                lblHrsLeft.Text = "- " + (hrsLeft / 60) + " hrs " + (hrsLeft % 60) + " mins";
            }
            else
            {
                lblHrsLeft.Text = "" + (hrsLeft / 60) + " hrs " + (hrsLeft % 60) + " mins";
            }
                
        }

        DataSet dsHrsReport = getHoursReport(task_id);

        if (dsHrsReport == null || dsHrsReport.Tables.Count <= 0 || dsHrsReport.Tables[0].Rows.Count <= 0)
        {
            
        }
        else
        {
            DataGrid1.DataSource = dsHrsReport.Tables[0];
            DataGrid1.DataBind();

            if (dsHrsReport.Tables[0].Rows.Count <= 100)
            {
                DataGrid1.PagerStyle.Visible = false;
            }
            else
            {
                DataGrid1.PagerStyle.Visible = false;
            }
        }
    }

    protected void DataGrid1_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemIndex != -1)
        {
            ((Label)e.Item.FindControl("lblStarted")).Text = DateTime.Parse(((Label)e.Item.FindControl("lblStarted")).Text).ToString("dd MMM h:mm tt").ToString();

            if (((Label)e.Item.FindControl("lblFinished")).Text != "")
            {
                ((Label)e.Item.FindControl("lblFinished")).Text = DateTime.Parse(((Label)e.Item.FindControl("lblFinished")).Text).ToString("dd MMM h:mm tt").ToString();
            }
            else
            {
                ((Label)e.Item.FindControl("lblFinished")).Text = "Currently working on";
            }

            int ETC = Convert.ToInt32(((Label)e.Item.FindControl("lblTimeTaken")).Text);

            ((Label)e.Item.FindControl("lblTimeTaken")).Text = "" + (ETC / 60) + " hrs " + (ETC % 60) + " mins";
        }
    }

    protected int getHoursTakenSoFar(int task_id)
    {
        sql = @"select sum(datediff(minute,started_date,finished_date))
                from hours_reporting
                where finished_date is not null and task_id=" + task_id.ToString();

        int hrsTaken = 0;
        object objHrsTaken = DatabaseHelper.executeScalar(sql);
        if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
        {
            hrsTaken += (int)objHrsTaken;
        }
        else
        {
            hrsTaken = 0;
        }

        sql = @"select sum(datediff(minute,started_date,getdate()))
                from hours_reporting
                where finished_date is null and task_id=" + task_id.ToString();

        objHrsTaken = DatabaseHelper.executeScalar(sql);

        if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
        {
            hrsTaken += (int)objHrsTaken;
        }

        return hrsTaken;
    }

    protected DataSet getHoursReport(int task_id)
    {
        sql = @"select task_id, started_date, finished_date,username, (datediff(minute,started_date,finished_date)) TimeTaken
                from hours_reporting
                where finished_date is not null and task_id=" + task_id.ToString() +" order by started_date asc" ;

        
        DataSet dsHrsTaken = DatabaseHelper.getDataset(sql);
        if (dsHrsTaken == null || dsHrsTaken.Tables.Count <= 0 || dsHrsTaken.Tables[0].Rows.Count <= 0)
        {
            
        }
        else
        {
            
        }

        sql = @"select task_id, started_date, finished_date,username, (datediff(minute,started_date,getdate())) TimeTaken
                from hours_reporting
                where finished_date is null and task_id=" + task_id.ToString() + " order by started_date asc";

        DataSet objHrsTaken1 = DatabaseHelper.getDataset(sql);

        if (objHrsTaken1 == null || objHrsTaken1.Tables.Count <= 0 || objHrsTaken1.Tables[0].Rows.Count <= 0)
        {

        }
        else
        {
            dsHrsTaken.Merge(objHrsTaken1);
        }

        DataView mDataView = new DataView();
        mDataView = dsHrsTaken.Tables[0].DefaultView;
        mDataView.Sort = "started_date asc";

        dsHrsTaken.Tables.Clear();
        dsHrsTaken = null;
        DataSet dsHrsTaken2 = new DataSet();
        dsHrsTaken2.Tables.Add(mDataView.ToTable());

        return dsHrsTaken2;
    }


}
