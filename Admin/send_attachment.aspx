<%@ Page Language="C#" AutoEventWireup="true" CodeFile="send_attachment.aspx.cs"
    Inherits="send_attachment" %>

<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc3" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc4" %>
<%@ Register Assembly="FlashUpload" Namespace="FlashUpload" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
       Send attachment</title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        function disableSendButton() {
            document.getElementById("btnSend1").disabled = true;
            document.getElementById("divMessage").innerHTML = "Please wait: your file is being sent...";
            document.getElementById("btnSend").click();
            return false;
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <uc3:Admin_Header ID="Admin_Header1" runat="server" />
        <div id="Content">
            <div id="pageTitle" style="height: 30px; font-weight: bold; text-align">

                <%@ page language="C#" autoeventwireup="true" codefile="send_attachment.aspx.cs"
                    inherits="send_attachment" %>

                <%@ register src="Admin_Header.ascx" tagname="Admin_Header" tagprefix="uc3" %>
                <%@ register src="../footer.ascx" tagname="footer" tagprefix="uc4" %>
                <%@ register assembly="FlashUpload" namespace="FlashUpload" tagprefix="cc2" %>
                <%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
                <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                <html xmlns="http://www.w3.org/1999/xhtml">
                <head id="Head1" runat="server">
                    <title>
                        <%= ConfigurationManager.AppSettings["CompanyName"].ToString()%></title>
                    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />

                    <script language="javascript" type="text/javascript">
                        function disableSendButton() {
                            document.getElementById("btnSend1").disabled = true;
                            document.getElementById("divMessage").innerHTML = "Please wait: your file is being sent...";
                            document.getElementById("btnSend").click();
                            return false;
                        }
                    </script>

                </head>
                <body>
                    <form id="form1" runat="server">
                    <div id="wrapper">
                        <uc3:Admin_Header ID="Admin_Header1" runat="server" />
                        <div id="Content">
                            <div id="pageTitle" style="height: 30px; font-weight: bold; text-align: left">
                                Send attachment</div>
                            <div id="divAttachment" runat="server" style="text-align: left">
                                <table cellpadding="5">
                                    <tr>
                                        <td align="left" colspan="1">
                                            <div id="divBackLink" runat="server">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <div id="divMessage" runat="server" style="font-weight: bold; color: red; text-align: left">
                                            </div>
                                            <div id="divFlash" runat="server">
                                                <table cellpadding="3">
                                                    <tr>
                                                        <td colspan="3">
                                                            <span style="color: Red; font-weight: bold;">(Please include text in the request itself.
                                                                Only send attachments for such things as images and ensure they are no bigger than
                                                                2MB in total)</span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    </form>
                </body>
                </html>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
