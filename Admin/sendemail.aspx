﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="sendemail.aspx.cs" Inherits="Admin_sendemail" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        Send mail
    </title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
    <script>
        function ValidateModuleList(source, args) {

            //var chkListModules = document.getElementById("chklstempname").ClientID;
            var chkListModules = document.getElementById("chklstempname");

            //  alert(chkListModules);
            var chkListinputs = chkListModules.getElementsByTagName("input");
            for (var i = 0; i < chkListinputs.length; i++) {
                if (chkListinputs[i].checked) {
                    args.IsValid = true;
                    return;
                }
            }
            args.IsValid = false;
        }
    </script>
    <script>
        function setAllChecked(obj) {
            //alert("Enter");
            chkbxs = document.getElementById("chklstempname").getElementsByTagName("input");
            for (var i = 0; i < chkbxs.length; i++) {
                chkbxs[i].checked = obj.checked;


            }

        }
       
    </script>
    <script type="text/javascript" language="javascript">

function confirm()
{

///if (e.keyCode==110)
if (confirm ==true)


var btndel = document.getElementById("BtnDelMsg");
btndel.click();  



else


return false;

}
    </script>
    <%--<script language="javascript" type="text/javascript">
        function checkCat() {
            var cat = document.getElementById("txtCategory").value;
            if (cat == "") {
                alert("Please enter category name");
                return false;
            }
            else {
                var btnsend = document.getElementById("btnAddCat");
                btnsend.click();
            }

        }
    
    </script>
    <script language="javascript" type="text/javascript">
        function chkcatEdit() {
            var cat = document.getElementById("txteditCat").value;
            if (cat == "") {
                alert("Please enter category name");
                return false;
            }
            else {
                var btnsend = document.getElementById("btnUpdateCat");
                btnsend.click();
            }
        }
    
    </script>--%>
</head>
<body onload="checkChecked();">
    <form id="form1" runat="server">
    <div id="wrapper">
        <div id="Content">
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
            <div class="titleText" style="margin: 10px 0; display: none;">
                Canned message</div>
            <div id="divMessage" runat="server" style="color: Red; font-weight: bold">
            </div>
            <div id="DivEntry" runat="server">
                <div id="divTask" runat="server" class="divBorder">
                    <table>
                        <tr>
                            <td>
                                <div>
                                    <input type="checkbox" name="checkbox2" id="chkSelectAll" runat="server" onclick="setAllChecked(this);" />
                                    Select All
                                </div>
                                <br />
                                <div style="border: 1px solid grey; overflow: scroll; height: 766px;">
                                    <asp:CheckBoxList ID="chklstempname" runat="server" ValidationGroup="error">
                                    </asp:CheckBoxList>
                                    <asp:CustomValidator runat="server" ID="cvmodulelist" ClientValidationFunction="ValidateModuleList"
                                        ErrorMessage="Please select atleast one email id" ValidationGroup="error" Display="None"></asp:CustomValidator>
                                </div>
                            </td>
                            <td>
                                <asp:UpdateProgress ID="updateprogress1" runat="server">
                                    <ProgressTemplate>
                                        <div id="processmessage" class="processMessage">
                                            <img alt="loading" src="images/ajax-loader.gif" style="width: 80px; height: 80px" />
                                            <span style="color: black; font-weight: bold; font-size: 18px; vertical-align: super;">
                                                Please wait... </span>
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <table width="760" cellpadding="2" border="0">
                                            <tr>
                                                <td align="left" colspan="4" valign="top">
                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td style="height: 31px">
                                                                Message category:
                                                            </td>
                                                            <td style="height: 31px">
                                                                <asp:DropDownList ID="DrpMsgCat" runat="server" Width="235px" OnSelectedIndexChanged="DrpMsgCat_SelectedIndexChanged"
                                                                    AutoPostBack="True">
                                                                </asp:DropDownList>
                                                                <asp:Button ID="Button1" runat="server" BorderColor="Transparent" BorderStyle="None"
                                                                    Text="Add new canned message" OnClick="Button1_Click" CausesValidation="False"
                                                                    Width="166px" Height="21px" CssClass="blueBtns" />
                                                                <asp:Button ID="btnEditCat" runat="server" BackColor="Transparent" BorderColor="Transparent"
                                                                    BorderStyle="None" Font-Underline="True" ForeColor="Blue" Text="Edit" OnClick="btnEditCat_Click"
                                                                    Width="38px" CausesValidation="False" />
                                                                <asp:Button ID="BtnDelcat" runat="server" BackColor="Transparent" BorderColor="Transparent"
                                                                    BorderStyle="None" Font-Underline="True" ForeColor="Blue" Text="Delete" OnClick="BtnDelcat_Click"
                                                                    Width="49px" CausesValidation="False" />
                                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="DrpMsgCat"
                                                ErrorMessage="please select message category">*</asp:RequiredFieldValidator>--%>
                                                            </td>
                                                            <td style="height: 31px">
                                                            </td>
                                                            <td style="height: 31px">
                                                            </td>
                                                            <td style="height: 31px">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr id="TRNewCat" runat="server" style="margin-top: 10px">
                                                            <td>
                                                            </td>
                                                            <td style="border: solid 1px #999999;">
                                                                Category name:
                                                                <asp:TextBox ID="txtCategory" runat="server" Width="389px" ValidationGroup="error1"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtCategory"
                                                                    ErrorMessage="Please enter Category" Display="None" ValidationGroup="error1"></asp:RequiredFieldValidator>
                                                                <asp:ValidationSummary ID="vs1" runat="server" ValidationGroup="error1" ShowMessageBox="true"
                                                                    ShowSummary="False" />
                                                                &nbsp;<asp:Button ID="btnAddCat" runat="server" Text="Add" OnClick="btnAddCat_Click"
                                                                    CssClass="blueBtns" ValidationGroup="error1" />&nbsp;<asp:Button ID="btncloseCat"
                                                                        runat="server" Text="Close" CausesValidation="False" OnClick="btncloseCat_Click"
                                                                        CssClass="blueBtns" /><br />
                                                                <span style="padding-right: 100px">
                                                                    <asp:Label ID="lblcatmsg" runat="server" ForeColor="Red" Text=""></asp:Label></span>
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr id="TREditCat" runat="server" style="margin-top: 10px">
                                                            <td style="height: 45px">
                                                            </td>
                                                            <td style="border: solid 1px #999999; height: 45px;">
                                                                Category name:
                                                                <asp:TextBox ID="txteditCat" runat="server" Width="366px" ValidationGroup="error2"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txteditCat"
                                                                    ErrorMessage="Please enter Category" Display="None" ValidationGroup="error2"></asp:RequiredFieldValidator>
                                                                <asp:ValidationSummary ID="Vs2" runat="server" ValidationGroup="error2" ShowMessageBox="true"
                                                                    ShowSummary="False" />
                                                                &nbsp;<asp:Button ID="btnUpdateCat" runat="server" Text="Update" OnClick="btnUpdateCat_Click"
                                                                    CssClass="blueBtns" ValidationGroup="error2" />&nbsp;<asp:Button ID="btnCnclUpdate"
                                                                        runat="server" Text="Close" CausesValidation="False" OnClick="btnCnclUpdate_Click"
                                                                        CssClass="blueBtns" /><br />
                                                                <asp:Label ID="lbleditmessase" runat="server" ForeColor="Red" Text=""></asp:Label>
                                                            </td>
                                                            <td style="height: 45px">
                                                            </td>
                                                            <td style="height: 45px">
                                                            </td>
                                                            <td style="height: 45px">
                                                            </td>
                                                        </tr>
                                                        <tr id="Tr1" runat="server" style="padding-top: 13px">
                                                            <td style="height: 25px">
                                                                Select Subject:
                                                            </td>
                                                            <td style="height: 25px">
                                                                <asp:DropDownList ID="DrpShrtDesc" runat="server" Width="577px" OnSelectedIndexChanged="DrpShrtDesc_SelectedIndexChanged"
                                                                    AutoPostBack="True">
                                                                    <asp:ListItem>[New message]</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td style="height: 25px">
                                                            </td>
                                                            <td style="height: 25px">
                                                            </td>
                                                            <td style="height: 25px">
                                                            </td>
                                                        </tr>
                                                        <tr id="TRAddShrtDesc" runat="server" style="padding-top: 13px">
                                                            <td style="height: 25px">
                                                                Subject:
                                                            </td>
                                                            <td style="height: 25px">
                                                                <asp:TextBox ID="txtshrtDesc" runat="server" Width="574px"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtshrtDesc"
                                                                    ErrorMessage="Please enter subject" Display="None" ValidationGroup="error"></asp:RequiredFieldValidator>
                                                            </td>
                                                            <td style="height: 25px">
                                                            </td>
                                                            <td style="height: 25px">
                                                            </td>
                                                            <td style="height: 25px">
                                                            </td>
                                                        </tr>
                                                        <tr id="TR2" runat="server" style="padding-top: 13px">
                                                            <td style="height: 25px">
                                                                Attach file:
                                                            </td>
                                                            <td style="height: 25px">
                                                                <asp:FileUpload ID="AttachFile" runat="server" />
                                                            </td>
                                                            <td style="height: 25px">
                                                            </td>
                                                            <td style="height: 25px">
                                                            </td>
                                                            <td style="height: 25px">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="4" valign="top">
                                                    <%-- <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator2">
                                            </cc1:ValidatorCalloutExtender>--%>
                                                    <%--  <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server" TargetControlID="RequiredFieldValidator3">
                                </cc1:ValidatorCalloutExtender>--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top">
                                                    Body:
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ftbComment"
                                                        ErrorMessage="please enter long description">*</asp:RequiredFieldValidator>
                                                </td>
                                                <td align="left" valign="top">
                                                    &nbsp;
                                                </td>
                                                <td align="left" valign="top">
                                                </td>
                                                <td align="right" valign="top">
                                                    <%--   <asp:CheckBox ID="chkdisable" runat="server" AutoPostBack="true" CausesValidation="false"
                                                        Text="Edit" OnCheckedChanged="chkdisable_CheckedChanged" Visible="false" />--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="4" valign="bottom">
                                                    <%--********************************************************************************************************--%>
                                                    <FTB:FreeTextBox ID="ftbComment" runat="server" Height="600px" Width="100%">
                                                    </FTB:FreeTextBox>
                                                    <%--<telerik:RadEditor ID="ftbComment" runat="server" Height="600px" Width="900px">
                                                        <Snippets>
                                                            <telerik:EditorSnippet Name="Product Template" Value="<table><tr><td><div style='width:300px;border:2px outset #D8D2BD;padding:34px;font-family:Arial, Verdana; font-size:11px;background- color:#F1EFE6;'>Dear ____________________,<br>Thank you for inquiring about ____________. Your request will be processed in 48 hours and shipped at the address you have provided.<b>Please, contact us if you have any problems.</b></div></td></tr></table>">            
                                                            </telerik:EditorSnippet>
                                                            <telerik:EditorSnippet Name="Signature" Value="<strong>John Smith</strong><br>Sales Manager<br>">
                                                            </telerik:EditorSnippet>
                                                        </Snippets>
                                                        <Links>
                                                            <telerik:EditorLink Name="Telerik" Href="http://www.telerik.com">
                                                                <telerik:EditorLink Name="Telerik Products" Href="http://www.telerik.com/products"
                                                                    Target="_blank" />
                                                                <telerik:EditorLink Name="Telerik Support Center" Href="http://www.telerik.com/support/home.aspx"
                                                                    Target="_blank" />
                                                                <telerik:EditorLink Name="Telerik Community" Href="http://www.telerik.com/community/home.aspx"
                                                                    Target="_blank" ToolTip="Telerik Community" />
                                                            </telerik:EditorLink>
                                                            
                                                        </Links>
                                                        <Tools>
                                                        </Tools>
                                                        <ImageManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                                        <MediaManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                                        <FlashManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                                        <TemplateManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                                        <DocumentManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                                        <Modules>
                                                            <telerik:EditorModule Name="RadEditorStatistics" Visible="False" Enabled="true" />
                                                            <telerik:EditorModule Name="RadEditorDomInspector" Visible="False" Enabled="true" />
                                                            <telerik:EditorModule Name="RadEditorNodeInspector" Visible="False" Enabled="true" />
                                                            <telerik:EditorModule Name="RadEditorHtmlInspector" Visible="False" Enabled="true" />
                                                        </Modules>
                                                        <Content>
                                                
                                                        </Content>
                                                    </telerik:RadEditor>--%>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ftbComment"
                                                        ErrorMessage="Please enter body" Display="None" ValidationGroup="error"></asp:RequiredFieldValidator>
                                                    <%--*****************************************************************************************************************--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <table width="98%">
                                    <tr>
                                        <td align="left" valign="top" style="height: 28px">
                                            <div id="divMessage1" runat="server" style="color: Red">
                                            </div>
                                        </td>
                                        <td align="right" valign="top" style="height: 28px">
                                            <%--<asp:Button ID="BtnAddMsg" runat="server" Text="Create" OnClick="BtnAddMsg_Click"
                                    CssClass="blueBtns" />--%>
                                            <%--====================== Change CSS Class=================================================================--%>
                                            <%--<asp:Button ID="BtnEditMsg" runat="server" Text="Insert" OnClick="BtnEditMsg_Click"
                                    Width="53px" CssClass="blueBtns" />--%>
                                            <asp:Button ID="btnsend" runat="server" Text="Send email" OnClick="btnsend_Click"
                                                ValidationGroup="error" />
                                            <asp:Button ID="BtnAddMsg" runat="server" Text="Create canned message" OnClick="BtnAddMsg_Click" />
                                            <asp:Button ID="BtnEditMsg" runat="server" Text="Update" OnClick="BtnEditMsg_Click"
                                                Width="53px" />
                                            <%--<asp:Button ID="BtnDelMsg" runat="server" Text="Delete" OnClick="BtnDelMsg_Click" CssClass="blueBtns" />--%>
                                            <asp:Button ID="BtnDelMsg" runat="server" Text="Delete" OnClick="BtnDelMsg_Click" />
                                            <%--=======================================================================================--%>
                                            <%--===================================================================================================================--%>
                                            <%--<asp:Button id="BtnEdit" runat="server" Text="Edit" CausesValidation="false"  CssClass="blueBtns" OnClick="BtnEdit_Click"/>--%>
                                            <%--===================================================================================================================--%>
                                            <asp:ValidationSummary ID="VS" runat="server" ValidationGroup="error" ShowMessageBox="true"
                                                ShowSummary="False" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" colspan="2">
                                            <%--<asp:Label ID="lblerror" runat="server" Style="color: Red; width: 750px;word-wrap: break-word;"></asp:Label>--%>
                                            <div id="lblerror" runat="server" style="color: Red; width: 750px; word-wrap: break-word;">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
