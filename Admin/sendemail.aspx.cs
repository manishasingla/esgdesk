﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;


public partial class Admin_sendemail : System.Web.UI.Page
{
    int id;
    string sql = "";
    string task_action = "";
    string emailSubject = "";
    string shortDescription = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        //btnAddCat.Attributes.Add("Onclick", "return checkCat();");
        //btnUpdateCat.Attributes.Add("Onclick", "return chkcatEdit();");


        BtnDelMsg.Attributes.Add("onclick", "return confirm('Are you sure want to delete message " + DrpShrtDesc.SelectedItem + " ? ');");

        ////// if (Session["admin"] == null || Session["admin"].ToString() == "")
        ////// {
        //////     //Session["returnUrl"] = "quick_question.aspx";
        ////////     Response.Redirect("login.aspx");
        //////     return;
        ////// }

        divMessage1.InnerHtml = "";
        divMessage.InnerHtml = "";
        if (!Page.IsPostBack)
        {
            fillemail();

            load_dropdowns();
            TRNewCat.Visible = false;

            BtnEditMsg.Visible = false;
            BtnDelMsg.Visible = false;
            ////BtnAddMsg.Visible = false;

            TREditCat.Visible = false;
            BtnDelcat.Visible = false;
            btnEditCat.Visible = false;

            //==========================================
            ////ftbComment.ReadOnly = true;
            //========Apply CSS with Hover==============
            //BtnEditMsg.CssClass = "BtnsNotHover";
            //BtnDelMsg.CssClass = "BtnsNotHover";
            ////BtnEditMsg.CssClass = BtnEditMsg.CssClass.Replace("blueBtns", "");
            ////BtnEditMsg.CssClass = "BtnsNotHover";
            BtnEditMsg.CssClass = "blueBtns";
            BtnDelMsg.CssClass = "blueBtns";
            ////BtnDelMsg.CssClass = BtnDelMsg.CssClass.Replace("blueBtns", "");
            ////BtnDelMsg.CssClass = "BtnsNotHover";


            ////BtnAddMsg.CssClass = BtnAddMsg.CssClass.Replace("blueBtns", "");
            ////BtnAddMsg.CssClass = "BtnsNotHover"; 
            BtnAddMsg.CssClass = "blueBtns";
            //==========================================
            //==========================================


            //// load_ShortDescdrp();

        }

    }

    void fillemail()
    {
        //query changed by chaitali - select active users only - 28/10/2015
        string sqlquery = "select distinct email from users where IsCW='y' and active = 'Y'";


        // do a batch of sql statements
        DataSet ds_dropdowns = DatabaseHelper.getDataset(sqlquery);

        chklstempname.DataSource = ds_dropdowns.Tables[0];
        chklstempname.DataTextField = "email";
        chklstempname.DataValueField = "email";

        chklstempname.DataBind();


    }

    void load_ShortDescdrp()
    {
        // projects
        DrpShrtDesc.Items.Clear();
        sql = @"select *
		from Canned_message where CategoryId = '" + DrpMsgCat.SelectedValue + "' and Type = 'EmployeesEmail' order by shortDesc";
        //from Canned_message where CategoryId = '" + DrpMsgCat.SelectedValue + "' and Type = 'Task' order by shortDesc";

        // do a batch of sql statements
        DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);
        if (ds_dropdowns.Tables[0].Rows.Count > 0)
        {

            DrpShrtDesc.DataSource = ds_dropdowns.Tables[0];
            DrpShrtDesc.DataTextField = "shortDesc";
            DrpShrtDesc.DataValueField = "MessageId";
            DrpShrtDesc.DataBind();
            DrpShrtDesc.Items.Insert(0, new ListItem("[New message]", ""));

        }
        else
        {
            DrpShrtDesc.Items.Insert(0, new ListItem("[New message]", ""));

        }
        /// DrpShrtDesc.Items.Insert(0, new ListItem("[New message]", ""));
    }

    void load_dropdowns()
    {
        // projects
        sql = @"select *
		from Message_category where Type ='EmployeesEmail'
		 order by CategoryName;";

        //        sql = @"select *
        //		from Message_category where Type ='Task'
        //		 order by CategoryName;";

        // do a batch of sql statements
        DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);

        DrpMsgCat.DataSource = ds_dropdowns.Tables[0];
        DrpMsgCat.DataTextField = "CategoryName";
        DrpMsgCat.DataValueField = "CatId";
        DrpMsgCat.DataBind();
        DrpMsgCat.Items.Insert(0, new ListItem("[No category]", ""));
    }

    protected void BtnAddMsg_Click(object sender, EventArgs e)
    {
        try
        {

            sql += " insert into Canned_message([CategoryId],[shortDesc],[LongDesc],[Type]) ";
            //*********************************************************************************
            //sql += " values('" + DrpMsgCat.SelectedValue.Replace("'", "''") + "','" + txtshrtDesc.Text.Replace("'", "''") + "','" + ftbComment.Text.Replace("'", "''") + "','Task')";
            sql += " values('" + DrpMsgCat.SelectedValue.Replace("'", "''") + "','" + txtshrtDesc.Text.Replace("'", "''") + "','" + ftbComment.Text.Replace("'", "''") + "','EmployeesEmail')";
            //*********************************************************************************
            int intResult = DatabaseHelper.executeNonQuery(sql);

            if (intResult != 0)
            {
                load_ShortDescdrp();
                ///// DrpMsgCat.Text = txtshrtDesc.Text;
                //***********************************************************
                //Session["Task_Canned_msg"] = ftbComment.Text;
                //// Session["Task_Canned_msg"] = ftbComment.Content;
                //***********************************************************
                //// Page.ClientScript.RegisterStartupScript(typeof(Page), "test", "<script> updateParentWindow()</script>");
                divMessage1.InnerHtml = "Canned message created successfully.";
            }
            else
            {
                divMessage1.InnerHtml = "Proccess failed,please try again.";
            }
        }
        catch
        { }

    }
    protected void btnAddCat_Click(object sender, EventArgs e)
    {
        try
        {

            sql += " insert into Message_category([CategoryName],[Type]) ";
            // sql += " values('" + txtCategory.Text.Trim().Replace("'", "''") + "','Task')";
            sql += " values('" + txtCategory.Text.Trim().Replace("'", "''") + "','EmployeesEmail')";
            int intResult = DatabaseHelper.executeNonQuery(sql);

            if (intResult != 0)
            {
                load_dropdowns();
                txtCategory.Text = "";
                TRNewCat.Visible = false;
                Button1.Visible = true;

            }
            else
            {

                TRNewCat.Visible = true;
                lblcatmsg.Text = "Proccess failed , please try again.";




            }
        }
        catch
        { }
    }
    protected void BtnDelcat_Click(object sender, EventArgs e)
    {
        sql += " Delete from Message_category where  CatId = '" + DrpMsgCat.SelectedValue + "' and Type = 'EmployeesEmail'";
        //      sql += " Delete from Message_category where  CatId = '" + DrpMsgCat.SelectedValue + "' and Type = 'Task'";
        int intResult = DatabaseHelper.executeNonQuery(sql);
        sql = "";
        sql += "Delete from Canned_message where  CategoryId = '" + DrpMsgCat.SelectedValue + "' and Type = 'EmployeesEmail'";
        //        sql += "Delete from Canned_message where  CategoryId = '" + DrpMsgCat.SelectedValue + "' and Type = 'Task'";
        int intResult2 = DatabaseHelper.executeNonQuery(sql);
        if (intResult != 0)
        {
            load_dropdowns();
            load_ShortDescdrp();
            //***********************************************************
            //ftbComment.Text = "";
            ftbComment.Text = "";
            //**********************************************************
            //===================================
            //===================================
            ////BtnAddMsg.Visible = false;
            BtnDelMsg.Visible = false;
            BtnEditMsg.Visible = false;
            //===================================
            //===================================


            txtshrtDesc.Text = "";
        }

    }
    protected void btncloseCat_Click(object sender, EventArgs e)
    {
        TRNewCat.Visible = false;
        Button1.Visible = true;
        if (DrpMsgCat.SelectedItem.Value != "")
        {
            btnEditCat.Visible = true;

            BtnDelcat.Visible = true;
        }
        lblcatmsg.Text = "";

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        TRNewCat.Visible = true;
        load_ShortDescdrp();
        //********************************************
        //ftbComment.Text = "";
        ftbComment.Text = "";
        //********************************************
        //==============================
        //==============================
        ////BtnAddMsg.Visible = false;
        ////BtnDelMsg.Visible = false;
        ////BtnEditMsg.Visible = false;
        if (DrpShrtDesc.SelectedItem.Value == "")
        {
            BtnAddMsg.Visible = true;
            BtnEditMsg.Visible = false;
            BtnDelMsg.Visible = false;
        }
        Button1.Visible = false;
        btnEditCat.Visible = false;
        BtnDelcat.Visible = false;
        //==============================
        //==============================

        txtshrtDesc.Text = "";

    }
    protected void DrpMsgCat_SelectedIndexChanged(object sender, EventArgs e)
    {
        load_ShortDescdrp();
        //**********************************************
        //ftbComment.Text = "";
        ftbComment.Text = "";
        //**********************************************

        //BtnDelMsg.Visible = false;
        //BtnEditMsg.Visible = false;


        txtshrtDesc.Text = "";
        TREditCat.Visible = false;
        if (DrpMsgCat.SelectedValue.ToString() != "")
        {
            BtnDelcat.Visible = true;
            btnEditCat.Visible = true;
            //=====================================
            ////BtnEditMsg.Enabled = false;
            ////BtnDelMsg.Enabled = false;
            //BtnAddMsg.Enabled = false;
            ////BtnDelMsg.Visible = true;
            ////BtnEditMsg.Visible = true;
            BtnAddMsg.Visible = true;
            Button1.Visible = true;
            TRNewCat.Visible = false;
            BtnEditMsg.Visible = false;
            BtnDelMsg.Visible = false;
            //=====================================
        }
        else
        {
            btnEditCat.Visible = false;
            BtnDelcat.Visible = false;
            TREditCat.Visible = false;
            Button1.Visible = true;
            TRNewCat.Visible = false;
            BtnEditMsg.Visible = false;
            BtnDelMsg.Visible = false;
            //=====================================
            //BtnEditMsg.Enabled = false;
            //BtnDelMsg.Enabled = false;
            //// BtnAddMsg.Enabled = false;
            ////BtnDelMsg.Visible = false;
            ////BtnEditMsg.Visible = false;
            ////BtnAddMsg.Visible = false;
            //=====================================
        }

    }
    protected void DrpShrtDesc_SelectedIndexChanged(object sender, EventArgs e)
    {

        ////txtshrtDesc.Enabled = false;
        //=====================================
        //Hide the ceate button
        //=====================================
        //

        //=====================================
        //=====================================



        //show but make disable the insert and Delete button
        //=====================================
        if (TREditCat.Visible == true)
        {
            BtnEditMsg.Text = "Update";
            BtnEditMsg.Visible = true;
            BtnDelMsg.Visible = true;
        }
        //else
        //{
        //    BtnEditMsg.Text = "Insert";
        //    BtnEditMsg.Visible = true;
        //}

        ////BtnEditMsg.Enabled = true;
        //BtnDelMsg.Enabled = false;
        //////BtnAddMsg.Enabled = false;
        //BtnDelMsg.Visible = true;
        BtnAddMsg.Visible = false;
        //=====================================


        sql = @"select *
		from Canned_message WHERE MessageId = '" + DrpShrtDesc.SelectedValue + "' and Type = 'EmployeesEmail' ";
        //from Canned_message WHERE MessageId = '" + DrpShrtDesc.SelectedValue + "' and Type = 'Task' ";



        // do a batch of sql statements
        DataSet ds_Message = DatabaseHelper.getDataset(sql);


        if (ds_Message != null && ds_Message.Tables.Count > 0 && ds_Message.Tables[0].Rows.Count > 0)
        {
            //*******************************************************************************************
            //ftbComment.Text = ds_Message.Tables[0].Rows[0]["LongDesc"].ToString();
            ftbComment.Text = ds_Message.Tables[0].Rows[0]["LongDesc"].ToString();
            //*******************************************************************************************


            txtshrtDesc.Text = ds_Message.Tables[0].Rows[0]["shortDesc"].ToString();
            //// DrpMsgCat.SelectedValue.ToString() ==  ds_Message.Tables[0].Rows[0]["CategoryId"].ToString();

            //======================================================================
            //Session["Canned"] = ds_Message.Tables[0].Rows[0]["LongDesc"].ToString();

            //*********************************************************************************************
            //ftbComment.Text = ds_Message.Tables[0].Rows[0]["LongDesc"].ToString();
            ftbComment.Text = ds_Message.Tables[0].Rows[0]["LongDesc"].ToString();
            //*********************************************************************************************
            //======================================================================

            //chkdisable.Visible = true;
        }
        else
        {
            //*********************************************************************************************
            //ftbComment.Text = "";
            ftbComment.Text = "";
            //*********************************************************************************************

            BtnEditMsg.Visible = false;
            BtnDelMsg.Visible = false;
            //=====================================
            //=====================================
            BtnAddMsg.Visible = true;
            //=====================================
            //=====================================
            /// txtshrtDesc.Enabled = true;
            txtshrtDesc.Text = "";
            //chkdisable.Visible = false;
        }

    }

    protected void BtnEditMsg_Click(object sender, EventArgs e)
    {
        try
        {
            if (BtnEditMsg.Text == "Update")
            {
                //************************************************************************************************************************************
                //sql += " update Canned_message set [CategoryId] = '" + DrpMsgCat.SelectedValue.Replace("'", "''") + "',[shortDesc] = '" + txtshrtDesc.Text.Replace("'", "''") + "',[LongDesc] = '" + ftbComment.Text.Replace("'", "''") + "' where MessageId ='" + DrpShrtDesc.SelectedValue + "' and Type = 'Task'";
                sql += " update Canned_message set [CategoryId] = '" + DrpMsgCat.SelectedValue.Replace("'", "''") + "',[shortDesc] = '" + txtshrtDesc.Text.Replace("'", "''") + "',[LongDesc] = '" + ftbComment.Text.Replace("'", "''") + "' where MessageId ='" + DrpShrtDesc.SelectedValue + "' and Type = 'EmployeesEmail'";
                //*************************************************************************************************************************************
                int intResult = DatabaseHelper.executeNonQuery(sql);

                if (intResult != 0)
                {

                    //load_ShortDescdrp();
                    //Session["Task_Canned_msg"] = ftbComment.Text;
                    //Session["Task_ShrtDescCanned_msg"] = txtshrtDesc.Text;
                    //Page.ClientScript.RegisterStartupScript(typeof(Page), "test", "<script> updateParentWindow()</script>");
                    divMessage1.InnerHtml = "Canned message updated successfully.";
                }
                else
                {
                    divMessage1.InnerHtml = "Proccess failed,please try again.";
                }
            }
            else
            {
                ////***************************************************************************
                ////Session["Task_Canned_msg"] = ftbComment.Text;
                //Session["Task_Canned_msg"] = ftbComment.Content;
                ////***************************************************************************
                //Session["Task_ShrtDescCanned_msg"] = txtshrtDesc.Text;
                //Page.ClientScript.RegisterStartupScript(typeof(Page), "test", "<script> updateParentWindow()</script>");
            }

        }
        catch { }
    }
    protected void BtnDelMsg_Click(object sender, EventArgs e)
    {
        try
        {
            //sql += "delete from Canned_message where [CategoryId] = '" + DrpMsgCat.SelectedValue.Replace("'", "''") + "'and [MessageId] = '" + DrpShrtDesc.SelectedValue + "' and Type = 'Task'";
            sql += "delete from Canned_message where [CategoryId] = '" + DrpMsgCat.SelectedValue.Replace("'", "''") + "'and [MessageId] = '" + DrpShrtDesc.SelectedValue + "' and Type = 'EmployeesEmail'";

            int intResult = DatabaseHelper.executeNonQuery(sql);

            if (intResult != 0)
            {
                //*********************************************************************************************
                //ftbComment.Text = "";
                ftbComment.Text = "";
                //*********************************************************************************************
                txtshrtDesc.Text = "";
                load_ShortDescdrp();
                divMessage1.InnerHtml = "Message deleted successfully.";


            }
            else
            {
                divMessage1.InnerHtml = "Proccess failed,please try again.";
            }
        }
        catch { }
    }
    protected void btnEditCat_Click(object sender, EventArgs e)
    {

        TRNewCat.Visible = false;
        TREditCat.Visible = true;

        load_ShortDescdrp();
        txteditCat.Text = DrpMsgCat.SelectedItem.ToString();
        //*********************************************************************************************
        //ftbComment.Text = "";
        ftbComment.Text = "";
        //*********************************************************************************************
        //=====================================
        //=====================================
        ////BtnAddMsg.Visible = false;
        ////BtnDelMsg.Visible = false;
        ////BtnEditMsg.Visible = false;
        if (DrpShrtDesc.SelectedItem.Value == "")
        {
            BtnAddMsg.Visible = true;
            BtnEditMsg.Visible = false;
            BtnDelMsg.Visible = false;
        }
        btnEditCat.Visible = false;
        Button1.Visible = false;
        BtnDelcat.Visible = false;
        //=====================================
        //=====================================

        txtshrtDesc.Text = "";


    }
    protected void btnCnclUpdate_Click(object sender, EventArgs e)
    {
        TREditCat.Visible = false;
        btnEditCat.Visible = true;
        Button1.Visible = true;
        BtnDelcat.Visible = true;
        BtnEditMsg.Visible = false;
        BtnDelMsg.Visible = false;
        //BtnEditMsg.Text = "Insert";
        BtnAddMsg.Visible = true;
    }
    protected void btnUpdateCat_Click(object sender, EventArgs e)
    {
        try
        {
            //sql += "Update Message_category set CategoryName = '" + txteditCat.Text + "' where  CatId = " + DrpMsgCat.SelectedValue + " and Type = 'Task'";
            sql += "Update Message_category set CategoryName = '" + txteditCat.Text + "' where  CatId = " + DrpMsgCat.SelectedValue + " and Type = 'EmployeesEmail'";


            int intResult = DatabaseHelper.executeNonQuery(sql);
            if (intResult != 0)
            {
                load_dropdowns();
                txteditCat.Text = "";
                TREditCat.Visible = false;
                btnEditCat.Visible = true;
                Button1.Visible = true;
                BtnDelcat.Visible = true;
            }
            else
            {

                TREditCat.Visible = true;
                lbleditmessase.Text = "Proccess failed , please try again.";
            }
        }
        catch { }

    }
    //protected void BtnEdit_Click(object sender, EventArgs e)
    //{
    //    //===============================
    //    //===============================
    //    //DrpMsgCat.Enabled = true;
    //    //Button1.Enabled = true;
    //    //DrpShrtDesc.Enabled = true;
    //    //txtshrtDesc.Enabled = true;
    //    //ftbComment.EnableHtmlMode = true;
    //    //BtnAddMsg.Enabled = true;
    //    //ftbComment.ReadOnly = false;
    //    //===============================
    //    //===============================
    //}

    string FileNameToAttache;

    protected void btnsend_Click(object sender, EventArgs e)
    {
        deletefile();
        string selectedUsers = "";
        string AttachedFile = "";
        for (int i = 0; i < chklstempname.Items.Count; i++)
        {
            if (chklstempname.Items[i].Selected)
            {
                //  Also, set a breakpoint here... are you hitting this line?
                selectedUsers += chklstempname.Items[i].Text + ";";
            }

        }
        selectedUsers = selectedUsers.TrimEnd(new char[] { ';', ' ' });

        try
        {

            //if (AttachFile.PostedFile != null)
            if (AttachFile.HasFile)
            {

                HttpPostedFile attFile = AttachFile.PostedFile;
                int attachFileLength = attFile.ContentLength;

                if (attachFileLength > 0)
                {
                    FileNameToAttache = AttachFile.PostedFile.FileName;
                    AttachFile.PostedFile.SaveAs(Server.MapPath("Emails attachments/" + FileNameToAttache));
                    AttachedFile = Server.MapPath("Emails attachments/" + FileNameToAttache);
                }

            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message + " , " + ex.StackTrace);
        }
        //bool result = true;

        bool result = DatabaseHelper.sendMultiEmail(selectedUsers, txtshrtDesc.Text, ftbComment.Text.ToString(), AttachedFile);
       
        //try
        //{
        //    FileInfo fb = new FileInfo(AttachedFile);
            
        //    if (fb.Exists)
        //    {
        //        fb.Delete();
        //    }
        //}
        //catch(Exception ex)
        //{
        //    Response.Write(ex.Message + " , " + ex.StackTrace);
        //}
        if (result == true)
        {
            lblerror.InnerHtml = "sucessfully sent email to " + selectedUsers.Replace(";", ",") + ".";
            clear();
        }
        else
        {
            lblerror.InnerHtml = "Try again later";
        }
    }

    void deletefile()
    {
        foreach (var file in Directory.GetFiles(Server.MapPath("Emails attachments")))
        {
            File.Delete(file);
        }

    }
    void clear()
    {
        chklstempname.ClearSelection();
        DrpMsgCat.SelectedIndex = 0;
        DrpShrtDesc.SelectedIndex = 0;
        txtshrtDesc.Text = string.Empty;
        ftbComment.Text = "";

    }
}
