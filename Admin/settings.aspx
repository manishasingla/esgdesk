<%@ Page Language="C#" AutoEventWireup="true" CodeFile="settings.aspx.cs" Inherits="Admin_settings" %>

<%@ Register Src="Notifications.ascx" TagName="Notifications" TagPrefix="uc4" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        Settings</title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <uc2:Admin_Header ID="Admin_Header1" runat="server" />
        <div id="Content">
            <div>
                <uc4:Notifications ID="Notifications" runat="server" />
            </div>
            <div id="DivEntry" runat="server">
                <table>
                    <tr>
                        <td align="left" valign="top">
                            <table style="width: 424px">
                                <tr>
                                    <td align="left" class="titleText" valign="top" style="width: 424px; display: none;">
                                        Settings
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 424px">
                                        <table width="424" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="left" valign="top">
                                                    <img height="16" src="../images/loginBox_top.png" width="420" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="background: url(../images/loginBox_mid.png) repeat-y left top;">
                                                    <div style="text-align: center">
                                                        <table width="100%" cellpadding="5">
                                                            <tr>
                                                                <td align="left" class="whitetext2" valign="top" width="5" style="height: 28px">
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext2" width="90" style="height: 28px">
                                                                    User ID:
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext2" style="height: 28px">
                                                                    <asp:Label ID="lblId" runat="server">New</asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" class="whitetext2" valign="top">
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext2">
                                                                    Username:
                                                                </td>
                                                                <td align="left" valign="top">
                                                                    <asp:TextBox ID="txtUserName" runat="server" MaxLength="50" Width="240px"></asp:TextBox><cc1:ValidatorCalloutExtender
                                                                        ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator1">
                                                                    </cc1:ValidatorCalloutExtender>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUserName"
                                                                        ErrorMessage="Username is required." Display="Dynamic">*</asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" class="whitetext2" valign="top">
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext2">
                                                                    First name:
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext2">
                                                                    <asp:TextBox ID="txtFirstName" runat="server" MaxLength="50" Width="240px"></asp:TextBox>
                                                                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server" TargetControlID="RequiredFieldValidator3">
                                                                    </cc1:ValidatorCalloutExtender>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtFirstName"
                                                                        ErrorMessage="First name is required." Display="Dynamic">*</asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" class="whitetext2" valign="top">
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext2">
                                                                    Last name:
                                                                </td>
                                                                <td align="left" valign="top">
                                                                    <asp:TextBox ID="txtLastName" runat="server" MaxLength="50" Width="240px"></asp:TextBox>
                                                                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="server" TargetControlID="RequiredFieldValidator4">
                                                                    </cc1:ValidatorCalloutExtender>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtLastName"
                                                                        ErrorMessage="Last name is required." Display="Dynamic">*</asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" class="whitetext2" valign="top">
                                                                </td>
                                                                <td align="left" valign="top" class="whitetext2">
                                                                    Email address:
                                                                </td>
                                                                <td align="left" valign="top">
                                                                    <asp:TextBox ID="txtEmail" runat="server" MaxLength="50" Width="240px"></asp:TextBox>
                                                                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="server" TargetControlID="RequiredFieldValidator5">
                                                                    </cc1:ValidatorCalloutExtender>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtEmail"
                                                                        ErrorMessage="Email is required." Display="Dynamic">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                                                            ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail"
                                                                            Display="Dynamic" ErrorMessage="Format of email address is invalid." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                                                                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="server" TargetControlID="RegularExpressionValidator1">
                                                                    </cc1:ValidatorCalloutExtender>
                                                                </td>
                                                            </tr>
                                            </tr>
                                            <tr>
                                                <td align="left" class="whitetext2" valign="top">
                                                </td>
                                                <td align="left" valign="top" class="whitetext2">
                                                    Image Url:
                                                </td>
                                                <td align="left" valign="top">
                                                    <asp:FileUpload ID="fileImgUpload" runat="server" onchange="setTextBoxValue(this);" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" colspan="1" valign="top">
                                                </td>
                                                <td align="right" colspan="3" valign="top">
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr runat="server" id="adminSettings">
                                                            <td height="166" colspan="5" align="left">
                                                                <table cellpadding="0" border="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td align="left" class="whitetext2">
                                                                            <asp:CheckBox ID="chkAdmin" runat="server" Text="Admin" />
                                                                        </td>
                                                                        <td align="left">
                                                                        </td>
                                                                        <td align="left">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" style="height: 10px">
                                                                        </td>
                                                                        <td align="left" style="height: 10px">
                                                                        </td>
                                                                        <td align="left" style="height: 10px">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" class="whitetext2">
                                                                            <asp:CheckBox ID="chkDeleteTasks" runat="server" Text="Can delete tasks" />
                                                                        </td>
                                                                        <td align="left" style="width: 15px">
                                                                        </td>
                                                                        <td align="left" class="whitetext2">
                                                                            <asp:CheckBox ID="chkShowDeletedTasks" runat="server" Text="Show deleted tasks" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" style="height: 10px">
                                                                        </td>
                                                                        <td align="left" style="height: 10px">
                                                                        </td>
                                                                        <td align="left" style="height: 10px">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" class="whitetext2">
                                                                            <asp:CheckBox ID="chkShowClientRequests" runat="server" Text="Show client requests" />
                                                                        </td>
                                                                        <td align="left" style="width: 15px">
                                                                        </td>
                                                                        <td align="left" class="whitetext2">
                                                                            <asp:CheckBox ID="chkShowClosedTasks" runat="server" Text="Show closed tasks" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" style="height: 10px">
                                                                        </td>
                                                                        <td align="left" style="width: 15px">
                                                                        </td>
                                                                        <td align="left">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" colspan="3" class="whitetext2">
                                                                            <asp:CheckBox ID="chkEditDelete" runat="server" Text="Can edit/delete comments & attachments" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" style="height: 10px">
                                                                        </td>
                                                                        <td align="left" style="width: 15px">
                                                                        </td>
                                                                        <td align="left">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" colspan="3">
                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                <tr>
                                                                                    <td width="130" class="whitetext2">
                                                                                        Default selected item
                                                                                    </td>
                                                                                    <td class="whitetext2">
                                                                                        <asp:DropDownList ID="drpFilter" runat="server" CssClass="filerDrpodown" Width="150px">
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="height: 10px">
                                                                                    </td>
                                                                                    <td>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" class="whitetext2">
                                                                                        <asp:CheckBox ID="chkActive" runat="server" Checked="True" Text="Active" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" colspan="1" valign="top" class="whitetext2">
                                                                Email notification settings
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" class="whitetext2">
                                                                <asp:RadioButtonList ID="rbEmailNotification" runat="server" RepeatColumns="2">
                                                                    <asp:ListItem Selected="True" Value="1">Email all changes</asp:ListItem>
                                                                    <asp:ListItem Value="0">Email comments only</asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" class="whitetext2">
                                                                <asp:CheckBox ID="chkSubscription" runat="server" Text="Subscribe to all tasks" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 100px">
                                        <img height="16" src="../images/loginBox_btm.png" width="420" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 424px">
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="left" valign="middle">
                                        <div id="divMessage" runat="server" style="color: Red; font-weight: bold">
                                        </div>
                                    </td>
                                    <td align="right" valign="middle">
                                        <asp:Button ID="btnCreate" runat="server" Text="Update" OnClick="btnCreate_Click"
                                            CssClass="blueBtns" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 424px">
                            <div id="div1" style="color: Red" runat="server" visible="true">
                            </div>
                        </td>
                    </tr>
                    </table> </td> <td valign="top"><table width="100%"><tr><td>
                        <img runat="server" ID="EmpImage" onerror="this.src='EmpImages/NoImage.png'"
                         onclick=""
                        style="margin: 0px 0 0 0px;cursor:pointer" width="142" height="155" />
                        </td></tr></table></td>
 
                    
                     </tr>

                </table>
            </div>
            <div style="clear: both">
            </div>
            <br />
            <div>
            </div>
        </div>
    </div>
    <uc1:footer ID="Footer1" runat="server" />
    </form>
</body>
</html>
