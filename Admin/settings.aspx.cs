using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

public partial class Admin_settings : System.Web.UI.Page
{
    int id;
    string sql = "";
    static string sstrEmpImgURL = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = (Request.QueryString["id"] == null || Request.QueryString["id"].ToString() == "" ? "tasks.aspx" : "settings.aspx?id=" + Request.QueryString["id"].ToString());
            Response.Redirect("login.aspx");
            return;
        }
        string var = Request.QueryString["id"];
        if (var == null)
        {
            id = 0;
            Response.Redirect("tasks.aspx");
            return;
        }
        else
        {
            try
            {
                id = Convert.ToInt32(var);
            }
            catch
            {
                divMessage.InnerHtml = "User ID must be an integer.";
                DivEntry.Visible = false;
                return;
            }
        }
        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            adminSettings.Visible = false;
            chkSubscription.Visible = false;
        }
        if (!IsPostBack)
        {
            load_filterDropDown();
            drpFilter.SelectedValue = "Show my open tasks";
            // add or edit?
            if (id == 0)
            {
                clearControls();
            }
            else
            {
                string strSql = "select * from users where user_id=" + id;
                DataSet ds = DatabaseHelper.getDataset(strSql);
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        lblId.Text = ds.Tables[0].Rows[0]["user_id"].ToString();
                        txtUserName.Text = ds.Tables[0].Rows[0]["username"].ToString();
                        txtFirstName.Text = ds.Tables[0].Rows[0]["firstname"].ToString();
                        txtLastName.Text = ds.Tables[0].Rows[0]["lastname"].ToString();
                        txtEmail.Text = ds.Tables[0].Rows[0]["email"].ToString();
                        chkAdmin.Checked = ds.Tables[0].Rows[0]["admin"].ToString() == "Y" ? true : false;
                        chkSubscription.Checked = ds.Tables[0].Rows[0]["subscribe_to_all_tasks"].ToString() == "Y" ? true : false;
                        chkDeleteTasks.Checked = ds.Tables[0].Rows[0]["can_delete_tasks"].ToString() == "Y" ? true : false;
                        chkEditDelete.Checked = ds.Tables[0].Rows[0]["Can_edit_delete_comments_attachments"].ToString() == "Y" ? true : false;
                        chkActive.Checked = ds.Tables[0].Rows[0]["active"].ToString() == "Y" ? true : false;
                        chkShowClosedTasks.Checked = ds.Tables[0].Rows[0]["show_closed_tasks"].ToString() == "Y" ? true : false;
                        chkShowDeletedTasks.Checked = ds.Tables[0].Rows[0]["show_deleted_tasks"].ToString() == "Y" ? true : false;
                        chkShowClientRequests.Checked = ds.Tables[0].Rows[0]["show_client_requests"].ToString() == "Y" ? true : false;
                        rbEmailNotification.SelectedValue = ds.Tables[0].Rows[0]["EmailNotification"].ToString() == "Y" ? "1" : "0";
                        drpFilter.SelectedValue = ds.Tables[0].Rows[0]["default_selected_item"].ToString();
                        btnCreate.Text = "Update";
                        DivEntry.Visible = true;
                        divMessage.InnerHtml = "";

                        txtUserName.Enabled = false;
                        if (ds.Tables[0].Rows[0]["EmpImgURL"].ToString().Trim() == "")
                        {
                            EmpImage.Src = "EmpImages/NoImage.png";//Admin/EmpImages/NoImage.png";
                            sstrEmpImgURL = "EmpImages/NoImage.png";
                        }
                        else
                        {
                            EmpImage.Src = ds.Tables[0].Rows[0]["EmpImgURL"].ToString().Trim();
                            sstrEmpImgURL = ds.Tables[0].Rows[0]["EmpImgURL"].ToString().Trim();
                        }
                    }
                    else
                    {
                        divMessage.InnerHtml = "User was not found.";
                        DivEntry.Visible = false;
                    }
                }
                else
                {
                    divMessage.InnerHtml = "User was not found.";
                    DivEntry.Visible = false;
                }
            }
           
        }

        if (Session["message"] != null)
        {
            divMessage.InnerHtml = Session["message"].ToString();
            Session.Remove("message");
        }
    }
    protected void btnCreate_Click(object sender, EventArgs e)
    {

        bool flag = false;
        string strMsg = "";
        if (txtUserName.Text.Trim() == "")
        {
            strMsg += "- Username is required.<br>";
            flag = true;
        }
        if (txtFirstName.Text.Trim() == "")
        {
            strMsg += "- First name is required.<br>";
            flag = true;
        }
        if (txtLastName.Text.Trim() == "")
        {
            strMsg += "- Last name is required.<br>";
            flag = true;
        }
        if (txtEmail.Text.Trim() == "")
        {
            strMsg += "- Email is required.<br>";
            flag = true;
        }
        else
        {
            if (!DatabaseHelper.validate_email(txtEmail.Text.Trim()))
            {
                strMsg += "- Format of email address is invalid.<br>";
                flag = true;
            }
        }

        if (flag)
        {
            divMessage.InnerHtml = strMsg;
            return;
        }

        if (btnCreate.Text == "Update")
        {


            if (fileImgUpload.HasFile)
            {
                string filename = Path.GetFileName(fileImgUpload.PostedFile.FileName);
                //Save images into Images folder
                fileImgUpload.SaveAs(Server.MapPath("EmpImages/" + id.ToString() + filename));
                sstrEmpImgURL = "EmpImages/" + id.ToString() + filename;

            }
            
            object objResult = DatabaseHelper.executeScalar("select count(*) from users where [username]='" + txtUserName.Text.Trim() + "' and [user_id]!=" + lblId.Text);

            if (objResult.ToString() != "0")
            {
                divMessage.InnerHtml = "Username already exists. Choose another username.";
                return;
            }
            string strSql = " update users set";
            strSql += " username = '" + txtUserName.Text.Trim().Replace("'", "''") + "',";
            strSql += " firstname = '" + txtFirstName.Text.Trim().Replace("'", "''") + "',";
            strSql += " lastname = '" + txtLastName.Text.Trim().Replace("'", "''") + "',";
            strSql += " email = '" + txtEmail.Text.Trim().Replace("'", "''") + "',";
            strSql += " admin = '" + (chkAdmin.Checked ? "Y" : "N") + "',";
            strSql += " subscribe_to_all_tasks = '" + (chkSubscription.Checked ? "Y" : "N") + "',";
            strSql += " can_delete_tasks = '" + (chkDeleteTasks.Checked ? "Y" : "N") + "',";
            strSql += " Can_edit_delete_comments_attachments = '" + (chkEditDelete.Checked ? "Y" : "N") + "',";
            strSql += " active ='" + (chkActive.Checked ? "Y" : "N") + "',";
            strSql += " show_closed_tasks ='" + (chkShowClosedTasks.Checked ? "Y" : "N") + "',";
            strSql += " show_client_requests ='" + (chkShowClientRequests.Checked ? "Y" : "N") + "',";
            strSql += " show_deleted_tasks ='" + (chkShowDeletedTasks.Checked ? "Y" : "N") + "',";
            strSql += " EmailNotification ='" + (rbEmailNotification.SelectedValue == "1" ? "Y" : "N") + "',";
            strSql += " default_selected_item ='" + drpFilter.SelectedValue + "',";
            strSql += "EmpImgURL='" + sstrEmpImgURL + "' ";
            strSql += " where user_id = " + lblId.Text.Trim();
            int intResult = DatabaseHelper.executeNonQuery(strSql);
            if (intResult != 0)
            {
                Session["message"] = "User was updated.";
                Response.Redirect("settings.aspx?id=" + lblId.Text.Trim());
            }
            else
            {
                divMessage.InnerHtml = "User was not updated.";
            }
        }
    }

    private void clearControls()
    {
        lblId.Text = "New";
        txtUserName.Text = "";
        txtFirstName.Text = "";
        txtLastName.Text = "";
        txtEmail.Text = "";
        chkAdmin.Checked = false;
        chkDeleteTasks.Checked = false;
        chkEditDelete.Checked = true;
        btnCreate.Text = "Update";
    }

    void load_filterDropDown()
    {
        drpFilter.Items.Clear();
        if (DatabaseHelper.isAdmin(Session["Admin"].ToString()))
        {
            sql = "select username from users where active='Y' and admin='Y' and username <> '" + Session["Admin"] + "' order by username";
            DataSet dsAdmin = DatabaseHelper.getDataset(sql);
            if (dsAdmin != null)
            {
                if (dsAdmin.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsAdmin.Tables[0].Rows.Count; i++)
                    {
                        drpFilter.Items.Add("Assigned by " + dsAdmin.Tables[0].Rows[i]["username"]);
                    }
                }
            }

            drpFilter.Items.Add("Assigned by " + Session["Admin"]);
            drpFilter.Items.Add("Assigned by me");
            drpFilter.Items.Add("All tasks by last updated");
            drpFilter.Items.Add("All emps CWO");
            drpFilter.Items.Add("Show my open tasks");
        }
        else
        {
            drpFilter.Items.Add("Show my open tasks");
        }
    }
   
}
