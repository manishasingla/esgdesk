﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="solutioncategoriesdetails.aspx.cs"
    Inherits="Admin_solutioncategoriesdetails" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
 <title>
        Solution categories detail</title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../DashStyle.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <uc2:Admin_Header ID="Admin_Header1" runat="server" />
    <div>
        <div id="inner">
            <div class="pagearea" style="width: 80%; float: left">
                <div id="TicketSummary">
                    <div style="width: 100%; float: right;">
                     <div style="padding: 4px; border-bottom: 1px solid;">
                        <a href="AddsolutionCategory.aspx" style="text-decoration: none">Project default category</a> →
                        <a id="linklist" runat="server" >Notes list</a>
                        
                    </div>
                        <br />
                        <div style="height: 50px; background: white; padding: 4px">
                            <div style="float: left">
                                <asp:Label ID="lblNH" runat="server" Style="font-weight: bolder;"></asp:Label><br />
                                By Support |
                                <asp:Label ID="lbldate" runat="server"></asp:Label>
                            </div>
                            <div style="float: right">
                                <a class="submit" id="A1"   runat="server">Add note</a> &nbsp;
                                <a class="submit" id="editnews" runat="server">Edit</a>
                            </div>
                        </div>
                        <div id="newsdesc" runat="server" style="background: white; padding: 4px">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
