﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Admin_solutioncategoriesdetails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Response.Redirect("login.aspx");
            return;
        }
        if (Request.QueryString["Noteid"] != null)
        {
            getcategory(Request.QueryString["Noteid"].ToString());
        }
        else
        {
            Response.Redirect("solutioncategoriesnotelist.aspx");
        }


    }
    void getcategory(string Nid)
    {

        string sql = @"select * from Solution_categoriesnotes where Note_id=" + Nid;

        DataSet ds = DatabaseHelper.getallstatus(sql);

        if (ds != null)
        {
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                lblNH.Text = ds.Tables[0].Rows[0]["NoteName"].ToString();
                newsdesc.InnerHtml = ds.Tables[0].Rows[0]["NoteDescription"].ToString();
                lbldate.Text = ds.Tables[0].Rows[0]["reported_date"].ToString();
                //editnews.HRef = "AddNews.aspx?id=" + Request.QueryString["Noteid"].ToString();
                editnews.HRef = "solutioncategoriesnote.aspx?folderid=" + ds.Tables[0].Rows[0]["Folder_Id"].ToString() + "&Noteid=" + ds.Tables[0].Rows[0]["Note_Id"].ToString();
                A1.HRef = "solutioncategoriesnote.aspx?folderid=" + ds.Tables[0].Rows[0]["Folder_Id"].ToString();
                linklist.HRef = "solutioncategoriesnotelist.aspx?folderid=" + ds.Tables[0].Rows[0]["Folder_Id"].ToString();
                this.Title = "View note - " + lblNH.Text;
            }
            else
            {
                Response.Redirect("Dashboard.aspx");
            }

        }
        else
        {
            Response.Redirect("Dashboard.aspx");
        }



    }
}