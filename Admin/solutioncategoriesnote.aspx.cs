﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Admin_Addsolution : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Response.Redirect("login.aspx");
            return;

        }

        bntdel.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete this solution?')");
        if (!IsPostBack)
        {
            if (Request.QueryString["folderid"] != null && Request.QueryString["Noteid"] != null)
            {
                // getdropvalues(Request.QueryString["projectname"].ToString());
                getsolution(Request.QueryString["folderid"], Request.QueryString["Noteid"]);

                  
            }
            else if (Request.QueryString["folderid"] != null)
            {
                // getdropvalues(Request.QueryString["projectname"].ToString());
                //ddlfolderlist.SelectedValue = Request.QueryString["folderid"].ToString();
                Page.Title = "Add solution note";
            }
            else
            {
                Response.Redirect("AddsolutionCategory.aspx");
            }
        }
    }

    void getsolution(string fid, string sid)
    {
        //string sql = @"select * from solutions where category_id=" + cid + " and folder_id= " + fid + " and solution_id=" + sid;
        //string sql = @"select * from ProjectNotes where Project_Name='" + cid + "' and folder_id= " + fid + " and Note_Id=" + sid;
        string sql = @"select * from Solution_categoriesnotes where Folder_Id= " + fid + " and Note_Id=" + sid;

        DataSet ds = DatabaseHelper.getallstatus(sql);
        if (ds != null)
        {
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                txtSolutionname.Text = ds.Tables[0].Rows[0]["NoteName"].ToString().Replace("|@", "'");
                ftbComment.Content = ds.Tables[0].Rows[0]["NoteDescription"].ToString().Replace("|@", "'");



                bntdel.Visible = true;
                btnupdate.Visible = true;
                btnsave.Visible = false;
                btnSaveAnother.Visible = false;
                Page.Title = "Edit note - " + txtSolutionname.Text;
            }
        }
    }
    private void CategoryInsert(string next)
    {

        //        string sql = @"insert into solutions 
        //(Folder_Id,category_id,SolutionName,SolutionDescription,Tags,SolutionType,SolutionStatus,reported_date)values
        //                     (" + ddlfolderlist.SelectedValue + "," + Request.QueryString["categoryid"].ToString() + ",'" + txtSolutionname.Text.Replace("'", "|@").Trim() + "','" + ftbComment.Content.Replace("'", "|@") + "','"
        //                         + txttag.Text.Replace("'", "|@").Trim() + "','" + ddlSolutionType.SelectedItem.Text + "','" + ddlSolutionStatus.SelectedItem.Text + "',getdate())";

        string sql = @"insert into Solution_categoriesnotes 
(Folder_Id,NoteName,NoteDescription,reported_date)values
(" + Request.QueryString["folderid"] + ",'" + txtSolutionname.Text.Replace("'", "|@").Trim() + "',N'" + ftbComment.Content.Replace("'", "|@") + "',getdate())";

        int intResult = DatabaseHelper.executeSQLquery(sql);

        if (intResult == 0)
        {
            errorExplanation.Visible = true;
            ulerror.Visible = true;
        }
        else
        {
            if (next == "")
            {
                if (Request.QueryString["folderid"] != null && Request.QueryString["page"] != null)
        
                {
                    Response.Redirect("solutioncategoriesnotelist.aspx?folderid=" + Request.QueryString["folderid"].ToString());
                    //Response.Redirect("ProjectNoteList.aspx?projectname=" + Request.QueryString["projectname"].ToString() + "&folderid=" + Request.QueryString["folderid"].ToString());
                }
                else
                {

                    Response.Redirect("AddsolutionCategory.aspx");
                    //Response.Redirect("ProjectNoteList.aspx?projectname=" + Request.QueryString["projectname"].ToString());
                }
            }
            else
            {
                clear();
            }
        }

    }

    private void Categoryupdate(string fid, string nid)
    {


        //        string sql = @"update solutions 
        //set Folder_Id=" + ddlfolderlist.SelectedValue + ",SolutionName='" + txtSolutionname.Text.Replace("'", "|@").Trim() + "',SolutionDescription='" + ftbComment.Content.Replace("'", "|@") + "',Tags='" + txttag.Text.Replace("'", "|@").Trim() + "',SolutionType='" + ddlSolutionType.SelectedItem.Text + "',SolutionStatus='" + ddlSolutionStatus.SelectedItem.Text + "' where Category_Id=" + cid + " and Folder_Id=" + fid + " and solution_id=" + sid;

        string sql = @"update Solution_categoriesnotes 
set NoteName='" + txtSolutionname.Text.Replace("'", "|@").Trim() + "',NoteDescription=N'" + ftbComment.Content.Replace("'", "|@") + "' where  Folder_Id=" + fid + " and Note_Id=" + nid;


        int intResult = DatabaseHelper.executeSQLquery(sql);

        if (intResult == 0)
        {
            errorExplanation.Visible = true;
            ulerror.Visible = true;
        }
        else
        {

            Response.Redirect("solutioncategoriesnotelist.aspx?folderid=" + Request.QueryString["folderid"].ToString());
             

            //Response.Redirect("Solutionslist.aspx?cid=" + Request.QueryString["cid"].ToString() + "&id=" + Request.QueryString["fid"].ToString());
            // Response.Redirect("ProjectNoteList.aspx?projectname=" + Request.QueryString["projectname"].ToString() + "&folderid=" + ddlfolderlist.SelectedValue + "&solutionid=" + sid);

        }

    }

    private void solutiondelete(string folderid, string noteid)
    {
        //string sqlsub = @"delete from solutions where Category_id=" + Categoryid + " and Folder_Id=" + folderid + " and solution_id=" + solutionid;
        string sqlsub = @"delete from Solution_categoriesnotes where Folder_Id=" + folderid + " and Note_Id=" + noteid;

        int Result = DatabaseHelper.executeSQLquery(sqlsub);

        if (Result == 0)
        {
            errorExplanation.Visible = true;
            ulerror.Visible = true;
        }
        else
        {
            //Response.Redirect("Solutionslist.aspx?cid=" + Request.QueryString["cid"].ToString() + "&id=" + Request.QueryString["fid"].ToString());
            //  Response.Redirect("ProjectNoteList.aspx?projectname=" + Request.QueryString["projectname"].ToString() + "&folderid=" + ddlfolderlist.SelectedValue);
           // Response.Redirect("AddsolutionCategory.aspx");
            if (Request.QueryString["folderid"] != null && Request.QueryString["page"] != null)
            {
                Response.Redirect("solutioncategoriesnotelist.aspx?folderid=" + Request.QueryString["folderid"].ToString());
                //Response.Redirect("ProjectNoteList.aspx?projectname=" + Request.QueryString["projectname"].ToString() + "&folderid=" + Request.QueryString["folderid"].ToString());
            }
            else
            {

                Response.Redirect("AddsolutionCategory.aspx");
                //Response.Redirect("ProjectNoteList.aspx?projectname=" + Request.QueryString["projectname"].ToString());
            }
        }

    }

    void clear()
    {
        txtSolutionname.Text = string.Empty;
        ftbComment.Content = string.Empty;
        errorExplanation.Visible = false;
        ulalready.Visible = false;
        ulerror.Visible = false;
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        CategoryInsert("");
    }
    protected void btnSaveAnother_Click(object sender, EventArgs e)
    {
        CategoryInsert("Yes");
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {

        //if (Request.QueryString["cid"] != null && Request.QueryString["fid"] != null)
        //{
        //    Response.Redirect("Solutionslist.aspx?cid=" + Request.QueryString["cid"].ToString() + "&id=" + Request.QueryString["fid"].ToString());
        //}
        //else
        //{
        //    Response.Redirect("Solutionslist.aspx?cid=" + Request.QueryString["cid"].ToString());
        //}
        if (Request.QueryString["folderid"] != null && Request.QueryString["page"] != null)
        {
            Response.Redirect("solutioncategoriesnotelist.aspx?folderid=" + Request.QueryString["folderid"].ToString());
          
        }
          else if (Request.QueryString["folderid"] != null)
        {
            Response.Redirect("AddsolutionCategory.aspx");
            
        }

        //else if (Request.QueryString["projectname"] != null && Request.QueryString["folderid"] != null && Request.QueryString["page"] != null)
        //else if (Request.QueryString["folderid"] != null && Request.QueryString["folderid"] != null)
        //{
        //    Response.Redirect("solutioncategoriesnotelist.aspx?folderid=" + Request.QueryString["folderid"].ToString());
        //}

        else
        {
            Response.Redirect("AddsolutionCategory.aspx");
        }
    }
    protected void bntdel_Click(object sender, EventArgs e)
    {
        solutiondelete(Request.QueryString["folderid"], Request.QueryString["Noteid"]);
    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        Categoryupdate(Request.QueryString["folderid"], Request.QueryString["Noteid"]);
    }
}