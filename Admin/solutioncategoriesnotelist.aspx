﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="solutioncategoriesnotelist.aspx.cs"
    Inherits="Admin_solutioncategoriesnotelist" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
     <title>
        Solution categories note</title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../DashStyle.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <uc2:Admin_Header ID="Admin_Header1" runat="server" />
    <div>
        <div id="inner">
            <div class="pagearea" style="width: 80%; float: left">
                <div id="TicketSummary">
                    <div style="width: 100%; float: right; background: #EBEBEB;">
                        <div style="padding: 4px; border-bottom: 1px solid;">
                            <a href="AddsolutionCategory.aspx" style="text-decoration: none">Project default category</a>
                        </div>
                        <br />
                        <br />
                        <div id="divaction" runat="server" style="height: 50px; padding: 4px">
                            <div style="float: left">
                                <asp:Label ID="lblFN" runat="server" Style="font-weight: bolder;"></asp:Label></div>
                            <div style="float: right">
                                <a class="submit" id="addsolu" runat="server" href="AddNews.aspx">Add note</a>
                            </div>
                        </div>
                        <div style="background: white">
                            <br />
                        </div>
                        <div style="background: white; padding: 4px">
                            <asp:GridView ID="gridlist" runat="server" AutoGenerateColumns="false" ShowHeader="false"
                                Width="100%">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <div style="padding: 2px;">
                                                <div width="90%" style="float: left">
                                                    <asp:Label ID="lblRcount" runat="server" Text='<%#Eval("RowNumber") %>'>' ></asp:Label>.&nbsp;
                                                    <%--<asp:Label ID="lblsolutionsname" runat="server" Text='<%# Eval("Solutionname") %>'> </asp:Label>--%>
                                                    <a href="solutioncategoriesdetails.aspx?Noteid=<%#Eval("note_id")%>">
                                                        <%# Eval("notename")%>
                                                    </a>
                                                </div>
                                                <div width="8%" style="float: right">
                                                    <a href="solutioncategoriesnote.aspx?folderid=<%#Eval("folder_id")%>&Noteid=<%#Eval("note_id")%>&page=list">
                                                        edit </a>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                        <div id="addsolution" runat="server" style="text-align: center" visible="false">
                            <div class="no-info-text">
                                No note available
                            </div>
                            <a class="submit" id="btnaddsolution" runat="server" href="AddNews.aspx">Add note</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
