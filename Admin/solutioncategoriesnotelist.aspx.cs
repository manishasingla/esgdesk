﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Admin_solutioncategoriesnotelist : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Response.Redirect("login.aspx");
            return;
        }
        if (Request.QueryString["folderid"] != null)
        {
            getcategory(Request.QueryString["folderid"].ToString());
            addsolu.HRef = "solutioncategoriesnote.aspx?folderid=" + Request.QueryString["folderid"].ToString() + "&page=list";
            btnaddsolution.HRef = "solutioncategoriesnote.aspx?folderid=" + Request.QueryString["folderid"].ToString() + "&page=list";
        }
    }
    void getcategory(string fid)
    {
        string sql = @"select * from Solution_categories where category_id=" + fid;
        
        DataSet dscheck = DatabaseHelper.getallstatus(sql);
        if (dscheck.Tables != null && dscheck.Tables[0].Rows.Count > 0)
        {
            //string sqlsub = @" select Row_Number()OVER(order by id) as RowNumber,* from newstbl ";
            string sqlsub = @"select Row_Number()OVER(order by note_id) as RowNumber, * from Solution_categoriesnotes where folder_id=" + fid;




            DataSet dssub = DatabaseHelper.getallstatus(sqlsub);
            if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0)
            {

                gridlist.DataSource = dssub.Tables[0];
                gridlist.DataBind();

            }
            else
            {
                gridlist.Visible = false;
                addsolution.Visible = true;

            }
        }
        else
        {
            Response.Redirect("AddsolutionCategory.aspx");
        }

    }
}