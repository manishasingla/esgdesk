<%@ Page Language="C#" AutoEventWireup="true" CodeFile="statuses.aspx.cs" Inherits="Admin_statuses" %>

<%@ Register Src="Notifications.ascx" TagName="Notifications" TagPrefix="uc4" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        Status </title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
     table#DataGrid1_ctl00 tr th a
     {
      display:block;
      text-decoration:none;
      margin:0;
      padding:0;
      width:100%;
      height:100%;
     }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <uc2:Admin_Header ID="Admin_Header1" runat="server" />
        <div id="Content">
            <div id="Content_98">
                <div class="titleText" style="margin: 10px 0; display: none;">
                    Statuses&nbsp;|&nbsp;</div>
                <div>
                    <uc4:Notifications ID="Notifications" runat="server" />
                </div>
                <div style="width: 100%; background: #ebebeb; padding: 5px 0; float: left;">
                    &nbsp;&nbsp;<asp:LinkButton ID="lnkAddNew" style="Border-bottom:1px dotted #000;" runat="server" OnClick="lnkAddNew_Click"
                        CausesValidation="False">Add new status</asp:LinkButton>
                    <asp:LinkButton ID="lnkBack" runat="server" style="Border-bottom:1px dotted #000;" OnClick="lnkBack_Click" CausesValidation="False">&nbsp;|&nbsp;Back to status</asp:LinkButton></div>
                <div>
                    <div id="DivEntry" runat="server" visible="false" style="float: left;">
                        <table>
                            <tr>
                                <td align="left" valign="top">
                                    <table style="width: 424px">
                                        <tr>
                                            <td style="width: 424px">
                                                <table width="424" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td align="left" valign="top">
                                                            <img height="16" src="../images/loginBox_top.png" width="420" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" style="background: url(../images/loginBox_mid.png) repeat-y left top;">
                                                            <div style="text-align: center">
                                                                <div id="Div1" runat="server">
                                                                    <table width="100%" cellpadding="8" cellspacing="0">
                                                                        <tr>
                                                                            <td align="left" valign="top" class="whitetext1" style="height: 28px">
                                                                            </td>
                                                                            <td width="200" align="left" valign="top" class="whitetext1" style="height: 28px">
                                                                                Status ID:
                                                                            </td>
                                                                            <td align="left" valign="top" style="width: 298px; height: 28px;" class="whitetext2">
                                                                                <asp:Label ID="lblId" runat="server">New</asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" valign="top" class="whitetext1">
                                                                            </td>
                                                                            <td align="left" valign="top" class="whitetext1">
                                                                                Status name:
                                                                            </td>
                                                                            <td align="left" valign="top" style="width: 298px">
                                                                                <asp:TextBox ID="txtName" runat="server" MaxLength="50" Width="220"></asp:TextBox>
                                                                                <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator1">
                                                                                </cc1:ValidatorCalloutExtender>
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName"
                                                                                    ErrorMessage="Please enter status name.">*</asp:RequiredFieldValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" valign="top" class="whitetext1">
                                                                            </td>
                                                                            <td align="left" valign="top" class="whitetext1">
                                                                                Sort sequence:
                                                                            </td>
                                                                            <td valign="top" align="left" style="width: 298px">
                                                                                <asp:TextBox ID="txtSortSequece" runat="server" Width="220px" MaxLength="2"></asp:TextBox>
                                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"
                                                                                    TargetControlID="txtSortSequece">
                                                                                </cc1:FilteredTextBoxExtender>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" valign="top" class="whitetext1">
                                                                            </td>
                                                                            <td align="left" valign="top" class="whitetext1">
                                                                                Default selection:
                                                                            </td>
                                                                            <td align="left" valign="top" style="width: 298px">
                                                                                <asp:CheckBox ID="chkDefault" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <asp:HiddenField ID="HiddenField1" runat="server" />
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" style="width: 100px">
                                                            <img height="16" src="../images/loginBox_btm.png" width="420" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top">
                                                <hr />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 424px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td align="left" valign="middle">
                                                            <div id="divMessage" runat="server" style="color: Red; font-weight: bold">
                                                            </div>
                                                        </td>
                                                        <td align="right" valign="middle">
                                                            <div align="right" style="float: right;">
                                                                <asp:Button ID="btnCreate" runat="server" Text="Create" OnClick="btnCreate_Click"
                                                                    CssClass="blueBtns" /></div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="clear: both">
                    </div>
                    <div>
                    
                    <%--*****************************************************************************************************--%>
                        <telerik:RadGrid ID="DataGrid1" OnItemCommand="DataGrid1_ItemCommand" OnItemDataBound="DataGrid1_ItemDataBound" runat="server" AutoGenerateColumns="False" CellPadding="3">
                        <MasterTableView PagerStyle-AlwaysVisible="true">
                        <Columns>
                        <%--0--%>
                        <telerik:GridTemplateColumn HeaderText="Status id">
                          <ItemTemplate>
                              <asp:Label id="lblstatusid" runat="server" Text='<%# Bind("status_id")%>'   ></asp:Label>
                          </ItemTemplate> 
                        </telerik:GridTemplateColumn> 
                        <%--1--%>
                        
                        <telerik:GridTemplateColumn HeaderText="Status name">
                        <ItemTemplate>
                        <asp:Label id="lblstatusname" runat="server" Text='<%#Bind("status_name") %>' ></asp:Label>
                        </ItemTemplate> 
                        </telerik:GridTemplateColumn> 
                         <%--2--%>
                         <telerik:GridTemplateColumn HeaderText="Sort sequence">
                           <ItemTemplate>
                               <asp:Label ID="lblsortseq" runat="server" Text='<%# Bind("sort_seq") %>' ></asp:Label>
                           </ItemTemplate> 
                         </telerik:GridTemplateColumn>  
                         <%--3--%>
                       <telerik:GridTemplateColumn HeaderText="Default selection">
                       <ItemTemplate>
                       <asp:Label ID="lbldefaultselection" runat="server"  Text='<%#bind("default_selection")%>' ></asp:Label>
                       </ItemTemplate> 
                       </telerik:GridTemplateColumn> 
                        <%--4--%>
                       
                       
                        
                        
                       <telerik:GridTemplateColumn HeaderText="Edit">
                       <ItemTemplate>
                       <%--<asp:LinkButton id="lnkedit" CommandName="edit" runat="server" Text="edit"></asp:LinkButton>--%>
                       <asp:LinkButton ID="lnkedit" runat="server" CommandName="edit"><img  src="images/edit-icon.png"></image></asp:LinkButton>
                       </ItemTemplate> 
                       </telerik:GridTemplateColumn> 
                       
                       
                       
                       
                       
                       
                       <%--5--%>
                       
                       
                       
                       
                       
                       <telerik:GridTemplateColumn HeaderText="Delete">
                       <ItemTemplate>
                      <%-- <asp:LinkButton id="lnkdelete" CommandName="delete" runat="server" Text="delete"></asp:LinkButton>--%>
                      <asp:LinkButton ID="lnkdelete" runat="server" CommandName="delete"><img src="images/delete-icon.png"></image></asp:LinkButton>
                       </ItemTemplate> 
                       </telerik:GridTemplateColumn> 
                       
                       
                       
                       
                       
                       
                       
                        </Columns> 
                        </MasterTableView> 
                        </telerik:RadGrid> 
                        
                        
                        <%--*****************************************************************************************************--%>
                        <asp:HiddenField ID="prevName" runat="server" />
                        &nbsp;
                    </div>
                </div>
            </div>
        </div>
    </div>
    <uc1:footer ID="Footer1" runat="server" />
    </form>
</body>
</html>
