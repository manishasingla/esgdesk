using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
using Telerik.Web;
public partial class Admin_statuses : System.Web.UI.Page
{
    string sql = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = "statuses.aspx";
            Response.Redirect("login.aspx");
            return;
        }

        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            Response.Redirect("tasks.aspx");
            return;
        }
        
        divMessage.InnerHtml = "";

        if (!Page.IsPostBack)
        {
            bindData();
        }
    }

    protected void btnCreate_Click(object sender, EventArgs e)
    {
        if (txtName.Text.Trim() == "")
        {
            divMessage.InnerHtml = "Please enter status name.";
            return;
        }
        if (btnCreate.Text == "Create")
        {
            object objResult = DatabaseHelper.executeScalar("select count(*) from statuses where [status_name]='" + txtName.Text.Trim() + "'");

            if (objResult.ToString() != "0")
            {
                divMessage.InnerHtml = "Status name alredy exist.";
                return;
            }
            string strSql = " insert into statuses ([status_name],[sort_seq],[default_selection]) ";
            strSql += " values('" + txtName.Text.Trim().Replace("'", "''") + "'," + ((txtSortSequece.Text.Trim() != "") ? txtSortSequece.Text.Trim() : "0") + ",'" + (chkDefault.Checked ? "Y" : "N") + "')";
            int intResult = DatabaseHelper.executeNonQuery(strSql);
            if (intResult != 0)
            {
                divMessage.InnerHtml = "Status was created.";
                clearControls();
                insertcache();
            }
            else
            {
                divMessage.InnerHtml = "Status was not created.";
            }
        }
        else if (btnCreate.Text == "Edit")
        {
            object objResult = DatabaseHelper.executeScalar("select count(*) from statuses where [status_name]='" + txtName.Text.Trim() + "' and [status_id]!=" + lblId.Text);

            if (objResult.ToString() != "0")
            {
                divMessage.InnerHtml = "Status name alredy exist.";
                return;
            }

            string strSql = " update statuses ";
            strSql += " set [status_name]= '" + txtName.Text.Trim().Replace("'", "''") + "',";
            strSql += " [sort_seq] = " + txtSortSequece.Text.Trim() + ",";
            strSql += " [default_selection] = '" + (chkDefault.Checked ? "Y" : "N") +"'";
            strSql += " where status_id = " + lblId.Text.Trim();

            int intResult = DatabaseHelper.executeNonQuery(strSql);

            if (intResult != 0)
            {
                strSql = "update tasks set status = '" + txtName.Text.Trim().Replace("'", "''") + "' ";
                strSql += " where status ='" + prevName.Value.Replace("'", "''") + "'";
                intResult = DatabaseHelper.executeNonQuery(strSql);
                divMessage.InnerHtml = "Status was updated.";
                insertcache();
            }
            else
            {
                divMessage.InnerHtml = "Status was not updated.";
            }
        }
    }

    private void insertcache()
    {
        DataSet ds_dropdowns;
        if (Cache["dropdowns"] != null)
        {
            Cache.Remove("dropdowns");
            sql = @"select project_name	from projects where active = 'Y' order by project_name;";

            // categories
            sql += "\nselect category_name from categories order by sort_seq, category_name;";

            // priorities
            sql += "\nselect priority_name from priorities order by sort_seq, priority_name;";

            // statuses
            sql += "\nselect status_name from statuses order by sort_seq, status_name;";

            // users
            sql += "\nselect username from users where active = 'Y' order by username;";

            ds_dropdowns = DatabaseHelper.getDataset(sql);
            //  Cache["Company_notes"] = ds;
            Cache.Insert("dropdowns", ds_dropdowns, null, DateTime.Now.AddHours(6), TimeSpan.Zero);
        }
        
   }
    private void bindData()
    {
        string strSql = "select * from statuses";

        DataSet ds = DatabaseHelper.getDataset(strSql);

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                DataGrid1.DataSource = ds.Tables[0];
                DataGrid1.DataBind();
                DataGrid1.Visible = true;
                lnkBack.Visible = false;
            }
            else
            {
                DataGrid1.DataSource = null;
                DataGrid1.DataBind();
                DataGrid1.Visible = false;
                lnkBack.Visible = false;
                divMessage.InnerHtml = "No status found.";
            }
        }
        else
        {
            DataGrid1.DataSource = null;
            DataGrid1.DataBind();
            DataGrid1.Visible = false;
            lnkBack.Visible = false;
            divMessage.InnerHtml = "No status found.";
        }
    }
    protected void lnkAddNew_Click(object sender, EventArgs e)
    {
        clearControls();
        DivEntry.Visible = true;
        DataGrid1.Visible = false;
        lnkBack.Visible = true;
        divMessage.InnerHtml = "";
        this.Title = "Add status";
    }

    protected void DataGrid1_ItemCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "edit")
        {
            GridDataItem dataItem = e.Item as GridDataItem;
            string strSql = "select * from statuses where status_id=" +((Label) dataItem.FindControl("lblstatusid")).Text ;
            DataSet ds = DatabaseHelper.getDataset(strSql);
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    lblId.Text = ds.Tables[0].Rows[0]["status_id"].ToString();
                    txtName.Text = ds.Tables[0].Rows[0]["status_name"].ToString();
                    prevName.Value = ds.Tables[0].Rows[0]["status_name"].ToString();
                    txtSortSequece.Text = ds.Tables[0].Rows[0]["sort_seq"].ToString();
                    chkDefault.Checked = ds.Tables[0].Rows[0]["default_selection"].ToString() == "Y" ? true : false;
                    btnCreate.Text = "Edit";
                    DivEntry.Visible = true;
                    DataGrid1.Visible = false;
                    lnkBack.Visible = true;
                    divMessage.InnerHtml = "";
                    this.Title = "Edit status - " + txtName.Text;
                }
            }
        }
        else if (e.CommandName == "delete")
        {
            GridDataItem dataItem = e.Item as GridDataItem;
            object objResult = DatabaseHelper.executeScalar("select count(*) from tasks where status =" + ((Label)dataItem.FindControl("lblstatusname")).Text.Replace("'", "''"));
            if (objResult.ToString() == "0")
            {
                string strSql = "delete from statuses where status_id=" + ((Label)dataItem.FindControl("lblstatusid")).Text;
                int intResult = DatabaseHelper.executeNonQuery(strSql);
                if (intResult != 0)
                {
                    divMessage.InnerHtml = "Status was deleted.";
                    bindData();
                    insertcache();
                    
                }
                else
                {
                    divMessage.InnerHtml = "Status was not deleted.";
                    bindData();
                }
            }
            else
            {
                divMessage.InnerHtml = "You can not delete status \"" + ((Label)dataItem.FindControl("lblstatusname")).Text + "\". It is been refered by another table.";
            }
        }
    }

    protected void DataGrid1_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        foreach (TableCell tc in e.Item.Cells)
        {

            tc.Attributes["style"] = "border:1px solid #00A651;";
        }
        if (e.Item.ItemType == GridItemType.Item || e.Item.ItemType == GridItemType.AlternatingItem)
        {
            GridDataItem dataItem = e.Item as GridDataItem;
            LinkButton deleteButton = (LinkButton)dataItem.FindControl("lnkdelete");
            deleteButton.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete this status?')");
        }
    }

    private void clearControls()
    {
        lblId.Text = "New";
        txtName.Text = "";
        txtSortSequece.Text = "";
        chkDefault.Checked = false;
        btnCreate.Text = "Create";
    }

    protected void lnkBack_Click(object sender, EventArgs e)
    {
        clearControls();
        DivEntry.Visible = false;
        bindData();
    }
    
}
