<%@ Page Language="C#" AutoEventWireup="true" ValidateRequest="false" CodeFile="task_Question.aspx.cs"
    Inherits="Admin_task_Question" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="Notifications.ascx" TagName="Notifications" TagPrefix="uc4" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        Task question</title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        function checkChecked()
        {
            if(document.getElementById("chkShowAllHistory").checked==true)
            {
                document.getElementById("divNoHistory").style.display="none";
                document.getElementById("divAllHistory").style.display="block";       
            }
            else
            {
                document.getElementById("divNoHistory").style.display="block";
                document.getElementById("divAllHistory").style.display="none";       
            }
        }       
    </script>

    <script language="javascript" type="text/javascript">
    function GotoIncludeRead()
    {
  
    
     document.getElementById('btnIncluderead').click();
     return true;
     
    }
    
    </script>

    <script language="javascript" type="text/javascript">
    function GotoChaekAllread()
    {
      
     document.getElementById('btnCheckAllasRead').click();
     return true;
     
    }
    
    </script>

    <script language="javascript" type="text/javascript">
    function GotoChaekAllDel()
    {
  
    
     document.getElementById('btnCheckAllToDelete').click();
     return true;
     
    }
    
    </script>

    <script language="javascript" type="text/javascript">
        function window.confirm(str)
        {
            execScript('n = msgbox("'+str+'","4132")', "vbscript");
            return(n == 6);
        }
    </script>

    <script language="javascript" type="text/javascript">
        
    function checkedOkConfirm()
    {
   
    
    document.getElementById("hdnTocheckStatus").value = "True";
    document.getElementById('btnUpdate').click();
   return true;
    }
    function checkedCancelConfirm()
    {
    
    document.getElementById("hdnTocheckStatus").value = "False";
    document.getElementById('btnUpdate').click();
    return true;
    }
      function GetStatus()
      {
     
      if (document.getElementById("drpStatuses").value == "to check")
      {
       document.getElementById("hdnTocheckStatus").value = "False";
       return true;
      }
      else
      {
       
       document.getElementById("hdnTocheckStatus").value = "True";
      return true;
      }
      }
    </script>

    <script type="text/javascript">
        
        var MyArr1aAssignedUsers = new Array();
    
        function disableSubmit()
        {
            if (typeof(Page_ClientValidate)=='function')
            {
                if (Page_ClientValidate() == true)
                {      
                    if(!CheckStatus())
                    {
                        return false;
                    }
                                       
                    
                    if(!CheckETC())
                    {
                        return false;
                    }
                    
                  
                              
                    if(document.getElementById("drpPriorities").value == "1a - DO NOW")
                    {                     
                       if(document.getElementById("drpUsers").value == document.getElementById("lblAssignedTo").innerHTML)
                       {
                           if(document.getElementById("prevPriority").value != document.getElementById("drpPriorities").value)
                           {
                               if(!check1aAssigned())
                               {
                                    return false;
                               } 
                           } 
                       } 
                       else
                       {
                           if(!check1aAssigned())
                           {
                                return false;
                           } 
                       }                                                                   
                    } 
                    
                    
                    
                    if(document.getElementById("btnUpdate").value == "Create" && document.getElementById("drpProjects").value=="general")
                    {
                        return checkSubmit();
                    }
                    
         if(document.getElementById("drpStatuses").value == "to check")
            {
           
            //alert(document.getElementById("hdnTocheckStatus").value);
            if(document.getElementById("hdnTocheckStatus").value == "False")
            {
         
            document.getElementById('hiddenButton').click();
            return false;
            }
            else
            {
             return true;
            }
            
            }
                }
                else
                {
                    return true;
                }
            }
            else
            {
                if(!CheckStatus())
                {
                        return false;
                }
                
                if(!CheckETC())
                {
                    return false;
                }
                
         
                
                if(document.getElementById("drpPriorities").value == "1a - DO NOW")
                {                     
                   if(document.getElementById("drpUsers").value == document.getElementById("lblAssignedTo").innerHTML)
                   {
                       if(document.getElementById("prevPriority").value != document.getElementById("drpPriorities").value)
                       {
                           if(!check1aAssigned())
                           {
                                return false;
                           } 
                       } 
                   } 
                   else
                   {
                       if(!check1aAssigned())
                       {
                            return false;
                       } 
                   }                                                                   
                } 
               
                if(document.getElementById("btnUpdate").value == "Create" && document.getElementById("drpProjects").value=="general")
                {
                    return checkSubmit();
                }
                
           if(document.getElementById("drpStatuses").value == "to check")
            {
            
            
            if(document.getElementById("hdnTocheckStatus").value == "False")
            {
         
            document.getElementById('hiddenButton').click();
            return false;
            }
            else
            {
             return true;
            }
            }
                
            }
        }
        
       
        function checkSubmit()
        {
            if(confirm("Can you be more specific with the Project type?"))
             return  false;
            else
             return true;
        }
        
       
        
        function check1aAssigned()
        {
            var i = 0;
            
            for(i=0;i<MyArr1aAssignedUsers.length;i++)
            {
                if (MyArr1aAssignedUsers[i] == document.getElementById("drpUsers").value)
                {
                    if(confirm(document.getElementById("drpUsers").value + " already has a 'Highest' task assigned. Sure you want to add another?"))
                        return true;
                    else
                        return false;                        
                }
            }                        
            return true;
        }      
        
        function CheckStatus()
        {
            if(document.getElementById("drpStatuses").value == "CWO" && document.getElementById("hdnStatusFlag").value =="true")
            {
                alert("You already have task assigned status 'CWO'. Please ensure only one task always has assigned status: �Currently working on�.");
                return false;
            }
           
            else
            {
                return true;
            }            
        }    
        
        
       
        
        function CheckETC()
        {
            if(document.getElementById("drpHrs").value == "0" && document.getElementById("drpMins").value =="0" && document.getElementById("drpStatuses").value == "CWO")
            {
                alert("Please select estimated time to completion.");
                return false;
            }
            else
            {
                return true;
            }   
        }    
    </script>

    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
</head>
<body onload="checkChecked();">
    <form id="form1" runat="server">
    <div id="wrapper">
        <uc2:Admin_Header ID="Admin_Header1" runat="server" />
        <div id="Content">
            <div id="Content_98">
                <div class="titleText" style="margin: 10px 0 10px 0; display: none;">
                    Question&nbsp;|&nbsp;</div>
                <div style="float: left;">
                    <table width="100%">
                        <tr>
                            <td align="right">
                                <div>
                                <uc4:Notifications ID="Notifications" runat="server" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="width: 100%; background: #ebebeb; padding: 5px 0; float: left;">
                    &nbsp;&nbsp;<span class="text_default"><a id="LinkBacktoTask" runat="server" href="edit_task.aspx">Back
                        to task</a><a id="lnkBackToRequest" runat="server" visible="false" href="edit_request.aspx">&nbsp;|&nbsp;Back
                            to change request</a><a id="lnkCreateNewCR" href="add_changeRequest.aspx" runat="server">&nbsp;|&nbsp;Clone
                                change request</a></span><span style="float: right"><asp:Label ID="lblIRN" runat="server"></asp:Label>
                                    &nbsp;&nbsp;
                                    <asp:Button ID="LnkRelated" runat="server" Text="Project related" OnClick="LnkRelated_Click"
                                        CssClass="text_button_trans" /><asp:Button ID="Lnkcomprelated" runat="server" Text="Company related"
                                            BorderStyle="none" Style="cursor: pointer" OnClick="Lnkcomprelated_Click" CssClass="text_button_trans" />&nbsp;&nbsp;
                                            
                                            <%--=====================================================================================================================================================--%>
                                            <a href="#" id="CompanyNotes" runat="server" class="text_button_trans">FHS notes</a> &nbsp;                             
                                            <a href="#" id="CRNotes" runat="server" class="text_button_trans">Client notes</a> &nbsp;                             
                                            <%--<asp:Button ID="CompanyNotes" runat="server" Text="FHS notes" OnClick="CompanyNotes_Click" CausesValidation="False" CssClass="text_button_trans" />
                                            <asp:Button ID="CRNotes" runat="server" Text="Client notes" OnClick="CRNotes_Click" CausesValidation="False" CssClass="text_button_trans" />--%>
                                            <%--=====================================================================================================================================================--%>        
                                                    
                                                    <asp:Button ID="BtnAnalytics" runat="server" Text="Analytics"
                                                        CausesValidation="False" OnClick="BtnAnalytics_Click" CssClass="text_button_trans" /><asp:Button
                                                            ID="BtnRelatedTask" runat="server" Text="Task" CausesValidation="False" OnClick="BtnRelatedTask_Click"
                                                            CssClass="text_button_trans" /></span></div>
                <div style="clear: both">
                </div>
                <div id="divMessage" runat="server" style="color: Red; font-weight: bold">
                </div>
                <div id="DivEntry" runat="server">
                    <div id="divTask" runat="server" class="divBorder">
                        <table width="100%" cellpadding="5" cellspacing="0">
                            <tr style="background: #282C5F; color: #ffffff;">
                                <td align="left" valign="top" style="height: 18px; width:20%;">
                                    &nbsp;&nbsp;Task id:
                                    <asp:Label ID="lblTaskId" runat="server" Font-Bold="True"></asp:Label>
                                </td>
                                
                                 <td align="left" valign="top" style="height: 18px; width:30%;">
                                    <asp:Label ID="lblReportedBy" runat="server"></asp:Label>
                                </td>
                                
                                <td align="left" valign="top" style="height: 18px; width:30%;">
                                    <asp:Label ID="lblLastUpdated" runat="server" ForeColor="white"></asp:Label>
                                </td>
                                
                               
                                
                                <td align="right" valign="top" style="height: 18px; width:20%;">
                                    <asp:LinkButton ID="lnkNudge" runat="server" ForeColor="#FFFFFF" OnClick="lnkNudge_Click" Visible="false"
                                        CausesValidation="False">Nudge</asp:LinkButton>
                                    &nbsp; <a id="lnkHrsReport" runat="server" href="#" style="color:#FFF;" target="_blank" visible="false">
                                        Hours Report</a> &nbsp; <a id="lnkSubscribers" runat="server" href="#" style="color:#FFF;" target="_blank"
                                            visible="false">subscribers</a>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="4" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td width="113" style="height: 18px">
                                                Project:&nbsp;
                                            </td>
                                            <td style="height: 18px">
                                                <asp:Label ID="lblProject" runat="server" Font-Bold="True"></asp:Label>
                                            </td>
                                            <td style="width: 10px; height: 18px">
                                            </td>
                                            <td style="height: 18px">
                                                Assigned to:&nbsp;
                                            </td>
                                            <td style="height: 18px">
                                                <asp:Label ID="lblAssignedTo" runat="server" Font-Bold="True"></asp:Label>
                                            </td>
                                            <td style="width: 10px; height: 18px">
                                            </td>
                                            <td style="height: 18px">&nbsp;
                                                
                                            </td>
                                            <td style="height: 18px">&nbsp;
                                                
                                            </td>
                                            <td style="height: 18px">
                                            </td>
                                            <td style="height: 18px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 10px">
                                            </td>
                                            <td style="height: 10px">
                                            </td>
                                            <td style="height: 10px; width: 10px;">
                                            </td>
                                            <td style="height: 10px">
                                            </td>
                                            <td style="height: 10px">
                                            </td>
                                            <td style="height: 10px">
                                            </td>
                                            <td style="height: 10px">
                                            </td>
                                            <td style="height: 10px">
                                            </td>
                                            <td style="width: 10px; height: 10px">
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Change project:&nbsp;
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="drpProjects" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpProjects_SelectedIndexChanged"
                                                    Width="175px">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10px">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="drpProjects"
                                                    ErrorMessage="Project is required.">*</asp:RequiredFieldValidator>
                                            </td>
                                            <td>
                                                Question to:&nbsp;
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="drpUsers" runat="server" Width="175px">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10px">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="drpUsers"
                                                    ErrorMessage="Re-assign to employee is required.">*</asp:RequiredFieldValidator>
                                            </td>
                                            <td>
                                                Status:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="drpStatuses" runat="server" Width="175px">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="drpStatuses"
                                                    ErrorMessage="Status is required.">*</asp:RequiredFieldValidator>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblIRTN" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 18px">
                                            </td>
                                            <td style="height: 18px">
                                            </td>
                                            <td style="width: 10px; height: 18px">
                                            </td>
                                            <td style="height: 18px">
                                            </td>
                                            <td style="height: 18px">
                                            </td>
                                            <td style="width: 10px; height: 18px">
                                            </td>
                                            <td style="height: 18px">
                                            </td>
                                            <td style="height: 18px">
                                            </td>
                                            <td style="height: 18px">
                                            </td>
                                            <td style="height: 18px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 22px">
                                                Project type:
                                            </td>
                                            <td style="height: 22px">
                                                <asp:DropDownList ID="drpProjectType" runat="server" Width="175px">
                                                    <asp:ListItem></asp:ListItem>
                                                    <asp:ListItem>es</asp:ListItem>
                                                    <asp:ListItem Selected="True">other</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10px; height: 22px">
                                            </td>
                                            <td style="height: 22px">
                                                Category:
                                            </td>
                                            <td style="height: 22px">
                                                <asp:DropDownList ID="drpCategories" runat="server" Width="175px">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10px; height: 22px">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="drpCategories"
                                                    ErrorMessage="Category is required.">*</asp:RequiredFieldValidator>
                                            </td>
                                            <td style="height: 22px">
                                                Priority:
                                            </td>
                                            <td style="height: 22px">
                                                <asp:DropDownList ID="drpPriorities" runat="server" Width="175px">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="height: 22px">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="drpPriorities"
                                                    ErrorMessage="Priority is required.">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="4" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                    </table>
                                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator1">
                                    </cc1:ValidatorCalloutExtender>
                                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server" TargetControlID="RequiredFieldValidator3">
                                    </cc1:ValidatorCalloutExtender>
                                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="server" TargetControlID="RequiredFieldValidator4">
                                    </cc1:ValidatorCalloutExtender>
                                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="server" TargetControlID="RequiredFieldValidator5">
                                    </cc1:ValidatorCalloutExtender>
                                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="server" TargetControlID="RequiredFieldValidator6">
                                    </cc1:ValidatorCalloutExtender>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="4" valign="top">
                                    <asp:Label ID="lblUserName" runat="server" Visible="False"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="4" valign="top">
                                    <table>
                                        <tr>
                                            <td align="left" valign="top">
                                                Short description:
                                            </td>
                                            <td align="left" valign="top">
                                                <asp:TextBox ID="txtShortDescr" runat="server" Width="665px"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtShortDescr"
                                                    ErrorMessage="Short description is required.">*</asp:RequiredFieldValidator><cc1:ValidatorCalloutExtender
                                                        ID="ValidatorCalloutExtender2" runat="server" TargetControlID="RequiredFieldValidator2">
                                                    </cc1:ValidatorCalloutExtender>
                                            </td>
                                            <td align="left" style="width: 10px" valign="top">
                                            </td>
                                            <td align="left" valign="top" rowspan="2" style="width: 333px">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td>
                                                            <table border="0" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td style="padding-right: 5px">
                                                                        Estimated time to completion
                                                                    </td>
                                                                    <td style="padding-right: 5px" align="left">
                                                                        <asp:DropDownList ID="drpHrs" runat="server">
                                                                            <asp:ListItem Value="0">00</asp:ListItem>
                                                                            <asp:ListItem Value="1">01</asp:ListItem>
                                                                            <asp:ListItem Value="2">02</asp:ListItem>
                                                                            <asp:ListItem Value="3">03</asp:ListItem>
                                                                            <asp:ListItem Value="4">04</asp:ListItem>
                                                                            <asp:ListItem Value="5">05</asp:ListItem>
                                                                            <asp:ListItem Value="6">06</asp:ListItem>
                                                                            <asp:ListItem Value="7">07</asp:ListItem>
                                                                            <asp:ListItem Value="8">08</asp:ListItem>
                                                                            <asp:ListItem Value="9">09</asp:ListItem>
                                                                            <asp:ListItem Value="10">10</asp:ListItem>
                                                                            <asp:ListItem Value="11">11</asp:ListItem>
                                                                            <asp:ListItem Value="12">12</asp:ListItem>
                                                                            <asp:ListItem Value="13">13</asp:ListItem>
                                                                            <asp:ListItem Value="14">14</asp:ListItem>
                                                                            <asp:ListItem Value="15">15</asp:ListItem>
                                                                            <asp:ListItem Value="16">16</asp:ListItem>
                                                                            <asp:ListItem Value="17">17</asp:ListItem>
                                                                            <asp:ListItem Value="18">18</asp:ListItem>
                                                                            <asp:ListItem Value="19">19</asp:ListItem>
                                                                            <asp:ListItem Value="20">20</asp:ListItem>
                                                                            <asp:ListItem Value="21">21</asp:ListItem>
                                                                            <asp:ListItem Value="22">22</asp:ListItem>
                                                                            <asp:ListItem Value="23">23</asp:ListItem>
                                                                            <asp:ListItem Value="24">24</asp:ListItem>
                                                                            <asp:ListItem Value="25">25</asp:ListItem>
                                                                            <asp:ListItem Value="26">26</asp:ListItem>
                                                                            <asp:ListItem Value="27">27</asp:ListItem>
                                                                            <asp:ListItem Value="28">28</asp:ListItem>
                                                                            <asp:ListItem Value="29">29</asp:ListItem>
                                                                            <asp:ListItem Value="30">30</asp:ListItem>
                                                                            <asp:ListItem Value="31">31</asp:ListItem>
                                                                            <asp:ListItem Value="32">32</asp:ListItem>
                                                                            <asp:ListItem Value="33">33</asp:ListItem>
                                                                            <asp:ListItem Value="34">34</asp:ListItem>
                                                                            <asp:ListItem Value="35">35</asp:ListItem>
                                                                            <asp:ListItem Value="36">36</asp:ListItem>
                                                                            <asp:ListItem Value="37">37</asp:ListItem>
                                                                            <asp:ListItem Value="38">38</asp:ListItem>
                                                                            <asp:ListItem Value="39">39</asp:ListItem>
                                                                            <asp:ListItem Value="40">40</asp:ListItem>
                                                                            <asp:ListItem Value="41">41</asp:ListItem>
                                                                            <asp:ListItem Value="42">42</asp:ListItem>
                                                                            <asp:ListItem Value="43">43</asp:ListItem>
                                                                            <asp:ListItem Value="44">44</asp:ListItem>
                                                                            <asp:ListItem Value="45">45</asp:ListItem>
                                                                            <asp:ListItem Value="46">46</asp:ListItem>
                                                                            <asp:ListItem Value="47">47</asp:ListItem>
                                                                            <asp:ListItem Value="48">48</asp:ListItem>
                                                                            <asp:ListItem Value="49">49</asp:ListItem>
                                                                            <asp:ListItem Value="50">50</asp:ListItem>
                                                                            <asp:ListItem Value="51">51</asp:ListItem>
                                                                            <asp:ListItem Value="52">52</asp:ListItem>
                                                                            <asp:ListItem Value="53">53</asp:ListItem>
                                                                            <asp:ListItem Value="54">54</asp:ListItem>
                                                                            <asp:ListItem Value="55">55</asp:ListItem>
                                                                            <asp:ListItem Value="56">56</asp:ListItem>
                                                                            <asp:ListItem Value="57">57</asp:ListItem>
                                                                            <asp:ListItem Value="58">58</asp:ListItem>
                                                                            <asp:ListItem Value="59">59</asp:ListItem>
                                                                            <asp:ListItem Value="60">60</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        hrs
                                                                    </td>
                                                                    <td style="padding-right: 5px">
                                                                        <asp:DropDownList ID="drpMins" runat="server">
                                                                            <asp:ListItem Value="0">00</asp:ListItem>
                                                                            <asp:ListItem Value="5">05</asp:ListItem>
                                                                            <asp:ListItem Value="10">10</asp:ListItem>
                                                                            <asp:ListItem Value="15">15</asp:ListItem>
                                                                            <asp:ListItem Value="20">20</asp:ListItem>
                                                                            <asp:ListItem Value="25">25</asp:ListItem>
                                                                            <asp:ListItem Value="30">30</asp:ListItem>
                                                                            <asp:ListItem Value="35">35</asp:ListItem>
                                                                            <asp:ListItem Value="40">40</asp:ListItem>
                                                                            <asp:ListItem Value="45">45</asp:ListItem>
                                                                            <asp:ListItem Value="50">50</asp:ListItem>
                                                                            <asp:ListItem Value="55">55</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        mins
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 10px">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblTotalHrs" runat="server" Font-Bold="False"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 10px">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 18px">
                                                            <asp:Label ID="lblHrsLeft" runat="server" Font-Bold="False"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" style="height: 49px">
                                                Relevant URL:
                                            </td>
                                            <td align="left" valign="top" style="height: 49px">
                                                <asp:TextBox ID="txtRelevantURL" runat="server" Width="665px" Rows="2" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                            <td align="left" valign="top" style="height: 49px">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top" style="height: 22px">
                                    Comment: <a href="#" id="A1" onclick="window.open('Task_Canned_message.aspx','','status=yes,width=760,height=560')">
                                        Canned message&nbsp;</a>
                                </td>
                                <td align="left" valign="top" style="height: 22px">&nbsp;
                                    
                                </td>
                                <td align="left" valign="top" style="height: 22px; width: 10px;">
                                </td>
                                <td align="left" valign="top" style="height: 22px">
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="4" valign="top">
                                <%--**********************************************************************************************************--%>
                                    <%--<FTB:FreeTextBox ID="ftbComment" runat="server" Height="200px" Width="100%"></FTB:FreeTextBox>--%>
                                <telerik:RadEditor ID="ftbComment" runat="server" Height="300px" Width="100%">
                                        <Snippets>
                                            <telerik:EditorSnippet Name="Product Template" Value="<table><tr><td><div style='width:300px;border:2px outset #D8D2BD;padding:34px;font-family:Arial, Verdana; font-size:11px;background- color:#F1EFE6;'>Dear ____________________,<br>Thank you for inquiring about ____________. Your request will be processed in 48 hours and shipped at the address you have provided.<b>Please, contact us if you have any problems.</b></div></td></tr></table>">            
                                            </telerik:EditorSnippet>
                                            <telerik:EditorSnippet Name="Signature" Value="<strong>John Smith</strong><br>Sales Manager<br>">
                                            </telerik:EditorSnippet>
                                        </Snippets>
                                        <Links>
                                            <telerik:EditorLink Name="Telerik" Href="http://www.telerik.com">
                                                <telerik:EditorLink Name="Telerik Products" Href="http://www.telerik.com/products"
                                                    Target="_blank" />
                                                <telerik:EditorLink Name="Telerik Support Center" Href="http://www.telerik.com/support/home.aspx"
                                                    Target="_blank" />
                                                <telerik:EditorLink Name="Telerik Community" Href="http://www.telerik.com/community/home.aspx"
                                                    Target="_blank" ToolTip="Telerik Community" />
                                            </telerik:EditorLink>
                                        </Links>
                                        <Tools>
                                        </Tools>
                                        <ImageManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                        <MediaManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                        <FlashManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                        <TemplateManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                        <DocumentManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                        <Modules>
                                            <telerik:EditorModule Name="RadEditorStatistics" Visible="False" Enabled="true" />
                                            <telerik:EditorModule Name="RadEditorDomInspector" Visible="False" Enabled="true" />
                                            <telerik:EditorModule Name="RadEditorNodeInspector" Visible="False" Enabled="true" />
                                            <telerik:EditorModule Name="RadEditorHtmlInspector" Visible="False" Enabled="true" />
                                        </Modules>
                                        <Content>
                                        </Content>
                                   </telerik:RadEditor>
                                <%--**********************************************************************************************************--%>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top" colspan="3" style="height: 42px; width:50%">
                                    <div id="divAttachments" runat="server">
                                        <a id="lnkSendAttachment" href="#" runat="server">Upload attachment</a> &nbsp;&nbsp
                                        <a id="lnkViewAttachment" href="#" runat="server">View attachments</a>&nbsp;
                                        <asp:CheckBox ID="chkShowAllHistory" runat="server" Text="Show all history" />
                                    </div>
                                    <div id="divMessage1" runat="server" style="color: Red">
                                    </div>
                                </td>
                                <td align="right" valign="top" style="height: 42px; width:50%;">
                                    <asp:CheckBox ID="chkEmailnotify" runat="server" Font-Bold="True" Checked="true"
                                        Text="Email notify" />
                                    &nbsp;&nbsp;&nbsp; &nbsp;<asp:CheckBox ID="chkQuestion" runat="server" Font-Bold="True"
                                        Text="as a question" Checked="True" />
                                    &nbsp;&nbsp;
                                    <asp:Button ID="btnUpdate" runat="server" Text="Update" OnClientClick="return disableSubmit();"
                                        OnClick="btnUpdate_Click" />&nbsp;
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" OnClick="btnDelete_Click"
                                        CausesValidation="False" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top" colspan="4">
                                </td>
                            </tr>
                        </table>
                    </div>
                    <br />
                    <div id="divTaskDetails" runat="server">
                        <div id="divNoHistory" runat="server">
                            <table width="100%" style="background-color:#EBEBEB">
                                <tr>
                                    <td align="right">
                                        <table width="100%">
                                            <tr>
                                                <td align="left">
                                                    <asp:CheckBox ID="CheckAllasRead" runat="server" Text="Check all as read" Font-Bold="true"
                                                        AutoPostBack="True" />&nbsp;&nbsp;
                                                    <asp:CheckBox ID="CheckAllToDelete" runat="server" Text="Check all to delete" Font-Bold="true"
                                                        AutoPostBack="true" />
                                                    &nbsp;&nbsp;<asp:Button ID="BtnDelAllCmnt" runat="server" Text="Delete comment" Width="140px"
                                                        OnClick="BtnDelAllCmnt_Click" />
                                                </td>
                                                <td align="right">
                                                    <asp:CheckBox ID="ChkIncludeRead" runat="server" Text="Include read comments" Font-Bold="true"
                                                        Checked="true" AutoPostBack="True" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                                            BorderWidth="0px" GridLines="None" OnItemCommand="DataGrid1_ItemCommand" OnItemDataBound="DataGrid1_ItemDataBound"
                                            ShowHeader="False" Width="100%">
                                            <Columns>
                                                <asp:BoundColumn DataField="task_id" Visible="False"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="tc_id" Visible="False"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="username" Visible="False"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="post_date" Visible="False"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="comment" Visible="False"></asp:BoundColumn>
                                                <asp:TemplateColumn>
                                                    <ItemTemplate>
                                                        <div style="border: solid 1px green; margin-bottom: 2px">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td align="left">
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Label ID="lblPosted" runat="server"></asp:Label>
                                                                                    &nbsp;
                                                                                    <asp:LinkButton ID="lnkReadMark" runat="server" CommandName="ReadMark">(Unread. Mark as read)</asp:LinkButton>&nbsp;<asp:CheckBox
                                                                                        ID="chkSelect" runat="server" />
                                                                                </td>
                                                                                <td align="right">
                                                                                    <asp:Button ID="lnkEdit" runat="server" CausesValidation="False" BackColor="transparent"
                                                                                        BorderStyle="none" ForeColor="blue" CommandName="edit" Style="cursor: pointer"
                                                                                        Text="edit" />
                                                                                    <asp:Button ID="lnkDelete" runat="server" CausesValidation="False" BackColor="transparent"
                                                                                        BorderStyle="none" ForeColor="blue" CommandName="delete" Style="cursor: pointer"
                                                                                        Text="delete" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblComment" runat="server" Text='<%# bind("comment") %>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:BoundColumn DataField="qflag" HeaderText="qflag" Visible="False"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="QuesTo" Visible="False"></asp:BoundColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="divAllHistory" runat="server">
                            <asp:DataGrid ID="DataGrid2" runat="server" AutoGenerateColumns="False" Width="100%"
                                ShowHeader="False" OnItemDataBound="DataGrid2_ItemDataBound" OnItemCommand="DataGrid2_ItemCommand"
                                BorderStyle="None" GridLines="None">
                                <Columns>
                                    <asp:BoundColumn DataField="task_id" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="tc_id" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="username" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="post_date" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="comment" Visible="False"></asp:BoundColumn>
                                    <asp:TemplateColumn>
                                        <ItemTemplate>
                                            <div style="border: solid 1px green; margin-bottom: 2px">
                                                <table width="100%">
                                                    <tr>
                                                        <td align="left">
                                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblPosted" runat="server" ForeColor="Green"></asp:Label>
                                                                        &nbsp;
                                                                        <asp:LinkButton ID="lnkReadMark" runat="server" CommandName="ReadMark">(Unread. Mark as read)</asp:LinkButton>
                                                                    </td>
                                                                    <td align="right">
                                                                        <asp:LinkButton ID="lnkEdit" runat="server" CausesValidation="False" CommandName="edit">edit</asp:LinkButton>&nbsp;
                                                                        <asp:LinkButton ID="lnkDelete" runat="server" CausesValidation="False" CommandName="delete">delete</asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <asp:Label ID="lblComment" runat="server" Text='<%# bind("comment") %>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="deleted" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="temp" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="qflag" HeaderText="qflag" Visible="False"></asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="prevProject" runat="server" />
        <asp:HiddenField ID="TaskProject" runat="server" />
        <asp:HiddenField ID="prevAssignedTo" runat="server" />
        <asp:HiddenField ID="prevStatus" runat="server" />
        <asp:HiddenField ID="prevProjectType" runat="server" />
        <asp:HiddenField ID="prevCategory" runat="server" />
        <asp:HiddenField ID="prevPriority" runat="server" />
        <asp:HiddenField ID="prevRelevantUrl" runat="server" />
        <asp:HiddenField ID="prevShortDescr" runat="server" />
        <asp:HiddenField ID="prevETC" runat="server" />
        <asp:HiddenField ID="hdnStatusFlag" runat="server" />
        <asp:HiddenField ID="hdnProjectCompany" runat="server" />
        <asp:HiddenField ID="hdnTocheckStatus" runat="server" />
        <asp:Button ID="btnIncluderead" OnClick="btnIncluderead_Click" runat="server" CausesValidation="False"
            BorderStyle="none" BackColor="transparent" Text=""></asp:Button><asp:Button ID="btnCheckAllasRead"
                OnClick="btnCheckAllasRead_Click" runat="server" CausesValidation="False" BorderStyle="none"
                BackColor="transparent" Text=""></asp:Button><asp:Button ID="btnCheckAllToDelete"
                    OnClick="btnCheckAllToDelete_Click" runat="server" CausesValidation="False" BorderStyle="none"
                    BackColor="transparent" Text=""></asp:Button>
    </div>
    <div id="hiddenDiv" style="display: none; visibility: hidden;">
        <asp:Button runat="server" ID="hiddenButton" />
    </div>
    <asp:Panel ID="Panalconfirm" runat="server" Width="700px" Style="display: none; background-color: White">
        <div>
            <table style="background-color: White">
                <tr style="background-color: White">
                    <td align="left" valign="top" colspan="4">
                        <FTB:FreeTextBox ID="FtbResources" runat="server" Height="300px" Width="700px" AutoGenerateToolbarsFromString="False">
                        </FTB:FreeTextBox>
                    </td>
                </tr>
                <tr style="background-color: White">
                    <td align="center" valign="top" style="background-color: White" colspan="4">
                        <asp:Button ID="btnOK" runat="server" Text="OK" />
                        &nbsp;
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalBackground"
        DropShadow="true" PopupControlID="Panalconfirm" TargetControlID="hiddenButton">
    </cc1:ModalPopupExtender>
    <uc1:footer ID="Footer1" runat="server" />
    </form>
</body>
</html>
