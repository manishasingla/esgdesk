<%@ Page Language="C#" AutoEventWireup="true" CodeFile="task_attachments.aspx.cs"
    Inherits="Admin_task_attachments" %>

<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc3" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc4" %>
<%@ Register Assembly="FlashUpload" Namespace="FlashUpload" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        Task attachments</title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        function disableSendButton()
        {            
            document.getElementById("btnSend1").disabled=true;
            document.getElementById("divMessage").innerHTML ="Please wait: your file is being sent...";
            document.getElementById("btnSend").click();
            return false;
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <uc3:Admin_Header ID="Admin_Header1" runat="server" />
        <div id="Content">
            <div class="titleText" style="margin: 10px 0 10px 0;">
                View attachment&nbsp;|&nbsp;<span id="divBackLink" runat="server" class="text_default"></span></div>
            <div id="divMessage" runat="server" style="color: Red; font-weight: bold;">
            </div>
            <div id="divAttachment" runat="server" style="text-align: left">
                <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" CellPadding="5"
                    AllowPaging="True" AllowSorting="True" OnSortCommand="DataGrid1_SortCommand"
                    OnDeleteCommand="DataGrid1_DeleteCommand" OnItemDataBound="DataGrid1_ItemDataBound"
                    OnPageIndexChanged="DataGrid1_PageIndexChanged" PageSize="100" Width="100%">
                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Top" />
                    <HeaderStyle BackColor="#282C5F" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                        ForeColor="#ffffff" Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                    <Columns>
                        <asp:BoundColumn DataField="Aid" HeaderText="Aid" Visible="False"></asp:BoundColumn>
                        <asp:BoundColumn DataField="task_id" HeaderText="task_id" Visible="False"></asp:BoundColumn>
                        <asp:BoundColumn DataField="FileName1" HeaderText="FileName1" Visible="False"></asp:BoundColumn>
                        <asp:BoundColumn DataField="FileName" HeaderText="File name" SortExpression="FileName">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Description" HeaderText="Description" SortExpression="Description">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="UploadedBy" HeaderText="Uploaded by" SortExpression="UploadedBy">
                        </asp:BoundColumn>
                       <%--===================================================================================================--%>
                       <%-- <asp:BoundColumn DataField="UploadedOn" HeaderText="Uploaded on" SortExpression="UploadedOn">
                        </asp:BoundColumn>--%>
                        <%--===================================================================================================--%>
                        <asp:TemplateColumn HeaderText="UploadedOn" SortExpression="UploadedOn">
                        <ItemTemplate>
                        <asp:Label ID="lbldate" runat="server" Text='<%# Bind("UploadedOn","{0:dd/MM/yyyy HH:mm:ss}") %>' ></asp:Label>
                        </ItemTemplate> 
                        </asp:TemplateColumn>
                        <%--============================================================================================================ --%>
                        <asp:HyperLinkColumn HeaderText="Download" Text="download" DataNavigateUrlField="Aid"
                            DataNavigateUrlFormatString="../setup2.aspx?file={0}" NavigateUrl="../setup2.aspx"
                            Target="_blank">
                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                Font-Underline="False" HorizontalAlign="Center" />
                        </asp:HyperLinkColumn>
                        <asp:ButtonColumn CommandName="Delete" HeaderText="Delete" Text="delete">
                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                Font-Underline="False" HorizontalAlign="Center" />
                        </asp:ButtonColumn>
                    </Columns>
                    <PagerStyle NextPageText="Next" PrevPageText="Prev" />
                </asp:DataGrid>
            </div>
        </div>
        <asp:Label ID="lblOrderBy" runat="server" Visible="false" Text=""></asp:Label>
    </div>
    <uc4:footer ID="Footer2" runat="server" />
    </form>
</body>
</html>
