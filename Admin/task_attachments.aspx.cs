using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Admin_task_attachments : System.Web.UI.Page
{
    int id;
    string sql = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = (Request.QueryString["id"] == null || Request.QueryString["id"].ToString() == "" ? "tasks.aspx" : "task_attachments.aspx?id=" + Request.QueryString["id"].ToString());
            Response.Redirect("login.aspx");
            return;
        }

        string task_id = Request["id"];
        if (task_id == null || task_id == "0" || task_id == "")
        {
            id = 0;
        }
        else
        {
            if (DatabaseHelper.is_int(task_id))
            {
                id = Convert.ToInt32(task_id);
            }
            else
            {
                // Display an error because the bugid must be an integer
                divMessage.InnerHtml = "<br>Task ID must be an integer.";
                divMessage.InnerHtml += "<p><a href='tasks.aspx'>View tasks</a></p>";
                return;
            }
        }

        divMessage.InnerHtml = "";
        if (!Page.IsPostBack)
        {
            sql = "select * from tasks where deleted <> 1 and task_id = " + id.ToString();
            
            DataSet dsTask = DatabaseHelper.getDataset(sql);

            if (dsTask == null || dsTask.Tables.Count <= 0 || dsTask.Tables[0].Rows.Count <= 0)
            {
                divMessage.InnerHtml = "<br>Task not found.<br><br><a href='tasks.aspx'>View tasks</a>";
                divAttachment.Visible = false;
            }
            else
            {
                divAttachment.Visible = true;
                divBackLink.InnerHtml = "<a href=\"edit_task.aspx?id=" + id.ToString() + "\">back to task</a>";
                lblOrderBy.Text = " ORDER BY Aid desc";
                BindData(id);
            }

            if (!DatabaseHelper.can_Delete_Comments_Attachments(Session["admin"].ToString()))
            {
                DataGrid1.Columns[8].Visible = false;
            }
        }                
    }

    public void BindData(int task_id)
    {
        DataSet dsAttachments = DatabaseHelper.getTaskAttachments(task_id, lblOrderBy.Text);

        if (dsAttachments == null || dsAttachments.Tables.Count <= 0 || dsAttachments.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "<br>Attachment not found.";
            divAttachment.Visible = false;
        }
        else
        {
            DataGrid1.DataSource = dsAttachments.Tables[0];
            DataGrid1.DataBind();

            if (dsAttachments.Tables[0].Rows.Count <= 100)
            {
                DataGrid1.PagerStyle.Visible = false;
            }
            else
            {
                DataGrid1.PagerStyle.Visible = false;
            }
        }
    }

    protected void DataGrid1_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        if (e.SortExpression.ToString() == Session["Column"])
        {
            //Reverse the sort order
            if (Session["Order"] == "ASC")
            {
                lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " DESC";
                Session["Order"] = "DESC";
            }
            else
            {
                lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " ASC";
                Session["Order"] = "ASC";
            }
        }
        else
        {
            //Different column selected, so default to ascending order
            lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " ASC";
            Session["Order"] = "ASC";
        }

        Session["Column"] = e.SortExpression.ToString();
        BindData(id);
    }

    protected void DataGrid1_DeleteCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            object objResult = DatabaseHelper.deleteTaskAttachment(e.Item.Cells[0].Text);

            if (objResult.ToString() != "0")
            {
                string uploadPath1 = Server.MapPath(Request.ApplicationPath + "\\Attachment\\backup");

                uploadPath1 = uploadPath1 + "\\" + e.Item.Cells[2].Text;

                try
                {
                    if (System.IO.File.Exists(uploadPath1))
                    {
                        System.IO.File.Delete(uploadPath1);
                    }
                }
                catch (Exception ex)
                {

                }
                BindData(id);
            }
            else
            {
                divMessage.InnerHtml = "There was problem in deleting attachment. Please try again.";
                divMessage.Visible = true;
                BindData(id);
            }
        }
    }

    protected void DataGrid1_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        DataGrid1.CurrentPageIndex = e.NewPageIndex;
        BindData(id);
    }

    protected void DataGrid1_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //Assume  the first column of DataGrid1 is the Delete ButtonColumn
            LinkButton deleteButton = (LinkButton)e.Item.Cells[8].Controls[0];

            //We can now add the onclick event handler
            deleteButton.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete this file?')");
        }
    }
}
