﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="tasks1.aspx.cs" Inherits="Admin_Demo" %>

<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Src="Notifications.ascx" TagName="Notifications" TagPrefix="uc4" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        <%= ConfigurationManager.AppSettings["CompanyName"].ToString()%></title>
    <%--<meta http-equiv="refresh" content="1200" />--%>
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        function window.confirm(str) {
            execScript('n = msgbox("' + str + '","4132")', "vbscript");
            return (n == 6);
        }
        function opacity() {

        }
       
    </script>

    <script type="text/javascript">
        function get_(div_) {

            div_ = div_.id + "1";

            document.getElementById(div_).style.display = "block";
        }
        function get_1(div_) {

            div_ = div_.id + "1";

            document.getElementById(div_).style.display = "none";
        }
    </script>

    <script type="text/javascript" language="javascript">
        function EnablePRlink() {
            var objPR = document.getElementById("DrpMultipleTaskPR").value;

            if (objPR != "0") {

                document.getElementById("LnkBtnToChngPR").disabled = false;

                // document.getElementById("LnkChngAssingedTo").disabled = "disabled";
                document.getElementById("LnkChngStatus").disabled = "disabled";
                document.getElementById("LnkChngAssingedTo").disabled = "disabled";
                document.getElementById("DrpMultipleTaskAssing").value = "0";
                document.getElementById("DrpMultipleTaskStatus").value = "0";

            }
            else {

                document.getElementById("LnkBtnToChngPR").disabled = "disabled";

                document.getElementById("LnkChngStatus").disabled = "disabled";
                document.getElementById("LnkChngAssingedTo").disabled = "disabled";
                document.getElementById("DrpMultipleTaskAssing").value = "0";
                document.getElementById("DrpMultipleTaskStatus").value = "0";
                document.getElementById("DrpMultipleTaskPR").value = "0";
            }

            return true;

        }


        function EnableAssinglink() {

            var objAssing = document.getElementById("DrpMultipleTaskAssing").value;

            if (objAssing != "0") {

                document.getElementById("LnkBtnToChngPR").disabled = "disabled";
                document.getElementById("LnkChngAssingedTo").disabled = false;
                document.getElementById("LnkChngStatus").disabled = "disabled";
                ///document.getElementById("DrpMultipleTaskAssing").value = "0";
                document.getElementById("DrpMultipleTaskStatus").value = "0";
                document.getElementById("DrpMultipleTaskPR").value = "0";
                ///document.getElementById("LnkChngAssingedTo").disabled = "disabled";
            }
            else {

                document.getElementById("LnkBtnToChngPR").disabled = "disabled";
                document.getElementById("LnkChngAssingedTo").disabled = "disabled";
                document.getElementById("LnkChngStatus").disabled = "disabled";
                document.getElementById("DrpMultipleTaskAssing").value = "0";
                document.getElementById("DrpMultipleTaskStatus").value = "0";
                document.getElementById("DrpMultipleTaskPR").value = "0";

            }

            return;
        }

        function EnableStatuslink() {
            var objStatus = document.getElementById("DrpMultipleTaskStatus").value;

            if (objStatus != "0") {

                document.getElementById("LnkBtnToChngPR").disabled = "disabled";
                document.getElementById("LnkChngAssingedTo").disabled = "disabled";
                document.getElementById("LnkChngStatus").disabled = false;
                document.getElementById("DrpMultipleTaskAssing").value = "0";
                ///document.getElementById("DrpMultipleTaskStatus").value = "0";
                document.getElementById("DrpMultipleTaskPR").value = "0";

            }
            else {

                document.getElementById("LnkBtnToChngPR").disabled = "disabled";
                document.getElementById("LnkChngAssingedTo").disabled = "disabled";
                document.getElementById("LnkChngStatus").disabled = "disabled";
                document.getElementById("DrpMultipleTaskAssing").value = "0";
                document.getElementById("DrpMultipleTaskStatus").value = "0";
                document.getElementById("DrpMultipleTaskPR").value = "0";

            }
            return;

        }
    </script>

    <link rel="stylesheet" type="text/css" href="slider.css" />

    <script type="text/javascript" src="slider.js"></script>

    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <uc2:Admin_Header ID="Admin_Header1" runat="server" />
        <div id="Content">
            <asp:UpdateProgress ID="updateprogress1" runat="server">
                <ProgressTemplate>
                    <div id="processmessage" class="processMessage">
                        <img alt="loading" src="images/ajax-loader.gif" style="width: 80px; height: 80px" />
                        <span style="color: black; font-weight: bold; font-size: 18px; vertical-align: super;">
                            Please wait... </span>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div id="Content_98">
                        <%--<div id="pageTitle">
                Tasks<span style="padding-left: 885px;"></span> <a href="http://www.esgdesk.com//Download/ESG_Desk.application"
                    style="color: white; font-size: 15px; text-decoration: bold; cursor: pointer;"
                    onmouseover="this.style.color='blue'" onmouseout="this.style.color='#ffffff'">Click
                    here to download the desktop application</a></div>--%>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="left" valign="top" style="display: none;">
                                    <div style="height: 40px; vertical-align: middle; line-height: 40px; margin: 20px 0 0 0;">
                                        <div class="titleText" style="float: left;">
                                            Tasks</div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <div style="padding: 0px 0 0px 5px; margin: 2px  0 -2px 0;">
                                        <table>
                                            <tr>
                                                <td id="s1" style="background: url(../images/yellowbar_filter.png) no-repeat left top;
                                                    width: 101px; height: 27px; text-align: center;">
                                                    <span class="link" onmousedown="slideContent('section-1')">Filter</span>
                                                </td>
                                                <td id="s2" style="background: url(../images/yellowbar_filter.png) no-repeat left top;
                                                    width: 101px; height: 27px; text-align: center;">
                                                    <span class="link" onmousedown="slideContent('section-2')">Action</span>
                                                </td>
                                                <td>
                                                    <span id="TopFilterLable" runat="server" class="topfilter"></span>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div>
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" onclick="return TABLE1_onclick()"
                                            id="TABLE1">
                                            <tr>
                                                <td align="right" valign="bottom">
                                                    <img src="../images/filter_corner_l.png" width="11" height="59" />
                                                </td>
                                                <td bgcolor="#282C5F" width="100%">
                                                    <div class="slider">
                                                        <div class="slidercontent" id="slider">
                                                            <div id="section-1" class="section upper" style="margin-top: 0px;">
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td width="45" valign="top" style="padding-top: 20px;">
                                                                            <b style="color: #e1ac10;">Filter:</b>
                                                                        </td>
                                                                        <td valign="top" class="whitetext2" width="9%">
                                                                            <br />
                                                                            <span class="whitetext2">
                                                                                <asp:DropDownList ID="drpFilter" runat="server" AutoPostBack="True" Width="96%" OnSelectedIndexChanged="drpFilter_SelectedIndexChanged"
                                                                                    CssClass="filerDrpodown">
                                                                                </asp:DropDownList>
                                                                            </span>
                                                                        </td>
                                                                        <td width="9%" valign="top" style="color: White; font-size: 14px">
                                                                            Project<br />
                                                                            <span class="whitetext2"><span class="whitetext2"><span class="whitetext2">
                                                                                <asp:DropDownList ID="drpProjects" runat="server" CssClass="filerDrpodown" AutoPostBack="True"
                                                                                    OnSelectedIndexChanged="drpProjects_SelectedIndexChanged" Width="96%">
                                                                                </asp:DropDownList>
                                                                            </span></span></span>
                                                                        </td>
                                                                        <td width="9%" valign="top" style="color: White; font-size: 14px">
                                                                            Category<br />
                                                                            <asp:DropDownList ID="drpCategories" runat="server" CssClass="filerDrpodown" AutoPostBack="True"
                                                                                OnSelectedIndexChanged="drpCategories_SelectedIndexChanged" Width="96%">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td width="9%" valign="top" style="color: White; font-size: 14px">
                                                                            Reported by<br />
                                                                            <asp:DropDownList ID="drpReportedBy" runat="server" CssClass="filerDrpodown" AutoPostBack="True"
                                                                                OnSelectedIndexChanged="drpReportedBy_SelectedIndexChanged" Width="96%">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td width="9%" valign="top" style="color: White; font-size: 14px">
                                                                            Priority<br />
                                                                            <asp:DropDownList ID="drpPriorities" runat="server" CssClass="filerDrpodown" AutoPostBack="True"
                                                                                OnSelectedIndexChanged="drpPriorities_SelectedIndexChanged" Width="96%">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td id="tdAssignedTo" runat="server" width="9%" valign="top" style="color: White;
                                                                            font-size: 14px">
                                                                            Assigned to<br />
                                                                            <span class="whitetext2">
                                                                                <asp:DropDownList ID="drpAssignedTo" runat="server" CssClass="filerDrpodown" AutoPostBack="True"
                                                                                    OnSelectedIndexChanged="drpAssignedTo_SelectedIndexChanged" Width="96%">
                                                                                </asp:DropDownList>
                                                                            </span>
                                                                        </td>
                                                                        <td width="9%" valign="top" style="color: White; font-size: 14px">
                                                                            Status<br />
                                                                            <asp:DropDownList ID="drpStatuses" runat="server" CssClass="filerDrpodown" AutoPostBack="True"
                                                                                OnSelectedIndexChanged="drpStatuses_SelectedIndexChanged" Width="96%">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td valign="top" width="85" style="padding-top: 20px">
                                                                            <asp:Button ID="btnClearFilters" runat="server" OnClick="btnClearFilters_Click" Text="Clear filters"
                                                                                CssClass="clearfilter" />
                                                                        </td>
                                                                        <td valign="top" class="whitetext2">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td valign="top" class="whitetext2" style="min-width: 140px; color: White; font-size: 14px">
                                                                            <asp:CheckBox ID="chkViewDeleted" runat="server" Text="View only deleted" AutoPostBack="True"
                                                                                OnCheckedChanged="chkViewDeleted_CheckedChanged" /><br />
                                                                            <asp:CheckBox ID="chkHideClosed" runat="server" Text="View closed" AutoPostBack="True"
                                                                                OnCheckedChanged="chkHideClosed_CheckedChanged" />
                                                                        </td>
                                                                        <td valign="top" class="whitetext2" style="min-width: 170px; color: White; font-size: 14px">
                                                                            <asp:CheckBox ID="ChkReadTask" runat="server" Text="Include read&nbsp;&nbsp;" Checked="true"
                                                                                AutoPostBack="True" OnCheckedChanged="ChkReadTask_CheckedChanged" /><br />
                                                                            <span style="color: #E1AC10; float: right;"><b>Records: </b>
                                                                                <asp:Label ID="LblRecordsno" runat="server" Text=""></asp:Label></span>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <div id="section-2" class="section upper" style="padding-top: 5px;">
                                                                <div id="DivMiltiselection" runat="server" style="color: White; font-size: 14px">
                                                                    <b style="color: #e1ac10;">Action:</b>
                                                                    <asp:LinkButton ID="LnkBtnToDeleteAll" runat="server" OnClick="LnkBtnToDeleteAll_Click"
                                                                        CssClass="whitetext2">Delete selected</asp:LinkButton>&nbsp;&nbsp;
                                                                    <asp:LinkButton ID="LnkBtnToMarkAllRead" runat="server" OnClick="LnkBtnToMarkAllRead_Click"
                                                                        CssClass="whitetext2">Mark read</asp:LinkButton><br />
                                                                    <span style="float: left;"><span style="color: Red;">&nbsp;</span></span><asp:Button
                                                                        ID="LnkBtnToChngPR" runat="server" BackColor="transparent" BorderStyle="none"
                                                                        Text="Change priority to" OnClick="LnkBtnToChngPR_Click" Style="color: White;
                                                                        font-size: 14px; margin-left: -15px" />
                                                                    <asp:DropDownList ID="DrpMultipleTaskPR" runat="server" CssClass="filerDrpodown"
                                                                        AutoPostBack="true" Width="180px">
                                                                    </asp:DropDownList>
                                                                    &nbsp;&nbsp;<asp:Button ID="LnkChngAssingedTo" runat="server" BackColor="transparent"
                                                                        BorderStyle="none" Text="Change assigned to" OnClick="LnkChngAssingedTo_Click"
                                                                        Style="color: White; font-size: 14px" />
                                                                    <asp:DropDownList ID="DrpMultipleTaskAssing" runat="server" CssClass="filerDrpodown"
                                                                        AutoPostBack="false" Width="180px">
                                                                    </asp:DropDownList>
                                                                    &nbsp;&nbsp;<asp:Button ID="LnkChngStatus" runat="server" BackColor="transparent"
                                                                        BorderStyle="none" Text="Change status to" OnClick="LnkChngStatus_Click" Style="color: White;
                                                                        font-size: 14px" />
                                                                    <asp:DropDownList ID="DrpMultipleTaskStatus" runat="server" CssClass="filerDrpodown"
                                                                        AutoPostBack="false" Width="180px">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td valign="bottom">
                                                    <img src="../images/filter_corner_r.png" width="11" height="59" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top" style="height: 4px">
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <span style="color: Black; font-size: 16px">
                                        <asp:LinkButton ID="LnkNotification" runat="server" ForeColor="red" OnClick="LnkNotification_Click"></asp:LinkButton>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <uc4:Notifications ID="Notifications" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top" style="height: 3px">
                                </td>
                            </tr>
                        </table>
                        <br />
                        <div id="divMessage" runat="server" style="color: Red; font-weight: bold;">
                        </div>
                        <div>
                            <asp:GridView ID="DataGrid1" runat="server" Width="100%" AutoGenerateColumns="False"
                                AllowPaging="True" AllowSorting="True" CellPadding="5" PageSize="20" OnItemCommand="DataGrid1_ItemCommand"
                                OnSelectedIndexChanged="DataGrid1_SelectedIndexChanged" BorderColor="#cccccc"
                                OnPageIndexChanging="DataGrid1_PageIndexChanging" OnRowDataBound="DataGrid1_RowDataBound"
                                OnRowCommand="DataGrid1_RowCommand" OnSorting="DataGrid1_Sorting">
                                <PagerSettings Mode="NextPrevious" FirstPageText="First" PreviousPageText="Prev"
                                    NextPageText="Next" LastPageText="Last" />
                                <HeaderStyle Font-Bold="false" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Top" BackColor="#282c5f"
                                    CssClass="tblTitle1" />
                                <Columns>
                                    <asp:BoundField DataField="task_id" HeaderText="ID" Visible="False" SortExpression="task_id" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" /></ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="short_desc" HeaderText="ShrtDesc" Visible="False" SortExpression="short_desc" />
                                    <asp:TemplateField HeaderText="Short description" SortExpression="short_desc">
                                        <ItemTemplate>
                                          <a  href="edit_task.aspx?id=<%# Eval("task_id")%>">
                                            <asp:Label Font-Size="12px" ID="lblshrtDesc" runat="server" Width="250px" Text='<%# bind("short_desc") %>'></asp:Label></a>
                                            <div id="lblshrtDesc1" runat="server" style="display: none; position: absolute; background-color: #FEFFB3;
                                                border: solid 1px #333333; width: 730px; margin-top: 5px; padding-left: 3px">
                                                <asp:Label ID="LblLastcomment" runat="server" Text='<%# bind("comment") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:HyperLinkField Text="(+)" Visible="False" DataNavigateUrlFields="task_id" NavigateUrl="edit_task.aspx"
                                        DataNavigateUrlFormatString="edit_task.aspx?id={0}" SortExpression="short_desc">
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" Font-Size="13px" />
                                    </asp:HyperLinkField>
                                    <asp:TemplateField HeaderText="Read/Unread">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkReadMark" runat="server" Width="130px" CommandName="ReadMark">(Unread. Mark as read)</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="project" HeaderText="Project" SortExpression="project" />
                                    <asp:BoundField DataField="status" HeaderText="Status" SortExpression="status" />
                                    <asp:BoundField DataField="priority" HeaderText="Priority" SortExpression="priority" />
                                    <asp:BoundField DataField="assigned_to_user" HeaderText="Assigned to" SortExpression="assigned_to_user" />
                                    <asp:BoundField DataField="last_updated_user" HeaderText="Last Updated By" SortExpression="last_updated_user" />
                                    <asp:TemplateField HeaderText="Last Updated" SortExpression="last_updated_date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLastUpdatedOn" runat="server" Text='<%# bind("last_updated_date") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="category" HeaderText="Category" SortExpression="category" />
                                    <asp:BoundField HeaderText="ETC" />
                                    <asp:BoundField HeaderText="Time to date" />
                                    <asp:BoundField HeaderText="Balance" />
                                    <asp:BoundField HeaderText="Reported by" SortExpression="reported_user" Visible="False"
                                        DataField="reported_user" />
                                    <asp:TemplateField HeaderText="Reported on" SortExpression="reported_date" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblReportedOn" runat="server" Text='<%# Bind("reported_date") %>'></asp:Label>
                                            <asp:HiddenField ID="hd" runat="server" Value='<%# Eval("ETC")%>' />
                                            <asp:HiddenField ID="hdhrs" runat="server" Value='<%# Eval("Hrs")%>' />
                                            <asp:HiddenField ID="hdgethrs" runat="server" Value='<%# Eval("gethrs")%>' /> 
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ETC" HeaderText="ETC" Visible="false" />
                                    <asp:BoundField DataField="task_id" HeaderText="ID" SortExpression="task_id" />
                                    <asp:HyperLinkField HeaderText="Edit" Text="edit" DataNavigateUrlFields="task_id"
                                        DataNavigateUrlFormatString="edit_task.aspx?id={0}" NavigateUrl="edit_task.aspx">
                                        <HeaderStyle Width="25px" />
                                    </asp:HyperLinkField>
                                    <%--<asp:ButtonField HeaderText="Delete" CommandName="delete" Text="delete" />--%>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkdel" runat="server" CommandName="delete">delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Top" Font-Size="12px" />
                                <%--<ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Top" Font-Size="12px" />--%>
                            </asp:GridView>
                            <asp:Panel ID="Panel1" runat="server">
                            </asp:Panel>
                        </div>
                        <div style="text-align: right" id="divTotalHrs" runat="server">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td align="right">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 85px;" align="right" valign="middle">
                                                    <b>Total Hrs:</b>
                                                </td>
                                                <td>
                                                    <table cellpadding="3" cellspacing="0" border="1">
                                                        <tr>
                                                            <td style="width: 85px" align="left" valign="top">
                                                                <asp:Label ID="lblETC" runat="server" Font-Bold="True" Font-Size="12px"></asp:Label>
                                                            </td>
                                                            <td style="width: 85px" align="left" valign="top">
                                                                <asp:Label ID="lblTotalHrsTaken" runat="server" Font-Bold="True" Font-Size="12px"></asp:Label>
                                                            </td>
                                                            <td style="width: 90px" align="left" valign="top">
                                                                <asp:Label ID="lblExpectedHrsLeft" runat="server" Font-Bold="True" Font-Size="12px"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="padding-right: 3px; padding-left: 3px; padding-bottom: 3px; width: 25px;
                                                    padding-top: 3px">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <uc1:footer ID="Footer1" runat="server" />
    <asp:Label ID="lblOrderBy" runat="server" Text="" Visible="false"></asp:Label>
    <div id="popup" class="popup" style="background-color: #e9e9e9; width: 650px; border: solid 1px #000000;
        border-bottom: solid 2px #000000; border-right: solid 2px #000000; padding: 10px 10px 10px 10px;
        position: absolute; display: none">
    </div>
    </form>
</body>
</html>
