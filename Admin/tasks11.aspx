<%@ Page Language="C#" AutoEventWireup="true" CodeFile="tasks11.aspx.cs" Inherits="Admin_tasks" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Src="Notifications.ascx" TagName="Notifications" TagPrefix="uc4" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        <%= ConfigurationManager.AppSettings["CompanyName"].ToString()%></title>
    <%--<meta http-equiv="refresh" content="1200" />--%>
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script language="javascript" type="text/javascript">
        function window.confirm(str) {
            execScript('n = msgbox("' + str + '","4132")', "vbscript");
            return (n == 6);
        }
        function opacity() {

        }
       
    </script>
    <script type="text/javascript">
        function get_(div_) {

            div_ = div_.id + "1";

            document.getElementById(div_).style.display = "block";
        }
        function get_1(div_) {

            div_ = div_.id + "1";

            document.getElementById(div_).style.display = "none";
        }
    </script>
    <script type="text/javascript" language="javascript">
        function EnablePRlink() {
            var objPR = document.getElementById("DrpMultipleTaskPR").value;

            if (objPR != "0") {

                document.getElementById("LnkBtnToChngPR").disabled = false;

                // document.getElementById("LnkChngAssingedTo").disabled = "disabled";
                document.getElementById("LnkChngStatus").disabled = "disabled";
                document.getElementById("LnkChngAssingedTo").disabled = "disabled";
                document.getElementById("DrpMultipleTaskAssing").value = "0";
                document.getElementById("DrpMultipleTaskStatus").value = "0";

            }
            else {

                document.getElementById("LnkBtnToChngPR").disabled = "disabled";

                document.getElementById("LnkChngStatus").disabled = "disabled";
                document.getElementById("LnkChngAssingedTo").disabled = "disabled";
                document.getElementById("DrpMultipleTaskAssing").value = "0";
                document.getElementById("DrpMultipleTaskStatus").value = "0";
                document.getElementById("DrpMultipleTaskPR").value = "0";
            }

            return true;

        }


        function EnableAssinglink() {

            var objAssing = document.getElementById("DrpMultipleTaskAssing").value;

            if (objAssing != "0") {

                document.getElementById("LnkBtnToChngPR").disabled = "disabled";
                document.getElementById("LnkChngAssingedTo").disabled = false;
                document.getElementById("LnkChngStatus").disabled = "disabled";
                ///document.getElementById("DrpMultipleTaskAssing").value = "0";
                document.getElementById("DrpMultipleTaskStatus").value = "0";
                document.getElementById("DrpMultipleTaskPR").value = "0";
                ///document.getElementById("LnkChngAssingedTo").disabled = "disabled";
            }
            else {

                document.getElementById("LnkBtnToChngPR").disabled = "disabled";
                document.getElementById("LnkChngAssingedTo").disabled = "disabled";
                document.getElementById("LnkChngStatus").disabled = "disabled";
                document.getElementById("DrpMultipleTaskAssing").value = "0";
                document.getElementById("DrpMultipleTaskStatus").value = "0";
                document.getElementById("DrpMultipleTaskPR").value = "0";

            }

            return;
        }

        function EnableStatuslink() {
            var objStatus = document.getElementById("DrpMultipleTaskStatus").value;

            if (objStatus != "0") {

                document.getElementById("LnkBtnToChngPR").disabled = "disabled";
                document.getElementById("LnkChngAssingedTo").disabled = "disabled";
                document.getElementById("LnkChngStatus").disabled = false;
                document.getElementById("DrpMultipleTaskAssing").value = "0";
                ///document.getElementById("DrpMultipleTaskStatus").value = "0";
                document.getElementById("DrpMultipleTaskPR").value = "0";

            }
            else {

                document.getElementById("LnkBtnToChngPR").disabled = "disabled";
                document.getElementById("LnkChngAssingedTo").disabled = "disabled";
                document.getElementById("LnkChngStatus").disabled = "disabled";
                document.getElementById("DrpMultipleTaskAssing").value = "0";
                document.getElementById("DrpMultipleTaskStatus").value = "0";
                document.getElementById("DrpMultipleTaskPR").value = "0";

            }
            return;

        }

        //function to select all checkbox of gridview DataGrid1
        function setCheckAll(obj) {
            var chkGridcontrols = document.getElementById("<%=DataGrid1.ClientID%>").getElementsByTagName("input");
            for (var i = 0; i < chkGridcontrols.length; i++) {
                chkGridcontrols[i].checked = obj.checked;
            }

            return true;
        }

        //function to uncheck 'select all' when any checkbox in gridview is unchecked
        function validate(obj) {
            debugger;
            var chkGridcontrols = document.getElementById("<%=DataGrid1.ClientID%>").getElementsByTagName("input");
            //var chkheaderSelect = document.getElementById("chkheaderSelect");
            var chkheaderSelect = document.getElementById("DataGrid1_ctl01_chkheaderSelect");

            if (!obj.checked)
                chkheaderSelect.checked = false;
        }

        //Validate Mark Button & Delete Button present inside Action area
        function validateonbtn(a) {
            var chkGridcontrols = document.getElementById("<%=DataGrid1.ClientID%>").getElementsByTagName("input");
            var count = 0;
            for (var i = 0; i < chkGridcontrols.length; i++) {
                if (chkGridcontrols[i].checked)
                    count = 1;
            }

            if (count > 0) {
                if (a == 0) {
                    if (!confirm("Are you sure you want to delete the selected tasks?"))
                        return false;
                    else
                        return true;
                }
                else if (a == 1) { }

            }
            else {
                alert("Please select at least one task");
                return false;
            }
        }
    </script>
    </telerik:RadCodeBlock>
    <link rel="stylesheet" type="text/css" href="slider.css" />
    <script type="text/javascript" src="slider.js"></script>
    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
    <%--  width: 650px !important;--%>
    <style type="text/css">
        .abc
        {
            left: 260px !important;
        }
    </style>
    <style type="text/css">
     table#DataGrid1_ctl00 tr th a
     {
      display:block;
      text-decoration:none;
      margin:0;
      padding:0;
      width:100%;
      height:100%;
     }
    </style>
    <style type="text/css">
    div.RadToolTip_Default table.rtWrapper td.rtWrapperContent
   {
    background-color: #FEFFB3 !important;
   }
</style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <uc2:Admin_Header ID="Admin_Header1" runat="server" />
        <div id="Content">
            <asp:UpdateProgress ID="updateprogress1" runat="server">
                <ProgressTemplate>
                    <div id="processmessage" class="processMessage">
                        <img alt="loading" src="images/ajax-loader.gif" style="width: 80px; height: 80px" />
                        <span style="color: black; font-weight: bold; font-size: 18px; vertical-align: super;">
                            Please wait... </span>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div id="Content_98">
                        <%--<div id="pageTitle">
                Tasks<span style="padding-left: 885px;"></span> <a href="http://www.esgdesk.com//Download/ESG_Desk.application"
                    style="color: white; font-size: 15px; text-decoration: bold; cursor: pointer;"
                    onmouseover="this.style.color='blue'" onmouseout="this.style.color='#ffffff'">Click
                    here to download the desktop application</a></div>--%>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="left" valign="top" style="display: none;">
                                    <div style="height: 40px; vertical-align: middle; line-height: 40px; margin: 20px 0 0 0;">
                                        <div class="titleText" style="float: left;">
                                            ESG Desk - Tasks</div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <div style="padding: 0px 0 0px 5px; margin: 14px  0 -2px 0;">
                                        <table>
                                            <tr>
                                                <td id="s1">
                                                    <span class="function-menu" onmousedown="slideContent('section-1')">Filter</span>
                                                </td>
                                                <td id="s2">
                                                    <span class="function-menu" onmousedown="slideContent('section-2')">Action</span>
                                                </td>
                                                <td id="s3">
                                                    <span class="function-menu-1" onmousedown="slideContent('section-3')">Quick Search</span>
                                                </td>
                                                <td>
                                                    <span id="TopFilterLable" runat="server" class="topfilter"></span>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div>
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" onclick="return TABLE1_onclick()"
                                            id="TABLE1">
                                            <tr>
                                               
                                                <td style="border-radius: 9px 9px 9px 9px; height:62px;" bgcolor="#3e3f3d" width="100%">
                                                    <div class="slider">
                                                        <div class="slidercontent" id="slider">
                                                            <div id="section-1" class="section upper" style="margin-top: 0px;">
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td width="45" valign="top" style="padding-top: 20px;">
                                                                            <b style="color: #00a651;">Filter:</b>
                                                                        </td>
                                                                        <td valign="top" class="whitetext2" width="9%">
                                                                            <br />
                                                                            <span class="whitetext2">
                                                                                <asp:DropDownList ID="drpFilter" runat="server" AutoPostBack="True" Width="96%" OnSelectedIndexChanged="drpFilter_SelectedIndexChanged"
                                                                                    CssClass="filerDrpodown">
                                                                                </asp:DropDownList>
                                                                            </span>
                                                                        </td>
                                                                        <td width="9%" valign="top" style="color: White; font-size: 14px">
                                                                            Pool<br />
                                                                            <span class="whitetext2"><span class="whitetext2"><span class="whitetext2">
                                                                                <asp:DropDownList ID="Drpool" runat="server" CssClass="filerDrpodown" AutoPostBack="True" OnSelectedIndexChanged="Drpool_SelectedIndexChanged" Width="96%">
                                                                                <asp:ListItem Value="0">[no filter]</asp:ListItem>
                                                                                <asp:ListItem Value="Developers Pool">Show my developers pool tasks</asp:ListItem> 
                                                                                <asp:ListItem Value="Designers Pool">Show my designers pool tasks</asp:ListItem> 
                                                                                </asp:DropDownList>
                                                                            </span></span></span>
                                                                        </td>
                                                                        <td width="9%" valign="top" style="color: White; font-size: 14px">
                                                                            Project<br />
                                                                            <span class="whitetext2"><span class="whitetext2"><span class="whitetext2">
                                                                                <asp:DropDownList ID="drpProjects" runat="server" CssClass="filerDrpodown" AutoPostBack="True"
                                                                                    OnSelectedIndexChanged="drpProjects_SelectedIndexChanged" Width="96%">
                                                                                </asp:DropDownList>
                                                                            </span></span></span>
                                                                        </td>
                                                                        <td width="9%" valign="top" style="color: White; font-size: 14px">
                                                                            Category<br />
                                                                            <asp:DropDownList ID="drpCategories" runat="server" CssClass="filerDrpodown" AutoPostBack="True"
                                                                                OnSelectedIndexChanged="drpCategories_SelectedIndexChanged" Width="96%">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td width="9%" valign="top" style="color: White; font-size: 14px">
                                                                            Reported by<br />
                                                                            <asp:DropDownList ID="drpReportedBy" runat="server" CssClass="filerDrpodown" AutoPostBack="True"
                                                                                OnSelectedIndexChanged="drpReportedBy_SelectedIndexChanged" Width="96%">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td width="9%" valign="top" style="color: White; font-size: 14px">
                                                                            Priority<br />
                                                                            <asp:DropDownList ID="drpPriorities" runat="server" CssClass="filerDrpodown" AutoPostBack="True"
                                                                                OnSelectedIndexChanged="drpPriorities_SelectedIndexChanged" Width="96%">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td id="tdAssignedTo" runat="server" width="9%" valign="top" style="color: White;
                                                                            font-size: 14px">
                                                                            Assigned to<br />
                                                                            <span class="whitetext2">
                                                                                <asp:DropDownList ID="drpAssignedTo" runat="server" CssClass="filerDrpodown" AutoPostBack="True"
                                                                                    OnSelectedIndexChanged="drpAssignedTo_SelectedIndexChanged" Width="96%">
                                                                                </asp:DropDownList>
                                                                            </span>
                                                                        </td>
                                                                        <td width="9%" valign="top" style="color: White; font-size: 14px">
                                                                            Status<br />
                                                                            <asp:DropDownList ID="drpStatuses" runat="server" CssClass="filerDrpodown" AutoPostBack="True"
                                                                                OnSelectedIndexChanged="drpStatuses_SelectedIndexChanged" Width="96%">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td width="5%" valign="top" style="color: White; font-size: 14px">
                                                                            Items<br />
                                                                            <asp:DropDownList ID="drpPerPage1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpPerPage1_SelectedIndexChanged">
                                                                                <asp:ListItem Value="20" Selected="True">20</asp:ListItem>
                                                                                <asp:ListItem Value="50">50</asp:ListItem>
                                                                                <asp:ListItem Value="100">100</asp:ListItem>
                                                                                <asp:ListItem Value="00">All</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td valign="top" width="85" style="padding-top: 20px">
                                                                            <asp:Button ID="btnClearFilters" runat="server" OnClick="btnClearFilters_Click" Text="Clear filters"
                                                                                CssClass="clearfilter" />
                                                                        </td>
                                                                        <td valign="top" class="whitetext2">&nbsp;
                                                                            
                                                                        </td>
                                                                        <td valign="top" class="whitetext2" style="min-width: 140px; color: White; font-size: 14px">
                                                                            <asp:CheckBox ID="chkViewDeleted" runat="server" Text="View only deleted" AutoPostBack="True"
                                                                                OnCheckedChanged="chkViewDeleted_CheckedChanged" /><br />
                                                                            <asp:CheckBox ID="chkHideClosed" runat="server" Text="View closed" AutoPostBack="True"
                                                                                OnCheckedChanged="chkHideClosed_CheckedChanged" />
                                                                        </td>
                                                                        <td valign="bottom" class="whitetext2" style="min-width: 170px; color: White; font-size: 15px">
                                                                            <asp:CheckBox ID="ChkReadTask" runat="server" Text="Include read&nbsp;&nbsp;" Checked="true"
                                                                                AutoPostBack="True" OnCheckedChanged="ChkReadTask_CheckedChanged" />
                                                                            <span style="color: #00a651; float: right;"><b>Records: </b>
                                                                                <asp:Label ID="LblRecordsno" runat="server" Text=""></asp:Label></span>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <div id="section-2" class="section upper" style="padding-top: 5px;height: 36px;">
                                                                <div id="DivMiltiselection" runat="server" style="color: White; font-size: 14px">
                                                                    <b style="color: #00a651;">Action:</b>
                                                                    <asp:LinkButton ID="LnkBtnToDeleteAll" runat="server" OnClick="LnkBtnToDeleteAll_Click"
                                                                        CssClass="whitetext2" OnClientClick="return validateonbtn(0);">Delete selected</asp:LinkButton>&nbsp;&nbsp;
                                                                    <asp:LinkButton ID="LnkBtnToMarkAllRead" runat="server" OnClientClick="return validateonbtn(1);"
                                                                        OnClick="LnkBtnToMarkAllRead_Click" CssClass="whitetext2">Mark read</asp:LinkButton><br />
                                                                    <span style="float: left;"><span style="color: Red;">&nbsp;</span></span><asp:Button
                                                                        ID="LnkBtnToChngPR" runat="server" BackColor="transparent" BorderStyle="none"
                                                                        Text="Change priority to" OnClick="LnkBtnToChngPR_Click" Style="color: White;
                                                                        font-size: 14px; margin-left: -15px" />
                                                                    <asp:DropDownList ID="DrpMultipleTaskPR" runat="server" CssClass="filerDrpodown"
                                                                        AutoPostBack="true" Width="180px">
                                                                    </asp:DropDownList>
                                                                    &nbsp;&nbsp;<asp:Button ID="LnkChngAssingedTo" runat="server" BackColor="transparent"
                                                                        BorderStyle="none" Text="Change assigned to" OnClick="LnkChngAssingedTo_Click"
                                                                        Style="color: White; font-size: 14px" />
                                                                    <asp:DropDownList ID="DrpMultipleTaskAssing" runat="server" CssClass="filerDrpodown"
                                                                        AutoPostBack="false" Width="180px">
                                                                    </asp:DropDownList>
                                                                    &nbsp;&nbsp;<asp:Button ID="LnkChngStatus" runat="server" BackColor="transparent"
                                                                        BorderStyle="none" Text="Change status to" OnClick="LnkChngStatus_Click" Style="color: White;
                                                                        font-size: 14px" />
                                                                    <asp:DropDownList ID="DrpMultipleTaskStatus" runat="server" CssClass="filerDrpodown"
                                                                        AutoPostBack="false" Width="180px">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div id="section-3" class="section upper" style="padding: 0px;">
                                                                <table>
                            <tr>
                                <td valign="bottom" style="padding-bottom: 4px;">
                                    <table id="tblSearchRequest" runat="server" border="0" cellpadding="0" cellspacing="2">
                                        <tr>
                                            <td>&nbsp;
                                                
                                            </td>
                                            <td  style="min-width: 70px;color:White;font-size:14px">
                                                Request ID:
                                            </td>
                                            <td class="whitetext2" width="40">
                                                <asp:TextBox ID="txtRequestId" runat="server" Width="100%" Height="15px" MaxLength="10"
                                                    Style="min-width: 20px;"></asp:TextBox>
                                            </td>
                                            <td style="color:White;font-size:14px">
                                                <asp:Button ID="btnGo" runat="server" Text="Go" CausesValidation="False" OnClick="btnGo_Click"
                                                    CssClass="goBtn" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td valign="bottom" style="padding-bottom: 4px;">
                                    <table cellpadding="0" cellspacing="2">
                                        <tr>
                                            <td>
                                            </td>
                                            <td style="min-width: 40px;color:White;font-size:14px">
                                                Task ID
                                            </td>
                                            <td width="40">
                                                <asp:TextBox ID="txtTaskId" runat="server" Width="100%" Height="15px" MaxLength="10"
                                                    Style="min-width: 20px;"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnGotoTask" runat="server" Text="Go" CausesValidation="False" OnClick="btnGotoTask_Click"
                                                    CssClass="goBtn" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="55%" style="padding-left: 3px; padding-bottom: 5px">
                                    <table id="Tblsearchbyword" runat="server" border="0" cellpadding="0" cellspacing="2">
                                        <tr>
                                            <td align="left" colspan="2" style="color:White;font-size:14px">
                                                Search by word:
                                                <asp:CheckBox ID="ChkTask" runat="server" Text="Task" />
                                                <asp:CheckBox ID="ChkCrs" runat="server" Text="CRs" />
                                                <asp:CheckBox ID="ChkIncludecomment" runat="server" Text="Include comments" />
                                            </td>
                                        </tr>
                                        <tr style="padding-top: 2px">
                                            <td width="100%">
                                                <asp:TextBox ID="txtWord" runat="server" Width="100%" Height="15px"></asp:TextBox>
                                            </td>
                                            <td width="37" align="right">
                                                <asp:Button ID="BtnGoWrdSearch" runat="server" Text="Go" CssClass="goBtn" CausesValidation="False"
                                                    OnClick="BtnGoWrdSearch_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                             
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top" style="height: 4px">
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <span style="color: Black; font-size: 16px">
                                        <asp:LinkButton ID="LnkNotification" runat="server" ForeColor="red" OnClick="LnkNotification_Click"></asp:LinkButton>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <uc4:Notifications ID="Notifications" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top" style="height: 3px">
                                </td>
                            </tr>
                        </table>
                        <div id="divMessage" runat="server" style="color: Red; font-weight: bold;">
                        </div>
                        <div>
                            <%--<asp:GridView ID="DataGrid1" runat="server" Width="100%" AutoGenerateColumns="False"
                                AllowPaging="false" AllowSorting="True" CellPadding="5" CssClass="tasktbl" PageSize="500" OnItemCommand="DataGrid1_ItemCommand"
                                OnSelectedIndexChanged="DataGrid1_SelectedIndexChanged" BorderColor="#cccccc"
                                OnPageIndexChanging="DataGrid1_PageIndexChanging" OnRowDataBound="DataGrid1_RowDataBound"
                                OnRowCommand="DataGrid1_RowCommand" OnSorting="DataGrid1_Sorting" ShowFooter="True">
                                <PagerSettings Mode="NextPrevious" FirstPageText="First" PreviousPageText="Prev"
                                    NextPageText="Next" LastPageText="Last" />
                                <RowStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Top" Font-Size="12px" />
                                <Columns>
                                    <asp:BoundField DataField="task_id" HeaderText="ID" Visible="False" SortExpression="task_id" />
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            
                                            <asp:CheckBox ID="chkheaderSelect" runat="server" onclick="return setCheckAll(this);"
                                                Text="Select all" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" onclick="return validate(this);" /></ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="short_desc" HeaderText="ShrtDesc" Visible="False" SortExpression="short_desc" />                                    
                                    <asp:TemplateField HeaderText="Short description" SortExpression="short_desc">
                                        <ItemTemplate>
                                            <asp:Label Font-Size="12px" ID="lblshrtDesc" runat="server" Width="250px" Text='<%# bind("short_desc") %>'></asp:Label>
                                            <div id="lblshrtDesc1" runat="server" style="display: none; background-color: #FEFFB3;
                                                border: solid 1px #333333; width: 730px; padding-left: 3px; top: 342px;" class="abc">
                                                <asp:Label ID="LblLastcomment" runat="server" Text='<%# bind("ShowAllComment") %>'></asp:Label>
                                            </div>
                                            <ajax:HoverMenuExtender ID="hme2" runat="Server" HoverCssClass="popupHover" PopupControlID="lblshrtDesc1"
                                                PopupPosition="top" TargetControlID="lblshrtDesc" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:HyperLinkField Text="(+)" Visible="False" DataNavigateUrlFields="task_id" NavigateUrl="edit_task.aspx"
                                        DataNavigateUrlFormatString="edit_task.aspx?id={0}" SortExpression="short_desc">
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" Font-Size="13px" />
                                    </asp:HyperLinkField>
                                    <asp:TemplateField HeaderText="Read/Unread">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkReadMark" runat="server" Width="130px" CommandName="ReadMark">(Unread. Mark as read)</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Project" SortExpression="project">
                                        <ItemTemplate>
                                            <a id="lblproject" runat="server" target="_blank" href='<%# "projectrequests.aspx?projectname="+ Eval("project") %>'>
                                                <%# Eval("project")%></a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="status" HeaderText="Status" SortExpression="status" />
                                    <asp:BoundField DataField="priority" HeaderText="Priority" SortExpression="priority" />
                                    <asp:TemplateField HeaderText="Assigned to" SortExpression="assigned_to_user">
                                        <ItemTemplate>
                                        <a id="lblassignedtouser" runat="server" target="_blank" href='<%# "Priority_reports.aspx?Ename="+ Eval("assigned_to_user") %>'><%# Eval("assigned_to_user") %></a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Last Updated By" SortExpression="last_updated_user">
                                        <ItemTemplate>
                                           <a id="lbllast_updated_user" runat="server" target="_blank" href='<%# "Priority_reports.aspx?Ename="+ Eval("last_updated_user") %>'>
                                                <%# Eval("last_updated_user")%></a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Last Updated" SortExpression="last_updated_date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLastUpdatedOn" runat="server" Text='<%# bind("last_updated_date") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    
                                    <asp:BoundField DataField="category" HeaderText="Category" SortExpression="category" />
                                    <asp:BoundField HeaderText="ETC" />
                                    <asp:BoundField HeaderText="Time to date" />
                                    <asp:BoundField HeaderText="Balance" />
                                    <asp:BoundField HeaderText="Reported by" SortExpression="reported_user" Visible="False" DataField="reported_user" />
                                    
                                    
                                    
                                    
                                    
                                    <asp:TemplateField HeaderText="Reported on" SortExpression="reported_date" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblReportedOn" runat="server" Text='<%# Bind("reported_date") %>'></asp:Label>
                                            <asp:HiddenField ID="hd" runat="server" Value='<%# Eval("ETC")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    
                                    <asp:BoundField DataField="ETC" HeaderText="ETC" Visible="False" />
                                    <asp:BoundField DataField="task_id" HeaderText="ID" SortExpression="task_id" />
                                    
                                    <asp:HyperLinkField HeaderText="Edit" Text="edit" DataNavigateUrlFields="task_id"
                                        DataNavigateUrlFormatString="edit_task.aspx?id={0}" NavigateUrl="edit_task.aspx">
                                        <HeaderStyle Width="25px" />
                                    </asp:HyperLinkField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkdel" runat="server" CommandName="delete">delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Top" BackColor="#3e3f3d"
                                    CssClass="tblTitle1" />
                            </asp:GridView>--%>
                            <%--****************************************************************************************************--%>
               
               <telerik:RadGrid ID="DataGrid1" PageSize="20" runat="server" AllowPaging="false" AllowSorting="true" 
                  AutoGenerateColumns="false" CellPadding="5"  GridLines="Both"    
                   OnSelectedIndexChanged="DataGrid1_SelectedIndexChanged" BorderColor="#cccccc"  
                    OnItemDataBound="RadGrid1_ItemDataBound" OnItemCommand="DataGrid1_OnItemCommand" OnSortCommand="DataGrid1_Sorting"      
                   ShowFooter="true">
                <MasterTableView PagerStyle-AlwaysVisible="true">
                    
                    <Columns>
                        <telerik:GridBoundColumn DataField="task_id" HeaderText="ID" Visible="False" SortExpression="short_desc"></telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn HeaderStyle-Width="15px" ItemStyle-Width="10px" FooterStyle-Width="10px" >
                            <HeaderTemplate>
                                  <%--<asp:CheckBox ID="chkheaderSelect"  runat="server" onclick="return setCheckAll(this);" Text="Select all" />--%>
                                  <asp:CheckBox ID="chkheaderSelect"  runat="server" onclick="return setCheckAll(this);"  />
                            </HeaderTemplate>
                            <ItemTemplate>
                                   <asp:CheckBox ID="chkSelect" runat="server"  onclick="return validate(this);" />
                             </ItemTemplate>
                         </telerik:GridTemplateColumn>
                         <telerik:GridBoundColumn DataField="short_desc" HeaderText="ShrtDesc" Visible="False" SortExpression="short_desc"></telerik:GridBoundColumn>
                         <telerik:GridTemplateColumn HeaderText="Short description" SortExpression="short_desc">
                            <ItemTemplate>
                              <asp:Label Font-Size="12px" ID="lblshrtDesc" runat="server" Width="250px" Text='<%# bind("short_desc") %>'></asp:Label>
                             <%-- <div id="lblshrtDesc1" runat="server" style="display:none;background-color:#FEFFB3;border:solid 1px #333333; width:730px;padding-left:10px;left:340px;top:342px;" >
                                 <asp:Label ID="Yearid" runat="server"  Text='<%#Eval("ShowAllComment") %>'></asp:Label>
                              </div>--%>
                              <telerik:RadToolTip ID="RadToolTip1" BackColor="#FEFFB3" runat="server" OffsetX ="-100" AutoCloseDelay="60000" HideEvent="LeaveTargetAndToolTip" OffsetY="10" TargetControlID="lblshrtDesc" Width="500px" RelativeTo="Element" Position="BottomLeft" ShowEvent="OnMouseOver"><%# DataBinder.Eval(Container, "DataItem.ShowAllComment")%></telerik:RadToolTip>
                                <%--<ajax:HoverMenuExtender ID="hme2" runat="Server" OffsetX ="100" OffsetY="20" PopupControlID="lblshrtDesc1" PopupPosition="Bottom" TargetControlID="lblshrtDesc" />--%>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridHyperLinkColumn Text="+" Visible="false" NavigateUrl="edit_task.aspx"  DataNavigateUrlFields="task_id" DataNavigateUrlFormatString="edit_task.aspx?id={0}" SortExpression="short_desc"></telerik:GridHyperLinkColumn> 
                        <telerik:GridTemplateColumn HeaderText="Read/Unread">
                        <ItemTemplate>
                        <asp:LinkButton ID="lnkReadMark" runat="server" Width="130px" CommandName="ReadMark">(Unread. Mark as read)</asp:LinkButton>
                        </ItemTemplate> 
                        </telerik:GridTemplateColumn> 
                        <telerik:GridTemplateColumn HeaderText="Project" SortExpression="project">
                        <ItemTemplate>
                        <a id="lblproject" runat="server" target="_blank" href='<%# "projectrequests.aspx?projectname="+ Eval("project") %>'><%# Eval("project")%></a>
                        </ItemTemplate> 
                        </telerik:GridTemplateColumn>
                        
                        <%--<telerik:GridBoundColumn DataField="status" HeaderText="status"  SortExpression="status"></telerik:GridBoundColumn>--%>
                        
                        <telerik:GridTemplateColumn  HeaderText="Status" SortExpression="status" UniqueName="StatusColor">
                        <ItemTemplate>
                         <asp:Label ID="statusvalue" runat="server"  Text='<%#Eval("status") %>'></asp:Label>
                        </ItemTemplate> 
                        </telerik:GridTemplateColumn>
                        
                        
                        <telerik:GridTemplateColumn  HeaderText="Priority" SortExpression="priority">
                        <ItemStyle Width="100px" />
                        <ItemTemplate>
                         <asp:Label ID="priorityvalue" runat="server"  Text='<%#Eval("priority") %>'></asp:Label>
                        </ItemTemplate> 
                        </telerik:GridTemplateColumn>
                        
                        
                        
                        <%--<telerik:GridBoundColumn DataField="priority" HeaderText="priority"  SortExpression="priority"></telerik:GridBoundColumn>--%>
                        <telerik:GridTemplateColumn HeaderText="Assigned" SortExpression="assigned_to_user">
                        <ItemTemplate>
                        <a id="lblassignedtouser" runat="server" target="_blank" href='<%# "Priority_reports.aspx?Ename="+ Eval("assigned_to_user") %>'><%# Eval("assigned_to_user") %></a>
                        </ItemTemplate> 
                        </telerik:GridTemplateColumn> 
                        <telerik:GridTemplateColumn HeaderText="Updated by" SortExpression="last_updated_user">
                        <ItemTemplate>
                        <a id="lbllast_updated_user" runat="server" target="_blank" href='<%# "Priority_reports.aspx?Ename="+ Eval("last_updated_user") %>'><%# Eval("last_updated_user")%></a>
                        </ItemTemplate> 
                        </telerik:GridTemplateColumn> 
                         <telerik:GridTemplateColumn HeaderText="Last updated" SortExpression="last_updated_date">
                         <ItemTemplate>
                          <asp:Label ID="lblLastUpdatedOn" runat="server" Text='<%# bind("last_updated_date") %>'></asp:Label>
                         </ItemTemplate> 
                         </telerik:GridTemplateColumn> 
                         <telerik:GridBoundColumn DataField="category" HeaderText="Category"  SortExpression="category"></telerik:GridBoundColumn>                 
                          
                          
                          <%--<telerik:GridBoundColumn HeaderText="ETC" ></telerik:GridBoundColumn>
                          <telerik:GridBoundColumn HeaderText="Time to date"></telerik:GridBoundColumn>
                          <telerik:GridBoundColumn HeaderText="Balance"></telerik:GridBoundColumn>
                          --%>
                        <telerik:GridTemplateColumn UniqueName="ETCfield" HeaderText="ETC" SortExpression="ETC" >
                         <ItemTemplate>
                             <asp:Label ID="lblETC" runat="server" ></asp:Label>
                          </ItemTemplate> 
                          </telerik:GridTemplateColumn> 
                      
                          
                          <telerik:GridTemplateColumn UniqueName="Timetodat" HeaderText="Time" SortExpression="Time to date" >
                              <ItemTemplate>
                                <asp:Label ID="lblTimetodat" runat="server" ></asp:Label>
                              </ItemTemplate> 
                          </telerik:GridTemplateColumn> 
                      
                          <telerik:GridTemplateColumn UniqueName="Balance" HeaderText="Balance" SortExpression="Balance" >
                         <ItemTemplate>
                             <asp:Label ID="lblBalance" runat="server" ></asp:Label>
                          </ItemTemplate> 
                          </telerik:GridTemplateColumn> 
                      
                          <telerik:GridBoundColumn DataField="Reported by" HeaderText="Reported by"  Visible="False" SortExpression="Reported by"></telerik:GridBoundColumn>                                     
                            <telerik:GridTemplateColumn UniqueName="ETC" HeaderText="Reported on" SortExpression="reported_date" Visible="False">
                      <ItemTemplate>
                         <asp:Label ID="lblReportedOn" runat="server" Text='<%# Bind("reported_date") %>'></asp:Label>
                          <asp:Label ID="hd" runat="server" Text='<%# Bind("ETC") %>'></asp:Label>
                         <%--<asp:HiddenField ID="hd" runat="server" Value='<%# Eval("ETC")%>' />--%>
                      </ItemTemplate> 
                      </telerik:GridTemplateColumn> 
                      <telerik:GridBoundColumn DataField="ETC" HeaderText="ETC"  Visible="False"></telerik:GridBoundColumn>
                      
                      <%--<telerik:GridBoundColumn DataField="task_id"  HeaderText="ID" SortExpression="task_id"></telerik:GridBoundColumn> --%>                
                      <telerik:GridTemplateColumn UniqueName="TaskId" HeaderText="ID" SortExpression="task_id" >
                      <ItemTemplate>
                         <asp:Label ID="lbltaskId" runat="server" Text='<%# Bind("task_id") %>'></asp:Label>
                       </ItemTemplate> 
                      </telerik:GridTemplateColumn> 
                      <telerik:GridHyperLinkColumn HeaderText="Edit" Text="edit" DataNavigateUrlFields="task_id" DataNavigateUrlFormatString="edit_task.aspx?id={0}" NavigateUrl="task_id"><HeaderStyle Width="25px" /></telerik:GridHyperLinkColumn> 
                      <telerik:GridTemplateColumn HeaderText="Delete">
                      <ItemTemplate>
                      <asp:LinkButton ID="lnkdel" runat="server" CommandName="delete">delete</asp:LinkButton>
                      </ItemTemplate> 
                      </telerik:GridTemplateColumn>                
                                                              
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
                            
                            <%--****************************************************************************************************--%>
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            <asp:Panel ID="Panel1" runat="server">
                            </asp:Panel>
                        </div>
                        <br />
                        <div style="text-align: right" id="divTotalHrs" runat="server">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td align="right">
                                        <table class="tasktbl" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 85px;" align="right" valign="middle">
                                                    <b>Total Hrs:</b>
                                                </td>
                                                <td>
                                                    <table cellpadding="3" cellspacing="0" border="1">
                                                        <tr>
                                                            <td style="width: 95px; background-color: #3e3f3d; color: #ffffff; font-weight: bold;"
                                                                align="left" valign="top">
                                                                Estimated time
                                                            </td>
                                                            <td style="width: 130px; background-color: #3e3f3d;; color: #ffffff; font-weight: bold;"
                                                                align="left" valign="top">
                                                                Total hours Taken
                                                            </td>
                                                            <td style="width: 90px; background-color: #3e3f3d;; color: #ffffff; font-weight: bold;"
                                                                align="left" valign="top">
                                                                Elapsed time
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 85px" align="left" valign="top">
                                                                <asp:Label ID="lblETC" runat="server" Font-Bold="True" Font-Size="12px"></asp:Label>
                                                            </td>
                                                            <td style="width: 85px" align="left" valign="top">
                                                                <asp:Label ID="lblTotalHrsTaken" runat="server" Font-Bold="True" Font-Size="12px"></asp:Label>
                                                            </td>
                                                            <td style="width: 90px" align="left" valign="top">
                                                                <asp:Label ID="lblExpectedHrsLeft" runat="server" Font-Bold="True" Font-Size="12px"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <uc1:footer ID="Footer1" runat="server" />
    <asp:Label ID="lblOrderBy" runat="server" Text="" Visible="false"></asp:Label>
    <div id="popup" class="popup" style="background-color: #e9e9e9; width: 650px; border: solid 1px #000000;
        border-bottom: solid 2px #000000; border-right: solid 2px #000000; padding: 10px 10px 10px 10px;
        position: absolute; display: none">
    </div>
    </form>
</body>
</html>
