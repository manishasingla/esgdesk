using System.Web.UI.WebControls;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
//using System.Web.UI.WebControls;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Configuration;
using Telerik.Web.UI;
using Telerik.Web;

public partial class Admin_tasks : System.Web.UI.Page
{
    int closecount = 0;
    string sql = "";
    string task_action = "";
    string emailSubject = "";
    DateTime duedate;
    bool hidecheck;
    bool adminhide;
    DataSet dstask;

    protected void Page_Load(object sender, EventArgs e)
    {



        if (Session["cwo"] == "y")
        {
            //ScriptManager.RegisterStartupScript(Page, typeof(Page), "init", "load();", true);
            //ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "init", "load();", true);
            //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('You already have task assigned status CWO please change it to CWO but away and then proceed');", true);
            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> { alert('You already have task assigned status CWO please change it to CWO but away and then proceed');} </script>");
            Session["cwo"] = "n";
        }
        //  hidecheck = false;
        // LnkMakecommentRead.Attributes.Add("onclick", "return confirm('Sure you want to mark every comment in every CR as read by you?');");
        // LnkMakeTaskcommentRead.Attributes.Add("onclick", "javascript:return confirm('Sure you want to mark every comment in each of your tasks as read by you (this means you must have read them all and acted on all that require action already)?');");
        //LnkMakeAllTaskCmmntRead.Attributes.Add("onclick", "javascript:return confirm('Sure you want to mark every comment in each of tasks as read by you (this means you must have read them all and acted on all that require action already)?');");
        //ME   LnkBtnToDeleteAll.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete all selected tasks?')");
        LnkBtnToChngPR.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to change priority of all selected tasks?')");
        LnkChngAssingedTo.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to change assigned to of all selected tasks?')");
        LnkChngStatus.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to change status of all selected tasks?')");
        // LnkBtnToMarkAllRead.Attributes.Add("onclick", "javascript:return confirm('Sure you want to mark every comment in each of selected tasks as read by you (this means you must have read them all and acted on all that require action already)?');");
        DrpMultipleTaskPR.Attributes.Add("onchange", "return EnablePRlink();");
        DrpMultipleTaskAssing.Attributes.Add("onchange", "return EnableAssinglink();");
        DrpMultipleTaskStatus.Attributes.Add("onchange", "return EnableStatuslink();");

        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Response.Redirect("login.aspx");
            return;
        }


        TopFilterLable.InnerHtml = "";
        divMessage.InnerHtml = "";

        if (!Page.IsPostBack)
        {

            //************************************************************
            string sqlQuestion = @"select distinct tasks.*,task_comments.username from task_comments,tasks 
                 where tasks.task_id = task_comments.task_id and  tasks.deleted <> 1 and task_comments.deleted <> 1 and qflag=1 
                and status <> 'closed' and task_comments.QuesTo =  '" + Session["admin"] + "' ";
            DataSet dsquestion = DatabaseHelper.getDataset(sqlQuestion);

            if (dsquestion != null && dsquestion.Tables.Count > 0 && dsquestion.Tables[0].Rows.Count > 0)
            {
                Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> { alert('You have " + dsquestion.Tables[0].Rows.Count + " new question.');} </script>");
            }
            //*************************************************************
            


            try
            {
                Session["pagesize"] = 20;
                if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
                {
                    Session["adm"] = "no";
                    tdAssignedTo.Visible = false;
                }
                else
                {
                    Session["adm"] = "yes";
                    tdAssignedTo.Visible = true;
                }
            }
            catch
            {

            }
            if (!DatabaseHelper.can_Show_Deleted_Tasks(Session["admin"].ToString()))
            {
                chkViewDeleted.Visible = false;
            }
            else
            {
                chkViewDeleted.Visible = true;
            }

            getAllNotification();

            if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
            {
                LnkBtnToDeleteAll.Visible = false;
                LnkBtnToChngPR.Visible = false;
                DivMiltiselection.Visible = false;
                // Notification.Visible = true;
            }
            else
            {
                LnkBtnToDeleteAll.Visible = true;
                LnkBtnToChngPR.Visible = true;
                DivMiltiselection.Visible = true;
                // Notification.Visible = true;
            }

            LnkBtnToChngPR.Enabled = false;
            LnkChngAssingedTo.Enabled = false;
            LnkChngStatus.Enabled = false;
            LnkChngAssingedTo.Enabled = false;

            if (Session["filter"] != null)
            {
                if (Session["boolclose"] == "true")
                {
                    chkHideClosed.Checked = true;
                    Session["boolclose"] = "false";
                    hidecheck = true;
                    adminhide = true;
                }
                else
                {
                    chkHideClosed.Checked = false;
                    hidecheck = false;
                    adminhide = false;
                }

                if (Session["filter"].ToString() == "Immediate")
                {
                    bindPR0Tasks();
                }
                else if (Session["filter"].ToString() == "Unanswered")
                {
                    bindUnansweredQuestions();
                }
                else if (Session["filter"].ToString() == "ToBeAnswered")
                {
                    bindtobeanswer();
                }
                else if (Session["filter"].ToString() == "1a - DO NOW")
                {
                    bindPR1aTasks();
                }
                else if (Session["filter"].ToString() == "HighTasks")
                {
                    bindPR1bTasks();
                }
                else if (Session["filter"].ToString() == "New")
                {
                    bindNewTask();
                }
                else if (Session["filter"].ToString() == "CWO")
                {
                    bindCrntWrkingTasks();
                }
                else if (Session["filter"].ToString() == "CWO/CWO but away")
                {
                    bindCrntWrkingTasks();
                }
                else if (Session["filter"].ToString() == "1c - normal")
                {
                    bindPR1cTasks();
                }
                else if (Session["filter"].ToString() == "2 - not urgent")
                {
                    bind2NTasks();
                }
                else
                {
                    bindDefault();
                }
            }
            else
            {


                load_dropdowns();
                load_filterDropDown();
                chkViewDeleted.Checked = false;
                ////chkHideClosed.Checked = false;
                if (Session["boolclose"] == "true")
                {
                    chkHideClosed.Checked = true;
                    Session["boolclose"] = "false";
                    hidecheck = true;
                }
                else
                {
                    chkHideClosed.Checked = false;
                    hidecheck = false;
                }
                ChkReadTask.Checked = true;


                bindDefault();
            }





        }
    }

    void getAllNotification()
    {

        // DataSet dataSourceCompany_name = Cache("Company_name");
        // if (dataSourceCompany_name == null)
        //  {
        sql = @"select distinct Company_name from Company_notes 
                where Allow_Notes ='True' and Notes !=''";

        DataSet ds;
        if (Cache["Company_notes"] == null)
        {
            ds = DatabaseHelper.getDataset(sql);
            //  Cache["Company_notes"] = ds;
            Cache.Insert("Company_notes", ds, null, DateTime.Now.AddHours(6), TimeSpan.Zero);
        }
        else
        {
            ds = (DataSet)Cache["Company_notes"];
        }


        //  Cache("Company_name") = ds;
        //    if (ds != null && ds.Tables[0].Rows.Count > 0)
        //   {


        ///LnkNotification.Text = "(" + ds.Tables[0].Rows.Count + ")";
        // Notification.Visible = true;

        //   }
        //    else
        //    {

        // Notification.Visible = true;
        // LnkNotification.Text = "";


        //   }

        //  }
    }

    private void BindData()
    {
        drpPerPage1.Visible = true;
        string filter = "where ";
        string filter2 = "and";
        string filter3 = " and ";
        LblRecordsno.Text = "";
        DateTime duedate;

        string topFilterLable = "";

        //if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        //{
        //    filter += " status <> 'closed' and ";
        //}


        if (!DatabaseHelper.can_Show_Closed_Tasks(Session["admin"].ToString()))
        {
            if (Session["filterunread"] == null || Session["filterunread"].ToString() == "")
            {

                filter += " status <> 'closed' and status <> 'Checked' and status <>'parked' and  ";
                filter2 += " status <> 'closed' and status <> 'Checked' and status <>'parked' and  ";
            }
            else
            {
                filter += " status <> 'closed' and ";
                filter2 += " status <> 'closed' and ";
            }

            ///filter3 += " status <> 'closed' and ";
            chkHideClosed.Visible = false;
        }


        else
        {
            if (chkHideClosed.Checked == true)
            {
                if (Session["filterunread"] == null || Session["filterunread"].ToString() == "")
                {
                    filter += " status <> 'checked' and status <>'parked' and status = 'closed' and ";
                    filter2 += " status <> 'checked' and status <>'parked' and status = 'closed' and ";
                    filter3 += " and status <> 'checked' and status <>'parked' and status = 'closed' and ";
                }
                //
                //else
                //{
                //    filter += " status = 'closed' and  ";
                //    filter2 += " status = 'closed' and ";
                //    filter3 += " status = 'closed' and ";
                //}

            }
            else
            {
                if (Session["filterunread"] == null || Session["filterunread"].ToString() == "")
                {

                    filter += " status <> 'closed' and status <> 'Checked' and status <>'parked' and ";
                    filter2 += " status <> 'closed' and status <> 'Checked' and status <>'parked' and ";
                    filter3 += " status <> 'closed' and status <> 'Checked' and status <>'parked' and ";

                }
                else
                {

                    filter += " status <> 'closed' and  ";
                    filter2 += " status <> 'closed' and ";
                    filter3 += " status <> 'closed' and ";

                }
            }
        }


        if (chkViewDeleted.Checked == true)
        {
            filter += " deleted = 1 ";
            filter2 += " deleted = 1 ";
            filter3 += " deleted = 1 ";
            ////filter3 += " deleted = 1 ";
        }
        else
        {

            filter += " deleted <> 1 ";
            filter2 += " deleted <> 1 ";
            filter3 += " deleted <> 1 ";

        }

        string drpValue = drpFilter.SelectedValue;
        string username = "";


        if (drpValue.Contains("Assigned by me"))
        {
            filter += " and reported_user = '" + username + "' ";
            filter2 += " and reported_user = '" + username + "' ";
        }
        else if (drpValue.Contains("Assigned by "))
        {
            username = drpValue.Replace("Assigned by ", "");

            filter += " and reported_user = '" + username + "' ";
            filter2 += " and reported_user = '" + username + "' ";
        }
        else if (drpValue.Contains("All emps CWO"))
        {
            filter += " and status = 'CWO' ";
            filter2 += " and status = 'CWO' ";
            filter3 += " and status = 'CWO' ";
        }
        else if (drpValue.Contains("Show my open tasks"))
        {
            //if (Session["adm"] == "no" || adminhide == true)
            //{
            filter += " and assigned_to_user = '" + Session["admin"] + "' ";
            filter2 += " and assigned_to_user = '" + Session["admin"] + "' ";
            adminhide = false;
            //}
        }
        else if (drpValue.Contains("All tasks by last updated"))
        {
            if (Session["adm"] == "no" || adminhide == true)
            {
                filter += " and assigned_to_user = '" + Session["admin"] + "' ";
                filter2 += " and assigned_to_user = '" + Session["admin"] + "' ";
                adminhide = false;
            }
        }


        if (drpProjects.SelectedIndex != 0)
        {
            filter += " and project = '" + drpProjects.SelectedValue + "' ";
            filter2 += " and project = '" + drpProjects.SelectedValue + "' ";
            filter3 += " and project = '" + drpProjects.SelectedValue + "' ";

            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Project: " + drpProjects.SelectedValue;
            }
            else
            {
                topFilterLable += " / Project: " + drpProjects.SelectedValue;
            }
        }

        if (drpCategories.SelectedIndex != 0)
        {
            filter += " and category = '" + drpCategories.SelectedValue + "' ";
            filter2 += " and category = '" + drpCategories.SelectedValue + "' ";
            filter3 += " and category = '" + drpCategories.SelectedValue + "' ";

            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Category: " + drpCategories.SelectedValue;
            }
            else
            {
                topFilterLable += " / Category: " + drpCategories.SelectedValue;
            }
        }

        if (drpReportedBy.SelectedIndex != 0)
        {
            filter += " and reported_user = '" + drpReportedBy.SelectedValue + "' ";
            filter2 += " and reported_user = '" + drpReportedBy.SelectedValue + "' ";
            filter3 += " and reported_user = '" + drpReportedBy.SelectedValue + "' ";

            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Reported by: " + drpReportedBy.SelectedValue;
            }
            else
            {
                topFilterLable += " / Reported by:" + drpReportedBy.SelectedValue;
            }
        }
        if (hidecheck == false)
        {
            if (drpPriorities.SelectedIndex != 0)
            {
                filter += " and priority = '" + drpPriorities.SelectedValue + "' ";
                filter2 += " and priority = '" + drpPriorities.SelectedValue + "' ";
                filter3 += " and priority = '" + drpPriorities.SelectedValue + "' ";

                if (topFilterLable == "")
                {
                    topFilterLable = "Filtered by Priority: " + drpPriorities.SelectedValue;
                }
                else
                {
                    topFilterLable += " / Priority:" + drpPriorities.SelectedValue;
                }
            }
        }

        if (drpAssignedTo.SelectedIndex != 0)
        {
            if (Session["adm"] != "yes")
            {
                filter = filter.Replace("and assigned_to_user = '" + Session["admin"] + "' ", " and assigned_to_user = '" + drpAssignedTo.SelectedValue + "' ");
                filter2 = filter2.Replace("and assigned_to_user = '" + Session["admin"] + "' ", " and assigned_to_user = '" + drpAssignedTo.SelectedValue + "' ");
                filter3 = filter3.Replace("and assigned_to_user = '" + Session["admin"] + "' ", " and assigned_to_user = '" + drpAssignedTo.SelectedValue + "' ");
            }
            else
            {
                filter += " and assigned_to_user = '" + drpAssignedTo.SelectedValue + "'";
                filter2 += " and assigned_to_user = '" + drpAssignedTo.SelectedValue + "'";
                filter3 += " and assigned_to_user = '" + drpAssignedTo.SelectedValue + "'";
            }
            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Assigned to: " + drpAssignedTo.SelectedValue;
            }
            else
            {
                topFilterLable += " / Assigned to: " + drpAssignedTo.SelectedValue;
            }
        }

        if (drpStatuses.SelectedIndex != 0)
        {
            if (drpStatuses.SelectedValue == "CWO")
            {
                filter = filter.Replace("and status <> 'Checked'", "").Replace("and status <>'parked'", "");
                filter2 = filter2.Replace("and status <> 'Checked'", "").Replace("and status <>'parked'", "");
                filter3 = filter3.Replace("and status <> 'Checked'", "").Replace("and status <>'parked'", "");
                filter += " and status = '" + drpStatuses.SelectedValue + "' ";
                filter2 += " and status = '" + drpStatuses.SelectedValue + "' ";
                filter3 += " and status = '" + drpStatuses.SelectedValue + "' ";

                if (topFilterLable == "")
                {
                    topFilterLable = "Filtered by Status: " + drpStatuses.SelectedValue;
                }
                else
                {
                    topFilterLable += " / Status: " + drpStatuses.SelectedValue;
                }
            }
            else if (drpStatuses.SelectedValue == "CWO but away")
            {
                filter = filter.Replace("and status <> 'Checked'", "").Replace("and status <>'parked'", "");
                filter2 = filter2.Replace("and status <> 'Checked'", "").Replace("and status <>'parked'", "");
                filter3 = filter3.Replace("and status <> 'Checked'", "").Replace("and status <>'parked'", "");
                filter += " and status =  '" + drpStatuses.SelectedValue + "' ";
                filter2 += " and status = '" + drpStatuses.SelectedValue + "' ";
                filter3 += " and status = '" + drpStatuses.SelectedValue + "' ";

                if (topFilterLable == "")
                {
                    topFilterLable = "Filtered by Status: " + drpStatuses.SelectedValue;
                }
                else
                {
                    topFilterLable += " / Status: " + drpStatuses.SelectedValue;
                }
            }
            else
            {
                filter = filter.Replace("and status <> 'Checked'", "").Replace("and status <>'parked'", "");
                filter2 = filter2.Replace("and status <> 'Checked'", "").Replace("and status <>'parked'", "");
                filter3 = filter3.Replace("and status <> 'Checked'", "").Replace("and status <>'parked'", "");
                filter += " and status =  '" + drpStatuses.SelectedValue + "' ";
                filter2 += " and status = '" + drpStatuses.SelectedValue + "' ";
                filter3 += " and status = '" + drpStatuses.SelectedValue + "' ";

                if (topFilterLable == "")
                {
                    topFilterLable = "Filtered by Status: " + drpStatuses.SelectedValue;
                }
                else
                {
                    topFilterLable += " / Status: " + drpStatuses.SelectedValue;
                }
            }

        }

        if (ChkReadTask.Checked)
        {


        }
        else
        {

            filter += " and  tasks.task_id  in(select task_id from task_comments where tc_id not in(select tc_id from read_comments where username='" + Session["admin"].ToString() + "')) ";
            filter2 += " and  tasks.task_id  in(select task_id from task_comments where tc_id not in(select tc_id from read_comments where username='" + Session["admin"].ToString() + "')) ";
            filter3 += " and  tasks.task_id  in(select task_id from task_comments where tc_id not in(select tc_id from read_comments where username='" + Session["admin"].ToString() + "')) ";

        }
        if (chkViewDeleted.Checked == true)
        {
            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by View only deleted";
            }
            else
            {
                topFilterLable += " / View only deleted";
            }
        }

        if (chkHideClosed.Checked == true)
        {
            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by view closed";
            }
            else
            {
                topFilterLable += " / view closed";
            }
        }

        TopFilterLable.InnerHtml = topFilterLable;

        DataSet ds=null;

        //==========================================================
        int ipagesize;
        string startquery = "";
        if (drpPerPage1.SelectedValue != "00")
        {

            ipagesize = Convert.ToInt16(Session["pagesize"]);
            startquery = "Select top " + ipagesize + "";
        }
        else
        {
            startquery = "Select";
        }

        //==========================================================

        if (Session["filterunread"] == null || Session["filterunread"].ToString() == "")
        {
            //================================================================
            //sql = "select * from tasks " + filter + " " + lblOrderBy.Text;
            try
            {
                sql = startquery;
                sql += @" t.*, tc.*,
                (select count(*) as commenttype from task_comments where tc_id not in 
                (select tc_id from read_comments where username ='" + Session["admin"].ToString() + @"')
                and task_comments.deleted <> 1 and task_id =t.task_id)
                from tasks t inner join task_comments tc on 
                t.task_id = tc.task_id and tc.post_date=(select top 1 post_date from task_comments where tc.task_id = task_id and deleted<>1 
                order by post_date desc)" + filter.Replace("deleted <> 1", "t.deleted <> 1") + " " + lblOrderBy.Text;
                ds = DatabaseHelper.getDataset(sql);
                ds = NewformatTaskDataSet(ds);
            }catch(Exception ex)
            {}

            try
            {
                DataSet NoofRecords = DatabaseHelper.getDataset(@"Select t.*, tc.*,
                (select count(*) as commenttype from task_comments where tc_id not in 
                (select tc_id from read_comments where username ='" + Session["admin"].ToString() + @"')
                and task_comments.deleted <> 1 and task_id =t.task_id) from tasks t inner join task_comments tc on 
                t.task_id = tc.task_id and tc.post_date=(select top 1 post_date from task_comments where tc.task_id = task_id and deleted<>1 
                order by post_date desc)" + filter.Replace("deleted <> 1", "t.deleted <> 1") + " " + lblOrderBy.Text);
                if (NoofRecords != null && NoofRecords.Tables.Count > 0 && NoofRecords.Tables[0].Rows.Count > 0)
                {
                    LblRecordsno.Text = NoofRecords.Tables[0].Rows.Count.ToString();
                }
                else
                {
                    LblRecordsno.Text = "0";
                }
            }catch(Exception ex)
            {}
            //================================================================
        }
        else
        {
            if (Session["filterunread"].ToString() == "TaskNewComment")
            {

                //sql = "select * from tasks where task_id in(select task_comments.task_id from task_comments where tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 )";
                //sql += filter2 + lblOrderBy.Text;

                //=============================================================================================================================
                try
                {
                    sql = startquery;
                    sql += @"t.*,tc.* ,
                    (select count(*) as commenttype from task_comments where tc_id not in 
                    (select tc_id from read_comments where username ='" + Session["admin"].ToString() + @"')
                    and task_comments.deleted <> 1 and task_id =t.task_id)
                    from tasks t , task_comments tc where t.task_id in
                    (select task_comments.task_id from task_comments where tc_id not in
                    (select tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + @"')
                    and task_comments.deleted <> 1) and t.task_id = tc.task_id and tc.post_date =
                    (select top 1 post_date from task_comments where tc.task_id = task_id
                     order by post_date desc)";
                    sql += filter2.Replace("deleted <> 1", "t.deleted <> 1").Replace("and status <> 'closed'", "") + lblOrderBy.Text;
                    ds = DatabaseHelper.getDataset(sql);
                    ds = NewformatTaskDataSet(ds);
                }catch(Exception ex)
                {}
                try
                {
                    DataSet NoofRecords = DatabaseHelper.getDataset(@"Select t.*,tc.* ,
                    (select count(*) as commenttype from task_comments where tc_id not in 
                    (select tc_id from read_comments where username ='" + Session["admin"].ToString() + @"')
                    and task_comments.deleted <> 1 and task_id =t.task_id)
                    from tasks t , task_comments tc where t.task_id in
                    (select task_comments.task_id from task_comments where tc_id not in
                    (select tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + @"')
                    and task_comments.deleted <> 1) and t.task_id = tc.task_id and tc.post_date =
                    (select top 1 post_date from task_comments where tc.task_id = task_id
                     order by post_date desc)" + filter2.Replace("deleted <> 1", "t.deleted <> 1").Replace("and status <> 'closed'", "") + lblOrderBy.Text);
                    if (NoofRecords != null && NoofRecords.Tables.Count > 0 && NoofRecords.Tables[0].Rows.Count > 0)
                    {
                        LblRecordsno.Text = NoofRecords.Tables[0].Rows.Count.ToString();
                    }
                    else
                    {
                        LblRecordsno.Text = "0";
                    }
                }
                catch(Exception ex)
                {}
                //=============================================================================================================================
            }

            else if (Session["filterunread"].ToString() == "AllTaskNewComment")
            {
                //sql = "select  tasks.* from tasks where task_id in(select task_comments.task_id from task_comments where tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 )";
                try
                {
                    sql = startquery;
                    sql += @" t.*,tc.*,
                     (select count(*) as commenttype from task_comments where tc_id not in 
                     (select tc_id from read_comments where username ='" + Session["admin"].ToString() + @"')
                     and task_comments.deleted <> 1 and task_id =t.task_id)
                     from tasks t,task_comments tc where t.task_id in
                     (select task_comments.task_id from task_comments where tc_id not in
                     (select tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + @"' )
                      and task_comments.deleted <> 1) and t.task_id = tc.task_id and tc.post_date =
                     (select top 1 post_date from task_comments where tc.task_id = task_id
                      order by post_date desc)";
                    if (Session["boolcount"] == "true")
                    {
                        sql += lblOrderBy.Text;
                    }
                    else
                    {
                        if (hidecheck == false)
                        {
                            sql += filter3 + " " + lblOrderBy.Text;
                        }
                        else
                        {
                            sql += lblOrderBy.Text;
                        }
                    }
                
                
                //=====================================================================================================
                ds = DatabaseHelper.getDataset(sql);
                ds = NewformatTaskDataSet(ds);
                }
                catch (Exception ex)
                { }
                try
                {
                    DataSet NoofRecords;
                    if (Session["boolcount"] == "true")
                    {
                        NoofRecords = DatabaseHelper.getDataset(@"select Count(*) as NoofRec
                      from tasks t,task_comments tc where t.task_id in
                     (select task_comments.task_id from task_comments where tc_id not in
                     (select tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + @"' )
                      and task_comments.deleted <> 1) and t.task_id = tc.task_id and tc.post_date =
                     (select top 1 post_date from task_comments where tc.task_id = task_id
                      order by post_date desc)");
                    }
                    else
                    {
                        NoofRecords = DatabaseHelper.getDataset(@"select Count(*) as NoofRec
                      from tasks t,task_comments tc where t.task_id in
                     (select task_comments.task_id from task_comments where tc_id not in
                     (select tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + @"' )
                      and task_comments.deleted <> 1) and t.task_id = tc.task_id and tc.post_date =
                     (select top 1 post_date from task_comments where tc.task_id = task_id
                      order by post_date desc)");
                    }
                    if (NoofRecords != null && NoofRecords.Tables.Count > 0 && NoofRecords.Tables[0].Rows.Count > 0)
                    {
                        LblRecordsno.Text = NoofRecords.Tables[0].Rows[0]["NoofRec"].ToString();
                    }
                    else
                    {
                        LblRecordsno.Text = "0";
                    }
                }catch(Exception ex)
                {}
                //=====================================================================================================
            }
            else
            {
                //========================================================================================================================
                //sql = "select * from tasks " + filter + " " + lblOrderBy.Text;
                try
                {
                    sql = startquery;
                    sql += @" t.*, tc.*, 
                           (select count(*) as commenttype from task_comments where tc_id not in 
                           (select tc_id from read_comments where username ='" + Session["admin"].ToString() + @"')
                           and task_comments.deleted <> 1 and task_id =t.task_id)
                           from tasks t inner join task_comments tc on 
                           t.task_id = tc.task_id and tc.post_date=(select top 1 post_date from task_comments where tc.task_id = task_id and deleted<>1 
                           order by post_date desc)" + filter.Replace("deleted <> 1", "t.deleted <> 1") + " " + lblOrderBy.Text;
                    ds = DatabaseHelper.getDataset(sql);
                    ds = NewformatTaskDataSet(ds);
                }catch(Exception ex)
                {}
                try
                {
                    DataSet NoofRecords = DatabaseHelper.getDataset(@"Select t.*, tc.*, 
                           (select count(*) as commenttype from task_comments where tc_id not in 
                           (select tc_id from read_comments where username ='" + Session["admin"].ToString() + @"')
                           and task_comments.deleted <> 1 and task_id =t.task_id)
                           from tasks t inner join task_comments tc on 
                           t.task_id = tc.task_id and tc.post_date=(select top 1 post_date from task_comments where tc.task_id = task_id and deleted<>1 
                           order by post_date desc)" + filter.Replace("deleted <> 1", "t.deleted <> 1") + " " + lblOrderBy.Text);
                    if (NoofRecords != null && NoofRecords.Tables.Count > 0 && NoofRecords.Tables[0].Rows.Count > 0)
                    {
                        LblRecordsno.Text = NoofRecords.Tables[0].Rows.Count.ToString();
                    }
                    else
                    {
                        LblRecordsno.Text = "0";
                    }
                }catch(Exception ex)
                {}
                //========================================================================================================================


            }
        }

        if (Session["OverdueTask"] == null || Session["OverdueTask"].ToString() == "")
        {
        }
        else
        {

            //====================================================================================================================================
            string strduedate = "";
            if (Session["AllOverdueTask"].ToString() == "True")
            {
                //========================================================================================================================================================
                //sql = "select * from tasks where deleted <> 1 and status <> 'checked' and status <> 'closed'and status <> 'parked' and DueDate <> '" + strduedate + "'";
                try
                {
                    sql = startquery;
                    sql += @" t.*, tc.*,
                       (select count(*) as commenttype from task_comments where tc_id not in 
                       (select tc_id from read_comments where username ='" + Session["admin"].ToString() + @"')
                       and task_comments.deleted <> 1 and task_id =t.task_id)
                       from tasks t inner join task_comments tc on  t.task_id = tc.task_id and 
                       tc.post_date=(select top 1 post_date from task_comments where tc.task_id = task_id and deleted<>1 
                       order by post_date desc)where  status <> 'closed' and status <> 'Checked' and status <>'parked'
                       and  t.deleted <> 1  and DueDate <> '" + strduedate + "'";
                    ds = DatabaseHelper.getDataset(sql);
                    ds = NewformatTaskDataSet(ds);
                }catch(Exception Ex)
                {}
                try
                {
                    DataSet NoofRecords = DatabaseHelper.getDataset(@"Select t.*, tc.*,
                       (select count(*) as commenttype from task_comments where tc_id not in 
                       (select tc_id from read_comments where username ='" + Session["admin"].ToString() + @"')
                       and task_comments.deleted <> 1 and task_id =t.task_id)
                       from tasks t inner join task_comments tc on  t.task_id = tc.task_id and 
                       tc.post_date=(select top 1 post_date from task_comments where tc.task_id = task_id and deleted<>1 
                       order by post_date desc)where  status <> 'closed' and status <> 'Checked' and status <>'parked'
                       and  t.deleted <> 1  and DueDate <> '" + strduedate + "'");
                    if (NoofRecords != null && NoofRecords.Tables.Count > 0 && NoofRecords.Tables[0].Rows.Count > 0)
                    {
                        LblRecordsno.Text = NoofRecords.Tables[0].Rows.Count.ToString();
                    }
                    else
                    {
                        LblRecordsno.Text = "0";
                    }
                }catch(Exception ex)
                {}
                //========================================================================================================================================================


            }
            else
            {
                //=====================================================================================================================================================================================================================================
                //sql = "select * from tasks where  status <> 'closed' and deleted <> 1 and status <> 'checked' and status <> 'parked' and assigned_to_user = '" + Session["admin"] + "' and  DueDate <> '" + strduedate + "'  " + lblOrderBy.Text;
                try
                {
                    sql = startquery;
                    sql += @" t.*, tc.*,
                       (select count(*) as commenttype from task_comments where tc_id not in 
                       (select tc_id from read_comments where username ='" + Session["admin"].ToString() + @"')
                       and task_comments.deleted <> 1 and task_id =t.task_id)
                       from tasks t inner join task_comments tc on  t.task_id = tc.task_id and
                       tc.post_date=(select top 1 post_date from task_comments where tc.task_id = task_id and deleted<>1 
                       order by post_date desc)where  status <> 'closed' and status <> 'Checked' and status <>'parked'
                       and  t.deleted <> 1  and assigned_to_user = '" + Session["admin"] + "' and DueDate <> '" + strduedate + "' ORDER BY last_updated_date desc";
                    ds = DatabaseHelper.getDataset(sql);
                    ds = NewformatTaskDataSet(ds);
                }catch(Exception ex)
                {}
                try
                {
                    DataSet NoofRecords = DatabaseHelper.getDataset(@"Select t.*, tc.*,
                       (select count(*) as commenttype from task_comments where tc_id not in 
                       (select tc_id from read_comments where username ='" + Session["admin"].ToString() + @"')
                       and task_comments.deleted <> 1 and task_id =t.task_id)
                       from tasks t inner join task_comments tc on  t.task_id = tc.task_id and
                       tc.post_date=(select top 1 post_date from task_comments where tc.task_id = task_id and deleted<>1 
                       order by post_date desc)where  status <> 'closed' and status <> 'Checked' and status <>'parked'
                       and  t.deleted <> 1  and assigned_to_user = '" + Session["admin"] + "' and DueDate <> '" + strduedate + "' ORDER BY last_updated_date desc");
                    if (NoofRecords != null && NoofRecords.Tables.Count > 0 && NoofRecords.Tables[0].Rows.Count > 0)
                    {
                        LblRecordsno.Text = NoofRecords.Tables[0].Rows.Count.ToString();
                    }
                    else
                    {
                        LblRecordsno.Text = "0";
                    }
                }
                catch(Exception Ex)
                {}
                //=====================================================================================================================================================================================================================================          
            }



        }

        //======================================================================================================
        //DataSet ds = DatabaseHelper.getDataset(sql);
        //=======================================================================================================
        if (Session["OverdueTask"] == null || Session["OverdueTask"].ToString() == "")
        {


        }
        else
        {
            if (Session["OverdueTask"].ToString() == "True")
            {
                try
                {
                    //rohit: removed for ttesting
                    //////if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    //////{
                    //////    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    //////    {
                    //////        DateTime crrntdate = DateTime.Now.Date;
                    //////        string djgfjs = ds.Tables[0].Rows[i]["DueDate"].ToString();
                    //////        if (ds.Tables[0].Rows[i]["DueDate"].ToString() != null && ds.Tables[0].Rows[i]["DueDate"].ToString() != "")
                    //////        {
                    //////            string strduedate = DateTime.Parse(ds.Tables[0].Rows[i]["DueDate"].ToString()).ToString("dd/MM/yyyy");
                    //////            duedate = DateTime.Parse(strduedate.ToString());
                    //////            if (DateTime.Compare(duedate, crrntdate) < 0)
                    //////            {

                    //////                //j++;

                    //////            }
                    //////            else
                    //////            {
                    //////                ds.Tables[0].Rows.RemoveAt(i);
                    //////                i--;
                    //////                continue;
                    //////            }

                    //////        }
                    //////        else
                    //////        {
                    //////            ds.Tables[0].Rows.RemoveAt(i);
                    //////            i--;
                    //////            continue;
                    //////        }

                    //////    }


                    //////}

                }
                catch { }
            }


        }
        //===========================================================================================
        //ds = formatTaskDataSet(ds);
        //===========================================================================================
        if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "No task yet.";
            DataGrid1.DataSource = null;
            DataGrid1.DataBind();
            drpPerPage1.Visible = false;
            LblRecordsno.Text = "0";
            divTotalHrs.Visible = false;
            if (closecount != 0)
            {
                chkHideClosed.Checked = true;
                closecount = 0;
            }
            else
            {
                chkHideClosed.Checked = false;
            }
            hidecheck = false;
        }
        else
        {
            PagedDataSource objds = new PagedDataSource();
            objds.DataSource = ds.Tables[0].DefaultView;
            objds.AllowPaging = true;

            if (drpPerPage1.SelectedValue != "00")
                objds.PageSize = Convert.ToInt16(Session["pagesize"]);
            else
                objds.PageSize = ds.Tables[0].Rows.Count;
            //========================================================
             //LblRecordsno.Text = objds.DataSourceCount.ToString();
            //========================================================
            divMessage.InnerHtml = "";
            DataGrid1.DataSource = null;
            DataGrid1.DataBind();
            //DataGrid1.DataSource = ds.Tables[0];
            DataGrid1.DataSource = objds;
            try
            {
                DataGrid1.DataBind();
            }
            catch (Exception ex)
            { }
            //==============================
            setRowValue(ds);
            //==============================

            if (closecount != 0)
            {
                chkHideClosed.Checked = true;
                closecount = 0;
            }
            else
            {
                chkHideClosed.Checked = false;
            }


            //if (drpPerPage1.SelectedValue != "00")
            //    DataGrid1.PageSize = Convert.ToInt16(Session["pagesize"]);
            //else
            //    DataGrid1.PageSize = ds.Tables[0].Rows.Count;

            getTotalHrsReport("select task_id from tasks " + filter);
            divTotalHrs.Visible = true;
            //hidecheck = false;


        }
    }
    private void setRowValue(DataSet ds)
    {
        //===================================================================================
        try
        {
            bool chkbool;
            chkbool = DatabaseHelper.can_Delete_Tasks(Session["admin"].ToString());
            
            //***************************************************
            //for (int i = 0; i < DataGrid1.Rows.Count; i++)
            for (int i = 0; i < DataGrid1.Items.Count; i++)
            //***************************************************
            {
                string countvalue = ds.Tables[0].Rows[i]["column1"].ToString();
                LinkButton lnkbtn = (LinkButton)DataGrid1.Items[i].FindControl("lnkReadMark");
                if (Convert.ToInt16(countvalue) > 0)
                {
                    lnkbtn.Text = "[<span style='color:red'>Unread.</span>  Mark as read]";
                }
                else
                {
                    lnkbtn.Text = "[<span style='color:green'>Read.</span> Mark as unread]";
                }

                CheckBox ChkTasks = (CheckBox)DataGrid1.Items[i].FindControl("chkSelect");

                LinkButton deleteButton = (LinkButton)DataGrid1.Items[i].FindControl("lnkdel");
                deleteButton.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete this task?')");

                if (!chkbool)
                {
                    deleteButton.Visible = false;
                    ChkTasks.Visible = false;
                }
                else
                {
                    deleteButton.Visible = true;
                    ChkTasks.Visible = true;
                }
            }
        }catch(Exception Ex)
        {}
        //=================================================================================
   
    }
    private DataSet NewformatTaskDataSet(DataSet ds)
    {
        try
        {

            ds.Tables[0].Columns.Add("ShowAllComment");
            //****************************************************************************************************************************
            //============================================================================================================================
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {

                    if (ds.Tables[0].Rows[i]["short_desc"] != null && ds.Tables[0].Rows[i]["short_desc"].ToString().Trim() != "")
                    {
                        ds.Tables[0].Rows[i]["short_desc"] = "<a  href=\"edit_task.aspx?id=" + ds.Tables[0].Rows[i]["task_id"].ToString() + "  \">" + ds.Tables[0].Rows[i]["short_desc"].ToString() + "</a>";

                    }
                    else
                    {
                        ds.Tables[0].Rows[i]["short_desc"] = "";

                    }

                    try
                    {
                        string LastComment = "";

                        if (ds.Tables[0].Rows[i]["qflag"].ToString() == "1")
                        {

                            LastComment += "<span style='color:Red'>" + "Question posted by " + ds.Tables[0].Rows[i]["username"].ToString() + " for  " + ds.Tables[0].Rows[i]["QuesTo"].ToString() + " on " + DateTime.Parse(ds.Tables[0].Rows[i]["post_date"].ToString()).ToString("dd MMM yyyy h:mm tt") + "</span>";

                            LastComment += "<br/><span>" + ds.Tables[0].Rows[i]["comment"].ToString() + "</span>";

                            LastComment += "<span style=\"border-bottom: solid 1px green; margin-bottom: 2px; height:1px\"></span><br/><hr />";

                        }
                        else
                        {

                            LastComment += "<span style='color:Green'>" + "Comment " + ds.Tables[0].Rows[i]["tc_id"].ToString() + " posted by " + ds.Tables[0].Rows[i]["username"].ToString() + " on " + DateTime.Parse(ds.Tables[0].Rows[i]["post_date"].ToString()).ToString("dd MMM yyyy h:mm tt") + "</span>";

                            LastComment += "<br/><span>" + ds.Tables[0].Rows[i]["comment"].ToString() + "</span>";

                            LastComment += "<span style=\"border-bottom: solid 1px green; margin-bottom: 2px; height:1px\"></span><br/><hr />";

                        }
                        ds.Tables[0].Rows[i]["ShowAllComment"] = LastComment.ToString();
                    }
                    catch
                    {

                    }

                }
                catch { }
            }


            //============================================================================================================================
            //****************************************************************************************************************************

            //=========================================================================================================================================
            //=========================================================================================================================================


            ////string strTaskId = "";
            ////for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            ////{
            ////    try
            ////    {
            ////        if (ds.Tables[0].Rows[i]["short_desc"] != null && ds.Tables[0].Rows[i]["short_desc"].ToString().Trim() != "")
            ////        {
            ////            ds.Tables[0].Rows[i]["short_desc"] = "<a  href=\"edit_task.aspx?id=" + ds.Tables[0].Rows[i]["task_id"].ToString() + "  \">" + ds.Tables[0].Rows[i]["short_desc"].ToString() + "</a>";
            ////        }
            ////        else
            ////        {
            ////            ds.Tables[0].Rows[i]["short_desc"] = "";
            ////        }
            ////        try
            ////        {
            ////            if (strTaskId == "")
            ////            {
            ////                strTaskId = ds.Tables[0].Rows[i]["task_id"].ToString() + ",";
            ////            }
            ////            else
            ////            {
            ////                strTaskId += ds.Tables[0].Rows[i]["task_id"].ToString() + ",";
            ////            }
            ////        }
            ////        catch
            ////        { }
            ////    }
            ////    catch { }
            ////}
            ////string LastComment = "";
            ////string strlistofTaskid = "";
            ////strlistofTaskid = strTaskId.TrimEnd(new char[] { ',' });
            ////sql = "select a.* from task_comments a where a.task_id in(" + strlistofTaskid + ") and  a.tc_id IN (select top 1 b.tc_id from task_comments b where b.task_id = a.task_id order by b.post_date desc)";
            //////testing by rohit
            ////DataSet dslastcomment = DatabaseHelper.getDataset(sql);
            ////DataView dv = new DataView(dslastcomment.Tables[0]);
            ////for (int icount = 0; icount < ds.Tables[0].Rows.Count; icount++)
            ////{
            ////    LastComment = "";
            ////    if (dslastcomment != null && dslastcomment.Tables[0].Rows.Count > 0)
            ////    {
            ////        dv.RowFilter = "task_id='" + ds.Tables[0].Rows[icount]["task_id"].ToString() + "'";
            ////        if (dv[0]["qflag"].ToString() == "1")
            ////        {

            ////            LastComment += "<span style='color:Red'>" + "Question posted by " + dv[0]["username"].ToString() + " for  " + dv[0]["QuesTo"].ToString() + " on " + DateTime.Parse(dv[0]["post_date"].ToString()).ToString("dd MMM yyyy h:mm tt") + "</span>";

            ////            LastComment += "<br/><span>" + dv[0]["comment"].ToString() + "</span>";

            ////            LastComment += "<span style=\"border-bottom: solid 1px green; margin-bottom: 2px; height:1px\"></span><br/><hr />";

            ////        }
            ////        else
            ////        {

            ////            LastComment += "<span style='color:Green'>" + "Comment " + dv[0]["tc_id"].ToString() + " posted by " + dv[0]["username"].ToString() + " on " + DateTime.Parse(dv[0]["post_date"].ToString()).ToString("dd MMM yyyy h:mm tt") + "</span>";

            ////            LastComment += "<br/><span>" + dv[0]["comment"].ToString() + "</span>";

            ////            LastComment += "<span style=\"border-bottom: solid 1px green; margin-bottom: 2px; height:1px\"></span><br/><hr />";
            ////        }
            ////    }
            ////    ds.Tables[0].Rows[icount]["ShowAllComment"] = LastComment.ToString();
            ////}
            //=======================================================================================================================================
            //=========================================================================================================================================


        }
        catch (Exception ex)
        {
            Server.ClearError();
            ///Response.Redirect("fhs.aspx");
        }
        return ds;
    }
    private DataSet formatTaskDataSet(DataSet ds)
    {
        try
        {

            ds.Tables[0].Columns.Add("ShowAllComment");


            //=============================================================================================================================
            //=============================================================================================================================

            ////for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            ////{
            ////    try
            ////    {

            ////        if (ds.Tables[0].Rows[i]["short_desc"] != null && ds.Tables[0].Rows[i]["short_desc"].ToString().Trim() != "")
            ////        {
            ////            ds.Tables[0].Rows[i]["short_desc"] = "<a  href=\"edit_task.aspx?id=" + ds.Tables[0].Rows[i]["task_id"].ToString() + "  \">" + ds.Tables[0].Rows[i]["short_desc"].ToString() + "</a>";

            ////        }
            ////        else
            ////        {
            ////            ds.Tables[0].Rows[i]["short_desc"] = "";

            ////        }
            ////        try
            ////        {

            ////            string LastComment = "";

            ////            sql = "select top 1.* from task_comments where task_comments.deleted <> 1  and task_id =" + ds.Tables[0].Rows[i]["task_id"].ToString() + " order by tc_id desc";

            ////            //testing by rohit
            ////            DataSet dslastcomment = DatabaseHelper.getDataset(sql);

            ////            if (dslastcomment != null && dslastcomment.Tables[0].Rows.Count > 0)
            ////            {

            ////                // for (int j = 0; j < dslastcomment.Tables[0].Rows.Count; j++)
            ////                for (int j = 0; j < 1; j++)
            ////                {

            ////                    if (dslastcomment.Tables[0].Rows[j]["qflag"].ToString() == "1")
            ////                    {

            ////                        LastComment += "<span style='color:Red'>" + "Question posted by " + dslastcomment.Tables[0].Rows[j]["username"].ToString() + " for  " + dslastcomment.Tables[0].Rows[j]["QuesTo"].ToString() + " on " + DateTime.Parse(dslastcomment.Tables[0].Rows[j]["post_date"].ToString()).ToString("dd MMM yyyy h:mm tt") + "</span>";

            ////                        LastComment += "<br/><span>" + dslastcomment.Tables[0].Rows[j]["comment"].ToString() + "</span>";

            ////                        LastComment += "<span style=\"border-bottom: solid 1px green; margin-bottom: 2px; height:1px\"></span><br/><hr />";

            ////                    }
            ////                    else
            ////                    {

            ////                        LastComment += "<span style='color:Green'>" + "Comment " + dslastcomment.Tables[0].Rows[j]["tc_id"].ToString() + " posted by " + dslastcomment.Tables[0].Rows[j]["username"].ToString() + " on " + DateTime.Parse(dslastcomment.Tables[0].Rows[j]["post_date"].ToString()).ToString("dd MMM yyyy h:mm tt") + "</span>";

            ////                        LastComment += "<br/><span>" + dslastcomment.Tables[0].Rows[j]["comment"].ToString() + "</span>";

            ////                        LastComment += "<span style=\"border-bottom: solid 1px green; margin-bottom: 2px; height:1px\"></span><br/><hr />";

            ////                    }

            ////                }
            ////            }

            ////            ds.Tables[0].Rows[i]["ShowAllComment"] = LastComment.ToString();
            ////        }
            ////        catch
            ////        {

            ////        }

            ////    }
            ////    catch { }
            ////}
            //=============================================================================================================================
            //=============================================================================================================================

            //=========================================================================================================================================
            //=========================================================================================================================================


            string strTaskId = "";
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {
                    if (ds.Tables[0].Rows[i]["short_desc"] != null && ds.Tables[0].Rows[i]["short_desc"].ToString().Trim() != "")
                    {
                        ds.Tables[0].Rows[i]["short_desc"] = "<a  href=\"edit_task.aspx?id=" + ds.Tables[0].Rows[i]["task_id"].ToString() + "  \">" + ds.Tables[0].Rows[i]["short_desc"].ToString() + "</a>";
                    }
                    else
                    {
                        ds.Tables[0].Rows[i]["short_desc"] = "";
                    }
                    try
                    {
                        if (strTaskId == "")
                        {
                            strTaskId = ds.Tables[0].Rows[i]["task_id"].ToString() + ",";
                        }
                        else
                        {
                            strTaskId += ds.Tables[0].Rows[i]["task_id"].ToString() + ",";
                        }
                    }
                    catch
                    { }
                }
                catch { }
            }
            string LastComment = "";
            string strlistofTaskid = "";
            strlistofTaskid = strTaskId.TrimEnd(new char[] { ',' });
            sql = "select a.* from task_comments a where a.task_id in(" + strlistofTaskid + ") and  a.tc_id IN (select top 1 b.tc_id from task_comments b where b.task_id = a.task_id order by b.post_date desc)";
            //testing by rohit
            DataSet dslastcomment = DatabaseHelper.getDataset(sql);
            DataView dv = new DataView(dslastcomment.Tables[0]);
            for (int icount = 0; icount < ds.Tables[0].Rows.Count; icount++)
            {
                LastComment = "";
                if (dslastcomment != null && dslastcomment.Tables[0].Rows.Count > 0)
                {
                    dv.RowFilter = "task_id='" + ds.Tables[0].Rows[icount]["task_id"].ToString() + "'";
                    if (dv[0]["qflag"].ToString() == "1")
                    {

                        LastComment += "<span style='color:Red'>" + "Question posted by " + dv[0]["username"].ToString() + " for  " + dv[0]["QuesTo"].ToString() + " on " + DateTime.Parse(dv[0]["post_date"].ToString()).ToString("dd MMM yyyy h:mm tt") + "</span>";

                        LastComment += "<br/><span>" + dv[0]["comment"].ToString() + "</span>";

                        LastComment += "<span style=\"border-bottom: solid 1px green; margin-bottom: 2px; height:1px\"></span><br/><hr />";

                    }
                    else
                    {

                        LastComment += "<span style='color:Green'>" + "Comment " + dv[0]["tc_id"].ToString() + " posted by " + dv[0]["username"].ToString() + " on " + DateTime.Parse(dv[0]["post_date"].ToString()).ToString("dd MMM yyyy h:mm tt") + "</span>";

                        LastComment += "<br/><span>" + dv[0]["comment"].ToString() + "</span>";

                        LastComment += "<span style=\"border-bottom: solid 1px green; margin-bottom: 2px; height:1px\"></span><br/><hr />";
                    }
                }
                ds.Tables[0].Rows[icount]["ShowAllComment"] = LastComment.ToString();
            }
            //=======================================================================================================================================
            //=========================================================================================================================================


        }
        catch (Exception ex)
        {
            Server.ClearError();
            ///Response.Redirect("fhs.aspx");
        }
        return ds;
    }
    void load_dropdowns()
    {

        // projects
        try
        {

            // do a batch of sql statements
            DataSet ds_dropdowns;
            if (Cache["dropdowns"] == null)
            {
                sql = @"select project_name
		from projects
		where active = 'Y' order by project_name;";

                // categories
                sql += "\nselect category_name from categories order by sort_seq, category_name;";

                // priorities
                sql += "\nselect priority_name from priorities order by sort_seq, priority_name;";

                // statuses
                sql += "\nselect status_name from statuses order by sort_seq, status_name;";

                // users
                sql += "\nselect username from users where active = 'Y' order by username;";

                ds_dropdowns = DatabaseHelper.getDataset(sql);
                //  Cache["Company_notes"] = ds;
                Cache.Insert("dropdowns", ds_dropdowns, null, DateTime.Now.AddHours(6), TimeSpan.Zero);
            }
            else
            {
                ds_dropdowns = (DataSet)Cache["dropdowns"];
            }



            // DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);

            drpProjects.DataSource = ds_dropdowns.Tables[0];
            drpProjects.DataTextField = "project_name";
            drpProjects.DataValueField = "project_name";
            drpProjects.DataBind();
            drpProjects.Items.Insert(0, new ListItem("[no filter]", "0"));


            drpCategories.DataSource = ds_dropdowns.Tables[1];
            drpCategories.DataTextField = "category_name";
            drpCategories.DataValueField = "category_name";
            drpCategories.DataBind();
            drpCategories.Items.Insert(0, new ListItem("[no filter]", "0"));

            drpPriorities.DataSource = ds_dropdowns.Tables[2];
            drpPriorities.DataTextField = "priority_name";
            drpPriorities.DataValueField = "priority_name";
            drpPriorities.DataBind();
            drpPriorities.Items.Insert(0, new ListItem("[no filter]", "0"));
            DrpMultipleTaskPR.DataSource = ds_dropdowns.Tables[2];
            DrpMultipleTaskPR.DataTextField = "priority_name";
            DrpMultipleTaskPR.DataValueField = "priority_name";
            DrpMultipleTaskPR.DataBind();
            DrpMultipleTaskPR.Items.Insert(0, new ListItem("[no filter]", "0"));

            drpStatuses.DataSource = ds_dropdowns.Tables[3];
            drpStatuses.DataTextField = "status_name";
            drpStatuses.DataValueField = "status_name";
            drpStatuses.DataBind();
            drpStatuses.Items.Insert(0, new ListItem("[no filter]", "0"));
            DrpMultipleTaskStatus.DataSource = ds_dropdowns.Tables[3];
            DrpMultipleTaskStatus.DataTextField = "status_name";
            DrpMultipleTaskStatus.DataValueField = "status_name";
            DrpMultipleTaskStatus.DataBind();
            DrpMultipleTaskStatus.Items.Insert(0, new ListItem("[no filter]", "0"));

            drpReportedBy.DataSource = ds_dropdowns.Tables[4];
            drpReportedBy.DataTextField = "username";
            drpReportedBy.DataValueField = "username";
            drpReportedBy.DataBind();
            drpReportedBy.Items.Insert(0, new ListItem("[no filter]", "0"));

            drpAssignedTo.DataSource = ds_dropdowns.Tables[4];
            drpAssignedTo.DataTextField = "username";
            drpAssignedTo.DataValueField = "username";
            drpAssignedTo.DataBind();
            drpAssignedTo.Items.Insert(0, new ListItem("[no filter]", "0"));
            DrpMultipleTaskAssing.DataSource = ds_dropdowns.Tables[4];
            DrpMultipleTaskAssing.DataTextField = "username";
            DrpMultipleTaskAssing.DataValueField = "username";
            DrpMultipleTaskAssing.DataBind();
            DrpMultipleTaskAssing.Items.Insert(0, new ListItem("[no filter]", "0"));
        }
        catch { }
    }
    void load_filterDropDown()
    {
        drpFilter.Items.Clear();
        if (DatabaseHelper.isAdmin(Session["Admin"].ToString()))
        {
            sql = "select username from users where active='Y' and admin='Y' and username <> '" + Session["Admin"] + "' order by username";

            DataSet dsAdmin = DatabaseHelper.getDataset(sql);

            if (dsAdmin != null)
            {
                if (dsAdmin.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsAdmin.Tables[0].Rows.Count; i++)
                    {
                        drpFilter.Items.Add("Assigned by " + dsAdmin.Tables[0].Rows[i]["username"]);
                    }
                }
            }

            drpFilter.Items.Add("Assigned by " + Session["Admin"]);
            drpFilter.Items.Add("Assigned by me");
            drpFilter.Items.Add("All tasks by last updated");
            drpFilter.Items.Add("All emps CWO");

            drpFilter.Items.Add("Show my open tasks");

            drpFilter.SelectedValue = (string)DatabaseHelper.executeScalar("select default_selected_item from users where username='" + Session["admin"] + "'");
        }
        else
        {
            drpFilter.Items.Add("Show my open tasks");
            //drpFilter.Items.Add("All tasks by last updated");
        }
    }
    protected void drpFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["filter"] = null;
        Session["filterunread"] = null;
        load_dropdowns();
        setFileterSession();
        //**********************************
        DataGrid1.MasterTableView.CurrentPageIndex = 0;
        //************************************
        //DataGrid1.PageIndex = 0;
        
        BindData();

    }

    protected void drpProjects_SelectedIndexChanged(object sender, EventArgs e)
    {
        setFileterSession();
        
        //DataGrid1.PageIndex = 0;
        //**********************************
        DataGrid1.MasterTableView.CurrentPageIndex = 0;
        //************************************
        BindData();
    }

    protected void drpCategories_SelectedIndexChanged(object sender, EventArgs e)
    {
        setFileterSession();
        
        //DataGrid1.PageIndex = 0;
        //**********************************
        DataGrid1.MasterTableView.CurrentPageIndex = 0;
        //************************************
        BindData();
    }

    protected void drpReportedBy_SelectedIndexChanged(object sender, EventArgs e)
    {
        ///Session["filterunread"] = null;
        setFileterSession();
        
        //DataGrid1.PageIndex = 0;
        //**********************************
        DataGrid1.MasterTableView.CurrentPageIndex = 0;
        //************************************
        BindData();
    }

    protected void drpPriorities_SelectedIndexChanged(object sender, EventArgs e)
    {
        setFileterSession();
        //DataGrid1.PageIndex = 0;
        //**********************************
        DataGrid1.MasterTableView.CurrentPageIndex = 0;
        //************************************
        BindData();
    }

    protected void drpAssignedTo_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["filterunread"] = null;
        setFileterSession();
        //**********************************
        DataGrid1.MasterTableView.CurrentPageIndex = 0;
        //************************************
        //DataGrid1.PageIndex = 0;
        BindData();
    }

    protected void drpStatuses_SelectedIndexChanged(object sender, EventArgs e)
    {
        setFileterSession();
        //**********************************
        DataGrid1.MasterTableView.CurrentPageIndex = 0;
        //************************************
        //DataGrid1.PageIndex = 0;
        BindData();
    }

    private void setFileterSession()
    {

        Session["HideClosed"] = chkHideClosed.Checked;
        Session["IncludeDeleted"] = chkViewDeleted.Checked;
        Session["Filter"] = drpFilter.SelectedValue;
        Session["Project"] = drpProjects.SelectedValue;
        Session["Category"] = drpCategories.SelectedValue;
        Session["ReportedBy"] = drpReportedBy.SelectedValue;
        Session["Priority"] = drpPriorities.SelectedValue;
        Session["AssignedTo"] = drpAssignedTo.SelectedValue;
        Session["Status"] = drpStatuses.SelectedValue;
        Session["OrderBy"] = lblOrderBy.Text;
        Session["ReadTask"] = ChkReadTask.Checked;

    }

    private void getFilterSession()
    {
        try
        {
            if (Session["HideClosed"] != null)
                chkHideClosed.Checked = (bool)Session["HideClosed"];
            if (hidecheck == false)
            {
                chkHideClosed.Checked = false;
            }
            else
            {
                chkHideClosed.Checked = true;
            }
            if (Session["ReadTask"] != null)
                ChkReadTask.Checked = (bool)Session["ReadTask"];

            if (Session["IncludeDeleted"] != null)
                chkViewDeleted.Checked = (bool)Session["IncludeDeleted"];

            if (Session["Filter"] != null)
                drpFilter.SelectedValue = Session["Filter"].ToString();

            if (Session["Project"] != null)
                drpProjects.SelectedValue = Session["Project"].ToString();

            if (Session["Category"] != null)
                drpCategories.SelectedValue = Session["Category"].ToString();

            if (Session["ReportedBy"] != null)
                drpReportedBy.SelectedValue = Session["ReportedBy"].ToString();

            if (Session["Priority"] != null)
                drpPriorities.SelectedValue = Session["Priority"].ToString();

            if (Session["AssignedTo"] != null)
                drpAssignedTo.SelectedValue = Session["AssignedTo"].ToString();

            if (Session["Status"] != null)
                drpStatuses.SelectedValue = Session["Status"].ToString();

            if (Session["OrderBy"] != null)
                lblOrderBy.Text = Session["OrderBy"].ToString();
        }
        catch
        {

        }
    }





    protected void DataGrid1_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //DataGrid1.CurrentPageIndex = e.NewPageIndex;
        BindData();
    }



    void sendNotification(string strTaskId, string StrShrtDesc)
    {
        sql = "select email, EmailNotification,PR_EmailNotification ";
        sql += " from users, task_subscriptions ";
        sql += " where users.username = task_subscriptions.username ";
        sql += " and task_subscriptions.task_id =" + strTaskId.ToString() + " and task_subscriptions.username <> '" + Session["admin"].ToString() + "'";

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            string strHtml = generateAdminEmail(strTaskId, StrShrtDesc);

            string toEmails = "";


            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (ds.Tables[0].Rows[i]["EmailNotification"].ToString() == "Y")
                {

                    string reqPR = "";
                    string[] strPR = ds.Tables[0].Rows[i]["PR_EmailNotification"].ToString().Split(new char[] { ';' });
                    for (int j = 0; j < strPR.Length - 1; j++)
                    {
                        reqPR = strPR[j];
                        if (reqPR == "All")
                        {
                            toEmails += ds.Tables[0].Rows[i]["email"].ToString() + ";";
                        }
                        else if (reqPR == drpPriorities.SelectedValue)
                        {
                            toEmails += ds.Tables[0].Rows[i]["email"].ToString() + ";";
                        }

                    }


                }


            }

            toEmails = toEmails.TrimEnd(';');

            if (emailSubject == "")
            {
                emailSubject = "Task ID:" + strTaskId.ToString() + " was updated - " + StrShrtDesc.ToString().Trim() + " (Task ID:" + strTaskId.ToString() + ")";
            }
            ////if (chkEmailnotify.Checked)
            ////{
            bool flag = DatabaseHelper.sendEmailTasks(toEmails, emailSubject, strHtml);
            ////}
            ////else
            ////{
            ////}
        }
    }

    private string generateAdminEmail(string strTaskId, string StrShrtDesc)
    {
        DataSet dsTask = DatabaseHelper.getDataset("select * from tasks where task_id=" + strTaskId);
        string strBody = "";
        if (dsTask == null || dsTask.Tables.Count <= 0 || dsTask.Tables[0].Rows.Count <= 0)
        {
            /// divMessage.InnerHtml = "Task not found.<br><br><a href='tasks.aspx'>View tasks</a>";
        }
        else
        {
            strBody += "Task: <span style=\"color:red\">";

            strBody += task_action;

            strBody += " (<a style=\"color:red\" href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_task.aspx?id=" + strTaskId + "'>" + strTaskId + "</a>) </span> " + DateTime.Parse(dsTask.Tables[0].Rows[0]["last_updated_date"].ToString()).ToString("dd MMM h:mm tt") + "<br><br>";

            //////if (lblTaskId.Text != "New")
            //////{
            //////   /// string attribures = record_changes_for_email();

            //////    if (attribures.ToString().Trim() != "")
            //////    {
            //////        strBody += "The attributes below have been amended.<br>";
            //////        strBody += "<span style=\"color:red\">" + attribures + "</span><br>";
            //////    }
            //////}

            strBody += "<a href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_task.aspx?id=" + strTaskId + "'>" + StrShrtDesc.ToString().Trim() + "</a>&nbsp;&nbsp;&nbsp;(" + dsTask.Tables[0].Rows[0]["priority"].ToString() + " : " + dsTask.Tables[0].Rows[0]["status"].ToString() + ")<br><br>";
            strBody += "Emp: " + ((string)DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + dsTask.Tables[0].Rows[0]["assigned_to_user"].ToString() + "'")) + "<br>";
            strBody += "Project: " + dsTask.Tables[0].Rows[0]["project"].ToString() + "<br>";
            strBody += "Relevant URL: <a href='" + dsTask.Tables[0].Rows[0]["Relevant_URL"].ToString() + "' target='_blank'>" + dsTask.Tables[0].Rows[0]["Relevant_URL"].ToString() + "</a><br>";
            strBody += "<br><br>";

            sql = "Select * from task_comments where task_id=" + strTaskId.ToString() + " and deleted <> 1 order by tc_id desc";

            DataSet dsTaskDetails = DatabaseHelper.getDataset(sql);

            strBody += "<table cellspacing=\"0\" border=\"1\" style=\"width: 99%;border-collapse: collapse;\">";
            strBody += "<tbody>";

            for (int i = 0; i < dsTaskDetails.Tables[0].Rows.Count; i++)
            {
                strBody += "<tr>";
                strBody += "<td>";
                strBody += "<div style=\"border: 1px none Green;\">";
                strBody += "<table width=\"100%\">";
                strBody += "<tbody>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                if (dsTaskDetails.Tables[0].Rows[i]["qflag"].ToString() == "1")
                {
                    strBody += "<span style=\"color:red;\">Question posted by " + dsTaskDetails.Tables[0].Rows[i]["username"].ToString() + " on " + DateTime.Parse(dsTaskDetails.Tables[0].Rows[i]["post_date"].ToString()).ToString("dd MMM h:mm tt") + "</span>";
                }
                else
                {
                    strBody += "<span style=\"color: Green;\">comment " + dsTaskDetails.Tables[0].Rows[i]["tc_id"].ToString() + " posted by " + dsTaskDetails.Tables[0].Rows[i]["username"].ToString() + " on " + DateTime.Parse(dsTaskDetails.Tables[0].Rows[i]["post_date"].ToString()).ToString("dd MMM h:mm tt") + "</span>";
                }
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\"> </td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span>" + dsTaskDetails.Tables[0].Rows[i]["comment"].ToString() + "</span>";
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "</tbody>";
                strBody += "</table>";
                strBody += "</div>";
                strBody += "</td>";
                strBody += "</tr>";
            }
            strBody += "</tbody>";
            strBody += "</table>";
        }
        return strBody;
    }

    protected void btnClearFilters_Click(object sender, EventArgs e)
    {
        Session["filter"] = null;
        Session["filterunread"] = null;
        load_dropdowns();
        load_filterDropDown();
        chkViewDeleted.Checked = false;
        chkHideClosed.Checked = false;
        ChkReadTask.Checked = true;
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        //if (DatabaseHelper.isAdmin(Session["admin"].ToString()))
        //{
        //    drpFilter.SelectedValue = "All tasks";
        //}
        //else
        //{
        //    drpFilter.SelectedValue = "Show my open tasks";
        //}
        setFileterSession();
        BindData();
    }




    void bindNewTask()
    {
        load_dropdowns();

        load_filterDropDown();

        chkViewDeleted.Checked = false;

        lblOrderBy.Text = " ORDER BY last_updated_date desc";

        if (DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            drpFilter.SelectedValue = (string)DatabaseHelper.executeScalar("select default_selected_item from users where username='" + Session["admin"] + "'");
            //drpFilter.SelectedValue = "All tasks";
        }
        else
        {
            drpFilter.SelectedValue = "Show my open tasks";
        }
        drpStatuses.SelectedValue = "new";
        // drpPriorities.SelectedIndex = 1;
        setFileterSession();
        //DataGrid1.CurrentPageIndex = 0;
        //DataGrid1.PageIndex = 0;
        //**********************************
        DataGrid1.MasterTableView.CurrentPageIndex = 0;
        //************************************
        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        BindData();

    }

    void bindCrntWrkingTasks()
    {
        load_dropdowns();

        load_filterDropDown();

        chkViewDeleted.Checked = false;

        lblOrderBy.Text = " ORDER BY last_updated_date desc";

        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            drpFilter.SelectedValue = "Show my open tasks";
        }
        else
        {

            drpFilter.SelectedValue = "All emps CWO";

        }
        if (Session["filter"].ToString() == "CWO")
        {
            drpStatuses.SelectedValue = "CWO";
        }
        else
        {
            drpStatuses.SelectedValue = "CWO/CWO but away";
        }
        ///drpPriorities.SelectedIndex = 1;
        setFileterSession();
        //DataGrid1.CurrentPageIndex = 0;

        //DataGrid1.PageIndex = 0;
        //**********************************
        DataGrid1.MasterTableView.CurrentPageIndex = 0;
        //************************************
        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        BindData();

    }
    void bindPR0Tasks()
    {
        load_dropdowns();

        load_filterDropDown();

        chkViewDeleted.Checked = false;

        lblOrderBy.Text = " ORDER BY last_updated_date desc";

        drpFilter.SelectedValue = "Show my open tasks";

        drpPriorities.SelectedIndex = 1;
        setFileterSession();
        // DataGrid1.CurrentPageIndex = 0;
        //DataGrid1.PageIndex = 0;
        //**********************************
        DataGrid1.MasterTableView.CurrentPageIndex = 0;
        //************************************

        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;

        BindData();
    }

    void bindTskNewComment()
    {
        load_dropdowns();

        load_filterDropDown();

        chkViewDeleted.Checked = false;

        lblOrderBy.Text = " ORDER BY last_updated_date desc";

        drpFilter.SelectedValue = "Show my open tasks";

        drpPriorities.SelectedIndex = 2;
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        setFileterSession();
        //DataGrid1.CurrentPageIndex = 0;
        //DataGrid1.PageIndex = 0;
        //**********************************
        DataGrid1.MasterTableView.CurrentPageIndex = 0;
        //************************************
        BindData();
    }

    void bindPR1aTasks()
    {
        load_dropdowns();

        load_filterDropDown();

        chkViewDeleted.Checked = false;

        lblOrderBy.Text = " ORDER BY last_updated_date desc";

        drpFilter.SelectedValue = "Show my open tasks";

        drpPriorities.SelectedIndex = 2;
        setFileterSession();
        // DataGrid1.CurrentPageIndex = 0;
        //DataGrid1.PageIndex = 0;
        //**********************************
        DataGrid1.MasterTableView.CurrentPageIndex = 0;
        //************************************
        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        BindData();
    }

    void bindPR1cTasks()
    {

        load_dropdowns();

        load_filterDropDown();

        chkViewDeleted.Checked = false;

        lblOrderBy.Text = " ORDER BY last_updated_date desc";

        drpFilter.SelectedValue = "Show my open tasks";

        drpPriorities.SelectedIndex = 4;
        setFileterSession();
        //DataGrid1.CurrentPageIndex = 0;
        //DataGrid1.PageIndex = 0;
        //**********************************
        DataGrid1.MasterTableView.CurrentPageIndex = 0;
        //************************************
        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        BindData();
    }
    void bind2NTasks()
    {
        load_dropdowns();

        load_filterDropDown();

        chkViewDeleted.Checked = false;

        lblOrderBy.Text = " ORDER BY last_updated_date desc";

        drpFilter.SelectedValue = "Show my open tasks";

        drpPriorities.SelectedIndex = 5;
        setFileterSession();
        //DataGrid1.CurrentPageIndex = 0;
        //DataGrid1.PageIndex = 0;
        //**********************************
        DataGrid1.MasterTableView.CurrentPageIndex = 0;
        //************************************
        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        BindData();
    }
    void bindPR1bTasks()
    {
        load_dropdowns();

        load_filterDropDown();

        chkViewDeleted.Checked = false;

        lblOrderBy.Text = " ORDER BY last_updated_date desc";

        drpFilter.SelectedValue = "Show my open tasks";

        drpPriorities.SelectedIndex = 3;
        setFileterSession();
        // DataGrid1.CurrentPageIndex = 0;
        //DataGrid1.PageIndex = 0;
        //**********************************
        DataGrid1.MasterTableView.CurrentPageIndex = 0;
        //************************************
        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        BindData();
    }
    void bindtobeanswer()
    {
        load_dropdowns();

        load_filterDropDown();

        drpFilter.SelectedValue = "Show my open tasks";

        chkViewDeleted.Checked = false;

        sql = @"select distinct tasks.* from task_comments,tasks 
                where tasks.task_id = task_comments.task_id and tasks.deleted <> 1 and task_comments.deleted <> 1 and qflag=1 ";

        //if (!DatabaseHelper.can_Show_Closed_Tasks(Session["admin"].ToString()))
        //{
        //sql += " and status <> 'closed' and task_comments.username = 'M' ";
        sql += " and status <> 'closed' and task_comments.username = '" + Session["admin"] + "' ";
        //}

        //sql += " and task_comments.QuesTo = '" + Session["admin"] + "' ";

        DataSet ds = DatabaseHelper.getDataset(sql);
        ds = formatTaskDataSet(ds);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            TopFilterLable.InnerHtml = "Filtered by Unanswered questions";
            //  PagedDataSource objds = new PagedDataSource();
            //  objds.DataSource = ds.Tables[0].DefaultView;

            LblRecordsno.Text = ds.Tables[0].Rows.Count.ToString();//objds.DataSourceCount.ToString();
            //   DataGrid1.CurrentPageIndex = 0;
            DataGrid1.DataSource = ds.Tables[0];
            DataGrid1.DataBind();
        }
        else
        {
            LblRecordsno.Text = "0";
            /// lnkUnansweredQuestions.Text = "(0)";
            /// unansweredque.Visible = false;
        }
    
    
    }

    void bindUnansweredQuestions()
    {
        load_dropdowns();

        load_filterDropDown();

        drpFilter.SelectedValue = "Show my open tasks";

        chkViewDeleted.Checked = false;

        sql = @"select distinct tasks.* from task_comments,tasks 
                where tasks.task_id = task_comments.task_id and tasks.deleted <> 1 and task_comments.deleted <> 1 and qflag=1 ";

        //if (!DatabaseHelper.can_Show_Closed_Tasks(Session["admin"].ToString()))
        //{
            sql += " and status <> 'closed' ";
        //}

        sql += " and task_comments.QuesTo = '" + Session["admin"] + "' ";

        DataSet ds = DatabaseHelper.getDataset(sql);
        ds = formatTaskDataSet(ds);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            TopFilterLable.InnerHtml = "Filtered by Unanswered questions";
            //  PagedDataSource objds = new PagedDataSource();
            //  objds.DataSource = ds.Tables[0].DefaultView;

            LblRecordsno.Text = ds.Tables[0].Rows.Count.ToString();//objds.DataSourceCount.ToString();
            //   DataGrid1.CurrentPageIndex = 0;
            DataGrid1.DataSource = ds.Tables[0];
            DataGrid1.DataBind();
        }
        else
        {
            LblRecordsno.Text = "0";
            /// lnkUnansweredQuestions.Text = "(0)";
            /// unansweredque.Visible = false;
        }
    }

    void bindDefault()
    {
        load_dropdowns();

        load_filterDropDown();

        lblOrderBy.Text = " ORDER BY last_updated_date desc";

        chkViewDeleted.Checked = false;





        if (DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            drpFilter.SelectedValue = (string)DatabaseHelper.executeScalar("select default_selected_item from users where username='" + Session["admin"] + "'");
            //drpFilter.SelectedValue = "All tasks";
        }
        else
        {
            drpFilter.SelectedValue = "Show my open tasks";
        }

        getFilterSession();

        BindData();
    }

    protected void chkViewDeleted_CheckedChanged(object sender, EventArgs e)
    {
        load_dropdowns();
        load_filterDropDown();
        lblOrderBy.Text = " ORDER BY last_updated_date desc";
        setFileterSession();
        BindData();
    }
    protected void chkHideClosed_CheckedChanged(object sender, EventArgs e)
    {
        setFileterSession();
        BindData();
    }

    protected int getHoursTakenSoFar(string task_id)
    {
        sql = @"select sum(datediff(minute,started_date,finished_date))
                from hours_reporting
                where finished_date is not null and task_id=" + task_id;

        int hrsTaken = 0;
        object objHrsTaken = DatabaseHelper.executeScalar(sql);
        if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
        {
            hrsTaken += (int)objHrsTaken;
        }
        else
        {
            hrsTaken = 0;
        }

        sql = @"select sum(datediff(minute,started_date,getdate()))
                from hours_reporting
                where finished_date is null and task_id=" + task_id;

        objHrsTaken = DatabaseHelper.executeScalar(sql);

        if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
        {
            hrsTaken += (int)objHrsTaken;
        }

        return hrsTaken;
    }

    protected void getTotalHrsReport(string strSql)
    {
        sql = @"select sum(ETC)
                from tasks
                where task_id in (" + strSql + ")";

        int ETC = 0;

        object objETC = DatabaseHelper.executeScalar(sql);

        if (objETC != null && objETC.ToString() != "" && objETC.ToString() != "0")
        {
            ETC += (int)objETC;
        }
        else
        {
            ETC = 0;
        }

        sql = @"select sum(datediff(minute,started_date,finished_date))
                from hours_reporting
                where finished_date is not null and task_id in (" + strSql + ")";

        int hrsTaken = 0;
        object objHrsTaken = DatabaseHelper.executeScalar(sql);
        if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
        {
            hrsTaken += (int)objHrsTaken;
        }
        else
        {
            hrsTaken = 0;
        }

        sql = @"select sum(datediff(minute,started_date,getdate()))
                from hours_reporting
                where finished_date is null and task_id in (" + strSql + ")";

        objHrsTaken = DatabaseHelper.executeScalar(sql);

        if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
        {
            hrsTaken += (int)objHrsTaken;
        }


        lblETC.Text = "" + (ETC / 60) + " hrs " + (ETC % 60) + " mins";

        lblTotalHrsTaken.Text = "" + (hrsTaken / 60) + " hrs " + (hrsTaken % 60) + " mins";

        int hrsLeft = ETC - hrsTaken;

        if (hrsLeft < 0)
        {
            hrsLeft = -1 * hrsLeft;
            lblExpectedHrsLeft.Text = "-" + (hrsLeft / 60) + " hrs " + (hrsLeft % 60) + " mins";
        }
        else
        {
            lblExpectedHrsLeft.Text = "" + (hrsLeft / 60) + " hrs " + (hrsLeft % 60) + " mins";
        }

    }

    protected void ChkReadTask_CheckedChanged(object sender, EventArgs e)
    {

        setFileterSession();
        BindData();

    }
    protected void LnkBtnToDeleteAll_Click(object sender, EventArgs e)
    {

        int i = 0;

        
        //*********************************************
        //GridDataItem dataItem = e.Item as GridDataItem;
        //foreach (GridViewRow row in DataGrid1.Rows)
        foreach (GridDataItem row in DataGrid1.Items)
        //*********************************************
        {
            // Access the CheckBox 
            try
            {
                CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                ///Label GetItemID = (Label)row.FindControl("lblID");
                if (cb != null && cb.Checked)
                {
                    i = i + 1;

                    //*******************************************************************************************************************************************************************************
                    //sql = "Update tasks set deleted=1,last_updated_date = getdate(),last_updated_user='" + Session["admin"].ToString().Replace("'", "''") + "' where task_id=" + row.Cells[19].Text;
                    sql = "Update tasks set deleted=1,last_updated_date = getdate(),last_updated_user='" + Session["admin"].ToString().Replace("'", "''") + "' where task_id=" + Convert.ToInt32(((Label)row.FindControl("lbltaskId")).Text);
                    //*******************************************************************************************************************************************************************************

                    int intResult = DatabaseHelper.executeNonQuery(sql);

                    if (intResult != 0)
                    {
                        //***********************************************************************************************************************************************************************************************************
                        //sql = @" insert into task_updates([task_id],[username],[post_date],[comment])values(" + row.Cells[19].Text + ",'" + Session["admin"].ToString().Replace("'", "''") + "',getdate(),'Task was deleted.'); ";
                        sql = @" insert into task_updates([task_id],[username],[post_date],[comment])values(" + Convert.ToInt32(((Label)row.FindControl("lbltaskId")).Text) + ",'" + Session["admin"].ToString().Replace("'", "''") + "',getdate(),'Task was deleted.'); ";
                        //***********************************************************************************************************************************************************************************************************
                        intResult = DatabaseHelper.executeNonQuery(sql);

                        try
                        {
                            //********************************************************************************************************************************************************************************************************
                            //sql = @"select *  from task_comments where task_id =" + row.Cells[19].Text + " and  tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "')";
                            sql = @"select *  from task_comments where task_id =" + Convert.ToInt32(((Label)row.FindControl("lbltaskId")).Text) + " and  tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "')";
                            //********************************************************************************************************************************************************************************************************
                            /// sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

                            DataSet dscomment = DatabaseHelper.getDataset(sql);

                            if (dscomment != null && dscomment.Tables[0].Rows.Count > 0)
                            {
                                for (int j = 0; j < dscomment.Tables[0].Rows.Count; j++)
                                {
                                    try
                                    {


                                        string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                                        sqlinsert += " values(" + dscomment.Tables[0].Rows[j]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                                        int intCommentResult = DatabaseHelper.executeNonQuery(sqlinsert);

                                    }
                                    catch
                                    {
                                    }
                                }

                            }
                        }
                        catch { }




                    }

                }
            }
            catch { }
        }
        if (i > 0)
        {
            Response.Redirect("tasks.aspx", false);
        }
        else
        {

            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('Please select task to delete.');</script>");
            /// lblMessage.Text = "Please enter date of birth.";
            return;

        }



    }
    protected void LnkBtnToChngPR_Click(object sender, EventArgs e)
    {

        int i = 0;
        //*********************************************
        //GridDataItem dataItem = e.Item as GridDataItem;
        //foreach (GridViewRow row in DataGrid1.Rows)
        foreach (GridDataItem row in DataGrid1.Items)
        //*********************************************
        {
            // Access the CheckBox 
            try
            {
                CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                if (cb != null && cb.Checked)
                {
                    i = i + 1;
                    //*********************************************************************************************************************************************************************************************************************************************
                    //sql = "Update tasks set priority='" + DrpMultipleTaskPR.SelectedItem.Text.Replace("'", "''") + "',last_updated_date =getdate(),last_updated_user='" + Session["admin"].ToString().Replace("'", "''") + "' where task_id=" + row.Cells[19].Text;
                    sql = "Update tasks set priority='" + DrpMultipleTaskPR.SelectedItem.Text.Replace("'", "''") + "',last_updated_date =getdate(),last_updated_user='" + Session["admin"].ToString().Replace("'", "''") + "' where task_id=" + Convert.ToInt32(((Label)row.FindControl("lbltaskId")).Text);
                    //*********************************************************************************************************************************************************************************************************************************************
                    int intResult = DatabaseHelper.executeNonQuery(sql);

                    if (intResult != 0)
                    {
                        string cmnt = "";
                        //**********************************************************************************************
                        //cmnt = "changed priority from " + row.Cells[8].Text + " to " + DrpMultipleTaskPR.SelectedItem;
                        cmnt = "changed priority from " + ((Label)row.FindControl("priorityvalue")).Text + " to " + DrpMultipleTaskPR.SelectedItem;
                        //**********************************************************************************************
                        
                        //******************************************************************************************************************************************************************************************************************
                        //sql = @" insert into task_updates([task_id],[username],[post_date],[comment])values(" + row.Cells[19].Text + ",'" + Session["admin"].ToString().Replace("'", "''") + "',getdate(),'" + cmnt.ToString() + "'); ";
                        sql = @" insert into task_updates([task_id],[username],[post_date],[comment])values(" + Convert.ToInt32(((Label)row.FindControl("lbltaskId")).Text) + ",'" + Session["admin"].ToString().Replace("'", "''") + "',getdate(),'" + cmnt.ToString() + "'); ";
                        
                        //******************************************************************************************************************************************************************************************************************
                        intResult = DatabaseHelper.executeNonQuery(sql);

                    }

                }
            }
            catch { }
        }
        if (i > 0)
        {
            Response.Redirect("tasks.aspx", false);
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('Please select task to change priority.');</script>");

            return;

        }



    }
    protected void LnkChngAssingedTo_Click(object sender, EventArgs e)
    {
        int i = 0;
        //*********************************************
          //GridDataItem dataItem = e.Item as GridDataItem;
        //foreach (GridViewRow row in DataGrid1.Rows)
          foreach (GridDataItem row in DataGrid1.Items)
        //*********************************************
        {
            // Access the CheckBox 
            try
            {
                CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                if (cb != null && cb.Checked)
                {
                    i = i + 1;
                    //*******************************************************************************************************************************************************************************************************************************************************************
                    
                    //sql = "Update tasks set assigned_to_user='" + DrpMultipleTaskAssing.SelectedItem.Text.Replace("'", "''") + "',last_updated_date =getdate(),last_updated_user='" + Session["admin"].ToString().Replace("'", "''") + "' where task_id=" + row.Cells[19].Text;
                    sql = "Update tasks set assigned_to_user='" + DrpMultipleTaskAssing.SelectedItem.Text.Replace("'", "''") + "',last_updated_date =getdate(),last_updated_user='" + Session["admin"].ToString().Replace("'", "''") + "' where task_id=" + Convert.ToInt32(((Label)row.FindControl("lbltaskId")).Text);
                    //*******************************************************************************************************************************************************************************************************************************************************************
                    int intResult = DatabaseHelper.executeNonQuery(sql);

                    if (intResult != 0)
                    {
                        string cmnt = "";
                        //*********************************************************************************************************************************************
                        //cmnt = "changed assinged to from " + row.Cells[9].Text + " to  " + DrpMultipleTaskAssing.SelectedItem.Text.Replace("'", "''");
                        cmnt = "changed assinged to from " + ((Label)row.FindControl("lblassignedtouser")).Text + " to  " + DrpMultipleTaskAssing.SelectedItem.Text.Replace("'", "''");
                        //*********************************************************************************************************************************************

                        //**************************************************************************************************************************************************************************************************************
                        //sql = @" insert into task_updates([task_id],[username],[post_date],[comment])values(" + row.Cells[19].Text + ",'" + Session["admin"].ToString().Replace("'", "''") + "',getdate(),'" + cmnt.ToString() + "'); ";
                        sql = @" insert into task_updates([task_id],[username],[post_date],[comment])values(" + Convert.ToInt32(((Label)row.FindControl("lbltaskId")).Text) + ",'" + Session["admin"].ToString().Replace("'", "''") + "',getdate(),'" + cmnt.ToString() + "'); ";
                        //**************************************************************************************************************************************************************************************************************
                        intResult = DatabaseHelper.executeNonQuery(sql);

                    }

                }
            }
            catch { }
        }
        if (i > 0)
        {
            Response.Redirect("tasks.aspx", false);
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('Please select task to change assigned to.');</script>");
            return;

        }

    }
    protected void LnkChngStatus_Click(object sender, EventArgs e)
    {

        int i = 0;
        //*********************************************
        //GridDataItem dataItem = e.Item as GridDataItem;
        //foreach (GridViewRow row in DataGrid1.Rows)
        foreach (GridDataItem row in DataGrid1.Items)
        //*********************************************
        {
            // Access the CheckBox 
            try
            {
                CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                if (cb != null && cb.Checked)
                {
                    i = i + 1;
                    //************************************************************************************************************************************************************************************************************************************************************************************************
                    //sql = "Update tasks set status='" + DrpMultipleTaskStatus.SelectedItem.Text.Replace("'", "''") + "',last_updated_date =getdate(),last_updated_user='" + Session["admin"].ToString().Replace("'", "''") + "' where task_id=" + row.Cells[19].Text;
                    sql = "Update tasks set status='" + DrpMultipleTaskStatus.SelectedItem.Text.Replace("'", "''") + "',last_updated_date =getdate(),last_updated_user='" + Session["admin"].ToString().Replace("'", "''") + "' where task_id=" + Convert.ToInt32(((Label)row.FindControl("lbltaskId")).Text);
                    //************************************************************************************************************************************************************************************************************************************************************************************************
                    int intResult = DatabaseHelper.executeNonQuery(sql);

                    if (intResult != 0)
                    {
                        string cmnt = "";
                        //*******************************************************************************************************************************************
                        //cmnt = "changed assinged to from " + row.Cells[9].Text + " to  " + DrpMultipleTaskAssing.SelectedItem.Text.Replace("'", "''");
                        cmnt = "changed assinged to from " + ((Label)row.FindControl("lblassignedtouser")).Text + " to  " + DrpMultipleTaskAssing.SelectedItem.Text.Replace("'", "''");
                        //*******************************************************************************************************************************************

                        //******************************************************************************************************************************************************************************************************************
                        //sql = @" insert into task_updates([task_id],[username],[post_date],[comment])values(" + row.Cells[19].Text + ",'" + Session["admin"].ToString().Replace("'", "''") + "',getdate(),'" + cmnt.ToString() + "'); ";
                        sql = @" insert into task_updates([task_id],[username],[post_date],[comment])values(" + Convert.ToInt32(((Label)row.FindControl("lbltaskId")).Text) + ",'" + Session["admin"].ToString().Replace("'", "''") + "',getdate(),'" + cmnt.ToString() + "'); ";
                        //******************************************************************************************************************************************************************************************************************
                        intResult = DatabaseHelper.executeNonQuery(sql);

                    }

                }
            }
            catch { }
        }
        if (i > 0)
        {
            Response.Redirect("tasks.aspx", false);
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('Please select task to change status.');</script>");

            return;

        }


    }
    protected void LnkBtnToMarkAllRead_Click(object sender, EventArgs e)
    {

        int i = 0;
        //*********************************************
        
        //foreach (GridViewRow row in DataGrid1.Rows)
        foreach (GridDataItem row in DataGrid1.Items)
        //*********************************************
        {
            try
            {
                CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                if (cb != null && cb.Checked)
                {
                    i = i + 1;
                    //****************************************************************************************************************************
                    //sql = "insert into read_task([task_id],[username])values(" + row.Cells[19].Text + ",'" + Session["admin"].ToString() + "')";
                    sql = "insert into read_task([task_id],[username])values(" + Convert.ToInt32(((Label)row.FindControl("lbltaskId")).Text) + ",'" + Session["admin"].ToString() + "')";
                    //****************************************************************************************************************************
                    DatabaseHelper.executeNonQuery(sql);
                    //*******************************************************************************************************************************************************
                    //sql = @"select *  from task_comments where task_id =" + row.Cells[19].Text + " and  tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "')";
                    sql = @"select *  from task_comments where task_id =" + Convert.ToInt32(((Label)row.FindControl("lbltaskId")).Text) + " and  tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "')";
                    //*******************************************************************************************************************************************************
                    /// sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

                    DataSet ds = DatabaseHelper.getDataset(sql);

                    if (ds != null && ds.Tables[0].Rows.Count > 0)
                    {
                        for (int j = 0; j < ds.Tables[0].Rows.Count; j++)
                        {
                            try
                            {


                                string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                                sqlinsert += " values(" + ds.Tables[0].Rows[j]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                                int intResult = DatabaseHelper.executeNonQuery(sqlinsert);

                            }
                            catch
                            {
                            }
                        }

                    }

                    Response.Redirect("tasks.aspx", false);
                }


            }

            catch { }
        }
        if (i > 0)
        {
            Response.Redirect("tasks.aspx", false);
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('Please select task to mark as read.');</script>");

            return;

        }


    }
    protected void LnkNotification_Click(object sender, EventArgs e)
    {
        Response.Redirect("Notification.aspx", false);
    }

    protected void DataGrid1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    void BindOverdueTask()
    {
        load_dropdowns();

        load_filterDropDown();

        chkViewDeleted.Checked = false;

        lblOrderBy.Text = " ORDER BY last_updated_date desc";

        drpFilter.SelectedValue = "Show my open tasks";

        drpPriorities.SelectedIndex = 4;
        setFileterSession();
        //DataGrid1.CurrentPageIndex = 0;
        Session["filterunread"] = "";
        Session["OverdueTask"] = "True";
        BindData();
    }


    //protected void DataGrid1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    //{

    //    //DataGrid1.PageIndex = e.NewPageIndex;
    //    BindData();
    //}
    
    protected void RadGrid1_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e) // similar to RowDataBound event in GridView 
    {
        foreach (TableCell tc in e.Item.Cells)
        {

            tc.Attributes["style"] = "border:1px solid #00A651;";


        }
        if (e.Item is GridDataItem)// gets the row collection  
        {
            
            GridDataItem dataItem = e.Item as GridDataItem;
            //***************************Hide Column**********************************************
            bool chkbool;
            chkbool = DatabaseHelper.can_Delete_Tasks(Session["admin"].ToString());
            if (chkbool == true)
            {
                ((CheckBox)dataItem.FindControl("chkSelect")).Visible = true;
                ((LinkButton)dataItem.FindControl("lnkdel")).Visible = true;
            }
            else
            {
                ((CheckBox)dataItem.FindControl("chkSelect")).Visible = false;
                ((LinkButton)dataItem.FindControl("lnkdel")).Visible = false;

            }
            //***************************Hide Column**********************************************
            if (((Label)dataItem.FindControl("statusvalue")).Text == "closed")
            {
               dataItem["StatusColor"].BackColor = Color.Red;
            }
            
            ((Label)e.Item.FindControl("lblReportedOn")).Text = DateTime.Parse(((Label)e.Item.FindControl("lblReportedOn")).Text).ToString("dd MMM yyyy h:mm tt").ToString();
            ((Label)e.Item.FindControl("lblLastUpdatedOn")).Text = DateTime.Parse(((Label)e.Item.FindControl("lblLastUpdatedOn")).Text).ToString("dd MMM yyyy h:mm tt").ToString();
            try
            {
                DataSet dsbackground;
                if (HttpContext.Current.Cache["BackgroundColor"] == null)
                {
                    //put in a cache
                    string sqlBackgrdcolor = @"select * from priorities";
                    dsbackground = DatabaseHelper.getDataset(sqlBackgrdcolor);
                    HttpContext.Current.Cache.Insert("BackgroundColor", dsbackground, null, DateTime.Now.AddMinutes(60), System.Web.Caching.Cache.NoSlidingExpiration);
                    DataView dv = new DataView(dsbackground.Tables[0]);
                    dv.RowFilter = "priority_name='" + ((Label)dataItem.FindControl("priorityvalue")).Text + "'";
                    dataItem.BackColor = System.Drawing.ColorTranslator.FromHtml(dv[0]["bg_color"].ToString());
                }
                else
                {
                    dsbackground = (DataSet)HttpContext.Current.Cache["BackgroundColor"];
                    DataView dvget = new DataView(dsbackground.Tables[0]);
                    dvget.RowFilter = "priority_name='" + ((Label)dataItem.FindControl("priorityvalue")).Text  + "'";
                    dataItem.BackColor = System.Drawing.ColorTranslator.FromHtml(dvget[0]["bg_color"].ToString());
                    
                }
            }
            catch (Exception ex)
            { }
            //===============================================================================================================


            try
            {
                //****************************************************************************
                TableCell cell = dataItem["ETC"];
                string itemValue = (cell.Controls[3] as Label).Text.ToString();
                //int ETC = Convert.ToInt32(((HiddenField)item.Cells[17].Controls[3]).Value);
                int ETC = Convert.ToInt32(itemValue);
                //****************************************************************************
                //***************************************************************************
                //item.Cells[13].Text = "" + (ETC / 60) + "' " + (ETC % 60) + '"';
                ((Label)dataItem.FindControl("lblETC")).Text = "" + (ETC / 60) + "' " + (ETC % 60) + '"';
                //***************************************************************************
                //===============================================================
                //int hrsTaken = getHoursTakenSoFar(e.Row.Cells[0].Text);
                int hrsTaken = getHoursTakenSoFar(((Label)dataItem.FindControl("lbltaskId")).Text);
                //===============================================================

                
                ((Label)dataItem.FindControl("lblTimetodat")).Text = "" + (hrsTaken / 60) + "' " + (hrsTaken % 60) + '"';
                //item.Cells[14].Text = "" + (hrsTaken / 60) + "' " + (hrsTaken % 60) + '"';
                int hrsLeft = ETC - hrsTaken;
                if (hrsLeft < 0)
                {
                    hrsLeft = -1 * hrsLeft;
                    //item.Cells[15].Text = "-" + (hrsLeft / 60) + "' " + (hrsLeft % 60) + '"';
                    ((Label)dataItem.FindControl("lblBalance")).Text = "-" + (hrsLeft / 60) + "' " + (hrsLeft % 60) + '"';
                }
                else
                {
                    //item.Cells[15].Text = "" + (hrsLeft / 60) + "' " + (hrsLeft % 60) + '"';
                    ((Label)dataItem.FindControl("lblBalance")).Text = "" + (hrsLeft / 60) + "' " + (hrsLeft % 60) + '"';
                }
            }
            catch (Exception ex)
            { }

                
            try
            {

                if (((Label)dataItem.FindControl("statusvalue")).Text == "closed")
                {
                    closecount++;
                }

            }
            catch { }
        }
        //else if (e.Row.RowType == DataControlRowType.Header)
        //else if (e.Item.ItemType == GridItemType.Header)
        else if(e.Item is GridHeaderItem)
        {
            try
            {
                //CheckBox chkheaderSelect = (CheckBox)e.Row.FindControl("chkheaderSelect");
                //CheckBox chkheaderSelect = ;
                GridHeaderItem headerItem = (GridHeaderItem)e.Item; 
                if (!DatabaseHelper.can_Delete_Tasks(Session["admin"].ToString()))
                {
                    //((CheckBox)e.Item.FindControl("chkheaderSelect")).Visible = false;
                    //header["chkheaderSelect"].Controls["0"].Visible=false;
                    //((CheckBox)headerItem["chkheaderSelect"].Controls[0]).Visible=false ;
                    ((CheckBox)headerItem.FindControl("chkheaderSelect")).Visible = false;
                }
                else
                {
                    //((CheckBox)e.Item.FindControl("chkheaderSelect")).Visible = true;
                    //header["chkheaderSelect"].Controls["0"].Visible = true;
                    //((CheckBox)headerItem["chkheaderSelect"].Controls[0]).Visible = true;
                    ((CheckBox)headerItem.FindControl("chkheaderSelect")).Visible = true;
                }

            }
            catch { }
        }
    }
    protected void DataGrid1_OnItemCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
    {
        //********************
        //GridViewRow row;
        //GridViewRow gvr;
        GridDataItem gvr, row;
        //*********************
        int rows;

        
        if (e.CommandName == "ReadMark")
        {
            //GridViewRow row = DataGrid1.Rows[];

            //***************************************************************
            //gvr = (GridViewRow)((LinkButton)e.CommandSource).NamingContainer;
            //rows = gvr.RowIndex;
            //row = DataGrid1.Rows[rows];
            
            gvr = (GridDataItem)((LinkButton)e.CommandSource).NamingContainer;
            rows = gvr.RowIndex;
            //row = DataGrid1.Items[rows];

            GridDataItem dataItem = e.Item as GridDataItem;
            
            //***************************************************************

            //********************************************************************
            //LinkButton readMarkButton = (LinkButton)row.FindControl("lnkReadMark");
            LinkButton readMarkButton = (LinkButton)dataItem.FindControl("lnkReadMark");
            //********************************************************************

            if (readMarkButton.Text == "[<span style='color:red'>Unread.</span>  Mark as read]")
            {
                //*******************************************************************************
                //sql = @"insert into read_task([task_id],[username])values(" + row.Cells[19].Text + ",'" + Session["admin"].ToString() + "')";
                sql = @"insert into read_task([task_id],[username])values(" + Convert.ToInt32(((Label)dataItem.FindControl("lbltaskId")).Text) + ",'" + Session["admin"].ToString() + "')";

                //*******************************************************************************
                DatabaseHelper.executeNonQuery(sql);


               //********************************************************************************
                //sql = @"select *  from task_comments where task_id =" + row.Cells[19].Text + " and  tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "')";
                sql = @"select *  from task_comments where task_id =" + Convert.ToInt32(((Label)dataItem.FindControl("lbltaskId")).Text) +" and  tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "')";

                
                //*************************************************************************
                /// sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

                DataSet ds = DatabaseHelper.getDataset(sql);

                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        try
                        {
                            string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                            sqlinsert += " values(" + ds.Tables[0].Rows[i]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                            int intResult = DatabaseHelper.executeNonQuery(sqlinsert);

                        }
                        catch
                        {
                        }
                    }

                }

                Response.Redirect("tasks.aspx", false);
            }
            else
            {

                //**********************************************************************************************************************
                //sql = @"delete from read_task where [task_id]=" + row.Cells[19].Text + " and [username]='" + Session["admin"] + "'";
                sql = @"delete from read_task where [task_id]=" + Convert.ToInt32(((Label)dataItem.FindControl("lbltaskId")).Text) + " and [username]='" + Session["admin"] + "'";

                //**********************************************************************************************************************
                DatabaseHelper.executeNonQuery(sql);
                //**********************************************************************************************************************
                //sql = @"select *  from task_comments where task_id =" + row.Cells[19].Text + " and  tc_id  in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "')";
                sql = @"select *  from task_comments where task_id =" + Convert.ToInt32(((Label)dataItem.FindControl("lbltaskId")).Text) + " and  tc_id  in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "')";
                //**********************************************************************************************************************
                DataSet ds = DatabaseHelper.getDataset(sql);

                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        try
                        {

                            string sqlinsert = "Delete from read_comments where tc_id =" + ds.Tables[0].Rows[i]["tc_id"].ToString() + " and username = '" + Session["admin"].ToString() + "'";
                            int intResult = DatabaseHelper.executeNonQuery(sqlinsert);

                        }
                        catch
                        {
                        }
                    }

                }

                Response.Redirect("tasks.aspx", false);
            }
        }

        else if (e.CommandName == "delete")
        {
            //******************************************************************
            //gvr = (GridViewRow)((LinkButton)e.CommandSource).NamingContainer;
            //rows = gvr.RowIndex;
            //row = DataGrid1.Rows[rows];


            gvr = (GridDataItem)((LinkButton)e.CommandSource).NamingContainer;
            rows = gvr.RowIndex;
            //row = DataGrid1.Items[rows];

            GridDataItem dataItem = e.Item as GridDataItem;

            //******************************************************************
            // LinkButton readMarkButton = (LinkButton)row.FindControl("lnkReadMark");

            //*******************************************************************************
            //sql = "update tasks set deleted=1 where task_id = " + row.Cells[19].Text + "; ";
            sql = "update tasks set deleted=1 where task_id = " + Convert.ToInt32(((Label)dataItem.FindControl("lbltaskId")).Text) + "; ";
            //*******************************************************************************

            //sql += " Update task_comments set deleted=1 where task_id=" + lblTaskId.Text;

            int intResult = DatabaseHelper.executeNonQuery(sql);

            if (intResult != 0)
            {
                task_action = ((string)DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + Session["admin"].ToString() + "'")) + " has deleted task ";

                //*******************************************************************************************************
                //emailSubject = "Task ID:" + row.Cells[19].Text + " was deleted - " + ((Label)row.FindControl("lblshrtDesc")).Text + " (Task ID:" + row.Cells[19].Text + ")";
                emailSubject = "Task ID:" + Convert.ToInt32(((Label)dataItem.FindControl("lbltaskId")).Text) + " was deleted - " + ((Label)dataItem.FindControl("lblshrtDesc")).Text + " (Task ID:" + ((Label)dataItem.FindControl("lbltaskId")).Text + ")";
                //sendNotification(row.Cells[19].Text, ((Label)row.FindControl("lblshrtDesc")).Text);
                //sql = @" insert into task_updates([task_id],[username],[post_date],[comment]) 
                            //values(" + row.Cells[19].Text + ",'" + Session["admin"].ToString().Replace("'", "''") + "',getdate(),'Task was deleted.'); ";

                sendNotification(((Label)dataItem.FindControl("lbltaskId")).Text.ToString(), ((Label)dataItem.FindControl("lblshrtDesc")).Text);
                sql = @" insert into task_updates([task_id],[username],[post_date],[comment]) 
                            values(" + Convert.ToInt32(((Label)dataItem.FindControl("lbltaskId")).Text) + ",'" + Session["admin"].ToString().Replace("'", "''") + "',getdate(),'Task was deleted.'); ";

                //*******************************************************************************************************
                intResult = DatabaseHelper.executeNonQuery(sql);
                try
                {
                    //*****************************************************************************************************
                    //sql = @"select *  from task_comments where task_id =" + row.Cells[19].Text + " and  tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "')";
                    sql = @"select *  from task_comments where task_id =" + Convert.ToInt32(((Label)dataItem.FindControl("lbltaskId")).Text) + " and  tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "')";
                    //*****************************************************************************************************
                    /// sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

                    DataSet dscomment = DatabaseHelper.getDataset(sql);

                    if (dscomment != null && dscomment.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < dscomment.Tables[0].Rows.Count; i++)
                        {
                            try
                            {


                                string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                                sqlinsert += " values(" + dscomment.Tables[0].Rows[i]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                                int intCommentResult = DatabaseHelper.executeNonQuery(sqlinsert);

                            }
                            catch
                            {

                            }
                        }



                    }
                }
                catch { }



                Response.Redirect("tasks.aspx");
            }



        }



    }


//    protected void DataGrid1_RowCommand(object sender, GridViewCommandEventArgs e)
//    {
//        //********************
//        //GridViewRow row;
//        //GridViewRow gvr;
//        GridDataItem gvr, row;
//        //*********************
//        int rows;
        

//        if (e.CommandName == "ReadMark")
//        {
//            //GridViewRow row = DataGrid1.Rows[];

//            //***************************************************************
//            //gvr = (GridViewRow)((LinkButton)e.CommandSource).NamingContainer;
//            //rows = gvr.RowIndex;
//            //row = DataGrid1.Rows[rows];

//            gvr = (GridDataItem)((LinkButton)e.CommandSource).NamingContainer;
//            rows = gvr.RowIndex;
//            row = DataGrid1.Items[rows];

//            //***************************************************************

            
//            LinkButton readMarkButton = (LinkButton)row.FindControl("lnkReadMark");

//            if (readMarkButton.Text == "[<span style='color:red'>Unread.</span>  Mark as read]")
//            {
//                sql = @"insert into read_task([task_id],[username]) 
//                        values(" + row.Cells[19].Text + ",'" + Session["admin"].ToString() + "')";
//                DatabaseHelper.executeNonQuery(sql);



//                sql = @"select *  from task_comments where task_id =" + row.Cells[19].Text + " and  tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "')";
//                /// sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

//                DataSet ds = DatabaseHelper.getDataset(sql);

//                if (ds != null && ds.Tables[0].Rows.Count > 0)
//                {
//                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
//                    {
//                        try
//                        {
//                            string sqlinsert = "insert into read_comments([tc_id],[username]) ";
//                            sqlinsert += " values(" + ds.Tables[0].Rows[i]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

//                            int intResult = DatabaseHelper.executeNonQuery(sqlinsert);

//                        }
//                        catch
//                        {
//                        }
//                    }

//                }

//                Response.Redirect("tasks.aspx", false);
//            }
//            else
//            {

//                sql = @"delete from read_task where [task_id]=" + row.Cells[19].Text + " and [username]='" + Session["admin"] + "'";
//                DatabaseHelper.executeNonQuery(sql);

//                sql = @"select *  from task_comments where task_id =" + row.Cells[19].Text + " and  tc_id  in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "')";
//                DataSet ds = DatabaseHelper.getDataset(sql);

//                if (ds != null && ds.Tables[0].Rows.Count > 0)
//                {
//                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
//                    {
//                        try
//                        {

//                            string sqlinsert = "Delete from read_comments where tc_id =" + ds.Tables[0].Rows[i]["tc_id"].ToString() + " and username = '" + Session["admin"].ToString() + "'";
//                            int intResult = DatabaseHelper.executeNonQuery(sqlinsert);

//                        }
//                        catch
//                        {
//                        }
//                    }

//                }

//                Response.Redirect("tasks.aspx", false);
//            }
//        }

//        else if (e.CommandName == "delete")
//        {
//            //******************************************************************
//            //gvr = (GridViewRow)((LinkButton)e.CommandSource).NamingContainer;
//            //rows = gvr.RowIndex;
//            //row = DataGrid1.Rows[rows];


//            gvr = (GridDataItem)((LinkButton)e.CommandSource).NamingContainer;
//            rows = gvr.RowIndex;
//            row = DataGrid1.Items[rows];

//            //******************************************************************
//            // LinkButton readMarkButton = (LinkButton)row.FindControl("lnkReadMark");


//            sql = "update tasks set deleted=1 where task_id = " + row.Cells[19].Text + "; ";
//            //sql += " Update task_comments set deleted=1 where task_id=" + lblTaskId.Text;

//            int intResult = DatabaseHelper.executeNonQuery(sql);

//            if (intResult != 0)
//            {
//                task_action = ((string)DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + Session["admin"].ToString() + "'")) + " has deleted task ";

//                emailSubject = "Task ID:" + row.Cells[19].Text + " was deleted - " + ((Label)row.FindControl("lblshrtDesc")).Text + " (Task ID:" + row.Cells[19].Text + ")";

//                sendNotification(row.Cells[19].Text, ((Label)row.FindControl("lblshrtDesc")).Text);

//                sql = @" insert into task_updates([task_id],[username],[post_date],[comment]) 
//                            values(" + row.Cells[19].Text + ",'" + Session["admin"].ToString().Replace("'", "''") + "',getdate(),'Task was deleted.'); ";

//                intResult = DatabaseHelper.executeNonQuery(sql);



//                try
//                {

//                    sql = @"select *  from task_comments where task_id =" + row.Cells[19].Text + " and  tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "')";
//                    /// sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

//                    DataSet dscomment = DatabaseHelper.getDataset(sql);

//                    if (dscomment != null && dscomment.Tables[0].Rows.Count > 0)
//                    {
//                        for (int i = 0; i < dscomment.Tables[0].Rows.Count; i++)
//                        {
//                            try
//                            {


//                                string sqlinsert = "insert into read_comments([tc_id],[username]) ";
//                                sqlinsert += " values(" + dscomment.Tables[0].Rows[i]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

//                                int intCommentResult = DatabaseHelper.executeNonQuery(sqlinsert);

//                            }
//                            catch
//                            {

//                            }
//                        }



//                    }
//                }
//                catch { }



//                Response.Redirect("tasks.aspx");
//            }



//        }


//    }
    protected void DataGrid1_Sorting(object source, GridSortCommandEventArgs e)
    {
        //string i=e.SortExpression.ToString();
        //string j=Session["Column"].ToString();
        // if ( i==j )
        if (Session["Column"] != null)
        {
            if (e.SortExpression.ToString() == Session["Column"].ToString())
            {
                //Reverse the sort order
                if (Session["Order"] == "ASC")
                {
                    lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " DESC";
                    Session["Order"] = "DESC";
                }
                else
                {
                    lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " ASC";
                    Session["Order"] = "ASC";
                }
            }
        }
        else
        {
            //Different column selected, so default to ascending order
            lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " ASC";
            Session["Order"] = "ASC";
        }

        Session["Column"] = e.SortExpression.ToString();
        Session["OrderBy"] = lblOrderBy.Text;
        BindData();

    }
    protected void drpPerPage1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            //pagesize = Convert.ToInt16(drpPerPage1.SelectedValue);
            Session["pagesize"] = drpPerPage1.SelectedValue;
            BindData();
        }
        catch (Exception ex)
        { }

    }


    protected void btnGo_Click(object sender, EventArgs e)
    {
        Response.Redirect("edit_request.aspx?reqid=" + txtRequestId.Text.Trim(), false);
    }
    protected void btnGotoTask_Click(object sender, EventArgs e)
    {
        Response.Redirect("edit_task.aspx?id=" + txtTaskId.Text.Trim(), false);
    }
    protected void BtnGoWrdSearch_Click(object sender, EventArgs e)
    {
        if (ChkTask.Checked)
        {
            Session["TaskByWord"] = "true";
        }
        else
        {

            Session["TaskByWord"] = "false";
        }
        if (ChkCrs.Checked)
        {
            Session["CRsByWord"] = "true";
        }
        else
        {
            Session["CRsByWord"] = "false";
        }
        if (ChkTask.Checked == false && ChkCrs.Checked == false)
        {
            Session["TaskByWord"] = "true";

        }
        if (ChkIncludecomment.Checked)
        {
            Session["Includecomment"] = "true";
        }
        else
        {
            Session["Includecomment"] = "false";
        }

        Session["SearchWord"] = txtWord.Text.Trim();
        Response.Redirect("WordSearchResult.aspx", false);

    }
    protected void Drpool_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Drpool.SelectedItem.Text.ToString() != "[no filter]")
        {
            load_dropdowns();
            load_filterDropDown();
            chkViewDeleted.Checked = false;
            
            sql = @"Select  t.*, tc.*,
                (select count(*) as commenttype from task_comments where tc_id not in 
                (select tc_id from read_comments where username ='" + Session["admin"] + "')";
            sql += " and task_comments.deleted <> 1 and task_id =t.task_id)";
            sql += " from tasks t inner join task_comments tc on ";
            sql += " t.task_id = tc.task_id and tc.post_date=(select top 1 post_date from task_comments where tc.task_id = task_id and deleted<>1 ";
            sql += " order by post_date desc)where  status <> 'closed' and status <> 'Checked' and status <>'parked' and  t.deleted <> 1  and assigned_to_user = '" + Drpool.SelectedItem.Value + "'  ORDER BY last_updated_date desc ";
            
            DataSet ds = DatabaseHelper.getDataset(sql);
            ds = formatTaskDataSet(ds);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                TopFilterLable.InnerHtml = "Filtered by Pool Tasks";
                LblRecordsno.Text = ds.Tables[0].Rows.Count.ToString();
                DataGrid1.DataSource = ds.Tables[0];
                DataGrid1.DataBind();
            }
            else
            {
                LblRecordsno.Text = "0";
            }
        }
    }
}
