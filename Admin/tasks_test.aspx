<%@ Page Language="C#" AutoEventWireup="true" CodeFile="tasks_test.aspx.cs" Inherits="Admin_tasks_test" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Src="Notifications.ascx" TagName="Notifications" TagPrefix="uc4" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        <%= ConfigurationManager.AppSettings["CompanyName"].ToString()%></title>
    <%--<meta http-equiv="refresh" content="1200" />--%>
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <script src="../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery.hashchange.min.js" type="text/javascript"></script>
    <script src="../js/jquery.easytabs.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $.noConflict();
        jQuery(document).ready(function ($) {
            $('#tab-container').easytabs();
        });        
    </script>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script language="javascript" type="text/javascript">
            function window.confirm(str) {
                execScript('n = msgbox("' + str + '","4132")', "vbscript");
                return (n == 6);
            }
            function opacity() {

            }
       
        </script>
        <script type="text/javascript">
            function get_(div_) {

                div_ = div_.id + "1";

                document.getElementById(div_).style.display = "block";
            }
            function get_1(div_) {

                div_ = div_.id + "1";

                document.getElementById(div_).style.display = "none";
            }
        </script>
        <script type="text/javascript" language="javascript">
            function EnablePRlink() {
                var objPR = document.getElementById("DrpMultipleTaskPR").value;

                if (objPR != "0") {

                    document.getElementById("LnkBtnToChngPR").disabled = false;

                    // document.getElementById("LnkChngAssingedTo").disabled = "disabled";
                    document.getElementById("LnkChngStatus").disabled = "disabled";
                    document.getElementById("LnkChngAssingedTo").disabled = "disabled";
                    document.getElementById("DrpMultipleTaskAssing").value = "0";
                    document.getElementById("DrpMultipleTaskStatus").value = "0";

                }
                else {

                    document.getElementById("LnkBtnToChngPR").disabled = "disabled";

                    document.getElementById("LnkChngStatus").disabled = "disabled";
                    document.getElementById("LnkChngAssingedTo").disabled = "disabled";
                    document.getElementById("DrpMultipleTaskAssing").value = "0";
                    document.getElementById("DrpMultipleTaskStatus").value = "0";
                    document.getElementById("DrpMultipleTaskPR").value = "0";
                }

                return true;

            }


            function EnableAssinglink() {

                var objAssing = document.getElementById("DrpMultipleTaskAssing").value;

                if (objAssing != "0") {

                    document.getElementById("LnkBtnToChngPR").disabled = "disabled";
                    document.getElementById("LnkChngAssingedTo").disabled = false;
                    document.getElementById("LnkChngStatus").disabled = "disabled";
                    ///document.getElementById("DrpMultipleTaskAssing").value = "0";
                    document.getElementById("DrpMultipleTaskStatus").value = "0";
                    document.getElementById("DrpMultipleTaskPR").value = "0";
                    ///document.getElementById("LnkChngAssingedTo").disabled = "disabled";
                }
                else {

                    document.getElementById("LnkBtnToChngPR").disabled = "disabled";
                    document.getElementById("LnkChngAssingedTo").disabled = "disabled";
                    document.getElementById("LnkChngStatus").disabled = "disabled";
                    document.getElementById("DrpMultipleTaskAssing").value = "0";
                    document.getElementById("DrpMultipleTaskStatus").value = "0";
                    document.getElementById("DrpMultipleTaskPR").value = "0";

                }

                return;
            }

            function EnableStatuslink() {
                var objStatus = document.getElementById("DrpMultipleTaskStatus").value;

                if (objStatus != "0") {

                    document.getElementById("LnkBtnToChngPR").disabled = "disabled";
                    document.getElementById("LnkChngAssingedTo").disabled = "disabled";
                    document.getElementById("LnkChngStatus").disabled = false;
                    document.getElementById("DrpMultipleTaskAssing").value = "0";
                    ///document.getElementById("DrpMultipleTaskStatus").value = "0";
                    document.getElementById("DrpMultipleTaskPR").value = "0";

                }
                else {

                    document.getElementById("LnkBtnToChngPR").disabled = "disabled";
                    document.getElementById("LnkChngAssingedTo").disabled = "disabled";
                    document.getElementById("LnkChngStatus").disabled = "disabled";
                    document.getElementById("DrpMultipleTaskAssing").value = "0";
                    document.getElementById("DrpMultipleTaskStatus").value = "0";
                    document.getElementById("DrpMultipleTaskPR").value = "0";

                }
                return;

            }

            //function to select all checkbox of gridview DataGrid1
            function setCheckAll(obj) {
                var chkGridcontrols = document.getElementById("<%=DataGrid1.ClientID%>").getElementsByTagName("input");
                for (var i = 0; i < chkGridcontrols.length; i++) {
                    chkGridcontrols[i].checked = obj.checked;
                }

                return true;
            }

            //function to uncheck 'select all' when any checkbox in gridview is unchecked
            function validate(obj) {
                debugger;
                var chkGridcontrols = document.getElementById("<%=DataGrid1.ClientID%>").getElementsByTagName("input");
                //var chkheaderSelect = document.getElementById("chkheaderSelect");
                var chkheaderSelect = document.getElementById("DataGrid1_ctl01_chkheaderSelect");

                if (!obj.checked)
                    chkheaderSelect.checked = false;
            }

            //Validate Mark Button & Delete Button present inside Action area
            function validateonbtn(a) {
                var chkGridcontrols = document.getElementById("<%=DataGrid1.ClientID%>").getElementsByTagName("input");
                var count = 0;
                for (var i = 0; i < chkGridcontrols.length; i++) {
                    if (chkGridcontrols[i].checked)
                        count = 1;
                }

                if (count > 0) {
                    if (a == 0) {
                        if (!confirm("Are you sure you want to delete the selected tasks?"))
                            return false;
                        else
                            return true;
                    }
                    else if (a == 1) { }

                }
                else {
                    alert("Please select at least one task");
                    return false;
                }
            }
        </script>
    </telerik:RadCodeBlock>
    <link rel="stylesheet" type="text/css" href="slider.css" />
    <script type="text/javascript" src="slider.js"></script>
    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .abc
        {
            left: 260px !important;
        }
        
        table#DataGrid1_ctl00 tr th a
        {
            display: block;
            text-decoration: none;
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
            color:#ffffff !important;
            font-weight:normal !important;
        }
        
        table#DataGrid1_ctl00 tr td
        {
            border: 1px solid #D8D2D2;
             padding: 0 4px;
        }
        table#DataGrid1_ctl00 tr th
        {
            border: 1px solid #D8D2D2;
            background: none repeat scroll 0 0 #0054A6;
            color: #FFFFFF !important;
            font-weight:normal !important;
        }
        
        div.RadToolTip_Default table.rtWrapper td.rtWrapperContent
        {
            background-color: #FEFFB3 !important;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <uc2:Admin_Header ID="Admin_Header1" runat="server" />
        <div id="Content">
            <asp:UpdateProgress ID="updateprogress1" runat="server">
                <ProgressTemplate>
                    <div id="processmessage" class="processMessage">
                        <img alt="loading" src="images/ajax-loader.gif" style="width: 80px; height: 80px" />
                        <span style="color: black; font-weight: bold; font-size: 18px; vertical-align: super;">
                            Please wait... </span>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div id="Content_98">
                        <%--<div id="pageTitle">
                Tasks<span style="padding-left: 885px;"></span> <a href="http://www.esgdesk.com//Download/ESG_Desk.application"
                    style="color: white; font-size: 15px; text-decoration: bold; cursor: pointer;"
                    onmouseover="this.style.color='blue'" onmouseout="this.style.color='#ffffff'">Click
                    here to download the desktop application</a></div>--%>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="left" valign="top" style="display: none;">
                                    <div style="height: 40px; vertical-align: middle; line-height: 40px; margin: 20px 0 0 0;">
                                        <div class="titleText" style="float: left;">
                                            ESG Desk - Tasks</div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <div>
                                        <div id="tab-container" class='tab-container'>
                                            <ul class='etabs'>
                                                <li class='tab'><a href="#Filter">Filter</a></li>
                                                <li class='tab'><a href="#Action">Action</a></li>
                                                <li class='tab'><a href="#Quick-Search">Quick Search</a></li>
                                                <span id="TopFilterLable" runat="server" class="topfilter"></span>
                                            </ul>
                                            <div class='panel-container' onclick="return TABLE1_onclick()" id="TABLE1">
                                                <div id="Filter">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td width="45" valign="top" style="padding-top: 20px;">
                                                                <%-- <b style="color: #333333;">Filter:</b>--%>
                                                            </td>
                                                            <td valign="top" class="whitetext2" width="9%">
                                                                <br />
                                                                <span class="whitetext2">
                                                                    <asp:DropDownList ID="drpFilter" runat="server" AutoPostBack="True" Width="96%" OnSelectedIndexChanged="drpFilter_SelectedIndexChanged"
                                                                        CssClass="filerDrpodown">
                                                                    </asp:DropDownList>
                                                                </span>
                                                            </td>
                                                            <%--====================================================================--%>
                                                            <%--<td width="9%" valign="top" style="color: White; font-size: 14px">
                                                                            Pool<br />
                                                                            <span class="whitetext2"><span class="whitetext2"><span class="whitetext2">
                                                                                <asp:DropDownList ID="Drpool" runat="server" CssClass="filerDrpodown" AutoPostBack="True" Width="96%" OnSelectedIndexChanged="Drpool_SelectedIndexChanged">
                                                                                <asp:ListItem Value="0">[no filter]</asp:ListItem>
                                                                                <asp:ListItem Value="Developers Pool">Show my developers pool tasks</asp:ListItem> 
                                                                                <asp:ListItem Value="Designers Pool">Show my designers pool tasks</asp:ListItem> 
                                                                                </asp:DropDownList>
                                                                            </span></span></span>
                                                                        </td>--%>
                                                            <%--====================================================================--%>
                                                            <td width="7%" valign="top" style="color: #484848; font-size: 12px">
                                                                Project<br />
                                                                <span class="whitetext2"><span class="whitetext2"><span class="whitetext2">
                                                                    <asp:DropDownList ID="drpProjects" runat="server" CssClass="filerDrpodown" AutoPostBack="True"
                                                                        OnSelectedIndexChanged="drpProjects_SelectedIndexChanged" Width="95%">
                                                                    </asp:DropDownList>
                                                                </span></span></span>
                                                            </td>
                                                            <td width="9%" valign="top" style="color: #484848; font-size: 12px">
                                                                Category<br />
                                                                <asp:DropDownList ID="drpCategories" runat="server" CssClass="filerDrpodown" AutoPostBack="True"
                                                                    OnSelectedIndexChanged="drpCategories_SelectedIndexChanged" Width="96%">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td width="9%" valign="top" style="color: #484848; font-size: 12px">
                                                                Reported by<br />
                                                                <asp:DropDownList ID="drpReportedBy" runat="server" CssClass="filerDrpodown" AutoPostBack="True"
                                                                    OnSelectedIndexChanged="drpReportedBy_SelectedIndexChanged" Width="96%">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td width="9%" valign="top" style="color: #484848; font-size: 12px">
                                                                Priority<br />
                                                                <asp:DropDownList ID="drpPriorities" runat="server" CssClass="filerDrpodown" AutoPostBack="True"
                                                                    OnSelectedIndexChanged="drpPriorities_SelectedIndexChanged" Width="96%">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td id="tdAssignedTo" runat="server" width="9%" valign="top" style="color: #484848;
                                                                font-size: 12px">
                                                                Assigned to<br />
                                                                <span class="whitetext2">
                                                                    <asp:DropDownList ID="drpAssignedTo" runat="server" CssClass="filerDrpodown" AutoPostBack="True"
                                                                        OnSelectedIndexChanged="drpAssignedTo_SelectedIndexChanged" Width="96%">
                                                                    </asp:DropDownList>
                                                                </span>
                                                            </td>
                                                            <td width="9%" valign="top" style="color: #484848; font-size: 12px">
                                                                Status<br />
                                                                <asp:DropDownList ID="drpStatuses" runat="server" CssClass="filerDrpodown" AutoPostBack="True"
                                                                    OnSelectedIndexChanged="drpStatuses_SelectedIndexChanged" Width="96%">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td width="5%" valign="top" style="color: #484848; font-size: 12px">
                                                                Items<br />
                                                                <asp:DropDownList ID="drpPerPage1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpPerPage1_SelectedIndexChanged">
                                                                    <asp:ListItem Value="20" Selected="True">20</asp:ListItem>
                                                                    <asp:ListItem Value="50">50</asp:ListItem>
                                                                    <asp:ListItem Value="100">100</asp:ListItem>
                                                                    <asp:ListItem Value="500">500</asp:ListItem>
                                                                    <asp:ListItem Value="1000">1000</asp:ListItem>
                                                                    <asp:ListItem Value="00">All</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td valign="top" width="30" style="color: #484848;">
                                                                Arch.<br />
                                                                <asp:CheckBox ID="chkArchive" runat="server" AutoPostBack="True" OnCheckedChanged="chkArchive_CheckedChanged" />
                                                            </td>
                                                            <td valign="bottom">
                                                                <asp:Button ID="btnClearFilters" runat="server" OnClick="btnClearFilters_Click" Text="Clear filters"
                                                                    CssClass="clearfilter" />
                                                            </td>
                                                            <td valign="top" class="whitetext2" style="min-width: 140px; color: #484848; font-size: 12px">
                                                                <asp:CheckBox ID="chkViewDeleted" runat="server" Text="View only deleted" AutoPostBack="True"
                                                                    OnCheckedChanged="chkViewDeleted_CheckedChanged" />
                                                                <br />
                                                                <asp:CheckBox ID="chkHideClosed" runat="server" Text="View closed" AutoPostBack="True"
                                                                    OnCheckedChanged="chkHideClosed_CheckedChanged" />
                                                            </td>
                                                            <td valign="bottom" class="whitetext2" style="min-width: 110px; color: #484848; font-size: 13px">
                                                                <asp:CheckBox ID="ChkReadTask" runat="server" Text="Include read&nbsp;&nbsp;" Checked="true"
                                                                    AutoPostBack="True" OnCheckedChanged="ChkReadTask_CheckedChanged" />
                                                            </td>
                                                            <td valign="bottom" class="whitetext2" style="min-width: 150px; color: #484848; font-size: 13px">
                                                                <span style="color: #484848;"><b>Records: </b>
                                                                    <asp:Label ID="LblRecordsno" runat="server" Text=""></asp:Label>
                                                                </span>
                                                            </td>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="Action">
                        <div id="DivMiltiselection" runat="server" style="color: White; font-size: 12px">
                            <%--<b style="color: #484848;">Action:</b>--%>
                            <table width="60%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <span style="float: left;"><span style="color: Red;">&nbsp;</span></span>
                                        <asp:Button ID="LnkBtnToChngPR" CssClass="noinput" runat="server" BackColor="transparent" BorderStyle="none"
                                            Text="Change priority to" OnClick="LnkBtnToChngPR_Click" Style="color: #484848;
                                            margin-left: -10px; font-size: 12px !important; font-family: Trebuchet MS !important;" />
                                    </td>
                                    <td>
                                        <asp:Button ID="LnkChngAssingedTo" CssClass="noinput" runat="server" BackColor="transparent" BorderStyle="none"
                                            Text="Change assigned to" OnClick="LnkChngAssingedTo_Click" Style="color: #484848;
                                            font-size: 12px !important; font-family: Trebuchet MS !important; margin-left: -8px;" />
                                    </td>
                                    <td>
                                        <asp:Button ID="LnkChngStatus" runat="server" CssClass="noinput" BackColor="transparent" BorderStyle="none"
                                            Text="Change status to" OnClick="LnkChngStatus_Click" Style="color: #484848;
                                            font-size: 12px !important; font-family: Trebuchet MS !important; margin-left: -8px;" />
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:DropDownList ID="DrpMultipleTaskPR" runat="server" CssClass="filerDrpodown"
                                            AutoPostBack="true" Width="180px">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="DrpMultipleTaskAssing" runat="server" CssClass="filerDrpodown"
                                            AutoPostBack="false" Width="180px">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="DrpMultipleTaskStatus" runat="server" CssClass="filerDrpodown"
                                            AutoPostBack="false" Width="180px">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="LnkBtnToDeleteAll" runat="server" OnClick="LnkBtnToDeleteAll_Click"
                                            CssClass="whitetext2 elinks" OnClientClick="return validateonbtn(0);">Delete selected</asp:LinkButton>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;&nbsp;
                                        <asp:LinkButton ID="LnkBtnToMarkAllRead" runat="server" OnClientClick="return validateonbtn(1);"
                                            OnClick="LnkBtnToMarkAllRead_Click" CssClass="whitetext2 elinks">Mark read</asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div id="Quick-Search">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="15%" valign="bottom" style="padding-bottom: 4px;">
                                    <table id="tblSearchRequest" runat="server" border="0" cellpadding="0" cellspacing="2">
                                        <tr>
                                            <td colspan="2" style="min-width: 70px; color: #484848;">
                                                Request ID
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="whitetext2" width="40">
                                                <asp:TextBox ID="txtRequestId" runat="server" Width="100%" Height="15px" MaxLength="10"
                                                    Style="min-width: 20px;"></asp:TextBox>
                                            </td>
                                            <td style="color: #484848; font-size: 14px">
                                                <asp:Button ID="btnGo" runat="server" Text="Go" CausesValidation="False" OnClick="btnGo_Click"
                                                    CssClass="goBtn" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="15%" valign="bottom" style="padding-bottom: 4px;">
                                    <table cellpadding="0" cellspacing="2">
                                        <tr>
                                            <td colspan="2" style="min-width: 40px; color: #484848;">
                                                Task ID
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="40">
                                                <asp:TextBox ID="txtTaskId" runat="server" Width="100%" Height="15px" MaxLength="10"
                                                    Style="min-width: 20px;"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnGotoTask" runat="server" Text="Go" CausesValidation="False" OnClick="btnGotoTask_Click"
                                                    CssClass="goBtn" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="55%" style="padding-left: 3px; padding-bottom: 5px">
                                    <table id="Tblsearchbyword" runat="server" border="0" cellpadding="0" cellspacing="2">
                                        <tr>
                                            <td align="left" colspan="2" style="color: #484848;">
                                                Search by word
                                                <asp:CheckBox ID="ChkTask" runat="server" Text="Task" />
                                                <asp:CheckBox ID="ChkCrs" runat="server" Text="CRs" />
                                                <asp:CheckBox ID="ChkIncludecomment" runat="server" Text="Include comments" />
                                            </td>
                                        </tr>
                                        <tr style="padding-top: 2px">
                                            <td width="100%">
                                                <asp:TextBox ID="txtWord" runat="server" Width="100%" Height="15px"></asp:TextBox>
                                            </td>
                                            <td width="37" align="right">
                                                <asp:Button ID="BtnGoWrdSearch" runat="server" Text="Go" CssClass="goBtn" CausesValidation="False"
                                                    OnClick="BtnGoWrdSearch_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    </div> </div> </div> </td> </tr>
                    <tr>
                        <td align="left" valign="top">
                            <span style="color: Black; font-size: 16px">
                                <asp:LinkButton ID="LnkNotification" runat="server" ForeColor="red" OnClick="LnkNotification_Click"></asp:LinkButton>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <uc4:Notifications ID="Notifications" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="height: 10px">
                        </td>
                    </tr>
                    </table>
                    <div id="divMessage" runat="server" style="color: Red; font-weight: bold;">
                    </div>
                    <div>
                        <%--****************************************************************************************************--%>
                        <%-- ClientSettings-AllowColumnsReorder="true" ClientSettings-ReorderColumnsOnClient="true"--%>
                        <telerik:RadGrid ID="DataGrid1" PageSize="20" runat="server" AllowPaging="false"
                            AllowSorting="true" AutoGenerateColumns="false" CellPadding="5" GridLines="Both"
                            OnSelectedIndexChanged="DataGrid1_SelectedIndexChanged" BorderColor="#cccccc"
                            OnItemDataBound="RadGrid1_ItemDataBound" OnItemCommand="DataGrid1_OnItemCommand"
                            OnSortCommand="DataGrid1_Sorting" ShowFooter="true">
                            <mastertableview pagerstyle-alwaysvisible="true">
                                <Columns>
                                    <telerik:GridBoundColumn DataField="task_id" HeaderText="ID" Visible="False" SortExpression="short_desc">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn HeaderStyle-Width="15px" ItemStyle-Width="10px" FooterStyle-Width="10px">
                                        <HeaderTemplate>
                                            <%--<asp:CheckBox ID="chkheaderSelect"  runat="server" onclick="return setCheckAll(this);" Text="Select all" />--%>
                                            <asp:CheckBox ID="chkheaderSelect" runat="server" onclick="return setCheckAll(this);" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" onclick="return validate(this);" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="short_desc" HeaderText="ShrtDesc" Visible="False"
                                        SortExpression="short_desc">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn HeaderText="Short description" SortExpression="short_desc">
                                        <ItemTemplate>
                                            <asp:Label Font-Size="12px" ID="lblshrtDesc" runat="server" Width="250px" Text='<%# bind("short_desc") %>'></asp:Label>
                                            <%-- <div id="lblshrtDesc1" runat="server" style="display:none;background-color:#FEFFB3;border:solid 1px #333333; width:730px;padding-left:10px;left:340px;top:342px;" >
                                 <asp:Label ID="Yearid" runat="server"  Text='<%#Eval("ShowAllComment") %>'></asp:Label>
                              </div>--%>
                                            <telerik:RadToolTip ID="RadToolTip1" BackColor="#FEFFB3" runat="server" OffsetX="-100"
                                                AutoCloseDelay="60000" HideEvent="LeaveTargetAndToolTip" OffsetY="10" TargetControlID="lblshrtDesc"
                                                Width="500px" RelativeTo="Element" Position="BottomLeft" ShowEvent="OnMouseOver">
                                                <%# DataBinder.Eval(Container, "DataItem.ShowAllComment")%></telerik:RadToolTip>
                                            <%--<ajax:HoverMenuExtender ID="hme2" runat="Server" OffsetX ="100" OffsetY="20" PopupControlID="lblshrtDesc1" PopupPosition="Bottom" TargetControlID="lblshrtDesc" />--%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridHyperLinkColumn Text="+" Visible="false" NavigateUrl="edit_task.aspx"
                                        DataNavigateUrlFields="task_id" DataNavigateUrlFormatString="edit_task.aspx?id={0}"
                                        SortExpression="short_desc">
                                    </telerik:GridHyperLinkColumn>
                                    <telerik:GridTemplateColumn HeaderText="Read/Unread">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkReadMark" runat="server" Width="130px" CommandName="ReadMark">(Unread. Mark as read)</asp:LinkButton>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Project" SortExpression="project">
                                        <ItemTemplate>
                                            <a id="lblproject" runat="server" target="_blank" href='<%# "projectrequests.aspx?projectname="+ HttpUtility.UrlEncode(Eval("project").ToString()) %>'>
                                                <%# Eval("project")%></a>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <%--<telerik:GridBoundColumn DataField="status" HeaderText="status"  SortExpression="status"></telerik:GridBoundColumn>--%>
                                    <telerik:GridTemplateColumn HeaderText="Status" SortExpression="status" UniqueName="StatusColor">
                                        <ItemTemplate>
                                            <asp:Label ID="statusvalue" runat="server" Text='<%#Eval("status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Priority" SortExpression="priority">
                                        <ItemStyle Width="100px" />
                                        <ItemTemplate>
                                            <asp:Label ID="priorityvalue" runat="server" Text='<%#Eval("priority") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <%--<telerik:GridBoundColumn DataField="priority" HeaderText="priority"  SortExpression="priority"></telerik:GridBoundColumn>--%>
                                    <telerik:GridTemplateColumn HeaderText="Assigned" SortExpression="assigned_to_user">
                                        <ItemTemplate>
                                            <a id="lblassignedtouser" runat="server" target="_blank" href='<%# "Priority_reports.aspx?Ename="+ Eval("assigned_to_user") %>'>
                                                <%# Eval("assigned_to_user") %></a>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Updated by" SortExpression="last_updated_user">
                                        <ItemTemplate>
                                            <a id="lbllast_updated_user" runat="server" target="_blank" href='<%# "Priority_reports.aspx?Ename="+ Eval("last_updated_user") %>'>
                                                <%# Eval("last_updated_user")%></a>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Last updated" SortExpression="last_updated_date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLastUpdatedOn" runat="server" Text='<%# bind("last_updated_date") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="category" HeaderText="Category" SortExpression="category">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn UniqueName="ETCfield" HeaderText="ETC" SortExpression="ETC">
                                        <ItemTemplate>
                                            <asp:Label ID="lblETC" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn UniqueName="Timetodat" HeaderText="Time" SortExpression="Time to date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTimetodat" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn UniqueName="Balance" HeaderText="Balance" SortExpression="Balance">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBalance" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="Reported by" HeaderText="Reported by" Visible="False"
                                        SortExpression="Reported by">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn UniqueName="ETC" HeaderText="Reported on" SortExpression="reported_date"
                                        Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblReportedOn" runat="server" Text='<%# Bind("reported_date") %>'></asp:Label>
                                            <asp:Label ID="hd" runat="server" Text='<%# Bind("ETC") %>'></asp:Label>
                                            <%--<asp:HiddenField ID="hd" runat="server" Value='<%# Eval("ETC")%>' />--%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="ETC" HeaderText="ETC" Visible="False">
                                    </telerik:GridBoundColumn>
                                    <%--<telerik:GridBoundColumn DataField="task_id"  HeaderText="ID" SortExpression="task_id"></telerik:GridBoundColumn> --%>
                                    <telerik:GridTemplateColumn UniqueName="TaskId" HeaderText="ID" SortExpression="task_id">
                                        <ItemTemplate>
                                            <asp:Label ID="lbltaskId" runat="server" Text='<%# Bind("task_id") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <%--=================================================================--%>
                                    <%--<telerik:GridHyperLinkColumn HeaderText="Edit" Text="edit" DataNavigateUrlFields="task_id" DataNavigateUrlFormatString="edit_task.aspx?id={0}" NavigateUrl="task_id">
                  <HeaderStyle Width="25px" />
                  
                  </telerik:GridHyperLinkColumn>--%>
                                    <telerik:GridTemplateColumn HeaderText="Edit">
                                        <ItemTemplate>
                                            <a href="edit_task.aspx?id=<%# Eval("task_id")%>">
                                                <asp:ImageButton CssClass="bgtrans" ID="editid" runat="server" ImageUrl="images/edit-icon.png" />
                                            </a>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <%--=================================================================--%>
                                    <%--===============================================--%>
                                    <telerik:GridTemplateColumn HeaderText="Order" SortExpression="OrderId">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtorderid" AutoPostBack="true" Enabled="true" Width="20px" OnTextChanged="TextBox2_textchanged"
                                                runat="server" Text='<%# Bind("OrderId") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <%--===============================================--%>
                                    <telerik:GridTemplateColumn HeaderText="Delete">
                                        <ItemTemplate>
                                            <%--<asp:LinkButton ID="lnkdel"  runat="server" CommandName="delete">delete</asp:LinkButton>--%>
                                            <asp:LinkButton ID="lnkdel" runat="server" CommandName="delete">
                     <img src="images/delete-icon.png" />
                      
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            </mastertableview>
                        </telerik:RadGrid>
                        <%--****************************************************************************************************--%>
                        <asp:Panel ID="Panel1" runat="server">
                        </asp:Panel>
                    </div>
                    <br />
                    <div style="text-align: right" id="divTotalHrs" runat="server">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td align="right">
                                 
                                         <table width="24%" border="0" cellspacing="0" cellpadding="0" class="tasktbl">
  <tr>
    <td rowspan="2"> Total Hrs:</td>
    <th> Estimated time</th>
    <th> Total hours Taken</th>
    <th>Elapsed time</th>
  </tr>
                                                
                                                    <tr>
                                                        <td align="center" style="width: 115px" valign="top">
                                                            <asp:Label ID="lblETC" runat="server" Font-Size="12px"></asp:Label>
                                                        </td>
                                                        <td align="center" style="width: 115px" valign="top">
                                                            <asp:Label ID="lblTotalHrsTaken" runat="server" Font-Size="12px"></asp:Label>
                                                        </td>
                                                        <td align="center" style="width: 115px" valign="top">
                                                            <asp:Label ID="lblExpectedHrsLeft" runat="server" Font-Size="12px"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                         
                                           
                                </td>
                            </tr>
                        </table>
                    </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <uc1:footer ID="Footer1" runat="server" />
    <asp:Label ID="lblOrderBy" runat="server" Text="" Visible="false"></asp:Label>
    <div id="popup" class="popup" style="background-color: #e9e9e9; width: 650px; border: solid 1px #000000;
        border-bottom: solid 2px #000000; border-right: solid 2px #000000; padding: 10px 10px 10px 10px;
        position: absolute; display: none">
    </div>
    </form>
</body>
</html>
