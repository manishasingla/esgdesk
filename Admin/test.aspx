<%@ Page Language="C#" AutoEventWireup="true" CodeFile="test.aspx.cs" Inherits="Admin_test" %>

<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= ConfigurationManager.AppSettings["CompanyName"].ToString()%></title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        function checkChecked()
        {
            if(document.getElementById("chkShowAllHistory").checked==true)
            {
                document.getElementById("divNoHistory").style.display="none";
                document.getElementById("divAllHistory").style.display="block";       
            }
            else
            {
                document.getElementById("divNoHistory").style.display="block";
                document.getElementById("divAllHistory").style.display="none";       
            }
        }
    </script>
    
</head>
<body onload="checkChecked();">
    <form id="form1" runat="server">
    <div id="wrapper">
        <uc2:Admin_Header ID="Admin_Header1" runat="server" />
        <div id="Content">
    <div id="pageTitle" style="height:30px;font-weight:bold;text-align:left">Task</div>
        <div><a href="edit_task.aspx">Add new task</a> &nbsp;&nbsp; <a id="lnkBackToRequest" runat="server" visible="false" href="edit_request.aspx">Back to change request</a>
        </div>
        <div id="divMessage" runat="server" style="color:Red;font-weight:bold"></div>
        <div id="DivEntry" runat="server">
        <div id="divTask" runat="server" class="divBorder">
        <table width="100%" cellpadding="2">
                        <tr>
                            <td align="left" valign="top">
                                Task id:
                                <asp:Label ID="lblTaskId" runat="server" Font-Bold="True"></asp:Label></td>
                            <td align="left" valign="top">
                                <asp:Label ID="lblLastUpdated" runat="server" ForeColor="Green"></asp:Label></td>
                            <td align="left" valign="top">
                            </td>
                            <td align="right" valign="top">
                            <a id="lnkSubscribers" runat="server" href="#" target="_blank" visible="false">subscribers</a>
                            </td>
                        </tr>
            <tr>
                <td align="left" colspan="4" valign="top">
                    <asp:Label ID="lblReportedBy" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td align="left" colspan="4" valign="top">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>Project:&nbsp;</td>
                            <td>
                                <asp:Label ID="lblProject" runat="server" Font-Bold="True"></asp:Label>
                            </td>                                                       
                            <td style="width:10px"></td>
                            <td>Assigned to:&nbsp;</td>
                            <td>
                                <asp:Label ID="lblAssignedTo" runat="server" Font-Bold="True"></asp:Label>
                            </td>
                            
                            <td style="width:10px"></td>
                            <td>&nbsp;</td>
                            <td>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="height:10px">
                            </td>
                            <td style="height: 10px">
                            </td>
                            <td style="height: 10px;">
                            </td>
                            <td style="height: 10px">
                            </td>
                            <td style="height: 10px">
                            </td>
                            <td style="height: 10px;">
                            </td>
                            <td style="height: 10px">
                            </td>
                            <td style="width: 10px">
                            </td>
                            <td style="height: 10px">
                            </td>
                        </tr>
                        <tr>
                            <td>Change project:&nbsp;</td>
                            <td>
                                <asp:DropDownList ID="drpProjects" runat="server" Width="175px">
                                </asp:DropDownList>
                            </td>
                            <td style="width:10px"></td>
                            <td>Re-assign:&nbsp;</td>
                            <td>
                                <asp:DropDownList ID="drpUsers" runat="server" Width="175px">
                                </asp:DropDownList>
                            </td>
                            <td style="width:10px"></td>
                            <td>Status:</td>
                            <td>
                            </td>
                            <td><asp:DropDownList ID="drpStatuses" runat="server" Width="175px">
                            </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td style="width: 10px">
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td style="width: 10px">
                            </td>
                            <td style="height: 10px">
                            </td>
                            <td style="height: 10px">
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Project type:</td>
                            <td>
                                <asp:DropDownList ID="drpProjectType" runat="server" Width="175px">
                                    <asp:ListItem></asp:ListItem>
                                    <asp:ListItem>es</asp:ListItem>
                                    <asp:ListItem Selected="True">other</asp:ListItem>
                                </asp:DropDownList></td>
                            <td style="width: 10px">
                            </td>
                            <td>
                                Categoty:</td>
                            <td>
                                <asp:DropDownList ID="drpCategories" runat="server" Width="175px">
                                </asp:DropDownList></td>
                            <td style="width: 10px">
                            </td>
                            <td>
                                Priority:</td>
                            <td>
                            </td>
                            <td>
                                <asp:DropDownList ID="drpPriorities" runat="server" Width="175px">
                                </asp:DropDownList></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left" colspan="4" valign="top">
                <table border="0" cellpadding="0" cellspacing="0">
                        
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left" colspan="4" valign="top">
                                <asp:Label ID="lblUserName" runat="server" Visible="False"></asp:Label></td>
            </tr>
            <tr>
                <td align="left" colspan="4" valign="top">
                    <table>
                        <tr>
                            <td align="left" valign="top">Short description:
                            </td>
                            <td align="left" valign="top">
                                <asp:TextBox ID="txtShortDescr" runat="server" Width="650px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtShortDescr"
                                    ErrorMessage="Please enter short description.">*</asp:RequiredFieldValidator><cc1:validatorcalloutextender
                                        id="ValidatorCalloutExtender2" runat="server" targetcontrolid="RequiredFieldValidator2">
                                </cc1:validatorcalloutextender>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top">
                                Relevant URL:</td>
                            <td align="left" valign="top">
                                <asp:TextBox ID="txtRelevantURL" runat="server" Width="650px" Rows="2" TextMode="MultiLine"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
                        <tr>
                            <td align="left" valign="top">
                                Comment:</td>
                            <td align="left" valign="top">
                                &nbsp;</td>
                            <td align="left" valign="top">
                            </td>
                            <td align="left" valign="top">
                            </td>
                        </tr>
                        <tr>
                            <td align="left" colspan="4" valign="top">
                                <FTB:FreeTextBox ID="ftbComment" runat="server" Height="200px" Width="100%">
                                </FTB:FreeTextBox>
                                &nbsp;</td>
                        </tr>
                       <tr>
                            <td align="left" valign="top" colspan="3">
                            <div><a id="lnkSendAttachment" href="#" runat="server" visible="false">Upload attachment</a> &nbsp;&nbsp  <a id="lnkViewAttachment" href="#" runat="server">View attachments</a>&nbsp;
                                <asp:CheckBox ID="chkShowAllHistory" runat="server" Text="Show all history" /></div>
                                <div id="divMessage1" runat="server" style="color:Red"></div>
                            </td>
                            <td align="right" valign="top">
                            <asp:Button ID="btnUpdate" runat="server" Text="Update" OnClick="btnUpdate_Click" />&nbsp;
                                <asp:Button ID="btnDelete" runat="server" Text="Delete" OnClick="btnDelete_Click" OnClientClick="javascript:return confirm('Are you sure you want to delele task?');"/></td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" colspan="4">
                            </td>
                        </tr>
                    </table>
        </div>  
        <br />
        <div id="divTaskDetails" runat="server">
        <div id="divNoHistory" runat="server">
            <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" Width="100%" ShowHeader="False" BorderStyle="None" OnItemDataBound="DataGrid1_ItemDataBound" OnItemCommand="DataGrid1_ItemCommand" BorderWidth="0px" GridLines="None">
                <Columns>
                    <asp:BoundColumn DataField="task_id" Visible="False"></asp:BoundColumn>
                    <asp:BoundColumn DataField="tc_id" Visible="False"></asp:BoundColumn>
                    <asp:BoundColumn DataField="username" Visible="False"></asp:BoundColumn>
                    <asp:BoundColumn DataField="post_date" Visible="False"></asp:BoundColumn>
                    <asp:BoundColumn DataField="comment" Visible="False"></asp:BoundColumn>
                    <asp:TemplateColumn>
                        <ItemTemplate>
                        <div style="border:solid 1px green; margin-bottom:2px">
                            <table width="100%">
                                <tr>
                                    <td align="left">
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td>
                                            <asp:Label ID="lblPosted" runat="server" ></asp:Label>
                                            </td>
                                            <td align="right">
                                                <asp:LinkButton ID="lnkEdit" runat="server" CausesValidation="False" CommandName="edit">edit</asp:LinkButton>&nbsp;
                                                <asp:LinkButton ID="lnkDelete" runat="server" CausesValidation="False" CommandName="delete">delete</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                    </td>                                    
                                </tr>
                                <tr>
                                    <td align="left">
                                    </td>
                                   
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="lblComment" runat="server" Text='<%# bind("comment") %>'></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
        </div>
        <div id="divAllHistory" runat="server">
            <asp:DataGrid ID="DataGrid2" runat="server" AutoGenerateColumns="False" Width="100%" ShowHeader="False" OnItemDataBound="DataGrid2_ItemDataBound" OnItemCommand="DataGrid2_ItemCommand" BorderStyle="None" GridLines="None">
                <Columns>
                    <asp:BoundColumn DataField="task_id" Visible="False"></asp:BoundColumn>
                    <asp:BoundColumn DataField="tc_id" Visible="False"></asp:BoundColumn>
                    <asp:BoundColumn DataField="username" Visible="False"></asp:BoundColumn>
                    <asp:BoundColumn DataField="post_date" Visible="False"></asp:BoundColumn>
                    <asp:BoundColumn DataField="comment" Visible="False"></asp:BoundColumn>
                    <asp:TemplateColumn>
                        <ItemTemplate>
                            <div style="border:solid 1px green; margin-bottom:2px">
                                <table width="100%">
                                    <tr>
                                        <td align="left">
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblPosted" runat="server" ForeColor="Green"></asp:Label>
                                                    </td>
                                                    <td align="right">
                                                        <asp:LinkButton ID="lnkEdit" runat="server" CausesValidation="False" CommandName="edit">edit</asp:LinkButton>&nbsp;
                                                        <asp:LinkButton ID="lnkDelete" runat="server" CausesValidation="False" CommandName="delete">delete</asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblComment" runat="server" Text='<%# bind("comment") %>'></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="deleted" Visible="False"></asp:BoundColumn>
                    <asp:BoundColumn DataField="temp" Visible="False"></asp:BoundColumn>
                </Columns>
            </asp:DataGrid>
        </div>
        </div>  
        </div>
        </div>    
        <uc1:footer ID="Footer1" runat="server" />
        <asp:HiddenField ID="prevProject" runat="server" />
        <asp:HiddenField ID="prevAssignedTo" runat="server" />
        <asp:HiddenField ID="prevStatus" runat="server" />
        <asp:HiddenField ID="prevProjectType" runat="server" />
        <asp:HiddenField ID="prevCategory" runat="server" />
        <asp:HiddenField ID="prevPriority" runat="server" />
        <asp:HiddenField ID="prevRelevantUrl" runat="server" />
        <asp:HiddenField ID="prevShortDescr" runat="server" />
        </div>
    </form>
</body>
</html>
