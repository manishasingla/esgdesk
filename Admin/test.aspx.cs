using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

public partial class Admin_test : System.Web.UI.Page
{
    int id;
    int reqid;
    string sql = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = (Request.QueryString["id"] == null || Request.QueryString["id"].ToString() == "" ? "edit_task.aspx" : "edit_task.aspx?id=" + Request.QueryString["id"].ToString());
            Response.Redirect("login.aspx");
            return;
        }

        divMessage1.InnerHtml = "";
        divMessage.InnerHtml = "";

        string task_id = Request["id"];
        if (task_id == null || task_id == "0" || task_id == "")
        {
            id = 0;
        }
        else
        {
            if (DatabaseHelper.is_int(task_id))
            {
                id = Convert.ToInt32(task_id);
            }
            else
            {
                // Display an error because the bugid must be an integer
                divMessage.InnerHtml = "Task ID must be an integer.";
                divMessage.InnerHtml += "<p><a href='tasks.aspx'>View tasks</a></p>";
                DivEntry.Visible = false;
                return;
            }
        }

        string req_id = Request["reqid"];
        if (req_id == null || req_id == "0" || req_id == "")
        {
            reqid = 0;
            lnkBackToRequest.Visible = false;
        }
        else
        {
            if (DatabaseHelper.is_int(req_id))
            {
                reqid = Convert.ToInt32(req_id);
                lnkBackToRequest.HRef = "edit_request.aspx?reqid=" + reqid.ToString();
                lnkBackToRequest.Visible = true;
            }
            else
            {
                // Display an error because the bugid must be an integer
                divMessage.InnerHtml = "<br>Change request ID must be an integer.";
                divMessage.InnerHtml += "<p><a href='client_requests.aspx'>View change requests</a></p>";
                DivEntry.Visible = false;
                return;
            }
        }

        if (!Page.IsPostBack)
        {
            chkShowAllHistory.Attributes.Add("onclick", "checkChecked();");
            load_dropdowns();
            set_default_selection();

            if (id == 0)
            {
                if (reqid != 0)
                {
                    sql = "\nSelect * from ClientRequest where RequestId=" + reqid.ToString() + ";";

                    sql += "\nSelect * from ClientRequest_Details where RequestId=" + reqid.ToString() + " order by CommentId desc";

                    DataSet dsClientRequest = DatabaseHelper.getDataset(sql);

                    // change request
                    if (dsClientRequest.Tables[0].Rows.Count > 0)
                    {
                        txtShortDescr.Text = dsClientRequest.Tables[0].Rows[0]["ShortDescr"].ToString();
                        txtRelevantURL.Text = dsClientRequest.Tables[0].Rows[0]["WebsiteURL"].ToString();
                    }
                    else
                    {
                        divMessage.InnerHtml = "<br>Request ID not found.";
                        divMessage.InnerHtml += "<p><a href='client_requests.aspx'>View change requests</a></p>";
                        DivEntry.Visible = false;
                        lnkBackToRequest.Visible = false;
                        return;
                    }

                    // merge change request comments
                    if (dsClientRequest.Tables[1].Rows.Count > 0)
                    {
                        ftbComment.Text = "";

                        for (int i = 0; i < dsClientRequest.Tables[1].Rows.Count; i++)
                        {
                            ftbComment.Text += dsClientRequest.Tables[1].Rows[i]["Comment"].ToString() + "<br/><hr/><br/>";
                        }
                    }

                    lnkSendAttachment.HRef = "upload_attachment.aspx?reqid=" + reqid.ToString();
                    lnkViewAttachment.HRef = "view_attachments.aspx?reqid=" + reqid.ToString();
                    DataSet dsTotalAttachments = DatabaseHelper.getAttachments(reqid, "");
                    lnkViewAttachment.InnerHtml = "View attachments (" + dsTotalAttachments.Tables[0].Rows.Count + ")";
                }

                btnUpdate.Text = "Create";
                btnDelete.Visible = false;
                lblTaskId.Text = "New";
                lblProject.Text = drpProjects.SelectedItem.Text;
                lblAssignedTo.Text = drpUsers.SelectedItem.Text;
            }
            else
            {
                // Get this entry's data from the db and fill in the form

                lnkSubscribers.HRef = "view_subscribers.aspx?id=" + id.ToString();
                lnkSubscribers.Visible = true;
                lblTaskId.Text = id.ToString();
                BindData();
            }

            if (!DatabaseHelper.can_Delete_Tasks(Session["admin"].ToString()))
            {
                btnDelete.Visible = false;
            }
        }
    }

    public void BindData()
    {
        sql = "select * from tasks where deleted <> 1 and task_id = " + lblTaskId.Text.Trim();

        DataSet dsTask = DatabaseHelper.getDataset(sql);

        if (dsTask == null || dsTask.Tables.Count <= 0 || dsTask.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "<br>Task not found.<br><br><a href='tasks.aspx'>View tasks</a>";
            DivEntry.Visible = false;
            divTaskDetails.Visible = false;
        }
        else
        {
            lblTaskId.Text = dsTask.Tables[0].Rows[0]["task_id"].ToString();
            lblLastUpdated.Text = " Last changed by <b>" + dsTask.Tables[0].Rows[0]["last_updated_user"].ToString() + "</b> on <b>" + DateTime.Parse(dsTask.Tables[0].Rows[0]["last_updated_date"].ToString()).ToString("dd MMM h:mm tt") + "</b>";
            lblReportedBy.Text = "Reported by <b>" + dsTask.Tables[0].Rows[0]["reported_user"].ToString() + "</b> on <b>" + DateTime.Parse(dsTask.Tables[0].Rows[0]["reported_date"].ToString()).ToString("dd MMM h:mm tt") + "</b>";

            lblProject.Text = dsTask.Tables[0].Rows[0]["project"].ToString();
            lblAssignedTo.Text = dsTask.Tables[0].Rows[0]["assigned_to_user"].ToString();
            drpProjects.SelectedValue = drpProjects.Items.FindByText(dsTask.Tables[0].Rows[0]["project"].ToString()).Value;
            drpUsers.SelectedValue = drpUsers.Items.FindByText(dsTask.Tables[0].Rows[0]["assigned_to_user"].ToString()).Value;
            drpStatuses.SelectedValue = drpStatuses.Items.FindByText(dsTask.Tables[0].Rows[0]["status"].ToString()).Value;
            drpCategories.SelectedValue = drpCategories.Items.FindByText(dsTask.Tables[0].Rows[0]["category"].ToString()).Value;
            drpPriorities.SelectedValue = drpPriorities.Items.FindByText(dsTask.Tables[0].Rows[0]["priority"].ToString()).Value;
            drpProjectType.SelectedValue = drpProjectType.Items.FindByText(dsTask.Tables[0].Rows[0]["project_type"].ToString()).Value;
            txtShortDescr.Text = dsTask.Tables[0].Rows[0]["short_desc"].ToString();
            txtRelevantURL.Text = dsTask.Tables[0].Rows[0]["Relevant_URL"].ToString();


            prevProject.Value = drpProjects.Items.FindByText(dsTask.Tables[0].Rows[0]["project"].ToString()).Value;
            prevAssignedTo.Value = drpUsers.Items.FindByText(dsTask.Tables[0].Rows[0]["assigned_to_user"].ToString()).Value;
            prevStatus.Value = drpStatuses.Items.FindByText(dsTask.Tables[0].Rows[0]["status"].ToString()).Value;
            prevCategory.Value = drpCategories.Items.FindByText(dsTask.Tables[0].Rows[0]["category"].ToString()).Value;
            prevPriority.Value = drpPriorities.Items.FindByText(dsTask.Tables[0].Rows[0]["priority"].ToString()).Value;
            prevProjectType.Value = drpProjectType.Items.FindByText(dsTask.Tables[0].Rows[0]["project_type"].ToString()).Value;
            prevShortDescr.Value = dsTask.Tables[0].Rows[0]["short_desc"].ToString();
            prevRelevantUrl.Value = dsTask.Tables[0].Rows[0]["Relevant_URL"].ToString();


            lnkSendAttachment.HRef = "upload_attachment.aspx?reqid=" + dsTask.Tables[0].Rows[0]["task_id"].ToString();
            lnkViewAttachment.HRef = "task_attachments.aspx?id=" + dsTask.Tables[0].Rows[0]["task_id"].ToString();
            DataSet dsTotalAttachments = DatabaseHelper.getAttachments(Convert.ToInt32(dsTask.Tables[0].Rows[0]["RequestId"].ToString()), "");
            lnkViewAttachment.InnerHtml = "View attachments (" + dsTotalAttachments.Tables[0].Rows.Count + ")";

            sql = "select * from task_comments where deleted <> 1 and  task_id = " + lblTaskId.Text.Trim() + " order by tc_id desc";

            DataSet dsTaskDetails = DatabaseHelper.getDataset(sql);

            DataGrid1.DataSource = dsTaskDetails.Tables[0];
            DataGrid1.DataBind();

            //--------------------------------
            sql = "select * from task_comments where task_id = " + lblTaskId.Text.Trim() + " order by tc_id desc";

            DataSet dsTaskDetails1 = DatabaseHelper.getDataset(sql);

            dsTaskDetails1.Tables[0].Columns.Add("temp");

            for (int i = 0; i < dsTaskDetails1.Tables[0].Rows.Count; i++)
            {
                dsTaskDetails1.Tables[0].Rows[i]["temp"] = "C";
            }

            sql = "select * from task_updates where task_id = " + lblTaskId.Text.Trim() + " order by tc_id desc";

            DataSet dsTaskUpdates = DatabaseHelper.getDataset(sql);

            System.Type myDataType;
            myDataType = System.Type.GetType("System.Int32");
            dsTaskUpdates.Tables[0].Columns.Add("deleted", myDataType);
            dsTaskUpdates.Tables[0].Columns.Add("temp");

            for (int i = 0; i < dsTaskUpdates.Tables[0].Rows.Count; i++)
            {
                dsTaskUpdates.Tables[0].Rows[i]["deleted"] = 0;
                dsTaskUpdates.Tables[0].Rows[i]["temp"] = "U";
            }

            dsTaskDetails1.Merge(dsTaskUpdates);


            DataView view = new DataView(dsTaskDetails1.Tables[0]);
            view.Sort= "post_date Desc, temp desc ";
            
            DataGrid2.DataSource = viusing System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

public partial class Admin_test : System.Web.UI.Page
{
    int id;
    int reqid;
    string sql = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = (Request.QueryString["id"] == null || Request.QueryString["id"].ToString() == "" ? "edit_task.aspx" : "edit_task.aspx?id=" + Request.QueryString["id"].ToString());
            Response.Redirect("login.aspx");
            return;
        }

        divMessage1.InnerHtml = "";
        divMessage.InnerHtml = "";

        string task_id = Request["id"];
        if (task_id == null || task_id == "0" || task_id == "")
        {
            id = 0;
        }
        else
        {
            if (DatabaseHelper.is_int(task_id))
            {
                id = Convert.ToInt32(task_id);
            }
            else
            {
                // Display an error because the bugid must be an integer
                divMessage.InnerHtml = "Task ID must be an integer.";
                divMessage.InnerHtml += "<p><a href='tasks.aspx'>View tasks</a></p>";
                DivEntry.Visible = false;
                return;
            }
        }

        string req_id = Request["reqid"];
        if (req_id == null || req_id == "0" || req_id == "")
        {
            reqid = 0;
            lnkBackToRequest.Visible = false;
        }
        else
        {
            if (DatabaseHelper.is_int(req_id))
            {
                reqid = Convert.ToInt32(req_id);
                lnkBackToRequest.HRef = "edit_request.aspx?reqid=" + reqid.ToString();
                lnkBackToRequest.Visible = true;
            }
            else
            {
                // Display an error because the bugid must be an integer
                divMessage.InnerHtml = "<br>Change request ID must be an integer.";
                divMessage.InnerHtml += "<p><a href='client_requests.aspx'>View change requests</a></p>";
                DivEntry.Visible = false;
                return;
            }
        }

        if (!Page.IsPostBack)
        {
            chkShowAllHistory.Attributes.Add("onclick", "checkChecked();");
            load_dropdowns();
            set_default_selection();

            if (id == 0)
            {
                if (reqid != 0)
                {
                    sql = "\nSelect * from ClientRequest where RequestId=" + reqid.ToString() + ";";

                    sql += "\nSelect * from ClientRequest_Details where RequestId=" + reqid.ToString() + " order by CommentId desc";

                    DataSet dsClientRequest = DatabaseHelper.getDataset(sql);

                    // change request
                    if (dsClientRequest.Tables[0].Rows.Count > 0)
                    {
                        txtShortDescr.Text = dsClientRequest.Tables[0].Rows[0]["ShortDescr"].ToString();
                        txtRelevantURL.Text = dsClientRequest.Tables[0].Rows[0]["WebsiteURL"].ToString();
                    }
                    else
                    {
                        divMessage.InnerHtml = "<br>Request ID not found.";
                        divMessage.InnerHtml += "<p><a href='client_requests.aspx'>View change requests</a></p>";
                        DivEntry.Visible = false;
                        lnkBackToRequest.Visible = false;
                        return;
                    }

                    // merge change request comments
                    if (dsClientRequest.Tables[1].Rows.Count > 0)
                    {
                        ftbComment.Text = "";

                        for (int i = 0; i < dsClientRequest.Tables[1].Rows.Count; i++)
                        {
                            ftbComment.Text += dsClientRequest.Tables[1].Rows[i]["Comment"].ToString() + "<br/><hr/><br/>";
                        }
                    }

                    lnkSendAttachment.HRef = "upload_attachment.aspx?reqid=" + reqid.ToString();
                    lnkViewAttachment.HRef = "view_attachments.aspx?reqid=" + reqid.ToString();
                    DataSet dsTotalAttachments = DatabaseHelper.getAttachments(reqid, "");
                    lnkViewAttachment.InnerHtml = "View attachments (" + dsTotalAttachments.Tables[0].Rows.Count + ")";
                }

                btnUpdate.Text = "Create";
                btnDelete.Visible = false;
                lblTaskId.Text = "New";
                lblProject.Text = drpProjects.SelectedItem.Text;
                lblAssignedTo.Text = drpUsers.SelectedItem.Text;
            }
            else
            {
                // Get this entry's data from the db and fill in the form

                lnkSubscribers.HRef = "view_subscribers.aspx?id=" + id.ToString();
                lnkSubscribers.Visible = true;
                lblTaskId.Text = id.ToString();
                BindData();
            }

            if (!DatabaseHelper.can_Delete_Tasks(Session["admin"].ToString()))
            {
                btnDelete.Visible = false;
            }
        }
    }

    public void BindData()
    {
        sql = "select * from tasks where deleted <> 1 and task_id = " + lblTaskId.Text.Trim();

        DataSet dsTask = DatabaseHelper.getDataset(sql);

        if (dsTask == null || dsTask.Tables.Count <= 0 || dsTask.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "<br>Task not found.<br><br><a href='tasks.aspx'>View tasks</a>";
            DivEntry.Visible = false;
            divTaskDetails.Visible = false;
        }
        else
        {
            lblTaskId.Text = dsTask.Tables[0].Rows[0]["task_id"].ToString();
            lblLastUpdated.Text = " Last changed by <b>" + dsTask.Tables[0].Rows[0]["last_updated_user"].ToString() + "</b> on <b>" + DateTime.Parse(dsTask.Tables[0].Rows[0]["last_updated_date"].ToString()).ToString("dd MMM h:mm tt") + "</b>";
            lblReportedBy.Text = "Reported by <b>" + dsTask.Tables[0].Rows[0]["reported_user"].ToString() + "</b> on <b>" + DateTime.Parse(dsTask.Tables[0].Rows[0]["reported_date"].ToString()).ToString("dd MMM h:mm tt") + "</b>";

            lblProject.Text = dsTask.Tables[0].Rows[0]["project"].ToString();
            lblAssignedTo.Text = dsTask.Tables[0].Rows[0]["assigned_to_user"].ToString();
            drpProjects.SelectedValue = drpProjects.Items.FindByText(dsTask.Tables[0].Rows[0]["project"].ToString()).Value;
            drpUsers.SelectedValue = drpUsers.Items.FindByText(dsTask.Tables[0].Rows[0]["assigned_to_user"].ToString()).Value;
            drpStatuses.SelectedValue = drpStatuses.Items.FindByText(dsTask.Tables[0].Rows[0]["status"].ToString()).Value;
            drpCategories.SelectedValue = drpCategories.Items.FindByText(dsTask.Tables[0].Rows[0]["category"].ToString()).Value;
            drpPriorities.SelectedValue = drpPriorities.Items.FindByText(dsTask.Tables[0].Rows[0]["priority"].ToString()).Value;
            drpProjectType.SelectedValue = drpProjectType.Items.FindByText(dsTask.Tables[0].Rows[0]["project_type"].ToString()).Value;
            txtShortDescr.Text = dsTask.Tables[0].Rows[0]["short_desc"].ToString();
            txtRelevantURL.Text = dsTask.Tables[0].Rows[0]["Relevant_URL"].ToString();


            prevProject.Value = drpProjects.Items.FindByText(dsTask.Tables[0].Rows[0]["project"].ToString()).Value;
            prevAssignedTo.Value = drpUsers.Items.FindByText(dsTask.Tables[0].Rows[0]["assigned_to_user"].ToString()).Value;
            prevStatus.Value = drpStatuses.Items.FindByText(dsTask.Tables[0].Rows[0]["status"].ToString()).Value;
            prevCategory.Value = drpCategories.Items.FindByText(dsTask.Tables[0].Rows[0]["category"].ToString()).Value;
            prevPriority.Value = drpPriorities.Items.FindByText(dsTask.Tables[0].Rows[0]["priority"].ToString()).Value;
            prevProjectType.Value = drpProjectType.Items.FindByText(dsTask.Tables[0].Rows[0]["project_type"].ToString()).Value;
            prevShortDescr.Value = dsTask.Tables[0].Rows[0]["short_desc"].ToString();
            prevRelevantUrl.Value = dsTask.Tables[0].Rows[0]["Relevant_URL"].ToString();


            lnkSendAttachment.HRef = "upload_attachment.aspx?reqid=" + dsTask.Tables[0].Rows[0]["task_id"].ToString();
            lnkViewAttachment.HRef = "task_attachments.aspx?id=" + dsTask.Tables[0].Rows[0]["task_id"].ToString();
            DataSet dsTotalAttachments = DatabaseHelper.getAttachments(Convert.ToInt32(dsTask.Tables[0].Rows[0]["RequestId"].ToString()), "");
            lnkViewAttachment.InnerHtml = "View attachments (" + dsTotalAttachments.Tables[0].Rows.Count + ")";

            sql = "select * from task_comments where deleted <> 1 and  task_id = " + lblTaskId.Text.Trim() + " order by tc_id desc";

            DataSet dsTaskDetails = DatabaseHelper.getDataset(sql);

            DataGrid1.DataSource = dsTaskDetails.Tables[0];
            DataGrid1.DataBind();

            //--------------------------------
            sql = "select * from task_comments where task_id = " + lblTaskId.Text.Trim() + " order by tc_id desc";

            DataSet dsTaskDetails1 = DatabaseHelper.getDataset(sql);

            dsTaskDetails1.Tables[0].Columns.Add("temp");

            for (int i = 0; i < dsTaskDetails1.Tables[0].Rows.Count; i++)
            {
                dsTaskDetails1.Tables[0].Rows[i]["temp"] = "C";
            }

            sql = "select * from task_updates where task_id = " + lblTaskId.Text.Trim() + " order by tc_id desc";

            DataSet dsTaskUpdates = DatabaseHelper.getDataset(sql);

            System.Type myDataType;
            myDataType = System.Type.GetType("System.Int32");
            dsTaskUpdates.Tables[0].Columns.Add("deleted", myDataType);
            dsTaskUpdates.Tables[0].Columns.Add("temp");

            for (int i = 0; i < dsTaskUpdates.Tables[0].Rows.Count; i++)
            {
                dsTaskUpdates.Tables[0].Rows[i]["deleted"] = 0;
                dsTaskUpdates.Tables[0].Rows[i]["temp"] = "U";
            }

            dsTaskDetails1.Merge(dsTaskUpdates);


            DataView view = new DataView(dsTaskDetails1.Tables[0]);
            view.Sort= "post_date Desc, temp desc ";
            
            DataGrid2.DataSource = view;
            DataGrid2.DataBind();

            if (chkShowAllHistory.Checked == true)
            {
                divAllHistory.Style.Add("display", "block");
                divNoHistory.Style.Add("display", "none");
            }
            else
            {
                divAllHistory.Style.Add("display", "none");
                divNoHistory.Style.Add("display", "block");
            }
        }
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        if (lblTaskId.Text == "New")
        {
            sql = @"DECLARE @TaskId INT;";
            sql += " insert into tasks([RequestId],[short_desc],[reported_user],[reported_date],[status],[priority],[category],[project],[project_type],[Relevant_URL],[assigned_to_user],[last_updated_user],[last_updated_date],[deleted]) ";
            sql += " values(" + reqid.ToString() + ",";
            sql += "'" + txtShortDescr.Text.Trim().Replace("'", "''") + "',";
            sql += "'" + Session["admin"].ToString().Replace("'", "''") + "',";
            sql += "getdate(),";
            sql += "'" + drpStatuses.SelectedItem.Text.Replace("'", "''") + "',";
            sql += "'" + drpPriorities.SelectedItem.Text.Replace("'", "''") + "',";
            sql += "'" + drpCategories.SelectedItem.Text.Replace("'", "''") + "',";
            sql += "'" + drpProjects.SelectedItem.Text.Replace("'", "''") + "',";
            sql += "'" + drpProjectType.SelectedItem.Text.Replace("'", "''") + "',";
            sql += "'" + txtRelevantURL.Text.Trim().Replace("'", "''") + "',";
            sql += "'" + drpUsers.SelectedItem.Text.Replace("'", "''") + "',";
            sql += "'" + Session["admin"].ToString().Replace("'", "''") + "',";
            sql += "getdate(),";
            sql += "0); ";
            sql += " SET @TaskId = SCOPE_IDENTITY(); ";

            if (ftbComment.Text.Trim() != "")
            {
                sql += " insert into task_comments([task_id],[username],[post_date],[comment],[deleted]) ";
                sql += " values(@TaskId,";
                sql += "'" + Session["admin"].ToString().Replace("'", "''") + "',";
                sql += "getdate(),";
                sql += "'" + ftbComment.Text.Trim().Replace("'", "''") + "',";
                sql += "0); ";
            }
            sql += "select @TaskId;";

            object objResult = DatabaseHelper.executeScalar(sql);

            if (objResult.ToString() != "0")
            {
                // add subscribers for the task
                add_subscription(objResult);

                // assign Internal reference number (task id) for change request
                object objIRN = DatabaseHelper.updateInternalReference(reqid, objResult.ToString());

                sendNotification(objResult.ToString());

                Response.Redirect("edit_task.aspx?id=" + objResult.ToString());
            }
            else
            {
                divMessage.InnerHtml = "Task was not created.";
            }
        }
        else
        {
            sql = " update tasks set ";
            sql += " [short_desc]='" + txtShortDescr.Text.Trim() + "',";
            sql += " [status]='" + drpStatuses.SelectedItem.Text.Replace("'", "''") + "',";
            sql += " [priority]='" + drpPriorities.SelectedItem.Text.Replace("'", "''") + "',";
            sql += " [category]='" + drpCategories.SelectedItem.Text.Replace("'", "''") + "',";
            sql += " [project]='" + drpProjects.SelectedItem.Text.Replace("'", "''") + "',";
            sql += " [project_type]='" + drpProjectType.SelectedItem.Text.Replace("'", "''") + "',";
            sql += " [Relevant_URL]='" + txtRelevantURL.Text.Trim().Replace("'", "''") + "',";
            sql += " [assigned_to_user]='" + drpUsers.SelectedItem.Text.Replace("'", "''") + "',";
            sql += " [last_updated_user]='" + Session["admin"].ToString().Replace("'", "''") + "',";
            sql += " [last_updated_date]=getdate()";
            sql += " where task_id = " + lblTaskId.Text.Trim() + "; ";

            if (ftbComment.Text.Trim() != "")
            {
                sql += " insert into task_comments([task_id],[username],[post_date],[comment],[deleted]) ";
                sql += " values(" + lblTaskId.Text.Trim() + ",";
                sql += "'" + Session["admin"].ToString().Replace("'", "''") + "',";
                sql += "getdate(),";
                sql += "'" + ftbComment.Text.Trim().Replace("'", "''") + "',";
                sql += "0); ";
            }

            sql += record_changes();

            int intResult = DatabaseHelper.executeNonQuery(sql);

            if (intResult != 0)
            {
                if (lblAssignedTo.Text.Trim() != drpUsers.SelectedItem.Text.Trim())
                {
                    object objResult = DatabaseHelper.executeScalar("select count(*) from task_subscriptions where task_id=" + lblTaskId.Text.Trim() + " and username='" + drpUsers.SelectedItem.Text.Trim().Replace("'", "''") + "'");

                    if (objResult.ToString() == "0")
                    {
                        sql = "insert into task_subscriptions ([task_id],[username]) ";
                        sql += "values(" + lblTaskId.Text.Trim() + ",'" + drpUsers.Text.Trim().Replace("'", "''") + "')";

                        intResult = DatabaseHelper.executeNonQuery(sql);
                    }
                }

                sendNotification(lblTaskId.Text.Trim());

                if (lblAssignedTo.Text.Trim() != drpUsers.SelectedItem.Text.Trim())
                {
                    if (!DatabaseHelper.isAdmin(lblAssignedTo.Text.Trim()))
                    {
                        intResult = DatabaseHelper.executeNonQuery("delete from task_subscriptions where task_id=" + lblTaskId.Text.Trim() + " and username='" + lblAssignedTo.Text.Trim().Replace("'", "''") + "'");
                    }
                }
                BindData();
                divMessage1.InnerHtml = "Task was updated.";
                ftbComment.Text = "";
            }
            else
            {
                divMessage1.InnerHtml = "Task was not updated.";
            }
        }
    }

    private string record_changes()
    {
        string base_sql = @" insert into task_updates([task_id],[username],[post_date],[comment]) 
                            values($task_id,'$us',getdate(),'$comment'); ";

        base_sql = base_sql.Replace("$task_id", lblTaskId.Text.Trim());
        base_sql = base_sql.Replace("$us", Session["admin"].ToString().Replace("'", "''"));

        string strSql = "";

        if (prevRelevantUrl.Value != txtRelevantURL.Text.Trim())
        {
            strSql += base_sql.Replace(
               "$comment",
               "changed Relevant URL  from \""
               + prevRelevantUrl.Value.Replace("'", "''") + "\" to \""
               + txtRelevantURL.Text.Trim().Replace("'", "''") + "\"");

            //prevRelevantUrl.Value = txtRelevantURL.Text.Trim();
        }

        if (prevShortDescr.Value != txtShortDescr.Text.Trim())
        {
            strSql += base_sql.Replace(
                "$comment",
                "changed desc from \""
                + prevShortDescr.Value.Replace("'", "''") + "\" to \""
                + txtShortDescr.Text.Trim().Replace("'", "''") + "\"");

            //prevShortDescr.Value = txtShortDescr.Text.Trim();
        }

        if (drpProjectType.SelectedValue != prevProjectType.Value)
        {
            strSql += base_sql.Replace(
               "$comment",
               "changed project type from \""
               + prevProjectType.Value.Replace("'", "''") + "\" to \""
               + drpProjectType.SelectedItem.Text.Replace("'", "''") + "\"");

            //prevProjectType.Value = drpProjectType.SelectedItem.Value;	    
        }

        if (drpProjects.SelectedValue != prevProject.Value)
        {
            strSql += base_sql.Replace(
                "$comment",
                "changed project from \""
                + prevProject.Value.Replace("'", "''") + "\" to \""
                + drpProjects.SelectedItem.Text.Replace("'", "''") + "\"");

            //prevProject.Value = drpProjects.SelectedItem.Value;
        }

        if (prevCategory.Value != drpCategories.SelectedValue)
        {
            strSql += base_sql.Replace(
                "$comment",
                "changed category from \""
                + prevCategory.Value.Replace("'", "''") + "\" to \""
                + drpCategories.SelectedItem.Text.Replace("'", "''") + "\"");

            //prev_category.Value = drpCategories.SelectedValue;
        }

        if (prevPriority.Value != drpPriorities.SelectedValue)
        {

            strSql += base_sql.Replace(
                "$comment",
                "changed priority from \""
                + prevPriority.Value.Replace("'", "''") + "\" to \""
                + drpPriorities.SelectedItem.Text.Replace("'", "''") + "\"");

            //prevPriority.Value = drpPriorities.SelectedValue;
        }

        if (prevAssignedTo.Value != drpUsers.SelectedValue)
        {

            strSql += base_sql.Replace(
                "$comment",
                "changed assigned_to from \""
                + prevAssignedTo.Value.Replace("'", "''") + "\" to \""
                + drpUsers.SelectedItem.Text.Replace("'", "''") + "\"");

            //prevAssignedTo.Value = drpUsers.SelectedValue;		    
        }

        if (prevStatus.Value != drpStatuses.SelectedValue)
        {
            strSql += base_sql.Replace(
                "$comment",
                "changed status from \""
                + prevStatus.Value.Replace("'", "''") + "\" to \""
                + drpStatuses.SelectedItem.Text.Replace("'", "''") + "\"");

            //prevStatus.Value = drpStatuses.SelectedValue;
        }

        return strSql;
    }

    void sendNotification(string strTaskId)
    {
        sql = "select email ";
        sql += " from users, task_subscriptions ";
        sql += " where users.username = task_subscriptions.username ";
        sql += " and task_subscriptions.task_id =" + strTaskId.ToString();

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            string toEmails = "";

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (toEmails == "")
                {
                    toEmails = ds.Tables[0].Rows[i]["email"].ToString();
                }
                else
                {
                    toEmails += ";" + ds.Tables[0].Rows[i]["email"].ToString();
                }
            }
            string strHtml = generateAdminEmail(strTaskId);

            bool flag = DatabaseHelper.sendEmailTasks(toEmails, "Task ID:" + strTaskId.ToString() + " was updated - " + txtShortDescr.Text.Trim() + " (Task ID:" + strTaskId.ToString() + ")", strHtml);
        }
        else
        {

        }
    }

    private string record_changes_for_email()
    {
        string strSql = "";
             
         if (drpProjects.SelectedValue != prevProject.Value)
        {
            strSql += "Project has been changed to \""
                + drpProjects.SelectedItem.Text.Replace("'", "''") + "\"<br>";

            //prevProject.Value = drpProjects.SelectedItem.Value;
        }

        if (prevAssignedTo.Value != drpUsers.SelectedValue)
        {
            strSql += "Assigned_to has been changed to \""
                + drpUsers.SelectedItem.Text.Replace("'", "''") + "\"<br>";

            //prevAssignedTo.Value = drpUsers.SelectedValue;		    
        }

        if (prevStatus.Value != drpStatuses.SelectedValue)
        {
            strSql += "Status has been changed to \""
                + drpStatuses.SelectedItem.Text.Replace("'", "''") + "\"<br>";

            //prevStatus.Value = drpStatuses.SelectedValue;
        }

        if (prevPriority.Value != drpPriorities.SelectedValue)
        {
            strSql += "Priority has been changed to \""
                + drpPriorities.SelectedItem.Text.Replace("'", "''") + "\"<br>";

            //prevPriority.Value = drpPriorities.SelectedValue;
        }

        if (prevCategory.Value != drpCategories.SelectedValue)
        {
            strSql += "Category has been changed to \""
                + prevCategory.Value.Replace("'", "''") + "\" to \""
                + drpCategories.SelectedItem.Text.Replace("'", "''") + "\"<br>";

            //prev_category.Value = drpCategories.SelectedValue;
        }

        if (drpProjectType.SelectedValue != prevProjectType.Value)
        {
            strSql += "Project type has been changed to \""
               + drpProjectType.SelectedItem.Text.Replace("'", "''") + "\"<br>";

            //prevProjectType.Value = drpProjectType.SelectedItem.Value;	    
        }

        if (prevRelevantUrl.Value != txtRelevantURL.Text.Trim())
        {
            strSql += "Relevant URL has been changed to  \""
               + txtRelevantURL.Text.Trim().Replace("'", "''") + "\"<br>";

            //prevRelevantUrl.Value = txtRelevantURL.Text.Trim();
        }

        if (prevShortDescr.Value != txtShortDescr.Text.Trim())
        {
            strSql += "Desc has been changed to \""
                + txtShortDescr.Text.Trim().Replace("'", "''") + "\"<br>";

            //prevShortDescr.Value = txtShortDescr.Text.Trim();
        }

        if (ftbComment.Text.Trim() != "" && lblTaskId.Text != "New")
        {
            strSql += "Comment has been added.<br>";
        }   

        return strSql;
    }

    private string generateAdminEmail(string strTaskId)
    {
        DataSet dsTask = DatabaseHelper.getDataset("select * from tasks where task_id=" + strTaskId);
        string strBody = "";
        if (dsTask == null || dsTask.Tables.Count <= 0 || dsTask.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "Task not found.<br><br><a href='tasks.aspx'>View tasks</a>";
        }
        else
        {
            strBody += "<div>Task: <span style=\"color:red\">";
            
            if (lblTaskId.Text == "New")
            {
                strBody += ((string)DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + Session["admin"].ToString() + "'")) + " has added task ";
            }

            strBody += " (<a href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_task.aspx?id=" + strTaskId + "'>" + strTaskId + "</a>) </span> " + DateTime.Parse(dsTask.Tables[0].Rows[0]["reported_date"].ToString()).ToString("dd MMM h:mm tt") + "<br><br>";

            if (lblTaskId.Text != "New")
            {
                strBody += "Below attributes are been updated.";
                strBody += "<span style=\"color:red\">" + record_changes_for_email() + "</span><br>";
            }

            strBody += "<a href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_task.aspx?id=" + strTaskId + "'>" + txtShortDescr.Text.Trim() + "</a>&nbsp;&nbsp;&nbsp;(" + dsTask.Tables[0].Rows[0]["priority"].ToString() + " : " + dsTask.Tables[0].Rows[0]["status"].ToString() +")<br><br>";
            strBody += "Emp: " + ((string)DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + dsTask.Tables[0].Rows[0]["assigned_to_user"].ToString() + "'")) + "<br>";
            strBody += "Project: " + dsTask.Tables[0].Rows[0]["project"].ToString() + "<br>";
            strBody += "Relevant URL: <a href='" + dsTask.Tables[0].Rows[0]["Relevant_URL"].ToString() + "' target='_blank'>" + dsTask.Tables[0].Rows[0]["Relevant_URL"].ToString() + "</a><br>";
            strBody += "<br><br>";

            sql = "Select * from task_comments where task_id=" + strTaskId.ToString() + " and deleted <> 1 order by tc_id desc";

            DataSet dsTaskDetails = DatabaseHelper.getDataset(sql);

            strBody += "<table cellspacing=\"0\" border=\"0\" style=\"width: 95%; border-collapse: collapse;\">";
            strBody += "<tbody>";

            for (int i = 0; i < dsTaskDetails.Tables[0].Rows.Count; i++)
            {
                strBody += "<tr>";
                strBody += "<td>";
                strBody += "<div style=\"border:solid 1px green; margin-bottom:2px\">";
                strBody += "<table width=\"100%\">";
                strBody += "<tbody>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span style=\"color: Green;\">comment " + dsTaskDetails.Tables[0].Rows[i]["tc_id"].ToString() + " posted by " + dsTaskDetails.Tables[0].Rows[i]["username"].ToString() + " on " + DateTime.Parse(dsTaskDetails.Tables[0].Rows[i]["post_date"].ToString()).ToString("dd MMM h:mm tt") + "</span>";
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\"> </td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span>" + dsTaskDetails.Tables[0].Rows[i]["comment"].ToString() + "</span>";
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "</tbody>";
                strBody += "</table>";
                strBody += "</div>";
                strBody += "</td>";
                strBody += "</tr>";
            }
            strBody += "</tbody>";
            strBody += "</table>";
        }
        return strBody;
    }

    //private string generateAdminEmail(string strTaskId)
    //{
    //    DataSet dsTask = DatabaseHelper.getDataset("select * from tasks where task_id=" + strTaskId);
    //    string strBody = "";
    //    if (dsTask == null || dsTask.Tables.Count <= 0 || dsTask.Tables[0].Rows.Count <= 0)
    //    {
    //        divMessage.InnerHtml = "Task not found.<br><br><a href='tasks.aspx'>View tasks</a>";
    //    }
    //    else
    //    {
    //        strBody += "<div>Task ID: <a href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_task.aspx?id=" + strTaskId + "'>" + strTaskId + "</a><br>";
    //        strBody += "Short Description: <a href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_task.aspx?id=" + strTaskId + "'>" + txtShortDescr.Text.Trim() + "</a><br><br>";
    //        strBody += "<table border=\"1\" cellpadding=\"3\" cellSpacing=\"0\">";
    //        strBody += "<tr><td>Last changed by </td><td>" + dsTask.Tables[0].Rows[0]["last_updated_user"].ToString() + "</td></tr>";
    //        strBody += "<tr><td>Reported By </td><td>" + dsTask.Tables[0].Rows[0]["reported_user"].ToString() + "</td></tr>";
    //        strBody += "<tr><td>Reported On </td><td>" + DateTime.Parse(dsTask.Tables[0].Rows[0]["reported_date"].ToString()).ToString("dd-MM-yyyy hh:mm tt") + "</td></tr>";
    //        strBody += "<tr><td>Project</td><td>" + dsTask.Tables[0].Rows[0]["project"].ToString() + "</td></tr>";
    //        strBody += "<tr><td>Category</td><td>" + dsTask.Tables[0].Rows[0]["category"].ToString() + "</td></tr>";
    //        strBody += "<tr><td>Priority</td><td>" + dsTask.Tables[0].Rows[0]["priority"].ToString() + "</td></tr>";
    //        strBody += "<tr><td>Assigned to </td><td>" + dsTask.Tables[0].Rows[0]["assigned_to_user"].ToString() + "</td></tr>";
    //        strBody += "<tr><td>Status </td><td>" + dsTask.Tables[0].Rows[0]["status"].ToString() + "</td></tr>";
    //        strBody += "<tr><td>Project type</td><td>" + dsTask.Tables[0].Rows[0]["project_type"].ToString() + "</td></tr>";
    //        strBody += "<tr><td>Relevant URL </td><td><a href='" + dsTask.Tables[0].Rows[0]["Relevant_URL"].ToString() + "' target='_blank'>" + dsTask.Tables[0].Rows[0]["Relevant_URL"].ToString() + "</a></td></tr>";
    //        strBody += "</table>";

    //        sql = "Select * from task_comments where task_id=" + strTaskId.ToString() + " and deleted <> 1 order by tc_id desc";

    //        DataSet dsTaskDetails = DatabaseHelper.getDataset(sql);

    //        sql = "Select * from task_updates where task_id=" + strTaskId.ToString() + " order by tc_id desc";

    //        DataSet dsTaskUpdates = DatabaseHelper.getDataset(sql);

    //        strBody += "<table cellspacing=\"0\" border=\"1\" style=\"width: 100%; border-collapse: collapse;\">";
    //        strBody += "<tbody>";

    //        for (int i = 0; i < dsTaskUpdates.Tables[0].Rows.Count; i++)
    //        {
    //            strBody += "<tr>";
    //            strBody += "<td>";
    //            strBody += "<div style=\"border: 1px none Green;\">";
    //            strBody += "<table width=\"100%\">";
    //            strBody += "<tbody>";
    //            strBody += "<tr>";
    //            strBody += "<td align=\"left\">";
    //            strBody += "<span style=\"color: Green;\">changed by  " + dsTaskUpdates.Tables[0].Rows[i]["username"].ToString() + " on " + DateTime.Parse(dsTaskUpdates.Tables[0].Rows[i]["post_date"].ToString()).ToString("dd-MM-yyyy hh:mm tt") + "</span>";
    //            strBody += "</td>";
    //            strBody += "</tr>";
    //            strBody += "<tr>";
    //            strBody += "<td align=\"left\"> </td>";
    //            strBody += "</tr>";
    //            strBody += "<tr>";
    //            strBody += "<td align=\"left\">";
    //            strBody += "<span>" + dsTaskUpdates.Tables[0].Rows[i]["comment"].ToString() + "</span>";
    //            strBody += "</td>";
    //            strBody += "</tr>";
    //            strBody += "</tbody>";
    //            strBody += "</table>";
    //            strBody += "</div>";
    //            strBody += "</td>";
    //            strBody += "</tr>";
    //        }

    //        for (int i = 0; i < dsTaskDetails.Tables[0].Rows.Count; i++)
    //        {
    //            strBody += "<tr>";
    //            strBody += "<td>";
    //            strBody += "<div style=\"border: 1px none Green;\">";
    //            strBody += "<table width=\"100%\">";
    //            strBody += "<tbody>";
    //            strBody += "<tr>";
    //            strBody += "<td align=\"left\">";
    //            strBody += "<span style=\"color: Green;\">comment " + dsTaskDetails.Tables[0].Rows[i]["tc_id"].ToString() + " posted by " + dsTaskDetails.Tables[0].Rows[i]["username"].ToString() + " on " + DateTime.Parse(dsTaskDetails.Tables[0].Rows[i]["post_date"].ToString()).ToString("dd-MM-yyyy hh:mm tt") + "</span>";
    //            strBody += "</td>";
    //            strBody += "</tr>";
    //            strBody += "<tr>";
    //            strBody += "<td align=\"left\"> </td>";
    //            strBody += "</tr>";
    //            strBody += "<tr>";
    //            strBody += "<td align=\"left\">";
    //            strBody += "<span>" + dsTaskDetails.Tables[0].Rows[i]["comment"].ToString() + "</span>";
    //            strBody += "</td>";
    //            strBody += "</tr>";
    //            strBody += "</tbody>";
    //            strBody += "</table>";
    //            strBody += "</div>";
    //            strBody += "</td>";
    //            strBody += "</tr>";
    //        }
    //        strBody += "</tbody>";
    //        strBody += "</table>";
    //    }
    //    return strBody;
    //}
    protected void btnDelete_Click(object sender, EventArgs e)
    {

        sql = "update tasks set deleted=1 where task_id = " + lblTaskId.Text + "; ";
        sql += " Update task_comments set deleted=1 where task_id=" + lblTaskId.Text;

        int intResult = DatabaseHelper.executeNonQuery(sql);

        if (intResult != 0)
        {
            Response.Redirect("tasks.aspx");
        }
        else
        {
            divMessage1.InnerHtml = "Task " + lblTaskId.Text + "  was not deleted.";
            divMessage1.Visible = true;
        }
    }
    protected void DataGrid1_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "edit")
        {
            Response.Redirect("edit_comment.aspx?id=" + e.Item.Cells[0].Text + "&tcid=" + e.Item.Cells[1].Text);
        }
        else if (e.CommandName == "delete")
        {
            sql = "Update task_comments set deleted=1 where tc_id=" + e.Item.Cells[1].Text;

            int intResult = DatabaseHelper.executeNonQuery(sql);

            if (intResult != 0)
            {
                divMessage1.InnerHtml = "Comment " + e.Item.Cells[1].Text + "  was deleted.";

                divMessage1.Visible = true;
                ftbComment.Text = "";
                BindData();
            }
            else
            {
                divMessage1.InnerHtml = "