<%@ Page Language="C#" AutoEventWireup="true" CodeFile="toh.aspx.cs" Inherits="toh" %>
<%@ Register TagPrefix="uc3" TagName="FeatureProps" Src="~/control/FeatureProps.ascx" %>
<%@ Register TagPrefix="uc1" TagName="topmenu" Src="~/control/admintopmenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="advert" Src="~/control/advert.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="~/control/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="googleadd" Src="~/control/googleadd.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="shortcut icon" href="images/fav.ico" type="image/x-icon" />
<title>Simply Self Catering</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type="text/javascript">
function script2()
  {
        var strname=document.getElementById("advertname").value;              
        if (confirm("Are you sure you want to delete " +"\""+strname+"\""+"?" )==true)
        {            
            var SearchProp = document.getElementById("Button1");    
            SearchProp.click();     
        }   
        
  }
</script>
<style type="text/css">
<!--

    .style1
    {
        padding: 0.5em;
        FONT-WEIGHT: normal;
        FONT-SIZE: 11px;
        COLOR: #000000;
        FONT-STYLE: normal;
        FONT-FAMILY:Trebuchet MS, Verdana, Arial, Helvetica, sans-serif;
        line-height: 1.5em;
        width: 220px;
    }
-->
</style>
<link href="class/master.css" rel="stylesheet" type="text/css">
<script type="text/javascript" language="javascript" src="js/calendarDateInput.js"></script>
<script type="text/javascript" src="lightbox.js"></script>
<script type="text/javascript">
 function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }
</script>
</head>
<body onLoad="MM_preloadImages('images/sendmessagehover.png')" topm	argin="0">
    <form id="form1" runat="server">
    <asp:HiddenField ID="advertname" runat="server" />
    <uc1:topmenu ID="topmenu1" runat="server" />
    <div id="main_bg">
    <table width="903" height="1088" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01">

	<tr>
	  <td width="903" height="877" valign="top" background="images/index_04.png"><table width="99%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td height="875" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="right">
              <tr>
                <td width="162"><div style="padding-top:0.3em;">
                <uc3:FeatureProps ID="FeatureProps1" runat="server" />
                </div>
                 </td>
              </tr>
              <tr>
                <td style="height:4px;"></td>
              </tr>
              <tr>
                <td><div id="goolgebigads"><script type="text/javascript">
				<!--
					google_ad_client = "pub-4055850565108441";
					/* Simply Self Catering 142x600 */
					google_ad_slot = "6042387017";
					google_ad_width = 142;
					google_ad_height = 588;
					//-->
				</script>
				<script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
				</script></div></td>
              </tr>
            </table>
          <br></td>
          <td width="83%" valign="top">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td style="height:6px;"></td>
            </tr>
            <tr>
              <td>
              <table  border="0" align="center" cellpadding="0" cellspacing="0" 
                      style="width:713px;">
              
              <tr>
                <td height="154" class="advert_input_red" style="width:698px;"><table width="100%" align="center" 
                        cellpadding="0" cellspacing="0" 
                        style="border:1px #042C5D solid; background-color:#FFFFFF;">
                    <tr>
                      <td class="text" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="83%" valign="top">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  
                                  <tr>
                                    <td class="advert_blue" colspan="3" align="center">
                                                    <asp:Label ID="Label2" runat="server"
                                                        Text="Add type of holiday"></asp:Label>
                                          </td>
                                      </tr>
                                  
                                  
                                  
                                  <tr>
                                    <td class="text" colspan="2" align="center">&nbsp;
                                                    </td>
                                    
                                      <td>&nbsp;</td>
                                  </tr>
                                  
                                  
                                  
                                  <tr>
                                    <td class="text" width="77" style="padding-left:5px;">
                                                    <asp:Label ID="lblLength" runat="server" Text="Type of holiday" 
                                                        Width="100px" Font-Bold="False"></asp:Label>
                                                </td>
                                    
                                    <td  class="style1" valign="top">
                                                                  
                                        <asp:DropDownList ID="ddlTOH" runat="server" Width="205px" 
                                            AutoPostBack="True" 
                                            onselectedindexchanged="ddlHoliday_SelectedIndexChanged" 
                                            DataTextField="TOHName" DataValueField="TOHId">
                                        </asp:DropDownList>
                                        
                                    
                                      </td>
                                      <td>
                                      <asp:ImageButton ID="btnAddUp" runat="server" onclick="btnAddUp_Click" 
                                            ImageUrl="~/images/addnew.png"/>
                                          &nbsp;
                                      <asp:ImageButton ID="btnDelete" runat="server" OnClientClick="script2()"
                                            ImageUrl="~/images/delete.png" Visible="False"/>
                                            <asp:Button ID="Button1" runat="server" onclick="Button1_Click" 
                                              BackColor="Transparent" BorderStyle="None" />
                                      </td>
                                  </tr>
                                  
                                  
                                  
                                  <tr>
                                    <td height="27" class="text" colspan="2">
                                        
                                        
                                        
                                    <asp:Panel ID="panelAdvert" runat="server" Width="340px" Visible="false">
                                        <table style="width:99%;" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="padding-left:5px;">
                                                    <asp:Label ID="Label1" runat="server" Text="Type of holiday" Width="100px" 
                                                        Font-Bold="False"></asp:Label>
                                                </td>
                                                <td><asp:TextBox ID="txtTOH" runat="server" Width="215px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                        ControlToValidate="txtTOH" ErrorMessage="Please enter type of holiday" 
                                                        ToolTip="Please enter type of holiday">*</asp:RequiredFieldValidator>
                                                    <br />
                                                    </td>
                                                
                                            </tr>
                                            <tr>
                                                <td valign="top" style="padding-left:5px;">
                                                    <asp:Label ID="Label3" runat="server" Font-Bold="False" 
                                                        Text="Short description" Width="100px"></asp:Label>
                                                </td>
                                                <td valign="top"><asp:TextBox ID="txtDesc" runat="server" Width="215px" Height="162px" 
                                                        TextMode="MultiLine"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                                        ControlToValidate="txtDesc" ErrorMessage="Please enter description" 
                                                        ToolTip="Please enter description">*</asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top">&nbsp;
                                                    </td>
                                                <td valign="top">
                                                    <asp:FileUpload ID="FileUpload1" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                              <td valign="top">&nbsp;</td>
                                              <td valign="top">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td valign="top">&nbsp;
                                                    </td>
                                                <td align="right" valign="top" style="padding-right: 13px;">
                                                    
                                                        </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;
                                                    
                                                </td>
                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:ImageButton ID="btnSubmit" runat="server" ImageUrl="~/images/save.png" 
                                                        onclick="btnSubmit_Click1" />
                                                        &nbsp;&nbsp;
                                                    <asp:ImageButton ID="btnCancel" runat="server" CausesValidation="False" 
                                                        ImageUrl="~/images/cancel.png" onclick="btnCancel_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                      </td>
                                     <td>
                                     <div id="divImg" runat="server">
                                     <img ID="imgMainphoto" runat="server" height="150" name="imgMainphoto" 
                                                        width="200"></img>
                                     </div>
                                     </td>
                                  </tr>
                                  
                                  
                                  
                                  <tr>
                                    <td height="27" class="text">&nbsp;
                                        
                                        
                                        
                                        </td>
                                    <td class="style1">
                <asp:Label ID="lblDisplay" runat="server" Text="Label" 
                    Visible="False" ForeColor="#FF3300"></asp:Label>
                                      </td>
                                  </tr>
                                  
                                  
                                  
                                  <tr>
                                    <td height="27" class="text" colspan="2">&nbsp;
                                        
                                        
                                        
                                        </td>
                                  </tr>
                                  
                                  
                                  
                                  <tr>
                                    <td height="27" class="text" colspan="2">
                                        
                                        
                                        
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Height="95px" 
                                            ShowMessageBox="True" ShowSummary="False" Width="114px" />
                                        
                                        
                                        
                                      </td>
                                  </tr>
                                </table>
                             </td>
                            
                          </tr>
                        </table>
                          </td>
                    </tr>
                </table></td>
              </tr>
            </table>
              
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
           
          </table></td>
        </tr>
        
      </table>
      </td>
</tr>

</table>
</div>
<uc1:footer id="footer1" runat="server" />
    </form>
</body>
</html>
