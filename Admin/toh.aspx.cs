﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class toh : System.Web.UI.Page
{
    string imgPath;
    //protected string strImagePath = @"E:\simplyselfcatering.estatesolutions.eu\TOH\";
    protected string strImagePath = "http://simplyselfcatering.estatesolutions.eu/TOH/";
    protected string strImagePathsave = "E://Websites/Rohit/simplyselfcatering.estatesolutions.eu/TOH/";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(IsPostBack))
        {
            fetchTOH();
            ViewState["AddUp"] = "Add new";
        }
    }
    public void fetchTOH()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = DatabaseHelper.fillTOH();


            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlTOH.DataSource = ds;
                ddlTOH.DataBind();
                ddlTOH.Items.Insert(0, new ListItem("Select holiday type", "Select holiday type"));
            }
            else
            {
                ddlTOH.DataSource = ds;
                ddlTOH.DataBind();
            }
        }
        catch
        {

        }
    }
    public void fetchTOHdata(int TOHId)
    {
        DataSet ds = new DataSet();
        ds = DatabaseHelper.fetchTOH(TOHId);
        try
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                ViewState["TOHId"] = ds.Tables[0].Rows[0]["TOHId"].ToString();
                txtTOH.Text = ds.Tables[0].Rows[0]["TOHName"].ToString();
                txtDesc.Text = ds.Tables[0].Rows[0]["TOHDesc"].ToString();
                imgMainphoto.Src =strImagePath+ ds.Tables[0].Rows[0]["TOHImg"].ToString();
                ViewState["photo"] = ds.Tables[0].Rows[0]["TOHImg"].ToString();  
            }
        }
        catch
        {
        }
    }
   
    protected void btnSubmit_Click1(object sender, ImageClickEventArgs e)
    {
        
        if (ViewState["save"].ToString() == "Edit")
        {
            if (FileUpload1.HasFile)
            {
                System.Drawing.Image img = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream);
                img.Save(strImagePathsave + "/" + txtTOH.Text.Trim() + "_" + "img1.jpg");
                imgPath = txtTOH.Text.Trim() + "_" + "img1.jpg";
            }
            else
            {
                if (ViewState["photo"] != null)
                {
                    imgPath = ViewState["photo"].ToString();
                }
            }
            int TOHUpdate = DatabaseHelper.updateTOH(Convert.ToInt32(ViewState["TOHId"]), txtTOH.Text.Trim(),txtDesc.Text.ToString().Replace("'"," ").Replace("/",""),imgPath);
            if (TOHUpdate > 0)
            {
                lblDisplay.Visible = true;
                lblDisplay.Text = "Updated successfully";
            }
            else
            {
                lblDisplay.Visible = true;
                lblDisplay.Text = "Error occurred";
            }
        }
        else
        {
            if (FileUpload1.HasFile)
            {
                System.Drawing.Image img = System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream);
                img.Save(strImagePathsave + "/" + txtTOH.Text.Trim() + "_" + "img1.jpg");
                imgPath = txtTOH.Text.Trim() + "_" + "img1.jpg";
            }
            int TOHInsert = DatabaseHelper.insertTOH(txtTOH.Text.Trim(), txtDesc.Text.ToString().Replace("'", " ").Replace("/", ""), imgPath);
            if (TOHInsert > 0)
            {
                lblDisplay.Visible = true;
                lblDisplay.Text = "Added successfully";
            }
            else
            {
                lblDisplay.Visible = true;
                lblDisplay.Text = "Error occurred";
            }
        }

        fetchTOH();
        panelAdvert.Visible = false;
        divImg.Style.Add("display", "none");
        lblLength.Visible = true;
        ddlTOH.Visible = true;
        btnAddUp.Visible = true;
        btnDelete.Visible = false;
        reset();
        ViewState["AddUp"] = "Add new";
        btnAddUp.ImageUrl = "images/addnew.png";
    }
    public void reset()
    {
        try
        {
            txtTOH.Text = string.Empty;
            ddlTOH.SelectedIndex = 0;
        }
        catch { }
    }
    protected void btnCancel_Click(object sender, ImageClickEventArgs e)
    {
        lblLength.Visible = true;
        ddlTOH.Visible = true;
        btnAddUp.Visible = true;
        panelAdvert.Visible = false;
        divImg.Style.Add("display", "none");
        ViewState["AddUp"] = "Add new";
        btnAddUp.ImageUrl = "images/addnew.png";
        fetchTOH();
    }
    protected void btnAddUp_Click(object sender, ImageClickEventArgs e)
    {
        lblDisplay.Visible = false;
        panelAdvert.Visible = true;
        divImg.Style.Add("display", "block");
        lblLength.Visible = false;
        ddlTOH.Visible = false;
        btnAddUp.Visible = false;
        btnDelete.Visible = false;
        if (ViewState["AddUp"].ToString() == "Edit")
        {
            ViewState["save"] = "Edit";
           // btnSubmit.ImageUrl = "images/edit.png";
            fetchTOHdata(Convert.ToInt32(ddlTOH.SelectedValue));
        }
        else
        {
            reset();
            ViewState["save"] = "Save";
           // btnSubmit.ImageUrl = "images/save.png";
        }
    }
    protected void ddlHoliday_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblDisplay.Visible = false;
        panelAdvert.Visible = false;
        divImg.Style.Add("display", "none");

        if (ddlTOH.SelectedValue == "Select swimming pool")
        {
            advertname.Value = ddlTOH.SelectedItem.ToString();
            btnAddUp.ImageUrl = "images/addnew.png";
            ViewState["AddUp"] = "Add new";
            btnDelete.Visible = false;
        }
        else
        {
            advertname.Value = ddlTOH.SelectedItem.ToString();
            btnAddUp.ImageUrl = "images/edit.png";
            ViewState["AddUp"] = "Edit";
            btnDelete.Visible = true;
        }
    }    
    protected void Button1_Click(object sender, EventArgs e)
    {
        DatabaseHelper.deleteTOH(ddlTOH.SelectedValue);
        fetchTOH();
        //ddlswimmingpool.SelectedIndex = 0;
        lblDisplay.Visible = true;
        lblDisplay.Text = "Deleted successfully";
        panelAdvert.Visible = false;
        divImg.Style.Add("display", "none");
        lblLength.Visible = true;
        ddlTOH.Visible = true;
        btnAddUp.Visible = true;
        btnDelete.Visible = false;
        reset();
        ViewState["AddUp"] = "Add new";
        btnAddUp.ImageUrl = "images/addnew.png";
    }
}
