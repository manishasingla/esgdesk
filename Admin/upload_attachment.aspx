<%@ Page Language="C#" AutoEventWireup="true" CodeFile="upload_attachment.aspx.cs"
    Inherits="send_attachment" %>

<%@ Register Src="~/Admin/Admin_Header.ascx" TagName="Header" TagPrefix="uc1" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc4" %>
<%@ Register Assembly="FlashUpload" Namespace="FlashUpload" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        Upload attachment</title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        function disableSendButton()
        {            
            document.getElementById("btnSend1").disabled=true;
            document.getElementById("divMessage").innerHTML ="Please wait: your file is being sent...";
            document.getElementById("btnSend").click();
            return false;
        }
    </script>
    <script type="text/javascript">
        function GetFileName(val) {
            document.getElementById("divMessage").innerHTML = "";
        }
   </script>

</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <uc1:Header ID="Header1" runat="server" />
        <div id="Content">
            <div id="pageTitle" style="height: 30px; font-weight: bold; text-align: left">
                Send attachment</div>
            <div id="divAttachment" runat="server" style="text-align: left">
                <table cellpadding="5">
                    <tr>
                        <td align="left" colspan="1">
                            <div id="divBackLink" runat="server">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <%--<div id="divMessage" runat="server" style="font-weight: bold; color: red; text-align: left">
                            </div>--%>
                            <div id="divFlash" runat="server">
                                <table cellpadding="3">
                                    <tr>
                                        <td colspan="3">
                                            <span style="color: Red; font-weight: bold;">
                                            (Please  upload attachments only for files such as images rather than attachments which have instructions for us. Include instructions in the request itself.
                                            Please do not exceed the size of 50MB in total)
                                            <%--(Please include text in the request itself. 
                                            Upload attachments only for files such as images and ensure they do not 
                                            exceed the size of 50MB in total)--%>

                                                <%--(Please include text in the request itself.
                                                Only upload attachments for such things as images and ensure they are no bigger
                                                than 50MB in total)--%>
                                                
                                                </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="3">
                                            Description(optional): &nbsp;&nbsp;
                                            <asp:TextBox ID="txtDescription" runat="server" Width="450px"></asp:TextBox>&nbsp;
                                            <asp:CheckBox ID="Chkalias" runat="server" Font-Bold="True" Text="Add as alias" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" align="left">
                                            
                                            <%--<cc2:FlashUpload ID="FlashUpload1" runat="server" UploadPage="Upload.axd" OnUploadComplete="UploadComplete()"
                                                FileTypeDescription="Images" FileTypes="*.*" UploadFileSizeLimit="52428800" TotalUploadSizeLimit="52428800">
                                            </cc2:FlashUpload>--%>
                                        <asp:FileUpload ID="FlashUpload1" runat="server" Onchange="GetFileName(this.value);" size="70px" />
                                       
                                         <asp:Button ID="btnSend" runat="server" Text="  Upload   " OnClick="btnSend_Click" />
                                        <div id="divMessage" runat="server" style="font-weight: bold; color: red; text-align: left">
                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                           
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <asp:Label ID="lblUserName" runat="server" Visible="False"></asp:Label>
                            <asp:Label ID="lblClientId" runat="server" Visible="False"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <uc4:footer ID="Footer2" runat="server" />
    </form>
</body>
</html>
