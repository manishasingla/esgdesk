using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Net.Mail;

public partial class send_attachment : System.Web.UI.Page
{
    private static int RequestId; 
    
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = (Request.QueryString["reqid"] == null || Request.QueryString["reqid"].ToString() == "" ? "client_requests.aspx" : "upload_attachment.aspx?reqid=" + Request.QueryString["reqid"].ToString());
            Response.Redirect("login.aspx");
            return;
        }

        divMessage.InnerHtml = "";
        if (Request.QueryString["reqid"] != null && Request.QueryString["reqid"] != "0")
        {
            if (!Page.IsPostBack)
            {
                try
                {
                    RequestId = Convert.ToInt16(Request.QueryString["reqid"].ToString());
                    divAttachment.Visible = true;
                    divBackLink.InnerHtml = "<a href=\"edit_request.aspx?reqid=" + RequestId.ToString() + "\">back to change request</a>";

                    string jscript = "function UploadComplete(){" + ClientScript.GetPostBackEventReference(btnSend, "") + "};";
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "FileCompleteUpload", jscript, true);
                    //=====================================================================================================
                    //FlashUpload1.QueryParameters = string.Format("UserName={0}", Session["admin"].ToString());
                    //=====================================================================================================

                    BindData(RequestId);
                }
                catch
                {
                    divMessage.InnerHtml = "Request Id must be Integer.<br><br><a href='client_requests.aspx'>View requests</a>";
                    divAttachment.Visible = false;
                }
            }
        }
        else
        {
            Response.Redirect("client_requests.aspx", false);
        }
    }

    public void BindData(int RequestId)
    {
        DataSet dsRequests = DatabaseHelper.getClientRequest(RequestId);

        if (dsRequests == null || dsRequests.Tables.Count <= 0 || dsRequests.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "Invalid requestId.<br><br><a href='client_requests.aspx'>back to change requests</a>";
            divMessage.Visible = true;
            divBackLink.Visible = false;
            divFlash.Visible = false;
        }
        else
        {
            lblClientId.Text = dsRequests.Tables[0].Rows[0]["ClientId"].ToString();
            lblUserName.Text = dsRequests.Tables[0].Rows[0]["UserName"].ToString();
        }
    }

    protected void btnSend_Click(object sender, EventArgs e)
    {

        bool flag = false;
        DataSet dsUserDetails = DatabaseHelper.getUserDetails(lblClientId.Text, lblUserName.Text);

        if (dsUserDetails == null || dsUserDetails.Tables.Count <= 0 || dsUserDetails.Tables[0].Rows.Count <= 0)
        {
            flag = sendAttachment("", RequestId);
        }
        else
        {
            flag = sendAttachment(dsUserDetails.Tables[0].Rows[0]["Email"].ToString(), RequestId);
        }
        if (flag == true)
        {
            divMessage.InnerHtml = "<br> Your attachment has been uploaded successfully..<br>";
            divMessage.Visible = true;
            //===============================
            //divFlash.Visible = false;
            divFlash.Visible = true;
            //===============================
            
            txtDescription.Text = "";
        }
        else
        {
            divMessage.InnerHtml = "<br>There was problem in uploading your attachment.";
            divMessage.Visible = true;
            //==========================
            //divFlash.Visible = false;
            divFlash.Visible = true;
           //==========================
        }
    }

    private bool sendAttachment(string email, int RequestId)
    {
        bool flag = false;

        try
        {
            string strBody = "";

            //======================UnComment After Testing=======================================
            //MailMessage mail = new MailMessage();
            //mail.From = ConfigurationManager.AppSettings["NoRplyFromEmail1"].ToString();
            //mail.Subject = "Attachment(s) for change request " + RequestId.ToString();

            //mail.BodyFormat = MailFormat.Html;
            //===============================================================

            //Change the path appropriately
            //string serverpath = Server.MapPath("Attachment") + "\\" + FileUpload1.FileName;
            //FileUpload1.SaveAs(serverpath);

            //=====================================================================================
            //string uploadPath = Server.MapPath(Request.ApplicationPath + "\\Attachment");
           // =====================================================================================
            
            string uploadPath1 = Server.MapPath(Request.ApplicationPath + "\\Attachment\\backup");

            //==================================================================
            //uploadPath = uploadPath + "\\" + Session["admin"].ToString(); // your code goes here
            //==================================================================

            //bool IsExists = System.IO.Directory.Exists(uploadPath);
            //if (IsExists)
            //{
            //    DirectoryInfo di = new DirectoryInfo(uploadPath);
            //    FileInfo[] rgFiles = di.GetFiles("*.*");
            //    foreach (FileInfo fi in rgFiles)
            //    {
            //        mail.Attachments.Add(new MailAttachment(uploadPath + "\\" + fi.Name));
            //    }
            //}
            //else
            //{
            //    return false;
            //}

            string strDownload = "";

            try
            {
                if (System.IO.Directory.Exists(uploadPath1))
                {
                    DirectoryInfo di = new DirectoryInfo(uploadPath1);
                    //=================================================
                    //FileInfo[] rgFiles = di.GetFiles("*.*");
                    //=================================================
                    if (Chkalias.Checked)
                    {



                        strDownload = "<b>" + lblUserName.Text + ": </b> <span style=\"color:red\">";
                        //strDownload += "Support has uploaded attachment(s) to your change request " + " (<a style=\"color:red\" href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/edit_request.aspx?reqid=" + RequestId + "'>" + RequestId + "</a>) </span> on " + DateTime.Parse(DateTime.Now.ToString()).ToString("dd MMM h:mm tt") + "<br>";
                        strDownload += "Attachment(s) have been added to your change request " + " (<a style=\"color:red\" href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/edit_request.aspx?reqid=" + RequestId + "'>" + RequestId + "</a>) </span> on " + DateTime.Parse(DateTime.Now.ToString()).ToString("dd MMM h:mm tt") + "<br>";
                    }
                    else
                    {
                        strDownload = "<b>" + lblUserName.Text + ": </b> <span style=\"color:red\">";
                        //strDownload += "Support has uploaded attachment(s) to your change request " + " (<a style=\"color:red\" href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/edit_request.aspx?reqid=" + RequestId + "'>" + RequestId + "</a>) </span> on " + DateTime.Parse(DateTime.Now.ToString()).ToString("dd MMM h:mm tt") + "<br>";
                        strDownload += "Attachment(s) have been added to your change request " + " (<a style=\"color:red\" href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/edit_request.aspx?reqid=" + RequestId + "'>" + RequestId + "</a>) </span> on " + DateTime.Parse(DateTime.Now.ToString()).ToString("dd MMM h:mm tt") + "<br>";
                    }


                  

                    //strBody += task_action;

                   //// strBody += task_action + " (<a style=\"color:red\" href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/edit_request.aspx?reqid=" + RequestId + "'>" + RequestId + "</a>) </span> " + DateTime.Parse(dsRequests.Tables[0].Rows[0]["LastUpdatedOn"].ToString()).ToString("dd MMM h:mm tt") + "<br><br>";


                    strDownload += txtDescription.Text.Trim() + "<br><br>Please download, Links to the attachment(s) are below:<br>";
                    //=====================================================
                   // foreach (FileInfo fi in rgFiles)
                   // {
                    //========================================================
                        string Pkey = System.Guid.NewGuid().ToString();

                       /// strDownload += "<br>" + fi.Name + "&nbsp; &nbsp;<a href='" + ConfigurationManager.AppSettings["WebAddress"] + "/setup.aspx?file=" + Pkey + "_" + fi.Name + "'>download</a>";
                        ////lblUserName.Text
                        if (Chkalias.Checked)
                        {
                            //======================================================================================================================
                            //object obj = DatabaseHelper.insertAttachment(RequestId, Pkey, fi.Name, txtDescription.Text, lblUserName.Text);
                            object obj = DatabaseHelper.insertAttachment(RequestId, Pkey, FlashUpload1.FileName , txtDescription.Text, lblUserName.Text);
                            //=======================================================================================================================
                            
                            try
                            {
                                //======================================================================================================================
                                //strDownload += "<br>" + fi.Name + "&nbsp; &nbsp;<a href='" + ConfigurationManager.AppSettings["WebAddress"] + "/setup.aspx?file=" + obj.ToString() + "'>download</a>";
                                strDownload += "<br>" + FlashUpload1.FileName + "&nbsp; &nbsp;<a href='" + ConfigurationManager.AppSettings["WebAddress"] + "/setup.aspx?file=" + obj.ToString() + "'>download</a>";
                                //======================================================================================================================
                            }
                            catch { }
                        }
                        else
                        {
                            //======================================================================================================================
                            //object obj = DatabaseHelper.insertAttachment(RequestId, Pkey, fi.Name, txtDescription.Text, "Support");
                            object obj = DatabaseHelper.insertAttachment(RequestId, Pkey, FlashUpload1.FileName, txtDescription.Text, "Support");
                            //======================================================================================================================
                            
                            try
                            {
                                //======================================================================================================================
                                //strDownload += "<br>" + fi.Name + "&nbsp; &nbsp;<a href='" + ConfigurationManager.AppSettings["WebAddress"] + "/setup.aspx?file=" + obj.ToString() + "'>download</a>";
                                strDownload += "<br>" + FlashUpload1.FileName + "&nbsp; &nbsp;<a href='" + ConfigurationManager.AppSettings["WebAddress"] + "/setup.aspx?file=" + obj.ToString() + "'>download</a>";
                                //======================================================================================================================
                            }
                            catch
                            {
                            }
                        }
                        try
                        {
                            //System.IO.File.Delete(uploadPath + "\\" + fi.Name);
                            //======================================================================================================================
                            //System.IO.File.Move(uploadPath + "\\" + fi.Name, uploadPath1 + "\\" + Pkey + "_" + fi.Name);
                            FlashUpload1.SaveAs(uploadPath1 + "\\" + Pkey + "_" + FlashUpload1.FileName);
                            //======================================================================================================================
                        }
                        catch { }
                    //=========================
                    //}
                    //=========================
                    di.Delete();
                }
            }
            catch (Exception ex)
            {
            }
            //*****************************************************
            string sqlcmt;
            sqlcmt = " insert into ClientRequest_Details(RequestId,Comment,PostedBy,PostedOn) ";
            sqlcmt += " values('" + RequestId.ToString() + "',";
            sqlcmt += "'New file is uploaded',";
            sqlcmt += "'Support',";
            sqlcmt += "getdate());";
            object objResult = DatabaseHelper.executeNonQuery(sqlcmt);
            //*****************************************************
            if (email != "")
            {
                strBody = strDownload + "<br><br>";

                strBody += generateEmail(RequestId);
                //============================================
                //mail.To = email;
                //mail.Body = strBody;
               
                //============================================
                

                try
                {
                    //============================================================================
                    MailMessage mMailMessage = new MailMessage();
                    System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                    msg.From = new MailAddress(ConfigurationManager.AppSettings["NoRplyFromEmail1"]);
                    msg.To.Add(new MailAddress(email));

                    msg.Subject = "Attachment(s) for change request " + RequestId.ToString();
                    msg.Body = strBody;
                    msg.IsBodyHtml = true;
                    msg.Priority = MailPriority.Normal;
                    System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("mail.estatesolutions.co.uk");
                    mSmtpClient.UseDefaultCredentials = false;
                    System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("test@estatesolutions.co.uk", "testtest");
                    mSmtpClient.Credentials = loginInfo;
                    mSmtpClient.Send(msg);
                    //============================================================================
                    //SmtpMail.Send(mail);
                }
                catch (Exception ex)
                {
                }
            }

            strBody = strDownload + "<br><br>"; 
            strBody += generateAdminEmail(RequestId);
            //=============================================================
            
            //mail.To = ConfigurationManager.AppSettings["ToEmail1"]; ///+ ";" + ConfigurationManager.AppSettings["ToEmail2"];
            //mail.Body = strBody; 
            //=============================================================
            

            try
            {
                //SmtpMail.Send(mail);
                //============================================================================
                MailMessage mMailMessage = new MailMessage();
                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                msg.From = new MailAddress(ConfigurationManager.AppSettings["NoRplyFromEmail1"]);
                msg.To.Add(new MailAddress(ConfigurationManager.AppSettings["ToEmail1"]));

                msg.Subject = "Attachment(s) for change request " + RequestId.ToString();
                msg.Body = strBody;
                msg.IsBodyHtml = true;
                msg.Priority = MailPriority.Normal;
                System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("mail.estatesolutions.co.uk");
                mSmtpClient.UseDefaultCredentials = false;
                System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("test@estatesolutions.co.uk", "testtest");
                mSmtpClient.Credentials = loginInfo;
                mSmtpClient.Send(msg);
                //============================================================================
                
            }
            catch (Exception ex)
            {
                flag = false;
            }

            // Uploaded file deleted after sending e-mail

            flag = true;
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
            flag = false;
        }
        return flag;
    }

    private string generateEmail(int RequestId)
    {
        DataSet dsRequests = DatabaseHelper.getClientRequest(RequestId);
        string strBody = "";
        if (dsRequests == null || dsRequests.Tables.Count <= 0 || dsRequests.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "Request not found.<br><br><a href='requests.aspx'>View requests</a>";
        }
        else
        {
            strBody += "<div>Request ID: <a href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/edit_request.aspx?reqid=" + RequestId + "'>" + RequestId + "</a><br>";
            strBody += "Short Description: <a href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/edit_request.aspx?reqid=" + RequestId + "'>" + dsRequests.Tables[0].Rows[0]["ShortDescr"].ToString() + "</a><br><br>";
            strBody += "<table border=\"1\" cellpadding=\"3\" cellSpacing=\"0\">";
            strBody += "<tr><td>Client ID:</td><td>" + dsRequests.Tables[0].Rows[0]["ClientId"].ToString() + "</td></tr>";
            strBody += "<tr><td>Last updated by:</td><td>" + dsRequests.Tables[0].Rows[0]["LastUpdatedBy"].ToString() + "</td></tr>";
            strBody += "<tr><td>Reported on:</td><td>" + dsRequests.Tables[0].Rows[0]["RequestDate"].ToString() + "</td></tr>";
            strBody += "<tr><td>Status:</td><td>" + dsRequests.Tables[0].Rows[0]["Status"].ToString() + "</td></tr>";
            strBody += "<tr><td>Title or preferably URL:</td><td>" + dsRequests.Tables[0].Rows[0]["WebsiteURL"].ToString() + "</td></tr>";
            strBody += "</table>";

            DataSet dsReqDetails = DatabaseHelper.getClientRequestDetails(RequestId);
            strBody += "<br>";
          
            strBody += "<table cellspacing=\"0\" border=\"1\" style=\"width: 100%; border-collapse: collapse;\">";
            strBody += "<tbody>";
            for (int i = 0; i < dsReqDetails.Tables[0].Rows.Count; i++)
            {
                strBody += "<tr>";
                strBody += "<td>";
                strBody += "<div style=\"border: 1px none Green;\">";
                strBody += "<table width=\"100%\">";
                strBody += "<tbody>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span style=\"color: Green;\">comment " + dsReqDetails.Tables[0].Rows[i]["CommentId"].ToString() + " posted by " + dsReqDetails.Tables[0].Rows[i]["PostedBy"].ToString() + " on " + dsReqDetails.Tables[0].Rows[i]["PostedOn"].ToString() + "</span>";
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\"> </td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span>" + dsReqDetails.Tables[0].Rows[i]["Comment"].ToString() + "</span>";
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "</tbody>";
                strBody += "</table>";
                strBody += "</div>";
                strBody += "</td>";
                strBody += "</tr>";
            }
            strBody += "</tbody>";
            strBody += "</table>";
            strBody += "<br>";
            strBody += "<span style=\'font-size:11.0pt;font-family:Calibri,sans-serif;color:red\'>Please do not reply to this email as it does not take replies. Please use the Change Request System as this is the quickest way to process your requests and also keeps a record of communications and progress for you to review at any time.</span>";
            strBody += "<br/><br/>" + "Thanks & Regards," + "<br/>Support.";
        }
        return strBody;
    }

    private string generateAdminEmail(int RequestId)
    {
        DataSet dsRequests = DatabaseHelper.getClientRequest(RequestId);
        string strBody = "";
        if (dsRequests == null || dsRequests.Tables.Count <= 0 || dsRequests.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "Request not found.<br><br><a href='requests.aspx'>View requests</a>";
        }
        else
        {
            strBody += "<div>Request ID: <a href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_request.aspx?reqid=" + RequestId + "'>" + RequestId + "</a><br>";
            strBody += "Short Description: <a href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_request.aspx?reqid=" + RequestId + "'>" + dsRequests.Tables[0].Rows[0]["ShortDescr"].ToString() + "</a><br><br>";
            strBody += "<table border=\"1\" cellpadding=\"3\" cellSpacing=\"0\">";
            strBody += "<tr><td>Client ID:</td><td>" + dsRequests.Tables[0].Rows[0]["ClientId"].ToString() + "</td></tr>";
            strBody += "<tr><td>Last updated by:</td><td>" + dsRequests.Tables[0].Rows[0]["LastUpdatedBy"].ToString() + "</td></tr>";
            strBody += "<tr><td>Reported on:</td><td>" + dsRequests.Tables[0].Rows[0]["RequestDate"].ToString() + "</td></tr>";
            strBody += "<tr><td>Status:</td><td>" + dsRequests.Tables[0].Rows[0]["Status"].ToString() + "</td></tr>";
            strBody += "<tr><td>Title or preferably URL:</td><td>" + dsRequests.Tables[0].Rows[0]["WebsiteURL"].ToString() + "</td></tr>";
            strBody += "</table>";

            DataSet dsReqDetails = DatabaseHelper.getClientRequestDetails(RequestId);
            strBody += "<br>";
            strBody += "<span style=\'font-size:11.0pt;font-family:Calibri,sans-serif;color:red\'>Please do not reply to this email as it does not take replies. Please use the Change Request System as this is the quickest way to process your requests and also keeps a record of communications and progress for you to review at any time.</span><br/>";
            strBody += "<br>";
            strBody += "<table cellspacing=\"0\" border=\"1\" style=\"width: 100%; border-collapse: collapse;\">";
            strBody += "<tbody>";
            for (int i = 0; i < dsReqDetails.Tables[0].Rows.Count; i++)
            {
                strBody += "<tr>";
                strBody += "<td>";
                strBody += "<div style=\"border: 1px none Green;\">";
                strBody += "<table width=\"100%\">";
                strBody += "<tbody>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span style=\"color: Green;\">comment " + dsReqDetails.Tables[0].Rows[i]["CommentId"].ToString() + " posted by " + dsReqDetails.Tables[0].Rows[i]["PostedBy"].ToString() + " on " + dsReqDetails.Tables[0].Rows[i]["PostedOn"].ToString() + "</span>";
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\"> </td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span>" + dsReqDetails.Tables[0].Rows[i]["Comment"].ToString() + "</span>";
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "</tbody>";
                strBody += "</table>";
                strBody += "</div>";
                strBody += "</td>";
                strBody += "</tr>";
            }
            strBody += "</tbody>";
            strBody += "</table>";
            strBody += "<br/><br/>" + "Thanks & Regards," + "<br/>Support.";
        }
        return strBody;
    }
}
