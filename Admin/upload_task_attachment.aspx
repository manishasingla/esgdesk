<%@ Page Language="C#" AutoEventWireup="true" CodeFile="upload_task_attachment.aspx.cs"
    Inherits="Admin_upload_test_attachment" %>

<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Src="Header.ascx" TagName="Header" TagPrefix="uc1" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc4" %>
<%@ Register Assembly="FlashUpload" Namespace="FlashUpload" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        Upload task attachment
    </title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        function disableSendButton() {
            document.getElementById("btnSend1").disabled = true;
            document.getElementById("divMessage").innerHTML = "Please wait: your file is being sent...";
            document.getElementById("btnSend").click();
            return false;
        }
    </script>

    <script type="text/javascript">
        function GetFileName(val)
        {
              document.getElementById("divMessage").innerHTML = "";   
        }
        function showWait()
        {
          if ($get('FileUpload2').value.length > 0)
            {
                $get('UpdateProgress1').style.display = 'block';
            }
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div>
        </div>
        <div id="wrapper">
            <uc2:Admin_Header ID="Admin_Header1" runat="server" />
            <div id="Content">
                <div id="pageTitle" style="height: 30px; font-weight: bold; text-align: left">
                    Upload task attachment</div>
                <div id="divAttachment" runat="server" style="text-align: left">
                    <table cellpadding="5" border="0">
                         <tr>
                            <td align="left" colspan="1">
                                <div id="divBackLink" runat="server">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" colspan="1">
                                <div id="divFlash" runat="server">
                                    <table cellpadding="3" border="0">
                                        <tr>
                                            <td colspan="3">
                                                <span style="color: Red; font-weight: bold;">(Please upload attachments only for files
                                                    such as images rather than attachments which have instructions for us. Include instructions
                                                    in the request itself. Please do not exceed the size of 20MB in total)
                                                    <%--(Please include text in the request itself. 
                                            Upload attachments only for files such as images and ensure they do not 
                                            exceed the size of 50MB in total)--%>
                                                    <%--(Please include text in the request itself.
                                                Only upload attachments for such things as images and ensure they are no bigger
                                                than 50MB in total)--%>
                                                    <asp:Label ID="lblmessage1" runat="server"></asp:Label>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" colspan="3">
                                                Description(optional): &nbsp;&nbsp;
                                                <asp:TextBox ID="txtDescription" runat="server" Width="450px"></asp:TextBox>
                                                <asp:CheckBox ID="chkEmailnotify" runat="server" Font-Bold="True" Text="Email notify"
                                                    Checked="true"></asp:CheckBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" align="left">
                                                <%--<asp:FileUpload ID="FlashUpload1" runat="server" 
                                                Onchange="GetFileName(this.value);"  size="70px"  />
                                                
                                            <asp:Button ID="btnSend" runat="server" Text="  Upload   " OnClick="btnSend_Click" />
                                            <br />
                                            <div id="divMessage" runat="server" style="font-weight: bold; color: red; text-align: left">
                                            </div>--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" align="left">
                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="Btnattach" />
                                                    </Triggers>
                                                    <ContentTemplate>
                                                        <input type="file" runat="server" id="FileUpload2" size="62" />
                                                        <asp:Button ID="Btnattach" runat="server" Text="Add to list" OnClick="Button1_Click" OnClientClick="javascript:showWait();"></asp:Button><br />
                                                   <asp:Label ID="lblMsg" runat="server"></asp:Label> 
                                                   <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                                    <ProgressTemplate>
                                                        <asp:Label ID="lblWait" runat="server" BackColor="#507CD1" Font-Bold="True" ForeColor="White"
                                                            Text="Please wait..."></asp:Label>
                                                    </ProgressTemplate>
                                                </asp:UpdateProgress>
                                                </ContentTemplate>
                                                </asp:UpdatePanel>
                                                <br/>
                                                <br/>
                                                <asp:Label ID="lbluploadedfile" runat="server" Text="List of file(s) to be uploaded"></asp:Label>
                                                <br/>
                                                <asp:ListBox ID="ListBox1" runat="server" Height="93px" Width="565px"></asp:ListBox>
                                                <%-- <asp:Button ID="BtnDelete" runat="server" Text="Delete" Width="70px" OnClick="Button3_Click"></asp:Button><br/>--%>
                                                <asp:Button ID="BtnUpload" runat="server" Text="Upload" OnClick="Button2_Click"></asp:Button>
                                                <div id="divMessage" runat="server" style="font-weight: bold; color: red; text-align: left">
                                                </div>
                                                <%--<div id="div1" runat="server" style="font-weight: bold; color: red; text-align: left">
                                                    </div>
                                                    <div id="div2" runat="server" style="font-weight: bold; color: red; text-align: left">
                                                    </div>
                                                    <div id="div3" runat="server" style="font-weight: bold; color: red; text-align: left">
                                                    </div>
                                                    <div id="div4" runat="server" style="font-weight: bold; color: red; text-align: left">
                                                    </div>
                                                    <div id="div5" runat="server" style="font-weight: bold; color: red; text-align: left">
                                                    </div>
                                                    <div id="div6" runat="server" style="font-weight: bold; color: red; text-align: left">
                                                    </div>
                                                    <div id="div7" runat="server" style="font-weight: bold; color: red; text-align: left">
                                                    </div>
                                                    <div id="div8" runat="server" style="font-weight: bold; color: red; text-align: left">
                                                    </div>--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <asp:Label ID="lblmessage2" runat="server"></asp:Label>
                                                <%--<asp:Button ID="btnSend" runat="server" Text="  Send   " OnClick="btnSend_Click"/>--%>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <asp:Label ID="lblUserName" runat="server" Visible="False"></asp:Label>
                                <asp:Label ID="lblClientId" runat="server" Visible="False"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <uc4:footer ID="Footer2" runat="server" />
    </form>
</body>
</html>
