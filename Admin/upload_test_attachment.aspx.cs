using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
using System.IO;

public partial class Admin_upload_test_attachment : System.Web.UI.Page
{
    int RequestId;
    int id;
    string sql = "";
    string strDownload = "";
    string task_action = "";
    string emailSubject = "";
    
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Session["admin"] == null || Session["admin"].ToString() == "")
        //{
        //Session["returnUrl"] = (Request.QueryString["reqid"] == null || Request.QueryString["reqid"].ToString() == "" ? "tasks.aspx" : "upload__task_attachment.aspx?id=" + Request.QueryString["id"].ToString());
        //Response.Redirect("login.aspx");
        //return;
        //}

        string task_id = Request["id"];
        if (task_id == null || task_id == "0" || task_id == "")
        {
            id = 0;
        }
        else
        {
            if (DatabaseHelper.is_int(task_id))
            {
                id = Convert.ToInt32(task_id);
            }
            else
            {
                // Display an error because the bugid must be an integer
                divMessage.InnerHtml = "<br>Task ID must be an integer.";
                divMessage.InnerHtml += "<p><a href='tasks.aspx'>View tasks</a></p>";
                return;
            }
        }

        divMessage.InnerHtml = "";

        {
            if (!Page.IsPostBack)
            {
                divAttachment.Visible = true;
                divBackLink.InnerHtml = "<a href=\"edit_task.aspx?id=" + id.ToString() + "\">back to task</a>";
                //======================comment for new task============================================================================
                //string jscript = "function UploadComplete(){" + ClientScript.GetPostBackEventReference(btnSend, "") + "};";
                //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "FileCompleteUpload", jscript, true);
                //==================================================================================================

                BindData();
            }
        }
    }

    public void BindData()
    {
        sql = "select * from tasks where deleted <>1 and task_id = " + id.ToString();

        DataSet dsRequests = DatabaseHelper.getDataset(sql);

        if (dsRequests == null || dsRequests.Tables.Count <= 0 || dsRequests.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "Task not found.<br><br><a href='tasks.aspx'>back to tasks</a>";
            divMessage.Visible = true;
            divBackLink.Visible = false;
            divFlash.Visible = false;
        }
    }

    //protected void btnSend_Click(object sender, EventArgs e)
    //{

    //    bool flag = false;

    //    flag = sendAttachment(id);

    //    if (flag == true)
    //    {
    //        divMessage.InnerHtml = "<br>Attachment was uploaded.<br>";
    //        divMessage.Visible = true;
    //        divFlash.Visible = true;
    //        txtDescription.Text = "";
    //    }
    //    else
    //    {
    //        divMessage.InnerHtml = "<br>Attachment was not uploaded.";
    //        divMessage.Visible = true;
    //        divFlash.Visible = true;
    //    }
    //}

    //private bool sendAttachment(int strId)
    //{

    //    bool flag = false;

    //    try
    //    {
    //        string uploadPath1 = Server.MapPath(Request.ApplicationPath + "\\Attachment\\backup");
    //        try
    //        {
    //            if (System.IO.Directory.Exists(uploadPath1))
    //            {
    //                DirectoryInfo di = new DirectoryInfo(uploadPath1);
    //                strDownload = txtDescription.Text.Trim() + "<br><br>Please download attachment(s)<br>";
    //                string Pkey = System.Guid.NewGuid().ToString();
    //                object obj = DatabaseHelper.insertTaskAttachment(strId, Pkey, FlashUpload1.FileName, txtDescription.Text, Session["admin"].ToString());
    //                strDownload += "<br>" + FlashUpload1.FileName + "&nbsp; &nbsp;<a href='" + ConfigurationManager.AppSettings["WebAddress"] + "/setup2.aspx?file=" + obj.ToString() + "'>download</a>";
    //                try
    //                {
    //                    FlashUpload1.SaveAs(uploadPath1 + "\\" + Pkey + "_" + FlashUpload1.FileName);
    //                }
    //                catch (Exception ex)
    //                {
    //                    System.IO.StreamWriter file = new System.IO.StreamWriter("D:\\log.txt");
    //                    file.WriteLine(ex.Message + ex.InnerException);

    //                    file.Close();
    //                }
    //                sendNotification(strId.ToString());

    //                flag = true;
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            flag = flag;
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        flag = false;
    //        System.IO.StreamWriter file = new System.IO.StreamWriter("D:\\log.txt");
    //        file.WriteLine(ex.Message + ex.InnerException);

    //        file.Close();

    //    }
    //    return flag;
    //}

    void sendNotification(string strTaskId)
    {
        sql = "select email ";
        sql += " from users, task_subscriptions ";
        sql += " where users.username = task_subscriptions.username ";
        sql += " and task_subscriptions.task_id =" + strTaskId.ToString() + " and task_subscriptions.username <> '" + Session["admin"].ToString() + "'";

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            string toEmails = "";

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (toEmails == "")
                {
                    toEmails = ds.Tables[0].Rows[i]["email"].ToString();
                }
                else
                {
                    toEmails += ";" + ds.Tables[0].Rows[i]["email"].ToString();
                }
            }


            task_action = ((string)DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + Session["admin"].ToString() + "'")) + " has uploaded attacment(s) to task ";

            emailSubject = "Task ID:" + strTaskId.ToString() + " was updated - (Task ID:" + strTaskId.ToString() + ")";

            string strHtml = generateAdminEmail(strTaskId);
            if (chkEmailnotify.Checked)
            {
                // ==============================================================================
                bool flag = DatabaseHelper.sendEmailTasks(toEmails, emailSubject, strHtml);
                // ==============================================================================
                //bool flag = DatabaseHelper.sendEmailTasks("avinashj@123SiteSolutions.com", emailSubject, strHtml);

            }
            else
            {
            }
        }
        else
        {

        }
    }

    private string generateAdminEmail(string strTaskId)
    {
        DataSet dsTask = DatabaseHelper.getDataset("select * from tasks where task_id=" + strTaskId);
        string strBody = "";
        if (dsTask == null || dsTask.Tables.Count <= 0 || dsTask.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "Task not found.<br><br><a href='tasks.aspx'>View tasks</a>";
        }
        else
        {
            strBody += "Task: <span style=\"color:red\">";

            strBody += task_action;

            strBody += " (<a style=\"color:red\" href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_task.aspx?id=" + strTaskId + "'>" + strTaskId + "</a>) </span> " + DateTime.Parse(dsTask.Tables[0].Rows[0]["last_updated_date"].ToString()).ToString("dd MMM h:mm tt") + "<br><br>";

            if (strDownload != "")
            {
                strBody += strDownload + "<br><br>";
            }

            strBody += "<a href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_task.aspx?id=" + strTaskId + "'>" + dsTask.Tables[0].Rows[0]["short_desc"].ToString() + "</a>&nbsp;&nbsp;&nbsp;(" + dsTask.Tables[0].Rows[0]["priority"].ToString() + " : " + dsTask.Tables[0].Rows[0]["status"].ToString() + ")<br><br>";
            strBody += "Emp: " + ((string)DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + dsTask.Tables[0].Rows[0]["assigned_to_user"].ToString() + "'")) + "<br>";
            strBody += "Project: " + dsTask.Tables[0].Rows[0]["project"].ToString() + "<br>";
            strBody += "Relevant URL: <a href='" + dsTask.Tables[0].Rows[0]["Relevant_URL"].ToString() + "' target='_blank'>" + dsTask.Tables[0].Rows[0]["Relevant_URL"].ToString() + "</a><br>";
            strBody += "<br><br>";

            sql = "Select * from task_comments where task_id=" + strTaskId.ToString() + " and deleted <> 1 order by tc_id desc";

            DataSet dsTaskDetails = DatabaseHelper.getDataset(sql);

            strBody += "<table cellspacing=\"0\" border=\"1\" style=\"width: 99%;border-collapse: collapse;\">";
            strBody += "<tbody>";

            for (int i = 0; i < dsTaskDetails.Tables[0].Rows.Count; i++)
            {
                strBody += "<tr>";
                strBody += "<td>";
                strBody += "<div style=\"border: 1px none Green;\">";
                strBody += "<table width=\"100%\">";
                strBody += "<tbody>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span style=\"color: Green;\">comment " + dsTaskDetails.Tables[0].Rows[i]["tc_id"].ToString() + " posted by " + dsTaskDetails.Tables[0].Rows[i]["username"].ToString() + " on " + DateTime.Parse(dsTaskDetails.Tables[0].Rows[i]["post_date"].ToString()).ToString("dd MMM h:mm tt") + "</span>";
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\"> </td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span>" + dsTaskDetails.Tables[0].Rows[i]["comment"].ToString() + "</span>";
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "</tbody>";
                strBody += "</table>";
                strBody += "</div>";
                strBody += "</td>";
                strBody += "</tr>";
            }
            strBody += "</tbody>";
            strBody += "</table>";
        }
        return strBody;
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        //==================Attach File===================
       if (Path.GetFileName(FileUpload2.PostedFile.FileName) != "")
         {
             
            string fileName = Path.GetFileName(FileUpload2.PostedFile.FileName);
             if (ViewState["i"] == null)
            {
                ViewState["i"] = 0;
            }
            if (fileName != null || fileName != string.Empty)
            {
                ListBox1.Items.Add(Path.GetFileName(FileUpload2.PostedFile.FileName));
                int i = Convert.ToInt32(ViewState["i"]);
                Session["myupload" + i] = FileUpload2;
                Session["Count"] = i;
               //============================================
                Session["DESC" + i] = txtDescription.Text.Trim();
               //=============================================
                i++;
                ViewState["i"] = i;
                txtDescription.Text = "";
            }
        }
    }
    //protected void Button3_Click(object sender, EventArgs e)
    //{
    //    //===========Delete==========================
    //    if (ListBox1.SelectedIndex > -1)
    //    {
    //        int uploadedFileIndex = ListBox1.SelectedIndex;
    //        Session.Remove("myupload" + uploadedFileIndex);
    //        ListBox1.Items.Remove(ListBox1.SelectedValue);
    //        if (Convert.ToInt32(Session["Count"]) > 0)
    //        {
    //            //Session["Count"] = Convert.ToInt32(Session["Count"]) - 1;
    //            ViewState["i"] = Convert.ToInt32(ViewState["i"]) - 1;
    //        }

    //    }
    //}
    protected void Button2_Click(object sender, EventArgs e)
    {
        bool flag = false;
        flag = sendAttachmentNew(id);
        if (flag == true)
        {
            divMessage.InnerHtml = "<br>Attachment was uploaded.<br>";
            divMessage.Visible = true;
            divFlash.Visible = true;
            txtDescription.Text = "";
        }
        else
        {
            //divMessage.InnerHtml = "<br>Attachment was not uploaded.";
            divMessage.Visible = true;
            divFlash.Visible = true;
        }

    }


    private bool sendAttachmentNew(int strId)
    {
        //=================Upload The File =============
        bool flag = false;
        if (Convert.ToInt32(Session["Count"]) > -1)
        {
            int sessionCount = Convert.ToInt32(Session["Count"]);
            //div7.InnerHtml = sessionCount.ToString();
            try
            {
                for (int i = sessionCount; i >= 0; i--)
                {

                    HtmlInputFile hif = (HtmlInputFile)Session["myupload" + i];
                    if (hif != null)
                    {
                        if (hif.PostedFile.ContentLength <= 22000000)
                        {
                            string storePath = Server.MapPath(Request.ApplicationPath + "\\Attachment\\backup");
                            if (!Directory.Exists(storePath))
                                Directory.CreateDirectory(storePath);
                            string Pkey = System.Guid.NewGuid().ToString();
                            strDownload = Session["DESC" + i] + "<br><br>Please download attachment(s)<br>";
                            object obj="";
                            try
                            {
                                
                                //div1.InnerHtml = Convert.ToInt32(strId) + "  ";
                                //div2.InnerHtml = Pkey.ToString() + "  ";
                                //div3.InnerHtml = Path.GetFileName(hif.PostedFile.FileName).ToString() + "  ";
                                //div4.InnerHtml = strDownload.ToString() + "  ";
                                //div5.InnerHtml = Session["admin"].ToString();
                                 obj = DatabaseHelper.insertTaskAttachment(Convert.ToInt32(strId), Pkey.ToString() , Path.GetFileName(hif.PostedFile.FileName).ToString() , strDownload.ToString() , Session["admin"].ToString());
                                 strDownload += "<br>" + Path.GetFileName(hif.PostedFile.FileName) + "&nbsp; &nbsp;<a href='" + ConfigurationManager.AppSettings["WebAddress"] + "/setup2.aspx?file=" + obj.ToString() + "'>download</a>";
                                 //DatabaseHelper.SendEmailList("avinashj@123SiteSolutions.com", "Error:In Save Record function ESGDESK when status is " + "DateTime : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), obj.ToString() + "   "  + strDownload + "\n");
                            }
                            catch (Exception ex)
                            {
                                flag = false;
                                DatabaseHelper.SendEmailList("avinashj@123SiteSolutions.com", "Error:In Save Record function ESGDESK when status is " + obj.ToString()  , " Input Parameters :" + " Message:" + ex.Message + "\n" + " Source:" + ex.Source + "\n" + " InnerException:" + ex.InnerException + " Stack Trace: " + ex.StackTrace + "\n");
                                divMessage.InnerHtml = ex.Message.ToString() + "Test";
                                return flag;
                            }
                            
                            try
                            {
                                string str1;
                                str1 = storePath + "\\" + Pkey + "_" + Path.GetFileName(hif.PostedFile.FileName);
                                //div6.InnerHtml = str1;
                                //div8.InnerHtml = i.ToString();
                                hif.PostedFile.SaveAs(str1);
                                divMessage.InnerHtml = "Your files are uploaded successfully";
                                flag = true;
                                Session["Count"] = -1;
                                ViewState["i"] = null;
                                ListBox1.Items.Clear();
                                txtDescription.Text = "";
                            }
                            catch (Exception ex)
                            {
                                flag = false;
                                divMessage.InnerHtml = "<br>Your uploaded file not save";
                                DatabaseHelper.SendEmailList("avinashj@123SiteSolutions.com", "Error:In file upload ESGDESK when status is " + "DateTime : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:" + ex.Message + "\n" + " Source:" + ex.Source + "\n" + " InnerException:" + ex.InnerException + " Stack Trace: " + ex.StackTrace + "\n");
                                return flag;
                            }
                            try
                            {
                                sendNotification(strId.ToString());
                            }
                            catch (Exception ex)
                            {
                                divMessage.InnerHtml = "<br>Error in sent notification";
                                flag = false;
                                DatabaseHelper.SendEmailList("avinashj@123SiteSolutions.com", "Error:In Notification function ESGDESK when status is " + "DateTime : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:" + ex.Message + "\n" + " Source:" + ex.Source + "\n" + " InnerException:" + ex.InnerException + " Stack Trace: " + ex.StackTrace + "\n");
                                return flag;
                            }
                        }
                        else
                        {
                            divMessage.InnerHtml = "An error occured";
                            flag = false; 
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                divMessage.InnerHtml = ex.Message.ToString() +"Error in newplace";
                flag = false;
                DatabaseHelper.SendEmailList("avinashj@123SiteSolutions.com", "Error:In new function ESGDESK when status is " + "DateTime : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:" + ex.Message + "\n" + " Source:" + ex.Source + "\n" + " InnerException:" + ex.InnerException + " Stack Trace: " + ex.StackTrace + "\n");
                return flag;
                
            }
            //Session.RemoveAll();

            return flag;
        }
        else
        {
            return flag;
        }
    }
















}
