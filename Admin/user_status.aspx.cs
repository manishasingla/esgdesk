using System.Web.UI.WebControls;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
//using System.Web.UI.WebControls;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Configuration;
using Telerik.Web.UI;
using Telerik.Web;

public partial class user_status : System.Web.UI.Page
{
    int closecount = 0;
    string sql = "";
    string task_action = "";
    string emailSubject = "";
    DateTime duedate;
    bool hidecheck;
    bool adminhide;
    DataSet dstask;
    int id;
   // string sql = "";
    string struname = "";
    ///page, dal  created by chaitali
    ///
    DataTable dt = new DataTable();
    Common dal = new Common();
    protected void Page_Load(object sender, EventArgs e)
    {

        //DataTable dt = new DataTable();
        //dt.Columns.Add("user_id", typeof(string));
        //dt.Columns.Add("username", typeof(string));
        //dt.Columns.Add("firstname", typeof(string)); 
        //dt.Columns.Add("lastname", typeof(string));
        //dt.Columns.Add("task", typeof(string));
        //dt.Columns.Add("Employeedetails", typeof(string));
        ////dt.Columns.Add("Req_CWO_status", typeof(string));
        ////dt.Columns.Add("admin", typeof(string));
        ViewState["SortExpression"] = "firstname ASC"; 
        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {

          
        }
        else
        {
            Session["adm"] = "yes";
            DataSet ds = dal.getUserreqcw("",3);
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                dt = ds.Tables[0];

                bindgrid();
                //DataGrid1.DataSource = ds;
                //DataGrid1.DataBind();
            }
          
            else
                divMessage.InnerHtml = "All employees have CWO status in their task list.";

        }
    }

    void bindgrid()
    {
        try
        {
            DataView dv = dt.DefaultView;
            if (ViewState["SortExpression"] != null)
            {
                dv.Sort = ViewState["SortExpression"].ToString();

            }
            DataGrid2.DataSource = dv;
            DataGrid2.DataBind();
        }
        catch (Exception ex)
        { 
        }

    }
    protected void DataGrid2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        DataGrid2.PageIndex = e.NewPageIndex;
        DataGrid2.DataSource = ViewState["dt"];
        DataGrid2.DataBind();
        //DataGrid2();
    }

    protected void DataGrid2_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            string[] strSortExpression = ViewState["SortExpression"].ToString().Split(' ');
            if (strSortExpression[0] == e.SortExpression)
            {
                if (strSortExpression[1] == "ASC")
                {
                    ViewState["SortExpression"] = e.SortExpression + " " + "DESC";
                }
                else
                {
                    ViewState["SortExpression"] = e.SortExpression + " " + "ASC";
                }
            }
            else
            {
                ViewState["SortExpression"] = e.SortExpression + " " + "ASC";
            }

            bindgrid();
           // DataGrid1.DataBind();
            //FnBIndEngineerGrid();
            //FnbindGrid();

        }
        catch (Exception EXE)
        {
            //objCommon.HandleException(EXE, "chaitalip@esolutionsgroup.co.uk", "", "");
        }

    }
}