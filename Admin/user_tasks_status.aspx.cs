using System.Web.UI.WebControls;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
//using System.Web.UI.WebControls;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Configuration;
using Telerik.Web.UI;
using Telerik.Web;

public partial class user_tasks : System.Web.UI.Page
{
    int closecount = 0;
    string sql = "";
    string task_action = "";
    string emailSubject = "";
    DateTime duedate;
    bool hidecheck;
    bool adminhide;
    DataSet dstask;
    int id;
   // string sql = "";
    string struname = "";
    ///page, dal  created by chaitali
    ///
    Common dal = new Common();
    DataTable dt = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {


        if (!IsPostBack)
        {
            dt.Columns.Add("user_id", typeof(string));
            dt.Columns.Add("username", typeof(string));
            dt.Columns.Add("firstname", typeof(string));
            dt.Columns.Add("lastname", typeof(string));
            dt.Columns.Add("task", typeof(string));
            dt.Columns.Add("Employeedetails", typeof(string));
            //dt.Columns.Add("Req_CWO_status", typeof(string));
            //dt.Columns.Add("admin", typeof(string));

            ViewState["SortExpression"] = "firstname ASC";
            if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
            {

                DataSet ds = dal.getUserreqcw(Session["admin"].ToString(), 2);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        DataSet ds_getdetails = dal.getUserStatus(ds.Tables[0].Rows[i]["username"].ToString());  //DatabaseHelper.NewgetusptaskDetail(filter, ds.Tables[0].Rows[i]["username"].ToString(), nofoRecord);
                        if (ds_getdetails.Tables.Count > 0 && ds_getdetails.Tables[0].Rows.Count > 0)
                        {

                        }
                        else
                        {
                            divMessage.InnerHtml = "No CWO status found in your task list. Make one task CWO. ";
                            //   dt.Rows.Add(struname);
                        }
                    }
                }

                //DataGrid1.DataSource = dt.DefaultView;
                //DataGrid1.DataBind();
            }
            else
            {
                Session["adm"] = "yes";
                DataSet ds = dal.getUserreqcw("", 1);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        //string filter = "where  status <> 'closed' and status <> 'Checked' and status <>'parked' and   deleted <> 1  and assigned_to_user = '" + ds.Tables[0].Rows[i]["username"].ToString() + "'";
                        //string nofoRecord = "Select top 20";

                        DataSet ds_getdetails = dal.getUserStatus(ds.Tables[0].Rows[i]["username"].ToString());  //DatabaseHelper.NewgetusptaskDetail(filter, ds.Tables[0].Rows[i]["username"].ToString(), nofoRecord);
                        if (ds_getdetails.Tables.Count > 0 && ds_getdetails.Tables[0].Rows.Count > 0)
                        {
                            //  divMessage.InnerHtml = "CWO status found in your task list.";



                        }
                        else
                        {
                            struname = ds.Tables[0].Rows[i]["username"].ToString();
                            string reqcwo = ds.Tables[0].Rows[i]["Req_CWO_status"].ToString();
                            string stradmin = ds.Tables[0].Rows[i]["admin"].ToString();
                            //  struname = ds.Tables[0].Rows[i]["firstname"].ToString() + " " + ds.Tables[0].Rows[i]["lastname"].ToString();
                            dt.Rows.Add(ds.Tables[0].Rows[i]["user_id"].ToString(), struname, ds.Tables[0].Rows[i]["firstname"].ToString(), ds.Tables[0].Rows[i]["lastname"].ToString());

                            //dt.Rows.Add(ds.Tables[0].Rows[i]["firstname"].ToString());
                            // dal.SendScheduleMail(ds.Tables[0].Rows[i]["email"].ToString(), ds.Tables[0].Rows[i]["firstname"].ToString());
                            //for (int j = 0; j < ds_getdetails.Tables[0].Rows.Count; j++)
                            //{
                            //struname = ds_getdetails.Tables[0].Rows[j]["assigned_to_user"].ToString();
                            //  struname = ds.Tables[0].Rows[i]["username"].ToString();
                            //if (struname == "")
                            //{
                            //    struname = ds.Tables[0].Rows[i]["assigned_to_user"].ToString() + ",";
                            //}
                            //else
                            //{
                            //    struname = struname + ds.Tables[0].Rows[i]["assigned_to_user"].ToString() + ",";
                            //}
                            //dt.Rows.Add(struname);
                            // divMessage.InnerHtml = "";
                            //}
                        }
                    }
                }

                //strReadUnread = "select distinct task_id from task_comments where tc_id not in(select tc_id from read_comments where username ='" + Session["admin"] + "')and task_comments.deleted <> 1 and task_id in (" + strtaskid + ")";
                if (dt.Rows.Count > 0)
                {

                    bindgrid();
                    //ViewState["dt"] = dt;
                    //DataGrid1.DataSource = dt.DefaultView;
                    //DataGrid1.DataBind();
                    //ViewState["dt"] = dt;

                }
                else
                    divMessage.InnerHtml = "All employees have CWO status in their task list.";

            }
        }
    }

    void bindgrid()
    {
        try
        {
            DataView dv = dt.DefaultView;
            if (ViewState["SortExpression"] != null)
            {
                dv.Sort = ViewState["SortExpression"].ToString();
            }

            DataGrid1.DataSource = dv;
            DataGrid1.DataBind();
        }
        catch (Exception ex)
        { 
        }
    }
    protected void DataGrid1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        DataGrid1.PageIndex = e.NewPageIndex;
        DataGrid1.DataSource = ViewState["dt"];
        DataGrid1.DataBind();
        //FnbindGrid();
    }

    //protected void DataGrid1_Sorting(object sender, GridViewSortEventArgs e)
    //{
    //    try
    //    {
    //        string[] strSortExpression = ViewState["SortExpression"].ToString().Split(' ');
    //        if (strSortExpression[0] == e.SortExpression)
    //        {
    //            if (strSortExpression[1] == "ASC")
    //            {
    //                ViewState["SortExpression"] = e.SortExpression + " " + "DESC";
    //            }
    //            else
    //            {
    //                ViewState["SortExpression"] = e.SortExpression + " " + "ASC";
    //            }
    //        }
    //        else
    //        {
    //            ViewState["SortExpression"] = e.SortExpression + " " + "ASC";
    //        }

    //        bindgrid();
    //        //DataGrid1.DataBind();
    //        //FnBIndEngineerGrid();
    //        //FnbindGrid();

    //    }
    //    catch (Exception EXE)
    //    {
    //        //objCommon.HandleException(EXE, "chaitalip@esolutionsgroup.co.uk", "", "");
    //    }

    //}
    protected void Datagrid1_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            string[] strSortExpression = ViewState["SortExpression"].ToString().Split(' ');
            if (strSortExpression[0] == e.SortExpression)
            {
                if (strSortExpression[1] == "ASC")
                {
                    ViewState["SortExpression"] = e.SortExpression + " " + "DESC";
                }
                else
                {
                    ViewState["SortExpression"] = e.SortExpression + " " + "ASC";
                }
            }
            else
            {
                ViewState["SortExpression"] = e.SortExpression + " " + "ASC";
            }

            bindgrid();
            //DataGrid1.DataBind();
            //FnBIndEngineerGrid();
            //FnbindGrid();

        }
        catch (Exception EXE)
        {
            //objCommon.HandleException(EXE, "chaitalip@esolutionsgroup.co.uk", "", "");
        }
    }
}