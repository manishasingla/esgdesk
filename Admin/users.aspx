<%@ Page Language="C#" AutoEventWireup="true" CodeFile="users.aspx.cs" Inherits="Admin_users" %>

<%@ Register Src="Notifications.ascx" TagName="Notifications" TagPrefix="uc4" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
       Users</title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
     table#DataGrid1_ctl00 tr th a
     {
      display:block;
      text-decoration:none;
      margin:0;
      padding:0;
      width:100%;
      height:100%;
     }
    </style>

</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <uc2:Admin_Header ID="Admin_Header1" runat="server" />
        <div id="Content">
            <div id="Content_98">
                <div class="titleText" style="margin: 10px 0 10px 0; display: none;">
                    Users&nbsp;|&nbsp;</div>
                <div>
                    <uc4:Notifications ID="Notifications" runat="server" />
                </div>
                <div style="float: right; width: 100%; background: #ebebeb; padding: 5px 0" align="right">
                    <span class="text_default" style="Border-bottom:1px dotted #000;float:left;"><a href="edit_user.aspx">&nbsp;&nbsp;Add
                        new user</a> </span><b>Records: </b>
                    <asp:Label ID="LblRecordsno" runat="server" Text=""></asp:Label>&nbsp;&nbsp;</div>
                <div style="clear: both">
                </div>
                <div id="divMessage" runat="server" style="color: Red; font-weight: bold">
                </div>
                <div>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            
                            
                            <%--**************************************************************************************************************--%>
                            <telerik:RadGrid ID="DataGrid1" runat="server" OnSortCommand="DataGrid1_SortCommand" OnPageIndexChanged="DataGrid1_PageIndexChanged" OnItemCommand="DataGrid1_ItemCommand" OnItemDataBound="DataGrid1_ItemDataBound" AutoGenerateColumns="False" CellPadding="5" AllowPaging="True" AllowSorting="True" PageSize="20" Width="100%">
                            <PagerStyle Mode="NextPrevAndNumeric"/>
                            <MasterTableView PagerStyle-AlwaysVisible="true">
                            <Columns>
                            
                            <%--0--%>
                            <telerik:GridTemplateColumn HeaderText="User id" SortExpression="user_id">
                            <ItemTemplate>
                            <asp:Label ID="lbluserid" runat="server" Text='<%#Bind("user_id") %>' ></asp:Label>
                            </ItemTemplate> 
                            </telerik:GridTemplateColumn>
                            
                            <%--1--%>
                            <telerik:GridTemplateColumn HeaderText="firstname" SortExpression="firstname">
                            <ItemTemplate>
                              <%--<a id="lblfirstname" runat="server" target="_blank" href='<%# "Priority_reports.aspx?Ename="+ Eval("username") %>'><%# Eval("firstname")%></a>--%>
                            <%--<asp:LinkButton  id="lblfirstname"  runat="server" PostBackUrl='<%# "Priority_reports.aspx?Ename="+ Eval("username") %>' ><%# Eval("firstname")%></asp:LinkButton>--%>
                            <asp:HyperLink id="lblfirstname"  runat="server"  NavigateUrl='<%# "Priority_reports.aspx?Ename="+ Eval("username") %>' Target="_blank" Text='<%# Eval("firstname")%>' ></asp:HyperLink>
                            </ItemTemplate> 
                            </telerik:GridTemplateColumn> 
                           
                            <%--2--%>
                            <telerik:GridTemplateColumn HeaderText="Last name" SortExpression="lastname">
                            <ItemTemplate>
                            <asp:Label id="lbllastname" runat="server" Text='<%#Bind("lastname") %>'></asp:Label>
                            </ItemTemplate> 
                            </telerik:GridTemplateColumn>
                             <%--3--%>
                            
                            <telerik:GridTemplateColumn HeaderText="Username" SortExpression="username">
                            <ItemTemplate>
                            <%--<a id="lblusername" runat="server" target="_blank" href='<%# "Priority_reports.aspx?Ename="+ Eval("username") %>'> <%# Eval("username")%></a>--%>
                            <%--<asp:LinkButton  id="lblusername"  runat="server"  PostBackUrl='<%# "Priority_reports.aspx?Ename="+ Eval("username") %>' ><%# Eval("username")%></asp:LinkButton>--%>
                            <asp:HyperLink id="lblusername"  runat="server"  NavigateUrl='<%# "Priority_reports.aspx?Ename="+ Eval("username") %>' Target="_blank" Text='<%# Eval("username")%>' ></asp:HyperLink>
                            
                            </ItemTemplate> 
                            </telerik:GridTemplateColumn> 
                             
                             <%--4--%>
                            
                           <%-- <telerik:GridTemplateColumn HeaderText="Password" SortExpression="password">
                            <ItemTemplate>
                            <asp:Label id="lblpassword" runat="server"  Text='<%#Bind("password")%>' ></asp:Label>
                            </ItemTemplate> 
                            </telerik:GridTemplateColumn>  --%> 
                            
                            <%--5--%>
                                  
                            <telerik:GridTemplateColumn HeaderText="Email" SortExpression="email">
                            <ItemTemplate>
                            <asp:Label ID="lblemail" runat="server"  Text='<%#bind("email") %>' ></asp:Label>
                            </ItemTemplate> 
                            </telerik:GridTemplateColumn>    
                            <%--6--%>
                            
                            <telerik:GridTemplateColumn HeaderText="Admin" SortExpression="admin">
                            <ItemTemplate>
                            <asp:Label id="lbladmin" runat="server" Text='<%#bind("admin") %>' ></asp:Label>
                            </ItemTemplate> 
                            </telerik:GridTemplateColumn> 
                            <%--7--%>
                            
                            <telerik:GridTemplateColumn HeaderText="Active" SortExpression="active">
                            <ItemTemplate>
                            <asp:Label ID="lblactive" runat="server" Text='<%#bind("active") %>'></asp:Label>
                            </ItemTemplate> 
                            </telerik:GridTemplateColumn>
                            
                            <%--8--%>
                            
                            <telerik:GridTemplateColumn HeaderText="Created by" SortExpression="created_user">
                            <ItemTemplate>
                            <asp:Label id="lblcreateduser" runat="server"  Text='<%# Bind("created_user") %>' ></asp:Label>
                            </ItemTemplate> 
                            </telerik:GridTemplateColumn>
                                             
                             <%--9--%> 
                                                     
                            <telerik:GridTemplateColumn HeaderText="Recent login on" SortExpression="most_recent_login_datetime">
                            <ItemTemplate>
                            <asp:Label ID="lblRecentLogin" runat="server" Text='<%# bind("most_recent_login_datetime","{0:g}") %>'></asp:Label>
                            </ItemTemplate> 
                            </telerik:GridTemplateColumn>
                            
                                                    
                                                  
                            
                            
                             
                            <%--10--%>
                            
                            <%--<telerik:GridHyperLinkColumn DataNavigateUrlFields="user_id" DataNavigateUrlFormatString="edit_user.aspx?id={0}"  HeaderText="Edit" NavigateUrl="edit_user.aspx" Text="edit">
                            </telerik:GridHyperLinkColumn> 
                            --%>
                            
                            <telerik:GridTemplateColumn HeaderText="Edit">
                              <ItemTemplate>
                                     <a href="edit_user.aspx?id=<%# Eval("user_id")%>">
                                        <%--<asp:ImageButton ID="editid" runat="server" ImageUrl="images/edit-icon.png" /> --%>
                                        <img   src="images/edit-icon.png" />
                                     </a>   
                               </ItemTemplate> 
                             </telerik:GridTemplateColumn>
                            
                            
                            
                            
                            
                            
                            
                             <%--11--%>
                            <%-- <telerik:GridTemplateColumn HeaderText="Delete">
                             <ItemTemplate>
                             <asp:LinkButton ID="lnkdelete" runat="server" CommandName="delete" Text="delete"></asp:LinkButton>
                             </ItemTemplate> 
                             </telerik:GridTemplateColumn>                                                             
                                --%>
                                
                             <telerik:GridTemplateColumn HeaderText="Delete">
                                   <ItemTemplate>
                                         <%--<asp:ImageButton ID="lnkdel" runat="server" CommandName="delete" ImageUrl="~/images/delete-icon.png" />--%>
                                         <asp:ImageButton ID="lnkdelete" runat="server" ImageUrl="images/delete-icon.png" CommandName="delete"/>
                                   <%-- <asp:LinkButton ID="lnkdelete" runat="server" CommandName="delete"><%--<image imageurl="~/images/delete-icon.png"></image></asp:LinkButton>--%>
                                  </ItemTemplate>
                              </telerik:GridTemplateColumn>
                                
                                
                                                             
                            </Columns>
                            </MasterTableView> 
                            </telerik:RadGrid>
                            
                            <%--**************************************************************************************************************--%>
                            
                            
                            
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
        <uc1:footer ID="Footer1" runat="server" />
        <asp:Label ID="lblOrderBy" runat="server" Text="" Visible="false"></asp:Label></div>
    </form>
</body>
</html>
