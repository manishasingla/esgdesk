using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
using Telerik.Web;

public partial class Admin_users : System.Web.UI.Page
{
    string sql = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = "users.aspx";
            Response.Redirect("login.aspx");
            return;
        }
        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            Response.Redirect("tasks.aspx");
            return;
        }
        divMessage.InnerHtml = "";
        if (!Page.IsPostBack)
        {
            lblOrderBy.Text = " ORDER BY user_id ASC";
            getFilterSession();
            bindData();
        }
    }
    private void setFileterSession()
    {
        Session["userOrderBy"] = lblOrderBy.Text;
    }

    private void getFilterSession()
    {
        try
        {
            if (Session["userOrderBy"] != null)
                lblOrderBy.Text = Session["userOrderBy"].ToString();
        }
        catch
        { }
    }

    private void bindData()
    {
        //string strSql = "select * from users " + lblOrderBy.Text;
        string strSql = "select * from users";
        DataSet ds = DatabaseHelper.getDataset(strSql);
        
        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                PagedDataSource objds = new PagedDataSource();
                objds.DataSource = ds.Tables[0].DefaultView;

                LblRecordsno.Text = objds.DataSourceCount.ToString();
                DataGrid1.DataSource = ds.Tables[0];
                DataGrid1.DataBind();
            }
            else
            {
                LblRecordsno.Text = "0";
                DataGrid1.DataSource = null;
                DataGrid1.DataBind();
                divMessage.InnerHtml = "No users in the database..";
            }
        }
        else
        {
            DataGrid1.DataSource = null;
            DataGrid1.DataBind();
            divMessage.InnerHtml = "No users in the database..";
        }
    }
    protected void DataGrid1_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        foreach (TableCell tc in e.Item.Cells)
        {
            tc.Attributes["style"] = "border:1px solid #00A651;";
        }
        if (e.Item is GridPagerItem)
        {
            GridPagerItem pagerItem = (GridPagerItem)e.Item;
            RadComboBox PageSizeComboBox = (RadComboBox)pagerItem.FindControl("PageSizeComboBox");
            PageSizeComboBox.Visible = false;
            Label changePageSizelbl = (Label)pagerItem.FindControl("ChangePageSizeLabel");
            changePageSizelbl.Visible = false;
        }
        if (e.Item.ItemType == GridItemType.Item || e.Item.ItemType == GridItemType.AlternatingItem)
        {
            GridDataItem dataItem = e.Item as GridDataItem;
            ImageButton deleteButton = (ImageButton)dataItem.FindControl("lnkdelete");
            deleteButton.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete user \"" + ((HyperLink)dataItem.FindControl("lblfirstname")).Text + " " + ((Label)dataItem.FindControl("lbllastname")).Text + "\"?')");
            ((Label)dataItem.FindControl("lblRecentLogin")).Text = DateTime.Parse(((Label)dataItem.FindControl("lblRecentLogin")).Text).ToString("dd MMM h:mm tt").ToString();
        }
    }
    protected void DataGrid1_ItemCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "delete")
        {
            GridDataItem dataItem = e.Item as GridDataItem;
            string strSql2 = "Update tasks set assigned_to_user='Rohit' where assigned_to_user='" + ((HyperLink)dataItem.FindControl("lblusername")).Text + "'";
            int intResult2 = DatabaseHelper.executeNonQuery(strSql2);
            string strSql15 = "delete from users where user_id=" + ((Label)dataItem.FindControl("lbluserid")).Text;
            int intResult15 = DatabaseHelper.executeNonQuery(strSql15);
            DataSet ds_dropdowns;
            Cache.Remove("dropdowns");
            sql = @"select project_name	from projects where active = 'Y' order by project_name;";
            // categories
            sql += "\nselect category_name from categories order by sort_seq, category_name;";
            // priorities
            sql += "\nselect priority_name from priorities order by sort_seq, priority_name;";
            // statuses
            sql += "\nselect status_name from statuses order by sort_seq, status_name;";
            // users
            sql += "\nselect username from users where active = 'Y' order by username;";
            ds_dropdowns = DatabaseHelper.getDataset(sql);
            //  Cache["Company_notes"] = ds;
            Cache.Insert("dropdowns", ds_dropdowns, null, DateTime.Now.AddHours(6), TimeSpan.Zero);
            if (intResult15 != 0)
            {
                divMessage.InnerHtml = "User was deleted.";
                bindData();

            }
            else
            {
                divMessage.InnerHtml = "User was not deleted.";
                bindData();
            }
        }

    }
    protected void DataGrid1_PageIndexChanged(object source, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        DataGrid1.CurrentPageIndex = e.NewPageIndex;
        bindData();
    }
    protected void DataGrid1_SortCommand(object source, Telerik.Web.UI.GridSortCommandEventArgs e)
    {
        if (e.SortExpression.ToString() == Session["userColumn"])
        {
            if (Session["userOrder"] == "DESC")
            {
                lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " ASC";
                Session["userOrder"] = "ASC";
            }
            else
            {
                lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " DESC";
                Session["userOrder"] = "DESC";
            }
        }
        else
        {
           lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " DESC";
            Session["userOrder"] = "DESC";
        }

        Session["userColumn"] = e.SortExpression.ToString();
        Session["userOrderBy"] = lblOrderBy.Text;
        bindData();
    }

    
    

    

    
}
