<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view_subscribers.aspx.cs"
    Inherits="Admin_view_subscribers" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <%= ConfigurationManager.AppSettings["CompanyName"].ToString()%> - view subscribers</title>
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <div id="divMessage" runat="server" style="color: Red; font-weight: bold">
        </div>
        <div id="DivEntry" runat="server">
            <div id="pageTitle">
                Subscribers for task
                <asp:Label ID="lblTaskId" runat="server" Text=""></asp:Label></div>
            <div style="border: solid 1px #cdcdcd;">
                <table cellpadding="2">
                    <tr>
                        <td align="left" valign="top">
                            Add subscriber:&nbsp;
                        </td>
                        <td align="left" valign="top">
                            <asp:DropDownList ID="drpUsers" runat="server" Width="175px">
                            </asp:DropDownList>
                        </td>
                        <td align="left" valign="top">
                            <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click" CssClass="blueBtns" />
                        </td>
                    </tr>
                </table>
            </div>
            <div id="divMessage1" runat="server" style="color: Red; font-weight: bold">
            </div>
            <div>
                <asp:DataGrid ID="DataGrid1" runat="server" AllowPaging="True" AllowSorting="True"
                    AutoGenerateColumns="False" CellPadding="5" OnItemCommand="DataGrid1_ItemCommand"
                    OnItemDataBound="DataGrid1_ItemDataBound" OnPageIndexChanged="DataGrid1_PageIndexChanged"
                    OnSortCommand="DataGrid1_SortCommand" PageSize="20" Width="100%">
                    <PagerStyle NextPageText="Next" PrevPageText="Prev" />
                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" HorizontalAlign="Center" />
                    <HeaderStyle BackColor="#282C5F" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" ForeColor="#ffffff" />
                    <Columns>
                        <asp:BoundColumn DataField="task_id" HeaderText="Task id" SortExpression="task_id"
                            Visible="False"></asp:BoundColumn>
                        <asp:BoundColumn DataField="username" HeaderText="Username">
                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                Font-Underline="False" HorizontalAlign="Left" />
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="name" HeaderText="Name"></asp:BoundColumn>
                        <asp:BoundColumn DataField="email" HeaderText="Email"></asp:BoundColumn>
                        <asp:BoundColumn DataField="admin" HeaderText="Admin"></asp:BoundColumn>
                        <asp:BoundColumn DataField="subscribe_to_all_tasks" HeaderText="Subscribe to all tasks">
                        </asp:BoundColumn>
                        <asp:ButtonColumn CommandName="remove" HeaderText="Remove" Text="remove"></asp:ButtonColumn>
                    </Columns>
                </asp:DataGrid>&nbsp;</div>
        </div>
    </div>
    </form>
</body>
</html>
