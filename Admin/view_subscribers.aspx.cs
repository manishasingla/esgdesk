using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Admin_view_subscribers : System.Web.UI.Page
{
    int id;
    string sql = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = (Request.QueryString["id"] == null || Request.QueryString["id"].ToString() == "" ? "edit_task.aspx" : "edit_task.aspx?id=" + Request.QueryString["id"].ToString());
            Response.Redirect("login.aspx");
            return;
        }

        divMessage.InnerHtml = "";
        divMessage1.InnerHtml = "";

        string task_id = Request["id"];
        if (task_id == null || task_id == "0" || task_id == "")
        {
            id = 0;
            // Display an error because the Task id must be an integer
            divMessage.InnerHtml = "<br>Invalid Task ID.";
            divMessage.InnerHtml += "<p><a href='tasks.aspx'>View tasks</a></p>";
            DivEntry.Visible = false;
            return;
        }
        else
        {
            if (DatabaseHelper.is_int(task_id))
            {
                id = Convert.ToInt32(task_id);
                lblTaskId.Text = id.ToString();
            }
            else
            {
                // Display an error because the Task id must be an integer
                divMessage.InnerHtml = "<br>Task ID must be an integer.";
                divMessage.InnerHtml += "<p><a href='tasks.aspx'>View tasks</a></p>";
                DivEntry.Visible = false;
                return;
            }
        }

        if (!Page.IsPostBack)
        {
            load_dropdowns();
            
            if (id == 0)
            {
                
            }
            else
            {
                // Get this entry's data from the db and fill in the datagrid
                BindData();
            }

            //if (!DatabaseHelper.can_Delete_Tasks(Session["admin"].ToString()))
            //{
            //    DataGrid1.Columns[6].Visible = false;
            //}
        }
    }


    public void BindData()
    {
        sql = "select task_subscriptions.task_id, users.username, firstname +' '+ lastname as name, email, admin, subscribe_to_all_tasks ";
        sql += " from users, task_subscriptions ";
        sql += " where users.username = task_subscriptions.username ";
        sql += " and task_subscriptions.task_id ="+ id.ToString();

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            DataGrid1.DataSource = ds.Tables[0];
            DataGrid1.DataBind();
        }
        else
        {
            DataGrid1.DataSource = null;
            DataGrid1.DataBind();
            divMessage1.InnerHtml = "<br> No subscribers for this task. ";
        }
    }

    void load_dropdowns()
    {

        // projects
        sql = @"select username from users where active = 'Y'
                and username not in (select username from task_subscriptions where task_id = "+ id.ToString() +") order by username";
        // do a batch of sql statements
        DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);

        drpUsers.DataSource = ds_dropdowns.Tables[0];
        drpUsers.DataTextField = "username";
        drpUsers.DataValueField = "username";
        drpUsers.DataBind();

        if (ds_dropdowns == null || ds_dropdowns.Tables.Count <= 0 || ds_dropdowns.Tables[0].Rows.Count <= 0 )
        {
            drpUsers.Items.Insert(0, new ListItem("[no users to select]", "0"));
        }
        else
        {
            drpUsers.Items.Insert(0, new ListItem("[Select to add]", "0"));
        }
    }
    protected void DataGrid1_SortCommand(object source, DataGridSortCommandEventArgs e)
    {

    }
    protected void DataGrid1_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {

    }
    protected void DataGrid1_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton removeButton = (LinkButton)e.Item.Cells[6].Controls[0];

            //We can now add the onclick event handler
            removeButton.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to remove this user "+ e.Item.Cells[1].Text +"?')");
        }
    }
    protected void DataGrid1_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "remove")
        {
            sql = "delete from task_subscriptions where task_id =" + e.Item.Cells[0].Text + " and username = '" + e.Item.Cells[1].Text +"'";

            int intResult = DatabaseHelper.executeNonQuery(sql);

            if (intResult != 0)
            {
                Response.Redirect("view_subscribers.aspx?id=" + id.ToString());
            }
            else
            {
                divMessage1.InnerHtml = "User was not removed.";
                
            }
        }
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (drpUsers.SelectedIndex > 0)
        {
            sql = "insert into task_subscriptions ([task_id],[username]) ";
            sql += "values(" + id.ToString() + ",'" + drpUsers.SelectedValue.Replace("'", "''") + "'); ";
            int intResult = DatabaseHelper.executeNonQuery(sql);
            
            if (intResult > 0)
            {
                Response.Redirect("view_subscribers.aspx?id="+ id.ToString());
                
            }
            else
            {
                divMessage1.InnerHtml = "User was not added.";
            }            
        }
        else
        {
            divMessage1.InnerHtml = "Select user to add for subscription"; 
        }
    }
}
