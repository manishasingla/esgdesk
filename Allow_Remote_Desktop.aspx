<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Allow_Remote_Desktop.aspx.cs" Inherits="TestDesktop" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="footer.ascx" TagName="footer" TagPrefix="uc2" %>

<%@ Register Src="Header.ascx" TagName="Header" TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
   <title>Allow remote desktop</title>
    <link href="StyleSheet.css" rel="stylesheet" type="text/css" />
    
</head>
<body>
    <form name="channel355391844" action="https://secure.logmeinrescue.com/Customer/Download.aspx" method="post">
    <div align="center">
    <%-- <uc1:Header ID="Header1" runat="server" />--%>
    <br/>
    <div align="left">
    Please fill in your details and click 'Start remote desktop' to access our secure remote screen sharing service for the very best quality <br />technical support, real-time one on one training and remote presentations.
<br/>
<br/>
    If you havent pre-arranged a specific time for the session let us know you are joining via our chat facility: 'Chat with our support guys' .
<br/>
<br/>
    If you are having any problems with connecting just let us know by telephone or via our chat facility: 'Chat with our support guys' 
<br/>
<br/>
    Once your session begins, you will always have overriding control by movement of your mouse. You will also be able to discontinue the session<br /> at any time by selecting "X" in the upper right-hand corner of session window. Remote desktop connection requires the download of a small applet to your computer.<br /> To proceed with your session, please select "Run" at the prompts. There is a chat facility which you can use once the remote desktop begins.
    </div>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <table>
        <tr><td align="center">Please your name: </td><td><input type="text" runat="server" id="name" name="name" maxlength="64"/></td></tr>
        <tr><td align="center">Company Name: </td><td><input type="text" runat="server" id="comment1" name="comment1" maxlength="512"/></td></tr>
        <%--<tr><td>Telephone No</td><td><input type="text" runat="server" id="comment2" name="comment2" maxlength="512"   /></td></tr>
        <tr><td>Custom Field 3: </td><td><input type="text" runat="server" id="comment3" name="comment3" maxlength="256"   /></td></tr>
        <tr><td>Custom Field 4: </td><td><input type="text" runat="server" id="comment4" name="comment4" maxlength="64"   /></td></tr>
        <tr><td>Custom Field 5: </td><td><input type="text" runat="server" id="comment5" name="comment5" maxlength="64"   /></td></tr>
        <tr><td>Custom Field 5: </td><td><input type="text" runat="server" id="comment6" name="company6" maxlength="64" /></td></tr>--%>
    </table>
    <br/>
    <br/>
    <input type="submit" runat="server"  value="Start remote desktop" />
    <input type="hidden" name="EntryID" value="355391844" />
    <input type="hidden" runat="server" name="tracking0" maxlength="64" /> <!-- optional -->
    <input type="hidden" runat="server" name="language" maxlength="5" /> <!-- optional -->
    <br/>
    <br/>
    <div>
         <%--<input id="Chat" type="submit" runat="server" causesvalidation="false"  value="To chat with support staff" />--%>
         Click <a id="Chat" runat="server" style="color:Blue;" href="#">here</a> to chat with support staff
    </div>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    
   <%-- <uc2:footer ID="Footer1" runat="server" />--%>
     </div>
    </form>
</body>
</html>
