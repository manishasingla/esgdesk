using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Google.GData.Analytics;
using Google.GData.Extensions;


public partial class Analytics : System.Web.UI.Page
{
    Int32 _intVisists = 0;
    Int32 _intPageViewVisit = 0;
    string _intPageKeywords = "";

    static DataTable dt = new DataTable();
    //static DataTable dt1 = new DataTable();
    static string sort = "DESC";
    static string sortpages = "DESC";
    static string sortwebsites = "DESC";
    DataColumn dtcol = new DataColumn("Keyword", typeof(string));
    DataColumn dtkeywordvisit = new DataColumn("Visits", typeof(Int32));
    DataColumn dtpage = new DataColumn("Pages", typeof(string));

    //static DataTable dtpath = new DataTable();
    static DataTable dtpagepath = new DataTable();
    DataColumn dtpath = new DataColumn("Pages", typeof(string));
    DataColumn dtpageview = new DataColumn("Page views", typeof(Int32));

    static DataTable dttraffic = new DataTable();
    DataColumn dtwebsite = new DataColumn("Website", typeof(string));
    DataColumn dtwebsitevisits = new DataColumn("Website visits", typeof(Int32));

    DataQuery query1;
    string str = "";
    AccountQuery query;
    AnalyticsService service;
    string errorMessage = "alert('Invalid range')";
    string company;
    string sql = "";
    string emailSubject = "";
    string userName;
    string passWord;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            userName = "homesandsteeple@estatesolutions.co.uk";///Session["GoogleUser"].ToString();
            passWord = "simonhomes";/// Session["GooglePwd"].ToString();
        }
        catch { }

        const string dataFeedUrl = "https://www.google.com/analytics/feeds/data";


        query = new AccountQuery();
        service = new AnalyticsService("AnalyticsSampleApp");
        service.setUserCredentials(userName, passWord);

        AccountFeed accountFeed = service.Query(query);
        foreach (AccountEntry entry in accountFeed.Entries)
        {
            str = entry.ProfileId.Value;
        }

        query1 = new DataQuery(dataFeedUrl);

        if (!IsPostBack)
        {
            dt.Columns.Clear();
            dt.Rows.Clear();

            dt.Columns.Add(dtcol);
            dt.Columns.Add(dtkeywordvisit);

            dtpagepath.Columns.Clear();
            dt.Rows.Clear();

            dtpagepath.Columns.Add(dtpage);
            dtpagepath.Columns.Add(dtpageview);

            dttraffic.Columns.Clear();
            dttraffic.Rows.Clear();

            dttraffic.Columns.Add(dtwebsite);
            dttraffic.Columns.Add(dtwebsitevisits);
            drpyear.Items.Add(DateTime.Now.ToString("yyyy"));
            drpyear.Items.Add((Convert.ToInt32(DateTime.Now.ToString("yyyy")) - 1).ToString());
            //drpyear.Items.Add(DateTime.Now.ToString("yyyy"));

            drpyear2.Items.Add((Convert.ToInt32(DateTime.Now.ToString("yyyy")) + 1).ToString());
            drpyear2.Items.Add(DateTime.Now.ToString("yyyy"));
            drpyear2.Items.Add((Convert.ToInt32(DateTime.Now.ToString("yyyy")) - 1).ToString());

            if (drpmonth2.SelectedItem.ToString() == "Jan" || drpmonth2.SelectedItem.ToString() == "Mar" || drpmonth2.SelectedItem.ToString() == "May" || drpmonth2.SelectedItem.ToString() == "Jul" || drpmonth2.SelectedItem.ToString() == "Aug" || drpmonth2.SelectedItem.ToString() == "Oct" || drpmonth2.SelectedItem.ToString() == "Dec")
            {
                drpday2.Text = "31";
            }
            else
                if (drpmonth2.SelectedItem.ToString() == "Apr" || drpmonth2.SelectedItem.ToString() == "Jun" || drpmonth2.SelectedItem.ToString() == "Sep" || drpmonth2.SelectedItem.ToString() == "Nov")
                {
                    drpday2.Text = "30";
                }
                else
                    if (drpmonth2.SelectedItem.ToString() == "Feb")
                    {
                        bool isLeapYear = DateTime.IsLeapYear(Convert.ToInt32(drpyear.SelectedItem.ToString()));
                        if (isLeapYear)
                        {
                            drpday2.Text = "29";
                        }
                        else
                        {
                            drpday2.Text = "28";
                        }
                    }
            //dtpath.co
            //dt.Columns.Add(dtpage);
            //drpday.Text = DateTime.Now.AddDays(-1).ToString("dd");
            drpmonth.Text = DateTime.Now.ToString("MM");
           
            drpmonth2.Text = DateTime.Now.ToString("MM");
            //drpmonth2.SelectedIndex = 0;
            drpday.Text = "01";
            drpday2.Text = DateTime.Now.ToString("dd");
            drpyear2.Text = DateTime.Now.ToString("yyyy");
            RefreshFeed();

          
            //loadDynamicGrid();

        }

    }

    private void RefreshFeed()
    {

        //if (drpmonth2.Text == drpmonth.Text && drpyear.Text == drpyear2.Text)
        //{
        //    datevalidate();
        //}

        // total visits
        try
        {
            query1.Ids = str;
            query1.Metrics = "ga:visits";//visitors
            query1.Sort = "ga:visits";

            query1.GAStartDate = drpyear.SelectedValue + "-" + drpmonth.SelectedValue + "-" + drpday.SelectedValue;
            query1.GAEndDate = drpyear2.SelectedValue + "-" + drpmonth2.SelectedValue + "-" + drpday2.SelectedValue;
            lblmessage.Text = "Displaying report from " + drpday.SelectedValue + " " + drpmonth.SelectedItem + " " + drpyear.SelectedValue + " to  " + drpday2.SelectedValue + " " + drpmonth2.SelectedItem + "  " + drpyear2.SelectedValue;

            query1.StartIndex = 1;
            DataFeed dataFeedVisits = service.Query(query1);
            foreach (DataEntry entry in dataFeedVisits.Entries)
            {
                //string st = entry.Title.Text;
                //string ss = entry.Metrics[0].Value;
                _intVisists = Int32.Parse(entry.Metrics[0].Value);
                Label1.Text = Int32.Parse(entry.Metrics[0].Value) + " Visits";

            }
            if (dataFeedVisits.Entries.Count == 0)
            {
                Label1.Text = "0" + " Visits";
            }


            // total pageviews of site
            query1.Ids = str;
            query1.Metrics = "ga:pageviews";//visitors
            query1.Sort = "ga:pageviews";

            query1.GAStartDate = drpyear.SelectedValue + "-" + drpmonth.SelectedValue + "-" + drpday.SelectedValue;
            query1.GAEndDate = drpyear2.SelectedValue + "-" + drpmonth2.SelectedValue + "-" + drpday2.SelectedValue;

            DataFeed dataFeedpageviews = service.Query(query1);
            foreach (DataEntry entry in dataFeedpageviews.Entries)
            {
                //string st = entry.Title.Text;
                //string ss = entry.Metrics[0].Value;
                _intPageViewVisit = Int32.Parse(entry.Metrics[0].Value);
                lblPageViews.Text = entry.Metrics[0].Value + " Page views";

            }

            if (dataFeedpageviews.Entries.Count == 0)
            {
                lblPageViews.Text = "0" + " Page views";
            }
            //// total New visits (count + %)
            query1.Ids = str;
            query1.Metrics = "ga:newVisits";//visitors
            query1.Sort = "ga:newVisits";

            query1.GAStartDate = drpyear.SelectedValue + "-" + drpmonth.SelectedValue + "-" + drpday.SelectedValue;
            query1.GAEndDate = drpyear2.SelectedValue + "-" + drpmonth2.SelectedValue + "-" + drpday2.SelectedValue;

            query1.StartIndex = 1;

            DataFeed dataFeedNwVisits = service.Query(query1);
            foreach (DataEntry entry in dataFeedNwVisits.Entries)
            {
                //string st = entry.Title.Text;
                //string ss = entry.Metrics[0].Value;
                double _dblPer1 = double.Parse(entry.Metrics[0].Value) * 100;
                double _dblPer2 = Math.Round(_dblPer1 / _intVisists, 2);

                double _avgpagevisit = double.Parse(_intPageViewVisit.ToString()) / _intVisists;
                lblpagespervisit.Text = Math.Round(_avgpagevisit, 2) + " Pages/visit";

            }
            if (dataFeedNwVisits.Entries.Count == 0)
            {
                lblpagespervisit.Text = "0" + " Pages/visit";
            }
            //keywords           

            keywordDisplay();

            pagenames();
            trafficsourcesdisplay();


        }
        catch (Exception ex)
        {

        }
    }
    protected void pagenames()
    {
        try
        {
            query1.Ids = str;
            query1.Metrics = "ga:pageviews";//visitors
            query1.Dimensions = "ga:pagePath";
            query1.Sort = "ga:pageviews";
            query1.GAStartDate = drpyear.SelectedValue + "-" + drpmonth.SelectedValue + "-" + drpday.SelectedValue;
            query1.GAEndDate = drpyear2.SelectedValue + "-" + drpmonth2.SelectedValue + "-" + drpday2.SelectedValue;

            DataFeed dataFeedpagepaths = service.Query(query1);
            dtpagepath.Rows.Clear();
            foreach (DataEntry entry in dataFeedpagepaths.Entries)
            {
                //string st = entry.Title.Text;
                //string ss = entry.Metrics[0].Value;
                //string ss1 = entry.Dimensions[0].Value;

                DataRow drow = dtpagepath.NewRow();
                drow["Pages"] = entry.Dimensions[0].Value;
                drow["Page views"] = Int32.Parse(entry.Metrics[0].Value);
                dtpagepath.Rows.Add(drow);
                _intPageViewVisit = Int32.Parse(entry.Metrics[0].Value);
                _intPageKeywords = entry.Dimensions[0].Value;

            }
            foreach (DataColumn col in dtpagepath.Columns)
            {
                BoundField bfield = new BoundField();
                //Initalize the DataField value.
                bfield.DataField = col.ColumnName;
                //Initialize the HeaderText field value.
                bfield.HeaderText = col.ColumnName;
                bfield.SortExpression = col.ColumnName;
                //bfield.ItemStyle - TextAlign = center;
                //bfield.ItemStyle - HorizontalAlign = "Right";
                //Add the newly created bound field to the GridView.
                //gdv .Columns.Add(bfield);
                gdvpages.Columns.Add(bfield);

            }
            //dt1 = dt;
            DataView dtviewpages = new DataView(dtpagepath);
            dtviewpages.Sort = "Page views DESC";
            gdvpages.DataSource = dtviewpages;
            gdvpages.DataBind();
            if (dataFeedpagepaths.Entries.Count == 0)
            {
                tblpages.Visible = true;
            }
            else
            {
                tblpages.Visible = false;
            }
            //tblpages
            //GrdDynamic.DataSource = dt;
            //GrdDynamic.DataBind();
        }
        catch (Exception ex)
        { }
    }
    private string DisplayPercentage(double ratio)
    {
        string _str = "";
        _str = string.Format("{0:0.0%}", ratio);
        return _str;
    }

    private string ReplaceSourceString(string str)
    {
        string _strRety = "";
        _strRety = str.Replace("ga:source=", "");
        return _strRety.Replace("ga:medium=", "");
    }

    private string ReplaceCountryString(string str)
    {
        string _strRety = "";
        _strRety = str.Replace("ga:country=", "");
        return _strRety;
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        //for(int i=0;i<GrdDynamic.Columns.Count;i++)
        //{
        GrdDynamic.Columns.Clear();
        gdvpages.Columns.Clear();
        Grdtraffic.Columns.Clear();
        dt.Rows.Clear();
        dtpagepath.Rows.Clear();
        //dttraffic.Columns.Clear();
        dttraffic.Rows.Clear();

        GrdDynamic.AllowPaging = true;
        Grdtraffic.AllowPaging = true;
        gdvpages.AllowPaging = true;

        //GrdDynamic.Rows.c
        //}
        //GrdDynamic.Columns.Clear();
        //GrdDynamic.Rows.ToString().c
        RefreshFeed();
    }

    protected void GrdDynamic_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GrdDynamic.PageIndex = e.NewPageIndex;
        DataView dtview = new DataView(dt);
        if (sort == "ASC")
        {
            dtview.Sort = "Visits" + " " + sort;
        }
        else
        {
            dtview.Sort = "Visits" + " " + sort;
        }
        //dtview.Sort = "Visits DESC";
        GrdDynamic.DataSource = dtview;
        //Bind the datatable with the GridView.
        GrdDynamic.DataBind();
        //dt.Columns.Remove("Keyword");
        //RefreshFeed();

    }
    protected void GrdDynamic_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            GrdDynamic.DataSource = dt;
            DataTable dataTable = GrdDynamic.DataSource as DataTable;
            if (dataTable != null)
            {
                DataView dataView = new DataView(dataTable);
                if (sort == "ASC")
                {
                    dataView.Sort = e.SortExpression + " " + sort;
                    sort = "DESC";
                }
                else
                {
                    sort = "DESC";
                    dataView.Sort = e.SortExpression + " " + sort;
                    sort = "ASC";
                }
                //if (e.SortDirection == SortDirection.Ascending)
                //{
                //    sortorder = "DESC";
                //    dataView.Sort = e.SortExpression + " " + sortorder;
                //    //e.SortDirection = SortDirection.Descending;

                //}
                //else
                //{
                //    sortorder = "ASC";
                //    dataView.Sort = e.SortExpression + " " + sortorder;
                //    ///e.SortDirection = SortDirection.Ascending;
                //}
                //dataView.Sort = e.SortExpression + (string)((e.SortDirection == SortDirection.Ascending) ? " ASC" : " DESC");
                //dataView.Sort = e.SortExpression + " " + ConvertSortDirectionToSql(e.SortDirection);
                //dataView.Sort = e.SortExpression + " " + (e.SortDirection.ToString());
                //dataView.Sort = e.SortExpression + " " + e.SortDirection.ToString();
                GrdDynamic.DataSource = dataView;
                GrdDynamic.DataBind();
            }
        }
        catch (Exception ex)
        { }

    }
    protected void keywordDisplay()
    {
        try
        {
            query1.Ids = str;
            query1.Metrics = "ga:visits";//visitors
            query1.Dimensions = "ga:keyword";
            query1.Sort = "ga:visits";
            query1.GAStartDate = drpyear.SelectedValue + "-" + drpmonth.SelectedValue + "-" + drpday.SelectedValue;
            query1.GAEndDate = drpyear2.SelectedValue + "-" + drpmonth2.SelectedValue + "-" + drpday2.SelectedValue;
            //string a = "Keyword";
            DataFeed dataFeedkeywords = service.Query(query1);
            dt.Rows.Clear();
            foreach (DataEntry entry in dataFeedkeywords.Entries)
            {
                DataRow drow = dt.NewRow();
                //string st = entry.Title.Text;
                //string ss = entry.Metrics[0].Value;               
                //_intPageViewVisit = Int32.Parse(ss);                
                //GrdDynamic.Rows[0] = Convert.ToString(_intPageKeywords);
                drow["keyword"] = Convert.ToString(entry.Dimensions[0].Value);
                drow["Visits"] = Convert.ToString(Int32.Parse(entry.Metrics[0].Value));
                //drow["Visits"] = Convert.ToString(_intPageViewVisit);
                //for (int i = 0; i < dataFeedkeywords.Entries.Count; i++)
                //{
                dt.Rows.Add(drow);
                //}

                //string aa= entry.
                //lblPageViews.Text = ss + " PageViews";
                //dt.Columns["Keyword"
                //Response.Write("<br/>");
                //Response.Write("keyword : " + ss1);
                //Response.Write(" " + "Visits : " + ss);
                //Response.Write("<br/>");
            }
            foreach (DataColumn col in dt.Columns)
            {
                BoundField bfield = new BoundField();
                //Initalize the DataField value.
                bfield.DataField = col.ColumnName;
                //Initialize the HeaderText field value.
                bfield.HeaderText = col.ColumnName;
                bfield.SortExpression = col.ColumnName;
                //Add the newly created bound field to the GridView.
                GrdDynamic.Columns.Add(bfield);
                //GrdDynamic.ItemStyle
                //ItemStyle-HorizontalAlign="Right"

            }
            //dt1 = dt;

            DataView dtview = new DataView(dt);
            dtview.Sort = "Visits DESC";
            GrdDynamic.DataSource = dtview;
            GrdDynamic.DataBind();
            //TdNoGridProp
            if (dataFeedkeywords.Entries.Count == 0)
            {
                TdNoGridProp.Visible = true;
            }
            else
            {
                TdNoGridProp.Visible = false;
            }
        }
        catch (Exception ex)
        { }
    }

    protected void gdvpages_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdvpages.PageIndex = e.NewPageIndex;
        DataView dtviewpages = new DataView(dtpagepath);
        if (sortpages == "ASC")
        {
            dtviewpages.Sort = "Page views" + " " + sortpages;
        }
        else
        {
            dtviewpages.Sort = "Page views" + " " + sortpages;
        }

        //dtviewpages.Sort = "Page views DESC";
        gdvpages.DataSource = dtviewpages;
        //Bind the datatable with the GridView.
        gdvpages.DataBind();
    }

    protected void gdvpages_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            gdvpages.DataSource = dtpagepath;
            DataTable dataTable = gdvpages.DataSource as DataTable;
            string sortorder = "";
            if (dataTable != null)
            {
                DataView dataView1 = new DataView(dataTable);
                if (sortpages == "ASC")
                {
                    dataView1.Sort = e.SortExpression + " " + sortpages;
                    sortpages = "DESC";
                }
                else
                {
                    //sort = "DESC";
                    dataView1.Sort = e.SortExpression + " " + sortpages;
                    sortpages = "ASC";
                }

                gdvpages.DataSource = dataView1;
                gdvpages.DataBind();
            }
        }
        catch (Exception ex)
        { }
    }
    protected void trafficsourcesdisplay()
    {
        query1.Ids = str;
        query1.Metrics = "ga:entrances";//visitors        
        query1.Dimensions = "ga:source,ga:medium";
        query1.Sort = "ga:medium,-ga:entrances";
        query1.GAStartDate = drpyear.SelectedValue + "-" + drpmonth.SelectedValue + "-" + drpday.SelectedValue;
        query1.GAEndDate = drpyear2.SelectedValue + "-" + drpmonth2.SelectedValue + "-" + drpday2.SelectedValue;
        query1.StartIndex = 1;

        int cntOrg = 0;
        int cntRef = 0;
        int cntDir = 0;
        DataFeed dataFeedTraffic = service.Query(query1);
        dttraffic.Rows.Clear();
        foreach (DataEntry entry in dataFeedTraffic.Entries)
        {
            //string st = entry.Title.Text;
            //string ss = entry.Metrics[0].Value;
            if (entry.Title.Text.ToLower().Contains("organic"))
            {
                cntOrg = cntOrg + int.Parse(entry.Metrics[0].Value);
            }
            else if (entry.Title.Text.ToLower().Contains("referral"))
            {
                cntRef = cntRef + int.Parse(entry.Metrics[0].Value);
            }
            else
            {
                cntDir = cntDir + int.Parse(entry.Metrics[0].Value);
            }
            DataRow drow = dttraffic.NewRow();
            drow["Website"] = entry.Dimensions[0].Value;
            drow["Website visits"] = Int32.Parse(entry.Metrics[0].Value);
            dttraffic.Rows.Add(drow);

        }
        foreach (DataColumn col in dttraffic.Columns)
        {
            BoundField bfield = new BoundField();
            //Initalize the DataField value.
            bfield.DataField = col.ColumnName;
            //Initialize the HeaderText field value.
            bfield.HeaderText = col.ColumnName;
            bfield.SortExpression = col.ColumnName;
            //Add the newly created bound field to the GridView.
            Grdtraffic.Columns.Add(bfield);
            //GrdDynamic.Columns.Add(bfield);

        }
        //dt1 = dt;
        DataView dtviewtraffic = new DataView(dttraffic);
        dtviewtraffic.Sort = "Website visits DESC";
        Grdtraffic.DataSource = dtviewtraffic;
        Grdtraffic.DataBind();
        if (dataFeedTraffic.Entries.Count == 0)
        {
            tblwebsites.Visible = true;
        }
        else
        {
            tblwebsites.Visible = false;
        }
    }
    protected void Grdtraffic_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Grdtraffic.PageIndex = e.NewPageIndex;
        DataView dtviewtraffic = new DataView(dttraffic);
        if (sortwebsites == "ASC")
        {
            dtviewtraffic.Sort = "Website visits" + " " + sortwebsites;
        }
        else
        {
            dtviewtraffic.Sort = "Website visits" + " " + sortwebsites;
        }
        //dtviewtraffic.Sort = "Website visits DESC";
        Grdtraffic.DataSource = dtviewtraffic;
        //Bind the datatable with the GridView.
        Grdtraffic.DataBind();
    }
    protected void Grdtraffic_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            Grdtraffic.DataSource = dttraffic;
            DataTable dataTable = Grdtraffic.DataSource as DataTable;
            string sortorder = "";
            if (dataTable != null)
            {
                DataView dataView1 = new DataView(dataTable);
                if (sortwebsites == "ASC")
                {
                    dataView1.Sort = e.SortExpression + " " + sortwebsites;
                    sortwebsites = "DESC";
                }
                else
                {
                    //sort = "DESC";
                    dataView1.Sort = e.SortExpression + " " + sortwebsites;
                    sortwebsites = "ASC";
                }

                Grdtraffic.DataSource = dataView1;
                Grdtraffic.DataBind();

            }
        }
        catch (Exception ex)
        { }
    }

    protected void drpmonth2_SelectedIndexChanged(object sender, EventArgs e)
    {
        //string sReturn="Invalid Date";
        //string errorMessage = "alert('Invalid range')";
        if (Convert.ToInt32(drpmonth.SelectedValue) > Convert.ToInt32(drpmonth2.SelectedValue) && drpyear.SelectedValue == drpyear2.SelectedValue)
        {
            drpmonth2.Text = DateTime.Now.ToString("MM");
            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), " ", errorMessage, true);
        }
        //if (drpmonth2.Text == drpmonth.Text && drpyear.Text == drpyear2.Text)
        //{
        if (drpday2.Text == "31" || drpday2.Text == "30")
        {
            datevalidate();
        }
        if (drpmonth2.Text == "Feb" && drpday2.Text == "29")
        {
            datevalidate();
        }


    }
    protected void drpyear2_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToInt32(drpmonth.SelectedValue) > Convert.ToInt32(drpmonth2.SelectedValue) && drpyear.SelectedValue == drpyear2.SelectedValue)
        {
            drpmonth2.Text = DateTime.Now.ToString("MM");
            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), " ", errorMessage, true);
        }
        //if (drpmonth2.Text == drpmonth.Text && drpyear.Text == drpyear2.Text)
        //{
        if (drpday2.Text == "31" || drpday2.Text == "30")
        {
            datevalidate();
        }
        if (drpmonth2.Text == "Feb" && drpday2.Text == "29")
        {
            datevalidate();
        }
        //}
        //else
        //{
        //    drpday2.Text = "01";
        //}

    }
    protected void datevalidate()
    {
        if (drpmonth2.SelectedItem.ToString() == "Jan" || drpmonth2.SelectedItem.ToString() == "Mar" || drpmonth2.SelectedItem.ToString() == "May" || drpmonth2.SelectedItem.ToString() == "Jul" || drpmonth2.SelectedItem.ToString() == "Aug" || drpmonth2.SelectedItem.ToString() == "Oct" || drpmonth2.SelectedItem.ToString() == "Dec")
        {
            drpday2.Text = "31";
        }
        else
            if (drpmonth2.SelectedItem.ToString() == "Apr" || drpmonth2.SelectedItem.ToString() == "Jun" || drpmonth2.SelectedItem.ToString() == "Sep" || drpmonth2.SelectedItem.ToString() == "Nov")
            {
                drpday2.Text = "30";
            }
            else
                if (drpmonth2.SelectedItem.ToString() == "Feb")
                {
                    bool isLeapYear = DateTime.IsLeapYear(Convert.ToInt32(drpyear2.SelectedItem.ToString()));
                    if (isLeapYear)
                    {
                        drpday2.Text = "29";
                    }
                    else
                    {
                        drpday2.Text = "28";
                    }
                }

    }
    protected void date()
    {
        if (drpmonth.SelectedItem.ToString() == "Jan" || drpmonth.SelectedItem.ToString() == "Mar" || drpmonth.SelectedItem.ToString() == "May" || drpmonth.SelectedItem.ToString() == "Jul" || drpmonth.SelectedItem.ToString() == "Aug" || drpmonth.SelectedItem.ToString() == "Oct" || drpmonth.SelectedItem.ToString() == "Dec")
        {
            drpday.Text = "31";
        }
        else
            if (drpmonth.SelectedItem.ToString() == "Apr" || drpmonth.SelectedItem.ToString() == "Jun" || drpmonth.SelectedItem.ToString() == "Sep" || drpmonth.SelectedItem.ToString() == "Nov")
            {
                drpday.Text = "30";
            }
            else
                if (drpmonth.SelectedItem.ToString() == "Feb")
                {
                    bool isLeapYear = DateTime.IsLeapYear(Convert.ToInt32(drpyear.SelectedItem.ToString()));
                    if (isLeapYear)
                    {
                        drpday.Text = "29";
                    }
                    else
                    {
                        drpday.Text = "28";
                    }
                }
    }
    protected void drpyear_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToInt32(drpmonth.SelectedValue) > Convert.ToInt32(drpmonth2.SelectedValue) && drpyear.SelectedValue == drpyear2.SelectedValue)
        {
            drpmonth2.Text = DateTime.Now.ToString("MM");
            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), " ", errorMessage, true);
        }
        //if (drpmonth2.Text == drpmonth.Text && drpyear.Text == drpyear2.Text)
        //{
        if (drpday.Text == "31" || drpday.Text == "30")
        {
            date();
        }
        if (drpmonth.Text == "Feb" && drpday.Text == "29")
        {
            date();
        }
        //}
        //else
        //{
        //    drpday2.Text = "01";
        //}
    }
    protected void drpmonth_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToInt32(drpmonth.SelectedValue) > Convert.ToInt32(drpmonth2.SelectedValue) && drpyear.SelectedValue == drpyear2.SelectedValue)
        {
            drpmonth2.Text = DateTime.Now.ToString("MM");
            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), " ", errorMessage, true);
        }
        //if (drpmonth2.Text == drpmonth.Text && drpyear.Text == drpyear2.Text)
        //{
        if (drpday.Text == "31" || drpday.Text == "30")
        {
            date();
        }
        if (drpmonth.Text == "Feb" && drpday.Text == "29")
        {
            date();
        }
        //}
        //else
        //{
        //drpday2.Text = "01";
        //}
    }
    protected void btnfirst30_Click(object sender, EventArgs e)
    {
        try
        {
            GrdDynamic.Columns.Clear();
            gdvpages.Columns.Clear();
            Grdtraffic.Columns.Clear();
            dt.Rows.Clear();
            dtpagepath.Rows.Clear();
            //dttraffic.Columns.Clear();
            dttraffic.Rows.Clear();

            GrdDynamic.AllowPaging = true;
            Grdtraffic.AllowPaging = true;
            gdvpages.AllowPaging = true;

          

            drpyear.SelectedValue = "2010";
            drpmonth.SelectedValue = "12";
            drpday.SelectedValue = "01";
            string date = drpyear.SelectedValue + "-" + drpmonth.SelectedValue + "-" + drpday.SelectedValue;

            drpyear2.SelectedValue = Convert.ToDateTime(date).AddDays(30).ToString("yyyy");
            drpmonth2.SelectedValue = Convert.ToDateTime(date).AddDays(30).ToString("MM");
            drpday2.SelectedValue = Convert.ToDateTime(date).AddDays(30).ToString("dd");

           
            RefreshFeed();
        }
        catch (Exception ex) { }

    }

    protected void btnthis30_Click(object sender, EventArgs e)
    {
        try
        {
            GrdDynamic.Columns.Clear();
            gdvpages.Columns.Clear();
            Grdtraffic.Columns.Clear();
            dt.Rows.Clear();
            dtpagepath.Rows.Clear();
            //dttraffic.Columns.Clear();
            dttraffic.Rows.Clear();

            GrdDynamic.AllowPaging = true;
            Grdtraffic.AllowPaging = true;
            gdvpages.AllowPaging = true;

            //string date = DateTime.Now.AddDays(-30).ToString("yyyy-MM-dd");
            //string[] datearray = date.Split('-');
            drpyear.SelectedValue = DateTime.Now.AddDays(-30).ToString("yyyy");
            drpmonth.SelectedValue = DateTime.Now.AddDays(-30).ToString("MM");
            drpday.SelectedValue = DateTime.Now.AddDays(-30).ToString("dd");

            drpday2.SelectedValue = DateTime.Now.ToString("dd");
            drpmonth2.SelectedValue = DateTime.Now.ToString("MM");
            drpyear2.SelectedValue = DateTime.Now.ToString("yyyy");
            RefreshFeed();
        }
        catch (Exception ex) { }
    }
    protected void btnprevious_Click(object sender, EventArgs e)
    {
        try
        {
            GrdDynamic.Columns.Clear();
            gdvpages.Columns.Clear();
            Grdtraffic.Columns.Clear();
            dt.Rows.Clear();
            dtpagepath.Rows.Clear();
            //dttraffic.Columns.Clear();
            dttraffic.Rows.Clear();

            GrdDynamic.AllowPaging = true;
            Grdtraffic.AllowPaging = true;
            gdvpages.AllowPaging = true;

            drpyear.SelectedValue = DateTime.Now.AddDays(-60).ToString("yyyy");
            drpmonth.SelectedValue = DateTime.Now.AddDays(-60).ToString("MM");
            drpday.SelectedValue = DateTime.Now.AddDays(-60).ToString("dd");

            drpday2.SelectedValue = DateTime.Now.AddDays(-30).ToString("dd");
            drpmonth2.SelectedValue = DateTime.Now.AddDays(-30).ToString("MM");
            drpyear2.SelectedValue = DateTime.Now.AddDays(-30).ToString("yyyy");
            RefreshFeed();
        }
        catch (Exception ex) { }
    }
    protected void drpday_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToInt32(drpmonth.SelectedValue) > Convert.ToInt32(drpmonth2.SelectedValue) && drpyear.SelectedValue == drpyear2.SelectedValue)
        {
            drpmonth2.Text = DateTime.Now.ToString("MM");
            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), " ", errorMessage);
        }
        //if (drpmonth2.Text == drpmonth.Text && drpyear.Text == drpyear2.Text)
        //{
        if (drpday.Text == "31" || drpday.Text == "30")
        {
            date();
        }
        if (drpmonth.Text == "Feb" && drpday.Text == "29")
        {
            date();
        }
        //}
        //else
        //{
        //    drpday2.Text = "01";
        //}
    }
    protected void drpday2_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToInt32(drpmonth.SelectedValue) > Convert.ToInt32(drpmonth2.SelectedValue) && drpyear.SelectedValue == drpyear2.SelectedValue)
        {
            drpmonth2.Text = DateTime.Now.ToString("MM");
            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), " ", errorMessage, true);
        }
        //if (drpmonth2.Text == drpmonth.Text && drpyear.Text == drpyear2.Text)
        //{
        if (drpday2.Text == "31" || drpday2.Text == "30")
        {
            datevalidate();
        }
        if (drpmonth2.Text == "Feb" && drpday2.Text == "29")
        {
            datevalidate();
        }
        //}
    }
    protected void LinkButton5_Click(object sender, EventArgs e)
    {
        try
        {
            GrdDynamic.Columns.Clear();
            gdvpages.Columns.Clear();
            Grdtraffic.Columns.Clear();
            dt.Rows.Clear();
            dtpagepath.Rows.Clear();
            //dttraffic.Columns.Clear();
            dttraffic.Rows.Clear();

            GrdDynamic.AllowPaging = false;
            Grdtraffic.AllowPaging = false;
            gdvpages.AllowPaging = false;
         
            RefreshFeed();
        }
        catch (Exception ex) { }
    }

    
}
