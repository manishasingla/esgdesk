using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Collections.Generic;

/// <summary>
/// Summary description for clsLeave_Logic
/// </summary>
public class clsLeave_Logic
{
    private string _LeaveTypeName,_UserName,_StartingMonth,_EndingMonth,_StartingDate,_EndingDate,_informedon; //changed by rutuja

    int _LeaveTypeId, _NoOfLeavesPerYear, _ApplicationNo, _AppStatusId, _IsHpl, _Year;
    double _BalanceLeaves ,_UsedLeaves;
    double _CasualBalanceLeaves, _EarnBalanceLeaves, _MedicalBalanceLeaves, _Planned,_noofwhf,_compoffs,_shortleaves;
    double _HalfPaidBalanceLeaves,_ExtraBalanceLeaves,_TotalBalance,_AllotedLeaves;
    static string _strErrorSubject;
   
    public string Sort_On;
    int intNo_Of_Days = 0;
    private DateTime _dtAbsentDate;
    string strReason = "";

    public double CL, EL, ML, HPL;

    public clsLeave_Logic()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    public string StartingMonth
    {
        get { return _StartingMonth; }
        set { _StartingMonth = value; }
    }
    public string EndingMonth
    {
        get { return _EndingMonth; }
        set { _EndingMonth = value; }
    }
    public string StartingDate
    {
        get { return _StartingDate; }
        set { _StartingDate = value; }
    }
    public string EndingDate
    {
        get { return _EndingDate; }
        set { _EndingDate = value; }
    }

    public int ApplicationNo
    {
        get { return _ApplicationNo; }
        set { _ApplicationNo = value; }
    }
    public int AppStatusId
    {
        get { return _AppStatusId; }
        set { _AppStatusId = value; }
    }
    public double CasualBalanceLeaves
    {
        get { return _CasualBalanceLeaves; }
        set { _CasualBalanceLeaves = value; }
    }
    public double EarnBalanceLeaves
    {
        get { return _EarnBalanceLeaves; }
        set { _EarnBalanceLeaves = value; }
    }
    public double HalfPaidBalanceLeaves
    {
        get { return _HalfPaidBalanceLeaves; }
        set { _HalfPaidBalanceLeaves = value; }
    }
    public double MedicalBalanceLeaves
    {
        get { return _MedicalBalanceLeaves; }
        set { _MedicalBalanceLeaves = value; }
    }
    public double ExtraBalanceLeaves
    {
        get { return _ExtraBalanceLeaves; }
        set { _ExtraBalanceLeaves = value; }
    }
    public double Planned
    {
        get { return _Planned; }
        set { _Planned = value; }
    }
    public double TotalBalance
    {
        get { return _TotalBalance; }
        set { _TotalBalance = value; }
    }

    public string UserName
    {
        get { return _UserName; }
        set { _UserName = value; }
    }
    public string LeaveTypeName
    {
        get { return _LeaveTypeName; }
        set { _LeaveTypeName = value; }
    }
    public int LeaveTypeId
    {
        get { return _LeaveTypeId; }
        set { _LeaveTypeId = value; }
    }

    public int NoOfLeavesPerYear
    {
        get { return _NoOfLeavesPerYear; }
        set { _NoOfLeavesPerYear = value; }
    }
    public double UsedLeaves
    {
        get { return _UsedLeaves; }
        set { _UsedLeaves = value; }
    }
    public double BalanceLeaves
    {
        get { return _BalanceLeaves; }
        set { _BalanceLeaves = value; }
    }

    public DateTime AbsentDate
    {
        get { return _dtAbsentDate; }
        set { _dtAbsentDate = value; }
    }

    public int IsHpl
    {
        get { return _IsHpl; }
        set { _IsHpl = value; }
    }

    public string strErrorSubject
    {
        get { return _strErrorSubject; }
         set{_strErrorSubject=value;}
}
    public double AllotedLeaves
    {
        get { return _AllotedLeaves; }
        set { _AllotedLeaves = value; }
    }


    public int Year
    {
        get { return _Year; }
        set { _Year = value; }
    }

    //---------------added by rutuja----------
    public string InformedOn
    {
        get { return _informedon; }
        set { _informedon = value; }
    }
    public double NoOfWHF
    {
        get { return _noofwhf; }
        set { _noofwhf = value; }
    }
    public double CompOffs
    {
        get { return _compoffs; }
        set { _compoffs = value; }
    }
    public double ShortLeaves
    {
        get { return _shortleaves; }
        set { _shortleaves = value; }
    }
    public double GetBalanceDays(int lpWYear)
    {
        double intBalnc = 0;
        try
        {
            SqlParameter[] p = new SqlParameter[5];
            p[0] = new SqlParameter("@LeaveTypeId", SqlDbType.Int);
            p[0].Value = _LeaveTypeId;
            p[1] = new SqlParameter("@UserName", SqlDbType.VarChar);
            p[1].Value = _UserName;
            p[2] = new SqlParameter("@Balanceleaves", SqlDbType.Float);
            p[2].Direction = ParameterDirection.Output;
            p[3] = new SqlParameter("@WYear", SqlDbType.Int);
            p[3].Value = lpWYear;
            p[4] = new SqlParameter("@TotalBalanceleaves", SqlDbType.Float);
            p[4].Direction = ParameterDirection.Output;

            SqlHelper.ExecuteDataset(ClsConnectionString.getConnectionString(), CommandType.StoredProcedure, "spGetBalanceDays", p);
            //return i;
            if (p[2].Value != null && p[2].Value.ToString() != "")
                _TotalBalance  = (double)p[2].Value;
            if (p[4].Value != null && p[4].Value.ToString() != "")
            {
                if (_LeaveTypeId == 1)
                    _CasualBalanceLeaves = (double)p[4].Value;
                else if (_LeaveTypeId == 2)
                    _MedicalBalanceLeaves = (double)p[4].Value;
                else if (_LeaveTypeId == 3)
                    _EarnBalanceLeaves = (double)p[4].Value;
                else if (_LeaveTypeId == 4)
                    _HalfPaidBalanceLeaves = (double)p[4].Value;
            }
        }
        catch
        {
        }
        return intBalnc;
    }
    public DataSet GetLeaveType()
    {
        return SqlHelper.ExecuteDataset(ClsConnectionString.getConnectionString(), CommandType.StoredProcedure, "spGetLeaveType");
    }

    //Jan 04 11.09am
    public void GetTotalBalanceDays(int lpWYear)
    {
        try
        {
            SqlParameter[] p = new SqlParameter[3];
            p[0] = new SqlParameter("@UserName", SqlDbType.VarChar);
            p[0].Value = _UserName;
            p[1] = new SqlParameter("@CasualBalanceLeaves", SqlDbType.Float);
            p[1].Direction = ParameterDirection.Output;
            p[2] = new SqlParameter("@MedicalBalanceLeaves", SqlDbType.Float);
            p[2].Direction = ParameterDirection.Output;
            //p[3] = new SqlParameter("@EarnBalanceLeaves", SqlDbType.Float);
            //p[3].Direction = ParameterDirection.Output;
            //p[4] = new SqlParameter("@HalfPaidBalanceLeaves", SqlDbType.Float);
            //p[4].Direction = ParameterDirection.Output;
            p[3] = new SqlParameter("@WYear", SqlDbType.Int);
            p[3].Value = lpWYear;


            SqlHelper.ExecuteDataset(ClsConnectionString.getConnectionString(), CommandType.StoredProcedure, "spGetTotalBalanceDays", p);
            if (p[1].Value != null && p[1].Value.ToString() != "")
                _CasualBalanceLeaves = (double)p[1].Value;
            if (p[2].Value != null && p[2].Value.ToString() != "")
                _MedicalBalanceLeaves = (double)p[2].Value;
            //if (p[3].Value != null && p[3].Value.ToString() != "")
            //    _EarnBalanceLeaves = (double)p[3].Value;
            //if (p[4].Value != null && p[4].Value.ToString() != "")
            //    _HalfPaidBalanceLeaves = (double)p[4].Value;
        }
        catch{
            }
        
    }

    public int AddLeaveDetails(int lpYearOfLeave, DateTime lpAbsentDate, int lpLeaveTypeId, int IsEHPL)
    {
        int i = 0;
        try
        {
            SqlParameter[] p = new SqlParameter[8];
            p[0] = new SqlParameter("@UserName", SqlDbType.VarChar);
            p[0].Value = _UserName;
            p[1] = new SqlParameter("@ApplicationNo", SqlDbType.Int);
            p[1].Value = _ApplicationNo;
            p[2] = new SqlParameter("@AppStatusId", SqlDbType.Int);
            p[2].Value = _AppStatusId;
            p[3] = new SqlParameter("@BalanceLeaves", SqlDbType.Float);
            p[3].Value = _TotalBalance;
            p[4] = new SqlParameter("@LeaveTypeId", SqlDbType.Int);
            //p[4].Value = _LeaveTypeId;
            p[4].Value = lpLeaveTypeId;
            p[5] = new SqlParameter("@WYear", SqlDbType.Int);
            p[5].Value = lpYearOfLeave;
            p[6] = new SqlParameter("@AbsentDate", SqlDbType.DateTime);
            p[6].Value = lpAbsentDate;
            p[7] = new SqlParameter("@IsEHPL", SqlDbType.Int);
            p[7].Value = IsEHPL;
           
         i = SqlHelper.ExecuteNonQuery(ClsConnectionString.getConnectionString(), CommandType.StoredProcedure, "spAddLeaveDetails", p);
        }
        catch
        {
        }

        return i;
    }

    /* added by rutuja - 10 May 2016*/
    public int AddLeaveDetails(int lpYearOfLeave, DateTime lpAbsentDate, int lpLeaveTypeId, int IsEHPL,DateTime informedon)
    {
        int i = 0;
        try
        {
            SqlParameter[] p = new SqlParameter[9];
            p[0] = new SqlParameter("@UserName", SqlDbType.VarChar);
            p[0].Value = _UserName;
            p[1] = new SqlParameter("@ApplicationNo", SqlDbType.Int);
            p[1].Value = _ApplicationNo;
            p[2] = new SqlParameter("@AppStatusId", SqlDbType.Int);
            p[2].Value = _AppStatusId;
            p[3] = new SqlParameter("@BalanceLeaves", SqlDbType.Float);
            p[3].Value = _TotalBalance;
            p[4] = new SqlParameter("@LeaveTypeId", SqlDbType.Int);
            p[4].Value = _LeaveTypeId;
            p[5] = new SqlParameter("@WYear", SqlDbType.Int);
            p[5].Value = lpYearOfLeave;
            p[6] = new SqlParameter("@AbsentDate", SqlDbType.DateTime);
            p[6].Value = lpAbsentDate;
            p[7] = new SqlParameter("@IsEHPL", SqlDbType.Int);
            p[7].Value = IsEHPL;
            p[8] = new SqlParameter("@informedon", SqlDbType.DateTime);
            p[8].Value = informedon;
            i = SqlHelper.ExecuteNonQuery(ClsConnectionString.getConnectionString(), CommandType.StoredProcedure, "spAddLeaveDetails", p);
        }
        catch
        {
        }

        return i;
    }
    public bool CheckAppNoInLeaveDetails()
    {
        SqlParameter[] p = new SqlParameter[1];
        p[0] = new SqlParameter("@ApplicationNo", SqlDbType.Int);
        p[0].Value = _ApplicationNo;
       DataSet ds= SqlHelper.ExecuteDataset(ClsConnectionString.getConnectionString(), CommandType.StoredProcedure, "spCheckAppNoInLeaveDetails", p);
       if (ds.Tables[0].Rows.Count > 0)
           return false;
       else
           return true;
    }

    

    public DataSet GetMonthlyLeavetransactions()
    {
        DataSet dsTemp ;
        try
        {
            string strSql = string.Empty;
            string strSql1 = string.Empty;
            // strSql = "select * from tbl_ApplicationData a,tbl_Status s,tbl_LeaveType l where a.ApplicationStatusId=s.StatusId and a.LeaveTypeId=l.LeaveTypeId  and a.ApplicationStatusId=1";
            if ((_StartingDate != null && _EndingDate != null) && (_StartingDate.Trim() != "" && _EndingDate.Trim() != ""))
            {
                strSql = " select distinct(UserName) from tbl_AbsentDetails where AbsentDate >= '" + _StartingDate + "' and  AbsentDate <= '" + _EndingDate + "'";
            }
            //else
            //{
            //    strSql = " select distinct(UserName) from tbl_AbsentDetails ";
                if (!string.IsNullOrEmpty(_UserName)&& _UserName.Trim()!="")
                {
                    strSql += " and UserName like '%" + _UserName + "%'";

                }

          //  }

            if (Sort_On != "")
            {
                strSql = strSql + " ORDER BY " + Sort_On;

            }

            dsTemp = SqlHelper.ExecuteDataset(ClsConnectionString.getConnectionString(), CommandType.Text, strSql);

            return dsTemp;
        }
        catch(Exception ex)
        {
            return null;
        }
    }

    public DataSet getEmployeeAbsentDetailsByDay(DateTime pStartingDate, DateTime pEndingDate)
    {
        SqlParameter[] p = new SqlParameter[2];
        //p[0] = new SqlParameter("@UserName", SqlDbType.VarChar);
        //p[0].Value = pUserName;
        p[0] = new SqlParameter("@StartingDate", SqlDbType.DateTime);
        p[0].Value = pStartingDate;
        p[1] = new SqlParameter("@EndingDate", SqlDbType.DateTime);
        p[1].Value = pEndingDate;
        DataSet ds = SqlHelper.ExecuteDataset(ClsConnectionString.getConnectionString(), CommandType.StoredProcedure, "spEmployeeAbsentDetailsByDay", p);
        if (ds.Tables[0].Rows.Count > 0)
            return ds;
        else
            return ds;
    }

    public DataSet getEmployeeAbsentDetailsByDayInGroup(DateTime pStartingDate, DateTime pEndingDate)
    {
        try
        {
            SqlParameter[] p = new SqlParameter[2];
            //p[0] = new SqlParameter("@UserName", SqlDbType.VarChar);
            //p[0].Value = pUserName;
            p[0] = new SqlParameter("@StartingDate", SqlDbType.DateTime);
            p[0].Value = pStartingDate;
            p[1] = new SqlParameter("@EndingDate", SqlDbType.DateTime);
            p[1].Value = pEndingDate;
            DataSet ds = SqlHelper.ExecuteDataset(ClsConnectionString.getConnectionString(), CommandType.StoredProcedure, "spEmployeeAbsentDetailsByDayInGroup", p);
            if (ds.Tables[0].Rows.Count > 0)
                return ds;
            else
                return ds;
        }
        catch
        {
            return null;
        }
    }


    //=====================For weekly report========================//

    public DataSet getWeeklyReport(string pStartingDate, string pEndingDate, string DOA,string CompanyName, string user_id,string[] strArrayUser_id, string strProjectId)
    {
        DataSet ds;
        try
        {
            string strSql = "select (t2.firstname+'  '+t2.lastname) as name, t2.username ,convert(varchar, AbsentDate,103) as AbsentDate ,Reason,PaidOrNPaid, IsPlanned from tbl_AbsentDetails t1";
            strSql += " inner join users t2 on t1.UserName=t2.UserName";
            strSql+=" inner join projectdetails pd on pd.user_id=t2.user_id ";
            strSql +=" where AbsentDate >= '" + pStartingDate;
            strSql += "' and AbsentDate<= '" + pEndingDate + "' and pd.CompanyName='" + CompanyName + "'";
        
          if(user_id.Trim()!="")
         {
             strSql+= " and pd.User_id="+ user_id ;
         }

          if(DOA.Trim()!="")
         {
             strSql +=" and t1.AbsentDate>='" + DOA + "' ";
         }

          if (strArrayUser_id!=null&&strArrayUser_id.Length > 1)
          {
              strSql += " and pd.user_id in(";

              for (int i = 0;i< strArrayUser_id.Length; i++)
              {
                  strSql +=  strArrayUser_id[i]+",";
              }
              strSql = strSql.TrimEnd(new char[] { ',' });
              strSql += ")";
          }
          if (strProjectId.Trim() != "")
          {
              strSql += " and pd.project_id=" + strProjectId;
          }
           
            strSql+=" order by AbsentDate";
            //string strSql = "select (t2.firstname+'  '+t2.lastname) as name, t2.username ,convert(varchar, AbsentDate,103) as AbsentDate ,Reason,PaidOrNPaid, IsPlanned from tbl_AbsentDetails t1";
            //strSql+=" inner join users t2 on t1.UserName=t2.UserName where AbsentDate >= '"+pStartingDate;
            //strSql+="' and AbsentDate<= '"+pEndingDate+"' and t2.CompanyName='"+CompanyName+"' and AbsentDate<='"+DOA+"' order by AbsentDate";

          //  select  (t2.firstname+'  '+t2.lastname) as name, convert(varchar,AbsentDate,103) as AbsentDate ,Reason,PaidOrNPaid as LeaveT, LeaveType from tbl_AbsentDetails where UserName='" + strSUserName + "' and AbsentDate >='" + dtSDate + "'"
                           //    + " and AbsentDate <='" + dtEDate + "' order by cast(AbsentDate as DateTime) desc ";
             ds = DatabaseHelper.getDataset(strSql);
           
        }
        catch
        {
            return null;
        }
        return ds;
    }

    //==============================================================//



    public DataSet getEmployeeOnLeaveToday()
    {
        DataSet ds;
        try
        {
            string strSql = "select ad.username,(u.firstname+' '+u.lastname) as EmpName , ad.Reason from tbl_AbsentDetails ad,users u where ad.username=u.username and CONVERT(varchar(10), ad.AbsentDate, 104)= CONVERT(varchar(20), GetDate(), 104)";
            ds = SqlHelper.ExecuteDataset(ClsConnectionString.getConnectionString(), CommandType.Text, strSql);
            return ds;
        }
        catch
        {
            return null;
        }
    }

    public int getNoOfDaysInMonth(string q)
    {
        try
        {
            object obj = null;
            int intNoOfDays = 0;

            obj = SqlHelper.ExecuteScalar(ClsConnectionString.getConnectionString(), CommandType.Text, q);
            if (obj != null && obj.ToString().Trim() != "")
            {
                intNoOfDays = (int)obj;

            }


            if (obj != null && obj.ToString().Trim() != "")
            {
                intNoOfDays = (int)obj;
            }

            return intNoOfDays;
        }
        catch
        {
            return 0;
        }
    }

    //public DataSet GetUser()
    //{
    //    string strSql = string.Empty;
    //    strSql = "select * from tbl_ApplicationData a,tbl_Status s,tbl_LeaveType l where a.ApplicationStatusId=s.StatusId and a.LeaveTypeId=l.LeaveTypeId ";
    //    if (!string.IsNullOrEmpty(_UserName))
    //        strSql += " and a.UserName like '%" + _UserName + "%'";
    //    if (_LeaveTypeId != 0)
    //        strSql += " and a.LeaveTypeId=" + _LeaveTypeId;
    //    if (!string.IsNullOrEmpty(Sort_On))
    //    {
    //        strSql = strSql + " ORDER BY " + Sort_On;
    //    }
    //    else
    //    {
    //        strSql = strSql + " Order By a.UserName";
    //    }
    //    DataSet dsTemp;
    //    dsTemp = SqlHelper.ExecuteDataset(ClsConnectionString.getConnectionString(), CommandType.Text, strSql);
    //    return dsTemp;
    //}
    public bool CheckLeaveDateInAbsentDetails(DateTime pAbsentDate,string pUserName)
    {
        SqlParameter[] p = new SqlParameter[2];
        p[0] = new SqlParameter("@AbsentDate", SqlDbType.DateTime);
        p[0].Value = pAbsentDate;
        p[1] = new SqlParameter("@UserName", SqlDbType.VarChar);
        p[1].Value = pUserName;
        DataSet ds = SqlHelper.ExecuteDataset(ClsConnectionString.getConnectionString(), CommandType.StoredProcedure, "spCheckLeaveDateInAbsentDetails", p);
        if (ds.Tables[0].Rows.Count > 0)
            return true;
        else
            return false;
    }

    public int AddLeaves(DateTime ad, string un,string ponp,string reason, string lt,int IsEHPL,bool blnIsPlanned,bool blnIsDOND)
    {

        int i;
        try
        {
            SqlParameter[] p = new SqlParameter[8];
            //p[0] = new SqlParameter("@EmpId", SqlDbType.Int);
            //p[0].Value = empid;      
            p[0] = new SqlParameter("@AbsentDate", SqlDbType.DateTime);
            p[0].Value = ad;
            p[1] = new SqlParameter("@UserName", SqlDbType.VarChar);
            p[1].Value = un;
            p[2] = new SqlParameter("@Reason", SqlDbType.VarChar);
            p[2].Value = reason;
            p[3] = new SqlParameter("@PaidOrNPaid", SqlDbType.VarChar);
            p[3].Value = ponp;
            p[4] = new SqlParameter("@LeaveType", SqlDbType.VarChar);
            p[4].Value = lt;
            p[5] = new SqlParameter("@IsEHPL", SqlDbType.Int);
            p[5].Value = IsEHPL;
            p[6] = new SqlParameter("@IsPlanned", SqlDbType.Bit);
            p[6].Value = blnIsPlanned;
            p[7] = new SqlParameter("@Deductable", SqlDbType.Bit);
            p[7].Value = blnIsDOND;

             i = SqlHelper.ExecuteNonQuery(ClsConnectionString.getConnectionString(), CommandType.StoredProcedure, "spAddLeaves", p);
            return i;
        }
        catch
        {
            return 0;
        }
    }
    /*------------added by rutuja - 10 May 2016 */
    public int AddLeaves(DateTime ad, string un, string ponp, string reason, string lt, int IsEHPL, bool blnIsPlanned, bool blnIsDOND,DateTime informedon)
    {

        int i;
        try
        {
            SqlParameter[] p = new SqlParameter[9];
            //p[0] = new SqlParameter("@EmpId", SqlDbType.Int);
            //p[0].Value = empid;      
            p[0] = new SqlParameter("@AbsentDate", SqlDbType.DateTime);
            p[0].Value = ad;
            p[1] = new SqlParameter("@UserName", SqlDbType.VarChar);
            p[1].Value = un;
            p[2] = new SqlParameter("@Reason", SqlDbType.VarChar);
            p[2].Value = reason;
            p[3] = new SqlParameter("@PaidOrNPaid", SqlDbType.VarChar);
            p[3].Value = ponp;
            p[4] = new SqlParameter("@LeaveType", SqlDbType.VarChar);
            p[4].Value = lt;
            p[5] = new SqlParameter("@IsEHPL", SqlDbType.Int);
            p[5].Value = IsEHPL;
            p[6] = new SqlParameter("@IsPlanned", SqlDbType.Bit);
            p[6].Value = blnIsPlanned;
            p[7] = new SqlParameter("@Deductable", SqlDbType.Bit);
            p[7].Value = blnIsDOND;
            p[8] = new SqlParameter("@informedon", SqlDbType.DateTime);
            p[8].Value = informedon;
            i = SqlHelper.ExecuteNonQuery(ClsConnectionString.getConnectionString(), CommandType.StoredProcedure, "spAddLeaves", p);
            return i;
        }
        catch
        {
            return 0;
        }
    }
    public void AddLeavesInAbsentTable(int pEmpId ,int pAppNo, string pUserName,int IsEHPL,bool blnIsPlanned,bool blnIsDOND)
    {
        try
        {
            string temp = "Unpaid";// Temprary Use
            DateTime DTSDate = DateTime.Now, DTEDate = DateTime.Now;
            string strSql = "select * from tbl_ApplicationData a,tbl_Status s,tbl_LeaveType l where a.ApplicationStatusId=s.StatusId and a.LeaveTypeId=l.LeaveTypeId  and a.ApplicationStatusId=1 and a.ApplicationNo=" + pAppNo + "and a.UserName='" + pUserName + "'";
            SqlDataReader drSEDate = SqlHelper.ExecuteReader(ClsConnectionString.getConnectionString(), CommandType.Text, strSql);
            while (drSEDate.Read())
            {
                DTSDate = Convert.ToDateTime(drSEDate["StartingDate"]);
                DTEDate = Convert.ToDateTime(drSEDate["EndingDate"]);
                strReason = drSEDate["LeavePurpose"].ToString();

            }

            List<DateTime> dates = new List<DateTime>();

            for (DateTime dt = DTSDate; dt <= DTEDate; dt = dt.AddDays(1))
            {
                dates.Add(dt);

            }
            foreach (DateTime dt in dates)
            {
                // (DateTime ad, string un,string ponp,string reason, string lt)
                AddLeaves(dt, pUserName, temp, strReason, temp, IsEHPL,blnIsPlanned,blnIsDOND);
            }
        }
        catch
        {
        }

    }

    public DataSet getEmployeeAbsentDetails()
    {
        try
        {
            if (_Year == null)
            {
                _Year = Year ;//DateTime.Now.Year;
            }
            //string strSql = "select UserName,convert(varchar, AbsentDate, 103) as AbsentDate,Reason,PaidOrNPaid,LeaveType from tbl_AbsentDetails where UserName='"+_UserName+"'";
            
            //------------added by rutuja------------------
            //string strSql = "select UserName,cast(AbsentDate as Datetime) as AbsentDate,datename(dw,AbsentDate)as day_name,Reason,PaidOrNPaid,LeaveType,IsEHPL,IsPlanned,Deductable,informedon from tbl_AbsentDetails where UserName='" + _UserName + "'" + " and DATEPART(yyyy,AbsentDate)=" + _Year + " order by AbsentDate desc";
            string strSql = "select UserName,cast(AbsentDate as Datetime) as AbsentDate,datename(dw,AbsentDate)as day_name,Reason,PaidOrNPaid,LeaveType,IsEHPL,IsPlanned,Deductable,informedon from tbl_AbsentDetails where UserName='" + _UserName + "'" + " and DATEPART(yyyy,AbsentDate)=" + _Year + " and LeaveType<>'Short leave' order by AbsentDate desc";
            DataSet ds = SqlHelper.ExecuteDataset(ClsConnectionString.getConnectionString(), CommandType.Text, strSql);
            return ds;
        }
        catch   
        {
            return null;
        }
    }

    public int UpdateLeaves(DateTime ad, string un, string ponp, string reason,string lt,int IsHPL, bool blnIsPlanned,bool blnIsDOND)
    {
        try
        {
            SqlParameter[] p = new SqlParameter[8];
            //p[0] = new SqlParameter("@EmpId", SqlDbType.Int);
            //p[0].Value = empid;      
            p[0] = new SqlParameter("@AbsentDate", SqlDbType.DateTime);
            p[0].Value = ad;
            p[1] = new SqlParameter("@UserName", SqlDbType.VarChar);
            p[1].Value = un;
            p[2] = new SqlParameter("@Reason", SqlDbType.VarChar);
            p[2].Value = reason;
            p[3] = new SqlParameter("@PaidOrNPaid", SqlDbType.VarChar);
            p[3].Value = ponp;
            p[4] = new SqlParameter("@LeaveType", SqlDbType.VarChar);
            p[4].Value = lt;
            p[5] = new SqlParameter("@IsEHPL", SqlDbType.Int);
            p[5].Value = IsHPL;
            p[6] = new SqlParameter("@IsPlanned", SqlDbType.Bit);
            p[6].Value = blnIsPlanned;
            p[7] = new SqlParameter("@Deductable", SqlDbType.Bit);
            p[7].Value = blnIsDOND;
            int i = SqlHelper.ExecuteNonQuery(ClsConnectionString.getConnectionString(), CommandType.StoredProcedure, "spUpdateLeaves", p);
            return i;
        }
        catch
        {
            return 0;
        }
    }
    /* added by Rutuja - 10 May 2016* not in use*/
    public int UpdateLeaves(DateTime ad, string un, string ponp, string reason, string lt, int IsHPL, bool blnIsPlanned, bool blnIsDOND, DateTime informedon)
    {
        try
        {
            SqlParameter[] p = new SqlParameter[8];
            //p[0] = new SqlParameter("@EmpId", SqlDbType.Int);
            //p[0].Value = empid;      
            p[0] = new SqlParameter("@AbsentDate", SqlDbType.DateTime);
            p[0].Value = ad;
            p[1] = new SqlParameter("@UserName", SqlDbType.VarChar);
            p[1].Value = un;
            p[2] = new SqlParameter("@Reason", SqlDbType.VarChar);
            p[2].Value = reason;
            p[3] = new SqlParameter("@PaidOrNPaid", SqlDbType.VarChar);
            p[3].Value = ponp;
            p[4] = new SqlParameter("@LeaveType", SqlDbType.VarChar);
            p[4].Value = lt;
            p[5] = new SqlParameter("@IsEHPL", SqlDbType.Int);
            p[5].Value = IsHPL;
            p[6] = new SqlParameter("@IsPlanned", SqlDbType.Bit);
            p[6].Value = blnIsPlanned;
            p[7] = new SqlParameter("@Deductable", SqlDbType.Bit);
            p[7].Value = blnIsDOND;
            p[8]= new SqlParameter("@@informedon", SqlDbType.DateTime);
            p[8].Value = informedon;
            int i = SqlHelper.ExecuteNonQuery(ClsConnectionString.getConnectionString(), CommandType.StoredProcedure, "spUpdateLeaves", p);
            return i;
        }
        catch
        {
            return 0;
        }
    }

    public int DeleteLeaves(DateTime ad, string un, int lid,int IsEHPL)
    {
        int i = 0;
        try
        {
            SqlParameter[] p = new SqlParameter[5];
            //p[0] = new SqlParameter("@EmpId", SqlDbType.Int);
            //p[0].Value = empid;      
            p[0] = new SqlParameter("@AbsentDate", SqlDbType.DateTime);
            p[0].Value = ad;
            p[1] = new SqlParameter("@UserName", SqlDbType.VarChar);
            p[1].Value = un;
            p[2] = new SqlParameter("@LeaveTypeId", SqlDbType.Int);
            p[2].Value = lid;
            p[3] = new SqlParameter("@WYear", SqlDbType.Int);
            p[3].Value = ad.Year;
            p[4] = new SqlParameter("@IsEHPL", SqlDbType.Int);
            p[4].Value = IsEHPL;
            i =  SqlHelper.ExecuteNonQuery(ClsConnectionString.getConnectionString(), CommandType.StoredProcedure, "spDeleteLeaves", p);
            return i;
        }
        catch
        {
            return i;
        }
    }

    public int DeleteHoliday(string strsql)
    {
        int i = SqlHelper.ExecuteNonQuery(ClsConnectionString.getConnectionString(), CommandType.Text, strsql);
        return i;
    }
  
    //public void UpdateLeaveDetails()
    //{
    //     SqlParameter[] p = new SqlParameter[5];
    //    //p[0] = new SqlParameter("@EmpId", SqlDbType.Int);
    //    //p[0].Value = empid;      
    //    p[0] = new SqlParameter("@AbsentDate", SqlDbType.DateTime);
    //    p[0].Value = ad;
    //    p[1] = new SqlParameter("@UserName", SqlDbType.VarChar);
    //    p[1].Value = un;
    //    p[2] = new SqlParameter("@Reason", SqlDbType.VarChar);
    //    p[2].Value = reason;
    //    p[3] = new SqlParameter("@PaidOrNPaid", SqlDbType.VarChar);
    //    p[3].Value = ponp;
    //    p[4] = new SqlParameter("@LeaveType", SqlDbType.VarChar);
    //    p[4].Value = lt;

    //    int i = SqlHelper.ExecuteNonQuery(ClsConnectionString.getConnectionString(), CommandType.StoredProcedure, "spUpdateLeaves", p);
    //    return i;
    
    //}


    public int SetLeaveDetails(int year,int intnoofleave)
    {
        try
        {
            SqlParameter[] p = new SqlParameter[2];
            p[0] = new SqlParameter("@Year", SqlDbType.VarChar);
            p[0].Value = year;
            p[1] = new SqlParameter("@NoOfleave", SqlDbType.Float);
            p[1].Value = intnoofleave;

            int i = SqlHelper.ExecuteNonQuery(ClsConnectionString.getConnectionString(), CommandType.StoredProcedure, "spSetLeaveDetails", p);
            return i;
        }
        catch
        {
            return 0;
        }
    }

    public int SetLeaveDetails4Users(int userid,int year, double noofleave, double EBL, int IsEBLSetting)
    {
        try
        {
            SqlParameter[] p = new SqlParameter[5];
            p[0] = new SqlParameter("@UserId", SqlDbType.Int);
            p[0].Value = userid;
            p[1] = new SqlParameter("@Year", SqlDbType.VarChar);
            p[1].Value = year;
            p[2] = new SqlParameter("@NoOfLeavesPerYear", SqlDbType.Float);
            p[2].Value = noofleave;
            p[3] = new SqlParameter("@EBL", SqlDbType.Float);
            p[3].Value = EBL;
            p[4] = new SqlParameter("@blnIsEBLSetting", SqlDbType.Int);
            p[4].Value = IsEBLSetting;
            int i = SqlHelper.ExecuteNonQuery(ClsConnectionString.getConnectionString(), CommandType.StoredProcedure, "spSetLeaveDetails4AllUsers", p);
            return i;
        }
        catch
        {
            return 0;
        }
    }

 

    public void fnGetLeaveDetails(int lpyear)
    {
        try
        {
            SqlParameter[] p = new SqlParameter[2];
            p[0] = new SqlParameter("@Year", SqlDbType.Int);
            p[0].Value = lpyear;
            p[1] = new SqlParameter("@Total", SqlDbType.Float);
            p[1].Direction = ParameterDirection.Output;


            SqlHelper.ExecuteDataset(ClsConnectionString.getConnectionString(), CommandType.StoredProcedure, "spGetLeaveDetails", p);
            if (p[1].Value != null && p[1].Value.ToString() != "")
                TotalBalance = (double)p[1].Value;
        }
        catch
        {
        }
    
    }

     public string fnGetTotalOfBalanceLeave(int wyear)
    {
        try
        {
            string strTotalOfBL = "";
            SqlParameter[] sp = new SqlParameter[3];
            sp[0] = new SqlParameter("@UserName", SqlDbType.VarChar);
            sp[0].Value = UserName;
            sp[1] = new SqlParameter("@WYear", SqlDbType.Int);
            sp[1].Value = wyear;//@TotalBalance

            sp[2] = new SqlParameter("@TotalBalance", SqlDbType.VarChar, 5);
            sp[2].Direction = ParameterDirection.Output;

            DataSet ds = SqlHelper.ExecuteDataset(ClsConnectionString.getConnectionString(), CommandType.StoredProcedure, "spGetTotalOfLeaveDays", sp);
            strTotalOfBL = sp[2].Value.ToString();
            return strTotalOfBL;
        }
        catch
        {
            return "0";
        }
    }

     public double GetTotalBalanceDaysInOne(int lpWYear) //GetTotalBalanceDaysInOne() RETURNS NO OF LEAVES TAKEN ON EACH CATEGORY FOR AN PARTICULAR 
     {                                                  //EMPLOYEE AND TOTAL REMAING BALANCE
         try
         {
          

             SqlParameter[] p = new SqlParameter[5];
             p[0] = new SqlParameter("@UserName", SqlDbType.VarChar);
             p[0].Value = _UserName;
             p[1] = new SqlParameter("@CasualBalanceLeaves", SqlDbType.Float);
             p[1].Direction = ParameterDirection.Output;
             p[2] = new SqlParameter("@MedicalBalanceLeaves", SqlDbType.Float);
             p[2].Direction = ParameterDirection.Output;

             p[3] = new SqlParameter("@WYear", SqlDbType.Int);
             p[3].Value = lpWYear;
             p[4] = new SqlParameter("@TotalBalance", SqlDbType.Float);
             p[4].Direction = ParameterDirection.Output;

             SqlHelper.ExecuteDataset(ClsConnectionString.getConnectionString(), CommandType.StoredProcedure, "spGetTotalBalanceDays", p);
             if (p[1].Value != null && p[1].Value.ToString() != "")
                 _CasualBalanceLeaves =(double) p[1].Value;
             if (p[2].Value != null && p[2].Value.ToString() != "")
                 _MedicalBalanceLeaves = (double)p[2].Value;
          
             if (p[4].Value != null && p[4].Value.ToString() != "")
                 _TotalBalance= (double)p[4].Value;
            
         }
         catch
         {
         }
         return _TotalBalance;
     }


     public double GetTotalBalanceDaysInOneBWDate(int lpWYear) //GetTotalBalanceDaysInOne() RETURNS NO OF LEAVES TAKEN ON EACH CATEGORY FOR AN PARTICULAR 
     {                                                  //EMPLOYEE AND TOTAL REMAING BALANCE
         try
         {
             //SqlParameter[] p = new SqlParameter[9];
             SqlParameter[] p = new SqlParameter[11];
             p[0] = new SqlParameter("@UserName", SqlDbType.VarChar);
             p[0].Value = _UserName;
             p[1] = new SqlParameter("@CasualBalanceLeaves", SqlDbType.Float);
             p[1].Direction = ParameterDirection.Output;
             p[2] = new SqlParameter("@MedicalBalanceLeaves", SqlDbType.Float);
             p[2].Direction = ParameterDirection.Output;
       
             p[3] = new SqlParameter("@WYear", SqlDbType.Int);
             p[3].Value = lpWYear;
             p[4] = new SqlParameter("@TotalBalance", SqlDbType.Float);
             p[4].Direction = ParameterDirection.Output;
             p[5] = new SqlParameter("@SDate", SqlDbType.DateTime);
             p[5].Value = _StartingDate;
             p[6] = new SqlParameter("@EDate", SqlDbType.DateTime);
             p[6].Value = _EndingDate;
             p[7] = new SqlParameter("@Planned", SqlDbType.Float);
             p[7].Direction = ParameterDirection.Output;

             p[8] = new SqlParameter("@noOfWFH", SqlDbType.Float);
             p[8].Direction = ParameterDirection.Output;

             p[9] = new SqlParameter("@compoffs", SqlDbType.Float);
             p[9].Direction = ParameterDirection.Output;

             p[10] = new SqlParameter("@shortleaves", SqlDbType.Float);
             p[10].Direction = ParameterDirection.Output;
         
             //SqlHelper.ExecuteDataset(ClsConnectionString.getConnectionString(), CommandType.StoredProcedure, "spGetTotalBalanceDaysBWDates", p);
             SqlHelper.ExecuteDataset(ClsConnectionString.getConnectionString(), CommandType.StoredProcedure, "spGetTotalBalanceDaysBWDatesChanged", p);
             if (p[1].Value != null && p[1].Value.ToString() != "")
                 _CasualBalanceLeaves = (double)p[1].Value;
             if (p[2].Value != null && p[2].Value.ToString() != "")
                 _MedicalBalanceLeaves = (double)p[2].Value;
             if (p[4].Value != null && p[4].Value.ToString() != "")
                 _TotalBalance = (double)p[4].Value;
             if (p[7].Value != null && p[7].Value.ToString() != "")
                 _Planned= (double)p[7].Value;
             if (p[8].Value != null && p[8].Value.ToString() != "")
                 _noofwhf = (double)p[8].Value;
             if (p[9].Value != null && p[9].Value.ToString() != "")
                 _compoffs = (double)p[9].Value;
             if (p[10].Value != null && p[10].Value.ToString() != "")
                 _shortleaves = (double)p[10].Value;

         }
         catch
         {
         }
         return _TotalBalance;
     }



     public int setHoliday(string strsql)
     {     
        int i= SqlHelper.ExecuteNonQuery(ClsConnectionString.getConnectionString(), CommandType.Text, strsql);
        return i;
     }

     public DataSet getHoliday(string strsql)
     {
         DataSet ds = SqlHelper.ExecuteDataset(ClsConnectionString.getConnectionString(), CommandType.Text, strsql);
         return ds;
     }

    public int updateHolidays(string strsql)
    {
         int i= SqlHelper.ExecuteNonQuery(ClsConnectionString.getConnectionString(), CommandType.Text, strsql);
         return i;
    }

    public bool CheckDateInHolidays(string strsql)
    {
        //int i = 0;
        bool blnTemp = false;
      object obj =SqlHelper.ExecuteScalar(ClsConnectionString.getConnectionString(), CommandType.Text, strsql);
      if (obj != null && (int)obj!=0)
      {
          blnTemp = true;
      }
      else
      {
          blnTemp = false;
      }
      return blnTemp;
    }


     public void GetDaysOfLeave(int lpWYear)
    {
        try
        {
            SqlParameter[] p = new SqlParameter[5];
            p[0] = new SqlParameter("@CasualBalanceLeaves", SqlDbType.Float);
            p[0].Direction = ParameterDirection.Output;
            p[1] = new SqlParameter("@MedicalBalanceLeaves", SqlDbType.Float);
            p[1].Direction = ParameterDirection.Output;
            p[2] = new SqlParameter("@EarnBalanceLeaves", SqlDbType.Float);
            p[2].Direction = ParameterDirection.Output;
            p[3] = new SqlParameter("@HalfPaidBalanceLeaves", SqlDbType.Float);
            p[3].Direction = ParameterDirection.Output;
            p[4] = new SqlParameter("@WYear", SqlDbType.Int);
            p[4].Value = lpWYear;


            SqlHelper.ExecuteDataset(ClsConnectionString.getConnectionString(), CommandType.StoredProcedure, "spGetDaysOfLeave", p);
            if (p[0].Value != null && p[0].Value.ToString() != "")
                _CasualBalanceLeaves = (double)p[0].Value;
            if (p[1].Value != null && p[1].Value.ToString() != "")
                _MedicalBalanceLeaves = (double)p[1].Value;
            if (p[2].Value != null && p[2].Value.ToString() != "")
                _EarnBalanceLeaves = (double)p[2].Value;
            if (p[3].Value != null && p[3].Value.ToString() != "")
                _HalfPaidBalanceLeaves = (double)p[3].Value;
        }
        catch
        {
        }

    }


     public void fnInsertLeavesDetail(DateTime dt, string strUserId, bool blnIsTimeOfJoiningYear)
     {
         try
         {
             int mm = dt.Month;
             clsLeave_Logic objLeave = new clsLeave_Logic();
             double TotalLeave=0.0;
             int intUserId = 0;
             int intYear = 0;

             intUserId = Convert.ToInt32(strUserId);
             fnGetLeaveDetails(dt.Year);
              //TotalLeave = TotalBalance;
              intYear = dt.Year;
              //fnGetTotalOfAllotedLeavesByEmp() Return total of leaves alloted to employee.
              // returns remaining balance and set seperately each category's used leaves

              if (blnIsTimeOfJoiningYear) //Calculation when yearly leaves are more than or equal to 12 in count
              {
                  if (mm == 1)
                  {
                      TotalLeave = TotalBalance;
                  }
                  else if (mm == 2)
                  {
                      TotalLeave = TotalBalance - 1;
                    
                  }
                  else if (mm == 3)
                  {
                      TotalLeave = TotalBalance - 2;
                     
                  }
                  else if (mm == 4)
                  {
                      TotalLeave = TotalBalance - 3;
                     
                  }

                  else if (mm == 5)
                  {
                      TotalLeave = TotalBalance - 4;
                     
                  }

                  else if (mm == 6)
                  {
                      TotalLeave = TotalBalance - 5;
                    
                  }

                  else if (mm == 7)
                  {
                      TotalLeave = TotalBalance - 6;
                   
                  }

                  else if (mm == 8)
                  {
                      TotalLeave = TotalBalance - 7;
                    
                  }
                  else if (mm == 9)
                  {
                      TotalLeave = TotalBalance - 8;
                     
                  }
                  else if (mm == 10)
                  {
                      TotalLeave = TotalBalance - 9;
                     
                  }
                  else if (mm == 11)
                  {
                      TotalLeave = TotalBalance - 10;
                     
                  }
                  else if (mm == 12)
                  {
                      TotalLeave = TotalBalance - 11;
                  }
              }
              else
              {
                  fnGetLeaveDetails(dt.Year);
                  TotalLeave = TotalBalance;
              }
             SetLeaveDetails4Users(intUserId, intYear, TotalLeave,0.0, 2); 
         }
         catch(Exception ex)
         {
           
         }
            
       
     }

    public DataSet fnGetUserId()
    {
        DataSet ds = null;
        string sql = "select [user_id],[DOJ] from users";
        ds = DatabaseHelper.getDataset(sql);
        return ds;
    }


    public void fnGetAllotedLeaveByEmp(int lpWYear)
    {
        try
        {
            SqlParameter[] p = new SqlParameter[6];
            p[0] = new SqlParameter("@UserName", SqlDbType.VarChar);
            p[0].Value = _UserName;
            p[1] = new SqlParameter("@CasualBalanceLeaves", SqlDbType.Float);
            p[1].Direction = ParameterDirection.Output;
            p[2] = new SqlParameter("@MedicalBalanceLeaves", SqlDbType.Float);
            p[2].Direction = ParameterDirection.Output;
            p[3] = new SqlParameter("@EarnBalanceLeaves", SqlDbType.Float);
            p[3].Direction = ParameterDirection.Output;
            p[4] = new SqlParameter("@HalfPaidBalanceLeaves", SqlDbType.Float);
            p[4].Direction = ParameterDirection.Output;
            p[5] = new SqlParameter("@WYear", SqlDbType.Int);
            p[5].Value = lpWYear;


            SqlHelper.ExecuteDataset(ClsConnectionString.getConnectionString(), CommandType.StoredProcedure, "spGetAllotedLeaveByEmp", p);
            if (p[1].Value != null && p[1].Value.ToString() != "")
                _CasualBalanceLeaves = (double)p[1].Value;
            if (p[2].Value != null && p[2].Value.ToString() != "")
                _MedicalBalanceLeaves = (double)p[2].Value;
            if (p[3].Value != null && p[3].Value.ToString() != "")
                _EarnBalanceLeaves = (double)p[3].Value;
            if (p[4].Value != null && p[4].Value.ToString() != "")
                _HalfPaidBalanceLeaves = (double)p[4].Value;
        }
        catch
        {
        }
        
    }
    public string fnGetTotalOfAllotedLeavesByEmp(int wyear)
    {
        string strTotalOfBL = "";
        SqlParameter[] sp = new SqlParameter[3];
        sp[0] = new SqlParameter("@UserName", SqlDbType.VarChar);
        sp[0].Value = UserName;
        sp[1] = new SqlParameter("@WYear", SqlDbType.Int);
        sp[1].Value = wyear;//@TotalBalance
        sp[2] = new SqlParameter("@Total", SqlDbType.VarChar, 5);
        sp[2].Direction = ParameterDirection.Output;
        DataSet ds = SqlHelper.ExecuteDataset(ClsConnectionString.getConnectionString(), CommandType.StoredProcedure, "spGetTotalOfAllotedLeaveByEmp", sp);
        strTotalOfBL = sp[2].Value.ToString();
        return strTotalOfBL;
    }

    public double[] fnGetTotalAndUsedLeave(string strUserName,int intYear)
    {
        double[] dblArrayOfUsedAndTotal = new double[2];
        double dblUsed = 0.0;
        string strTotal = "0.0";
        double dblTotal = 0.0;
        UserName = strUserName;
        GetTotalBalanceDaysInOne(intYear);
        // returns remaining balance and set seperately each category's used leaves
        dblUsed = CasualBalanceLeaves + MedicalBalanceLeaves ;

        strTotal = fnGetTotalOfAllotedLeavesByEmp(intYear);//fnGetTotalOfAllotedLeavesByEmp() Return total of leaves alloted to employee.
        if (strTotal.Trim() == "")
            strTotal = "0.0";
       
        dblTotal = Convert.ToDouble(strTotal);
        _AllotedLeaves = dblTotal;
        // e.Row.Cells[4].Text = objLeave.fnGetTotalOfBalanceLeave(DateTime.Now.Year) + " / " + strTotal;//fnGetTotalOfBalanceLeave() return total of leaves taken by employee.
        dblUsed = CasualBalanceLeaves + MedicalBalanceLeaves ;
        dblTotal = dblTotal - dblUsed;
          
        dblArrayOfUsedAndTotal[0] = dblUsed;
       
        dblArrayOfUsedAndTotal[1] = dblTotal;
       
        _TotalBalance = dblTotal;
        _BalanceLeaves = dblTotal;
        _UsedLeaves = dblUsed;

        return dblArrayOfUsedAndTotal;
    }

    public double[] fnGetTotalAndUsedLeaveBWDates(string strUserName, int intYear)
    {
        //double[] dblArrayOfUsedAndTotal = new double[2];
        double[] dblArrayOfUsedAndTotal = new double[5];
        double dblUsed = 0.0;
        string strTotal = "0.0";
        double dblTotal = 0.0;
        UserName = strUserName;
        GetTotalBalanceDaysInOneBWDate(intYear);
        // returns remaining balance and set seperately each category's used leaves
        dblUsed = CasualBalanceLeaves + MedicalBalanceLeaves;

        strTotal = fnGetTotalOfAllotedLeavesByEmp(intYear);//fnGetTotalOfAllotedLeavesByEmp() Return total of leaves alloted to employee.
        if (strTotal.Trim() == "")
            strTotal = "0.0";

        dblTotal = Convert.ToDouble(strTotal);
        _AllotedLeaves = dblTotal;
        // e.Row.Cells[4].Text = objLeave.fnGetTotalOfBalanceLeave(DateTime.Now.Year) + " / " + strTotal;//fnGetTotalOfBalanceLeave() return total of leaves taken by employee.
        dblUsed = CasualBalanceLeaves + MedicalBalanceLeaves;
        dblTotal = dblTotal - dblUsed;

        dblArrayOfUsedAndTotal[0] = dblUsed;

        dblArrayOfUsedAndTotal[1] = dblTotal;

        _TotalBalance = dblTotal;
        _BalanceLeaves = dblTotal;
        _UsedLeaves = dblUsed;
        _CasualBalanceLeaves = CasualBalanceLeaves;
        _MedicalBalanceLeaves = MedicalBalanceLeaves;
        _noofwhf = NoOfWHF;
        _shortleaves = ShortLeaves;
        return dblArrayOfUsedAndTotal;
    }

}
