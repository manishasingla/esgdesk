﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;
using System.Net.Mail;
using System.Configuration;
using System.Data;
/// <summary>
/// Summary description for Common
/// </summary>
public class Common
{
    string STRSQL = "";

	public Common()
	{
		//
		// TODO: Add constructor logic here
		//
	}
     
    public void HandleException(Exception ex, string emailid, string strSubject, string strQuery)
    {
        String strError = "Error Message: " + ex.Message + "<br><br>";
        if (ex.InnerException != null)
        {
            strError += "InnerException: " + Convert.ToString(ex.InnerException) + "<br><br>";
        }
        strError += "Stack Trace: " + ex.StackTrace + "<br><br>";
        strError += "source: " + ex.Source + "<br><br>";

        if (strQuery != null && strQuery != "")
        {
            strError += "\r\n <br> Query: " + strQuery;
        }
       // SendEmailList(emailid, strSubject, strError);
    }

    public string SendEmailList(string strTo, string strSubject, string strBody)
    {
        ///********* Method to send emails to the user as well as the admin*************///
        string done = "False";
        try
        {
           // MailMessage msg = new MailMessage();
           // msg.From = new MailAddress(ConfigurationManager.AppSettings["FromEmailId"]);
           // msg.To.Add(new MailAddress(strTo));
           // msg.Subject = strSubject;
           // msg.Body = strBody;
           // msg.IsBodyHtml = true;
           // msg.Priority = MailPriority.Normal;
           // //System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("mail.estatesolutions.co.uk");
           // System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("mail.estatesolutions.co.uk");
           // mSmtpClient.UseDefaultCredentials = false;
           // System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("test@estatesolutions.co.uk", "xyz");
           // //System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("support@internationalhorizons.co.uk", "support1!");
           //// System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("james@estatesolutions.co.uk", "jbRm64#2");
           // mSmtpClient.Credentials = loginInfo;
           // mSmtpClient.Send(msg);
           // done = "true";
        }
        catch (Exception ex)
        {
            done = ex.Message;
        }
        return done;
    }


    public void SendScheduleMail(string uname, string username)
    {
        //chaitali.
        ///********* Method to send emails to the user as well as the admin*************///
        ///

        

            string done = "False";
            try
            {
                string strBodys = "";
                strBodys = "Dear " + username + ",";
                strBodys += "<BR>";
                strBodys += "<BR>";
                strBodys += "Please ensure that you always have one task (one only) in ESG Desk as 'CWO' (currently working on) unless you are not at your desk (away from office, at lunch etc) in which case one should be 'CWO - but away'.";
                strBodys += "<BR>";
                strBodys += "<BR>";
                strBodys += "<BR>";
                strBodys += "<BR>";
                strBodys += "Thanks and regards,";
                strBodys += "<BR>";
                strBodys += "EsgDesk";
                strBodys += "<BR>";
                
                MailMessage msg = new MailMessage();
                //msg.From = new MailAddress(ConfigurationManager.AppSettings["chaitalip@esolutionsgroup.co.uk"]);
                msg.From = new MailAddress(ConfigurationManager.AppSettings["FromEmail2"]);
                msg.To.Add(new MailAddress(uname));
                msg.CC.Add(new MailAddress("michaelr@esolutionsgroup.co.uk"));
                msg.CC.Add(new MailAddress("imadk@esolutionsgroup.co.uk"));
              //  msg.CC.Add(new MailAddress("parvezc@esolutionsgroup.co.uk"));
              //  msg.Bcc.Add(new MailAddress("chaitalip@esolutionsgroup.co.uk"));
               
                msg.Subject = "ESG Desk: CWO Task " + DateTime.Now;
                msg.Body = strBodys;
                msg.IsBodyHtml = true;
                msg.Priority = MailPriority.Normal;
               // System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("mail.estatesolutions.co.uk");
                System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("213.171.222.69");
                //  System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("mail.estatesolutions.co.uk");
                mSmtpClient.UseDefaultCredentials = false;
                //System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("test@estatesolutions.co.uk", "aoNq74%6");
                System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("LMS@esolutionsgroup.co.uk", "Z0ngo1&2");// used only for esolutions site
                //System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("support@internationalhorizons.co.uk", "support1!");
                // System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("james@estatesolutions.co.uk", "jbRm64#2");
                mSmtpClient.Credentials = loginInfo;
                mSmtpClient.Send(msg);
                  done = "true";
            }
            catch (Exception ex)
            {
                done = ex.Message;
            }
            //return done;
        }
    

    public DataSet getUserreqcw(string username, int option)
    {
        STRSQL = "GetReqCwStatusUsers";
        DAL.DataAccessLayer dal = new DAL.DataAccessLayer();
        dal.ParameterSP("@username", username);
        dal.ParameterSPint("@option", option);
        return dal.ExecuteDatasetSP(STRSQL);
    }

    public DataSet getUserStatus(string username)
    {
        STRSQL = "GetStatusUsers";
        DAL.DataAccessLayer dal = new DAL.DataAccessLayer();
        dal.ParameterSP("@username", username);
        return dal.ExecuteDatasetSP(STRSQL);
    }

    public DataSet getUsersendemail(string username, int option)
    {
        STRSQL = "GetSendmailStatusUsers";
        DAL.DataAccessLayer dal = new DAL.DataAccessLayer();
        dal.ParameterSP("@username", username);
        dal.ParameterSPint("@option", option);
        //return dal.ExecuteNonQuerySTR(STRSQL);
        return dal.ExecuteDatasetSP(STRSQL);
    }


    //public  string Session { get; set; }

}