﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Configuration;

namespace DAL
{
    /// <summary>
    /// Summary description for DataAccessLayer
    /// </summary>
    /// 
    public class DataAccessLayer
    {
        private SqlCommand _command;
        private SqlParameter _commandpara;
        private SqlConnection _conn;
        private string _connectionstring;
        private SqlTransaction _trans;
        private string connectionString;
        private IDbCommand DBCMDSP;
        private IDbCommand DBCMDSTR;
        private IDbConnection dbConnection;
        private IDataAdapter IDBADTP;
        private string queryString;
        Common objCommon = new Common();

        public DataAccessLayer()
        {
            this._conn = new SqlConnection();
            this._connectionstring = ConfigurationManager.ConnectionStrings["strConn"].ToString();  //ConfigurationSettings.AppSettings["constr"];
            this._command = new SqlCommand();
            this.connectionString = ConfigurationManager.ConnectionStrings["strConn"].ToString();
            this.dbConnection = new SqlConnection(this.connectionString);
            this.IDBADTP = new SqlDataAdapter();
            this.DBCMDSP = new SqlCommand();
            this.DBCMDSTR = new SqlCommand();
            this.MyConnclosed();
            this._commandpara = new SqlParameter();
            this.DBCMDSP.Connection = this.dbConnection;
            this.DBCMDSTR.Connection = this.dbConnection;
            this.MyConn();
            this.DBCMDSP.CommandType = CommandType.StoredProcedure;
            this.DBCMDSTR.CommandType = CommandType.Text;
            this.DBCMDSTR.Parameters.Clear();
            this.DBCMDSP.Parameters.Clear();

            //

            // TODO: Add constructor logic here
            //
        }

        public SqlCommand OleDbCommandSP
        {
            get
            {
                return (SqlCommand)this.DBCMDSP;
            }
            set
            {
                this.DBCMDSP = value;
            }
        }

        public SqlCommand OleDbCommandSTR
        {
            get
            {
                return (SqlCommand)this.DBCMDSTR;
            }
            set
            {
                this.DBCMDSTR = value;
            }
        }

        public SqlDataAdapter OleDbDataAdapter
        {
            get
            {
                return (SqlDataAdapter)this.IDBADTP;
            }
            set
            {
                this.IDBADTP = value;
            }
        }

        public SqlParameter OLEParameterCommand
        {
            get
            {
                return this._commandpara;
            }
            set
            {
                this._commandpara = value;
            }
        }

        public SqlCommand SqlCommand
        {
            get
            {
                return this._command;
            }
            set
            {
                this._command = value;
            }
        }

        public SqlConnection SqlConnection
        {
            get
            {
                return this._conn;
            }
            set
            {
                this._conn = value;
            }
        }

        public string SqlConnectionString
        {
            get
            {
                return this._connectionstring;
            }
            set
            {
                this._connectionstring = value;
            }
        }

        public SqlTransaction Transaction
        {
            get
            {
                return this._trans;
            }
            set
            {
                this._trans = value;
            }
        }

        public DataSet ExecuteDatasetSP()
        {
            DataSet set2 = new DataSet();
            DataSet dataSet = new DataSet();
            try
            {
                this.MyConnDBCMDSP();
                this.OleDbDataAdapter.SelectCommand = this.OleDbCommandSP;
                this.OleDbDataAdapter.Fill(dataSet);
                set2 = dataSet;
            }
            catch (Exception EXE)
            {
                objCommon.HandleException(EXE, "chaitalip@esolutionsgroup.co.uk", "", "");
            }

            finally
            {
                this.MyConnclosed();
                this.dbConnection.Dispose();
            }
            return set2;
        }

        public DataSet ExecuteDatasetSP(string SPQUERY)
        {
            DataSet set = new DataSet();

            try
            {
                this.MyConnDBCMDSP();
                this.OleDbCommandSP.CommandText = SPQUERY;
                set = this.ExecuteDatasetSP();
            }
            catch (Exception EXE)
            {
                objCommon.HandleException(EXE, "chaitalip@esolutionsgroup.co.uk", "", "");
            }
            finally
            {
                this.MyConnclosed();
                this.dbConnection.Dispose();
            }
            return set;
        }

        public DataSet ExecuteDatasetSTR()
        {
            DataSet set2 = new DataSet();
            DataSet dataSet = new DataSet();

            try
            {
                this.MyConnDBCMDSTR();
                this.OleDbDataAdapter.SelectCommand = this.OleDbCommandSTR;
                this.OleDbDataAdapter.Fill(dataSet);
                set2 = dataSet;
            }
            catch (Exception EXE)
            {
                objCommon.HandleException(EXE, "chaitalip@esolutionsgroup.co.uk", "", "");
            }
            finally
            {
                this.MyConnclosed();
                this.dbConnection.Dispose();
            }
            return set2;
        }

        public DataSet ExecuteDatasetSTR(string STPQUERY)
        {
            DataSet set = new DataSet();

            try
            {
                this.MyConnDBCMDSTR();
                this.OleDbCommandSTR.CommandText = STPQUERY;
                set = this.ExecuteDatasetSTR();
            }
            catch (Exception EXE)
            {
                objCommon.HandleException(EXE, "chaitalip@esolutionsgroup.co.uk", "", "");
            }
            finally
            {
                this.MyConnclosed();
                this.dbConnection.Dispose();
            }
            return set;
        }

        //public void Executestringbuilder(string sc)
        //{
        //    this.MyConn();
        //    SqlCommand cmd = new SqlCommand(sc.ToString(),);
        //    cmd.CommandType = CommandType.Text;
        //    cmd.ExecuteNonQuery();

        //      this.OleDbCommandSP.CommandText = sc;
        //      str = this.ExecuteNonQuerySP();

        //    this.MyConnclosed();
        //}

        public string ExecuteNonQuerySP()
        {
            string str = "";

            try
            {
                this.MyConnDBCMDSP();
                str = Convert.ToString(this.OleDbCommandSP.ExecuteNonQuery());
                //if (str != "" && str != "-1")
                //{
                //    str = "1";
                //}
            }
            catch (Exception EXE)
            {
                str = "0";
                objCommon.HandleException(EXE, "chaitalip@esolutionsgroup.co.uk", "", "");
            }
            finally
            {
                this.MyConnclosed();
                this.dbConnection.Dispose();
            }
            return str;
        }


        public string ExecuteNonQuerySP(string STRQuery)
        {
            string str = "";

            try
            {
                this.MyConnDBCMDSP();
                this.OleDbCommandSP.CommandText = STRQuery;
                str = this.ExecuteNonQuerySP();
            }
            catch (Exception EXE)
            {
                str = "0";
                objCommon.HandleException(EXE, "chaitalip@esolutionsgroup.co.uk", "", "");
            }
            finally
            {
                this.MyConnclosed();
                this.dbConnection.Dispose();
            }
            return str;
        }

        public string ExecuteNonQuerySTR()
        {
            string str = "";

            try
            {
                this.MyConnDBCMDSTR();
                str = Convert.ToString(this.OleDbCommandSTR.ExecuteNonQuery());

                //this.MyConnclosed();
            }
            catch (Exception EXE)
            {
                objCommon.HandleException(EXE, "chaitalip@esolutionsgroup.co.uk", "", "");
            }
            finally
            {
                this.MyConnclosed();
                this.dbConnection.Dispose();
            }
            return str;
        }

        public string ExecuteNonQuerySTR(string STRQuery)
        {
            string str = "";
            try
            {
                this.MyConnDBCMDSTR();
                this.OleDbCommandSTR.CommandText = STRQuery;
                str = this.ExecuteNonQuerySTR();
            }
            catch (Exception EXE)
            {
                objCommon.HandleException(EXE, "chaitalip@esolutionsgroup.co.uk", "", "");
            }
            finally
            {
                this.MyConnclosed();
                this.dbConnection.Dispose();
            }
            return str;
        }


        //public SqlDataReader ExecuteReaderSP()
        //{
        //    using (SqlConnection conn = new SqlConnection(_connectionstring))
        //    {
        //        using (SqlDataReader reader = this.OleDbCommandSP.ExecuteReader())
        //        {
        //            //this.MyConn();
        //            //reader = this.OleDbCommandSP.ExecuteReader();
        //            conn.Open();
        //            return reader;
        //        }
        //    }
        //}

        public SqlDataReader ExecuteReaderSP()
        {
            SqlDataReader reader;
            this.MyConn();
            reader = this.OleDbCommandSP.ExecuteReader();
            return reader;
        }

        ////////idata reader testing reader close
        //public IDataReader ExecuteReader(string cmdTxt)    
        //{        
        //    SqlConnection conn = new SqlConnection(this._connectionstring);
        //    try
        //    {
        //        SqlCommand cmd = new SqlCommand(cmdTxt, conn);
        //        conn.Open();                
        //        return cmd.ExecuteReader(CommandBehavior.CloseConnection);           
        //    }
        //    catch
        //    {
        //        conn.Close();
        //        throw;
        //    }
        //    finally
        //    {
        //        conn.Close();
        //    }
        //}

        //idata end

        public SqlDataReader ExecuteReaderSP(string STRQuery)
        {

            //using (SqlConnection conn = new SqlConnection(_connectionstring))
            //{
            //    this.OleDbCommandSP.CommandText = STRQuery;
            //    using (SqlDataReader reader = this.OleDbCommandSP.ExecuteReader())
            //    {

            //        conn.Open();
            //        return reader;
            //    }
            //}


            this.OleDbCommandSP.CommandText = STRQuery;
            SqlDataReader reader = this.ExecuteReaderSP();
            return reader;
        }

        public SqlDataReader ExecuteReaderSTR()
        {
            SqlDataReader reader;
            this.MyConn();
            reader = this.OleDbCommandSTR.ExecuteReader();
            return reader;
        }

        public SqlDataReader ExecuteReaderSTR(string STRQuery)
        {
            SqlDataReader reader;
            this.MyConn();
            this.OleDbCommandSTR.CommandText = STRQuery;
            reader = this.ExecuteReaderSTR();
            return reader;
        }


        public string ExecuteScalarSP()
        {
            string str = "";
            this.MyConn();
            try
            {
                str = Convert.ToString(this.OleDbCommandSP.ExecuteScalar());
            }
            catch (Exception EXE)
            {
                objCommon.HandleException(EXE, "chaitalip@esolutionsgroup.co.uk", "", "");
            }
            finally
            {
                this.MyConnclosed();
                this.dbConnection.Dispose();
            }
            return str;
        }

        public string ExecuteScalarSP(string STRQuery)
        {
            string str = "";
            this.MyConn();
            try
            {
                this.OleDbCommandSP.CommandText = STRQuery;
                str = this.ExecuteScalarSP();
            }
            catch (Exception EXE)
            {
                objCommon.HandleException(EXE, "chaitalip@esolutionsgroup.co.uk", "", "");
            }
            finally
            {
                this.MyConnclosed();
                this.dbConnection.Dispose();
            }
            return str;
        }

        public string ExecuteScalarSTR()
        {
            string str = "";
            this.MyConn();
            try
            {
                str = Convert.ToString(this.OleDbCommandSTR.ExecuteScalar());
            }
            catch (Exception EXE)
            {
                objCommon.HandleException(EXE, "chaitalip@esolutionsgroup.co.uk", "", "");
            }
            finally
            {
                this.MyConnclosed();
                this.dbConnection.Dispose();
            }
            return str;
        }

        public string ExecuteScalarSTR(string STRQuery)
        {
            string str = "";
            this.MyConn();
            this.OleDbCommandSTR.CommandText = STRQuery;
            str = this.ExecuteScalarSTR().ToString();
            return str;
        }

        private void MyConn()
        {
            try
            {
                if (this.DBCMDSP.Connection.State == ConnectionState.Closed)
                {
                    this.DBCMDSP.Connection.Open();
                }
                //else if(this.DBCMDSP.Connection.State == ConnectionState.Open)
                //{
                //    this.DBCMDSP.Connection.Close();
                //    this.DBCMDSP.Connection.Open();
                //}

                if (this.DBCMDSTR.Connection.State == ConnectionState.Closed)
                {
                    this.DBCMDSTR.Connection.Open();
                }
                //else if (this.DBCMDSTR.Connection.State == ConnectionState.Open)
                //{
                //    this.DBCMDSTR.Connection.Close();
                //    this.DBCMDSTR.Connection.Open();
                //}

            }
            catch (Exception EXE)
            {
                objCommon.HandleException(EXE, "chaitalip@esolutionsgroup.co.uk", "", "");
            }
        }

        private void MyConnDBCMDSP()
        {
            try
            {
                if (this.DBCMDSP.Connection.State == ConnectionState.Closed)
                {
                    this.DBCMDSP.Connection.Open();
                }

            }
            catch (Exception EXE)
            {
                objCommon.HandleException(EXE, "chaitalip@esolutionsgroup.co.uk", "", "");
            }
        }

        private void MyConnDBCMDSTR()
        {
            try
            {
                if (this.DBCMDSTR.Connection.State == ConnectionState.Closed)
                {
                    this.DBCMDSTR.Connection.Open();
                }
            }
            catch (Exception EXE)
            {
                objCommon.HandleException(EXE, "chaitalip@esolutionsgroup.co.uk", "", "");
            }
        }

        private void MyConnclosed()
        {
            try
            {
                if (this.DBCMDSP.Connection != null)
                {
                    if (this.DBCMDSP.Connection.State == ConnectionState.Open)
                    {
                        this.DBCMDSP.Connection.Close();
                    }
                    //else
                    //{
                    //    this.DBCMDSP.Connection.Open();
                    //}
                }
                if (this.DBCMDSTR.Connection != null)
                {
                    if (this.DBCMDSTR.Connection.State == ConnectionState.Open)
                    {
                        this.DBCMDSTR.Connection.Close();
                    }
                    //else
                    //{
                    //     this.DBCMDSTR.Connection.Open();
                    //}
                }

            }
            catch (Exception EXE)
            {
                objCommon.HandleException(EXE, "chaitalip@esolutionsgroup.co.uk", "", "");
            }
        }


        public void Parameterrefresh()
        {
            this.DBCMDSTR.Parameters.Clear();
            this.DBCMDSP.Parameters.Clear();
            this.OleDbCommandSTR.Parameters.Clear();
            this.OleDbCommandSP.Parameters.Clear();
        }

        public void ParameterSP(SqlParameter STRPARA)
        {
            this.OleDbCommandSP.Parameters.Add(STRPARA);
        }


        public void ParameterSP(string parametername, string Paramaterval)
        {
            SqlParameter sTRPARA = new SqlParameter(parametername, Paramaterval);
            this.ParameterSP(sTRPARA);
        }

        public void ParameterSPDate(string parametername, DateTime Paramaterval)
        {
            SqlParameter sTRPARA = new SqlParameter(parametername, Paramaterval);
            this.ParameterSP(sTRPARA);
        }

        public void ParameterSPDec(string parametername, Decimal Paramaterval)
        {
            SqlParameter sTRPARA = new SqlParameter(parametername, Paramaterval);
            this.ParameterSP(sTRPARA);
        }

        public void ParameterSPfloat(string parametername, float Paramaterval)
        {
            SqlParameter sTRPARA = new SqlParameter(parametername, Paramaterval);
            this.ParameterSP(sTRPARA);
        }

        public void ParameterSPint(string parametername, int Paramaterval)
        {
            SqlParameter sTRPARA = new SqlParameter(parametername, Paramaterval);
            this.ParameterSP(sTRPARA);
        }


        public void ParameterSTR(IDbDataParameter STRPARA)
        {
            this.OleDbCommandSTR.Parameters.Add(STRPARA);
        }

        public void ParameterSTR(string parametername, string Paramaterval)
        {
            SqlParameter sTRPARA = new SqlParameter(parametername, Paramaterval);
            this.ParameterSTR(sTRPARA);
        }

    }
}