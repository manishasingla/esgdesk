using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net.Mail;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for DatabaseHelper
/// </summary>
public static class DatabaseHelper
{

    static string strConn = ConfigurationManager.AppSettings["strConn"];

    static string strConnECRM = ConfigurationManager.AppSettings["strConnECR"];

    static string strExceptionEmail = ConfigurationManager.AppSettings["ExceptionEmailId"];
    

    private static string getConnStr(string strFrom)
    {
        string strPath = HttpContext.Current.Server.MapPath(strFrom) + "\\MembersDB.mdb";
        string strDBConn = "Provider=Microsoft.Jet.OLEDB.4.0;Password=\"\";User ID=Admin;Data Source=" + strPath;
        return strDBConn;
    }


    public static object authenticateEsSCRMuser(string ClientId, string UserName, string Password)
    {
        object objResult;
        string strQuery = "Select count(*) from UserDetails where ClientId=" + ClientId + " and UserName='" + UserName.Replace("'", "''") + "' and Password='" + Password.Replace("'", "''") + "'";
        SqlConnection sqlConn = new SqlConnection(strConnECRM);
        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        try
        {
            //=============================================
            //sqlConn.Close();
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //==============================================


            objResult = sqlCmd.ExecuteScalar();
        }
        catch
        {
            objResult = "0";
        }
        finally
        {

            //=============================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();

            }
            //=============================================  

        }
        return objResult;
    }

    public static object authenticateLogin(string ClientId, string UserName, string Password)
    {
        object objResult;
        //string strQuery = "Select count(*) from NonesCRMusers where ClientId=" + ClientId + " or UserName='" + UserName.Replace("'", "''") + "' and Password='" + Password.Replace("'", "''") + "'";
        string strQuery = "Select count(*) from NonesCRMusers where ClientId=" + ClientId + " and UserName='" + UserName.Replace("'", "''") + "' and Password='" + Password.Replace("'", "''") + "'";
        SqlConnection sqlConn = new SqlConnection(strConn);
        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
       // try
       // {
            //=============================================
            //sqlConn.Close();
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //==============================================
            objResult = sqlCmd.ExecuteScalar();
      //  }
      //  catch
      //  {
       //     objResult = "0";
      //  }
      //  finally
      //  {
            //=============================================
         //   if (sqlConn.State == ConnectionState.Open)
         //   {
           //     sqlConn.Close();

          //  }
            //============================================= 
      //  }
        return objResult;
    }





    //============================================================================================================================

    public static void SendEmailList(string strTo, string strSubject, string strBody)
    {
        //bool blndone = false;
        //try
        //{
        //    System.Web.Mail.MailMessage msg = new System.Web.Mail.MailMessage();
        //    msg.To = strTo;
        //    msg.From = ConfigurationManager.AppSettings["FromEmail2"];
        //    msg.Subject = strSubject;
        //    msg.Body = strBody;
        //    msg.Priority = System.Web.Mail.MailPriority.High;
        //    msg.BodyFormat = System.Web.Mail.MailFormat.Html;
        //    System.Web.Mail.SmtpMail.Send(msg);
        //    blndone = true;
        //}
        //catch { blndone = false; }
        //return blndone;
        //******************************New Mail Format*****************************************************
        //rohit
     //   return ;
        bool blndone = false;
        try
        {


            MailMessage mMailMessage = new MailMessage();
            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
            msg.From = new MailAddress(ConfigurationManager.AppSettings["FromEmail2"]);

            string[] strToEmails = strTo.Split(';');
            for (int i = 0; i < strToEmails.Length; i++)
            {
                msg.To.Add(new MailAddress(strToEmails[i].ToString().Trim()));
            }


            msg.Subject = strSubject;
            msg.Body = strBody;
            msg.IsBodyHtml = true;
            msg.Priority = MailPriority.Normal;
            //System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("mail.estatesolutions.co.uk");
            //mSmtpClient.UseDefaultCredentials = false;
            //System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("test@estatesolutions.co.uk", "testtest");

            //System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("mail.internationalhorizons.co.uk");
            //mSmtpClient.UseDefaultCredentials = false;
            //System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("support@internationalhorizons.co.uk", "support1!");

            //-----------------Added by Rutuja - 7 June 2017---------------------
            System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("213.171.222.69");//213.171.196.58
            mSmtpClient.UseDefaultCredentials = false;
            System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("LMS@esolutionsgroup.co.uk", "Z0ngo1&2");
            //-----------------close-----------------------------
            mSmtpClient.Credentials = loginInfo;
            mSmtpClient.Send(msg);

            //blndone = true;
        }
        catch
        { //blndone = false; 
        }
        //return blndone;
        //************************************************************************************
    }


    public static bool SendEmailListEmp(string strTo, string strSubject, string strBody)
    {
        //bool blndone = false;
        //try
        //{
        //    System.Web.Mail.MailMessage msg = new System.Web.Mail.MailMessage();
        //    msg.To = strTo;
        //    msg.From = ConfigurationManager.AppSettings["FromEmail2"];
        //    msg.Subject = strSubject;
        //    msg.Body = strBody;
        //    msg.Priority = System.Web.Mail.MailPriority.High;
        //    msg.BodyFormat = System.Web.Mail.MailFormat.Html;
        //    System.Web.Mail.SmtpMail.Send(msg);
        //    blndone = true;
        //}
        //catch { blndone = false; }
        //return blndone;
        //******************************New Mail Format*****************************************************
        //rohit
        //   return ;
        bool blndone = false;
        try
        {


            MailMessage mMailMessage = new MailMessage();
            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
            msg.From = new MailAddress(ConfigurationManager.AppSettings["FromEmail2"]);

            string[] strToEmails = strTo.Split(';');
            for (int i = 0; i < strToEmails.Length; i++)
            {
                msg.To.Add(new MailAddress(strToEmails[i].ToString().Trim()));
            }


            msg.Subject = strSubject;
            msg.Body = strBody;
            msg.IsBodyHtml = true;
            msg.Priority = MailPriority.Normal;
            //System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("mail.estatesolutions.co.uk");
            //mSmtpClient.UseDefaultCredentials = false;
            //System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("test@estatesolutions.co.uk", "testtest");

            //System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("mail.internationalhorizons.co.uk");
            //mSmtpClient.UseDefaultCredentials = false;
            //System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("support@internationalhorizons.co.uk", "support1!");

            //-----------------Added by Rutuja - 7 June 2017---------------------
            System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("213.171.222.69");//213.171.196.58
            mSmtpClient.UseDefaultCredentials = false;
            System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("LMS@esolutionsgroup.co.uk", "Z0ngo1&2");
            //-----------------close-----------------------------
            mSmtpClient.Credentials = loginInfo;
            mSmtpClient.Send(msg);

            blndone = true;
        }
        catch
        {
            blndone = false;
        }
        return blndone;
        //************************************************************************************
    }

    //============================================================================================================================


    public static bool sendEmailChangeRequestNorply(string strTo, string strSubject, string strBody)
    {
        //bool blndone = false;
        //try
        //{
        //    System.Web.Mail.MailMessage msg = new System.Web.Mail.MailMessage();
        //    msg.To = strTo;
        //    msg.From = ConfigurationManager.AppSettings["NoRplyFromEmail1"];
        //    msg.Subject = strSubject;
        //    msg.Body = strBody;
        //    msg.Priority = System.Web.Mail.MailPriority.High;
        //    msg.BodyFormat = System.Web.Mail.MailFormat.Html;
        //    System.Web.Mail.SmtpMail.Send(msg);
        //    blndone = true;
        //}
        //catch { blndone = false; }
        //return blndone;

        //******************************New Mail Format*****************************************************
        //rohit
    ///    return true;
        bool blndone = false;
        try
        {
            MailMessage mMailMessage = new MailMessage();
            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
            msg.From = new MailAddress(ConfigurationManager.AppSettings["NoRplyFromEmail1"]);

            string[] strToEmails = strTo.Split(';');
            for (int i = 0; i < strToEmails.Length; i++)
            {
                msg.To.Add(new MailAddress(strToEmails[i].ToString().Trim()));
            }

            msg.Bcc.Add("michaelr@esolutionsgroup.co.uk");


            msg.Subject = strSubject;
            msg.Body = strBody;
            msg.IsBodyHtml = true;
            msg.Priority = MailPriority.Normal;

            //System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("mail.estatesolutions.co.uk");
            //mSmtpClient.UseDefaultCredentials = false;
            //System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("test@estatesolutions.co.uk", "testtest");

            System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("mail.internationalhorizons.co.uk");
            mSmtpClient.UseDefaultCredentials = false;
            System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("support@internationalhorizons.co.uk", "support1!");

            mSmtpClient.Credentials = loginInfo;
            mSmtpClient.Send(msg);

            blndone = true;
        }
        catch { blndone = false; }
        return blndone;
        //************************************************************************************
    }

    public static bool sendEmailChangeRequest(string strTo, string strSubject, string strBody)
    {
        //bool blndone = false;
        //try
        //{
        //    System.Web.Mail.MailMessage msg = new System.Web.Mail.MailMessage();
        //    msg.To = strTo;
        //    msg.From = ConfigurationManager.AppSettings["FromEmail1"];
        //    msg.Subject = strSubject;
        //    msg.Body = strBody;
        //    msg.Priority = System.Web.Mail.MailPriority.High;
        //    msg.BodyFormat = System.Web.Mail.MailFormat.Html;
        //    System.Web.Mail.SmtpMail.Send(msg);
        //    blndone = true;
        //}
        //catch { blndone = false; }
        //return blndone;

        //******************************New Mail Format*****************************************************
        bool blndone = false;
        try
        {
            MailMessage mMailMessage = new MailMessage();
            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
            msg.From = new MailAddress(ConfigurationManager.AppSettings["FromEmail1"]);

            string[] strToEmails = strTo.Split(';');
            for (int i = 0; i < strToEmails.Length; i++)
            {
                msg.To.Add(new MailAddress(strToEmails[i].ToString().Trim()));
            }


            msg.Subject = strSubject;
            msg.Body = strBody;
            msg.IsBodyHtml = true;
            msg.Priority = MailPriority.Normal;
            //System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("mail.estatesolutions.co.uk");
            //mSmtpClient.UseDefaultCredentials = false;
            //System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("test@estatesolutions.co.uk", "testtest");

            System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("mail.internationalhorizons.co.uk");
            mSmtpClient.UseDefaultCredentials = false;
            System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("support@internationalhorizons.co.uk", "support1!");
            mSmtpClient.Credentials = loginInfo;
            mSmtpClient.Send(msg);

            blndone = true;
        }
        catch { blndone = false; }
        return blndone;
        //************************************************************************************

    }


    public static bool sendEmailTasks(string strTo, string strSubject, string strBody)
    {
        //bool blndone = false;
        //try
        //{
        //    System.Web.Mail.MailMessage msg = new System.Web.Mail.MailMessage();
        //    msg.To = strTo;
        //    msg.From = ConfigurationManager.AppSettings["FromEmail2"];
        //    msg.Subject = strSubject;
        //    msg.Body = strBody;
        //    msg.Priority = System.Web.Mail.MailPriority.High;
        //    msg.BodyFormat = System.Web.Mail.MailFormat.Html;
        //    System.Web.Mail.SmtpMail.Send(msg);
        //    blndone = true;
        //}
        //catch { blndone = false; }
        //return blndone;
        //******************************New Mail Format*****************************************************
        //rohit
     //   return true;
        bool blndone = false;
        try
        {


            MailMessage mMailMessage = new MailMessage();
            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
            msg.From = new MailAddress(ConfigurationManager.AppSettings["FromEmail2"]);

            string[] strToEmails = strTo.Split(';');
            for (int i = 0; i < strToEmails.Length; i++)
            {
                msg.To.Add(new MailAddress(strToEmails[i].ToString().Trim()));
            }


            msg.Subject = strSubject;
            msg.Body = strBody;
            msg.IsBodyHtml = true;
            msg.Priority = MailPriority.Normal;

            System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("213.171.222.69");
            mSmtpClient.UseDefaultCredentials = false;
            System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("LMS@esolutionsgroup.co.uk", "Z0ngo1&2");// used only for esolutions site


            //System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("mail.estatesolutions.co.uk");
            //mSmtpClient.UseDefaultCredentials = false;
            //System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("test@estatesolutions.co.uk", "testtest");

            //System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("mail.internationalhorizons.co.uk");
            //mSmtpClient.UseDefaultCredentials = false;
            //System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("support@internationalhorizons.co.uk", "support1!");

            mSmtpClient.Credentials = loginInfo;
            mSmtpClient.Send(msg);

            blndone = true;
        }
        catch { blndone = false; }
        return blndone;
        //************************************************************************************



    }

    public static bool sendEmailEODC(string strTo, string strFrom, string strSubject, string strBody, int i)
    {

        //******************************New Mail Format*****************************************************
        bool blndone = false;
        try
        {
            MailMessage mMailMessage = new MailMessage();
            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
            msg.From = new MailAddress(strFrom);
            string[] strToEmails = strTo.Split(';');

            for (int j = 0; j < strToEmails.Length; j++)
            {
                msg.To.Add(new MailAddress(strToEmails[j].ToString().Trim()));
            }
            if (i == 1)
            {
               //Adding By Sushama Acharya
                 msg.CC.Add(new MailAddress("imadk@esolutionsgroup.co.uk"));
                 msg.CC.Add(new MailAddress("harshadag@esolutionsgroup.co.uk"));

               //Commented By Sushama Acharya
                //msg.Cc = "rohitd@123sitesolutions.com; priyab@123sitesolutions.com";
            }
            //msg.Bcc.Add(new MailAddress("rohitd@123sitesolutions.com"));
            msg.Subject = strSubject;
            msg.Body = strBody;
            msg.IsBodyHtml = true;
            msg.Priority = MailPriority.Normal;


            //Commented By Sushama Acharya
                //System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("mail.estatesolutions.co.uk");
                //mSmtpClient.UseDefaultCredentials = false;
                //System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("test@estatesolutions.co.uk", "testtest");

                //Comment By Sushama Acharya Dt 7 Apr 2015
                //System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("mail.internationalhorizons.co.uk");
                //mSmtpClient.UseDefaultCredentials = false;
                //System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("support@internationalhorizons.co.uk", "support1!");
            // //Commented By Sushama Acharya

            System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("213.171.196.58");
            mSmtpClient.UseDefaultCredentials = false;          
            System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("LMS@esolutionsgroup.co.uk", "Z0ngo1&2");



            mSmtpClient.Credentials = loginInfo;
            mSmtpClient.Send(msg);

            blndone = true;
        }
        catch { blndone = false; }
        return blndone;
        //************************************************************************************

        
    }

    public static object insertClientRequest(string ClientId, string UserName, string WebsiteURL, string ShrtDescr, string Comment, string companyname, string Project, string lastupdatedby, string CRPostedby, string interbalref, string commentaddedby)
    {
        object objResult;
        string company = "";
        if (companyname != null)
        {
            company = companyname;
        }
        else
        {
            company = "";
        }

        string strQuery = @"DECLARE @RequestId INT;";
        strQuery += " insert into ClientRequest(ClientId,UserName,WebsiteURL,ShortDescr,Status,Priority,LastUpdatedBy,LastUpdatedOn,RequestDate,CompanyName,ProjectName,AddedBy,Internalref) ";
        strQuery += " values(" + ClientId + ",";
        strQuery += "'" + UserName.Replace("'", "''") + "',";
        strQuery += "'" + WebsiteURL + "',";
        strQuery += "'" + ShrtDescr.Replace("'", "''") + "',";
        strQuery += "'new',";
        strQuery += "8,";
        strQuery += "'" + lastupdatedby.Replace("'", "''") + "',";
        strQuery += "getdate(),";
        strQuery += "getdate(), '" + company.ToString().Replace("'", "''") + "', '" + Project.ToString().Replace("'", "''") + "','" + CRPostedby.ToString().Replace("'", "''") + "', '" + interbalref.ToString() + "'); ";
        strQuery += " SET @RequestId = SCOPE_IDENTITY(); ";
        strQuery += "DECLARE @CommentId INT;";
        strQuery += " insert into ClientRequest_Details(RequestId,Comment,PostedBy,PostedOn) ";
        strQuery += " values(@RequestId,";
        strQuery += "'" + Comment.Replace("'", "''") + "',";
        strQuery += "'" + lastupdatedby.Replace("'", "''") + "',";
        strQuery += "getdate()); ";
        strQuery += " SET @CommentId = SCOPE_IDENTITY(); ";
        strQuery += " insert into read_CR_comments([CommentId],[UserName]) ";
        strQuery += " values(@CommentId,'" + commentaddedby.ToString().Replace("'", "''") + "')";

        strQuery += "select @RequestId;";

        SqlConnection sqlConn = new SqlConnection(strConn);

        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        try
        {
            //============================================
            //sqlConn.Close();
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //================================================


            objResult = sqlCmd.ExecuteScalar();
        }
        catch
        {
            objResult = "0";
        }
        finally
        {
            //============================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            // sqlConn.Close();
            //===========================================


        }
        return objResult;
    }

    public static object insertClientRequest(string ClientId, string UserName, string WebsiteURL, string ShrtDescr, string Comment, string companyname, string Project, string lastupdatedby, string CRPostedby, string interbalref, string commentaddedby, string strMacId, string strip, string stradmin)
    {
        object objResult;
        string company = "";
        if (companyname != null)
        {
            company = companyname;
        }
        else
        {
            company = "";
        }

        string strQuery = @"DECLARE @RequestId INT;";
        strQuery += " insert into ClientRequest(ClientId,UserName,CRApprove,WebsiteURL,ShortDescr,Status,Priority,LastUpdatedBy,LastUpdatedOn,RequestDate,CompanyName,ProjectName,AddedBy,Internalref) ";
        strQuery += " values(" + ClientId + ",";
        strQuery += "'" + UserName.Replace("'", "''") + "',";
        if (CanApprove(stradmin))
        {
            strQuery += "'True',";
        }
        else
        {
            strQuery += "'False',";
        }
        strQuery += "'" + WebsiteURL + "',";
        strQuery += "'" + ShrtDescr.Replace("'", "''") + "',";
        strQuery += "'new',";
        strQuery += "8,";
        strQuery += "'" + lastupdatedby.Replace("'", "''") + "',";
        strQuery += "getdate(),";
        strQuery += "getdate(), '" + company.ToString().Replace("'", "''") + "', '" + Project.ToString().Replace("'", "''") + "','" + CRPostedby.ToString().Replace("'", "''") + "', '" + interbalref.ToString() + "'); ";
        strQuery += " SET @RequestId = SCOPE_IDENTITY(); ";
        strQuery += "DECLARE @CommentId INT;";
        strQuery += " insert into ClientRequest_Details(RequestId,Comment,Ip,Macid,Approve,ApprovUnApprov,PostedBy,PostedOn) ";
        strQuery += " values(@RequestId,";
        strQuery += "'" + Comment.Replace("'", "''") + "',";
        //***********************************************************
        strQuery += "'" + strip + "',";
        strQuery += "'" + strMacId + "',";
        if (CanApprove(stradmin))
        {
            strQuery += "'True',";
            strQuery += "'UnApprove',";
        }
        else
        {
            strQuery += "'False',";
            strQuery += "'Approve',";
        }
        //***********************************************************
        strQuery += "'" + lastupdatedby.Replace("'", "''") + "',";
        strQuery += "getdate()); ";
        strQuery += " SET @CommentId = SCOPE_IDENTITY(); ";
        strQuery += " insert into read_CR_comments([CommentId],[UserName]) ";
        strQuery += " values(@CommentId,'" + commentaddedby.ToString().Replace("'", "''") + "')";

        strQuery += "select @RequestId;";

        SqlConnection sqlConn = new SqlConnection(strConn);

        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        try
        {
            //============================================
            //sqlConn.Close();
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //================================================


            objResult = sqlCmd.ExecuteScalar();
        }
        catch
        {
            objResult = "0";
        }
        finally
        {
            //============================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            // sqlConn.Close();
            //===========================================


        }
        return objResult;
    }
    public static object insertClientRequest(string ClientId, string UserName, string WebsiteURL, string ShrtDescr, string Comment, string companyname, string Project, string lastupdatedby, string CRPostedby, string interbalref, string commentaddedby, string strMacId, string strip)
    {
        object objResult;
        string company = "";
        if (companyname != null)
        {
            company = companyname;
        }
        else
        {
            company = "";
        }

        string strQuery = @"DECLARE @RequestId INT;";
        strQuery += " insert into ClientRequest(ClientId,UserName,CRApprove,WebsiteURL,ShortDescr,Status,Priority,LastUpdatedBy,LastUpdatedOn,RequestDate,CompanyName,ProjectName,AddedBy,Internalref) ";
        strQuery += " values(" + ClientId + ",";
        strQuery += "'" + UserName.Replace("'", "''") + "',";
        strQuery += "'True',";
        strQuery += "'" + WebsiteURL + "',";
        strQuery += "'" + ShrtDescr.Replace("'", "''") + "',";
        strQuery += "'new',";
        strQuery += "8,";
        strQuery += "'" + lastupdatedby.Replace("'", "''") + "',";
        strQuery += "getdate(),";
        strQuery += "getdate(), '" + company.ToString().Replace("'", "''") + "', '" + Project.ToString().Replace("'", "''") + "','" + CRPostedby.ToString().Replace("'", "''") + "', '" + interbalref.ToString() + "'); ";
        strQuery += " SET @RequestId = SCOPE_IDENTITY(); ";
        strQuery += "DECLARE @CommentId INT;";
        strQuery += " insert into ClientRequest_Details(RequestId,Comment,Ip,Macid,Approve,ApprovUnApprov,PostedBy,PostedOn) ";
        strQuery += " values(@RequestId,";
        strQuery += "'" + Comment.Replace("'", "''") + "',";
        //***********************************************************
        strQuery += "'" + strip + "',";
        strQuery += "'" + strMacId + "',";
        strQuery += "'True',";
        strQuery += "'UnApprove',";
        //***********************************************************
        strQuery += "'" + lastupdatedby.Replace("'", "''") + "',";
        strQuery += "getdate()); ";
        strQuery += " SET @CommentId = SCOPE_IDENTITY(); ";
        strQuery += " insert into read_CR_comments([CommentId],[UserName]) ";
        strQuery += " values(@CommentId,'" + commentaddedby.ToString().Replace("'", "''") + "')";

        strQuery += "select @RequestId;";

        SqlConnection sqlConn = new SqlConnection(strConn);

        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        try
        {
            //============================================
            //sqlConn.Close();
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //================================================


            objResult = sqlCmd.ExecuteScalar();
        }
        catch
        {
            objResult = "0";
        }
        finally
        {
            //============================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            // sqlConn.Close();
            //===========================================


        }
        return objResult;
    }

    public static object insertClientRequests(string ClientId, string UserName, string WebsiteURL, string ShrtDescr, string Comment, string companyname, string Project, string Type, string lastupdatedby, string CRPostedby, string interbalref, string commentaddedby)
    {
        object objResult;
        string company = "";
        if (companyname != null)
        {
            company = companyname;
        }
        else
        {
            company = "";
        }

        string strQuery = @"DECLARE @RequestId INT;";
        strQuery += " insert into ClientRequest(ClientId,UserName,WebsiteURL,ShortDescr,Status,Priority,LastUpdatedBy,LastUpdatedOn,RequestDate,CompanyName,ProjectName,ExchangeType,AddedBy,Internalref) ";
        strQuery += " values(" + ClientId + ",";
        strQuery += "'" + UserName.Replace("'", "''") + "',";
        strQuery += "'" + WebsiteURL + "',";
        strQuery += "'" + ShrtDescr.Replace("'", "''") + "',";
        strQuery += "'new',";
        strQuery += "8,";
        strQuery += "'" + lastupdatedby.Replace("'", "''") + "',";
        strQuery += "getdate(),";
        strQuery += "getdate(), '" + company.ToString().Replace("'", "''") + "', '" + Project.ToString().Replace("'", "''") + "','" + Type.ToString().Replace("'", "''") + "','" + CRPostedby.ToString().Replace("'", "''") + "', '" + interbalref.ToString() + "'); ";
        strQuery += " SET @RequestId = SCOPE_IDENTITY(); ";
        strQuery += "DECLARE @CommentId INT;";
        strQuery += " insert into ClientRequest_Details(RequestId,Comment,PostedBy,PostedOn) ";
        strQuery += " values(@RequestId,";
        strQuery += "'" + Comment.Replace("'", "''") + "',";
        strQuery += "'" + lastupdatedby.Replace("'", "''") + "',";
        strQuery += "getdate()); ";
        strQuery += " SET @CommentId = SCOPE_IDENTITY(); ";
        strQuery += " insert into read_CR_comments([CommentId],[UserName]) ";
        strQuery += " values(@CommentId,'" + commentaddedby.ToString().Replace("'", "''") + "')";

        strQuery += "select @RequestId;";

        SqlConnection sqlConn = new SqlConnection(strConn);

        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        try
        {
            //============================================
            //sqlConn.Close();
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //================================================


            objResult = sqlCmd.ExecuteScalar();
        }
        catch
        {
            objResult = "0";
        }
        finally
        {

            //============================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            // sqlConn.Close();
            //===========================================

        }
        return objResult;
    }

    public static object updateClientRequest(int RequestId, string ClientId, string UserName, string WebsiteURL, string ShrtDescr, string Comment, string companyname, string project)
    {
        object objResult;
        string strQuery = "";

        if (Comment != "")
        {
            strQuery = " Update ClientRequest ";
            strQuery += " set WebsiteURL='" + WebsiteURL.Replace("'", "''") + "', ";
            strQuery += "ShortDescr='" + ShrtDescr.Replace("'", "''") + "', ";
            strQuery += "LastUpdatedBy='" + UserName.Replace("'", "''") + "', ";
            strQuery += "LastUpdatedOn=getdate() ,";
            strQuery += "CompanyName='" + companyname + "',";
            strQuery += "ProjectName='" + project.Replace("'", "''") + "' ";

            strQuery += "where RequestId=" + RequestId.ToString() + "; ";
            strQuery += "DECLARE @CommentId INT;";
            strQuery += " insert into ClientRequest_Details(RequestId,Comment,PostedBy,PostedOn) ";
            strQuery += " values(" + RequestId.ToString() + ",";
            strQuery += "'" + Comment.Replace("'", "''") + "',";
            strQuery += "'" + UserName.Replace("'", "''") + "',";
            strQuery += "getdate()); ";
            strQuery += " SET @CommentId = SCOPE_IDENTITY(); ";
            strQuery += " insert into read_CR_comments([CommentId],[UserName]) ";
            strQuery += " values(@CommentId,'" + UserName.Replace("'", "''") + "')";
        }
        else
        {
            strQuery = " Update ClientRequest ";
            strQuery += " set WebsiteURL='" + WebsiteURL.Replace("'", "''") + "', ";
            strQuery += "ShortDescr='" + ShrtDescr.Replace("'", "''") + "', ";
            strQuery += "LastUpdatedBy='" + UserName.Replace("'", "''") + "', ";
            strQuery += "LastUpdatedOn=getdate(),";
            strQuery += "CompanyName='" + companyname + "', ";
            strQuery += "ProjectName='" + project.Replace("'", "''") + "' ";
            strQuery += "where RequestId=" + RequestId.ToString() + "; ";
        }

        SqlConnection sqlConn = new SqlConnection(strConn);
        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        try
        {
            //============================================
            //sqlConn.Close();
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //================================================
            objResult = sqlCmd.ExecuteNonQuery();
        }
        catch
        {
            objResult = "0";
        }
        finally
        {
            //============================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            // sqlConn.Close();
            //===========================================

        }
        return objResult;
    }
    public static object updateClientRequest(int RequestId, string ClientId, string UserName, string WebsiteURL, string ShrtDescr, string Comment, string companyname, string project, string strMacid, string strIP)
    {
        object objResult;
        string strQuery = "";

        if (Comment != "")
        {
            strQuery = " Update ClientRequest ";
            strQuery += " set WebsiteURL='" + WebsiteURL.Replace("'", "''") + "', ";
            strQuery += "ShortDescr='" + ShrtDescr.Replace("'", "''") + "', ";
            strQuery += "LastUpdatedBy='" + UserName.Replace("'", "''") + "', ";
            strQuery += "LastUpdatedOn=getdate() ,";
            strQuery += "CompanyName='" + companyname + "',";
            strQuery += "ProjectName='" + project.Replace("'", "''") + "',";
            //******************************************************************
            strQuery += "CRApprove='True'";
            //******************************************************************
            strQuery += "where RequestId=" + RequestId.ToString() + "; ";
            strQuery += "DECLARE @CommentId INT;";
            strQuery += " insert into ClientRequest_Details(RequestId,Comment,Macid,Ip,Approve,ApprovUnApprov,PostedBy,PostedOn) ";
            strQuery += " values(" + RequestId.ToString() + ",";
            strQuery += "'" + Comment.Replace("'", "''") + "',";
            //*************************************************
            strQuery += "'" + strMacid + "',";
            strQuery += "'" + strIP + "',";
            strQuery += "'True',";
            strQuery += "'UnApprove',";
            //*************************************************
            strQuery += "'" + UserName.Replace("'", "''") + "',";
            strQuery += "getdate()); ";
            strQuery += " SET @CommentId = SCOPE_IDENTITY(); ";
            strQuery += " insert into read_CR_comments([CommentId],[UserName]) ";
            strQuery += " values(@CommentId,'" + UserName.Replace("'", "''") + "')";
        }
        else
        {
            strQuery = " Update ClientRequest ";
            strQuery += " set WebsiteURL='" + WebsiteURL.Replace("'", "''") + "', ";
            strQuery += "ShortDescr='" + ShrtDescr.Replace("'", "''") + "', ";
            strQuery += "LastUpdatedBy='" + UserName.Replace("'", "''") + "', ";
            strQuery += "LastUpdatedOn=getdate(),";
            strQuery += "CompanyName='" + companyname + "', ";
            strQuery += "ProjectName='" + project.Replace("'", "''") + "' ";
            strQuery += "where RequestId=" + RequestId.ToString() + "; ";
        }

        SqlConnection sqlConn = new SqlConnection(strConn);
        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        try
        {
            //============================================
            //sqlConn.Close();
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //================================================
            objResult = sqlCmd.ExecuteNonQuery();
        }
        catch
        {
            objResult = "0";
        }
        finally
        {
            //============================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            // sqlConn.Close();
            //===========================================

        }
        return objResult;
    }

    public static DataSet getClientRequests(string ClientId, string UserName, string strFilter, string strOrderBy)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select * " +
                              "from ClientRequest " +
                              "where ClientId=" + ClientId + " and UserName='" + UserName.Replace("'", "''") + "' " + strFilter + " " + strOrderBy;

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    public static DataSet getClientRequest(int RequestId)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select * " +
                              "from ClientRequest " +
                              "where RequestId=" + RequestId.ToString();

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    public static DataSet getClientRequestDetails(int RequestId)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            //===================================================================================================================
            //string strQuery = "Select * from ClientRequest_Details where deleted <> 1 and RequestId=" + RequestId.ToString() + " order by CommentId desc";
            string strQuery = "Select *  from ClientRequest_Details where deleted <> 1 and (Approve=1 or Approve Is NULL)and RequestId=" + RequestId.ToString() + " order by CommentId desc";
            //===================================================================================================================
            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    public static object registerUser(string UserName, string Password, string FirstName, string Surname, string CompanyName, string Phone, string Email, string ClientId, string WebSite)
    {
        object objResult;
        string strQuery = "";

        strQuery += "insert into NonesCRMusers(UserName,Password,FirstName,Surname,CompanyName,Phone,Email,ClientId,WebSite,RegDate) ";
        strQuery += "values('" + UserName.Replace("'", "''") + "',";
        strQuery += "'" + Password.Replace("'", "''") + "',";
        strQuery += "'" + FirstName.Replace("'", "''") + "',";
        strQuery += "'" + Surname.Replace("'", "''") + "',";
        strQuery += "'" + CompanyName.Replace("'", "''") + "',";
        strQuery += "'" + Phone + "',";
        strQuery += "'" + Email.Replace("'", "''") + "',";
        strQuery += "" + ClientId + ",";
        strQuery += "'" + WebSite.Replace("'", "''") + "',";
        strQuery += "'" + DateTime.Parse(System.DateTime.Now.ToString(), System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat).ToString("MM/dd/yyyy hh:mm:ss") + "')";

        SqlConnection sqlConn = new SqlConnection(strConn);
        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        try
        {
            //============================================
            //sqlConn.Close();
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //================================================


            objResult = sqlCmd.ExecuteNonQuery();
        }
        catch
        {
            objResult = "0";
        }
        finally
        {
            //============================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            // sqlConn.Close();
            //===========================================
        }
        return objResult;
    }

    public static object updateRegisterUser(string UserName, string FirstName, string Surname, string CompanyName, string Phone, string Email, string ClientId, string WebSite)
    {
        object objResult;
        string strQuery = "";

        strQuery += "Update NonesCRMusers ";
        strQuery += "set ";
        strQuery += "FirstName='" + FirstName.Replace("'", "''") + "',";
        strQuery += "Surname='" + Surname.Replace("'", "''") + "',";
        strQuery += "CompanyName='" + CompanyName.Replace("'", "''") + "',";
        strQuery += "Phone='" + Phone + "',";
        strQuery += "Email='" + Email.Replace("'", "''") + "',";
        strQuery += "WebSite='" + WebSite.Replace("'", "''") + "' ";
        strQuery += "where UserName='" + UserName.Replace("'", "''") + "'";

        SqlConnection sqlConn = new SqlConnection(strConn);
        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        try
        {
            //============================================
            //sqlConn.Close();
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //================================================
            objResult = sqlCmd.ExecuteNonQuery();
        }
        catch
        {
            objResult = "0";
        }
        finally
        {
            //============================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            // sqlConn.Close();
            //===========================================
        }

        return objResult;
    }

    public static object deleteaddedsubscription(string strSuscriptionID)
    {
        object objResult = "0";
        SqlConnection sqlConn = new SqlConnection(strConn);

        try
        {
            string sqlQuery = "delete from  [Client_subscription]  where SubscriptionID = " + strSuscriptionID.Replace("'", "''") + "";

            SqlCommand sqlCmd = new SqlCommand(sqlQuery, sqlConn);
            //============================================
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //================================================

            objResult = sqlCmd.ExecuteNonQuery();
        }
        catch { objResult = "0"; }
        finally
        {
            //============================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            //=========================================== 



        }
        return objResult;
    }


    public static object deleteRegisteredUser(string strUserName)
    {
        object objResult = "0";
        SqlConnection sqlConn = new SqlConnection(strConn);

        try
        {
            string sqlQuery = "delete from  ClientRequest_Details Where [RequestId] in (select RequestId from ClientRequest where username = '" + strUserName.Replace("'", "''") + "');";
            sqlQuery += "delete from  ClientRequest Where [UserName]='" + strUserName.Replace("'", "''") + "';";
            sqlQuery += "delete from  NonesCRMusers Where [UserName]='" + strUserName.Replace("'", "''") + "'";
            SqlCommand sqlCmd = new SqlCommand(sqlQuery, sqlConn);
            //============================================
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //================================================
            objResult = sqlCmd.ExecuteNonQuery();
        }
        catch { objResult = "0"; }
        finally
        {
            //===========================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            //=========================================

        }
        return objResult;
    }


    public static DataSet getClientSubscription(string strOrderBy)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select * " +
                              "from Client_subscription " + strOrderBy;

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }



    public static DataSet getUserDetails(string strOrderBy)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select * " +
                              "from NonesCRMusers " + strOrderBy;

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    public static DataSet getUserDetails(string ClientId, string UserName)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select * " +
                              "from NonesCRMusers " +
                              "where ClientId=" + ClientId.ToString() + " and UserName='" + UserName.Replace("'", "''") + "'";

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    public static DataSet getUserDetails2(string company, string UserName)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select * " +
                              "from NonesCRMusers " +
                              "where CompanyName='" + company.Replace("'", "''") + "' and UserName='" + UserName.Replace("'", "''") + "'";

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    public static object checkUserName(string UserName)
    {
        object objResult;
        string strQuery = "";
        strQuery += "select count(*) from NonesCRMusers where UserName ='" + UserName.Replace("'", "''") + "'";
        SqlConnection sqlConn = new SqlConnection(strConn);
        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        try
        {
            //============================================
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //================================================
            objResult = sqlCmd.ExecuteScalar();
        }
        catch
        {
            objResult = null;
        }
        finally
        {
            //===========================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            //=========================================
        }
        return objResult;
    }

    public static int changePassword(string ClientId, string UserName, string pwd)
    {
        int intResult = 0;
        SqlConnection sqlConn = new SqlConnection(strConn);

        try
        {
            string sqlQuery = "Update NonesCRMusers Set [Password]='" + pwd.Replace("'", "''") +
                              "' Where [ClientId]=" + ClientId.Replace("'", "''") + " and [UserName]='" + UserName.Replace("'", "''") + "'";
            SqlCommand sqlCmd = new SqlCommand(sqlQuery, sqlConn);
            //============================================
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //================================================

            intResult = sqlCmd.ExecuteNonQuery();
        }
        catch { intResult = 0; }
        finally
        {
            //===========================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            //=========================================


        }
        return intResult;
    }


    public static DataSet getUserNamePassword(string ClientId, string UserName, bool isEmail)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);

            string strQuery = "";

            if (isEmail == false)
            {
                strQuery = "Select UserName, Password, Email " +
                                   "from NonesCRMusers " +
                                   "where ClientId=" + ClientId.ToString() + " and UserName='" + UserName.Replace("'", "''") + "'";
            }
            else
            {
                strQuery = "Select UserName, Password, Email " +
                                   "from NonesCRMusers " +
                                   "where ClientId=" + ClientId.ToString() + " and Email='" + UserName.Replace("'", "''") + "'";
            }
            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    public static DataSet getAdminClientRequest(string strFilter, string strOrderBy)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select * " +
                              "from ClientRequest " + strFilter + " " + strOrderBy;

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    public static object updateAdminClientRequest(int RequestId, string ClientId, string UserName, string status, string priority, string WebsiteURL, string ShrtDescr, string Comment)
    {
        object objResult;
        string strQuery = "";

        if (Comment != "")
        {
            strQuery = " Update ClientRequest ";
            strQuery += " set WebsiteURL='" + WebsiteURL.Replace("'", "''") + "', ";
            strQuery += "ShortDescr='" + ShrtDescr.Replace("'", "''") + "', ";
            strQuery += "Status='" + status + "', ";
            strQuery += "Priority=" + priority + ", ";
            strQuery += "LastUpdatedBy='Admin', ";
            strQuery += "LastUpdatedOn=getdate() ";
            strQuery += "where RequestId=" + RequestId.ToString() + "; ";
            strQuery += " insert into ClientRequest_Details(RequestId,Comment,PostedBy,PostedOn) ";
            strQuery += " values(" + RequestId.ToString() + ",";
            strQuery += "'" + Comment.Replace("'", "''") + "',";
            strQuery += "'Admin',";
            strQuery += "getdate())";
        }
        else
        {
            strQuery = " Update ClientRequest ";
            strQuery += " set WebsiteURL='" + WebsiteURL.Replace("'", "''") + "', ";
            strQuery += "ShortDescr='" + ShrtDescr.Replace("'", "''") + "', ";
            strQuery += "Status='" + status + "', ";
            strQuery += "Priority=" + priority + ", ";
            strQuery += "LastUpdatedBy='Admin', ";
            strQuery += "LastUpdatedOn=getdate() ";
            strQuery += "where RequestId=" + RequestId.ToString() + "; ";
        }

        SqlConnection sqlConn = new SqlConnection(strConn);
        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        try
        {
            //================================================
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //================================================



            objResult = sqlCmd.ExecuteNonQuery();
        }
        catch
        {
            objResult = "0";
        }
        finally
        {
            //===========================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            //=========================================
        }
        return objResult;
    }

    public static int deleteComment(string RequestId, string CommentId)
    {
        int intResult = 0;
        SqlConnection sqlConn = new SqlConnection(strConn);

        try
        {
            string sqlQuery = "delete from  ClientRequest_Details Where [RequestId]=" + RequestId + " and [CommentId]=" + CommentId;
            SqlCommand sqlCmd = new SqlCommand(sqlQuery, sqlConn);
            //================================================
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //================================================
            intResult = sqlCmd.ExecuteNonQuery();
        }
        catch { intResult = 0; }
        finally
        {
            //===========================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            //=========================================


        }
        return intResult;
    }

    public static object deleteClientRequest(int RequestId)
    {
        object objResult = "0";
        SqlConnection sqlConn = new SqlConnection(strConn);

        try
        {
            string sqlQuery = "delete from  ClientRequest_Details Where [RequestId]=" + RequestId + "; ";
            sqlQuery += "delete from  ClientRequest Where [RequestId]=" + RequestId;
            SqlCommand sqlCmd = new SqlCommand(sqlQuery, sqlConn);
            //================================================
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //================================================

            objResult = sqlCmd.ExecuteNonQuery();
        }
        catch { objResult = "0"; }
        finally
        {
            //===========================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            //=========================================
        }
        return objResult;
    }


    public static DataSet getCompanyNameCR()
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            //////string strQuery = "Select distinct WebsiteURL " +
            //////                  "from ClientRequest where ClientId = " + ClientId + " and UserName='" + UserName.Replace("'", "''") + "' order by WebsiteURL";
            ////string strQuery = "Select distinct CompanyName " +
            ////                "from NonesCRMusers order by CompanyName";

            string strQuery = @"select distinct Company_Name
		from Company
		where active = 'Y' order by Company_Name;";

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    public static DataSet getWebsiteURL()
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select distinct WebsiteURL " +
                              "from ClientRequest order by WebsiteURL";

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    public static DataSet getRequestPostedBy()
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select distinct UserName, ClientId ,  cast(ClientId as varchar) +'+'+ UserName   as Cid " +
                              "from ClientRequest order by UserName";

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);

        }
        catch
        {
        }
        finally { }
        return ds;
    }
    public static DataSet getWebsiteURL(string ClientId, string UserName)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select distinct WebsiteURL " +
                              "from ClientRequest where ClientId = " + ClientId + " and UserName='" + UserName.Replace("'", "''") + "' order by WebsiteURL";
            //////string strQuery = "Select *,(select CompanyName from [NonesCRMusers] where  NonesCRMusers.UserName = ClientRequest.UserName ) as companyname " +
            //////                "from ClientRequest where ClientId = " + ClientId + " and UserName='" + UserName.Replace("'", "''") + "' order by WebsiteURL";

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }


    public static DataSet getCompanyName(string ClientId, string UserName)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            //////string strQuery = "Select distinct WebsiteURL " +
            //////                  "from ClientRequest where ClientId = " + ClientId + " and UserName='" + UserName.Replace("'", "''") + "' order by WebsiteURL";
            string strQuery = "Select distinct CompanyName " +
                            "from NonesCRMusers where ClientId = " + ClientId + " and UserName='" + UserName.Replace("'", "''") + "' order by CompanyName";

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);

            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }
    public static DataSet getEmpsName()
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);

            string strQuery = "Select distinct assigned_to_user from  tasks a join  hours_reporting b ON a.task_id = b.task_id";

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }
    public static object checkWebsiteURL(string ClientId, string UserName, string WebsiteURL)
    {
        object objResult;
        string strQuery = "";
        strQuery += "select RequestId from ClientRequest where ClientId = " + ClientId + " and UserName='" + UserName.Replace("'", "''") + "' and WebsiteURL='" + WebsiteURL.Replace("'", "''") + "' and status <> 'closed'";
        SqlConnection sqlConn = new SqlConnection(strConn);
        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        try
        {
            //================================================
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //================================================

            objResult = sqlCmd.ExecuteScalar();
        }
        catch
        {
            objResult = null;
        }
        finally
        {
            //===========================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            //=========================================

        }
        return objResult;
    }

    public static object insertAttachment(int RequestId, string Pkey, string FileName, string Description, string UploadedBy)
    {
        object objResult;
        SqlConnection sqlConn = new SqlConnection(strConn);

        string strQuery = " insert into ClientRequest_Attachment(RequestId,Pkey,FileName,Description,UploadedBy,UploadedOn) ";
        strQuery += " values(" + RequestId + ",";
        strQuery += "'" + Pkey + "',";
        strQuery += "'" + FileName.Replace("'", "''") + "',";
        strQuery += "'" + Description.Replace("'", "''") + "',";
        strQuery += "'" + UploadedBy.Replace("'", "''") + "',";
        strQuery += "getdate()); ";



        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        //================================================
        if (sqlConn.State == ConnectionState.Closed)
        {
            sqlConn.Open();
        }
        //================================================

        objResult = sqlCmd.ExecuteNonQuery();
        try
        {
            sqlConn.Close();
            sqlConn.Open();
            string sqlQuery = "Select Aid from  ClientRequest_Attachment Where RequestId =" + RequestId + " and Pkey = '" + Pkey + "' and FileName ='" + FileName.Replace("'", "''") + "'";

            SqlCommand sqlCmd2 = new SqlCommand(sqlQuery, sqlConn);
            /// sqlConn.Open();

            objResult = sqlCmd2.ExecuteScalar();

        }
        catch
        {
            objResult = "0";
        }
        finally
        {
            //===========================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            //=========================================

        }
        return objResult;
    }

    public static object insertTaskAttachment(int task_id, string Pkey, string FileName, string Description, string UploadedBy)
    {
        object objResult;
        int atchment = 0;
        SqlConnection sqlConn = new SqlConnection(strConn);

        string strQuery = " insert into task_Attachments(task_id,Pkey,FileName,Description,UploadedBy,UploadedOn) ";
        strQuery += " values(" + task_id + ",";
        strQuery += "'" + Pkey + "',";
        strQuery += "'" + FileName.Replace("'", "''") + "',";
        strQuery += "'" + Description.Replace("'", "''") + "',";
        strQuery += "'" + UploadedBy.Replace("'", "''") + "',";
        strQuery += "getdate()); ";



        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        //================================================
        if (sqlConn.State == ConnectionState.Closed)
        {
            sqlConn.Open(); ;
        }
        //================================================
        try
        {
            objResult = sqlCmd.ExecuteNonQuery();

        }
        catch (Exception ex)
        {
            //DatabaseHelper.SendEmailList("avinashj@123SiteSolutions.com", "Error:In first function ESGDESK when status is " + "DateTime : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:" + ex.Message + "\n" + " Source:" + ex.Source + "\n" + " InnerException:" + ex.InnerException + " Stack Trace: " + ex.StackTrace + "\n");
        }
        try
        {
            //================================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            //================================================
            //================================================
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //================================================

            string sqlQuery = "Select Aid from  task_attachments Where task_id =" + task_id + " and Pkey = '" + Pkey + "' and FileName ='" + FileName.Replace("'", "''") + "'";

            SqlCommand sqlCmd2 = new SqlCommand(sqlQuery, sqlConn);
            /// sqlConn.Open();

            objResult = sqlCmd2.ExecuteScalar();

        }
        catch (Exception ex)
        {
            objResult = "0";
            //DatabaseHelper.SendEmailList("avinashj@123SiteSolutions.com", "Error:In Second function ESGDESK when status is " + "DateTime : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:" + ex.Message + "\n" + " Source:" + ex.Source + "\n" + " InnerException:" + ex.InnerException + " Stack Trace: " + ex.StackTrace + "\n");
        }
        finally
        {
            //================================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            //================================================

        }

        return objResult;
    }

    public static DataSet getTaskAttachments(int task_id, string strOrderBy)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select * , pkey +'_'+ FileName as FileName1 " +
                              "from task_attachments " +
                              "where task_id=" + task_id.ToString() + " " + strOrderBy;

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    public static int deleteTaskAttachment(string Aid)
    {
        int intResult = 0;
        SqlConnection sqlConn = new SqlConnection(strConn);

        try
        {
            string sqlQuery = "delete from  task_attachments Where [Aid]=" + Aid;
            SqlCommand sqlCmd = new SqlCommand(sqlQuery, sqlConn);

            //============================================
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //================================================



            intResult = sqlCmd.ExecuteNonQuery();
        }
        catch { intResult = 0; }
        finally
        {
            //===========================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            //=========================================

        }
        return intResult;
    }


    public static DataSet getTaskAllfilename(string FileAid)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select * , pkey +'_'+ FileName as FileName1 " +
                              "from task_attachments " +
                              "where Aid=" + FileAid + " ";

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }
    public static DataSet getAllfilename(string FileAid)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select * , pkey +'_'+ FileName as FileName1 " +
                              "from ClientRequest_Attachment " +
                              "where Aid=" + FileAid + " ";

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    public static DataSet getAttachments(int RequestId, string strOrderBy)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select * , pkey +'_'+ FileName as FileName1 " +
                              "from ClientRequest_Attachment " +
                              "where RequestId=" + RequestId.ToString() + " " + strOrderBy;

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    public static int deleteAttachment(string Aid)
    {
        int intResult = 0;
        SqlConnection sqlConn = new SqlConnection(strConn);

        try
        {
            string sqlQuery = "delete from  ClientRequest_Attachment Where [Aid]=" + Aid;
            SqlCommand sqlCmd = new SqlCommand(sqlQuery, sqlConn);
            //============================================
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //================================================


            intResult = sqlCmd.ExecuteNonQuery();
        }
        catch { intResult = 0; }
        finally
        {
            //===========================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            //=========================================
        }
        return intResult;
    }

    public static object updateInternalReference(int RequestId, string Internalref)
    {
        DataSet dsRequests = getClientRequest(RequestId);
        string strIRN = "";
        if (dsRequests == null || dsRequests.Tables.Count <= 0 || dsRequests.Tables[0].Rows.Count <= 0)
        {

        }
        else
        {
            strIRN = dsRequests.Tables[0].Rows[0]["Internalref"].ToString();

            if (strIRN == "")
            {
                strIRN = Internalref;
            }
            else
            {
                if (!strIRN.Contains(Internalref))
                {
                    strIRN += "," + Internalref;
                }
            }
        }

        object objResult;
        string strQuery = "";

        strQuery = " Update ClientRequest ";
        strQuery += " set Internalref='" + strIRN + "' ";
        strQuery += "where RequestId=" + RequestId.ToString() + "; ";

        SqlConnection sqlConn = new SqlConnection(strConn);
        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        try
        {
            //============================================
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //================================================
            objResult = sqlCmd.ExecuteNonQuery();
        }
        catch
        {
            objResult = "0";
        }
        finally
        {


            //===========================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            //=========================================




        }
        return objResult;
    }

    public static object removeInternalReference(int RequestId, string Internalref)
    {
        DataSet dsRequests = getClientRequest(RequestId);
        string strIRN = "";
        if (dsRequests == null || dsRequests.Tables.Count <= 0 || dsRequests.Tables[0].Rows.Count <= 0)
        {

        }
        else
        {
            strIRN = dsRequests.Tables[0].Rows[0]["Internalref"].ToString();

            if (strIRN.Contains(Internalref))
            {
                strIRN = strIRN.Replace(Internalref, "");
                strIRN = strIRN.Replace(",,", ",");
            }
        }

        object objResult;
        string strQuery = "";

        strQuery = " Update ClientRequest ";
        strQuery += " set Internalref='" + strIRN + "' ";
        strQuery += "where RequestId=" + RequestId.ToString() + "; ";

        SqlConnection sqlConn = new SqlConnection(strConn);
        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        try
        {
            //============================================
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //============================================

            objResult = sqlCmd.ExecuteNonQuery();
        }
        catch
        {
            objResult = "0";
        }
        finally
        {

            //===========================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            //=========================================


        }
        return objResult;
    }

    public static int executeNonQuery(string strSql)
    {
        int objResult;

        SqlConnection sqlConn = new SqlConnection(strConn);
        SqlCommand sqlCmd = new SqlCommand(strSql, sqlConn);
        try
        {
            //============================================
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //=============================================


            objResult = sqlCmd.ExecuteNonQuery();
        }
        catch(Exception ex)
        {
            objResult = 0;
            DatabaseHelper.HandleException(ex, "Error in executeQuery", "");
            //DatabaseHelper.SendEmailList("avinashj@123SiteSolutions.com", "Error:In Query", " Input Parameters :" + "executeNonQuery Query:->" + strSql.ToString());
        }
        finally
        {

            //===========================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            //=========================================


        }
        return objResult;
    }

    public static DataSet getDataset(string strSql)
    {

        DataSet vds = new DataSet();
           try
           {
        //using (SqlConnection conn = new SqlConnection(strConn))
        //{
        //    conn.Open();
        //    using (SqlDataAdapter da = new SqlDataAdapter(strSql, conn))
        //    {
               SqlConnection conn=new SqlConnection(strConn);
               SqlDataAdapter da = new SqlDataAdapter(strSql, conn);

                da.Fill(vds);
              

               // conn.Close(); // redundant, but just to be clear

                //return ds;


            //}
        //}
           }
           catch { }

        return vds;

    }

    public static object getclientid(string ClientId, string UserName, string Password)
    {
        object objResult;
        //string strQuery = "Select count(*) from NonesCRMusers where ClientId=" + ClientId + " or UserName='" + UserName.Replace("'", "''") + "' and Password='" + Password.Replace("'", "''") + "'";
        string strQuery = "Select Regid from NonesCRMusers where ClientId=" + ClientId + " and UserName='" + UserName.Replace("'", "''") + "' and Password='" + Password.Replace("'", "''") + "'";
        SqlConnection sqlConn = new SqlConnection(strConn);
        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
       
            //=============================================
            //sqlConn.Close();
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //==============================================
            objResult = sqlCmd.ExecuteScalar();
       
            //=============================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();

            }
            //============================================= 
        
        return objResult;
    }



    #region stored procedure methods for EODC1




    public static DataSet getTaskdataset(string projectName)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "select cast(task_id as Varchar) as task_id from tasks where deleted <>'1' and project ='" + projectName + "' order by tasks.task_id";

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
            DataRow nrow = ds.Tables[0].NewRow();
            nrow["task_id"] = "Select";
            ds.Tables[0].Rows.InsertAt(nrow, 0);

        }
        catch
        {
        }
        finally { }
        return ds;
    }
    public static string getAllProjectDs(ref DropDownList drppro0)
    {
        DataSet ds = new DataSet();
        string Retval;
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "select cast(project_id as Varchar) as project_id,project_name from projects order by project_name";

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
            DataRow nrow = ds.Tables[0].NewRow();
            nrow["project_name"] = "Select";
            nrow["project_id"] = "Select";
            ds.Tables[0].Rows.InsertAt(nrow, 0);
            drppro0.DataSource = ds.Tables[0];
            drppro0.DataTextField = "project_name";
            drppro0.DataValueField = "project_name";
            drppro0.DataBind();
        }
        catch
        {
        }
        finally { }
        Retval = "Done";
        return Retval;
    }




    public static DataSet getProjectDs()
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "select cast(project_id as Varchar) as project_id,project_name from projects order by project_name";

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
            DataRow nrow = ds.Tables[0].NewRow();
            nrow["project_name"] = "Select";
            nrow["project_id"] = "Select";
            ds.Tables[0].Rows.InsertAt(nrow, 0);
            //drppro0.DataSource = ds.Tables[0];
            //drppro0.DataTextField = "project_name";
            //drppro0.DataValueField = "project_name";
            //drppro0.DataBind();
        }
        catch
        {
        }
        finally { }
        return ds;
    }



    public static DataSet getProjectDataset()
    {
        DataSet ds = new DataSet();
        try
        {

            SqlConnection Conn = new SqlConnection(strConn);
            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.Connection = Conn;
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.CommandText = "fetchProjectname";

            SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
            da.Fill(ds);
        }
        catch (Exception ex)
        {

        }
        finally { }
        return ds;

    }

    public static DataSet getTaskIdDataset()
    {
        DataSet ds = new DataSet();
        try
        {

            SqlConnection Conn = new SqlConnection(strConn);
            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.Connection = Conn;
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.CommandText = "fetchtaskId";

            SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
            da.Fill(ds);
        }
        catch (Exception ex)
        {

        }
        finally { }
        return ds;

    }



    public static DataSet getDynamicTaskIdDs(string sqlParameter)
    {
        DataSet ds = new DataSet();
        try
        {

            SqlConnection Conn = new SqlConnection(strConn);
            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.Connection = Conn;
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.CommandText = "fetchF0taskId";

            SqlParameter sqlParameter1 = new SqlParameter("@project0", SqlDbType.NVarChar, Int32.MaxValue);
            sqlParameter1.Value = sqlParameter;

            sqlCmd.Parameters.Add(sqlParameter1);
            SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
            da.Fill(ds);
        }
        catch (Exception ex)
        {

        }
        finally { }
        return ds;

    }




    public static DataSet getDetailDataset(string sqlparameter)
    {
        DataSet ds = new DataSet();
        try
        {

            SqlConnection Conn = new SqlConnection(strConn);
            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.Connection = Conn;
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.CommandText = "fetchdetail0";

            SqlParameter sqlParameter1 = new SqlParameter("@task0", SqlDbType.NVarChar, Int32.MaxValue);
            sqlParameter1.Value = sqlparameter;

            sqlCmd.Parameters.Add(sqlParameter1);
            SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
            da.Fill(ds);
        }
        catch (Exception ex)
        {

        }
        finally { }
        return ds;

    }


    public static DataSet getsp_clientrequest(string sqlparameter)
    {
        DataSet ds = new DataSet();
        try
        {

            SqlConnection Conn = new SqlConnection(strConn);
            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.Connection = Conn;
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.CommandText = "fetchclientRequest";

            SqlParameter sqlParameter1 = new SqlParameter("@sql", SqlDbType.NVarChar, Int32.MaxValue);
            sqlParameter1.Value = '"' + sqlparameter + '"';

            sqlCmd.Parameters.Add(sqlParameter1);
            SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
            da.Fill(ds);
        }
        catch (Exception ex)
        {

        }
        finally { }
        return ds;

    }

    #endregion



    public static object executeScalar(string strSql)
    {
        object objResult = null;

        SqlConnection sqlConn = new SqlConnection(strConn);
        SqlCommand sqlCmd = new SqlCommand(strSql, sqlConn);
        try
        {
            //============================================
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //================================================



            objResult = sqlCmd.ExecuteScalar();
        }
        catch
        {
            objResult = 0;
            //DatabaseHelper.SendEmailList("avinashj@123SiteSolutions.com", "Error:In Query", " Input Parameters :" + "executeScalar Query:->" + strSql.ToString());
        }
        finally
        {
            //===========================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            //=========================================

        }
        return objResult;
    }

    public static bool validate_email(string s)
    {
        Regex reEmail = new Regex("^([a-zA-Z0-9_\\-\\'\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$");
        return reEmail.IsMatch(s);
    }

    public static bool isAdmin(string username)
    {
        bool flag = false;

        SqlConnection sqlConn = new SqlConnection(strConn);
        SqlCommand sqlCmd = new SqlCommand("select admin from users where username = '" + username.Replace("'", "''") + "'", sqlConn);
        try
        {
            //============================================
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //=============================================



            object objResult = sqlCmd.ExecuteScalar();

            if (objResult.ToString() == "Y")
            {
                flag = true;
            }
            else
            {
                flag = false;
            }
        }
        catch
        {
            flag = false;
        }
        finally
        {
            //===========================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            //============================================


        }

        
        return flag;
    }
    
    public static DataSet getcompanyname(string strSql)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlDataAdapter sqladp = new SqlDataAdapter(strSql, strConn);
            sqladp.Fill(ds);


        }
        catch
        {

        }
        finally
        {

        }
        return ds;
    }

    public static bool can_Delete_Tasks(string username)
    {
        bool flag = false;

        SqlConnection sqlConn = new SqlConnection(strConn);
        SqlCommand sqlCmd = new SqlCommand("select can_delete_tasks from users where username = '" + username.Replace("'", "''") + "'", sqlConn);
        try
        {
            //============================================
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //==============================================


            object objResult = sqlCmd.ExecuteScalar();

            if (objResult.ToString() == "Y")
            {
                flag = true;
            }
            else
            {
                flag = false;
            }
        }
        catch
        {
            flag = false;
        }
        finally
        {
            //===========================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            //============================================

        }
        return flag;
    }

    public static bool can_Delete_Comments_Attachments(string username)
    {
        bool flag = false;

        SqlConnection sqlConn = new SqlConnection(strConn);
        SqlCommand sqlCmd = new SqlCommand("select Can_edit_delete_comments_attachments from users where username = '" + username.Replace("'", "''") + "'", sqlConn);
        try
        {
            //============================================
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //================================================

            object objResult = sqlCmd.ExecuteScalar();

            if (objResult.ToString() == "Y")
            {
                flag = true;
            }
            else
            {
                flag = false;
            }
        }
        catch
        {
            flag = false;
        }
        finally
        {
            //==============================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            //============================================
        }
        return flag;
    }

    public static bool can_Show_Closed_Tasks(string username)
    {
        bool flag = false;

        SqlConnection sqlConn = new SqlConnection(strConn);
        SqlCommand sqlCmd = new SqlCommand("select show_closed_tasks from users where username = '" + username.Replace("'", "''") + "'", sqlConn);
        try
        {
            //============================================
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //============================================


            object objResult = sqlCmd.ExecuteScalar();

            if (objResult.ToString() == "Y")
            {
                flag = true;
            }
            else
            {
                flag = false;
            }
        }
        catch
        {
            flag = false;
        }
        finally
        {

            //===========================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            //============================================



        }
        return flag;
    }

    public static bool can_Show_Deleted_Tasks(string username)
    {
        bool flag = false;

        SqlConnection sqlConn = new SqlConnection(strConn);
        SqlCommand sqlCmd = new SqlCommand("select show_deleted_tasks from users where username = '" + username.Replace("'", "''") + "'", sqlConn);
        try
        {

            //============================================
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //================================================

            object objResult = sqlCmd.ExecuteScalar();

            if (objResult.ToString() == "Y")
            {
                flag = true;
            }
            else
            {
                flag = false;
            }
        }
        catch
        {
            flag = false;
        }
        finally
        {
            //============================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            //============================================



        }
        return flag;
    }


    public static bool can_Show_Client_Requests(string username)
    {
        bool flag = false;

        SqlConnection sqlConn = new SqlConnection(strConn);
        SqlCommand sqlCmd = new SqlCommand("select show_client_requests from users where username = '" + username.Replace("'", "''") + "'", sqlConn);
        try
        {
            //============================================
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //================================================
            object objResult = sqlCmd.ExecuteScalar();
            if (objResult.ToString() == "Y")
            {
                flag = true;
            }
            else
            {
                flag = false;
            }
        }
        catch
        {
            flag = false;
        }
        finally
        {
            //===========================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            //============================================



        }
        return flag;
    }

    public static bool is_int(string maybe_int)
    {
        try
        {
            int i = Int32.Parse(maybe_int);
            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }

    public static DataSet getCompany()
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConnECRM);
            string strQuery = "select distinct companyname from userdetails ";
            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }
    public static DataSet getCompanyAll()
    {
        DataSet ds = new DataSet();
        try
        {
            //distinct top(20)
            SqlConnection Conn = new SqlConnection(strConnECRM);
            string strQuery = "select distinct companyname,clientid from userdetails ";
            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }
    public static DataSet getCompanyClientid(string strcompany)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConnECRM);
            string strQuery = "select  distinct companyname,clientid from userdetails where companyname='" + strcompany + "'";
            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }
    public static DataSet bindCompanydetails(string clientid, string strtop)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConnECRM);
            string[] Split;
            string tick = clientid;
            Split = tick.Split(',');
            if (strtop != "")
            {
                strtop = "Top (" + strtop + ")";
            }
            else
            {
                strtop = "";
            }
            for (int j = 0; j < Split.Length; j++)
            {
                clientid = Split[j].ToString();

                string strQuery = "SELECT " + strtop + "  PropertyActivityLog.ClientId,PropertyActivityLog.UserName, PropertyActivityLog.DateTime, PropertyActivityLog.Price, PropertyActivityLog.PricePer,";
                strQuery = strQuery + " PropertyActivityLog.Descreption, PropertyLocation.LocationId,";
                strQuery = strQuery + " PropertyLocation.Address  , PropertyLocation.AreaOrRegionName  as region,PropertyLocation.CityOrTownName as town,PropertyLocation.PostCode, Property.DealType, ";
                strQuery = strQuery + " PropertyActivityLog.ClientId AS clientid, PropertyActivityLog.eventtype, PropertyActivityLog.PropertyId,UserDetails.CompanyName ";
                strQuery = strQuery + "  FROM Property INNER JOIN ";
                strQuery = strQuery + "  PropertyActivityLog ON Property.ClientId = PropertyActivityLog.ClientId AND Property.PropertyId = PropertyActivityLog.PropertyId INNER JOIN ";
                strQuery = strQuery + " PropertyLocation ON Property.LocationId = PropertyLocation.LocationId AND Property.ClientId = PropertyLocation.ClientId ";
                strQuery = strQuery + " INNER JOIN UserDetails ON PropertyActivityLog.ClientId = UserDetails.ClientId AND PropertyActivityLog.UserName = UserDetails.UserName ";
                strQuery = strQuery + " WHERE (PropertyActivityLog.eventtype = 'New') and PropertyActivityLog.ClientId in (" + clientid + ") order by PropertyActivityLog.datetime desc;";


                string strQuery2 = "SELECT " + strtop + " PropertyActivityLog.ClientId,PropertyActivityLog.UserName, PropertyActivityLog.DateTime, PropertyActivityLog.Price, PropertyActivityLog.PricePer, ";
                strQuery2 = strQuery2 + " PropertyActivityLog.Descreption, PropertyLocation.LocationId, ";
                strQuery2 = strQuery2 + " PropertyLocation.Address  , PropertyLocation.AreaOrRegionName  as region,PropertyLocation.CityOrTownName as town,PropertyLocation.PostCode, Property.DealType, ";
                strQuery2 = strQuery2 + " PropertyActivityLog.ClientId AS clientid, PropertyActivityLog.eventtype, PropertyActivityLog.PropertyId,UserDetails.CompanyName ";
                strQuery2 = strQuery2 + " FROM Property INNER JOIN";
                strQuery2 = strQuery2 + " PropertyActivityLog ON Property.ClientId = PropertyActivityLog.ClientId AND Property.PropertyId = PropertyActivityLog.PropertyId INNER JOIN";
                strQuery2 = strQuery2 + " PropertyLocation ON Property.LocationId = PropertyLocation.LocationId AND Property.ClientId = PropertyLocation.ClientId";
                strQuery2 = strQuery2 + " INNER JOIN UserDetails ON PropertyActivityLog.ClientId = UserDetails.ClientId AND PropertyActivityLog.UserName = UserDetails.UserName ";
                strQuery2 = strQuery2 + "  WHERE (PropertyActivityLog.eventtype = 'Updated') and PropertyActivityLog.ClientId in (" + clientid + ") order by PropertyActivityLog.datetime desc";


                string strQuery3 = "SELECT  " + strtop + " PropertyActivityLog.ClientId,PropertyActivityLog.UserName, PropertyActivityLog.DateTime, PropertyActivityLog.Price, PropertyActivityLog.PricePer, ";
                strQuery3 = strQuery3 + " PropertyActivityLog.Descreption,PropertyActivityLog.PropertyId,UserDetails.CompanyName, ";
                strQuery3 = strQuery3 + " PropertyActivityLog.ClientId AS clientid, PropertyActivityLog.eventtype FROM PropertyActivityLog ";
                strQuery3 = strQuery3 + " INNER JOIN UserDetails ON PropertyActivityLog.ClientId = UserDetails.ClientId AND PropertyActivityLog.UserName = UserDetails.UserName ";
                strQuery3 = strQuery3 + " WHERE     (PropertyActivityLog.eventtype = 'Deleted') and PropertyActivityLog.ClientId in (" + clientid + ") order by PropertyActivityLog.datetime desc";
                SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
                SqlDataAdapter da2 = new SqlDataAdapter(strQuery2, Conn);
                SqlDataAdapter da3 = new SqlDataAdapter(strQuery3, Conn);
                da.Fill(ds);
                da2.Fill(ds);
                da3.Fill(ds);
            }
        }
        catch
        {
        }
        finally { }
        return ds;
    }
    public static DataSet bindDeleted(string clientid)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConnECRM);
            string strQuery = @"SELECT TOP (10) PropertyActivityLog.UserName, PropertyActivityLog.DateTime, PropertyActivityLog.Price, PropertyActivityLog.PricePer, 
                                    PropertyActivityLog.Descreption,PropertyActivityLog.PropertyId,                      
                                    PropertyActivityLog.ClientId AS clientid, PropertyActivityLog.eventtype
                                    FROM PropertyActivityLog 
                                    WHERE (PropertyActivityLog.eventtype = 'Deleted') and PropertyActivityLog.ClientId in (" + clientid + ") order by PropertyActivityLog.datetime,PropertyActivityLog.EventType desc";
            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }
    public static DataSet bindSale(string propertyid, string clientid)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConnECRM);
            string strQuery = "select * from PropertySale where PropertyId='" + propertyid + "' and clientid in (" + clientid + ") ";
            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }
    public static DataSet bindRent(string propertyid, string clientid)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConnECRM);
            string strQuery = "select * from PropertyLet where PropertyId='" + propertyid + "' and clientid in (" + clientid + ") ";
            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    public static DataSet getprocDataset(string sql, string admin, string orderby)
    {
        DataSet ds = new DataSet();
        try
        {

            SqlConnection Conn = new SqlConnection(strConn);
            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.Connection = Conn;
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.CommandText = "proc_task";

            SqlParameter sqlParameter1 = new SqlParameter("@filter", SqlDbType.NVarChar, Int32.MaxValue);
            sqlParameter1.Value = sql;
            SqlParameter sqlParameter2 = new SqlParameter("@admin", SqlDbType.NVarChar, Int32.MaxValue);
            sqlParameter2.Value = admin;
            SqlParameter sqlParameter3 = new SqlParameter("@orderby", SqlDbType.NVarChar, Int32.MaxValue);
            sqlParameter3.Value = orderby;
            sqlCmd.Parameters.Add(sqlParameter1);
            sqlCmd.Parameters.Add(sqlParameter2);
            sqlCmd.Parameters.Add(sqlParameter3);
            SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
            da.Fill(ds);
        }
        catch (Exception ex)
        {

        }
        finally { }
        return ds;

    }
    public static DataSet getsp_questionDataset(string sql)
    {
        DataSet ds = new DataSet();
        try
        {

            SqlConnection Conn = new SqlConnection(strConn);
            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.Connection = Conn;
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.CommandText = "SP_question";

            SqlParameter sqlParameter1 = new SqlParameter("@filter", SqlDbType.NVarChar, Int32.MaxValue);
            sqlParameter1.Value = sql;
            sqlCmd.Parameters.Add(sqlParameter1);
            SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
            da.Fill(ds);
        }
        catch (Exception ex)
        {

        }
        finally { }
        return ds;

    }

    public static DataSet getUnreadTaskbyAdmin()
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select Task_id,short_desc,project,status,priority,assigned_to_user from tasks where task_id in ";
            strQuery += "(select Distinct task_comments.task_id  from task_comments inner join ";
            strQuery += "tasks on tasks.task_id=task_comments.task_id ";
            strQuery += "where tc_id not in(select tc_id from read_comments where username ='M') ";
            strQuery += "and task_comments.deleted <> 1 ";
            strQuery += "and tasks.status <> 'closed' and tasks.status <> 'Checked' and tasks.status <>'parked' ";
            strQuery += "and tasks.deleted <>1 ";
            strQuery += ")order by project";

            //string strQuery ="select Distinct task_comments.task_id  from task_comments inner join "; 
            //      strQuery +="tasks on tasks.task_id=task_comments.task_id ";
            //      strQuery +="where tc_id not in(select tc_id from read_comments where username ='M') ";
            //      strQuery +="and task_comments.deleted <> 1 "; 
            //      strQuery +="and tasks.status <> 'closed' and tasks.status <> 'Checked' and tasks.status <>'parked' ";
            //      strQuery +="and tasks.deleted <>1 ";



            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
            string i = ds.Tables[0].Rows.Count.ToString();
        }
        catch (Exception ex)
        {
        }
        finally { }
        return ds;
    }


    #region smita----------------
    public static DataSet getUnreadTaskbyAdmin1(string strCondition, string pagination)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);

            string strQuery = @"select top 20 * from (
            Select ROW_NUMBER() OVER (ORDER BY project Asc) AS RowNumber, Task_id,short_desc,project,
            status,priority,assigned_to_user from tasks 
            where task_id in (select Distinct task_comments.task_id  from task_comments 
            inner join tasks on tasks.task_id=task_comments.task_id where tc_id 
            not in(select tc_id from read_comments where username ='M') and 
            task_comments.deleted <> 1 and tasks.status <> 'closed' and tasks.status <> 'Checked' 
            and tasks.status <>'parked' and tasks.deleted <>1 " + strCondition + "))_myresult where RowNumber > " + pagination;

            //string strQuery = "Select Task_id,short_desc,project,status,priority,assigned_to_user from tasks where task_id in ";
            //strQuery += "(select Distinct task_comments.task_id  from task_comments inner join ";
            //strQuery += "tasks on tasks.task_id=task_comments.task_id ";
            //strQuery += "where tc_id not in(select tc_id from read_comments where username ='M') ";
            //strQuery += "and task_comments.deleted <> 1 ";
            //strQuery += "and tasks.status <> 'closed' and tasks.status <> 'Checked' and tasks.status <>'parked' ";
            //strQuery += "and tasks.deleted <>1 " + strCondition + "";
            //strQuery += ")order by project";

            //string strQuery ="select Distinct task_comments.task_id  from task_comments inner join "; 
            //      strQuery +="tasks on tasks.task_id=task_comments.task_id ";
            //      strQuery +="where tc_id not in(select tc_id from read_comments where username ='M') ";
            //      strQuery +="and task_comments.deleted <> 1 "; 
            //      strQuery +="and tasks.status <> 'closed' and tasks.status <> 'Checked' and tasks.status <>'parked' ";
            //      strQuery +="and tasks.deleted <>1 ";



            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
            string i = ds.Tables[0].Rows.Count.ToString();
        }
        catch (Exception ex)
        {
        }
        finally
        {
        }
        return ds;
    }

    public static int getCountUnreadTaskbyAdmin1(string strCondition)
    {
        DataSet ds = new DataSet();
        int i = 0;
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);

            string strQuery = "Select count(*) as totcount from tasks where task_id in ";
            strQuery += "(select Distinct task_comments.task_id  from task_comments inner join ";
            strQuery += "tasks on tasks.task_id=task_comments.task_id ";
            strQuery += "where tc_id not in(select tc_id from read_comments where username ='M') ";
            strQuery += "and task_comments.deleted <> 1 ";
            strQuery += "and tasks.status <> 'closed' and tasks.status <> 'Checked' and tasks.status <>'parked' ";
            strQuery += "and tasks.deleted <>1 " + strCondition + "";
            strQuery += ")";

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);


            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    i = Convert.ToInt16(ds.Tables[0].Rows[0]["totcount"].ToString());
                }
            }
        }
        catch (Exception ex)
        {
        }
        finally
        {
        }
        return i;
    }
    #endregion




    public static object insertNews(string sqlQuery)
    {
        object objResult = "0";
        SqlConnection sqlConn = new SqlConnection(strConn);

        try
        {
            SqlCommand sqlCmd = new SqlCommand(sqlQuery, sqlConn);
            //============================================
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //================================================

            objResult = sqlCmd.ExecuteNonQuery();
        }
        catch { objResult = "0"; }
        finally
        {
            //============================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            //=========================================== 



        }
        return objResult;
    }

    public static bool sendEmailLMS(string strTo, string strSubject, string strBody)
    {

        //******************************LMS Mail Format*****************************************************
        bool blndone = false;
        try
        {
            MailMessage mMailMessage = new MailMessage();
            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
            msg.From = new MailAddress(ConfigurationManager.AppSettings["FHSHR"]);

            string[] strToEmails = strTo.Split(';');
            for (int i = 0; i < strToEmails.Length; i++)
            {
                msg.To.Add(new MailAddress(strToEmails[i].ToString().Trim()));
            }


            msg.Subject = strSubject;
            msg.Body = strBody;
            msg.IsBodyHtml = true;
            msg.Priority = MailPriority.Normal;
            //System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("mail.estatesolutions.co.uk");
            //mSmtpClient.UseDefaultCredentials = false;
            //System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("test@estatesolutions.co.uk", "testtest");

            System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("mail.internationalhorizons.co.uk");
            mSmtpClient.UseDefaultCredentials = false;
            System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("support@internationalhorizons.co.uk", "support1!");
            mSmtpClient.Credentials = loginInfo;
            mSmtpClient.Send(msg);

            blndone = true;
        }
        catch { blndone = false; }
        return blndone;
        //************************************************************************************

    }

    public static bool sendEmailEsgGallery(string strTo, string strSubject, string strBody)
    {

        //******************************LMS Mail Format*****************************************************
        //rohit
    //    return true;
        bool blndone = false;
        try
        {
            MailMessage mMailMessage = new MailMessage();
            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
            msg.From = new MailAddress("EsgGallery@esgdesk.com");

            string[] strToEmails = strTo.Split(';');
            for (int i = 0; i < strToEmails.Length; i++)
            {
                msg.To.Add(new MailAddress(strToEmails[i].ToString().Trim()));
            }


            msg.Subject = strSubject;
            msg.Body = strBody;
            msg.IsBodyHtml = true;
            msg.Priority = MailPriority.Normal;
            //System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("mail.estatesolutions.co.uk");
            //mSmtpClient.UseDefaultCredentials = false;
            //System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("test@estatesolutions.co.uk", "testtest");

            System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("mail.internationalhorizons.co.uk");
            mSmtpClient.UseDefaultCredentials = false;
            System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("support@internationalhorizons.co.uk", "support1!");
            mSmtpClient.Credentials = loginInfo;
            mSmtpClient.Send(msg);

            blndone = true;
        }
        catch { blndone = false; }
        return blndone;
        //************************************************************************************

    }



    public static bool sendEmailLMSReport(string strTo, string strSubject, string strBody)
    {

        //******************************LMS Mail Format*****************************************************
        //rohit
      //  return true;
        bool blndone = false;
        try
        {
            MailMessage mMailMessage = new MailMessage();
            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
            //msg.From = new MailAddress(ConfigurationManager.AppSettings["FHSHR"]);
            msg.From = new MailAddress("LMS@esolutionsgroup.co.uk");//("LMS@estatesolutionsgroup.co.uk");
            string[] strToEmails = strTo.Split(';');
            for (int i = 0; i < strToEmails.Length; i++)
            {
                msg.To.Add(new MailAddress(strToEmails[i].ToString().Trim()));
            }
            msg.Subject = strSubject;
            msg.Body = strBody;
            msg.IsBodyHtml = true;
            msg.Priority = MailPriority.Normal;




            System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("213.171.196.58");//("mail.esolutionsgroup.co.uk");
            mSmtpClient.UseDefaultCredentials = false;
            System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("LMS@esolutionsgroup.co.uk", "Z0ngo1&2");
            mSmtpClient.Credentials = loginInfo;
            mSmtpClient.Send(msg);
            blndone = true;



            //System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("mail.estatesolutions.co.uk");        
            //mSmtpClient.UseDefaultCredentials = false;
            //System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("test@estatesolutions.co.uk", "aoNq74%6"); //("test@estatesolutions.co.uk", "testtest");

            //System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("mail.internationalhorizons.co.uk");
            //mSmtpClient.UseDefaultCredentials = false;
            //System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("support@internationalhorizons.co.uk", "support1!");



            //System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient();
            //mSmtpClient.Host = "smtp.gmail.com";
            //System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("sushamasuman31@gmail.com", "birendra1234");           
            //mSmtpClient.EnableSsl = true;         

            
            //mSmtpClient.Credentials = loginInfo;
            //mSmtpClient.Send(msg);

            blndone = true;
        }
        catch { blndone = false; }
        
        //************************************************************************************



        //bool blndone = false;
        try
        {
            MailMessage mMailMessage = new MailMessage();
            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
            msg.From = new MailAddress("LMS@esolutionsgroup.co.uk");

            msg.To.Add(new MailAddress("sushamaa@esolutionsgroup.co.uk"));
            msg.Subject = strSubject;
            msg.Body = strBody;
            msg.IsBodyHtml = true;
            msg.Priority = MailPriority.Normal;
            System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("mail.estatesolutions.co.uk");
            mSmtpClient.UseDefaultCredentials = false;
            System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("test@estatesolutions.co.uk", "aoNq74%6");
            mSmtpClient.Credentials = loginInfo;
            mSmtpClient.Send(msg);
            blndone = true;
        }
        catch { blndone = false; }
        return blndone;





    }

    public static string SearchStringtaskId(string sqlQuery)
    {
        string strTaskid = "";
        SqlConnection sqlConn = new SqlConnection(strConn);
        try
        {
            SqlCommand cmd = new SqlCommand(sqlQuery, sqlConn);
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            SqlDataReader r = cmd.ExecuteReader();


            string LastValue = null;
            while (r.Read())
            {
                if (strTaskid == "")
                {
                    strTaskid = r["task_id"].ToString();
                    LastValue = r["task_id"].ToString();
                }
                else
                {
                    if (string.Compare(LastValue, r["task_id"].ToString()) == -1)
                    {
                        LastValue = r["task_id"].ToString();
                        strTaskid += "," + LastValue.ToString();
                    }
                    else
                    {
                        LastValue = r["task_id"].ToString();
                    }

                }
            }
        }
        catch (Exception ex)
        { }
        finally
        {
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }


        }
        return strTaskid;

    }
    public static string SearchStringrequestid(string sqlQuery)
    {
        string strRequestId = "";
        SqlConnection sqlConn = new SqlConnection(strConn);

        try
        {
            SqlCommand cmd = new SqlCommand(sqlQuery, sqlConn);
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            SqlDataReader r = cmd.ExecuteReader();


            string LastValue = null;
            while (r.Read())
            {
                if (strRequestId == "")
                {
                    strRequestId = r["RequestId"].ToString();
                    LastValue = r["RequestId"].ToString();
                }
                else
                {
                    if (string.Compare(LastValue, r["RequestId"].ToString()) == -1)
                    {
                        LastValue = r["RequestId"].ToString();
                        strRequestId += "," + LastValue.ToString();
                    }
                    else
                    {
                        LastValue = r["RequestId"].ToString();
                    }

                }
            }
        }
        catch (Exception ex)
        {
            //DatabaseHelper.SendEmailList("avinashj@123SiteSolutions.com", "Error:In Query", " Input Parameters :" + " Query:->" + sqlQuery.ToString());

        }
        finally
        {
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }

        }
        return strRequestId;

    }

    public static DataSet getDatasetDuplicate(string strSql)
    {

        DataSet ds = new DataSet();

        using (SqlConnection conn = new SqlConnection(strConn))
        {
            using (SqlDataAdapter da = new SqlDataAdapter(strSql, conn))
            {


                try
                {
                    da.Fill(ds);
                }
                catch (Exception ex)
                {
                    //DatabaseHelper.SendEmailList("avinashj@123SiteSolutions.com", "Error:In Query", " Input Parameters :" + " Query:->" + strSql.ToString());

                }

                conn.Close();

            }
        }
        //   }
        //   catch { }

        return ds;

    }
    public static object checkProjectId(string strQuery)
    {
        object objResult;
        SqlConnection sqlConn = new SqlConnection(strConn);
        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        try
        {
            //============================================
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //================================================
            objResult = sqlCmd.ExecuteScalar();
        }
        catch
        {
            objResult = null;
        }
        finally
        {
            //===========================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            //=========================================
        }
        return objResult;
    }


    public static bool CanApprove(string username)
    {
        bool flag = false;

        SqlConnection sqlConn = new SqlConnection(strConn);
        SqlCommand sqlCmd = new SqlCommand("select CanApprove from users where username = '" + username.Replace("'", "''") + "'", sqlConn);
        try
        {
            //============================================
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //=============================================



            object objResult = sqlCmd.ExecuteScalar();

            if (objResult.ToString() == "Y")
            {
                flag = true;
            }
            else
            {
                flag = false;
            }
        }
        catch
        {
            flag = false;
        }
        finally
        {
            //===========================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            //============================================


        }
        return flag;
    }





    public static bool sendMultiEmail(string strTo, string strSubject, string strBody, string filepath)
    {

        bool blndone = false;
        try
        {


            MailMessage mMailMessage = new MailMessage();
            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
            msg.From = new MailAddress(ConfigurationManager.AppSettings["FromEmail2"]);

            string[] strToEmails = strTo.Split(';');
            for (int i = 0; i < strToEmails.Length; i++)
            {
                msg.To.Add(new MailAddress(strToEmails[i].ToString().Trim()));
            }

            //string aa="abhisheksingh2320@hotmail.com;vashish17@live.in";
            //msg.To.Add(aa);

            msg.Subject = strSubject;
            msg.Body = strBody;
            msg.IsBodyHtml = true;
            // msg.Priority = MailPriority.Normal;
            msg.Priority = MailPriority.High;
            if (filepath != "")
            {
                msg.Attachments.Add(new Attachment(filepath));
            }

            //System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("mail.internationalhorizons.co.uk");
            //mSmtpClient.UseDefaultCredentials = false;
            //// mSmtpClient.UseDefaultCredentials = true;
            //System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("support@internationalhorizons.co.uk", "support1!");
            System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("213.171.222.69");
            mSmtpClient.UseDefaultCredentials = false;
            //System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("test@estatesolutions.co.uk", "aoNq74%6");
            System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("LMS@esolutionsgroup.co.uk", "Z0ngo1&2");

            mSmtpClient.Credentials = loginInfo;
            mSmtpClient.Send(msg);

            blndone = true;
        }
        catch(Exception ex) { blndone = false; }
        return blndone;
        //************************************************************************************



    }

    public static DataSet getallstatus(string strSql)
    {

        DataSet ds = new DataSet();
        using (SqlConnection conn = new SqlConnection(strConn))
        {
            using (SqlDataAdapter da = new SqlDataAdapter(strSql, conn))
            {


                da.Fill(ds);
                conn.Close();

            }
        }

        return ds;

    }

    public static int executeSQLquery(string strSql)
    {
        int objResult = 0;

        SqlConnection sqlConn = new SqlConnection(strConn);
        SqlCommand sqlCmd = new SqlCommand(strSql, sqlConn);
        try
        {
            //============================================
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //================================================



            objResult = sqlCmd.ExecuteNonQuery();
        }
        catch
        {
            objResult = 0;
            //DatabaseHelper.SendEmailList("avinashj@123SiteSolutions.com", "Error:In Query", " Input Parameters :" + "executeScalar Query:->" + strSql.ToString());
        }
        finally
        {
            //===========================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            //=========================================

        }
        return objResult;
    }

    public static int checkuser(string strSql)
    {
        int result = 0;
        try
        {
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(strConn);

            SqlDataAdapter da = new SqlDataAdapter(strSql, conn);



            da.Fill(ds);
            if (ds != null)
            {
                if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                {
                    result = 1;
                }
            }
        }
        catch
        { }

        return result;

    }

    public static string fnSetImgURL(string strUserName)
    {
        string strEmpImgURL = "EmpImages/NoImage.png";
        try
        {
            string strSql = "select EmpImgURL from users where username='" + strUserName + "'";
            DataSet ds = getDataset(strSql);
         
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (ds.Tables[0].Rows[0]["EmpImgURL"].ToString() != "")
                        strEmpImgURL = ds.Tables[0].Rows[0]["EmpImgURL"].ToString();
                    else
                        strEmpImgURL = "EmpImages/NoImage.png";
                }
                if (strEmpImgURL.Trim() == "")
                {
                    strEmpImgURL = "EmpImages/NoImage.png";
                }
            }
        }
        catch
        {
            strEmpImgURL = "EmpImages/NoImage.png";
        }
        return strEmpImgURL;
    }


    public static bool isPrivateSolutions(string username)
    {
        bool flag = false;

        SqlConnection sqlConn = new SqlConnection(strConn);
        //SqlCommand sqlCmd = new SqlCommand("Select Private_Solutions from users where username = '" + username.Replace("'", "''") + "' and admin='Y' ", sqlConn);
        SqlCommand sqlCmd = new SqlCommand("Select Private_Solutions from users where username = '" + username.Replace("'", "''") + "'", sqlConn);
        try
        {

            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }

            object objResult = sqlCmd.ExecuteScalar();

            if (objResult.ToString() == "True")
            {
                flag = true;
            }
            else
            {
                flag = false;
            }
        }
        catch
        {
            flag = false;
        }
        finally
        {

            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }

        }
        return flag;
    }

    public static bool isPrivatecategory(string cid)
    {
        bool flag = false;

        SqlConnection sqlConn = new SqlConnection(strConn);
        SqlCommand sqlCmd = new SqlCommand("Select VisibleTo from Category where Category_id = '" + cid.Replace("'", "''") + "' ", sqlConn);
        try
        {

            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }

            object objResult = sqlCmd.ExecuteScalar();

            if (objResult.ToString() == "Private")
            {
                flag = true;
            }
            else
            {
                flag = false;
            }
        }
        catch
        {
            flag = false;
        }
        finally
        {

            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }

        }
        return flag;
    }

    #region Handle exception and Send Email

    public static void HandleException(Exception ex, string strSubject, string strQuery)
    {
        String strError = "Error Message: " + ex.Message + "<br><br>";
        if (ex.InnerException != null)
        {
            strError += "InnerException: " + Convert.ToString(ex.InnerException) + "<br><br>";
        }
        strError += "Stack Trace: " + ex.StackTrace + "<br><br>";
        strError += "source: " + ex.Source + "<br><br>";

        if (strQuery != null && strQuery != "")
        {
            strError += "\r\n <br> Query: " + strQuery;
        }

        //DatabaseHelper.SendEmailList(strExceptionEmail, strSubject, strError);
    }
    #endregion


    public static int InsertDefaultprojectcategory(string ProjectName)
    {
        DataSet ds = new DataSet();
        int result = 0;

        SqlConnection myConnection = new SqlConnection(strConn);

        try
        {
            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.Connection = myConnection;
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.CommandText = "Defaultprojectcategory";

            SqlParameter sqlParameter1 = new SqlParameter("@projectname", SqlDbType.NVarChar, Int32.Parse("1000"));
            sqlParameter1.Value = ProjectName;

            sqlCmd.Parameters.Add(sqlParameter1);
            if (myConnection.State == ConnectionState.Closed)
            {
                myConnection.Open();
            }
            result = sqlCmd.ExecuteNonQuery();
            //SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
            //da.Fill(ds);
        }
        catch (Exception ex)
        {

        }
        finally
        {
            if (myConnection.State == ConnectionState.Open)
            {
                myConnection.Close();
            }
        }
        return result;

    }



    public static bool sendEmailRequestForPassword(string strTo, string strSubject, string strBody)
    {


        //******************************New Mail Format*****************************************************
        bool blndone = false;
        try
        {
            MailMessage mMailMessage = new MailMessage();
            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
            msg.From = new MailAddress(ConfigurationManager.AppSettings["NoRplyFromEmail1"]);

            string[] strToEmails = strTo.Split(';');
            for (int i = 0; i < strToEmails.Length; i++)
            {
                msg.To.Add(new MailAddress(strToEmails[i].ToString().Trim()));
            }

            
            //msg.Bcc.Add(new MailAddress("avinashj@esolutionsgroup.co.uk"));
            msg.Bcc.Add(new MailAddress("michaelr@esolutionsgroup.co.uk"));
            msg.Bcc.Add(new MailAddress("michaelr@estatesolutions.co.uk"));
            msg.Bcc.Add(new MailAddress("mail@mjrsite.com"));
            msg.Bcc.Add(new MailAddress("rohitd@esolutionsgroup.co.uk"));

            //michaelr@estatesolutions.co.uk and michaelr@esolutionsgroup.co.uk
            //mail@mjrsite.com

            //msg.Bcc.Add("krishnap@esolutionsgroup.co.uk");
            //msg.Bcc.Add("ashishv@esolutionsgroup.co.uk");
            //================================================
            //msg.Bcc.Add("rohitd@esolutionsgroup.co.uk");
            //msg.Bcc.Add("michaelr@esolutionsgroup.co.uk");
            //================================================
            
            
            

            msg.Subject = strSubject;
            msg.Body = strBody;
            msg.IsBodyHtml = true;
            msg.Priority = MailPriority.Normal;

            //System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("mail.estatesolutions.co.uk");
            //mSmtpClient.UseDefaultCredentials = false;
            //System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("test@estatesolutions.co.uk", "testtest");

            System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("mail.internationalhorizons.co.uk");
            mSmtpClient.UseDefaultCredentials = false;
            System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("support@internationalhorizons.co.uk", "support1!");

            mSmtpClient.Credentials = loginInfo;
            mSmtpClient.Send(msg);

            blndone = true;
        }
        catch { blndone = false; }
        return blndone;
        //************************************************************************************
    }




    //public static void insertNoteAttachment(string vProject_Name, int vFolder_Id, int vNote_Id, string vPkey, string vFileName, string vDescription, string vUploadedBy)
    //{
    //    object objResult;

    //    SqlConnection sqlConn = new SqlConnection(strConn);

    //    string strQuery = " insert into Note_Attachments(Project_Name,Folder_Id,Note_Id,Pkey,FileName,Description,Uploaded_by,UploadedOn) values('" + vProject_Name + "','" + vFolder_Id + "','" + vNote_Id + "','" + vPkey + "', '" + vFileName + "', '" + vDescription + "', '" + vUploadedBy + "',Getdate()) ";

    //    SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
    //    //================================================
    //    if (sqlConn.State == ConnectionState.Closed)
    //    {
    //        sqlConn.Open();
    //    }
    //    //================================================
    //    try
    //    {
    //        objResult = sqlCmd.ExecuteNonQuery();

    //    }
    //    catch (Exception ex)
    //    {

    //    }
    //}
    //public static DataSet getNoteAttachments(string vProject_Name, int Folder_id, int Note_Id)
    //{
    //    DataSet ds = new DataSet();
    //    try
    //    {
    //        SqlConnection Conn = new SqlConnection(strConn);
    //        string strQuery = "Select * , pkey +'_'+ FileName as FileName1 from Note_Attachments where Project_Name='" + vProject_Name.ToString() + "' and Folder_Id='" + Folder_id + "' and Note_Id='" + Note_Id + "'";
    //        SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
    //        da.Fill(ds);
    //    }
    //    catch
    //    {
    //    }
    //    finally { }
    //    return ds;
    //}
    //public static DataSet getNoteAllfilename(int Uploadid)
    //{
    //    DataSet ds = new DataSet();
    //    try
    //    {
    //        SqlConnection Conn = new SqlConnection(strConn);
    //        string strQuery = "Select * , pkey +'_'+ FileName as FileName1 from Note_Attachments where UploadedId='" + Uploadid + " '";

    //        SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
    //        da.Fill(ds);
    //    }
    //    catch
    //    {
    //    }
    //    finally { }
    //    return ds;
    //}
    //public static int deleteNoteAttachment(int DelId)
    //{
    //    int intResult = 0;
    //    SqlConnection sqlConn = new SqlConnection(strConn);

    //    try
    //    {
    //        string sqlQuery = "delete from  Note_Attachments Where [UploadedId]=" + DelId;
    //        SqlCommand sqlCmd = new SqlCommand(sqlQuery, sqlConn);

    //        //============================================
    //        if (sqlConn.State == ConnectionState.Closed)
    //        {
    //            sqlConn.Open();
    //        }
    //        //================================================



    //        intResult = sqlCmd.ExecuteNonQuery();
    //    }
    //    catch { intResult = 0; }
    //    finally
    //    {
    //        //===========================================
    //        if (sqlConn.State == ConnectionState.Open)
    //        {
    //            sqlConn.Close();
    //        }
    //        //=========================================

    //    }
    //    return intResult;
    //}


    //================================Admin section for uploading the Admin =====================================================
    public static void insertNoteAttachment(string vProject_Name, int vFolder_Id, int vNote_Id, string vPkey, string vFileName, string vDescription, string vUploadedBy)
    {
        object objResult;

        SqlConnection sqlConn = new SqlConnection(strConn);

        string strQuery = "insert into Note_Attachments(Project_Name,Folder_Id,Note_Id,Pkey,FileName,Description,Uploaded_by,UploadedOn,IsProjectAttachment) values('" + vProject_Name + "','" + vFolder_Id + "','" + vNote_Id + "','" + vPkey + "', '" + vFileName + "', '" + vDescription + "', '" + vUploadedBy + "',Getdate(),'True') ";

        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        //================================================
        if (sqlConn.State == ConnectionState.Closed)
        {
            sqlConn.Open();
        }
        //================================================
        try
        {
            objResult = sqlCmd.ExecuteNonQuery();

        }
        catch (Exception ex)
        {

        }
    }
    public static DataSet getNoteAttachments(string vProject_Name, int Folder_id, int Note_Id)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select * , pkey +'_'+ FileName as FileName1 from Note_Attachments where Project_Name='" + vProject_Name.ToString() + "' and Folder_Id='" + Folder_id + "' and Note_Id='" + Note_Id + "'and IsProjectAttachment='True'";
            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }
    public static DataSet getNoteAllfilename(int Uploadid)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select * , pkey +'_'+ FileName as FileName1 from Note_Attachments where IsProjectAttachment='True' and UploadedId='" + Uploadid + " '";

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }
    public static int deleteNoteAttachment(int DelId)
    {
        int intResult = 0;
        SqlConnection sqlConn = new SqlConnection(strConn);

        try
        {
            string sqlQuery = "delete from Note_Attachments Where IsProjectAttachment='True' and [UploadedId]=" + DelId;
            SqlCommand sqlCmd = new SqlCommand(sqlQuery, sqlConn);

            //============================================
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //================================================



            intResult = sqlCmd.ExecuteNonQuery();
        }
        catch { intResult = 0; }
        finally
        {
            //===========================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            //=========================================

        }
        return intResult;
     }
    //===============================CR section==================================================================================
    public static void insertNoteAttachmentC(string vProject_Name, int vFolder_Id, int vNote_Id, string vPkey, string vFileName, string vDescription, string vUploadedBy)
    {
        object objResult;

        SqlConnection sqlConn = new SqlConnection(strConn);

        string strQuery = "insert into Note_Attachments(Project_Name,Folder_Id,Note_Id,Pkey,FileName,Description,Uploaded_by,UploadedOn,IsProjectAttachment) values('" + vProject_Name + "','" + vFolder_Id + "','" + vNote_Id + "','" + vPkey + "', '" + vFileName + "', '" + vDescription + "', 'Support',Getdate(),'False') ";

        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        //================================================
        if (sqlConn.State == ConnectionState.Closed)
        {
            sqlConn.Open();
        }
        //================================================
        try
        {
            objResult = sqlCmd.ExecuteNonQuery();

        }
        catch (Exception ex)
        {

        }
    }
    public static DataSet getNoteAttachmentsC(string vProject_Name, int Folder_id, int Note_Id)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select * , pkey +'_'+ FileName as FileName1 from Note_Attachments where Project_Name='" + vProject_Name.ToString() + "' and Folder_Id='" + Folder_id + "' and Note_Id='" + Note_Id + "'and IsProjectAttachment='False'";
            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }
    public static DataSet getNoteAllfilenameC(int Uploadid)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select * , pkey +'_'+ FileName as FileName1 from Note_Attachments where IsProjectAttachment='False' and UploadedId='" + Uploadid + " '";

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }
    public static int deleteNoteAttachmentC(int DelId)
    {
        int intResult = 0;
        SqlConnection sqlConn = new SqlConnection(strConn);

        try
        {
            string sqlQuery = "delete from Note_Attachments Where IsProjectAttachment='False' and [UploadedId]=" + DelId;
            SqlCommand sqlCmd = new SqlCommand(sqlQuery, sqlConn);

            //============================================
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //================================================



            intResult = sqlCmd.ExecuteNonQuery();
        }
        catch { intResult = 0; }
        finally
        {
            //===========================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            //=========================================

        }
        return intResult;
    }
    //===============================ADMIN section for set the Client Notes =====================================================
    public static void insertNoteAttachmentA(string vProject_Name, int vFolder_Id, int vNote_Id, string vPkey, string vFileName, string vDescription, string vUploadedBy)
    {
        object objResult;

        SqlConnection sqlConn = new SqlConnection(strConn);

        string strQuery = "insert into Note_Attachments(Project_Name,Folder_Id,Note_Id,Pkey,FileName,Description,Uploaded_by,UploadedOn,IsProjectAttachment) values('" + vProject_Name + "','" + vFolder_Id + "','" + vNote_Id + "','" + vPkey + "', '" + vFileName + "', '" + vDescription + "', 'Support',Getdate(),'False') ";

        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        //================================================
        if (sqlConn.State == ConnectionState.Closed)
        {
            sqlConn.Open();
        }
        //================================================
        try
        {
            objResult = sqlCmd.ExecuteNonQuery();

        }
        catch (Exception ex)
        {

        }
    }
    public static DataSet getNoteAttachmentsA(string vProject_Name, int Folder_id, int Note_Id)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select * , pkey +'_'+ FileName as FileName1 from Note_Attachments where Project_Name='" + vProject_Name.ToString() + "' and Folder_Id='" + Folder_id + "' and Note_Id='" + Note_Id + "'and IsProjectAttachment='False'";
            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }
    public static DataSet getNoteAllfilenameA(int Uploadid)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select * , pkey +'_'+ FileName as FileName1 from Note_Attachments where IsProjectAttachment='False' and UploadedId='" + Uploadid + " '";

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }
    public static int deleteNoteAttachmentA(int DelId)
    {
        int intResult = 0;
        SqlConnection sqlConn = new SqlConnection(strConn);

        try
        {
            string sqlQuery = "delete from Note_Attachments Where IsProjectAttachment='False' and [UploadedId]=" + DelId;
            SqlCommand sqlCmd = new SqlCommand(sqlQuery, sqlConn);

            //============================================
            if (sqlConn.State == ConnectionState.Closed)
            {
                sqlConn.Open();
            }
            //================================================



            intResult = sqlCmd.ExecuteNonQuery();
        }
        catch { intResult = 0; }
        finally
        {
            //===========================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            //=========================================

        }
        return intResult;
    }
    //============================================================================================================================
    public static DataSet getusptaskDetail(string filter, string Username, string NoOfRecord)
    {
        DataSet ds = new DataSet();
        try
        {

            SqlConnection Conn = new SqlConnection(strConn);
            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.Connection = Conn;
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.CommandText = "uspGetTaskDetails";

            SqlParameter sqlParameter1 = new SqlParameter("@filter", SqlDbType.NVarChar, Int32.MaxValue);
            sqlParameter1.Value = filter;
            sqlCmd.Parameters.Add(sqlParameter1);
            SqlParameter Sqlparameter2 = new SqlParameter("@username", SqlDbType.VarChar, Int32.MaxValue);
            Sqlparameter2.Value = Username;
            sqlCmd.Parameters.Add(Sqlparameter2);

            SqlParameter Sqlparameter3 = new SqlParameter("@nofoRecord", SqlDbType.VarChar, Int32.MaxValue);
            Sqlparameter3.Value = NoOfRecord;
            sqlCmd.Parameters.Add(Sqlparameter3);

            SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
            da.Fill(ds);
        }
        catch (Exception ex)
        {

        }
        finally { }
        return ds;
    }


    public static DataSet NewgetusptaskDetailTest(string filter, string Username, string NoOfRecord)
    {
        DataSet ds = new DataSet();
        try
        {

            SqlConnection Conn = new SqlConnection(strConn);
            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.Connection = Conn;
            sqlCmd.CommandType = CommandType.StoredProcedure;
            //sqlCmd.CommandText = "NewuspGetTaskDetails";
            sqlCmd.CommandText = "NewuspGetTaskDetails";


            SqlParameter sqlParameter1 = new SqlParameter("@filter", SqlDbType.NVarChar, Int32.MaxValue);
            sqlParameter1.Value = filter;
            sqlCmd.Parameters.Add(sqlParameter1);
            SqlParameter Sqlparameter2 = new SqlParameter("@username", SqlDbType.VarChar, Int32.MaxValue);
            Sqlparameter2.Value = Username;
            sqlCmd.Parameters.Add(Sqlparameter2);

            SqlParameter Sqlparameter3 = new SqlParameter("@nofoRecord", SqlDbType.VarChar, Int32.MaxValue);
            Sqlparameter3.Value = NoOfRecord;
            sqlCmd.Parameters.Add(Sqlparameter3);

            SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
            da.Fill(ds);
        }
        catch (Exception ex)
        {

        }
        finally { }
        return ds;
    }


    public static DataSet NewgetusptaskDetail(string filter, string Username, string NoOfRecord)
    {
        DataSet ds = new DataSet();
        try
        {

            SqlConnection Conn = new SqlConnection(strConn);
            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.Connection = Conn;
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.CommandText = "NewuspGetTaskDetails";

            SqlParameter sqlParameter1 = new SqlParameter("@filter", SqlDbType.NVarChar, Int32.MaxValue);
            sqlParameter1.Value = filter;
            sqlCmd.Parameters.Add(sqlParameter1);
            SqlParameter Sqlparameter2 = new SqlParameter("@username", SqlDbType.VarChar, Int32.MaxValue);
            Sqlparameter2.Value = Username;
            sqlCmd.Parameters.Add(Sqlparameter2);

            SqlParameter Sqlparameter3 = new SqlParameter("@nofoRecord", SqlDbType.VarChar, Int32.MaxValue);
            Sqlparameter3.Value = NoOfRecord;
            sqlCmd.Parameters.Add(Sqlparameter3);

            SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
            da.Fill(ds);
        }
        catch (Exception ex)
        {

        }
        finally { }
        return ds;
    }

    public static DataSet NewgetusptaskDetailChanged(string filter, string Username, string NoOfRecord)
    {
        DataSet ds = new DataSet();
        try
        {

            SqlConnection Conn = new SqlConnection(strConn);
            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.Connection = Conn;
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.CommandText = "NewuspGetTaskDetailsChanged";

            SqlParameter sqlParameter1 = new SqlParameter("@filter", SqlDbType.NVarChar, Int32.MaxValue);
            sqlParameter1.Value = filter;
            sqlCmd.Parameters.Add(sqlParameter1);
            SqlParameter Sqlparameter2 = new SqlParameter("@username", SqlDbType.VarChar, Int32.MaxValue);
            Sqlparameter2.Value = Username;
            sqlCmd.Parameters.Add(Sqlparameter2);

            SqlParameter Sqlparameter3 = new SqlParameter("@nofoRecord", SqlDbType.VarChar, Int32.MaxValue);
            Sqlparameter3.Value = NoOfRecord;
            sqlCmd.Parameters.Add(Sqlparameter3);

            SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
            da.Fill(ds);
        }
        catch (Exception ex)
        {

        }
        finally { }
        return ds;
    }
    public static DataSet NewgetuspRequestDetail(string filter, string NoOfRecord)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.Connection = Conn;
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.CommandText = "uspGetClient_Request_Details";

            SqlParameter sqlParameter1 = new SqlParameter("@filter", SqlDbType.VarChar, Int32.MaxValue);
            sqlParameter1.Value = filter;
            sqlCmd.Parameters.Add(sqlParameter1);

            SqlParameter Sqlparameter2 = new SqlParameter("@nofoRecord", SqlDbType.VarChar, Int32.MaxValue);
            Sqlparameter2.Value = NoOfRecord;
            sqlCmd.Parameters.Add(Sqlparameter2);

            SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
            da.Fill(ds);
        }
        catch (Exception ex)
        {

        }
        finally { }
        return ds;
    }

    public static object Employee_Previous_Company(int vUser_id, string vCompanyname, string vLocation, string vDurationfrom, string vDurationto, string Reasonforleaving)
    {
        object objResult = 0;
        int atchment = 0;
        SqlConnection sqlConn = new SqlConnection(strConn);

        string strQuery = " insert into Employee_Previous_Company(User_id,NameofOrganization,Location,DurationFrom,DurationTo,ReasonforLeaving) ";
        strQuery += " values(" + vUser_id + ",";
        strQuery += "'" + vCompanyname.Replace("'", "''") + "',";
        strQuery += "'" + vLocation + "',";
        strQuery += "'" + vDurationfrom + "',";
        strQuery += "'" + vDurationto + "',";
        strQuery += "'" + Reasonforleaving.Replace("'", "''") + "')";

        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        //================================================
        if (sqlConn.State == ConnectionState.Closed)
        {
            sqlConn.Open(); ;
        }
        //================================================
        try
        {
            objResult = sqlCmd.ExecuteNonQuery();

        }
        catch (Exception ex)
        {

        }

        finally
        {
            //================================================
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            //================================================

        }

        return objResult;
    }




    #region faq , glossary search section---------------- 

    public static DataSet GetRecords(string SearchString)
    {
      
        DataSet ds = new DataSet();
        try
        {
            string[] item = SearchString.Split(new char[] { ' ', ',', ';' });

            string sqlQuery = "select question, answer from FAQ where ";

            for (int i = 0; i <= item.Length - 1; i++)
            {
                if (item[i] != "")
                {
                    sqlQuery += "(Lower(question) like '%" + item[i].Replace("'", "''") + "%' OR Upper(answer) like '%" + item[i].Replace("'", "''") + "%') OR ";
                }
            }

            if (sqlQuery.EndsWith(" OR "))
            {
                sqlQuery = sqlQuery.TrimEnd(new char[] { 'O', 'R', ' ' });
            }

            SqlDataAdapter da = new SqlDataAdapter(sqlQuery, strConn);
            da.Fill(ds);
        }
        catch { }
        finally { }
        return ds;
    }
    public static DataSet GetAllRecords()
    {
        DataSet ds = new DataSet();
        try
        {
            string sqlQuery = "select question, answer from FAQ";
            SqlDataAdapter da = new SqlDataAdapter(sqlQuery, strConn);
            da.Fill(ds);
        }
        catch { }
        finally { }
        return ds;
    }

    public static DataSet GetGlossaryRecords(string SearchString)
    {
     
        DataSet ds = new DataSet();
        try
        {
            string[] item = SearchString.Split(new char[] { ' ', ',', ';' });

            string sqlQuery = "select Word, Desc from Glossary where ";

            for (int i = 0; i <= item.Length - 1; i++)
            {
                if (item[i] != "")
                {
                    sqlQuery += "(Lower(Word) like '%" + item[i].Replace("'", "''") + "%' OR Upper([Desc]) like '%" + item[i].Replace("'", "''") + "%') OR ";
                }
            }

            if (sqlQuery.EndsWith(" OR "))
            {
                sqlQuery = sqlQuery.TrimEnd(new char[] { 'O', 'R', ' ' });
            }
            SqlDataAdapter da = new SqlDataAdapter(sqlQuery,strConn);
            da.Fill(ds);
        }
        catch { }
        finally { }
        return ds;
    }
    public static DataSet GetAllGlossaryRecords()
    {
        DataSet ds = new DataSet();
        try
        {
            string sqlQuery = "select * from Glossary";
            SqlDataAdapter da = new SqlDataAdapter(sqlQuery, strConn);
            da.Fill(ds);
        }
        catch { }
        finally { }
        return ds;
    }
    #endregion-----------------------


    #region Admin section-----------------------

    public static int insertFaq(string ques, string answ)
    {
        int intResult = 0;
        string sqlQuery = "";
        SqlConnection myConnection = new SqlConnection(strConn);
        try
        {
            sqlQuery = "Insert into FAQ([Question],[Answer]) "
                            + "values('" + ques.Trim().Replace("'", "|@") + "','" + answ.Trim().Replace("'","|@")
                            + "')";
            SqlCommand myCommand = new SqlCommand(sqlQuery,myConnection);
            myConnection.Open();
            intResult = myCommand.ExecuteNonQuery();


        }
        catch (Exception ex) { intResult = 0; HandleException(ex, "Error in website - Esgdesk : insertFaq() ", sqlQuery); }
        finally { myConnection.Close(); }
        return intResult;
    }

    public static int insertGlossary(string word, string desc)
    {
        int intResult = 0;
        string sqlQuery = "";
        SqlConnection myConnection = new SqlConnection(strConn);
        try
        {
            sqlQuery = "Insert into glossary([Word],[Desc]) "
                            + "values('" + word.Trim().Replace("'", "|@") + "','" + desc.Trim().Replace("'","|@")
                            + "')";
            SqlCommand myCommand = new SqlCommand(sqlQuery,myConnection);
            myConnection.Open();
            intResult = myCommand.ExecuteNonQuery();
        }
        catch (Exception ex) { intResult = 0; HandleException(ex, "Error in website - Esgdesk : insertGlossary() ", sqlQuery); }
        finally { myConnection.Close(); }
        return intResult;
    }
    #endregion----------------------------------

    
    /*------------------Rutuja - 5 July 2016---------------*/
    
    public static DataSet getUserData(string username)
    {
        
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.Connection = Conn;
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.CommandText = "sp_getUserData";

            SqlParameter sqlParameter1 = new SqlParameter("@username", SqlDbType.VarChar, Int32.MaxValue);
            sqlParameter1.Value = username;
            sqlCmd.Parameters.Add(sqlParameter1);

            SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
            da.Fill(ds);
           
        }
        catch (Exception ex)
        { }
        finally { }
        return ds;
    }
    public static DataSet getCompanyDetails()
    {

        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.Connection = Conn;
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.CommandText = "sp_getCompanyNotes";
            SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
            da.Fill(ds);

        }
        catch (Exception ex)
        { }
        finally { }
        return ds;
    }
    public static DataSet getDataForDropDowns()
    {

        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.Connection = Conn;
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.CommandText = "sp_loadDropDowns";
            SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
            da.Fill(ds);

        }
        catch (Exception ex)
        { }
        finally { }
        return ds;
    }

    /*-----------------rutuja - 11 July 2016---------------------*/
    public static DataSet getUserData(string username,string password,string indate,string option)
    {

        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.Connection = Conn;
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.CommandText = "sp_getAllUserData";

            SqlParameter sqlParameter1 = new SqlParameter("@username", SqlDbType.VarChar, Int32.MaxValue);
            sqlParameter1.Value = username;
            sqlCmd.Parameters.Add(sqlParameter1);

            SqlParameter sqlParameter2 = new SqlParameter("@password", SqlDbType.VarChar, Int32.MaxValue);
            sqlParameter2.Value = password;
            sqlCmd.Parameters.Add(sqlParameter2);

            SqlParameter sqlParameter3 = new SqlParameter("@indate", SqlDbType.VarChar, Int32.MaxValue);
            sqlParameter3.Value = indate;
            sqlCmd.Parameters.Add(sqlParameter3);

            SqlParameter sqlParameter4 = new SqlParameter("@option", SqlDbType.VarChar, Int32.MaxValue);
            sqlParameter4.Value = option;
            sqlCmd.Parameters.Add(sqlParameter4);

            SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
            da.Fill(ds);

        }
        catch (Exception ex)
        { }
        finally { }
        return ds;
    }
}
