using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Collections;
using System.Configuration;
using System.Collections.Generic;
using System.IO;
//using java.util.zip;
//using java.util;
//using java.io;
using System.Web;
using System.Web.UI;
using System.Net.Mail;
using System.Net;
using System.Net.NetworkInformation;
using System.Web.Configuration;
using System.Xml;

public class DatabaseHelperEmail
{

    public DatabaseHelperEmail()
    {

    }
    static string provider = "RSAProtectedConfigurationProvider";
    static string section = "connectionStrings";
    string BRANCH_ID = ConfigurationManager.AppSettings["BRANCH_ID"];
    string strConn = ConfigurationManager.ConnectionStrings["strConn"].ToString();
    string strExceptionEmail = ConfigurationManager.AppSettings["ExceptionEmailId"];

    public string ClientId = ConfigurationManager.AppSettings["ClientId"];
    public string saleLatestPropid = "";


    public System.Collections.ArrayList SalelstIds = new System.Collections.ArrayList();

    #region Methods needed for Encryption/Decryption of connection string
    public static void EncryptString()
    {
        Configuration confg = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
        ConfigurationSection configSect = confg.GetSection(section);
        if (configSect != null && !configSect.SectionInformation.IsProtected)
        {
            configSect.SectionInformation.ProtectSection(provider);
            confg.Save();
        }
    }

    public static void DecryptString()
    {
        Configuration config = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
        ConfigurationSection configSect = config.GetSection(section);
        if (configSect.SectionInformation.IsProtected)
        {
            configSect.SectionInformation.UnprotectSection();
            config.Save();
        }
    }

    #endregion

    // returns the connection string to the ms access database------------------------
    private string getConnStr(string strFrom)
    {
        string strPath = HttpContext.Current.Server.MapPath(strFrom) + "\\App_Data\\MembersDB.mdb";
        string strDBConn = "Provider=Microsoft.Jet.OLEDB.4.0;Password=\"\";User ID=Admin;Data Source=" + strPath;
        return strDBConn;
    }

    public DataSet GetPropertyDatasetPrint(string PropId, string strType)
    {
        SqlConnection Conn = new SqlConnection(strConn);
        string sql = "";
        if (strType == "2")
        {
            sql = @"SELECT Property.PropertyId, Property.FurtherNotes,Property.UserDefined6,Property.UserDefined7,Property.UserDefined8,Property.UserDefined9,Property.Reference, Property.DealType,
                       (Select SubValue from SubType Where MajorId=11 and SubId=Property.Status) as Status,
                       (Select SubValue from SubType Where MajorId=Property.PropertyMajorType and SubId=Property.PropertySubType) as proptype,
                        PropertyLet.PricePer,PropertyLet.Price As CostToOrder,PropertyLet.Price as Cost,
                        --(Select SubValue from SubType Where MajorId=53 and SubId=PropertySale.PriceQualifier) as PropPriceQualifier,
                       PropertyDescriptive.ShortDesc, PropertyDescriptive.LongDesc, PropertyLocation.CountryName, PropertyLocation.CityOrTownName,
                       PropertyLocation.Address, PropertyLocation.PostCode, PropertyLocation.LocationAKA, PropertyLocation.AreaOrRegionName,
                       PropertyLocation.Latitude, PropertyLocation.Longitude,(Select top 1 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId
                       and Files.Category like 'Main photo') As mainimage,
(Select top 1  Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId and Files.Category like 'Photo link' and Priority!=0 order by Priority asc) As LinkMainImage, 
                    (SELECT count(*) FROM PropertyRoom LEFT JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                    Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=1) as NumberOfBedrooms,
                    (SELECT count(*) FROM PropertyRoom LEFT JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                    Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=6) as NumberOfReceptions,
                    (SELECT count(*) FROM PropertyRoom LEFT JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                    Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=40) as NumberOfBathrooms,
                    (SELECT (Case when exists(select * from PropertyFeature where PropertyFeature.PropertyId = Property.PropertyId
                     and PropertyFeature.ClientId = Property.ClientId and PropertyFeature.FeatureDesc='furnished') then 'true' else 'false' end)) as furnished,
                    (SELECT (Case when exists(select * from PropertyFeature where PropertyFeature.PropertyId = Property.PropertyId
                     and PropertyFeature.ClientId = Property.ClientId and PropertyFeature.FeatureDesc='unfurnished') then 'true' else 'false' end)) as unfurnished,
                    (SELECT (Case when exists(select * from PropertyFeature where PropertyFeature.PropertyId = Property.PropertyId
                     and PropertyFeature.ClientId = Property.ClientId and PropertyFeature.FeatureDesc='furnished or unfurnished') then 'true' else 'false' end)) as furnorunfurn
                       FROM Property Left Join PropertyDescriptive ON (Property.PropertyId = PropertyDescriptive.PropertyId and Property.ClientId = PropertyDescriptive.ClientId)
                       Left JOIN PropertyLocation ON (Property.LocationId = PropertyLocation.LocationId and Property.ClientId = PropertyLocation.ClientId)
                    --Inner Join PropertySale ON (Property.PropertyId = PropertySale.PropertyId and Property.ClientId = PropertySale.ClientId)
                      Inner Join PropertyLet ON (Property.PropertyId = PropertyLet.PropertyId and Property.ClientId = PropertyLet.ClientId)
                       WHERE Property.ClientId=" + ClientId + " And Property.PropertyId='" + PropId + "'";
        }
        else
        {
            sql = @"SELECT Property.PropertyId, Property.FurtherNotes,Property.UserDefined6,Property.UserDefined7,Property.UserDefined8,Property.UserDefined9,Property.Reference, Property.DealType,
                       (Select SubValue from SubType Where MajorId=11 and SubId=Property.Status) as Status,
                       (Select SubValue from SubType Where MajorId=Property.PropertyMajorType and SubId=Property.PropertySubType) as proptype,
                       (Select SubValue from SubType Where MajorId=53 and SubId=PropertySale.PriceQualifier) as PropPriceQualifier,
                       PropertyDescriptive.ShortDesc, PropertyDescriptive.LongDesc, PropertyLocation.CountryName, PropertyLocation.CityOrTownName,
                       PropertyLocation.Address, PropertyLocation.PostCode, PropertyLocation.LocationAKA, PropertyLocation.AreaOrRegionName,
                       PropertyLocation.Latitude, PropertyLocation.Longitude,(Select top 1 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId
                       and Files.Category like 'Main photo') As mainimage,
(Select top 1  Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId and Files.Category like 'Photo link' and Priority!=0 order by Priority asc) As LinkMainImage, 
                    (SELECT count(*) FROM PropertyRoom LEFT JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                    Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=1) as NumberOfBedrooms,
                    (SELECT count(*) FROM PropertyRoom LEFT JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                    Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=6) as NumberOfReceptions,
                    (SELECT count(*) FROM PropertyRoom LEFT JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                    Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=40) as NumberOfBathrooms,
                    (SELECT (Case when exists(select * from PropertyFeature where PropertyFeature.PropertyId = Property.PropertyId
                     and PropertyFeature.ClientId = Property.ClientId and PropertyFeature.FeatureDesc='furnished') then 'true' else 'false' end)) as furnished,
                    (SELECT (Case when exists(select * from PropertyFeature where PropertyFeature.PropertyId = Property.PropertyId
                     and PropertyFeature.ClientId = Property.ClientId and PropertyFeature.FeatureDesc='unfurnished') then 'true' else 'false' end)) as unfurnished,
                    (SELECT (Case when exists(select * from PropertyFeature where PropertyFeature.PropertyId = Property.PropertyId
                     and PropertyFeature.ClientId = Property.ClientId and PropertyFeature.FeatureDesc='furnished or unfurnished') then 'true' else 'false' end)) as furnorunfurn
                       FROM Property Left Join PropertyDescriptive ON (Property.PropertyId = PropertyDescriptive.PropertyId and Property.ClientId = PropertyDescriptive.ClientId)
                       Left JOIN PropertyLocation ON (Property.LocationId = PropertyLocation.LocationId and Property.ClientId = PropertyLocation.ClientId)
                     Inner Join PropertySale ON (Property.PropertyId = PropertySale.PropertyId and Property.ClientId = PropertySale.ClientId)
                       WHERE Property.ClientId=" + ClientId + " And Property.PropertyId='" + PropId + "'";
        }
        SqlDataAdapter PropertyDA = new SqlDataAdapter(sql, Conn);
        DataSet ds = new DataSet();
        try
        {
            // Conn.Open();
            PropertyDA.Fill(ds);
        }
        catch
        {
        }
        finally
        {
            // Conn.Close();
        }

        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            //ds.Tables[0].Columns.Add("Cost");
            if (strType == "2")
            { }
            else
            {
                //ds.Tables[0].Columns.Add("Cost");
                ds.Tables[0].Columns.Add("CostToOrder");
            }

            ds.Tables[0].Columns.Add("FeaturesCount");
            ds.Tables[0].Columns.Add("PriceSuffix");
            ds.Tables[0].Columns.Add("CostToShow");
            ds.Tables[0].Columns.Add("video");
            ds.Tables[0].Rows[0]["CostToShow"] = "";
            ds.Tables[0].Columns.Add("bigimage1");
            ds.Tables[0].Columns.Add("bigimage2");
            ds.Tables[0].Columns.Add("bigimage3");
            ds.Tables[0].Columns.Add("bigimage4");
            ds.Tables[0].Columns.Add("bigimage5");
            ds.Tables[0].Columns.Add("bigimage6");
            ds.Tables[0].Columns.Add("bigimage7");
            ds.Tables[0].Columns.Add("bigimage8");
            ds.Tables[0].Columns.Add("bigimage9");
            ds.Tables[0].Columns.Add("bigimage10");
            ds.Tables[0].Columns.Add("imagescount");

            SqlCommand cmd;
            SqlDataReader dr = null;
            int l = 1;
            ///Conn.Open();

            try
            {
                if (ds.Tables[0].Rows[0]["LinkMainImage"] != null && ds.Tables[0].Rows[0]["LinkMainImage"].ToString() != "")
                {
                    sql = @"SELECT FileName,Category,Pkey,priority FROM Files WHERE Category In('Photo link') and ClientId=" + ClientId + " and ModuleId='" + ds.Tables[0].Rows[0]["PropertyId"].ToString() + "'";
                }
                else
                {
                    sql = @"SELECT FileName,Category,Pkey,priority FROM Files WHERE Category In('Photo','Main photo') and ClientId=" + ClientId + " and ModuleId='" + ds.Tables[0].Rows[0]["PropertyId"].ToString() + "'";
                }
                cmd = new SqlCommand(sql, Conn);
                if (Conn.State == ConnectionState.Closed)
                {
                    Conn.Open();
                }
                dr = cmd.ExecuteReader();
                int j = 2;

                try
                {
                    for (int k = 1; k <= 10; k++)
                    {
                        ds.Tables[0].Rows[0]["bigimage" + k] = "";
                    }
                }
                catch (Exception ex)
                {
                }

                if (ds.Tables[0].Rows[0]["LinkMainImage"] != null && ds.Tables[0].Rows[0]["LinkMainImage"].ToString() != "")
                {
                    while (dr.Read())
                    {
                        if (dr["priority"].ToString() == "1")
                        {
                            ds.Tables[0].Rows[0]["bigimage1"] = dr["Filename"].ToString();
                        }
                        else
                        {
                            string str = "bigimage" + j;
                            ds.Tables[0].Rows[0][str] = dr["Filename"].ToString();
                            j++;
                        }
                    }
                }
                else
                {
                    while (dr.Read())
                    {
                        if (dr["Category"].ToString() == "Main photo")
                        {
                            string strimagepath = ClientId + "-" + dr["pkey"].ToString() + "-" + dr["Filename"].ToString();
                            ds.Tables[0].Rows[0]["bigimage1"] = strimagepath;
                        }
                        else
                        {
                            string str = "bigimage" + j;
                            string strimagepath = ClientId + "-" + dr["pkey"].ToString() + "-" + dr["Filename"].ToString();
                            ds.Tables[0].Rows[0][str] = strimagepath;
                            j++;
                        }
                    }
                }

                if (!dr.IsClosed) dr.Close();


            }
            catch (Exception ex) { }

            //dr.Close();
            try
            {
                if (ds.Tables[0].Rows[0]["LinkMainImage"] != null && ds.Tables[0].Rows[0]["LinkMainImage"].ToString() != "")
                {
                    sql = @"SELECT Files.FileName FROM Files 
                             WHERE Files.ModuleId='" + PropId + "' and Files.Category like 'Photo link' and Files.ClientId=" + ClientId + " Order by Files.Priority Asc";
                }
                else
                {
                    sql = @"SELECT Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName FROM Files 
                             WHERE Files.ModuleId='" + PropId + "' and Files.ClientId=" + ClientId + " and Files.Category like 'Photo' Order by Files.Priority Asc";
                }
                cmd = new SqlCommand(sql, Conn);

                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    if (!ds.Tables[0].Columns.Contains("image" + l))
                        ds.Tables[0].Columns.Add("image" + l);
                    ds.Tables[0].Rows[0]["image" + l] = dr[0];
                    l++;
                }
                ds.Tables[0].Rows[0]["imagescount"] = l;
                if (!dr.IsClosed) dr.Close();
            }
            catch { }


            if (strType == "1" || ds.Tables[0].Rows[0]["DealType"].ToString().Trim() == "1")
            {


                sql = @"SELECT AskingPrice, PriceSuffix FROM PropertySale where PropertyId='" + ds.Tables[0].Rows[0]["PropertyId"] + "' And ClientId=" + ClientId;
                cmd = new SqlCommand(sql, Conn);
                try
                {
                    if (!dr.IsClosed) dr.Close();
                    dr = cmd.ExecuteReader();
                    if (dr.Read())
                    {
                        //ds.Tables[0].Rows[0]["Cost"] = dr["AskingPrice"];
                        ds.Tables[0].Rows[0]["CostToOrder"] = dr["AskingPrice"];

                        ds.Tables[0].Rows[0]["PriceSuffix"] = dr["PriceSuffix"];
                    }
                    if (dr != null || !dr.IsClosed)
                        dr.Close();
                }
                catch { }
            }
            else if (strType == "1" || ds.Tables[0].Rows[0]["DealType"].ToString().Trim() == "4")
            {


                sql = @"SELECT AskingPrice, PriceSuffix FROM PropertySale where PropertyId='" + ds.Tables[0].Rows[0]["PropertyId"] + "' And ClientId=" + ClientId;
                cmd = new SqlCommand(sql, Conn);
                try
                {
                    dr = cmd.ExecuteReader();
                    if (dr.Read())
                    {
                        //ds.Tables[0].Rows[0]["Cost"] = dr["AskingPrice"];
                        ds.Tables[0].Rows[0]["CostToOrder"] = dr["AskingPrice"];

                        ds.Tables[0].Rows[0]["PriceSuffix"] = dr["PriceSuffix"];
                    }
                    if (dr != null || !dr.IsClosed)
                        dr.Close();
                }
                catch { }
            }
            else
            {
                //for let property(rent)----------------------------------------------------------------
                try
                {
                    double price;

                    if (ds.Tables[0].Columns.Contains("PricePer"))
                    {
                        switch (ds.Tables[0].Rows[0]["PricePer"].ToString())
                        {
                            case "0": //week
                                ds.Tables[0].Rows[0]["CostToShow"] = " pcm";
                                price = Convert.ToDouble(ds.Tables[0].Rows[0]["Cost"]);
                                price = (price * 52) / 12;
                                ds.Tables[0].Rows[0]["CostToOrder"] = price;
                                break;
                            case "1": //month
                                ds.Tables[0].Rows[0]["CostToShow"] = " pcm";
                                price = Convert.ToDouble(ds.Tables[0].Rows[0]["Cost"]);
                                ds.Tables[0].Rows[0]["CostToOrder"] = price;
                                break;
                            case "2": //quarter
                                ds.Tables[0].Rows[0]["CostToShow"] = " pcm";
                                price = Convert.ToDouble(ds.Tables[0].Rows[0]["Cost"]);
                                price = (price / 3);
                                ds.Tables[0].Rows[0]["CostToOrder"] = price;
                                break;
                            case "3": //year
                                ds.Tables[0].Rows[0]["CostToShow"] = " pcm";
                                price = Convert.ToDouble(ds.Tables[0].Rows[0]["Cost"]);
                                price = (price / 12);
                                ds.Tables[0].Rows[0]["CostToOrder"] = price;
                                break;
                            default:
                                ds.Tables[0].Rows[0]["CostToShow"] = " pcm";
                                price = Convert.ToDouble(ds.Tables[0].Rows[0]["Cost"]);
                                ds.Tables[0].Rows[0]["CostToOrder"] = price;
                                break;
                        }
                    }
                }
                catch { }

                //--------------------------------------------------------------------------------------
            }
            try
            {
                ds.Tables[0].Rows[0]["Address"] = ds.Tables[0].Rows[0]["Address"].ToString().Replace("|@", "'");
                ds.Tables[0].Rows[0]["CityOrTownName"] = ds.Tables[0].Rows[0]["CityOrtownName"].ToString().Replace("|@", "'");
                ds.Tables[0].Rows[0]["AreaOrRegionName"] = ds.Tables[0].Rows[0]["AreaOrRegionName"].ToString().Replace("|@", "'");
                ds.Tables[0].Rows[0]["CountryName"] = ds.Tables[0].Rows[0]["CountryName"].ToString().Replace("|@", "'");
                ds.Tables[0].Rows[0]["PostCode"] = ds.Tables[0].Rows[0]["PostCode"].ToString().Replace("|@", "'");
                ds.Tables[0].Rows[0]["LocationAKA"] = ds.Tables[0].Rows[0]["LocationAKA"].ToString().Replace("|@", "'");
                ds.Tables[0].Rows[0]["ShortDesc"] = ds.Tables[0].Rows[0]["ShortDesc"].ToString().Replace("|@", "'");
                ds.Tables[0].Rows[0]["LongDesc"] = ds.Tables[0].Rows[0]["LongDesc"].ToString().Replace("|@", "'");

                /// sql = "SELECT '" + ClientId + "'+'-'+'" + PropId + "'+'-'+" + "Files.FileName FROM Files WHERE Files.ClientId=" + ClientId + " and Files.ModuleId='" + PropId + "' and (Files.Category like 'Virtual Tour')";
                sql = "SELECT Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName FROM Files WHERE Files.ClientId=" + ClientId +
                            " and Files.ModuleId='" + PropId + "' and (Files.Category like '%Virtual Tour%' or Files.Category like '%Virtual  Tour%')";
                cmd = new SqlCommand(sql, Conn);
                ds.Tables[0].Rows[0]["video"] = cmd.ExecuteScalar();

                ds.Tables[0].Rows[0]["FeaturesCount"] = 0;
                sql = @"SELECT PropertyFeature.FeatureDesc FROM PropertyFeature 
                             WHERE PropertyFeature.PropertyId='" + PropId + "' and PropertyFeature.ClientId=" + ClientId + " Order By PropertyFeatureRank Desc";
                cmd = new SqlCommand(sql, Conn);
                dr = cmd.ExecuteReader();
                int j = 1;
                while (dr.Read())
                {
                    if (!ds.Tables[0].Columns.Contains("Feature" + j))
                        ds.Tables[0].Columns.Add("Feature" + j);
                    ds.Tables[0].Rows[0]["Feature" + j] = dr[0];
                    j++;
                }
                ds.Tables[0].Rows[0]["FeaturesCount"] = j - 1;
                if (!dr.IsClosed) dr.Close();

            }
            catch { }
            finally
            {
                if (Conn.State == ConnectionState.Open)
                { Conn.Close(); }
            }
        }
        return ds;
    }


    #region Points of Interest

    // pulls poi categories from database---------------------------------------------
    public DataSet getPOICategories()
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = @"Select distinct POI_Categories.name, POIs.categoryid from POIs Inner Join POI_Categories
                                on POIs.categoryid=POI_Categories.categoryid order by POI_Categories.name Asc";
            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
            if (ds == null || ds.Tables.Count <= 0)
            {
                ds = new DataSet();
                ds.Tables.Add();
            }
        }
        catch
        {
            ds = new DataSet();
            ds.Tables.Add();
        }
        finally { }
        return ds;
    }

    // pulls pois from database depending on the bound area of map--------------------
    public DataSet getPOIs(string categoryid, string northeastlat, string northeastlon,
        string southwestlat, string southwestlon)
    {
        DataSet ds = new DataSet();
        try
        {
            string strQuery = "Select * from POIs where (";
            if (categoryid != null && categoryid != "")
            {
                string[] strCategoryIds = categoryid.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                if (strCategoryIds.Length <= 0)
                {
                    strQuery = " categoryid='" + categoryid + "'";
                }
                else
                {
                    foreach (string strCategoryId in strCategoryIds)
                    {
                        strQuery += " categoryid='" + strCategoryId + "' or";
                    }
                }
                strQuery = strQuery.TrimEnd(new char[] { 'o', 'r' });
                strQuery += ")";

                SqlConnection Conn = new SqlConnection(strConn);
                double dblnortheastlat = 0;
                double dblnortheastlon = 0;
                double dblsouthwestlat = 0;
                double dblsouthwestlon = 0;

                try
                {
                    dblnortheastlat = Convert.ToDouble(northeastlat);
                }
                catch { }
                try
                {
                    dblnortheastlon = Convert.ToDouble(northeastlon);
                }
                catch { }
                try
                {
                    dblsouthwestlat = Convert.ToDouble(southwestlat);
                }
                catch { }
                try
                {
                    dblsouthwestlon = Convert.ToDouble(southwestlon);
                }
                catch { }
                strQuery += dblnortheastlat > dblsouthwestlat ? (" and latitude < " + dblnortheastlat + " and latitude > " + dblsouthwestlat) : "";
                strQuery += dblnortheastlat < dblsouthwestlat ? (" and latitude > " + dblnortheastlat + " and latitude < " + dblsouthwestlat) : "";
                strQuery += dblnortheastlon > dblsouthwestlon ? (" and longitude < " + dblnortheastlon + " and longitude > " + dblsouthwestlon) : "";
                strQuery += dblnortheastlon < dblsouthwestlon ? (" and longitude > " + dblnortheastlon + " and longitude < " + dblsouthwestlon) : "";
                SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
                da.Fill(ds);
                if (ds == null || ds.Tables.Count <= 0)
                {
                    ds = new DataSet();
                    ds.Tables.Add();
                }
            }
        }
        catch
        {
            ds = new DataSet();
            ds.Tables.Add();
        }
        finally { }
        return ds;
    }

    #endregion

    public DataSet GetAreas2(string strType)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select Distinct REPLACE(PropertyLocation.LocationAKA,'|@','''') as Area " +
                              "from PropertyLocation Inner Join Property On (PropertyLocation.LocationId = Property.LocationId and PropertyLocation.ClientId = Property.ClientId) " +
                              "Where PropertyLocation.ClientId=" + ClientId + " and PropertyLocation.LocationAKA != ''";
            strQuery += " and (Property.DealType=" + strType + " Or Property.DealType=4)";
            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    public DataTable GetCountry()
    {
        DataTable dt = new DataTable();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select Distinct countryname from country where countryname <> '..' and countryname is not null";
            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(dt);
        }
        catch
        {
        }
        finally { }
        return dt;
    }



    public void getFeaturedProps(ClientScriptManager csMngr)
    {
        try
        {
            DataSet dsFrdProps = getFeaturedProps();
            string strPropIds = "";
            string strImgs = "";
            string strAddresses = "";
            string strPrices = "";
            string cost = "";
            string strproptype = "";
            if (dsFrdProps != null && dsFrdProps.Tables.Count > 0 && dsFrdProps.Tables[0].Rows.Count > 0)
            {

                string strImgPath = ConfigurationManager.AppSettings["ImagePath"];
                foreach (DataRow row in dsFrdProps.Tables[0].Rows)
                {
                    strproptype += "'" + row["DealType"].ToString() + "',";
                    strPropIds += "'" + row["PropertyId"].ToString() + "',";
                    strImgs += "'" + (row["image"] != null ? strImgPath + row["image"].ToString() : "images/image_soon-copy.jpg") + "',";

                    strAddresses += "'" + row["Address"].ToString() + "',";
                    cost = row["Cost"].ToString();
                    double cost1 = Convert.ToDouble(cost);
                    if (cost1 != null)
                    {
                        //cost = cost.Replace(".", "");
                        cost = Math.Round(cost1).ToString();
                    }

                    if (cost.Trim().Length > 4)
                    {
                        int j = cost.Trim().Length;
                        for (int k = j; k > 0; k--)
                        {
                            if ((j - k) == 3)
                            {
                                cost = cost.Insert(k, ",");
                                j = k;
                            }
                        }
                    }
                    strPrices += "'" + (cost.Trim().Length > 0 ? "�" + cost + row["CostToShow"].ToString() : "") + "',";
                }
                strPropIds = strPropIds.TrimEnd(new char[] { ',', ' ' });
                strImgs = strImgs.TrimEnd(new char[] { ',', ' ' });
                strAddresses = strAddresses.TrimEnd(new char[] { ',', ' ' });
                strPrices = strPrices.TrimEnd(new char[] { ',', ' ' });
                strproptype = strproptype.TrimEnd(new char[] { ',', ' ' });
            }

            if (!csMngr.IsClientScriptBlockRegistered("arrPropIds"))
            {
                csMngr.RegisterArrayDeclaration("arrPropIds", strPropIds);
            }
            if (!csMngr.IsClientScriptBlockRegistered("arrImgs"))
            {
                csMngr.RegisterArrayDeclaration("arrImgs", strImgs);
            }
            if (!csMngr.IsClientScriptBlockRegistered("arrAddresses"))
            {
                csMngr.RegisterArrayDeclaration("arrAddresses", strAddresses);
            }
            if (!csMngr.IsClientScriptBlockRegistered("arrPrices"))
            {
                csMngr.RegisterArrayDeclaration("arrPrices", strPrices);
            }
            if (!csMngr.IsClientScriptBlockRegistered("arrproptype"))
            {
                csMngr.RegisterArrayDeclaration("arrproptype", strproptype);
            }
        }
        catch { }
    }

    private DataSet getFeaturedProps()
    {
        ///  Property.FurtherNotes as Address,
        SqlConnection Conn = new SqlConnection(strConn);


        string sql = @"SELECT top (3) Property.PropertyId, Property.DealType, Property.TimeStampId,
                        (select top 1 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName) as image,
                       PropertyLocation.Address + ', ' + PropertyLocation.CityOrTownName as Address,
                       PropertySale.AskingPrice As Cost
                        FROM Property Left JOIN PropertyLocation ON (Property.LocationId = PropertyLocation.LocationId and Property.ClientId = PropertyLocation.ClientId)
                        Inner Join PropertySale ON (Property.PropertyId = PropertySale.PropertyId and Property.ClientId = PropertySale.ClientId)
                        Left Join Files On (Property.PropertyId = Files.ModuleId and Property.ClientId = Files.ClientId And Files.Category='Main photo') 
                        Where Property.ClientId =" + ClientId + " And Property.DealType in(1,2) And Property.FeaturedProperty='True' Order by Property.TimeStampId Desc";

        SqlDataAdapter PropertyDA = new SqlDataAdapter(sql, Conn);
        DataSet ds = new DataSet();
        try
        {

            PropertyDA.Fill(ds);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                ds.Tables[0].Columns.Add("CostToShow");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    try
                    {

                        ds.Tables[0].Rows[i]["Address"] = ds.Tables[0].Rows[i]["Address"].ToString().Replace("|@", "'")
                            .TrimEnd(new char[] { ' ', ',' }).TrimStart(new char[] { ',', ' ' });
                    }
                    catch { }
                }
            }
        }
        catch
        {
        }
        finally
        {

        }
        return ds;
    }

    public void getSaleLatesProps(ClientScriptManager csMngr)
    {
        try
        {
            DataSet dsFrdProps = getSaleLatestProps();
            string strPropIds = "";
            string strImgs = "";
            string strAddresses = "";
            string strPrices = "";
            string cost = "";
            if (dsFrdProps != null && dsFrdProps.Tables.Count > 0 && dsFrdProps.Tables[0].Rows.Count > 0)
            {
                string strImgPath = ConfigurationManager.AppSettings["SlideImagePath"];
                foreach (DataRow row in dsFrdProps.Tables[0].Rows)
                {
                    strPropIds += "'" + row["PropertyId"].ToString() + "',";
                    saleLatestPropid += "'" + row["PropertyId"].ToString() + "',";

                    SalelstIds.Add(row["PropertyId"].ToString());
                    if (row["image"] != null && row["image"].ToString() != "")
                    {
                        string[] strimagename = row["image"].ToString().Split(new char[] { '.' });
                        string strImage = strimagename[0];
                        string strexten = strimagename[1];


                        strImgs += "'" + strImgPath + strImage + "_slide." + strexten + "',";
                    }
                    else
                    {
                        strImgs += "'" + "images/coming-soon_lates.jpg" + "',";
                    }

                    strAddresses += "'" + row["Address"].ToString() + "',";
                    cost = row["Cost"].ToString();
                    if (cost.Trim().Length > 4)
                    {
                        int j = cost.Trim().Length;
                        for (int k = j; k > 0; k--)
                        {
                            if ((j - k) == 3)
                            {
                                cost = cost.Insert(k, ",");
                                j = k;
                            }
                        }
                    }
                    strPrices += "'" + (cost.Trim().Length > 0 ? "Price: &euro;" + cost : "") + "',";
                }
                strPropIds = strPropIds.TrimEnd(new char[] { ',', ' ' });
                strImgs = strImgs.TrimEnd(new char[] { ',', ' ' });
                strAddresses = strAddresses.TrimEnd(new char[] { ',', ' ' });
                strPrices = strPrices.TrimEnd(new char[] { ',', ' ' });
                saleLatestPropid = saleLatestPropid.TrimEnd(new char[] { ',', ' ' });
            }

            if (!csMngr.IsClientScriptBlockRegistered("arrSalePropIds"))
            {
                csMngr.RegisterArrayDeclaration("arrSalePropIds", strPropIds);
            }
            if (!csMngr.IsClientScriptBlockRegistered("arrSaleImgs"))
            {
                csMngr.RegisterArrayDeclaration("arrSaleImgs", strImgs);
            }
            if (!csMngr.IsClientScriptBlockRegistered("arrSaleAddresses"))
            {
                csMngr.RegisterArrayDeclaration("arrSaleAddresses", strAddresses);
            }
            if (!csMngr.IsClientScriptBlockRegistered("arrSalePrices"))
            {
                csMngr.RegisterArrayDeclaration("arrSalePrices", strPrices);
            }
        }
        catch { }
    }

    private DataSet getSaleLatestProps()
    {
        ///  Property.FurtherNotes as Address,
        SqlConnection Conn = new SqlConnection(strConn);


        string sql = @"SELECT top (5) Property.PropertyId, Property.DealType, Property.TimeStampId,
                        (select top 1 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName) as image,
                       PropertyLocation.Address + ', ' + PropertyLocation.CityOrTownName as Address,
                       PropertySale.AskingPrice As Cost
                        FROM Property Left JOIN PropertyLocation ON (Property.LocationId = PropertyLocation.LocationId and Property.ClientId = PropertyLocation.ClientId)
                        Inner Join PropertySale ON (Property.PropertyId = PropertySale.PropertyId and Property.ClientId = PropertySale.ClientId)
                        Left Join Files On (Property.PropertyId = Files.ModuleId and Property.ClientId = Files.ClientId And Files.Category='Main photo') 
                        Where Property.ClientId =" + ClientId + " And Property.DealType in(1,2) And Property.FeaturedProperty !='True' Order by Property.TimeStampId Desc";

        SqlDataAdapter PropertyDA = new SqlDataAdapter(sql, Conn);
        DataSet ds = new DataSet();
        try
        {

            PropertyDA.Fill(ds);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                ds.Tables[0].Columns.Add("CostToShow");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    try
                    {

                        ds.Tables[0].Rows[i]["Address"] = ds.Tables[0].Rows[i]["Address"].ToString().Replace("|@", "'")
                            .TrimEnd(new char[] { ' ', ',' }).TrimStart(new char[] { ',', ' ' });
                    }
                    catch { }
                }
            }
        }
        catch
        {
        }
        finally
        {

        }
        return ds;
    }

    public void getRentLatesProps(ClientScriptManager csMngr)
    {
        try
        {
            DataSet dsFrdProps = getRentLatestProps();
            string strPropIds = "";
            string strImgs = "";
            string strAddresses = "";
            string strPrices = "";
            string cost = "";

            if (dsFrdProps != null && dsFrdProps.Tables.Count > 0 && dsFrdProps.Tables[0].Rows.Count > 0)
            {
                string strImgPath = ConfigurationManager.AppSettings["SlideImagePath"];
                foreach (DataRow row in dsFrdProps.Tables[0].Rows)
                {
                    strPropIds += "'" + row["PropertyId"].ToString() + "',";
                    if (row["image"] != null && row["image"].ToString() != "")
                    {
                        string[] strimagename = row["image"].ToString().Split(new char[] { '.' });
                        string strImage = strimagename[0];
                        string strexten = strimagename[1];


                        strImgs += "'" + strImgPath + strImage + "_slide." + strexten + "',";
                    }
                    else
                    {
                        strImgs += "'" + "images/coming-soon_lates.jpg" + "',";
                    }

                    strAddresses += "'" + row["Address"].ToString() + "',";
                    cost = row["Cost"].ToString();
                    if (cost.Trim().Length > 4)
                    {
                        int j = cost.Trim().Length;
                        for (int k = j; k > 0; k--)
                        {
                            if ((j - k) == 3)
                            {
                                cost = cost.Insert(k, ",");
                                j = k;
                            }
                        }
                    }

                    strPrices += "'" + (cost.Trim().Length > 0 ? "Price: &euro;" + cost : "") + "',";
                }
                strPropIds = strPropIds.TrimEnd(new char[] { ',', ' ' });
                strImgs = strImgs.TrimEnd(new char[] { ',', ' ' });
                strAddresses = strAddresses.TrimEnd(new char[] { ',', ' ' });
                strPrices = strPrices.TrimEnd(new char[] { ',', ' ' });
            }

            if (!csMngr.IsClientScriptBlockRegistered("arrRentPropIds"))
            {
                csMngr.RegisterArrayDeclaration("arrRentPropIds", strPropIds);
            }
            if (!csMngr.IsClientScriptBlockRegistered("arrRentImgs"))
            {
                csMngr.RegisterArrayDeclaration("arrRentImgs", strImgs);
            }
            if (!csMngr.IsClientScriptBlockRegistered("arrRentAddresses"))
            {
                csMngr.RegisterArrayDeclaration("arrRentAddresses", strAddresses);
            }
            if (!csMngr.IsClientScriptBlockRegistered("arrRentPrices"))
            {
                csMngr.RegisterArrayDeclaration("arrRentPrices", strPrices);
            }
        }
        catch { }
    }

    private DataSet getRentLatestProps()
    {
        ///  Property.FurtherNotes as Address,
        SqlConnection Conn = new SqlConnection(strConn);




        ////        string sql = @"SELECT Top(10)  Property.PropertyId, Property.DealType, Property.TimeStampId,
        ////                        (select top 1 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName) as image,
        ////                       PropertyLocation.Address + ', ' + PropertyLocation.CityOrTownName as Address,
        ////                     PropertyLet.Price As Cost, PropertyLet.Price As CostToOrder, PropertyLet.PricePer
        ////                        FROM Property Left JOIN PropertyLocation ON (Property.LocationId = PropertyLocation.LocationId and Property.ClientId = PropertyLocation.ClientId)
        ////                        Inner Join PropertyLet ON (Property.PropertyId = PropertyLet.PropertyId and Property.ClientId = PropertyLet.ClientId)
        ////                        Left Join Files On (Property.PropertyId = Files.ModuleId and Property.ClientId = Files.ClientId And Files.Category='Main photo') 
        ////                        Where Property.ClientId =" + ClientId + " And Property.DealType in(1,2) And Property.FeaturedProperty !='True' Order by Property.TimeStampId Desc";

        string sql = @"SELECT top (15) Property.PropertyId, Property.DealType, Property.TimeStampId,
                        (select top 1 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName) as image,
                       PropertyLocation.Address + ', ' + PropertyLocation.CityOrTownName as Address,
                       PropertySale.AskingPrice As Cost
                        FROM Property Left JOIN PropertyLocation ON (Property.LocationId = PropertyLocation.LocationId and Property.ClientId = PropertyLocation.ClientId)
                        Inner Join PropertySale ON (Property.PropertyId = PropertySale.PropertyId and Property.ClientId = PropertySale.ClientId)
                        Left Join Files On (Property.PropertyId = Files.ModuleId and Property.ClientId = Files.ClientId And Files.Category='Main photo') 
                        Where Property.ClientId =" + ClientId + " And Property.DealType in(1,2) And Property.PropertyId not in (" + saleLatestPropid.ToString() + ")  And Property.FeaturedProperty !='True' Order by Property.TimeStampId Desc";


        SqlDataAdapter PropertyDA = new SqlDataAdapter(sql, Conn);
        DataSet ds = new DataSet();
        try
        {

            PropertyDA.Fill(ds);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                int j = 0;

                ////  and Property.PropertyId not in(" + saleLatestPropid + ") 



                ds.Tables[0].Columns.Add("CostToShow");
                double price = 0;
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {

                    ////try
                    ////{
                    ////    object objId = ds.Tables[0].Rows[i]["PropertyId"];
                    ////    if (SalelstIds.Contains(objId.ToString().ToLower()) == true)
                    ////    {
                    ////        ds.Tables[0].Rows.RemoveAt(j);
                    ////        j--;
                    ////        continue;
                    ////    }

                    ////}
                    ////catch { }

                    try
                    {

                        ds.Tables[0].Rows[i]["Address"] = ds.Tables[0].Rows[i]["Address"].ToString().Replace("|@", "'")
                            .TrimEnd(new char[] { ' ', ',' }).TrimStart(new char[] { ',', ' ' });
                    }
                    catch { }

                    //////try
                    //////{

                    //////    if (ds.Tables[0].Columns.Contains("PricePer"))
                    //////    {


                    //////        switch (ds.Tables[0].Rows[i]["PricePer"].ToString())
                    //////        {
                    //////            case "1":
                    //////                ds.Tables[0].Rows[i]["CostToShow"] = " pm";
                    //////                price = Convert.ToDouble(ds.Tables[0].Rows[i]["Cost"]);
                    //////                price = price / 4;
                    //////                ds.Tables[0].Rows[i]["CostToOrder"] = price;
                    //////                break;
                    //////            case "2":
                    //////                ds.Tables[0].Rows[i]["CostToShow"] = " pq";
                    //////                price = Convert.ToDouble(ds.Tables[0].Rows[i]["Cost"]);
                    //////                price = price / 12;
                    //////                ds.Tables[0].Rows[i]["CostToOrder"] = price;
                    //////                break;
                    //////            case "3":
                    //////                ds.Tables[0].Rows[i]["CostToShow"] = " pa";
                    //////                price = Convert.ToDouble(ds.Tables[0].Rows[i]["Cost"]);
                    //////                price = price / 50;
                    //////                ds.Tables[0].Rows[i]["CostToOrder"] = price;
                    //////                break;
                    //////            default:
                    //////                ds.Tables[0].Rows[i]["CostToShow"] = " pw";
                    //////                price = Convert.ToDouble(ds.Tables[0].Rows[i]["Cost"]);
                    //////                ds.Tables[0].Rows[i]["CostToOrder"] = price;
                    //////                break;
                    //////        }
                    //////    }
                    //////}
                    //////catch { }
                }
            }
        }
        catch
        {
        }
        finally
        {

        }
        return ds;
    }

    public int deleteUserAccount(string strUsername)
    {
        int result = 0;
        OleDbConnection oleConn = new OleDbConnection(getConnStr("."));
        try
        {
            string strQuery = "Select * from MemberDetails where [EmailId]='" + strUsername.Replace("'", "|@") + "';";
            OleDbDataAdapter da = new OleDbDataAdapter(strQuery, oleConn);
            oleConn.Open();
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                ds.Tables[0].Columns.Add("fname");
                ds.Tables[0].Columns.Add("lname");
                ds.Tables[0].Rows[0]["fname"] = "";
                ds.Tables[0].Rows[0]["lname"] = "";
                if (ds.Tables[0].Rows[0]["ContactId"] != null && ds.Tables[0].Rows[0]["ContactId"].ToString().Trim() != "")
                {
                    strQuery = "Select FirstName,LastName from PeopleContacts where ContactId='" + ds.Tables[0].Rows[0]["ContactId"].ToString().Trim() + "';";
                    SqlConnection sqlConn = new SqlConnection(strConn);
                    SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
                    try
                    {
                        oleConn.Close();
                        sqlConn.Open();
                        SqlDataReader sqlDr = sqlCmd.ExecuteReader();
                        if (sqlDr != null && sqlDr.Read())
                        {
                            ds.Tables[0].Rows[0]["fname"] = sqlDr["FirstName"];
                            ds.Tables[0].Rows[0]["lname"] = sqlDr["LastName"];
                        }
                    }
                    catch { }
                    finally { sqlConn.Close(); }
                }
                string sb = "";
                sb += "----------------------------<br/>";
                sb += "A user has deleted his/her profile on My best move.<br/><br/>";
                sb += "The details of the user's account are as follows:<br/><br/>";
                sb += "Username: " + strUsername + "<br/>";
                sb += "Password: " + ds.Tables[0].Rows[0]["LoginPassword"] + "<br/>";
                sb += "First name: " + ds.Tables[0].Rows[0]["fname"] + "<br/>";
                sb += "Last name: " + ds.Tables[0].Rows[0]["lname"] + "<br/>";
                sb += "Email: " + ds.Tables[0].Rows[0]["EmailId"] + "<br/>";


                sb += "----------------------------<br/><br/>";
                sb += "<Ideal Team<br/><br/>";
                try
                {
                    SendEmailList(ConfigurationManager.AppSettings["ToEmailId"], "User deleted account:", sb);
                    SendEmailList(strUsername, "You have deleted your account:", sb);
                }
                catch { }
                strQuery = "Delete from MemberDetails where [EmailId]='" + strUsername.Replace("'", "|@") + "';";
                OleDbCommand oleCmd = new OleDbCommand(strQuery, oleConn);
                if (oleConn.State == ConnectionState.Closed)
                    oleConn.Open();
                result = oleCmd.ExecuteNonQuery();
            }
        }
        catch { }
        finally { oleConn.Close(); }
        return result;
    }

    public DataSet GetBranch()
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);



            string strQuery = "select Distinct BranchName from Branches where ClientId =" + ClientId + " Order by BranchName ";
            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    public DataSet GetBuyRentAreas()
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select Distinct REPLACE(PropertyLocation.AreaOrRegionName,'|@','''') as Area " +
                              "from PropertyLocation Inner Join Property On (PropertyLocation.LocationId = Property.LocationId and PropertyLocation.ClientId = Property.ClientId) " +
                              "Where PropertyLocation.ClientId =" + ClientId + " and PropertyLocation.AreaOrRegionName != ''";
            strQuery += " and PropertyLocation.AreaOrRegionName != 'Any region' and PropertyLocation.AreaOrRegionName != 'All'";
            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);

            da.Fill(ds);


            //////string strQuery = "select Distinct BranchName from Branches where ClientId ="+ ClientId +" Order by BranchName ";
            //////SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            //////da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    public DataSet GetAreas(string strType)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select Distinct REPLACE(PropertyLocation.LocationAKA,'|@','''') as Area " +
                              "from PropertyLocation Inner Join Property On (PropertyLocation.LocationId = Property.LocationId and PropertyLocation.ClientId = Property.ClientId) " +
                              "Where PropertyLocation.ClientId =" + ClientId + " and PropertyLocation.LocationAKA != ''";
            strQuery += " and (Property.DealType= " + strType + " Or Property.DealType=4) and PropertyLocation.LocationAKA != 'All'";
            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
            //////string strQuery = "select Distinct BranchName from Branches where ClientId ="+ ClientId +" Order by BranchName ";
            //////SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            //////da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    public DataSet GetCities(string strType)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select Distinct REPLACE(PropertyLocation.CityOrTownName,'|@','''') as Area " +
                              "from PropertyLocation Inner Join Property On (PropertyLocation.LocationId = Property.LocationId and PropertyLocation.ClientId = Property.ClientId) " +
                              "Where PropertyLocation.ClientId =" + ClientId + " and PropertyLocation.CityOrTownName != ''";
            strQuery += " and (Property.DealType= " + strType + " Or Property.DealType=4) and PropertyLocation.CityOrTownName != 'All' and PropertyLocation.CityOrTownName != 'Any town'";
            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);

            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    public DataSet GetRegion(string strType)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select Distinct REPLACE(PropertyLocation.AreaOrRegionName,'|@','''') as Area " +
                              "from PropertyLocation Inner Join Property On (PropertyLocation.LocationId = Property.LocationId and PropertyLocation.ClientId = Property.ClientId) ";
            if (strType == "1")
            {
                strQuery += " Inner Join PropertySale On (Property.PropertyId = PropertySale.PropertyId and Property.ClientId = PropertySale.ClientId) ";
                strQuery += "Where PropertyLocation.ClientId =" + ClientId + " and PropertyLocation.AreaOrRegionName != ''";
                strQuery += " and (Property.DealType=" + strType + " Or Property.DealType=4) and PropertySale.IncludeOnWebsite='True' and Property.Status<>7";
            }
            else
            {
                strQuery += " Inner Join PropertyLet On (Property.PropertyId = PropertyLet.PropertyId and Property.ClientId = PropertyLet.ClientId) ";
                strQuery += "Where PropertyLocation.ClientId =" + ClientId + " and PropertyLocation.AreaOrRegionName != ''";
                strQuery += " and (Property.DealType=" + strType + " Or Property.DealType=4) and PropertyLet.IncludeOnWebsite='True' and Property.Status<>7";
            }
            //                  "Where PropertyLocation.ClientId =" + ClientId + " and PropertyLocation.AreaOrRegionName != ''";
            //strQuery += " and (Property.DealType= " + strType + " Or Property.DealType=4)  ";
            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);

            da.Fill(ds);


            //////string strQuery = "select Distinct BranchName from Branches where ClientId ="+ ClientId +" Order by BranchName ";
            //////SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            //////da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    public DataSet GetPostcodes(string strType)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select Distinct REPLACE(PropertyLocation.Postcode,'|@','''') as Area " +
                              "from PropertyLocation Inner Join Property On (PropertyLocation.LocationId = Property.LocationId and PropertyLocation.ClientId = Property.ClientId) ";
            if (strType == "1")
            {
                strQuery += " Inner Join PropertySale On (Property.PropertyId = PropertySale.PropertyId and Property.ClientId = PropertySale.ClientId) ";
                strQuery += "Where PropertyLocation.ClientId =" + ClientId + " and PropertyLocation.LocationAKA != ''";
                strQuery += " and (Property.DealType=" + strType + " Or Property.DealType=4) and PropertySale.IncludeOnWebsite='True' and (Property.Status<>1 and Property.Status<>7) and PropertyLocation.Postcode!=''";
            }
            else
            {
                strQuery += " Inner Join PropertyLet On (Property.PropertyId = PropertyLet.PropertyId and Property.ClientId = PropertyLet.ClientId) ";
                strQuery += "Where PropertyLocation.ClientId =" + ClientId + " and PropertyLocation.LocationAKA != ''";
                strQuery += " and (Property.DealType=" + strType + " Or Property.DealType=4) and PropertyLet.IncludeOnWebsite='True' and (Property.Status<>1 and Property.Status<>7) and PropertyLocation.Postcode!=''";
            }
            //strQuery += "Where PropertyLocation.ClientId =" + ClientId + " and PropertyLocation.LocationAKA != ''";

            //strQuery += " and (Property.DealType=" + strType + " Or Property.DealType=4) and PropertyLet.IncludeOnWebsite='True'";
            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    public DataSet getfeatureprop(string locations)
    {
        string sql = "";

        SqlConnection Conn = new SqlConnection(strConn);


        if (locations != null && locations != "" && locations.Trim() != "Any")
        {

            sql = @"SELECT Top(1) Property.PropertyId,
                        (select top 1 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName) as image,
                       PropertyLocation.Address + ', ' + PropertyLocation.AreaOrRegionName +', ' + PropertyLocation.CityOrTownName as Address,
                        PropertySale.AskingPrice As Cost
                        FROM Property Left JOIN PropertyLocation ON (Property.LocationId = PropertyLocation.LocationId and Property.ClientId = PropertyLocation.ClientId)
                       Inner Join PropertySale ON (Property.PropertyId = PropertySale.PropertyId and Property.ClientId = PropertySale.ClientId)
                        Left Join Files On (Property.PropertyId = Files.ModuleId and Property.ClientId = Files.ClientId And Files.Category='Main photo') 
                        Where Property.ClientId=" + ClientId + " And PropertyLocation.LocationAKA='" + locations.Replace("'", "|@") + "' And Property.FeaturedProperty='True' Order by Property.TimeStampId Desc";



        }
        else
        {


            sql = @"SELECT Top(1) Property.PropertyId, Property.DealType, Property.TimeStampId,
                        (select top 1 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName) as image,
                       PropertyLocation.Address + ', ' + PropertyLocation.AreaOrRegionName +', ' + PropertyLocation.CityOrTownName as Address,
                        PropertySale.AskingPrice As Cost
                        FROM Property Left JOIN PropertyLocation ON (Property.LocationId = PropertyLocation.LocationId and Property.ClientId = PropertyLocation.ClientId)
                       Inner Join PropertySale ON (Property.PropertyId = PropertySale.PropertyId and Property.ClientId = PropertySale.ClientId)
                        Left Join Files On (Property.PropertyId = Files.ModuleId and Property.ClientId = Files.ClientId And Files.Category='Main photo') 
                        Where Property.ClientId=" + ClientId + " And Property.FeaturedProperty='True' Order by Property.TimeStampId Desc";

        }

        SqlDataAdapter PropertyDA = new SqlDataAdapter(sql, Conn);
        DataSet ds = new DataSet();
        try
        {

            PropertyDA.Fill(ds);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    try
                    {


                        ds.Tables[0].Rows[i]["Address"] = ds.Tables[0].Rows[i]["Address"].ToString().Replace("|@", "'")
                  .TrimEnd(new char[] { ' ', ',' }).TrimStart(new char[] { ',', ' ' });

                    }
                    catch { }
                }


            }
        }
        catch
        {
        }
        finally
        {

        }
        return ds;

    }

    public DataSet getbuyProp()
    {
        SqlConnection Conn = new SqlConnection(strConn);
        string sqlQuery = "";

        string strCondition = "Where Property.ClientId =" + ClientId + " AND ";
        strCondition += " (Property.DealType = 1 Or Property.DealType = 4)  AND (PropertySale.IncludeOnWebsite = 'True') AND property.Status <> 7 AND Property.Status!='1'";



        sqlQuery = @"SELECT Property.PropertyId,Property.dealtype,Property.timestampid,(Select SubValue from SubType Where MajorId=11 and SubId=Property.Status) as Status,
                    (Select SubValue from SubType Where MajorId=Property.PropertyMajorType and SubId=Property.PropertySubType) as proptype,
                    (Select top 1 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId
                    and Files.Category like 'Main photo') As image, PropertyDescriptive.ShortDesc,
                    (SELECT count(*) FROM PropertyRoom Inner JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                    Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=1) as NumberOfBedrooms,
                    (SELECT count(*) FROM PropertyRoom Inner JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                    Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=40) as NumberOfBathrooms,
                    (SELECT count(*) FROM PropertyRoom Inner JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                    Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=6) as NumberOfReceptions,
                    PropertyLocation.Address + ', '  + PropertyLocation.CityOrTownName as Address,
                    PropertyLocation.PropertyNameNumber+', '+PropertyLocation.Address as street, PropertyLocation.PostCode,PropertyLocation.Latitude,PropertyLocation.Longitude,PropertyLocation.ShowGSVOnWebsite,
                      PropertySale.AskingPrice As Cost, PropertySale.AskingPrice As CostToOrder, PropertySale.PriceSuffix FROM Property Left Join PropertyDescriptive ON 
                    (Property.PropertyId = PropertyDescriptive.PropertyId and Property.ClientId = PropertyDescriptive.ClientId)
                    Left JOIN PropertyLocation ON (Property.LocationId = PropertyLocation.LocationId and Property.ClientId = PropertyLocation.ClientId)
                    Left JOIN UserDetails ON (Property.UserName = UserDetails.UserName and Property.ClientId = UserDetails.ClientId)
                    Inner Join PropertySale ON (Property.PropertyId = PropertySale.PropertyId and Property.ClientId = PropertySale.ClientId) " + strCondition;

        SqlDataAdapter PropertyDA = new SqlDataAdapter(sqlQuery, Conn);
        DataSet ds = new DataSet();
        try
        {

            PropertyDA.Fill(ds);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                ds.Tables[0].Columns.Add("CostToShow");
                double price = 0;
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    try
                    {

                        DataRow row = ds.Tables[0].Rows[i];

                        row["PostCode"] = row["PostCode"].ToString().Replace("|@", "'");
                        row["street"] = row["street"].ToString().Replace("|@", "'");
                        row["ShortDesc"] = row["ShortDesc"].ToString().Replace("|@", "'");
                    }
                    catch { }
                    try
                    {

                        if (ds.Tables[0].Columns.Contains("PricePer"))
                        {

                            switch (ds.Tables[0].Rows[i]["PricePer"].ToString())
                            {
                                case "1":
                                    ds.Tables[0].Rows[i]["CostToShow"] = " pm";
                                    price = Convert.ToDouble(ds.Tables[0].Rows[i]["Cost"]);
                                    price = price / 4;
                                    ds.Tables[0].Rows[i]["CostToOrder"] = price;
                                    break;
                                case "2":
                                    ds.Tables[0].Rows[i]["CostToShow"] = " pq";
                                    price = Convert.ToDouble(ds.Tables[0].Rows[i]["Cost"]);
                                    price = price / 12;
                                    ds.Tables[0].Rows[i]["CostToOrder"] = price;
                                    break;
                                case "3":
                                    ds.Tables[0].Rows[i]["CostToShow"] = " pa";
                                    price = Convert.ToDouble(ds.Tables[0].Rows[i]["Cost"]);
                                    price = price / 50;
                                    ds.Tables[0].Rows[i]["CostToOrder"] = price;
                                    break;
                                default:
                                    ds.Tables[0].Rows[i]["CostToShow"] = " pw";
                                    price = Convert.ToDouble(ds.Tables[0].Rows[i]["Cost"]);
                                    ds.Tables[0].Rows[i]["CostToOrder"] = price;
                                    break;
                            }
                        }


                        string[] addressparts = ds.Tables[0].Rows[i]["Address"].ToString().Replace("|@", "'").Split(new char[] { ',' });
                        string address = addressparts[0];
                        address += addressparts.Length > 1 ? ", " + addressparts[1] : "";
                        address += addressparts.Length > 2 ? ", " + addressparts[2] : "";
                        address += addressparts.Length > 4 ? ", " + addressparts[4] : "";
                        address = address.TrimEnd(new char[] { ',', ' ' });
                        ds.Tables[0].Rows[i]["Address"] = address;
                    }
                    catch { }
                }
            }
        }
        catch
        {
        }
        finally
        {

        }
        return ds;
    }

    public DataSet getrentProp()
    {
        SqlConnection Conn = new SqlConnection(strConn);
        string sqlQuery = "";

        string strCondition = "Where Property.ClientId =" + ClientId + " AND ";
        strCondition += " (Property.DealType = 2 Or Property.DealType = 4) AND (PropertyLet.IncludeOnWebsite = 'True') AND property.Status <> 7 AND Property.Status!='1' ";



        sqlQuery = @"SELECT Property.PropertyId,Property.dealtype,Property.timestampid,(Select SubValue from SubType Where MajorId=11 and SubId=Property.Status) as Status,
                    (Select SubValue from SubType Where MajorId=Property.PropertyMajorType and SubId=Property.PropertySubType) as proptype,
                    (Select top 1 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId
                    and Files.Category like 'Main photo') As image, PropertyDescriptive.ShortDesc,
                    (SELECT count(*) FROM PropertyRoom Inner JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                    Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=1) as NumberOfBedrooms,
                    (SELECT count(*) FROM PropertyRoom Inner JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                    Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=40) as NumberOfBathrooms,
                    (SELECT count(*) FROM PropertyRoom Inner JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                    Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=6) as NumberOfReceptions,
                    PropertyLocation.Address + ', '  + PropertyLocation.CityOrTownName as Address,
                    PropertyLocation.PropertyNameNumber+', '+PropertyLocation.Address as street, PropertyLocation.PostCode,PropertyLocation.Latitude,PropertyLocation.Longitude,PropertyLocation.ShowGSVOnWebsite,
                      PropertyLet.Price As Cost, PropertyLet.Price As CostToOrder, PropertyLet.PriceSuffix,PropertyLet.PricePer FROM Property Left Join PropertyDescriptive ON 
                    (Property.PropertyId = PropertyDescriptive.PropertyId and Property.ClientId = PropertyDescriptive.ClientId)
                    Left JOIN PropertyLocation ON (Property.LocationId = PropertyLocation.LocationId and Property.ClientId = PropertyLocation.ClientId)
                    Left JOIN UserDetails ON (Property.UserName = UserDetails.UserName and Property.ClientId = UserDetails.ClientId)
                    Inner Join PropertyLet ON (Property.PropertyId = PropertyLet.PropertyId and Property.ClientId = PropertyLet.ClientId) " + strCondition;

        SqlDataAdapter PropertyDA = new SqlDataAdapter(sqlQuery, Conn);
        DataSet ds = new DataSet();
        try
        {

            PropertyDA.Fill(ds);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                ds.Tables[0].Columns.Add("CostToShow");
                double price = 0;
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    try
                    {

                        DataRow row = ds.Tables[0].Rows[i];

                        row["PostCode"] = row["PostCode"].ToString().Replace("|@", "'");
                        row["street"] = row["street"].ToString().Replace("|@", "'");
                        row["ShortDesc"] = row["ShortDesc"].ToString().Replace("|@", "'");
                    }
                    catch { }
                    try
                    {

                        if (ds.Tables[0].Columns.Contains("PricePer"))
                        {

                            switch (ds.Tables[0].Rows[i]["PricePer"].ToString())
                            {
                                case "1":
                                    ds.Tables[0].Rows[i]["CostToShow"] = " pw";
                                    price = Convert.ToDouble(ds.Tables[0].Rows[i]["Cost"]);
                                    price = (price * 12) / 52;
                                    ds.Tables[0].Rows[i]["CostToOrder"] = Math.Round(price, 2);
                                    break;
                                case "2":
                                    ds.Tables[0].Rows[i]["CostToShow"] = " pw";
                                    price = Convert.ToDouble(ds.Tables[0].Rows[i]["Cost"]);
                                    price = ((price / 6) * 12) / 52;
                                    ds.Tables[0].Rows[i]["CostToOrder"] = Math.Round(price, 2);
                                    break;
                                case "3":
                                    ds.Tables[0].Rows[i]["CostToShow"] = " pw";
                                    price = Convert.ToDouble(ds.Tables[0].Rows[i]["Cost"]);
                                    price = ((price / 12) * 12) / 52;
                                    ds.Tables[0].Rows[i]["CostToOrder"] = Math.Round(price, 2);
                                    break;
                                default:
                                    ds.Tables[0].Rows[i]["CostToShow"] = " pw";
                                    price = Convert.ToDouble(ds.Tables[0].Rows[i]["Cost"]);
                                    ds.Tables[0].Rows[i]["CostToOrder"] = Math.Round(price, 2);
                                    break;
                            }
                        }
                        ds.Tables[0].Rows[i]["Cost"] = ds.Tables[0].Rows[i]["CostToOrder"].ToString();

                        string[] addressparts = ds.Tables[0].Rows[i]["Address"].ToString().Replace("|@", "'").Split(new char[] { ',' });
                        string address = addressparts[0];
                        address += addressparts.Length > 1 ? ", " + addressparts[1] : "";
                        address += addressparts.Length > 2 ? ", " + addressparts[2] : "";
                        address += addressparts.Length > 4 ? ", " + addressparts[4] : "";
                        address = address.TrimEnd(new char[] { ',', ' ' });
                        ds.Tables[0].Rows[i]["Address"] = address;
                    }
                    catch { }
                }
            }
        }
        catch
        {
        }
        finally
        {

        }
        return ds;
    }

    public DataSet GetDataForSlideShowMainImage(string PropId)
    {
        SqlConnection Conn = new SqlConnection(strConn);
        string sqlQuery = "";
        string main = "Main photo";
        sqlQuery = "select * from Files where ModuleId='" + PropId + "' and Category='" + main + "'";
        SqlDataAdapter PropertyDA = new SqlDataAdapter(sqlQuery, Conn);
        DataSet ds = new DataSet();
        try
        {

            PropertyDA.Fill(ds);
        }
        catch
        {
        }
        finally
        {

        }
        return ds;
    }

    public DataSet GetDataForSlideShowImage(string PropId)
    {
        SqlConnection Conn = new SqlConnection(strConn);
        string sqlQuery = "";

        sqlQuery = "select * from Files where ModuleId='" + PropId + "' and Category in ('photo','photo link','Main photo')and Files.ClientId=" + ClientId + " order by Priority";
        //sqlQuery = "select * from Files where ModuleId='" + PropId + "' and Category in ('photo','Main photo')and Files.ClientId=" + ClientId + " order by Priority";
        SqlDataAdapter PropertyDA = new SqlDataAdapter(sqlQuery, Conn);
        DataSet ds = new DataSet();
        try
        {

            PropertyDA.Fill(ds);
        }
        catch
        {
        }
        finally
        {

        }
        return ds;
    }

    //private Coordinate GetLatLong(string PostCode)
    //{
    //    Coordinate c = new Coordinate();

    //    string add = PostCode;

    //    if (add != "")
    //    {
    //        try { c = Geocode.GetCoordinates(add); }
    //        catch { }
    //    }
    //    return c;
    //}
   //  GetPrevPropDataset(string propid, string orderby, string strminprice, string strmaxprice, string strbedrooms, string strlocation, string strType)
    public DataSet GetPrevPropDataset(string propid, string orderby, string strminprice, string strmaxprice, string strbedrooms, string strlocation, string SSTC, string strbranch)
    {
        SqlConnection Conn = new SqlConnection(strConn);
        string sqlQuery = "";
        string strCondition = "";
        int arrCnt = 0;
        System.Collections.ArrayList arrpropid = new System.Collections.ArrayList();

        strlocation = strbranch;
        if (orderby.Trim() != "" && orderby != "Any")
        {
            strCondition = @"Where Property.ClientId =" + ClientId + " AND (Property.DealType = 1 Or Property.DealType = 4) AND ";


            if (strlocation.Trim() != "" && strlocation != "Any")
            {

                strCondition += "PropertyLocation.LocationAKA='" + strlocation.Replace("'", "|@") + "' AND";

            }
            //if (strbranch.Trim() != "" && strbranch != "Any")
            //{
            //    strCondition += "UserDetails.BranchName='" + strbranch + "' AND ";
            //}


            if ((strminprice.ToString().Replace(",", "") != "" && strminprice.ToString().Replace(",", "") != "0") && (strmaxprice.ToString().Replace(",", "") == "" || strmaxprice.ToString().Replace(",", "") == "0"))
            {
                strCondition += "PropertySale.AskingPrice >= " + strminprice.ToString().Replace(",", "") + " AND PropertySale.IncludeOnWebsite = 'True' AND ";
            }
            else if ((strminprice.ToString().Replace(",", "") == "" || strminprice.ToString().Replace(",", "") == "0") && (strmaxprice.ToString().Replace(",", "") != "" && strmaxprice.ToString().Replace(",", "") != "0"))
            {
                strCondition += "PropertySale.AskingPrice <= " + strmaxprice.ToString().Replace(",", "") + " AND PropertySale.IncludeOnWebsite = 'True' AND ";
            }
            else if (strminprice.ToString().Replace(",", "") != "" && strminprice.ToString().Replace(",", "") != "0" && strmaxprice.ToString().Replace(",", "") != "" && strmaxprice.ToString().Replace(",", "") != "0")
            {
                strCondition += "(PropertySale.AskingPrice Between " + strminprice.ToString().Replace(",", "") + " AND " + strmaxprice.ToString().Replace(",", "") + " AND PropertySale.IncludeOnWebsite = 'True') AND ";
            }
            else
            {
                strCondition += "(PropertySale.IncludeOnWebsite = 'True') AND ";
            }

            if (SSTC != "" && SSTC != "True")
            {

                strCondition += "Property.Status!='1'";
            }

            //////if (SSTC != "" && SSTC != "False")
            //////{

            //////    strCondition += "Property.Status='1'";
            //////}
            //////else
            //////{
            //////    strCondition += "Property.Status!='1'";
            //////}
            strCondition = strCondition.TrimEnd(new char[] { 'A', 'N', 'D', ' ' });


            sqlQuery = @"SELECT Property.PropertyId,(Select top 1 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId
                    and Files.Category like 'Main photo') As image,PropertyLocation.Address as Address,
                    PropertyLocation.PostCode,PropertyLocation.Latitude,PropertyLocation.Longitude,
                    PropertySale.AskingPrice As Cost, PropertySale.AskingPrice As CostToOrder, PropertySale.PriceSuffix FROM Property Inner Join PropertySale ON (Property.PropertyId = PropertySale.PropertyId and Property.ClientId = PropertySale.ClientId) 
                   Left JOIN PropertyLocation ON (Property.LocationId = PropertyLocation.LocationId and Property.ClientId = PropertyLocation.ClientId)
                   Left JOIN UserDetails ON (Property.UserName = UserDetails.UserName and Property.ClientId = UserDetails.ClientId) 
  " + strCondition + "order by " + orderby;
        }
        else
        {

            strCondition = @"Where Property.ClientId =" + ClientId + " AND (Property.DealType = 1 Or Property.DealType = 4) AND ";


            if (strlocation.Trim() != "" && strlocation != "Any")
            {

                strCondition += "PropertyLocation.LocationAKA='" + strlocation.Replace("'", "|@") + "' AND";

            }

            //if (strbranch.Trim() != "" && strbranch != "Any")
            //{
            //    strCondition += "UserDetails.BranchName='" + strbranch + "' AND ";
            //}


            if ((strminprice.ToString().Replace(",", "") != "" && strminprice.ToString().Replace(",", "") != "0") && (strmaxprice.ToString().Replace(",", "") == "" || strmaxprice.ToString().Replace(",", "") == "0"))
            {
                strCondition += "PropertySale.AskingPrice >= " + strminprice.ToString().Replace(",", "") + " AND PropertySale.IncludeOnWebsite = 'True' AND ";
            }
            else if ((strminprice.ToString().Replace(",", "") == "" || strminprice.ToString().Replace(",", "") == "0") && (strmaxprice.ToString().Replace(",", "") != "" && strmaxprice.ToString().Replace(",", "") != "0"))
            {
                strCondition += "PropertySale.AskingPrice <= " + strmaxprice.ToString().Replace(",", "") + " AND PropertySale.IncludeOnWebsite = 'True' AND ";
            }
            else if (strminprice.ToString().Replace(",", "") != "" && strminprice.ToString().Replace(",", "") != "0" && strmaxprice.ToString().Replace(",", "") != "" && strmaxprice.ToString().Replace(",", "") != "0")
            {
                strCondition += "(PropertySale.AskingPrice Between " + strminprice.ToString().Replace(",", "") + " AND " + strmaxprice.ToString().Replace(",", "") + " AND PropertySale.IncludeOnWebsite = 'True') AND ";
            }
            else
            {
                strCondition += "(PropertySale.IncludeOnWebsite = 'True') AND ";
            }

            ////if (SSTC != "" && SSTC != "False")
            ////{

            ////    strCondition += "Property.Status='1'";
            ////}
            ////else
            ////{
            ////    strCondition += "Property.Status!='1'";
            ////}

            if (SSTC != "" && SSTC != "True")
            {

                strCondition += "Property.Status!='1'";
            }

            strCondition = strCondition.TrimEnd(new char[] { 'A', 'N', 'D', ' ' });

            sqlQuery = @"SELECT Property.PropertyId,(Select top 1 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId
                    and Files.Category like 'Main photo') As image,PropertyLocation.Address as Address,
                    PropertyLocation.Latitude,PropertyLocation.Longitude,
PropertySale.AskingPrice As Cost, PropertySale.AskingPrice As CostToOrder, PropertySale.PriceSuffix FROM Property Inner Join PropertySale ON (Property.PropertyId = PropertySale.PropertyId and Property.ClientId = PropertySale.ClientId)
Left JOIN PropertyLocation ON (Property.LocationId = PropertyLocation.LocationId and Property.ClientId = PropertyLocation.ClientId)
Left JOIN UserDetails ON (Property.UserName = UserDetails.UserName and Property.ClientId = UserDetails.ClientId) 
 " + strCondition;

        }
        SqlDataAdapter PropertyDA = new SqlDataAdapter(sqlQuery, Conn);
        DataSet ds = new DataSet();
        try
        {

            PropertyDA.Fill(ds);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    arrpropid.Add(ds.Tables[0].Rows[i]["PropertyId"].ToString());


                }
                for (int k = 0; k < arrpropid.Count; k++)
                {
                    if (propid == arrpropid[k].ToString())
                    {
                        arrCnt = k;
                        break;
                    }
                }
                string propertyid = "";
                if (arrCnt == 0)
                {
                    propertyid = "";
                    ds.Tables.Clear();
                }
                else
                {

                    propertyid = ds.Tables[0].Rows[arrCnt - 1]["PropertyId"].ToString();
                    for (int j = 0; j < ds.Tables[0].Rows.Count; j++)
                    {

                        if (propertyid != ds.Tables[0].Rows[j]["PropertyId"].ToString())
                        {
                            ds.Tables[0].Rows.RemoveAt(j);
                            j--;
                            continue;
                        }


                    }

                }





            }
        }
        catch { }
        finally
        {

        }
        return ds;
    }
    public DataSet GetNextPropDataset(string propid, string orderby, string strminprice, string strmaxprice, string strbedrooms, string strlocation, string SSTC, string strbranch)
    {
        SqlConnection Conn = new SqlConnection(strConn);
        string sqlQuery = "";
        string strCondition = "";
        int arrCnt = 0;
        System.Collections.ArrayList arrpropid = new System.Collections.ArrayList();

        strlocation = strbranch;
        if (orderby.Trim() != "" && orderby != "Any")
        {

            strCondition = @"Where Property.ClientId =" + ClientId + " AND (Property.DealType = 1 Or Property.DealType = 4) AND ";


            if (strlocation.Trim() != "" && strlocation != "Any")
            {

                strCondition += "PropertyLocation.LocationAKA='" + strlocation.Replace("'", "|@") + "' AND";

            }
            //if (strbranch.Trim() != "" && strbranch != "Any")
            //{
            //   // strCondition += "UserDetails.BranchName='" + strbranch + "' AND ";
            //}

            if ((strminprice.ToString().Replace(",", "") != "" && strminprice.ToString().Replace(",", "") != "0") && (strmaxprice.ToString().Replace(",", "") == "" || strmaxprice.ToString().Replace(",", "") == "0"))
            {
                strCondition += "PropertySale.AskingPrice >= " + strminprice.ToString().Replace(",", "") + " AND PropertySale.IncludeOnWebsite = 'True' AND ";
            }
            else if ((strminprice.ToString().Replace(",", "") == "" || strminprice.ToString().Replace(",", "") == "0") && (strmaxprice.ToString().Replace(",", "") != "" && strmaxprice.ToString().Replace(",", "") != "0"))
            {
                strCondition += "PropertySale.AskingPrice <= " + strmaxprice.ToString().Replace(",", "") + " AND PropertySale.IncludeOnWebsite = 'True' AND ";
            }
            else if (strminprice.ToString().Replace(",", "") != "" && strminprice.ToString().Replace(",", "") != "0" && strmaxprice.ToString().Replace(",", "") != "" && strmaxprice.ToString().Replace(",", "") != "0")
            {
                strCondition += "(PropertySale.AskingPrice Between " + strminprice.ToString().Replace(",", "") + " AND " + strmaxprice.ToString().Replace(",", "") + " AND PropertySale.IncludeOnWebsite = 'True') AND ";
            }
            else
            {
                strCondition += "(PropertySale.IncludeOnWebsite = 'True') AND ";
            }

            ////if (SSTC != "" && SSTC != "True")
            ////{

            ////    strCondition += "Property.Status='1'";
            ////}
            ////else
            ////{
            ////    strCondition += "Property.Status!='1'";
            ////}

            if (SSTC != "" && SSTC != "True")
            {

                strCondition += "Property.Status!='1'";
            }
            strCondition = strCondition.TrimEnd(new char[] { 'A', 'N', 'D', ' ' });
            ////**************

            sqlQuery = @"SELECT Property.PropertyId,(Select top 1 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId
            and Files.Category like 'Main photo') As image,PropertyLocation.Address as Address ,
            PropertyLocation.Latitude,PropertyLocation.Longitude,
            PropertySale.AskingPrice As Cost, PropertySale.AskingPrice As CostToOrder, PropertySale.PriceSuffix FROM Property Inner Join PropertySale ON (Property.PropertyId = PropertySale.PropertyId and Property.ClientId = PropertySale.ClientId) Left JOIN PropertyLocation ON (Property.LocationId = PropertyLocation.LocationId and Property.ClientId = PropertyLocation.ClientId)
Left JOIN UserDetails ON (Property.UserName = UserDetails.UserName and Property.ClientId = UserDetails.ClientId) 
           " + strCondition + "order by " + orderby;

        }
        else
        {

            strCondition = @"Where Property.ClientId =" + ClientId + " AND (Property.DealType = 1 Or Property.DealType = 4) AND ";


            if (strlocation.Trim() != "" && strlocation != "Any")
            {

                strCondition += "PropertyLocation.LocationAKA='" + strlocation.Replace("'", "|@") + "' AND";

            }

            //if (strbranch.Trim() != "" && strbranch != "Any")
            //{
            //   // strCondition += "UserDetails.BranchName='" + strbranch + "' AND ";
            //}

            if ((strminprice.ToString().Replace(",", "") != "" && strminprice.ToString().Replace(",", "") != "0") && (strmaxprice.ToString().Replace(",", "") == "" || strmaxprice.ToString().Replace(",", "") == "0"))
            {
                strCondition += "PropertySale.AskingPrice >= " + strminprice.ToString().Replace(",", "") + " AND PropertySale.IncludeOnWebsite = 'True' AND ";
            }
            else if ((strminprice.ToString().Replace(",", "") == "" || strminprice.ToString().Replace(",", "") == "0") && (strmaxprice.ToString().Replace(",", "") != "" && strmaxprice.ToString().Replace(",", "") != "0"))
            {
                strCondition += "PropertySale.AskingPrice <= " + strmaxprice.ToString().Replace(",", "") + " AND PropertySale.IncludeOnWebsite = 'True' AND ";
            }
            else if (strminprice.ToString().Replace(",", "") != "" && strminprice.ToString().Replace(",", "") != "0" && strmaxprice.ToString().Replace(",", "") != "" && strmaxprice.ToString().Replace(",", "") != "0")
            {
                strCondition += "(PropertySale.AskingPrice Between " + strminprice.ToString().Replace(",", "") + " AND " + strmaxprice.ToString().Replace(",", "") + " AND PropertySale.IncludeOnWebsite = 'True') AND ";
            }
            else
            {
                strCondition += "(PropertySale.IncludeOnWebsite = 'True') AND ";
            }

            ////if (SSTC != "" && SSTC != "False")
            ////{

            ////    strCondition += "Property.Status='1'";
            ////}
            ////else
            ////{
            ////    strCondition += "Property.Status!='1'";
            ////}

            if (SSTC != "" && SSTC != "True")
            {

                strCondition += "Property.Status!='1'";
            }
            strCondition = strCondition.TrimEnd(new char[] { 'A', 'N', 'D', ' ' });

            sqlQuery = @"SELECT Property.PropertyId,(Select top 1 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId
                    and Files.Category like 'Main photo') As image,PropertyLocation.Address as Address,
                    PropertyLocation.Latitude,PropertyLocation.Longitude,
                    PropertySale.AskingPrice As Cost, PropertySale.AskingPrice As CostToOrder, PropertySale.PriceSuffix FROM Property Inner Join PropertySale ON (Property.PropertyId = PropertySale.PropertyId and Property.ClientId = PropertySale.ClientId)  Left JOIN PropertyLocation ON (Property.LocationId = PropertyLocation.LocationId and Property.ClientId = PropertyLocation.ClientId) 
Left JOIN UserDetails ON (Property.UserName = UserDetails.UserName and Property.ClientId = UserDetails.ClientId) 
                   " + strCondition;

        }
        SqlDataAdapter PropertyDA = new SqlDataAdapter(sqlQuery, Conn);
        DataSet ds = new DataSet();
        try
        {

            PropertyDA.Fill(ds);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    arrpropid.Add(ds.Tables[0].Rows[i]["PropertyId"].ToString());


                }
                for (int k = 0; k < arrpropid.Count; k++)
                {
                    if (propid == arrpropid[k].ToString())
                    {
                        arrCnt = k;
                        break;
                    }
                }
                string propertyid = "";
                if (arrCnt + 1 == ds.Tables[0].Rows.Count)
                {
                    propertyid = "";
                    ds.Tables.Clear();
                }
                else
                {

                    propertyid = ds.Tables[0].Rows[arrCnt + 1]["PropertyId"].ToString();

                    for (int j = 0; j < ds.Tables[0].Rows.Count; j++)
                    {

                        if (propertyid != ds.Tables[0].Rows[j]["PropertyId"].ToString())
                        {
                            ds.Tables[0].Rows.RemoveAt(j);
                            j--;
                            continue;
                        }


                    }
                }

            }
        }
        catch { }
        finally
        {

        }
        return ds;
    }

   

    public DataSet getProperties(string strminprice, string strmaxprice, string minBedRooms, string maxBedRooms, string strlocation, string strRegion, string strproptype, string cityname, ArrayList arrItems, string proptype, string strRefNo, string NH, string LORAtype, string type)
    {
        SqlConnection Conn = new SqlConnection(strConn);
        string sqlQuery = "";
        string strCondition = "";
        string strImgPath = ConfigurationManager.AppSettings["ThumbImagePath"];
        string[] strTemp;
        if (strproptype == "2")
        {
            strCondition = @"Where Property.ClientId =" + ClientId + " AND (Property.DealType = 2 Or Property.DealType = 4) AND PropertyLet.IncludeOnWebsite = 'True' AND Property.Archived = 0 AND ";

            //  if (NH.Trim() != "" && NH != "False")
            // {
            //      strCondition += "Property.IsNewHome='" + NH + "' AND ";
            //  }
            if (strlocation.Trim() != "" && strlocation != "Any")
            {
                strCondition += "PropertyLocation.LocationAKA='" + strlocation.Replace("'", "|@") + "' AND";
            }
            if (strRegion == "Other")
            {
                strCondition += "(PropertyLocation.AreaOrRegionName is null OR PropertyLocation.AreaOrRegionName='') AND ";
            }
            else if (strRegion.Trim() != "" && strRegion != "Any")
            {
                strCondition += "PropertyLocation.AreaOrRegionName in ('" + strRegion + "') AND ";
            }

            if (cityname == "Other")
            {
                strCondition += "(PropertyLocation.CityOrTownName is null OR PropertyLocation.CityOrTownName='') AND ";
            }
            else if (cityname.Trim() != "" && cityname != "Any")
            {
                strCondition += "PropertyLocation.CityOrTownName in ('" + cityname + "') AND ";
            }
            int i = 0;

            if (arrItems != null && arrItems.Count > 0)
            {
                if (arrItems[0].ToString() != null && arrItems[0].ToString() != "" && arrItems[0].ToString() != "Any")
                {
                    strCondition += " (";
                    foreach (string obj in arrItems)
                    {
                        if (LORAtype == "Postcode")
                            strCondition += " PropertyLocation.Postcode like '" + obj.ToString().Replace("'", "|@") + "%' OR ";
                        else
                            strCondition += " PropertyLocation.LocationAKA like '" + obj.ToString().Replace("'", "|@") + "%' OR ";

                    }
                    strCondition = strCondition.TrimEnd(new char[] { 'O', 'R', ' ' });
                    strCondition += " ) AND";

                }
            }


            if (strRefNo.Trim() != "" && strRefNo != "Any")
            {
                strCondition += "Property.Reference='" + strRefNo + "' AND";
            }

            if (proptype != "" && proptype != "Any" && proptype.Contains("_"))
            {
                //     strCondition += "(Property.PropertyMajorType in(" + proptype.Split(new char[] { '_' })[1] + ")) AND ";

                if (proptype.Split(new char[] { '_' })[1] == "1")
                    strCondition += "( (Property.PropertyMajorType in(" + proptype.Split(new char[] { '_' })[1] + ") ) OR (Property.PropertyMajorType = 3 AND Property.PropertySubType in( 6,19))) AND ";
                else
                    strCondition += "(Property.PropertyMajorType in(" + proptype.Split(new char[] { '_' })[1] + ") ) AND ";
            }

            if (proptype != "" && proptype != "Any" && !(proptype.Contains("_")))
            {
                if (proptype != "All Types")
                {
                    strTemp = proptype.Split(new char[] { '_' });
                    strCondition += " ( Property.PropertyMajorType=" + strTemp[1] + " And PropertySubType=" + strTemp[2] + ") ";
                }

            }
            if (proptype != "" && proptype != "Any" && proptype == "House")
            {


                strCondition += " ( Property.PropertyMajorType=1 And PropertySubType in (128,4,22,131,27,26,28,20) ) OR ( Property.PropertyMajorType=2 And PropertySubType in (2,29 ) AND ";


            }

            else if (proptype != "" && proptype != "Any" && proptype == "Bunglow")
            {


                strCondition += " ( Property.PropertyMajorType=1 And PropertySubType = 8 ) OR ( Property.PropertyMajorType = 45 And PropertySubType = 14 ) AND ";


            }
            else if (proptype != "" && proptype != "Any" && proptype == "Flat")
            {


                strCondition += " ( Property.PropertyMajorType=2 And PropertySubType in (8,7,28) ) OR ( Property.PropertyMajorType=47 And PropertySubType =110 )  OR  ( Property.PropertyMajorType=3 And PropertySubType =6 ) ";


            }
            //xxxxxxxxxxxx


            if ((strminprice.ToString().Replace(",", "") != "" && strminprice.ToString().Replace(",", "") != "0") && (strmaxprice.ToString().Replace(",", "") != "" && strmaxprice.ToString().Replace(",", "") == "1000000$"))
            {
                strCondition += " (PropertyLet.Price >= " + strminprice.ToString().Replace(",", "") + ") AND PropertyLet.IncludeOnWebsite = 'True' AND ";

            }
            if ((strminprice.ToString().Replace(",", "") != "" && strminprice.ToString().Replace(",", "") == "0") && (strmaxprice.ToString().Replace(",", "") != "" && strmaxprice.ToString().Replace(",", "") == "1000000$"))
            {
                strCondition += "( PropertyLet.Price > 1000000) AND PropertyLet.IncludeOnWebsite = 'True' AND ";

            }

            //xxxxxxxxxxxx

            else if ((strminprice.ToString().Replace(",", "") != "" && strminprice.ToString().Replace(",", "") != "0") && (strmaxprice.ToString().Replace(",", "") == "" || strmaxprice.ToString().Replace(",", "") == "0"))
            {
                strCondition += "(PropertyLet.Price >= " + strminprice.ToString().Replace(",", "") + ") AND PropertyLet.IncludeOnWebsite = 'True' AND ";
            }
            else if ((strminprice.ToString().Replace(",", "") == "" || strminprice.ToString().Replace(",", "") == "0") && (strmaxprice.ToString().Replace(",", "") != "" && strmaxprice.ToString().Replace(",", "") != "0"))
            {
                strCondition += "(PropertyLet.Price <= " + strmaxprice.ToString().Replace(",", "") + " ) AND PropertyLet.IncludeOnWebsite = 'True' AND ";
            }
            else if (strminprice.ToString().Replace(",", "") != "" && strminprice.ToString().Replace(",", "") != "0" && strmaxprice.ToString().Replace(",", "") != "" && strmaxprice.ToString().Replace(",", "") != "0")
            {
                strCondition += "(PropertyLet.Price Between " + strminprice.ToString().Replace(",", "") + " AND " + strmaxprice.ToString().Replace(",", "") + " ) AND  ";
            }


            strCondition = strCondition.TrimEnd(new char[] { 'A', 'N', 'D', ' ' });
            strCondition = strCondition.TrimEnd(new char[] { 'O', 'R', ' ' });
            // strCondition += " And Status!= 1 and Status!=7"; 
            sqlQuery = @"SELECT Property.PropertyId,Property.DealType,Property.Reference,Property.propertymajortype, Property.propertysubtype,Property.timestampid,Property.IsNewHome,(Select SubValue from SubType Where MajorId=11 and SubId=Property.Status) as Status,
                     CONVERT(VARCHAR(12), Property.timestampid, 107) as PropertyDate ,
                    (Select SubValue from SubType Where MajorId=Property.PropertyMajorType and SubId=Property.PropertySubType) as proptype,
                    (Select majorValue from majorType Where MajorId=Property.PropertyMajorType) as majortype,
                    (Select top 1  Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId and Files.Category like 'Photo link' and Priority!=0 order by Priority asc) As LinkMainImage, 
                    (Select top 1 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId
                    and Files.Category like 'Main photo') As image, PropertyDescriptive.ShortDesc,PropertyLocation.AreaOrRegionName,PropertyLocation.CityOrTownName,PropertyLocation.CountryName,
                    (select count(*) from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId and Files.Category in('Main photo','photo') ) As imagecount,
                    (select count(*) from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId and Files.Category in('Photo link') ) As Linkimagecount, 
                    (SELECT count(*) FROM PropertyRoom Inner JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                    Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=1) as NumberOfBedrooms,
                    (SELECT count(*) FROM PropertyRoom Inner JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                    Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=40) as NumberOfBathrooms,
                    (SELECT count(*) FROM PropertyRoom Inner JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                    Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=6) as NumberOfReceptions,
                    PropertyLocation.Address + ', ' + PropertyLocation.CityOrTownName as Address,PropertyLocation.StreetViewAttributes,
                    PropertyLocation.PropertyNameNumber+', '+PropertyLocation.Address as street, PropertyLocation.PostCode,PropertyLocation.Latitude,PropertyLocation.Longitude,PropertyLocation.ShowGSVOnWebsite,
                    PropertyLet.Price As Cost, PropertyLet.Price As CostToOrder, PropertyLet.PriceSuffix,PropertyLet.PricePer FROM Property Left Join PropertyDescriptive ON 
                    (Property.PropertyId = PropertyDescriptive.PropertyId and Property.ClientId = PropertyDescriptive.ClientId)
                    Left JOIN PropertyLocation ON (Property.LocationId = PropertyLocation.LocationId and Property.ClientId = PropertyLocation.ClientId)
                    Left JOIN UserDetails ON (Property.UserName = UserDetails.UserName and Property.ClientId = UserDetails.ClientId)
                    Inner Join PropertyLet ON (Property.PropertyId = PropertyLet.PropertyId and Property.ClientId = PropertyLet.ClientId) " + strCondition;

        }
        else
        {
            strCondition = @"Where Property.ClientId =" + ClientId + " AND (Property.DealType = 1 Or Property.DealType = 4) AND  PropertySale.IncludeOnWebsite = 'True' AND Property.Archived=0 AND ";
            // if (NH.Trim() != "" && NH != "False")
            // {
            //     strCondition += "Property.IsNewHome='" + NH + "' AND ";
            // }

            if (strlocation.Trim() != "" && strlocation != "Any")
            {
                strCondition += "PropertyLocation.LocationAKA='" + strlocation.Replace("'", "|@") + "' AND";
            }
            if (strRegion == "Other")
            {
                strCondition += "(PropertyLocation.AreaOrRegionName is null OR PropertyLocation.AreaOrRegionName='') AND ";
            }
            else if (strRegion.Trim() != "" && strRegion != "Any")
            {
                strCondition += "PropertyLocation.AreaOrRegionName in ('" + strRegion + "') AND ";
            }

            if (cityname == "Other")
            {
                strCondition += "(PropertyLocation.CityOrTownName is null OR PropertyLocation.CityOrTownName='') AND ";
            }
            else if (cityname.Trim() != "" && cityname != "Any")
            {
                strCondition += "PropertyLocation.CityOrTownName in ('" + cityname + "') AND ";
            }

            //if (cityname != "" && cityname != "Any")
            //{
            //    strCondition = "PropertyLocation.CityOrTownName='" + cityname + "' AND ";
            //}
            //if (postcode.Trim() != "" && postcode != "Any")
            //{
            //    strCondition += "PropertyLocation.PostCode='" + postcode.Replace("'", "|@") + "' AND";
            //}

            if (arrItems != null && arrItems.Count > 0)
            {
                if (arrItems[0].ToString() != null && arrItems[0].ToString() != "" && arrItems[0].ToString() != "Any")
                {
                    strCondition += " (";
                    foreach (string obj in arrItems)
                    {
                        if (LORAtype == "Postcode")
                            strCondition += " PropertyLocation.Postcode like '" + obj.ToString().Replace("'", "|@") + "%' OR ";
                        else
                            strCondition += " PropertyLocation.LocationAKA like '" + obj.ToString().Replace("'", "|@") + "%' OR ";

                    }
                    strCondition = strCondition.TrimEnd(new char[] { 'O', 'R', ' ' });
                    strCondition += " ) AND";

                }
            }



            if (strRefNo.Trim() != "" && strRefNo != "Any")
            {
                strCondition += "Property.Reference='" + strRefNo + "' AND";
            }

            if (proptype != "" && proptype != "Any" && proptype.Contains("_"))
            {
                // strTemp = proptype.Split(new char[] { '_' });
                // strCondition += " ( Property.PropertyMajorType=" + strTemp[1] + " And PropertySubType=" + strTemp[2] + ") ";
                if (proptype.Split(new char[] { '_' })[1] == "1")
                    strCondition += "( (Property.PropertyMajorType in(" + proptype.Split(new char[] { '_' })[1] + ") ) Or (Property.PropertyMajorType = 3 AND Property.PropertySubType in(6,19))) AND ";
                else
                    strCondition += "(Property.PropertyMajorType in(" + proptype.Split(new char[] { '_' })[1] + ") ) AND ";
            }
            else if (proptype != "" && proptype != "Any" && !(proptype.Contains("_")))
            {
                if (proptype != "All Types")
                {
                    strCondition += "(Property.PropertyMajorType in(" + proptype + ")) AND ";
                }

            }
            //xxxxxxxxxxxx

            if (proptype != "" && proptype != "Any" && proptype == "House")
            {


                strCondition += " ( Property.PropertyMajorType=1 And PropertySubType in (128,4,22,131,27,26,28,20) ) OR ( Property.PropertyMajorType=2 And PropertySubType in (2,29 ) ) AND ";


            }

            else if (proptype != "" && proptype != "Any" && proptype == "Bunglow")
            {


                strCondition += " ( Property.PropertyMajorType=1 And PropertySubType = 8 ) OR ( Property.PropertyMajorType = 45 And PropertySubType = 14 ) AND ";


            }
            else if (proptype != "" && proptype != "Any" && proptype == "Flat")
            {


                strCondition += " ( Property.PropertyMajorType=2 And PropertySubType in (8,7,28) ) OR ( Property.PropertyMajorType=47 And PropertySubType =110 )  OR  ( Property.PropertyMajorType=3 And PropertySubType =6 ) ";


            }

            if ((strminprice.ToString().Replace(",", "") != "" && strminprice.ToString().Replace(",", "") != "0") && (strmaxprice.ToString().Replace(",", "") != "" && strmaxprice.ToString().Replace(",", "") == "1000000$"))
            {
                strCondition += " PropertySale.AskingPrice >= " + strminprice.ToString().Replace(",", "") + " AND PropertySale.IncludeOnWebsite = 'True' AND ";

            }
            else if ((strminprice.ToString().Replace(",", "") != "" && strminprice.ToString().Replace(",", "") == "0") && (strmaxprice.ToString().Replace(",", "") != "" && strmaxprice.ToString().Replace(",", "") == "1000000$"))
            {
                strCondition += " PropertySale.AskingPrice > 1000000 AND ";

            }

            //xxxxxxxxxxxx

            else if ((strminprice.ToString().Replace(",", "") != "" && strminprice.ToString().Replace(",", "") != "0") && (strmaxprice.ToString().Replace(",", "") == "" || strmaxprice.ToString().Replace(",", "") == "0"))
            {
                strCondition += "PropertySale.AskingPrice >= " + strminprice.ToString().Replace(",", "") + " AND PropertySale.IncludeOnWebsite = 'True' AND ";
            }
            else if ((strminprice.ToString().Replace(",", "") == "" || strminprice.ToString().Replace(",", "") == "0") && (strmaxprice.ToString().Replace(",", "") != "" && strmaxprice.ToString().Replace(",", "") != "0"))
            {
                strCondition += "PropertySale.AskingPrice <= " + strmaxprice.ToString().Replace(",", "") + "  AND PropertySale.IncludeOnWebsite = 'True' AND ";
            }
            else if (strminprice.ToString().Replace(",", "") != "" && strminprice.ToString().Replace(",", "") != "0" && strmaxprice.ToString().Replace(",", "") != "" && strmaxprice.ToString().Replace(",", "") != "0")
            {
                strCondition += "(PropertySale.AskingPrice Between " + strminprice.ToString().Replace(",", "") + " AND " + strmaxprice.ToString().Replace(",", "") + " )  AND ";
            }
            else
            {
                strCondition += "(PropertySale.IncludeOnWebsite = 'True') AND ";
            }

            strCondition = strCondition.TrimEnd(new char[] { 'A', 'N', 'D', ' ' });
            strCondition = strCondition.TrimEnd(new char[] { 'O', 'R', ' ' });

            // strCondition+=" And Status!= 1 and Status!=7";

            sqlQuery = @"SELECT Property.PropertyId,Property.DealType,Property.Reference,Property.timestampid,Property.IsNewHome,Property.propertymajortype, Property.propertysubtype,(Select SubValue from SubType Where MajorId=11 and SubId=Property.Status) as Status,
                    CONVERT(VARCHAR(12), Property.timestampid, 107) as PropertyDate ,
                    (Select SubValue from SubType Where MajorId=Property.PropertyMajorType and SubId=Property.PropertySubType) as proptype,
                    (Select majorValue from majorType Where MajorId=Property.PropertyMajorType) as majortype,
                    (Select top 1  Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId and Files.Category like 'Photo link' and Priority!=0 order by Priority asc) As LinkMainImage, 
                    (Select top 1 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId
                    and Files.Category like 'Main photo') As image,PropertyLocation.AreaOrRegionName,PropertyLocation.CityOrTownName,PropertyLocation.CountryName,
                    (select count(*) from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId and Files.Category in('Main photo','photo') ) As imagecount,
                    (select count(*) from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId and Files.Category in('Photo link') ) As Linkimagecount, 
                    PropertyDescriptive.ShortDesc,
                    (SELECT count(*) FROM PropertyRoom Inner JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                    Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=1) as NumberOfBedrooms,
                    (SELECT count(*) FROM PropertyRoom Inner JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                    Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=40) as NumberOfBathrooms,
                    (SELECT count(*) FROM PropertyRoom Inner JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                    Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=6) as NumberOfReceptions,
                    PropertyLocation.Address + ', ' + PropertyLocation.CityOrTownName as Address,PropertyLocation.StreetViewAttributes,
                    PropertyLocation.PropertyNameNumber+', '+PropertyLocation.Address as street, PropertyLocation.PostCode,PropertyLocation.Latitude,PropertyLocation.Longitude,PropertyLocation.ShowGSVOnWebsite,
                    PropertySale.AskingPrice As Cost, PropertySale.AskingPrice As CostToOrder, PropertySale.PriceSuffix FROM Property Left Join PropertyDescriptive ON 
                    (Property.PropertyId = PropertyDescriptive.PropertyId and Property.ClientId = PropertyDescriptive.ClientId)
                    Left JOIN PropertyLocation ON (Property.LocationId = PropertyLocation.LocationId and Property.ClientId = PropertyLocation.ClientId)
                    Left JOIN UserDetails ON (Property.UserName = UserDetails.UserName and Property.ClientId = UserDetails.ClientId)
                    Inner Join PropertySale ON (Property.PropertyId = PropertySale.PropertyId and Property.ClientId = PropertySale.ClientId) " + strCondition;
        }

        SqlDataAdapter PropertyDA = new SqlDataAdapter(sqlQuery, Conn);
        DataSet dsRecord = new DataSet();
        //SqlDataReader dr = new SqlDataReader();
        SqlDataReader dr = null;
        int j = 1;
        try
        {

            PropertyDA.Fill(dsRecord);

            if (type == "2")
            {
                return dsRecord;
            }
            else
            {
                if (dsRecord != null && dsRecord.Tables.Count > 0 && dsRecord.Tables[0].Rows.Count > 0)
                {


                    dsRecord.Tables[0].Columns.Add("CostToShow");
                    dsRecord.Tables[0].Columns.Add("image1");
                    dsRecord.Tables[0].Columns.Add("image2");
                    double price = 0;

                    double dblminprice = 0;
                    double dblmaxprice = 0;
                    try
                    {
                        dblminprice = Convert.ToDouble(strminprice);
                        dblmaxprice = Convert.ToDouble(strmaxprice);
                    }
                    catch { }
                    SqlCommand cmd = new SqlCommand();
                    for (int i = 0; i < dsRecord.Tables[0].Rows.Count; i++)
                    {
                        try
                        {
                            try
                            {
                                if (dsRecord.Tables[0].Rows[i]["Linkimagecount"] != null && dsRecord.Tables[0].Rows[i]["Linkimagecount"].ToString() != "" && dsRecord.Tables[0].Rows[i]["Linkimagecount"].ToString() != "0")
                                {
                                    dsRecord.Tables[0].Rows[i]["imagecount"] = dsRecord.Tables[0].Rows[i]["Linkimagecount"].ToString();
                                }

                                if (minBedRooms == "0" && maxBedRooms == "0" && Convert.ToInt32(dsRecord.Tables[0].Rows[i]["NumberOfBedrooms"]) > 0)
                                {
                                    dsRecord.Tables[0].Rows.RemoveAt(i);
                                    i--;
                                    continue;
                                }
                                else if (int.Parse(dsRecord.Tables[0].Rows[i]["NumberOfBedrooms"].ToString()) < int.Parse(minBedRooms))
                                {
                                    dsRecord.Tables[0].Rows.RemoveAt(i);
                                    i--;
                                    continue;
                                }
                                else if (int.Parse(maxBedRooms) > 0 && int.Parse(dsRecord.Tables[0].Rows[i]["NumberOfBedrooms"].ToString()) > int.Parse(maxBedRooms))
                                {
                                    dsRecord.Tables[0].Rows.RemoveAt(i);
                                    i--;
                                    continue;
                                }
                            }
                            catch { }
                            DataRow row = dsRecord.Tables[0].Rows[i];

                            row["Address"] = row["Address"].ToString().Replace("|@", "'").TrimEnd(new char[] { ',', ' ' }).TrimStart(new char[] { ',', ' ' });
                            row["PostCode"] = row["PostCode"].ToString().Replace("|@", "'");

                            if (dsRecord.Tables[0].Columns.Contains("PricePer"))
                            {
                                switch (dsRecord.Tables[0].Rows[i]["PricePer"].ToString())
                                {
                                    case "0": //week
                                        dsRecord.Tables[0].Rows[i]["CostToShow"] = " pcm";
                                        price = Convert.ToDouble(dsRecord.Tables[0].Rows[i]["Cost"]);
                                        price = (price * 52) / 12;
                                        dsRecord.Tables[0].Rows[i]["CostToOrder"] = price;
                                        break;
                                    case "1": //month
                                        dsRecord.Tables[0].Rows[i]["CostToShow"] = " pcm";
                                        price = Convert.ToDouble(dsRecord.Tables[0].Rows[i]["Cost"]);
                                        dsRecord.Tables[0].Rows[i]["CostToOrder"] = price;
                                        break;
                                    case "2": //quarter
                                        dsRecord.Tables[0].Rows[i]["CostToShow"] = " pcm";
                                        price = Convert.ToDouble(dsRecord.Tables[0].Rows[i]["Cost"]);
                                        price = (price / 3);
                                        dsRecord.Tables[0].Rows[i]["CostToOrder"] = price;
                                        break;
                                    case "3": //year
                                        dsRecord.Tables[0].Rows[i]["CostToShow"] = " pcm";
                                        price = Convert.ToDouble(dsRecord.Tables[0].Rows[i]["Cost"]);
                                        price = (price / 12);
                                        dsRecord.Tables[0].Rows[i]["CostToOrder"] = price;
                                        break;
                                    default:
                                        dsRecord.Tables[0].Rows[i]["CostToShow"] = " pcm";
                                        price = Convert.ToDouble(dsRecord.Tables[0].Rows[i]["Cost"]);
                                        dsRecord.Tables[0].Rows[i]["CostToOrder"] = price;
                                        break;
                                }
                            }
                            dsRecord.Tables[0].Rows[i]["Cost"] = dsRecord.Tables[0].Rows[i]["CostToOrder"].ToString();
                            string[] addressparts = dsRecord.Tables[0].Rows[i]["Address"].ToString().Replace("|@", "'").Split(new char[] { ',' });
                            string address = addressparts[0];
                            address += addressparts.Length > 1 ? ", " + addressparts[1] : "";
                            address += addressparts.Length > 2 ? ", " + addressparts[2] : "";
                            address += addressparts.Length > 4 ? ", " + addressparts[4] : "";
                            address = address.TrimEnd(new char[] { ',', ' ' });
                            dsRecord.Tables[0].Rows[i]["Address"] = address;
                            if (strproptype == "2")
                            {
                                if (strminprice == "2000$" && strmaxprice == "3000$" && (price <= Convert.ToDouble("3000")))
                                {
                                    dsRecord.Tables[0].Rows.RemoveAt(i);
                                    i--;
                                    continue;
                                }
                                else if (strminprice == "2000$" && strmaxprice != "0" && (price >= Convert.ToDouble("2000") || price <= Convert.ToDouble(strmaxprice)))
                                {
                                    dsRecord.Tables[0].Rows.RemoveAt(i);
                                    i--;
                                    continue;
                                }
                                else if (strmaxprice == "3000$" && strminprice != "0" && (price >= Convert.ToDouble("3000") || price <= Convert.ToDouble(strminprice)))
                                {
                                    dsRecord.Tables[0].Rows.RemoveAt(i);
                                    i--;
                                    continue;
                                }
                                else if (strmaxprice == "0" && strminprice == "2000$" && price <= Convert.ToDouble("2000"))
                                {
                                    dsRecord.Tables[0].Rows.RemoveAt(i);
                                    i--;
                                    continue;
                                }
                                else if (strminprice == "0" && strmaxprice == "3000$" && price <= Convert.ToDouble("3000"))
                                {
                                    dsRecord.Tables[0].Rows.RemoveAt(i);
                                    i--;
                                    continue;
                                }
                                else if (strmaxprice == "0" && strminprice != "0" && price < Convert.ToDouble(strminprice))
                                {
                                    dsRecord.Tables[0].Rows.RemoveAt(i);
                                    i--;
                                    continue;
                                }
                                else if (strminprice == "0" && strmaxprice != "0" && price >= Convert.ToDouble(strmaxprice))
                                {
                                    dsRecord.Tables[0].Rows.RemoveAt(i);
                                    i--;
                                    continue;
                                }
                                else if (strminprice != "0" && strmaxprice != "0" && (price >= Convert.ToDouble(strmaxprice) || price <= Convert.ToDouble(strminprice)))
                                {
                                    dsRecord.Tables[0].Rows.RemoveAt(i);
                                    i--;
                                    continue;
                                }
                            }
                            string strQuery = "";
                            j = 1;
                            try
                            {
                                if (dsRecord.Tables[0].Rows[i]["LinkMainImage"] != null && dsRecord.Tables[0].Rows[i]["LinkMainImage"].ToString() != "")
                                {
                                    try
                                    {
                                        strQuery = @"SELECT top 2 Files.FileName FROM Files 
                                               WHERE Files.ModuleId='" + dsRecord.Tables[0].Rows[i]["PropertyId"] + "' and Files.Category like 'Photo link' and Files.ClientId=" + ClientId + " and Priority Not in(0,1) Order by Files.Priority Asc";//prority is cheked not in 0,1 to exclude  images with o priority it can be other images like epc and floorplan and 1 is for main image

                                        cmd = new SqlCommand(strQuery, Conn);
                                        if (Conn.State == ConnectionState.Closed)
                                        { Conn.Open(); }
                                        dr = cmd.ExecuteReader();
                                        while (dr.Read())
                                        {
                                            dsRecord.Tables[0].Rows[i]["image" + j] = dr[0].ToString();
                                            j++;
                                        }
                                    }
                                    catch { }
                                    finally
                                    {
                                        if (Conn.State == ConnectionState.Open)
                                        { Conn.Close(); }
                                    }
                                }
                                else
                                {
                                    try
                                    {
                                        strQuery = @"SELECT top 2 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName FROM Files 
                                                WHERE Files.ModuleId='" + dsRecord.Tables[0].Rows[i]["PropertyId"] + "' and Files.Category like 'Photo' and Files.ClientId=" + ClientId + " Order by Files.Priority Asc";
                                        cmd = new SqlCommand(strQuery, Conn);
                                        if (Conn.State == ConnectionState.Closed)
                                        {
                                            Conn.Open();
                                        }
                                        dr = cmd.ExecuteReader();
                                        while (dr.Read())
                                        {
                                            //if (!dsRecord.Tables[0].Columns.Contains("image" + j))
                                            //    dsRecord.Tables[0].Columns.Add("image" + j);

                                            string[] strimagename = dr[0].ToString().Split(new char[] { '.' });
                                            string strImage = strimagename[0];
                                            string strexten = strimagename[1];

                                            dsRecord.Tables[0].Rows[i]["image" + j] = strImgPath + strImage + "_thumb." + strexten;
                                            j++;
                                        }
                                    }
                                    catch
                                    { }
                                    finally
                                    {
                                        if (Conn.State == ConnectionState.Open)
                                        {
                                            Conn.Close();
                                        }
                                    }
                                }
                                //dsRecord.Tables[0].Rows[0]["imagescount"] = j;
                                if (!dr.IsClosed) dr.Close();
                            }
                            catch { }
                        }
                        catch { }
                    }

                }
            }
        }
        catch (Exception ex)
        {
        }
        finally
        {
            if (Conn.State == ConnectionState.Open) { Conn.Close(); }
        }
        return dsRecord;
    }

    public DataSet getbuyProperties(System.Collections.ArrayList arrItems, string strhouse, string strapartment, string strMinPrice,
      string strMaxPrice, string minBedRooms, string maxBedRooms, string searchtype, string refno)
    {
        SqlConnection Conn = new SqlConnection(strConn);
        string strImgPath = ConfigurationManager.AppSettings["ThumbImagePath"];
        string sqlQuery = "";

        string strCondition = "Where Property.ClientId =" + ClientId + " AND ";
        if (refno != "")
        {
            strCondition += "Property.Reference='" + refno + "' AND ";
        }
        strCondition += " (Property.DealType = 1 Or Property.DealType = 4) AND (";

        if (searchtype == "Postcode")
        {
            foreach (object obj in arrItems)
            {
                if (obj != "" && obj != "Any" && obj != null)
                {
                    strCondition += "PropertyLocation.Postcode like '" + obj.ToString().Replace("'", "|@") + "%' OR ";
                }
            }
        }
        else
        {
            foreach (object obj in arrItems)
            {
                if (obj != "" && obj != "Any" && obj != null)
                {
                    strCondition += "PropertyLocation.AreaOrRegionName='" + obj.ToString().Replace("'", "|@") + "' OR ";
                }
                ///strCondition += "PropertyLocation.LocationAKA='" + obj.ToString().Replace("'", "|@") + "' OR ";
            }
        }
        strCondition = strCondition.TrimEnd(new char[] { '(', 'O', 'R', ' ' });
        //if (strCondition.Contains("UserDetails."))
        //{
        //   // strCondition += ") AND ";
        //}
        //Added condition to solve exception while displaying properties as per postcode.
        if (strCondition.Contains("PropertyLocation."))
        {
            strCondition += ") AND ";
        }
        if (strhouse == "true" && strapartment == "false")
        {
            strCondition += " PropertyMajorType = 1 AND ";
        }
        else if (strhouse == "false" && strapartment == "true")
        {
            strCondition += " PropertyMajorType = 2 AND ";
        }
        else if (strhouse == "true" && strapartment == "true")
        {
            strCondition += " (PropertyMajorType = 1 OR PropertyMajorType = 2) AND ";
        }


        if ((strMinPrice.ToString().Replace(",", "") != "" && strMinPrice.ToString().Replace(",", "") != "0") && (strMaxPrice.ToString().Replace(",", "") == "" || strMaxPrice.ToString().Replace(",", "") == "0"))
        {
            strCondition += "PropertySale.AskingPrice >= " + strMinPrice.ToString().Replace(",", "") + " AND PropertySale.IncludeOnWebsite = 'True'";
            strCondition += " AND ";
        }
        else if ((strMinPrice.ToString().Replace(",", "") == "" || strMinPrice.ToString().Replace(",", "") == "0") && (strMaxPrice.ToString().Replace(",", "") != "" && strMaxPrice.ToString().Replace(",", "") != "0"))
        {
            strCondition += "PropertySale.AskingPrice <= " + strMaxPrice.ToString().Replace(",", "") + " AND PropertySale.IncludeOnWebsite = 'True' ";
            strCondition += " AND ";
        }
        else if (strMinPrice.ToString().Replace(",", "") != "" && strMinPrice.ToString().Replace(",", "") != "0" && strMaxPrice.ToString().Replace(",", "") != "" && strMaxPrice.ToString().Replace(",", "") != "0")
        {
            strCondition += "(PropertySale.AskingPrice Between " + strMinPrice.ToString().Replace(",", "") + " AND " + strMaxPrice.ToString().Replace(",", "") + " AND PropertySale.IncludeOnWebsite = 'True')";
            strCondition += " AND ";
        }
        else
        {
            strCondition += " (PropertySale.IncludeOnWebsite = 'True') AND ";
        }

        if (strCondition.EndsWith("AND "))
        {
            strCondition = strCondition.TrimEnd(new char[] { 'A', 'N', 'D', ' ' });
        }

        sqlQuery = @"SELECT Property.PropertyId,Property.DealType,Property.Reference,Property.timestampid,Property.propertymajortype, Property.propertysubtype,(Select SubValue from SubType Where MajorId=11 and SubId=Property.Status) as Status,
                     CONVERT(VARCHAR(12), Property.timestampid, 107) as PropertyDate ,
                    (Select SubValue from SubType Where MajorId=Property.PropertyMajorType and SubId=Property.PropertySubType) as proptype,
                    (Select majorValue from majorType Where MajorId=Property.PropertyMajorType) as majortype,
                    (Select top 1  Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId and Files.Category like 'Photo link' and Priority!=0 order by Priority asc) As LinkMainImage, 
                    (Select top 1 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId
                    and Files.Category like 'Main photo') As image, 
                    (select count(*) from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId and Files.Category in('Main photo','photo') ) As imagecount,
                    (select count(*) from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId and Files.Category in('Photo link') ) As Linkimagecount, 
                    PropertyDescriptive.ShortDesc,
                    (SELECT count(*) FROM PropertyRoom Inner JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                    Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=1) as NumberOfBedrooms,
                    (SELECT count(*) FROM PropertyRoom Inner JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                    Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=40) as NumberOfBathrooms,
                    (SELECT count(*) FROM PropertyRoom Inner JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                    Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=6) as NumberOfReceptions,
                    PropertyLocation.Address + ', '  + PropertyLocation.CityOrTownName as Address,PropertyLocation.AreaOrRegionName,
                    PropertyLocation.PropertyNameNumber+', '+PropertyLocation.Address as street, PropertyLocation.PostCode,PropertyLocation.Latitude,PropertyLocation.Longitude,
                    PropertyLocation.ShowGSVOnWebsite,PropertyLocation.StreetViewAttributes,
                      PropertySale.AskingPrice As Cost, PropertySale.AskingPrice As CostToOrder, PropertySale.PriceSuffix FROM Property Left Join PropertyDescriptive ON 
                    (Property.PropertyId = PropertyDescriptive.PropertyId and Property.ClientId = PropertyDescriptive.ClientId)
                    Left JOIN PropertyLocation ON (Property.LocationId = PropertyLocation.LocationId and Property.ClientId = PropertyLocation.ClientId)
                    Left JOIN UserDetails ON (Property.UserName = UserDetails.UserName and Property.ClientId = UserDetails.ClientId)
                    Inner Join PropertySale ON (Property.PropertyId = PropertySale.PropertyId and Property.ClientId = PropertySale.ClientId) " + strCondition;

        SqlDataAdapter PropertyDA = new SqlDataAdapter(sqlQuery, Conn);
        DataSet ds = new DataSet();
        SqlDataReader dr;
        int j = 1;
        try
        {
            PropertyDA.Fill(ds);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                ds.Tables[0].Columns.Add("CostToShow");
                ds.Tables[0].Columns.Add("image1");
                ds.Tables[0].Columns.Add("image2");

                double price = 0;
                SqlCommand cmd = new SqlCommand();

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    try
                    {
                        try
                        {
                            if (minBedRooms == "0" && maxBedRooms == "0" && Convert.ToInt32(ds.Tables[0].Rows[i]["NumberOfBedrooms"]) > 0)
                            {
                                ds.Tables[0].Rows.RemoveAt(i);
                                i--;
                                continue;
                            }
                            else if (int.Parse(ds.Tables[0].Rows[i]["NumberOfBedrooms"].ToString()) < int.Parse(minBedRooms))
                            {
                                ds.Tables[0].Rows.RemoveAt(i);
                                i--;
                                continue;
                            }
                            else if (int.Parse(maxBedRooms) > 0 && int.Parse(ds.Tables[0].Rows[i]["NumberOfBedrooms"].ToString()) > int.Parse(maxBedRooms))
                            {
                                ds.Tables[0].Rows.RemoveAt(i);
                                i--;
                                continue;
                            }
                        }
                        catch { }
                        DataRow row = ds.Tables[0].Rows[i];

                        row["PostCode"] = row["PostCode"].ToString().Replace("|@", "'");
                    }
                    catch { }
                    try
                    {

                        if (ds.Tables[0].Columns.Contains("PricePer"))
                        {
                            switch (ds.Tables[0].Rows[i]["PricePer"].ToString())
                            {
                                case "1":
                                    ds.Tables[0].Rows[i]["CostToShow"] = " pw";
                                    price = Convert.ToDouble(ds.Tables[0].Rows[i]["Cost"]);
                                    price = (price * 12) / 52;
                                    ds.Tables[0].Rows[i]["CostToOrder"] = Math.Round(price, 2);
                                    break;
                                case "2":
                                    ds.Tables[0].Rows[i]["CostToShow"] = " pw";
                                    price = Convert.ToDouble(ds.Tables[0].Rows[i]["Cost"]);
                                    price = ((price / 6) * 12) / 52;
                                    ds.Tables[0].Rows[i]["CostToOrder"] = Math.Round(price, 2);
                                    break;
                                case "3":
                                    ds.Tables[0].Rows[i]["CostToShow"] = " pw";
                                    price = Convert.ToDouble(ds.Tables[0].Rows[i]["Cost"]);
                                    price = ((price / 12) * 12) / 52;
                                    ds.Tables[0].Rows[i]["CostToOrder"] = Math.Round(price, 2);
                                    break;
                                default:
                                    ds.Tables[0].Rows[i]["CostToShow"] = " pw";
                                    price = Convert.ToDouble(ds.Tables[0].Rows[i]["Cost"]);
                                    ds.Tables[0].Rows[i]["CostToOrder"] = Math.Round(price, 2);
                                    break;
                            }
                        }
                        ds.Tables[0].Rows[i]["Cost"] = ds.Tables[0].Rows[i]["CostToOrder"].ToString();

                        string strQuery = "";
                        j = 1;
                        try
                        {
                            if (ds.Tables[0].Rows[i]["LinkMainImage"] != null && ds.Tables[0].Rows[i]["LinkMainImage"].ToString() != "")
                            {
                                strQuery = @"SELECT top 2 Files.FileName FROM Files 
                             WHERE Files.ModuleId='" + ds.Tables[0].Rows[i]["PropertyId"] + "' and Files.Category like 'Photo link' and Files.ClientId=" + ClientId + " and Priority Not in(0,1) Order by Files.Priority Asc";//prority is cheked not in 0,1 to exclude  images with o priority it can be other images like epc and floorplan and 1 is for main image

                                cmd = new SqlCommand(strQuery, Conn);
                                if (Conn.State == ConnectionState.Closed)
                                {
                                    Conn.Open();
                                }
                                dr = cmd.ExecuteReader();
                                while (dr.Read())
                                {
                                    ds.Tables[0].Rows[i]["image" + j] = dr[0].ToString();
                                    j++;
                                }
                            }
                            else
                            {
                                strQuery = @"SELECT top 2 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName FROM Files 
                             WHERE Files.ModuleId='" + ds.Tables[0].Rows[i]["PropertyId"] + "' and Files.Category like 'Photo' and Files.ClientId=" + ClientId + " Order by Files.Priority Asc";
                                cmd = new SqlCommand(strQuery, Conn);
                                if (Conn.State == ConnectionState.Closed)
                                {
                                    Conn.Open();
                                }
                                dr = cmd.ExecuteReader();
                                while (dr.Read())
                                {
                                    //if (!ds.Tables[0].Columns.Contains("image" + j))
                                    //    ds.Tables[0].Columns.Add("image" + j);

                                    string[] strimagename = dr[0].ToString().Split(new char[] { '.' });
                                    string strImage = strimagename[0];
                                    string strexten = strimagename[1];

                                    ds.Tables[0].Rows[i]["image" + j] = strImgPath + strImage + "_thumb." + strexten;
                                    j++;
                                }
                            }
                            //ds.Tables[0].Rows[0]["imagescount"] = j;
                            if (!dr.IsClosed) dr.Close();
                        }
                        catch { }
                        finally
                        {
                            if (Conn.State == ConnectionState.Open)
                            {
                                Conn.Close();
                            }
                        }

                        string[] addressparts = ds.Tables[0].Rows[i]["Address"].ToString().Replace("|@", "'").Split(new char[] { ',' });
                        string address = addressparts[0];
                        address += addressparts.Length > 1 ? ", " + addressparts[1] : "";
                        address += addressparts.Length > 2 ? ", " + addressparts[2] : "";
                        address += addressparts.Length > 4 ? ", " + addressparts[4] : "";
                        address = address.TrimEnd(new char[] { ',', ' ' });
                        ds.Tables[0].Rows[i]["Address"] = address;
                    }
                    catch { }
                }

            }
        }
        catch
        {
        }
        finally
        {
            if (Conn.State == ConnectionState.Open) { Conn.Close(); }
        }
        return ds;
    }


    public DataSet getLetProperties(System.Collections.ArrayList arrItems, string strhouse, string strapartment, string strMinPrice,
        string strMaxPrice, string minBedRooms, string maxBedRooms, string searchtype, string refno)
    {
        SqlConnection Conn = new SqlConnection(strConn);
        string sqlQuery = "";
        string strImgPath = ConfigurationManager.AppSettings["ThumbImagePath"];

        string strCondition = "Where Property.ClientId=" + ClientId + " ";
        if (refno != "")
        {
            strCondition += "Property.Reference='" + refno + "' AND ";
        }
        strCondition += " AND (Property.DealType = 2 Or Property.DealType = 4) AND (";

        if (searchtype == "Postcode")
        {
            foreach (object obj in arrItems)
            {
                strCondition += "PropertyLocation.Postcode like '" + obj.ToString().Replace("'", "|@") + "%' OR ";
            }
        }
        else
        {
            foreach (object obj in arrItems)
            {
                if (obj.ToString() != null && obj.ToString() != "" && obj.ToString() != "Any")
                    //strCondition += "PropertyLocation.LocationAKA='" + obj.ToString().Replace("'", "|@") + "' OR ";
                    strCondition += "PropertyLocation.AreaOrRegionName='" + obj.ToString().Replace("'", "|@") + "' OR ";
            }
        }
        strCondition = strCondition.TrimEnd(new char[] { '(', 'O', 'R', ' ' });
        if (strCondition.Contains("PropertyLocation."))
        {
            strCondition += ") AND ";
        }
        if (strhouse == "true" && strapartment == "false")
        {
            strCondition += " (PropertyMajorType = 1 AND PropertySubtype = 26) AND ";
        }
        else if (strhouse == "false" && strapartment == "true")
        {
            strCondition += " (PropertyMajorType = 2 AND PropertySubtype = 28) AND ";
        }
        else if (strhouse == "true" && strapartment == "true")
        {
            strCondition += " (PropertyMajorType = 1 AND PropertySubtype = 26) OR (PropertyMajorType = 2 AND PropertySubtype = 28) AND ";
        }

        strCondition += "  (PropertyLet.IncludeOnWebsite = 'True')";

        if (strCondition.EndsWith("AND "))
        {
            strCondition = strCondition.TrimEnd(new char[] { 'A', 'N', 'D', ' ' });
        }
        ////Property.FurtherNotes as Address,
        sqlQuery = @"SELECT Property.PropertyId,Property.DealType,Property.Reference,Property.timestampid,Property.propertymajortype, Property.propertysubtype,(Select SubValue from SubType Where MajorId=11 and SubId=Property.Status) as Status,
                    CONVERT(VARCHAR(12), Property.timestampid, 107) as PropertyDate ,
                    (Select SubValue from SubType Where MajorId=Property.PropertyMajorType and SubId=Property.PropertySubType) as proptype,
                    (Select majorValue from majorType Where MajorId=Property.PropertyMajorType) as majortype,
                    (Select top 1  Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId and Files.Category like 'Photo link' and Priority!=0 order by Priority asc) As LinkMainImage, 
                    (Select top 1 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId
                    and Files.Category like 'Main photo') As image,
                    (select count(*) from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId and Files.Category in('Main photo','photo') ) As imagecount,
                    (select count(*) from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId and Files.Category in('Photo link') ) As Linkimagecount, 
                    PropertyDescriptive.LongDesc, PropertyDescriptive.ShortDesc,PropertyLocation.AreaOrRegionName,
                    (SELECT count(*) FROM PropertyRoom Inner JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                    Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=1) as NumberOfBedrooms,
                    (SELECT count(*) FROM PropertyRoom Inner JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                    Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=40) as NumberOfBathrooms,
                    (SELECT count(*) FROM PropertyRoom Inner JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                    Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=6) as NumberOfReceptions,
                    PropertyLocation.Address + ', ' + PropertyLocation.AreaOrRegionName +', ' + PropertyLocation.CityOrTownName as Address,PropertyLocation.CityOrTownName,PropertyLocation.AreaOrRegionName,
                    PropertyLocation.PropertyNameNumber+', '+PropertyLocation.Address as street,PropertyLocation.LocationAKA,PropertyLocation.Address as Area, PropertyLocation.PostCode,PropertyLocation.Latitude,PropertyLocation.Longitude,PropertyLocation.StreetViewAttributes,
                    PropertyLet.Price As Cost, PropertyLet.Price As CostToOrder, PropertyLet.PricePer FROM Property Left Join PropertyDescriptive ON 
                    (Property.PropertyId = PropertyDescriptive.PropertyId and Property.ClientId = PropertyDescriptive.ClientId)
                    Left JOIN PropertyLocation ON (Property.LocationId = PropertyLocation.LocationId and Property.ClientId = PropertyLocation.ClientId)
                    Inner Join PropertyLet ON (Property.PropertyId = PropertyLet.PropertyId and Property.ClientId = PropertyLet.ClientId) " + strCondition;

        SqlDataAdapter PropertyDA = new SqlDataAdapter(sqlQuery, Conn);
        DataSet ds = new DataSet();
        try
        {

            PropertyDA.Fill(ds);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                ds.Tables[0].Columns.Add("CostToShow");
                ds.Tables[0].Columns.Add("image1");
                ds.Tables[0].Columns.Add("image2");
                double price = 0;
                SqlCommand cmd = new SqlCommand();
                SqlDataReader dr;
                int j = 1;

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    try
                    {
                        try
                        {
                            if (minBedRooms == "0" && maxBedRooms == "0" && Convert.ToInt32(ds.Tables[0].Rows[i]["NumberOfBedrooms"]) > 0)
                            {
                                ds.Tables[0].Rows.RemoveAt(i);
                                i--;
                                continue;
                            }
                            else if (int.Parse(ds.Tables[0].Rows[i]["NumberOfBedrooms"].ToString()) < int.Parse(minBedRooms))
                            {
                                ds.Tables[0].Rows.RemoveAt(i);
                                i--;
                                continue;
                            }
                            else if (int.Parse(maxBedRooms) > 0 && int.Parse(ds.Tables[0].Rows[i]["NumberOfBedrooms"].ToString()) > int.Parse(maxBedRooms))
                            {
                                ds.Tables[0].Rows.RemoveAt(i);
                                i--;
                                continue;
                            }
                        }
                        catch { }
                        DataRow row = ds.Tables[0].Rows[i];
                        row["CostToShow"] = " pw";
                        row["Address"] = row["Address"].ToString().Replace("|@", "'").TrimEnd(new char[] { ',', ' ' }).TrimStart(new char[] { ',', ' ' });
                        row["PostCode"] = row["PostCode"].ToString().Replace("|@", "'");
                    }
                    catch { }
                    try
                    {
                        if (ds.Tables[0].Columns.Contains("PricePer"))
                        {
                            switch (ds.Tables[0].Rows[i]["PricePer"].ToString())
                            {
                                case "0": //week
                                    ds.Tables[0].Rows[i]["CostToShow"] = " pcm";
                                    price = Convert.ToDouble(ds.Tables[0].Rows[i]["Cost"]);
                                    price = (price * 52) / 12;
                                    ds.Tables[0].Rows[i]["CostToOrder"] = price;
                                    break;
                                case "1": //month
                                    ds.Tables[0].Rows[i]["CostToShow"] = " pcm";
                                    price = Convert.ToDouble(ds.Tables[0].Rows[i]["Cost"]);
                                    ds.Tables[0].Rows[i]["CostToOrder"] = price;
                                    break;
                                case "2": //quarter
                                    ds.Tables[0].Rows[i]["CostToShow"] = " pcm";
                                    price = Convert.ToDouble(ds.Tables[0].Rows[i]["Cost"]);
                                    price = (price / 3);
                                    ds.Tables[0].Rows[i]["CostToOrder"] = price;
                                    break;
                                case "3": //year
                                    ds.Tables[0].Rows[i]["CostToShow"] = " pcm";
                                    price = Convert.ToDouble(ds.Tables[0].Rows[i]["Cost"]);
                                    price = (price / 12);
                                    ds.Tables[0].Rows[i]["CostToOrder"] = price;
                                    break;
                                default:
                                    ds.Tables[0].Rows[i]["CostToShow"] = " pcm";
                                    price = Convert.ToDouble(ds.Tables[0].Rows[i]["Cost"]);
                                    ds.Tables[0].Rows[i]["CostToOrder"] = price;
                                    break;
                            }
                        }

                        ds.Tables[0].Rows[i]["Cost"] = ds.Tables[0].Rows[i]["CostToOrder"].ToString();
                        if (strMaxPrice == "0" && strMinPrice != "0" && price < Convert.ToDouble(strMinPrice))
                        {
                            ds.Tables[0].Rows.RemoveAt(i);
                            i--;
                            continue;
                        }
                        else if (strMinPrice == "0" && strMaxPrice != "0" && price > Convert.ToDouble(strMaxPrice))
                        {
                            ds.Tables[0].Rows.RemoveAt(i);
                            i--;
                            continue;
                        }
                        else if (strMinPrice != "0" && strMaxPrice != "0" && (price >= Convert.ToDouble(strMaxPrice) || price <= Convert.ToDouble(strMinPrice)))
                        {
                            ds.Tables[0].Rows.RemoveAt(i);
                            i--;
                            continue;
                        }

                        string strQuery = "";
                        j = 1;
                        try
                        {
                            strQuery = @"SELECT top 2 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName FROM Files 
                             WHERE Files.ModuleId='" + ds.Tables[0].Rows[i]["PropertyId"] + "' and Files.Category like 'Photo' and Files.ClientId=" + ClientId + " Order by Files.Priority Asc";
                            cmd = new SqlCommand(strQuery, Conn);
                            if (Conn.State == ConnectionState.Closed)
                            {
                                Conn.Open();
                            }
                            dr = cmd.ExecuteReader();
                            while (dr.Read())
                            {
                                //if (!ds.Tables[0].Columns.Contains("image" + j))
                                //    ds.Tables[0].Columns.Add("image" + j);

                                string[] strimagename = dr[0].ToString().Split(new char[] { '.' });
                                string strImage = strimagename[0];
                                string strexten = strimagename[1];

                                ds.Tables[0].Rows[i]["image" + j] = strImgPath + strImage + "_thumb." + strexten;
                                j++;
                            }
                            //ds.Tables[0].Rows[0]["imagescount"] = j;
                            if (!dr.IsClosed) dr.Close();
                        }
                        catch { }
                    }
                    catch { }
                    finally
                    {
                        if (Conn.State == ConnectionState.Open)
                        {
                            Conn.Close();
                        }
                    }
                }

            }
        }
        catch
        {
        }
        finally
        {
            if (Conn.State == ConnectionState.Open) { Conn.Close(); }
        }
        return ds;
    }

    public DataSet getOnerDetails(string propid)
    {
        SqlConnection Conn = new SqlConnection(strConn);
        string sqlQuery = "";
        sqlQuery = @"select distinct PropertyActivity.Prikey,PropertyActivity.PropertyId,PropertyActivity.ClientId,DateOfActivity as [Date/Time],
                    [EventId],[Price],[NegotiatorId],[AssignedTo],[Property].[LocationId],[PropertyActivity].[AppointmentId],[Notes],
                    [PropertyLocation].[PropertyNameNumber],[PropertyLocation].Address as Addresss,[PropertyLocation].[CityOrTownName],
                    [PropertyLocation].[locationAka] from ((PropertyActivity inner join [Property] on PropertyActivity.PropertyId = [Property].PropertyId) 
                    Inner join  PropertyLocation on PropertyLocation.LocationId=Property.locationId) where PropertyActivity.clientId=" + ClientId + " and  PropertyActivity.PropertyId = '" + propid + "' order by PropertyActivity.DateOfActivity asc";

        SqlDataAdapter PropertyDA = new SqlDataAdapter(sqlQuery, Conn);

        DataSet ds = new DataSet();
        try
        {

            PropertyDA.Fill(ds);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {

                try
                {
                    ds.Tables[0].Columns.Add("address");
                    ds.Tables[0].Columns.Add("Negotiator");
                    ds.Tables[0].Columns.Add("Event");
                    ds.Tables[0].Columns.Add("EventMainCategory");
                    string strPropertyNameNumber = "";
                    DataView dv = new DataView(ds.Tables[0]);
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        strPropertyNameNumber = "";

                        dv.RowFilter = "LocationId='" + row["LocationId"].ToString() + "' and Address is not null";
                        if (dv.Count > 0)
                        {
                            strPropertyNameNumber = dv[0]["Address"].ToString();
                        }
                        else
                        {

                            if ((row["PropertyNameNumber"] != null) && (row["PropertyNameNumber"].ToString() != ""))
                            {
                                strPropertyNameNumber += row["PropertyNameNumber"].ToString();
                            }
                            if ((row["Addresss"] != null) && (row["Addresss"].ToString() != ""))
                            {
                                if (strPropertyNameNumber.Trim() == "")
                                {
                                    strPropertyNameNumber += row["Addresss"].ToString();
                                }
                                else
                                {
                                    strPropertyNameNumber += ", " + row["Addresss"].ToString();
                                }
                            }
                            if ((row["LocationAKA"] != null) && (row["LocationAKA"].ToString() != ""))
                            {
                                if (strPropertyNameNumber.Trim() == "")
                                {
                                    strPropertyNameNumber += row["LocationAKA"].ToString();
                                }
                                else
                                {
                                    strPropertyNameNumber += ", " + row["LocationAKA"].ToString();
                                }
                            }
                            if ((row["CityOrTownName"] != null) && (row["CityOrTownName"].ToString() != ""))
                            {
                                if (strPropertyNameNumber.Trim() == "")
                                {
                                    strPropertyNameNumber += row["CityOrTownName"].ToString() + " ";
                                }
                                else
                                {
                                    strPropertyNameNumber += ", " + row["CityOrTownName"].ToString() + " ";
                                }
                            }

                        }
                        row["address"] = strPropertyNameNumber;
                        if (row["NegotiatorId"] != null && row["NegotiatorId"].ToString().Trim() != "")
                        {

                            dv.RowFilter = "NegotiatorId='" + row["NegotiatorId"].ToString() + "' and Negotiator is not null";
                            if (dv.Count > 0)
                            {
                                string ssss = dv[0]["NegotiatorId"].ToString();
                                row["Negotiator"] = dv[0]["Negotiator"].ToString();
                            }
                            else
                            {
                                row["Negotiator"] = GetFullNameFromEntryID(row["NegotiatorId"].ToString().Trim(), ClientId);

                            }
                        }
                        if (row["EventId"] != null && row["EventId"].ToString().Trim() != "0")
                        {
                            try
                            {
                                string ApptCategoryId = "";
                                sqlQuery = "select distinct EventSubId,EventSubValue from EventSubType where EventSubId=" + row["EventId"] + " and ClientId=" + ClientId + "";

                                SqlCommand cmd = new SqlCommand(sqlQuery, Conn);
                                if (Conn.State == ConnectionState.Closed)
                                {
                                    Conn.Open();
                                }
                                SqlDataReader dr1 = cmd.ExecuteReader();

                                if (dr1.Read())
                                {
                                    row["Event"] = dr1["EventSubValue"];
                                    ApptCategoryId = "";

                                    string strEvent = dr1["EventSubValue"].ToString();

                                    ApptCategoryId = strEvent;
                                }
                                row["EventMainCategory"] = ApptCategoryId + " events";

                            }
                            catch { }
                            finally
                            {
                                if (Conn.State == ConnectionState.Open)
                                {
                                    Conn.Close();
                                }
                            }
                        }
                    }

                }
                catch
                {

                }
            }
        }
        catch
        {
        }
        finally
        {
            if (Conn.State == ConnectionState.Open) { Conn.Close(); }
        }
        return ds;
    }

    private object GetFullNameFromEntryID(string EntryID, string ClientId)
    {
        string FullName = "";
        SqlConnection Conn = new SqlConnection(strConn);
        string sqlQuery = "";
        if (EntryID != null && EntryID != "")
        {
            sqlQuery = @"Select FirstName,MiddleName,LastName, SubValue From PeopleContacts Left Join SubType On 
                                          PeopleContacts.Title = SubType.SubId And SubType.MajorId = 15 
                                          Where ContactId ='" + EntryID + "' And ClientId = " + ClientId;
            try
            {
                SqlCommand cmd = new SqlCommand(sqlQuery, Conn);
                if (Conn.State == ConnectionState.Closed)
                {
                    Conn.Open();
                }
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {

                    if ((dr["FirstName"] != null) && (dr["FirstName"].ToString() != ""))
                        FullName = dr["FirstName"].ToString() + " ";

                    if ((dr["MiddleName"] != null) && (dr["MiddleName"].ToString() != ""))
                        FullName += dr["MiddleName"].ToString() + " ";
                    if ((dr["LastName"] != null) && (dr["LastName"].ToString() != ""))
                        FullName += dr["LastName"].ToString();
                    if ((dr["subvalue"] != null) && (dr["subvalue"].ToString() != ""))
                    {
                        string subval = dr["subvalue"].ToString().Replace(".", "");

                        FullName += " (" + subval + ")";
                    }

                }


            }
            catch
            {

            }
            finally
            {
                if (Conn.State == ConnectionState.Open)
                {
                    Conn.Close();
                }
            }

        }
        return FullName;
    }

    public DataSet GetImages(string PropId)
    {
        SqlConnection Conn = new SqlConnection(strConn);
        string strImgPath = ConfigurationManager.AppSettings["ImagePath"];
        string sqlQuery = "";
        sqlQuery = "select '" + strImgPath + "'+CAST(ClientId as Varchar(10))+'-'+CAST(PKey as Varchar(50))+'-'+FileName as image from Files where ClientId='" + ClientId + "' and ModuleId='" + PropId + "' and (Category='Main photo' or Category='photo') order by Files.Priority Asc";
        SqlDataAdapter PropertyDA = new SqlDataAdapter(sqlQuery, Conn);
        DataSet ds = new DataSet();
        try
        {

            PropertyDA.Fill(ds);
        }
        catch
        {
        }
        finally
        {

        }
        return ds;
    }

    //new function by sandhya 30 Oct 2014
    public DataSet GetPropertyRoomDimension(string PropId, string strType)
    {

        string strQuery = "select  PropertyRoom.RoomSize1,PropertyRoom.RoomSize2,PropertyRoomMain.RoomId, Subtype.SubValue, ";
        strQuery += "case When(PropertyRoom.RoomSize1 IS Not null and PropertyRoom.RoomSize1<>'') Then ";
        strQuery += "case when(PropertyRoom.RoomSize1  Not like '%x%' and PropertyRoom.RoomSize1  Not like '%X%') Then ";
        strQuery += "(isnull(PropertyRoom.RoomSize1,'') + ' (l) ' + 'x' + isnull(PropertyRoom.RoomSize2,'') + ' (w)') ";
            strQuery +="else ";
            strQuery +="" ;
            strQuery +="isnull(PropertyRoom.RoomSize1,'') ";
            strQuery +="End ";
            strQuery +=" else ";
            strQuery += "'' ";
            strQuery += " End ";
        strQuery += " as [Dimension], ";
            strQuery += " count(PropertyRoomMain.RoomId) ";
            strQuery += " as [Count], ";
            strQuery += " case When(PropertyRoom.RoomSize1 IS Not null and PropertyRoom.RoomSize1<>'') Then ";
            strQuery += " (Subtype.SubValue + ' ' +  convert(varchar(200),PropertyRoom.RoomNumber) + ' -')  ";
            strQuery += " else ";
            strQuery += "'' ";
            strQuery += " End ";
            strQuery += " as [Desc] ";
        strQuery +=" from PropertyRoom,dbo.PropertyRoomMain ";
            strQuery +=" ,SubType ";
            strQuery +=" where PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and ";
            strQuery += " PropertyRoom.ClientId = PropertyRoomMain.ClientId ";
            strQuery += " and PropertyRoomMain.RoomId = Subtype.SubId   ";
            strQuery += " and PropertyRoomMain.PropertyId='"+ PropId +"'  and subtype.majorid=9  ";
            strQuery += "   group by  PropertyRoom.RoomSize1,PropertyRoom.RoomSize2,PropertyRoomMain.RoomId,Subtype.SubValue,PropertyRoom.RoomNumber  order by Subtype.SubValue,PropertyRoom.RoomNumber ";

        SqlConnection Conn = new SqlConnection(strConn);

           SqlDataAdapter PropertyDA = new SqlDataAdapter(strQuery, Conn);
            DataSet dsDimen = new DataSet();
            try
            {
                if (Conn.State == ConnectionState.Closed)
                {
                    Conn.Open();
                }
                PropertyDA.Fill(dsDimen);
            }
            catch { }
            finally
            {
                if (Conn.State == ConnectionState.Open)
                {
                    Conn.Close();
                }
            }
            return dsDimen;

    }

    public DataSet GetPropertyDataset(string PropId, string strType)
    {

        SqlConnection Conn = new SqlConnection(strConn);
        if (strType.ToString() == "2")
        {
            string strQuery = @"SELECT Property.PropertyId,CouncilTax,Property.AvailableDate, Property.InternalArea, Property.TotalArea, Property.Floors, Property.Reference,
                        (Select REPLACE(SubValue,'|@','''') from SubType Where MajorId=11 and SubId=Property.Status) as Status,
                       (Select REPLACE(SubValue,'|@','''') from SubType Where MajorId=Property.PropertyMajorType and SubId=Property.PropertySubType) as proptype,
                       PropertyDescriptive.LongDesc,PropertyDescriptive.ShortDesc, PropertyLocation.CityOrTownName as Address,PropertyLocation.AreaOrRegionName,
                        PropertyLocation.CityOrTownName, PropertyLocation.CountryName,PropertyLocation.AreaOrRegionName,
                        PropertyLocation.PropertyNameNumber+', '+PropertyLocation.Address as street,PropertyLocation.PostCode,PropertyLocation.Latitude,PropertyLocation.ShowGSVOnWebsite,PropertyLocation.Longitude,PropertyLocation.StreetViewAttributes,
                        (Select top 1  Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId and Files.Category like 'Photo link' and Priority!=0 order by Priority asc) As LinkMainImage, 
                       (Select top 1 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId
                       and Files.Category like 'Main photo') As mainimage, PropertyLet.Price as Cost, PropertyLet.PricePer,PropertyLet.Price as CostToOrder,
                        (SELECT count(*) FROM PropertyRoom LEFT JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                       Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=1) as NumberOfBedrooms,
                        (SELECT count(*) FROM PropertyRoom LEFT JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                       Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=40) as NumberOfBathrooms
                       FROM Property Left Join PropertyDescriptive ON (Property.PropertyId = PropertyDescriptive.PropertyId and Property.ClientId = PropertyDescriptive.ClientId)
                       Left Join PropertyLocation ON (Property.LocationId = PropertyLocation.LocationId and Property.ClientId = PropertyLocation.ClientId)
                       Inner Join PropertyLet ON (Property.PropertyId = PropertyLet.PropertyId and Property.ClientId = PropertyLet.ClientId)
                       WHERE Property.ClientId =" + ClientId + " And Property.PropertyId='" + PropId + "'";
            SqlDataAdapter PropertyDA = new SqlDataAdapter(strQuery, Conn);
            DataSet ds = new DataSet();
            try
            {

                PropertyDA.Fill(ds);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    ds.Tables[0].Columns.Add("imagescount");
                    ds.Tables[0].Columns.Add("Floorcount");
                    ds.Tables[0].Columns.Add("CostToShow");
                    ds.Tables[0].Columns.Add("floorplan");
                    ds.Tables[0].Columns.Add("PropEPC");
                    ds.Tables[0].Columns.Add("BranchName");
                    ds.Tables[0].Columns.Add("BranchPhone");
                    ds.Tables[0].Columns.Add("BranchEmailId");
                    ds.Tables[0].Columns.Add("BranchAddress");
                    ds.Tables[0].Columns.Add("video");
                    ds.Tables[0].Columns.Add("AllRooms");
                    ds.Tables[0].Columns.Add("propfeatures");
                    ds.Tables[0].Rows[0]["CostToShow"] = "";
                    SqlCommand cmd;
                    SqlDataReader dr;
                    int j = 1;
                    try
                    {
                        double price = 0;
                        if (ds.Tables[0].Columns.Contains("PricePer"))
                        {
                            switch (ds.Tables[0].Rows[0]["PricePer"].ToString())
                            {
                                case "0": //week
                                    ds.Tables[0].Rows[0]["CostToShow"] = " pcm";
                                    price = Convert.ToDouble(ds.Tables[0].Rows[0]["Cost"]);
                                    price = (price * 52) / 12;
                                    ds.Tables[0].Rows[0]["CostToOrder"] = Math.Round(price);
                                    break;
                                case "1": //month
                                    ds.Tables[0].Rows[0]["CostToShow"] = " pcm";
                                    price = Convert.ToDouble(ds.Tables[0].Rows[0]["Cost"]);
                                    ds.Tables[0].Rows[0]["CostToOrder"] = Math.Round(price);
                                    break;
                                case "2": //quarter
                                    ds.Tables[0].Rows[0]["CostToShow"] = " pcm";
                                    price = Convert.ToDouble(ds.Tables[0].Rows[0]["Cost"]);
                                    price = (price / 3);
                                    ds.Tables[0].Rows[0]["CostToOrder"] = Math.Round(price);
                                    break;
                                case "3": //year
                                    ds.Tables[0].Rows[0]["CostToShow"] = " pcm";
                                    price = Convert.ToDouble(ds.Tables[0].Rows[0]["Cost"]);
                                    price = (price / 12);
                                    ds.Tables[0].Rows[0]["CostToOrder"] = Math.Round(price);
                                    break;
                                default:
                                    ds.Tables[0].Rows[0]["CostToShow"] = " pcm";
                                    price = Convert.ToDouble(ds.Tables[0].Rows[0]["Cost"]);
                                    ds.Tables[0].Rows[0]["CostToOrder"] = Math.Round(price);
                                    break;
                            }
                        }
                        ds.Tables[0].Rows[0]["Cost"] = ds.Tables[0].Rows[0]["CostToOrder"].ToString();
                    }
                    catch { }

                    try
                    {
                        strQuery = @"SELECT (Select SubValue from SubType Where MajorId=9 and SubId=PropertyRoomMain.RoomId) as RoomType, 
                                    (select count(PRoomMainKeyId) from PropertyRoom where PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and 
                                    PropertyRoom.ClientId = PropertyRoomMain.ClientId) as Rcount
                                    FROM   dbo.PropertyRoomMain 
                                    where PropertyRoomMain.PropertyId='" + PropId + "'";

                        cmd = new SqlCommand(strQuery, Conn);
                        if (Conn.State == ConnectionState.Closed)
                        {
                            Conn.Open();
                        }

                        dr = cmd.ExecuteReader();
                        string strRooms = "";
                        while (dr.Read())
                        {
                            strRooms = strRooms + dr["RoomType"] + "(" + dr["Rcount"] + ")" + ";";
                        }
                        dr.Close();

                        ds.Tables[0].Rows[0]["AllRooms"] = strRooms.Remove(strRooms.Length - 1);

                    }
                    catch { }
                    finally
                    {
                        if (Conn.State == ConnectionState.Open)
                        {
                            Conn.Close();
                        }
                    }

                    try
                    {
                        string propfeature = "";
                        strQuery = @"SELECT FeatureDesc  FROM   PropertyFeature  where clientid=" + ClientId + " and PropertyFeature.PropertyId='" + PropId + "' order by propertyfeaturerank asc";

                        cmd = new SqlCommand(strQuery, Conn);
                        if (Conn.State == ConnectionState.Closed)
                        {
                            Conn.Open();
                        }
                        dr = cmd.ExecuteReader();

                        while (dr.Read())
                        {
                            //propfeature = propfeature + "<li style='margin-bottom:5px;padding-left:8px;list-style-position:outside'>" + dr["FeatureDesc"] + "<br/></li>";

                            //code changes by sandhya 29 10 2014
                            //propfeature = propfeature + "<li style='border-bottom: 1px solid #CCCCCC; list-style-position: outside;margin-bottom: 2px;margin-top: 5px;padding-bottom: 6px;padding-left: 8px;'>" + dr["FeatureDesc"] + "<br/></li>";
                            propfeature = propfeature + "<li style='border-bottom: 1px solid #CCCCCC; list-style-position: inside !important ;margin-bottom: 2px;padding-bottom: 6px;list-style: disc !important;margin-left: 15px;'>" + dr["FeatureDesc"] + "<br/></li>";

                        }
                        dr.Close();

                        ds.Tables[0].Rows[0]["propfeatures"] = propfeature.ToString();

                    }
                    catch { }
                    finally
                    {
                        if (Conn.State == ConnectionState.Open)
                        {
                            Conn.Close();
                        }
                    }
                    bool blnLink = false;
                    try
                    {
                        if (ds.Tables[0].Rows[0]["LinkMainImage"] != null && ds.Tables[0].Rows[0]["LinkMainImage"].ToString() != "")
                        {
                            blnLink = true;
                            strQuery = @"SELECT Files.FileName FROM Files 
                             WHERE Files.ModuleId='" + PropId + "' and Files.Category like 'Photo link' and Files.ClientId=" + ClientId + " Order by Files.Priority Asc";
                        }
                        else
                        {
                            strQuery = @"SELECT Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName FROM Files 
                             WHERE Files.ModuleId='" + PropId + "' and Files.Category like 'Photo' and Files.ClientId=" + ClientId + " Order by Files.Priority Asc";
                        }
                        cmd = new SqlCommand(strQuery, Conn);
                        if (Conn.State == ConnectionState.Closed)
                        {
                            Conn.Open();
                        }
                        dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            if (!ds.Tables[0].Columns.Contains("image" + j))
                                ds.Tables[0].Columns.Add("image" + j);
                            ds.Tables[0].Rows[0]["image" + j] = dr[0];
                            j++;
                        }
                        ds.Tables[0].Rows[0]["imagescount"] = j;
                        if (!dr.IsClosed) dr.Close();
                    }
                    catch { }
                    finally
                    {
                        if (Conn.State == ConnectionState.Open)
                        {
                            Conn.Close();
                        }
                    }

                    try
                    {

                        int m = 0;
                        try
                        {

                            if (blnLink)
                            {
                                strQuery = "SELECT Files.FileName FROM Files WHERE  Files.ModuleId='" + PropId + "' and (Files.Category like 'floor plan' or Files.Category like 'Floorplan') and clientid=" + ClientId + "";
                            }
                            else
                            {
                                strQuery = "SELECT Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName FROM Files WHERE  Files.ModuleId='" + PropId + "' and (Files.Category like 'floor plan' or Files.Category like 'Floorplan') and clientid=" + ClientId + "";
                            }

                        //   strQuery  = "SELECT filename FROM Files WHERE Files.ClientId=" + ClientId +
                            //   " and Files.ModuleId='" + PropId + "' and (Files.Category like 'floor plan' or Files.Category like 'Floorplan')";
                           if (Conn.State == ConnectionState.Closed)
                               Conn.Open();
                            cmd = new SqlCommand(strQuery, Conn);
                            dr = cmd.ExecuteReader();
                            while (dr.Read())
                            {
                                if (!ds.Tables[0].Columns.Contains("Floor" + m))
                                    ds.Tables[0].Columns.Add("Floor" + m);
                                ds.Tables[0].Rows[0]["Floor" + m] = dr[0];
                                m++;
                            }
                            ds.Tables[0].Rows[0]["Floorcount"] = m;
                            if (!dr.IsClosed) dr.Close();
                            Conn.Close();
                        }
                        catch { Conn.Close(); }


                        if (blnLink)
                        {
                            strQuery = "SELECT Files.FileName FROM Files WHERE Files.ClientId=" + ClientId + " and Files.ModuleId='" + PropId + "' and (Files.Category like 'Floor plan' or Files.Category like 'Floorplan')";
                        }
                        else
                        {
                            strQuery = "SELECT Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName FROM Files WHERE Files.ClientId=" + ClientId + " and Files.ModuleId='" + PropId + "' and (Files.Category like 'floor plan' or Files.Category like 'Floorplan')";
                        }
                        cmd = new SqlCommand(strQuery, Conn);

                        //sandhya 
                        if (Conn.State == ConnectionState.Closed)
                        {
                            Conn.Open();
                        }
                        ds.Tables[0].Rows[0]["floorplan"] = cmd.ExecuteScalar();

                        strQuery = "SELECT Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName FROM Files WHERE Files.ClientId=" + ClientId + " and Files.ModuleId='" + PropId + "' and (Files.Category like '%Virtual Tour%' or Files.Category like '%Virtual  Tour%')";

                        cmd = new SqlCommand(strQuery, Conn);
                        ds.Tables[0].Rows[0]["video"] = cmd.ExecuteScalar();

                        if (blnLink)
                        {
                            strQuery = "SELECT Files.FileName FROM Files WHERE Files.ClientId=" + ClientId + " and Files.ModuleId='" + PropId + "' and (Files.Category like '%EPC Graph%')";
                        }
                        else
                        {
                            strQuery = "SELECT Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName FROM Files WHERE Files.ClientId=" + ClientId + " and Files.ModuleId='" + PropId + "' and (Files.Category like '%EPC Graph%')";
                        }
                        cmd = new SqlCommand(strQuery, Conn);
                        if (Conn.State == ConnectionState.Closed)
                        {
                            Conn.Open();
                        }
                        ds.Tables[0].Rows[0]["PropEPC"] = cmd.ExecuteScalar();

                        ds.Tables[0].Rows[0]["Address"] = ds.Tables[0].Rows[0]["Address"].ToString().Replace("|@", "'").TrimEnd(new char[] { ',', ' ' }).TrimStart(new char[] { ',', ' ' });
                        ds.Tables[0].Rows[0]["LongDesc"] = ds.Tables[0].Rows[0]["LongDesc"].ToString().Replace("|@", "'").Replace("\r\n", "<br/>");
                        ds.Tables[0].Rows[0]["PostCode"] = ds.Tables[0].Rows[0]["PostCode"].ToString().Replace("|@", "'");
                        ds.Tables[0].Rows[0]["street"] = ds.Tables[0].Rows[0]["street"].ToString().Replace("|@", "'");
                    }
                    catch { }
                    finally
                    {
                        if (Conn.State == ConnectionState.Open)
                        {
                            Conn.Close();
                        }
                    }
                }
            }
            catch
            {
            }
            finally
            {
                if (Conn.State == ConnectionState.Open)
                {
                    Conn.Close();
                }
            }
            return ds;
        }
        else
        {
            string strQuery = @"SELECT Property.PropertyId,CouncilTax,Property.AvailableDate, Property.InternalArea, Property.TotalArea, Property.Floors, Property.Reference,(Select REPLACE(SubValue,'|@','''') from SubType Where MajorId=11 and SubId=Property.Status) as Status,
                       (Select REPLACE(SubValue,'|@','''') from SubType Where MajorId=Property.PropertyMajorType and SubId=Property.PropertySubType) as proptype,
                        (Select  SubValue from SubType Where MajorId=6 and SubId=Property.PropertySubType) as subvalue,
                        (Select SubValue from SubType Where MajorId=27 and SubId=PropertySale.Tenure) as RentType,
                       PropertyDescriptive.LongDesc,PropertyDescriptive.ShortDesc,PropertyLocation.CityOrTownName as Address,
                        PropertyLocation.CityOrTownName, PropertyLocation.CountryName,PropertyLocation.AreaOrRegionName,
                        PropertyLocation.PropertyNameNumber+', '+PropertyLocation.Address as street,PropertyLocation.PostCode,PropertyLocation.Latitude,PropertyLocation.ShowGSVOnWebsite,PropertyLocation.Longitude,PropertyLocation.StreetViewAttributes,PropertyLocation.ShowGSVOnWebsite,
                        (Select top 1  Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId and Files.Category like 'Photo link' and Priority!=0 order by Priority asc) As LinkMainImage,                        
                        (Select top 1 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId
                       and Files.Category like 'Main photo') As mainimage, PropertySale.AskingPrice as Cost,PropertySale.AskingPrice As CostToOrder, PropertySale.PriceSuffix,PropertySale.IncludeOnWebsite,
                       (SELECT count(*) FROM PropertyRoom LEFT JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                       Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=1) as NumberOfBedrooms,
                       (SELECT count(*) FROM PropertyRoom LEFT JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                       Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=40) as NumberOfBathrooms
                       FROM Property Left Join PropertyDescriptive ON (Property.PropertyId = PropertyDescriptive.PropertyId and Property.ClientId = PropertyDescriptive.ClientId)
                       Left Join PropertyLocation ON (Property.LocationId = PropertyLocation.LocationId and Property.ClientId = PropertyLocation.ClientId)
                       Inner Join PropertySale ON (Property.PropertyId = PropertySale.PropertyId and Property.ClientId = PropertySale.ClientId)
                      WHERE Property.ClientId =" + ClientId + " And Property.PropertyId='" + PropId + "'";
            SqlDataAdapter PropertyDA = new SqlDataAdapter(strQuery, Conn);
            DataSet ds = new DataSet();
            try
            {

                PropertyDA.Fill(ds);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    ds.Tables[0].Columns.Add("imagescount");
                    ds.Tables[0].Columns.Add("Floorcount");
                    ds.Tables[0].Columns.Add("CostToShow");
                    ds.Tables[0].Columns.Add("floorplan");
                    ds.Tables[0].Columns.Add("PropEPC");
                    ds.Tables[0].Columns.Add("BranchName");
                    ds.Tables[0].Columns.Add("BranchPhone");
                    ds.Tables[0].Columns.Add("BranchEmailId");
                    ds.Tables[0].Columns.Add("BranchAddress");
                    ds.Tables[0].Columns.Add("video");
                    ds.Tables[0].Columns.Add("AllRooms");
                    ds.Tables[0].Columns.Add("propfeatures");
                    ds.Tables[0].Rows[0]["CostToShow"] = "";
                    SqlCommand cmd;
                    SqlDataReader dr;
                    int j = 1;
                    try
                    {

                    }
                    catch { }
                    bool blnLink = false;
                    try
                    {
                        try
                        {
                            if (blnLink)
                            {
                                strQuery = "SELECT Files.FileName FROM Files WHERE  Files.ModuleId='" + PropId + "' and (Files.Category like 'floor plan' or Files.Category like 'Floorplan') and clientid=" + ClientId + "";
                            }
                            else
                            {
                                strQuery = "SELECT Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName FROM Files WHERE  Files.ModuleId='" + PropId + "' and (Files.Category like 'floor plan' or Files.Category like 'Floorplan') and clientid=" + ClientId + "";
                            }


                          int  m = 0;
                           
                           // strQuery = "SELECT filename FROM Files WHERE Files.ClientId=" + ClientId +
                              //  " and Files.ModuleId='" + PropId + "' and (Files.Category like 'floor plan' or Files.Category like 'Floorplan')";
                            if (Conn.State == ConnectionState.Closed)
                            {
                                Conn.Open();
                            }
                            cmd = new SqlCommand(strQuery, Conn);
                            dr = cmd.ExecuteReader();
                            while (dr.Read())
                            {
                                if (!ds.Tables[0].Columns.Contains("Floor" + m))
                                    ds.Tables[0].Columns.Add("Floor" + m);
                                ds.Tables[0].Rows[0]["Floor" + m] = dr[0];
                                m++;
                            }
                            ds.Tables[0].Rows[0]["Floorcount"] = m;
                            if (!dr.IsClosed) dr.Close();
                            Conn.Close();
                        }
                        catch { Conn.Close(); }


                        if (ds.Tables[0].Rows[0]["LinkMainImage"] != null && ds.Tables[0].Rows[0]["LinkMainImage"].ToString() != "")
                        {
                            blnLink = true;
                            strQuery = @"SELECT Files.FileName FROM Files 
                             WHERE Files.ModuleId='" + PropId + "' and Files.Category like 'Photo link' and Files.ClientId=" + ClientId + " Order by Files.Priority Asc";
                        }
                        else
                        {
                            strQuery = @"SELECT Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName FROM Files 
                             WHERE Files.ModuleId='" + PropId + "' and Files.Category like 'Photo' and Files.ClientId=" + ClientId + " Order by Files.Priority Asc";
                        }
                        cmd = new SqlCommand(strQuery, Conn);
                        if (Conn.State == ConnectionState.Closed)
                        {
                            Conn.Open();
                        }
                        dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            if (!ds.Tables[0].Columns.Contains("image" + j))
                                ds.Tables[0].Columns.Add("image" + j);
                            ds.Tables[0].Rows[0]["image" + j] = dr[0];
                            j++;
                        }
                        ds.Tables[0].Rows[0]["imagescount"] = j;
                        //ds.Tables[0].Rows[0]["imagescount"] = j - 1;
                        if (!dr.IsClosed) dr.Close();
                    }
                    catch { }
                    finally
                    {
                        if (Conn.State == ConnectionState.Open)
                        {
                            Conn.Close();
                        }
                    }

                    try
                    {
                        strQuery = "Select BranchName,PhoneNo,EmailId,Address from Branches where BranchName in (select BranchName from UserDetails where UserName in( select UserName from Property where  Property.PropertyId='" + PropId + "' and Property.clientid = UserDetails.clientid))and  ClientId =" + ClientId + "";

                        cmd = new SqlCommand(strQuery, Conn);
                        if (Conn.State == ConnectionState.Closed)
                        {
                            Conn.Open();
                        }

                        dr = cmd.ExecuteReader();

                        while (dr.Read())
                        {
                            ds.Tables[0].Rows[0]["BranchName"] = dr["BranchName"].ToString();
                            ds.Tables[0].Rows[0]["BranchPhone"] = dr["PhoneNo"].ToString();
                            ds.Tables[0].Rows[0]["BranchEmailId"] = dr["EmailId"].ToString();
                            ds.Tables[0].Rows[0]["BranchAddress"] = dr["Address"].ToString();
                        }
                        dr.Close();
                    }
                    catch { }
                    finally
                    {
                        if (Conn.State == ConnectionState.Open)
                        {
                            Conn.Close();
                        }
                    }


                    try
                    {
                        strQuery = @"SELECT (Select SubValue from SubType Where MajorId=9 and SubId=PropertyRoomMain.RoomId) as RoomType, (select count(PRoomMainKeyId) from PropertyRoom where PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId) as Rcount
                    FROM   dbo.PropertyRoomMain 
                    where PropertyRoomMain.PropertyId='" + PropId + "' and PropertyRoomMain.ClientId=" + ClientId;

                        cmd = new SqlCommand(strQuery, Conn);
                        if (Conn.State == ConnectionState.Closed)
                        {
                            Conn.Open();
                        }
                        dr = cmd.ExecuteReader();
                        string strRooms = "";
                        while (dr.Read())
                        {
                            strRooms = strRooms + dr["RoomType"] + "(" + dr["Rcount"] + ")" + ";";

                        }

                        dr.Close();
                        ds.Tables[0].Rows[0]["AllRooms"] = strRooms.Remove(strRooms.Length - 1);


                    }


                    catch
                    {

                    }
                    finally
                    {
                        if (Conn.State == ConnectionState.Open)
                        {
                            Conn.Close();
                        }
                    }



                    try
                    {

                        string propfeature = "";
                        strQuery = @"SELECT FeatureDesc  FROM   PropertyFeature  where PropertyFeature.PropertyId='" + PropId + "' and clientid=" + ClientId + " order by propertyfeaturerank asc";



                        cmd = new SqlCommand(strQuery, Conn);
                        if (Conn.State == ConnectionState.Closed)
                        {
                            Conn.Open();
                        }

                        dr = cmd.ExecuteReader();

                        while (dr.Read())
                        {
                            //propfeature = propfeature + "<li style='margin-left:13px;padding-left:8px;list-style-position:outside'>" + dr["FeatureDesc"] + "<br/></li>";

                            //comented and changed by sandhya 30 Oct 2014
                            //propfeature = propfeature + "<li style='border-bottom: 1px solid #CCCCCC; list-style-position: outside;margin-bottom: 2px;margin-top: 5px;padding-bottom: 6px;padding-left: 8px;'>" + dr["FeatureDesc"] + "<br/></li>";
                            propfeature = propfeature + "<li style='border-bottom: 1px solid #CCCCCC; list-style-position: inside !important ;margin-bottom: 2px;padding-bottom: 6px;list-style: disc !important;margin-left: 15px;'>" + dr["FeatureDesc"] + "<br/></li>";

                            //border-bottom: 1px solid #CCCCCC; list-style-position: outside;margin-bottom: 2px;margin-top: 5px;padding-bottom: 6px;padding-left: 8px;
                        }
                        dr.Close();

                        ds.Tables[0].Rows[0]["propfeatures"] = propfeature.ToString();

                    }
                    catch { }
                    finally
                    {
                        if (Conn.State == ConnectionState.Open)
                        {
                            Conn.Close();
                        }
                    }


                    try
                    {
                        if (blnLink)
                        {
                            strQuery = "SELECT Files.FileName FROM Files WHERE  Files.ModuleId='" + PropId + "' and (Files.Category like 'floor plan' or Files.Category like 'Floorplan') and clientid=" + ClientId + "";
                        }
                        else
                        {
                            strQuery = "SELECT Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName FROM Files WHERE  Files.ModuleId='" + PropId + "' and (Files.Category like 'floor plan' or Files.Category like 'Floorplan') and clientid=" + ClientId + "";
                        }
                        cmd = new SqlCommand(strQuery, Conn);
                        if (Conn.State == ConnectionState.Closed)
                        {
                            Conn.Open();
                        }
                        ds.Tables[0].Rows[0]["floorplan"] = cmd.ExecuteScalar();

                        strQuery = "SELECT Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName FROM Files WHERE Files.ModuleId='" + PropId + "' and (Files.Category like '%Virtual Tour%' or Files.Category like '%Virtual  Tour%') and clientid=" + ClientId + "";
                        cmd = new SqlCommand(strQuery, Conn);
                        ds.Tables[0].Rows[0]["video"] = cmd.ExecuteScalar();

                        if (blnLink)
                        {
                            strQuery = "SELECT Files.FileName FROM Files WHERE Files.ModuleId='" + PropId + "'  and (Files.Category like '%EPC Graph%') and clientid=" + ClientId + "";
                        }
                        else
                        {
                            strQuery = "SELECT Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName FROM Files WHERE Files.ModuleId='" + PropId + "'  and (Files.Category like '%EPC Graph%') and clientid=" + ClientId + "";
                        }
                        cmd = new SqlCommand(strQuery, Conn);
                        ds.Tables[0].Rows[0]["PropEPC"] = cmd.ExecuteScalar();

                        ds.Tables[0].Rows[0]["Address"] = ds.Tables[0].Rows[0]["Address"].ToString().Replace("|@", "'").TrimEnd(new char[] { ',', ' ' }).TrimStart(new char[] { ',', ' ' });
                        ds.Tables[0].Rows[0]["LongDesc"] = ds.Tables[0].Rows[0]["LongDesc"].ToString().Replace("|@", "'").Replace("\r\n", "<br/>");
                        ds.Tables[0].Rows[0]["PostCode"] = ds.Tables[0].Rows[0]["PostCode"].ToString().Replace("|@", "'");
                        ds.Tables[0].Rows[0]["street"] = ds.Tables[0].Rows[0]["street"].ToString().Replace("|@", "'");

                    }
                    catch { }
                    finally
                    {
                        if (Conn.State == ConnectionState.Open)
                        {
                            Conn.Close();
                        }
                    }
                }
            }
            catch
            {
            }
            finally
            {
                if (Conn.State == ConnectionState.Open)
                {
                    Conn.Close();
                }
            }
            return ds;
        }
    }

    public DataSet GetPropertyAddress(string PropId, string strType)
    {
        DataSet ds = new DataSet();
        SqlConnection Conn = new SqlConnection(strConn);
        if (strType.ToString() == "2")
        {
            string strQuery = @"SELECT Property.PropertyId,CouncilTax,Property.AvailableDate, Property.InternalArea, Property.TotalArea, Property.Floors, Property.Reference,
                        (Select REPLACE(SubValue,'|@','''') from SubType Where MajorId=11 and SubId=Property.Status) as Status,
                       (Select REPLACE(SubValue,'|@','''') from SubType Where MajorId=Property.PropertyMajorType and SubId=Property.PropertySubType) as proptype,
                       PropertyDescriptive.LongDesc,PropertyDescriptive.ShortDesc, PropertyLocation.CityOrTownName as Address,PropertyLocation.AreaOrRegionName,
                        PropertyLocation.CityOrTownName,PropertyLocation.CountryName,
                        PropertyLocation.PropertyNameNumber+', '+PropertyLocation.Address as street,PropertyLocation.PostCode,PropertyLocation.Latitude,PropertyLocation.ShowGSVOnWebsite,PropertyLocation.Longitude,PropertyLocation.StreetViewAttributes,
                       (Select top 1 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId
                       and Files.Category like 'Main photo') As mainimage, PropertyLet.Price as Cost, PropertyLet.PricePer,PropertyLet.Price as CostToOrder,
                        (SELECT count(*) FROM PropertyRoom LEFT JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                       Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=1) as NumberOfBedrooms,
                        (SELECT count(*) FROM PropertyRoom LEFT JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                       Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=40) as NumberOfBathrooms
                       FROM Property Left Join PropertyDescriptive ON (Property.PropertyId = PropertyDescriptive.PropertyId and Property.ClientId = PropertyDescriptive.ClientId)
                       Left Join PropertyLocation ON (Property.LocationId = PropertyLocation.LocationId and Property.ClientId = PropertyLocation.ClientId)
                       Inner Join PropertyLet ON (Property.PropertyId = PropertyLet.PropertyId and Property.ClientId = PropertyLet.ClientId)
                       WHERE Property.ClientId =" + ClientId + " And Property.PropertyId='" + PropId + "'";
            SqlDataAdapter PropertyDA = new SqlDataAdapter(strQuery, Conn);

            try
            {

                PropertyDA.Fill(ds);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    ds.Tables[0].Columns.Add("imagescount");
                    ds.Tables[0].Columns.Add("CostToShow");
                    ds.Tables[0].Rows[0]["CostToShow"] = "";
                    try
                    {
                        double price = 0;
                        if (ds.Tables[0].Columns.Contains("PricePer"))
                        {
                            switch (ds.Tables[0].Rows[0]["PricePer"].ToString())
                            {
                                case "0": //week
                                    ds.Tables[0].Rows[0]["CostToShow"] = " pcm";
                                    price = Convert.ToDouble(ds.Tables[0].Rows[0]["Cost"]);
                                    price = (price * 52) / 12;
                                    ds.Tables[0].Rows[0]["CostToOrder"] = price;
                                    break;
                                case "1": //month
                                    ds.Tables[0].Rows[0]["CostToShow"] = " pcm";
                                    price = Convert.ToDouble(ds.Tables[0].Rows[0]["Cost"]);
                                    ds.Tables[0].Rows[0]["CostToOrder"] = price;
                                    break;
                                case "2": //quarter
                                    ds.Tables[0].Rows[0]["CostToShow"] = " pcm";
                                    price = Convert.ToDouble(ds.Tables[0].Rows[0]["Cost"]);
                                    price = (price / 3);
                                    ds.Tables[0].Rows[0]["CostToOrder"] = price;
                                    break;
                                case "3": //year
                                    ds.Tables[0].Rows[0]["CostToShow"] = " pcm";
                                    price = Convert.ToDouble(ds.Tables[0].Rows[0]["Cost"]);
                                    price = (price / 12);
                                    ds.Tables[0].Rows[0]["CostToOrder"] = price;
                                    break;
                                default:
                                    ds.Tables[0].Rows[0]["CostToShow"] = " pcm";
                                    price = Convert.ToDouble(ds.Tables[0].Rows[0]["Cost"]);
                                    ds.Tables[0].Rows[0]["CostToOrder"] = price;
                                    break;
                            }
                        }
                        ds.Tables[0].Rows[0]["Cost"] = ds.Tables[0].Rows[0]["CostToOrder"].ToString();
                    }
                    catch { }

                    try
                    {
                        ds.Tables[0].Rows[0]["Address"] = ds.Tables[0].Rows[0]["Address"].ToString().Replace("|@", "'").TrimEnd(new char[] { ',', ' ' }).TrimStart(new char[] { ',', ' ' });
                        ds.Tables[0].Rows[0]["PostCode"] = ds.Tables[0].Rows[0]["PostCode"].ToString().Replace("|@", "'");
                        ds.Tables[0].Rows[0]["street"] = ds.Tables[0].Rows[0]["street"].ToString().Replace("|@", "'");
                    }
                    catch { }
                    finally { }
                }
            }
            catch
            {
            }
            finally
            {

            }
        }
        return ds;
    }
    public DataSet GetPropertyForLocalmap(string PropId, string strType)
    {
        SqlConnection Conn = new SqlConnection(strConn);
        string strQuery = @"SELECT distinct Property.PropertyId,Property.Reference,Property.DealType,(Select REPLACE(SubValue,'|@','''') from SubType Where MajorId=11 and SubId=Property.Status) as Status,
                           (Select REPLACE(SubValue,'|@','''') from SubType Where MajorId=Property.PropertyMajorType and SubId=Property.PropertySubType) as proptype,
                       PropertyDescriptive.ShortDesc, PropertyLocation.Address + ', ' + PropertyLocation.AreaOrRegionName +', ' + PropertyLocation.CityOrTownName as Address,PropertyLocation.Latitude,PropertyLocation.Longitude,
                        (Select top 1  Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId and Files.Category like 'Photo link' and Priority!=0 order by Priority asc) As LinkMainImage,                        
                        (Select top 1 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId
                       and Files.Category like 'Main photo') As mainimage,PropertyLocation.PostCode,PropertyLocation.AreaOrRegionName,
                        PropertyLocation.CityOrTownName, PropertyLocation.CountryName,
                       (SELECT count(*) FROM PropertyRoom LEFT JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                       Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=1) as NumberOfBedrooms
                       FROM Property Left Join PropertyDescriptive ON (Property.PropertyId = PropertyDescriptive.PropertyId and Property.ClientId = PropertyDescriptive.ClientId)
                       Left Join PropertyLocation ON (Property.LocationId = PropertyLocation.LocationId and Property.ClientId = PropertyLocation.ClientId)
                      WHERE Property.ClientId =" + ClientId + " And Property.PropertyId='" + PropId + "'";


        SqlDataAdapter PropertyDA = new SqlDataAdapter(strQuery, Conn);
        DataSet ds = new DataSet();
        try
        {

            PropertyDA.Fill(ds);
        }
        catch
        {
        }
        finally
        {

        }

        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            ds.Tables[0].Columns.Add("Cost");
            ds.Tables[0].Columns.Add("PriceSuffix");
            ds.Tables[0].Columns.Add("CostToShow");
            ds.Tables[0].Rows[0]["CostToShow"] = "";
            ds.Tables[0].Columns.Add("imagescount");
            try
            {
                SqlCommand cmd;
                SqlDataReader dr;
                int j = 1;
                if (Conn.State == ConnectionState.Closed)
                {
                    Conn.Open();
                }

                if (ds.Tables[0].Rows[0]["DealType"].ToString().Trim() == "1")
                {
                    strQuery = @"SELECT AskingPrice, PriceSuffix FROM PropertySale where PropertyId='" + ds.Tables[0].Rows[0]["PropertyId"] + "' and clientid='" + ClientId + "'";
                    cmd = new SqlCommand(strQuery, Conn);
                    dr = cmd.ExecuteReader();
                    if (dr.Read())
                    {
                        ds.Tables[0].Rows[0]["Cost"] = dr["AskingPrice"];
                        ds.Tables[0].Rows[0]["PriceSuffix"] = dr["PriceSuffix"];
                        dr.Close();
                    }
                }
                else
                {
                    strQuery = @"SELECT Price, PriceSuffix FROM PropertyLet where PropertyId='" + ds.Tables[0].Rows[0]["PropertyId"] + "' and clientid='" + ClientId + "'";
                    cmd = new SqlCommand(strQuery, Conn);
                    dr = cmd.ExecuteReader();
                    if (dr.Read())
                    {
                        ds.Tables[0].Rows[0]["Cost"] = dr["Price"];
                        ds.Tables[0].Rows[0]["PriceSuffix"] = dr["PriceSuffix"];
                        dr.Close();
                    }
                    strQuery = @"SELECT SubType.SubValue FROM MajorType INNER JOIN
                                    SubType ON MajorType.MajorId = SubType.MajorId INNER JOIN
                                    PropertyLet ON SubType.SubId = PropertyLet.PricePer where MajorType.MajorValue='PricePer'
                                    and PropertyLet.PropertyId='" + ds.Tables[0].Rows[0]["PropertyId"] + "'";
                    cmd = new SqlCommand(strQuery, Conn);
                    object objPricePer = cmd.ExecuteScalar();

                    double price = 0;
                    switch (objPricePer.ToString())
                    {
                        case "Week": //week
                            ds.Tables[0].Rows[0]["CostToShow"] = " pcm";
                            price = Convert.ToDouble(ds.Tables[0].Rows[0]["Cost"]);
                            price = (price * 52) / 12;
                            ds.Tables[0].Rows[0]["Cost"] = price;
                            break;
                        case "Month": //month
                            ds.Tables[0].Rows[0]["CostToShow"] = " pcm";
                            price = Convert.ToDouble(ds.Tables[0].Rows[0]["Cost"]);
                            ds.Tables[0].Rows[0]["Cost"] = price;
                            break;
                        case "Quarter": //quarter
                            ds.Tables[0].Rows[0]["CostToShow"] = " pcm";
                            price = Convert.ToDouble(ds.Tables[0].Rows[0]["Cost"]);
                            price = (price / 3);
                            ds.Tables[0].Rows[0]["Cost"] = price;
                            break;
                        case "Year": //year
                            ds.Tables[0].Rows[0]["CostToShow"] = " pcm";
                            price = Convert.ToDouble(ds.Tables[0].Rows[0]["Cost"]);
                            price = (price / 12);
                            ds.Tables[0].Rows[0]["Cost"] = price;
                            break;
                        default:
                            ds.Tables[0].Rows[0]["CostToShow"] = " pcm";
                            price = Convert.ToDouble(ds.Tables[0].Rows[0]["Cost"]);
                            ds.Tables[0].Rows[0]["Cost"] = price;
                            break;
                    }
                }

                try
                {
                    if (ds.Tables[0].Rows[0]["LinkMainImage"] != null && ds.Tables[0].Rows[0]["LinkMainImage"].ToString() != "")
                    {
                        strQuery = @"SELECT Files.FileName FROM Files 
                             WHERE Files.ModuleId='" + PropId + "' and Files.Category like 'Photo link' and Files.ClientId=" + ClientId + " Order by Files.Priority Asc";
                    }
                    else
                    {
                        strQuery = @"SELECT Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName FROM Files 
                             WHERE Files.ModuleId='" + PropId + "' and clientid='" + ClientId + "' and Files.Category in('Photo','Main photo') Order by Files.Priority Asc";
                    }
                    cmd = new SqlCommand(strQuery, Conn);
                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        if (!ds.Tables[0].Columns.Contains("image" + j))
                            ds.Tables[0].Columns.Add("image" + j);
                        ds.Tables[0].Rows[0]["image" + j] = dr[0];
                        j++;
                    }
                    ds.Tables[0].Rows[0]["imagescount"] = j;
                    if (!dr.IsClosed) dr.Close();
                }
                catch { }


                ds.Tables[0].Rows[0]["Address"] = ds.Tables[0].Rows[0]["Address"].ToString().Replace("|@", "'").TrimEnd(new char[] { ',', ' ' }).TrimStart(new char[] { ',', ' ' });
                ds.Tables[0].Rows[0]["ShortDesc"] = ds.Tables[0].Rows[0]["ShortDesc"].ToString().Replace("|@", "'");
            }
            catch { }



            finally
            {
                if (Conn.State == ConnectionState.Open)
                {
                    Conn.Close();
                }
            }
        }

        return ds;
    }

    //Emails For LMS 

    public bool SendEmailList(string strTo, string strSubject, string strBody)
    {
        #region comment
        //bool blndone = false;
        //try
        //{
        //    System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage(ConfigurationManager.AppSettings["FromEmailId"].ToString(), strTo);

        //    msg.Subject = strSubject;
        //    msg.Body = strBody;
        //    msg.Priority = System.Net.Mail.MailPriority.High;
        //    msg.IsBodyHtml = true;
        //    System.Net.Mail.SmtpClient se = new System.Net.Mail.SmtpClient(ConfigurationManager.AppSettings["SmtpServer"].ToString());
        //    se.Send(msg);
        //    blndone = true;
        //}
        //catch { blndone = false; }
        //return blndone;
        #endregion comment
        bool blndone = false;
        try
        {
           // strTo = "james@estatesolutions.co.uk";
            MailMessage mMailMessage = new MailMessage();
            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
            msg.From = new MailAddress(ConfigurationManager.AppSettings["FromEmailId"]);

            msg.To.Add(new MailAddress(strTo));
            msg.Subject = strSubject;
            msg.Body = strBody;
            msg.IsBodyHtml = true;
            msg.Priority = MailPriority.Normal;
           // System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("mail.estatesolutions.co.uk");
            System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("213.171.196.58");
            mSmtpClient.UseDefaultCredentials = false;
            //System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("test@estatesolutions.co.uk", "aoNq74%6");
            System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("LMS@esolutionsgroup.co.uk", "Z0ngo1&2");
            mSmtpClient.Credentials = loginInfo;
            mSmtpClient.Send(msg);
            blndone = true;
        }
        catch { blndone = false; }
        return blndone;
    }
    //By sushama For sending emails from lms_add_leave page
    public static bool sendEmailLMS(string strTo, string strSubject, string strBody)
    {

        //******************************LMS Mail Format*****************************************************
        bool blndone = false;
        try
        {
            MailMessage mMailMessage = new MailMessage();
            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
            msg.From = new MailAddress(ConfigurationManager.AppSettings["FHSHR"]);

            string[] strToEmails = strTo.Split(';');
            for (int i = 0; i < strToEmails.Length; i++)
            {
                msg.To.Add(new MailAddress(strToEmails[i].ToString().Trim()));
            }

            msg.Subject = strSubject;
            msg.Body = strBody;
            msg.IsBodyHtml = true;
            msg.Priority = MailPriority.Normal;


            ////System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("213.171.196.58");
            ////mSmtpClient.UseDefaultCredentials = false;
            ////System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("LMS@esolutionsgroup.co.uk", "Z0ngo1&2");

            System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("213.171.222.69");
            mSmtpClient.UseDefaultCredentials = false;
            System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("LMS@esolutionsgroup.co.uk", "Z0ngo1&2");// used only for esolutions site

            mSmtpClient.Credentials = loginInfo;
            mSmtpClient.Send(msg);

            blndone = true;
        }
        catch { blndone = false; }
        return blndone;
        //************************************************************************************

    }

    public static bool sendEmailLMSNew(string strTo, string strSubject, string strBody)
    {
        DatabaseHelperEmail dh = new DatabaseHelperEmail();
        //******************************LMS Mail Format*****************************************************
        bool blndone = false;
        try
        {
            MailMessage mMailMessage = new MailMessage();
            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
            msg.From = new MailAddress(ConfigurationManager.AppSettings["FHSHR"]);
            //msg.From = new MailAddress("rutujak@esolutionsgroup.co.uk");

            string[] strToEmails = strTo.Split(';');
            for (int i = 0; i < strToEmails.Length; i++)
            {
                string uname = dh.fngetUsername(strToEmails[i].ToString(),1);
                if (uname != null && uname != "")
                {
                    if (i == 0)
                    {
                        msg.To.Add(new MailAddress(strToEmails[i].ToString().Trim()));
                    }
                    else if (i > 0)
                    {
                        msg.CC.Add(new MailAddress(strToEmails[i].ToString().Trim(),uname));
                    }
                }
   
            }
            msg.Subject = strSubject;
            msg.Body = strBody;
            msg.IsBodyHtml = true;
            msg.Priority = MailPriority.Normal;


            ////System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("213.171.196.58");
            ////mSmtpClient.UseDefaultCredentials = false;
            ////System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("LMS@esolutionsgroup.co.uk", "Z0ngo1&2");

            System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("213.171.222.69");
            mSmtpClient.UseDefaultCredentials = false;
            System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("LMS@esolutionsgroup.co.uk", "Z0ngo1&2");// used only for esolutions site

            mSmtpClient.Credentials = loginInfo;
            mSmtpClient.Send(msg);

            blndone = true;
        }
        catch { blndone = false; }
        return blndone;
        //************************************************************************************

    }

    public string fngetUsername(string p,int option)
    {
        string uname = "";
        SqlConnection cn = new SqlConnection(strConn);
        string sqlQuery = "select * from users where email='"+p+"' and active='Y'" ;
        //SqlCommand cmd = new SqlCommand(sqlQuery, cn);
        SqlDataAdapter da=new SqlDataAdapter(sqlQuery,cn);
        DataSet ds = new DataSet();
        da.Fill(ds);
        if(ds!=null && ds.Tables.Count>0 && ds.Tables[0].Rows.Count>0)
        {
            if (option == 1)
            { uname = ds.Tables[0].Rows[0]["firstname"].ToString(); }
            else if (option == 2)
            { uname = ds.Tables[0].Rows[0]["firstname"].ToString() + " " + ds.Tables[0].Rows[0]["lastname"].ToString(); }
            

        }
        cn.Open();
        
        return uname;
    }


    //Emails For EODC
    public static bool sendEmailEODC(string strTo, string strFrom, string strSubject, string strBody, int i)
    {

        //******************************New Mail Format*****************************************************
        bool blndone = false;
        try
        {
            MailMessage mMailMessage = new MailMessage();
            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
            msg.From = new MailAddress(strFrom);
            string[] strToEmails = strTo.Split(';');

            for (int j = 0; j < strToEmails.Length; j++)
            {
                msg.To.Add(new MailAddress(strToEmails[j].ToString().Trim()));
            }
            if (i == 1)
            {
                //Adding By Sushama Acharya
                msg.CC.Add(new MailAddress("imadk@esolutionsgroup.co.uk"));
                msg.CC.Add(new MailAddress("harshadag@esolutionsgroup.co.uk"));
                msg.CC.Add(new MailAddress("patrickc@esolutionsgroup.co.uk"));
                msg.CC.Add(new MailAddress("sunilr@esolutionsgroup.co.uk"));
               // msg.CC.Add(new MailAddress("parvezc@esolutionsgroup.co.uk"));
               
            }
            
            msg.Subject = strSubject;
            msg.Body = strBody;
            msg.IsBodyHtml = true;
            msg.Priority = MailPriority.Normal;


            //Commented By Sushama Acharya
            //System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("mail.estatesolutions.co.uk");
            //mSmtpClient.UseDefaultCredentials = false;
            //System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("test@estatesolutions.co.uk", "testtest");

            //Comment By Sushama Acharya Dt 7 Apr 2015
            //System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("mail.internationalhorizons.co.uk");
            //mSmtpClient.UseDefaultCredentials = false;
            //System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("support@internationalhorizons.co.uk", "support1!");
            // //Commented By Sushama Acharya

            System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("213.171.222.69");//213.171.196.58
            mSmtpClient.UseDefaultCredentials = false;
            System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("LMS@esolutionsgroup.co.uk", "Z0ngo1&2");



            mSmtpClient.Credentials = loginInfo;
            mSmtpClient.Send(msg);

            blndone = true;
        }
        catch { blndone = false; }
        return blndone;
        //************************************************************************************
        
    }




    public string[] authenticateLogin(string emailid, string pwd)
    {
        string[] strValues = { "", "" };
        OleDbConnection myConnection = new OleDbConnection();
        myConnection.ConnectionString = getConnStr(".");
        try
        {
            myConnection.Open();
            string sqlQuery = "SELECT [LoginPassword],[ContactId] from MemberDetails where [EmailId]='" + emailid.Replace("'", "|@")
                              + "' And [LoginPassword]='" + pwd.Replace("'", "|@") + "'";
            OleDbCommand myCommand = new OleDbCommand(sqlQuery, myConnection);
            OleDbDataReader dr = myCommand.ExecuteReader();
            if (dr != null && dr.Read())
            {
                strValues[0] = dr["LoginPassword"].ToString();
                strValues[1] = dr["ContactId"].ToString();
            }
            if (!dr.IsClosed) dr.Close();
            myConnection.Close();
            SqlConnection cn = new SqlConnection(strConn);
            if (strValues[0] != "")
            {
                try
                {
                    sqlQuery = "Select ContactId from PeopleContacts where ContactId='" + strValues[1] + "' and ClientId =" + ClientId + "";
                    SqlCommand cmd = new SqlCommand(sqlQuery, cn);
                    cn.Open();
                    object objResult = cmd.ExecuteScalar();
                    if (objResult == null || objResult.ToString().ToLower() != strValues[1].ToLower())
                    {
                        strValues[1] = "";
                        cn.Close();
                        sqlQuery = "Delete from MemberDetails where [EmailId]='" + emailid.Replace("'", "|@")
                              + "' And [LoginPassword]='" + pwd.Replace("'", "|@") + "'";
                        myCommand.CommandText = sqlQuery;
                        myConnection.Open();
                        myCommand.ExecuteNonQuery();
                    }

                }
                catch { strValues[1] = ""; }
                finally { if (cn.State != ConnectionState.Closed) cn.Close(); }
            }
        }
        catch
        {
        }
        finally { if (myConnection.State != ConnectionState.Closed) myConnection.Close(); }
        return strValues;
    }
    public DataSet GetPropertyForLocalmap123(string PropId, string strType)
    {
        SqlConnection Conn = new SqlConnection(strConn);
        string strImgPath = ConfigurationManager.AppSettings["ThumbImagePath"];
        string strQuery = @"SELECT distinct Property.PropertyId,Property.Reference,Property.DealType,(Select REPLACE(SubValue,'|@','''') from SubType Where MajorId=11 and SubId=Property.Status) as Status,
                           (Select REPLACE(SubValue,'|@','''') from SubType Where MajorId=Property.PropertyMajorType and SubId=Property.PropertySubType) as proptype,
                        PropertyDescriptive.ShortDesc, PropertyLocation.Address + ', ' + PropertyLocation.AreaOrRegionName +', ' + PropertyLocation.CityOrTownName as Address,PropertyLocation.Latitude,PropertyLocation.Longitude,
                        (Select top 1 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId
                        and Files.Category like 'Main photo') As image,
                        (Select FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId
                        and Files.Category like 'Main photo') As LinkMainImage, PropertyLocation.PostCode,PropertyLocation.AreaOrRegionName,
                        PropertyLocation.CityOrTownName, PropertyLocation.CountryName,
                       (SELECT count(*) FROM PropertyRoom LEFT JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                       Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=1) as NumberOfBedrooms
                       FROM Property Left Join PropertyDescriptive ON (Property.PropertyId = PropertyDescriptive.PropertyId and Property.ClientId = PropertyDescriptive.ClientId)
                       Left Join PropertyLocation ON (Property.LocationId = PropertyLocation.LocationId and Property.ClientId = PropertyLocation.ClientId)
                      WHERE Property.ClientId =" + ClientId + " And Property.PropertyId='" + PropId + "'";


        SqlDataAdapter PropertyDA = new SqlDataAdapter(strQuery, Conn);
        DataSet ds = new DataSet();
        try
        {
            Conn.Open();
            PropertyDA.Fill(ds);
        }
        catch
        {
        }
        finally
        {
            Conn.Close();
        }

        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            ds.Tables[0].Columns.Add("Cost");
            ds.Tables[0].Columns.Add("PriceSuffix");
            ds.Tables[0].Columns.Add("CostToShow");
            ds.Tables[0].Rows[0]["CostToShow"] = "";
            ds.Tables[0].Columns.Add("imagescount");
            try
            {
                SqlCommand cmd;
                SqlDataReader dr;
                int j = 1;
                Conn.Open();

                //for main image---------------------
                if (ds.Tables[0].Rows[0]["LinkMainImage"].ToString() != "" && (ds.Tables[0].Rows[0]["LinkMainImage"].ToString().Contains("http://") || ds.Tables[0].Rows[0]["LinkMainImage"].ToString().Contains("www.")))
                {
                    ds.Tables[0].Rows[0]["image"] = ds.Tables[0].Rows[0]["LinkMainImage"].ToString();
                }
                else
                {
                    if (ds.Tables[0].Rows[0]["image"].ToString() != "" && !(ds.Tables[0].Rows[0]["image"].ToString().Contains("http://") || ds.Tables[0].Rows[0]["image"].ToString().Contains("www.")))
                    {
                        string mainImage = ds.Tables[0].Rows[0]["image"].ToString();
                        string[] strimagename = mainImage.ToString().Split(new char[] { '.' });
                        string strImage = strimagename[0];
                        string strexten = strimagename[1];

                        ds.Tables[0].Rows[0]["image"] = strImgPath + strImage + "_thumb." + strexten;
                    }
                }
                //-----------------------------------

                if (ds.Tables[0].Rows[0]["DealType"].ToString().Trim() == "1")
                {
                    strQuery = @"SELECT AskingPrice, PriceSuffix FROM PropertySale where PropertyId='" + ds.Tables[0].Rows[0]["PropertyId"] + "' and clientid='" + ClientId + "'";
                    cmd = new SqlCommand(strQuery, Conn);
                    dr = cmd.ExecuteReader();
                    if (dr.Read())
                    {
                        ds.Tables[0].Rows[0]["Cost"] = dr["AskingPrice"];
                        ds.Tables[0].Rows[0]["PriceSuffix"] = dr["PriceSuffix"];
                        dr.Close();
                    }
                }
                else
                {
                    strQuery = @"SELECT Price, PriceSuffix FROM PropertyLet where PropertyId='" + ds.Tables[0].Rows[0]["PropertyId"] + "' and clientid='" + ClientId + "'";
                    cmd = new SqlCommand(strQuery, Conn);
                    dr = cmd.ExecuteReader();
                    if (dr.Read())
                    {
                        ds.Tables[0].Rows[0]["Cost"] = dr["Price"];
                        ds.Tables[0].Rows[0]["PriceSuffix"] = dr["PriceSuffix"];
                        dr.Close();
                    }
                    strQuery = @"SELECT SubType.SubValue FROM MajorType INNER JOIN
                                    SubType ON MajorType.MajorId = SubType.MajorId INNER JOIN
                                    PropertyLet ON SubType.SubId = PropertyLet.PricePer where MajorType.MajorValue='PricePer'
                                    and PropertyLet.PropertyId='" + ds.Tables[0].Rows[0]["PropertyId"] + "'";
                    cmd = new SqlCommand(strQuery, Conn);
                    object objPricePer = cmd.ExecuteScalar();

                    double price = 0;
                    switch (objPricePer.ToString())
                    {
                        case "Week": //week
                            ds.Tables[0].Rows[0]["CostToShow"] = " pcm";
                            price = Convert.ToDouble(ds.Tables[0].Rows[0]["Cost"]);
                            price = (price * 52) / 12;
                            ds.Tables[0].Rows[0]["Cost"] = price;
                            break;
                        case "Month": //month
                            ds.Tables[0].Rows[0]["CostToShow"] = " pcm";
                            price = Convert.ToDouble(ds.Tables[0].Rows[0]["Cost"]);
                            ds.Tables[0].Rows[0]["Cost"] = price;
                            break;
                        case "Quarter": //quarter
                            ds.Tables[0].Rows[0]["CostToShow"] = " pcm";
                            price = Convert.ToDouble(ds.Tables[0].Rows[0]["Cost"]);
                            price = (price / 3);
                            ds.Tables[0].Rows[0]["Cost"] = price;
                            break;
                        case "Year": //year
                            ds.Tables[0].Rows[0]["CostToShow"] = " pcm";
                            price = Convert.ToDouble(ds.Tables[0].Rows[0]["Cost"]);
                            price = (price / 12);
                            ds.Tables[0].Rows[0]["Cost"] = price;
                            break;
                        default:
                            ds.Tables[0].Rows[0]["CostToShow"] = " pcm";
                            price = Convert.ToDouble(ds.Tables[0].Rows[0]["Cost"]);
                            ds.Tables[0].Rows[0]["Cost"] = price;
                            break;
                    }
                }

                try
                {
                    if (ds.Tables[0].Rows[0]["LinkMainImage"].ToString() != "" && (ds.Tables[0].Rows[0]["LinkMainImage"].ToString().Contains("http://") || ds.Tables[0].Rows[0]["LinkMainImage"].ToString().Contains("www.")))
                    {
                        strQuery = @"SELECT Files.FileName FROM Files 
                             WHERE Files.ModuleId='" + PropId + "' and Files.Category like 'Photo' and Files.ClientId=" + ClientId + " Order by Files.Priority Asc";

                        cmd = new SqlCommand(strQuery, Conn);
                        dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            if (!ds.Tables[0].Columns.Contains("image" + j))
                                ds.Tables[0].Columns.Add("image" + j);
                            ds.Tables[0].Rows[0]["image" + j] = dr[0];
                            j++;
                        }
                        ds.Tables[0].Rows[0]["imagescount"] = j;
                        if (!dr.IsClosed) dr.Close();
                    }
                    else
                    {
                        strQuery = @"SELECT Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName as filename, filename as newfile FROM Files 
                             WHERE Files.ModuleId='" + PropId + "' and Files.Category like 'Photo' and Files.ClientId=" + ClientId + " Order by Files.Priority Asc";

                        cmd = new SqlCommand(strQuery, Conn);
                        dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            if (!ds.Tables[0].Columns.Contains("image" + j))
                                ds.Tables[0].Columns.Add("image" + j);

                            string newfile = dr[1].ToString();
                            if (newfile != "" && (newfile.ToString().Contains("http://") || newfile.ToString().Contains("www.")))
                            {
                                ds.Tables[0].Rows[0]["image" + j] = newfile;
                            }
                            else
                            {
                                string Images = dr[0].ToString();
                                string[] strimagename = Images.ToString().Split(new char[] { '.' });
                                string strImage = strimagename[0];
                                string strexten = strimagename[1];

                                ds.Tables[0].Rows[0]["image" + j] = strImgPath + strImage + "_thumb." + strexten;
                            }
                            j++;
                        }
                        ds.Tables[0].Rows[0]["imagescount"] = j;
                        if (!dr.IsClosed) dr.Close();
                    }
                }
                catch { }


                ds.Tables[0].Rows[0]["Address"] = ds.Tables[0].Rows[0]["Address"].ToString().Replace("|@", "'").TrimEnd(new char[] { ',', ' ' }).TrimStart(new char[] { ',', ' ' });
                ds.Tables[0].Rows[0]["ShortDesc"] = ds.Tables[0].Rows[0]["ShortDesc"].ToString().Replace("|@", "'");
            }
            catch { }
            finally { Conn.Close(); }
        }
        return ds;
    }


    public DataSet getregArea()
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);

            string strQuery = "Select Distinct REPLACE(LocationAKA,'|@','''') as Area from LocationAKA " +
                              "Where ClientId =" + ClientId + " and LocationAKA != '' and LocationAKA != 'All'";
            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    public int operateWishlist(string emailid, string strId, string strAction, string strType)
    {
        int intResult = 0;
        OleDbConnection oleConn = new OleDbConnection(getConnStr("."));
        try
        {
            string strQuery = "Select MemberId from MemberDetails Where [EmailId]='" + emailid.Replace("'", "|@") + "'";
            OleDbCommand cmd = new OleDbCommand(strQuery, oleConn);
            oleConn.Open();
            object memberid = cmd.ExecuteScalar();
            if (memberid != null)
            {
                if (strAction == "Add")
                {
                    try
                    {
                        strQuery = "Insert into SavedProps (MemberId,SavedPropId,PropType) Values(" + memberid + ",'" + strId + "','" + strType + "')";
                        cmd.CommandText = strQuery;
                        intResult = cmd.ExecuteNonQuery();
                    }
                    catch
                    {

                    }
                }
                else if (strAction == "Delete")
                {
                    strQuery = "Delete from SavedProps Where MemberId=" + memberid + " And SavedPropId='" + strId + "'";
                    cmd.CommandText = strQuery;
                    intResult = cmd.ExecuteNonQuery();
                }
                else { oleConn.Close(); return 0; }
            }
        }
        catch { intResult = 0; }
        finally { oleConn.Close(); }
        return intResult;
    }

    private System.Collections.ArrayList getWishlistPropsIds(string emailid)
    {
        System.Collections.ArrayList lsts = new System.Collections.ArrayList();
        System.Collections.ArrayList lstIds = new System.Collections.ArrayList();
        System.Collections.ArrayList lstTypes = new System.Collections.ArrayList();
        OleDbConnection oleConn = new OleDbConnection(getConnStr("."));
        try
        {
            string strQuery = "Select [SavedPropId],[PropType] from SavedProps Inner Join MemberDetails On SavedProps.[MemberId] = MemberDetails.[MemberId] "
                            + "Where MemberDetails.[EmailId]='" + emailid.Replace("'", "|@") + "'";
            OleDbCommand cmd = new OleDbCommand(strQuery, oleConn);
            oleConn.Open();
            OleDbDataReader dr = cmd.ExecuteReader();

            if (dr != null && dr.Read())
            {
                lstIds.Add(dr["SavedPropId"].ToString().ToLower());
                lstTypes.Add(dr["PropType"].ToString().ToLower());
                while (dr.Read())
                {
                    lstIds.Add(dr["SavedPropId"].ToString().ToLower());
                    lstTypes.Add(dr["PropType"].ToString().ToLower());
                }
            }
            if (!dr.IsClosed) dr.Close();
        }
        catch { }
        finally { oleConn.Close(); }
        lsts.Add(lstIds);
        lsts.Add(lstTypes);
        return lsts;

    }

    public DataSet getWishlistProps(string emailid)
    {
        System.Collections.ArrayList lsts = getWishlistPropsIds(emailid);

        System.Collections.ArrayList lstIds = (System.Collections.ArrayList)lsts[0];

        System.Collections.ArrayList lstTypes = (System.Collections.ArrayList)lsts[1];
        DataSet ds = new DataSet();
        if (lstIds.Count > 0)
        {


            SqlConnection Conn = new SqlConnection(strConn);
            string sqlQuery = @"SELECT Property.PropertyId,Property.timestampid,Property.DealType,(Select SubValue from SubType Where MajorId=11 and SubId=Property.Status) as Status,
                    (Select SubValue from SubType Where MajorId=Property.PropertyMajorType and SubId=Property.PropertySubType) as proptype,
                    (Select top 1  Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId and Files.Category like 'Photo link' and Priority!=0 order by Priority asc) As LinkMainImage, 
                    (Select top 1 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId
                    and Files.Category like 'Main photo') As image, PropertyDescriptive.ShortDesc,
                    PropertyLocation.AreaOrRegionName, Property.Reference, PropertyLocation.CityOrTownName, PropertyLocation.CountryName,   
                    (SELECT count(*) FROM PropertyRoom Inner JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                    Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=1) as NumberOfBedrooms,
                    (SELECT count(*) FROM PropertyRoom Inner JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                    Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=40) as NumberOfBathrooms,
                    (SELECT count(*) FROM PropertyRoom Inner JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                    Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=6) as NumberOfReceptions,
                    PropertyLocation.Address + ', ' + PropertyLocation.CityOrTownName as Address, PropertyLocation.PostCode,PropertyLocation.Latitude,PropertyLocation.Longitude,PropertyLocation.StreetViewAttributes
                    FROM Property Left Join PropertyDescriptive ON 
                    (Property.PropertyId = PropertyDescriptive.PropertyId and Property.ClientId = PropertyDescriptive.ClientId)
                    Left JOIN PropertyLocation ON (Property.LocationId = PropertyLocation.LocationId and Property.ClientId = PropertyLocation.ClientId)
                    WHERE Property.ClientId =" + ClientId + "";


            SqlDataAdapter PropertyDA = new SqlDataAdapter(sqlQuery, Conn);
            try
            {

                PropertyDA.Fill(ds);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    SqlCommand cmd;
                    SqlDataReader dr;
                    Conn.Open();
                    ds.Tables[0].Columns.Add("SaleCost");
                    ds.Tables[0].Columns.Add("LetCost");
                    ds.Tables[0].Columns.Add("SalePS");
                    ds.Tables[0].Columns.Add("LetPS");
                    ds.Tables[0].Columns.Add("CostToShow");
                    ds.Tables[0].Columns.Add("Cost");
                    ds.Tables[0].Columns.Add("CostToOrder", typeof(Double));
                    ds.Tables[0].Columns.Add("PriceSuffix");
                    ds.Tables[0].Columns.Add("detailstype");
                    ds.Tables[0].Columns.Add("PricePer");
                    int arrCnt = 0;

                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        object objId = ds.Tables[0].Rows[i]["PropertyId"];
                        if (lstIds.Contains(objId.ToString().ToLower()) == false)
                        {
                            ds.Tables[0].Rows.RemoveAt(i);
                            i--;
                            continue;
                        }

                        for (int jj = 0; jj < lstIds.Count; jj++)
                        {
                            if (objId.ToString() == lstIds[jj].ToString())
                            {
                                arrCnt = jj;
                                break;
                            }
                        }
                        string id = objId.ToString();
                        ds.Tables[0].Rows[i]["SaleCost"] = "";
                        ds.Tables[0].Rows[i]["LetCost"] = "";
                        ds.Tables[0].Rows[i]["SalePS"] = "";
                        ds.Tables[0].Rows[i]["LetPS"] = "";
                        ds.Tables[0].Rows[i]["CostToShow"] = "";
                        ds.Tables[0].Rows[i]["Cost"] = "";
                        ds.Tables[0].Rows[i]["PriceSuffix"] = "";
                        if (lstTypes[arrCnt].ToString().ToLower() == "1" || ds.Tables[0].Rows[i]["DealType"].ToString() == "1")
                        {
                            ds.Tables[0].Rows[i]["detailstype"] = "1";
                            sqlQuery = "Select AskingPrice,PriceSuffix from PropertySale where PropertyId='" + id + "' And ClientId =" + ClientId + "";
                            cmd = new SqlCommand(sqlQuery, Conn);
                            if (Conn.State == ConnectionState.Closed)
                            {
                                Conn.Open();
                            }
                            dr = cmd.ExecuteReader();
                            if (dr.Read())
                            {
                                ds.Tables[0].Rows[i]["SaleCost"] = dr["AskingPrice"];
                                ds.Tables[0].Rows[i]["SalePS"] = dr["PriceSuffix"];
                                ds.Tables[0].Rows[i]["Cost"] = dr["AskingPrice"];
                                ds.Tables[0].Rows[i]["CostToOrder"] = dr["AskingPrice"];
                                ds.Tables[0].Rows[i]["PriceSuffix"] = ds.Tables[0].Rows[i]["SalePS"];
                            }
                            if (dr != null && !dr.IsClosed)
                                dr.Close();
                        }
                        else if (lstTypes[arrCnt].ToString().ToLower() == "2" || ds.Tables[0].Rows[i]["DealType"].ToString() == "2")
                        {
                            ds.Tables[0].Rows[i]["detailstype"] = "2";
                            sqlQuery = @"SELECT PropertyLet.Price, PropertyLet.PriceSuffix, SubType.SubValue, PropertyLet.PricePer  FROM PropertyLet Left Join Subtype
                                    On PropertyLet.PricePer = SubType.SubId Where
                                    PropertyId='" + id + "' And ClientId =" + ClientId + "";
                            cmd = new SqlCommand(sqlQuery, Conn);
                            if (Conn.State == ConnectionState.Closed)
                            {
                                Conn.Open();
                            }
                            dr = cmd.ExecuteReader();
                            if (dr.Read())
                            {
                                ds.Tables[0].Rows[i]["Cost"] = dr["Price"];
                                ds.Tables[0].Rows[i]["CostToOrder"] = dr["Price"];
                                ds.Tables[0].Rows[i]["LetPS"] = dr["PriceSuffix"];
                                ds.Tables[0].Rows[i]["PricePer"] = dr["PricePer"];
                                //if (dr["SubValue"] != null && dr["SubValue"].ToString().ToLower().Contains("week"))
                                //{

                                //    ds.Tables[0].Rows[i]["CostToShow"] = " pw";
                                //}
                                //else if (dr["SubValue"] != null && dr["SubValue"].ToString().ToLower().Contains("month"))
                                //{
                                //    double price = Convert.ToDouble(ds.Tables[0].Rows[i]["LetCost"].ToString());
                                //    price = price / 4;
                                //    ds.Tables[0].Rows[i]["CostToShow"] = " pm";
                                //}
                                //else if (dr["SubValue"] != null && dr["SubValue"].ToString().ToLower().Contains("quarter"))
                                //{
                                //    double price = Convert.ToDouble(ds.Tables[0].Rows[i]["LetCost"].ToString());
                                //    price = price / 12;
                                //    ds.Tables[0].Rows[i]["CostToShow"] = " pq";
                                //}
                                //else if (dr["SubValue"] != null && dr["SubValue"].ToString().ToLower().Contains("year"))
                                //{
                                //    double price = Convert.ToDouble(ds.Tables[0].Rows[i]["LetCost"].ToString());
                                //    price = price / 50;
                                //    ds.Tables[0].Rows[i]["CostToShow"] = " pa";
                                //}
                                //ds.Tables[0].Rows[i]["Cost"] = ds.Tables[0].Rows[i]["LetCost"];
                                //ds.Tables[0].Rows[i]["PriceSuffix"] = ds.Tables[0].Rows[i]["LetPS"];
                                double price = 0.0;
                                if (ds.Tables[0].Columns.Contains("PricePer"))
                                {
                                    switch (ds.Tables[0].Rows[i]["PricePer"].ToString())
                                    {
                                        case "0": //week
                                            ds.Tables[0].Rows[i]["CostToShow"] = " pcm";
                                            price = Convert.ToDouble(ds.Tables[0].Rows[i]["Cost"]);
                                            price = (price * 52) / 12;
                                            ds.Tables[0].Rows[i]["Cost"] = price;
                                            ds.Tables[0].Rows[i]["CostToOrder"] = price;
                                            break;
                                        case "1": //month
                                            ds.Tables[0].Rows[i]["CostToShow"] = " pcm";
                                            price = Convert.ToDouble(ds.Tables[0].Rows[i]["Cost"]);
                                            ds.Tables[0].Rows[i]["Cost"] = price;
                                            ds.Tables[0].Rows[i]["CostToOrder"] = price;
                                            break;
                                        case "2": //quarter
                                            ds.Tables[0].Rows[i]["CostToShow"] = " pcm";
                                            price = Convert.ToDouble(ds.Tables[0].Rows[i]["Cost"]);
                                            price = (price / 3);
                                            ds.Tables[0].Rows[i]["Cost"] = price;
                                            ds.Tables[0].Rows[i]["CostToOrder"] = price;
                                            break;
                                        case "3": //year
                                            ds.Tables[0].Rows[i]["CostToShow"] = " pcm";
                                            price = Convert.ToDouble(ds.Tables[0].Rows[i]["Cost"]);
                                            price = (price / 12);
                                            ds.Tables[0].Rows[i]["Cost"] = price;
                                            ds.Tables[0].Rows[i]["CostToOrder"] = price;
                                            break;
                                        default:
                                            ds.Tables[0].Rows[i]["CostToShow"] = " pcm";
                                            price = Convert.ToDouble(ds.Tables[0].Rows[i]["Cost"]);
                                            ds.Tables[0].Rows[i]["Cost"] = price;
                                            ds.Tables[0].Rows[i]["CostToOrder"] = price;
                                            break;
                                    }
                                }

                            }
                            if (dr != null && !dr.IsClosed)
                                dr.Close();

                        }


                        ds.Tables[0].Rows[i]["Address"] = ds.Tables[0].Rows[i]["Address"].ToString().Replace("|@", "'").TrimEnd(new char[] { ',', ' ' }).TrimStart(new char[] { ',', ' ' });
                        ds.Tables[0].Rows[i]["PostCode"] = ds.Tables[0].Rows[i]["PostCode"].ToString().Replace("|@", "'");

                        ds.Tables[0].Rows[i]["ShortDesc"] = ds.Tables[0].Rows[i]["ShortDesc"].ToString().Replace("|@", "'");

                    }
                }
            }
            catch
            {
            }
            finally
            {
                if (Conn.State == ConnectionState.Open) { Conn.Close(); }
            }
        }
        return ds;
    }

    public object checkWishlist(string emailid, string strId, string strType)
    {
        object objResult = null;
        OleDbConnection oleConn = new OleDbConnection(getConnStr("."));
        try
        {
            string strQuery = @"Select [SavedPropId] from [SavedProps] Inner Join MemberDetails On SavedProps.MemberId = MemberDetails.MemberId
                                Where SavedProps.MemberId=(Select MemberDetails.MemberId from MemberDetails
                                Where [EmailId]='" + emailid.Replace("'", "|@") + "') And [SavedPropId]='" + strId + "' And [PropType]='" + strType + "'";
            OleDbCommand cmd = new OleDbCommand(strQuery, oleConn);
            oleConn.Open();
            objResult = cmd.ExecuteScalar();
        }
        catch { }
        finally { oleConn.Close(); }
        return objResult;
    }
    public DataTable GetAllWishlist(string emailid, string strType)
    {
        DataTable dt = new DataTable();

        OleDbConnection oleConn = new OleDbConnection(getConnStr("."));
        try
        {
            string strQuery = @"Select [SavedPropId] from [SavedProps] Inner Join MemberDetails On SavedProps.MemberId = MemberDetails.MemberId
                                Where SavedProps.MemberId=(Select MemberDetails.MemberId from MemberDetails
                                Where [EmailId]='" + emailid.Replace("'", "|@") + "') And [PropType]='" + strType + "'";
            OleDbCommand cmd = new OleDbCommand(strQuery, oleConn);
            if (oleConn.State == ConnectionState.Closed)
            {
                oleConn.Open();
            }
            OleDbDataAdapter da = new OleDbDataAdapter(cmd);
            da.Fill(dt);
        }
        catch { }
        finally
        {
            if (oleConn.State == ConnectionState.Open)
            {
                oleConn.Close();
            }
        }
        return dt;
    }


    private object getContactId(string emailid)
    {
        object ContactId = null;
        OleDbConnection oleConn = new OleDbConnection(getConnStr("."));
        SqlConnection sqlConn = new SqlConnection(strConn);
        try
        {
            string strQuery = "Select ContactId from MemberDetails Where [EmailId]='" + emailid.Replace("'", "|@") + "'";
            OleDbCommand oleCmd = new OleDbCommand(strQuery, oleConn);
            oleConn.Open();
            ContactId = oleCmd.ExecuteScalar();
            if (ContactId == null || ContactId.ToString().Trim() == "")
            {
                oleConn.Close();
                strQuery = "Select ContactId from PeopleContacts Where NickName='" + emailid.Replace("|@", "'") + "'";
                SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
                sqlConn.Open();
                ContactId = sqlCmd.ExecuteScalar();
                ContactId = ContactId != null && ContactId.ToString().Trim() != "" ? ContactId : System.Guid.NewGuid();
            }
        }
        catch
        {
        }
        finally { oleConn.Close(); sqlConn.Close(); }
        return ContactId;
    }

    public string getMemberId(string emailid)
    {
        string strMemberId = "0";
        OleDbConnection myConnection = new OleDbConnection();
        try
        {
            myConnection.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Password=\"\";User ID=Admin;Data Source=" + System.Web.HttpContext.Current.Server.MapPath(".") + "\\MembersDB.mdb";
            myConnection.Open();
            string sqlstring = "SELECT [MemberId] from MemberDetails where [EmailId]='" + emailid.Replace("'", "|@") + "'";
            OleDbCommand myCommand = new OleDbCommand(sqlstring, myConnection);
            strMemberId = myCommand.ExecuteScalar().ToString();
        }
        catch { }
        finally { myConnection.Close(); }
        return strMemberId;
    }

    #region Login and forgot password
    public object Authenticate(string email, string pwd)
    {
        string[] strValues = { "", "", "" };
        OleDbConnection myConnection = new OleDbConnection();
        myConnection.ConnectionString = getConnStr(".");
        try
        {
            myConnection.Open();
            string sqlQuery = "SELECT [LoginPassword],[ContactId] from MemberDetails where [EmailId]='" + email.Replace("'", "|@")
                              + "' And [LoginPassword]='" + pwd.Replace("'", "|@") + "'";
            OleDbCommand myCommand = new OleDbCommand(sqlQuery, myConnection);
            OleDbDataReader dr = myCommand.ExecuteReader();
            if (dr != null && dr.Read())
            {
                strValues[0] = dr["LoginPassword"].ToString();
                strValues[1] = dr["ContactId"].ToString();
            }
            if (!dr.IsClosed) dr.Close();
            myConnection.Close();
            SqlConnection cn = new SqlConnection(strConn);
            if (strValues[0] != "")
            {
                try
                {
                    sqlQuery = "Select ContactId from PeopleContacts where ContactId='" + strValues[1] + "' and ClientId=" + ClientId;
                    SqlCommand cmd = new SqlCommand(sqlQuery, cn);
                    cn.Open();
                    object objResult = cmd.ExecuteScalar();
                    if (objResult == null || objResult.ToString().ToLower() != strValues[1].ToLower())
                    {
                        strValues[1] = "";

                        sqlQuery = "Delete from MemberDetails where [EmailId]='" + email.Replace("'", "|@")
                              + "' And [LoginPassword]='" + pwd.Replace("'", "|@") + "'";
                        myCommand.CommandText = sqlQuery;
                        myConnection.Open();
                        myCommand.ExecuteNonQuery();
                    }
                    sqlQuery = "Select FirstName from PeopleContacts where ContactId='" + strValues[1] + "' and ClientId=" + ClientId;
                    SqlCommand cmnd = new SqlCommand(sqlQuery, cn);
                    object objResults = cmnd.ExecuteScalar();
                    if (objResult != null)
                    {
                        strValues[2] = objResults.ToString();
                    }
                    //   cn.Close();
                }
                catch { strValues[1] = ""; }
                finally { if (cn.State != ConnectionState.Closed) cn.Close(); }
            }
        }
        catch
        {
        }
        finally { if (myConnection.State != ConnectionState.Closed) myConnection.Close(); }
        if (myConnection.State != ConnectionState.Closed) myConnection.Close();
        return strValues;
    }

    public object FetchPassword(string email)
    {
        object objpwd = null;
        OleDbConnection myConnection = new OleDbConnection();
        myConnection.ConnectionString = getConnStr(".");
        try
        {
            string strQuery = "Select loginpassword from MemberDetails where Emailid='" + email + "'";
            OleDbCommand cmd = new OleDbCommand(strQuery, myConnection);
            myConnection.Open();
            objpwd = cmd.ExecuteScalar();
            myConnection.Close();
        }
        catch
        {
        }
        finally { if (myConnection.State != ConnectionState.Closed) myConnection.Close(); }

        return objpwd;
    }
    #endregion

    #region macaddress And Ip address
    public string[] fnGetMacIDAndIPAddress()
    {
        string[] strArrayMacIDAndIP = new string[2];
        try
        {
            string strMacId = "";
            string strIpAddress = "";

            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (nic.OperationalStatus == OperationalStatus.Up)
                {
                    strMacId += nic.GetPhysicalAddress().ToString();
                    break;
                }
            }

            strArrayMacIDAndIP[0] = strMacId;

            strIpAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (strIpAddress == null)

                strIpAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

            strArrayMacIDAndIP[1] = strIpAddress;

            return strArrayMacIDAndIP;
        }
        catch { }
        return strArrayMacIDAndIP;
    }

    #endregion

    #region featured property of the week
    public DataSet getFeaturedPropOfWeek()
    {
        SqlConnection Conn = new SqlConnection(strConn);
        string sqlQuery = "";
        string strCondition = "";


        strCondition = @"Where Property.ClientId =" + ClientId + " AND (Property.DealType = 2 Or Property.DealType = 4) AND Property.FeaturedProperty='True' ";

        strCondition = strCondition.TrimEnd(new char[] { 'A', 'N', 'D', ' ' });
        sqlQuery = @"SELECT top(1) Property.PropertyId,Property.DealType,Property.timestampid,(Select SubValue from SubType Where MajorId=11 and SubId=Property.Status) as Status,
                    (Select SubValue from SubType Where MajorId=Property.PropertyMajorType and SubId=Property.PropertySubType) as proptype,
                    (Select top 1 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId
                    and Files.Category like 'Main photo') As image, PropertyDescriptive.ShortDesc,
                    (SELECT count(*) FROM PropertyRoom Inner JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                    Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=1) as NumberOfBedrooms,
                    (SELECT count(*) FROM PropertyRoom Inner JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                    Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=40) as NumberOfBathrooms,
                    (SELECT count(*) FROM PropertyRoom Inner JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                    Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=6) as NumberOfReceptions,
                    PropertyLocation.Address + ', ' + PropertyLocation.CityOrTownName as Address,PropertyLocation.StreetViewAttributes,
                    PropertyLocation.PropertyNameNumber+', '+PropertyLocation.Address as street, PropertyLocation.PostCode,PropertyLocation.Latitude,PropertyLocation.Longitude,PropertyLocation.ShowGSVOnWebsite,
                    PropertyLet.Price As Cost, PropertyLet.Price As CostToOrder, PropertyLet.PriceSuffix,PropertyLet.PricePer FROM Property Left Join PropertyDescriptive ON 
                    (Property.PropertyId = PropertyDescriptive.PropertyId and Property.ClientId = PropertyDescriptive.ClientId)
                    Left JOIN PropertyLocation ON (Property.LocationId = PropertyLocation.LocationId and Property.ClientId = PropertyLocation.ClientId)
                    Left JOIN UserDetails ON (Property.UserName = UserDetails.UserName and Property.ClientId = UserDetails.ClientId)
                    Inner Join PropertyLet ON (Property.PropertyId = PropertyLet.PropertyId and Property.ClientId = PropertyLet.ClientId) " + strCondition + " Order by Property.TimeStampId Desc ";

        SqlDataAdapter PropertyDA = new SqlDataAdapter(sqlQuery, Conn);
        DataSet ds = new DataSet();
        try
        {

            PropertyDA.Fill(ds);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {

                ds.Tables[0].Columns.Add("CostToShow");
                double price = 0;
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    try
                    {

                        DataRow row = ds.Tables[0].Rows[i];

                        row["Address"] = row["Address"].ToString().Replace("|@", "'").TrimEnd(new char[] { ',', ' ' }).TrimStart(new char[] { ',', ' ' });
                        row["PostCode"] = row["PostCode"].ToString().Replace("|@", "'");
                        row["street"] = row["street"].ToString().Replace("|@", "'");
                        row["ShortDesc"] = row["ShortDesc"].ToString().Replace("|@", "'");

                        if (ds.Tables[0].Columns.Contains("PricePer"))
                        {
                            switch (ds.Tables[0].Rows[i]["PricePer"].ToString())
                            {
                                case "0": //week
                                    ds.Tables[0].Rows[i]["CostToShow"] = " pcm";
                                    price = Convert.ToDouble(ds.Tables[0].Rows[i]["Cost"]);
                                    price = (price * 52) / 12;
                                    ds.Tables[0].Rows[i]["CostToOrder"] = price;
                                    break;
                                case "1": //month
                                    ds.Tables[0].Rows[i]["CostToShow"] = " pcm";
                                    price = Convert.ToDouble(ds.Tables[0].Rows[i]["Cost"]);
                                    ds.Tables[0].Rows[i]["CostToOrder"] = price;
                                    break;
                                case "2": //quarter
                                    ds.Tables[0].Rows[i]["CostToShow"] = " pcm";
                                    price = Convert.ToDouble(ds.Tables[0].Rows[i]["Cost"]);
                                    price = (price / 3);
                                    ds.Tables[0].Rows[i]["CostToOrder"] = price;
                                    break;
                                case "3": //year
                                    ds.Tables[0].Rows[i]["CostToShow"] = " pcm";
                                    price = Convert.ToDouble(ds.Tables[0].Rows[i]["Cost"]);
                                    price = (price / 12);
                                    ds.Tables[0].Rows[i]["CostToOrder"] = price;
                                    break;
                                default:
                                    ds.Tables[0].Rows[i]["CostToShow"] = " pcm";
                                    price = Convert.ToDouble(ds.Tables[0].Rows[i]["Cost"]);
                                    ds.Tables[0].Rows[i]["CostToOrder"] = price;
                                    break;
                            }
                        }
                        ds.Tables[0].Rows[i]["Cost"] = ds.Tables[0].Rows[i]["CostToOrder"].ToString();
                        string[] addressparts = ds.Tables[0].Rows[i]["Address"].ToString().Replace("|@", "'").Split(new char[] { ',' });
                        string address = addressparts[0];
                        address += addressparts.Length > 1 ? ", " + addressparts[1] : "";
                        address += addressparts.Length > 2 ? ", " + addressparts[2] : "";
                        address += addressparts.Length > 4 ? ", " + addressparts[4] : "";
                        address = address.TrimEnd(new char[] { ',', ' ' });
                        ds.Tables[0].Rows[i]["Address"] = address;
                    }
                    catch { }
                }
            }
        }
        catch (Exception ex)
        {
        }
        finally
        {
            if (Conn.State == ConnectionState.Open) { Conn.Close(); }
        }
        return ds;
    }

    #endregion

    #region QrCode
    public int InsertQrCodeDetails(string propid, string refno, string filename)
    {
        int result = 0;
        SqlConnection conn = new SqlConnection(strConn);
        try
        {
            SqlCommand cmd = new SqlCommand();
            string qry = "insert into QrCodeDetails(PropertyId,ReferenceNo,ClientId,FileName) values('" + propid + "','" + refno + "','" + ClientId + "','" + filename + "')";
            cmd.CommandText = qry;
            cmd.Connection = conn;
            conn.Open();
            result = cmd.ExecuteNonQuery();
            conn.Close();
        }
        catch { conn.Close(); }
        finally
        {
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
        }
        return result;
    }
    public DataTable GetQrCodeDetails(string propid, string refno, string filename, string clid)
    {

        SqlConnection conn = new SqlConnection(strConn);
        DataTable dt = new DataTable();
        try
        {
            string strQry = "select FileName from QrCodeDetails where ClientId=" + clid + " and FileName='" + filename + "' and PropertyId='" + propid + "'";
            SqlDataAdapter da = new SqlDataAdapter(strQry, conn);

            da.Fill(dt);

            return dt;
        }
        catch { return dt; }
    }
    #endregion

    #region register user
    public object getUserName(string emailid)
    {
        object objEmailId = null;
        OleDbConnection myConnection = new OleDbConnection();
        myConnection.ConnectionString = getConnStr(".");
        try
        {
            myConnection.Open();
            string sqlQuery = "SELECT [ContactId] from MemberDetails where [EmailId]='" + emailid.Replace("'", "|@") + "'";
            OleDbCommand myCommand = new OleDbCommand(sqlQuery, myConnection);
            objEmailId = myCommand.ExecuteScalar();
            if (objEmailId == null || objEmailId.ToString() == "")
            {
                sqlQuery = "Delete from MemberDetails where [EmailId]='" + emailid.Replace("'", "|@") + "'";
                myCommand.CommandText = sqlQuery;
                myCommand.ExecuteNonQuery();
            }
            else
            {
                myConnection.Close();
                SqlConnection cn = new SqlConnection(strConn);
                try
                {
                    sqlQuery = "Select ContactId from PeopleContacts where ContactId='" + objEmailId.ToString() + "'";
                    SqlCommand cmd = new SqlCommand(sqlQuery, cn);
                    cn.Open();
                    objEmailId = cmd.ExecuteScalar();
                    if (objEmailId == null)
                    {
                        cn.Close();
                        sqlQuery = "Delete from MemberDetails where [EmailId]='" + emailid.Replace("'", "|@") + "'";
                        myCommand.CommandText = sqlQuery;
                        myConnection.Open();
                        myCommand.ExecuteNonQuery();
                    }
                }
                catch { }
                finally { if (cn.State != ConnectionState.Closed) cn.Close(); }
            }
        }
        catch
        {
        }
        finally { if (myConnection.State != ConnectionState.Closed) myConnection.Close(); }
        return objEmailId;
    }
    public int UpdateRegMember(string emailid, string pwd)
    {
        int intResult = 0;
        OleDbConnection myConnection = new OleDbConnection(getConnStr("."));
        try
        {
            string sqlQuery = "Update MemberDetails set [LoginPassword]='" + pwd.Replace("'", "|@") + "' where [EmailId] ='" + emailid + "'";
            OleDbCommand myCommand = new OleDbCommand(sqlQuery, myConnection);
            myConnection.Open();
            intResult = myCommand.ExecuteNonQuery();
        }
        catch { intResult = 0; }
        finally { myConnection.Close(); }
        return intResult;
    }

    public int insertMember(string emailid, string pwd, object ContactId)
    {
        int intResult = 0;
        OleDbConnection myConnection = new OleDbConnection(getConnStr("."));
        try
        {
            string sqlQuery = "Insert into MemberDetails([EmailId],[LoginPassword],[ContactId]) values('" + emailid.Replace("'", "|@")
                              + "','" + pwd.Replace("'", "|@") + "','" + ContactId + "')";
            OleDbCommand myCommand = new OleDbCommand(sqlQuery, myConnection);
            myConnection.Open();
            intResult = myCommand.ExecuteNonQuery();
        }
        catch { intResult = 0; }
        finally { myConnection.Close(); }
        return intResult;
    }

    public DataSet getUserDetails(string strEmailId)
    {
        DataSet ds = new DataSet();
        OleDbConnection oleConn = new OleDbConnection(getConnStr("."));
        SqlConnection cn = new SqlConnection(strConn);
        try
        {
            string strQuery = "Select * from MemberDetails where [EmailId]='" + strEmailId.Replace("'", "|@") + "';";
            OleDbDataAdapter da = new OleDbDataAdapter(strQuery, oleConn);
            oleConn.Open();
            da.Fill(ds);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                oleConn.Close();
                ds.Tables[0].Columns.Add("title");
                ds.Tables[0].Columns.Add("fname");
                ds.Tables[0].Columns.Add("lname");
                ds.Tables[0].Columns.Add("phone");
                ds.Tables[0].Columns.Add("Mobile");
                ds.Tables[0].Columns.Add("company");
                ds.Tables[0].Columns.Add("locationid");
                ds.Tables[0].Columns.Add("address");
                ds.Tables[0].Columns.Add("FromPrice1");
                ds.Tables[0].Columns.Add("ToPrice1");
                ds.Tables[0].Columns.Add("PropertySubTypes");
                ds.Tables[0].Columns.Add("Region");
                ds.Tables[0].Columns.Add("ConSource");
                ds.Tables[0].Columns.Add("Notes");
                ds.Tables[0].Columns.Add("Minbed");
                ds.Tables[0].Columns.Add("requestNote");


                try
                {

                    string ContactId = ds.Tables[0].Rows[0]["ContactId"].ToString();

                    strQuery = "Select PeopleContacts.Title,PeopleContacts.FirstName,PeopleContacts.LastName,PeopleContacts.BusinessPhone,PeopleContacts.Mobile,PeopleContacts.CompanyName,PeopleContacts.Notes,PeopleContacts.Mobile,PeopleContacts.ContactSource,PropertyLocation.LocationId,";
                    strQuery += "PropertyLocation.Address, PropertyLocation.CityOrTownName, PropertyLocation.AreaOrRegionName, PropertyLocation.CountryName,";
                    strQuery += "PropertyLocation.PostCode, BuyerRequirement.FromPrice1, BuyerRequirement.ToPrice1, BuyerRequirement.PropertySubTypes, BuyerRequirement.Region, BuyerRequirement.MaxBedrooms,BuyerRequirement.MinBedrooms,BuyerRequirement.RequisiteNotes ";
                    strQuery += "from PeopleContacts Left Join PropertyLocation ON (PeopleContacts.BusinessLocationId=PropertyLocation.LocationId and PeopleContacts.ClientId=PropertyLocation.ClientId) ";
                    strQuery += "Left Join BuyerRequirement ON (PeopleContacts.ContactId=BuyerRequirement.ContactId and PeopleContacts.ClientId=BuyerRequirement.ClientId) ";
                    strQuery += "Where PeopleContacts.ContactId='" + ContactId + "'";

                    SqlCommand cmd = new SqlCommand(strQuery, cn);
                    cn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    if (dr != null && dr.Read())
                    {
                        ds.Tables[0].Rows[0]["title"] = dr["Title"] != null ? dr["Title"] : "";
                        ds.Tables[0].Rows[0]["fname"] = dr["FirstName"] != null ? dr["FirstName"] : "";
                        ds.Tables[0].Rows[0]["lname"] = dr["LastName"] != null ? dr["LastName"] : "";
                        ds.Tables[0].Rows[0]["phone"] = dr["BusinessPhone"] != null ? dr["BusinessPhone"] : "";
                        ds.Tables[0].Rows[0]["Mobile"] = dr["Mobile"] != null ? dr["Mobile"] : "";
                        ds.Tables[0].Rows[0]["ConSource"] = dr["ContactSource"] != null ? dr["ContactSource"] : "";
                        ds.Tables[0].Rows[0]["Notes"] = dr["Notes"] != null ? dr["Notes"] : "";
                        ds.Tables[0].Rows[0]["company"] = dr["CompanyName"] != null ? dr["CompanyName"] : "";
                        ds.Tables[0].Rows[0]["locationid"] = dr["LocationId"];
                        ds.Tables[0].Rows[0]["address"] = dr["Address"] != null ? dr["Address"] : "";
                        ds.Tables[0].Rows[0]["Minbed"] = dr["MinBedrooms"] != null ? dr["MinBedrooms"] : "";
                        ds.Tables[0].Rows[0]["requestNote"] = dr["RequisiteNotes"] != null ? dr["RequisiteNotes"] : "";

                        ds.Tables[0].Rows[0]["FromPrice1"] = dr["FromPrice1"] != null ? dr["FromPrice1"] : "";
                        ds.Tables[0].Rows[0]["ToPrice1"] = dr["ToPrice1"] != null ? dr["ToPrice1"] : "";
                        string PropertySubTypes = dr["PropertySubTypes"] != null ? dr["PropertySubTypes"].ToString() : "";
                        ds.Tables[0].Rows[0]["Region"] = dr["Region"] != null ? dr["Region"] : "";

                        dr.Close();
                        ds.Tables[0].Rows[0]["PropertySubTypes"] = PropertySubTypes;

                        if (PropertySubTypes != "" && PropertySubTypes.ToLower() != "all" && PropertySubTypes.ToLower() != "all;")
                        {
                            strQuery = "Select SubValue from SubType where (MajorId=59) and (";
                            string[] strPropertySubTypes = PropertySubTypes.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                            if (strPropertySubTypes != null && strPropertySubTypes.Length > 0)
                            {
                                foreach (string strItem in strPropertySubTypes)
                                {
                                    strQuery += "SubId=" + strItem.Split(new char[] { '-' })[1] + " OR ";
                                }
                                strQuery = strQuery.TrimEnd(new char[] { 'O', 'R', ' ' });
                                strQuery += ")";
                            }
                            else
                            {
                                strQuery += "SubId=" + PropertySubTypes.Split(new char[] { '-' })[1] + ")";
                            }
                        }
                        else
                        {
                            strQuery = "Select SubValue from SubType where (MajorId=59)";
                        }
                        cmd.CommandText = strQuery;
                        PropertySubTypes = "";
                        dr = cmd.ExecuteReader();
                        if (dr != null)
                        {
                            while (dr.Read())
                            {
                                PropertySubTypes += dr[0].ToString() + ";";
                            }
                        }
                        if (!dr.IsClosed) dr.Close();
                        ds.Tables[0].Rows[0]["PropertySubTypes"] = PropertySubTypes;

                    }
                }
                catch { }
                finally { cn.Close(); }
            }
            else
            {
                ds = new DataSet();
                ds.Tables.Add();
                ds.Tables[0].Columns.Add("EmailId");
                ds.Tables[0].Columns.Add("ContactId");
                ds.Tables[0].Columns.Add("DoNotContact");

                ds.Tables[0].Columns.Add("fname");
                ds.Tables[0].Columns.Add("lname");
                ds.Tables[0].Columns.Add("phone");
                ds.Tables[0].Columns.Add("company");
                ds.Tables[0].Columns.Add("locationid");
                ds.Tables[0].Columns.Add("address");
                ds.Tables[0].Columns.Add("FromPrice1");
                ds.Tables[0].Columns.Add("ToPrice1");
                ds.Tables[0].Columns.Add("PropertySubTypes");
                ds.Tables[0].Columns.Add("Region");
            }
        }
        catch
        {
            ds = new DataSet();
            ds.Tables.Add();
            ds.Tables[0].Columns.Add("EmailId");
            ds.Tables[0].Columns.Add("ContactId");
            ds.Tables[0].Columns.Add("DoNotContact");
            ds.Tables[0].Columns.Add("title");
            ds.Tables[0].Columns.Add("fname");
            ds.Tables[0].Columns.Add("lname");
            ds.Tables[0].Columns.Add("phone");
            ds.Tables[0].Columns.Add("company");
            ds.Tables[0].Columns.Add("locationid");
            ds.Tables[0].Columns.Add("address");
            ds.Tables[0].Columns.Add("city");
            ds.Tables[0].Columns.Add("area");
            ds.Tables[0].Columns.Add("country");
            ds.Tables[0].Columns.Add("postcode");
            ds.Tables[0].Columns.Add("FromPrice1");
            ds.Tables[0].Columns.Add("ToPrice1");
            ds.Tables[0].Columns.Add("PropertySubTypes");
            ds.Tables[0].Columns.Add("Region");
        }
        finally { oleConn.Close(); cn.Close(); }
        return ds;
    }


    public int updateMember(string memberid, string emailid, string pwd)
    {
        int intResult = 0;
        OleDbConnection myConnection = new OleDbConnection(getConnStr("."));
        try
        {
            string sqlQuery = "Update MemberDetails Set [EmailId]='" + emailid.Replace("'", "|@") +
                              "',[LoginPassword]='" + pwd.Replace("'", "|@") +
                              "' Where [MemberId]=" + memberid;
            OleDbCommand myCommand = new OleDbCommand(sqlQuery, myConnection);
            myConnection.Open();
            intResult = myCommand.ExecuteNonQuery();
        }
        catch { intResult = 0; }
        finally { myConnection.Close(); }
        return intResult;
    }

    private int insertNewContact(object objContactId, string strTitle, string firstname, string lstname, string email,
     string otherphone, string workhone, string homephone, string mobilephone, string CompanyName, string contsource,
     string contstatus, object BusinessLocationId, string address, string strMinPrice, string strMaxPrice,
     string strAreas, string strreqbedrooms, string requisitenotes, bool userbuyer, bool userTenant, string businessfax, string country)
    {
        BusinessLocationId = BusinessLocationId == null || BusinessLocationId.ToString() == "" ? System.Guid.NewGuid() : BusinessLocationId;
        object OtherLocationId = System.Guid.NewGuid();
        object HomeLocationId = System.Guid.NewGuid();
        string StrInsertPeopleContacts;
        StrInsertPeopleContacts = "declare @username varchar(1000)\r\nif exists(select top 1 username from userdetails where usertype='admin' and clientid=" + ClientId + ")";
        StrInsertPeopleContacts += " \r\n begin \r\n set @username = (select top 1 username from userdetails where usertype='admin' and clientid=" + ClientId + " ) \r\n end \r\n";
        StrInsertPeopleContacts += "else \r\n begin \r\n set @username = (select top 1 username from userdetails where clientid=" + ClientId + ") \r\n end \r\n ";
        StrInsertPeopleContacts += "INSERT INTO PeopleContacts(ContactId,ClientId,UserName,Title,";
        StrInsertPeopleContacts += "FirstName,MiddleName,LastName,Gender,NickName,IsOwner,IsBuyer,";
        StrInsertPeopleContacts += "IsLandlord,IsNegotiator,IsSolicitor,IsSurveyor,IsTenant,IsVendor,";
        StrInsertPeopleContacts += "ContactSource,ContactStatus,Email,Email2,Email3,";
        StrInsertPeopleContacts += "EmailDisplayAs,Email2DisplayAs,Email3DisplayAs,EmailSelector,";
        StrInsertPeopleContacts += "AssistantPhone,BusinessPhone,BusinessPhone2,BusinessFax,CallbackPhone,";
        StrInsertPeopleContacts += "CarPhone,CompanyPhone,HomePhone,HomePhone2,HomeFax,ISDN,Mobile,";
        StrInsertPeopleContacts += "OtherPhone,OtherFax,Pager,PrimaryPhone,Radio,Telex,TTYTDDPhone,";
        StrInsertPeopleContacts += "Phone1Selector,Phone2Selector,Phone3Selector,Phone4Selector,AddressSelector,";
        StrInsertPeopleContacts += "OtherLocationId,HomeLocationId,BusinessLocationId,MailingAddress,IsArchived,FlagStatus,FollowUpFlag,";
        StrInsertPeopleContacts += "IMAddress,Notes,NotesRecordedHere,Salutation,TimeStampId,CompanyName,WebsiteURL) values(";
        StrInsertPeopleContacts += "'" + objContactId + "',";                   //ContactId
        StrInsertPeopleContacts += ClientId + ",";                              //ClientId
        StrInsertPeopleContacts += "@username,";                                //UserName
        StrInsertPeopleContacts += strTitle + ",";                              //Title 
        StrInsertPeopleContacts += "'" + firstname.Replace("'", "|@") + "',";   //FirstName
        StrInsertPeopleContacts += "'',";                                       //MiddleName
        StrInsertPeopleContacts += "'" + lstname.Replace("'", "|@") + "',";     //LastName
        StrInsertPeopleContacts += 0 + ",";                                     //Gender
        StrInsertPeopleContacts += "'" + email.Replace("'", "|@") + "',";       //NickName
        StrInsertPeopleContacts += "'False',";                                  //IsOwner
        StrInsertPeopleContacts += "'" + userbuyer + "',";                      //IsBuyer
        StrInsertPeopleContacts += "'False',";                                  //IsLandlord
        StrInsertPeopleContacts += "'False',";                                  //IsNegotiator
        StrInsertPeopleContacts += "'False',";                                  //IsSolicitor
        StrInsertPeopleContacts += "'False',";                                  //IsSurveyor
        StrInsertPeopleContacts += "'" + userTenant + "',";                     //IsTenant
        StrInsertPeopleContacts += "'False',";                                  //IsVendor    
        StrInsertPeopleContacts += contsource + ",";                            //ContactSource
        StrInsertPeopleContacts += contstatus + ",";                            //ContactStatus
        StrInsertPeopleContacts += "'" + email.Replace("'", "|@") + "',";       //Email
        StrInsertPeopleContacts += "'',";                                       //Email2
        StrInsertPeopleContacts += "'',";                                       //Email3
        StrInsertPeopleContacts += "'" + email.Replace("'", "|@") + "',";       //EmailDisplayAs
        StrInsertPeopleContacts += "'',";                                       //Email2DisplayAs
        StrInsertPeopleContacts += "'',";                                       //Email3DisplayAs
        StrInsertPeopleContacts += 1 + ",";                                     //EmailSelector
        StrInsertPeopleContacts += "'',";                                   //AssistantPhone
        StrInsertPeopleContacts += "'" + workhone.Replace("'", "|@") + "',"; //BusinessPhone
        StrInsertPeopleContacts += "'',";                                   //BusinessPhone2
        StrInsertPeopleContacts += "'" + businessfax.Replace("'", "|@") + "',";//BusinessFax
        StrInsertPeopleContacts += "'',";                                   //CallbackPhone
        StrInsertPeopleContacts += "'',";                                   //CarPhone
        StrInsertPeopleContacts += "'',";                                   //CompanyPhone
        StrInsertPeopleContacts += "'" + homephone.Replace("'", "|@") + "',";//HomePhone
        StrInsertPeopleContacts += "'',";                                   //HomePhone2
        StrInsertPeopleContacts += "'',";                                   //HomeFax
        StrInsertPeopleContacts += "'',";                                   //ISDN
        StrInsertPeopleContacts += "'" + mobilephone.Replace("'", "|@") + "',";//Mobile
        StrInsertPeopleContacts += "'" + otherphone.Replace("'", "|@") + "',"; //OtherPhone
        StrInsertPeopleContacts += "'',";                                   //OtherFax
        StrInsertPeopleContacts += "'',";                                   //Pager
        StrInsertPeopleContacts += "'',";                                   //PrimaryPhone
        StrInsertPeopleContacts += "'',";                                   //Radio
        StrInsertPeopleContacts += "'',";                                   //Telex
        StrInsertPeopleContacts += "'',";                                   //TTYTDDPhone
        StrInsertPeopleContacts += 1 + ",";                                   //Phone1Selector
        StrInsertPeopleContacts += 2 + ",";                                   //Phone2Selector
        StrInsertPeopleContacts += 3 + ",";                                   //Phone3Selector
        StrInsertPeopleContacts += 4 + ",";                                   //Phone4Selector
        StrInsertPeopleContacts += 1 + ",";                                   //AddressSelector
        StrInsertPeopleContacts += "'" + OtherLocationId + "',";              //OtherLocationId
        StrInsertPeopleContacts += "'" + HomeLocationId + "',";                //HomeLocationId
        StrInsertPeopleContacts += "'" + BusinessLocationId + "',";           //BusinessLocationId
        StrInsertPeopleContacts += 1 + ",";                                   //MailingAddress
        StrInsertPeopleContacts += "'False',";                              //IsArchived
        StrInsertPeopleContacts += 0 + ",";                                   //FlagStatus
        StrInsertPeopleContacts += "'',";                                   //FollowUpFlag
        StrInsertPeopleContacts += "'',";                                   //IMAddress
        StrInsertPeopleContacts += "'',";                                   //Notes
        StrInsertPeopleContacts += "'',";                                   //NotesRecordedHere
        StrInsertPeopleContacts += "'',";                                   //Salutation
        //StrInsertPeopleContacts += "'" + country + "',";                                   //Country
        StrInsertPeopleContacts += "'" + DateTime.Parse(System.DateTime.Now.ToString(), System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat).ToString("MM/dd/yyyy hh:mm:ss") + "',";
        StrInsertPeopleContacts += "'" + CompanyName.Replace("'", "|@") + "',";
        StrInsertPeopleContacts += "'')";

        SqlConnection cn = new SqlConnection(strConn);
        int result = 0;
        try
        {
            SqlCommand cmd = new SqlCommand(StrInsertPeopleContacts, cn);
            cn.Open();
            result = cmd.ExecuteNonQuery();
            if (result > 0)
            {
                insUpdBusinessAddress(cn, cmd, BusinessLocationId, address, OtherLocationId, HomeLocationId);

                if (userbuyer && userTenant)
                {
                    insertRequirement("BuyerRequirement", strMinPrice, strMaxPrice, strAreas, strreqbedrooms, requisitenotes, objContactId);
                    insertRequirement("TenantRequirement", strMinPrice, strMaxPrice, strAreas, strreqbedrooms, requisitenotes, objContactId);
                }
                else if (userTenant)
                {
                    insertRequirement("TenantRequirement", strMinPrice, strMaxPrice, strAreas, strreqbedrooms, requisitenotes, objContactId);
                }
                else if (userbuyer)
                {
                    insertRequirement("BuyerRequirement", strMinPrice, strMaxPrice, strAreas, strreqbedrooms, requisitenotes, objContactId);
                }

            }
        }
        catch
        {
            result = -1;
        }
        finally
        {
            cn.Close();
        }
        OleDbConnection myConnection = new OleDbConnection(getConnStr("."));
        try
        {
            myConnection.Open();
            string sqlsrting = "Update MemberDetails Set [ContactId]='" + objContactId + "' Where [EmailId]='" + email.Replace("'", "|@") + "'";
            OleDbCommand myCommand = new OleDbCommand(sqlsrting, myConnection);
            myCommand.ExecuteNonQuery();
            myConnection.Close();

        }
        catch
        {
        }
        finally { myConnection.Close(); }
        return result;
    }


    private void insUpdBusinessAddress(SqlConnection cn, SqlCommand cmd, object locationid, string address, object OtherLocationId, object HomeLocationId)
    {
        string strInsertAddress = "INSERT INTO PropertyLocation(LocationId,ClientId,Address,";
        strInsertAddress += "CityOrTownId,AreaOrRegionId,CountryId,CityOrTownName,AreaOrRegionName,";
        strInsertAddress += "CountryName,PostCode,LocationAKA,";
        strInsertAddress += "Latitude,Longitude) VALUES('" + locationid + "',";
        strInsertAddress += ClientId + ",";
        strInsertAddress += "'" + address + "',";
        strInsertAddress += 0 + ",";
        strInsertAddress += 0 + ",";
        strInsertAddress += 0 + ",";
        strInsertAddress += "'',";
        strInsertAddress += "'',";
        strInsertAddress += "'',";
        strInsertAddress += "'',";
        strInsertAddress += "'',";
        strInsertAddress += "'',";
        strInsertAddress += "'')";
        cmd = new SqlCommand(strInsertAddress, cn);
        cmd.ExecuteNonQuery();

        strInsertAddress = "INSERT INTO PropertyLocation(LocationId,ClientId,Address,";
        strInsertAddress += "CityOrTownId,AreaOrRegionId,CountryId,CityOrTownName,AreaOrRegionName,";
        strInsertAddress += "CountryName,PostCode,LocationAKA,";
        strInsertAddress += "Latitude,Longitude) VALUES('" + OtherLocationId + "',";
        strInsertAddress += ClientId + ",";
        strInsertAddress += "'',";
        strInsertAddress += 0 + ",";
        strInsertAddress += 0 + ",";
        strInsertAddress += 0 + ",";
        strInsertAddress += "'',";
        strInsertAddress += "'',";
        strInsertAddress += "'',";
        strInsertAddress += "'',";
        strInsertAddress += "'',";
        strInsertAddress += "'',";
        strInsertAddress += "'')";
        cmd = new SqlCommand(strInsertAddress, cn);
        cmd.ExecuteNonQuery();

        strInsertAddress = "INSERT INTO PropertyLocation(LocationId,ClientId,Address,";
        strInsertAddress += "CityOrTownId,AreaOrRegionId,CountryId,CityOrTownName,AreaOrRegionName,";
        strInsertAddress += "CountryName,PostCode,LocationAKA,";
        strInsertAddress += "Latitude,Longitude) VALUES('" + HomeLocationId + "',";
        strInsertAddress += ClientId + ",";
        strInsertAddress += "'',";
        strInsertAddress += 0 + ",";
        strInsertAddress += 0 + ",";
        strInsertAddress += 0 + ",";
        strInsertAddress += "'',";
        strInsertAddress += "'',";
        strInsertAddress += "'',";
        strInsertAddress += "'',";
        strInsertAddress += "'',";
        strInsertAddress += "'',";
        strInsertAddress += "'')";
        cmd = new SqlCommand(strInsertAddress, cn);
        cmd.ExecuteNonQuery();
    }

    private int insertRequirement(string tablename, string minPrice, string maxPrice, string region,
       string strreqbedrooms, string requisitenotes, object ContactId)
    {
        int intResult = 0;
        OleDbConnection myConnection = new OleDbConnection(getConnStr("."));
        SqlConnection cn = new SqlConnection(strConn);
        try
        {
            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.Connection = cn;
            cn.Open();
            int totalregions = 0;
            try
            {
                totalregions = region.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Length;
            }
            catch { }
            if (tablename == "BuyerRequirement")
            {
                string strQuery = "INSERT INTO BuyerRequirement(ContactId,ClientId,SelectedPrice1,";
                strQuery += "SelectedPrice2,FromPrice1,ToPrice1,FromPrice2,ToPrice2,Region,TotalRegions,";
                strQuery += "MinBedrooms,MaxBedrooms,Freehold,Garden,OffStreetParking,NewlyBuilt,Garage,";
                strQuery += "RequisiteNotes,EmailAlerts) VALUES('" + ContactId + "',";
                strQuery += ClientId + ",";
                strQuery += "'Ideal Price',";
                strQuery += "'',";
                strQuery += minPrice + ",";
                strQuery += maxPrice + ",";
                strQuery += 0 + ",";
                strQuery += 0 + ",";
                strQuery += "'" + region.Replace("'", "|@") + "',";
                strQuery += totalregions + ",";
                strQuery += strreqbedrooms + ",";
                strQuery += "99,";
                //strQuery += "'All',";
                //strQuery += "'All',";
                strQuery += 1 + ",";
                strQuery += 1 + ",";
                strQuery += 1 + ",";
                strQuery += 1 + ",";
                strQuery += 1 + ",";
                strQuery += "'" + requisitenotes.Replace("'", "|@") + "',";
                strQuery += "'True')";
                sqlCmd.CommandText = strQuery;
                intResult = sqlCmd.ExecuteNonQuery();
            }
            else if (tablename == "TenantRequirement")
            {
                string strQuery = "INSERT INTO TenantRequirement(ContactId,ClientId,SelectedPrice1,";
                strQuery += "SelectedPrice2,FromPrice1,ToPrice1,FromPrice2,ToPrice2,Region,TotalRegions,";
                strQuery += "MinBedrooms,MaxBedrooms,Furnished,Garden,OffStreetParking,TypeOfLetting,Garage,";
                strQuery += "RequisiteNotes,EmailAlerts) VALUES('" + ContactId + "',";
                strQuery += ClientId + ",";
                strQuery += "'Ideal Price',";
                strQuery += "'',";
                if (minPrice == "")
                { strQuery += "0,"; }
                else { strQuery += minPrice + ","; }

                strQuery += maxPrice + ",";
                strQuery += 0 + ",";
                strQuery += 0 + ",";
                strQuery += "'" + region.Replace("'", "|@") + "',";
                strQuery += totalregions + ",";
                strQuery += strreqbedrooms + ",";
                strQuery += "99,";
                //strQuery += "'All',";
                //strQuery += "'All',";
                strQuery += 1 + ",";
                strQuery += 1 + ",";
                strQuery += 1 + ",";
                strQuery += 1 + ",";
                strQuery += 1 + ",";
                strQuery += "'" + requisitenotes.Replace("'", "|@") + "',";
                strQuery += "'True')";
                sqlCmd.CommandText = strQuery;
                intResult = sqlCmd.ExecuteNonQuery();
            }
        }
        catch
        {
            intResult = -1;
        }
        finally { cn.Close(); }
        return intResult;
    }

    private int updateRequirement(string emailid, string tablename, string minprice, string maxprice, string region, string strreqbedrooms, string requisitenotes, string strAction, string BranchCid)
    {
        int intResult = 0;
        SqlConnection cn = new SqlConnection(strConn);
        object ContactId = null;
        try
        {
            ContactId = getContactId(emailid);
            string strQuery = "";
            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.Connection = cn;
            cn.Open();
            if (strAction == "Delete")
            {
                strQuery = "Delete From " + tablename + " Where ContactId='" + ContactId + "'";
                sqlCmd.CommandText = strQuery;
                intResult = sqlCmd.ExecuteNonQuery();
            }
            else
            {
                strQuery = "UPDATE " + tablename + " SET";
                strQuery += " FromPrice1 = " + minprice;
                strQuery += ", ToPrice1 = " + maxprice;

                strQuery += ", Region = '" + region.Replace("'", "|@");
                strQuery += "', RequisiteNotes = '" + requisitenotes.Replace("'", "|@");
                strQuery += "' Where ContactId = '" + ContactId + "' And ClientId=" + ClientId;
                sqlCmd.CommandText = strQuery;
                intResult = sqlCmd.ExecuteNonQuery();
                if (intResult == 0)
                {
                    cn.Close();
                    //intResult = insertRequirement(tablename, minprice, maxprice, region, strreqbedrooms, requisitenotes, ContactId, BranchCid);
                }
            }
        }
        catch
        {
            intResult = -1;
            cn.Close();
            //intResult = insertRequirement(tablename, minprice, maxprice, region, strreqbedrooms, requisitenotes, ContactId, BranchCid);
        }
        finally { cn.Close(); }
        return intResult;
    }

    public bool registeremail(string emailid, object objContactId, string strTitle,
    string firstname, string lstname, string phone, string mobilephone, string CompanyName, string contsource,
    string contstatus, object BusinessLocationId, string address, string strMinPrice, string strMaxPrice,
    string strAreas, string strreqbedrooms, string requisitenotes, bool userbuyer, bool userTenant)
    {
        bool blndone = false;
        try
        {

            //int result = insertNewContact(objContactId, strTitle, firstname, lstname, emailid, phone, mobilephone,
            //    CompanyName, contsource, contstatus, BusinessLocationId, address, strMinPrice, strMaxPrice, strAreas,
            //    strreqbedrooms, requisitenotes, userbuyer, userTenant);
            //if (result > 0)
            //{

            //    blndone = true;
            //}
        }
        catch { }

        return blndone;
    }

    public int registeremail1(string emailid, object objContactId, string strTitle,
  string firstname, string lstname, string otherphone, string workhone, string homephone, string mobilephone, string CompanyName, string contsource,
  string contstatus, object BusinessLocationId, string address, string strMinPrice, string strMaxPrice,
  string strAreas, string strreqbedrooms, string requisitenotes, bool userbuyer, bool userTenant, string businessfax, string country)
    {
        int result = 0;
        try
        {
            //////int result = insertNewContact(objContactId, strTitle, firstname, lstname, emailid, phone, mobilephone,
            //////     CompanyName, contsource, contstatus, BusinessLocationId, address, strMinPrice, strMaxPrice, strAreas,
            //////     strreqbedrooms, requisitenotes, userbuyer, userTenant);

            result = insertNewContact(objContactId, strTitle, firstname, lstname, emailid, otherphone, workhone, homephone, mobilephone,
                CompanyName, contsource, contstatus, BusinessLocationId, address, strMinPrice, strMaxPrice, strAreas,
                strreqbedrooms, requisitenotes, userbuyer, userTenant, businessfax, country);
            //if (result > 0)
            //{
            //    try
            //    {
            //        blndone = true;
            //        System.Web.Mail.MailMessage msg = new System.Web.Mail.MailMessage();
            //        msg.To = emailid;
            //        msg.Cc = ConfigurationManager.AppSettings["ToEmailId"];
            //        msg.From = ConfigurationManager.AppSettings["FromEmailId"];
            //        msg.Subject = strSubject;
            //        msg.Body = strBody;
            //        msg.Priority = System.Web.Mail.MailPriority.High;
            //        msg.BodyFormat = System.Web.Mail.MailFormat.Html;
            //        System.Web.Mail.SmtpMail.SmtpServer = ConfigurationManager.AppSettings["SmtpServer"];
            //        System.Web.Mail.SmtpMail.Send(msg);
            //    }
            //    catch { }
            //}
        }
        catch { }

        return result;
    }
    #endregion
    #region treeview binding
    public DataTable GetTreeviewPropType(string dealtype, string proptype) //not in use
    {
        DataTable dt = new DataTable();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = @"select distinct
                             (Select SubValue from SubType Where MajorId=Property.PropertyMajorType and SubId=Property.PropertySubType) as proptype,
                              count(propertyid) as totalprop,Property.propertymajortype, Property.propertysubtype ";
            strQuery += "from property where clientid=" + ClientId + " and (dealtype=" + dealtype + " or dealtype=4)";
            if (proptype != "" && proptype != "Any")
            {
                strQuery += " AND (Property.PropertyMajorType=" + proptype.Split(new char[] { '_' })[0] + " AND ";
                strQuery += "Property.PropertySubType=" + proptype.Split(new char[] { '_' })[1] + ")";
            }
            strQuery += "group by Property.PropertyMajorType,Property.PropertySubType ";
            strQuery += "order by proptype";

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);

            da.Fill(dt);
        }
        catch
        {
        }
        finally { }
        return dt;
    }
    #endregion

    #region Handle exception and Send Email
    public void HandleException(Exception ex, string strSubject, string strQuery)
    {
        String strError = "Error Message: " + ex.Message + "<br><br>";
        if (ex.InnerException != null)
        {
            strError += "InnerException: " + Convert.ToString(ex.InnerException) + "<br><br>";
        }
        strError += "Stack Trace: " + ex.StackTrace + "<br><br>";
        strError += "source: " + ex.Source + "<br><br>";

        if (strQuery != null && strQuery != "")
        {
            strError += "\r\n <br> Query: " + strQuery;
        }

        SendEmailList(strExceptionEmail, strSubject, strError);
    }
    #endregion

    public DataSet getLatestProp()
    {
        string strImgPath = ConfigurationManager.AppSettings["ThumbImagePath"];
        string sqlQuery = "";
        sqlQuery = @"SELECT Top 3 Property.PropertyId,Property.DealType,Property.Reference,Property.propertymajortype, Property.propertysubtype,Property.timestampid,Property.IsNewHome,
                    (Select SubValue from SubType Where MajorId=11 and SubId=Property.Status) as Status,
                     CONVERT(VARCHAR(12), Property.timestampid, 107) as PropertyDate ,
                    (Select SubValue from SubType Where MajorId=Property.PropertyMajorType and SubId=Property.PropertySubType) as proptype,
                    (Select majorValue from majorType Where MajorId=Property.PropertyMajorType) as majortype,
                    (Select top 1  Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId and Files.Category like 'Main photo') As LinkMainImage, 
                    (Select top 1 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId
                    and Files.Category like 'Main photo') As image, PropertyDescriptive.ShortDesc,PropertyLocation.AreaOrRegionName,PropertyLocation.CityOrTownName,PropertyLocation.CountryName,
                    (select count(*) from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId and Files.Category in('Main photo','photo') ) As imagecount,
                    (select count(*) from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId and Files.Category in('Photo link') ) As Linkimagecount, 
                    (SELECT count(*) FROM PropertyRoom Inner JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                    Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=1) as NumberOfBedrooms,
                    (SELECT count(*) FROM PropertyRoom Inner JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                    Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=40) as NumberOfBathrooms,
                    (SELECT count(*) FROM PropertyRoom Inner JOIN PropertyRoomMain ON PropertyRoom.PRoomMainKeyId = PropertyRoomMain.PRoomMainKeyId and PropertyRoom.ClientId = PropertyRoomMain.ClientId
                    Where PropertyRoomMain.PropertyId = Property.propertyId and PropertyRoom.ClientId = Property.ClientId and PropertyRoomMain.RoomId=6) as NumberOfReceptions,
                    PropertyLocation.Address + ', ' + PropertyLocation.CityOrTownName as Address,PropertyLocation.StreetViewAttributes,
                    PropertyLocation.PropertyNameNumber+', '+PropertyLocation.Address as street, PropertyLocation.PostCode,PropertyLocation.Latitude,PropertyLocation.Longitude,PropertyLocation.ShowGSVOnWebsite,
                    PropertyLet.Price As Cost, PropertyLet.Price As CostToOrder, PropertyLet.PriceSuffix,PropertyLet.PricePer FROM Property Left Join PropertyDescriptive ON 
                    (Property.PropertyId = PropertyDescriptive.PropertyId and Property.ClientId = PropertyDescriptive.ClientId)
                    Left JOIN PropertyLocation ON (Property.LocationId = PropertyLocation.LocationId and Property.ClientId = PropertyLocation.ClientId)
                    Left JOIN UserDetails ON (Property.UserName = UserDetails.UserName and Property.ClientId = UserDetails.ClientId)
                    Inner Join PropertyLet ON (Property.PropertyId = PropertyLet.PropertyId and Property.ClientId = PropertyLet.ClientId) And Status=0 And Property.FeaturedProperty !='True' And Property.ClientId=" + ClientId + " and Property.Reference= ''  Order by Property.TimeStampId Desc";

        SqlConnection Conn = new SqlConnection(strConn);
        SqlDataAdapter PropertyDA = new SqlDataAdapter(sqlQuery, Conn);
        DataSet ds = new DataSet();
        SqlDataReader dr = null;
        int j = 1;
        try
        {

            PropertyDA.Fill(ds);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {

                ds.Tables[0].Columns.Add("CostToShow");
                ds.Tables[0].Columns.Add("image1");
                ds.Tables[0].Columns.Add("image2");
                double price = 0;

                double dblminprice = 0;
                double dblmaxprice = 0;
                try
                {
                    // dblminprice = Convert.ToDouble(strminprice);
                    // dblmaxprice = Convert.ToDouble(strmaxprice);
                }
                catch { }
                SqlCommand cmd = new SqlCommand();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    try
                    {

                        try
                        {
                            if (ds.Tables[0].Rows[i]["Linkimagecount"] != null && ds.Tables[0].Rows[i]["Linkimagecount"].ToString() != "" && ds.Tables[0].Rows[i]["Linkimagecount"].ToString() != "0")
                            {
                                ds.Tables[0].Rows[i]["imagecount"] = ds.Tables[0].Rows[i]["Linkimagecount"].ToString();
                            }

                            //if (minBedRooms == "0" && maxBedRooms == "0" && Convert.ToInt32(ds.Tables[0].Rows[i]["NumberOfBedrooms"]) > 0)
                            //{
                            //    ds.Tables[0].Rows.RemoveAt(i);
                            //    i--;
                            //    continue;
                            //}
                            //else if (int.Parse(ds.Tables[0].Rows[i]["NumberOfBedrooms"].ToString()) < int.Parse(minBedRooms))
                            //{
                            //    ds.Tables[0].Rows.RemoveAt(i);
                            //    i--;
                            //    continue;
                            //}
                            //else if (int.Parse(maxBedRooms) > 0 && int.Parse(ds.Tables[0].Rows[i]["NumberOfBedrooms"].ToString()) > int.Parse(maxBedRooms))
                            //{
                            //    ds.Tables[0].Rows.RemoveAt(i);
                            //    i--;
                            //    continue;
                            //}
                        }
                        catch { }
                        DataRow row = ds.Tables[0].Rows[i];

                        row["Address"] = row["Address"].ToString().Replace("|@", "'").TrimEnd(new char[] { ',', ' ' }).TrimStart(new char[] { ',', ' ' });
                        row["PostCode"] = row["PostCode"].ToString().Replace("|@", "'");

                        if (ds.Tables[0].Columns.Contains("PricePer"))
                        {
                            switch (ds.Tables[0].Rows[i]["PricePer"].ToString())
                            {
                                case "0": //week
                                    ds.Tables[0].Rows[i]["CostToShow"] = " pcm";
                                    price = Convert.ToDouble(ds.Tables[0].Rows[i]["Cost"]);
                                    price = (price * 52) / 12;
                                    ds.Tables[0].Rows[i]["CostToOrder"] = price;
                                    break;
                                case "1": //month
                                    ds.Tables[0].Rows[i]["CostToShow"] = " pcm";
                                    price = Convert.ToDouble(ds.Tables[0].Rows[i]["Cost"]);
                                    ds.Tables[0].Rows[i]["CostToOrder"] = price;
                                    break;
                                case "2": //quarter
                                    ds.Tables[0].Rows[i]["CostToShow"] = " pcm";
                                    price = Convert.ToDouble(ds.Tables[0].Rows[i]["Cost"]);
                                    price = (price / 3);
                                    ds.Tables[0].Rows[i]["CostToOrder"] = price;
                                    break;
                                case "3": //year
                                    ds.Tables[0].Rows[i]["CostToShow"] = " pcm";
                                    price = Convert.ToDouble(ds.Tables[0].Rows[i]["Cost"]);
                                    price = (price / 12);
                                    ds.Tables[0].Rows[i]["CostToOrder"] = price;
                                    break;
                                default:
                                    ds.Tables[0].Rows[i]["CostToShow"] = " pcm";
                                    price = Convert.ToDouble(ds.Tables[0].Rows[i]["Cost"]);
                                    ds.Tables[0].Rows[i]["CostToOrder"] = price;
                                    break;
                            }
                        }
                        ds.Tables[0].Rows[i]["Cost"] = ds.Tables[0].Rows[i]["CostToOrder"].ToString();
                        string[] addressparts = ds.Tables[0].Rows[i]["Address"].ToString().Replace("|@", "'").Split(new char[] { ',' });
                        string address = addressparts[0];
                        address += addressparts.Length > 1 ? ", " + addressparts[1] : "";
                        address += addressparts.Length > 2 ? ", " + addressparts[2] : "";
                        address += addressparts.Length > 4 ? ", " + addressparts[4] : "";
                        address = address.TrimEnd(new char[] { ',', ' ' });
                        ds.Tables[0].Rows[i]["Address"] = address;
                        //if (strproptype == "2")
                        //{
                        //    if (strminprice == "2000$" && strmaxprice == "3000$" && (price <= Convert.ToDouble("3000")))
                        //    {
                        //        ds.Tables[0].Rows.RemoveAt(i);
                        //        i--;
                        //        continue;
                        //    }
                        //    else if (strminprice == "2000$" && strmaxprice != "0" && (price >= Convert.ToDouble("2000") || price <= Convert.ToDouble(strmaxprice)))
                        //    {
                        //        ds.Tables[0].Rows.RemoveAt(i);
                        //        i--;
                        //        continue;
                        //    }
                        //    else if (strmaxprice == "3000$" && strminprice != "0" && (price >= Convert.ToDouble("3000") || price <= Convert.ToDouble(strminprice)))
                        //    {
                        //        ds.Tables[0].Rows.RemoveAt(i);
                        //        i--;
                        //        continue;
                        //    }
                        //    else if (strmaxprice == "0" && strminprice == "2000$" && price <= Convert.ToDouble("2000"))
                        //    {
                        //        ds.Tables[0].Rows.RemoveAt(i);
                        //        i--;
                        //        continue;
                        //    }
                        //    else if (strminprice == "0" && strmaxprice == "3000$" && price <= Convert.ToDouble("3000"))
                        //    {
                        //        ds.Tables[0].Rows.RemoveAt(i);
                        //        i--;
                        //        continue;
                        //    }
                        //    else if (strmaxprice == "0" && strminprice != "0" && price < Convert.ToDouble(strminprice))
                        //    {
                        //        ds.Tables[0].Rows.RemoveAt(i);
                        //        i--;
                        //        continue;
                        //    }
                        //    else if (strminprice == "0" && strmaxprice != "0" && price >= Convert.ToDouble(strmaxprice))
                        //    {
                        //        ds.Tables[0].Rows.RemoveAt(i);
                        //        i--;
                        //        continue;
                        //    }
                        //    else if (strminprice != "0" && strmaxprice != "0" && (price >= Convert.ToDouble(strmaxprice) || price <= Convert.ToDouble(strminprice)))
                        //    {
                        //        ds.Tables[0].Rows.RemoveAt(i);
                        //        i--;
                        //        continue;
                        //    }
                        //}

                        try
                        {
                            if (ds.Tables[0].Rows[i]["LinkMainImage"].ToString() != "" && (ds.Tables[0].Rows[i]["LinkMainImage"].ToString().Contains("http://") || ds.Tables[0].Rows[i]["LinkMainImage"].ToString().Contains("www.")))
                            {
                                ds.Tables[0].Rows[i]["image"] = ds.Tables[0].Rows[i]["LinkMainImage"].ToString();
                            }
                            else
                            {
                                if (ds.Tables[0].Rows[i]["image"].ToString() != "" && !(ds.Tables[0].Rows[i]["image"].ToString().Contains("http://") || ds.Tables[0].Rows[i]["image"].ToString().Contains("www.")))
                                {
                                    string mainImage = ds.Tables[0].Rows[i]["image"].ToString();
                                    string[] strimagename = mainImage.ToString().Split(new char[] { '.' });
                                    string strImage = strimagename[0];
                                    string strexten = strimagename[1];

                                    ds.Tables[0].Rows[i]["image"] = strImgPath + strImage + "_thumb." + strexten;
                                }
                            }
                        }
                        catch { }

                        string strQuery = "";
                        j = 1;
                        try
                        {
                            if (ds.Tables[0].Rows[i]["LinkMainImage"] != null && ds.Tables[0].Rows[i]["LinkMainImage"].ToString() != ""
                                && (ds.Tables[0].Rows[i]["LinkMainImage"].ToString().Contains("http://") || ds.Tables[0].Rows[i]["LinkMainImage"].ToString().Contains("www.")))
                            {
                                try
                                {
                                    strQuery = @"SELECT top 2 Files.FileName FROM Files WHERE Files.ModuleId='" + ds.Tables[0].Rows[i]["PropertyId"] +
                                                "' and Files.Category like 'Photo' and Files.ClientId=" + ClientId + " Order by Files.Priority Asc";
                                    //strQuery = @"SELECT top 2 Files.FileName FROM Files WHERE Files.ModuleId='" + ds.Tables[0].Rows[i]["PropertyId"] + "' and 
                                    //Files.Category like 'Photo link' and Files.ClientId=" + ClientId + " and Priority Not in(0,1) Order by Files.Priority Asc";
                                    //prority is cheked not in 0,1 to exclude  images with o priority it can be other images like epc and floorplan and 1 is for main image

                                    cmd = new SqlCommand(strQuery, Conn);
                                    if (Conn.State == ConnectionState.Closed)
                                    { Conn.Open(); }
                                    dr = cmd.ExecuteReader();
                                    while (dr.Read())
                                    {
                                        //ds.Tables[0].Rows[i]["image" + j] = dr[0].ToString();
                                        //j++;

                                        if (!ds.Tables[0].Columns.Contains("image" + j))
                                            ds.Tables[0].Columns.Add("image" + j);

                                        ds.Tables[0].Rows[i]["image" + j] = dr[0];
                                        j++;
                                    }
                                    if (!dr.IsClosed) dr.Close();
                                }
                                catch { }
                                finally
                                {
                                    if (Conn.State == ConnectionState.Open)
                                    { Conn.Close(); }
                                }
                            }
                            else
                            {
                                try
                                {
                                    strQuery = @"SELECT top 2 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName as image,
                                                 Files.FileName FROM Files WHERE Files.ModuleId='" + ds.Tables[0].Rows[i]["PropertyId"] +
                                                "' and Files.Category like 'Photo' and Files.ClientId=" + ClientId + " Order by Files.Priority Asc";
                                    //strQuery = @"SELECT top 2 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName FROM Files 
                                    //WHERE Files.ModuleId='" + ds.Tables[0].Rows[i]["PropertyId"] + "' and Files.Category like 'Photo' and Files.ClientId=" + ClientId + " Order by Files.Priority Asc";
                                    cmd = new SqlCommand(strQuery, Conn);
                                    if (Conn.State == ConnectionState.Closed)
                                    {
                                        Conn.Open();
                                    }
                                    dr = cmd.ExecuteReader();
                                    while (dr.Read())
                                    {
                                        //string[] strimagename = dr[0].ToString().Split(new char[] { '.' });
                                        //string strImage = strimagename[0];
                                        //string strexten = strimagename[1];

                                        //ds.Tables[0].Rows[i]["image" + j] = strImgPath + strImage + "_thumb." + strexten;
                                        //j++;

                                        if (!ds.Tables[0].Columns.Contains("image" + j))
                                            ds.Tables[0].Columns.Add("image" + j);

                                        if (dr[1] != null && dr[1].ToString() != "" && (dr[1].ToString().Contains("http://") || dr[1].ToString().Contains("www.")))
                                        {
                                            ds.Tables[0].Rows[i]["image" + j] = dr[1];
                                        }
                                        else
                                        {
                                            string Images = dr[0].ToString();
                                            string[] strimagename = Images.ToString().Split(new char[] { '.' });
                                            string strImage = strimagename[0];
                                            string strexten = strimagename[1];

                                            ds.Tables[0].Rows[i]["image" + j] = strImgPath + strImage + "_thumb." + strexten;
                                        }

                                        j++;
                                    }
                                    if (!dr.IsClosed) dr.Close();
                                }
                                catch
                                { }
                                finally
                                {
                                    if (Conn.State == ConnectionState.Open)
                                    {
                                        Conn.Close();
                                    }
                                }
                            }
                            //ds.Tables[0].Rows[0]["imagescount"] = j;
                            if (!dr.IsClosed) dr.Close();
                        }
                        catch { }

                    }
                    catch { }
                }

            }
        }
        catch (Exception ex)
        {
        }
        finally
        {
            if (Conn.State == ConnectionState.Open) { Conn.Close(); }
        }
        return ds;

    }

    #region fetch propertyid---------------
    public string getPropertyID(string RefNo, string id, string buyrent)
    {
        SqlConnection sqlConn = new SqlConnection(strConn);
        string strQuerry = "";
        if (buyrent == "2")
        {
            if (RefNo != "")
            {
                strQuerry = "select distinct Property.propertyID from Property where ";
                strQuerry += " (Property.Reference = '" + RefNo + "') ";
                strQuerry += " and Property.clientid='" + ClientId + "'";
            }
            else
            {
                strQuerry = "select distinct Property.propertyID from Property where ";
                strQuerry += "(Property.propertyID like '%" + id + "%')";
                strQuerry += " and Property.clientid='" + ClientId + "'";
            }
        }
        else
        {
            if (RefNo != "")
            {
                strQuerry = "select distinct Property.propertyID from Property where ";
                strQuerry += " (Property.Reference = '" + RefNo + "')";
                strQuerry += " and Property.clientid='" + ClientId + "'";
            }
            else
            {
                strQuerry = "select distinct Property.propertyID from Property where ";
                strQuerry += "(Property.propertyID like '%" + id + "%')";
                strQuerry += " and Property.clientid='" + ClientId + "'";
            }
        }

        SqlCommand sqlCmd = new SqlCommand(strQuerry, sqlConn);
        string strproopID = null;
        sqlConn.Open();
        try
        {
            object proopID = sqlCmd.ExecuteScalar();
            strproopID = proopID.ToString();
        }
        catch { strproopID = ""; }
        finally
        {
            sqlConn.Close();
        }
        return strproopID;
    }

    public string getPropertyID1(string country, string region, string City, string RefNo, string id, string buyrent)
    {
        if (country == "confidentialproperty")
        {
            country = "";
        }
        if (region == "confidentialproperty")
        {
            region = "";
            City = "";
        }
        if (region == "confidentialregion")
        {
            region = "";
        }
        if (City == "confidentialcity")
        {
            City = "";
        }

        SqlConnection sqlConn = new SqlConnection(strConn);
        string strQuerry = "";
        if (buyrent == "2")
        {
            if (RefNo != "")
            {
                strQuerry = "select distinct Property.propertyID from Property,Propertylocation where Property.Locationid=Propertylocation.locationid ";
                if (region == "")
                {
                    strQuerry += " and (Propertylocation.AreaOrRegionName= '' or Propertylocation.AreaOrRegionName is null) ";
                }
                else
                {
                    strQuerry += "and Propertylocation.AreaOrRegionName= '" + region.Replace("'", "|@").Replace("$and$", "&") + "' ";
                }
                if (City == "")
                {
                    strQuerry += " and (Propertylocation.CityOrTownName ='' or Propertylocation.CityOrTownName is null)";
                }
                else
                {
                    strQuerry += " and Propertylocation.CityOrTownName ='" + City.Replace("'", "|@").Replace("$and$", "&") + "'";
                }

                strQuerry += " and (Property.propertyID ='" + RefNo + "')";
                strQuerry += " and Property.clientid='" + ClientId + "'";
            }
            else
            {
                strQuerry = "select distinct Property.propertyID from Property,Propertylocation where Property.Locationid=Propertylocation.locationid ";
                if (country == "")
                {
                    strQuerry += " and (Propertylocation.CountryName= '' or Propertylocation.CountryName is null) ";
                }
                else
                {
                    strQuerry += " and Propertylocation.CountryName= '" + country.Replace("'", "|@").Replace("$and$", "&") + "' ";
                }
                if (region == "")
                {
                    strQuerry += " and (Propertylocation.AreaOrRegionName= '' or Propertylocation.AreaOrRegionName is null) ";
                }
                else
                {
                    strQuerry += " and Propertylocation.AreaOrRegionName= '" + region.Replace("'", "|@").Replace("$and$", "&") + "' ";
                }
                if (City == "")
                {
                    strQuerry += " and (Propertylocation.CityOrTownName ='' or Propertylocation.CityOrTownName is null)";
                }
                else
                {
                    strQuerry += " and Propertylocation.CityOrTownName ='" + City.Replace("'", "|@").Replace("$and$", "&") + "' ";
                }
                strQuerry += " and (Property.PropertyId like '%" + id + "%')";
                strQuerry += " and Property.clientid='" + ClientId + "'";
            }
        }
        else
        {
            if (RefNo != "")
            {
                strQuerry = "select distinct Property.propertyID from Property,Propertylocation where Property.Locationid=Propertylocation.locationid ";
                if (region == "")
                {
                    strQuerry += " and (Propertylocation.AreaOrRegionName= '' or Propertylocation.AreaOrRegionName is null) ";
                }
                else
                {
                    strQuerry += "and Propertylocation.AreaOrRegionName= '" + region.Replace("'", "|@").Replace("$and$", "&") + "' ";
                }
                if (City == "")
                {
                    strQuerry += " and (Propertylocation.CityOrTownName ='' or Propertylocation.CityOrTownName is null)";
                }
                else
                {
                    strQuerry += " and Propertylocation.CityOrTownName ='" + City.Replace("'", "|@").Replace("$and$", "&") + "'";
                }

                strQuerry += " and (Property.propertyID like '%" + RefNo + "%')";
                strQuerry += " and Property.clientid='" + ClientId + "'";
                //strQuerry += " and PropertySale.PropertyId=property.PropertyId and Property.clientid='" + ClientId + "'";
            }
            else
            {
                strQuerry = "select distinct Property.propertyID from Property,Propertylocation where Property.Locationid=Propertylocation.locationid ";
                if (country == "")
                {
                    strQuerry += " and (Propertylocation.CountryName= '' or Propertylocation.CountryName is null) ";
                }
                else
                {
                    strQuerry += " and Propertylocation.CountryName= '" + country.Replace("'", "|@").Replace("$and$", "&") + "' ";
                }
                if (region == "")
                {
                    strQuerry += " and (Propertylocation.AreaOrRegionName= '' or Propertylocation.AreaOrRegionName is null) ";
                }
                else
                {
                    strQuerry += " and Propertylocation.AreaOrRegionName= '" + region.Replace("'", "|@").Replace("$and$", "&") + "' ";
                }
                if (City == "")
                {
                    strQuerry += " and (Propertylocation.CityOrTownName ='' or Propertylocation.CityOrTownName is null)";
                }
                else
                {
                    strQuerry += " and Propertylocation.CityOrTownName ='" + City.Replace("'", "|@").Replace("$and$", "&") + "' ";
                }

                strQuerry += " and (Property.PropertyId like '%" + id + "%')";

                strQuerry += " and Property.clientid='" + ClientId + "'";
            }
        }

        SqlCommand sqlCmd = new SqlCommand(strQuerry, sqlConn);
        string strproopID = null;
        sqlConn.Open();
        try
        {
            object proopID = sqlCmd.ExecuteScalar();
            strproopID = proopID.ToString();
        }
        catch { strproopID = ""; }
        finally
        {
            sqlConn.Close();
        }
        return strproopID;
    }
    #endregion

    #region property full details XML downloading and updating
    public bool DownLoadXML(string strXMLUrl, string strPath)
    {
        bool blnSuccess = false;
        try
        {
            WebRequest request = WebRequest.Create(strXMLUrl);
            WebResponse response = request.GetResponse();
            Stream data = response.GetResponseStream();
            string html = String.Empty;
            using (StreamReader sr = new StreamReader(data))
            {
                html = sr.ReadToEnd();
            }

            if (html.Contains("Access denied"))
            {
                return blnSuccess;
            }
            else
            {
                blnSuccess = true;
                StreamWriter sw = new StreamWriter(strPath);
                try
                {
                    sw.Write(html);
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    sw.Flush();
                    sw.Close();
                    sw.Dispose();
                }
            }
        }
        catch (Exception ex)
        {
            //lblmsg.Text += "Some error occured";
        }
        return blnSuccess;
    }
    public void UpdateXML(string strPath, string NodetoRemove)
    {
        XmlDocument xmldoc = new XmlDocument();
        xmldoc.Load(strPath);
        XmlNode node = xmldoc.SelectSingleNode(NodetoRemove);
        //XmlNode node = xmldoc.SelectSingleNode("/response/propertySearchSales/properties/pages");
        node.ParentNode.RemoveChild(node);
        xmldoc.Save(strPath);
    }
    #endregion

    #region XML importing

    public void GetPropertyTypes()
    {
        DataSet dsPropType = new DataSet();
        dsPropType.Tables.Add();

        dsPropType.Tables[0].Columns.Add("Dezrez Property Type values");
        dsPropType.Tables[0].Columns.Add("Type");
        dsPropType.Tables[0].Columns.Add("Estate CRM Major Id");
        dsPropType.Tables[0].Columns.Add("Estate CRM Sub Id");
        dsPropType.Tables[0].Columns.Add("Sub Value");


        //-------------------------------For Flat -------------------------------------


        dsPropType.Tables[0].Rows.Add(new object[] { "9", "Flat", "2", "8", "Flat" });
        dsPropType.Tables[0].Rows.Add(new object[] { "10", "Flat", "2", "8", "Flat" });
        dsPropType.Tables[0].Rows.Add(new object[] { "43", "Flat", "2", "8", "Flat" });
        dsPropType.Tables[0].Rows.Add(new object[] { "46", "Flat", "2", "8", "Flat" });
        dsPropType.Tables[0].Rows.Add(new object[] { "58", "Flat", "2", "8", "Flat" });
        dsPropType.Tables[0].Rows.Add(new object[] { "59", "Flat", "2", "8", "Flat" });
        dsPropType.Tables[0].Rows.Add(new object[] { "71", "Flat", "2", "8", "Flat" });
        dsPropType.Tables[0].Rows.Add(new object[] { "44", "Flat", "2", "8", "Flat" });
        dsPropType.Tables[0].Rows.Add(new object[] { "45", "Flat", "2", "8", "Flat" });
        dsPropType.Tables[0].Rows.Add(new object[] { "47", "Flat", "2", "8", "Flat" });
        dsPropType.Tables[0].Rows.Add(new object[] { "48", "Flat", "2", "8", "Flat" });
        dsPropType.Tables[0].Rows.Add(new object[] { "67", "Flat", "2", "8", "Flat" });
        dsPropType.Tables[0].Rows.Add(new object[] { "51", "Flat", "2", "8", "Flat" });
        dsPropType.Tables[0].Rows.Add(new object[] { "49", "Flat", "2", "8", "Flat" });
        dsPropType.Tables[0].Rows.Add(new object[] { "50", "Flat", "2", "8", "Flat" });
        dsPropType.Tables[0].Rows.Add(new object[] { "52", "Flat", "2", "8", "Flat" });
        dsPropType.Tables[0].Rows.Add(new object[] { "68", "Flat", "2", "8", "Flat" });
        dsPropType.Tables[0].Rows.Add(new object[] { "72", "Flat", "2", "8", "Flat" });


        //--------------------For Detached house--------------------------------------

        dsPropType.Tables[0].Rows.Add(new object[] { "5", "House (Detached)", "1", "4", "Detached house" });
        dsPropType.Tables[0].Rows.Add(new object[] { "6", "House (Detached)", "1", "4", "Detached house" });
        dsPropType.Tables[0].Rows.Add(new object[] { "29", "House (Detached)", "1", "4", "Detached house" });
        dsPropType.Tables[0].Rows.Add(new object[] { "30", "House (Detached)", "1", "4", "Detached house" });
        dsPropType.Tables[0].Rows.Add(new object[] { "50", "House (Detached)", "1", "4", "Detached house" });
        dsPropType.Tables[0].Rows.Add(new object[] { "53", "House (Detached)", "1", "4", "Detached house" });
        dsPropType.Tables[0].Rows.Add(new object[] { "65", "House (Detached)", "1", "4", "Detached house" });
        dsPropType.Tables[0].Rows.Add(new object[] { "66", "House (Detached)", "1", "4", "Detached house" });

        //--------------------For Semi-detached house----------------------------------------------------

        dsPropType.Tables[0].Rows.Add(new object[] { "4", "House (Semi-Detached)", "1", "3", "Semi-detached house" });
        dsPropType.Tables[0].Rows.Add(new object[] { "7", "House (Semi-Detached)", "1", "3", "Semi-detached house" });
        dsPropType.Tables[0].Rows.Add(new object[] { "17", "House (Semi-Detached)", "1", "3", "Semi-detached house" });
        dsPropType.Tables[0].Rows.Add(new object[] { "18", "House (Semi-Detached)", "1", "3", "Semi-detached house" });
        dsPropType.Tables[0].Rows.Add(new object[] { "28", "House (Semi-Detached)", "1", "3", "Semi-detached house" });
        dsPropType.Tables[0].Rows.Add(new object[] { "31", "House (Semi-Detached)", "1", "3", "Semi-detached house" });
        dsPropType.Tables[0].Rows.Add(new object[] { "32", "House (Semi-Detached)", "1", "3", "Semi-detached house" });
        dsPropType.Tables[0].Rows.Add(new object[] { "33", "House (Semi-Detached)", "1", "3", "Semi-detached house" });
        dsPropType.Tables[0].Rows.Add(new object[] { "34", "House (Semi-Detached)", "1", "3", "Semi-detached house" });
        dsPropType.Tables[0].Rows.Add(new object[] { "54", "House (Semi-Detached)", "1", "3", "Semi-detached house" });
        dsPropType.Tables[0].Rows.Add(new object[] { "55", "House (Semi-Detached)", "1", "3", "Semi-detached house" });
        dsPropType.Tables[0].Rows.Add(new object[] { "61", "House (Semi-Detached)", "1", "3", "Semi-detached house" });
        dsPropType.Tables[0].Rows.Add(new object[] { "2", "House (Semi-Detached)", "1", "3", "Semi-detached house" });
        dsPropType.Tables[0].Rows.Add(new object[] { "12", "House (Semi-Detached)", "1", "3", "Semi-detached house" });
        dsPropType.Tables[0].Rows.Add(new object[] { "20", "House (Semi-Detached)", "1", "3", "Semi-detached house" });
        dsPropType.Tables[0].Rows.Add(new object[] { "26", "House (Semi-Detached)", "1", "3", "Semi-detached house" });
        dsPropType.Tables[0].Rows.Add(new object[] { "36", "House (Semi-Detached)", "1", "3", "Semi-detached house" });

        //-----------------For Terraced house-- -------------------------------------------------------

        dsPropType.Tables[0].Rows.Add(new object[] { "1", "House (Terraced)", "1", "1", "Terraced house" });
        dsPropType.Tables[0].Rows.Add(new object[] { "2", "House (Terraced)", "1", "1", "Terraced house" });
        dsPropType.Tables[0].Rows.Add(new object[] { "3", "House (Terraced)", "1", "1", "Terraced house" });
        dsPropType.Tables[0].Rows.Add(new object[] { "8", "House (Terraced)", "1", "1", "Terraced house" });
        dsPropType.Tables[0].Rows.Add(new object[] { "25", "House (Terraced)", "1", "1", "Terraced house" });
        dsPropType.Tables[0].Rows.Add(new object[] { "26", "House (Terraced)", "1", "1", "Terraced house" });
        dsPropType.Tables[0].Rows.Add(new object[] { "27", "House (Terraced)", "1", "1", "Terraced house" });
        dsPropType.Tables[0].Rows.Add(new object[] { "56", "House (Terraced)", "1", "1", "Terraced house" });

        //-------------------For Detached bungalow------------------------------------------------------------

        dsPropType.Tables[0].Rows.Add(new object[] { "15", "Bungalow (Detached)", "45", "15", "Detached bungalow" });
        dsPropType.Tables[0].Rows.Add(new object[] { "16", "Bungalow (Detached)", "45", "15", "Detached bungalow" });
        dsPropType.Tables[0].Rows.Add(new object[] { "39", "Bungalow (Detached)", "45", "15", "Detached bungalow" });

        //------------------For Semi-detached bungalow---------------------------------------------------------------

        dsPropType.Tables[0].Rows.Add(new object[] { "14", "Bungalow (Semi-Detached)", "45", "14", "Semi-detached bungalow" });
        dsPropType.Tables[0].Rows.Add(new object[] { "38", "Bungalow (Semi-Detached)", "45", "14", "Semi-detached bungalow" });

        //------------------For Terraced bungalow-----------------------------------------------------------------------------------------

        dsPropType.Tables[0].Rows.Add(new object[] { "11", "Bungalow (Terraced)", "45", "13", "Terraced bungalow" });
        dsPropType.Tables[0].Rows.Add(new object[] { "12", "Bungalow (Terraced)", "45", "13", "Terraced bungalow" });
        dsPropType.Tables[0].Rows.Add(new object[] { "13", "Bungalow (Terraced)", "45", "13", "Terraced bungalow" });
        dsPropType.Tables[0].Rows.Add(new object[] { "35", "Bungalow (Terraced)", "45", "13", "Terraced bungalow" });
        dsPropType.Tables[0].Rows.Add(new object[] { "36", "Bungalow (Terraced)", "45", "13", "Terraced bungalow" });
        dsPropType.Tables[0].Rows.Add(new object[] { "37", "Bungalow (Terraced)", "45", "13", "Terraced bungalow" });

    }

    public string SaveToEstateCRMDatabase(DataTable dt, string strDetailsXMLPath)
    {
        string strResult = "";

        string strType = "";
        string strPricePer = "";

        string strFullDetailsXMLURL = "http://www.dezrez.com/DRApp/DotNetSites/WebEngine/property/Property.aspx?apikey=354A3470-8530-4ABB-9D5D-76B05AD15C7D&eaid=924&xslt=-1";

        string steTempPath = strFullDetailsXMLURL;
        string strTempDetailsPath = strDetailsXMLPath;

        DataSet ds = new DataSet();
        string strLongDesc = "";

        DataTable ObjDtRefNoTemp = new DataTable();
        SqlConnection sqlConn = new SqlConnection(strConn);
        SqlCommand sqlCmd = new SqlCommand();

        sqlCmd.Connection = sqlConn;

        try
        {
            sqlConn.Open();
            int insertcount = 0;
            //int updatecount = 0;

            object PropId = "";
            int intResult = 0;
            string LocationId = "";

            ObjDtRefNoTemp = GetRefNos();
            string ReferenceId = "";

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ReferenceId = dt.Rows[i]["id"].ToString();
                DataRow[] dr = ObjDtRefNoTemp.Select("[ReferenceNo]='" + ReferenceId + "'");

                if (dr != null && dr.Length > 0)
                {

                }
                else //For Inserting Data into database
                {
                    PropId = System.Guid.NewGuid();
                    LocationId = Convert.ToString(System.Guid.NewGuid());
                    string StrContactId = Convert.ToString(System.Guid.NewGuid());
                    try
                    {
                        strType = dt.Rows[i]["rentalperiod"].ToString();
                        if (strType == "0" || strType == "" || strType == "1")
                        {
                            strType = "1";
                        }
                        else
                        {
                            strType = "2";
                        }

                        string strStatus = "";
                        if (dt.Rows[i]["sold"] != null && dt.Rows[i]["sold"].ToString() != "")
                        {
                            if (dt.Rows[i]["sold"].ToString().Trim() == "0")
                            {
                                strStatus = "0"; // 0 means Available 
                            }
                            else if (dt.Rows[i]["sold"].ToString().Trim() == "1")//For Reduced status
                            {
                                strStatus = "9";// 9 - Reduced 
                            }
                            else if (dt.Rows[i]["sold"].ToString().Trim() == "2")// For sold subject to contract
                            {
                                strStatus = "7";//7- Sold in Estate CRM database
                            }
                        }

                        if (dt.Rows[i]["UO_LA"] != null && dt.Rows[i]["UO_LA"].ToString() != "")
                        {
                            if (dt.Rows[i]["UO_LA"].ToString().Trim().ToLower() == "true")
                            {
                                strStatus = "3";// 3 - Under offer in Estate CRM database 
                            }
                        }

                        if (strStatus == "")
                            strStatus = "0";

                        sqlCmd.CommandText = InsertProp(dt.Rows[i]["propertyType"].ToString(), PropId.ToString(), LocationId,
                            dt.Rows[i]["featured"].ToString(), strType, ReferenceId, strStatus);

                        intResult = sqlCmd.ExecuteNonQuery();
                        insertcount++;
                    }
                    catch (Exception ex)
                    {
                        HandleException(ex, "Error in website - Colney estates XML importing: SaveToEstateCRMDatabase() Property insert ", sqlCmd.CommandText);
                    }

                    //For inserting property address 
                    if (intResult > 0)
                    {
                        try
                        {
                            sqlCmd.CommandText = InsertintoPropertyLocation(LocationId, dt.Rows[i]["sa1"].ToString() + " " + dt.Rows[i]["sa2"].ToString(),
                                dt.Rows[i]["county"].ToString(), dt.Rows[i]["city"].ToString(),
                                dt.Rows[i]["country"].ToString(), dt.Rows[i]["postcode"].ToString(),
                                dt.Rows[i]["latitude"].ToString(), dt.Rows[i]["longitude"].ToString());

                            intResult = sqlCmd.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            HandleException(ex, "Error in website - Colney estates XML importing: SaveToEstateCRMDatabase() Property Location insert ", sqlCmd.CommandText);
                        }
                    }

                    //for inserting data in to Property sale or let 
                    if (intResult > 0)
                    {
                        try
                        {
                            string strPrice = dt.Rows[i]["priceVal"].ToString();
                            strType = dt.Rows[i]["rentalperiod"].ToString();

                            if (strType == "2")// 2 means per day
                            {
                                strPrice = Convert.ToDecimal(7 * Convert.ToDecimal(strPrice)).ToString();//this is for converting to per week 
                                strPricePer = "0";
                            }
                            if (strType == "3")
                            {
                                strPricePer = "0";//Per week
                            }
                            else if (strType == "4")
                            {
                                strPricePer = "1";//Per month
                            }
                            else if (strType == "5")
                            {
                                strPricePer = "2";//Per quarter
                            }
                            else if (strType == "6")
                            {
                                strPricePer = "3";//Per year
                            }

                            if (strType == "1")//For buy
                            {
                                int intTenure = 0;

                                if (dt.Rows[i]["leaseType"] != null && dt.Rows[i]["leaseType"].ToString() != "")
                                {
                                    switch (dt.Rows[i]["leaseType"].ToString())
                                    {
                                        case "3": //3 - Freehold
                                        case "5": //5 = Freehold (to be confirmed)
                                        case "8": //8 = Share of Freehold
                                        case "9": //9 = Flying Freehold

                                            intTenure = 1;// 1- Freehold in estate CRM
                                            break;

                                        case "2"://2 = Leasehold
                                        case "4": //4 = Leasehold (to be confirmed)

                                            intTenure = 2;// 2 - Leasehold
                                            break;

                                        case "6":  //6 = To be Advised

                                            intTenure = 3;// 3 - Feudal in estate CRM 
                                            break;

                                        case "11": //11 = Leasehold (Share of Freehold)
                                        case "7":// 7 = Share of Leasehold

                                            intTenure = 5;//5 - Share of freehold in estate CRM
                                            break;

                                        default:
                                            intTenure = 0;
                                            break;
                                    }
                                }

                                sqlCmd.CommandText = InsertintoPropSale(strPrice, PropId.ToString(), intTenure);
                            }
                            else//For Rent
                            {
                                sqlCmd.CommandText = InsertintoPropLet(strPrice, PropId.ToString(), strPricePer);
                            }
                            intResult = sqlCmd.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            HandleException(ex, "Error in website - Colney estates XML importing: SaveToEstateCRMDatabase() Property Sale or Property Let insert ", sqlCmd.CommandText);
                        }
                    }

                    if (intResult > 0)
                    {
                        try
                        {
                            sqlCmd.CommandText = InsertintoPropDescriptive(PropId.ToString(), dt.Rows[i]["summaryDescription"].ToString());
                            intResult = sqlCmd.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            HandleException(ex, "Error in website - Colney estates XML importing: SaveToEstateCRMDatabase() Property Descriptive insert ", sqlCmd.CommandText);
                        }
                    }

                    //for bedrooms----------------------
                    if (intResult > 0)
                    {
                        try
                        {
                            int bedrooms = 0;
                            bedrooms = Convert.ToInt32(dt.Rows[i]["bedrooms"].ToString());

                            //For adding other rooms to bed rooms 
                            if (dt.Rows[i]["otherrooms"].ToString() != "" && dt.Rows[i]["otherrooms"].ToString() != "0")
                                bedrooms += Convert.ToInt32(dt.Rows[i]["otherrooms"].ToString());

                            if (bedrooms > 0)
                            {
                                sqlCmd.CommandText = InsertintoPropertyRoom(PropId, bedrooms, "1");
                                intResult = sqlCmd.ExecuteNonQuery();
                            }
                        }
                        catch (Exception ex)
                        {
                            HandleException(ex, "Error in website - Colney estates XML importing: SaveToEstateCRMDatabase() bedrooms insert ", sqlCmd.CommandText);
                        }
                    }

                    //for bathrooms-----------------------------------------------
                    if (intResult > 0)
                    {
                        try
                        {
                            int bathrooms = 0;
                            bathrooms = Convert.ToInt32(dt.Rows[i]["bathrooms"].ToString());
                            if (bathrooms > 0)
                            {
                                sqlCmd.CommandText = InsertintoPropertyRoom(PropId, bathrooms, "40");
                                intResult = sqlCmd.ExecuteNonQuery();
                            }
                        }
                        catch (Exception ex)
                        {
                            HandleException(ex, "Error in website - Colney estates XML importing: SaveToEstateCRMDatabase() Bathrooms insert ", sqlCmd.CommandText);
                        }
                    }

                    //for reception rooms-----------------------------------
                    if (intResult > 0)
                    {
                        try
                        {
                            int intReceptionRooms = 0;
                            intReceptionRooms = Convert.ToInt32(dt.Rows[i]["receptions"].ToString());
                            sqlCmd.CommandText = InsertintoPropertyRoom(PropId, intReceptionRooms, "6");
                            intResult = sqlCmd.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            HandleException(ex, "Error in website - Colney estates XML importing: SaveToEstateCRMDatabase() Reception rooms insert ", sqlCmd.CommandText);
                        }
                    }

                    //For Garages-----------------------------
                    if (dt.Rows[i]["garages"].ToString() != "" && dt.Rows[i]["garages"].ToString() != "0")
                    {
                        try
                        {
                            int intGarages = 0;
                            intGarages = Convert.ToInt32(dt.Rows[i]["garages"].ToString());
                            sqlCmd.CommandText = InsertintoPropertyRoom(PropId, intGarages, "23");
                            intResult = sqlCmd.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            HandleException(ex, "Error in website - Colney estates XML importing: SaveToEstateCRMDatabase() Garages insert ", sqlCmd.CommandText);
                        }
                    }

                    //For Gardens------------------------------
                    if (dt.Rows[i]["gardens"].ToString() != "" && dt.Rows[i]["gardens"].ToString() != "0")
                    {
                        try
                        {
                            int intGardens = 0;
                            intGardens = Convert.ToInt32(dt.Rows[i]["gardens"].ToString());
                            sqlCmd.CommandText = InsertintoPropertyRoom(PropId, intGardens, "2");
                            intResult = sqlCmd.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            HandleException(ex, "Error in website - Colney estates XML importing: SaveToEstateCRMDatabase() Gardens insert ", sqlCmd.CommandText);
                        }
                    }

                    //For parking Spaces  ------------------------------
                    if (dt.Rows[i]["parkingSpaces"].ToString() != "" && dt.Rows[i]["parkingSpaces"].ToString() != "0")
                    {
                        try
                        {
                            int intGardens = 0;
                            intGardens = Convert.ToInt32(dt.Rows[i]["parkingSpaces"].ToString());
                            sqlCmd.CommandText = InsertintoPropertyRoom(PropId, intGardens, "48");
                            intResult = sqlCmd.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            HandleException(ex, "Error in website - Colney estates XML importing: SaveToEstateCRMDatabase() parkingSpaces insert ", sqlCmd.CommandText);
                        }
                    }


                    //For fetching full details XML and importing data 

                    strDetailsXMLPath = strTempDetailsPath + dt.Rows[i]["id"].ToString() + ".xml";
                    strFullDetailsXMLURL = steTempPath + "&pid=" + dt.Rows[i]["id"].ToString();

                    bool blnsuccess = DownLoadXML(strFullDetailsXMLURL, strDetailsXMLPath);

                    if (blnsuccess)
                    {
                        UpdateXML(strDetailsXMLPath, "/response/propertyFullDetails/property/branch");
                        ds = new DataSet();
                        ds.ReadXml(strDetailsXMLPath);

                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            // For Property descriptive update 
                            try
                            {
                                strLongDesc = "";
                                for (int j = 0; j < ds.Tables["description"].Rows.Count; j++)
                                {
                                    if (ds.Tables["description"].Rows[j]["description_text"].ToString() != "")
                                    {
                                        if (j == 0)
                                            strLongDesc += ds.Tables["description"].Rows[j]["description_text"].ToString();
                                        else
                                            strLongDesc += "\r\n\r\n" + ds.Tables["description"].Rows[j]["description_text"].ToString();
                                    }
                                }

                                sqlCmd.CommandText = UpdatePropertyDescriptive(ds.Tables["text"].Rows[0]["particularsText"].ToString(), strLongDesc.Trim(), PropId);
                                sqlCmd.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                HandleException(ex, "Error in website - Colney estates XML importing: SaveToEstateCRMDatabase() Property descriptive update ", sqlCmd.CommandText);
                            }

                            //For images 

                            string StrDeleteCommand = "Delete from files where moduleid='" + PropId + "' and Clientid=" + ClientId + "";
                            sqlCmd.CommandText = StrDeleteCommand;
                            intResult = sqlCmd.ExecuteNonQuery();

                            string strImageCat = "";
                            string strPriority = "";
                            int priority = 1;
                            try
                            {
                                for (int j = 0; j < ds.Tables["picture"].Rows.Count; j++)
                                {
                                    if (ds.Tables["picture"].Rows[j]["categoryID"].ToString() == "1")//1- Primary - main image
                                    {
                                        strImageCat = "Photo link";//"Main photo";
                                        strPriority = "1";
                                    }
                                    else if (ds.Tables["picture"].Rows[j]["categoryID"].ToString() == "2")//2 - secondary - other images 
                                    {
                                        strImageCat = "Photo link";
                                        priority++;
                                        strPriority = priority.ToString();
                                    }
                                    else if (ds.Tables["picture"].Rows[j]["categoryID"].ToString() == "3")//3 - Floorplan
                                    {
                                        strImageCat = "Floor plan";
                                        strPriority = null;
                                    }
                                    else if (ds.Tables["picture"].Rows[j]["categoryID"].ToString() == "5")//5 - Aerial photo like EPC graph etc.
                                    {
                                        strImageCat = "EPC Graph";
                                        strPriority = null;
                                    }

                                    sqlCmd.CommandText = UpdatePropertyImages(ds.Tables["picture"].Rows[j]["picture_Text"].ToString(), strImageCat, PropId, strPriority);
                                    sqlCmd.ExecuteNonQuery();
                                }
                            }
                            catch (Exception ex)
                            {
                                HandleException(ex, "Error in website - Colney estates XML importing: SaveToEstateCRMDatabase() Images update ", sqlCmd.CommandText);
                            }

                            //For inserting Property features

                            try
                            {
                                string strInsertForFeatures = "";
                                System.Collections.ArrayList ArrFeatures = new System.Collections.ArrayList();

                                // DataTable dtFeature = ds.Tables["feature"].DefaultView.ToTable(true);

                                for (int k = 0; k < ds.Tables["feature"].Rows.Count; k++)
                                {
                                    if (!ArrFeatures.Contains(ds.Tables["feature"].Rows[k]["heading"].ToString().Trim()))
                                    {
                                        ArrFeatures.Add(ds.Tables["feature"].Rows[k]["heading"].ToString().Trim());
                                        strInsertForFeatures += InsertintoPropertyFeatures(ds.Tables["feature"].Rows[k]["heading"].ToString(), PropId, k + 1);
                                    }
                                }

                                sqlCmd.CommandText = strInsertForFeatures;
                                sqlCmd.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                HandleException(ex, "Error in website - Colney estates XML importing: SaveToEstateCRMDatabase() Property Features insert ", sqlCmd.CommandText);
                            }
                        }
                    }
                    //------------------------------------------------
                }
            }

            strResult = insertcount + " records inserted.";
            return strResult;
        }
        catch { }
        finally
        { sqlConn.Close(); }

        return strResult;
    }

    public string InsertProp(string strPropertyType, string strPropId, string strLocationId, string strFeatured, string strType, string strReference, string strStatus)
    {
        string StrInsertCommand = "";

        string propertysubtype = "";
        string PropertyMajorType = "";


        switch (strPropertyType)
        {
            case "10":
            case "58":
            case "59":
                PropertyMajorType = "2";//For apartment
                propertysubtype = "28";
                break;

            case "57":
            case "69":
                PropertyMajorType = "47";//Land
                propertysubtype = "20";
                break;

            case "60":
            case "70":
                PropertyMajorType = "3";//Commercial property
                propertysubtype = "19";
                break;

            case "56":
                PropertyMajorType = "1";//Cluster house
                propertysubtype = "6";
                break;

            case "4":
            case "54":
            case "61":
            case "28":
            case "32":
            case "55":
                PropertyMajorType = "1";//Semi-detached house
                propertysubtype = "3";
                break;

            case "40":
            case "41":
            case "42":
                PropertyMajorType = "48"; //Barn conversion
                propertysubtype = "43";
                break;

            case "15":
            case "16":
                PropertyMajorType = "45"; //Detached bungalow
                propertysubtype = "15";
                break;

            case "35":
            case "36":
            case "37":
            case "38":
            case "39":
                PropertyMajorType = "1";//Chalet
                propertysubtype = "24";
                break;

            case "19":
            case "20":
            case "21":
            case "22":
            case "23":
            case "24":
                PropertyMajorType = "1";//Cottage
                propertysubtype = "23";
                break;

            case "6":
            case "29":
            case "30":
            case "5":
            case "53":
                PropertyMajorType = "1";//Detached house
                propertysubtype = "4";
                break;

            case "52":
                PropertyMajorType = "2";//Duplex
                propertysubtype = "56";
                break;

            case "31":
            case "33":
            case "34 ":
                PropertyMajorType = "1";//Country house
                propertysubtype = "7";
                break;

            case "17":
            case "18":
                PropertyMajorType = "1";//Bungalow
                propertysubtype = "8";
                break;

            case "7":
            case "8":
                PropertyMajorType = "1";//House
                propertysubtype = "26";
                break;

            case "11":
            case "12":
            case "13":
                PropertyMajorType = "45";//Terraced bungalow
                propertysubtype = "13";
                break;

            case "1":
            case "2":
            case "3":
            case "25":
            case "26":
            case "27":
                PropertyMajorType = "1";//Terraced house
                propertysubtype = "1";
                break;

            case "9":
            case "47":
            case "44":
            case "46":
            case "43":
            case "48":
            case "45":
                PropertyMajorType = "2"; //Flat
                propertysubtype = "8";
                break;

            case "49":
            case "50":
            case "68":
                PropertyMajorType = "2"; //Maisonette
                propertysubtype = "11";
                break;

            case "66":
                PropertyMajorType = "1"; //Link detached house
                propertysubtype = "21";
                break;

            case "51":
                PropertyMajorType = "2"; //Penthouse
                propertysubtype = "29";
                break;

            case "14":
                PropertyMajorType = "45"; //Semi-detached bungalow
                propertysubtype = "14";
                break;

            case "67":
                PropertyMajorType = "2";//Studio
                propertysubtype = "9";
                break;

            case "65":
                PropertyMajorType = "1";//Village house
                propertysubtype = "95";
                break;

            case "62":
            case "63":
                PropertyMajorType = "1";//Detached villa
                propertysubtype = "131";
                break;

            case "64":
                PropertyMajorType = "1";//Semi-detached villa
                propertysubtype = "128";
                break;

            case "71":
                PropertyMajorType = "2";//Retirement flat
                propertysubtype = "145";
                break;

            case "72":
                PropertyMajorType = "2";//Bedsit
                propertysubtype = "3";
                break;

            case "73":
                PropertyMajorType = "6";//Mobile / Park homes
                propertysubtype = "49";
                break;

            default:
                PropertyMajorType = "21";// Any
                propertysubtype = "5";
                break;
        }


        StrInsertCommand = "declare @username varchar(1000) \r\n ";
        StrInsertCommand += "if exists(select top 1 username from userdetails where usertype='admin' and clientid=" + ClientId + ") \r\n ";
        StrInsertCommand += " begin \r\n set @username = (select top 1 username from userdetails where usertype='admin' and clientid=" + ClientId + ") \r\n end \r\n ";
        StrInsertCommand += "else \r\n begin \r\n set @username = (select top 1 username from userdetails where clientid=" + ClientId + ") \r\n end \r\n ";
        StrInsertCommand += "INSERT INTO [Property]";
        StrInsertCommand += "([PropertyId], [ClientId], [UserName],[LocationId],[PropertyMajorType], [PropertySubType],[PropertyContact1Id],[FeaturedProperty],[Reference],[DealType],[Status],[TimeStampId]) VALUES ";
        StrInsertCommand += "('" + strPropId + "',";
        StrInsertCommand += "'" + ClientId + "',";
        StrInsertCommand += "@username ,";
        StrInsertCommand += "'" + strLocationId + "',";
        StrInsertCommand += PropertyMajorType + ",";
        StrInsertCommand += propertysubtype + ",";

        StrInsertCommand += "8,";
        StrInsertCommand += "'" + strFeatured + "',";

        StrInsertCommand += "'" + strReference + "',";
        StrInsertCommand += strType + ",";

        StrInsertCommand += strStatus + ",";

        StrInsertCommand += "GetDate())";
        return StrInsertCommand;
    }

    // to prepare the query for sale property insert in PropertySale table ---------------------------
    private string InsertintoPropSale(string strPrice, object PropId, int intTenure)
    {
        string StrInsertCommand;
        StrInsertCommand = "INSERT INTO [PropertySale]";
        StrInsertCommand += "([PropertyId],[ClientId],[AskingPrice],[PriceQualifier],[Tenure]";
        StrInsertCommand += ",[LeaseTerm],[IncludeOnWebsite] ,[IncludeOnRightMove],[IncludeOnPrimeLocation]";
        StrInsertCommand += ",[IncludeOnPropertyFinder])VALUES (";
        StrInsertCommand += "'" + PropId;
        StrInsertCommand += "'," + ClientId;
        StrInsertCommand += "," + strPrice;//row["price"];
        StrInsertCommand += ",0";
        StrInsertCommand += ",0";
        StrInsertCommand += ",0,'True','False','False','False')";
        return StrInsertCommand;
    }

    // to prepare the query for let property insert in PropertyLet table-----------------------------
    private string InsertintoPropLet(string strPrice, object PropId, string strRentPer)
    {
        //string RentPer = "1";

        string StrInsertCommand;
        StrInsertCommand = "INSERT INTO [PropertyLet]";
        StrInsertCommand += "([PropertyId],[ClientId],[Price],[PricePer],[RentRenewalDate],[IsBondRequired],[BondAmount]";
        StrInsertCommand += ",[IncludeOnWebsite] ,[IncludeOnRightMove],[IncludeOnPrimeLocation],[IncludeOnPropertyFinder]";
        StrInsertCommand += ",[PriceQualifier],[LetType]) VALUES ( ";
        StrInsertCommand += "'" + PropId + "', ";
        StrInsertCommand += ClientId + ", ";
        StrInsertCommand += strPrice;
        StrInsertCommand += "," + strRentPer;
        StrInsertCommand += ", null ";
        StrInsertCommand += ",0";
        StrInsertCommand += ",null";
        StrInsertCommand += ",'True','False','False','False'";
        StrInsertCommand += ",'0'";
        StrInsertCommand += ",0)";
        return StrInsertCommand;
    }

    //property rooms(bed, bath, recp,garage,garden) insert funtion(table: -PropertyRoomMain,PropertyRoom )---------------------------
    private string InsertintoPropertyRoom(object PropId, int rooms, string roomtype)
    {
        string StrInsertCommand;
        StrInsertCommand = "declare @roommainid uniqueidentifier \r\n ";
        StrInsertCommand += "set @roommainid=newid() \r\n ";
        StrInsertCommand += "Insert into PropertyRoomMain(ProomMainKeyId, ClientId, PropertyId, RoomId) Values(";
        StrInsertCommand += "@roommainid";
        StrInsertCommand += "," + ClientId;
        StrInsertCommand += ",'" + PropId + "'";
        StrInsertCommand += "," + roomtype + ") \r\n ";
        for (int i = 1; i <= rooms; i++)
        {
            StrInsertCommand += "Insert into PropertyRoom(ProomKeyId, ClientId, ProomMainKeyId, RoomNumber) Values("; //,RoomSize1
            StrInsertCommand += "newid()";
            StrInsertCommand += "," + ClientId;
            StrInsertCommand += ",@roommainid";
            StrInsertCommand += "," + i + ") \r\n ";
        }
        return StrInsertCommand;
    }

    // to prepare the query for insert in PropertyDescriptive table----------------------------------
    private string InsertintoPropDescriptive(object PropId, string strdesc)
    {
        string StrInsertCommand = "INSERT INTO [PropertyDescriptive]";
        StrInsertCommand += "([PropertyId], [ClientId],[Shortdesc]) VALUES (";
        StrInsertCommand += "'" + PropId + "'";
        StrInsertCommand += "," + ClientId;
        StrInsertCommand += ",'" + strdesc.Replace("'", "''") + "')";
        return StrInsertCommand;
    }

    private string InsertintoPropertyLocation(object objLocationId, string strAddress, string strRegion, string strCity,
                                    string strCountry, string strPostCode, string strLatitude, string strLongitude)
    {
        string strQuery = "";

        if (strCountry == null || strCountry == "")
        {
            strCountry = "England";
        }

        strQuery = "INSERT INTO [PropertyLocation]";
        strQuery += "([LocationId],[ClientId],[Address],[AreaOrRegionName],[CityOrTownName],[CountryName], [PostCode], [Latitude], [Longitude]) VALUES (";
        strQuery += "'" + objLocationId + "'";
        strQuery += ", " + ClientId;
        strQuery += ",'" + strAddress.ToString().Replace("'", "''") + "'"; //address
        strQuery += ",'" + strRegion.Replace("'", "''") + "'"; //areaorregioname
        strQuery += ",'" + strCity.ToString().Replace("'", "''") + "'"; //CityOrTownName
        strQuery += ",'" + strCountry + "'";  //CountryName
        strQuery += ",'" + strPostCode.ToString().Replace("'", "''") + "'";  //postcode        
        strQuery += ",'" + strLatitude + "'"; //lat
        strQuery += ",'" + strLongitude + "')";  //lang

        return strQuery;
    }

    // to prepare the query for property features insert in PropertyFeature table--------------------
    private string InsertintoPropertyFeatures(string strFeature, object PropId, int intRank)
    {
        string StrInsertCommand = "";
        if (strFeature != "")
        {
            StrInsertCommand += "Insert into PropertyFeature(PropertyId,ClientId,FeatureDesc,PropertyFeatureRank) values(";
            StrInsertCommand += "'" + PropId + "'";
            StrInsertCommand += "," + ClientId;
            StrInsertCommand += ",'" + strFeature.Replace("'", "''") + "'";
            StrInsertCommand += "," + intRank + ") \r\n ";
        }
        return StrInsertCommand;
    }
    //to insert the room details of Vebra properties from uploaded rooms text files-------------
    private string UpdatePropertyDescriptive(string strShortDesc, string strLongDesc, object PropId)
    {
        string strUpdateCommand = "";

        strUpdateCommand += "UPDATE [PropertyDescriptive] SET ";
        strUpdateCommand += " [ShortDesc] = '" + strShortDesc.Replace("'", "''") + "',";
        strUpdateCommand += " [LongDesc] = '" + strLongDesc.Replace("'", "''") + "'";
        strUpdateCommand += " WHERE PropertyID = '" + PropId + "' ";
        strUpdateCommand += " and ClientId = " + ClientId;

        return strUpdateCommand;
    }

    private string UpdatePropertyImages(string strImageLink, string strCategory, object PropId, string priority)
    {
        string StrInsertCommand = "";
        StrInsertCommand += "INSERT INTO Files";
        StrInsertCommand += "([PKey],ClientId,FileName,Type,Category,ModuleId,Priority)";
        StrInsertCommand += "VALUES(";
        StrInsertCommand += "newid()";
        StrInsertCommand += "," + ClientId + ",";

        string StrTempString = strImageLink;
        if (StrTempString == "")
        {
            StrInsertCommand += "'" + "',";
        }
        else
        {
            StrInsertCommand += "'" + (StrTempString) + "',";
        }

        StrInsertCommand += "'All Files',";

        //if (cnt == 0)
        //{
        //    StrInsertCommand += "'Main photo',";
        //}
        //else
        //{
        //    StrInsertCommand += "'Photo link',"; //StrInsertCommand += "'Photo',";
        //}

        StrInsertCommand += "'" + strCategory + "',";

        StrInsertCommand += "'" + PropId + "','" + priority + "')";
        //StrInsertCommand += "'" + PropId + "')";
        return StrInsertCommand;
    }

    //For selecting Ref.No
    public DataTable GetRefNos()
    {
        DataTable ds = new DataTable();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "select distinct (Reference) ReferenceNo from property where clientid=" + ClientId;
            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    public void DeleteAllProperties()
    {
        SqlConnection sqlConn = new SqlConnection(strConn);
        SqlCommand sqlCmd = new SqlCommand();
        try
        {
            sqlConn.Open();
            string StrDeleteCommand = "";

            StrDeleteCommand += "Delete from Property where clientid=" + ClientId + "\r\n";
            StrDeleteCommand += "Delete from PropertyLocation where clientid=" + ClientId + "\r\n";

            StrDeleteCommand += "Delete from PropertySale where clientid=" + ClientId + "\r\n";
            StrDeleteCommand += "Delete from PropertyLet where clientid=" + ClientId + "\r\n";

            StrDeleteCommand += "Delete from PropertyDescriptive where clientid=" + ClientId + "\r\n";
            StrDeleteCommand += "Delete from propertyFeature where clientid=" + ClientId + "\r\n";
            StrDeleteCommand += "Delete from files where clientid=" + ClientId + "\r\n";

            StrDeleteCommand += "Delete from PropertyRoom where clientid=" + ClientId + "\r\n";

            StrDeleteCommand += "Delete from PropertyRoomMain where clientid=" + ClientId + "\r\n";

            sqlCmd.CommandText = StrDeleteCommand;
            sqlCmd.Connection = sqlConn;
            sqlCmd.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            HandleException(ex, "Error in website - Colney estates XML importing: DeleteAllProperties() Delete property  ", sqlCmd.CommandText);
        }
        finally { sqlConn.Close(); }
    }

    #endregion

    public DataSet GetNextPropDataset(string propid, string orderby, string strminprice, string strmaxprice, string strbedrooms, string strlocation, string strType)
    {
        SqlConnection Conn = new SqlConnection(strConn);
        string sqlQuery = "";
        string strCondition = "";
        int arrCnt = 0;
        System.Collections.ArrayList arrpropid = new System.Collections.ArrayList();


        if (orderby.Trim() != "" && orderby != "Any")
        {
            if (strType == "1")
            {
                strCondition = @"Where Property.ClientId = " + ClientId + "  AND (Property.DealType = 1 Or Property.DealType = 4) AND ";

                //////if (strlocation.Trim() != "" && strlocation != "Any")
                //////{

                //////    strCondition += "PropertyLocation.LocationAKA='" + strlocation.Replace("'", "|@") + "' AND";

                //////}

                if (strlocation.Trim() != "" && strlocation.ToLower().Trim() != "all" && strlocation.ToLower() != "select" && strlocation != "Any")
                {
                    string[] strAllArea = strlocation.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    if (strAllArea != null && strAllArea.Length > 0)
                    {
                        strCondition += "(";
                        foreach (string strItem in strAllArea)
                        {
                            strCondition += "PropertyLocation.LocationAKA='" + strItem.Replace("'", "|@").Trim() + "' OR ";
                        }
                        strCondition = strCondition.TrimEnd(new char[] { 'O', 'R', ' ' });
                        strCondition += ") AND ";
                    }
                    else
                    {
                        strCondition += "(PropertyLocation.LocationAKA='" + strlocation.Replace("'", "|@").Trim() + "') AND ";
                    }
                }

                if ((strminprice.ToString().Replace(",", "") != "" && strminprice.ToString().Replace(",", "") != "0") && (strmaxprice.ToString().Replace(",", "") == "" || strmaxprice.ToString().Replace(",", "") == "0"))
                {
                    strCondition += "PropertySale.AskingPrice >= " + strminprice.ToString().Replace(",", "") + "";
                }
                else if ((strminprice.ToString().Replace(",", "") == "" || strminprice.ToString().Replace(",", "") == "0") && (strmaxprice.ToString().Replace(",", "") != "" && strmaxprice.ToString().Replace(",", "") != "0"))
                {
                    strCondition += "PropertySale.AskingPrice <= " + strmaxprice.ToString().Replace(",", "") + "";
                }
                else if (strminprice.ToString().Replace(",", "") != "" && strminprice.ToString().Replace(",", "") != "0" && strmaxprice.ToString().Replace(",", "") != "" && strmaxprice.ToString().Replace(",", "") != "0")
                {
                    strCondition += "(PropertySale.AskingPrice Between " + strminprice.ToString().Replace(",", "") + " AND " + strmaxprice.ToString().Replace(",", "") + "";
                }

                strCondition = strCondition.TrimEnd(new char[] { 'A', 'N', 'D', ' ' });
                ////**************

                sqlQuery = @"SELECT Property.PropertyId,Property.FurtherNotes,  (Select top 1 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId
                          and Files.Category like 'Main photo') As image , PropertyLocation.Latitude,PropertyLocation.Longitude,PropertyLocation.AreaOrRegionName,PropertyLocation.CityOrTownName,PropertyLocation.CountryName,PropertyLocation.Address as Address,
                          PropertySale.AskingPrice As Cost, PropertySale.AskingPrice As CostToOrder, PropertySale.PriceSuffix FROM Property Inner Join PropertySale ON (Property.PropertyId = PropertySale.PropertyId and Property.ClientId = PropertySale.ClientId) Left JOIN PropertyLocation ON (Property.LocationId = PropertyLocation.LocationId and Property.ClientId = PropertyLocation.ClientId)
                          Left JOIN UserDetails ON (Property.UserName = UserDetails.UserName and Property.ClientId = UserDetails.ClientId) 
                          " + strCondition + "order by " + orderby;
            }
            else
            {
                strCondition = @"Where Property.ClientId = " + ClientId + "  AND (Property.DealType = 2 Or Property.DealType = 4) AND ";

                //////if (strlocation.Trim() != "" && strlocation != "Any")
                //////{

                //////    strCondition += "PropertyLocation.LocationAKA='" + strlocation.Replace("'", "|@") + "' AND";

                //////}

                if (strlocation.Trim() != "" && strlocation.ToLower().Trim() != "all" && strlocation.ToLower() != "select" && strlocation != "Any")
                {
                    string[] strAllArea = strlocation.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    if (strAllArea != null && strAllArea.Length > 0)
                    {
                        strCondition += "(";
                        foreach (string strItem in strAllArea)
                        {
                            strCondition += "PropertyLocation.LocationAKA='" + strItem.Replace("'", "|@").Trim() + "' OR ";
                        }
                        strCondition = strCondition.TrimEnd(new char[] { 'O', 'R', ' ' });
                        strCondition += ") AND ";
                    }
                    else
                    {
                        strCondition += "(PropertyLocation.LocationAKA='" + strlocation.Replace("'", "|@").Trim() + "') AND ";
                    }
                }

                if ((strminprice.ToString().Replace(",", "") != "" && strminprice.ToString().Replace(",", "") != "0") && (strmaxprice.ToString().Replace(",", "") == "" || strmaxprice.ToString().Replace(",", "") == "0"))
                {
                    strCondition += " PropertyLet.Price >= " + strminprice.ToString().Replace(",", "") + "";
                }
                else if ((strminprice.ToString().Replace(",", "") == "" || strminprice.ToString().Replace(",", "") == "0") && (strmaxprice.ToString().Replace(",", "") != "" && strmaxprice.ToString().Replace(",", "") != "0"))
                {
                    strCondition += "PropertyLet.Price <= " + strmaxprice.ToString().Replace(",", "") + "";
                }
                else if (strminprice.ToString().Replace(",", "") != "" && strminprice.ToString().Replace(",", "") != "0" && strmaxprice.ToString().Replace(",", "") != "" && strmaxprice.ToString().Replace(",", "") != "0")
                {
                    strCondition += "(PropertyLet.Price Between " + strminprice.ToString().Replace(",", "") + " AND " + strmaxprice.ToString().Replace(",", "") + ")";
                }

                strCondition = strCondition.TrimEnd(new char[] { 'A', 'N', 'D', ' ' });
                ////**************

                sqlQuery = @"SELECT Property.PropertyId,Property.FurtherNotes,Property.PropertyId,  (Select top 1 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId
                    and Files.Category like 'Main photo') As image,PropertyLocation.AreaOrRegionName,PropertyLocation.CityOrTownName,PropertyLocation.CountryName,PropertyLocation.Address as Address,
            PropertyLocation.Latitude,PropertyLocation.Longitude,PropertyLet.Price as Cost,PropertyLet.Price as CostToOrder,PropertyLet.BondAmount, PropertyLet.PricePer,PropertyLet.MinimumTerm
            FROM Property Inner Join PropertyLet ON (Property.PropertyId = PropertyLet.PropertyId and Property.ClientId = PropertyLet.ClientId) Left JOIN PropertyLocation ON (Property.LocationId = PropertyLocation.LocationId and Property.ClientId = PropertyLocation.ClientId)
            Left JOIN UserDetails ON (Property.UserName = UserDetails.UserName and Property.ClientId = UserDetails.ClientId) 
           " + strCondition + "order by " + orderby;

            }

        }
        else
        {
            if (strType == "1")
            {
                strCondition = @"Where Property.ClientId = " + ClientId + "  AND (Property.DealType = 1 Or Property.DealType = 4) AND ";
                //////if (strlocation.Trim() != "" && strlocation != "Any")
                //////{

                //////    strCondition += "PropertyLocation.LocationAKA='" + strlocation.Replace("'", "|@") + "' AND";

                //////}

                if (strlocation.Trim() != "" && strlocation.ToLower().Trim() != "all" && strlocation.ToLower() != "select" && strlocation != "Any")
                {
                    string[] strAllArea = strlocation.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    if (strAllArea != null && strAllArea.Length > 0)
                    {
                        strCondition += "(";
                        foreach (string strItem in strAllArea)
                        {
                            strCondition += "PropertyLocation.LocationAKA='" + strItem.Replace("'", "|@").Trim() + "' OR ";
                        }
                        strCondition = strCondition.TrimEnd(new char[] { 'O', 'R', ' ' });
                        strCondition += ") AND ";
                    }
                    else
                    {
                        strCondition += "(PropertyLocation.LocationAKA='" + strlocation.Replace("'", "|@").Trim() + "') AND ";
                    }
                }

                if ((strminprice.ToString().Replace(",", "") != "" && strminprice.ToString().Replace(",", "") != "0") && (strmaxprice.ToString().Replace(",", "") == "" || strmaxprice.ToString().Replace(",", "") == "0"))
                {
                    strCondition += "PropertySale.AskingPrice >= " + strminprice.ToString().Replace(",", "") + " AND PropertySale.IncludeOnWebsite = 'True' AND ";
                }
                else if ((strminprice.ToString().Replace(",", "") == "" || strminprice.ToString().Replace(",", "") == "0") && (strmaxprice.ToString().Replace(",", "") != "" && strmaxprice.ToString().Replace(",", "") != "0"))
                {
                    strCondition += "PropertySale.AskingPrice <= " + strmaxprice.ToString().Replace(",", "") + " AND PropertySale.IncludeOnWebsite = 'True' AND ";
                }
                else if (strminprice.ToString().Replace(",", "") != "" && strminprice.ToString().Replace(",", "") != "0" && strmaxprice.ToString().Replace(",", "") != "" && strmaxprice.ToString().Replace(",", "") != "0")
                {
                    strCondition += "(PropertySale.AskingPrice Between " + strminprice.ToString().Replace(",", "") + " AND " + strmaxprice.ToString().Replace(",", "") + " AND PropertySale.IncludeOnWebsite = 'True') AND ";
                }



                strCondition = strCondition.TrimEnd(new char[] { 'A', 'N', 'D', ' ' });

                sqlQuery = @"SELECT Property.PropertyId,Property.FurtherNotes, (Select top 1 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId
                    and Files.Category like 'Main photo') As image,PropertyLocation.AreaOrRegionName,PropertyLocation.CityOrTownName,PropertyLocation.CountryName,PropertyLocation.Address as Address,
                    PropertyLocation.Latitude,PropertyLocation.Longitude,PropertySale.AskingPrice As Cost, PropertySale.AskingPrice As CostToOrder, PropertySale.PriceSuffix FROM Property Inner Join PropertySale ON (Property.PropertyId = PropertySale.PropertyId and Property.ClientId = PropertySale.ClientId)  Left JOIN PropertyLocation ON (Property.LocationId = PropertyLocation.LocationId and Property.ClientId = PropertyLocation.ClientId) 
                    Left JOIN UserDetails ON (Property.UserName = UserDetails.UserName and Property.ClientId = UserDetails.ClientId) 
                   " + strCondition;
            }
            else
            {
                strCondition = @"Where Property.ClientId = " + ClientId + "  AND (Property.DealType = 2 Or Property.DealType = 4) AND ";
                ////if (strlocation.Trim() != "" && strlocation != "Any")
                ////{

                ////    strCondition += "PropertyLocation.LocationAKA='" + strlocation.Replace("'", "|@") + "' AND";

                ////}
                if (strlocation.Trim() != "" && strlocation.ToLower().Trim() != "all" && strlocation.ToLower() != "select" && strlocation != "Any")
                {
                    string[] strAllArea = strlocation.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    if (strAllArea != null && strAllArea.Length > 0)
                    {
                        strCondition += "(";
                        foreach (string strItem in strAllArea)
                        {
                            strCondition += "PropertyLocation.LocationAKA='" + strItem.Replace("'", "|@").Trim() + "' OR ";
                        }
                        strCondition = strCondition.TrimEnd(new char[] { 'O', 'R', ' ' });
                        strCondition += ") AND ";
                    }
                    else
                    {
                        strCondition += "(PropertyLocation.LocationAKA='" + strlocation.Replace("'", "|@").Trim() + "') AND ";
                    }
                }

                //////if (strbranch.Trim() != "" && strbranch != "Any")
                //////{
                //////    strCondition += "UserDetails.BranchName='" + strbranch + "' AND ";
                //////}

                if ((strminprice.ToString().Replace(",", "") != "" && strminprice.ToString().Replace(",", "") != "0") && (strmaxprice.ToString().Replace(",", "") == "" || strmaxprice.ToString().Replace(",", "") == "0"))
                {
                    strCondition += " PropertyLet.Price >= " + strminprice.ToString().Replace(",", "") + "";
                }
                else if ((strminprice.ToString().Replace(",", "") == "" || strminprice.ToString().Replace(",", "") == "0") && (strmaxprice.ToString().Replace(",", "") != "" && strmaxprice.ToString().Replace(",", "") != "0"))
                {
                    strCondition += "PropertyLet.Price <= " + strmaxprice.ToString().Replace(",", "") + "";
                }
                else if (strminprice.ToString().Replace(",", "") != "" && strminprice.ToString().Replace(",", "") != "0" && strmaxprice.ToString().Replace(",", "") != "" && strmaxprice.ToString().Replace(",", "") != "0")
                {
                    strCondition += "(PropertyLet.Price Between " + strminprice.ToString().Replace(",", "") + " AND " + strmaxprice.ToString().Replace(",", "") + ")";
                }


                strCondition = strCondition.TrimEnd(new char[] { 'A', 'N', 'D', ' ' });

                sqlQuery = @"SELECT Property.PropertyId,Property.FurtherNotes,  (Select top 1 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId
                           and Files.Category like 'Main photo') As image,PropertyLocation.AreaOrRegionName,PropertyLocation.CityOrTownName,PropertyLocation.CountryName,PropertyLocation.Address as Address,
                           PropertyLocation.Latitude,PropertyLocation.Longitude,PropertyLet.Price as Cost,PropertyLet.Price as CostToOrder,PropertyLet.BondAmount, PropertyLet.PricePer,PropertyLet.MinimumTerm FROM Property Inner Join PropertyLet ON (Property.PropertyId = PropertyLet.PropertyId and Property.ClientId = PropertyLet.ClientId)  Left JOIN PropertyLocation ON (Property.LocationId = PropertyLocation.LocationId and Property.ClientId = PropertyLocation.ClientId) 
                           Left JOIN UserDetails ON (Property.UserName = UserDetails.UserName and Property.ClientId = UserDetails.ClientId) 
                           " + strCondition;
            }

        }
        SqlDataAdapter PropertyDA = new SqlDataAdapter(sqlQuery, Conn);
        DataSet ds = new DataSet();
        try
        {
            Conn.Open();
            PropertyDA.Fill(ds);
            if (Conn.State == ConnectionState.Open) { Conn.Close(); }
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    arrpropid.Add(ds.Tables[0].Rows[i]["PropertyId"].ToString());


                }
                for (int k = 0; k < arrpropid.Count; k++)
                {
                    if (propid == arrpropid[k].ToString())
                    {
                        arrCnt = k;
                        break;
                    }
                }
                string propertyid = "";
                if (arrCnt + 1 == ds.Tables[0].Rows.Count)
                {
                    propertyid = "";
                    ds.Tables.Clear();
                }
                else
                {
                    propertyid = ds.Tables[0].Rows[arrCnt + 1]["PropertyId"].ToString();

                    //for (int j = 0; j < ds.Tables[0].Rows.Count; j++)
                    //{

                    //    if (propertyid != ds.Tables[0].Rows[j]["PropertyId"].ToString())
                    //    {
                    //        ds.Tables[0].Rows.RemoveAt(j);
                    //        j--;
                    //        continue;
                    //    }
                    //}
                }
            }
        }
        catch { }
        finally
        {
            if (Conn.State == ConnectionState.Open) { Conn.Close(); }
        }
        return ds;
    }

    public  DataSet GetPrevPropDataset(string propid, string orderby, string strminprice, string strmaxprice, string strbedrooms, string strlocation, string strType)
    {
        SqlConnection Conn = new SqlConnection(strConn);
        string sqlQuery = "";
        string strCondition = "";
        int arrCnt = 0;
        System.Collections.ArrayList arrpropid = new System.Collections.ArrayList();
        if (orderby.Trim() != "" && orderby != "Any")
        {
            if (strType == "2")
            {
                strCondition = @"Where Property.ClientId =" + ClientId + "  AND (Property.DealType = 2 Or Property.DealType = 4) AND ";
                ////if (strlocation.Trim() != "" && strlocation != "Any")
                ////{

                ////    strCondition += "PropertyLocation.LocationAKA='" + strlocation.Replace("'", "|@") + "' AND";

                ////}
                if (strlocation.Trim() != "" && strlocation.ToLower().Trim() != "all" && strlocation.ToLower() != "select" && strlocation != "Any")
                {
                    string[] strAllArea = strlocation.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    if (strAllArea != null && strAllArea.Length > 0)
                    {
                        strCondition += "(";
                        foreach (string strItem in strAllArea)
                        {
                            strCondition += "PropertyLocation.LocationAKA='" + strItem.Replace("'", "|@").Trim() + "' OR ";
                        }
                        strCondition = strCondition.TrimEnd(new char[] { 'O', 'R', ' ' });
                        strCondition += ") AND ";
                    }
                    else
                    {
                        strCondition += "(PropertyLocation.LocationAKA='" + strlocation.Replace("'", "|@").Trim() + "') AND ";
                    }
                }
                
                if ((strminprice.ToString().Replace(",", "") != "" && strminprice.ToString().Replace(",", "") != "0") && (strmaxprice.ToString().Replace(",", "") == "" || strmaxprice.ToString().Replace(",", "") == "0"))
                {
                    strCondition += " PropertyLet.Price >= " + strminprice.ToString().Replace(",", "") + "";
                }
                else if ((strminprice.ToString().Replace(",", "") == "" || strminprice.ToString().Replace(",", "") == "0") && (strmaxprice.ToString().Replace(",", "") != "" && strmaxprice.ToString().Replace(",", "") != "0"))
                {
                    strCondition += "PropertyLet.Price <= " + strmaxprice.ToString().Replace(",", "") + "";
                }
                else if (strminprice.ToString().Replace(",", "") != "" && strminprice.ToString().Replace(",", "") != "0" && strmaxprice.ToString().Replace(",", "") != "" && strmaxprice.ToString().Replace(",", "") != "0")
                {
                    strCondition += "(PropertyLet.Price Between " + strminprice.ToString().Replace(",", "") + " AND " + strmaxprice.ToString().Replace(",", "") + ")";
                }

                strCondition = strCondition.TrimEnd(new char[] { 'A', 'N', 'D', ' ' });

                sqlQuery = @"SELECT Property.PropertyId,Property.FurtherNotes,  (Select top 1 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId
                    and Files.Category like 'Main photo') As image,PropertyLocation.AreaOrRegionName,PropertyLocation.CityOrTownName,PropertyLocation.CountryName,PropertyLocation.Address as Address,
                    PropertyLocation.PostCode,PropertyLocation.Latitude,PropertyLocation.Longitude, PropertyLet.Price as Cost,PropertyLet.Price as CostToOrder,PropertyLet.BondAmount, PropertyLet.PricePer,PropertyLet.MinimumTerm FROM Property Inner Join PropertyLet ON (Property.PropertyId = PropertyLet.PropertyId and Property.ClientId = PropertyLet.ClientId)
                    Left JOIN PropertyLocation ON (Property.LocationId = PropertyLocation.LocationId and Property.ClientId = PropertyLocation.ClientId)
                    Left JOIN UserDetails ON (Property.UserName = UserDetails.UserName and Property.ClientId = UserDetails.ClientId) 
                    " + strCondition + "order by " + orderby;
            }
            else
            {
                strCondition = @"Where Property.ClientId =" + ClientId + "  AND (Property.DealType =1 Or Property.DealType = 4) AND ";

                ////if (strlocation.Trim() != "" && strlocation != "Any")
                ////{

                ////    strCondition += "PropertyLocation.LocationAKA='" + strlocation.Replace("'", "|@") + "' AND";

                ////}

                if (strlocation.Trim() != "" && strlocation.ToLower().Trim() != "all" && strlocation.ToLower() != "select" && strlocation != "Any")
                {
                    string[] strAllArea = strlocation.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    if (strAllArea != null && strAllArea.Length > 0)
                    {
                        strCondition += "(";
                        foreach (string strItem in strAllArea)
                        {
                            strCondition += "PropertyLocation.LocationAKA='" + strItem.Replace("'", "|@").Trim() + "' OR ";
                        }
                        strCondition = strCondition.TrimEnd(new char[] { 'O', 'R', ' ' });
                        strCondition += ") AND ";
                    }
                    else
                    {
                        strCondition += "(PropertyLocation.LocationAKA='" + strlocation.Replace("'", "|@").Trim() + "') AND ";
                    }
                }

                if ((strminprice.ToString().Replace(",", "") != "" && strminprice.ToString().Replace(",", "") != "0") && (strmaxprice.ToString().Replace(",", "") == "" || strmaxprice.ToString().Replace(",", "") == "0"))
                {
                    strCondition += "PropertySale.AskingPrice >= " + strminprice.ToString().Replace(",", "") + "";
                }
                else if ((strminprice.ToString().Replace(",", "") == "" || strminprice.ToString().Replace(",", "") == "0") && (strmaxprice.ToString().Replace(",", "") != "" && strmaxprice.ToString().Replace(",", "") != "0"))
                {
                    strCondition += "PropertySale.AskingPrice <= " + strmaxprice.ToString().Replace(",", "") + "";
                }
                else if (strminprice.ToString().Replace(",", "") != "" && strminprice.ToString().Replace(",", "") != "0" && strmaxprice.ToString().Replace(",", "") != "" && strmaxprice.ToString().Replace(",", "") != "0")
                {
                    strCondition += "(PropertySale.AskingPrice Between " + strminprice.ToString().Replace(",", "") + " AND " + strmaxprice.ToString().Replace(",", "") + "";
                }

                strCondition = strCondition.TrimEnd(new char[] { 'A', 'N', 'D', ' ' });
                sqlQuery = @"SELECT Property.PropertyId,Property.FurtherNotes,  (Select top 1 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId
                    and Files.Category like 'Main photo') As image,PropertyLocation.AreaOrRegionName,PropertyLocation.CityOrTownName,PropertyLocation.CountryName,PropertyLocation.Address as Address,
                    PropertyLocation.PostCode,PropertyLocation.Latitude,PropertyLocation.Longitude, PropertySale.AskingPrice As Cost, PropertySale.AskingPrice As CostToOrder, PropertySale.PriceSuffix FROM Property Inner Join PropertySale ON
                    (Property.PropertyId = PropertySale.PropertyId and Property.ClientId = PropertySale.ClientId)
                    Left JOIN PropertyLocation ON (Property.LocationId = PropertyLocation.LocationId and Property.ClientId = PropertyLocation.ClientId)
                   Left JOIN UserDetails ON (Property.UserName = UserDetails.UserName and Property.ClientId = UserDetails.ClientId) 
  " + strCondition + "order by " + orderby;
            }
        }
        else
        {
            if (strType == "1")
            {
                strCondition = @"Where Property.ClientId =" + ClientId + "  AND (Property.DealType = 1 Or Property.DealType = 4) AND ";

                ////if (strlocation.Trim() != "" && strlocation != "Any")
                ////{

                ////    strCondition += "PropertyLocation.LocationAKA='" + strlocation.Replace("'", "|@") + "' AND";

                ////}

                if (strlocation.Trim() != "" && strlocation.ToLower().Trim() != "all" && strlocation.ToLower() != "select" && strlocation != "Any")
                {
                    string[] strAllArea = strlocation.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    if (strAllArea != null && strAllArea.Length > 0)
                    {
                        strCondition += "(";
                        foreach (string strItem in strAllArea)
                        {
                            strCondition += "PropertyLocation.LocationAKA='" + strItem.Replace("'", "|@").Trim() + "' OR ";
                        }
                        strCondition = strCondition.TrimEnd(new char[] { 'O', 'R', ' ' });
                        strCondition += ") AND ";
                    }
                    else
                    {
                        strCondition += "(PropertyLocation.LocationAKA='" + strlocation.Replace("'", "|@").Trim() + "') AND ";
                    }
                }

                if ((strminprice.ToString().Replace(",", "") != "" && strminprice.ToString().Replace(",", "") != "0") && (strmaxprice.ToString().Replace(",", "") == "" || strmaxprice.ToString().Replace(",", "") == "0"))
                {
                    strCondition += "PropertySale.AskingPrice >= " + strminprice.ToString().Replace(",", "") + "";
                }
                else if ((strminprice.ToString().Replace(",", "") == "" || strminprice.ToString().Replace(",", "") == "0") && (strmaxprice.ToString().Replace(",", "") != "" && strmaxprice.ToString().Replace(",", "") != "0"))
                {
                    strCondition += "PropertySale.AskingPrice <= " + strmaxprice.ToString().Replace(",", "") + "";
                }
                else if (strminprice.ToString().Replace(",", "") != "" && strminprice.ToString().Replace(",", "") != "0" && strmaxprice.ToString().Replace(",", "") != "" && strmaxprice.ToString().Replace(",", "") != "0")
                {
                    strCondition += "(PropertySale.AskingPrice Between " + strminprice.ToString().Replace(",", "") + " AND " + strmaxprice.ToString().Replace(",", "") + "";
                }
                strCondition = strCondition.TrimEnd(new char[] { 'A', 'N', 'D', ' ' });

                sqlQuery = @"SELECT Property.PropertyId,Property.FurtherNotes,  (Select top 1 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId
                            and Files.Category like 'Main photo') As image,PropertyLocation.AreaOrRegionName,PropertyLocation.CityOrTownName,PropertyLocation.CountryName,PropertyLocation.Address as Address, PropertyLocation.Latitude,PropertyLocation.Longitude,PropertySale.AskingPrice
                            As Cost, PropertySale.AskingPrice As CostToOrder, PropertySale.PriceSuffix FROM Property Inner Join PropertySale ON (Property.PropertyId = PropertySale.PropertyId and Property.ClientId = PropertySale.ClientId)
                           Left JOIN PropertyLocation ON (Property.LocationId = PropertyLocation.LocationId and Property.ClientId = PropertyLocation.ClientId)
                           Left JOIN UserDetails ON (Property.UserName = UserDetails.UserName and Property.ClientId = UserDetails.ClientId) 
                           " + strCondition;
            }
            else
            {
                strCondition = @"Where Property.ClientId =" + ClientId + "  AND (Property.DealType = 2 Or Property.DealType = 4) AND ";

                if (strlocation.Trim() != "" && strlocation.ToLower() != "all" && strlocation.ToLower() != "select" && strlocation != "Any")
                {
                    string[] strAllArea = strlocation.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    if (strAllArea != null && strAllArea.Length > 0)
                    {
                        strCondition += "(";
                        foreach (string strItem in strAllArea)
                        {
                            strCondition += "PropertyLocation.LocationAKA='" + strItem.Replace("'", "|@").Replace(" ", "") + "' OR ";
                        }
                        strCondition = strCondition.TrimEnd(new char[] { 'O', 'R', ' ' });
                        strCondition += ") AND ";
                    }
                    else
                    {
                        strCondition += "(PropertyLocation.LocationAKA='" + strlocation.Replace("'", "|@").Replace(" ", "") + "') AND ";
                    }
                }

                ////if (strlocation.Trim() != "" && strlocation != "Any")
                ////{

                ////    strCondition += "PropertyLocation.LocationAKA='" + strlocation.Replace("'", "|@") + "' AND";

                ////}

                if ((strminprice.ToString().Replace(",", "") != "" && strminprice.ToString().Replace(",", "") != "0") && (strmaxprice.ToString().Replace(",", "") == "" || strmaxprice.ToString().Replace(",", "") == "0"))
                {
                    strCondition += " PropertyLet.Price >= " + strminprice.ToString().Replace(",", "") + "";
                }
                else if ((strminprice.ToString().Replace(",", "") == "" || strminprice.ToString().Replace(",", "") == "0") && (strmaxprice.ToString().Replace(",", "") != "" && strmaxprice.ToString().Replace(",", "") != "0"))
                {
                    strCondition += "PropertyLet.Price <= " + strmaxprice.ToString().Replace(",", "") + "";
                }
                else if (strminprice.ToString().Replace(",", "") != "" && strminprice.ToString().Replace(",", "") != "0" && strmaxprice.ToString().Replace(",", "") != "" && strmaxprice.ToString().Replace(",", "") != "0")
                {
                    strCondition += "(PropertyLet.Price Between " + strminprice.ToString().Replace(",", "") + " AND " + strmaxprice.ToString().Replace(",", "") + ")";
                }

                strCondition = strCondition.TrimEnd(new char[] { 'A', 'N', 'D', ' ' });

                sqlQuery = @"SELECT Property.PropertyId,Property.FurtherNotes,(Select top 1 Cast(Files.ClientId as varchar(50))+'-'+Cast(Files.PKey as varchar(50))+'-'+Files.FileName from Files Where Files.ModuleId=Property.PropertyId and Files.ClientId=Property.ClientId
                           and Files.Category like 'Main photo') As image,PropertyLocation.AreaOrRegionName,PropertyLocation.CityOrTownName,PropertyLocation.CountryName,PropertyLocation.Address as Address,
                           PropertyLocation.Latitude,PropertyLocation.Longitude,PropertyLet.Price as Cost,PropertyLet.Price as CostToOrder,PropertyLet.BondAmount, PropertyLet.PricePer,PropertyLet.MinimumTerm FROM Property Inner Join PropertyLet ON (Property.PropertyId = PropertyLet.PropertyId and Property.ClientId = PropertyLet.ClientId)
                           Left JOIN PropertyLocation ON (Property.LocationId = PropertyLocation.LocationId and Property.ClientId = PropertyLocation.ClientId)
                           Left JOIN UserDetails ON (Property.UserName = UserDetails.UserName and Property.ClientId = UserDetails.ClientId) 
                           " + strCondition;
            }

        }
        SqlDataAdapter PropertyDA = new SqlDataAdapter(sqlQuery, Conn);
        DataSet ds = new DataSet();
        try
        {
            Conn.Open();
            PropertyDA.Fill(ds);
            if (Conn.State == ConnectionState.Open) { Conn.Close(); }
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    arrpropid.Add(ds.Tables[0].Rows[i]["PropertyId"].ToString());
                }
                for (int k = 0; k < arrpropid.Count; k++)
                {
                    if (propid == arrpropid[k].ToString())
                    {
                        arrCnt = k;
                        break;
                    }
                }
                string propertyid = "";
                if (arrCnt == 0)
                {
                    propertyid = "";
                    ds.Tables.Clear();
                }
                else
                {

                    propertyid = ds.Tables[0].Rows[arrCnt - 1]["PropertyId"].ToString();
                    for (int j = 0; j < ds.Tables[0].Rows.Count; j++)
                    {

                        if (propertyid != ds.Tables[0].Rows[j]["PropertyId"].ToString())
                        {
                            ds.Tables[0].Rows.RemoveAt(j);
                            j--;
                            continue;
                        }
                    }
                }
            }
        }
        catch { }
        finally
        {
            if (Conn.State == ConnectionState.Open) { Conn.Close(); }
        }
        return ds;
    }

    public DataSet getPost(string strType)
    {
        DataSet ds = new DataSet();
        SqlConnection Conn = new SqlConnection(strConn);
        try
        {
        string strCondition="",strTable="";
        if(strType =="2")
        {
         strCondition = @"Where Property.ClientId =" + ClientId + " AND (Property.DealType = 2 Or Property.DealType = 4) and PropertyLocation.Postcode!='' ";
            strTable ="PropertyLet";
        }
        else
        {
            strCondition = @"Where Property.ClientId =" + ClientId + " AND (Property.DealType = 1 Or Property.DealType = 4) and PropertyLocation.PostCode!='' ";
            strTable="PropertySale";
        }

        string strQuery = @"SELECT distinct PropertyLocation.PostCode as Area FROM Property Left Join PropertyDescriptive ON 
                    (Property.PropertyId = PropertyDescriptive.PropertyId and Property.ClientId = PropertyDescriptive.ClientId)
                    Left JOIN PropertyLocation ON (Property.LocationId = PropertyLocation.LocationId and Property.ClientId = PropertyLocation.ClientId)
                    Left JOIN UserDetails ON (Property.UserName = UserDetails.UserName and Property.ClientId = UserDetails.ClientId)
                    Inner Join "+strTable+" ON (Property.PropertyId ="+ strTable +".PropertyId and Property.ClientId ="+ strTable+".ClientId) "  + strCondition;
           
          SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
          
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;

    }


}

