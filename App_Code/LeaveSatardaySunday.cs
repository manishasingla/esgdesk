﻿using System;
using System.Collections.Generic;
using System.Data.Sql;
using System.Web;
using DAL;
using System.Data;


/// <summary>
/// Summary description for LeaveSatardaySunday
/// </summary>
public class LeaveSatardaySunday
{
    
    string STRSQL = "";
	public LeaveSatardaySunday()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public DataSet getLeaveTupe()
    {
        DataAccessLayer DAL = new DataAccessLayer();        
        STRSQL = "spGetLeaveType"; //Previous sp
        return DAL.ExecuteDatasetSP(STRSQL);      
    }
    public DataSet InsertSatSundayLeave(string AbsentDate, string UserName, string Reason, string PaidOrNPaid, string LeaveType, int IsEHPL, int IsPlanned, int Deductable)
    {
        DataAccessLayer Dal = new DataAccessLayer();
        //STRSQL = "spAddLeaves";//sp_SatardaySunday//usp_insertLeave
        //STRSQL = "sp_SatardaySunday";
        STRSQL = "usp_insertLeave";  //Bishnu
        Dal.ParameterSP("@AbsentDate1",AbsentDate.ToString());
        Dal.ParameterSP("@UserName", UserName.ToString());
        Dal.ParameterSP("@Reason", Reason.ToString());
        Dal.ParameterSP("@PaidOrNPaid", PaidOrNPaid.ToString());
        Dal.ParameterSP("@LeaveType", LeaveType.ToString());
        Dal.ParameterSP("@IsEHPL", IsEHPL.ToString());
        Dal.ParameterSP("@IsPlanned", IsPlanned.ToString());
        Dal.ParameterSP("@Deductable", Deductable.ToString());
        return Dal.ExecuteDatasetSP(STRSQL);       
    }
    public DataSet GetUserLeave(string UserName,string year)
    {
        DataAccessLayer DAL = new DataAccessLayer();
        STRSQL = "sp_GetUserLeaveDetails"; //Bishnu
        DAL.ParameterSP("@UserName", UserName.ToString());
        DAL.ParameterSP("@year", year.ToString());//added by rutuja - 18 July 2016
        return DAL.ExecuteDatasetSP(STRSQL);
    }
    public DataSet checkAbsent(string UserName,string AbDate)
    {
        DataAccessLayer DAL = new DataAccessLayer();
        STRSQL = "spCheckLeaveDateInAbsentDetails1";  //Bishnu
        DAL.ParameterSP("@UserName", UserName.ToString());
        DAL.ParameterSP("@AbsentDate", AbDate.ToString());
        return DAL.ExecuteDatasetSP(STRSQL);
    }
    public DataSet DeleteAbsent(string UserName, string AbDate)
    {
        DataAccessLayer DAL = new DataAccessLayer();
        STRSQL = "usp_DeleteAbsentDetail"; //Bishnu       
        DAL.ParameterSP("@AbsentDate", AbDate.ToString());
        DAL.ParameterSP("@UserName", UserName.ToString());
        return DAL.ExecuteDatasetSP(STRSQL);
    }
    //spUpdateLeaves
    public DataSet updateAbsent(string AbsentDate, string UserName, string Reason, string PaidOrNPaid, string LeaveType, int IsEHPL, int IsPlanned, int Deductable)
    {
        DataAccessLayer DAL = new DataAccessLayer();
        STRSQL = "spUpdateLeaves1"; //Bishnu
        DAL.ParameterSP("@AbsentDate1", AbsentDate.ToString());
        DAL.ParameterSP("@UserName", UserName.ToString());
        DAL.ParameterSP("@Reason", Reason.ToString());
        DAL.ParameterSP("@PaidOrNPaid", PaidOrNPaid.ToString());
        DAL.ParameterSP("@LeaveType", LeaveType.ToString());
        DAL.ParameterSP("@IsEHPL", IsEHPL.ToString());
        DAL.ParameterSP("@IsPlanned", IsPlanned.ToString());
        DAL.ParameterSP("@Deductable", Deductable.ToString());
        return DAL.ExecuteDatasetSP(STRSQL);
    }
    public DataSet CountConsumedPaid(string UserName)
    {
        DataAccessLayer DAL = new DataAccessLayer();
        STRSQL = "usp_CountPaidLeave"; //Bishnu
        DAL.ParameterSP("@employee", UserName.ToString());       
        return DAL.ExecuteDatasetSP(STRSQL);
    }

    //For Leave Report
    public DataSet GetAbsentDetails(string UserName, int nYear)
    {
        DataAccessLayer DAL = new DataAccessLayer();
        STRSQL = "sp_GetLeaveDetails";
        DAL.ParameterSP("@UserName", UserName.ToString());
        DAL.ParameterSP("@Year", nYear.ToString());
        return DAL.ExecuteDatasetSP(STRSQL);
    }
    public DataSet GetAbsentDetails(string UserName, int nYear ,string option)
    {
        DataAccessLayer DAL = new DataAccessLayer();
        STRSQL = "sp_GetLeaveDetails";
        DAL.ParameterSP("@UserName", UserName.ToString());
        DAL.ParameterSP("@Year", nYear.ToString());
        DAL.ParameterSP("@option",option.ToString());
        return DAL.ExecuteDatasetSP(STRSQL);
    }
}