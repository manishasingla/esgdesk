﻿using System;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.IO;

public class PhotosDAL
{
    public static string ConnectionString
    {
        get
        {
            //return WebConfigurationManager.ConnectionStrings["strConn"].ConnectionString;
            return ConfigurationManager.AppSettings["strConn"].ToString();
        }
    }

    public static DataTable GetAlbums(String username) 
    {
        SqlConnection con = new SqlConnection(ConnectionString);
        try
        {
            SqlDataAdapter da = new SqlDataAdapter("getalbums", con);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("@username", username);
            DataSet ds = new DataSet();
            da.Fill(ds, "albums"); // create table in ds
            return ds.Tables[0]; // return first table
        }  // try
        catch (Exception ex)
        {
            HttpContext.Current.Trace.Write(
                "Error From GetAlbums -->" + ex.Message);
            return null;
        }
    }

    public static DataTable GetAlbums()
    {
        SqlConnection con = new SqlConnection(ConnectionString);
        try
        {
            SqlDataAdapter da = new SqlDataAdapter("getalbums", con);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("@username", "");
            DataSet ds = new DataSet();
            da.Fill(ds, "albums"); // create table in ds
            return ds.Tables[0]; // return first table
        }  // try
        catch (Exception ex)
        {
            HttpContext.Current.Trace.Write(
                "Error From GetAlbums -->" + ex.Message);
            return null;
        }
    }

    public static  bool DeleteAlbum(String aid)
    {
        SqlConnection con = new SqlConnection(ConnectionString);
        try
        {
            SqlDataAdapter da = new SqlDataAdapter("getphotoIds", con);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("@aid",aid);
            DataSet ds = new DataSet();
            da.Fill(ds, "photoids");

            con.Open(); 
            // call deletealbum stored procedure 
            da.SelectCommand.CommandText = "deletealbum";
            da.SelectCommand.ExecuteNonQuery();

            // now delete files from PHOTOS folder
            String photospath = HttpContext.Current.Request.MapPath("photos");
            foreach ( DataRow row in ds.Tables[0].Rows) {
                File.Delete(photospath + "/" + row[0] + ".jpg"); 
            }

            return true;

        }  // try
        catch (Exception ex)
        {
            HttpContext.Current.Trace.Write(
                "Error From DeleteAlbum -->" + ex.Message);
            return false;
        }
    } // DeleteAlbum


    public static void DeletePhoto(String photoid)
    {
        SqlConnection con = new SqlConnection(ConnectionString);
        try
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("delete from photos where photoid = @photoid", con);
            cmd.Parameters.AddWithValue("@photoid", photoid);
            cmd.ExecuteNonQuery();

            // now delete file from PHOTOS folder
            String photospath = HttpContext.Current.Request.MapPath("photos");
            File.Delete(photospath + "/" + photoid + ".jpg");
        }  // try
        catch (Exception ex)
        {
            HttpContext.Current.Trace.Write(
                "Error From DeletePhoto -->" + ex.Message);
        }
        finally
        {
            con.Close();
        }
    } // DeleteAlbum

    public static void UpdatePhoto(String photoid, String title, String tags)
    {
        SqlConnection con = new SqlConnection(ConnectionString);
        try
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("update photos set title = @title, tags = @tags where photoid = @photoid", con);
            cmd.Parameters.AddWithValue("@photoid", photoid);
            cmd.Parameters.AddWithValue("@title", title);
            cmd.Parameters.AddWithValue("@tags", tags);
            cmd.ExecuteNonQuery();
        }  // try
        catch (Exception ex)
        {
            HttpContext.Current.Trace.Write(
                "Error From UpdatePhoto -->" + ex.Message);
        }
        finally
        {
            con.Close();
        }
    } // DeleteAlbum


    public static DataTable GetPhotoDetails(String photoid)
    {
        SqlConnection con = new SqlConnection(ConnectionString);
        try
        { 
            
            string strQuery = "select p.photoid , p.title, p.tags, p.addedon , a.title atitle";
	        strQuery+=" from  photos p inner join  albums a  on a.aid= p.aid where photoid = @photoid";
	 
            //SqlDataAdapter da = new SqlDataAdapter("select photoid,title,tags,addedon from photos where photoid = @photoid", con);
          SqlDataAdapter da = new SqlDataAdapter(strQuery, con);
            da.SelectCommand.Parameters.AddWithValue("@photoid", photoid);
            DataSet ds = new DataSet();
            da.Fill(ds, "photos"); // create table in ds
            return ds.Tables[0]; // return first table
        }  // try
        catch (Exception ex)
        {
            HttpContext.Current.Trace.Write(
                "Error From GetPhotoDetails -->" + ex.Message);
            return null;
        }
    }

    public static DataTable GetAlbumDetails(String aid)
    {
        SqlConnection con = new SqlConnection(ConnectionString);
        try
        {
            SqlDataAdapter da = new SqlDataAdapter("get_album_details", con);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("@aid", aid);
            DataSet ds = new DataSet();
            da.Fill(ds, "albumdetails"); // create table in ds
            return ds.Tables[0]; // return first table
        }  // try
        catch (Exception ex)
        {
            HttpContext.Current.Trace.Write(
                "Error From GetAlbumDetails -->" + ex.Message);
            return null;
        }
    }


    public static DataTable GetPhotosFromAlbum(String aid)
    {
        SqlConnection con = new SqlConnection(ConnectionString);
        try
        {
            SqlDataAdapter da = new SqlDataAdapter("GetPhotosFromAlbum", con);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("@aid",aid);
            DataSet ds = new DataSet();
            da.Fill(ds, "photos");
            return ds.Tables[0];
        }  // try
        catch (Exception ex)
        {
            HttpContext.Current.Trace.Write("Error From GetAlbums -->" + ex.Message);
            return null;
        }
    }

    public static int CreateAlbum(String username,String title, String description,String strEventDate)
    {
        SqlConnection con = new SqlConnection(ConnectionString);
        DateTime dtEventDate = Convert.ToDateTime(strEventDate);
        try
        {
            con.Open();

            SqlParameter aid = new SqlParameter("@aid", SqlDbType.Int);
            aid.Direction = ParameterDirection.Output;
            SqlCommand cmd = new SqlCommand("createalbum", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@username", username);
            cmd.Parameters.AddWithValue("@title", title);
            cmd.Parameters.AddWithValue("@description", description);
            cmd.Parameters.AddWithValue("@eventdate", dtEventDate);
            cmd.Parameters.Add(aid);
            cmd.ExecuteNonQuery();
            return (int)aid.Value;
        }  // try
        catch(Exception ex )
        {
            // error to trace
            return 0;
        }
        finally 
        {
            con.Close();
        }
    } // end of CreateAlbum

    // returns photo id
    public static int AddPhoto(String aid, String filename, String title, String tags)
    {
        SqlConnection con = new SqlConnection(ConnectionString);
        try
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("addphoto", con);
            cmd.CommandType = CommandType.StoredProcedure;
            
            SqlParameter photoid = new SqlParameter("@photoid", SqlDbType.Int);
            photoid.Direction = ParameterDirection.Output;

            cmd.Parameters.AddWithValue("@aid", aid);
            cmd.Parameters.AddWithValue("@filename", filename);
            cmd.Parameters.AddWithValue("@title", title);
            cmd.Parameters.AddWithValue("@tags",tags);
            
            cmd.Parameters.Add(photoid);
            cmd.ExecuteNonQuery();

            return (int)photoid.Value;
        }  // try
        catch (Exception ex)
        {
            HttpContext.Current.Trace.Write(ex.Message);
            return -1;
        }
        finally
        {
            con.Close();
        }
    } // end of AddPhoto

    public static DataTable SearchForPhotos(String pattern)
    {
        SqlConnection con = new SqlConnection(ConnectionString);
        try
        {
            SqlDataAdapter da = new SqlDataAdapter("SearchForPhotos", con);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("@pattern", pattern);
            DataSet ds = new DataSet();
            da.Fill(ds, "photos");
            return ds.Tables[0];
        }  // try
        catch (Exception ex)
        {
            HttpContext.Current.Trace.Write("Error From SearchForPhotos -->" + ex.Message);
            return null;
        }
    }



    public static int SetCoverPhoto(String strphotoid, String aid)
    {
        SqlConnection con = new SqlConnection(ConnectionString);
        SqlParameter photoid2 = new SqlParameter("@photoid2", SqlDbType.Int);
        int intphotoid = Convert.ToInt32(strphotoid);
        try
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("setCover", con);

          
            photoid2.Direction = ParameterDirection.Output;

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@photoid1", intphotoid);
            cmd.Parameters.AddWithValue("@aid", aid);
            cmd.Parameters.Add(photoid2);
            cmd.ExecuteNonQuery();

        }  // try
        catch (Exception ex)
        {
            HttpContext.Current.Trace.Write(
                "Error From UpdatePhoto -->" + ex.Message);
        }
        finally
        {
            con.Close();
        }
        return (int)photoid2.Value;
    }



    public static void UpdateComments(String photoid, String Comments, string username)
    {
        SqlConnection con = new SqlConnection(ConnectionString);
        try
        {
            string photoid1="";
         
            con.Open();
            SqlCommand cmd = new SqlCommand("update photos set Comments = @Comments where photoid = @photoid and username=@username", con);
            cmd.Parameters.AddWithValue("@photoid", Comments);
            cmd.Parameters.AddWithValue("@username", username);
            cmd.Parameters.AddWithValue("@Comments", Comments);
            cmd.ExecuteNonQuery();
        }  // try
        catch (Exception ex)
        {
            HttpContext.Current.Trace.Write(
                "Error From UpdatePhoto -->" + ex.Message);
        }
        finally
        {
            con.Close();
        }
    }

    public static DataTable GetComments(String photoid)
    {
        SqlConnection con = new SqlConnection(ConnectionString);
        DataSet ds = new DataSet();
        try
        {
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter("select * from Comments", con);
            ds = new DataSet();
            da.Fill(ds, "Comments"); // create table in ds
           
           
        }  // try
        catch (Exception ex)
        {
            HttpContext.Current.Trace.Write(
                "Error From UpdateComments -->" + ex.Message);
        }
        finally
        {
            con.Close();
        }
        return  ds.Tables[0]; 
    } // DeleteAlbum
    
    // DeleteAlbum


     public static void InsertComment(String photoid, String Comment, String username, String aid)
    {
        SqlConnection con = new SqlConnection(ConnectionString);
        try
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("addComment", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@photoid", photoid);
            cmd.Parameters.AddWithValue("@Comment", Comment);
            cmd.Parameters.AddWithValue("@username", username);
            cmd.Parameters.AddWithValue("@aid",aid);
          
            cmd.ExecuteNonQuery();

            //return (int)photoid.Value;
        }  // try
        catch (Exception ex)
        {
            HttpContext.Current.Trace.Write(ex.Message);
           // return -1;
        }
        finally
        {
            con.Close();
        }
    } 
}  // end of PhotosDAL

