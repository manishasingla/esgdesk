﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

/// <summary>
/// Summary description for UpdateData
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class UpdateData : System.Web.Services.WebService {

    //public UpdateData () {

    //    //Uncomment the following line if using designed components 
    //    //InitializeComponent(); 
    //}
    

    [WebMethod]
    public DataSet GetData() {

        SqlConnection Con = new SqlConnection(ConfigurationManager.AppSettings["strConn"]);
        string sqlQuery = "";
       sqlQuery = "select WebsiteUrl, project_name from projects where  active = 'Y' and WebsiteUrl<>''";
       // sqlQuery = "select WebsiteUrl, project_name from projects where datalength(WebsiteUrl) > 0 and active= 'Y'";
        SqlDataAdapter MonitorDA = new SqlDataAdapter(sqlQuery, Con);
        DataSet ds = new DataSet();

        SqlDataAdapter Sqlda;

        try
        {

            Sqlda = new SqlDataAdapter(sqlQuery, Con);
            Sqlda.Fill(ds, "PR");

        }


        catch (Exception ex)
        {
            FnInsertErrorlog(ex.Message.ToString());
        }
        finally
        {
            Con.Close();
        }
        return ds;
    }
    public void FnInsertErrorlog(string Errlog)
    {



        SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["strConn"]);
        try
        {
            SqlCommand cmdInsert = new SqlCommand("usp_InsertErrorLog", con);
            cmdInsert.CommandType = CommandType.StoredProcedure;
            cmdInsert.Parameters.Add("@flag", SqlDbType.Bit);
            cmdInsert.Parameters["@flag"].Direction = ParameterDirection.Output;
            cmdInsert.Parameters.Add("@SDate", SqlDbType.DateTime).Value = DateTime.Now.ToString();
            cmdInsert.Parameters.Add("@AlertMessage", SqlDbType.NVarChar).Value = Errlog;
            con.Open();
            cmdInsert.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            FnInsertErrorlog(ex.Message.ToString());
        }

        finally
        {
            con.Close();
        }

    }
    
}
