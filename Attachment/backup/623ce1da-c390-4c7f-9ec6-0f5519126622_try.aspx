﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="index" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Welcome to <%= System.Configuration.ConfigurationManager.AppSettings["companyname"] %></title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="keyphase" content="" />
<link id="mainCSS" href="xample.css" type="text/css" rel="stylesheet" media="screen" />
<link id="slideshowCSS" rel="stylesheet" href="css/lightbox.css" media="screen" />
<link id="colorpickerCSS" rel="stylesheet" href="js_color_picker_v2.css" media="screen" />
<script type="text/javascript" language="javascript">
var gkey = '<%= System.Configuration.ConfigurationManager.AppSettings["gKey"] %>';
var bodycolor = '<%= System.Configuration.ConfigurationManager.AppSettings["bodycolor"] %>';
var endcolor = '<%= System.Configuration.ConfigurationManager.AppSettings["endcolor"] %>';
var menucolor = '<%= System.Configuration.ConfigurationManager.AppSettings["menucolor"] %>';
var companyname = '<%= System.Configuration.ConfigurationManager.AppSettings["companyname"] %>';
</script>
<script type="text/javascript" src="changePage.js"></script>
</head>
<body id="body1" runat="server" oncontextmenu="return false;"></iframe>
<div id="Calluspopup"><h3>Call us on 020 7510 9500 </h3></div>
<div id="topline" runat="server"></div>
<form id="form1" runat="server">
<input type="text" maxlength="7" style="display:none" id="newval" name="newval" size="10">
<div id="maindiv">
	<div id="topnav">
    	<ul>
        <li id="liHome"><a href="#" onclick="return getOtherPage('index.aspx');" id="Home" runat="server"><span>Home</span></a></li>
         <li id="liWorks"><a href="#" id="Work" onclick="return getOtherPage('working.aspx');" onmouseover="this.style.backgroundColor=menucolor;" onmouseout="this.style.backgroundColor=document.body.style.backgroundColor;"><span>How it Works</span></a></li>
        <li id="liLandlord"><a href="#" id="Landlrd" onclick="return getOtherPage('landlords.aspx');" onmouseover="this.style.backgroundColor=menucolor;" onmouseout="this.style.backgroundColor=document.body.style.backgroundColor;"><span>Landlords</span></a></li>
       <%-- <li id="liRent"><a href="#" id="Rent" onclick="return getOtherPage('rent_whychooseus.aspx');" onmouseover="this.style.backgroundColor=menucolor;" onmouseout="this.style.backgroundColor=document.body.style.backgroundColor;"><span>Lettings</span></a></li>--%>
        <li id="liTenants"><a href="#" id="Tenants" onclick="return getOtherPage('tenant.aspx');" onmouseover="this.style.backgroundColor=menucolor;" onmouseout="this.style.backgroundColor=document.body.style.backgroundColor;"><span>Tenants</span></a></li>
               <li id="liroom"><a href="#" id="room" onclick="return getOtherPage('rooms.aspx');" onmouseover="this.style.backgroundColor=menucolor;" onmouseout="this.style.backgroundColor=document.body.style.backgroundColor;"><span>Rooms</span></a></li>
        <li id="liInvestment"><a href="#" id="Invstmnt" onclick="return getOtherPage('investment.aspx');" onmouseover="this.style.backgroundColor=menucolor;" onmouseout="this.style.backgroundColor=document.body.style.backgroundColor;"><span>Investment</span></a></li>
        <li id="liMembersArea"><a href="#" id="Members" onclick="return getOtherPage('editprofile.aspx');" runat="server"><span>REGISTER</span></a></li>
      <li id="liAboutUs"><a href="#" id="AboutUs" onclick="return getOtherPage('aboutus.aspx');" onmouseover="this.style.backgroundColor=menucolor;" onmouseout="this.style.backgroundColor=document.body.style.backgroundColor;"><span>About Us</span></a></li>
      <li id="liContactUs"><a href="#" id="ContactUs" onclick="return getOtherPage('contactus.aspx');" onmouseover="this.style.backgroundColor=menucolor;" onmouseout="this.style.backgroundColor=document.body.style.backgroundColor;"><span>Contact Us</span></a></li>
        </ul>
        <div class="topicons2"><a href="#" onclick="return getOtherPage('index.aspx');"><img src="images/homeicon.png" border="0" title="Home" /></a><a href="#" onclick="return getOtherPage('contactus.aspx');"><img src="images/contacticon.png" border="0" title="Contact Us" /></a></div></div>
          <div id="header"  >
           <div class="logo2"><a href="index.aspx"><img src="LOGO.png" width="200" height="79" border="0"/></a></div>
    <div class="logan2"><%= System.Configuration.ConfigurationManager.AppSettings["strapline"] %></div>
           </div>         
       <br/>
    <div class="dateset"><span id="settimenow"></span></div>
    <div id="welcome"><asp:Label CssClass="welcome" id="lblWelcome" runat="server"></asp:Label></div>
    <div class="clear"></div>
    <div class="shadowtop"></div>
    <div id="contentdiv" class="contentdiv"><div id="mainimage" class="mainimage">
      <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="608" height="382">
        <param name="movie" value="mainflash.swf" />
        <param name="quality" value="high" />
        <param name="wmode" value="transparent" />
        <param name="menu" value="false" />
        <embed src="mainflash.swf" quality="high" wmode="transparent" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="608" height="382"></embed>
      </object>    
      
      <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="540" height="77">
          <param name="movie" value="boticons.swf" />
          <param name="menu" value="false" />
          <param name="wmode" value="transparent" />
          <param name="quality" value="high" />
          <embed src="boticons.swf" quality="high" wmode="transparent" menu="false" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="540" height="77"></embed></object>
            <div id="flashicon">
        </div>
       
              
        </div>
                <%--<div class="searchf"><div id="advancesearchimg"><img src="images/img_advsearch.jpg" /></div><div id="divSearch"><%= psearch() %></div><div id="callus" class="callus" ><h3>&nbsp;</h3>
        <h3>&nbsp;</h3>
       Call us on <br/>
        <%= System.Configuration.ConfigurationManager.AppSettings["companyphonenumber"] %>
                </div></div>--%>

<div class="searchf"><h4>Rent a Room</h4>
    <p>&nbsp;</p>
House <asp:CheckBox ID="CheckBox1" runat="server" /> <span style="margin-left:30px;"></span>       Apartment <asp:CheckBox ID="CheckBox2" runat="server" />
    

    <br /></br>
 <strong>Locations  </strong> <br />
    <asp:DropDownList ID="DropDownList1" runat="server" Height="20px" Width="165px">
  <asp:ListItem>All</asp:ListItem>  
  <asp:ListItem>Canary Wharf</asp:ListItem>  
  <asp:ListItem>Docklands</asp:ListItem>  
  <asp:ListItem>Isle of Dogs</asp:ListItem>  
  <asp:ListItem>Limehouse</asp:ListItem>  
    <asp:ListItem>Royal Docks</asp:ListItem>   
  </asp:DropDownList>
  <br /></br>
   <strong>Room Types</strong><br />
    <asp:DropDownList ID="DropDownList2" runat="server" Height="20px" Width="165px">
     <asp:ListItem>Select</asp:ListItem>
    <asp:ListItem>Single Room</asp:ListItem>
       <asp:ListItem>Double Room</asp:ListItem>
          <asp:ListItem>Large Double Room</asp:ListItem>
             <asp:ListItem>En-Suite Double</asp:ListItem>
              </asp:DropDownList>
              <br /></br>
               <strong>Prices</strong>
    <br />
    <asp:DropDownList ID="DropDownList3" runat="server" Height="20px" Width="165px">
    <asp:ListItem>Min</asp:ListItem>
     <asp:ListItem>£100pw</asp:ListItem>
 <asp:ListItem>£125pw</asp:ListItem>
 <asp:ListItem>£150pw</asp:ListItem>
 <asp:ListItem>£175pw</asp:ListItem>
 <asp:ListItem>£200pw</asp:ListItem>
  <asp:ListItem>£225pw</asp:ListItem>
      <asp:ListItem>£250pw</asp:ListItem>
       <asp:ListItem>£275pw</asp:ListItem>
         <asp:ListItem>£300pw</asp:ListItem>
    </asp:DropDownList>
    <br /></br>
    <asp:DropDownList ID="DropDownList4" runat="server" Height="20px" Width="165px">
    <asp:ListItem>Max</asp:ListItem>
     <asp:ListItem>£100pw</asp:ListItem>
      <asp:ListItem>£125pw</asp:ListItem>
       <asp:ListItem>£150pw</asp:ListItem>
        <asp:ListItem>£175pw</asp:ListItem>
         <asp:ListItem>£200pw</asp:ListItem>
         <asp:ListItem>£225pw</asp:ListItem>
         <asp:ListItem>£250pw</asp:ListItem>
         <asp:ListItem>£275pw</asp:ListItem>
         <asp:ListItem>£300pw</asp:ListItem>
    </asp:DropDownList>
    <br /></br>
     <strong>Areas</strong>
    <br />
    <asp:DropDownList ID="DropDownList5" runat="server" Height="20px" Width="165px">
    <asp:ListItem>Select</asp:ListItem>
    <asp:ListItem>Zone 1</asp:ListItem>
       <asp:ListItem>Zone 2</asp:ListItem>
          <asp:ListItem>Zone 3</asp:ListItem>
             <asp:ListItem>Zone 4</asp:ListItem>
                <asp:ListItem>Zone 5</asp:ListItem>
                   <asp:ListItem>Zone 6</asp:ListItem>
    </asp:DropDownList>
     
     <br /></br><a class="formsearch">Search</a>
     <br /><br />
     <div class="callus" id="callus"><h3>&nbsp;</h3>
        <h3>&nbsp;</h3>
       Call us on <br>
        <span class="skype_c2c_print_container notranslate">020 7510 9500</span><span data-ismobile="false" data-isrtl="false" data-isfreecall="false" data-numbertype="paid" data-numbertocall="+912075109500" onclick="SkypeClick2Call.MenuInjectionHandler.makeCall(this, event)" onmouseout="SkypeClick2Call.MenuInjectionHandler.hideMenu(this, event)" onmouseover="SkypeClick2Call.MenuInjectionHandler.showMenu(this, event)" tabindex="-1" dir="ltr" class="skype_c2c_container notranslate" id="skype_c2c_container"><span skypeaction="skype_dropdown" dir="ltr" class="skype_c2c_highlighting_inactive_common"><span id="non_free_num_ui" class="skype_c2c_textarea_span"><img width="0" height="0" src="resource://skype_ff_extension-at-jetpack/skype_ff_extension/data/call_skype_logo.png" class="skype_c2c_logo_img"><span class="skype_c2c_text_span">020 7510 9500</span><span class="skype_c2c_free_text_span"></span></span></span></span>
                </div>
</div>

    <div class="clear"></div></div>
  <div class="shadowbottom"><div class="hidbtn"><asp:Button ID="lnkSearch" runat="server" CssClass="hidnbtn" Text="" Height="0px" Width="0px" /><asp:HiddenField ID="hdnSearchValues" runat="server" /><asp:HiddenField ID="hdnSaveSearch" runat="server" /></div></div>
  <div id="copyright">Created by  <a href ="http://www.estatesolutions.co.uk" target="_blank" >www.estatesolutions.co.uk </a>| <a href="#" onclick="MM_openBrWindow('privacylaw.aspx?','','status=yes,scrollbars=yes,width=660,height=650')">Privacy Law</a> | <a href="#" onclick="MM_openBrWindow('disclaimer.aspx?','','status=yes,scrollbars=yes,width=660,height=509')">Disclaimer</a></div>
</div>
</form>
<div id="hiddenDiv" style="display:none"></div>
</body>
</html>
