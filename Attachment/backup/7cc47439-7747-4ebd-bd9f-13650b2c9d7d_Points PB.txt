
Changes in Packaging Bazaar Project
------------------------------------



1 Product Name replace with 

Typical Product display for the chosen Style 2 - http://www.packagingbazaar.e-s-g.co.uk/Supplier/supplier-showroom.aspx


2 Button label change - Send Message to Contact Vendor 


3 Menu Capitalisation in all forms required.


4 The SUpplier Login name is not displayed when clicked on create showroom. Should be available in all pages.\


5 First Time the Category is not getting populated in Add products screen


6 All dropdowns in all forms should be sorted A-Z


7 fEATURED PRODUCT CHECKBOX NOT VISIBLE IN Add PRODUCTS sCREEN


8 sUBMIT bUTTON lABEL SHOULD BE CHANGED TO Next lABEL


9 The Go Back label should be changed to Add Product and another button similar to Next Button of Product Page with a Label Finish


10 Add Products to Showcase Form Header


11 Paypal Or Redemption Word in Supplier Invoice Form


12 Product Master - Edit and Delete not working.
Delete to be aligned with Edit. Cancel not required.



13 Logic of adding or deleting Product in add products to showcase is not working. 


14 Add to cart, Buy and Enquiry Buttons together on the right in all the pages.


15 To check in Suppliers forms for the iMAGE DISPLAY. - eXAMPLE sUP 188


16 Hi uSERNAME SHOULD NOT HAVE EXCLAMATION MARK FOR ALL LOGINS 


17 cHECKOUT MENU LINK TO BE REMOVED


18 eVEN AFTER DELETING THE PRODUCTS FROM CART, THE PAYMENT SCREEN IS DISPLAYING ALL THE PRODUCTS