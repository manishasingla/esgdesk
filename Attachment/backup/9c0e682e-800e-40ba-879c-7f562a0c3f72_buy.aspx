<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head>


<script type="text/javascript" src="/static/js/analytics.js"></script>
<script type="text/javascript">archive_analytics.values.server_name="wwwb-app7.us.archive.org";archive_analytics.values.server_ms=125;</script>
<link type="text/css" rel="stylesheet" href="/static/css/banner-styles.css"/>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>
	 Xample
</title><link href="class/class.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="js/featprop.js"></script>
<script type="text/javascript" language="javascript" src="js/script.js"></script>
<script type="text/javascript" language="javascript">
function setLocations() {
	var hiddenareas = document.getElementById("hdnLocations");
	var chkbxs = document.getElementById("Repeater1").getElementsByTagName("input");
	hiddenareas.value = "";
	for(var i=0;i<chkbxs.length;i++)
	{
		if(chkbxs[i].checked==true) { hiddenareas.value += chkbxs[i].title + ";"; }
	}
}

function setPostcodes() {
	var hiddenareas = document.getElementById("hdnPostcodes");
	var chkbxs = document.getElementById("Repeater2").getElementsByTagName("input");
	hiddenareas.value = "";
	for(var i=0;i<chkbxs.length;i++)
	{
		if(chkbxs[i].checked==true) { hiddenareas.value += chkbxs[i].title + ";"; }
	}
}

function setAllChecked(obj) {
	var chkbxs = document.getElementById("Repeater1").getElementsByTagName("input");
	for(var i=0;i<chkbxs.length;i++)
	{
		chkbxs[i].checked=obj.checked;
	}
	chkbxs = document.getElementById("Repeater2").getElementsByTagName("input");
	for(var i=0;i<chkbxs.length;i++)
	{
		chkbxs[i].checked=obj.checked;
	}
	setLocations();
	setPostcodes();
}

function checkAnyChecked() {
	var blnchecked = false;
	if(document.getElementById("rdbArea").checked == true) {
		var hidden =  document.getElementById("hdnLocations");
		if(hidden.value == "" || hidden.value == "all" || hidden.value == "All")
		{
			alert("Please check any of the locations first.");
			return;
		}
	}
	else if(document.getElementById("rdbPostcode").checked == true) {
		var hidden =  document.getElementById("hdnPostcodes");
		if(hidden.value == "" || hidden.value == "all" || hidden.value == "All")
		{
			alert("Please check any of the postcodes first.");
			return;
		}
	}
	document.getElementById("btnSearch").click();
}
</script>
</head>
<body>


<!-- BEGIN WAYBACK TOOLBAR INSERT -->
<script type="text/javascript" src="/static/js/disclaim-element.js" ></script>
<script type="text/javascript" src="/static/js/graph-calc.js" ></script>
<script type="text/javascript">//<![CDATA[
var __wm = (function(imgWidth,imgHeight,yearImgWidth,monthImgWidth){
var wbPrefix = "/web/";
var wbCurrentUrl = "http://www.gates.esuk.co.uk/buy.aspx";

var firstYear = 1996;
var displayDay = "21";
var displayMonth = "Apr";
var displayYear = "2010";
var prettyMonths = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
var $D=document,$=function(n){return document.getElementById(n)};
var trackerVal,curYear = -1,curMonth = -1;
var yearTracker,monthTracker;
function showTrackers(val) {
  if (val===trackerVal) return;
  var $ipp=$("wm-ipp");
  var $y=$("displayYearEl"),$m=$("displayMonthEl"),$d=$("displayDayEl");
  if (val) {
    $ipp.className="hi";
  } else {
    $ipp.className="";
    $y.innerHTML=displayYear;$m.innerHTML=displayMonth;$d.innerHTML=displayDay;
  }
  yearTracker.style.display=val?"inline":"none";
  monthTracker.style.display=val?"inline":"none";
  trackerVal = val;
}
function trackMouseMove(event,element) {
  var eventX = getEventX(event);
  var elementX = getElementX(element);
  var xOff = Math.min(Math.max(0, eventX - elementX),imgWidth);
  var monthOff = xOff % yearImgWidth;

  var year = Math.floor(xOff / yearImgWidth);
  var monthOfYear = Math.min(11,Math.floor(monthOff / monthImgWidth));
  // 1 extra border pixel at the left edge of the year:
  var month = (year * 12) + monthOfYear;
  var day = monthOff % 2==1?15:1;
  var dateString = zeroPad(year + firstYear) + zeroPad(monthOfYear+1,2) +
    zeroPad(day,2) + "000000";

  $("displayYearEl").innerHTML=year+firstYear;
  $("displayMonthEl").innerHTML=prettyMonths[monthOfYear];
  // looks too jarring when it changes..
  //$("displayDayEl").innerHTML=zeroPad(day,2);
  var url = wbPrefix + dateString + '/' +  wbCurrentUrl;
  $("wm-graph-anchor").href=url;

  if(curYear != year) {
    var yrOff = year * yearImgWidth;
    yearTracker.style.left = yrOff + "px";
    curYear = year;
  }
  if(curMonth != month) {
    var mtOff = year + (month * monthImgWidth) + 1;
    monthTracker.style.left = mtOff + "px";
    curMonth = month;
  }
}
function hideToolbar() {
  $("wm-ipp").style.display="none";
}
function bootstrap() {
  var $spk=$("wm-ipp-sparkline");
  yearTracker=$D.createElement('div');
  yearTracker.className='yt';
  with(yearTracker.style){
    display='none';width=yearImgWidth+"px";height=imgHeight+"px";
  }
  monthTracker=$D.createElement('div');
  monthTracker.className='mt';
  with(monthTracker.style){
    display='none';width=monthImgWidth+"px";height=imgHeight+"px";
  }
  $spk.appendChild(yearTracker);
  $spk.appendChild(monthTracker);

  var $ipp=$("wm-ipp");
  $ipp&&disclaimElement($ipp);
}
return{st:showTrackers,mv:trackMouseMove,h:hideToolbar,bt:bootstrap};
})(500, 27, 25, 2);//]]>
</script>
<style type="text/css">
body {
  margin-top:0 !important;
  padding-top:0 !important;
  min-width:800px !important;
}
</style>
<div id="wm-ipp" lang="en" style="display:none;">

<div style="position:fixed;left:0;top:0;width:100%!important">
<div id="wm-ipp-inside">
   <table style="width:100%;"><tbody><tr>
   <td id="wm-logo">
       <a href="/web/" title="Wayback Machine home page"><img src="/static/images/toolbar/wayback-toolbar-logo.png" alt="Wayback Machine" width="110" height="39" border="0" /></a>
   </td>
   <td class="c">
       <table style="margin:0 auto;"><tbody><tr>
       <td class="u" colspan="2">
       <form target="_top" method="get" action="/web/form-submit.jsp" name="wmtb" id="wmtb"><input type="text" name="url" id="wmtbURL" value="http://www.gates.esuk.co.uk/buy.aspx" style="width:400px;" onfocus="this.focus();this.select();" /><input type="hidden" name="type" value="replay" /><input type="hidden" name="date" value="20100421073852" /><input type="submit" value="Go" /><span id="wm_tb_options" style="display:block;"></span></form>
       </td>
       <td class="n" rowspan="2">
           <table><tbody>
           <!-- NEXT/PREV MONTH NAV AND MONTH INDICATOR -->
           <tr class="m">
           	<td class="b" nowrap="nowrap">
		
		    <a href="/web/20100317210058/http://www.gates.esuk.co.uk/buy.aspx" title="17 Mar 2010">MAR</a>
		
		</td>
		<td class="c" id="displayMonthEl" title="You are here: 7:38:52 Apr 21, 2010">APR</td>
		<td class="f" nowrap="nowrap">
		
		    May
		
                </td>
	    </tr>
           <!-- NEXT/PREV CAPTURE NAV AND DAY OF MONTH INDICATOR -->
           <tr class="d">
               <td class="b" nowrap="nowrap">
               
                   <a href="/web/20100317210058/http://www.gates.esuk.co.uk/buy.aspx" title="21:00:58 Mar 17, 2010"><img src="/static/images/toolbar/wm_tb_prv_on.png" alt="Previous capture" width="14" height="16" border="0" /></a>
               
               </td>
               <td class="c" id="displayDayEl" style="width:34px;font-size:24px;" title="You are here: 7:38:52 Apr 21, 2010">21</td>
	       <td class="f" nowrap="nowrap">
               
                   <img src="/static/images/toolbar/wm_tb_nxt_off.png" alt="Next capture" width="14" height="16" border="0"/>
               
	       </td>
           </tr>
           <!-- NEXT/PREV YEAR NAV AND YEAR INDICATOR -->
           <tr class="y">
	       <td class="b" nowrap="nowrap">
               
                   <a href="/web/20090408002144/http://www.gates.esuk.co.uk/buy.aspx" title="8 Apr 2009"><strong>2009</strong></a>
               
               </td>
               <td class="c" id="displayYearEl" title="You are here: 7:38:52 Apr 21, 2010">2010</td>
	       <td class="f" nowrap="nowrap">
               
                   2011
               
	       </td>
           </tr>
           </tbody></table>
       </td>
       </tr>
       <tr>
       <td class="s">
           <a class="t" href="/web/20100421073852*/http://www.gates.esuk.co.uk/buy.aspx" title="See a list of every capture for this URL">9 captures</a>
           <div class="r" title="Timespan for captures of this URL">8 Apr 09 - 21 Apr 10</div>
       </td>
       <td class="k">
       <a href="" id="wm-graph-anchor">
       <div id="wm-ipp-sparkline" title="Explore captures for this URL">
	 <img id="sparklineImgId" alt="sparklines"
		 onmouseover="__wm.st(1)" onmouseout="__wm.st(0)"
		 onmousemove="__wm.mv(event,this)"
		 width="500"
		 height="27"
		 border="0"
		 src="/web/jsp/graph.jsp?graphdata=500_27_1996:-1:000000000000_1997:-1:000000000000_1998:-1:000000000000_1999:-1:000000000000_2000:-1:000000000000_2001:-1:000000000000_2002:-1:000000000000_2003:-1:000000000000_2004:-1:000000000000_2005:-1:000000000000_2006:-1:000000000000_2007:-1:000000000000_2008:-1:000000000000_2009:-1:000101011001_2010:3:111100000000_2011:-1:000000000000_2012:-1:000000000000_2013:-1:000000000000_2014:-1:000000000000_2015:-1:000000000000" />
       </div>
       </a>
       </td>
       </tr></tbody></table>
   </td>
   <td class="r">
       <a href="#close" onclick="__wm.h();return false;" style="background-image:url(/static/images/toolbar/wm_tb_close.png);top:5px;" title="Close the toolbar">Close</a>
       <a href="http://faq.web.archive.org/" style="background-image:url(/static/images/toolbar/wm_tb_help.png);bottom:5px;" title="Get some help using the Wayback Machine">Help</a>
   </td>
   </tr></tbody></table>
</div>
</div>
</div>
<script type="text/javascript">__wm.bt();</script>
<!-- END WAYBACK TOOLBAR INSERT -->

    <form name="form1" method="post" action="buy.aspx" id="form1">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKMTUzNTE4NDkxMw9kFgICAw9kFgYCFw88KwAJAQAPFgQeCERhdGFLZXlzFgAeC18hSXRlbUNvdW50AgdkFg5mD2QWBAIBDxYCHgV0aXRsZQUJQmF0dGVyc2VhZAICDxUBCUJhdHRlcnNlYWQCAQ9kFgQCAQ8WAh8CBQxDYW5hZGEgV2F0ZXJkAgIPFQEMQ2FuYWRhIFdhdGVyZAICD2QWBAIBDxYCHwIFEEVhc3QgRmluY2hsZXkgTjJkAgIPFQEQRWFzdCBGaW5jaGxleSBOMmQCAw9kFgQCAQ8WAh8CBQtGaW5jaGxleSBOM2QCAg8VAQtGaW5jaGxleSBOM2QCBA9kFgQCAQ8WAh8CBQtIaWdoZ2F0ZSBONmQCAg8VAQtIaWdoZ2F0ZSBONmQCBQ9kFgQCAQ8WAh8CBQtSb3RoZXJoaXRoZWQCAg8VAQtSb3RoZXJoaXRoZWQCBg9kFgQCAQ8WAh8CBQxTdXJyZXkgUXVheXNkAgIPFQEMU3VycmV5IFF1YXlzZAIZDzwrAAkBAA8WBB8AFgAfAQIIZBYQZg9kFgQCAQ8WAh8CBQJOMmQCAg8VAQJOMmQCAQ9kFgQCAQ8WAh8CBQJOM2QCAg8VAQJOM2QCAg9kFgQCAQ8WAh8CBQJONmQCAg8VAQJONmQCAw9kFgQCAQ8WAh8CBQRTRTE2ZAICDxUBBFNFMTZkAgQPZBYEAgEPFgIfAgUDU0U1ZAICDxUBA1NFNWQCBQ9kFgQCAQ8WAh8CBQNTRThkAgIPFQEDU0U4ZAIGD2QWBAIBDxYCHwIFBFNXMTFkAgIPFQEEU1cxMWQCBw9kFgQCAQ8WAh8CBQNTVzRkAgIPFQEDU1c0ZAIdD2QWBAIBDxAPZBYCHghvbmNoYW5nZQUWcmV0dXJuIGNoYW5nZVZhbHVlcygpO2RkZAIDDxAPFgYeDURhdGFUZXh0RmllbGQFBEFyZWEeDkRhdGFWYWx1ZUZpZWxkBQRBcmVhHgtfIURhdGFCb3VuZGdkEBUIA0FueQlCYXR0ZXJzZWEMQ2FuYWRhIFdhdGVyEEVhc3QgRmluY2hsZXkgTjILRmluY2hsZXkgTjMLSGlnaGdhdGUgTjYLUm90aGVyaGl0aGUMU3VycmV5IFF1YXlzFQgDQW55CUJhdHRlcnNlYQxDYW5hZGEgV2F0ZXIQRWFzdCBGaW5jaGxleSBOMgtGaW5jaGxleSBOMwtIaWdoZ2F0ZSBONgtSb3RoZXJoaXRoZQxTdXJyZXkgUXVheXMUKwMIZ2dnZ2dnZ2dkZBgBBR5fX0NvbnRyb2xzUmVxdWlyZVBvc3RCYWNrS2V5X18WFwUKc3RyZWV0dmlldwUIY2hrSG91c2UFB3JkYkFyZWEFDGNoa0FwYXJ0bWVudAULcmRiUG9zdGNvZGUFDGNoa1NlbGVjdEFsbAUbUmVwZWF0ZXIxJGN0bDAwJGNoa0xvY2F0aW9uBRtSZXBlYXRlcjEkY3RsMDEkY2hrTG9jYXRpb24FG1JlcGVhdGVyMSRjdGwwMiRjaGtMb2NhdGlvbgUbUmVwZWF0ZXIxJGN0bDAzJGNoa0xvY2F0aW9uBRtSZXBlYXRlcjEkY3RsMDQkY2hrTG9jYXRpb24FG1JlcGVhdGVyMSRjdGwwNSRjaGtMb2NhdGlvbgUbUmVwZWF0ZXIxJGN0bDA2JGNoa0xvY2F0aW9uBRtSZXBlYXRlcjIkY3RsMDAkY2hrUG9zdGNvZGUFG1JlcGVhdGVyMiRjdGwwMSRjaGtQb3N0Y29kZQUbUmVwZWF0ZXIyJGN0bDAyJGNoa1Bvc3Rjb2RlBRtSZXBlYXRlcjIkY3RsMDMkY2hrUG9zdGNvZGUFG1JlcGVhdGVyMiRjdGwwNCRjaGtQb3N0Y29kZQUbUmVwZWF0ZXIyJGN0bDA1JGNoa1Bvc3Rjb2RlBRtSZXBlYXRlcjIkY3RsMDYkY2hrUG9zdGNvZGUFG1JlcGVhdGVyMiRjdGwwNyRjaGtQb3N0Y29kZQUGY2hrTWFwBRpRdWlja1NlYXJjaDEkcmVudFNob3dPbk1hcNQkiOXHifDJpftgV/GwHCwO7zlf" />

    <div id="wrapper"><table width="100%" height="171" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="right" valign="middle">
        
<link href="/web/20100421073852cs_/http://www.gates.esuk.co.uk/class/class.css" rel="stylesheet" type="text/css" />
<table border="0" cellpadding="0" cellspacing="0" width="100%" >
<tr>
    <td>
    <span class="logo"><img src="images/Xample.jpg" width="236" height="63" /></span>
      <div class="call">
        <div style="margin-top:55px; height:22; color:#FFFFFF; font-size:12px; font-weight:bold; padding-right:14px; text-decoration:underline; cursor:pointer; "><span onclick="window.open('/web/20100421073852/http://livechat.boldchat.com/aid/2178106990504207724/bc.chat?cwdid=848434019943815447&amp;wdid=3507451889334081925&amp;vr=&amp;vn=&amp;vi=&amp;ve=&amp;vp=&amp;iq=&amp;url=','','scrollbars=yes,width=500,height=500')" >Chat live</span><img src="images/chat_live.gif" style="padding-top:3px" width="28" height="22" onclick="window.open('/web/20100421073852/http://livechat.boldchat.com/aid/2178106990504207724/bc.chat?cwdid=848434019943815447&amp;wdid=3507451889334081925&amp;vr=&amp;vn=&amp;vi=&amp;ve=&amp;vp=&amp;iq=&amp;url=','','scrollbars=yes,width=500,height=500')" /></div>
      </div>
      <div class="topmenu">
        <table height="30">
          <tr>
            <td valign="bottom" style="color:#790000; font-size:14px;"><a href="index.aspx" class="topmenu_font">&nbsp;&nbsp;&nbsp;Home</a>|<a href="buy.aspx" class="topmenu_font">&nbsp;&nbsp;&nbsp;Buy</a>|<a href="rent.aspx" class="topmenu_font">&nbsp;&nbsp;&nbsp;Rent</a>|<a href="Mortgage.aspx" class="topmenu_font">&nbsp;&nbsp;&nbsp;Mortgages</a>|<a href="vendors.aspx" class="topmenu_font" >&nbsp;&nbsp;&nbsp;Vendors</a>|<a href="landlords.aspx" class="topmenu_font">&nbsp;&nbsp;&nbsp;Landlords</a>|<a href="aboutus.aspx" class="topmenu_font">&nbsp;&nbsp;&nbsp;About us</a>|<a href="contactus.aspx" class="topmenu_font">&nbsp;&nbsp;&nbsp;Contact us</a>|<a href="register.aspx" class="topmenu_font">&nbsp;&nbsp;&nbsp;Register</a>|<a href="favourites.aspx" class="topmenu_font">&nbsp;&nbsp;&nbsp;My favourites</a> </td>
          </tr>
        </table>
      </div>
    </td>
</tr>

</table>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="26%" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td colspan="2"><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="/web/20100421073852oe_/http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="702" height="280">
              <param name="movie" value="banner.swf" />
              <param name="quality" value="high" />
              <param name="wmode" value="transparent" />  
<embed src="banner.swf" wmode="transparent" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="702" height="280"></embed>
            </object></td>
            <td width="0%">&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td height="24" colspan="2" valign="top" style="border-bottom:1px dotted #666666"><span class="titel">&nbsp;Buy</span></td>
            <td>&nbsp;</td>
          </tr>
          <tr><td>&nbsp;</td></tr>
          <tr>
            <td width="32%" rowspan="2"  align="left" valign="top">
            <table width="47%" height="254" border="0" align="left" cellpadding="0" cellspacing="0">
             <tr>
                <td height="130" align="center" valign="top"><input type="image" name="streetview" id="streetview" src="images/streetview.jpg" border="0" /></td>
              </tr>
              <tr>
                <td height="121" align="left" valign="top"><img src="images/index2_16.jpg" /></td>
                  </tr>
              <tr>
                <td height="55"><img src="images/register.jpg" width="217" height="114" border="0" style="cursor:pointer;" onclick="document.location='register.aspx';" /></td>
                  </tr>
              <tr>
                <td height="19"></td>
                  </tr>
            </table></td>
            <td height="24" align="left" valign="top" >
            
            <div>
            <table border="0" cellpadding="2" cellspacing="0">
            <tr>
                <td align="left" valign="middle" style="font-size:13px; color:#08175F;">Choose Language </td>
                <td width="20" valign="bottom" align="left"><span onclick="location.href='http://' + 'gates.esuk.co.uk/buy.aspx';return false;" style="cursor:pointer"><img title="Ingl�s" border="0" src="images/flags/gb.gif" height="16" width="20"></span></td>
                <td width="20" valign="bottom" align="left"><span onclick="location.href='http://' + 'translate.google.com/translate_c?langpair=en|de&amp;u=/web/20100421073852/http://gates.esuk.co.uk/buy.aspx';return false;" style="cursor:pointer" ><img title="Alem�n" border="0" src="images/flags/de.gif" alt="Alem�n" height="16" width="20"></span></td>
                <td width="20" valign="bottom" align="left"><span onclick="location.href='http://' +'translate.google.com/translate_c?langpair=en|fr&amp;u=/web/20100421073852/http://gates.esuk.co.uk/buy.aspx';return false;" style="cursor:pointer"><img title="Franc�s" border="0" src="images/flags/fr.gif" alt="Franc�s" height="16" width="20"></span></td>
                <td width="20" valign="bottom" align="left"><span onclick="location.href='http://' +'translate.google.com/translate_c?langpair=en|es&amp;u=/web/20100421073852/http://gates.esuk.co.uk/buy.aspx';return false;" style="cursor:pointer"><img title="Espa�ol" border="0" src="images/flags/es.gif" alt="Espa�ol" height="16" width="20"></span></td>
                <td width="20" valign="bottom" align="left"><span onclick="location.href='http://' + 'translate.google.com/translate_c?langpair=en|pt&amp;u=/web/20100421073852/http://gates.esuk.co.uk/buy.aspx';return false;" style="cursor:pointer"><img title="Portugu�s" border="0" src="images/flags/pt.gif" alt="Portugu�s" height="16" width="20"></span></td>
                <td width="20" valign="bottom" align="left"><span onclick="location.href='http://' +'translate.google.com/translate_c?langpair=en|nl&amp;u=/web/20100421073852/http://gates.esuk.co.uk/buy.aspx';return false;" style="cursor:pointer"><img title="Holand�s" border="0" src="images/flags/nl.gif" alt="Holand�s" height="16" width="20"></span></td>
                <td width="20" valign="bottom" align="left"><span onclick="location.href='http://' +'translate.google.com/translate_c?langpair=en|zh&amp;u=/web/20100421073852/http://gates.esuk.co.uk/buy.aspx';return false;" style="cursor:pointer"><img title="Chino" border="0" src="images/flags/ch.jpg" alt="Chino" height="16" width="20"></span></td>
	            <td width="20" valign="bottom" align="left"><span onclick="location.href='http://' +'translate.google.com/translate_c?langpair=en|it&amp;u=/web/20100421073852/http://gates.esuk.co.uk/buy.aspx';return false;" style="cursor:pointer"><img title="Italiano" border="0" src="images/flags/it.gif" alt="Italiano" height="16" width="20"></span></td>
                <td align="left" valign="bottom" width="20"><span onclick="location.href='http://' +'translate.google.com/translate_c?langpair=en|ar&amp;u=/web/20100421073852/http://gates.esuk.co.uk/buy.aspx';return false;" style="cursor:pointer"><img title="Arabic" border="0" src="images/flags/ar.gif" alt="Arabic" height="16" width="20"></span></td>
                <td align="left" valign="bottom" style="padding-left:2px;padding-right:2px;"><span onclick="location.href='http://' +'translate.google.com/translate_c?langpair=en|iw&amp;u=/web/20100421073852/http://gates.esuk.co.uk/buy.aspx';return false;" style="cursor:pointer">Hebrew</span></td>
                <td align="left" valign="bottom" style="padding-left:2px;padding-right:2px;"><span onclick="alert('This translation is not yet posible but is coming soon.');return false;" style="cursor:pointer">Urdu</span></td>
                <td align="left" valign="bottom" style="padding-left:2px;padding-right:2px;"><span onclick="alert('This translation is not yet posible but is coming soon.');return false;" style="cursor:pointer">Bengal</span></td>
	        </tr>
	        </table>
        </div><br />
            <div class="text_bold">Properties for sale</div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td width="68%" height="360" align="left" valign="top">
            <table width="99%" height="355" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="25%" height="25" bgcolor="#F4F4F4" style=" border-bottom:#CCCCCC dotted 1px; color:#770000;">&nbsp;Price:</td>
                      <td width="25%" bgcolor="#F4F4F4" style=" border-bottom:#CCCCCC dotted 1px; color:#770000;">Bedrooms:</td>
                      <td width="25%" bgcolor="#F4F4F4" style=" border-bottom:#CCCCCC dotted 1px; color:#770000;">Property type:</td>
                      <td width="25%" bgcolor="#F4F4F4" style=" border-bottom:#CCCCCC dotted 1px; color:#770000;">Location(s):</td>
                    </tr>
                    <tr>
                      <td height="36"><label>
                        <select name="lstminprice" id="lstminprice" style="width:110px;">
	<option selected="selected" value="0">min</option>
	<option value="50000">&#163;50k</option>
	<option value="60000">&#163;60k</option>
	<option value="70000">&#163;70k</option>
	<option value="80000">&#163;80k</option>
	<option value="90000">&#163;90k</option>
	<option value="100000">&#163;100k</option>
	<option value="150000">&#163;150k</option>
	<option value="200000">&#163;200k</option>
	<option value="250000">&#163;250k</option>
	<option value="300000">&#163;300k</option>
	<option value="350000">&#163;350k</option>
	<option value="450000">&#163;450k</option>
	<option value="500000">&#163;500k</option>
	<option value="550000">&#163;550k</option>
	<option value="600000">&#163;600k</option>
	<option value="650000">&#163;650k</option>
	<option value="700000">&#163;700k</option>
	<option value="750000">&#163;750k</option>
	<option value="800000">&#163;800k</option>
	<option value="850000">&#163;850k</option>
	<option value="900000">&#163;900k</option>
	<option value="900000">&#163;900k</option>
	<option value="1000000">&#163;1m</option>
</select>
                        </label>                      </td>
                      <td><select name="lstminbedrooms" id="lstminbedrooms" style="width:110px;">
	<option selected="selected" value="0">studio</option>
	<option value="1">1</option>
	<option value="2">2</option>
	<option value="3">3</option>
	<option value="4">4</option>
	<option value="5">5</option>
	<option value="6">6</option>
	<option value="7">7</option>
</select></td>
                      <td><input name="chkHouse" type="checkbox" id="chkHouse" checked="checked" />
                          <label style="font-size:13px; font-weight:100; color:#333333">House</label>                      </td>
                      <td><input value="rdbArea" name="abcd" type="radio" id="rdbArea" onclick="document.getElementById('ares').style.display='block'; document.getElementById('postcod').style.display='none'; document.getElementById('rdbPostcode').checked=false;" checked="checked" />
                          <label style="font-size:13px; font-weight:100; color:#333333">Location</label>                      </td>
                    </tr>
                    <tr>
                      <td height="29"><select name="lstmaxprice" id="lstmaxprice" style="width:110px;">
	<option selected="selected" value="0">max</option>
	<option value="60000">&#163;60k</option>
	<option value="70000">&#163;70k</option>
	<option value="80000">&#163;80k</option>
	<option value="90000">&#163;90k</option>
	<option value="100000">&#163;100k</option>
	<option value="150000">&#163;150k</option>
	<option value="200000">&#163;200k</option>
	<option value="250000">&#163;250k</option>
	<option value="300000">&#163;300k</option>
	<option value="350000">&#163;350k</option>
	<option value="450000">&#163;450k</option>
	<option value="500000">&#163;500k</option>
	<option value="550000">&#163;550k</option>
	<option value="600000">&#163;600k</option>
	<option value="650000">&#163;650k</option>
	<option value="700000">&#163;700k</option>
	<option value="750000">&#163;750k</option>
	<option value="800000">&#163;800k</option>
	<option value="850000">&#163;850k</option>
	<option value="900000">&#163;900k</option>
	<option value="900000">&#163;900k</option>
	<option value="1000000">&#163;1m</option>
</select></td>
                      <td><select name="lstmaxbedrooms" id="lstmaxbedrooms" style="width:110px;">
	<option selected="selected" value="99">max</option>
	<option value="0">studio</option>
	<option value="1">1</option>
	<option value="2">2</option>
	<option value="3">3</option>
	<option value="4">4</option>
	<option value="5">5</option>
	<option value="6">6</option>
	<option value="99">7+</option>
</select></td>
                      <td style="font-size:13px; font-weight:100; color:#333333"><input name="chkApartment" type="checkbox" id="chkApartment" checked="checked" />
                        Apartment </td>
                      <td style="font-size:13px; font-weight:100; color:#333333"><input value="rdbPostcode" name="abcd" type="radio" id="rdbPostcode" onclick="document.getElementById('postcod').style.display='block'; document.getElementById('ares').style.display='none'; document.getElementById('rdbArea').checked='';" />
                        Postcode</td>
                    </tr>
                    <tr>
                      <td height="9" colspan="4" style="font-size:13px; font-weight:100; color:#333333"></td>
                    </tr>
                    <tr>
                      <td height="24" style="font-size:13px; font-weight:100; color:#333333"><label>
                        <input name="chkSelectAll" type="checkbox" id="chkSelectAll" onclick="setAllChecked(this);" />
                        </label>
                        Select All </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td height="30" colspan="4"><div id="ares">
                          <table width="98%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td height="23" bgcolor="#F4F4F4" style="font-size:13px; font-weight:100; color:#333333"><span style="margin-top:20px; color:#770000 "><strong>Location</strong></span></td>
                              <td bgcolor="#F4F4F4" style="font-size:13px; font-weight:100; color:#333333">&nbsp;</td>
                              <td bgcolor="#F4F4F4" style="font-size:13px; font-weight:100; color:#333333">&nbsp;</td>
                            </tr>
                          </table>
                        <table id="Repeater1" cellspacing="0" border="0">
	<tr>
		<td>
                              <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td style="padding-right:20px; font-size:13px; font-weight:100; color:#333333"><input name="Repeater1$ctl00$chkLocation" type="checkbox" id="Repeater1_ctl00_chkLocation" onclick="javascript:setLocations();" title="Battersea" />
                                      <span class="contact_t">Battersea</span></td>
                                </tr>
                              </table>
                            </td><td>
                              <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td style="padding-right:20px; font-size:13px; font-weight:100; color:#333333"><input name="Repeater1$ctl01$chkLocation" type="checkbox" id="Repeater1_ctl01_chkLocation" onclick="javascript:setLocations();" title="Canada Water" />
                                      <span class="contact_t">Canada Water</span></td>
                                </tr>
                              </table>
                            </td>
	</tr><tr>
		<td>
                              <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td style="padding-right:20px; font-size:13px; font-weight:100; color:#333333"><input name="Repeater1$ctl02$chkLocation" type="checkbox" id="Repeater1_ctl02_chkLocation" onclick="javascript:setLocations();" title="East Finchley N2" />
                                      <span class="contact_t">East Finchley N2</span></td>
                                </tr>
                              </table>
                            </td><td>
                              <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td style="padding-right:20px; font-size:13px; font-weight:100; color:#333333"><input name="Repeater1$ctl03$chkLocation" type="checkbox" id="Repeater1_ctl03_chkLocation" onclick="javascript:setLocations();" title="Finchley N3" />
                                      <span class="contact_t">Finchley N3</span></td>
                                </tr>
                              </table>
                            </td>
	</tr><tr>
		<td>
                              <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td style="padding-right:20px; font-size:13px; font-weight:100; color:#333333"><input name="Repeater1$ctl04$chkLocation" type="checkbox" id="Repeater1_ctl04_chkLocation" onclick="javascript:setLocations();" title="Highgate N6" />
                                      <span class="contact_t">Highgate N6</span></td>
                                </tr>
                              </table>
                            </td><td>
                              <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td style="padding-right:20px; font-size:13px; font-weight:100; color:#333333"><input name="Repeater1$ctl05$chkLocation" type="checkbox" id="Repeater1_ctl05_chkLocation" onclick="javascript:setLocations();" title="Rotherhithe" />
                                      <span class="contact_t">Rotherhithe</span></td>
                                </tr>
                              </table>
                            </td>
	</tr><tr>
		<td>
                              <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td style="padding-right:20px; font-size:13px; font-weight:100; color:#333333"><input name="Repeater1$ctl06$chkLocation" type="checkbox" id="Repeater1_ctl06_chkLocation" onclick="javascript:setLocations();" title="Surrey Quays" />
                                      <span class="contact_t">Surrey Quays</span></td>
                                </tr>
                              </table>
                            </td><td></td>
	</tr>
</table>
                        </div>
                          <div id="postcod" style="display:none;">
                            <table width="98%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td height="26" bgcolor="#F4F4F4" style="margin-top:20px; font-size:13px; color:#EEA701 "><strong>Postcode</strong></td>
                                <td bgcolor="#F4F4F4" class="contact_t">&nbsp;</td>
                                <td bgcolor="#F4F4F4" class="contact_t">&nbsp;</td>
                              </tr>
                            </table>
                            <table id="Repeater2" cellspacing="0" border="0">
	<tr>
		<td>
                                <table border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td class="contact_t" style="padding-right:20px;"><input name="Repeater2$ctl00$chkPostcode" type="checkbox" id="Repeater2_ctl00_chkPostcode" onclick="javascript:setPostcodes();" title="N2" />
                                        N2</td>
                                  </tr>
                                </table>
                              </td><td>
                                <table border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td class="contact_t" style="padding-right:20px;"><input name="Repeater2$ctl01$chkPostcode" type="checkbox" id="Repeater2_ctl01_chkPostcode" onclick="javascript:setPostcodes();" title="N3" />
                                        N3</td>
                                  </tr>
                                </table>
                              </td><td>
                                <table border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td class="contact_t" style="padding-right:20px;"><input name="Repeater2$ctl02$chkPostcode" type="checkbox" id="Repeater2_ctl02_chkPostcode" onclick="javascript:setPostcodes();" title="N6" />
                                        N6</td>
                                  </tr>
                                </table>
                              </td><td>
                                <table border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td class="contact_t" style="padding-right:20px;"><input name="Repeater2$ctl03$chkPostcode" type="checkbox" id="Repeater2_ctl03_chkPostcode" onclick="javascript:setPostcodes();" title="SE16" />
                                        SE16</td>
                                  </tr>
                                </table>
                              </td><td>
                                <table border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td class="contact_t" style="padding-right:20px;"><input name="Repeater2$ctl04$chkPostcode" type="checkbox" id="Repeater2_ctl04_chkPostcode" onclick="javascript:setPostcodes();" title="SE5" />
                                        SE5</td>
                                  </tr>
                                </table>
                              </td><td>
                                <table border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td class="contact_t" style="padding-right:20px;"><input name="Repeater2$ctl05$chkPostcode" type="checkbox" id="Repeater2_ctl05_chkPostcode" onclick="javascript:setPostcodes();" title="SE8" />
                                        SE8</td>
                                  </tr>
                                </table>
                              </td>
	</tr><tr>
		<td>
                                <table border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td class="contact_t" style="padding-right:20px;"><input name="Repeater2$ctl06$chkPostcode" type="checkbox" id="Repeater2_ctl06_chkPostcode" onclick="javascript:setPostcodes();" title="SW11" />
                                        SW11</td>
                                  </tr>
                                </table>
                              </td><td>
                                <table border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td class="contact_t" style="padding-right:20px;"><input name="Repeater2$ctl07$chkPostcode" type="checkbox" id="Repeater2_ctl07_chkPostcode" onclick="javascript:setPostcodes();" title="SW4" />
                                        SW4</td>
                                  </tr>
                                </table>
                              </td><td></td><td></td><td></td><td></td>
	</tr>
</table>
                        </div></td>
                    </tr>
                    <tr>
                      <td height="7" colspan="4"></td>
                    </tr>
                </table></td>
              </tr>
              <tr>
                <td height="28"><input name="chkMap" type="checkbox" id="chkMap" />
                    <span class="contact_t">Search on map </span></td>
              </tr>
              <tr>
                <td height="32" align="left" style="cursor:pointer;" valign="top"><img src="images/search.jpg"  width="66" height="26" onclick="checkAnyChecked();" /></td>
              </tr>
            </table></td>
            <td>&nbsp;</td>
          </tr>
        </table></td>
        <td width="74%" valign="top">
        <div id="leftcol">
            
<script type="text/javascript">
<!--
function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { 
//v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function roll_over(img_name, img_src)
   {
 
   document[img_name].src = img_src;
   }

//-->
</script>
<script type="text/javascript" language="javascript">
function gotosaerch()
{

var SearchProp = document.getElementById("QuickSearch1_Button3");
SearchProp.click(); 

}

</script>
<script type="text/javascript" language="javascript">
function changeValues()
{
   var obj1 = document.getElementById("QuickSearch1_PropType").value;
       
    if(obj1 == '1')
        {
            var ddlOrders = document.getElementById("QuickSearch1_rentMinPrice");
            for (var count = ddlOrders.options.length-1; count >-1; count--)
                {
                    ddlOrders.options[count] = null;
                }
               
            text = 'Min';
            text1 = '0';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 50k';
            text1 = '50000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 60k';
            text1 = '60000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 70k';
            text1 = '70000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 80k';
            text1 = '80000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 90k';
            text1 = '90000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 100k';
            text1 = '100000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 150k';
            text1 = '150000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 200k';
            text1 = '200000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 250k';
            text1 = '250000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
              
             text = '\u00A3 300k';
            text1 = '300000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem; 
             
             text = '\u00A3 350k';
            text1 = '350000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;  
            
             text = '\u00A3 450k';
            text1 = '450000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem; 
             
             text = '\u00A3 500k';
            text1 = '500000';
            listItem = new Option(text, text1, false, false)    
            ddlOrders.options[ddlOrders.length] = listItem;  
            
             text = '\u00A3 550k';
            text1 = '550000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 600k';
            text1 = '600000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 650k';
            text1 = '650000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 700k';
            text1 = '700000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 750k';
            text1 = '750000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 800k';
            text1 = '800000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 850k';
            text1 = '850000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
               text = '\u00A3 900k';
            text1 = '900000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
             text = '\u00A3 1m';
            text1 = '1000000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            
            
            var ddlOrders2 = document.getElementById("QuickSearch1_rentMaxPrice");
            for (var count = ddlOrders2.options.length-1; count >-1; count--)
                {
                    ddlOrders2.options[count] = null;
                }
                
            text = 'Max';
            text1 = '0';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
             text = '\u00A3 50k';
            text1 = '50000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 60k';
            text1 = '60000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 70k';
            text1 = '70000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 80k';
            text1 = '80000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 90k';
            text1 = '90000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 100k';
            text1 = '100000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 150k';
            text1 = '150000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 200k';
            text1 = '200000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 250k';
            text1 = '250000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
              
             text = '\u00A3 300k';
            text1 = '300000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem; 
             
             text = '\u00A3 350k';
            text1 = '350000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;  
            
             text = '\u00A3 450k';
            text1 = '450000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem; 
             
             text = '\u00A3 500k';
            text1 = '500000';
            listItem = new Option(text, text1, false, false)    
            ddlOrders2.options[ddlOrders2.length] = listItem;  
            
             text = '\u00A3 550k';
            text1 = '550000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 600k';
            text1 = '600000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
             text = '\u00A3 650k';
            text1 = '650000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
             text = '\u00A3 700k';
            text1 = '700000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
             text = '\u00A3 750k';
            text1 = '750000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
             text = '\u00A3 800k';
            text1 = '800000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
             text = '\u00A3 850k';
            text1 = '850000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
               text = '\u00A3 900k';
            text1 = '900000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
             text = '\u00A3 1m';
            text1 = '1000000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            GetBuyLocations();
                   
        }
        else if(obj1 == '2')
        {
            var ddlOrders = document.getElementById("QuickSearch1_rentMinPrice");
            for (var count = ddlOrders.options.length-1; count >-1; count--)
            {
                    ddlOrders.options[count] = null;
            }
            text = 'Min';
            text1 = '0';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 100 per week';
            text1 = '100';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 200 per week';
            text1 = '200';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 300 per week';
            text1 = '300';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 400 per week';
            text1 = '400';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            text = '\u00A3 500 per week';
            text1 = '500';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            text = '\u00A3 600 per week';
            text1 = '600';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            text = '\u00A3 750 per week';
            text1 = '750';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 1000 per week';
            text1 = '1000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            
            var ddlOrders2 = document.getElementById("QuickSearch1_rentMaxPrice");
            for (var count = ddlOrders2.options.length-1; count >-1; count--)
            {
                    ddlOrders2.options[count] = null;
            }
            text = 'Max';
            text1 = '0';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 100 per week';
            text1 = '100';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 200 per week';
            text1 = '200';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 300 per week';
            text1 = '300';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 400 per week';
            text1 = '400';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 500 per week';
            text1 = '500';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 600 per week';
            text1 = '600';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 750 per week';
            text1 = '750';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 1000 per week';
            text1 = '1000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            GetRentLocations();
        }
        else

        {
            var ddlOrders = document.getElementById("QuickSearch1_rentMaxPrice");
            for (var count = ddlOrders.options.length-1; count >-1; count--)
                {
                    ddlOrders.options[count] = null;
                }
           text = 'Max';
            text1 = '0';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 60k';
            text1 = '60000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 70k';
            text1 = '70000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 80k';
            text1 = '30000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 90k';
            text1 = '90000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 100k';
            text1 = '100000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 150k';
            text1 = '150000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 200k';
            text1 = '200000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 250k';
            text1 = '250000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 300k';
            text1 = '300000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem; 
            
             text = '\u00A3 300k';
            text1 = '300000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem; 
             
             text = '\u00A3 350k';
            text1 = '350000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;  
            
             text = '\u00A3 450k';
            text1 = '450000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem; 
             
             text = '\u00A3 500k';
            text1 = '500000';
            listItem = new Option(text, text1, false, false)    
            ddlOrders.options[ddlOrders.length] = listItem;  
            
             text = '\u00A3 550k';
            text1 = '550000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 600k';
            text1 = '600000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 650k';
            text1 = '650000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 700k';
            text1 = '700000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 750k';
            text1 = '750000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 800k';
            text1 = '800000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 850k';
            text1 = '850000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
               text = '\u00A3 900k';
            text1 = '900000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
               text = '\u00A3 1m';
            text1 = '1000000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            GetBuyLocations();
            
            }
}

function GetBuyLocations()
{

            var buyLocations = new Array();
            
            buyLocations = document.getElementById("QuickSearch1_hdnBuyLocations").value.split(',');
            
            var text; 
            var text1;
            var listItem;
            
            var ddlOrders = document.getElementById("QuickSearch1_rentArea");
            
//            for (var count = ddlOrders.options.length-1; count >-1; count--)
//            {
//                ddlOrders.options[count] = null;
//            }

            if(ddlOrders.options.length > 0)
            {
                for(var i = ddlOrders.options.length - 1; i >= 0; i--)
                {
                    ddlOrders.remove(i);
                }
            }
            
            for (var count = 0; count < buyLocations.length; count++)
            {
                text = buyLocations[count];
                text1 =  buyLocations[count];
                listItem = new Option(text, text1, false, false);
                ddlOrders.options[ddlOrders.options.length] = listItem;
            }       
}

function GetRentLocations()
{
            
           var rentLocations = new Array();
            
            rentLocations = document.getElementById("QuickSearch1_hdnRentLocations").value.split(',');
            
            var text; 
            var text1;
            var listItem;
            
            var ddlOrders = document.getElementById("QuickSearch1_rentArea");
            
//            for (var count = ddlOrders.options.length-1; count >-1; count--)
//            {
//                ddlOrders.options[count] = null;
//            }
            
            if(ddlOrders.options.length > 0)
            {
                for(var i = ddlOrders.options.length - 1; i >= 0; i--)
                {
                    ddlOrders.remove(i);
                }
            }
            
            for (var count = 0; count < rentLocations.length; count++)
            {
                text = rentLocations[count];
                text1 =  rentLocations[count];
                listItem = new Option(text, text1, false, false);
                ddlOrders.options[ddlOrders.options.length] = listItem;
            }       
}
</script>
<div >
        <table bgcolor="white" border="0" cellpadding="0" cellspacing="0" width="96%">
                       <tr>
                           <td align="center"  valign="middle">
                               <div  id="searchNew" style="background-color:white">
                                   <div class="text_bold" style="text-align:left; padding-bottom:10px; padding-top:3px; padding-left:7px;">Quick Search</div>
                                   <div style="padding-right: 2px; padding-left: 2px; background-color:;">
                                       <table style="background-color:#ebebeb; padding-left:5px; padding-right:2px; width: 100%;  border:solid 1px #999999" align="left"  border="0" cellpadding="0" cellspacing="0">
                                          
                                           <tr>
                                               <td align="left" class="text_bold" style="height: 16px">
                                               </td>
                                           </tr>
                                           <tr>
                                               <td align="left">
                                                   <table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 5px; width: 100%">
                                                       <tr>
                                                           <td style="width: 68px; font-size:11px; font-family:Arial,Helvetica,sans-serif; color:#666666">
                                                               Type:</td>
                                                           <td style="width: 100px; font-size:11px; font-family:Arial,Helvetica,sans-serif; color:#666666">
                                                               <select name="QuickSearch1$PropType" id="QuickSearch1_PropType" onchange="return changeValues();">
	<option value="1">Buy</option>
	<option value="2">Rent</option>

</select></td>
                                                       </tr>
                                                   </table>
                                               </td>
                                           </tr>
                                           <tr>
                                               <td align="left">
                                                   <table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 5px; width: 100%">
                                                       <tr>
                                                           <td style="width: 68px; font-size:11px; font-family:Arial,Helvetica,sans-serif; color:#666666">
                                                               Location:</td>
                                                           <td style="width: 100px; font-size:11px; font-family:Arial,Helvetica,sans-serif; color:#666666">
                                                               <select name="QuickSearch1$rentArea" id="QuickSearch1_rentArea">
	<option value="Any">Any</option>
	<option value="Battersea">Battersea</option>
	<option value="Canada Water">Canada Water</option>
	<option value="East Finchley N2">East Finchley N2</option>
	<option value="Finchley N3">Finchley N3</option>
	<option value="Highgate N6">Highgate N6</option>
	<option value="Rotherhithe">Rotherhithe</option>
	<option value="Surrey Quays">Surrey Quays</option>

</select></td>
                                                       </tr>
                                                   </table>
                                               </td>
                                           </tr>
                                           <tr>
                                               <td align="left">
                                                   <table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 5px; width: 100%">
                                                       <tr>
                                                           <td style="width: 68px; font-size:11px; font-family:Arial,Helvetica,sans-serif; color:#666666">
                                                               Bedrooms:
                                                           </td>
                                                           <td style="width: 100px; font-size:11px; font-family:Arial,Helvetica,sans-serif; color:#666666">
                                                               <select name="QuickSearch1$rentMinBedrooms" id="QuickSearch1_rentMinBedrooms" style="width: 140px">
	<option selected="selected" value="-1">Any</option>
	<option value="0">Studio</option>
	<option value="1">1</option>
	<option value="2">2</option>
	<option value="3">3</option>
	<option value="4">4</option>
	<option value="99">5+</option>

</select></td>
                                                       </tr>
                                                   </table>
                                               </td>
                                           </tr>
                                           <tr>
                                               <td>
                                                   <table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 5px; width: 100%">
                                                       <tr>
                                                           <td align="left" style="width: 68px; font-size:11px; font-family:Arial,Helvetica,sans-serif; color:#666666">
                                                               Min price:
                                                           </td>
                                                           <td style="width: 100px; font-size:11px; font-family:Arial,Helvetica,sans-serif; color:#666666">
                                                               <select name="QuickSearch1$rentMinPrice" id="QuickSearch1_rentMinPrice" style="width: 140px">
	<option value="0">Min</option>
	<option value="50000">&#163;50k</option>
	<option value="60000">&#163;60k</option>
	<option value="70000">&#163;70k</option>
	<option value="80000">&#163;80k</option>
	<option value="90000">&#163;90k</option>
	<option value="100000">&#163;100k</option>
	<option value="150000">&#163;150k</option>
	<option value="200000">&#163;200k</option>
	<option value="250000">&#163;250k</option>
	<option value="300000">&#163;300k</option>
	<option value="350000">&#163;350k</option>
	<option value="450000">&#163;450k</option>
	<option value="500000">&#163;500k</option>
	<option value="550000">&#163;550k</option>
	<option value="600000">&#163;600k</option>
	<option value="650000">&#163;650k</option>
	<option value="700000">&#163;700k</option>
	<option value="750000">&#163;750k</option>
	<option value="800000">&#163;800k</option>
	<option value="850000">&#163;850k</option>
	<option value="900000">&#163;900k</option>
	<option value="1000000">&#163;1m</option>

</select></td>
                                                       </tr>
                                                   </table>
                                               </td>
                                           </tr>
                                           <tr>
                                               <td align="left">
                                                   <table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 5px; width: 100%">
                                                       <tr>
                                                           <td style="width: 68px; font-size:11px; font-family:Arial,Helvetica,sans-serif; color:#666666">
                                                               Max. Price: 
                                                           </td>
                                                           <td style="width: 100px; font-size:11px; font-family:Arial,Helvetica,sans-serif; color:#666666">
                                                               <select name="QuickSearch1$rentMaxPrice" id="QuickSearch1_rentMaxPrice" style="width: 140px">
	<option value="0">Max</option>
	<option value="50000">&#163;50k</option>
	<option value="60000">&#163;60k</option>
	<option value="70000">&#163;70k</option>
	<option value="80000">&#163;80k</option>
	<option value="90000">&#163;90k</option>
	<option value="100000">&#163;100k</option>
	<option value="150000">&#163;150k</option>
	<option value="200000">&#163;200k</option>
	<option value="250000">&#163;250k</option>
	<option value="300000">&#163;300k</option>
	<option value="350000">&#163;350k</option>
	<option value="450000">&#163;450k</option>
	<option value="500000">&#163;500k</option>
	<option value="550000">&#163;550k</option>
	<option value="600000">&#163;600k</option>
	<option value="650000">&#163;650k</option>
	<option value="700000">&#163;700k</option>
	<option value="750000">&#163;750k</option>
	<option value="800000">&#163;800k</option>
	<option value="850000">&#163;850k</option>
	<option value="900000">&#163;900k</option>
	<option value="1000000">&#163;1m</option>

</select></td>
                                                       </tr>
                                                   </table>
                                               </td>
                                           </tr>
                                           <tr>
                                               <td align="center" colspan="3" style="padding-left: 27px; padding-top: 5px; height: 37px; width: 214px;" valign="middle">
                                               <a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image1','','images/button_over.gif',1)"><img src="Images/button_main.gif" name="Image1" width="85" height="29" border="0" id="Image1" onclick="gotosaerch();" /></a>
                                               </td>
                                           </tr>
                                           <tr style="display:none">
                                               <td align="left" colspan="3" valign="top" style="width: 214px; height: 20px">
                                                   <span style="padding-bottom: 5px"><input name="QuickSearch1$rentShowOnMap" type="checkbox" id="QuickSearch1_rentShowOnMap" /><span style="padding-bottom: 5px"><strong>By
                                                           map</strong></span></span></td>
                                           </tr>
                                           <tr>
                                               <td align="left" colspan="3" style="height:15px; width: 214px;" valign="top">
                                               </td>
                                           </tr>
                                           
                                       </table>
                                   </div>
                                  
                               </div>
                           </td>
                       </tr>
                       <tr>
                           <td align="center" style="height: 16px" valign="middle"> <input type="submit" name="QuickSearch1$Button3" value="" id="QuickSearch1_Button3" />
                           </td>
                       </tr>
                   </table></div>
        <input type="hidden" name="QuickSearch1$hdnBuyLocations" id="QuickSearch1_hdnBuyLocations" value="Any,Battersea,Canada Water,East Finchley N2,Finchley N3,Highgate N6,Rotherhithe,Surrey Quays" />
        <input type="hidden" name="QuickSearch1$hdnRentLocations" id="QuickSearch1_hdnRentLocations" value="Any,All,Archway N19,Battersea,Canada Water,Devon,East Finchley N2,Exeter,Finchley N3,Highgate N6,Rotherhithe,Surrey Quays" />
            
<script type="text/javascript" language="javascript" src="js/featprop.js"></script>
          <table id="FeaturedProp" bgcolor="#ebebeb"  border="0" cellpadding="0" cellspacing="0" width="96%">
                       <tr>
                           <td align="center" valign="middle" style="height: 39px">
                               <img height="39" src="images/feature_p.jpg" width="220" /></td>
                       </tr>
                       <tr>
                           <td id="futurebg" align="center" height="180" style="padding-top: 5px" valign="middle">
                               <table align="center" border="0" cellpadding="0" cellspacing="0" height="180" width="82%">
                                   <tr>
                                       <td id="imganddetails" align="left" height="138" style="margin-bottom: 5px; width: 181px;" valign="top">
                                           <img height="120" src="images/image_soon-copy.jpg" width="120" /></td>
                                   </tr>
                                   <tr>
                                       <td align="left" style="width: 181px">
                                           <div>
                                               <span id="divPropAddress" style="font-size:11px; font-family:Arial,Helvetica,sans-serif; color:#666666;
                                                   text-align: left"></span>
                                               <br />
                                               <div id="divCost" style="font-size:11px; font-family:Arial,Helvetica,sans-serif; color:#666666;
                                                   text-align: left">
                                               </div>
                                           </div>
                                       </td>
                                   </tr>
                                   <br />
                                   <tr>
                                       <td align="left" height="19" style="width: 181px">
                                           <a id="buymoredetails" href="#" style="font-size:11px; font-weight:bold; font-family:Arial,Helvetica,sans-serif; color:#666666;
                                               text-align: left; text-decoration: underline">More details</a></td>
                                   </tr>
                               </table>
                           </td>
                       </tr>
                       <tr>
                           <td style="height: 11px">
                               <img height="11" src="images/future_bo.jpg" width="220" /></td>
                       </tr>
                   </table>
        </div>
        </td>
      </tr>
    </table></td>
  </tr>
</table>
</div>


<div id="footer" style="text-align:left; "><br />
<table width="73%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="52%"><span style="font-size:11px;text-decoration:none; font-weight:normal;color:#790000;cursor:pointer" onclick="window.open('privacylaw.htm','','status=yes,scrollbars=yes,width=660,height=650')">Privacy Law</span>|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size:11px;text-decoration:none; font-weight:normal;color:#790000;cursor:pointer" onclick="window.open('Disclaimer.htm','','status=yes,scrollbars=yes,width=660,height=509')">Disclaimer</span></td>
    <td width="48%" align="right">&nbsp;</td>
    </tr>
</table><div style="width:200px;"></div>
</div>
<script language="javascript" type="text/javascript">
        if (top.location != self.location)
        {
            top.location.replace(self.location); // pop to top
        }
</script>
<input type="hidden" name="hdnLocations" id="hdnLocations" value="All" />
<input type="hidden" name="hdnPostcodes" id="hdnPostcodes" value="All" />
<div style="display:none;"><input type="submit" name="btnSearch" value="Search" id="btnSearch" class="log_submit" /></div>
    </form>
</body>
</html>





<!--
     FILE ARCHIVED ON 7:38:52 Apr 21, 2010 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 15:40:52 Dec 16, 2015.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
-->
