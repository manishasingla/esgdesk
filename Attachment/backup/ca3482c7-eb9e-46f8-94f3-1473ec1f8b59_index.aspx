


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>


<script type="text/javascript" src="/static/js/analytics.js"></script>
<script type="text/javascript">archive_analytics.values.server_name="wwwb-app8.us.archive.org";archive_analytics.values.server_ms=467;</script>
<link type="text/css" rel="stylesheet" href="/static/css/banner-styles.css"/>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>
	Xample
</title><link href="class/class2.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="js/featprop.js"></script>
<script type="text/javascript" language="javascript" src="js/script.js"></script>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script type="text/javascript" language="JavaScript" src="/web/20140408060828js_/http://j.maxmind.com/app/geoip.js"></script>
<script type="text/javascript" src="/web/20140408060828js_/http://maps.googleapis.com/maps/api/js?sensor=false"></script>

</head>
<body>


<!-- BEGIN WAYBACK TOOLBAR INSERT -->
<script type="text/javascript" src="/static/js/disclaim-element.js" ></script>
<script type="text/javascript" src="/static/js/graph-calc.js" ></script>
<script type="text/javascript">//<![CDATA[
var __wm = (function(imgWidth,imgHeight,yearImgWidth,monthImgWidth){
var wbPrefix = "/web/";
var wbCurrentUrl = "http://www.gates.esuk.co.uk/";

var firstYear = 1996;
var displayDay = "8";
var displayMonth = "Apr";
var displayYear = "2014";
var prettyMonths = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
var $D=document,$=function(n){return document.getElementById(n)};
var trackerVal,curYear = -1,curMonth = -1;
var yearTracker,monthTracker;
function showTrackers(val) {
  if (val===trackerVal) return;
  var $ipp=$("wm-ipp");
  var $y=$("displayYearEl"),$m=$("displayMonthEl"),$d=$("displayDayEl");
  if (val) {
    $ipp.className="hi";
  } else {
    $ipp.className="";
    $y.innerHTML=displayYear;$m.innerHTML=displayMonth;$d.innerHTML=displayDay;
  }
  yearTracker.style.display=val?"inline":"none";
  monthTracker.style.display=val?"inline":"none";
  trackerVal = val;
}
function trackMouseMove(event,element) {
  var eventX = getEventX(event);
  var elementX = getElementX(element);
  var xOff = Math.min(Math.max(0, eventX - elementX),imgWidth);
  var monthOff = xOff % yearImgWidth;

  var year = Math.floor(xOff / yearImgWidth);
  var monthOfYear = Math.min(11,Math.floor(monthOff / monthImgWidth));
  // 1 extra border pixel at the left edge of the year:
  var month = (year * 12) + monthOfYear;
  var day = monthOff % 2==1?15:1;
  var dateString = zeroPad(year + firstYear) + zeroPad(monthOfYear+1,2) +
    zeroPad(day,2) + "000000";

  $("displayYearEl").innerHTML=year+firstYear;
  $("displayMonthEl").innerHTML=prettyMonths[monthOfYear];
  // looks too jarring when it changes..
  //$("displayDayEl").innerHTML=zeroPad(day,2);
  var url = wbPrefix + dateString + '/' +  wbCurrentUrl;
  $("wm-graph-anchor").href=url;

  if(curYear != year) {
    var yrOff = year * yearImgWidth;
    yearTracker.style.left = yrOff + "px";
    curYear = year;
  }
  if(curMonth != month) {
    var mtOff = year + (month * monthImgWidth) + 1;
    monthTracker.style.left = mtOff + "px";
    curMonth = month;
  }
}
function hideToolbar() {
  $("wm-ipp").style.display="none";
}
function bootstrap() {
  var $spk=$("wm-ipp-sparkline");
  yearTracker=$D.createElement('div');
  yearTracker.className='yt';
  with(yearTracker.style){
    display='none';width=yearImgWidth+"px";height=imgHeight+"px";
  }
  monthTracker=$D.createElement('div');
  monthTracker.className='mt';
  with(monthTracker.style){
    display='none';width=monthImgWidth+"px";height=imgHeight+"px";
  }
  $spk.appendChild(yearTracker);
  $spk.appendChild(monthTracker);

  var $ipp=$("wm-ipp");
  $ipp&&disclaimElement($ipp);
}
return{st:showTrackers,mv:trackMouseMove,h:hideToolbar,bt:bootstrap};
})(500, 27, 25, 2);//]]>
</script>
<style type="text/css">
body {
  margin-top:0 !important;
  padding-top:0 !important;
  min-width:800px !important;
}
</style>
<div id="wm-ipp" lang="en" style="display:none;">

<div style="position:fixed;left:0;top:0;width:100%!important">
<div id="wm-ipp-inside">
   <table style="width:100%;"><tbody><tr>
   <td id="wm-logo">
       <a href="/web/" title="Wayback Machine home page"><img src="/static/images/toolbar/wayback-toolbar-logo.png" alt="Wayback Machine" width="110" height="39" border="0" /></a>
   </td>
   <td class="c">
       <table style="margin:0 auto;"><tbody><tr>
       <td class="u" colspan="2">
       <form target="_top" method="get" action="/web/form-submit.jsp" name="wmtb" id="wmtb"><input type="text" name="url" id="wmtbURL" value="http://www.gates.esuk.co.uk/" style="width:400px;" onfocus="this.focus();this.select();" /><input type="hidden" name="type" value="replay" /><input type="hidden" name="date" value="20140408060828" /><input type="submit" value="Go" /><span id="wm_tb_options" style="display:block;"></span></form>
       </td>
       <td class="n" rowspan="2">
           <table><tbody>
           <!-- NEXT/PREV MONTH NAV AND MONTH INDICATOR -->
           <tr class="m">
           	<td class="b" nowrap="nowrap">
		
		    <a href="/web/20121108093129/http://www.gates.esuk.co.uk/" title="8 Nov 2012">NOV</a>
		
		</td>
		<td class="c" id="displayMonthEl" title="You are here: 6:08:28 Apr 8, 2014">APR</td>
		<td class="f" nowrap="nowrap">
		
		    May
		
                </td>
	    </tr>
           <!-- NEXT/PREV CAPTURE NAV AND DAY OF MONTH INDICATOR -->
           <tr class="d">
               <td class="b" nowrap="nowrap">
               
                   <a href="/web/20121108093129/http://www.gates.esuk.co.uk/" title="9:31:29 Nov 8, 2012"><img src="/static/images/toolbar/wm_tb_prv_on.png" alt="Previous capture" width="14" height="16" border="0" /></a>
               
               </td>
               <td class="c" id="displayDayEl" style="width:34px;font-size:24px;" title="You are here: 6:08:28 Apr 8, 2014">8</td>
	       <td class="f" nowrap="nowrap">
               
                   <img src="/static/images/toolbar/wm_tb_nxt_off.png" alt="Next capture" width="14" height="16" border="0"/>
               
	       </td>
           </tr>
           <!-- NEXT/PREV YEAR NAV AND YEAR INDICATOR -->
           <tr class="y">
	       <td class="b" nowrap="nowrap">
               
                   <a href="/web/20121108093129/http://www.gates.esuk.co.uk/" title="8 Nov 2012"><strong>2012</strong></a>
               
               </td>
               <td class="c" id="displayYearEl" title="You are here: 6:08:28 Apr 8, 2014">2014</td>
	       <td class="f" nowrap="nowrap">
               
                   2015
               
	       </td>
           </tr>
           </tbody></table>
       </td>
       </tr>
       <tr>
       <td class="s">
           <a class="t" href="/web/20140408060828*/http://www.gates.esuk.co.uk/" title="See a list of every capture for this URL">14 captures</a>
           <div class="r" title="Timespan for captures of this URL">6 Apr 09 - 8 Apr 14</div>
       </td>
       <td class="k">
       <a href="" id="wm-graph-anchor">
       <div id="wm-ipp-sparkline" title="Explore captures for this URL">
	 <img id="sparklineImgId" alt="sparklines"
		 onmouseover="__wm.st(1)" onmouseout="__wm.st(0)"
		 onmousemove="__wm.mv(event,this)"
		 width="500"
		 height="27"
		 border="0"
		 src="/web/jsp/graph.jsp?graphdata=500_27_1996:-1:000000000000_1997:-1:000000000000_1998:-1:000000000000_1999:-1:000000000000_2000:-1:000000000000_2001:-1:000000000000_2002:-1:000000000000_2003:-1:000000000000_2004:-1:000000000000_2005:-1:000000000000_2006:-1:000000000000_2007:-1:000000000000_2008:-1:000000000000_2009:-1:000111111001_2010:-1:111010010000_2011:-1:000000000000_2012:-1:000000000010_2013:-1:000000000000_2014:3:000100000000_2015:-1:000000000000" />
       </div>
       </a>
       </td>
       </tr></tbody></table>
   </td>
   <td class="r">
       <a href="#close" onclick="__wm.h();return false;" style="background-image:url(/static/images/toolbar/wm_tb_close.png);top:5px;" title="Close the toolbar">Close</a>
       <a href="http://faq.web.archive.org/" style="background-image:url(/static/images/toolbar/wm_tb_help.png);bottom:5px;" title="Get some help using the Wayback Machine">Help</a>
   </td>
   </tr></tbody></table>
</div>
</div>
</div>
<script type="text/javascript">__wm.bt();</script>
<!-- END WAYBACK TOOLBAR INSERT -->

<form name="form1" method="post" action="fhs.aspx" id="form1">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKLTY5NTE0MzE3NA9kFgICAw9kFgQCAQ9kFgICFw9kFgJmD2QWAgIBDxYEHghJbnRlcnZhbAIBHgdFbmFibGVkZ2QCBw9kFgQCAQ8QD2QWAh4Ib25jaGFuZ2UFFnJldHVybiBjaGFuZ2VWYWx1ZXMoKTtkZGQCAw8QDxYGHg1EYXRhVGV4dEZpZWxkBQRBcmVhHg5EYXRhVmFsdWVGaWVsZAUEQXJlYR4LXyFEYXRhQm91bmRnZBAVEQNBbnkUNjgtMTE0IEtuaWdodHNicmlkZ2ULQXJjaHdheSBOMTkJQmF0dGVyc2VhDENhbmFkYSBXYXRlcgZDYW5uZXMNQ2hlc2hhbSBQbGFjZRBFYXN0IEZpbmNobGV5IE4yDEVhdG9uIFNxdWFyZQtGaW5jaGxleSBOMwtIaWdoZ2F0ZSBONg1LbmlnaHRzYnJpZGdlBk1hbGFnYQtSb3RoZXJoaXRoZQxTdXJyZXkgUXVheXMJV2V5YnJpZGdlD1dpbHRvbiBDcmVzY2VudBURA0FueRQ2OC0xMTQgS25pZ2h0c2JyaWRnZQtBcmNod2F5IE4xOQlCYXR0ZXJzZWEMQ2FuYWRhIFdhdGVyBkNhbm5lcw1DaGVzaGFtIFBsYWNlEEVhc3QgRmluY2hsZXkgTjIMRWF0b24gU3F1YXJlC0ZpbmNobGV5IE4zC0hpZ2hnYXRlIE42DUtuaWdodHNicmlkZ2UGTWFsYWdhC1JvdGhlcmhpdGhlDFN1cnJleSBRdWF5cwlXZXlicmlkZ2UPV2lsdG9uIENyZXNjZW50FCsDEWdnZ2dnZ2dnZ2dnZ2dnZ2dnZGRk/74F9hJ1eB7N+5EOuK3T53NTWY8=" />


<script src="/web/20140408060828js_/http://www.gates.esuk.co.uk/ScriptResource.axd?d=SlJmBcKe9XLcSLAE8_40TD1ANNHYclG4oiTJK3Qf3Z71ejpV5ZnZdUgZIy41BTVLu-5wqmQH3gMkwjrdYmpvVRCa7f5inCotztyscz2AkbMoojyGxm0k_T1d9v99LEi6I-krZxm9oR_El5IPdb47KEUTwKdwWdv4nuShMxJZtduQOgMa0&amp;t=634795983732886895" type="text/javascript"></script>
<script src="/web/20140408060828js_/http://www.gates.esuk.co.uk/ScriptResource.axd?d=4ECeFf_Idn7wQR_5_CxD5aEmIYHp8ybIMN6YI-lcK8cYgB3XxoPn34XkQTQWzSVg5WCVFXTBypibqnYW0y0ionxYsl0tlgOpYmiLuLF5KsSgvx5i-Y1XLm9Q8yRLPJ1gFHcDGw76zyUADWJVBFpCK5EB0lCG7AOBqoGXgVUZ9eToPPdS0&amp;t=634795983732886895" type="text/javascript"></script>
  <div id="main">
    <div class="header">
      <div class="innerhead">
       <div id = "header">
<script type='text/javascript' language='JavaScript' src='/web/20140408060828js_/http://j.maxmind.com/app/geoip.js'></script>
<script type='text/javascript' src='/web/20140408060828js_/http://maps.googleapis.com/maps/api/js?sensor=false'></script>
<html>
<head>
<style>
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
    #yourinfo
    {
        width: 263px;
    }
</style>
</head>
<body>
  <input type="hidden" name="top_right123$Hgeoip_area_code" id="top_right123_Hgeoip_area_code" />
    <input type="hidden" name="top_right123$Hgeoip_city" id="top_right123_Hgeoip_city" />
    <input type="hidden" name="top_right123$Hgeoip_country_code" id="top_right123_Hgeoip_country_code" />
    <input type="hidden" name="top_right123$Hgeoip_country_name" id="top_right123_Hgeoip_country_name" />
    <input type="hidden" name="top_right123$Hgeoip_latitude" id="top_right123_Hgeoip_latitude" />
    <input type="hidden" name="top_right123$Hgeoip_longitude" id="top_right123_Hgeoip_longitude" />
    <input type="hidden" name="top_right123$Hgeoip_metro_code" id="top_right123_Hgeoip_metro_code" />
    <input type="hidden" name="top_right123$Hgeoip_postal_code" id="top_right123_Hgeoip_postal_code" />
    <input type="hidden" name="top_right123$Hgeoip_region" id="top_right123_Hgeoip_region" />
    <input type="hidden" name="top_right123$Hgeoip_region_name" id="top_right123_Hgeoip_region_name" />
    
     <div id="top_right123_UpdatePanelMaster">
	
           <span id="top_right123_Timer1" style="display:none;"></span>
        
</div>
    <div id = "yourinfo" align="left" width = "50%" >
    <table Width="100%"><tr><td width = 10%><img src="/web/20140408060828im_/http://www.gates.esuk.co.uk/images/LOC.jpg" 
            style="height: 32px; width: 26px" /></td><td align =left ><b>
       <span style="font-size:14px;  color:#790000;"> 
        <input name="top_right123$TXTHEAD" type="text" readonly="readonly" id="top_right123_TXTHEAD" border="1" /></span></b></td></tr></table>
    </div>
</body></html></div>
         
<div class="logo_top"> 
            	<span class="main_logo"><img src="images/Xample.png" /></span> 
                <span class="contact_top"> 
                	<span class="top_contact">020 7609 2800</span> 
                    <span class="email"><a href="mailto:info@xample.co.uk">info@xample.co.uk</a></span> 
                    
               </span> 
           </div>
        
<div class="menu">
          <ul>
            <li style="padding-left:0px !important;"><a href="fhs.aspx">Home</a></li>
            <li><a href="buy.aspx">Buy</a></li>
            <li><a href="rent.aspx">Rent</a></li>
            <li><a href="mortgages.aspx">Mortgages</a></li>
            <li><a href="vendors.aspx">Vendors</a></li>
            <li><a href="landlords.aspx">Landlords</a></li>
            <li><a href="aboutus.aspx">About us</a></li>
            <li><a href="contactus.aspx">Contact us</a></li>
            <li><a href="Register.aspx">Register</a></li>
            <li style="padding-right:0px !important;"><a href="favourites.aspx">My favourites</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="banner">
    <div class="call"></div>
      <div class="inner_banner">
        
<script type="text/javascript" language="javascript">
function changeValues()
{

   var obj1 = document.getElementById("quick_search_drpDealType").value;
       
    if(obj1 == '1')
        {
            var ddlOrders = document.getElementById("quick_search_rentMinPrice");
            for (var count = ddlOrders.options.length-1; count >-1; count--)
                {
                    ddlOrders.options[count] = null;
                }
               
            text = 'Min';
            text1 = '0';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 50k';
            text1 = '50000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 60k';
            text1 = '60000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 70k';
            text1 = '70000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 80k';
            text1 = '80000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 90k';
            text1 = '90000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 100k';
            text1 = '100000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 150k';
            text1 = '150000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 200k';
            text1 = '200000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 250k';
            text1 = '250000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
              
             text = '\u00A3 300k';
            text1 = '300000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem; 
             
             text = '\u00A3 350k';
            text1 = '350000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;  
            
             text = '\u00A3 450k';
            text1 = '450000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem; 
             
             text = '\u00A3 500k';
            text1 = '500000';
            listItem = new Option(text, text1, false, false)    
            ddlOrders.options[ddlOrders.length] = listItem;  
            
             text = '\u00A3 550k';
            text1 = '550000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 600k';
            text1 = '600000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 650k';
            text1 = '650000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 700k';
            text1 = '700000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 750k';
            text1 = '750000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 800k';
            text1 = '800000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 850k';
            text1 = '850000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
               text = '\u00A3 900k';
            text1 = '900000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
             text = '\u00A3 1m';
            text1 = '1000000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            
            
            var ddlOrders2 = document.getElementById("quick_search_rentMaxPrice");
            for (var count = ddlOrders2.options.length-1; count >-1; count--)
                {
                    ddlOrders2.options[count] = null;
                }
                
            text = 'Max';
            text1 = '0';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
             text = '\u00A3 50k';
            text1 = '50000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 60k';
            text1 = '60000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 70k';
            text1 = '70000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 80k';
            text1 = '80000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 90k';
            text1 = '90000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 100k';
            text1 = '100000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 150k';
            text1 = '150000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 200k';
            text1 = '200000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 250k';
            text1 = '250000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
              
             text = '\u00A3 300k';
            text1 = '300000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem; 
             
             text = '\u00A3 350k';
            text1 = '350000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;  
            
             text = '\u00A3 450k';
            text1 = '450000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem; 
             
             text = '\u00A3 500k';
            text1 = '500000';
            listItem = new Option(text, text1, false, false)    
            ddlOrders2.options[ddlOrders2.length] = listItem;  
            
             text = '\u00A3 550k';
            text1 = '550000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 600k';
            text1 = '600000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
             text = '\u00A3 650k';
            text1 = '650000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
             text = '\u00A3 700k';
            text1 = '700000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
             text = '\u00A3 750k';
            text1 = '750000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
             text = '\u00A3 800k';
            text1 = '800000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
             text = '\u00A3 850k';
            text1 = '850000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
               text = '\u00A3 900k';
            text1 = '900000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
             text = '\u00A3 1m';
            text1 = '1000000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            GetBuyLocations();
                   
        }
        else if(obj1 == '2')
        {
            var ddlOrders = document.getElementById("quick_search_rentMinPrice");
            for (var count = ddlOrders.options.length-1; count >-1; count--)
            {
                    ddlOrders.options[count] = null;
            }
            text = 'Min';
            text1 = '0';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 100 per week';
            text1 = '100';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 200 per week';
            text1 = '200';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 300 per week';
            text1 = '300';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 400 per week';
            text1 = '400';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            text = '\u00A3 500 per week';
            text1 = '500';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            text = '\u00A3 600 per week';
            text1 = '600';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            text = '\u00A3 750 per week';
            text1 = '750';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 1000 per week';
            text1 = '1000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            
            var ddlOrders2 = document.getElementById("quick_search_rentMaxPrice");
            for (var count = ddlOrders2.options.length-1; count >-1; count--)
            {
                    ddlOrders2.options[count] = null;
            }
            text = 'Max';
            text1 = '0';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 100 per week';
            text1 = '100';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 200 per week';
            text1 = '200';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 300 per week';
            text1 = '300';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 400 per week';
            text1 = '400';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 500 per week';
            text1 = '500';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 600 per week';
            text1 = '600';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 750 per week';
            text1 = '750';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 1000 per week';
            text1 = '1000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            GetRentLocations();
        }
        else

        {
            var ddlOrders = document.getElementById("quick_search_rentMaxPrice");
            for (var count = ddlOrders.options.length-1; count >-1; count--)
                {
                    ddlOrders.options[count] = null;
                }
           text = 'Max';
            text1 = '0';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 60k';
            text1 = '60000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 70k';
            text1 = '70000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 80k';
            text1 = '30000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 90k';
            text1 = '90000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 100k';
            text1 = '100000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 150k';
            text1 = '150000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 200k';
            text1 = '200000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 250k';
            text1 = '250000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 300k';
            text1 = '300000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem; 
            
             text = '\u00A3 300k';
            text1 = '300000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem; 
             
             text = '\u00A3 350k';
            text1 = '350000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;  
            
             text = '\u00A3 450k';
            text1 = '450000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem; 
             
             text = '\u00A3 500k';
            text1 = '500000';
            listItem = new Option(text, text1, false, false)    
            ddlOrders.options[ddlOrders.length] = listItem;  
            
             text = '\u00A3 550k';
            text1 = '550000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 600k';
            text1 = '600000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 650k';
            text1 = '650000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 700k';
            text1 = '700000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 750k';
            text1 = '750000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 800k';
            text1 = '800000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 850k';
            text1 = '850000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
               text = '\u00A3 900k';
            text1 = '900000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
               text = '\u00A3 1m';
            text1 = '1000000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            GetBuyLocations();
            
            }
}

function GetBuyLocations()
{

            var buyLocations = new Array();
            
            buyLocations = document.getElementById("quick_search_hdnBuyLocations").value.split(',');
            
            var text; 
            var text1;
            var listItem;
            
            var ddlOrders = document.getElementById("quick_search_rentArea");
            
            if(ddlOrders.options.length > 0)
            {
                for(var i = ddlOrders.options.length - 1; i >= 0; i--)
                {
                    ddlOrders.remove(i);
                }
            }
            
            for (var count = 0; count < buyLocations.length; count++)
            {
                text = buyLocations[count];
                text1 =  buyLocations[count];
                listItem = new Option(text, text1, false, false);
                ddlOrders.options[ddlOrders.options.length] = listItem;
            }       
}

function GetRentLocations()
{
            
           var rentLocations = new Array();
           
            rentLocations = document.getElementById("quick_search_hdnRentLocations").value.split(',');
             
            var text; 
            var text1;
            var listItem;
            
            var ddlOrders = document.getElementById("quick_search_rentArea");
            
      
            if(ddlOrders.options.length > 0)
            {
                for(var i = ddlOrders.options.length - 1; i >= 0; i--)
                {
                    ddlOrders.remove(i);
                }
            }
            
            for (var count = 0; count < rentLocations.length; count++)
            {
                text = rentLocations[count];
                text1 =  rentLocations[count];
                listItem = new Option(text, text1, false, false);
                ddlOrders.options[ddlOrders.options.length] = listItem;
            }       
}
</script>


<div class="left_content">
          <div class="block">
            <!--<h3 id="divQuickSearchPanel1">Quick search</h3>-->
            <div class="content02">
              <!--<div style="padding-top:10px;">Type</div>-->
              <div>
                <select name="quick_search$drpDealType" id="quick_search_drpDealType" onchange="return changeValues();">
	<option value="1">Buy</option>
	<option value="2">Rent</option>

</select>
              </div>
              <!--<div style="margin-top:10px;">Location:</div>-->
              <div style="margin-top:14px;">
                <select name="quick_search$rentArea" id="quick_search_rentArea">
	<option value="Any">Any</option>
	<option value="68-114 Knightsbridge">68-114 Knightsbridge</option>
	<option value="Archway N19">Archway N19</option>
	<option value="Battersea">Battersea</option>
	<option value="Canada Water">Canada Water</option>
	<option value="Cannes">Cannes</option>
	<option value="Chesham Place">Chesham Place</option>
	<option value="East Finchley N2">East Finchley N2</option>
	<option value="Eaton Square">Eaton Square</option>
	<option value="Finchley N3">Finchley N3</option>
	<option value="Highgate N6">Highgate N6</option>
	<option value="Knightsbridge">Knightsbridge</option>
	<option value="Malaga">Malaga</option>
	<option value="Rotherhithe">Rotherhithe</option>
	<option value="Surrey Quays">Surrey Quays</option>
	<option value="Weybridge">Weybridge</option>
	<option value="Wilton Crescent">Wilton Crescent</option>

</select>
              </div>
              <!--<div style="margin-top:10px;">Bedrooms:</div>-->
              <div style="margin-top:14px;">
                <select name="quick_search$rentMinBedrooms" id="quick_search_rentMinBedrooms">
	<option selected="selected" value="-1">Bedrooms</option>
	<option value="select">Any</option>
	<option value="0">Studio</option>
	<option value="1">1</option>
	<option value="2">2</option>
	<option value="3">3</option>
	<option value="4">4</option>
	<option value="99">5+</option>

</select>
              </div>
              <!--<div style="margin-top:10px;">Min price: </div>-->
              <div style="margin-top:14px;">
                <select name="quick_search$rentMinPrice" id="quick_search_rentMinPrice">
	<option value="0">Min price</option>
	<option value="50000">&#163;50k</option>
	<option value="60000">&#163;60k</option>
	<option value="70000">&#163;70k</option>
	<option value="80000">&#163;80k</option>
	<option value="90000">&#163;90k</option>
	<option value="100000">&#163;100k</option>
	<option value="150000">&#163;150k</option>
	<option value="200000">&#163;200k</option>
	<option value="250000">&#163;250k</option>
	<option value="300000">&#163;300k</option>
	<option value="350000">&#163;350k</option>
	<option value="400000">&#163;400k</option>
	<option value="450000">&#163;450k</option>
	<option value="500000">&#163;500k</option>
	<option value="550000">&#163;550k</option>
	<option value="600000">&#163;600k</option>
	<option value="650000">&#163;650k</option>
	<option value="700000">&#163;700k</option>
	<option value="800000">&#163;800k</option>
	<option value="900000">&#163;900k</option>
	<option value="1000000">&#163;1m</option>

</select>
              </div>
             <!-- <div style="margin-top:10px;">Max. Price:</div>-->
              <div style="margin-top:14px;">
                <select name="quick_search$rentMaxPrice" id="quick_search_rentMaxPrice">
	<option value="0">Max.Price</option>
	<option value="25000">&#163;25k</option>
	<option value="50000">&#163;50k</option>
	<option value="60000">&#163;60k</option>
	<option value="70000">&#163;70k</option>
	<option value="75000">&#163;75k</option>
	<option value="80000">&#163;80k</option>
	<option value="90000">&#163;90k</option>
	<option value="100000">&#163;100k</option>
	<option value="110000">&#163;110k</option>
	<option value="120000">&#163;120k</option>
	<option value="125000">&#163;125k</option>
	<option value="130000">&#163;130k</option>
	<option value="140000">&#163;140k</option>
	<option value="150000">&#163;150k</option>
	<option value="160000">&#163;160k</option>
	<option value="170000">&#163;170k</option>
	<option value="175000">&#163;175k</option>
	<option value="180000">&#163;180k</option>
	<option value="190000">&#163;190k</option>
	<option value="200000">&#163;200k</option>
	<option value="210000">&#163;210k</option>
	<option value="220000">&#163;220k</option>
	<option value="230000">&#163;230k</option>
	<option value="240000">&#163;240k</option>
	<option value="250000">&#163;250k</option>
	<option value="260000">&#163;260k</option>
	<option value="270000">&#163;270k</option>
	<option value="280000">&#163;280k</option>
	<option value="290000">&#163;290k</option>
	<option value="300000">&#163;300k</option>
	<option value="325000">&#163;325k</option>
	<option value="350000">&#163;350k</option>
	<option value="375000">&#163;375k</option>
	<option value="400000">&#163;400k</option>
	<option value="425000">&#163;425k</option>
	<option value="450000">&#163;450k</option>
	<option value="475000">&#163;475k</option>
	<option value="500000">&#163;500k</option>
	<option value="550000">&#163;550k</option>
	<option value="600000">&#163;600k</option>
	<option value="650000">&#163;650k</option>
	<option value="700000">&#163;700k</option>
	<option value="800000">&#163;800k</option>
	<option value="900000">&#163;900k</option>
	<option value="1000000">&#163;1m+</option>

</select>
              </div>
              <div class="search_button">
                <input type="submit" name="quick_search$btnSearch" value="Search" id="quick_search_btnSearch" class="searchbutton" />
              </div>
            </div>
          </div>
        </div>
        <input type="hidden" name="quick_search$hdnBuyLocations" id="quick_search_hdnBuyLocations" value="Any,68-114 Knightsbridge,Archway N19,Battersea,Canada Water,Cannes,Chesham Place,East Finchley N2,Eaton Square,Finchley N3,Highgate N6,Knightsbridge,Malaga,Rotherhithe,Surrey Quays,Weybridge,Wilton Crescent" />
        <input type="hidden" name="quick_search$hdnRentLocations" id="quick_search_hdnRentLocations" value="Any,Archway N19,Battersea,Bournemouth,Bush Road,Canada Water,Charminster,Chesham Place,East Finchley N2,Finchley N3,Hacienda Las Chapas,Heath Street,Highgate N6,Moordown,Plough Way,Rotherhithe,Springbourne,Surrey Quays,Weston House,Winton" />
        
<div class="languages">
          <table cellspacing="0" cellpadding="4" border="0" align="center">
            <tbody>
              <tr>
                <td valign="top" align="left" style="font-size:14px; color:#333; height: 36px;">Choose language </td>
                <td width="20" valign="top" align="left" style="height: 36px"><span style="cursor:pointer" onclick="location.href='http://' + 'gates.esuk.co.uk/index.aspx';return false;"><img width="20" height="16" border="0" src="images/flags/gb.gif" title="Ingl�s"></span></td>
                <td width="20" valign="top" align="left" style="height: 36px"><span style="cursor:pointer" onclick="location.href='http://' + 'translate.google.com/translate_c?langpair=en|de&amp;u=/web/20140408060828/http://gates.esuk.co.uk/index.aspx';return false;"><img width="20" height="16" border="0" alt="Alem�n" src="images/flags/de.gif" title="Alem�n"></span></td>
                <td width="20" valign="top" align="left" style="height: 36px"><span style="cursor:pointer" onclick="location.href='http://' +'translate.google.com/translate_c?langpair=en|fr&amp;u=/web/20140408060828/http://gates.esuk.co.uk/index.aspx';return false;"><img width="20" height="16" border="0" alt="Franc�s" src="images/flags/fr.gif" title="Franc�s"></span></td>
                <td width="20" valign="top" align="left" style="height: 36px"><span style="cursor:pointer" onclick="location.href='http://' +'translate.google.com/translate_c?langpair=en|es&amp;u=/web/20140408060828/http://gates.esuk.co.uk/index.aspx';return false;"><img width="20" height="16" border="0" alt="Espa�ol" src="images/flags/es.gif" title="Espa�ol"></span></td>
                <td width="20" valign="top" align="left" style="height: 36px"><span style="cursor:pointer" onclick="location.href='http://' + 'translate.google.com/translate_c?langpair=en|pt&amp;u=/web/20140408060828/http://gates.esuk.co.uk/index.aspx';return false;"><img width="20" height="16" border="0" alt="Portugu�s" src="images/flags/pt.gif" title="Portugu�s"></span></td>
                <td width="20" valign="top" align="left" style="height: 36px"><span style="cursor:pointer" onclick="location.href='http://' +'translate.google.com/translate_c?langpair=en|nl&amp;u=/web/20140408060828/http://gates.esuk.co.uk/index.aspx';return false;"><img width="20" height="16" border="0" alt="Holand�s" src="images/flags/nl.gif" title="Holand�s"></span></td>
                <td width="20" valign="top" align="left" style="height: 36px"><span style="cursor:pointer" onclick="location.href='http://' +'translate.google.com/translate_c?langpair=en|zh&amp;u=/web/20140408060828/http://gates.esuk.co.uk/index.aspx';return false;"><img width="20" height="16" border="0" alt="Chino" src="images/flags/ch.jpg" title="Chino"></span></td>
                <td width="20" valign="top" align="left" style="height: 36px"><span style="cursor:pointer" onclick="location.href='http://' +'translate.google.com/translate_c?langpair=en|it&amp;u=/web/20140408060828/http://gates.esuk.co.uk/index.aspx';return false;"><img width="20" height="16" border="0" alt="Italiano" src="images/flags/it.gif" title="Italiano"></span></td>
                <td width="20" valign="top" align="left" style="height: 36px"><span style="cursor:pointer" onclick="location.href='http://' +'translate.google.com/translate_c?langpair=en|ar&amp;u=/web/20140408060828/http://gates.esuk.co.uk/index.aspx';return false;"><img width="20" height="16" border="0" alt="Arabic" src="images/flags/ar.gif" title="Arabic"></span></td>
                <td valign="top" align="left" style="padding-left:2px;padding-right:2px; height: 36px; font-size:14px; color:#333; "><span style="cursor:pointer" onclick="location.href='http://' +'translate.google.com/translate_c?langpair=en|iw&amp;u=/web/20140408060828/http://gates.esuk.co.uk/index.aspx';return false;">Hebrew</span></td>
                <td valign="top" align="left" style="padding-left:2px;padding-right:2px; height: 36px; font-size:14px; color:#333; "><span style="cursor:pointer" onclick="alert('This translation is not yet posible but is coming soon.');return false;">Urdu</span></td>
                <td valign="top" align="left" style="padding-left:2px;padding-right:2px; height: 36px; font-size:14px; color:#333; "><span style="cursor:pointer" onclick="alert('This translation is not yet posible but is coming soon.');return false;">Bengal</span></td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="right_content">
          <div class="block">
            <h3 id="divQuickSearchPanel1">Newly listed properties</h3>
            <div class="content02" style="width:195px; border:1px solid #B2B2BC; border-top:none;">
              

<script type="text/javascript" language="javascript" src="js/featprop.js"></script>
          <table id="FeaturedProp" bgcolor="#ebebeb"  border="0" cellpadding="0" cellspacing="0" width="100%">
                         <tr>
                           <td id="futurebg_new" align="center" height="180" valign="middle"><table align="center" border="0" cellpadding="0" cellspacing="0" height="180" width="82%">
                                   <tr>
                                       <td height="125" id="imganddetails" align="left" style="margin-bottom: 5px; " valign="top" >
                                           <img height="120" src="images/image_soon-copy.jpg" width="120" /></td>
                                   </tr>
                                   <tr>
                                       <td align="left" style="width: 181px">
                                           <div>
                                               <span id="divPropAddress" style="font-size:12px; color:#333; text-align: left"></span>
                                               <br />
                                               <div id="divCost" style="font-size:12px; color:#333; text-align: left"> </div>
                                           </div>
                                       </td>
                                   </tr>
                                   <br />
                                   <tr>
                                       <td align="left" height="19" style="width: 181px">
                                           <a id="buymoredetails" href="#" style="font-size:12px; font-weight:bold; color:#666666; text-align: left; text-decoration: underline">More details</a></td>
                                   </tr>
                               </table>
                           </td>
                       </tr>
                     </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="main_rows">
      
<div class="rows">
        <div class="coloumn">
          <ul style="margin:13px 0px;">
            <li class="blue"><img src="images/street-view.png" alt="" width="45" height="43" border="0" /></li>
            <li>
              <h3>New: Street view</h3>
            </li>
            <li>You can now view all our properties in 3D using "Street View". <br />
Take a walk</li>
            <li class="read_more"><a href="#">Read more</a></li>
          </ul>
        </div>
        <div class="coloumn">
          <ul style="margin:13px 0px;">
            <li class="green"><img src="images/map-icon.png" alt="" border="0" width="45" height="43" /></li>
            <li>
              <h3>Search by map</h3>
            </li>
            <li>Now you can seacrh for properties using our interactive <br />
map!</li>
            <li class="read_more"><a href="#">Read more</a></li>
          </ul>
        </div>
        <div class="coloumn">
          <ul style="margin:13px 0px;">
            <li class="red"><img src="images/register-icon.png" width="45" height="43" alt="" border="0" /></li>
            <li>
              <h3>Register with us</h3>
            </li>
            <li>And we will send you matching lets as they are instructed - often before<br />
 they are published </li>
            <li class="read_more"><a href="register.aspx">Read more</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="content">
      <div class="inner_content">
        <div class="middle_content">
          <p>This website has the latest functionality which can be incorporated into an estate &amp; letting website to help achieve instructions and sell and let properties.</p>
          <p>Such functionality as: Interactive Mapping and Google Street View. The site also integrates with estateCRM cutting edge software so it is updated in real time and applicants are added to estateCRM when registering on this website</p>
          <p>Click on Search on the right and you will view all the dummy properties in the test database. You can then click on an individiual property to view the map with local schools etc plotted, Google Street View, images, slideshows etc . If you click first on 'by map' before you click on search you will see all the properties on an interactive map.</p>
          <p>If you are using or trialling our cutting edge software, EstateCRM, you can also add or amend data and you will see that here updated live in real time. To trial estateCRM just call <font style="color:#0C4576;">020 7609 2800</font> or email <a href="mailto:trial@estatesolutions.co.uk"><font>trial@estatesolutions.co.uk</font></a></p>
        </div>
      </div>
    </div>
    

<div class="footer">
  <div class="inner_footer" style="margin:0px auto; width:1000px;">
    <div class="footer_left"> <span onclick="window.open('privacylaw.htm','','status=yes,scrollbars=yes,width=700,height=700')" style="cursor:pointer;">Privacy Law</span>
     | <span onclick="window.open('Disclaimer.htm','','status=yes,scrollbars=yes,width=700,height=700')" style="cursor:pointer">Disclaimer</span>
     | <a href="sitemap.aspx">Sitemap</a></div>
    <div class="footer_right">Created by <a href="/web/20140408060828/http://www.estatesolutions.co.uk/" target="_blank">Estate Solutions</a></div>
    <div class="footer_logo"><a href="fhs.aspx"><img src="images/Xample-footer.png" alt="" border="0" /></a></div>
    <div class="footer_menu">
      <ul>
        <li style="padding-left:0px !important;"><a href="fhs.aspx">Home</a></li>
        <li><a href="buy.aspx">Buy</a></li>
        <li><a href="rent.aspx">Rent</a></li>
        <li><a href="mortgages.aspx">Mortgages</a></li>
        <li><a href="vendors.aspx">Vendors</a></li>
        <li><a href="landlords.aspx">Landlords</a></li>
        <li><a href="aboutus.aspx">About us</a></li>
        <li><a href="contactus.aspx">Contact us</a></li>
        <li><a href="Register.aspx">Register</a></li>
        <li style="padding-right:0px !important;"><a href="favourites.aspx">My favourites</a></li>
      </ul>
    </div>
  </div>
</div>

  </div>
  <script language="javascript" type="text/javascript">
        if (top.location != self.location)
        {
            top.location.replace(self.location); // pop to top
        }
</script>

<script type='text/javascript' language='JavaScript' src='/web/20140408060828js_/http://j.maxmind.com/app/geoip.js'></script><script type='text/javascript' src='/web/20140408060828js_/http://maps.googleapis.com/maps/api/js?sensor=false'></script><script type='text/javascript'> var geval = geoip_area_code(); document.getElementById('top_right123_Hgeoip_area_code').value = geval;  var geoip_city = geoip_city(); document.getElementById('top_right123_Hgeoip_city').value = geoip_city;  var geoip_country_code = geoip_country_code(); document.getElementById('top_right123_Hgeoip_country_code').value = geoip_country_code;  var geoip_country_names = geoip_country_name(); document.getElementById('top_right123_Hgeoip_country_name').value = geoip_country_names;  var geoip_latitude = geoip_latitude(); document.getElementById('top_right123_Hgeoip_latitude').value = geoip_latitude;  var geoip_longitude = geoip_longitude(); document.getElementById('top_right123_Hgeoip_longitude').value = geoip_longitude;  var geoip_metro_code = geoip_metro_code(); document.getElementById('top_right123_Hgeoip_metro_code').value = geoip_metro_code;  var geoip_postal_code = geoip_postal_code(); document.getElementById('top_right123_Hgeoip_postal_code').value = geoip_postal_code;  var geoip_region = geoip_region(); document.getElementById('top_right123_Hgeoip_region').value = geoip_region;  var geoip_region_name = geoip_region_name(); document.getElementById('top_right123_Hgeoip_region_name').value = geoip_region_name;  var geoip_Head = '' + geoip_city + ',' + geoip_country_name(); document.getElementById('top_right123_TXTHEAD').value = geoip_Head; </script>
<script type="text/javascript">
//<![CDATA[
Sys.Application.initialize();
Sys.Application.add_init(function() {
    $create(Sys.UI._Timer, {"enabled":true,"interval":1,"uniqueID":"top_right123$Timer1"}, null, null, $get("top_right123_Timer1"));
});
//]]>
</script>
</form>

<!-- BoldChat Visitor Monitor HTML v1.40 (Website=- None -,ChatWindow=- None -,ChatInvitation=xample 1) --> 
<script language="JavaScript" type="text/javascript"><!--
document.write('<scr' + 'ipt language="JavaScript" type="text/javascript" src="/web/20140408060828/http://vms.boldchat.com/aid/2178106990504207724/bc.vms/vms.js?url=' + escape(document.location.href) + '&amp;referrer=' + escape(document.referrer) + '&amp;cidid=8129246933384324650&amp;cp=http&amp;cw=640&amp;ch=480"></scr' + 'ipt>');
//-->
</script>
<noscript>
<a href="/web/20140408060828/http://www.boldchat.com/" title="Live Chat Software" target="_blank"><img alt="Live Chat Software" src="/web/20140408060828im_/http://vms.boldchat.com/aid/2178106990504207724/bc.vmi" border="0" width="1" height="1" /></a>
</noscript>
<!-- /BoldChat Visitor Monitor HTML v1.40 -->

</body>
</html>





<!--
     FILE ARCHIVED ON 6:08:28 Apr 8, 2014 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 15:31:57 Dec 16, 2015.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
-->
