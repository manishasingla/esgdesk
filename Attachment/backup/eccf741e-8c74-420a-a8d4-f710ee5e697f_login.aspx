

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head>


<script type="text/javascript" src="/static/js/analytics.js"></script>
<script type="text/javascript">archive_analytics.values.server_name="wwwb-app34.us.archive.org";archive_analytics.values.server_ms=177;</script>
<link type="text/css" rel="stylesheet" href="/static/css/banner-styles.css"/>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>
	 Xample
</title><link href="class/class.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
.style1 {color: #EEA701}
.style2 {color: #FFFFFF}
-->
</style>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/featprop.js"></script>
</head>
<body>


<!-- BEGIN WAYBACK TOOLBAR INSERT -->
<script type="text/javascript" src="/static/js/disclaim-element.js" ></script>
<script type="text/javascript" src="/static/js/graph-calc.js" ></script>
<script type="text/javascript">//<![CDATA[
var __wm = (function(imgWidth,imgHeight,yearImgWidth,monthImgWidth){
var wbPrefix = "/web/";
var wbCurrentUrl = "http://www.gates.esuk.co.uk/login.aspx";

var firstYear = 1996;
var displayDay = "12";
var displayMonth = "Feb";
var displayYear = "2010";
var prettyMonths = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
var $D=document,$=function(n){return document.getElementById(n)};
var trackerVal,curYear = -1,curMonth = -1;
var yearTracker,monthTracker;
function showTrackers(val) {
  if (val===trackerVal) return;
  var $ipp=$("wm-ipp");
  var $y=$("displayYearEl"),$m=$("displayMonthEl"),$d=$("displayDayEl");
  if (val) {
    $ipp.className="hi";
  } else {
    $ipp.className="";
    $y.innerHTML=displayYear;$m.innerHTML=displayMonth;$d.innerHTML=displayDay;
  }
  yearTracker.style.display=val?"inline":"none";
  monthTracker.style.display=val?"inline":"none";
  trackerVal = val;
}
function trackMouseMove(event,element) {
  var eventX = getEventX(event);
  var elementX = getElementX(element);
  var xOff = Math.min(Math.max(0, eventX - elementX),imgWidth);
  var monthOff = xOff % yearImgWidth;

  var year = Math.floor(xOff / yearImgWidth);
  var monthOfYear = Math.min(11,Math.floor(monthOff / monthImgWidth));
  // 1 extra border pixel at the left edge of the year:
  var month = (year * 12) + monthOfYear;
  var day = monthOff % 2==1?15:1;
  var dateString = zeroPad(year + firstYear) + zeroPad(monthOfYear+1,2) +
    zeroPad(day,2) + "000000";

  $("displayYearEl").innerHTML=year+firstYear;
  $("displayMonthEl").innerHTML=prettyMonths[monthOfYear];
  // looks too jarring when it changes..
  //$("displayDayEl").innerHTML=zeroPad(day,2);
  var url = wbPrefix + dateString + '/' +  wbCurrentUrl;
  $("wm-graph-anchor").href=url;

  if(curYear != year) {
    var yrOff = year * yearImgWidth;
    yearTracker.style.left = yrOff + "px";
    curYear = year;
  }
  if(curMonth != month) {
    var mtOff = year + (month * monthImgWidth) + 1;
    monthTracker.style.left = mtOff + "px";
    curMonth = month;
  }
}
function hideToolbar() {
  $("wm-ipp").style.display="none";
}
function bootstrap() {
  var $spk=$("wm-ipp-sparkline");
  yearTracker=$D.createElement('div');
  yearTracker.className='yt';
  with(yearTracker.style){
    display='none';width=yearImgWidth+"px";height=imgHeight+"px";
  }
  monthTracker=$D.createElement('div');
  monthTracker.className='mt';
  with(monthTracker.style){
    display='none';width=monthImgWidth+"px";height=imgHeight+"px";
  }
  $spk.appendChild(yearTracker);
  $spk.appendChild(monthTracker);

  var $ipp=$("wm-ipp");
  $ipp&&disclaimElement($ipp);
}
return{st:showTrackers,mv:trackMouseMove,h:hideToolbar,bt:bootstrap};
})(500, 27, 25, 2);//]]>
</script>
<style type="text/css">
body {
  margin-top:0 !important;
  padding-top:0 !important;
  min-width:800px !important;
}
</style>
<div id="wm-ipp" lang="en" style="display:none;">

<div style="position:fixed;left:0;top:0;width:100%!important">
<div id="wm-ipp-inside">
   <table style="width:100%;"><tbody><tr>
   <td id="wm-logo">
       <a href="/web/" title="Wayback Machine home page"><img src="/static/images/toolbar/wayback-toolbar-logo.png" alt="Wayback Machine" width="110" height="39" border="0" /></a>
   </td>
   <td class="c">
       <table style="margin:0 auto;"><tbody><tr>
       <td class="u" colspan="2">
       <form target="_top" method="get" action="/web/form-submit.jsp" name="wmtb" id="wmtb"><input type="text" name="url" id="wmtbURL" value="http://www.gates.esuk.co.uk/login.aspx" style="width:400px;" onfocus="this.focus();this.select();" /><input type="hidden" name="type" value="replay" /><input type="hidden" name="date" value="20100212065818" /><input type="submit" value="Go" /><span id="wm_tb_options" style="display:block;"></span></form>
       </td>
       <td class="n" rowspan="2">
           <table><tbody>
           <!-- NEXT/PREV MONTH NAV AND MONTH INDICATOR -->
           <tr class="m">
           	<td class="b" nowrap="nowrap">
		
		    <a href="/web/20100109070030/http://www.gates.esuk.co.uk/login.aspx" title="9 Jan 2010">JAN</a>
		
		</td>
		<td class="c" id="displayMonthEl" title="You are here: 6:58:18 Feb 12, 2010">FEB</td>
		<td class="f" nowrap="nowrap">
		
		    <a href="/web/20100315043050/http://www.gates.esuk.co.uk/login.aspx" title="15 Mar 2010"><strong>MAR</strong></a>
		
                </td>
	    </tr>
           <!-- NEXT/PREV CAPTURE NAV AND DAY OF MONTH INDICATOR -->
           <tr class="d">
               <td class="b" nowrap="nowrap">
               
                   <a href="/web/20100109070030/http://www.gates.esuk.co.uk/login.aspx" title="7:00:30 Jan 9, 2010"><img src="/static/images/toolbar/wm_tb_prv_on.png" alt="Previous capture" width="14" height="16" border="0" /></a>
               
               </td>
               <td class="c" id="displayDayEl" style="width:34px;font-size:24px;" title="You are here: 6:58:18 Feb 12, 2010">12</td>
	       <td class="f" nowrap="nowrap">
               
		   <a href="/web/20100315043050/http://www.gates.esuk.co.uk/login.aspx" title="4:30:50 Mar 15, 2010"><img src="/static/images/toolbar/wm_tb_nxt_on.png" alt="Next capture" width="14" height="16" border="0" /></a>
	       
	       </td>
           </tr>
           <!-- NEXT/PREV YEAR NAV AND YEAR INDICATOR -->
           <tr class="y">
	       <td class="b" nowrap="nowrap">
               
                   2009
               
               </td>
               <td class="c" id="displayYearEl" title="You are here: 6:58:18 Feb 12, 2010">2010</td>
	       <td class="f" nowrap="nowrap">
               
                   2011
               
	       </td>
           </tr>
           </tbody></table>
       </td>
       </tr>
       <tr>
       <td class="s">
           <a class="t" href="/web/20100212065818*/http://www.gates.esuk.co.uk/login.aspx" title="See a list of every capture for this URL">14 captures</a>
           <div class="r" title="Timespan for captures of this URL">6 Apr 09 - 29 May 10</div>
       </td>
       <td class="k">
       <a href="" id="wm-graph-anchor">
       <div id="wm-ipp-sparkline" title="Explore captures for this URL">
	 <img id="sparklineImgId" alt="sparklines"
		 onmouseover="__wm.st(1)" onmouseout="__wm.st(0)"
		 onmousemove="__wm.mv(event,this)"
		 width="500"
		 height="27"
		 border="0"
		 src="/web/jsp/graph.jsp?graphdata=500_27_1996:-1:000000000000_1997:-1:000000000000_1998:-1:000000000000_1999:-1:000000000000_2000:-1:000000000000_2001:-1:000000000000_2002:-1:000000000000_2003:-1:000000000000_2004:-1:000000000000_2005:-1:000000000000_2006:-1:000000000000_2007:-1:000000000000_2008:-1:000000000000_2009:-1:000212120001_2010:1:111110000000_2011:-1:000000000000_2012:-1:000000000000_2013:-1:000000000000_2014:-1:000000000000_2015:-1:000000000000" />
       </div>
       </a>
       </td>
       </tr></tbody></table>
   </td>
   <td class="r">
       <a href="#close" onclick="__wm.h();return false;" style="background-image:url(/static/images/toolbar/wm_tb_close.png);top:5px;" title="Close the toolbar">Close</a>
       <a href="http://faq.web.archive.org/" style="background-image:url(/static/images/toolbar/wm_tb_help.png);bottom:5px;" title="Get some help using the Wayback Machine">Help</a>
   </td>
   </tr></tbody></table>
</div>
</div>
</div>
<script type="text/javascript">__wm.bt();</script>
<!-- END WAYBACK TOOLBAR INSERT -->

    <form name="form1" method="post" action="login.aspx" id="form1">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUJMzM5ODMxODA1DxYCHglSZXR1cm5VcmxkFgICAw9kFgQCEQ8PFgIeBFRleHRlZGQCFQ9kFgQCAQ8QD2QWAh4Ib25jaGFuZ2UFFnJldHVybiBjaGFuZ2VWYWx1ZXMoKTtkZGQCAw8QDxYGHg1EYXRhVGV4dEZpZWxkBQRBcmVhHg5EYXRhVmFsdWVGaWVsZAUEQXJlYR4LXyFEYXRhQm91bmRnZBAVCgNBbnkJQmF0dGVyc2VhDENhbmFkYSBXYXRlchBFYXN0IEZpbmNobGV5IE4yE0ZpbmNobGV5IENlbnRyYWwgTjMLRmluY2hsZXkgTjMUSGFtcHN0ZWFkIFN1YnVyYnMgTjILSGlnaGdhdGUgTjYLUm90aGVyaGl0aGUMU3VycmV5IFF1YXlzFQoDQW55CUJhdHRlcnNlYQxDYW5hZGEgV2F0ZXIQRWFzdCBGaW5jaGxleSBOMhNGaW5jaGxleSBDZW50cmFsIE4zC0ZpbmNobGV5IE4zFEhhbXBzdGVhZCBTdWJ1cmJzIE4yC0hpZ2hnYXRlIE42C1JvdGhlcmhpdGhlDFN1cnJleSBRdWF5cxQrAwpnZ2dnZ2dnZ2dnZGQYAQUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgQFCnN0cmVldHZpZXcFDHN1Ym1pdGJ1dHRvbgUXUXVpY2tTZWFyY2gxJHJlbnRTZWFyY2gFGlF1aWNrU2VhcmNoMSRyZW50U2hvd09uTWFwL0jw4j0YwOuempPG8zXkJPmwi7w=" />

    <div id="wrapper"><table width="100%" height="171" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="right" valign="middle">
        
<link href="/web/20100212065818cs_/http://www.gates.esuk.co.uk/class/class.css" rel="stylesheet" type="text/css" />
<table border="0" cellpadding="0" cellspacing="0" width="100%" >
<tr>
    <td>
    <span class="logo"><img src="images/Xample.jpg" width="236" height="63" /></span>
      <div class="call">
        <div style="margin-top:55px; height:22; color:#FFFFFF; font-size:12px; font-weight:bold; padding-right:14px; text-decoration:underline; cursor:pointer; "><span onclick="window.open('/web/20100212065818/http://livechat.boldchat.com/aid/2178106990504207724/bc.chat?cwdid=848434019943815447&amp;wdid=3507451889334081925&amp;vr=&amp;vn=&amp;vi=&amp;ve=&amp;vp=&amp;iq=&amp;url=','','scrollbars=yes,width=500,height=500')" >Chat live</span><img src="images/chat_live.gif" style="padding-top:3px" width="28" height="22" onclick="window.open('/web/20100212065818/http://livechat.boldchat.com/aid/2178106990504207724/bc.chat?cwdid=848434019943815447&amp;wdid=3507451889334081925&amp;vr=&amp;vn=&amp;vi=&amp;ve=&amp;vp=&amp;iq=&amp;url=','','scrollbars=yes,width=500,height=500')" /></div>
      </div>
      <div class="topmenu">
        <table height="30">
          <tr>
            <td valign="bottom" style="color:#790000; font-size:14px;"><a href="index.aspx" class="topmenu_font">&nbsp;&nbsp;&nbsp;Home</a>|<a href="buy.aspx" class="topmenu_font">&nbsp;&nbsp;&nbsp;Buy</a>|<a href="rent.aspx" class="topmenu_font">&nbsp;&nbsp;&nbsp;Rent</a>|<a href="Mortgage.aspx" class="topmenu_font">&nbsp;&nbsp;&nbsp;Mortgages</a>|<a href="vendors.aspx" class="topmenu_font" >&nbsp;&nbsp;&nbsp;Vendors</a>|<a href="landlords.aspx" class="topmenu_font">&nbsp;&nbsp;&nbsp;Landlords</a>|<a href="aboutus.aspx" class="topmenu_font">&nbsp;&nbsp;&nbsp;About us</a>|<a href="contactus.aspx" class="topmenu_font">&nbsp;&nbsp;&nbsp;Contact us</a>|<a href="register.aspx" class="topmenu_font">&nbsp;&nbsp;&nbsp;Register</a>|<a href="favourites.aspx" class="topmenu_font">&nbsp;&nbsp;&nbsp;My favourites</a> </td>
          </tr>
        </table>
      </div>
    </td>
</tr>

</table>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="26%" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td colspan="2"><img src="images/banner_1.jpg" width="702" height="280" border="0" /></td>
            <td width="0%">&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td height="27" colspan="2" valign="top" style="border-bottom:1px dotted #666666"><span class="titel">&nbsp;Login</span></td>
            <td>&nbsp;</td>
          </tr>
          <tr><td>&nbsp;</td></tr>
          <tr>
            <td width="31%" valign="top"><table width="47%" height="254" border="0" align="left" cellpadding="0" cellspacing="0">
             <tr>
                <td height="130" align="center" valign="top"><input type="image" name="streetview" id="streetview" src="images/streetview.jpg" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;streetview&quot;, &quot;&quot;, true, &quot;&quot;, &quot;&quot;, false, false))" border="0" /></td>
              </tr>
              <tr>
                <td height="121" align="left" valign="top"><img src="images/index2_16.jpg" /></td>
              </tr>
              <tr>
                <td height="55"><img src="images/register.jpg" width="217" height="114" border="0" style="cursor:pointer;" onclick="document.location='register.aspx';" /></td>
              </tr>
              <tr>
                <td height="19"></td>
              </tr>
            </table>               &nbsp;</td>
            <td width="69%" align="left" valign="top">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                    <div>
            <table border="0" cellpadding="2" cellspacing="0">
            <tr>
                <td align="left" valign="middle" style="font-size:13px; color:#08175F;">Choose Language </td>
                <td width="20" valign="bottom" align="left"><span onclick="location.href='http://' + 'gates.esuk.co.uk/login.aspx';return false;" style="cursor:pointer"><img title="Ingl�s" border="0" src="images/flags/gb.gif" height="16" width="20"></span></td>
                <td width="20" valign="bottom" align="left"><span onclick="location.href='http://' + 'translate.google.com/translate_c?langpair=en|de&amp;u=/web/20100212065818/http://gates.esuk.co.uk/login.aspx';return false;" style="cursor:pointer" ><img title="Alem�n" border="0" src="images/flags/de.gif" alt="Alem�n" height="16" width="20"></span></td>
                <td width="20" valign="bottom" align="left"><span onclick="location.href='http://' +'translate.google.com/translate_c?langpair=en|fr&amp;u=/web/20100212065818/http://gates.esuk.co.uk/login.aspx';return false;" style="cursor:pointer"><img title="Franc�s" border="0" src="images/flags/fr.gif" alt="Franc�s" height="16" width="20"></span></td>
                <td width="20" valign="bottom" align="left"><span onclick="location.href='http://' +'translate.google.com/translate_c?langpair=en|es&amp;u=/web/20100212065818/http://gates.esuk.co.uk/login.aspx';return false;" style="cursor:pointer"><img title="Espa�ol" border="0" src="images/flags/es.gif" alt="Espa�ol" height="16" width="20"></span></td>
                <td width="20" valign="bottom" align="left"><span onclick="location.href='http://' + 'translate.google.com/translate_c?langpair=en|pt&amp;u=/web/20100212065818/http://gates.esuk.co.uk/login.aspx';return false;" style="cursor:pointer"><img title="Portugu�s" border="0" src="images/flags/pt.gif" alt="Portugu�s" height="16" width="20"></span></td>
                <td width="20" valign="bottom" align="left"><span onclick="location.href='http://' +'translate.google.com/translate_c?langpair=en|nl&amp;u=/web/20100212065818/http://gates.esuk.co.uk/login.aspx';return false;" style="cursor:pointer"><img title="Holand�s" border="0" src="images/flags/nl.gif" alt="Holand�s" height="16" width="20"></span></td>
                <td width="20" valign="bottom" align="left"><span onclick="location.href='http://' +'translate.google.com/translate_c?langpair=en|zh&amp;u=/web/20100212065818/http://gates.esuk.co.uk/login.aspx';return false;" style="cursor:pointer"><img title="Chino" border="0" src="images/flags/ch.jpg" alt="Chino" height="16" width="20"></span></td>
	            <td width="20" valign="bottom" align="left"><span onclick="location.href='http://' +'translate.google.com/translate_c?langpair=en|it&amp;u=/web/20100212065818/http://gates.esuk.co.uk/login.aspx';return false;" style="cursor:pointer"><img title="Italiano" border="0" src="images/flags/it.gif" alt="Italiano" height="16" width="20"></span></td>
                <td align="left" valign="bottom" width="20"><span onclick="location.href='http://' +'translate.google.com/translate_c?langpair=en|ar&amp;u=/web/20100212065818/http://gates.esuk.co.uk/login.aspx';return false;" style="cursor:pointer"><img title="Arabic" border="0" src="images/flags/ar.gif" alt="Arabic" height="16" width="20"></span></td>
                <td align="left" valign="bottom" style="padding-left:2px;padding-right:2px;"><span onclick="location.href='http://' +'translate.google.com/translate_c?langpair=en|iw&amp;u=/web/20100212065818/http://gates.esuk.co.uk/login.aspx';return false;" style="cursor:pointer">Hebrew</span></td>
                <td align="left" valign="bottom" style="padding-left:2px;padding-right:2px;"><span onclick="alert('This translation is not yet posible but is coming soon.');return false;" style="cursor:pointer">Urdu</span></td>
                <td align="left" valign="bottom" style="padding-left:2px;padding-right:2px;"><span onclick="alert('This translation is not yet posible but is coming soon.');return false;" style="cursor:pointer">Bengal</span></td>
	        </tr>
	        </table>
        </div>
                    </td>
                </tr>
              <tr>
                <td height="61"><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis blandit tempor   velit. Quisque ultricies bibendum elit. Praesent mi risus, pharetra dictum,   feugiat eu, blandit id, orci.</strong> Phasellus mollis orci at lacus.</td>
                </tr>
              <tr>
                <td><table width="449" border="0" align="center" cellpadding="0" cellspacing="4" bgcolor="#F7F7F9" style="font-size:11px;">
                    <tr>
                      <td width="99" height="35" align="left" class="contact_us">User name:</td>
                      <td width="338" align="left" style="padding-left:23px;"><input name="txtUserName" type="text" id="txtUserName" style="width:300px;" class="Login" />
                        &nbsp;
                        &nbsp;
                        &nbsp;</td>
                    </tr>
                    <tr>
                      <td height="34" align="left" class="contact_us">Password:</td>
                      <td style="padding-left:23px;" align="left"><input name="txtPassword" type="password" id="txtPassword" style="width:300px;" class="Login" />
                        &nbsp;
                        &nbsp;</td>
                    </tr>
                </table></td>
                </tr>
              <tr>
                <td height="55"><div style="float:left; width:20%">
                    <input type="image" name="submitbutton" id="submitbutton" src="images/btn_login.png" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;submitbutton&quot;, &quot;&quot;, true, &quot;&quot;, &quot;&quot;, false, false))" border="0" />            
                </div>
                    <div style="float:left;text-align:justify; padding-left:23px; width:70%">
                      <span id="lblMessage"><font color="Red" size="3"></font></span>
                  </div></td>
                </tr>
              <tr>
                <td height="33" align="left"><br />
                  New user? <a href="register.aspx" style="color:#EDA400; text-decoration:underline; font-weight:bold; padding:0; margin:0;">Click here</a> to register</td>
                </tr>
            </table></td>
            <td>&nbsp;</td>
          </tr>
        </table></td>
        <td width="74%" valign="top">
        <div id="leftcol">
            

<script type="text/javascript" language="javascript">
function changeValues()
{
   var obj1 = document.getElementById("QuickSearch1_PropType").value;
       
    if(obj1 == '1')
        {
            var ddlOrders = document.getElementById("QuickSearch1_rentMinPrice");
            for (var count = ddlOrders.options.length-1; count >-1; count--)
                {
                    ddlOrders.options[count] = null;
                }
               
            text = 'Min';
            text1 = '0';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 50k';
            text1 = '50000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 60k';
            text1 = '60000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 70k';
            text1 = '70000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 80k';
            text1 = '80000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 90k';
            text1 = '90000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 100k';
            text1 = '100000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 150k';
            text1 = '150000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 200k';
            text1 = '200000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 250k';
            text1 = '250000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
              
             text = '\u00A3 300k';
            text1 = '300000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem; 
             
             text = '\u00A3 350k';
            text1 = '350000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;  
            
             text = '\u00A3 450k';
            text1 = '450000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem; 
             
             text = '\u00A3 500k';
            text1 = '500000';
            listItem = new Option(text, text1, false, false)    
            ddlOrders.options[ddlOrders.length] = listItem;  
            
             text = '\u00A3 550k';
            text1 = '550000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 600k';
            text1 = '600000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 650k';
            text1 = '650000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 700k';
            text1 = '700000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 750k';
            text1 = '750000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 800k';
            text1 = '800000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 850k';
            text1 = '850000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
               text = '\u00A3 900k';
            text1 = '900000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
             text = '\u00A3 1m';
            text1 = '1000000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            
            
            var ddlOrders2 = document.getElementById("QuickSearch1_rentMaxPrice");
            for (var count = ddlOrders2.options.length-1; count >-1; count--)
                {
                    ddlOrders2.options[count] = null;
                }
                
            text = 'Max';
            text1 = '0';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
             text = '\u00A3 50k';
            text1 = '50000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 60k';
            text1 = '60000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 70k';
            text1 = '70000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 80k';
            text1 = '80000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 90k';
            text1 = '90000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 100k';
            text1 = '100000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 150k';
            text1 = '150000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 200k';
            text1 = '200000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 250k';
            text1 = '250000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
              
             text = '\u00A3 300k';
            text1 = '300000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem; 
             
             text = '\u00A3 350k';
            text1 = '350000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;  
            
             text = '\u00A3 450k';
            text1 = '450000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem; 
             
             text = '\u00A3 500k';
            text1 = '500000';
            listItem = new Option(text, text1, false, false)    
            ddlOrders2.options[ddlOrders2.length] = listItem;  
            
             text = '\u00A3 550k';
            text1 = '550000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 600k';
            text1 = '600000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
             text = '\u00A3 650k';
            text1 = '650000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
             text = '\u00A3 700k';
            text1 = '700000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
             text = '\u00A3 750k';
            text1 = '750000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
             text = '\u00A3 800k';
            text1 = '800000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
             text = '\u00A3 850k';
            text1 = '850000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
               text = '\u00A3 900k';
            text1 = '900000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
             text = '\u00A3 1m';
            text1 = '1000000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            GetBuyLocations();
                   
        }
        else if(obj1 == '2')
        {
            var ddlOrders = document.getElementById("QuickSearch1_rentMinPrice");
            for (var count = ddlOrders.options.length-1; count >-1; count--)
            {
                    ddlOrders.options[count] = null;
            }
            text = 'Min';
            text1 = '0';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 100 per week';
            text1 = '100';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 200 per week';
            text1 = '200';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 300 per week';
            text1 = '300';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 400 per week';
            text1 = '400';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            text = '\u00A3 500 per week';
            text1 = '500';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            text = '\u00A3 600 per week';
            text1 = '600';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            text = '\u00A3 750 per week';
            text1 = '750';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 1000 per week';
            text1 = '1000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            
            var ddlOrders2 = document.getElementById("QuickSearch1_rentMaxPrice");
            for (var count = ddlOrders2.options.length-1; count >-1; count--)
            {
                    ddlOrders2.options[count] = null;
            }
            text = 'Max';
            text1 = '0';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 100 per week';
            text1 = '100';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 200 per week';
            text1 = '200';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 300 per week';
            text1 = '300';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 400 per week';
            text1 = '400';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 500 per week';
            text1 = '500';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 600 per week';
            text1 = '600';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 750 per week';
            text1 = '750';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            text = '\u00A3 1000 per week';
            text1 = '1000';
            listItem = new Option(text, text1, false, false);
            ddlOrders2.options[ddlOrders2.length] = listItem;
            
            GetRentLocations();
        }
        else

        {
            var ddlOrders = document.getElementById("QuickSearch1_rentMaxPrice");
            for (var count = ddlOrders.options.length-1; count >-1; count--)
                {
                    ddlOrders.options[count] = null;
                }
           text = 'Max';
            text1 = '0';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 60k';
            text1 = '60000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 70k';
            text1 = '70000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 80k';
            text1 = '30000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 90k';
            text1 = '90000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 100k';
            text1 = '100000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 150k';
            text1 = '150000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 200k';
            text1 = '200000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 250k';
            text1 = '250000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            text = '\u00A3 300k';
            text1 = '300000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem; 
            
             text = '\u00A3 300k';
            text1 = '300000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem; 
             
             text = '\u00A3 350k';
            text1 = '350000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;  
            
             text = '\u00A3 450k';
            text1 = '450000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem; 
             
             text = '\u00A3 500k';
            text1 = '500000';
            listItem = new Option(text, text1, false, false)    
            ddlOrders.options[ddlOrders.length] = listItem;  
            
             text = '\u00A3 550k';
            text1 = '550000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 600k';
            text1 = '600000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 650k';
            text1 = '650000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 700k';
            text1 = '700000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 750k';
            text1 = '750000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 800k';
            text1 = '800000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
             text = '\u00A3 850k';
            text1 = '850000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
               text = '\u00A3 900k';
            text1 = '900000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
               text = '\u00A3 1m';
            text1 = '1000000';
            listItem = new Option(text, text1, false, false);
            ddlOrders.options[ddlOrders.length] = listItem;
            
            GetBuyLocations();
            
            }
}

function GetBuyLocations()
{

            var buyLocations = new Array();
            
            buyLocations = document.getElementById("QuickSearch1_hdnBuyLocations").value.split(',');
            
            var text; 
            var text1;
            var listItem;
            
            var ddlOrders = document.getElementById("QuickSearch1_rentArea");
            
//            for (var count = ddlOrders.options.length-1; count >-1; count--)
//            {
//                ddlOrders.options[count] = null;
//            }

            if(ddlOrders.options.length > 0)
            {
                for(var i = ddlOrders.options.length - 1; i >= 0; i--)
                {
                    ddlOrders.remove(i);
                }
            }
            
            for (var count = 0; count < buyLocations.length; count++)
            {
                text = buyLocations[count];
                text1 =  buyLocations[count];
                listItem = new Option(text, text1, false, false);
                ddlOrders.options[ddlOrders.options.length] = listItem;
            }       
}

function GetRentLocations()
{
            
           var rentLocations = new Array();
            
            rentLocations = document.getElementById("QuickSearch1_hdnRentLocations").value.split(',');
            
            var text; 
            var text1;
            var listItem;
            
            var ddlOrders = document.getElementById("QuickSearch1_rentArea");
            
//            for (var count = ddlOrders.options.length-1; count >-1; count--)
//            {
//                ddlOrders.options[count] = null;
//            }
            
            if(ddlOrders.options.length > 0)
            {
                for(var i = ddlOrders.options.length - 1; i >= 0; i--)
                {
                    ddlOrders.remove(i);
                }
            }
            
            for (var count = 0; count < rentLocations.length; count++)
            {
                text = rentLocations[count];
                text1 =  rentLocations[count];
                listItem = new Option(text, text1, false, false);
                ddlOrders.options[ddlOrders.options.length] = listItem;
            }       
}
</script>
<div >
    <table width="96%" border="0" cellpadding="0" cellspacing="0" bgcolor="#EBEBEB">
            <tr>
              <td align="center" valign="middle" style="height: 332px"><div id="search">
                <h3></h3>
                <div style="margin: -10px 0 0 0; padding-left:10px; padding-right:10px;">
                  <table width="98%" height="239" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                      <td height="22" align="left"><strong>Type:</strong></td>
                    </tr>
                    <tr>
                      <td height="22" align="left"><select name="QuickSearch1$PropType" id="QuickSearch1_PropType" onchange="return changeValues();" style="width:199px; height:23px;">
	<option value="1">Buy</option>
	<option value="2">Rent</option>

</select></td>
                    </tr>
                    <tr>
                      <td height="22" align="left"><strong>Location:</strong></td>
                      </tr>
                    <tr>
                      <td><select name="QuickSearch1$rentArea" id="QuickSearch1_rentArea" style="width:199px; height:23px;">
	<option value="Any">Any</option>
	<option value="Battersea">Battersea</option>
	<option value="Canada Water">Canada Water</option>
	<option value="East Finchley N2">East Finchley N2</option>
	<option value="Finchley Central N3">Finchley Central N3</option>
	<option value="Finchley N3">Finchley N3</option>
	<option value="Hampstead Suburbs N2">Hampstead Suburbs N2</option>
	<option value="Highgate N6">Highgate N6</option>
	<option value="Rotherhithe">Rotherhithe</option>
	<option value="Surrey Quays">Surrey Quays</option>

</select></td>
                      </tr>
                    <tr>
                      <td height="22" align="left"><strong>Bedrooms:</strong></td>
                      </tr>
                    <tr>
                      <td><select name="QuickSearch1$rentMinBedrooms" id="QuickSearch1_rentMinBedrooms" style="width:199px; height:23px;">
	<option selected="selected" value="-1">Any</option>
	<option value="0">Studio</option>
	<option value="1">1</option>
	<option value="2">2</option>
	<option value="3">3</option>
	<option value="4">4</option>
	<option value="99">5+</option>

</select></td>
                      </tr>
                    <tr>
                      <td height="22" align="left"><label for="label"><strong>Min Price</strong></label></td>
                      </tr>
                    <tr>
                      <td><select name="QuickSearch1$rentMinPrice" id="QuickSearch1_rentMinPrice" style="width:190px; height:23px;">
	<option value="0">Min</option>
	<option value="50000">&#163;50k</option>
	<option value="60000">&#163;60k</option>
	<option value="70000">&#163;70k</option>
	<option value="80000">&#163;80k</option>
	<option value="90000">&#163;90k</option>
	<option value="100000">&#163;100k</option>
	<option value="150000">&#163;150k</option>
	<option value="200000">&#163;200k</option>
	<option value="250000">&#163;250k</option>
	<option value="300000">&#163;300k</option>
	<option value="350000">&#163;350k</option>
	<option value="450000">&#163;450k</option>
	<option value="500000">&#163;500k</option>
	<option value="550000">&#163;550k</option>
	<option value="600000">&#163;600k</option>
	<option value="650000">&#163;650k</option>
	<option value="700000">&#163;700k</option>
	<option value="750000">&#163;750k</option>
	<option value="800000">&#163;800k</option>
	<option value="850000">&#163;850k</option>
	<option value="900000">&#163;900k</option>
	<option value="1000000">&#163;1m</option>

</select></td>
                      </tr>
                    <tr>
                      <td height="22" align="left"><label for="service"><strong>Max. Price: </strong></label></td>
                      </tr>
                    <tr>
                      <td height="32" valign="top"><select name="QuickSearch1$rentMaxPrice" id="QuickSearch1_rentMaxPrice" style="width:190px; height:23px;">
	<option value="0">Max</option>
	<option value="50000">&#163;50k</option>
	<option value="60000">&#163;60k</option>
	<option value="70000">&#163;70k</option>
	<option value="80000">&#163;80k</option>
	<option value="90000">&#163;90k</option>
	<option value="100000">&#163;100k</option>
	<option value="150000">&#163;150k</option>
	<option value="200000">&#163;200k</option>
	<option value="250000">&#163;250k</option>
	<option value="300000">&#163;300k</option>
	<option value="350000">&#163;350k</option>
	<option value="450000">&#163;450k</option>
	<option value="500000">&#163;500k</option>
	<option value="550000">&#163;550k</option>
	<option value="600000">&#163;600k</option>
	<option value="650000">&#163;650k</option>
	<option value="700000">&#163;700k</option>
	<option value="750000">&#163;750k</option>
	<option value="800000">&#163;800k</option>
	<option value="850000">&#163;850k</option>
	<option value="900000">&#163;900k</option>
	<option value="1000000">&#163;1m</option>

</select></td>
                      </tr>
                    
                    <tr>
                      <td  height="19" colspan="3" align="left" valign="top"><input type="image" name="QuickSearch1$rentSearch" id="QuickSearch1_rentSearch" src="images/btn_searchproperties.gif" border="0" />
                      <span style="padding-bottom:5px;"><input name="QuickSearch1$rentShowOnMap" type="checkbox" id="QuickSearch1_rentShowOnMap" /><span style="padding-bottom:5px;"><strong>By map</strong></span></span></tr>
                  </table>
                </div>
                <div style="background-color:#FFFFFF;"><img src="images/img_srcbottom.gif" alt="" width="220" height="14" /> </div>
              </div></td>
            </tr>
          </table></div>
        <input type="hidden" name="QuickSearch1$hdnBuyLocations" id="QuickSearch1_hdnBuyLocations" value="Any,Battersea,Canada Water,East Finchley N2,Finchley Central N3,Finchley N3,Hampstead Suburbs N2,Highgate N6,Rotherhithe,Surrey Quays" />
        <input type="hidden" name="QuickSearch1$hdnRentLocations" id="QuickSearch1_hdnRentLocations" value="Any,All,Archway N19,Battersea,Canada Water,East Finchley N2,Finchley Central N3,Finchley N3,Hampstead Suburbs N2,Highgate N6,Rotherhithe,Surrey Quays" />
            
<script type="text/javascript" language="javascript" src="js/featprop.js"></script>
<table width="96%" border="0" cellpadding="0" cellspacing="0" bgcolor="#EBEBEB">
            <tr>
              <td height="39" align="center" valign="middle" ><img src="images/feature_p.jpg" width="220" height="39" /></td>
            </tr>
            <tr>
              <td height="180" style="padding-top:5px;" align="center" valign="middle" id="futurebg"><table width="82%" height="180" border="0" align="center" cellpadding="0" cellspacing="0">
                  <tr>
                    <td id="imganddetails" height="138" width="156" align="left" style="margin-bottom:5px;" valign="top"><img src="images/image_soon-copy.jpg" width="120" height="120" /></td>
                  </tr>
                  <tr>
                    <td align="left"><div ><span id="divPropAddress" style="font-size:14px; color:#770000; font-family:Georgia, Times New Roman, Times, serif; text-align:left; "  > </span><br />
                            <div id="divCost" style="font-size:11px; color:#770000; font-family:Georgia, Times New Roman, Times, serif;  text-align:left; "></div>
                    </div></td>
                  </tr><br />
                   <tr>
                    <td height="19" align="right" ><a id="buymoredetails" href="#" style="font-size:12px; text-decoration:underline; font-weight:normal; text-align:right; color:#000000">More details</a></td>
                  </tr>
              </table>
              </td>
            </tr>
            <tr>
              <td><img src="images/future_bo.jpg" width="220" height="11" /></td>
            </tr>
</table>
        </div>
        </td>
      </tr>
    </table></td>
  </tr>
</table>
</div>


<div id="footer" style="text-align:left; "><br />
<table width="73%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="52%"><span style="font-size:11px;text-decoration:none; font-weight:normal;color:#790000;cursor:pointer" onclick="window.open('privacylaw.htm','','status=yes,scrollbars=yes,width=660,height=650')">Privacy Law</span>|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size:11px;text-decoration:none; font-weight:normal;color:#790000;cursor:pointer" onclick="window.open('Disclaimer.htm','','status=yes,scrollbars=yes,width=660,height=509')">Disclaimer</span></td>
    <td width="48%" align="right">&nbsp;</td>
    </tr>
</table><div style="width:200px;"></div>
</div>
<script language="javascript" type="text/javascript">
        if (top.location != self.location)
        {
            top.location.replace(self.location); // pop to top
        }
</script>
</form>
</body>
</html>





<!--
     FILE ARCHIVED ON 6:58:18 Feb 12, 2010 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 16:00:40 Dec 16, 2015.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
-->
