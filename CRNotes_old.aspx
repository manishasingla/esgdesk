<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CRNotes.aspx.cs" Inherits="CRNotes" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Client notes</title>
    
     <script type="text/javascript" language="javascript">
            
                    
     function ActiveTabChanged(sender, e)
     
      {
 
       var index = $find("TabContainer1").get_activeTabIndex();
    
      document.getElementById("hdnCategories").value = index;
                  
    document.getElementById("BtnRdbCat").click();
             
      }
 </script>

</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="smanager" runat="server"></asp:ScriptManager>
     
        <div id="CompanyNote" runat="server" style="background-color: #ffffff; text-align:center;">
     
        <table style="background-color: #ffffff;float:left; font-weight:bold; font-size:16px; border:solid 1px black;  width:100%; height:100%"><tr><td colspan="2" align="left" valign="middle" style="width:100%; padding-bottom:10px; padding-top:10px;  border:solid 1px black; font-size:18px; background-color: #e9e9e9 ">
            Client notes</td></tr>
            
         
            <tr><td colspan="2" align="left" style="width:100%; padding-top:10px">
            
          <cc1:TabContainer ID="TabContainer1" runat="server" Width="100%" AutoPostBack="true" OnClientActiveTabChanged="ActiveTabChanged" OnActiveTabChanged="TabContainer1_ActiveTabChanged"  EnableViewState="true">
         
          
        </cc1:TabContainer>
            </td></tr>
          
           <tr><td colspan="2" align="left" valign="top" style="width:100%; border:solid 1px red;  display:none"><FTB:FreeTextBox ID="FtbCompNotes" runat="server" Height="625px"  Width="100%">
                                </FTB:FreeTextBox></td></tr><tr>
                                <td align="left" ><div id="div1" runat="server" style="color:Red"></div></td><td align="right" style="padding-right:8px">
                                    <asp:HiddenField id="hdnProjectCompany" runat="server"></asp:HiddenField>
                                <asp:HiddenField ID="hdnCategories" runat="server" Value="All" />
                                    <asp:CheckBox ID="chkEmailnotify" runat="server" Checked="true" Font-Bold="True"
                                        Text="Email notify" />&nbsp;
                                          <asp:Button id="BtnRdbCat" runat="server" Text="" BackColor="transparent" BorderStyle="none" OnClick="BtnRdbCat_Click" />
                                    <asp:Button id="btnote" runat="server" Text="Update" Enabled="false" OnClick="btnote_Click"></asp:Button>
                                    <asp:Button ID="btnRemove" runat="server" OnClick="btnRemove_Click" Text="Delete" />
                                  </td></tr></table>
                               
  
                                </div>

    </form>
    </body>
</html>
