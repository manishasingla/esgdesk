<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ClientHeader.ascx.cs" Inherits="ClientHeader" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>
<div style="background-color:InactiveBorder">
   
    <div style="text-align:left;vertical-align:top;background-color:#e9e9e9" id="divHeader2" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
       <tr>
        <td width="85%" style="padding-left:4px">
           <asp:LinkButton ID="myRequests" runat="server" Text="My requests" style="cursor:pointer;" ForeColor="black" onmouseover="this.style.color='blue'" onmouseout="this.style.color='black'"  CausesValidation="False" OnClick="myRequests_Click" ></asp:LinkButton> &nbsp;&nbsp;<a href="changerequest.aspx" style="cursor:pointer; color:black"  onmouseover="this.style.color='blue'" onmouseout="this.style.color='black'" >Add new request</a> &nbsp;&nbsp;<a href="edit_registration.aspx" id="EditRegistration" style="cursor:pointer; color:black"  onmouseover="this.style.color='blue'" onmouseout="this.style.color='black'" runat="server">Edit registration</a> &nbsp;&nbsp;<a href="change_password.aspx" id="ChangePassword" style="cursor:pointer; color:black"  onmouseover="this.style.color='blue'" onmouseout="this.style.color='black'" runat="server">Change password</a> &nbsp;&nbsp;<asp:LinkButton ID="lnkLogout" runat="server" style="cursor:pointer; color:black; display:none"  onmouseover="this.style.color='blue'" onmouseout="this.style.color='black'" Visible="false" CausesValidation="False" OnClick="lnkLogout_Click">Logout</asp:LinkButton> &nbsp;&nbsp;<asp:LinkButton ID="CRNotes" runat="server" Text="" style="cursor:pointer; color:black"  onmouseover="this.style.color='blue'" onmouseout="this.style.color='black'" Visible="false" OnClick="CRNotes_Click" CausesValidation="False" ></asp:LinkButton>
            <asp:Label ID="LblCompany" Visible="false" runat="server" Text=""></asp:Label><asp:LinkButton ID="Chat" runat="server" Text="Chat with support staff" style="cursor:pointer; color:black"  onmouseover="this.style.color='blue'" onmouseout="this.style.color='black'" CausesValidation="False" ></asp:LinkButton>&nbsp;&nbsp;<asp:LinkButton ID="LnkRmtDsktp" runat="server" Text="Remote desktop session" style="cursor:pointer; color:black"  onmouseover="this.style.color='blue'" onmouseout="this.style.color='black'"  CausesValidation="False"  ></asp:LinkButton>
             
            </td>
        <td align="right" width="15%">
            <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>Request Id:&nbsp;
                </td>
                <td>
                    <asp:TextBox ID="txtRequestId" runat="server" Width="50px"></asp:TextBox>
                    &nbsp;
                </td>
                <td>
                    <asp:Button ID="btnGo" runat="server" Text="Go" CausesValidation="False" OnClick="btnGo_Click" /></td>
            </tr>
            </table>
            <cc1:filteredtextboxextender id="FilteredTextBoxExtender1" runat="server" filtertype="Numbers"
                targetcontrolid="txtRequestId"></cc1:filteredtextboxextender>
        </td>
       </tr>       
       </table>          
    </div>
    
     <div style="text-align:right;padding:5px; background-color:Gainsboro">
    <table width="0" cellpadding="0" cellspacing="0" style="width:100%">
    <tr>
    <td align="left" valign="middle" width="20%">
        <span id="pageHeader">
           <img src="Exchange-2007-Icon-pack-thumb.png" style="width: 35px; height: 35px" />
            <asp:Label ID="lblTitle" runat="server" Text="ESG Desk - ESG Desk"></asp:Label></span>
            
    </td>
        <td width="60%" valign="middle" align="right">
        <a ID="lnkLogin" runat="server" style="display:none" href="login.aspx">Client Login</a>
        &nbsp;&nbsp;<a ID="lnkAdminLogin" runat="server" style="display:none" href="Admin/login.aspx">Support login</a>
        &nbsp;&nbsp;<a id="lnkRegister"  runat="server" style="display:none;"  href="register.aspx">Register</a>

        &nbsp;&nbsp;<a id="lnkUserName"  runat="server" href="#" target="_blank"></a>
        
        <span id="SpnSupportlnk" runat="server" style="font-size:11px;" >Please call support to enable these links (subscription based).Click <a onclick="window.open('http://www.request.estatesolutions.eu/Support_and_maintenance_contracts.pdf','','')" style="cursor:pointer" >here</a>  to download packages details.</span>
            &nbsp; <asp:LinkButton ID="LinkSupport" runat="server" Text="Support" Font-Underline="true" CausesValidation="False" ></asp:LinkButton>
     
            <span style="display:none"><a id="CompNotesIcon" runat="server" title="Click to view notes" style="border:none; font-size:16px; color:Red; padding-left:5px; text-decoration:none; cursor:pointer;"></a></span>
       </td>
        <td align="right" valign="top" width="30%">
        <a class="addthis_button" style="text-align:left;" href="http://www.addthis.com/bookmark.php?v=250&amp;pub=esatesolutions"><img src="http://s7.addthis.com/static/btn/lg-share-en.gif" alt="Bookmark and Share" width="125" height="16" align="right" style="border:0"/></a><br />
        Call: 020 7609 2800<br />
        </td>
    </tr>
        <tr>
            <td align="left" valign="top" width="20%">
            </td>
            <td align="right" style="padding-right: 20px" valign="top" width="60%">
            </td>
            <td align="right" valign="top" width="20%">
            </td>
        </tr>
    </table>        
    </div>    
</div>