﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;


public partial class ClientNote : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string strtrace = "",Qryproject="",newvalue="";
        DataSet dsproject = null;
        if (Session["clientid"] == null || Session["username"] == null || Session["clientid"].ToString() == "" || Session["username"].ToString() == "")
        {
            Session["returnUrl"] = "requests.aspx";
            Response.Redirect("login.aspx");
            return;
        }
        
        try
        {
            try
            {
                Qryproject = @"Select * from NonesCRMusers where UserName='" + Session["UserName"] + "'";
                dsproject = DatabaseHelper.getDataset(Qryproject);
            }
            catch (Exception ex)
            {
                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at Page_Load in Client Note :-> " + Session["admin"] + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :->" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Sql Query:-> " + Qryproject);
            }

            if (dsproject != null && dsproject.Tables.Count > 0 && dsproject.Tables[0].Rows.Count > 0)
            {
                newvalue = "ProjectNoteListCR.aspx?projectname=" + HttpUtility.UrlEncode(HttpUtility.UrlDecode(strtrace));
                strtrace = dsproject.Tables[0].Rows[0]["ProjectName"].ToString();
                Clientnotes.Attributes["src"] = "ProjectNoteListCR.aspx?projectname=" + HttpUtility.UrlEncode(HttpUtility.UrlDecode(strtrace));
                // DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at Iframe Binding 7:-> " + Session["admin"] + "  DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), strtrace);
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at Page_Load in Client Note :-> " + Session["admin"] + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :->" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Sql Query:-> " + newvalue);
        }
    }
}
