﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InOutTR.ascx.cs" Inherits="InOutTR" %>
 <script language="javascript" type="text/javascript">
     function hideCalendar() {
         $find("<%=CalendarExtender1.ClientID%>").hide();
         $find("<%=CalendarExtender2.ClientID%>").hide();
         return false;
     } 
    </script>

     <script language="javascript" >

         function fnValidateProj(sender, args) {
             alert("called");
             var control1 = document.getElementById('<%= ddlProj.ClientID %>');
             var control2 = document.getElementById('<%= ddlEmp.ClientID %>');
             var selectedvalue = control.options[control.selectedIndex].value;
             alert(selectedvalue);
             if (selectedvalue == "0") {
                 control2.disabled = true;
             }
         }
    </script> 

    <script language="javascript" type="text/javascript">
       
</script>
    <style>
.leavetbl td,th{ border:1px solid #999999;}
</style>
<table width="99%" cellpadding="0" cellspacing="0" border="0" style="margin-left:-3px;">
                                        <tr valign="top">
                                            <td width="15%">
                                                <div>
                                                    <fieldset style="height: 90px; margin: 10px 0px 0px 10px;">
                                                        <legend style="font-size: 16px; padding: 0 2px;"><strong>Custom</strong></legend>
                                                        <table width="99%" cellpadding="0" cellspacing="0" border="0" align="center">
                                                            <tr>
                                                                <td height="25" align="left" valign="middle" style="padding-left: 5px; display: none">
                                                                    Select interval</td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" style="padding-left: 5px;">
                                                                    <asp:DropDownList ID="ddlTimeInterval" runat="server" valign="top" Height="25px"
                                                                        OnSelectedIndexChanged="ddlTimeInterval_SelectedIndexChanged" Width="105px" AutoPostBack="True"
                                                                        Visible="false">
                                                                        <asp:ListItem Value="1">Today</asp:ListItem>
                                                                        <asp:ListItem Value="2">This week</asp:ListItem>
                                                                        <asp:ListItem Value="3">Last 7 days</asp:ListItem>
                                                                        <asp:ListItem Value="4">This month</asp:ListItem>
                                                                        <asp:ListItem Value="5">Last 31 days</asp:ListItem>
                                                                    </asp:DropDownList></td>
                                                              
                                                            </tr>
                                                            <tr>
                                                                <td height="25" align="left" valign="middle">
                                                                    <asp:RadioButtonList ID="rdoid" runat="server" AutoPostBack="True" OnSelectedIndexChanged="rdoid_SelectedIndexChanged"
                                                                        RepeatDirection="Horizontal">
                                                                        <asp:ListItem Selected="True" Value="1">Weekly</asp:ListItem>
                                                                        <asp:ListItem Value="2">Custom</asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </td>
                                                                <td>&nbsp;
                                                                    </td><td>Projects</td><td>Employee(s)</td><td>Time zone</td>
                                                            </tr>
                                                            <tr>
                                                                <td height="25" align="left" valign="middle" style="padding-left: 7px;">
                                                                    From&nbsp;<asp:TextBox ID="GMDStartDate" runat="server" CssClass="inout_boxline"
                                                                        CausesValidation="True" ValidationGroup="g1" OnTextChanged="GMDStartDate_TextChanged"
                                                                        AutoPostBack="true"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    To
                                                                    <asp:TextBox ID="GMDEndDate" runat="server" CssClass="inout_boxline" AutoPostBack="True"
                                                                        OnTextChanged="GMDEndDate_TextChanged" Style="" CausesValidation="True"
                                                                        ValidationGroup="g1" Enabled="false"></asp:TextBox>
                                                                    <ajaxtoolkit:calendarextender id="CalendarExtender2" runat="server" targetcontrolid="GMDEndDate" onclientdateselectionchanged='hideCalendar'
                                                                         format="dd/MM/yyyy"></ajaxtoolkit:calendarextender>
                                                                </td>
                                                              <td  valign="top"><asp:DropDownList ID="ddlProj" runat="server" CssClass="inout_boxline_seclect"
                    onselectedindexchanged="ddlProj_SelectedIndexChanged" AutoPostBack="True" > </asp:DropDownList></td>
                    <td  valign="top"><asp:DropDownList ID="ddlEmp" runat="server" AutoPostBack="true" 
                    OnSelectedIndexChanged="ddlEmp_SelectedIndexChanged" Visible ="true" 
                    ValidationGroup="g1" CssClass="inout_boxline_seclect"> </asp:DropDownList></td>
                                                              <td vlign="top">
                                                                    <asp:DropDownList ID="DrpZone" runat="server" CssClass="inout_boxline_seclect01" AutoPostBack="True" OnSelectedIndexChanged="DrpZone_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="25" align="left" style="padding-left: 5px;">
                                                                    <%--    <img id="Image1" runat="server" alt="Date" src="~/Admin/LMSAdmin/images/cs.png"/>--%>
                                                                    <%--    <asp:ImageButton runat="Server" ID="Image1" ImageUrl="~/Admin/LMSAdmin/images/cs.png" AlternateText="Click to show calendar"/> --%>
                                                                   <ajaxtoolkit:calendarextender id="CalendarExtender1" runat="server" targetcontrolid="GMDStartDate"
                                                                        onclientdateselectionchanged='hideCalendar' format="dd/MM/yyyy"> </ajaxtoolkit:calendarextender>
                                                                         
                                                                </td>
                                                                <td>&nbsp;
                                                                    </td>
                                            
                                        </tr>
                                        <tr>
                                            <td align="left">
                                            </td>
                                        </tr>
                                    </table>
                                    </fieldset>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td width="85%">
                                <div style="margin-bottom: 12px; padding-left: 10px; text-align: center;">
                                    <fieldset style="height: auto; margin: 10px 0px 0px 0px;">
                                        <legend style="font-size: 16px; padding: 0 2px;">
                                            <asp:DropDownList ID="ddlEmp1" runat="server" Style="border: 1px solid #cccccc; padding: 2px;"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlEmp_SelectedIndexChanged"
                                                ValidationGroup="g1" Height="25px" Width="176px" Visible="false">
                                            </asp:DropDownList>
                                        </legend><legend style="margin-left: 95%; margin-top: -27px;">
                                            <asp:ImageButton ID="ImageButton1" runat="server" OnClick="btnexport_Click" ValidationGroup="g1"
                                                AlternateText="Export to excel" ImageUrl="~/xls.png" />
                                        </legend></span>
                                        <asp:Label ID="lblLR4R1Title" runat="server" Visible="false" Font-Underline="True"></asp:Label>
                                        <div id="main" runat="server">
                                            <div style="height: auto;" id="HtmlDesign" runat="server">
                                                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" Width="100%"
                                                    OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowCommand="GridView1_RowCommand"
                                                    OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" OnRowDataBound="GridView1_RowDataBound">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%">
                                                            <HeaderTemplate>
                                                                <b>
                                                                    <asp:Label ID="lblempname" runat="server" Text='Employee Name'></asp:Label></b>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblempname" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Employee_name")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblempname1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Employee_name")%>'></asp:Label>
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                                            <HeaderTemplate>
                                                                <b>
                                                                    <asp:Label ID="lblempname" runat="server" Text='Skills'></asp:Label></b>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblskill" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Skills") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblskill1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Skills") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" ItemStyle-Width="12%">
                                                            <HeaderTemplate>
                                                                <asp:Label runat="server" ID="lblHeader"></asp:Label>
                                                                <b>
                                                                    <asp:Label ID="lblempname" runat="server" Text='In|Out(GMT)|Total'></asp:Label></b>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                               
                                                                            <span style='color: green'>
                                                                                <asp:Label ID="MON" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Date_of_Monday") %>'></asp:Label></span>|
                                                                      
                                                                            <span style='color: #0070c0'>
                                                                                <asp:Label ID="MON1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Date_of_Monday1") %>'></asp:Label></span>
                                                                        
                                                                            |<span style='color: red'><asp:Label ID="MON2" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Date_of_Monday2") %>'></asp:Label></span>
                                                                   
                                                              
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="TXtMON" runat="server" AutoPostBack="true" OnTextChanged="txt_TextChanged"
                                                                                Width="35px" Text='<%# DataBinder.Eval(Container.DataItem,"Date_of_Monday") %>'></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="TXtMON1" Width="35px" runat="server" AutoPostBack="true" OnTextChanged="txt_TextChanged"
                                                                                Text='<%# DataBinder.Eval(Container.DataItem,"Date_of_Monday1") %>'></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="TXtMON2" Width="35px" Enabled="false" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Date_of_Monday2") %>'></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" ItemStyle-Width="12%">
                                                            <HeaderTemplate>
                                                                <asp:Label runat="server" ID="lblHeader1"></asp:Label>
                                                                <b>
                                                                    <asp:Label ID="lblempname" runat="server" Text='In|Out(GMT)|Total'></asp:Label></b>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <span style='color: green'>
                                                                <asp:Label ID="Tues" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Date_of_Tuesday") %>'></asp:Label></span>
                                                                |<span style='color: #0070c0'><asp:Label ID="Tues1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Date_of_Tuesday1") %>'></asp:Label></span>
                                                                |<span style='color: red'><asp:Label ID="Tues2" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Date_of_Tuesday2") %>'></asp:Label></span>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="TXtTues" runat="server" Width="35px" AutoPostBack="true" OnTextChanged="TXtTues_TextChanged"
                                                                                Text='<%# DataBinder.Eval(Container.DataItem,"Date_of_Tuesday") %>'></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="TXtTues1" Width="35px" AutoPostBack="true" OnTextChanged="TXtTues_TextChanged"
                                                                                runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Date_of_Tuesday1") %>'></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="TXtTues2" Width="35px" Enabled="false" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Date_of_Tuesday2") %>'></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" ItemStyle-Width="12%">
                                                            <HeaderTemplate>
                                                                <asp:Label runat="server" ID="lblHeader2"></asp:Label>
                                                                <b>
                                                                    <asp:Label ID="lblempname" runat="server" Text='In|Out(GMT)|Total'></asp:Label></b>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <span style='color: green'>
                                                                    <asp:Label ID="Wed" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Date_of_Wednesday") %>'></asp:Label></span>
                                                                |<span style='color: #0070c0'><asp:Label ID="Wed1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Date_of_Wednesday1") %>'></asp:Label></span>
                                                                |<span style='color: red'><asp:Label ID="Wed2" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Date_of_Wednesday2") %>'></asp:Label></span>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="TXtWed" runat="server" Width="35px" AutoPostBack="true" OnTextChanged="TXtWed_TextChanged"
                                                                                Text='<%# DataBinder.Eval(Container.DataItem,"Date_of_Wednesday") %>'></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="TXtWed1" runat="server" AutoPostBack="true" OnTextChanged="TXtWed_TextChanged"
                                                                                Width="35px" Text='<%# DataBinder.Eval(Container.DataItem,"Date_of_Wednesday1") %>'></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="TXtWed2" runat="server" Enabled="false" Width="35px" Text='<%# DataBinder.Eval(Container.DataItem,"Date_of_Wednesday2") %>'></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" ItemStyle-Width="12%">
                                                            <HeaderTemplate>
                                                                <asp:Label runat="server" ID="lblHeader3"></asp:Label>
                                                                <b>
                                                                    <asp:Label ID="lblempname" runat="server" Text='In|Out(GMT)|Total'></asp:Label>
                                                                </b>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <span style='color: green'>
                                                                    <asp:Label ID="Thus" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Date_of_Thusday") %>'></asp:Label>
                                                                </span>|<span style='color: #0070c0'><asp:Label ID="Thus1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Date_of_Thusday1") %>'></asp:Label></span>
                                                                |<span style='color: red'><asp:Label ID="Thus2" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Date_of_Thusday2") %>'></asp:Label></span>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="TXtThus" runat="server" Width="35px" AutoPostBack="true" OnTextChanged="TXtThus_TextChanged"
                                                                                Text='<%# DataBinder.Eval(Container.DataItem,"Date_of_Thusday") %>'></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="TXtThus1" runat="server" AutoPostBack="true" OnTextChanged="TXtThus_TextChanged"
                                                                                Width="35px" Text='<%# DataBinder.Eval(Container.DataItem,"Date_of_Thusday1") %>'></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="TXtThus2" runat="server" Enabled="false" Width="35px" Text='<%# DataBinder.Eval(Container.DataItem,"Date_of_Thusday2") %>'></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" ItemStyle-Width="12%">
                                                            <HeaderTemplate>
                                                                <asp:Label runat="server" ID="lblHeader4"></asp:Label>
                                                                <b>
                                                                    <asp:Label ID="lblempname" runat="server" Text='In|Out(GMT)|Total'></asp:Label>
                                                                </b>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <span style='color: green'>
                                                                    <asp:Label ID="Frid" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Date_of_Friday") %>'></asp:Label>
                                                                </span>|<span style='color: #0070c0'><asp:Label ID="Frid1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Date_of_Friday1") %>'></asp:Label></span>
                                                                |<span style='color: red'>
                                                                    <asp:Label ID="Frid2" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Date_of_Friday2") %>'></asp:Label></span>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="TXtFrid" runat="server" Width="35px" AutoPostBack="true" OnTextChanged="TXtFrid_TextChanged"
                                                                                Text='<%# DataBinder.Eval(Container.DataItem,"Date_of_Friday") %>'></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="TXtFrid1" runat="server" Width="35px" AutoPostBack="true" OnTextChanged="TXtFrid_TextChanged"
                                                                                Text='<%# DataBinder.Eval(Container.DataItem,"Date_of_Friday1") %>'></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="TXtFrid2" runat="server" Enabled="false" Width="35px" Text='<%# DataBinder.Eval(Container.DataItem,"Date_of_Friday2") %>'></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" ItemStyle-Width="12%">
                                                            <HeaderTemplate>
                                                                <asp:Label runat="server" ID="lblHeader5"></asp:Label>
                                                                <b>
                                                                    <asp:Label ID="lblempname" runat="server" Text='In|Out(GMT)|Total'></asp:Label></b>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <span style='color: green'>
                                                                    <asp:Label ID="Sat" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Date_of_Saturday") %>'></asp:Label>
                                                                </span>|<span style='color: #0070c0'><asp:Label ID="Sat1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Date_of_Saturday1") %>'></asp:Label></span>
                                                                |<span style='color: red'>
                                                                    <asp:Label ID="Sat2" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Date_of_Saturday2") %>'></asp:Label></span>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="TXtSat" runat="server" Width="35px" AutoPostBack="true" OnTextChanged="TXtSat_TextChanged"
                                                                                Text='<%# DataBinder.Eval(Container.DataItem,"Date_of_Saturday") %>'></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="TXtSat1" runat="server" Width="35px" AutoPostBack="true" OnTextChanged="TXtSat_TextChanged"
                                                                                Text='<%# DataBinder.Eval(Container.DataItem,"Date_of_Saturday1") %>'></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="TXtSat2" runat="server" Enabled="false" Width="35px" Text='<%# DataBinder.Eval(Container.DataItem,"Date_of_Saturday2") %>'></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkEdit" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"
                                                                    CommandName="EDIT" runat="server">Edit</asp:LinkButton>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:LinkButton ID="lnkUPDATE" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"
                                                                    CommandName="UPDATE" runat="server" ForeColor="#CC3300">Update</asp:LinkButton>
                                                                <asp:LinkButton ID="lnkCancel" CommandName="CANCEL" runat="server">Cancel</asp:LinkButton>
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                                <br />
                                                <asp:Label runat="server" ID="lbl_pageinfo" Visible="false" />
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </td>
                        </tr>
                        <tr>
                        <td>
                        <div id="showexportcontent" runat="server" visible="false">
                        </div>
                        <div>
                          <script type="text/javascript">
                              ddtreemenu.createTree("treemenu2", true, 5)

                              validatePage = function (button) {
                                  var isValid = Page_ClientValidate();
                                  button.disabled = isValid;

                                  //return validation - if true then postback
                                  return isValid;
                              }
    </script>

    <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableClientScript="true"
        ShowMessageBox="true" ShowSummary="false" ValidationGroup="g1" DisplayMode="BulletList" />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="g1"
        Display="None" ErrorMessage="Please select a start date" ControlToValidate="GMDStartDate">*
    </asp:RequiredFieldValidator>
    <%--<asp:CompareValidator ID="CompareValidator2" runat="server" Display="None" ValidationGroup="g1"
        ErrorMessage="Please enter a valid  date" Operator="DataTypeCheck" ControlToValidate="GMDStartDate"
        Type="Date">*</asp:CompareValidator>
    <asp:CompareValidator ID="CompareValidator3" runat="server" Display="None" ValidationGroup="g1"
        ErrorMessage="Please enter a valid date" Operator="DataTypeCheck" ControlToValidate="GMDEndDate"
        Type="Date">*</asp:CompareValidator>--%>
        </div>
                        </td>
                        </tr>
                    </table>