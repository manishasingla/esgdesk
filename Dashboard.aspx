﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Dashboard.aspx.cs" Inherits="Admin_Dashboard"
    EnableEventValidation="false" ValidateRequest="false" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI" TagPrefix="cc1" %>
<%@ Register Src="Header.ascx" TagName="Header" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="refresh" content="200">
    <%-- <link href="../skin.css" rel="stylesheet" type="text/css" />
   <script src="../js/tabber.js" type="text/javascript"></script>
    <script src="../js/tabber-minimized.js" type="text/javascript"></script>--%>
    <title>Dashboard</title>
    <%--<meta http-equiv="Page-Enter" content="blendTrans(Duration=.3)" />
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=.3)" />--%>
    <link href="StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="ajax1.css" rel="stylesheet" type="text/css" />
    <style>
        .demo
        {
            overflow: auto !important;
        }
        .newsticker
        {
            text-decoration: none;
        }
        .processMessage
        {
            background-color: Transparent;
            border-color: inherit;
            border-style: none;
            border-width: medium;
            left: 50%;
            padding: 10px;
            position: fixed;
            top: 21%;
            width: 18%;
        }
        .pagearea
        {
            background-color: #FFFFFF;
            padding: 14px 20px;
            border-color: #D9D9D9;
            box-shadow: 0 2px 3px rgba(0, 0, 0, 0.2);
        }
        .summary
        {
            background: none repeat scroll 0 0 #EBEBEB;
            border: 1px solid #00A651;
            border-radius: 6px 6px 6px 6px;
            padding: 4px;
            margin-bottom: 10px;
            clear: both;
            content: "";
            display: table;
            width: 96%;
        }
        ul.summary li
        {
            border-right: 1px solid #00A651;
            display: inline;
            float: left;
            font-size: 18px;
            padding-top: 18px;
            text-align: center;
            width: 14.0%;
        }
        ul.summary li.last
        {
            border: 0 none;
        }
        ul.summary li a
        {
            color: #0054A5;
            text-align: center;
            text-decoration: none;
        }
        .sidebar
        {
            float: right;
            width: 310px;
        }
        .reminders:before
        {
            border-color: #FFFFFF #FFFFFF #734E3D #734E3D;
        }
        .reminders:before
        {
            background: none repeat scroll 0 0 #E3B558;
            border-color: #EFEFEF #EFEFEF transparent transparent;
            border-radius: 0 0 0 5px;
            border-style: solid;
            border-width: 14px;
            content: "";
            display: block;
            font-size: 0;
            height: 0;
            position: absolute;
            right: -2px;
            top: -2px;
            width: 0;
        }
        .reminders, .todo
        {
            background-color: #FFFFE2;
            position: relative;
        }
    </style>
    <script>
        function click123(filtername) {
            //alert(filtername);
            if (filtername == "new") {
                var clickButton = document.getElementById("btnNew");
                //alert(clickButton);
                clickButton.click();
            }
            if (filtername == "Progess") {
                var clickButton = document.getElementById("btnProcess");
                //alert(clickButton);
                clickButton.click();
            }
            if (filtername == "Queued") {
                alert("enter");
                var clickButton = document.getElementById("btnQueued");
                //alert(clickButton);
                clickButton.click();
            }
            if (filtername == "Parked") {
                alert("enter p");
                var clickButton = document.getElementById("btnParked");
                //alert(clickButton);
                clickButton.click();
            }

            if (filtername == "ACR") {
                var clickButton = document.getElementById("btnACR");
                //alert(clickButton);
                clickButton.click();
            }
            if (filtername == "toclose") {
                var clickButton = document.getElementById("btnClose");
                //alert(clickButton);
                clickButton.click();
            }
            if (filtername == "all") {
                var clickButton = document.getElementById("btnAll");
                // alert(clickButton);
                clickButton.click();
            }
        }
    </script>
    <script>
        function a() {
            Sys.Browser.WebKit = {};
            if (navigator.userAgent.indexOf('WebKit/') > -1) {
                Sys.Browser.agent = Sys.Browser.WebKit;
                Sys.Browser.version = parseFloat(navigator.userAgent.match(/WebKit\/(\d+(\.\d+)?)/)[1]);
                Sys.Browser.name = 'WebKit';
            }
        }
    </script>
</head>
<body onload="a();">
    <form id="form1" runat="server">
    <%--   <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager> --%>
    <uc1:Header ID="Header1" runat="server" />
    <div id="inner">
        <div class="pagearea" style="width: 80%; float: left">
            <div id="TicketSummary">
                <asp:Button ID="btnNew" runat="server" Style="display: none" OnClick="btnNewclick_Click" />
                <asp:Button ID="btnProcess" runat="server" Style="display: none" OnClick="btnProcessclick_Click" />
                <asp:Button ID="btnQueued" runat="server" Style="display: none" OnClick="btnQueuedclick_Click" />
                <asp:Button ID="btnParked" runat="server" Style="display: none" OnClick="btnParkedclick_Click" />
                <asp:Button ID="btnACR" runat="server" Style="display: none" OnClick="btnACRclick_Click" />
                <asp:Button ID="btnClose" runat="server" Style="display: none" OnClick="btnClose_Click" />
                <asp:Button ID="btnAll" runat="server" Style="display: none" OnClick="btnAllclick_Click" />
                <center>
                    <ul class="summary">
                        <li style="width: 16.5%"><a style="cursor: pointer" onclick="click123('new');"><span
                            id="spanNew" runat="server">New</span>
                            <%--<li style="width: 16.5%"><a style="cursor: pointer" href="#"><span>--%>
                            <h4>
                                <span id="countNew" runat="server">0</span>
                            </h4>
                        </a></li>
                        <li style="width: 16.5%"><a style="cursor: pointer" onclick="click123('Progess');"><span
                            id="spanInprogess" runat="server">
                            <%--<li style="width: 16.5%"><a style="cursor: pointer" href="#"><span>--%>In progress</span>
                            <h4>
                                <span id="CountInprogress" runat="server">0</span>
                            </h4>
                        </a></li>
                        <li style="width: 16.5%"><a style="cursor: pointer" onclick="click123('ACR');"><span
                            id="spanResponse" runat="server">
                            <%--<li style="width: 16.5%"><a style="cursor: pointer" href="#"><span>--%>Response
                            required</span>
                            <h4>
                                <span id="CountResponse" runat="server">0</span>
                            </h4>
                        </a></li>
                        <li style="width: 16.5%"><a style="cursor: pointer" onclick="click123('toclose');"><span
                            id="spanToclose" runat="server">To close</span>
                            <h4>
                                <span id="CountClose" runat="server">0</span>
                            </h4>
                        </a></li>
                        <li style="display: none"><a style="cursor: pointer" onclick="click123('Queued');"><span
                            id="spanQueued" runat="server">
                            <%--<li style="width: 16.5%"><a style="cursor: pointer" href="#"><span>--%>Queued</span>
                            <h4>
                                <span id="CountQueued" runat="server">0</span>
                            </h4>
                        </a></li>
                        <li style="width: 16.5%"><a style="cursor: pointer" onclick="click123('Parked');"><span
                            id="spanParked" runat="server">
                            <%--<li style="width: 16.5%"><a style="cursor: pointer" href="#"><span>--%>Parked</span>
                            <h4>
                                <span id="CountParked" runat="server">0</span>
                            </h4>
                        </a></li>
                        <li class="last" style="width: 16.5%"><a style="cursor: pointer" onclick="click123('all');">
                            <span id="spanAll" runat="server">
                                <%--<li class="last" style="width: 16.5%"><a style="cursor: pointer" href="#"><span>--%>All</span>
                            <h4>
                                <span id="CountAll" runat="server">0</span>
                            </h4>
                        </a></li>
                    </ul>
                </center>
            </div>
            <br />
            <div id="lblmamarquee" runat="server" style="text-align: right; color: #003C7F">
                <%--<asp:Label ></asp:Label>--%>
            </div>
            <br />
            <div id="divAllHistory" runat="server">
                <%--<h3 style="font-size: 16px; font-weight: bold; padding: 5px 0">--%>
                <%--<h3 style="font-size: 16px; font-weight: bold; padding: 5px 0px 5px 6px; background-color: rgb(62, 63, 61);
                            color: white; border-top: 4px solid rgb(0, 166, 81);">
                            Recent Activity</h3>--%>
                <%--<asp:Timer ID="Timer1" runat="server" Interval="1000" OnTick="Timer1_Tick">
                </asp:Timer>--%>
                <asp:Timer ID="Timer2" runat="server" Interval="1000" OnTick="Timer2_Tick">
                </asp:Timer>
                <%-- <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
                </Triggers>
                <ContentTemplate>--%>
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Width="100%">
                    <ajaxToolkit:TabPanel runat="server" HeaderText="TabPanel1" ID="TabPanel1" Style="color: Black;">
                        <HeaderTemplate>
                            News
                        </HeaderTemplate>
                        <ContentTemplate>
                            <div class="tab-content-holder">
                                <div id="div" runat="server">
                                    <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="UpdatePanel2">
                                        <ProgressTemplate>
                                            <div id="processmessage">
                                                <img alt="loading" src="images/ajax-loader.gif" style="width: 30px;" />
                                                <span style="color: black; font-weight: bold; vertical-align: super;">Please wait...
                                                </span>
                                            </div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
                                        </Triggers>
                                        <ContentTemplate>
                                            <asp:Timer ID="Timer1" runat="server" Interval="1000" OnTick="Timer1_Tick">
                                            </asp:Timer>
                                            <asp:GridView ID="NewsGrid" runat="server" AutoGenerateColumns="False" Width="100%"
                                                ShowHeader="False" BorderStyle="None" GridLines="None" Font-Size="14px" OnRowDataBound="NewsGrid_DataBound"
                                                PageSize="10" AllowPaging="True" OnPageIndexChanging="NewsGrid_PageIndexChanging">
                                                <Columns>
                                                    <asp:BoundField DataField="Addeddate" Visible="False" />
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <div style="border-bottom: solid 1px green; margin-bottom: 2px">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td align="left" width="80%">
                                                                            <%--<a href="newsdetails.aspx?id=<%# Eval("id") %>" style="text-decoration: none">--%>
                                                                            <asp:Label ID="lblComment" runat="server"> <%# Eval("Heading").ToString().Replace("|@", "'")%></asp:Label><%--</a>--%>
                                                                        </td>
                                                                        <td align="right" width="19%" style="text-align: right">
                                                                            <asp:Label ID="lblPosted" runat="server" ForeColor="Green" Text='<%# Bind("Addeddate") %>'> </asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="Server"
                                                                                TargetControlID="Panel1" CollapsedSize="0" ExpandedSize="100" Collapsed="True"
                                                                                ExpandControlID="pHeader" CollapseControlID="pHeader" AutoCollapse="False" AutoExpand="False"
                                                                                ScrollContents="True" TextLabelID="lblText" CollapsedText="Expand" ExpandedText="Collapse"
                                                                                ExpandDirection="Vertical" />
                                                                            <asp:Panel ID="pHeader" runat="server" CssClass="cpHeader" Width="100px">
                                                                                <a style="text-decoration: underline">
                                                                                    <asp:Label ID="lblText" runat="server" Style="cursor: pointer" /></a>
                                                                            </asp:Panel>
                                                                            <asp:Panel ID="panel1" runat="server" CssClass="demo" Style="border: 0px solid lightgrey;
                                                                                padding: 2px;">
                                                                                <span>
                                                                                    <%# Eval("Comment").ToString().Replace("|@","'")%></span>
                                                                            </asp:Panel>
                                                                            <%--<span id="panel1" runat="server" style="border: 0px solid lightgrey; padding: 2px;
                                                                                overflow: auto !important"><span>
                                                                                    <%# Eval("Comment").ToString().Replace("|@","'")%></span> </span>--%>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel runat="server" HeaderText="TabPanel1" ID="TabPanel2">
                        <HeaderTemplate>
                            Recent Activity
                        </HeaderTemplate>
                        <ContentTemplate>
                            <div id="divSpecifications" runat="server">
                                <asp:UpdateProgress ID="updateprogress3" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                    <ProgressTemplate>
                                        <div id="processmessage">
                                            <img alt="loading" src="images/ajax-loader.gif" style="width: 30px;" />
                                            <span style="color: black; font-weight: bold; vertical-align: super;">Please wait...
                                            </span>
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="Timer2" EventName="Tick" />
                                    </Triggers>
                                    <ContentTemplate>
                                        <asp:DataGrid ID="DataGrid2" runat="server" AutoGenerateColumns="False" Width="100%"
                                            ShowHeader="False" BorderStyle="None" GridLines="None" Font-Size="14px" OnItemDataBound="DataGrid2_ItemDataBound">
                                            <Columns>
                                                <asp:BoundColumn DataField="post_date" Visible="False"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="status" Visible="False"></asp:BoundColumn>
                                                <asp:TemplateColumn>
                                                    <ItemTemplate>
                                                        <div style="border-bottom: solid 1px green; margin-bottom: 2px">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td align="left" width="80%">
                                                                        <a href="edit_request.aspx?reqid=<%# Eval("RequestId") %>" style="text-decoration: none">
                                                                            <asp:Label ID="lblComment" runat="server"><%# Eval("shortdescr").ToString().Replace("|@", "'")%></asp:Label></a>
                                                                        <div id="lblcomm" runat="server" style="display: none; background-color: #FEFFB3;
                                                                            border: solid 1px #333333; width: 65%; margin-left: 180px  !important; position: absolute">
                                                                            <asp:Label ID="LblLastcomment" runat="server" Text='<%# bind("comment") %>'></asp:Label>
                                                                        </div>
                                                                        <ajaxToolkit:HoverMenuExtender ID="hme2" runat="Server" HoverCssClass="popupHover"
                                                                            PopupControlID="lblcomm" PopupPosition="Top" TargetControlID="lblComment" />
                                                                    </td>
                                                                    <td align="right" width="19%" style="text-align: right">
                                                                        <asp:Label ID="lblPosted" runat="server" ForeColor="Green"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
                <%-- </ContentTemplate>
            </asp:UpdatePanel> --%>
            </div>
        </div>
    </div>
    <div class="pagearea" style="width: 16%; float: right; padding: 4px">
        <center>
            <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <h3 style="font-size: 16px; font-weight: bold; padding: 5px 0px 5px 6px; background-color: rgb(62, 63, 61);
                        color: white; border-top: 4px solid rgb(0, 166, 81);">
                        <%--To-Dos--%>To Do</h3>
                    <table style="background-color: #EBEBEB; border: 1px solid #3E3F3D">
                        <%-- <tr>
                            <td>
                                <h3 style="font-size: 16px; font-weight: bold; padding: 5px 0px 5px 6px; background-color: rgb(62, 63, 61);
                                    color: white; border-top: 4px solid rgb(0, 166, 81);">
                                    To-Dos</h3>
                                 
                            </td>
                        </tr>--%>
                        <tr>
                            <td>
                                <asp:DataGrid ID="To_Dogrid" runat="server" AutoGenerateColumns="False" Width="90%"
                                    ShowHeader="False" BorderStyle="None" GridLines="None" Font-Size="14px" OnItemCommand="To_Dogrid_ItemCommand">
                                    <Columns>
                                        <asp:BoundColumn DataField="id" Visible="False"></asp:BoundColumn>
                                        <%--<asp: ButtonType="checkbox" CommandName="ProductName" DataTextField="Active" />--%>
                                        <asp:TemplateColumn>
                                            <ItemTemplate>
                                                <div style="border-bottom: solid 1px green; margin-bottom: 2px">
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="left" style="width: 22px">
                                                                <%--<asp:HiddenField ID="hndid" runat="server" Value='<%# Eval("id") %>' />--%>
                                                                <%--  <asp:Label Style="display: none" ID="hndid" runat="server"><%# Eval("id") %></asp:Label>--%>
                                                                <asp:CheckBox ID="chkcompleted" runat="server" AutoPostBack="true" Text='<%# Eval("id") %>'
                                                                    Style="color: #EBEBEB" OnCheckedChanged="chkcompleted_OnCheckedChanged" Font-Size="0px" />
                                                            </td>
                                                            <td align="left" style="text-align: left">
                                                                <asp:Label ID="lblComment" runat="server"><%# Eval("comments").ToString().Replace("|@", "'")%></asp:Label>
                                                            </td>
                                                            <td align="right">
                                                                <asp:ImageButton ID="btndel" runat="server" CommandName="delete" src="images/delete_icon2.png"
                                                                    Style="width: 20px;" />
                                                                <asp:ImageButton ID="btnupdate" runat="server" CommandName="update" src="images/delete_icon2.png"
                                                                    Style="width: 20px; display: none" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                        <tr>
                            <%--   <td style="border-bottom: solid 1px green;padding-bottom:2px">--%>
                            <td>
                                <asp:Panel ID="Panel" runat="server" DefaultButton="btnsave">
                                    <asp:UpdateProgress ID="updateprogress2" runat="server" AssociatedUpdatePanelID="UpdatePanel4">
                                        <ProgressTemplate>
                                            <div id="processmessage" style="position: absolute;">
                                                <img alt="loading" src="images/ajax-loader.gif" style="width: 21px;" />
                                                <span style="color: black; font-weight: bold; vertical-align: super;">Please wait...
                                                </span>
                                            </div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                    <asp:TextBox ID="txtrToDo" runat="server" Style="width: 200px"> </asp:TextBox><br />
                                    <asp:Button ID="btnsave" runat="server" Text="Save" OnClick="btnsave_Click" Style="display: none" />
                                    <hr style="float: left; color: green; width: 184px; margin-left: 2px;" />
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DataGrid ID="ToDogridcel" runat="server" AutoGenerateColumns="False" Width="90%"
                                    ShowHeader="False" BorderStyle="None" GridLines="None" Font-Size="14px" OnItemCommand="To_Dogrid_ItemCommand">
                                    <Columns>
                                        <asp:BoundColumn DataField="id" Visible="False"></asp:BoundColumn>
                                        <asp:TemplateColumn>
                                            <ItemTemplate>
                                                <div style="border-bottom: solid 1px green; margin-bottom: 2px">
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="left" style="width: 20px">
                                                                <%--<asp:HiddenField ID="hndid" runat="server" Value='<%# Eval("id") %>' />--%>
                                                                <%--  <asp:Label Style="display: none" ID="hndid" runat="server"><%# Eval("id") %></asp:Label>--%>
                                                                <asp:CheckBox ID="chkuncompleted" runat="server" AutoPostBack="true" Checked="true"
                                                                    Text='<%# Eval("id") %>' Style="color: #EBEBEB" OnCheckedChanged="chkuncompleted_OnCheckedChanged"
                                                                    Font-Size="0px" />
                                                            </td>
                                                            <td align="left" style="text-align: left">
                                                                <del>
                                                                    <asp:Label ID="lblComment" runat="server"><%# Eval("comments").ToString().Replace("|@", "'")%></asp:Label></del>
                                                            </td>
                                                            <td align="right">
                                                                <asp:ImageButton ID="btndel" runat="server" CausesValidation="False" CommandName="delete"
                                                                    src="images/delete_icon2.png" Style="width: 20px;" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </center>
    </div>
    </form>
</body>
</html>
