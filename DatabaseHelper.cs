using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for DatabaseHelper
/// </summary>
public static class DatabaseHelper
{

    static string strConn = ConfigurationManager.AppSettings["strConn"];

    static string strConnECRM = ConfigurationManager.AppSettings["strConnECR"];
    
	private static string getConnStr(string strFrom)
    {
        string strPath = HttpContext.Current.Server.MapPath(strFrom) + "\\MembersDB.mdb";
        string strDBConn = "Provider=Microsoft.Jet.OLEDB.4.0;Password=\"\";User ID=Admin;Data Source=" + strPath;
        return strDBConn;
    }

   
    public static object authenticateEsSCRMuser(string ClientId, string UserName, string Password)
    {
        object objResult;
        string strQuery = "Select count(*) from UserDetails where ClientId=" + ClientId + " and UserName='" + UserName.Replace("'", "''") + "' and Password='" + Password.Replace("'", "''") + "'";
        SqlConnection sqlConn = new SqlConnection(strConnECRM);
        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        try
        {
            sqlConn.Close();
            sqlConn.Open();
            objResult = sqlCmd.ExecuteScalar();
        }
        catch {
            objResult = "0";
        }
        finally
        {
            sqlConn.Close();
        }
        return objResult;
    }

    public static object authenticateLogin(string ClientId, string UserName, string Password)
    {
        object objResult;
        string strQuery = "Select count(*) from NonesCRMusers where ClientId=" + ClientId + " and UserName='" + UserName.Replace("'", "''") + "' and Password='" + Password.Replace("'", "''") + "'";
        SqlConnection sqlConn = new SqlConnection(strConn);
        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        try
        {
            sqlConn.Close();
            sqlConn.Open();
            objResult = sqlCmd.ExecuteScalar();
        }
        catch
        {
            objResult = "0";
        }
        finally
        {
            sqlConn.Close();
        }
        return objResult;
    }

    public static bool sendEmailChangeRequestNorply(string strTo, string strSubject, string strBody)
    {
        bool blndone = false;
        try
        {
            System.Web.Mail.MailMessage msg = new System.Web.Mail.MailMessage();
            msg.To = strTo;
            msg.From =ConfigurationManager.AppSettings["NoRplyFromEmail1"];
            msg.Subject = strSubject;
            msg.Body = strBody;
            msg.Priority = System.Web.Mail.MailPriority.High;
            msg.BodyFormat = System.Web.Mail.MailFormat.Html;
            System.Web.Mail.SmtpMail.Send(msg);
            blndone = true;
        }
        catch { blndone = false; }
        return blndone;
    }
        
    public static bool sendEmailChangeRequest(string strTo, string strSubject, string strBody)
    {
        bool blndone = false;
        try
        {
            System.Web.Mail.MailMessage msg = new System.Web.Mail.MailMessage();
            msg.To = strTo;
            msg.From = ConfigurationManager.AppSettings["FromEmail1"];
            msg.Subject = strSubject;
            msg.Body = strBody;
            msg.Priority = System.Web.Mail.MailPriority.High;
            msg.BodyFormat = System.Web.Mail.MailFormat.Html;
            System.Web.Mail.SmtpMail.Send(msg);
            blndone = true;
        }
        catch { blndone = false; }
        return blndone;
    }
    public static bool sendEmailTasks(string strTo, string strSubject, string strBody)
    {
        bool blndone = false;
        try
        {
            System.Web.Mail.MailMessage msg = new System.Web.Mail.MailMessage();
            msg.To = strTo;
            msg.From = ConfigurationManager.AppSettings["FromEmail2"];
            msg.Subject = strSubject;
            msg.Body = strBody;
            msg.Priority = System.Web.Mail.MailPriority.High;
            msg.BodyFormat = System.Web.Mail.MailFormat.Html;
            System.Web.Mail.SmtpMail.Send(msg);
            blndone = true;
        }
        catch { blndone = false; }
        return blndone;
    }

    public static bool sendEmailEODC(string strTo, string strFrom, string strSubject, string strBody, int i)
    {
        bool blndone = false;
        try
        {
            System.Web.Mail.MailMessage msg = new System.Web.Mail.MailMessage();
            msg.To = strTo;
            if (i == 1)
            {
                msg.Cc = "rohitd@123sitesolutions.com; priyab@123sitesolutions.com";
            }
            msg.From = strFrom;
            msg.Subject = strSubject;
            msg.Body = strBody;
            msg.Priority = System.Web.Mail.MailPriority.High;
            msg.BodyFormat = System.Web.Mail.MailFormat.Html;
            System.Web.Mail.SmtpMail.Send(msg);
            blndone = true;
        }
        catch { blndone = false; }
        return blndone;
    }

    public static object insertClientRequest(string ClientId, string UserName, string WebsiteURL, string ShrtDescr, string Comment, string companyname, string Project, string lastupdatedby, string CRPostedby, string interbalref, string commentaddedby)
    {
        object objResult;
        string company = "";
        if (companyname != null)
        {
            company = companyname;
        }
        else
        {
            company = "";
        }

        string strQuery = @"DECLARE @RequestId INT;";
        strQuery += " insert into ClientRequest(ClientId,UserName,WebsiteURL,ShortDescr,Status,Priority,LastUpdatedBy,LastUpdatedOn,RequestDate,CompanyName,ProjectName,AddedBy,Internalref) ";
                strQuery += " values(" + ClientId + ",";
                strQuery += "'" + UserName.Replace("'", "''") + "',";
                strQuery += "'" + WebsiteURL + "',";
                strQuery += "'" + ShrtDescr.Replace("'", "''") + "',";
                strQuery += "'new',";
                strQuery += "0,";
                strQuery += "'" + lastupdatedby.Replace("'", "''") + "',";
                strQuery += "getdate(),";
                strQuery += "getdate(), '" + company.ToString().Replace("'", "''") + "', '" + Project.ToString().Replace("'", "''") + "','" + CRPostedby.ToString().Replace("'", "''") + "', '" + interbalref.ToString() + "'); ";
                strQuery += " SET @RequestId = SCOPE_IDENTITY(); ";
                strQuery += "DECLARE @CommentId INT;";
                strQuery += " insert into ClientRequest_Details(RequestId,Comment,PostedBy,PostedOn) ";
                strQuery += " values(@RequestId,";
                strQuery += "'" + Comment.Replace("'","''") + "',";
                strQuery += "'" + lastupdatedby.Replace("'", "''") + "',";
                strQuery += "getdate()); ";
                strQuery += " SET @CommentId = SCOPE_IDENTITY(); ";
                strQuery += " insert into read_CR_comments([CommentId],[UserName]) ";
                strQuery += " values(@CommentId,'" + commentaddedby.ToString().Replace("'", "''") + "')";

                strQuery += "select @RequestId;";

        SqlConnection sqlConn = new SqlConnection(strConn);

        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        try
        {
            sqlConn.Close();
            sqlConn.Open();
            objResult = sqlCmd.ExecuteScalar();
        }
        catch
        {
            objResult = "0";
        }
        finally
        {
            sqlConn.Close();
        }
        return objResult;
    }

    public static object updateClientRequest(int RequestId, string ClientId, string UserName, string WebsiteURL, string ShrtDescr, string Comment, string companyname,string project)
    {
        object objResult;
        string strQuery = "";

        if (Comment != "")
        {
            strQuery = " Update ClientRequest ";
            strQuery += " set WebsiteURL='" + WebsiteURL.Replace("'", "''") + "', ";
            strQuery += "ShortDescr='" + ShrtDescr.Replace("'", "''") + "', ";
            strQuery += "LastUpdatedBy='" + UserName.Replace("'", "''") + "', ";
            strQuery += "LastUpdatedOn=getdate() ,";
            strQuery += "CompanyName='" + companyname + "',";
            strQuery += "ProjectName='" + project.Replace("'", "''") + "' ";

            strQuery += "where RequestId=" + RequestId.ToString() + "; ";
            strQuery += "DECLARE @CommentId INT;";
            strQuery += " insert into ClientRequest_Details(RequestId,Comment,PostedBy,PostedOn) ";
            strQuery += " values(" + RequestId.ToString() + ",";
            strQuery += "'" + Comment.Replace("'", "''") + "',";
            strQuery += "'" + UserName.Replace("'", "''") + "',";
            strQuery += "getdate()); ";
            strQuery += " SET @CommentId = SCOPE_IDENTITY(); ";
            strQuery += " insert into read_CR_comments([CommentId],[UserName]) ";
            strQuery += " values(@CommentId,'" + UserName.Replace("'", "''") + "')";
        }
        else
        {
            strQuery = " Update ClientRequest ";
            strQuery += " set WebsiteURL='" + WebsiteURL.Replace("'", "''") + "', ";
            strQuery += "ShortDescr='" + ShrtDescr.Replace("'", "''") + "', ";
            strQuery += "LastUpdatedBy='" + UserName.Replace("'", "''") + "', ";
            strQuery += "LastUpdatedOn=getdate(),";
            strQuery += "CompanyName='" + companyname + "', ";
            strQuery += "ProjectName='" + project.Replace("'", "''") + "' ";
            strQuery += "where RequestId=" + RequestId.ToString() + "; ";
        }

        SqlConnection sqlConn = new SqlConnection(strConn);
        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        try
        {
            sqlConn.Close();
            sqlConn.Open();
            objResult = sqlCmd.ExecuteNonQuery();
        }
        catch
        {
            objResult = "0";
        }
        finally
        {
            sqlConn.Close();
        }
        return objResult;
    }

    public static DataSet getClientRequests(string ClientId, string UserName, string strFilter, string strOrderBy)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select * " +
                              "from ClientRequest "+
                              "where ClientId=" + ClientId + " and UserName='" + UserName.Replace("'", "''") + "' " + strFilter + " " + strOrderBy; 

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    public static DataSet getClientRequest(int RequestId)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select * " +
                              "from ClientRequest " +
                              "where RequestId=" + RequestId.ToString() ;

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }    

    public static DataSet getClientRequestDetails(int RequestId)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select * " +
                              "from ClientRequest_Details " +
                              "where deleted <> 1 and RequestId=" + RequestId.ToString() + " order by CommentId desc";

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    public static object registerUser(string UserName, string Password,  string FirstName, string Surname, string CompanyName, string Phone, string Email, string ClientId, string WebSite)
    {
        object objResult;
        string strQuery = "";

        strQuery += "insert into NonesCRMusers(UserName,Password,FirstName,Surname,CompanyName,Phone,Email,ClientId,WebSite,RegDate) ";
        strQuery += "values('" + UserName.Replace("'", "''") + "',";
        strQuery += "'" + Password.Replace("'", "''") + "',";
        strQuery += "'" + FirstName.Replace("'", "''") + "',";
        strQuery += "'" + Surname.Replace("'", "''") + "',";
        strQuery += "'" + CompanyName.Replace("'", "''") + "',";
        strQuery += "'" + Phone + "',";
        strQuery += "'" + Email.Replace("'", "''") + "',";
        strQuery += "" + ClientId + ",";
        strQuery += "'" + WebSite.Replace("'", "''") + "',";
        strQuery += "'" + DateTime.Parse(System.DateTime.Now.ToString(), System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat).ToString("MM/dd/yyyy hh:mm:ss") + "')";

        SqlConnection sqlConn = new SqlConnection(strConn);
        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        try
        {
            sqlConn.Close();
            sqlConn.Open();
            objResult = sqlCmd.ExecuteNonQuery();
        }
        catch
        {
            objResult = "0";
        }
        finally
        {
            sqlConn.Close();
        }
        return objResult;
    }

    public static object updateRegisterUser(string UserName, string FirstName, string Surname, string CompanyName, string Phone, string Email, string ClientId, string WebSite)
    {
        object objResult;
        string strQuery = "";

        strQuery += "Update NonesCRMusers ";
        strQuery += "set ";
        strQuery += "FirstName='" + FirstName.Replace("'", "''") + "',";
        strQuery += "Surname='" + Surname.Replace("'", "''") + "',";
        strQuery += "CompanyName='" + CompanyName.Replace("'", "''") + "',";
        strQuery += "Phone='" + Phone + "',";
        strQuery += "Email='" + Email.Replace("'", "''") + "',";
        strQuery += "WebSite='" + WebSite.Replace("'", "''") + "' ";
        strQuery += "where UserName='" + UserName.Replace("'", "''") + "'";
        
        SqlConnection sqlConn = new SqlConnection(strConn);
        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        try
        {
            sqlConn.Close();
            sqlConn.Open();
            objResult = sqlCmd.ExecuteNonQuery();
        }
        catch
        {
            objResult = "0";
        }
        finally
        {
            sqlConn.Close();
        }
        return objResult;
    }

    public static object deleteaddedsubscription(string strSuscriptionID)
    {
        object objResult = "0";
        SqlConnection sqlConn = new SqlConnection(strConn);

        try
        {
            string sqlQuery = "delete from  [Client_subscription]  where SubscriptionID = " + strSuscriptionID.Replace("'", "''") + "";
           
            SqlCommand sqlCmd = new SqlCommand(sqlQuery, sqlConn);
            sqlConn.Open();
            objResult = sqlCmd.ExecuteNonQuery();
        }
        catch { objResult = "0"; }
        finally { sqlConn.Close(); }
        return objResult;
    }


    public static object deleteRegisteredUser(string strUserName)
    {
        object objResult = "0";
        SqlConnection sqlConn = new SqlConnection(strConn);

        try
        {
            string sqlQuery = "delete from  ClientRequest_Details Where [RequestId] in (select RequestId from ClientRequest where username = '"+ strUserName.Replace("'","''") + "');";
            sqlQuery += "delete from  ClientRequest Where [UserName]='" + strUserName.Replace("'","''") +"';";
            sqlQuery += "delete from  NonesCRMusers Where [UserName]='" + strUserName.Replace("'", "''") + "'";
            SqlCommand sqlCmd = new SqlCommand(sqlQuery, sqlConn);
            sqlConn.Open();
            objResult = sqlCmd.ExecuteNonQuery();
        }
        catch { objResult = "0"; }
        finally { sqlConn.Close(); }
        return objResult;
    }


    public static DataSet getClientSubscription(string strOrderBy)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select * " +
                              "from Client_subscription " + strOrderBy;

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    

    public static DataSet getUserDetails(string strOrderBy)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select * " +
                              "from NonesCRMusers " + strOrderBy;                         

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    public static DataSet getUserDetails(string ClientId, string UserName)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select * " +
                              "from NonesCRMusers " +
                              "where ClientId=" + ClientId.ToString() + " and UserName='" + UserName.Replace("'", "''") + "'";

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    public static DataSet getUserDetails2(string company, string UserName)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select * " +
                              "from NonesCRMusers " +
                              "where CompanyName='" + company.Replace("'", "''") + "' and UserName='" + UserName.Replace("'", "''") + "'";

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    public static object checkUserName(string UserName)
    {
        object objResult;
        string strQuery = "";
        strQuery += "select count(*) from NonesCRMusers where UserName ='" + UserName.Replace("'", "''") + "'";
        SqlConnection sqlConn = new SqlConnection(strConn);
        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        try
        {            
            sqlConn.Open();
            objResult = sqlCmd.ExecuteScalar();
        }
        catch
        {
            objResult = null;
        }
        finally
        {
            sqlConn.Close();
        }
        return objResult;
    }

    public static int changePassword(string ClientId, string UserName, string pwd)
    {
        int intResult = 0;
        SqlConnection sqlConn = new SqlConnection(strConn);
        
        try
        {
            string sqlQuery = "Update NonesCRMusers Set [Password]='" + pwd.Replace("'", "''") +
                              "' Where [ClientId]=" + ClientId.Replace("'", "''") + " and [UserName]='" + UserName.Replace("'", "''") + "'";
            SqlCommand sqlCmd = new SqlCommand(sqlQuery, sqlConn);
            sqlConn.Open();
            intResult = sqlCmd.ExecuteNonQuery();
        }
        catch { intResult = 0; }
        finally { sqlConn.Close(); }
        return intResult;
    }


    public static DataSet getUserNamePassword(string ClientId, string UserName, bool isEmail)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);

            string strQuery = "";

            if (isEmail == false)
            {
                strQuery = "Select UserName, Password, Email " +
                                   "from NonesCRMusers " +
                                   "where ClientId=" + ClientId.ToString() + " and UserName='" + UserName.Replace("'", "''") + "'";
            }
            else
            {
                strQuery = "Select UserName, Password, Email " +
                                   "from NonesCRMusers " +
                                   "where ClientId=" + ClientId.ToString() + " and Email='" + UserName.Replace("'", "''") + "'";
            }
            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    public static DataSet getAdminClientRequest(string strFilter, string strOrderBy)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select * " +
                              "from ClientRequest " + strFilter + " " + strOrderBy;

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    public static object updateAdminClientRequest(int RequestId, string ClientId, string UserName, string status, string priority, string WebsiteURL, string ShrtDescr, string Comment)
    {
        object objResult;
        string strQuery = "";

        if (Comment != "")
        {
            strQuery = " Update ClientRequest ";
            strQuery += " set WebsiteURL='" + WebsiteURL.Replace("'", "''") + "', ";
            strQuery += "ShortDescr='" + ShrtDescr.Replace("'", "''") + "', ";
            strQuery += "Status='" + status + "', ";
            strQuery += "Priority=" + priority + ", ";
            strQuery += "LastUpdatedBy='Admin', ";
            strQuery += "LastUpdatedOn=getdate() ";
            strQuery += "where RequestId=" + RequestId.ToString() + "; ";
            strQuery += " insert into ClientRequest_Details(RequestId,Comment,PostedBy,PostedOn) ";
            strQuery += " values(" + RequestId.ToString() + ",";
            strQuery += "'" + Comment.Replace("'", "''") + "',";
            strQuery += "'Admin',";
            strQuery += "getdate())";
        }
        else
        {
            strQuery = " Update ClientRequest ";
            strQuery += " set WebsiteURL='" + WebsiteURL.Replace("'", "''") + "', ";
            strQuery += "ShortDescr='" + ShrtDescr.Replace("'", "''") + "', ";
            strQuery += "Status='" + status + "', ";
            strQuery += "Priority=" + priority + ", ";
            strQuery += "LastUpdatedBy='Admin', ";
            strQuery += "LastUpdatedOn=getdate() ";
            strQuery += "where RequestId=" + RequestId.ToString() + "; ";
        }

        SqlConnection sqlConn = new SqlConnection(strConn);
        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        try
        {
            sqlConn.Close();
            sqlConn.Open();
            objResult = sqlCmd.ExecuteNonQuery();
        }
        catch
        {
            objResult = "0";
        }
        finally
        {
            sqlConn.Close();
        }
        return objResult;
    }

    public static int deleteComment(string RequestId, string CommentId)
    {
        int intResult = 0;
        SqlConnection sqlConn = new SqlConnection(strConn);

        try
        {
            string sqlQuery = "delete from  ClientRequest_Details Where [RequestId]=" + RequestId + " and [CommentId]=" + CommentId;
            SqlCommand sqlCmd = new SqlCommand(sqlQuery, sqlConn);
            sqlConn.Open();
            intResult = sqlCmd.ExecuteNonQuery();
        }
        catch { intResult = 0; }
        finally { sqlConn.Close(); }
        return intResult;
    }

    public static object deleteClientRequest(int RequestId)
    {
        object objResult = "0";
        SqlConnection sqlConn = new SqlConnection(strConn);

        try
        {
            string sqlQuery = "delete from  ClientRequest_Details Where [RequestId]=" + RequestId + "; ";
            sqlQuery += "delete from  ClientRequest Where [RequestId]=" + RequestId ;
            SqlCommand sqlCmd = new SqlCommand(sqlQuery, sqlConn);
            sqlConn.Open();
            objResult = sqlCmd.ExecuteNonQuery();
        }
        catch { objResult = "0"; }
        finally { sqlConn.Close(); }
        return objResult;
    }


    public static DataSet getCompanyNameCR()
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            //////string strQuery = "Select distinct WebsiteURL " +
            //////                  "from ClientRequest where ClientId = " + ClientId + " and UserName='" + UserName.Replace("'", "''") + "' order by WebsiteURL";
            ////string strQuery = "Select distinct CompanyName " +
            ////                "from NonesCRMusers order by CompanyName";

            string strQuery = @"select distinct Company_Name
		from Company
		where active = 'Y' order by Company_Name;";

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    public static DataSet getWebsiteURL()
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select distinct WebsiteURL " +
                              "from ClientRequest order by WebsiteURL";

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    public static DataSet getRequestPostedBy()
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select distinct UserName, ClientId ,  cast(ClientId as varchar) +'+'+ UserName   as Cid " +
                              "from ClientRequest order by UserName";

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);

        }
        catch
        {
        }
        finally { }
        return ds;
    }
     public static DataSet getWebsiteURL(string ClientId, string UserName)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select distinct WebsiteURL " +
                              "from ClientRequest where ClientId = " + ClientId + " and UserName='" + UserName.Replace("'", "''") + "' order by WebsiteURL";
            //////string strQuery = "Select *,(select CompanyName from [NonesCRMusers] where  NonesCRMusers.UserName = ClientRequest.UserName ) as companyname " +
            //////                "from ClientRequest where ClientId = " + ClientId + " and UserName='" + UserName.Replace("'", "''") + "' order by WebsiteURL";

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }


    public static DataSet getCompanyName(string ClientId, string UserName)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            //////string strQuery = "Select distinct WebsiteURL " +
            //////                  "from ClientRequest where ClientId = " + ClientId + " and UserName='" + UserName.Replace("'", "''") + "' order by WebsiteURL";
            string strQuery = "Select distinct CompanyName " +
                            "from NonesCRMusers where ClientId = " + ClientId + " and UserName='" + UserName.Replace("'", "''") + "' order by CompanyName";

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    public static object checkWebsiteURL(string ClientId, string UserName, string WebsiteURL)
    {
        object objResult;
        string strQuery = "";
        strQuery += "select RequestId from ClientRequest where ClientId = " + ClientId + " and UserName='" + UserName.Replace("'", "''") + "' and WebsiteURL='" + WebsiteURL.Replace("'", "''") + "' and status <> 'closed'";
        SqlConnection sqlConn = new SqlConnection(strConn);
        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        try
        {
            sqlConn.Open();
            objResult = sqlCmd.ExecuteScalar();
        }
        catch
        {
            objResult = null;
        }
        finally
        {
            sqlConn.Close();
        }
        return objResult;
    }

    public static object insertAttachment(int RequestId, string Pkey, string FileName, string Description, string UploadedBy)
    {
        object objResult;
        SqlConnection sqlConn = new SqlConnection(strConn);

        string strQuery = " insert into ClientRequest_Attachment(RequestId,Pkey,FileName,Description,UploadedBy,UploadedOn) ";
        strQuery += " values(" + RequestId + ",";
        strQuery += "'" + Pkey + "',";
        strQuery += "'" + FileName.Replace("'", "''") + "',";
        strQuery += "'" + Description.Replace("'", "''") + "',";
        strQuery += "'" + UploadedBy.Replace("'", "''") + "',";
        strQuery += "getdate()); ";
        
       

        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        sqlConn.Open();
        objResult = sqlCmd.ExecuteNonQuery();
        try
        {
            sqlConn.Close();
            sqlConn.Open();
            string sqlQuery = "Select Aid from  ClientRequest_Attachment Where RequestId =" + RequestId + " and Pkey = '" + Pkey + "' and FileName ='" + FileName.Replace("'", "''") + "'";

            SqlCommand sqlCmd2 = new SqlCommand(sqlQuery, sqlConn);
            /// sqlConn.Open();

            objResult = sqlCmd2.ExecuteScalar();

        }
        catch
        {
            objResult = "0";
        }
        finally
        {
            sqlConn.Close();
        }
        return objResult;
    }

    public static object insertTaskAttachment(int task_id, string Pkey, string FileName, string Description, string UploadedBy)
    {
        object objResult;
        int atchment = 0;
        SqlConnection sqlConn = new SqlConnection(strConn);

        string strQuery = " insert into task_Attachments(task_id,Pkey,FileName,Description,UploadedBy,UploadedOn) ";
        strQuery += " values(" + task_id + ",";
        strQuery += "'" + Pkey + "',";
        strQuery += "'" + FileName.Replace("'", "''") + "',";
        strQuery += "'" + Description.Replace("'", "''") + "',";
        strQuery += "'" + UploadedBy.Replace("'", "''") + "',";
        strQuery += "getdate()); ";



        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        sqlConn.Open();
        objResult = sqlCmd.ExecuteNonQuery();

        try
        {
            sqlConn.Close();
            sqlConn.Open();
            string sqlQuery = "Select Aid from  task_attachments Where task_id =" + task_id + " and Pkey = '" + Pkey + "' and FileName ='" + FileName.Replace("'", "''") + "'";

            SqlCommand sqlCmd2 = new SqlCommand(sqlQuery, sqlConn);
            /// sqlConn.Open();

            objResult = sqlCmd2.ExecuteScalar();

        }
        catch
        {
            objResult = "0";
        }
        finally
        {
            sqlConn.Close();
        }

        return objResult;
    }

    public static DataSet getTaskAttachments(int task_id, string strOrderBy)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select * , pkey +'_'+ FileName as FileName1 " +
                              "from task_attachments " +
                              "where task_id=" + task_id.ToString() + " " + strOrderBy;

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    public static int deleteTaskAttachment(string Aid)
    {
        int intResult = 0;
        SqlConnection sqlConn = new SqlConnection(strConn);

        try
        {
            string sqlQuery = "delete from  task_attachments Where [Aid]=" + Aid;
            SqlCommand sqlCmd = new SqlCommand(sqlQuery, sqlConn);
            sqlConn.Open();
            intResult = sqlCmd.ExecuteNonQuery();
        }
        catch { intResult = 0; }
        finally { sqlConn.Close(); }
        return intResult;
    }

   
         public static DataSet getTaskAllfilename(string FileAid)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select * , pkey +'_'+ FileName as FileName1 " +
                              "from task_attachments " +
                              "where Aid=" + FileAid + " ";

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }
    public static DataSet getAllfilename(string FileAid)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select * , pkey +'_'+ FileName as FileName1 " +
                              "from ClientRequest_Attachment " +
                              "where Aid=" + FileAid + " ";

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    public static DataSet getAttachments(int RequestId, string strOrderBy)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select * , pkey +'_'+ FileName as FileName1 " +
                              "from ClientRequest_Attachment " +
                              "where RequestId=" + RequestId.ToString() + " " + strOrderBy;

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }
    
    public static int deleteAttachment(string Aid)
    {
        int intResult = 0;
        SqlConnection sqlConn = new SqlConnection(strConn);

        try
        {
            string sqlQuery = "delete from  ClientRequest_Attachment Where [Aid]=" + Aid ;
            SqlCommand sqlCmd = new SqlCommand(sqlQuery, sqlConn);
            sqlConn.Open();
            intResult = sqlCmd.ExecuteNonQuery();
        }
        catch { intResult = 0; }
        finally { sqlConn.Close(); }
        return intResult;
    }

    public static object updateInternalReference(int RequestId, string Internalref)
    {
        DataSet dsRequests = getClientRequest(RequestId);
        string strIRN = "";
        if (dsRequests == null || dsRequests.Tables.Count <= 0 || dsRequests.Tables[0].Rows.Count <= 0)
        {
            
        }
        else
        {
            strIRN = dsRequests.Tables[0].Rows[0]["Internalref"].ToString();

            if (strIRN == "")
            {
                strIRN = Internalref;
            }
            else
            {
                if (!strIRN.Contains(Internalref))
                {
                    strIRN += "," + Internalref;
                }
            }
        }        
        
        object objResult;
        string strQuery = "";

        strQuery = " Update ClientRequest ";
        strQuery += " set Internalref='" + strIRN + "' ";
        strQuery += "where RequestId=" + RequestId.ToString() + "; ";
        
        SqlConnection sqlConn = new SqlConnection(strConn);
        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        tryusing System;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for DatabaseHelper
/// </summary>
public static class DatabaseHelper
{

    static string strConn = ConfigurationManager.AppSettings["strConn"];

    static string strConnECRM = ConfigurationManager.AppSettings["strConnECR"];
    
	private static string getConnStr(string strFrom)
    {
        string strPath = HttpContext.Current.Server.MapPath(strFrom) + "\\MembersDB.mdb";
        string strDBConn = "Provider=Microsoft.Jet.OLEDB.4.0;Password=\"\";User ID=Admin;Data Source=" + strPath;
        return strDBConn;
    }

   
    public static object authenticateEsSCRMuser(string ClientId, string UserName, string Password)
    {
        object objResult;
        string strQuery = "Select count(*) from UserDetails where ClientId=" + ClientId + " and UserName='" + UserName.Replace("'", "''") + "' and Password='" + Password.Replace("'", "''") + "'";
        SqlConnection sqlConn = new SqlConnection(strConnECRM);
        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        try
        {
            sqlConn.Close();
            sqlConn.Open();
            objResult = sqlCmd.ExecuteScalar();
        }
        catch {
            objResult = "0";
        }
        finally
        {
            sqlConn.Close();
        }
        return objResult;
    }

    public static object authenticateLogin(string ClientId, string UserName, string Password)
    {
        object objResult;
        string strQuery = "Select count(*) from NonesCRMusers where ClientId=" + ClientId + " and UserName='" + UserName.Replace("'", "''") + "' and Password='" + Password.Replace("'", "''") + "'";
        SqlConnection sqlConn = new SqlConnection(strConn);
        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        try
        {
            sqlConn.Close();
            sqlConn.Open();
            objResult = sqlCmd.ExecuteScalar();
        }
        catch
        {
            objResult = "0";
        }
        finally
        {
            sqlConn.Close();
        }
        return objResult;
    }

    public static bool sendEmailChangeRequestNorply(string strTo, string strSubject, string strBody)
    {
        bool blndone = false;
        try
        {
            System.Web.Mail.MailMessage msg = new System.Web.Mail.MailMessage();
            msg.To = strTo;
            msg.From =ConfigurationManager.AppSettings["NoRplyFromEmail1"];
            msg.Subject = strSubject;
            msg.Body = strBody;
            msg.Priority = System.Web.Mail.MailPriority.High;
            msg.BodyFormat = System.Web.Mail.MailFormat.Html;
            System.Web.Mail.SmtpMail.Send(msg);
            blndone = true;
        }
        catch { blndone = false; }
        return blndone;
    }
        
    public static bool sendEmailChangeRequest(string strTo, string strSubject, string strBody)
    {
        bool blndone = false;
        try
        {
            System.Web.Mail.MailMessage msg = new System.Web.Mail.MailMessage();
            msg.To = strTo;
            msg.From = ConfigurationManager.AppSettings["FromEmail1"];
            msg.Subject = strSubject;
            msg.Body = strBody;
            msg.Priority = System.Web.Mail.MailPriority.High;
            msg.BodyFormat = System.Web.Mail.MailFormat.Html;
            System.Web.Mail.SmtpMail.Send(msg);
            blndone = true;
        }
        catch { blndone = false; }
        return blndone;
    }
    public static bool sendEmailTasks(string strTo, string strSubject, string strBody)
    {
        bool blndone = false;
        try
        {
            System.Web.Mail.MailMessage msg = new System.Web.Mail.MailMessage();
            msg.To = strTo;
            msg.From = ConfigurationManager.AppSettings["FromEmail2"];
            msg.Subject = strSubject;
            msg.Body = strBody;
            msg.Priority = System.Web.Mail.MailPriority.High;
            msg.BodyFormat = System.Web.Mail.MailFormat.Html;
            System.Web.Mail.SmtpMail.Send(msg);
            blndone = true;
        }
        catch { blndone = false; }
        return blndone;
    }

    public static bool sendEmailEODC(string strTo, string strFrom, string strSubject, string strBody, int i)
    {
        bool blndone = false;
        try
        {
            System.Web.Mail.MailMessage msg = new System.Web.Mail.MailMessage();
            msg.To = strTo;
            if (i == 1)
            {
                msg.Cc = "rohitd@123sitesolutions.com; priyab@123sitesolutions.com";
            }
            msg.From = strFrom;
            msg.Subject = strSubject;
            msg.Body = strBody;
            msg.Priority = System.Web.Mail.MailPriority.High;
            msg.BodyFormat = System.Web.Mail.MailFormat.Html;
            System.Web.Mail.SmtpMail.Send(msg);
            blndone = true;
        }
        catch { blndone = false; }
        return blndone;
    }

    public static object insertClientRequest(string ClientId, string UserName, string WebsiteURL, string ShrtDescr, string Comment, string companyname, string Project, string lastupdatedby, string CRPostedby, string interbalref, string commentaddedby)
    {
        object objResult;
        string company = "";
        if (companyname != null)
        {
            company = companyname;
        }
        else
        {
            company = "";
        }

        string strQuery = @"DECLARE @RequestId INT;";
        strQuery += " insert into ClientRequest(ClientId,UserName,WebsiteURL,ShortDescr,Status,Priority,LastUpdatedBy,LastUpdatedOn,RequestDate,CompanyName,ProjectName,AddedBy,Internalref) ";
                strQuery += " values(" + ClientId + ",";
                strQuery += "'" + UserName.Replace("'", "''") + "',";
                strQuery += "'" + WebsiteURL + "',";
                strQuery += "'" + ShrtDescr.Replace("'", "''") + "',";
                strQuery += "'new',";
                strQuery += "0,";
                strQuery += "'" + lastupdatedby.Replace("'", "''") + "',";
                strQuery += "getdate(),";
                strQuery += "getdate(), '" + company.ToString().Replace("'", "''") + "', '" + Project.ToString().Replace("'", "''") + "','" + CRPostedby.ToString().Replace("'", "''") + "', '" + interbalref.ToString() + "'); ";
                strQuery += " SET @RequestId = SCOPE_IDENTITY(); ";
                strQuery += "DECLARE @CommentId INT;";
                strQuery += " insert into ClientRequest_Details(RequestId,Comment,PostedBy,PostedOn) ";
                strQuery += " values(@RequestId,";
                strQuery += "'" + Comment.Replace("'","''") + "',";
                strQuery += "'" + lastupdatedby.Replace("'", "''") + "',";
                strQuery += "getdate()); ";
                strQuery += " SET @CommentId = SCOPE_IDENTITY(); ";
                strQuery += " insert into read_CR_comments([CommentId],[UserName]) ";
                strQuery += " values(@CommentId,'" + commentaddedby.ToString().Replace("'", "''") + "')";

                strQuery += "select @RequestId;";

        SqlConnection sqlConn = new SqlConnection(strConn);

        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        try
        {
            sqlConn.Close();
            sqlConn.Open();
            objResult = sqlCmd.ExecuteScalar();
        }
        catch
        {
            objResult = "0";
        }
        finally
        {
            sqlConn.Close();
        }
        return objResult;
    }

    public static object updateClientRequest(int RequestId, string ClientId, string UserName, string WebsiteURL, string ShrtDescr, string Comment, string companyname,string project)
    {
        object objResult;
        string strQuery = "";

        if (Comment != "")
        {
            strQuery = " Update ClientRequest ";
            strQuery += " set WebsiteURL='" + WebsiteURL.Replace("'", "''") + "', ";
            strQuery += "ShortDescr='" + ShrtDescr.Replace("'", "''") + "', ";
            strQuery += "LastUpdatedBy='" + UserName.Replace("'", "''") + "', ";
            strQuery += "LastUpdatedOn=getdate() ,";
            strQuery += "CompanyName='" + companyname + "',";
            strQuery += "ProjectName='" + project.Replace("'", "''") + "' ";

            strQuery += "where RequestId=" + RequestId.ToString() + "; ";
            strQuery += "DECLARE @CommentId INT;";
            strQuery += " insert into ClientRequest_Details(RequestId,Comment,PostedBy,PostedOn) ";
            strQuery += " values(" + RequestId.ToString() + ",";
            strQuery += "'" + Comment.Replace("'", "''") + "',";
            strQuery += "'" + UserName.Replace("'", "''") + "',";
            strQuery += "getdate()); ";
            strQuery += " SET @CommentId = SCOPE_IDENTITY(); ";
            strQuery += " insert into read_CR_comments([CommentId],[UserName]) ";
            strQuery += " values(@CommentId,'" + UserName.Replace("'", "''") + "')";
        }
        else
        {
            strQuery = " Update ClientRequest ";
            strQuery += " set WebsiteURL='" + WebsiteURL.Replace("'", "''") + "', ";
            strQuery += "ShortDescr='" + ShrtDescr.Replace("'", "''") + "', ";
            strQuery += "LastUpdatedBy='" + UserName.Replace("'", "''") + "', ";
            strQuery += "LastUpdatedOn=getdate(),";
            strQuery += "CompanyName='" + companyname + "', ";
            strQuery += "ProjectName='" + project.Replace("'", "''") + "' ";
            strQuery += "where RequestId=" + RequestId.ToString() + "; ";
        }

        SqlConnection sqlConn = new SqlConnection(strConn);
        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        try
        {
            sqlConn.Close();
            sqlConn.Open();
            objResult = sqlCmd.ExecuteNonQuery();
        }
        catch
        {
            objResult = "0";
        }
        finally
        {
            sqlConn.Close();
        }
        return objResult;
    }

    public static DataSet getClientRequests(string ClientId, string UserName, string strFilter, string strOrderBy)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select * " +
                              "from ClientRequest "+
                              "where ClientId=" + ClientId + " and UserName='" + UserName.Replace("'", "''") + "' " + strFilter + " " + strOrderBy; 

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    public static DataSet getClientRequest(int RequestId)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select * " +
                              "from ClientRequest " +
                              "where RequestId=" + RequestId.ToString() ;

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }    

    public static DataSet getClientRequestDetails(int RequestId)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select * " +
                              "from ClientRequest_Details " +
                              "where deleted <> 1 and RequestId=" + RequestId.ToString() + " order by CommentId desc";

            SqlDataAdapter da = new SqlDataAdapter(strQuery, Conn);
            da.Fill(ds);
        }
        catch
        {
        }
        finally { }
        return ds;
    }

    public static object registerUser(string UserName, string Password,  string FirstName, string Surname, string CompanyName, string Phone, string Email, string ClientId, string WebSite)
    {
        object objResult;
        string strQuery = "";

        strQuery += "insert into NonesCRMusers(UserName,Password,FirstName,Surname,CompanyName,Phone,Email,ClientId,WebSite,RegDate) ";
        strQuery += "values('" + UserName.Replace("'", "''") + "',";
        strQuery += "'" + Password.Replace("'", "''") + "',";
        strQuery += "'" + FirstName.Replace("'", "''") + "',";
        strQuery += "'" + Surname.Replace("'", "''") + "',";
        strQuery += "'" + CompanyName.Replace("'", "''") + "',";
        strQuery += "'" + Phone + "',";
        strQuery += "'" + Email.Replace("'", "''") + "',";
        strQuery += "" + ClientId + ",";
        strQuery += "'" + WebSite.Replace("'", "''") + "',";
        strQuery += "'" + DateTime.Parse(System.DateTime.Now.ToString(), System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat).ToString("MM/dd/yyyy hh:mm:ss") + "')";

        SqlConnection sqlConn = new SqlConnection(strConn);
        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        try
        {
            sqlConn.Close();
            sqlConn.Open();
            objResult = sqlCmd.ExecuteNonQuery();
        }
        catch
        {
            objResult = "0";
        }
        finally
        {
            sqlConn.Close();
        }
        return objResult;
    }

    public static object updateRegisterUser(string UserName, string FirstName, string Surname, string CompanyName, string Phone, string Email, string ClientId, string WebSite)
    {
        object objResult;
        string strQuery = "";

        strQuery += "Update NonesCRMusers ";
        strQuery += "set ";
        strQuery += "FirstName='" + FirstName.Replace("'", "''") + "',";
        strQuery += "Surname='" + Surname.Replace("'", "''") + "',";
        strQuery += "CompanyName='" + CompanyName.Replace("'", "''") + "',";
        strQuery += "Phone='" + Phone + "',";
        strQuery += "Email='" + Email.Replace("'", "''") + "',";
        strQuery += "WebSite='" + WebSite.Replace("'", "''") + "' ";
        strQuery += "where UserName='" + UserName.Replace("'", "''") + "'";
        
        SqlConnection sqlConn = new SqlConnection(strConn);
        SqlCommand sqlCmd = new SqlCommand(strQuery, sqlConn);
        try
        {
            sqlConn.Close();
            sqlConn.Open();
            objResult = sqlCmd.ExecuteNonQuery();
        }
        catch
        {
            objResult = "0";
        }
        finally
        {
            sqlConn.Close();
        }
        return objResult;
    }

    public static object deleteaddedsubscription(string strSuscriptionID)
    {
        object objResult = "0";
        SqlConnection sqlConn = new SqlConnection(strConn);

        try
        {
            string sqlQuery = "delete from  [Client_subscription]  where SubscriptionID = " + strSuscriptionID.Replace("'", "''") + "";
           
            SqlCommand sqlCmd = new SqlCommand(sqlQuery, sqlConn);
            sqlConn.Open();
            objResult = sqlCmd.ExecuteNonQuery();
        }
        catch { objResult = "0"; }
        finally { sqlConn.Close(); }
        return objResult;
    }


    public static object deleteRegisteredUser(string strUserName)
    {
        object objResult = "0";
        SqlConnection sqlConn = new SqlConnection(strConn);

        try
        {
            string sqlQuery = "delete from  ClientRequest_Details Where [RequestId] in (select RequestId from ClientRequest where username = '"+ strUserName.Replace("'","''") + "');";
            sqlQuery += "delete from  ClientRequest Where [UserName]='" + strUserName.Replace("'","''") +"';";
            sqlQuery += "delete from  NonesCRMusers Where [UserName]='" + strUserName.Replace("'", "''") + "'";
            SqlCommand sqlCmd = new SqlCommand(sqlQuery, sqlConn);
            sqlConn.Open();
            objResult = sqlCmd.ExecuteNonQuery();
        }
        catch { objResult = "0"; }
        finally { sqlConn.Close(); }
        return objResult;
    }


    public static DataSet getClientSubscription(string strOrderBy)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection Conn = new SqlConnection(strConn);
            string strQuery = "Select * " +
                    