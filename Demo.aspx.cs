﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class Admin_Demo : System.Web.UI.Page
{
    int closecount = 0;
    string sql = "";
    string task_action = "";
    string emailSubject = "";
    DateTime duedate;
    bool hidecheck;
    bool adminhide;
    static DataSet ds;

    protected void Page_Load(object sender, EventArgs e)
    {



        if (Session["cwo"] == "y")
        {

            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> { alert('You already have task assigned status CWO please change it to CWO but away and then proceed');} </script>");
            Session["cwo"] = "n";

        }

        LnkBtnToDeleteAll.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete all selected tasks?')");
        LnkBtnToChngPR.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to change priority of all selected tasks?')");
        LnkChngAssingedTo.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to change assigned to of all selected tasks?')");
        LnkChngStatus.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to change status of all selected tasks?')");

        DrpMultipleTaskPR.Attributes.Add("onchange", "return EnablePRlink();");
        DrpMultipleTaskAssing.Attributes.Add("onchange", "return EnableAssinglink();");
        DrpMultipleTaskStatus.Attributes.Add("onchange", "return EnableStatuslink();");

        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Response.Redirect("login.aspx");
            return;
        }


        TopFilterLable.InnerHtml = "";
        divMessage.InnerHtml = "";

        if (!Page.IsPostBack)
        {
            BindData();
            try
            {
                DataSet ds1 = ds;
                // if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
                if (ds.Tables[7].Rows[0]["admin"].ToString() != "Y")
                {
                    Session["adm"] = "no";
                    tdAssignedTo.Visible = false;
                }
                else
                {
                    Session["adm"] = "yes";
                    tdAssignedTo.Visible = true;
                }
            }
            catch
            {

            }
            if (ds.Tables[8].Rows[0]["show_deleted_tasks"].ToString() != "Y")
            {
                chkViewDeleted.Visible = false;
            }
            else
            {
                chkViewDeleted.Visible = true;
            }

            // getAllNotification();

            if (ds.Tables[7].Rows[0]["admin"].ToString() != "Y")
            {
                LnkBtnToDeleteAll.Visible = false;
                LnkBtnToChngPR.Visible = false;
                DivMiltiselection.Visible = false;

            }
            else
            {
                LnkBtnToDeleteAll.Visible = true;
                LnkBtnToChngPR.Visible = true;
                DivMiltiselection.Visible = true;

            }

            LnkBtnToChngPR.Enabled = false;
            LnkChngAssingedTo.Enabled = false;
            LnkChngStatus.Enabled = false;
            LnkChngAssingedTo.Enabled = false;

            if (Session["filter"] != null)
            {
                if (Session["boolclose"] == "true")
                {
                    chkHideClosed.Checked = true;
                    Session["boolclose"] = "false";
                    hidecheck = true;
                    adminhide = true;
                }
                else
                {
                    chkHideClosed.Checked = false;
                    hidecheck = false;
                    adminhide = false;
                }

                if (Session["filter"].ToString() == "Immediate")
                {
                    bindPR0Tasks();
                }
                else if (Session["filter"].ToString() == "Unanswered")
                {
                    bindUnansweredQuestions();
                }
                else if (Session["filter"].ToString() == "1a - DO NOW")
                {
                    bindPR1aTasks();
                }
                else if (Session["filter"].ToString() == "HighTasks")
                {
                    bindPR1bTasks();
                }
                else if (Session["filter"].ToString() == "New")
                {
                    bindNewTask();
                }
                else if (Session["filter"].ToString() == "CWO")
                {
                    bindCrntWrkingTasks();
                }
                else if (Session["filter"].ToString() == "CWO/CWO but away")
                {
                    bindCrntWrkingTasks();
                }
                else if (Session["filter"].ToString() == "1c - normal")
                {
                    bindPR1cTasks();
                }
                else
                {
                    bindDefault();
                }
            }
            else
            {


                load_dropdowns();
                load_filterDropDown();
                chkViewDeleted.Checked = false;

                if (Session["boolclose"] == "true")
                {
                    chkHideClosed.Checked = true;
                    Session["boolclose"] = "false";
                    hidecheck = true;
                }
                else
                {
                    chkHideClosed.Checked = false;
                    hidecheck = false;
                }
                ChkReadTask.Checked = true;


                bindDefault();
            }





        }
    }

    void getAllNotification()
    {


        sql = @"select distinct Company_name from Company_notes 
                where Allow_Notes ='True' and Notes !=''";



        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {


            ///LnkNotification.Text = "(" + ds.Tables[0].Rows.Count + ")";
            // Notification.Visible = true;

        }
        else
        {

            // Notification.Visible = true;
            // LnkNotification.Text = "";


        }


    }

    private void BindData()
    {
        string filter = " where ";
        string filter2 = " and";
        string filter3 = " and ";
        LblRecordsno.Text = "";
        DateTime duedate;

        string topFilterLable = "";




        if (!DatabaseHelper.can_Show_Closed_Tasks(Session["admin"].ToString()))
        //if (ds.Tables[15].Rows[0]["can_Show_Closed_Tasks"].ToString() != "Y")
        {
            if (Session["filterunread"] == null || Session["filterunread"].ToString() == "")
            {

                filter += " status <> 'closed' and status <> 'Checked' and status <>'parked' and  ";
                filter2 += " status <> 'closed' and status <>'Checked' and status <>'parked' and  ";
            }
            else
            {
                filter += " status <> 'closed' and ";
                filter2 += " status <> 'closed' and ";
            }


            chkHideClosed.Visible = false;
        }


        else
        {
            if (chkHideClosed.Checked == true)
            {
                if (Session["filterunread"] == null || Session["filterunread"].ToString() == "")
                {
                    filter += " status <> 'checked' and status <>'parked' and ";
                    filter2 += " status <> 'checked' and status <>'parked' and ";
                    filter3 += " and status <> 'checked' and status <>'parked' and ";
                }

            }
            else
            {
                if (Session["filterunread"] == null || Session["filterunread"].ToString() == "")
                {

                    filter += " status <> 'closed' and status <> 'Checked' and status <>'parked' and ";
                    filter2 += " status <> 'closed' and status <> 'Checked' and status <>'parked' and ";
                    filter3 += " status <> 'closed' and status <> 'Checked' and status <>'parked' and ";

                }
                else
                {

                    filter += " status <> 'closed' and  ";
                    filter2 += " status <> 'closed' and ";
                    filter3 += " status <> 'closed' and ";

                }
            }
        }


        if (chkViewDeleted.Checked == true)
        {
            filter += " deleted = 1 ";
            filter2 += " deleted = 1 ";
            filter3 += " deleted = 1 ";

        }
        else
        {

            filter += " deleted <> 1 ";
            filter2 += " deleted <> 1 ";
            filter3 += " deleted <> 1 ";

        }

        string drpValue = drpFilter.SelectedValue;
        string username = "";

        if (drpValue.Contains("Assigned by me"))
        {
            filter += " and reported_user = '" + username + "' ";
            filter2 += " and reported_user = '" + username + "' ";
        }
        else if (drpValue.Contains("Assigned by "))
        {
            username = drpValue.Replace("Assigned by ", "");

            filter += " and reported_user = '" + username + "' ";
            filter2 += " and reported_user = '" + username + "' ";
        }
        else if (drpValue.Contains("All emps CWO"))
        {
            filter += " and status = 'CWO' ";
            filter2 += " and status = 'CWO' ";
            filter3 += " and status = 'CWO' ";
        }
        else if (drpValue.Contains("Show my open tasks"))
        {

            filter += " and assigned_to_user = '" + Session["admin"] + "' ";
            filter2 += " and assigned_to_user = '" + Session["admin"] + "' ";
            adminhide = false;

        }
        else if (drpValue.Contains("All tasks by last updated"))
        {
            if (Session["adm"] == "no" || adminhide == true)
            {
                filter += " and assigned_to_user = '" + Session["admin"] + "' ";
                filter2 += " and assigned_to_user = '" + Session["admin"] + "' ";
                adminhide = false;
            }
        }


        if (drpProjects.SelectedIndex != 0)
        {
            filter += " and project = '" + drpProjects.SelectedValue + "' ";
            filter2 += " and project = '" + drpProjects.SelectedValue + "' ";
            filter3 += " and project = '" + drpProjects.SelectedValue + "' ";

            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Project: " + drpProjects.SelectedValue;
            }
            else
            {
                topFilterLable += " / Project: " + drpProjects.SelectedValue;
            }
        }

        if (drpCategories.SelectedIndex != 0)
        {
            filter += " and category = '" + drpCategories.SelectedValue + "' ";
            filter2 += " and category = '" + drpCategories.SelectedValue + "' ";
            filter3 += " and category = '" + drpCategories.SelectedValue + "' ";

            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Category: " + drpCategories.SelectedValue;
            }
            else
            {
                topFilterLable += " / Category: " + drpCategories.SelectedValue;
            }
        }

        if (drpReportedBy.SelectedIndex != 0)
        {
            filter += " and reported_user = '" + drpReportedBy.SelectedValue + "' ";
            filter2 += " and reported_user = '" + drpReportedBy.SelectedValue + "' ";
            filter3 += " and reported_user = '" + drpReportedBy.SelectedValue + "' ";

            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Reported by: " + drpReportedBy.SelectedValue;
            }
            else
            {
                topFilterLable += " / Reported by:" + drpReportedBy.SelectedValue;
            }
        }
        if (hidecheck == false)
        {
            if (drpPriorities.SelectedIndex != 0)
            {
                filter += " and priority = '" + drpPriorities.SelectedValue + "' ";
                filter2 += " and priority = '" + drpPriorities.SelectedValue + "' ";
                filter3 += " and priority = '" + drpPriorities.SelectedValue + "' ";

                if (topFilterLable == "")
                {
                    topFilterLable = "Filtered by Priority: " + drpPriorities.SelectedValue;
                }
                else
                {
                    topFilterLable += " / Priority:" + drpPriorities.SelectedValue;
                }
            }
        }

        if (drpAssignedTo.SelectedIndex != 0)
        {
            if (Session["adm"] != "yes")
            {
                filter = filter.Replace("and assigned_to_user = '" + Session["admin"] + "' ", " and assigned_to_user = '" + drpAssignedTo.SelectedValue + "' ");
                filter2 = filter2.Replace("and assigned_to_user = '" + Session["admin"] + "' ", " and assigned_to_user = '" + drpAssignedTo.SelectedValue + "' ");
                filter3 = filter3.Replace("and assigned_to_user = '" + Session["admin"] + "' ", " and assigned_to_user = '" + drpAssignedTo.SelectedValue + "' ");
            }
            else
            {
                filter += " and assigned_to_user = '" + drpAssignedTo.SelectedValue + "'";
                filter2 += " and assigned_to_user = '" + drpAssignedTo.SelectedValue + "'";
                filter3 += " and assigned_to_user = '" + drpAssignedTo.SelectedValue + "'";
            }
            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Assigned to: " + drpAssignedTo.SelectedValue;
            }
            else
            {
                topFilterLable += " / Assigned to: " + drpAssignedTo.SelectedValue;
            }
        }

        if (drpStatuses.SelectedIndex != 0)
        {
            if (drpStatuses.SelectedValue == "CWO")
            {
                filter = filter.Replace("and status <> 'Checked'", "").Replace("and status <>'parked'", "");
                filter2 = filter2.Replace("and status <> 'Checked'", "").Replace("and status <>'parked'", "");
                filter3 = filter3.Replace("and status <> 'Checked'", "").Replace("and status <>'parked'", "");
                filter += " and status = '" + drpStatuses.SelectedValue + "' ";
                filter2 += " and status = '" + drpStatuses.SelectedValue + "' ";
                filter3 += " and status = '" + drpStatuses.SelectedValue + "' ";

                if (topFilterLable == "")
                {
                    topFilterLable = "Filtered by Status: " + drpStatuses.SelectedValue;
                }
                else
                {
                    topFilterLable += " / Status: " + drpStatuses.SelectedValue;
                }
            }
            else if (drpStatuses.SelectedValue == "CWO but away")
            {
                filter = filter.Replace("and status <> 'Checked'", "").Replace("and status <>'parked'", "");
                filter2 = filter2.Replace("and status <> 'Checked'", "").Replace("and status <>'parked'", "");
                filter3 = filter3.Replace("and status <> 'Checked'", "").Replace("and status <>'parked'", "");
                filter += " and status =  '" + drpStatuses.SelectedValue + "' ";
                filter2 += " and status = '" + drpStatuses.SelectedValue + "' ";
                filter3 += " and status = '" + drpStatuses.SelectedValue + "' ";

                if (topFilterLable == "")
                {
                    topFilterLable = "Filtered by Status: " + drpStatuses.SelectedValue;
                }
                else
                {
                    topFilterLable += " / Status: " + drpStatuses.SelectedValue;
                }
            }
            else
            {
                filter = filter.Replace("and status <> 'Checked'", "").Replace("and status <>'parked'", "");
                filter2 = filter2.Replace("and status <> 'Checked'", "").Replace("and status <>'parked'", "");
                filter3 = filter3.Replace("and status <> ''Checked'", "").Replace("and status <>'parked'", "");
                filter += " and status =  '" + drpStatuses.SelectedValue + "' ";
                filter2 += " and status = '" + drpStatuses.SelectedValue + "' ";
                filter3 += " and status = '" + drpStatuses.SelectedValue + "' ";

                if (topFilterLable == "")
                {
                    topFilterLable = "Filtered by Status: " + drpStatuses.SelectedValue;
                }
                else
                {
                    topFilterLable += " / Status: " + drpStatuses.SelectedValue;
                }
            }

        }

        if (ChkReadTask.Checked)
        {


        }
        else
        {

            filter += " and  tasks.task_id  in(select task_id from task_comments where tc_id not in(select tc_id from read_comments where username='" + Session["admin"].ToString() + "')) ";
            filter2 += " and  tasks.task_id  in(select task_id from task_comments where tc_id not in(select tc_id from read_comments where username='" + Session["admin"].ToString() + "')) ";
            filter3 += " and  tasks.task_id  in(select task_id from task_comments where tc_id not in(select tc_id from read_comments where username='" + Session["admin"].ToString() + "')) ";

        }
        if (chkViewDeleted.Checked == true)
        {
            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by View only deleted";
            }
            else
            {
                topFilterLable += " / View only deleted";
            }
        }

        if (chkHideClosed.Checked == true)
        {
            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by view closed";
            }
            else
            {
                topFilterLable += " / view closed";
            }
        }

        TopFilterLable.InnerHtml = topFilterLable;
        if (Session["filterunread"] == null || Session["filterunread"].ToString() == "")
        {
            sql = filter + " " ;
        }

        else
        {
            if (Session["filterunread"].ToString() == "TaskNewComment")
            {

                sql = " where task_id in(select task_comments.task_id from task_comments where tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 )";

                sql += filter2 ;

            }

            else if (Session["filterunread"].ToString() == "AllTaskNewComment")
            {
                sql = " where task_id in(select task_comments.task_id from task_comments where tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 )";

                if (Session["boolcount"] == "true")
                {
                    //sql += lblOrderBy.Text;
                }
                else
                {
                    if (hidecheck == false)
                    {
                        sql += filter3 + " " ;
                    }
                    else
                    {
                      //  sql += lblOrderBy.Text;
                    }
                }

            }
            else
            {
                sql = filter + " ";
            }
        }

        if (Session["OverdueTask"] == null || Session["OverdueTask"].ToString() == "")
        {


        }
        else
        {
            string strduedate = "";
            sql = filter + "  and deleted <> 1 and status <> 'checked' and status <> 'parked' and DueDate <> '" + strduedate + "'  " ;


        }

        // DataSet ds = DatabaseHelper.getprocDataset(sql);
        ds = DatabaseHelper.getprocDataset(sql, Session["admin"].ToString(), lblOrderBy.Text);


        if (Session["OverdueTask"] == null || Session["OverdueTask"].ToString() == "")
        {


        }
        else
        {
            if (Session["OverdueTask"].ToString() == "True")
            {
                try
                {
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            DateTime crrntdate = DateTime.Now.Date;
                            string djgfjs = ds.Tables[0].Rows[i]["DueDate"].ToString();
                            if (ds.Tables[0].Rows[i]["DueDate"].ToString() != null && ds.Tables[0].Rows[i]["DueDate"].ToString() != "")
                            {
                                string strduedate = DateTime.Parse(ds.Tables[0].Rows[i]["DueDate"].ToString()).ToString("dd/MM/yyyy");
                                duedate = DateTime.Parse(strduedate.ToString());
                                if (DateTime.Compare(duedate, crrntdate) < 0)
                                {

                                    //j++;

                                }
                                else
                                {
                                    ds.Tables[0].Rows.RemoveAt(i);
                                    i--;
                                    continue;
                                }

                            }
                            else
                            {
                                ds.Tables[0].Rows.RemoveAt(i);
                                i--;
                                continue;
                            }

                        }


                    }

                }
                catch { }
            }


        }

        //ds = formatTaskDataSet(ds);

        if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "No task yet.";
            DataGrid1.DataSource = null;
            DataGrid1.DataBind();
            LblRecordsno.Text = "0";
            divTotalHrs.Visible = false;
            if (closecount != 0)
            {
                chkHideClosed.Checked = true;
                closecount = 0;
            }
            else
            {
                chkHideClosed.Checked = false;
            }
            hidecheck = false;
        }
        else
        {
            if (ds.Tables[0].Rows.Count <= 21)
            {

            }
            else
            {

            }

            PagedDataSource objds = new PagedDataSource();
            objds.DataSource = ds.Tables[0].DefaultView;

            LblRecordsno.Text = objds.DataSourceCount.ToString();

            divMessage.InnerHtml = "";
            DataGrid1.DataSource = null;
            DataGrid1.DataBind();
            DataGrid1.DataSource = ds.Tables[0];
            DataGrid1.DataBind();
            if (closecount != 0)
            {
                chkHideClosed.Checked = true;
                closecount = 0;
            }
            else
            {
                chkHideClosed.Checked = false;
            }

            getTotalHrsReport("select task_id from tasks " + filter);
            divTotalHrs.Visible = true;

        }
    }
    //private DataSet formatTaskDataSet(DataSet ds)
    //{
    //    try
    //    {

    //        ds.Tables[0].Columns.Add("ShowAllComment");
    //        /// ds.Tables[0].Columns.Add("ItemPriority", typeof(string));

    //        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
    //        {
    //            try
    //            {

    //                if (ds.Tables[0].Rows[i]["short_desc"] != null && ds.Tables[0].Rows[i]["short_desc"].ToString().Trim() != "")
    //                {
    //                    ds.Tables[0].Rows[i]["short_desc"] = "<a  href=\"edit_task.aspx?id=" + ds.Tables[0].Rows[i]["task_id"].ToString() + "  \">" + ds.Tables[0].Rows[i]["short_desc"].ToString() + "</a>";

    //                }
    //                else
    //                {
    //                    ds.Tables[0].Rows[i]["short_desc"] = "";

    //                }
    //                try
    //                {

    //                    string LastComment = "";

    //                    sql = "select *  from task_comments where task_comments.deleted <> 1  and task_id =" + ds.Tables[0].Rows[i]["task_id"].ToString() + " order by tc_id desc";


    //                    DataSet dslastcomment = DatabaseHelper.getDataset(sql);

    //                    if (dslastcomment != null && dslastcomment.Tables[0].Rows.Count > 0)
    //                    {

    //                        for (int j = 0; j < dslastcomment.Tables[0].Rows.Count; j++)
    //                        {

    //                            if (dslastcomment.Tables[0].Rows[j]["qflag"].ToString() == "1")
    //                            {

    //                                LastComment += "<span style='color:Red'>" + "Question posted by " + dslastcomment.Tables[0].Rows[j]["username"].ToString() + " for  " + dslastcomment.Tables[0].Rows[j]["QuesTo"].ToString() + " on " + DateTime.Parse(dslastcomment.Tables[0].Rows[j]["post_date"].ToString()).ToString("dd MMM h:mm tt") + "</span>";

    //                                LastComment += "<br/><span>" + dslastcomment.Tables[0].Rows[j]["comment"].ToString() + "</span>";

    //                                LastComment += "<span style=\"border-bottom: solid 1px green; margin-bottom: 2px; height:1px\"></span><br/><hr />";

    //                            }
    //                            else
    //                            {

    //                                LastComment += "<span style='color:Green'>" + "Comment " + dslastcomment.Tables[0].Rows[j]["tc_id"].ToString() + " posted by " + dslastcomment.Tables[0].Rows[j]["username"].ToString() + " on " + DateTime.Parse(dslastcomment.Tables[0].Rows[j]["post_date"].ToString()).ToString("dd MMM h:mm tt") + "</span>";

    //                                LastComment += "<br/><span>" + dslastcomment.Tables[0].Rows[j]["comment"].ToString() + "</span>";

    //                                LastComment += "<span style=\"border-bottom: solid 1px green; margin-bottom: 2px; height:1px\"></span><br/><hr />";

    //                            }

    //                        }
    //                    }

    //                    ds.Tables[0].Rows[i]["ShowAllComment"] = LastComment.ToString();
    //                }
    //                catch
    //                {

    //                }

    //            }
    //            catch { }
    //        }
    //    }
    //    catch
    //    {
    //        Server.ClearError();
    //        ///Response.Redirect("fhs.aspx");
    //    }
    //    return ds;
    //}
    void load_dropdowns()
    {

        // projects
        try
        {
            ////            sql = @"select project_name
            ////		from projects
            ////		where active = 'Y' order by project_name;";

            // categories
            ////sql += "\nselect category_name from categories order by sort_seq, category_name;";

            // priorities
            ////sql += "\nselect priority_name from priorities order by sort_seq, priority_name;";

            // statuses
            ////sql += "\nselect status_name from statuses order by sort_seq, status_name;";

            // users
            ////sql += "\nselect username from users where active = 'Y' order by username;";

            // do a batch of sql statements
            ////DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);

            drpProjects.DataSource = ds.Tables[2];
            drpProjects.DataTextField = "project_name";
            drpProjects.DataValueField = "project_name";
            drpProjects.DataBind();
            drpProjects.Items.Insert(0, new ListItem("[no filter]", "0"));


            drpCategories.DataSource = ds.Tables[3];
            drpCategories.DataTextField = "category_name";
            drpCategories.DataValueField = "category_name";
            drpCategories.DataBind();
            drpCategories.Items.Insert(0, new ListItem("[no filter]", "0"));

            drpPriorities.DataSource = ds.Tables[4];
            drpPriorities.DataTextField = "priority_name";
            drpPriorities.DataValueField = "priority_name";
            drpPriorities.DataBind();
            drpPriorities.Items.Insert(0, new ListItem("[no filter]", "0"));

            DrpMultipleTaskPR.DataSource = ds.Tables[4];
            DrpMultipleTaskPR.DataTextField = "priority_name";
            DrpMultipleTaskPR.DataValueField = "priority_name";
            DrpMultipleTaskPR.DataBind();
            DrpMultipleTaskPR.Items.Insert(0, new ListItem("[no filter]", "0"));

            drpStatuses.DataSource = ds.Tables[5];
            drpStatuses.DataTextField = "status_name";
            drpStatuses.DataValueField = "status_name";
            drpStatuses.DataBind();
            drpStatuses.Items.Insert(0, new ListItem("[no filter]", "0"));
            DrpMultipleTaskStatus.DataSource = ds.Tables[5];
            DrpMultipleTaskStatus.DataTextField = "status_name";
            DrpMultipleTaskStatus.DataValueField = "status_name";
            DrpMultipleTaskStatus.DataBind();
            DrpMultipleTaskStatus.Items.Insert(0, new ListItem("[no filter]", "0"));

            drpReportedBy.DataSource = ds.Tables[6];
            drpReportedBy.DataTextField = "username";
            drpReportedBy.DataValueField = "username";
            drpReportedBy.DataBind();
            drpReportedBy.Items.Insert(0, new ListItem("[no filter]", "0"));

            drpAssignedTo.DataSource = ds.Tables[6];
            drpAssignedTo.DataTextField = "username";
            drpAssignedTo.DataValueField = "username";
            drpAssignedTo.DataBind();
            drpAssignedTo.Items.Insert(0, new ListItem("[no filter]", "0"));
            DrpMultipleTaskAssing.DataSource = ds.Tables[6];
            DrpMultipleTaskAssing.DataTextField = "username";
            DrpMultipleTaskAssing.DataValueField = "username";
            DrpMultipleTaskAssing.DataBind();
            DrpMultipleTaskAssing.Items.Insert(0, new ListItem("[no filter]", "0"));
        }

        catch { }
    }
    void load_filterDropDown()
    {
        drpFilter.Items.Clear();
        if (ds.Tables[7].Rows[0]["admin"].ToString() == "Y")
        {
            // sql = "select username from users where active='Y' and admin='Y' and username <> '" + Session["Admin"] + "' order by username";

            DataTable dsAdmin = ds.Tables[9];

            if (dsAdmin != null)
            {
                if (dsAdmin.Rows.Count > 0)
                {
                    for (int i = 0; i < dsAdmin.Rows.Count; i++)
                    {
                        drpFilter.Items.Add("Assigned by " + dsAdmin.Rows[i]["username"]);
                    }
                }
            }

            drpFilter.Items.Add("Assigned by " + Session["Admin"]);
            drpFilter.Items.Add("Assigned by me");
            drpFilter.Items.Add("All tasks by last updated");
            drpFilter.Items.Add("All emps CWO");
            drpFilter.Items.Add("Show my open tasks");
            //drpFilter.SelectedValue = (string)DatabaseHelper.executeScalar("select default_selected_item from users where username='" + Session["admin"] + "'");
            drpFilter.SelectedValue = ds.Tables[10].Rows[0]["default_selected_item"].ToString();
        }
        else
        {
            drpFilter.Items.Add("Show my open tasks");
        }
    }
    protected void drpFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["filter"] = null;
        Session["filterunread"] = null;
        load_dropdowns();
        setFileterSession();
        //DataGrid1.CurrentPageIndex = 0;
        DataGrid1.PageIndex = 0;
        BindData();

    }

    protected void drpProjects_SelectedIndexChanged(object sender, EventArgs e)
    {
        setFileterSession();
        //DataGrid1.CurrentPageIndex = 0;
        DataGrid1.PageIndex = 0;
        BindData();
    }

    protected void drpCategories_SelectedIndexChanged(object sender, EventArgs e)
    {
        setFileterSession();
        //DataGrid1.CurrentPageIndex = 0;
        DataGrid1.PageIndex = 0;
        BindData();
    }

    protected void drpReportedBy_SelectedIndexChanged(object sender, EventArgs e)
    {
        ///Session["filterunread"] = null;
        setFileterSession();
        // DataGrid1.CurrentPageIndex = 0;
        DataGrid1.PageIndex = 0;
        BindData();
    }

    protected void drpPriorities_SelectedIndexChanged(object sender, EventArgs e)
    {
        setFileterSession();
        //DataGrid1.CurrentPageIndex = 0;
        DataGrid1.PageIndex = 0;
        BindData();
    }

    protected void drpAssignedTo_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["filterunread"] = null;
        setFileterSession();
        // DataGrid1.CurrentPageIndex = 0;
        DataGrid1.PageIndex = 0;
        BindData();
    }

    protected void drpStatuses_SelectedIndexChanged(object sender, EventArgs e)
    {
        setFileterSession();
        //  DataGrid1.CurrentPageIndex = 0;
        DataGrid1.PageIndex = 0;
        BindData();
    }

    private void setFileterSession()
    {

        Session["HideClosed"] = chkHideClosed.Checked;
        Session["IncludeDeleted"] = chkViewDeleted.Checked;
        Session["Filter"] = drpFilter.SelectedValue;
        Session["Project"] = drpProjects.SelectedValue;
        Session["Category"] = drpCategories.SelectedValue;
        Session["ReportedBy"] = drpReportedBy.SelectedValue;
        Session["Priority"] = drpPriorities.SelectedValue;
        Session["AssignedTo"] = drpAssignedTo.SelectedValue;
        Session["Status"] = drpStatuses.SelectedValue;
        Session["OrderBy"] = lblOrderBy.Text;
        Session["ReadTask"] = ChkReadTask.Checked;

    }

    private void getFilterSession()
    {
        try
        {
            if (Session["HideClosed"] != null)
                chkHideClosed.Checked = (bool)Session["HideClosed"];
            if (hidecheck == false)
            {
                chkHideClosed.Checked = false;
            }
            else
            {
                chkHideClosed.Checked = true;
            }
            if (Session["ReadTask"] != null)
                ChkReadTask.Checked = (bool)Session["ReadTask"];

            if (Session["IncludeDeleted"] != null)
                chkViewDeleted.Checked = (bool)Session["IncludeDeleted"];

            if (Session["Filter"] != null)
                drpFilter.SelectedValue = Session["Filter"].ToString();

            if (Session["Project"] != null)
                drpProjects.SelectedValue = Session["Project"].ToString();

            if (Session["Category"] != null)
                drpCategories.SelectedValue = Session["Category"].ToString();

            if (Session["ReportedBy"] != null)
                drpReportedBy.SelectedValue = Session["ReportedBy"].ToString();

            if (Session["Priority"] != null)
                drpPriorities.SelectedValue = Session["Priority"].ToString();

            if (Session["AssignedTo"] != null)
                drpAssignedTo.SelectedValue = Session["AssignedTo"].ToString();

            if (Session["Status"] != null)
                drpStatuses.SelectedValue = Session["Status"].ToString();

            if (Session["OrderBy"] != null)
                lblOrderBy.Text = Session["OrderBy"].ToString();
        }
        catch
        {

        }
    }





    protected void DataGrid1_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //DataGrid1.CurrentPageIndex = e.NewPageIndex;
        //BindData();

    }



    void sendNotification(string strTaskId, string StrShrtDesc)
    {
        //sql = "select email, EmailNotification,PR_EmailNotification ";
        //sql += " from users, task_subscriptions ";
        //sql += " where users.username = task_subscriptions.username ";
        //sql += " and task_subscriptions.task_id =" + strTaskId.ToString() + " and task_subscriptions.username <> '" + Session["admin"].ToString() + "'";
        //DataSet ds = DatabaseHelper.getDataset(sql);
        DataView dvsendN = new DataView(ds.Tables[19]);
        dvsendN.RowFilter = "task_id=" + strTaskId;
        //string htmlColor = dvcolor.ToTable().Rows[0]["bg_color"].ToString();

        if (dvsendN.ToTable() != null && dvsendN.ToTable().Rows.Count > 0)
        {
            string strHtml = generateAdminEmail(strTaskId, StrShrtDesc);

            string toEmails = "";


            for (int i = 0; i < dvsendN.ToTable().Rows.Count; i++)
            {
                if (dvsendN.ToTable().Rows[i]["EmailNotification"].ToString() == "Y")
                {

                    string reqPR = "";
                    string[] strPR = dvsendN.ToTable().Rows[i]["PR_EmailNotification"].ToString().Split(new char[] { ';' });
                    for (int j = 0; j < strPR.Length - 1; j++)
                    {
                        reqPR = strPR[j];
                        if (reqPR == "All")
                        {
                            toEmails += dvsendN.ToTable().Rows[i]["email"].ToString() + ";";
                        }
                        else if (reqPR == drpPriorities.SelectedValue)
                        {
                            toEmails += dvsendN.ToTable().Rows[i]["email"].ToString() + ";";
                        }

                    }


                }


            }

            toEmails = toEmails.TrimEnd(';');

            if (emailSubject == "")
            {
                emailSubject = "Task ID:" + strTaskId.ToString() + " was updated - " + StrShrtDesc.ToString().Trim() + " (Task ID:" + strTaskId.ToString() + ")";
            }
            ////if (chkEmailnotify.Checked)
            ////{
            bool flag = DatabaseHelper.sendEmailTasks(toEmails, emailSubject, strHtml);
            ////}
            ////else
            ////{
            ////}
        }
    }

    private string generateAdminEmail(string strTaskId, string StrShrtDesc)
    {
        //DataSet dsTask = DatabaseHelper.getDataset("select * from tasks where task_id=" + strTaskId);
        DataView dvTask = new DataView(ds.Tables[0]);
        dvTask.RowFilter = "task_id=" + strTaskId;
        string strBody = "";
        if (dvTask == null || dvTask.ToTable().Rows.Count <= 0 || dvTask.ToTable().Rows.Count <= 0)
        {
            /// divMessage.InnerHtml = "Task not found.<br><br><a href='Demo.aspx'>View tasks</a>";
        }
        else
        {
            strBody += "Task: <span style=\"color:red\">";

            strBody += task_action;

            strBody += " (<a style=\"color:red\" href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_task.aspx?id=" + strTaskId + "'>" + strTaskId + "</a>) </span> " + DateTime.Parse(dvTask.ToTable().Rows[0]["last_updated_date"].ToString()).ToString("dd MMM h:mm tt") + "<br><br>";

            //////if (lblTaskId.Text != "New")
            //////{
            //////   /// string attribures = record_changes_for_email();

            //////    if (attribures.ToString().Trim() != "")
            //////    {
            //////        strBody += "The attributes below have been amended.<br>";
            //////        strBody += "<span style=\"color:red\">" + attribures + "</span><br>";
            //////    }
            //////}

            strBody += "<a href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_task.aspx?id=" + strTaskId + "'>" + StrShrtDesc.ToString().Trim() + "</a>&nbsp;&nbsp;&nbsp;(" + dvTask.ToTable().Rows[0]["priority"].ToString() + " : " + dvTask.ToTable().Rows[0]["status"].ToString() + ")<br><br>";
            strBody += "Emp: " + ((string)DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + dvTask.ToTable().Rows[0]["assigned_to_user"].ToString() + "'")) + "<br>";
            strBody += "Project: " + dvTask.ToTable().Rows[0]["project"].ToString() + "<br>";
            strBody += "Relevant URL: <a href='" + dvTask.ToTable().Rows[0]["Relevant_URL"].ToString() + "' target='_blank'>" + dvTask.ToTable().Rows[0]["Relevant_URL"].ToString() + "</a><br>";
            strBody += "<br><br>";

            //sql = "Select * from task_comments where task_id=" + strTaskId.ToString() + " and deleted <> 1 order by tc_id desc";
            //DataSet dsTaskDetails = DatabaseHelper.getDataset(sql);

            DataView dvTaskdetails = new DataView(ds.Tables[20]);
            dvTaskdetails.RowFilter = "task_id=" + strTaskId;

            strBody += "<table cellspacing=\"0\" border=\"1\" style=\"width: 99%;border-collapse: collapse;\">";
            strBody += "<tbody>";

            for (int i = 0; i < dvTaskdetails.ToTable().Rows.Count; i++)
            {
                strBody += "<tr>";
                strBody += "<td>";
                strBody += "<div style=\"border: 1px none Green;\">";
                strBody += "<table width=\"100%\">";
                strBody += "<tbody>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                if (dvTaskdetails.ToTable().Rows[i]["qflag"].ToString() == "1")
                {
                    strBody += "<span style=\"color:red;\">Question posted by " + dvTaskdetails.ToTable().Rows[i]["username"].ToString() + " on " + DateTime.Parse(dvTaskdetails.ToTable().Rows[i]["post_date"].ToString()).ToString("dd MMM h:mm tt") + "</span>";
                }
                else
                {
                    strBody += "<span style=\"color: Green;\">comment " + dvTaskdetails.ToTable().Rows[i]["tc_id"].ToString() + " posted by " + dvTaskdetails.ToTable().Rows[i]["username"].ToString() + " on " + DateTime.Parse(dvTaskdetails.ToTable().Rows[i]["post_date"].ToString()).ToString("dd MMM h:mm tt") + "</span>";
                }
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\"> </td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span>" + dvTaskdetails.ToTable().Rows[i]["comment"].ToString() + "</span>";
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "</tbody>";
                strBody += "</table>";
                strBody += "</div>";
                strBody += "</td>";
                strBody += "</tr>";
            }
            strBody += "</tbody>";
            strBody += "</table>";
        }
        return strBody;
    }

    protected void btnClearFilters_Click(object sender, EventArgs e)
    {
        Session["filter"] = null;
        Session["filterunread"] = null;
        load_dropdowns();
        load_filterDropDown();
        chkViewDeleted.Checked = false;
        chkHideClosed.Checked = false;
        ChkReadTask.Checked = true;
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        //if (DatabaseHelper.isAdmin(Session["admin"].ToString()))
        //{
        //    drpFilter.SelectedValue = "All tasks";
        //}
        //else
        //{
        //    drpFilter.SelectedValue = "Show my open tasks";
        //}
        setFileterSession();
        BindData();
    }




    void bindNewTask()
    {
        load_dropdowns();

        load_filterDropDown();

        chkViewDeleted.Checked = false;

        lblOrderBy.Text = " ORDER BY last_updated_date desc";

        if (ds.Tables[7].Rows[0]["admin"].ToString() == "Y")
        {
            drpFilter.SelectedValue = ds.Tables[10].Rows[0]["default_selected_item"].ToString();

        }
        else
        {
            drpFilter.SelectedValue = "Show my open tasks";
        }
        drpStatuses.SelectedValue = "new";

        setFileterSession();
        DataGrid1.PageIndex = 0;
        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        BindData();

    }

    void bindCrntWrkingTasks()
    {
        load_dropdowns();

        load_filterDropDown();

        chkViewDeleted.Checked = false;

        lblOrderBy.Text = " ORDER BY last_updated_date desc";

        if (ds.Tables[7].Rows[0]["admin"].ToString() != "Y")
        {
            drpFilter.SelectedValue = "Show my open tasks";
        }
        else
        {

            drpFilter.SelectedValue = "All emps CWO";

        }
        if (Session["filter"].ToString() == "CWO")
        {
            drpStatuses.SelectedValue = "CWO";
        }
        else
        {
            drpStatuses.SelectedValue = "CWO/CWO but away";
        }

        setFileterSession();


        DataGrid1.PageIndex = 0;
        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        BindData();

    }
    void bindPR0Tasks()
    {
        load_dropdowns();

        load_filterDropDown();

        chkViewDeleted.Checked = false;

        lblOrderBy.Text = " ORDER BY last_updated_date desc";

        drpFilter.SelectedValue = "Show my open tasks";

        drpPriorities.SelectedIndex = 1;
        setFileterSession();

        DataGrid1.PageIndex = 0;
        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;

        BindData();
    }

    void bindTskNewComment()
    {
        load_dropdowns();

        load_filterDropDown();

        chkViewDeleted.Checked = false;

        lblOrderBy.Text = " ORDER BY last_updated_date desc";

        drpFilter.SelectedValue = "Show my open tasks";

        drpPriorities.SelectedIndex = 2;
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        setFileterSession();

        DataGrid1.PageIndex = 0;
        BindData();
    }

    void bindPR1aTasks()
    {
        load_dropdowns();

        load_filterDropDown();

        chkViewDeleted.Checked = false;

        lblOrderBy.Text = " ORDER BY last_updated_date desc";

        drpFilter.SelectedValue = "Show my open tasks";

        drpPriorities.SelectedIndex = 2;
        setFileterSession();

        DataGrid1.PageIndex = 0;
        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        BindData();
    }

    void bindPR1cTasks()
    {

        load_dropdowns();

        load_filterDropDown();

        chkViewDeleted.Checked = false;

        lblOrderBy.Text = " ORDER BY last_updated_date desc";

        drpFilter.SelectedValue = "Show my open tasks";

        drpPriorities.SelectedIndex = 4;
        setFileterSession();

        DataGrid1.PageIndex = 0;
        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        BindData();
    }

    void bindPR1bTasks()
    {
        load_dropdowns();

        load_filterDropDown();

        chkViewDeleted.Checked = false;

        lblOrderBy.Text = " ORDER BY last_updated_date desc";

        drpFilter.SelectedValue = "Show my open tasks";

        drpPriorities.SelectedIndex = 3;
        setFileterSession();

        DataGrid1.PageIndex = 0;
        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        BindData();
    }

    void bindUnansweredQuestions()
    {
        load_dropdowns();

        load_filterDropDown();

        drpFilter.SelectedValue = "Show my open tasks";

        chkViewDeleted.Checked = false;

        //        sql = @"select distinct tasks.* from task_comments,tasks 
        //                where tasks.task_id = task_comments.task_id and tasks.deleted <> 1 and task_comments.deleted <> 1 and qflag=1 ";
        sql = "";
        //if (!DatabaseHelper.can_Show_Closed_Tasks(Session["admin"].ToString()))
        if (ds.Tables[15].Rows[0]["Show_Closed_Tasks"].ToString() != "Y")
        {
            sql += " " + "and status <> 'closed' ";
        }

        sql += " and task_comments.QuesTo = '" + Session["admin"] + "' ";

        // DataSet ds = DatabaseHelper.getDataset(sql);
       DataSet  dsques = DatabaseHelper.getsp_questionDataset(sql);
        //ds = formatTaskDataSet(ds);

       if (dsques != null && dsques.Tables[0].Rows.Count > 0)
        {
            TopFilterLable.InnerHtml = "Filtered by Unanswered questions";
            PagedDataSource objds = new PagedDataSource();
            objds.DataSource = dsques.Tables[0].DefaultView;

            LblRecordsno.Text = objds.DataSourceCount.ToString();
            DataGrid1.DataSource = dsques.Tables[0];
            DataGrid1.DataBind();
        }
        else
        {
            LblRecordsno.Text = "0";

        }
    }

    void bindDefault()
    {
        load_dropdowns();

        load_filterDropDown();

        lblOrderBy.Text = " ORDER BY last_updated_date desc";

        chkViewDeleted.Checked = false;





        if (ds.Tables[7].Rows[0]["admin"].ToString() == "Y")
        {
            drpFilter.SelectedValue = ds.Tables[10].Rows[0]["default_selected_item"].ToString();

        }
        else
        {
            drpFilter.SelectedValue = "Show my open tasks";
        }

        getFilterSession();

        BindData();
    }

    protected void chkViewDeleted_CheckedChanged(object sender, EventArgs e)
    {
        load_dropdowns();
        load_filterDropDown();
        lblOrderBy.Text = " ORDER BY last_updated_date desc";
        setFileterSession();
        BindData();
    }
    protected void chkHideClosed_CheckedChanged(object sender, EventArgs e)
    {
        setFileterSession();
        BindData();
    }

     

    protected void getTotalHrsReport(string strSql)
    {
        //        sql = @"select sum(ETC)
        //                from tasks
        //                where task_id in (" + strSql + ")";

        int ETC = 0;

        //  object objETC = DatabaseHelper.executeScalar(sql);
        string objETC = ds.Tables[16].Rows[0]["ETC"].ToString();

        if (objETC != null && objETC.ToString() != "" && objETC.ToString() != "0")
        {
            ETC = int.Parse(objETC);
        }
        else
        {
            ETC = 0;
        }

        //        sql = @"select sum(datediff(minute,started_date,finished_date))
        //                from hours_reporting
        //                where finished_date is not null and task_id in (" + strSql + ")";

        int hrsTaken = 0;
        //object objHrsTaken = DatabaseHelper.executeScalar(sql);

        string objHrsTaken = ds.Tables[17].Rows[0]["Total_ETC"].ToString();
        if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
        {
            hrsTaken += int.Parse(objHrsTaken);
        }
        else
        {
            hrsTaken = 0;
        }

        //        sql = @"select sum(datediff(minute,started_date,getdate()))
        //                from hours_reporting
        //                where finished_date is null and task_id in (" + strSql + ")";
        if (hrsTaken == 0)
        {
            //objHrsTaken = DatabaseHelper.executeScalar(sql);
            objHrsTaken = ds.Tables[18].Rows[0]["NTotalEtc"].ToString();
            if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
            {
                hrsTaken += int.Parse(objHrsTaken);
            }

        }
        lblETC.Text = "" + (ETC / 60) + " hrs " + (ETC % 60) + " mins";

        lblTotalHrsTaken.Text = "" + (hrsTaken / 60) + " hrs " + (hrsTaken % 60) + " mins";

        int hrsLeft = ETC - hrsTaken;

        if (hrsLeft < 0)
        {
            hrsLeft = -1 * hrsLeft;
            lblExpectedHrsLeft.Text = "-" + (hrsLeft / 60) + " hrs " + (hrsLeft % 60) + " mins";
        }
        else
        {
            lblExpectedHrsLeft.Text = "" + (hrsLeft / 60) + " hrs " + (hrsLeft % 60) + " mins";
        }

    }

    protected void ChkReadTask_CheckedChanged(object sender, EventArgs e)
    {

        setFileterSession();
        BindData();

    }
    protected void LnkBtnToDeleteAll_Click(object sender, EventArgs e)
    {

        int i = 0;
        foreach (GridViewRow row in DataGrid1.Rows)
        {
            // Access the CheckBox 
            try
            {
                CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                ///Label GetItemID = (Label)row.FindControl("lblID");
                if (cb != null && cb.Checked)
                {
                    i = i + 1;


                    sql = "Update tasks set deleted=1,last_updated_date = getdate(),last_updated_user='" + Session["admin"].ToString().Replace("'", "''") + "' where task_id=" + row.Cells[19].Text;

                    int intResult = DatabaseHelper.executeNonQuery(sql);

                    if (intResult != 0)
                    {

                        sql = @" insert into task_updates([task_id],[username],[post_date],[comment]) 
                            values(" + row.Cells[19].Text + ",'" + Session["admin"].ToString().Replace("'", "''") + "',getdate(),'Task was deleted.'); ";

                        intResult = DatabaseHelper.executeNonQuery(sql);

                        try
                        {

                            sql = @"select *  from task_comments where task_id =" + row.Cells[19].Text + " and  tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "')";
                            /// sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

                            DataSet dscomment = DatabaseHelper.getDataset(sql);

                            if (dscomment != null && dscomment.Tables[0].Rows.Count > 0)
                            {
                                for (int j = 0; j < dscomment.Tables[0].Rows.Count; j++)
                                {
                                    try
                                    {


                                        string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                                        sqlinsert += " values(" + dscomment.Tables[0].Rows[j]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                                        int intCommentResult = DatabaseHelper.executeNonQuery(sqlinsert);

                                    }
                                    catch
                                    {
                                    }
                                }

                            }
                        }
                        catch { }




                    }

                }
            }
            catch { }
        }
        if (i > 0)
        {
            Response.Redirect("Demo.aspx", false);
        }
        else
        {

            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('Please select task to delete.');</script>");
            /// lblMessage.Text = "Please enter date of birth.";
            return;

        }



    }
    protected void LnkBtnToChngPR_Click(object sender, EventArgs e)
    {

        int i = 0;
        foreach (GridViewRow row in DataGrid1.Rows)
        {
            // Access the CheckBox 
            try
            {
                CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                if (cb != null && cb.Checked)
                {
                    i = i + 1;
                    sql = "Update tasks set priority='" + DrpMultipleTaskPR.SelectedItem.Text.Replace("'", "''") + "',last_updated_date =getdate(),last_updated_user='" + Session["admin"].ToString().Replace("'", "''") + "' where task_id=" + row.Cells[0].Text;

                    int intResult = DatabaseHelper.executeNonQuery(sql);

                    if (intResult != 0)
                    {
                        string cmnt = "";
                        cmnt = "changed priority from " + row.Cells[8].Text + " to " + DrpMultipleTaskPR.SelectedItem;

                        sql = @" insert into task_updates([task_id],[username],[post_date],[comment]) 
                            values(" + row.Cells[0].Text + ",'" + Session["admin"].ToString().Replace("'", "''") + "',getdate(),'" + cmnt.ToString() + "'); ";

                        intResult = DatabaseHelper.executeNonQuery(sql);

                    }

                }
            }
            catch { }
        }
        if (i > 0)
        {
            Response.Redirect("Demo.aspx", false);
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('Please select task to change priority.');</script>");

            return;

        }



    }
    protected void LnkChngAssingedTo_Click(object sender, EventArgs e)
    {
        int i = 0;
        foreach (GridViewRow row in DataGrid1.Rows)
        {
            // Access the CheckBox 
            try
            {
                CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                if (cb != null && cb.Checked)
                {
                    i = i + 1;
                    sql = "Update tasks set assigned_to_user='" + DrpMultipleTaskAssing.SelectedItem.Text.Replace("'", "''") + "',last_updated_date =getdate(),last_updated_user='" + Session["admin"].ToString().Replace("'", "''") + "' where task_id=" + row.Cells[0].Text;

                    int intResult = DatabaseHelper.executeNonQuery(sql);

                    if (intResult != 0)
                    {
                        string cmnt = "";
                        cmnt = "changed assinged to from " + row.Cells[9].Text + " to  " + DrpMultipleTaskAssing.SelectedItem.Text.Replace("'", "''");


                        sql = @" insert into task_updates([task_id],[username],[post_date],[comment]) 
                            values(" + row.Cells[0].Text + ",'" + Session["admin"].ToString().Replace("'", "''") + "',getdate(),'" + cmnt.ToString() + "'); ";

                        intResult = DatabaseHelper.executeNonQuery(sql);

                    }

                }
            }
            catch { }
        }
        if (i > 0)
        {
            Response.Redirect("Demo.aspx", false);
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('Please select task to change assigned to.');</script>");
            return;

        }

    }
    protected void LnkChngStatus_Click(object sender, EventArgs e)
    {

        int i = 0;
        foreach (GridViewRow row in DataGrid1.Rows)
        {
            // Access the CheckBox 
            try
            {
                CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                if (cb != null && cb.Checked)
                {
                    i = i + 1;
                    sql = "Update tasks set status='" + DrpMultipleTaskStatus.SelectedItem.Text.Replace("'", "''") + "',last_updated_date =getdate(),last_updated_user='" + Session["admin"].ToString().Replace("'", "''") + "' where task_id=" + row.Cells[0].Text;

                    int intResult = DatabaseHelper.executeNonQuery(sql);

                    if (intResult != 0)
                    {
                        string cmnt = "";
                        cmnt = "changed assinged to from " + row.Cells[9].Text + " to  " + DrpMultipleTaskAssing.SelectedItem.Text.Replace("'", "''");


                        sql = @" insert into task_updates([task_id],[username],[post_date],[comment]) 
                            values(" + row.Cells[0].Text + ",'" + Session["admin"].ToString().Replace("'", "''") + "',getdate(),'" + cmnt.ToString() + "'); ";

                        intResult = DatabaseHelper.executeNonQuery(sql);

                    }

                }
            }
            catch { }
        }
        if (i > 0)
        {
            Response.Redirect("Demo.aspx", false);
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('Please select task to change status.');</script>");

            return;

        }


    }
    protected void LnkBtnToMarkAllRead_Click(object sender, EventArgs e)
    {

        int i = 0;
        foreach (GridViewRow row in DataGrid1.Rows)
        {
            try
            {
                CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                if (cb != null && cb.Checked)
                {
                    i = i + 1;

                    sql = "insert into read_task([task_id],[username])values(" + row.Cells[19].Text + ",'" + Session["admin"].ToString() + "')";
                    DatabaseHelper.executeNonQuery(sql);

                    sql = @"select *  from task_comments where task_id =" + row.Cells[19].Text + " and  tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "')";
                    /// sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

                    DataSet ds = DatabaseHelper.getDataset(sql);

                    if (ds != null && ds.Tables[0].Rows.Count > 0)
                    {
                        for (int j = 0; j < ds.Tables[0].Rows.Count; j++)
                        {
                            try
                            {


                                string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                                sqlinsert += " values(" + ds.Tables[0].Rows[j]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                                int intResult = DatabaseHelper.executeNonQuery(sqlinsert);

                            }
                            catch
                            {
                            }
                        }

                    }

                    Response.Redirect("Demo.aspx", false);
                }


            }

            catch { }
        }
        if (i > 0)
        {
            Response.Redirect("Demo.aspx", false);
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('Please select task to mark as read.');</script>");

            return;

        }


    }
    protected void LnkNotification_Click(object sender, EventArgs e)
    {
        Response.Redirect("Notification.aspx", false);
    }

    protected void DataGrid1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    void BindOverdueTask()
    {
        load_dropdowns();

        load_filterDropDown();

        chkViewDeleted.Checked = false;

        lblOrderBy.Text = " ORDER BY last_updated_date desc";

        drpFilter.SelectedValue = "Show my open tasks";

        drpPriorities.SelectedIndex = 4;
        setFileterSession();
        //DataGrid1.CurrentPageIndex = 0;
        Session["filterunread"] = "";
        Session["OverdueTask"] = "True";
        BindData();
    }


    protected void DataGrid1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        //DataGrid1.PageIndex = e.NewPageIndex;
        //BindData();
        DataGrid1.PageIndex = e.NewPageIndex;
        DataGrid1.DataSource = ds;
        DataGrid1.Page.DataBind();



    }
    protected void DataGrid1_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.DataItemIndex != -1)
        {



            ((Label)e.Row.FindControl("lblReportedOn")).Text = DateTime.Parse(((Label)e.Row.FindControl("lblReportedOn")).Text).ToString("dd MMM h:mm tt").ToString();
            ((Label)e.Row.FindControl("lblLastUpdatedOn")).Text = DateTime.Parse(((Label)e.Row.FindControl("lblLastUpdatedOn")).Text).ToString("dd MMM h:mm tt").ToString();

            LinkButton deleteButton = (LinkButton)e.Row.FindControl("lnkdel");
            deleteButton.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete this task?')");
            //string htmlColor = (string)DatabaseHelper.executeScalar("select bg_color from priorities where priority_name='" + e.Row.Cells[8].Text + "'");
            //e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml(htmlColor);
            DataView dvcolor = new DataView(ds.Tables[12]);
            dvcolor.RowFilter = "priority_name='" + e.Row.Cells[8].Text + "'";
            string htmlColor = dvcolor.ToTable().Rows[0]["bg_color"].ToString();
            e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml(htmlColor);

            int ETC = Convert.ToInt32(((HiddenField)e.Row.Cells[17].Controls[3]).Value);

            e.Row.Cells[13].Text = "" + (ETC / 60) + "' " + (ETC % 60) + '"';


            int hrsTaken = 0;
            if (((HiddenField)e.Row.FindControl("hdhrs")).Value != "" && ((HiddenField)e.Row.FindControl("hdhrs")).Value != null)
            {
                hrsTaken = Convert.ToInt32(((HiddenField)e.Row.FindControl("hdhrs")).Value);
            }
            if (hrsTaken == 0)
            {
                if (((HiddenField)e.Row.FindControl("hdgethrs")).Value != "" && ((HiddenField)e.Row.FindControl("hdgethrs")).Value != null)
                {
                    hrsTaken = Convert.ToInt32(((HiddenField)e.Row.FindControl("hdgethrs")).Value);
                }

            }
            e.Row.Cells[14].Text = "" + (hrsTaken / 60) + "' " + (hrsTaken % 60) + '"';

            int hrsLeft = ETC - hrsTaken;

            if (hrsLeft < 0)
            {
                hrsLeft = -1 * hrsLeft;
                e.Row.Cells[15].Text = "-" + (hrsLeft / 60) + "' " + (hrsLeft % 60) + '"';
            }
            else
            {
                e.Row.Cells[15].Text = "" + (hrsLeft / 60) + "' " + (hrsLeft % 60) + '"';
            }
            LinkButton readMarkButton = (LinkButton)e.Row.FindControl("lnkReadMark");
            // int objIsRead;

            try
            {



                DataView dv = new DataView(ds.Tables[11]);

                dv.RowFilter = "task_id='" + e.Row.Cells[19].Text + "'";

                if (dv.Count != null && dv.Count >= 1)
                {


                    readMarkButton.Text = "[<span style='color:red'>Unread.</span>  Mark as read]";

                }
                else
                {

                    readMarkButton.Text = "[<span style='color:green'>Read.</span> Mark as unread]";
                }
            }
            catch
            {

            }


            Label LblAllComment = (Label)e.Row.FindControl("lblshrtDesc");
            LblAllComment.Attributes.Add("onmouseover", "get_(this);");
            LblAllComment.Attributes.Add("onmouseout", "get_1(this);");
            CheckBox ChkTasks = (CheckBox)e.Row.FindControl("chkSelect");




            try
            {
                if (!DatabaseHelper.can_Delete_Tasks(Session["admin"].ToString()))
                {
                    deleteButton.Visible = false;
                    ChkTasks.Visible = false;
                }
                else
                {
                    deleteButton.Visible = true;
                    ChkTasks.Visible = true;

                }
            }
            catch { }

            try
            {

                if (e.Row.Cells[7].Text == "closed")
                {
                    closecount++;
                }

            }
            catch { }
        }




    }
    protected void DataGrid1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        GridViewRow gvr;
        int rows;
        GridViewRow row;

        if (e.CommandName == "ReadMark")
        {

            gvr = (GridViewRow)((LinkButton)e.CommandSource).NamingContainer;
            rows = gvr.RowIndex;

            row = DataGrid1.Rows[rows];
            LinkButton readMarkButton = (LinkButton)row.FindControl("lnkReadMark");

            if (readMarkButton.Text == "[<span style='color:red'>Unread.</span>  Mark as read]")
            {
                sql = @"insert into read_task([task_id],[username]) 
                        values(" + row.Cells[19].Text + ",'" + Session["admin"].ToString() + "')";
                DatabaseHelper.executeNonQuery(sql);



                sql = @"select *  from task_comments where task_id =" + row.Cells[19].Text + " and  tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "')";

                DataSet ds = DatabaseHelper.getDataset(sql);

                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        try
                        {


                            string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                            sqlinsert += " values(" + ds.Tables[0].Rows[i]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                            int intResult = DatabaseHelper.executeNonQuery(sqlinsert);

                        }
                        catch
                        {
                        }
                    }

                }

                Response.Redirect("Demo.aspx", false);
            }
            else
            {

                sql = @"delete from read_task where [task_id]=" + row.Cells[19].Text + " and [username]='" + Session["admin"] + "'";
                DatabaseHelper.executeNonQuery(sql);

                sql = @"select *  from task_comments where task_id =" + row.Cells[19].Text + " and  tc_id  in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "')";
                DataSet ds = DatabaseHelper.getDataset(sql);

                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        try
                        {

                            string sqlinsert = "Delete from read_comments where tc_id =" + ds.Tables[0].Rows[i]["tc_id"].ToString() + " and username = '" + Session["admin"].ToString() + "'";
                            int intResult = DatabaseHelper.executeNonQuery(sqlinsert);

                        }
                        catch
                        {
                        }
                    }

                }

                Response.Redirect("Demo.aspx", false);
            }
        }

        else if (e.CommandName == "delete")
        {
            gvr = (GridViewRow)((LinkButton)e.CommandSource).NamingContainer;
            rows = gvr.RowIndex;

            row = DataGrid1.Rows[rows];

            sql = "update tasks set deleted=1 where task_id = " + row.Cells[19].Text + "; ";

            int intResult = DatabaseHelper.executeNonQuery(sql);

            if (intResult != 0)
            {
                task_action = ((string)DatabaseHelper.executeScalar("select firstname +' '+ lastname as name from users where username = '" + Session["admin"].ToString() + "'")) + " has deleted task ";

                emailSubject = "Task ID:" + row.Cells[19].Text + " was deleted - " + ((Label)row.FindControl("lblshrtDesc")).Text + " (Task ID:" + row.Cells[19].Text + ")";

                sendNotification(row.Cells[19].Text, ((Label)row.FindControl("lblshrtDesc")).Text);

                sql = @" insert into task_updates([task_id],[username],[post_date],[comment]) 
                            values(" + row.Cells[19].Text + ",'" + Session["admin"].ToString().Replace("'", "''") + "',getdate(),'Task was deleted.'); ";

                intResult = DatabaseHelper.executeNonQuery(sql);



                try
                {

                    sql = @"select *  from task_comments where task_id =" + row.Cells[19].Text + " and  tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "')";

                    DataSet dscomment = DatabaseHelper.getDataset(sql);

                    if (dscomment != null && dscomment.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < dscomment.Tables[0].Rows.Count; i++)
                        {
                            try
                            {


                                string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                                sqlinsert += " values(" + dscomment.Tables[0].Rows[i]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                                int intCommentResult = DatabaseHelper.executeNonQuery(sqlinsert);

                            }
                            catch
                            {

                            }
                        }



                    }
                }
                catch { }



                Response.Redirect("Demo.aspx");
            }



        }


    }
    protected void DataGrid1_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
    {

        if (Session["Column"] != null)
        {
            if (e.SortExpression.ToString() == Session["Column"].ToString())
            {

                if (Session["Order"] == "ASC")
                {
                    lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " DESC";
                    Session["Order"] = "DESC";
                }
                else
                {
                    lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " ASC";
                    Session["Order"] = "ASC";
                }
            }
        }
        else
        {

            lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " ASC";
            Session["Order"] = "ASC";
        }

        Session["Column"] = e.SortExpression.ToString();
        Session["OrderBy"] = lblOrderBy.Text;
        BindData();

    }
}
