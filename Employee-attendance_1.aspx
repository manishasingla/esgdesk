﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Employee-attendance_1.aspx.cs" Inherits="Employee_attendance_1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%--<%@ Register TagName="WeeklyReport" TagPrefix="WR" Src="~/Admin/LMSAdmin/LMS-Weekly-report.ascx" %>--%>
<%@ Register Src="~/Header.ascx" TagName="Header" TagPrefix="uc1" %>
<%@ Register Src="~/footer.ascx" TagName="footer" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<title><%= ConfigurationManager.AppSettings["CompanyName"].ToString()%></title>
<link href="StyleSheet.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
        function get_(div_) {

            div_ = div_.id + "1";

            document.getElementById(div_).style.display = "block";
        }
        function get_1(div_) {

            div_ = div_.id + "1";

            document.getElementById(div_).style.display = "none";
        }

      

    </script>
<script language="javascript" type="text/javascript">
        function hideCalendar() {
            $find("<%=CalendarExtender3.ClientID%>").hide();
            $find("<%=CalendarExtender2.ClientID%>").hide();
            return false;
        } 
</script>
<link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
<link href="../SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link href="../SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/MenuMatic.css" type="text/css" media="screen" charset="utf-8" />
<link rel="stylesheet" type="text/css" href="../slider.css" />

<style type="text/css">
.style2 {
	color: #686868;
	width: 670px;
}
.style6 {
	width: 161px;
}
fieldset {
	background-color: #F3F3F3;
	border: 1px solid #CCCCCC;
	padding: 10px 20px 20px;
}

.leavetbl td{ border:1px solid #999999; padding:3px;}
.xls_button
{
	background-image:url(xls.png);
	background-repeat:no-repeat;
}

</style>
</head>

<body>
<form id="form1" runat="server" style="font-size: small;">
  <div> 
    <script language="javascript" >

            function fnValidateProj(sender, args) {
                alert("called");
                var control1 = document.getElementById('<%= ddlProj.ClientID %>');
                var control2 = document.getElementById('<%= ddlEmp.ClientID %>');
                var selectedvalue = control.options[control.selectedIndex].value;
                alert(selectedvalue);
                if (selectedvalue == "0") {
                    control2.disabled = true;
                }
            }
    </script> 
  </div>
  <div>
    <uc1:Header ID="Header1" runat="server" />
    <table width="100%"  onkeypress="javascript:keypressHandler(event)">
      <tr>
        <td  valign="top" align="left" width="20%"></td>
        <td valign="top" align="left" width="20%" ></td>
        <td  valign="top" align="left" width="20%"  ></td>
        <td valign="top" align="left" width="20%" ></td>
        <td valign="top" align="left"  width="20%"></td>
      </tr>
      <tr>
        <td></td>
        <td valign="top"></td>
        <td valign="top"></td>
        <td valign="top" class="style6"></td>
        <td></td>
      </tr>
      <tr>
        <td></td>
        <td ></td>
        <td ></td>
        <td ></td>
      </tr>
    </table>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableClientScript="true" ShowMessageBox="true" ShowSummary="false" ValidationGroup="g1" />
  </div>
  <div> </div>
  <div>
  <br />
    <table align="right" style="margin-right:20px;">
      <tr align="right" >
        <td colspan="5" align="right" ><%-- <asp:ScriptManager ID="ScriptManager1" EnablePageMethods="true" runat="server">
                    </asp:ScriptManager>--%>
          <asp:Button ID="btnshow" runat="server" Text="Show time sheet" OnClick="btnshow_Click" ValidationGroup="g1" Enabled="false" Visible="false"/>
          <asp:ImageButton ID="btnexport" runat="server" OnClick="btnexport_Click" ValidationGroup="g1" AlternateText="Export to excel"  ImageUrl="~/xls.png" /></td>
      </tr>
    </table>
  </div>
  <table width="97%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><fieldset style="padding-bottom:5px;">
          <legend style="font-size:16px; padding:0 2px;"><strong>Fliter by</strong></legend>
          <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="20%" height="25" valign="top">Interval</td>
              <td width="20%" height="25" valign="top">Starting date</td>
              <td width="20%" height="25" valign="top"> Ending date </td>
              <td width="20%" height="25" valign="top">Projects </td>
              <td width="20%" height="25" valign="top">Filter by employee </td>
            </tr>
            <tr>
              <td width="20%" valign="top"><asp:DropDownList ID="ddlTimeInterval" runat="server" valign="top" 
                        Height="22px" onselectedindexchanged="ddlTimeInterval_SelectedIndexChanged" 
                        Width="176px" AutoPostBack="True" >
                  <asp:ListItem Value="1">Today</asp:ListItem>
                  <asp:ListItem Value="2">This week</asp:ListItem>
                  <asp:ListItem Value="3">Last 7 days</asp:ListItem>
                  <asp:ListItem Value="4">This month</asp:ListItem>
                  <asp:ListItem Value="5">Last 31 days</asp:ListItem>
                </asp:DropDownList></td>
              <td width="20%" valign="top"><asp:TextBox ID="GMDStartDate" runat="server" CssClass="input_boxline" 
                    Height="18px" Width="176px"></asp:TextBox>
                <ajaxtoolkit:calendarextender ID="CalendarExtender2" runat="server" TargetControlID="GMDStartDate"
                    Format="yyyy-MM-dd" OnClientDateSelectionChanged='hideCalendar'> </ajaxtoolkit:calendarextender></td>
              <td width="20%" valign="top"><asp:TextBox ID="GMDEndDate" runat="server" CssClass="input_boxline"  
                    AutoPostBack="True" ontextchanged="GMDEndDate_TextChanged" Height="18px" 
                    Width="176px"></asp:TextBox>
                <ajaxtoolkit:calendarextender ID="CalendarExtender3" runat="server" 
                      TargetControlID="GMDEndDate"  OnClientDateSelectionChanged='hideCalendar'
                    Format="yyyy-MM-dd"> </ajaxtoolkit:calendarextender></td>
              <td width="20%" valign="top"><asp:DropDownList ID="ddlProj" runat="server" Height="22px" Width="176px" 
                    initialvalue="0" CausesValidation="true"
                    onselectedindexchanged="ddlProj_SelectedIndexChanged" AutoPostBack="True"
                    ValidationGroup="g1" UseSubmitBehavior = "false" > </asp:DropDownList></td>
              <td width="20%" valign="top"><asp:DropDownList ID="ddlEmp" runat="server" AutoPostBack="true" 
                    OnSelectedIndexChanged="ddlEmp_SelectedIndexChanged" Visible ="true" 
                    ValidationGroup="g1" Height="22px" Width="176px"> </asp:DropDownList></td>
            </tr>
            <tr>
            <td height="25" valign="top"><%-- <asp:CompareValidator ID="CompareValidator5" runat="server" ErrorMessage="Please enter a valid date"
                    Operator="DataTypeCheck" ControlToValidate="GMDEndDate" Type="Date" 
                    ValidationGroup="g1"></asp:CompareValidator>--%></td>
            <td height="25" valign="top"><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please select a start date"
         ControlToValidate="GMDStartDate" ValidationGroup="g1"></asp:RequiredFieldValidator>
          <br />
          <asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="Please enter a valid  date"
         Operator="DataTypeCheck" ControlToValidate="GMDStartDate" Type="Date" 
         ValidationGroup="g1"></asp:CompareValidator></td>
            <td height="25" valign="top"><asp:CompareValidator ID="CompareValidator3" runat="server" ErrorMessage="Please enter a valid date"
                    Operator="DataTypeCheck" ControlToValidate="GMDEndDate" Type="Date" 
                    ValidationGroup="g1"></asp:CompareValidator>
          <br />
          <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="End date should be greater or equal to start date."
                    ControlToCompare="GMDStartDate" ControlToValidate="GMDEndDate" Operator="GreaterThanEqual"
                    Type="Date" ValidationGroup="g1"></asp:CompareValidator></td>
            <td height="25" valign="top"><%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please select a project" 
                     ControlToValidate="ddlProj" ValidationGroup="g1" initialvalue="--Select--" ></asp:RequiredFieldValidator>--%>
        <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="Please select a project" ValidationGroup="g1" ControlToValidate="ddlProj"
                    ClientValidationFunction="fnValidateProj"> </asp:CustomValidator></td>
            <td height="25" valign="top"></td>
            
            </tr>
          </table>
        </fieldset></td>
    </tr>
    <tr>
      <td><fieldset style="margin-top:10px;">
          <legend  style="font-size:16px; padding:0 2px;"><strong>Resource planning report</strong></legend>
          <div id="main" runat ="server">
            <div id="HtmlDesign" runat="server"> </div>
            <div id="DateWiseReport" runat="server">
              <table width="100%" cellpadding="0" cellspacing="0" id="WeeklyReport" style="color: #333333; font-size: 16px; margin-top:10px;" runat="server">
                <tr>
                  <td align="left" height="10px"><asp:Label ID="lblLRHeading" runat="server" Text="" style="font-size:medium;  color:#00a651; font-weight:bold" align="left"></asp:Label></td>
                </tr>
                <tr>
                  <td align="left"><asp:Label ID="lbl_pageinfo" runat="server" Text="" style="font-size:medium; " align="left"></asp:Label></td>
                </tr>
                <tr>
                  <td align="center" ><asp:Repeater ID="rptAbsentEmpByWeek" runat="server" OnItemDataBound="rptAbsentEmpByWeek_ItemDataBound">
                      <ItemTemplate>
                        <table style="font-size: 15px" width="100%" border="0">
                          <tr>
                            <td colspan="2" style="font-size: 16px; padding-top:15px;"><b> <%# String.Format("{0:D}",(Container.DataItem)) %> (<%# Convert.ToDateTime(Container.DataItem).DayOfWeek %>)</b></td>
                          </tr>
                          <tr>
                            <td colspan="2"><asp:Repeater ID="Repeater2" runat="server">
                                <FooterTemplate> <%="</ul>" %> </FooterTemplate>
                                <HeaderTemplate>
                                  <table style="font-size: 12px ;" width="100%" border="0" cellpadding="0" cellspacing="0" class="leavetbl">
                                    <tr style="font-size:12px; background:#e1e1e1;">
                                      <td  width="10%" height="25"><strong>Employee name</strong></td>
                                      <td  width="10%"><strong>Planned</strong></td>
                                      <td  width="10%"><strong>Unplanned</strong></td>
                                      <td  width="70%"><strong>Reason</strong></td>
                                    </tr>
                                  </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                  <table style="font-size: 12px" width="100%" border="0" cellpadding="0" cellspacing="0"  class="leavetbl">
                                    <tr style="background:#f7f7f7; line-height:25px;">
                                      <td width="10%" height="25"><%#((System.Data.DataRow)Container.DataItem)[0] %></td>
                                      <td width="10%"><%#((System.Data.DataRow)Container.DataItem)[6] %></td>
                                      <td width="10%"><%#((System.Data.DataRow)Container.DataItem)[7] %></td>
                                      <td width="70%"><%# ((System.Data.DataRow)Container.DataItem)[3]%></td>
                                    </tr>
                                  </table>
                                </ItemTemplate>
                                <AlternatingItemTemplate >
                                  <table style="font-size: 12px" width="100%" border="0"  cellpadding="0" cellspacing="0"  >
                                    <tr>
                                      <td width="8%"><%#((System.Data.DataRow)Container.DataItem)[0] %></td>
                                      <td width="8%"><%#((System.Data.DataRow)Container.DataItem)[6] %></td>
                                      <td width="8%"><%#((System.Data.DataRow)Container.DataItem)[7] %></td>
                                      <td width="73%"><%# ((System.Data.DataRow)Container.DataItem)[3]%></td>
                                    </tr>
                                  </table>
                                </AlternatingItemTemplate>
                              </asp:Repeater></td>
                          </tr>
                        </table>
                      </ItemTemplate>
                    </asp:Repeater></td>
                </tr>
              </table>
            </div>
          </div>
        </fieldset></td>
    </tr>
  </table>
</form>
</body>
</html>
