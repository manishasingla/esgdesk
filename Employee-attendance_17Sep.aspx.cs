﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;
public partial class Employee_attendance : System.Web.UI.Page
{
    static string strreportname = "";

    // =============Weekly Report=================//

    clsEmployee_Logic1 objEmployee = new clsEmployee_Logic1();
    clsLeave_Logic objLeave = new clsLeave_Logic();
    private string strError = "No records found.";
    static DataSet dsAbsentDates = new DataSet();
    static DataView dv = null;
    static string sortByAndArrangeBy = "";
    static double SdblUsed = 0.0;
    string dtSDate;
    string dtEDate;
    DateTime datetime;
    DataSet dsTemp;
    DataTable dtTemp, dtTemp1;
    DateTime dtStartingDate;  // Monday
    DateTime dtEndingDate;
    static int DayNo = 0;
    DataView dvWeeklyReport;
    Boolean blnIsAnyExist = false;
    DataSet dsProj;
    Boolean blnIsDateInterval = false;
    string[] strArrayUser_id = null, strArrayDOA = null, strArrayUserName = null;
    //=============================================//


    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Header.DataBind();
        if (!Page.IsPostBack)
        {
            Session["ProjectName"] = "--Select--";
            //===========Weekly Report ===============//
            ddlTimeInterval.SelectedIndex = 1;
            Session["TI"] = "TW";
            fnSetDefaultDate();
            fnFillDates();
            btnexport.Enabled = false;
            ddlEmp.Enabled = true ;
            //if (dtTemp != null)
            //{
            //    if (dtTemp.Rows.Count > 0)
            //    {
            //        rptAbsentEmpByWeek.DataSource = (from x in dvWeeklyReport.ToTable().AsEnumerable() select x["AbsentDate"]).Distinct();
            //        rptAbsentEmpByWeek.DataBind();
            //    }
            //}

            //=====================//

            string strdate = @"SELECT 
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0),103)   as Date_of_Monday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+1,103) as Date_of_Tuesday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+2,103) as Date_of_Wednesday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+3,103) as Date_of_Thusday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+4,103) as Date_of_Friday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+5,103) as Date_of_Saturday";
            //*****************************For ALL Employee**********************************************

            DataSet ds = DatabaseHelper.getDataset(strdate);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {

                clear();
                dynamically(ds);
                fnFillProjectsddl();
                // fnFillEmpddl();
                //if (ddlEmp.SelectedItem.Text == "All")
                //{
                //    strreportname = ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "_To_" + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString();
                //}
                //else
                //{
                //    strreportname = ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "_To_" + ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString();
                //}
            }


            // ddlProj.SelectedIndex = Convert.ToInt32(Session["DeptId"]);
            //ddlProj.Enabled = false;
        }

    }

    public void clear()
    { }


    protected void btnshow_Click(object sender, EventArgs e)
    {
        string strQuery = "";

        strQuery = @"SELECT  CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0),103) as Date_of_Monday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+1,103) as Date_of_Tuesday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+2,103) as Date_of_Wednesday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+3,103) as Date_of_Thusday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+4,103) as Date_of_Friday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+5,103) as Date_of_Saturday";

        DataSet ds = DatabaseHelper.getDataset(strQuery);
        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            clear();
            strreportname = ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "_To_" + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString();
            dynamically(ds);
        }
        //showAttendence();y
    }

    protected void btnexport_Click(object sender, EventArgs e)
    {
        Response.ContentType = "application/x-msexcel";
        Response.AddHeader("Content-Disposition", "attachment; filename=Timesheet_report_from_'" + strreportname + "'.xls");
        //Response.ContentEncoding = Encoding.UTF8;
        System.IO.StringWriter tw = new System.IO.StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(tw);
        main.RenderControl(hw);
        Response.Write(tw.ToString());
        Response.End();

    }


    //========================Weekly report===========================//

    public void fnFillProjectsddl()
    {
        try
        {
            if (Session["username"] != null)
            {
                DataSet ds = DatabaseHelper.getCompanyName(Session["clientid"].ToString(), Session["username"].ToString());
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        string strCompName = ds.Tables[0].Rows[0]["CompanyName"].ToString();
                        Session["CompanyName"] = strCompName;
                        ViewState["CompanyName"] = strCompName;

                      

                        ds = DatabaseHelper.getDataset("select distinct pd.project_id , p.project_name  from projectdetails pd inner join projects p on pd.Project_id = p.project_id Where pd.CompanyName='" + strCompName + "'");// DatabaseHelper.getCompanyName(Session["clientid"].ToString(), Session["username"].ToString());
                        if (ds != null && ds.Tables.Count >= 0 && ds.Tables[0].Rows.Count >= 0)
                        {

                            ddlProj.DataSource = ds.Tables[0];
                            ddlProj.DataTextField = "Project_name";
                            ddlProj.DataValueField = "Project_id";
                            ddlProj.DataBind();
                            ddlProj.Items.Insert(0, "--Select--");

                            if (ds.Tables[0].Rows.Count == 1)
                            {
                                ddlProj.SelectedIndex = 1;
                                btnexport.Enabled = true;
                                fnFillEmpddl();
                                fnGenerateLeaveReoprt();
                                fnGenerateTimeSheet();
                            }
                        }
                    }
                }
            }
            else
            {
                Response.Redirect("login.aspx");
            }
        }
        catch
        {
        }

    }

    void fnFillEmpddl()
    {
        try
        {

            if (Session["ProjectName"] != null && Session["CompanyName"] != null)
            {
                ddlEmp.Items.Clear();

                // if (Session["ProjectName"].ToString().Trim() != "--Select--")
                if (ddlProj.SelectedIndex != 0)
                {
                    string sql = "select u.firstname+' '+u.lastname as Name, u.user_id ,u.username, pd.companyname , pd.DOA from users u inner join projectdetails pd ";
                    sql += "on u.user_id= pd.user_id where u.IsCW='Y' and pd.CompanyName='" + Session["CompanyName"].ToString(); //"Select ( firstname+' '+lastname) as Name ,username from users where IsCW='Y' and CompanyName='" + Session["CompanyName"].ToString() + "' order by username";
                    sql += "' and pd.Project_id =" + ddlProj.SelectedValue.ToString();

                    DataSet dsAdmin = fnSetProjectDetails();


                    if (dsAdmin != null && dsAdmin.Tables.Count > 0 && dsAdmin.Tables[0].Rows.Count > 0)
                    {
                        ddlEmp.DataSource = dsAdmin;
                        ddlEmp.DataValueField = "user_id";
                        ddlEmp.DataTextField = "Name";
                        ddlEmp.DataBind();
                        //strArrayUser_id = dsAdmin.Tables[0].AsEnumerable().Select(a => a["user_id"].ToString()).ToArray();
                        //strArrayUserName = dsAdmin.Tables[0].AsEnumerable().Select(a => a["username"].ToString()).ToArray();
                        //strArrayDOA = dsAdmin.Tables[0].AsEnumerable().Select(a => a["DOA"].ToString()).ToArray();
                        ddlEmp.Items.Insert(0, "ALL");

                    }
                    else
                    {
                        ddlEmp.Items.Insert(0, "--Empty--");
                    }
                }
                else
                {
                    ddlEmp.Items.Insert(0, "--Empty--");
                }

            }
            else
            {
                Response.Redirect("login.aspx");
            }
        }
        catch
        {
            ddlEmp.Items.Insert(0, "--Empty--");
        }

    }


    public DataSet fnSetProjectDetails()
    {
        DataSet ds = null;
        if (Session["ProjectName"] != null && Session["CompanyName"] != null)
        {
            string sql = "select u.firstname+' '+u.lastname as Name, u.user_id ,u.username, pd.companyname , pd.DOA from users u inner join projectdetails pd ";
            sql += "on u.user_id= pd.user_id where u.IsCW='Y' and pd.CompanyName='" + Session["CompanyName"].ToString(); //"Select ( firstname+' '+lastname) as Name ,username from users where IsCW='Y' and CompanyName='" + Session["CompanyName"].ToString() + "' order by username";
            sql += "' and pd.Project_id =" + ddlProj.SelectedValue.ToString();

            ds = DatabaseHelper.getDataset(sql);

            strArrayUser_id = ds.Tables[0].AsEnumerable().Select(a => a["user_id"].ToString()).ToArray();
            strArrayUserName = ds.Tables[0].AsEnumerable().Select(a => a["username"].ToString()).ToArray();
            strArrayDOA = ds.Tables[0].AsEnumerable().Select(a => a["DOA"].ToString()).ToArray();
        }
        else
        {
            Response.Redirect("login.aspx");
        }
        return ds;
    }


    protected void ddlProj_SelectedIndexChanged(object sender, EventArgs e)
    {
      //  CustomValidator1.Validate();
        Session["ProjectName"] = ddlProj.SelectedValue.ToString();
        ddlEmp.Enabled = true;
        if (ddlProj.SelectedItem.Text != "--Select--")
        {
            btnexport.Enabled = true;
            fnFillEmpddl();
            fnGenerateLeaveReoprt();
            fnGenerateTimeSheet();
            //if (dtTemp != null)
            //{
            //    if (dtTemp.Rows.Count > 0)
            //    {
            //        lbl_pageinfo.Visible = false;
            //        lbl_pageinfo.Text = "--Employee leave report--";
            //        rptAbsentEmpByWeek.Visible = true;
            //        rptAbsentEmpByWeek.DataSource = (from x in dvWeeklyReport.ToTable().AsEnumerable() select x["AbsentDate"]).Distinct();
            //        rptAbsentEmpByWeek.DataBind();
            //    }
            //    else
            //    {
            //        //  HtmlDesign.InnerHtml = "";
            //        rptAbsentEmpByWeek.Visible = false;
            //        lbl_pageinfo.Visible = true;
            //        lbl_pageinfo.Text = "No one was absent between selected time interval.";
            //    }

            //}
            //else
            //{
            //    //btnexport.Enabled = false;
            //    // btnshow.Enabled = false;
            //    // HtmlDesign.InnerHtml = "";
            //    rptAbsentEmpByWeek.Visible = false;
            //    lbl_pageinfo.Visible = true;
            //    lbl_pageinfo.Text = "No one was absent between selected interval.";
            //}
        }
        else
        {
            fnFillEmpddl();
            btnexport.Enabled = false;
            HtmlDesign.InnerHtml = "";
            rptAbsentEmpByWeek.Visible = false;
            lbl_pageinfo.Visible = true;
            lbl_pageinfo.Text = "No one was absent between selected interval";
        }
    }

    protected void ddlEmp_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {

            // RequiredFieldValidator2.Validate();
            string strdate = "";
            fnGenerateTimeSheet();
            fnGenerateLeaveReoprt();
            if (dvWeeklyReport != null)
            {
                if (dvWeeklyReport.ToTable().Rows.Count > 0)
                {
                    rptAbsentEmpByWeek.Visible = true;
                    lbl_pageinfo.Visible = false;
                    rptAbsentEmpByWeek.DataSource = (from x in dvWeeklyReport.ToTable().AsEnumerable() select x["AbsentDate"]).Distinct();
                    rptAbsentEmpByWeek.DataBind();
                }
                else
                {
                    rptAbsentEmpByWeek.Visible = false;
                    lbl_pageinfo.Text = "No one was absent between selected interval.";
                    btnshow.Enabled = false;
                    //btnexport.Enabled = false;
                }
            }
            else
            {
                // lbl_pageinfo.Text = "No one was absent between selected interval.";
                // btnshow.Enabled = false;
                // btnexport.Enabled = false;
            }

            if (GMDStartDate.Text.ToString() == "")
            {
                strdate = @"SELECT 
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0),103)   as Date_of_Monday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+1,103) as Date_of_Tuesday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+2,103) as Date_of_Wednesday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+3,103) as Date_of_Thusday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+4,103) as Date_of_Friday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+5,103) as Date_of_Saturday";
            }
            else
            {
                strdate = @"SELECT  CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0),103) as Date_of_Monday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+1,103) as Date_of_Tuesday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+2,103) as Date_of_Wednesday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+3,103) as Date_of_Thusday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+4,103) as Date_of_Friday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+5,103) as Date_of_Saturday";

            }
            DataSet ds = DatabaseHelper.getDataset(strdate);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                dynamically(ds);
                if (ddlEmp.SelectedItem.Text != "ESG") //(ddlEmp.SelectedItem.Text == "PHG")
                {
                    strreportname = ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "_To_" + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString();
                }
                else
                {
                    strreportname = ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "_To_" + ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString();
                }
            }
        }
        catch
        {
        }
    }

    public void fnSetDefaultDate()
    {

        // lblError.Visible = false;
        String DayName = DateTime.Now.DayOfWeek.ToString();

        if (DayName == "Monday")
        {
            DayNo = 1;
        }
        else if (DayName == "Tuesday")
        {
            DayNo = 2;
        }

        else if (DayName == "Wednesday")
        {
            DayNo = 3;
        }

        else if (DayName == "Thursday")
        {
            DayNo = 4;
        }
        else if (DayName == "Friday")
        {
            DayNo = 5;
        }


        dtStartingDate = DateTime.Today.AddDays(-DayNo + 1);    // Monday
        dtEndingDate = DateTime.Today.AddDays(6 - DayNo);      //Friday
    }

    void fnGenerateLeaveReoprt()
    {
        try
        {
            if (blnIsDateInterval == false) // Means 
            {
                if (Session["TI"] == null)
                {
                    dtStartingDate = DateTime.Today.AddDays(DayNo); // DayNo value has been set by dropdown. 
                    dtEndingDate = DateTime.Today;
                }

                else
                {
                    if (Session["TI"] == "TW")
                    {
                        fnSetDefaultDate(); //Set dates Inteval for Week values for dtStartingDate,dtEndingDate.
                    }
                    else if (Session["TI"] == "TM")
                    {
                        DateTime today = DateTime.Today;
                        DateTime FirstDate = new DateTime(today.Year, today.Month, 1);
                        today = new DateTime(today.Year, today.Month + 1, 1);
                        DateTime LastDate = today.AddDays(-1);
                        dtStartingDate = FirstDate;
                        dtEndingDate = LastDate;
                    }
                }


                dtSDate = dtStartingDate.ToString("yyyy/MM/dd");
                dtEDate = dtEndingDate.ToString("yyyy/MM/dd");

                GMDStartDate.Text = dtSDate.ToString();
                GMDEndDate.Text = dtEDate.ToString();
            }
            else
            {
                fnFillDates();
            }

            lblLRHeading.Text = "Employee leave report, Mumbai (from " + dtStartingDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) + " to " + dtEndingDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) + ")";

            string DOA = "", strUser_id = "";

            DataSet dsTemp = objLeave.getWeeklyReport(dtSDate, dtEDate, DOA, ViewState["CompanyName"].ToString(), strUser_id, strArrayUser_id, ddlProj.SelectedValue.ToString());


            if (dsTemp != null && dsTemp.Tables.Count > 0 && dsTemp.Tables[0].Rows.Count > 0)
            {
                rptAbsentEmpByWeek.Visible = true;
                DataTable _datatable = null;
                fnSetProjectDetails(); // FILL Array of user_id, user_name , DOA

                DataSet _ds = dsTemp.Clone();
                int i = 0;

                //Area filter the employee's leave report by DOA means emloyee leave report should not display leaves taken before date of joing for selected project

                foreach (string username in strArrayUserName)
                {
                    DateTime dt = Convert.ToDateTime(strArrayDOA[i]);
                    string exp = "AbsentDate>=#" + dt.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture) + "# and Username='" + username + "'";

                    DataRow[] copyRows = dsTemp.Tables[0].Select(exp);

                    _datatable = _ds.Tables[0];

                    foreach (DataRow copyRow in copyRows)
                    {

                        _datatable.ImportRow(copyRow);

                    }

                    _datatable.AcceptChanges();

                    i++;
                }

                // End

                dtTemp = _datatable; //dsTemp.Tables[0];

                List<string> strDateArray = new List<string>();

                if (dtTemp != null && dtTemp.Rows.Count > 0)
                {

                    rptAbsentEmpByWeek.Visible = true;
                    btnshow.Enabled = true;
                    btnexport.Enabled = true;

                    dtTemp.Columns.Add("Planned", typeof(string));
                    dtTemp.Columns.Add("Unplanned", typeof(string));
                    dtTemp1 = dtTemp.Clone();


                    foreach (DataRow dr in dtTemp.Rows)
                    {
                        DateTime ldt = Convert.ToDateTime(dr["AbsentDate"].ToString());
                        string strReason = dr["Reason"].ToString();
                        dr["AbsentDate"] = ldt.ToString("dd-MM-yyyy");//ldt.ToString("yyyy/MM/dd hh:mm:ss.fff");
                        dr["Reason"] = strReason.Replace("@", "'").Trim();
                        if (dr["IsPlanned"].ToString().Trim() == "True")
                        {
                            dr["Planned"] = "Yes";
                            dr["Unplanned"] = "No";
                            dr["Reason"] = "";
                        }
                        else
                        {
                            dr["Planned"] = "No";
                            dr["Unplanned"] = "Yes";
                        }
                        dtTemp1.ImportRow(dr);
                    }


                    dvWeeklyReport = new DataView(dtTemp1);

                    if (ddlEmp.SelectedItem.Text != "ALL")
                    {
                        strUser_id = ddlEmp.SelectedValue.ToString();
                        string strEmpName = ddlEmp.SelectedItem.Text.Trim();
                        string[] strTemp = strEmpName.Split(new char[] { ' ' });
                        strEmpName = strTemp[0].Trim();
                        dvWeeklyReport.RowFilter = "username='" + strEmpName + "'";
                    }

                    if (dvWeeklyReport != null && dvWeeklyReport.ToTable().Rows.Count > 0)
                    {
                        rptAbsentEmpByWeek.Visible = true;

                        lbl_pageinfo.Visible = false;

                        fnBindRepeater();
                        HtmlDesign.Visible = true;
                        btnshow.Enabled = true;
                        btnexport.Enabled = true;
                    }
                    else
                    {
                        rptAbsentEmpByWeek.Visible = false;

                        lbl_pageinfo.Visible = true;

                        if (ddlEmp.SelectedItem.Text != "ALL")
                        {
                            lbl_pageinfo.Text = ddlEmp.SelectedItem.Text + " was not absent between selected interval.";
                        }
                        else
                        {
                            lbl_pageinfo.Text = "No one was absent between selected interval.";
                        }
                    }

                }

                else
                {

                    if (ddlEmp.SelectedItem.Text != "ALL")
                    {
                        lbl_pageinfo.Text = ddlEmp.SelectedItem.Text + " was not absent between selected interval.";
                    }
                    else
                    {
                        lbl_pageinfo.Text = "No one was absent between selected interval.";
                    }

                }


            }
            else
            {
                rptAbsentEmpByWeek.Visible = false;
                if (ddlEmp.SelectedItem.Text != "ALL")
                {
                    lbl_pageinfo.Text = ddlEmp.SelectedItem.Text + " was not absent between selected interval.";
                }
                else
                {
                    lbl_pageinfo.Text = "No one was absent between selected interval.";
                }

            }

        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, "Exception in LMS frmAdminHome.aspx.cs", "Method : Binddata()");
        }
    }

    void fnGenerateTimeSheet()
    {
        string strQuery = "";

        strQuery = @"SELECT  CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0),103) as Date_of_Monday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+1,103) as Date_of_Tuesday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+2,103) as Date_of_Wednesday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+3,103) as Date_of_Thusday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+4,103) as Date_of_Friday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+5,103) as Date_of_Saturday";

        DataSet ds = DatabaseHelper.getDataset(strQuery);
        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            clear();
            strreportname = ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "_To_" + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString();
            dynamically(ds);
        }
    }

    protected void rptAbsentEmpByWeek_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item |
                     e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater rptr = (Repeater)e.Item.FindControl("Repeater2");
                rptr.DataSource = dvWeeklyReport.ToTable().AsEnumerable().Where(x => x["AbsentDate"].Equals(e.Item.DataItem));
                rptr.DataBind();
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, "Exception in LMS frmAdminHome.aspx.cs", "Event : rptAbsentEmpByWeek_ItemDataBound()");
        }
    }

    static DateTime LastDayOfYear()
    {
        return LastDayOfYear(DateTime.Today);
    }

    static DateTime LastDayOfYear(DateTime d)
    {
        // 1
        // Get first of next year
        DateTime n = new DateTime(d.Year + 1, 1, 1);
        // 2
        // Subtract 1 from it
        return n.AddDays(-1);
    }

    static DateTime FirstDayOfYear()
    {
        return FirstDayOfYear(DateTime.Today);
    }

    static DateTime FirstDayOfYear(DateTime y)
    {
        return new DateTime(y.Year, 1, 1);
    }

    protected void GMDEndDate_TextChanged(object sender, EventArgs e)
    {
        blnIsDateInterval = true;
        fnGenerateTimeSheet();
        fnGenerateLeaveReoprt();
    }

    public void fnFillDates()
    {
        if (GMDStartDate.Text.Trim() == "")
        {
            dtSDate = DateTime.Today.AddDays(-DayNo + 1).ToString("yyyy-MM-dd");  //FirstDayOfYear(dt).ToString("dd/MM/yyyy");
            GMDStartDate.Text = dtSDate;
            dtStartingDate = DateTime.Today.AddDays(-DayNo + 1);
        }
        else
        {
            dtStartingDate = Convert.ToDateTime(GMDStartDate.Text);
            dtSDate = dtStartingDate.ToString("yyyy-MM-dd");
        }

        if (GMDEndDate.Text.Trim() == "")
        {
            dtEDate = DateTime.Today.AddDays(6 - DayNo).ToString("yyyy-MM-dd"); //LastDayOfYear(dt).ToString("dd/MM/yyyy");
            GMDEndDate.Text = dtEDate;
            dtEndingDate = DateTime.Today.AddDays(6 - DayNo);

        }
        else
        {
            dtEndingDate = Convert.ToDateTime(GMDEndDate.Text);
            dtEDate = dtEndingDate.ToString("yyyy-MM-dd");
        }
    }
    //New Dynamically
    public void dynamically(DataSet ds)
    {
        try
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            string strQuery = "";

            if (ddlProj.SelectedItem != null)
            {
                HtmlDesign.Visible = true;

                //if (ddlProj.SelectedItem.Text.Trim() != "--Select--")
                if (ddlProj.SelectedIndex != 0)
                {
                    strQuery = @"select u.* from users u inner join ProjectDetails pd on u.user_id = pd.user_id where IsCW='Y'";
                    strQuery += " and pd.Project_id= " + ddlProj.SelectedValue.ToString() + " and pd.CompanyName='" + Session["CompanyName"].ToString() + "'";

                    if (ddlEmp.SelectedItem.Text != "ALL")
                    {
                        strQuery += " and u.user_id='" + ddlEmp.SelectedValue.ToString() + "' order by username ";
                    }

                    DataSet dsAllEmp = DatabaseHelper.getDataset(strQuery);
                    if (dsAllEmp != null && dsAllEmp.Tables.Count > 0 && dsAllEmp.Tables[0].Rows.Count > 0)
                    {
                        sb.Append("<table width='100%' border='0' cellpadding='0' cellspacing='0'>");
                        sb.Append("<tr>");
                        sb.Append("<td height='10px' colspan='8' align='left' style='font-size:medium'>");
                        sb.Append("<b>Resource planning reports, Mumbai (from ");
                        // sb.Append(ds.Tables[0].Rows[0]["Date_of_Monday"].ToString());
                        sb.Append(ds.Tables[0].Rows[0]["Date_of_Monday"].ToString());
                        sb.Append(" to ");
                        if (ddlEmp.SelectedItem.Text == "PHG")
                        {
                            sb.Append(ds.Tables[0].Rows[0]["Date_of_Friday"].ToString() + ")");
                        }
                        else
                        {
                            sb.Append(ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString() + ")");
                        }
                        sb.Append("</b></td>");
                        sb.Append("</tr>");
                        //sb.Append("<tr>");
                        //sb.Append("<td height='10px'>");
                        //sb.Append("</td>");
                        //sb.Append("</tr>");
                        sb.Append("</table>");
                        sb.Append("<br><table width='100%' border='0' cellpadding='0' cellspacing='0'>");


                        sb.Append("<tr style='background:#e1e1e1;'>");
                        sb.Append("<td>");

                        sb.Append("<table border='0' width='100%' cellpadding='0' cellspacing='0'  class='leavetbl'><tr><td height='50px' rowspan='2'><b>Employee name</b></td></tr></table>");
                        sb.Append("</td>");


                        sb.Append("<td>");

                        sb.Append("<table border='0' width='100%' cellpadding='0' cellspacing='0'  class='leavetbl'><tr><td height='50px' rowspan='2' align='center'><b>Skills</b></td></tr></table>");
                        sb.Append("</td>");
                        sb.Append("<td>");


                        sb.Append("<table width='100%' border='0' cellpadding='0' cellspacing='0'   class='leavetbl'>");
                        sb.Append("<tr>");



                        if (ddlEmp.SelectedItem.Text == "PHG")
                        {
                            sb.Append("<td width='20%'>");
                        }
                        else
                        {
                            sb.Append("<td width='17%' height='50px'>");
                        }

                        sb.Append("<b>Mon " + ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "</b>");
                        sb.Append("<br><b>In|Out(GMT)|Total</b>");
                        //sb.Append("<table border='0' cellpadding='0' cellspacing='0' align='center'><tr><td align='center'>In/Out (GMT)</td><td> Total </td></tr></table>");
                        sb.Append("</td>");


                        if (ddlEmp.SelectedItem.Text == "PHG")
                        {
                            sb.Append("<td width='20%'>");
                        }
                        else
                        {
                            sb.Append("<td width='17%'>");
                        }

                        sb.Append("<b>Tue " + ds.Tables[0].Rows[0]["Date_of_Tuesday"].ToString() + "</b>");
                        sb.Append("<br><b>In|Out(GMT)|Total</b>");
                        //sb.Append("<table border='0' cellpadding='0' cellspacing='0' align='center'><tr><td align='center'>In/Out (GMT)</td><td> Total </td></tr></table>");
                        sb.Append("</td>");


                        if (ddlEmp.SelectedItem.Text == "PHG")
                        {
                            sb.Append("<td width='20%'>");
                        }
                        else
                        {
                            sb.Append("<td width='17%'>");
                        }


                        sb.Append("<b>Wed " + ds.Tables[0].Rows[0]["Date_of_Wednesday"].ToString() + " </b>");
                        sb.Append("<br><b>In|Out(GMT)|Total</b>");
                        //sb.Append("<table border='0' cellpadding='0' cellspacing='0' align='center'><tr><td align='center'>In/Out (GMT)</td><td> Total </td></tr></table>");
                        sb.Append("</td>");


                        if (ddlEmp.SelectedItem.Text == "PHG")
                        {
                            sb.Append("<td width='20%'>");
                        }
                        else
                        {
                            sb.Append("<td width='17%'>");
                        }


                        sb.Append("<b>Thur " + ds.Tables[0].Rows[0]["Date_of_Thusday"].ToString() + "</b>");
                        sb.Append("<br><b>In|Out(GMT)|Total</b>");
                        //sb.Append("<table border='0' cellpadding='0' cellspacing='0' align='center'><tr><td align='center'>In/Out (GMT)</td><td> Total </td></tr></table>");
                        sb.Append("</td>");

                        if (ddlEmp.SelectedItem.Text == "PHG")
                        {
                            sb.Append("<td width='20%'>");
                        }
                        else
                        {
                            sb.Append("<td width='17%'>");
                        }

                        sb.Append("<b>Fri " + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString() + "</b>");
                        sb.Append("<br><b>In|Out(GMT)|Total</b>");
                        //sb.Append("<table border='0' cellpadding='0' cellspacing='0' align='center'><tr><td align='center'>In/Out (GMT)</td><td> Total </td></tr></table>");
                        sb.Append("</td>");


                        if (ddlEmp.SelectedItem.Text != "PHG")
                        {
                            sb.Append("<td width='20%'>");
                            sb.Append("<b>Sat " + ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString() + "</b>");
                            sb.Append("<br><b>In|Out(GMT)|Total</b>");
                            //sb.Append("<table border='0' cellpadding='0' cellspacing='0' align='center'><tr><td align='center'>In/Out (GMT)</td><td> Total </td></tr></table>");
                            sb.Append("</td>");
                        }


                        sb.Append("</tr>");
                        sb.Append("</table>");
                        sb.Append("</td>");

                        sb.Append("</tr>");
                        for (int iEmp = 0; iEmp < dsAllEmp.Tables[0].Rows.Count; iEmp++)
                        {
                            if (iEmp % 2 == 0)
                            {
                                sb.Append("<tr style='background:#f7f7f7; height:30px;'>");
                            }
                            else
                            {
                                sb.Append("<tr style='background:#ffffff; height:30px;'>");
                            }
                            sb.Append("<td align='left' style='width:100.75pt;' >");
                            sb.Append("<table border='0' width='100%' cellpadding='0' cellspacing='0' class='leavetbl'><tr  style='height:30px'><td>" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "</td></tr></table>");
                            if (dsAllEmp.Tables[0].Rows[iEmp]["skills"].ToString() != "")
                            {
                                sb.Append("</td><td align='left'  ><table border='0' width='100%' height='100%' cellpadding='0' cellspacing='0' class='leavetbl'><tr style='height:30px'><td >" + dsAllEmp.Tables[0].Rows[iEmp]["skills"].ToString() + "</td></tr></table></td>");
                            }
                            else
                            {
                                sb.Append("</td><td align='left' ><table border='0' width='100%' height='100%' cellpadding='0' cellspacing='0' class='leavetbl'><tr  style='height:30px'><td  align='center'> -/ </td></tr></table></td>");
                            }
                            //******************************************* Create In Time ******************************************************************
                            sb.Append("<td>");

                            sb.Append("<table width='100%' border='0' cellpadding='0' cellspacing='0'>");
                            sb.Append("<tr  style='height:30px'>");


                            if (ddlEmp.SelectedItem.Text == "PHG")
                            {
                                sb.Append("<td width='20%'>");
                            }
                            else
                            {
                                sb.Append("<td width='17%'>");
                            }

                            sb.Append("<table width='100%' border='0' height='100%' cellpadding='0' cellspacing='0' class='leavetbl'>");
                            sb.Append("<tr  style='height:30px'>");
                            sb.Append("<td colspan='2'>");

                            string strInout1 = "";
                            object objIntimeALL1 = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "'");
                            if (objIntimeALL1 != null && objIntimeALL1.ToString() != "")
                            {
                                DateTime dtALL1 = Convert.ToDateTime(objIntimeALL1).AddHours(-5).AddMinutes(-30);
                                strInout1 = "<span style='color:green'>"+dtALL1.ToString("HH:mm").ToString() +"</span>" +"|";
                            }
                            else
                            {
                                strInout1 = "-/";
                            }


                            object objOuttimeALL1 = DatabaseHelper.executeScalar("select Outtime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "'");
                            if (objOuttimeALL1 != null && objOuttimeALL1.ToString() != "")
                            {
                                DateTime dtoutALL1 = Convert.ToDateTime(objOuttimeALL1).AddHours(-5).AddMinutes(-30);
                                strInout1 +="<span style='color:#0070c0'>"+ dtoutALL1.ToString("HH:mm")+"</span>";
                            }
                            else
                            {
                                strInout1 += "-";
                            }

                            sb.Append(strInout1);


                            object objTotalHours1 = DatabaseHelper.executeScalar("select TotalHrs from tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "'  and EodcDate='" + ds.Tables[0].Rows[0]["Date_of_Monday"].ToString().Trim() + "'");
                            if (objTotalHours1 != null && objTotalHours1.ToString() != "")
                            {
                                string totime = "";
                                string[] strbreak = objTotalHours1.ToString().Split(':');
                                totime = strbreak[0] + ":" + strbreak[1];
                                sb.Append("|" +"<span style='color:red'>"+ totime+"</span>");


                            }
                            else
                            {
                                sb.Append("-");

                            }

                            sb.Append("</td>");

                            string firstuserdate1 = "";
                            if (GMDStartDate.Text != "")
                            {
                                firstuserdate1 = @"Select CONVERT(VARCHAR(10),AbsentDate,103) as LeaveDate from tbl_LeaveDetails where UserName='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and  (AbsentDate >=DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text + "'),0) and AbsentDate <= DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text + "'),0)+4)";
                            }
                            else
                            {
                                firstuserdate1 = @"Select CONVERT(VARCHAR(10),AbsentDate,103)  as LeaveDate from tbl_LeaveDetails where UserName='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and (AbsentDate >=DATEADD(week, DATEDIFF(week, 0,GETDATE()),0) and AbsentDate <= DATEADD(week, DATEDIFF(week, 0,GETDATE()),0)+4)";
                            }
                            DataSet dsFirstdate = DatabaseHelper.getDataset(firstuserdate1);
                            sb.Append("</tr>");
                            sb.Append("</table>");
                            sb.Append("</td>");
                            //----------------------------------------------------------------
                            //----------------------------------------------------------------
                            if (ddlEmp.SelectedItem.Text == "PHG")
                            {
                                sb.Append("<td width='20%'>");
                            }
                            else
                            {
                                sb.Append("<td width='17%'>");
                            }
                            sb.Append("<table width='100%' border='0' height='100%' cellpadding='0' cellspacing='0' class='leavetbl'>");
                            sb.Append("<tr  style='height:30px'>");
                            sb.Append("<td colspan='2'>");

                            string strInout2 = "";
                            object objIntimeALL2 = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Tuesday"].ToString() + "'");
                            if (objIntimeALL2 != null && objIntimeALL2.ToString() != "")
                            {
                                DateTime dtALL2 = Convert.ToDateTime(objIntimeALL2).AddHours(-5).AddMinutes(-30);

                                strInout2 = "<span style='color:green'>"+dtALL2.ToString("HH:mm").ToString() +"</span>" +"|";
                            }
                            else
                            {

                                strInout2 = "-/";
                            }

                            object objOuttimeALL2 = DatabaseHelper.executeScalar("select Outtime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Tuesday"].ToString() + "'");
                            if (objOuttimeALL2 != null && objOuttimeALL2.ToString() != "")
                            {
                                DateTime dtoutALL2 = Convert.ToDateTime(objOuttimeALL2).AddHours(-5).AddMinutes(-30);

                                strInout2 += "<span style='color:#0070c0'>"+dtoutALL2.ToString("HH:mm")+"</span>";
                            }
                            else
                            {

                                strInout2 += "-";
                            }

                            sb.Append(strInout2);


                            object objTotalHours2 = DatabaseHelper.executeScalar("select TotalHrs from tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and EodcDate='" + ds.Tables[0].Rows[0]["Date_of_Tuesday"].ToString().Trim() + "'");
                            if (objTotalHours2 != null && objTotalHours2.ToString() != "")
                            {
                                string totime2 = "";
                                string[] strbreak2 = objTotalHours2.ToString().Split(':');
                                totime2 = strbreak2[0] + ":" + strbreak2[1];
                                sb.Append("|" + "<span style='color:red'>"+totime2+"</span>");

                            }
                            else
                            {

                                sb.Append("-");

                            }
                            sb.Append("</td>");
                            sb.Append("</tr>");
                            sb.Append("</table>");
                            sb.Append("</td>");

                            //---------------------------------------------------------------
                            //----------------------------------------------------------------
                            string strInout3 = "";

                            if (ddlEmp.SelectedItem.Text == "PHG")
                            {
                                sb.Append("<td width='20%'>");
                            }
                            else
                            {
                                sb.Append("<td width='17%'>");
                            }


                            sb.Append("<table width='100%' border='0' height='100%' cellpadding='0' cellspacing='0' class='leavetbl'>");
                            sb.Append("<tr  style='height:30px'>");
                            sb.Append("<td colspan='2'>");
                            object objIntimeALL3 = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Wednesday"].ToString() + "'");
                            if (objIntimeALL3 != null && objIntimeALL3.ToString() != "")
                            {
                                DateTime dtALL3 = Convert.ToDateTime(objIntimeALL3).AddHours(-5).AddMinutes(-30);

                                strInout3 = "<span style='color:green'>"+dtALL3.ToString("HH:mm")+"</span>" + "|";

                            }
                            else
                            {

                                strInout3 = "-/";
                            }
                            object objOuttimeALL3 = DatabaseHelper.executeScalar("select Outtime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Wednesday"].ToString() + "'");
                            if (objOuttimeALL3 != null && objOuttimeALL3.ToString() != "")
                            {
                                DateTime dtoutALL3 = Convert.ToDateTime(objOuttimeALL3).AddHours(-5).AddMinutes(-30);

                                strInout3 += "<span style='color:#0070c0'>"+dtoutALL3.ToString("HH:mm")+"</span>";
                            }
                            else
                            {

                                strInout3 += "-";
                            }
                            sb.Append(strInout3);

                            object objTotalHours3 = DatabaseHelper.executeScalar("select TotalHrs from tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and EodcDate='" + ds.Tables[0].Rows[0]["Date_of_Wednesday"].ToString().Trim() + "'");
                            if (objTotalHours3 != null && objTotalHours3.ToString() != "")
                            {
                                string totime3 = "";
                                string[] strbreak3 = objTotalHours3.ToString().Split(':');
                                totime3 = strbreak3[0] + ":" + strbreak3[1];
                                sb.Append("|" + "<span style='color:red'>"+totime3+"</span>");
                                //sb.Append("-" + objTotalHours3.ToString());
                            }
                            else
                            {
                                sb.Append("-");
                            }

                            sb.Append("</td>");
                            sb.Append("</tr>");
                            sb.Append("</table>");
                            sb.Append("</td>");

                            //---------------------------------------------------------------
                            //----------------------------------------------------------------
                            string strInout4 = "";


                            if (ddlEmp.SelectedItem.Text == "PHG")
                            {
                                sb.Append("<td width='20%'>");
                            }
                            else
                            {
                                sb.Append("<td width='17%'>");
                            }

                            sb.Append("<table width='100%' border='0' height='100%' cellpadding='0' cellspacing='0' class='leavetbl'>");
                            sb.Append("<tr  style='height:30px'>");
                            sb.Append("<td colspan='2'>");

                            object objIntimeALL4 = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Thusday"].ToString() + "'");
                            if (objIntimeALL4 != null && objIntimeALL4.ToString() != "")
                            {
                                DateTime dtALL4 = Convert.ToDateTime(objIntimeALL4).AddHours(-5).AddMinutes(-30);

                                strInout4 = "<span style='color:green'>"+dtALL4.ToString("HH:mm")+"</span>" + "|";
                            }
                            else
                            {

                                strInout4 = "-/";
                            }

                            object objOuttimeALL4 = DatabaseHelper.executeScalar("select Outtime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Thusday"].ToString() + "'");
                            if (objOuttimeALL4 != null && objOuttimeALL4.ToString() != "")
                            {
                                DateTime dtoutALL4 = Convert.ToDateTime(objOuttimeALL4).AddHours(-5).AddMinutes(-30);

                                strInout4 += "<span style='color:#0070c0'>"+dtoutALL4.ToString("HH:mm")+"</span>";
                            }
                            else
                            {

                                strInout4 += "-";
                            }

                            sb.Append(strInout4);
                            object objTotalHours4 = DatabaseHelper.executeScalar("select TotalHrs from tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and EodcDate='" + ds.Tables[0].Rows[0]["Date_of_Thusday"].ToString().Trim() + "'");
                            if (objTotalHours4 != null && objTotalHours4.ToString() != "")
                            {
                                string totime4 = "";
                                string[] strbreak4 = objTotalHours4.ToString().Split(':');
                                totime4 = strbreak4[0] + ":" + strbreak4[1];
                                sb.Append("|" + "<span style='color:red'>"+totime4+"</span>");

                            }
                            else
                            {
                                sb.Append("-");
                            }

                            sb.Append("</td>");
                            sb.Append("</tr>");
                            sb.Append("</table>");
                            sb.Append("</td>");
                            //----------------------------------------------------------------
                            //----------------------------------------------------------------
                            string strInout5 = "";


                            if (ddlEmp.SelectedItem.Text == "PHG")
                            {
                                sb.Append("<td width='20%'>");
                            }
                            else
                            {
                                sb.Append("<td width='17%'>");
                            }

                            sb.Append("<table width='100%' border='0' height='100%' cellpadding='0' cellspacing='0' class='leavetbl'>");
                            sb.Append("<tr  style='height:30px'>");
                            sb.Append("<td colspan='2'>");

                            object objIntimeALL5 = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString() + "'");
                            if (objIntimeALL5 != null && objIntimeALL5.ToString() != "")
                            {
                                DateTime dtALL5 = Convert.ToDateTime(objIntimeALL5).AddHours(-5).AddMinutes(-30);

                                strInout5 = "<span style='color:green'>"+dtALL5.ToString("HH:mm")+"</span>" + "|";
                            }
                            else
                            {

                                strInout5 = "-/";
                            }
                            object objOuttimeALL5 = DatabaseHelper.executeScalar("select Outtime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString() + "'");
                            if (objOuttimeALL5 != null && objOuttimeALL5.ToString() != "")
                            {
                                DateTime dtoutALL5 = Convert.ToDateTime(objOuttimeALL5).AddHours(-5).AddMinutes(-30);

                                strInout5 += "<span style='color:#0070c0'>"+dtoutALL5.ToString("HH:mm")+"</span>";
                            }
                            else
                            {

                                strInout5 += "-";
                            }
                            sb.Append(strInout5);

                            object objTotalHours5 = DatabaseHelper.executeScalar("select TotalHrs from tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and EodcDate='" + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString().Trim() + "'");
                            if (objTotalHours5 != null && objTotalHours5.ToString() != "")
                            {
                                string totime5 = "";
                                string[] strbreak5 = objTotalHours5.ToString().Split(':');
                                totime5 = strbreak5[0] + ":" + strbreak5[1];
                                sb.Append("|" + "<span style='color:red'>"+totime5+"</span>");

                            }

                            sb.Append("</td>");

                            sb.Append("</tr>");
                            sb.Append("</table>");
                            sb.Append("</td>");
                            //----------------------------------------------------------------
                            //----------------------------------------------------------------
                            if (ddlEmp.SelectedItem.Text != "PHG")
                            {
                                string strInout6 = "";
                                sb.Append("<td width='20%'>");
                                sb.Append("<table width='100%' border='0' height='100%' cellpadding='0' cellspacing='0' class='leavetbl'>");
                                sb.Append("<tr  style='height:30px'>");
                                sb.Append("<td colspan='2'>");


                                object objIntimeALL6 = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString() + "'");
                                if (objIntimeALL6 != null && objIntimeALL6.ToString() != "")
                                {
                                    DateTime dtALL6 = Convert.ToDateTime(objIntimeALL6).AddHours(-5).AddMinutes(-30);

                                    strInout6 = "<span style='color:green'>"+dtALL6.ToString("HH:mm")+"</span>" + "|";
                                }
                                else
                                {

                                    strInout6 = "-/";
                                }
                                object objOuttimeALL6 = DatabaseHelper.executeScalar("select Outtime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString() + "'");
                                if (objOuttimeALL6 != null && objOuttimeALL6.ToString() != "")
                                {
                                    DateTime dtoutALL6 = Convert.ToDateTime(objOuttimeALL6).AddHours(-5).AddMinutes(-30);

                                    strInout6 += "<span style='color:#0070c0'>"+dtoutALL6.ToString("HH:mm")+"</span>";
                                }
                                else
                                {

                                    strInout6 += "-";
                                }
                                sb.Append(strInout6);


                                object objTotalHours6 = DatabaseHelper.executeScalar("select TotalHrs from tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and EodcDate='" + ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString().Trim() + "'");
                                if (objTotalHours6 != null && objTotalHours6.ToString() != "")
                                {
                                    string totime6 = "";
                                    string[] strbreak6 = objTotalHours6.ToString().Split(':');
                                    totime6 = strbreak6[0] + ":" + strbreak6[1];
                                    sb.Append("|" + "<span style='color:red'>"+totime6+"</span>");
                                    //sb.Append("-" + objTotalHours6.ToString());
                                }

                                sb.Append("</td>");
                                sb.Append("</tr>");
                                sb.Append("</table>");
                                sb.Append("</td>");
                            }


                            sb.Append("</table>");
                            sb.Append("</td>");


                            //******************************************* End Create In Time ******************************************************************

                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");
                        HtmlDesign.InnerHtml = sb.ToString();
                    }
              }
           }
        }
        catch (Exception ex)
        {
        }
    }

    protected void ddlTimeInterval_SelectedIndexChanged(object sender, EventArgs e)
    {
        blnIsDateInterval = false;

        if (ddlTimeInterval.SelectedValue.ToString() == "1") //Today
        {
            DayNo = 0;
            Session["TI"] = null;
        }

        else if (ddlTimeInterval.SelectedValue.ToString() == "3")//Last 7 days 
        {
            DayNo = -7;
            Session["TI"] = null;
        }


        else if (ddlTimeInterval.SelectedValue.ToString() == "5")// Last 31 days
        {
            DayNo = -31;
            Session["TI"] = null;
        }

        else if (ddlTimeInterval.SelectedValue.ToString() == "4")// This month
        {
            Session["TI"] = "TM";
        }

        else if (ddlTimeInterval.SelectedValue.ToString() == "2") //This Week
        {
            Session["TI"] = "TW";
        }
        //  BindData();

        fnGenerateLeaveReoprt();
        fnGenerateTimeSheet();
    }

    public void fnBindRepeater()
    {
        if (dvWeeklyReport.ToTable() != null)
        {
            if (dvWeeklyReport.ToTable().Rows.Count > 0)
            {
                rptAbsentEmpByWeek.DataSource = (from x in dvWeeklyReport.ToTable().AsEnumerable() select x["AbsentDate"]).Distinct();
                rptAbsentEmpByWeek.DataBind();
            }
        }
    }
    //
}
