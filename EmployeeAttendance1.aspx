<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EmployeeAttendance1.aspx.cs"
    Inherits="Admin_EmployeeAttendance" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagName="WeeklyReport" TagPrefix="WR" Src="~/Admin/LMSAdmin/LMS-Weekly-report.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server" style="font-size: small;">
    <div>
      <table style="width: 100%; padding: 10px; height: 373" class="publicloginTable" onkeypress="javascript:keypressHandler(event)">
        <tr>
            <td valign="top">
                Starting date
            </td>
            <td align="center" valign="top">
                :
            </td>
            <td valign="top" style="width: 287px">
                <asp:TextBox ID="GMDStartDate" runat="server" CssClass="input_boxline"></asp:TextBox>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="GMDStartDate"
                    Format="yyyy-MM-dd" PopupButtonID="GMDStartDate">
                </ajaxToolkit:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please select a start date"
                    ControlToValidate="GMDStartDate" ValidationGroup="g1"></asp:RequiredFieldValidator>
                <br />
                <asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="Please enter a valid  date"
                    Operator="DataTypeCheck" ControlToValidate="GMDStartDate" Type="Date" 
                    ValidationGroup="g1"></asp:CompareValidator>
            </td>
            <td style="width: 5%; height: 10px" valign="top">
                Ending date
            </td>
            <td align="center" style="width: 2%" valign="top">
                :
            </td>
            <td valign="top" class="style1">
                <asp:TextBox ID="GMDEndDate" runat="server" CssClass="input_boxline" 
                    AutoPostBack="True" ontextchanged="GMDEndDate_TextChanged"></asp:TextBox>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="GMDEndDate"
                    Format="yyyy-MM-dd" PopupButtonID="GMDEndDate">
                </ajaxToolkit:CalendarExtender>
                <asp:CompareValidator ID="CompareValidator3" runat="server" ErrorMessage="Please enter a valid date"
                    Operator="DataTypeCheck" ControlToValidate="GMDEndDate" Type="Date" 
                    ValidationGroup="g1"></asp:CompareValidator>
                <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="End date should be greater or equal to start date."
                    ControlToCompare="GMDStartDate" ControlToValidate="GMDEndDate" Operator="GreaterThanEqual"
                    Type="Date" ValidationGroup="g1"></asp:CompareValidator>
                &nbsp;&nbsp;&nbsp;&nbsp;
            </td>
            <td>
            </td>
            <td valign="top">
                Department
            </td>
            <td valign="top">
                :
            </td>
            <td valign="top">
                <asp:DropDownList ID="Compddl" runat="server" Height="18px" Width="201px" 
                    onselectedindexchanged="Compddl_SelectedIndexChanged" AutoPostBack="True">
                </asp:DropDownList>
            </td>
        </tr>
        </table>
    </div>
    <div>
        <asp:DropDownList ID="druser" runat="server" AutoPostBack="true" OnSelectedIndexChanged="druser_SelectedIndexChanged" Visible ="true">
        </asp:DropDownList>
    </div>
    <div>
        <table>
            <tr>
                <td colspan="5" >
                    <asp:ScriptManager ID="ScriptManager1" EnablePageMethods="true" runat="server">
                    </asp:ScriptManager>
                    <asp:Button ID="btnshow" runat="server" Text="Show time sheet" OnClick="btnshow_Click" />
                    <asp:Button ID="btnexport" runat="server" Text="Export to Excel" OnClick="btnexport_Click" />
                </td>
            </tr>
        </table>
    </div>
    <div id="main" runat ="server">  
    <div id="HtmlDesign" runat="server">
    </div>
    <div id="DateWiseReport" runat="server">
    <table style="height: 300px; color: #333333; font-size: 26px;" width="100%" runat="server" id="WeeklyReport">
                                      <%--  <tr align="center">
                                            
                                            <td>   Weekly Report</td>
                                            
                                        </tr>--%>
                                        <tr>
                                            <td align="center" >
                                            
                                                <asp:Label ID="lbl_pageinfo" runat="server" Text="" Visible="false"></asp:Label>
                                                <asp:Repeater ID="rptAbsentEmpByWeek" runat="server" OnItemDataBound="rptAbsentEmpByWeek_ItemDataBound">
                                                    <ItemTemplate>
                                                        <table style="font-size: 15px" width="100%" border="1">
                                                            <tr>
                                                                <td colspan="2" style="font-size: 20px">
                                                                    <b>
                                                                        <%# String.Format("{0:D}",(Container.DataItem)) %>
                                                                        (<%# Convert.ToDateTime(Container.DataItem).DayOfWeek %>)</b>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <asp:Repeater ID="Repeater2" runat="server">
                                                                        <FooterTemplate>
                                                                            <%="</ul>" %>
                                                                        </FooterTemplate>
                                                                        <HeaderTemplate>
                                                                            <table style="font-size: 12px" width="100%" border="1">
                                                                            <tr style="font-weight:bold; font-size:14px">
                                                                            <td  width="8%">Employee name</td> <td  width="8%">Planned</td> <td  width="8%">Unplanned</td> <td  width="73%">Reason</td>
                                                                            </tr>
                                                                            </table>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                    
                                                                         <table style="font-size: 12px" width="100%" border="1">
                                                                                <tr>
                                                                                    <td width="8%">
                                                                                        <%#((System.Data.DataRow)Container.DataItem)[0] %>
                                                                                    </td>

                                                                                    <td width="8%">
                                                                                       <%#((System.Data.DataRow)Container.DataItem)[6] %>
                                                                                    </td>

                                                                                     <td width="8%">
                                                                                       <%#((System.Data.DataRow)Container.DataItem)[7] %>
                                                                                    </td>

                                                                                    <td width="73%">
                                                                                        <%# ((System.Data.DataRow)Container.DataItem)[3]%>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </td>
                                        </tr>
     </table>
    </div>
    </div>
    </form>
</body>
</html>
