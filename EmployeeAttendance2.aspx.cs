using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Linq;
using System.Collections.Generic;
public partial class Admin_EmployeeAttendance : System.Web.UI.Page
{
    static string strreportname = "";

   // =============Weekly Report=================//

    clsEmployee_Logic1 objEmployee = new clsEmployee_Logic1();
    clsLeave_Logic objLeave = new clsLeave_Logic();
    private string strError = "No records found.";
    static DataSet dsAbsentDates = new DataSet();
    static DataView dv = null;
    static string sortByAndArrangeBy = "";
    static double SdblUsed = 0.0;
    string dtSDate;
    string dtEDate;
    DateTime datetime;
    DataSet dsTemp;
    DataTable dtTemp, dtTemp1;
    DateTime dtStartingDate;  // Monday
    DateTime dtEndingDate;
    static int DayNo = 0;
    DataView dvWeeklyReport;
    Boolean blnIsAnyExist = false;
    //=============================================//


    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            Session["CompanyName"] = "--Select--";
            //===========Weekly Report ===============//
            fnSetDefaultDate();
            fnFillDates();
           // fnGenatateLeaveReoprt();
           // fnGenerateTimeSheet();
            btnexport.Enabled = false;
            if (dtTemp != null)
            {
                if (dtTemp.Rows.Count > 0)
                {
                    rptAbsentEmpByWeek.DataSource = (from x in dvWeeklyReport.ToTable().AsEnumerable() select x["AbsentDate"]).Distinct();
                    rptAbsentEmpByWeek.DataBind();
                }
            }

        //=====================//

            string strdate = @"SELECT 
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0),103)   as Date_of_Monday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+1,103) as Date_of_Tuesday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+2,103) as Date_of_Wednesday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+3,103) as Date_of_Thusday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+4,103) as Date_of_Friday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+5,103) as Date_of_Saturday";
            //*****************************For ALL Employee**********************************************

            DataSet ds = DatabaseHelper.getDataset(strdate);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                fnFillEmpddl();
                clear();
                dynamically(ds);
                if (ddlEmp.SelectedItem.Text == "All")
                {
                    strreportname = ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "_To_" + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString();
                }
                else 
                {
                    strreportname = ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "_To_" + ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString();
                }               
            }

            fnFillCompddl();
           // Compddl.SelectedIndex = Convert.ToInt32(Session["DeptId"]);
            //Compddl.Enabled = false;
        }

    }
    
    public void clear()
    {}

    void fnFillEmpddl()
    {
        
        if (Session["CompanyName"] != null)
        {
            ddlEmp.Items.Clear();

            if (Session["CompanyName"].ToString().Trim() != "--Select--")
            {
                string sql = "Select ( firstname+' '+lastname) as Name ,username from users where IsCW='Y' and CompanyName='" + Session["CompanyName"].ToString() + "' order by username";

                DataSet dsAdmin = DatabaseHelper.getDataset(sql);

                //  ddlEmp.Items.Add(new ListItem("ALL", "ALL"));
                //  for (int i = 1; i < dsAdmin.Tables[0].Rows.Count; i++)
                //  {
                // ListItem item = new ListItem(dsAdmin.Tables[0].Rows[i]["username"].ToString(), dsAdmin.Tables[0].Rows[i]["Name"].ToString());
                // ddlEmp.Items.Add(item);
                // }
                ddlEmp.DataSource = dsAdmin;
                ddlEmp.DataValueField = "username";
                ddlEmp.DataTextField = "Name";
                ddlEmp.DataBind();
                ddlEmp.Items.Insert(0, "ALL");
            }
            else
            {
                ddlEmp.Items.Insert(0, "--Empty--");
            }
        }
        else
        {
            Response.Redirect("login.aspx");
        }
        
    }       
    
    public void dynamically(DataSet ds) 
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        string strAllEmployee="";

        if (Compddl.SelectedItem != null)
        {
            HtmlDesign.Visible = true;
            if (Compddl.SelectedItem.Text.Trim() != "--Select--")
            {
                if (ddlEmp.SelectedItem.Text == "ALL")
                {
                    //strAllEmployee = @"select * from users where IsCW='Y' and username in ('Satyendra','Sheereen','Aditi','Suraj') order by username";
                    strAllEmployee = @"select * from users where IsCW='Y' and  CompanyName='" + Compddl.SelectedItem.Text + "'";
                }
                else
                {
                    strAllEmployee = @"select * from users where IsCW='Y' and username = '" + ddlEmp.SelectedValue.ToString() + "' order by username ";
                }
                DataSet dsAllEmp = DatabaseHelper.getDataset(strAllEmployee);
                if (dsAllEmp != null && dsAllEmp.Tables.Count > 0 && dsAllEmp.Tables[0].Rows.Count > 0)
                {
                    sb.Append("<table width='100%'>");
                    sb.Append("<tr>");
                    sb.Append("<td height='10px' colspan='8' align='left' style='font-size:medium'>");
                    sb.Append("<b>Resource Planning Reports, MUMBAI ( from ");
                    sb.Append(ds.Tables[0].Rows[0]["Date_of_Monday"].ToString());
                    sb.Append(" to ");
                    //  if (ddlEmp.SelectedItem.Text == "PHG")
                    //  {
                    sb.Append(ds.Tables[0].Rows[0]["Date_of_Friday"].ToString() + ")");
                    // }
                    //  else
                    //   {
                    //    sb.Append(ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString() + ")");
                    //  }
                    sb.Append("</b></td>");
                    sb.Append("</tr>");
                    sb.Append("<tr>");
                    sb.Append("<td height='10px'>");
                    sb.Append("</td>");
                    sb.Append("</tr>");
                    sb.Append("<tr>");
                    sb.Append("<td height='10px'>");
                    sb.Append("</td>");
                    sb.Append("</tr>");
                    sb.Append("<tr>");
                    sb.Append("<td height='10px'>");
                    sb.Append("</td>");
                    sb.Append("</tr>");
                    sb.Append("</table>");
                    sb.Append("<table width='100%' border='1'>");


                    sb.Append("<tr>");

                    sb.Append("<td style='border-width: 1pt 1pt 1pt medium; border-style: solid solid solid none;");
                    sb.Append("border-color: black black black -moz-use-text-color; -moz-border-top-colors: none;");
                    sb.Append("-moz-border-right-colors: none; -moz-border-bottom-colors: none; -moz-border-left-colors: none;");
                    sb.Append("border-image: none; background: none repeat scroll 0% 0%; padding: 0cm 5.4pt;'>");
                    sb.Append("<b>Employee name</b>");
                    sb.Append("</td>");



                    sb.Append("<td style='border-width: 1pt 1pt 1pt medium; border-style: solid solid solid none;");
                    sb.Append("border-color: black black black -moz-use-text-color; -moz-border-top-colors: none;");
                    sb.Append("-moz-border-right-colors: none; -moz-border-bottom-colors: none; -moz-border-left-colors: none;");
                    sb.Append("border-image: none; background: none repeat scroll 0% 0%; padding: 0cm 5.4pt;'>");
                    sb.Append("Skill");
                    sb.Append("</td>");

                    sb.Append("<td style='border-width: 1pt 1pt 1pt medium; border-style: solid solid solid none;");
                    sb.Append("border-color: black black black -moz-use-text-color; -moz-border-top-colors: none;");
                    sb.Append("-moz-border-right-colors: none; -moz-border-bottom-colors: none; -moz-border-left-colors: none;");
                    sb.Append("border-image: none; background: none repeat scroll 0% 0%; padding: 0cm 5.4pt;'>");

                    sb.Append("<table width='100%' border='1'>");
                    sb.Append("<tr>");
                    sb.Append("<td>");

                    sb.Append(" " + ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + " (Monday)");
                    sb.Append("<br><table border='1'><tr><td>In/Out (GMT)</td><td> Total </td></tr></table>");
                    //sb.Append("<br><table border='1'><tr><td>In/Out (GMT)</td><td> Total </td> <td> Planned leave </td></tr></table>");
                    sb.Append("</td>");


                    sb.Append("<td>");

                    sb.Append(" " + ds.Tables[0].Rows[0]["Date_of_Tuesday"].ToString() + " (Tuesday)");
                    sb.Append("<br><table border='1'><tr><td>In/Out (GMT)</td><td> Total </td></tr></table>");
                    //sb.Append("<br><table border='1'><tr><td>In/Out (GMT)</td><td> Total </td><td> Planned leave </td></tr></table>");
                    sb.Append("</td>");


                    sb.Append("<td>");
                    sb.Append(" " + ds.Tables[0].Rows[0]["Date_of_Wednesday"].ToString() + " (Wednesday)");
                    sb.Append("<br><table border='1'><tr><td>In/Out (GMT)</td><td> Total </td></tr></table>");
                    //sb.Append("<br><table border='1'><tr><td>In/Out (GMT)</td><td>  Total </td><td> Planned leave </td></tr></table>");
                    sb.Append("</td>");


                    sb.Append("<td>");
                    sb.Append(" " + ds.Tables[0].Rows[0]["Date_of_Thusday"].ToString() + " (Thusday)");
                    sb.Append("<br><table border='1'><tr><td>In/Out (GMT)</td><td> Total </td></tr></table>");
                    //sb.Append("<br><table border='1'><tr><td>In/Out (GMT)</td><td> Total </td><td> Planned leave </td></tr></table>");
                    sb.Append("</td>");

                    sb.Append("<td>");
                    sb.Append(" " + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString() + " (Friday)");
                    sb.Append("<br><table border='1'><tr><td>In/Out (GMT)</td><td> Total </td></tr></table>");
                    //sb.Append("<br><table border='1'><tr><td>In/Out (GMT)</td><td> Total </td><td> Planned leave </td></tr></table>");
                    sb.Append("</td>");


                    if (ddlEmp.SelectedItem.Text != "ESG")
                    {
                        sb.Append("<td>");
                        sb.Append(" " + ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString() + " (Saturday)");
                        sb.Append("<br><table border='1'><tr><td>In/Out (GMT)</td><td> Total </td></tr></table>");
                        //sb.Append("<br><table border='1'><tr><td>In/Out (GMT)</td><td> Total </td><td> Planned leave </td></tr></table>");
                        sb.Append("</td>");
                    }


                    sb.Append("</tr>");
                    sb.Append("</table>");
                    sb.Append("</td>");

                    sb.Append("</tr>");
                    for (int iEmp = 0; iEmp < dsAllEmp.Tables[0].Rows.Count; iEmp++)
                    {
                        if (iEmp % 2 == 0)
                        {
                            sb.Append("<tr style='background:#fde9d9;'>");
                        }
                        else
                        {
                            sb.Append("<tr>");
                        }
                        sb.Append("<td align='left' style='width: 168.75pt; border-width: 1pt 1pt 1pt medium; border-style: solid solid solid none;border-color: black black black -moz-use-text-color; -moz-border-top-colors: none;-moz-border-right-colors: none; -moz-border-bottom-colors: none; -moz-border-left-colors: none;border-image: none; background: none repeat scroll 0% 0%; padding: 0cm 5.4pt;'>");
                        sb.Append("<b>" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "</b>");
                        sb.Append("</td><td align='left' >" + dsAllEmp.Tables[0].Rows[iEmp]["skills"].ToString() + "</td>");

                        //******************************************* Create In Time ******************************************************************
                        sb.Append("<td style='border-width: medium 1pt 1pt medium; border-style: none solid solid none;");
                        sb.Append("border-color: -moz-use-text-color black black -moz-use-text-color; background: none repeat scroll 0% 0%;");
                        sb.Append("padding: 0cm 5.4pt;'>");
                        sb.Append("<table width='100%' border='1'>");
                        sb.Append("<tr>");
                        sb.Append("<td>");

                        sb.Append("<table width='100%' border='1' height='100%'>");
                        sb.Append("<tr>");
                        sb.Append("<td>");

                        string strInout1 = "";
                        object objIntimeALL1 = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "'");
                        if (objIntimeALL1 != null && objIntimeALL1.ToString() != "")
                        {
                            DateTime dtALL1 = Convert.ToDateTime(objIntimeALL1).AddHours(-5).AddMinutes(-30);
                            //strInout1 = dtALL1.ToString("HH:mm tt").ToString() + "/";
                            strInout1 = dtALL1.ToString("HH:mm").ToString() + "/";
                        }
                        else
                        {
                            strInout1 = "-/";
                        }


                        object objOuttimeALL1 = DatabaseHelper.executeScalar("select Outtime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "'");
                        if (objOuttimeALL1 != null && objOuttimeALL1.ToString() != "")
                        {
                            DateTime dtoutALL1 = Convert.ToDateTime(objOuttimeALL1).AddHours(-5).AddMinutes(-30);
                            strInout1 += dtoutALL1.ToString("HH:mm");
                        }
                        else
                        {
                            strInout1 += "-";
                        }

                        sb.Append(strInout1);
                        sb.Append("</td>");
                        sb.Append("<td>");

                        object objTotalHours1 = DatabaseHelper.executeScalar("select TotalHrs from tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "'  and EodcDate='" + ds.Tables[0].Rows[0]["Date_of_Monday"].ToString().Trim() + "'");
                        if (objTotalHours1 != null && objTotalHours1.ToString() != "")
                        {
                            sb.Append(objTotalHours1.ToString());

                        }
                        else
                        {
                            sb.Append("-");

                        }

                        sb.Append("</td>");
                        //sb.Append("<td>");



                        string firstuserdate1 = "";
                        if (GMDStartDate.Text != "")
                        {
                            firstuserdate1 = @"Select CONVERT(VARCHAR(10),AbsentDate,103) as LeaveDate from tbl_LeaveDetails where UserName='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and  (AbsentDate >=DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text + "'),0) and AbsentDate <= DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text + "'),0)+4)";
                        }
                        else
                        {
                            firstuserdate1 = @"Select CONVERT(VARCHAR(10),AbsentDate,103)  as LeaveDate from tbl_LeaveDetails where UserName='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and (AbsentDate >=DATEADD(week, DATEDIFF(week, 0,GETDATE()),0) and AbsentDate <= DATEADD(week, DATEDIFF(week, 0,GETDATE()),0)+4)";
                        }
                        DataSet dsFirstdate = DatabaseHelper.getDataset(firstuserdate1);

                        //if (dsFirstdate != null && dsFirstdate.Tables.Count > 0 && dsFirstdate.Tables[0].Rows.Count > 0)
                        //{
                        //    for (int fi = 0; fi < dsFirstdate.Tables[0].Rows.Count; fi++)
                        //    {

                        //        if (dsFirstdate.Tables[0].Rows[fi]["LeaveDate"].ToString() == ds.Tables[0].Rows[0]["Date_of_Monday"].ToString())
                        //        {

                        //            sb.Append("PL");

                        //        }
                        //    }
                        //}
                        //sb.Append("</td>");
                        sb.Append("</tr>");
                        sb.Append("</table>");
                        sb.Append("</td>");
                        //-----------------------------------------
                        sb.Append("<td>");
                        sb.Append("<table width='100%' border='1' height='100%'>");
                        sb.Append("<tr>");
                        sb.Append("<td>");

                        string strInout2 = "";
                        object objIntimeALL2 = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Tuesday"].ToString() + "'");
                        if (objIntimeALL2 != null && objIntimeALL2.ToString() != "")
                        {
                            DateTime dtALL2 = Convert.ToDateTime(objIntimeALL2).AddHours(-5).AddMinutes(-30);

                            strInout2 = dtALL2.ToString("HH:mm").ToString() + "/";
                        }
                        else
                        {

                            strInout2 = "-/";
                        }

                        object objOuttimeALL2 = DatabaseHelper.executeScalar("select Outtime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Tuesday"].ToString() + "'");
                        if (objOuttimeALL2 != null && objOuttimeALL2.ToString() != "")
                        {
                            DateTime dtoutALL2 = Convert.ToDateTime(objOuttimeALL2).AddHours(-5).AddMinutes(-30);

                            strInout2 += dtoutALL2.ToString("HH:mm");
                        }
                        else
                        {

                            strInout2 += "-";
                        }

                        sb.Append(strInout2);

                        sb.Append("</td>");
                        sb.Append("<td>");

                        object objTotalHours2 = DatabaseHelper.executeScalar("select TotalHrs from tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and EodcDate='" + ds.Tables[0].Rows[0]["Date_of_Tuesday"].ToString().Trim() + "'");
                        if (objTotalHours2 != null && objTotalHours2.ToString() != "")
                        {
                            sb.Append(objTotalHours2.ToString());
                        }
                        else
                        {

                            sb.Append("-");

                        }
                        sb.Append("</td>");
                        //sb.Append("<td>");
                        //if (dsFirstdate != null && dsFirstdate.Tables.Count > 0 && dsFirstdate.Tables[0].Rows.Count > 0)
                        //{
                        //    for (int fi = 0; fi < dsFirstdate.Tables[0].Rows.Count; fi++)
                        //    {

                        //        if (dsFirstdate.Tables[0].Rows[fi]["LeaveDate"].ToString() == ds.Tables[0].Rows[0]["Date_of_Tuesday"].ToString())
                        //        {
                        //          sb.Append("PL");
                        //        }
                        //     }
                        //}

                        //sb.Append("</td>");
                        sb.Append("</tr>");
                        sb.Append("</table>");
                        sb.Append("</td>");

                        //--------------------------------------------------------
                        string strInout3 = "";

                        sb.Append("<td>");
                        sb.Append("<table width='100%' border='1' height='100%'>");
                        sb.Append("<tr>");
                        sb.Append("<td>");
                        object objIntimeALL3 = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Wednesday"].ToString() + "'");
                        if (objIntimeALL3 != null && objIntimeALL3.ToString() != "")
                        {
                            DateTime dtALL3 = Convert.ToDateTime(objIntimeALL3).AddHours(-5).AddMinutes(-30);

                            strInout3 = dtALL3.ToString("HH:mm tt") + "/";

                        }
                        else
                        {

                            strInout3 = "-/";
                        }
                        object objOuttimeALL3 = DatabaseHelper.executeScalar("select Outtime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Wednesday"].ToString() + "'");
                        if (objOuttimeALL3 != null && objOuttimeALL3.ToString() != "")
                        {
                            DateTime dtoutALL3 = Convert.ToDateTime(objOuttimeALL3).AddHours(-5).AddMinutes(-30);

                            strInout3 += dtoutALL3.ToString("HH:mm");
                        }
                        else
                        {

                            strInout3 += "-";
                        }
                        sb.Append(strInout3);

                        sb.Append("</td>");
                        sb.Append("<td>");



                        object objTotalHours3 = DatabaseHelper.executeScalar("select TotalHrs from tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and EodcDate='" + ds.Tables[0].Rows[0]["Date_of_Wednesday"].ToString().Trim() + "'");
                        if (objTotalHours3 != null && objTotalHours3.ToString() != "")
                        {
                            sb.Append(objTotalHours3.ToString());
                        }
                        else
                        {
                            sb.Append("-");
                        }

                        sb.Append("</td>");
                        //sb.Append("<td>");

                        //if (dsFirstdate != null && dsFirstdate.Tables.Count > 0 && dsFirstdate.Tables[0].Rows.Count > 0)
                        //{
                        //    for (int fi = 0; fi < dsFirstdate.Tables[0].Rows.Count; fi++)
                        //    {

                        //        if (dsFirstdate.Tables[0].Rows[fi]["LeaveDate"].ToString() == ds.Tables[0].Rows[0]["Date_of_Wednesday"].ToString())
                        //        {

                        //          sb.Append("PL");

                        //        }

                        //    }
                        //}

                        //sb.Append("</td>");
                        sb.Append("</tr>");
                        sb.Append("</table>");
                        sb.Append("</td>");

                        //------------------------------------------------------
                        string strInout4 = "";
                        sb.Append("<td>");
                        sb.Append("<table width='100%' border='1' height='100%'>");
                        sb.Append("<tr>");
                        sb.Append("<td>");

                        object objIntimeALL4 = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Thusday"].ToString() + "'");
                        if (objIntimeALL4 != null && objIntimeALL4.ToString() != "")
                        {
                            DateTime dtALL4 = Convert.ToDateTime(objIntimeALL4).AddHours(-5).AddMinutes(-30);

                            strInout4 = dtALL4.ToString("HH:mm") + "/";
                        }
                        else
                        {

                            strInout4 = "-/";
                        }

                        object objOuttimeALL4 = DatabaseHelper.executeScalar("select Outtime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Thusday"].ToString() + "'");
                        if (objOuttimeALL4 != null && objOuttimeALL4.ToString() != "")
                        {
                            DateTime dtoutALL4 = Convert.ToDateTime(objOuttimeALL4).AddHours(-5).AddMinutes(-30);

                            strInout4 += dtoutALL4.ToString("HH:mm");
                        }
                        else
                        {

                            strInout4 += "-";
                        }

                        sb.Append(strInout4);

                        sb.Append("</td>");
                        sb.Append("<td>");


                        object objTotalHours4 = DatabaseHelper.executeScalar("select TotalHrs from tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and EodcDate='" + ds.Tables[0].Rows[0]["Date_of_Thusday"].ToString().Trim() + "'");
                        if (objTotalHours4 != null && objTotalHours4.ToString() != "")
                        {
                            sb.Append(objTotalHours4.ToString());
                        }
                        else
                        {
                            sb.Append("-");
                        }

                        sb.Append("</td>");
                        //sb.Append("<td>");

                        //if (dsFirstdate != null && dsFirstdate.Tables.Count > 0 && dsFirstdate.Tables[0].Rows.Count > 0)
                        //{
                        //    for (int fi = 0; fi < dsFirstdate.Tables[0].Rows.Count; fi++)
                        //    {

                        //        if (dsFirstdate.Tables[0].Rows[fi]["LeaveDate"].ToString() == ds.Tables[0].Rows[0]["Date_of_Thusday"].ToString())
                        //        {

                        //           sb.Append("PL");

                        //        }
                        //    }
                        //}
                        //sb.Append("</td>");
                        sb.Append("</tr>");
                        sb.Append("</table>");
                        sb.Append("</td>");
                        //----------------------------------------------------------------
                        string strInout5 = "";
                        sb.Append("<td>");
                        sb.Append("<table width='100%' border='1' height='100%'>");
                        sb.Append("<tr>");
                        sb.Append("<td>");

                        object objIntimeALL5 = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString() + "'");
                        if (objIntimeALL5 != null && objIntimeALL5.ToString() != "")
                        {
                            DateTime dtALL5 = Convert.ToDateTime(objIntimeALL5).AddHours(-5).AddMinutes(-30);

                            strInout5 = dtALL5.ToString("HH:mm") + "/";
                        }
                        else
                        {

                            strInout5 = "-/";
                        }
                        object objOuttimeALL5 = DatabaseHelper.executeScalar("select Outtime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString() + "'");
                        if (objOuttimeALL5 != null && objOuttimeALL5.ToString() != "")
                        {
                            DateTime dtoutALL5 = Convert.ToDateTime(objOuttimeALL5).AddHours(-5).AddMinutes(-30);

                            strInout5 += dtoutALL5.ToString("HH:mm");
                        }
                        else
                        {

                            strInout5 += "-";
                        }
                        sb.Append(strInout5);
                        sb.Append("</td>");
                        sb.Append("<td>");

                        object objTotalHours5 = DatabaseHelper.executeScalar("select TotalHrs from tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and EodcDate='" + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString().Trim() + "'");
                        if (objTotalHours5 != null && objTotalHours5.ToString() != "")
                        {
                            sb.Append(objTotalHours5.ToString());
                        }

                        sb.Append("</td>");
                        //sb.Append("<td>");
                        //if (dsFirstdate != null && dsFirstdate.Tables.Count > 0 && dsFirstdate.Tables[0].Rows.Count > 0)
                        //{
                        //    for (int fi = 0; fi < dsFirstdate.Tables[0].Rows.Count; fi++)
                        //    {

                        //        if (dsFirstdate.Tables[0].Rows[fi]["LeaveDate"].ToString() == ds.Tables[0].Rows[0]["Date_of_Friday"].ToString())
                        //        {


                        //            sb.Append("PL");

                        //        }
                        //     }
                        //}
                        //sb.Append("</td>");
                        sb.Append("</tr>");
                        sb.Append("</table>");
                        sb.Append("</td>");
                        //----------------------------------------------------------------
                        if (ddlEmp.SelectedItem.Text == "ESG") //(ddlEmp.SelectedItem.Text != "PHG")
                        {
                            string strInout6 = "";
                            sb.Append("<td>");
                            sb.Append("<table width='100%' border='1' height='100%'>");
                            sb.Append("<tr>");
                            sb.Append("<td>");


                            object objIntimeALL6 = DatabaseHelper.executeScalar("select intime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString() + "'");
                            if (objIntimeALL6 != null && objIntimeALL6.ToString() != "")
                            {
                                DateTime dtALL6 = Convert.ToDateTime(objIntimeALL6).AddHours(-5).AddMinutes(-30);

                                strInout6 = dtALL6.ToString("HH:mm") + "/";
                            }
                            else
                            {

                                strInout6 = "-/";
                            }
                            object objOuttimeALL6 = DatabaseHelper.executeScalar("select Outtime from tblIntime where username ='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and indate ='" + ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString() + "'");
                            if (objOuttimeALL6 != null && objOuttimeALL6.ToString() != "")
                            {
                                DateTime dtoutALL6 = Convert.ToDateTime(objOuttimeALL6).AddHours(-5).AddMinutes(-30);

                                strInout6 += dtoutALL6.ToString("HH:mm");
                            }
                            else
                            {

                                strInout6 += "-";
                            }
                            sb.Append(strInout6);
                            sb.Append("</td>");
                            sb.Append("<td>");

                            object objTotalHours6 = DatabaseHelper.executeScalar("select TotalHrs from tblEmp where username='" + dsAllEmp.Tables[0].Rows[iEmp]["username"].ToString() + "' and EodcDate='" + ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString().Trim() + "'");
                            if (objTotalHours6 != null && objTotalHours6.ToString() != "")
                            {
                                sb.Append(objTotalHours6.ToString());
                            }

                            sb.Append("</td>");
                            //sb.Append("<td>");
                            //if (dsFirstdate != null && dsFirstdate.Tables.Count > 0 && dsFirstdate.Tables[0].Rows.Count > 0)
                            //{
                            //    for (int fi = 0; fi < dsFirstdate.Tables[0].Rows.Count; fi++)
                            //    {

                            //        if (dsFirstdate.Tables[0].Rows[fi]["LeaveDate"].ToString() == ds.Tables[0].Rows[0]["Date_of_Friday"].ToString())
                            //        {


                            //            sb.Append("PL");

                            //        }
                            //    }
                            //}
                            //sb.Append("</td>");
                            sb.Append("</tr>");
                            sb.Append("</table>");
                            sb.Append("</td>");
                        }


                        sb.Append("</table>");
                        sb.Append("</td>");




                        //******************************************* End Create In Time ******************************************************************



                        sb.Append("</tr>");
                    }
                    sb.Append("</table>");
                   // if (DateWiseReport.InnerText!= null)
                        HtmlDesign.InnerHtml = sb.ToString() + "<br/>"; //+ DateWiseReport.InnerText;
                    //else
                      //  HtmlDesign.InnerHtml = sb.ToString();
                }
            }
            else
            {
                lbl_pageinfo.Visible = true;
                HtmlDesign.InnerHtml = "";
            }
        }
    }

    protected void btnshow_Click(object sender, EventArgs e)
    {
        string strQuery = "";
        
        strQuery = @"SELECT  CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0),103) as Date_of_Monday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+1,103) as Date_of_Tuesday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+2,103) as Date_of_Wednesday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+3,103) as Date_of_Thusday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+4,103) as Date_of_Friday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+5,103) as Date_of_Saturday";

        DataSet ds = DatabaseHelper.getDataset(strQuery);
        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            clear();
            strreportname = ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "_To_" + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString();
            dynamically(ds);
        }
        //showAttendence();y
    }

    protected void btnexport_Click(object sender, EventArgs e)
    {
        Response.ContentType = "application/x-msexcel";
        Response.AddHeader("Content-Disposition", "attachment; filename=Timesheet_report_from_'" + strreportname + "'.xls");
        //Response.ContentEncoding = Encoding.UTF8;
        System.IO.StringWriter tw = new System.IO.StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(tw);
        main.RenderControl(hw);
        Response.Write(tw.ToString());
        Response.End();

    }

  

//========================Weekly report===========================//

    public void fnFillCompddl()
    {
        if (Session["username"] != null)
        {
            DataSet ds = DatabaseHelper.getCompanyName(Session["clientid"].ToString(), Session["username"].ToString());
            if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0].Rows.Count <= 0)
            {
                ds = new DataSet();
                ds.Tables.Add();
                ds.Tables[0].Columns.Add("CompanyName");
            }
            DataRow row = ds.Tables[0].NewRow();
            row["CompanyName"] = "--Select--";
            ds.Tables[0].Rows.InsertAt(row, 0);

            Compddl.DataSource = ds.Tables[0];
            Compddl.DataTextField = "CompanyName";
            Compddl.DataValueField = "CompanyName";
            Compddl.DataBind();
        }
        else
        {
            Response.Redirect("login.aspx");
        }
       
    }

    protected void Compddl_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["CompanyName"] = Compddl.SelectedValue.ToString();
        fnFillEmpddl();

        if (Compddl.SelectedItem.Text != "--Select--")
        {
            fnGenatateLeaveReoprt();
            fnGenerateTimeSheet();
            if (dtTemp != null)
            {
                if (dtTemp.Rows.Count > 0)
                {
                   // lbl_pageinfo.Visible = false;
                    lbl_pageinfo.Text = "--Employee leave report--";
                    rptAbsentEmpByWeek.Visible = true;
                    rptAbsentEmpByWeek.DataSource = (from x in dvWeeklyReport.ToTable().AsEnumerable() select x["AbsentDate"]).Distinct();
                    rptAbsentEmpByWeek.DataBind();
                }
                else
                {
                    //  HtmlDesign.InnerHtml = "";
                    rptAbsentEmpByWeek.Visible = false;
                    lbl_pageinfo.Visible = true;
                    lbl_pageinfo.Text = "No one was absent between selected time interval.";
                }

            }
            else
            {
                //btnexport.Enabled = false;
               // btnshow.Enabled = false;
                // HtmlDesign.InnerHtml = "";
                rptAbsentEmpByWeek.Visible = false;
                lbl_pageinfo.Visible = true;
                lbl_pageinfo.Text = "No one was absent between selected interval.";
            }
        }
        else
        {
            HtmlDesign.InnerHtml = "";
            rptAbsentEmpByWeek.Visible = false;
            lbl_pageinfo.Visible = true;
            lbl_pageinfo.Text = "No one was absent between selected interval";
        }
    }

    protected void ddlEmp_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {

            RequiredFieldValidator2.Validate();
            string strdate = "";
            fnGenerateTimeSheet();
            fnGenatateLeaveReoprt();
            if (dvWeeklyReport != null)
            {
                if (dvWeeklyReport.ToTable().Rows.Count > 0)
                {
                    rptAbsentEmpByWeek.Visible = true;
                    lbl_pageinfo.Text = "--Employee leave report--";
                    rptAbsentEmpByWeek.DataSource = (from x in dvWeeklyReport.ToTable().AsEnumerable() select x["AbsentDate"]).Distinct();
                    rptAbsentEmpByWeek.DataBind();
                }
                else
                {
                    rptAbsentEmpByWeek.Visible = false;
                    lbl_pageinfo.Text = "No one was absent between selected interval.";
                    btnshow.Enabled = false;
                  //btnexport.Enabled = false;
                }
            }
            else
            {
                lbl_pageinfo.Text = "No one was absent between selected interval.";
                // btnshow.Enabled = false;
                // btnexport.Enabled = false;
            }

            if (GMDStartDate.Text.ToString() == "")
            {
            strdate = @"SELECT 
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0),103)   as Date_of_Monday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+1,103) as Date_of_Tuesday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+2,103) as Date_of_Wednesday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+3,103) as Date_of_Thusday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+4,103) as Date_of_Friday,
            CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, GETDATE()) , 0)+5,103) as Date_of_Saturday";
            }
            else
            {
                strdate = @"SELECT  CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0),103) as Date_of_Monday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+1,103) as Date_of_Tuesday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+2,103) as Date_of_Wednesday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+3,103) as Date_of_Thusday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+4,103) as Date_of_Friday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+5,103) as Date_of_Saturday";

            }
            DataSet ds = DatabaseHelper.getDataset(strdate);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                dynamically(ds);
                if (ddlEmp.SelectedItem.Text != "ESG") //(ddlEmp.SelectedItem.Text == "PHG")
                {
                    strreportname = ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "_To_" + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString();
                }
                else
                {
                    strreportname = ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "_To_" + ds.Tables[0].Rows[0]["Date_of_Saturday"].ToString();
                }
            }
        }
        catch
        {
        }
    }

    public void fnSetDefaultDate()
    {

        // lblError.Visible = false;
        String DayName = DateTime.Now.DayOfWeek.ToString();
      
        if (DayName == "Monday")
        {
            DayNo = 1;
        }
        else if (DayName == "Tuesday")
        {
            DayNo = 2;
        }

        else if (DayName == "Wednesday")
        {
            DayNo = 3;
        }

        else if (DayName == "Thursday")
        {
            DayNo = 4;
        }
        else if (DayName == "Friday")
        {
            DayNo = 5;
        }


    //  dtStartingDate = DateTime.Today.AddDays(-DayNo + 1);    // Monday
    //  dtEndingDate = DateTime.Today.AddDays(5 - DayNo);      //Friday
    }

    void fnGenatateLeaveReoprt()
    {
        try
        {
           // TextBox txt = this.Parent.FindControl("txtVisitorName") as TextBox;
           // txt.Text = "Krishna"; //u can set ur value here
           // TextBox d1= this.Parent.FindControl("GMDStartDate") as TextBox;
           // TextBox d2= this.Parent.FindControl("GMDEndDate") as TextBox;


            fnFillDates();
       
            DataSet dsTemp = objLeave.getWeeklyReport(dtSDate, dtEDate, Session["CompanyName"].ToString());

            if(dsTemp!=null)
            {
                if (dsTemp.Tables.Count > 0)
                {
                    dtTemp = dsTemp.Tables[0];

                    List<string> strDateArray = new List<string>();

                    if (dtTemp != null)
                    {
                        if (dtTemp.Rows.Count > 0)
                        {
                            btnshow.Enabled = true;
                            btnexport.Enabled = true;

                            dtTemp.Columns.Add("Planned", typeof(string));
                            dtTemp.Columns.Add("Unplanned", typeof(string));
                            dtTemp1 = dtTemp.Clone();

                            //foreach (DataColumn dc in dtTemp.Columns)
                            //{
                            //    if (dc.ColumnName == "AbsentDate")
                            //    {
                            //        dc.DataType = typeof(DateTime);
                            //    }
                            //}

                            foreach (DataRow dr in dtTemp.Rows)
                            {
                                DateTime ldt = Convert.ToDateTime(dr["AbsentDate"].ToString());
                                string strReason = dr["Reason"].ToString();
                                dr["AbsentDate"] = ldt.ToString("dd-MM-yyyy");//ldt.ToString("yyyy/MM/dd hh:mm:ss.fff");
                                dr["Reason"] = strReason.Replace("@", "'").Trim();
                                if (dr["IsPlanned"].ToString().Trim() == "True")
                                {
                                    dr["Planned"] = "Yes";
                                    dr["Unplanned"] = "No";
                                    dr["Reason"] = "";
                                }
                                else
                                {
                                    dr["Planned"] = "No";
                                    dr["Unplanned"] = "Yes";
                                }
                                dtTemp1.ImportRow(dr);
                            }
                            //End Datatype conversion for AbsentDate Column in dtTemp

                            dvWeeklyReport = new DataView(dtTemp1);

                            if (ddlEmp.SelectedItem.Text != "ALL")
                            {
                                dvWeeklyReport.RowFilter = "username = '" + ddlEmp.SelectedValue.ToString().Trim() + "'";
                            }
                            if (dvWeeklyReport != null)
                            {
                                if (dvWeeklyReport.ToTable().Rows.Count > 0)
                                {
                                    HtmlDesign.Visible = true;
                                    btnshow.Enabled = true;
                                    btnexport.Enabled = true;
                                    //lbl_pageinfo.Text="No one was absent between selected interval.";
                                }
                                else
                                {
                                    lbl_pageinfo.Text = "No one was absent between selected interval.";
                                    HtmlDesign.Visible = false;
                                    btnshow.Enabled = false;
                                   // btnexport.Enabled = false;
                                }
                            }

                        }
                        else
                        {
                            lbl_pageinfo.Text = "No one was absent between selected interval.";
                            btnshow.Enabled = false;
                           //btnexport.Enabled = false;
                        }

                    }
                }
            }
            else
            {
                btnshow.Enabled = false;
                //btnexport.Enabled= false;
                // this.lbl_pageinfo.Text = "No one was absent in last 5 days";
                //  this.lbl_pageinfo.Visible = true;
            }

        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, "Exception in LMS frmAdminHome.aspx.cs", "Method : Binddata()");
        }
    }

    void fnGenerateTimeSheet()
    {
        string strQuery = "";

        strQuery = @"SELECT  CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0),103) as Date_of_Monday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+1,103) as Date_of_Tuesday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+2,103) as Date_of_Wednesday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+3,103) as Date_of_Thusday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+4,103) as Date_of_Friday, CONVERT(VARCHAR(10),DATEADD(week, DATEDIFF(week, 0, '" + GMDStartDate.Text.ToString() + "') , 0)+5,103) as Date_of_Saturday";

        DataSet ds = DatabaseHelper.getDataset(strQuery);
        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            clear();
            strreportname = ds.Tables[0].Rows[0]["Date_of_Monday"].ToString() + "_To_" + ds.Tables[0].Rows[0]["Date_of_Friday"].ToString();
            dynamically(ds);
        }
    }

    protected void rptAbsentEmpByWeek_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item |
                     e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater rptr = (Repeater)e.Item.FindControl("Repeater2");
                rptr.DataSource = dvWeeklyReport.ToTable().AsEnumerable().Where(x => x["AbsentDate"].Equals(e.Item.DataItem));
                rptr.DataBind();
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.HandleException(ex, "Exception in LMS frmAdminHome.aspx.cs", "Event : rptAbsentEmpByWeek_ItemDataBound()");
        }
    }

    static DateTime LastDayOfYear()
    {
        return LastDayOfYear(DateTime.Today);
    }

    static DateTime LastDayOfYear(DateTime d)
    {
        // 1
        // Get first of next year
        DateTime n = new DateTime(d.Year + 1, 1, 1);
        // 2
        // Subtract 1 from it
        return n.AddDays(-1);
    }

    static DateTime FirstDayOfYear()
    {
        return FirstDayOfYear(DateTime.Today);
    }

    static DateTime FirstDayOfYear(DateTime y)
    {
        return new DateTime(y.Year, 1, 1);
    }

    protected void GMDEndDate_TextChanged(object sender, EventArgs e)
    {
        fnGenatateLeaveReoprt();
        if (dtTemp != null)
        {
            if (dtTemp.Rows.Count > 0)
            {
                rptAbsentEmpByWeek.DataSource = (from x in dvWeeklyReport.ToTable().AsEnumerable() select x["AbsentDate"]).Distinct();
                rptAbsentEmpByWeek.DataBind();
            }
        }

    }

    public void fnFillDates()
    {
        if (GMDStartDate.Text.Trim() == "")
        {
            dtSDate = DateTime.Today.AddDays(-DayNo + 1).ToString("yyyy-MM-dd");  //FirstDayOfYear(dt).ToString("dd/MM/yyyy");
            GMDStartDate.Text = dtSDate;
            dtStartingDate = DateTime.Today.AddDays(-DayNo + 1);
        }
        else
        {
            dtStartingDate = Convert.ToDateTime(GMDStartDate.Text);
            dtSDate = dtStartingDate.ToString("yyyy-MM-dd");
        }

        if (GMDEndDate.Text.Trim() == "")
        {
            dtEDate = DateTime.Today.AddDays(5 - DayNo).ToString("yyyy-MM-dd"); //LastDayOfYear(dt).ToString("dd/MM/yyyy");
            GMDEndDate.Text = dtEDate;
            dtEndingDate = DateTime.Today.AddDays(5 - DayNo);

        }
        else
        {
            dtEndingDate = Convert.ToDateTime(GMDEndDate.Text);
            dtEDate = dtEndingDate.ToString("yyyy-MM-dd");
        }
    }
}
