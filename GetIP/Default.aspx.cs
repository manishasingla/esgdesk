﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Net;

using System.Xml;
using System.Management;
using System.Runtime.InteropServices;
using System.Net.NetworkInformation;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
      
        string ipaddress = fnGetMacAddress();

        ipaddress +="IP Address of your Machine"+" "+fnGetIpAddress();

        lblMsg.Text = "Your IP is" +" "+ ipaddress;
  
    }
    private string fnGetMacAddress()
    {
        string macAddresses = "";

        foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
        {
            if (nic.OperationalStatus == OperationalStatus.Up)
            {
                macAddresses += nic.GetPhysicalAddress().ToString();
                break;
            }
        }
        return macAddresses;
    }

    public string fnGetIpAddress()
    {

        string strIpAddress;

        strIpAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (strIpAddress == null)

            strIpAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

        return strIpAddress;

    }



}
