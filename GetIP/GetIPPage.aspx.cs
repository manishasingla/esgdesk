﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Net;
using System.Xml;
using System.Management;
using System.Runtime.InteropServices;
using System.Net.NetworkInformation;

public partial class GetIPPage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        string strMacAdds  = fnGetMacAddress();
        string strIpaddress = fnGetIpAddress();

        lblMsg.Text = "Your IP is" + " " + strIpaddress +" ";

        lblMsg.Text += "and Mac ID is"+" "+ strMacAdds ;

       

    }
    private string fnGetMacAddress()
    {
        string macAddresses = "";

        foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
        {
            if (nic.OperationalStatus == OperationalStatus.Up)
            {
                macAddresses += nic.GetPhysicalAddress().ToString();
                break;
            }
        }
        return macAddresses;
    }

    public string fnGetIpAddress()
    {

        string strIpAddress;

        strIpAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (strIpAddress == null)

            strIpAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

        return strIpAddress;

        //string strHostName = "";

        //strHostName = System.Net.Dns.GetHostName();

        //IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(strHostName);

        //IPAddress[] addr = ipEntry.AddressList;

        //return addr[addr.Length - 1].ToString();

    }

}