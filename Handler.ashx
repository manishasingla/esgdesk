﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.Web;
using System.Data;
using System.Data.SqlClient;

public class Handler : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {

        try
        {
            context.Response.ContentType = "text/plain";
            
            UpdateData obj = new UpdateData();
            DataSet ds = new DataSet();
            ds = obj.GetData();

            if (ds.Tables[0].Rows.Count > 0)
            {
                context.Response.Write("WebsiteUrl,project_name" + "\n");
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    context.Response.Write(row["WebsiteUrl"].ToString() + "," + row["project_name"].ToString() + "\n");
                }
            }
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}