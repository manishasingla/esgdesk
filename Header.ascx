<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Header.ascx.cs" Inherits="Header" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%--<link rel="stylesheet" href="css/newmenuesg.css" type="text/css" media="screen" charset="utf-8" />--%>
<link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link href="SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/main.css" type="text/css" media="screen" charset="utf-8" />
<style type="text/css">
    <!
    -- .style2
    {
        color: #FFFFFF;
    }
    -- >
    
    
    
    .menu
        {
            height: 42px;
            position: relative;
            z-index: 100;
            background: #3E3F3D;
            float: left;
			margin-left: 15px;
            margin-right: 130px;
            color: #FFFFFF;
        }
        .menu ul
        {
            padding: 0;
            margin: 0;
            list-style-type: none;
			
        }
        .menu li
        {
            list-style: none;
            float: left;
            display: block;
            position: relative;
	-webkit-transition: opacity .25s ease .1s;
	-moz-transition: opacity .25s ease .1s;
	-o-transition: opacity .25s ease .1s;
	-ms-transition: opacity .25s ease .1s;
	transition: opacity .25s ease .1s;
        }
        .menu ul li a
        {
            display: block;
            padding: 0 14px;
            margin: 6px 0;
            line-height: 28px;
            text-decoration: none;
           border-left: 1px solid #3E3F3D;
           border-right: 1px solid #4F5058;
		   text-decoration: none !important;
            font-family: Helvetica, Arial, sans-serif;
            font-weight: bold;
            font-size: 12px;
            color: #f3f3f3 !important;
            text-shadow: 1px 1px 1px rgba(0,0,0,.6);
            -webkit-transition: color .2s ease-in-out;
            -moz-transition: color .2s ease-in-out;
            -o-transition: color .2s ease-in-out;
            -ms-transition: color .2s ease-in-out;
            transition: color .2s ease-in-out;
        }
        .menu ul li ul li a
        {
            width: 140px;
            padding: 0px 0 0px 40px;
            margin: 0;
            line-height: 30px;
            border: none;
            border-bottom: 1px solid #353539;
            height:30px;
			transition: color 0.2s ease-in-out 0s;
        }
		
	
        .menu a, .menu a:hover
        {
            display: block;
            font-size: 12px;
            text-decoration: none;
            color: #01ad55 !important;
            height: 30px;
			transition: color 0.2s ease-in-out 0s;
        }
        .menu ul ul
        {
            visibility: hidden;
            position: absolute;
            height: 0;
            top: 40px;
            left: 0;
            width: 179px;
            background: #3E3F3D;
           /* border: 1px solid #353539;*/
		
        }
        .menu ul ul li
        {
            background: #3E3F3D;
            text-align: left;
        }
        .menu ul ul li:hover
        {
            background: #454545;
            color: #01ad55 !important;
        }
        .menu ul ul ul
        {
            visibility: hidden;
            position: absolute;
            height: 0;
            top: 0;
            left: 146px;
            width: 182px;
        }
        .menu ul ul ul li
        {
            background: #3E3F3D;
            text-align: left;
        width:183px;
        }
        .menu ul ul ul li:hover
        {
            background: #454545;
            color: #01ad55 !important;
        }
        .menu table
        {
            position: absolute;
            top: 0;
            left: 0;
            border-collapse: collapse;
        }
        /* style the table so that it takes no ppart in the layout - required for IE to work */
        .menu ul ul a, .menu ul ul a:visited, .menu ul ul ul a, .menu ul ul ul a:visited
        {
            color: #fff;
            height: 40px;
			transition: color 0.2s ease-in-out 0s;
        }
        .menu a:hover, .menu ul ul li:hover
        {
        }
        .menu :hover > a, .menu ul ul :hover > a, .menu ul ul ul :hover > a
        {
        }
        .menu ul li:hover > ul, .menu ul a:hover ul, .menu ul ul li:hover ul
        {
            visibility: visible;
        }
		
		/* Icons */

.menu a.documents { background: url("../images/docs123.png") no-repeat scroll 6px center transparent; }
.menu a.messages { background: url("../images/arrow123.png") no-repeat 6px center; }
.menu a.taskmessage { background: url("../images/bubble123.png") no-repeat 6px center; }
.menu a.add_request { background: url("../images/add_request.png") no-repeat 6px center; }		
.menu a.password { background: url("../images/change_password.png") no-repeat 6px center; }		
.menu a.registration{ background: url("../images/edit_leave.png") no-repeat 6px center; }		
.menu a.new_request{ background: url("../images/request.png") no-repeat 6px center; }  
    
     </style>

<script src="http://www.google.com/jsapi"></script>

<script>    google.load("mootools", "1.2.1");</script>

<!-- Load the MenuMatic Class -->

<script src="js/MenuMatic_0.68.3.js" type="text/javascript" charset="utf-8"></script>

<!-- Create a MenuMatic Instance -->

<script type="text/javascript">
    window.addEvent('domready', function() {
        var myMenu = new MenuMatic();
    });		
</script>

<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>
<div class="divBorder">
    <div>
        <table width="0" cellpadding="0" cellspacing="0" style="width: 100%; background:#ffffff;">
            <tr>
                <td width="31%" rowspan="2" align="left" valign="middle" class="yellowtext1" style="padding-left: 20px">
                <img class="imagemargin" src="images/esg-desklogo.png" />
                    <span id="pageHeader">
<%--                    <asp:Label ID="lblTitle" runat="server" Text="ESG Desk - Exchange System" CssClass="firstword"></asp:Label>--%>
                    <asp:Label ID="lblTitle" runat="server" Text="Exchange System" CssClass="firstword"></asp:Label>
                    </span>
                </td>
          <td width="69%" align="right" valign="middle">
<div id="divHeader1" runat="server" align="right" class="style1_bg">
                        <a id="lnkLogin2" runat="server" href="../login.aspx"></a>
                        <asp:LinkButton ID="LinkButton1" runat="server" Visible="false" CausesValidation="false">Download</asp:LinkButton>
                        &nbsp;<a id="lnkLogin" runat="server" href="login.aspx"><img border="0" height="24"
                            src="images/clientlogin_icon.png" />&nbsp;Client Login</a>&nbsp;&nbsp;<a id="lnkAdminLogin" runat="server" href="admin/login.aspx">Support
                                    login</a>&nbsp;<span class="MenuBarHorizontal"> </span>&nbsp;<a id="lnkRegister"
                                        style="display: none;" runat="server" href="../register.aspx">Register</a>
                        <a id="RmtdeskLink" runat="server" href="http://www.logmein123.com" target="_blank">
                            Remote desktop</a>
                    </div>
                   <img src="images/support_icon.png"height="22"border="0" style="margin-right: 6px;"/><span class="style1_bg"> <a id="lnkLogin4" runat="server" href="../login.aspx"></a>
                   <a id="lnkUserName" runat="server" href="#" target="_blank" style="border-bottom:1px dotted #000000;"></a> |
                        <asp:Button ID="lnkLogout" runat="server" Text="Logout" CausesValidation="False"
                            OnClick="lnkLogout_Click" CssClass="blueBtns" />
                    </span>
                </td>
          </tr>
            <tr>
                <td height="28" align="right" valign="bottom">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td align="left" valign="middle">&nbsp;
                                
                            </td>
                            <td width="400" align="center" valign="bottom">
                             
                            </td>
                            <td width="120" align="left" valign="middle">
                                <a class="addthis_button" style="text-align: left;" href="http://www.addthis.com/bookmark.php?v=250&amp;pub=esatesolutions">
                                    <img src="http://s7.addthis.com/static/btn/lg-share-en.gif" alt="Bookmark and Share"
                                        width="125" height="16" align="right" style="border: 0px; margin-right:21px;" /></a>
                            </td>
                        </tr>
                    </table>
                    <a href="http://www.esgdesk.com//Download/ESG_Desk.application"></a>
                </td>
            </tr>
            <tr>
                <td align="left" height="6" valign="middle" bgcolor="#00a651">
                </td>
                <td align="right" height="6" valign="bottom" bgcolor="#00a651" style="height: 5px;">
                </td>
            </tr>
        </table>
    </div>
</div>
<div style="text-align: left; vertical-align: bottom; padding: 0px;" id="divHeader2"
    runat="server">
    <!-- /login -->
    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="background: #3e3f3d;">
        <tr>
            <td width="75%" align="left" valign="middle" bgcolor="#3e3f3d">

                <script type="text/javascript" language="javascript">

                    function Gotorequest() {


                        document.getElementById('Header_myRequests').click();

                    }
                </script>
				 <div class="menu">
                <ul>
                    <li><a href="#" id="myRequestslink" runat="server" style="background-color: Transparent"
                        onclick="Gotorequest();">Request</a>
                        <ul>
                            <li>
                                <asp:LinkButton ID="myRequests" runat="server" Text="My requests" Font-Underline="false"
                                    CausesValidation="False" OnClick="myRequests_Click" CssClass="new_request"></asp:LinkButton>
                            </li>
                            <li><a href="changerequest.aspx" class="add_request">Add new request</a> </li>
                        </ul>
                    </li>
                    <li><a href="#" style="background-color: Transparent">My account</a>
                        <ul>
                            <li><a href="edit_registration.aspx" id="EditRegistration" runat="server" class="registration">Edit registration</a></li>
                            <li><a href="change_password.aspx" id="ChangePassword" runat="server" class="password">Change password</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <asp:LinkButton ID="CRNotes" runat="server" CausesValidation="False" Font-Underline="true"
                            OnClick="CRNotes_Click" Text="" Visible="false"></asp:LinkButton>
                    </li>
                    <li>
                        <asp:Label ID="LblCompany" runat="server" Text="" Visible="false"></asp:Label>
                    </li>
                    <li>
                        <asp:LinkButton ID="Chat" runat="server" CausesValidation="False" Font-Underline="true"
                            Text="Chat with support staff">
                            <%--<a href="#"style="background-color:Transparent">Chat with support staff</a>--%>
                            </asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="LnkRmtDsktp" runat="server" CausesValidation="False" Font-Underline="true"
                            Text="Remote desktop session"></asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="LinkSupport" runat="server" Text="Support" Font-Underline="true"
                            CausesValidation="False"></asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="LinkClientNote" runat="server" Text="Client notes" Font-Underline="true"
                          CausesValidation="False"></asp:LinkButton>
                    </li>
                    <li><a id="CompNotesIcon" runat="server" title="Click to view notes" style="background-color:Transparent;
                        cursor:pointer">Notification:</a></li>

                         <li>
                        <asp:LinkButton ID="LinkEmployeeList" runat="server" Text="Employee list" Font-Underline="true"
                          CausesValidation="False" onclick="LinkEmployeeList_Click" Enabled="False"></asp:LinkButton>
                    </li>
                </ul>
                </div>
                <span id="SpnSupportlnk" runat="server" style="font-size: 11px;"><span class="style2">
                    Please call support to enable these links (subscription based).Click <a onclick="window.open('http://www.request.estatesolutions.eu/Support_and_maintenance_contracts.pdf','','')"
                        style="cursor: pointer">here</a> to download packages details</span>.</span>
                <span style="display: none"><a id="CompNotesIcongjkgj" runat="server" title="Click to view notes"
                    style="border: none; font-size: 16px; color: Blue; padding-left: 5px; text-decoration: none;
                    cursor: pointer;"></a></span>
            </td>
            <td width="25%" align="center" valign="middle" bgcolor="#3e3f3d">
                <table border="0" cellpadding="0" cellspacing="0" style="margin-right: -36px;">
                    <tr>
                        <td>
                            <span style="color:#FFFFFF;" class="style2">Request Id:&nbsp;</span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtRequestId" runat="server" Width="200px"></asp:TextBox>
                            &nbsp;
                        </td>
                        <td>
                            <asp:Button ID="btnGo" runat="server" Text="Go" CausesValidation="False" OnClick="btnGo_Click"
                                CssClass="goBtn" />
                        </td>
                    </tr>
                </table>
                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"
                    TargetControlID="txtRequestId">
                </cc1:FilteredTextBoxExtender>
            </td>
        </tr>
    </table>
</div>
