using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.Drawing.Drawing2D;
using System.IO;
using System.Net;
using System.Text;


public partial class Header : System.Web.UI.UserControl
{
    string sql = "";
    static string companyname = "";
    protected void Page_Load(object sender, EventArgs e)
    {
       

        if (Session["clientid"] == null && Session["username"] == null)
        {
            lnkLogin.Visible = true;
            lnkAdminLogin.Visible = true;
            lnkLogout.Visible = false;
            lnkRegister.Visible = true;
            divHeader2.Visible = false;
           //// lblTitle.Text = "Exchange";
            //lblTitle.ForeColor = Color.Red;
            lnkUserName.InnerHtml = "";
            RmtdeskLink.Visible = true;
           // Download.Visible = false;
        }
        else
        {
            lnkUserName.InnerHtml = "Logged in as client: " + Session["username"].ToString();
          ////  lblTitle.Text = "Exchange";
           /// lblTitle.ForeColor = Color.Red;
            lnkLogin.Visible = false;
            lnkAdminLogin.Visible = false;
            lnkLogout.Visible = true;
            lnkRegister.Visible = false;
            divHeader2.Visible = true;
            RmtdeskLink.Visible = false;
          ///  Download.Visible = true;

            if (Session["ESCRMuser"] != null)
            {
                if (Session["ESCRMuser"].ToString() == "true")
                {
                    EditRegistration.Visible = false;
                    ChangePassword.Visible = false;
                }
                else
                {
                    EditRegistration.Visible = true;
                    ChangePassword.Visible = true;
                }
            }

           /// DataSet dsUserDetails = DatabaseHelper.getUserDetails(Session["clientid"].ToString(), Session["username"].ToString());
            try
            {

                string strAllowchat = "select [AllowChat] from [NonesCRMusers]  where UserName = '" + Session["username"].ToString() + "' and ClientId = " + Session["clientid"].ToString() + "";
                object objAllowchat = DatabaseHelper.executeScalar(strAllowchat);
            if (objAllowchat != null)
            {
                if (objAllowchat.ToString() == "True")
                {
                    DataSet ds = DatabaseHelper.getcompanyname("select Companyname from NonesCRMusers where UserName = '" + Session["username"].ToString() + "' and ClientId = " + Session["clientid"].ToString() + "");

                    //Chat.Attributes.Add("onclick", "window.open('http://livechat.boldchat.com/aid/2178106990504207724/bc.chat?cwdid=848434019943815447&amp;wdid=3507451889334081925&amp;vr=&amp;vn=&amp;vi=&amp;ve=&amp;vp=&amp;iq=&amp;url=','','scrollbars=yes,width=500,height=500');");
                    //Chat.Attributes.Add("onclick", "window.open('http://livechat.boldchat.com/aid/2178106990504207724/bc.chat?cwdid=848434019943815447&amp;wdid=3507451889334081925&vr=&vn=Abhishek&vi=&ve=&vp=&iq=&url=','','scrollbars=yes,width=500,height=500');");
                    if (ds != null && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        Chat.Attributes.Add("onclick", "window.open('http://livechat.boldchat.com/aid/2178106990504207724/bc.chat?cwdid=848434019943815447&amp;wdid=3507451889334081925&vr=&vn=" + Session["username"].ToString() + " (" + ds.Tables[0].Rows[0]["Companyname"].ToString() + ") " + "&vi=&ve=&vp=&iq=&url=','','scrollbars=yes,width=500,height=500');");
                    }
                    else
                    {
                        Chat.Attributes.Add("onclick", "window.open('http://livechat.boldchat.com/aid/2178106990504207724/bc.chat?cwdid=848434019943815447&amp;wdid=3507451889334081925&vr=&vn=" + Session["username"].ToString() + "&vi=&ve=&vp=&iq=&url=','','scrollbars=yes,width=500,height=500');");
                    }
                    SpnSupportlnk.Visible = false;
                }
                else
                {
                    Chat.Attributes.Add("onclick", " alert('Please contact support as your subscription level does not allow Support chat.');");
                    SpnSupportlnk.Visible = false;
                }
            }
            else
            {
                Chat.Attributes.Add("onclick", " alert('Please contact support as your subscription level does not allow Support chat.');");
                SpnSupportlnk.Visible = false;
            }
            }
            catch { }

          
            try
            {
                string strRmtDsktp = "select [AllowRemotDesktop] from [NonesCRMusers]  where UserName = '" + Session["username"].ToString() + "' and ClientId = " + Session["clientid"].ToString() + "";
                object objRmtDsktop = DatabaseHelper.executeScalar(strRmtDsktp);
                if (objRmtDsktop != null)
                {
                    if (objRmtDsktop.ToString() == "True")
                    {

                        LnkRmtDsktp.Attributes.Add("onclick", "window.open('Allow_Remote_Desktop.aspx','','scrollbars=no,width=500,height=350');");
                        SpnSupportlnk.Visible = false;
                    }
                    else
                    {
                        LnkRmtDsktp.Attributes.Add("onclick", "return alert('Please contact support as your subscription level does not allow Remote desktop assistance.');");
                        SpnSupportlnk.Visible = false;

                    }
                }
                else
                {
                    LnkRmtDsktp.Attributes.Add("onclick", "return alert('Please contact support as your subscription level does not allow Remote desktop assistance.');");
                    SpnSupportlnk.Visible = false;
                }

            }
            catch { }

            try
            {
                string strSupportLink = "select [SupportUrl] from [NonesCRMusers]  where UserName = '" + Session["username"].ToString() + "' and ClientId = " + Session["clientid"].ToString() + "";
                object objSupportLink = DatabaseHelper.executeScalar(strSupportLink);
                if (objSupportLink.ToString() != "")
                {
                    LinkSupport.Visible = true;
                    LinkSupport.Text = "Support";
                    LinkSupport.ForeColor = Color.Red;
                    string strSLink = objSupportLink.ToString();

                    if (strSLink.Contains("http://") == true)
                    {
                        LinkSupport.Attributes.Add("onclick", "window.open('" + strSLink.ToString() + "','',',,');");
                    }
                    else
                    {
                        LinkSupport.Attributes.Add("onclick", "window.open('" + "http://" + strSLink.ToString() + "','',',,');");
                    }
                   
                }
                else
                {
                    LinkSupport.Visible = false;
                }

            }
            catch { }

            //=================================================
              //LinkClientNote.PostBackUrl="http://localhost:3182/EsgDesk/ClientNote.aspx";
            LinkClientNote.PostBackUrl = "http://www.esgdesk.com/ClientNote.aspx";
            //=================================================
           

            try
            {
            string Clientcomp = "select [CompanyName] from [NonesCRMusers]  where UserName = '" + Session["username"].ToString() + "' and ClientId = " + Session["clientid"].ToString() + "";
            object objClientComp = DatabaseHelper.executeScalar(Clientcomp);
            if (objClientComp.ToString() != "")
            {
               
                companyname = objClientComp.ToString();

                string sqlCRnote = "select count(*) from [Company_notes]  where Company_Name = '" + objClientComp.ToString() + "' and Note_Type ='Client'  and Allow_Notes='True'";
                int objCRNoteResult = int.Parse(DatabaseHelper.executeScalar(sqlCRnote).ToString());



                if (objCRNoteResult > 0)
                {
                    CRNotes.ForeColor = Color.Red;
                    CRNotes.Text = "Notes";
                    LblCompany.ForeColor = Color.Red;
                    LblCompany.Text = " for  " + objClientComp.ToString();

                    CompNotesIcon.InnerHtml =  "Notification: " + objClientComp.ToString();
                    Session["TaskComp"] = companyname;
                    CompNotesIcon.Attributes.Add("onclick", "window.open('CRNotes.aspx','','status=no,scrollbars=yes,position=center,resizable=yes,width=screen.width,height=screen.height');");
                   

                }
                else
                {
                    CompNotesIcon.Attributes.Add("onclick", " alert('Client notes not available');");
                    CRNotes.ForeColor = Color.Blue;
                    CRNotes.Text = "";
                    LblCompany.Text = "";

                }

            }

            }
            catch { }
        }
    }
    protected void CRNotes_Click(object sender, EventArgs e)
    {
        Session["TaskComp"] = companyname;
        Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> window.open('CRNotes.aspx','','status=no,scrollbars=yes,position=center,resizable=yes,width=screen.width,height=screen.height'); </script>");
    }

    protected void lnkRegister_Click(object sender, EventArgs e)
    {
        Response.Redirect("register.aspx", false);
    }
    protected void lnkEditRegistration_Click(object sender, EventArgs e)
    {
        Response.Redirect("edit_registration.aspx",false);
    }
    protected void lnkLogout_Click(object sender, EventArgs e)
    {
        //Check iIf the cookies with name PBLOGIN exist on user's machine

        Session.Remove("clientid");
     
        Session.Remove("username");
     
        Session["message"] = "You have been logged out successfully.";
        Response.Redirect("confirmation.aspx", false);

  }
    protected void btnGo_Click(object sender, EventArgs e)
    {
        Response.Redirect("edit_request.aspx?reqid=" + txtRequestId.Text.Trim(), false);
    }
    protected void myRequests_Click(object sender, EventArgs e)
    {
        Session["filterunreadCR"] = "";
        Session["filterunreadCR"] = null;
      Session["Status"] = "";
        Session["Status"] = null;
      
      Response.Redirect("ClientRequests.aspx", true);
         
    }

    ////////protected void Download_Click(object sender, EventArgs e)
    ////////{

    ////////    string DesktopDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);

    ////////    using (StreamWriter ShortcutWriter = new StreamWriter(DesktopDirectory + "\\Change_Request_System.url"))
    ////////    {
    ////////        ShortcutWriter.WriteLine("[InternetShortcut]");
    ////////        ShortcutWriter.WriteLine("URL=www.request.estatesolutions.eu");
    ////////        ShortcutWriter.Flush();
    ////////    }

    ////////}
    protected void LinkEmployeeList_Click(object sender, EventArgs e)
    {
       // Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> window.open('EmployeeAttendance1.aspx','','status=no,scrollbars=yes,position=center,resizable=yes,width=screen.width,height=screen.height'); </script>");

        Server.Transfer("Employee-Attendance.aspx");
    }
}
