using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class ProjectDetailCR : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["clientid"] == null || Session["username"] == null || Session["clientid"].ToString() == "" || Session["username"].ToString() == "")
            {
                Session["returnUrl"] = "requests.aspx";
                Response.Redirect("login.aspx");
                return;
            }

            if (!DatabaseHelper.isAdmin(Session["username"].ToString()))
            {
                idfileupload.Visible = false;
                hideForEmp.Visible = false;
                DataGrid1.Columns[7].Visible = false;
            }
            else
            {
                idfileupload.Visible = false;
                hideForEmp.Visible = false;
                DataGrid1.Columns[7].Visible = true;
            }
            if (Server.UrlDecode(Request.QueryString["projectname"]) != null && Request.QueryString["folderid"] != null && Request.QueryString["solutionid"] != null)
            {
                try
                {
                    getcategory(Server.UrlDecode(Request.QueryString["projectname"].ToString()), Request.QueryString["folderid"].ToString(), Request.QueryString["solutionid"].ToString());
                    BindData();
                }
                catch (Exception ex)
                {
                    DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectdetailCR.aspx:2 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Page Load Outer 2");
                }

                list.HRef = "ProjectNoteListCR.aspx?projectname=" + Server.UrlEncode(Request.QueryString["projectname"].ToString()) + "&folderid=" + Request.QueryString["folderid"].ToString();
                //====================================================================================
                if (!Page.IsPostBack)
                {
                    string SelectSql = "";
                    DataSet dsonload = null;
                    SelectSql = @"Select NoteStatus from NotesStatus where IsProject='False' and Note_Id='" + Convert.ToInt32(Request.QueryString["solutionid"].ToString()) + " ' and Folder_Id='" + Convert.ToInt32(Request.QueryString["folderid"].ToString()) + " '  and Project_Name='" + Server.UrlDecode(Request.QueryString["projectname"].ToString()) + " ' and UserName='" + Session["username"] + "'";
                    try
                    {
                        dsonload = DatabaseHelper.getDataset(SelectSql);
                    }
                    catch (Exception ex)
                    {
                        DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectdetailCR.aspx:2 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Page Load Outer 3" + SelectSql);
                    }
                    if (dsonload != null && dsonload.Tables.Count > 0 && dsonload.Tables[0].Rows.Count > 0)
                    {
                        idreadNote.Checked = Convert.ToBoolean(dsonload.Tables[0].Rows[0]["NoteStatus"]);
                    }
                }
                //====================================================================================

            }
            else
            {
                Response.Redirect("ProjectNoteListCR.aspx");
            }
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectdetailCR.aspx:1 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "Page Load Outer 1,Project Name:-" + Request.QueryString["projectname"] + " FolderId:-" + Request.QueryString["folderid"] + "Solution Id:-" + Request.QueryString["solutionid"]);
        }
    }

    void getcategory(string category_id, string folder_id, string solution_id)
    {

        string sqlfoldername = @"select foldername from Project_Folder where IsProject='False' and Project_Name = '" + category_id + "'  and folder_id  =" + folder_id;
        object FN = null;
        try
        {
            FN = DatabaseHelper.executeScalar(sqlfoldername);
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectdetailCR.aspx:1 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "getcategory()" + sqlfoldername);
        }

        if (FN != null)
        {
            lblPfoldername.Text = FN.ToString();
        }

        string sqlcount = @"select foldername,folder_id,Project_Name from  Project_Folder where IsProject='False' and Project_Name  = '" + category_id + "' order by Folder_Id";
        DataSet dscount = null;
        try
        {
            dscount = DatabaseHelper.getallstatus(sqlcount);
        }
        catch (Exception ex)
        {
            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectdetailCR.aspx:2 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "getcategory()" + sqlcount);
        }

        if (dscount != null)
        {
            if (dscount.Tables.Count > 0 && dscount.Tables[0].Rows.Count > 0)
            {

                editsolution.HRef = "AddProjectCR.aspx?projectname=" + Server.UrlEncode(category_id) + "&folderid=" + folder_id + "&solutionid=" + solution_id;

                //string sqlsub = @"select  COUNT(*) as counts,Project_Folder.Foldername,Project_Folder.folder_id, Project_Folder.Project_Name  from Project_Folder inner join ProjectNotes on Project_Folder.folder_id = ProjectNotes.folder_id
//inner join NotesStatus on  Project_Folder.Folder_Id = NotesStatus.Folder_Id 
//where NotesStatus.UserName='" + Session["username"] + "' and NotesStatus.IsProject='False' and Project_Folder.IsProject='False' and ProjectNotes.IsProject='False' and Project_Folder.Project_Name  = '" + category_id + "' group by Project_Folder.folder_id,Project_Folder.Foldername ,Project_Folder.Project_Name order by Folder_Id ; select Row_Number()OVER(order by Note_Id) as RowNumber,* from ProjectNotes where folder_id= " + folder_id + " and Note_Id=" + solution_id;

                string sqlsub = @"select  COUNT(*) as counts,Project_Folder.Foldername,Project_Folder.folder_id, Project_Folder.Project_Name  from Project_Folder inner join ProjectNotes on Project_Folder.folder_id = ProjectNotes.folder_id
where Project_Folder.IsProject='False' and ProjectNotes.IsProject='False' and Project_Folder.Project_Name  = '" + category_id + "' group by Project_Folder.folder_id,Project_Folder.Foldername ,Project_Folder.Project_Name order by Folder_Id ; select Row_Number()OVER(order by Note_Id) as RowNumber,* from ProjectNotes where folder_id= " + folder_id + " and Note_Id=" + solution_id;
                
                DataSet dssub = null;
                try
                {
                    dssub = DatabaseHelper.getallstatus(sqlsub);
                }
                catch (Exception ex)
                {
                    DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectdetailCR.aspx:3 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "getcategory()" + sqlsub);
                }

                if (dscount.Tables[0].Rows.Count == dssub.Tables[0].Rows.Count)
                {

                    if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0 && dssub.Tables[1].Rows.Count > 0)
                    {
                        //==============================================
                        dssub = setFolderStatus(dssub);
                        //==============================================
                        Folderlstview.DataSource = dssub.Tables[0];
                        Folderlstview.DataBind();
                        lblFN.Text = dssub.Tables[1].Rows[0]["NoteName"].ToString().Replace("|@", "'");
                        lbldate.Text = dssub.Tables[1].Rows[0]["reported_date"].ToString();
                        solutiondesc.InnerHtml = dssub.Tables[1].Rows[0]["NoteDescription"].ToString().Replace("|@", "'");

                    }
                    else
                    {
                        Response.Redirect("Solution.aspx");
                    }
                }
                else
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Foldername");
                    dt.Columns.Add("folder_id");
                    dt.Columns.Add("Project_Name");
                    dt.Columns.Add("counts");
                    DataSet a = new DataSet();
                    for (int j = 0; j < dscount.Tables[0].Rows.Count; j++)
                    {
//                        sqlsub = @"select  COUNT(*) as counts,Project_Folder.Foldername,Project_Folder.folder_id, Project_Folder.Project_Name  from Project_Folder inner join ProjectNotes on Project_Folder.folder_id = ProjectNotes.folder_id
//inner join NotesStatus on  Project_Folder.Folder_Id = NotesStatus.Folder_Id
//where NotesStatus.UserName='" + Session["username"] + "' and NotesStatus.IsProject='False' and Project_Folder.IsProject='False' and ProjectNotes.IsProject='False' and ProjectNotes.Project_Name  = '" + dscount.Tables[0].Rows[j]["Project_Name"] + "' and ProjectNotes.folder_id  =" + dscount.Tables[0].Rows[j]["folder_id"] + " group by Project_Folder.folder_id,Project_Folder.Foldername ,Project_Folder.Project_Name order by Folder_Id ; select Row_Number()OVER(order by Note_Id) as RowNumber,* from ProjectNotes where ProjectNotes.IsProject='False' and folder_id= " + folder_id + " and Note_Id=" + solution_id;
                        sqlsub = @"select  COUNT(*) as counts,Project_Folder.Foldername,Project_Folder.folder_id, Project_Folder.Project_Name  from Project_Folder inner join ProjectNotes on Project_Folder.folder_id = ProjectNotes.folder_id
where  Project_Folder.IsProject='False' and ProjectNotes.IsProject='False' and ProjectNotes.Project_Name  = '" + dscount.Tables[0].Rows[j]["Project_Name"] + "' and ProjectNotes.folder_id  =" + dscount.Tables[0].Rows[j]["folder_id"] + " group by Project_Folder.folder_id,Project_Folder.Foldername ,Project_Folder.Project_Name order by Folder_Id ; select Row_Number()OVER(order by Note_Id) as RowNumber,* from ProjectNotes where ProjectNotes.IsProject='False' and folder_id= " + folder_id + " and Note_Id=" + solution_id;

                        try
                        {
                            dssub = DatabaseHelper.getallstatus(sqlsub);
                        }
                        catch (Exception ex)
                        {
                            DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectdetailCR.aspx:4 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "getcategory()" + sqlsub);
                        }
                        if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0)
                        {

                            DataRow dr = dssub.Tables[0].Rows[0];

                            dt.ImportRow(dssub.Tables[0].Rows[0]);
                            if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0 && dssub.Tables[1].Rows.Count > 0)
                            {

                                lblFN.Text = dssub.Tables[1].Rows[0]["NoteName"].ToString().Replace("|@", "'");
                                lbldate.Text = dssub.Tables[1].Rows[0]["reported_date"].ToString();
                                solutiondesc.InnerHtml = dssub.Tables[1].Rows[0]["NoteDescription"].ToString().Replace("|@", "'");

                            }
                            else
                            {
                                Response.Redirect("Solution.aspx");
                            }
                        }
                        else
                        {
                            //                    sqlsub = @"select foldername,folder_id,Category_id from Category_Folder
                            //                                 where Category_id  = " + dscount.Tables[0].Rows[j]["Category_id"] + " and folder_id =" + dscount.Tables[0].Rows[j]["folder_id"];
                            sqlsub = @"select foldername,folder_id,Project_Name from Project_Folder
where Project_Folder.IsProject='False' and Project_Name  = '" + dscount.Tables[0].Rows[j]["Project_Name"] + "' and folder_id =" + dscount.Tables[0].Rows[j]["folder_id"] + " order by Folder_Id";
                            try
                            {
                                dssub = DatabaseHelper.getallstatus(sqlsub);
                            }
                            catch (Exception ex)
                            {
                                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectdetailCR.aspx:4 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "getcategory()" + sqlsub);
                            }
                            if (dssub.Tables != null && dssub.Tables[0].Rows.Count > 0)
                            {
                                DataColumn dc = new DataColumn("Counts");
                                dc.DefaultValue = 0;
                                dssub.Tables[0].Columns.Add(dc);
                                DataRow dr = dssub.Tables[0].Rows[0];
                                //a.Tables[0].Rows.Add(dr);
                                dt.ImportRow(dssub.Tables[0].Rows[0]);
                            }

                        }
                    }
                    //============================
                    dt = setFolderStatusdt(dt);
                    //============================
                    Folderlstview.DataSource = dt;
                    Folderlstview.DataBind();
                }
            }
        }
    }

    public DataTable setFolderStatusdt(DataTable dtFolderStatus)
    {
        try
        {
            string Addstatusdt = "", allstatusdt = "";
            DataTable dtFolderstatus = null, Allstatusdt = null;
            DataSet dsfol = null, dsalstatus = null;
            dtFolderStatus.Columns.Add("AddCategoryStatus");
            for (int ichk = 0; ichk < dtFolderStatus.Rows.Count; ichk++)
            {
                Addstatusdt = "Select * from NotesStatus where IsProject='False' and Folder_Id='" + Convert.ToInt32(dtFolderStatus.Rows[ichk]["Folder_Id"].ToString()) + "'  and Project_Name='" + dtFolderStatus.Rows[ichk]["Project_Name"].ToString() + "' and UserName='" + Session["username"] + "'";
                //Addstatusdt = "Select * from NotesStatus where IsProject='False' and Folder_Id='" + Convert.ToInt32(dtFolderStatus.Rows[ichk]["Folder_Id"].ToString()) + "'  and Project_Name='" + dtFolderStatus.Rows[ichk]["Project_Name"].ToString() + "'";
                try
                {
                    dsfol = DatabaseHelper.getDataset(Addstatusdt);
                }
                catch (Exception ex)
                {
                    DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectdetailCR.aspx:1 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "setFolderStatusdt()" + Addstatusdt);
                }

                dtFolderstatus = dsfol.Tables[0];

                allstatusdt = "Select * from NotesStatus where IsProject='False' and Folder_Id='" + Convert.ToInt32(dtFolderStatus.Rows[ichk]["Folder_Id"].ToString()) + "'  and Project_Name='" + dtFolderStatus.Rows[ichk]["Project_Name"].ToString() + "' and UserName='" + Session["username"] + "' and NoteStatus='True'";
                try
                {
                    dsalstatus = DatabaseHelper.getDataset(allstatusdt);
                }
                catch (Exception ex)
                {
                    DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectdetailCR.aspx:1 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "setFolderStatusdt()" + allstatusdt);
                }

                Allstatusdt = dsalstatus.Tables[0];


                if ((dtFolderstatus.Rows.Count == Allstatusdt.Rows.Count))
                {
                    if (dtFolderstatus.Rows.Count >= 1)
                    {
                        dtFolderStatus.Rows[ichk]["AddCategoryStatus"] = Convert.ToBoolean(true);
                    }
                    else
                    {
                        dtFolderStatus.Rows[ichk]["AddCategoryStatus"] = Convert.ToBoolean(false);
                    }
                }
                else
                {
                    dtFolderStatus.Rows[ichk]["AddCategoryStatus"] = Convert.ToBoolean(false);
                }

            }
            return dtFolderStatus;
        }
        catch (Exception ex)
        {

        }
        return dtFolderStatus;
    }
    public DataSet setFolderStatus(DataSet dsFolderStatus)
    {
        try
        {
            string Addstatus = "", allstatus = "";
            DataSet dsFolderstatus = null, Allstatusds = null;
            dsFolderStatus.Tables[0].Columns.Add("AddCategoryStatus");
            for (int ichk = 0; ichk < dsFolderStatus.Tables[0].Rows.Count; ichk++)
            {
                Addstatus = "Select * from NotesStatus where IsProject='False' and Folder_Id='" + Convert.ToInt32(dsFolderStatus.Tables[0].Rows[ichk]["Folder_Id"].ToString()) + "'  and Project_Name='" + dsFolderStatus.Tables[0].Rows[ichk]["Project_Name"].ToString() + "' and UserName='" + Session["username"] + "'";
                try
                {
                    dsFolderstatus = DatabaseHelper.getDataset(Addstatus);

                }
                catch (Exception ex)
                {
                    DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectdetailCR.aspx:1 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "setFolderStatus()" + Addstatus);
                }

                allstatus = "Select * from NotesStatus where IsProject='False' and Folder_Id='" + Convert.ToInt32(dsFolderStatus.Tables[0].Rows[ichk]["Folder_Id"].ToString()) + "'  and Project_Name='" + dsFolderStatus.Tables[0].Rows[ichk]["Project_Name"].ToString() + "' and UserName='" + Session["username"] + "' and NoteStatus='True'";
                try
                {
                    Allstatusds = DatabaseHelper.getDataset(allstatus);
                }
                catch (Exception ex)
                {
                    DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectdetailCR.aspx:2 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "setFolderStatus()" + allstatus);
                }


                if (dsFolderstatus.Tables[0].Rows.Count == Allstatusds.Tables[0].Rows.Count)
                {
                    if (dsFolderstatus.Tables[0].Rows.Count >= 1)
                    {
                        dsFolderStatus.Tables[0].Rows[ichk]["AddCategoryStatus"] = true;
                    }
                    else
                    {

                        dsFolderStatus.Tables[0].Rows[ichk]["AddCategoryStatus"] = false;
                    }
                }
                else
                {
                    dsFolderStatus.Tables[0].Rows[ichk]["AddCategoryStatus"] = false;
                }

            }
            return dsFolderStatus;
        }
        catch (Exception ex)
        {

        }
        return dsFolderStatus;
    }


    protected void idreadNote_CheckedChanged(object sender, EventArgs e)
    {
        if (Server.UrlDecode(Request.QueryString["projectname"]) != null && Request.QueryString["folderid"] != null && Request.QueryString["solutionid"] != null)
        {
            string SqlInsert = "", UpdateSql = "", DeleteSql = "", SelectSql = "", UpdateSqlUchk = "";
            DataSet dsselect = null;
            if (idreadNote.Checked)
            {

                SelectSql = @"Select * from NotesStatus where IsProject='False' and  Note_Id='" + Convert.ToInt32(Request.QueryString["solutionid"].ToString()) + " ' and Folder_Id='" + Convert.ToInt32(Request.QueryString["folderid"].ToString()) + " '  and Project_Name='" + Request.QueryString["projectname"].ToString() + " ' and UserName='" + Session["username"] + "'";
                try
                {
                    dsselect = DatabaseHelper.getDataset(SelectSql);
                }
                catch (Exception ex)
                {
                    DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectdetailCR.aspx:1 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "idreadNote_CheckedChanged" + SelectSql);
                }


                if (dsselect != null && dsselect.Tables.Count > 0 && dsselect.Tables[0].Rows.Count > 0)
                {
                    UpdateSql = @"Update NotesStatus set NoteStatus='" + idreadNote.Checked + "' where IsProject='False' and Note_Id='" + Convert.ToInt32(Request.QueryString["solutionid"].ToString()) + " ' and Folder_Id='" + Convert.ToInt32(Request.QueryString["folderid"].ToString()) + " ' and Project_Name= '" + Request.QueryString["projectname"].ToString() + " ' and UserName='" + Session["username"] + "'";
                    int insertvalue = 0;
                    try
                    {
                        insertvalue = DatabaseHelper.executeNonQuery(UpdateSql);
                    }
                    catch (Exception ex)
                    {
                        DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectdetailCR.aspx:2 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "idreadNote_CheckedChanged" + UpdateSql);
                    }

                }
                else
                {
                    SqlInsert = @"insert into NotesStatus(Note_Id,Folder_Id,Project_Name,UserName,NoteStatus,IsProject) 
                  values('" + Convert.ToInt32(Request.QueryString["solutionid"].ToString()) + " ','" + Convert.ToInt32(Request.QueryString["folderid"].ToString()) + " ','" + Request.QueryString["projectname"].ToString() + " ','" + Session["username"] + "','" + idreadNote.Checked + "','False')";
                    int insertvalue = 0;
                    try
                    {
                        insertvalue = DatabaseHelper.executeNonQuery(SqlInsert);
                    }
                    catch (Exception ex)
                    {
                        DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectdetailCR.aspx:3 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "idreadNote_CheckedChanged" + SqlInsert);
                    }
                }
            }
            else
            {
                UpdateSqlUchk = @"Update NotesStatus set NoteStatus='" + idreadNote.Checked + "' where IsProject='False' and Note_Id='" + Convert.ToInt32(Request.QueryString["solutionid"].ToString()) + " ' and Folder_Id='" + Convert.ToInt32(Request.QueryString["folderid"].ToString()) + " ' and Project_Name= '" + Request.QueryString["projectname"].ToString() + " ' and UserName='" + Session["username"] + "'";
                try
                {
                    int insertUpdate = DatabaseHelper.executeNonQuery(UpdateSqlUchk);
                }
                catch (Exception ex)
                {
                    DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectdetailCR.aspx:3 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "idreadNote_CheckedChanged" + UpdateSqlUchk);
                }

            }
            getcategory(Server.UrlDecode(Request.QueryString["projectname"]), Request.QueryString["folderid"], Request.QueryString["solutionid"]);
        }
    }

    protected void OnItemDataBound_Folderlstview(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            try
            {
                ListViewDataItem dataitem = (ListViewDataItem)e.Item;
                int policyid = Convert.ToInt32(DataBinder.Eval(dataitem.DataItem, "counts").ToString());
                if (policyid > 0)
                {
                    System.Web.UI.HtmlControls.HtmlControl cell = (System.Web.UI.HtmlControls.HtmlControl)e.Item.FindControl("ChangeColor");

                    cell.Style.Add("color", "Green");

                }
            }
            catch (Exception ex)
            {
                DatabaseHelper.SendEmailList("avinashj@esolutionsgroup.co.uk", "Error at ProjectdetailCR.aspx:1 " + Session["username"] + "DateTime:-> : " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n" + "OnItemDataBound_Folderlstview()");
            }
        }
    }


    protected void btnsave_Click(object sender, EventArgs e)
    {
        string Pkey = "";
        string uploadPath1 = Server.MapPath(Request.ApplicationPath + "\\Attachment\\backup");
        if (System.IO.Directory.Exists(uploadPath1))
        {
            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(uploadPath1);
            Pkey = System.Guid.NewGuid().ToString();
            //======================================================================================================================
            //object obj = DatabaseHelper.insertAttachment(RequestId, Pkey, idupload.FileName, "ProjectNote", Session["Admin"].ToString());
            DatabaseHelper.insertNoteAttachmentC(Server.UrlDecode(Request.QueryString["projectname"]), Convert.ToInt32(Request.QueryString["folderid"]), Convert.ToInt32(Request.QueryString["solutionid"]), Pkey, idupload.FileName.ToString(), txtdesc.Text.ToString(), Session["Admin"].ToString());
            //======================================================================================================================
            idupload.SaveAs(uploadPath1 + "\\" + Pkey + "_" + idupload.FileName);
            txtdesc.Text = "";
            BindData();
        }
    }
    protected void Linkedit_click(object sender, EventArgs e)
    {
        string Fullfilename = "", originalfilename = "";
        LinkButton lnkButton = sender as LinkButton;
        DataGridItem row = (DataGridItem)lnkButton.NamingContainer;
        DataSet dsforfilename = DatabaseHelper.getNoteAllfilenameC(Convert.ToInt32(row.Cells[0].Text.ToString()));
        if (dsforfilename != null && dsforfilename.Tables.Count > 0 && dsforfilename.Tables[0].Rows.Count > 0)
        {
            Fullfilename = dsforfilename.Tables[0].Rows[0]["FileName1"].ToString();
            originalfilename = dsforfilename.Tables[0].Rows[0]["FileName"].ToString().Replace(" ", "_");
            string path = Server.MapPath(Request.ApplicationPath + "\\Attachment\\backup");

            path += "\\" + Fullfilename;
            System.IO.FileInfo file = new System.IO.FileInfo(path);
            if (file.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + originalfilename);
                Response.AddHeader("Content-Length", file.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(file.FullName);
                Response.End();
            }
        }
    }
    protected void LinkDelete_click(object sender, EventArgs e)
    {
        if (DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            LinkButton lnkButton = sender as LinkButton;
            DataGridItem row = (DataGridItem)lnkButton.NamingContainer;
            object objResult = DatabaseHelper.deleteNoteAttachmentC(Convert.ToInt32(row.Cells[0].Text.ToString()));

            if (objResult.ToString() != "0")
            {
                string uploadPath1 = Server.MapPath(Request.ApplicationPath + "\\Attachment\\backup");
                uploadPath1 = uploadPath1 + "\\" + row.Cells[2].Text.ToString();
                try
                {
                    if (System.IO.File.Exists(uploadPath1))
                    {
                        System.IO.File.Delete(uploadPath1);
                    }
                }
                catch (Exception ex)
                {

                }
            }
            BindData();
        }
    }
    public void BindData()
    {
        DataSet dsAttachments = DatabaseHelper.getNoteAttachmentsC(Server.UrlDecode(Request.QueryString["projectname"]), Convert.ToInt32(Request.QueryString["folderid"]), Convert.ToInt32(Request.QueryString["solutionid"]));

        if (dsAttachments == null || dsAttachments.Tables.Count <= 0 || dsAttachments.Tables[0].Rows.Count <= 0)
        {

            //Showmge.Visible = true;
            DataGrid1.DataSource = dsAttachments.Tables[0];
            DataGrid1.DataBind();
            DataGrid1.Visible = false;
            showdownload.Visible = false;
            DataGrid1.PagerStyle.Visible = false;
        }
        else
        {
            DataGrid1.Visible = true;
            DataGrid1.DataSource = dsAttachments.Tables[0];
            DataGrid1.DataBind();
            if (dsAttachments.Tables[0].Rows.Count <= 100)
            {
                DataGrid1.PagerStyle.Visible = false;
            }
            else
            {
                DataGrid1.PagerStyle.Visible = false;
            }
            //Showmge.Visible = false;
            showdownload.Visible = true;
        }
    }
}
