using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Timers;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.IO; 

namespace NewESGdeskYearcompletion
{
    public partial class ScheduledService : ServiceBase
    {
        Timer timer = new Timer();
        public ScheduledService()
        {
            InitializeComponent();
        }

        public static string getPath()
        {
            //StreamReader readFile = new StreamReader("D:\\Services\\YearCompletingByEmpConn.txt");
            string str1 = "";
            try
            {
                StreamReader readFile = new StreamReader(@"D:\\EmployeeYearComplete\ConnectionString.txt");
                str1 = readFile.ReadToEnd();
                readFile.Close();
            }
            catch (Exception ex)
            {
                SendEmailList("avinashj@esolutionsgroup.co.uk", "Error:In getting connection function " + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n");
                string strerror = "Error:In getting connection function " + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm") + " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n";
                TraceServiceWrite(strerror);
            }
            return str1;
        }
        protected override void OnStart(string[] args)
        {
            timer.Interval = 82800000; //43200000; //86400000;
            //timer.Interval = 60000;
            timer.AutoReset = true;
            timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
            timer.Start();

            //timer.Start(); 
        }

        protected override void OnStop()
        {
            timer.Enabled = false;

        }

        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            TraceService();
        }
        private void TraceService()
        {
            DataSet ds = null;

            try
            {

                ds = getDataset("Select DATEDIFF(YY,DOJ,getdate()) as Year ,firstname +' '+ lastname  as Name, email from users where (DATEPART(DD, DOJ) = DATEPART(DD, getdate()) and DATEPART(MM, DOJ) = DATEPART(MM, getdate()))");

            }
            catch (Exception ex)
            {
                SendEmailList("avinashj@esolutionsgroup.co.uk", "Error:In Execute query " + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n");
                string strerror = "Error:In Execute query " + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm") + " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n";
                TraceServiceWrite(strerror);
            }
            try
            {
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    for (int iCount = 0; iCount < Convert.ToInt32(ds.Tables[0].Rows.Count); iCount++)
                    {
                        if (Convert.ToInt32(ds.Tables[0].Rows[iCount]["Year"].ToString()) > 0)
                        {

                            bool RetMail1 = true;
                            string strHtml = "";
                            strHtml += "<br>" + "Hi," + "<br>";
                            strHtml += "Employee name: " + ds.Tables[0].Rows[iCount]["Name"].ToString() + "<br>";
                            strHtml += "<b>" + "Today i have completed " + "</b>" + Convert.ToInt32(ds.Tables[0].Rows[iCount]["Year"].ToString()) + " year." + "<br/><br/>";
                            strHtml += "Thanks & regards" + "<br/>";
                            strHtml += "Time:- " + System.DateTime.Now + "<br/>";
                            strHtml += ds.Tables[0].Rows[iCount]["Name"].ToString().ToString();
                            //ritub@123sitesolutions.com;juhis@esolutionsgroup.co.uk;rohitd@123sitesolutions.com;michaelr@estatesolutions.co.uk
                            StreamReader readFileemail = new StreamReader(@"D:\\EmployeeYearComplete\EmailtoSelectedPerson.txt");
                            string strto1 = readFileemail.ReadToEnd();
                            RetMail1 = sendEmailonYesrComplete(strto1, "tasks@e-s-g.co.uk", "Year completion information by  " + ds.Tables[0].Rows[iCount]["Name"].ToString(), strHtml, 0);

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                SendEmailList("avinashj@esolutionsgroup.co.uk", "Error:In loop and sending email function " + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n");
                string strerror = "Error:In getDataset function " + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm") + " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n";
                TraceServiceWrite(strerror);

            }




        }


        public static DataSet getDataset(string strSql)
        {
            //Data Source=FHS-AVINASH;Initial Catalog=TestBySir;Integrated Security=True
            //Data Source=88.208.204.82,1615;Network Library=DBMSSOCN;Initial Catalog=esgdesk;User ID=sa;Password=fhsadmin123
            DataSet ds = new DataSet();
            using (SqlConnection conn = new SqlConnection(getPath()))
            {
                using (SqlDataAdapter da = new SqlDataAdapter(strSql, conn))
                {
                    try
                    {

                        da.Fill(ds);
                        conn.Close();
                    }
                    catch (Exception ex)
                    {
                        SendEmailList("avinashj@esolutionsgroup.co.uk", "Error:In getdatabase function " + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n");
                        string strerror = "Error:In getDataset function " + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm") + " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n";
                        TraceServiceWrite(strerror);
                    }
                }
            }
            return ds;
        }

        public static bool sendEmailonYesrComplete(string strTo, string strFrom, string strSubject, string strBody, int i)
        {

            //******************************New Mail Format*****************************************************
            bool blndone = true;
            try
            {
                MailMessage mMailMessage = new MailMessage();
                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                msg.From = new MailAddress(strFrom);
                string[] strToEmails = strTo.Split(';');

                for (int j = 0; j < strToEmails.Length; j++)
                {
                    msg.To.Add(new MailAddress(strToEmails[j].ToString().Trim()));
                }

                if (i == 1)
                {
                    //msg.CC.Add(new MailAddress("rohitd@123sitesolutions.com"));
                    //msg.CC.Add(new MailAddress("priyab@123sitesolutions.com"));

                }
                msg.Subject = strSubject;
                msg.Body = strBody;
                msg.IsBodyHtml = true;
                msg.Priority = MailPriority.Normal;
                System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("mail.internationalhorizons.co.uk");
                mSmtpClient.UseDefaultCredentials = false;
                System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("support@internationalhorizons.co.uk", "support1!");
                mSmtpClient.Credentials = loginInfo;
                mSmtpClient.Send(msg);

                blndone = true;

            }
            catch (Exception ex)
            {
                blndone = false;
                SendEmailList("avinashj@esolutionsgroup.co.uk", "Error:In getdatabase function " + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"), " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n");
                string strerror = "Error:In sendEmailonYesrComplete function " + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm") + " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n";
                TraceServiceWrite(strerror);
            }
            return blndone;
            //************************************************************************************
        }

        public static void SendEmailList(string strTo, string strSubject, string strBody)
        {

            //******************************New Mail Format*****************************************************

            try
            {
                MailMessage mMailMessage = new MailMessage();
                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                msg.From = new MailAddress("avinashj@esolutionsgroup.co.uk");

                string[] strToEmails = strTo.Split(';');
                for (int i = 0; i < strToEmails.Length; i++)
                {
                    msg.To.Add(new MailAddress(strToEmails[i].ToString().Trim()));
                }
                msg.Subject = strSubject;
                msg.Body = strBody;
                msg.IsBodyHtml = true;
                msg.Priority = MailPriority.Normal;
                System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("mail.internationalhorizons.co.uk");
                mSmtpClient.UseDefaultCredentials = false;
                System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("support@internationalhorizons.co.uk", "support1!");
                mSmtpClient.Credentials = loginInfo;
                mSmtpClient.Send(msg);

            }
            catch (Exception ex)
            {
                string strerror = "Error:In SendEmailList function " + "DateTime :-> " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm") + " Input Parameters :" + " Message:->" + ex.Message + "\n" + " Source:->" + ex.Source + "\n" + " InnerException:->" + ex.InnerException + " Stack Trace:-> " + ex.StackTrace + "\n";
                TraceServiceWrite(strerror);

            }

            //return blndone;
            //************************************************************************************
        }

        public static void TraceServiceWrite(string content)
        {
            //set up a filestream
            try
            {
                FileStream fs = new FileStream(@"D:\\EmployeeYearComplete\ErrorMessage.txt", FileMode.OpenOrCreate, FileAccess.Write);

                //set up a streamwriter for adding text
                StreamWriter sw = new StreamWriter(fs);

                //find the end of the underlying filestream
                sw.BaseStream.Seek(0, SeekOrigin.End);

                //add the text
                sw.WriteLine(content);
                //add the text to the underlying filestream

                sw.Flush();
                //close the writer
                sw.Close();
            }
            catch (Exception ex)
            {
                string strmsg = "";
                strmsg = ex.Message;
            }

        }



















    }
}
