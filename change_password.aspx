<%@ Page Language="C#" AutoEventWireup="true" CodeFile="change_password.aspx.cs" Inherits="change_password" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Src="footer.ascx" TagName="footer" TagPrefix="uc2" %>

<%@ Register Src="Header.ascx" TagName="Header" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Change password</title>
    <link href="StyleSheet.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <uc1:Header ID="Header1" runat="server" />
    <div id="Content">
      <table width="100%"><tr><td height="26" align="right">   <span style="color:red;" id="NewCR" runat="server">Unread messages
              <asp:Button id="lnkUnread" runat="server" BackColor="transparent" ForeColor="blue"  BorderStyle="none" style="cursor:pointer;width:auto" CausesValidation="False" OnClick="lnkUnread_Click" /></span><span style="color:red;" id="SpanWOCR" runat="server">Message waiting for response<asp:Button id="lnkWORC" runat="server" BackColor="transparent" ForeColor="blue"  BorderStyle="none" style="cursor:pointer;width:auto" CausesValidation="False" OnClick="lnkWORC_Click"  /></span><span style="display:none"><a id="CompNotesIcon" runat="server" title="Click to view notes" style="border:none; font-size:16px; color:Red; padding-left:5px; text-decoration:none; cursor:pointer;"></a></span></td></tr></table>
        
      </div>
    <table width="47%" cellpadding="5" cellspacing="2">
<tr>
          <td width="559" align="left" valign="top" style="height: 10px">To reset your password, provide your current password. </td>
        </tr>
        <tr>
          <td height="224" align="left" valign="top" style="height: 10px"><table width="566" cellpadding="0" cellspacing="0">
            <tr>
              <td colspan="2" align="left" valign="top"><img height="16" src="images/loginBox_top.png" width="420" /></td>
            </tr>
            <tr>
              <td colspan="2" align="left" style="background: url(images/loginBox_mid.png) repeat-y left top;"><table width="566" cellpadding="5" cellspacing="2">
                <tr>
                  <td width="172" class="whitetext1">Current password:<span style="color:Red">*</span></td>
                  <td width="223"><asp:TextBox ID="txtCurrentPassword" runat="server" TextMode="Password" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtCurrentPassword"
                        ErrorMessage="Please enter old password." Display="Dynamic">*</asp:RequiredFieldValidator></td>
                  <td width="131"><a href="forgot_password.aspx">Forgot your password?</a>
  <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" TargetControlID="RequiredFieldValidator2"> </cc1:ValidatorCalloutExtender></td></tr>
                <tr>
                  <td class="whitetext1">New password:<span style="color:Red">*</span></td>
                  <td colspan="2"><asp:TextBox ID="txtNewPassword" runat="server" TextMode="Password" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtNewPassword"
                        ErrorMessage="Please enter new password." Display="Dynamic">*</asp:RequiredFieldValidator>
                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="server" TargetControlID="RequiredFieldValidator3"> </cc1:ValidatorCalloutExtender>                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server" TargetControlID="RequiredFieldValidator3"></cc1:ValidatorCalloutExtender></td>
                  </tr>
                <tr>
                  <td class="whitetext1">Confirm new password:<span style="color:Red">*</span></td>
                  <td colspan="2"><asp:TextBox ID="txtConfirmPassword" runat="server" TextMode="Password" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtConfirmPassword"
                        ErrorMessage="Please enter password to confirm new password." Display="Dynamic">*</asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="The confirm password entered does not match the new password entered." ControlToCompare="txtNewPassword" ControlToValidate="txtConfirmPassword" Display="Dynamic">*</asp:CompareValidator>
                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="server" TargetControlID="RequiredFieldValidator1"></cc1:ValidatorCalloutExtender>
                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator1"></cc1:ValidatorCalloutExtender>
                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="server" TargetControlID="CompareValidator1"></cc1:ValidatorCalloutExtender></td>
                  </tr>
                
              </table></td>
            </tr>
            <tr>
              <td colspan="2" align="left" style="width: 100px"><img height="16" src="images/loginBox_btm.png" width="420" /></td>
            </tr>
            <tr>
         
            <td width="416" height="10" align="left" valign="top"><hr/>           </td>
            <td width="148" align="left" valign="top"></td>
            </tr>
            
            <tr>
         
            <td width="416" height="10" align="right" valign="top"><asp:Button CssClass="blueBtns" ID="btnChangePassword" runat="server" Text="Change password" OnClick="btnChangePassword_Click" /></td>
            <td width="148" align="left" valign="top"></td>
            </tr>
            
          </table></td>
        </tr>
    
        <tr>
            <td align="left" valign="top">
                <div id="divMessage" style="color:Red" runat="server" visible="false"></div>            </td>
        </tr>
        </table>
    </div>
       
    </div>       
         <uc2:footer ID="Footer1" runat="server" />
    </form>
</body>
</html>
