using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.Drawing.Drawing2D;

public partial class change_password : System.Web.UI.Page
{
    string sql="";
    string companyname;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["clientid"] == null || Session["username"] == null || Session["clientid"].ToString() == "" || Session["username"].ToString() == "")
        {
            Session["returnUrl"] = "change_password.aspx";
            Response.Redirect("login.aspx");
            return;
        }

        if (!Page.IsPostBack)
        {
           
            getCRwaiting();
            getunreadmessage();

            try
            {
                string Clientcomp = "select [CompanyName] from [NonesCRMusers]  where UserName = '" + Session["username"].ToString() + "' and ClientId = " + Session["clientid"].ToString() + "";
                object objClientComp = DatabaseHelper.executeScalar(Clientcomp);
                if (objClientComp.ToString() != "")
                {

                    companyname = objClientComp.ToString();

                    string sqlCRnote = "select count(*) from [Company_notes]  where Company_Name = '" + objClientComp.ToString() + "' and Note_Type ='Client'  and Allow_Notes='True'";
                    int objCRNoteResult = int.Parse(DatabaseHelper.executeScalar(sqlCRnote).ToString());



                    if (objCRNoteResult > 0)
                    {


                        CompNotesIcon.InnerHtml = "<img src=\"images/notes.png\" style=\"cursor:pointer\" title=\"Notifications\" width=\"25\" height=\"25\" alt=\"Notifications\" />" + "Notification: for " + objClientComp.ToString();
                        Session["TaskComp"] = companyname;
                        CompNotesIcon.Attributes.Add("onclick", "window.open('CRNotes.aspx','','status=no,scrollbars=yes,position=center,resizable=yes,width=screen.width,height=screen.height');");


                    }
                    else
                    {

                        CompNotesIcon.InnerHtml = "";
                    }

                }

            }
            catch { }
        }
    }


    void getunreadmessage()
    {
        sql = "select ClientRequest.* from ClientRequest where RequestId in(select ClientRequest_Details.RequestId from ClientRequest_Details where [CommentId] not in (select  CommentId from read_CR_comments where read_CR_comments.username='" + Session["username"].ToString() + "') and ClientRequest_Details.deleted <> 1) and UserName = '" + Session["username"].ToString() + "' and ClientRequest.deleted <> 1";
        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {

            lnkUnread.Text = "(" + ds.Tables[0].Rows.Count + ")";

            NewCR.Visible = true;
        }
        else
        {



            lnkUnread.Visible = false;
            NewCR.Visible = false;
        }
    }

    void getCRwaiting()
    {
        sql = @"select ClientRequest.* from ClientRequest 
                where ClientRequest.Status = 'awaiting client response- required' and UserName ='" + Session["username"].ToString() + "' and status <> 'closed' and deleted <> 1 ";



        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {


            lnkWORC.Text = "(" + ds.Tables[0].Rows.Count + ")";
            SpanWOCR.Visible = true;


        }
        else
        {




            lnkWORC.Text = "(0)";
            SpanWOCR.Visible = false;



        }
    }

    protected void lnkUnread_Click(object sender, EventArgs e)
    {

        Session["filterunreadCR"] = "CRNewComment";
        Session["Status"] = "";
        Session["Status"] = null;

        Response.Redirect("requests.aspx");
    }
    protected void lnkWORC_Click(object sender, EventArgs e)
    {
        Session["filterunreadCR"] = "";
        Session["Status"] = "awaiting client response- required";
        Response.Redirect("requests.aspx", false);

    }

    protected void btnChangePassword_Click(object sender, EventArgs e)
    {
        DataSet ds = DatabaseHelper.getUserDetails(Session["clientid"].ToString(), Session["username"].ToString());

        divMessage.InnerHtml = "";
        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            if (txtCurrentPassword.Text.ToString() != ds.Tables[0].Rows[0]["Password"].ToString())
            {
                divMessage.InnerHtml = "The current password you entered is incorrect.";
                divMessage.Visible = true;
                txtCurrentPassword.Focus();
                return;
            }
            else
            {
                int result = 0;

                result = DatabaseHelper.changePassword(Session["clientid"].ToString(), Session["username"].ToString(), txtNewPassword.Text.ToString());

                if (result > 0)
                {
                    bool flag = DatabaseHelper.sendEmailChangeRequestNorply(ds.Tables[0].Rows[0]["Email"].ToString(), "Account details changed in the Change Request System", generateEmail(ds));
                    flag = DatabaseHelper.sendEmailChangeRequestNorply(ConfigurationManager.AppSettings["ToEmail1"].ToString(), "Account details changed in the Change Request System", generateEmail(ds));
                   /// flag = DatabaseHelper.sendEmailChangeRequestNorply(ConfigurationManager.AppSettings["ToEmail1"].ToString(), "Password details from " + ConfigurationManager.AppSettings["CompanyName"].ToString(), generateEmail(ds));

                    clearControls();
                    divMessage.InnerHtml = "Your new password has been saved.";
                    divMessage.Visible = true;
                }
                else
                {
                    divMessage.InnerHtml = "Your new password has not been saved at this time. Please try again...";
                    divMessage.Visible = true;
                }
            }
        }
    }

    private void clearControls()
    {
        txtConfirmPassword.Text = "";
        txtNewPassword.Text = "";
        txtConfirmPassword.Text = "";
    }

    private string generateEmail(DataSet ds)
    {
        string strBody = "";

        strBody += "<br>";
        strBody += ds.Tables[0].Rows[0]["FirstName"].ToString() +" "+ ds.Tables[0].Rows[0]["Surname"].ToString() +"<br><br>";
        strBody += "This is to inform you that you have changed your password in the Change Request System. If you have not please contact support as soon as possible."+"<br><br>";
        strBody += "<span style=\'font-size:11.0pt;font-family:Calibri,sans-serif;color:red\'>Please do not reply to this email as it does not take replies. Please use the Change Request System as this is the quickest way to process your requests and also keeps a record of communications and progress for you to review at any time.</span><br/>";
        strBody += "<br>";
        strBody += "Your login details are as follows:<br>";
        strBody += "Username: "+ Session["username"].ToString() +"<br>";
        strBody += "Password: " + txtNewPassword.Text.Trim() + "<br><br>";
        strBody += "<a href=\"" + ConfigurationManager.AppSettings["WebAddress2"].ToString() + "/login.aspx\">Click here to login</a><br><br>";
        strBody += "-------------------------------------------<br><br>";
        strBody += ConfigurationManager.AppSettings["CompanyName"].ToString() + "<br>";
        strBody += ConfigurationManager.AppSettings["WebAddress2"].ToString() + "<br>";
        strBody += "<br/>" + "Regards" + "<br/>Support.";

        return strBody;
    }
}
