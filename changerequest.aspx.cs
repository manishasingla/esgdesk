using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.Drawing.Drawing2D;
using System.Net;
using System.Net.NetworkInformation;

public partial class changerequest : System.Web.UI.Page
{

    string companyname;
    string usercompany;
    string sql = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["clientid"] == null || Session["username"] == null || Session["clientid"].ToString() == "" || Session["username"].ToString() == "")
        {
            Session["returnUrl"] = "requests.aspx";
            Response.Redirect("login.aspx");
            return;
        }

        if (!Page.IsPostBack)
        {
            object objUserId = DatabaseHelper.executeScalar("select ShowSMP from  NonesCRMusers where UserName = '" + Session["username"].ToString() + "'");
            if (objUserId != null)
            {
                if (objUserId.ToString() == "True")
                {
                    ShowSMP.Visible = true;
                }
                else
                {
                    ShowSMP.Visible = false;
                }
            }
            else
            {
                ShowSMP.Visible = false;
            }


            object objUserCompany = DatabaseHelper.executeScalar("select CompanyName from  NonesCRMusers where UserName = '" + Session["username"].ToString() + "'");
            if (objUserCompany != null)
            {
                usercompany = objUserCompany.ToString();

                hdnProjectCompany.Value = objUserCompany.ToString();
            }
            else
            {
                hdnProjectCompany.Value = "";
            }

            Loadprogect_drp(hdnProjectCompany.Value);
            getCRwaiting();
            getunreadmessage();

            try
            {
                string Clientcomp = "select [CompanyName] from [NonesCRMusers]  where UserName = '" + Session["username"].ToString() + "' and ClientId = " + Session["clientid"].ToString() + "";
                object objClientComp = DatabaseHelper.executeScalar(Clientcomp);
                if (objClientComp.ToString() != "")
                {

                    companyname = objClientComp.ToString();

                    string sqlCRnote = "select count(*) from [Company_notes]  where Company_Name = '" + objClientComp.ToString() + "' and Note_Type ='Client'  and Allow_Notes='True'";
                    int objCRNoteResult = int.Parse(DatabaseHelper.executeScalar(sqlCRnote).ToString());



                    if (objCRNoteResult > 0)
                    {


                        CompNotesIcon.InnerHtml = "<img src=\"images/notes.png\" style=\"cursor:pointer\" title=\"Notifications\" width=\"25\" height=\"25\" alt=\"Notifications\" />" + "Notification: for " + objClientComp.ToString();
                        Session["TaskComp"] = companyname;
                        CompNotesIcon.Attributes.Add("onclick", "window.open('CRNotes.aspx','','status=no,scrollbars=yes,position=center,resizable=yes,width=screen.width,height=screen.height');");


                    }
                    else
                    {

                        CompNotesIcon.InnerHtml = "";
                    }

                }

            }
            catch { }
        }
    }

    void getunreadmessage()
    {
        sql = "select ClientRequest.* from ClientRequest where RequestId in(select ClientRequest_Details.RequestId from ClientRequest_Details where [CommentId] not in (select  CommentId from read_CR_comments where read_CR_comments.username='" + Session["username"].ToString() + "') and ClientRequest_Details.deleted <> 1) and UserName = '" + Session["username"].ToString() + "' and ClientRequest.deleted <> 1";
        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {

            lnkUnread.Text = "(" + ds.Tables[0].Rows.Count + ")";

            NewCR.Visible = true;
        }
        else
        {



            lnkUnread.Visible = false;
            NewCR.Visible = false;
        }
    }

    void getCRwaiting()
    {
        sql = @"select ClientRequest.* from ClientRequest 
                where ClientRequest.Status = 'awaiting client response- required' and UserName ='" + Session["username"].ToString() + "' and status <> 'closed' and deleted <> 1 ";



        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {


            lnkWORC.Text = "(" + ds.Tables[0].Rows.Count + ")";
            SpanWOCR.Visible = true;


        }
        else
        {




            lnkWORC.Text = "(0)";
            SpanWOCR.Visible = false;



        }
    }


    void Loadprogect_drp(string Company)
    {
        drpProjects.Items.Clear();
        try
        {
            string sql = @"select project_name
		from projects
		where active = 'Y' and CompanyName='" + Company.ToString() + "' order by project_name;";
            DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);
            if (ds_dropdowns.Tables[0].Rows.Count > 0)
            {
                drpProjects.DataSource = ds_dropdowns.Tables[0];
                drpProjects.DataTextField = "project_name";
                drpProjects.DataValueField = "project_name";
                drpProjects.DataBind();
                drpProjects.Items.Insert(0, new ListItem("[no project]", ""));
                ////drpProjects.Items.Insert(1, new ListItem("[Other]", "other"));
            }
            else
            {
                drpProjects.Items.Insert(0, new ListItem("[no project]", ""));
                /// drpProjects.Items.Insert(1, new ListItem("[Other]", "other"));
            }
        }
        catch { }

    }
    public void InsertCRCommentintoTask(string vComment, string vIp, string vMacid, string vPostedby, object objRequestid)
    {
        string qryFindtask, qryClosed, qryICRcommt = "", qryReOpenTask;
        DataSet dscommt, dsTask, dsCount;
        qryClosed = "Select * from tasks where RequestId=" + Convert.ToInt16(objRequestid.ToString()) + ""; ;
        dsCount = DatabaseHelper.getDataset(qryClosed);
        if (dsCount.Tables[0].Rows.Count == 0)
        { }
        else if (dsCount.Tables[0].Rows.Count >= 1)
        {
            qryFindtask = "Select Max(task_id) as task_id from tasks where RequestId=" + Convert.ToInt16(objRequestid.ToString()) + "";
            dsTask = DatabaseHelper.getDataset(qryFindtask);
            if (dsTask != null && dsTask.Tables.Count > 0 && dsTask.Tables[0].Rows.Count > 0)
            {
                string chkclosed = "Select task_id from tasks where status='Closed' and task_id=" + Convert.ToInt16(dsTask.Tables[0].Rows[0]["task_id"]) + "";
                DataSet dschk;
                dschk = DatabaseHelper.getDataset(chkclosed);
                if (dschk != null && dschk.Tables.Count > 0 && dschk.Tables[0].Rows.Count > 0)
                {
                    string chkOpen = "Update tasks set status='re-opened' where task_id='" + dschk.Tables[0].Rows[0]["task_id"] + "'";
                    DatabaseHelper.executeNonQuery(chkOpen);
                }
                qryICRcommt = " insert into task_comments([task_id],[username],[post_date],[comment],[Ip],[Macid],[deleted],[qflag],[QuesTo]) ";
                qryICRcommt += " values(" + dsTask.Tables[0].Rows[0]["task_id"] + ",";
                qryICRcommt += "'" + vPostedby.ToString().Replace("'", "''") + "',";
                qryICRcommt += "getdate(),";
                qryICRcommt += "'" + vComment.Trim().Replace("'", "''") + "',";
                //************************************************
                qryICRcommt += "'" + vIp.ToString() + "',";
                qryICRcommt += "'" + vMacid.ToString() + "',";
                //************************************************
                qryICRcommt += "0,";
                qryICRcommt += "0,";
                qryICRcommt += "0);";
                DatabaseHelper.executeNonQuery(qryICRcommt);
            }
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {

        //*****************************************************************

        string[] strArrayMacIDAndIP = new string[2];
        string strMacId = "";
        string strIpAddress = "";

        foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
        {
            if (nic.OperationalStatus == OperationalStatus.Up)
            {
                strMacId += nic.GetPhysicalAddress().ToString();
                break;
            }
        }

        strArrayMacIDAndIP[0] = strMacId;

        strIpAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (strIpAddress == null)

            strIpAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

        strArrayMacIDAndIP[1] = strIpAddress;

        //*****************************************************************


        //**************************************************************************
        //if (ftbComment.Text.Trim() == "")
          if (ftbComment.Content.Trim() == "")
        //**************************************************************************
        {
            divMessage.InnerHtml = "Enter your change request.";
            divMessage.Visible = true;
        }
        else
        {

            //object objRequestId = DatabaseHelper.checkWebsiteURL(Session["Clientid"].ToString(), Session["username"].ToString(), txtWebsiteURL.Text.Trim());
            //if (objRequestId == null)
            //{
            string interbalref = "0";
            //***************************************************************************************************************************************
            //object objResult = DatabaseHelper.insertClientRequest(Session["Clientid"].ToString(), Session["username"].ToString(), txtWebsiteURL.Text.Trim(), txtShortDescr.Text.Trim(), ftbComment.Text.Trim(), hdnProjectCompany.Value, drpProjects.SelectedValue, Session["username"].ToString(), Session["username"].ToString(), interbalref, Session["Clientid"].ToString());
            object objResult = DatabaseHelper.insertClientRequest(Session["Clientid"].ToString(), Session["username"].ToString(), txtWebsiteURL.Text.Trim(), txtShortDescr.Text.Trim(), ftbComment.Content.Trim(), hdnProjectCompany.Value, drpProjects.SelectedValue, Session["username"].ToString(), Session["username"].ToString(), interbalref, Session["Clientid"].ToString(), strArrayMacIDAndIP[0], strArrayMacIDAndIP[1]);
            string AuthMigrate = "";
            DataSet dsMigrate;
            AuthMigrate = "select Migrate_CR_comments_Tasks from NonesCRMusers  where UserName='" + Session["username"] + "'";
            dsMigrate = DatabaseHelper.getDataset(AuthMigrate);

            if (dsMigrate != null && dsMigrate.Tables.Count > 0 && dsMigrate.Tables[0].Rows.Count > 0)
            {
                if (dsMigrate.Tables[0].Rows[0]["Migrate_CR_comments_Tasks"].ToString() == "True")
                {
                    InsertCRCommentintoTask(ftbComment.Content.Trim(), strArrayMacIDAndIP[1], strArrayMacIDAndIP[0], Session["username"].ToString(), objResult.ToString());
                }
            }

              //**************************************************************************************************************************************
                if (objResult.ToString() != "0")
                {

                    DataSet dsUserDetails = DatabaseHelper.getUserDetails(Session["clientid"].ToString(), Session["username"].ToString());

                    bool flag = false;

                    if (dsUserDetails == null || dsUserDetails.Tables.Count <= 0 || dsUserDetails.Tables[0].Rows.Count <= 0)
                    {
                    }
                    else
                    {
                        companyname = dsUserDetails.Tables[0].Rows[0]["CompanyName"].ToString();
                        flag = DatabaseHelper.sendEmailChangeRequestNorply(dsUserDetails.Tables[0].Rows[0]["Email"].ToString(), "Request ID:" + objResult.ToString() + " was updated - Change request form  (Request ID:" + objResult.ToString() + ")", generateEmail(Convert.ToInt32(objResult)));
                    }

                    flag = DatabaseHelper.sendEmailChangeRequestNorply(ConfigurationManager.AppSettings["ToEmail1"].ToString(), "Request ID:" + objResult.ToString() + " was updated - Change request form  (Request ID:" + objResult.ToString() + ")", generateAdminEmail(Convert.ToInt32(objResult)));
                   /// flag = DatabaseHelper.sendEmailChangeRequestNorply(ConfigurationManager.AppSettings["ToEmail2"].ToString(), "Request ID:" + objResult.ToString() + " was updated - Change request form  (Request ID:" + objResult.ToString() + ")", generateAdminEmail(Convert.ToInt32(objResult)));

                    divMessage.InnerHtml = "Your change request has been sent.";
                    divMessage.Visible = true;
                    txtShortDescr.Text = "";
                    txtWebsiteURL.Text = "";
                    //*********************************************************
                    //ftbComment.Text = "";
                    ftbComment.Content = "";
                    //*********************************************************
                    Response.Redirect("edit_request.aspx?reqid=" + objResult.ToString(), false);
                }
                else
                {
                    divMessage.InnerHtml = "There was problem in saving you change request. Please try again.";
                    divMessage.Visible = true;
                }
            //}
            //else
            //{
            //    divMessage.InnerHtml = "You have already added request for provided <b>Title or preferably URL: " + txtWebsiteURL.Text + "</b>. Would you like to ammend comments? please <a href=\"edit_request.aspx?reqid=" + objRequestId.ToString() + "\">click here</a>";
            //    divMessage.Visible = true;
            //}
        }

    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        clearControls();
    }

    private void clearControls()
    {
        txtWebsiteURL.Text = "";
        txtShortDescr.Text = "";
        //*********************************************************
        //ftbComment.Text = "";
          ftbComment.Content = "";
        //*********************************************************
        divMessage.InnerHtml = "";
    }
    protected void btnClear_Click1(object sender, EventArgs e)
    {
        clearControls();
    }

    private string generateEmail(int RequestId)
    {
        DataSet dsRequests = DatabaseHelper.getClientRequest(RequestId);
        string strBody = "";
        if (dsRequests == null || dsRequests.Tables.Count <= 0 || dsRequests.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "Request not found.<br><br><a href='requests.aspx'>View requests</a>";
            divMessage.Visible = true;
        }
        else
        {
             //strBody += "<b>CR:</b> <span style=\"color:red\">";
            strBody += "<b>" + "FAO  " + Session["username"].ToString() + " :" + "</b><br><span style=\"color:red\">";


            //strBody += task_action;
            strBody += dsRequests.Tables[0].Rows[0]["LastUpdatedBy"].ToString() + " has added Change Request "+"<span style=\"color:red\"> (number  </span>" + "<a style=\"color:red\" href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/edit_request.aspx?reqid=" + RequestId + "'>" + RequestId + "</a>) </span> " + DateTime.Parse(dsRequests.Tables[0].Rows[0]["LastUpdatedOn"].ToString()).ToString("dd MMM h:mm tt") + "<br><br>";
            ////strBody += dsRequests.Tables[0].Rows[0]["LastUpdatedBy"].ToString() + " has added CR (<a style=\"color:red\" href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/edit_request.aspx?reqid=" + RequestId + "'>" + RequestId + "</a>) </span> " + DateTime.Parse(dsRequests.Tables[0].Rows[0]["LastUpdatedOn"].ToString()).ToString("dd MMM h:mm tt") + "<br><br>";
            strBody += "<span style=\'font-size:11.0pt;font-family:Calibri,sans-serif;color:red\'>Click on the following link to access or amend the change request:  " + " " + " <a href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/edit_request.aspx?reqid=" + RequestId + "'>" + txtShortDescr.Text.Trim() + " . " + "</a>" + "  Do not start a new change request as this will slow the resolution." + "</span><br><br>";
             strBody += "Client: " + dsRequests.Tables[0].Rows[0]["UserName"].ToString() + "<br>";
            strBody += "Company name: " + companyname + "<br>";
            strBody += "Reported on: " + DateTime.Parse(dsRequests.Tables[0].Rows[0]["RequestDate"].ToString()).ToString("dd MMM h:mm tt") + "<br>";
            strBody += "Title or preferably URL: <a href='" + dsRequests.Tables[0].Rows[0]["WebsiteURL"].ToString() + "' target='_blank'>" + dsRequests.Tables[0].Rows[0]["WebsiteURL"].ToString() + "</a><br>";
            strBody += "Status:  " + dsRequests.Tables[0].Rows[0]["status"].ToString();
            strBody += "<br><br>";
          

            DataSet dsReqDetails = DatabaseHelper.getClientRequestDetails(RequestId);
           
            strBody += "<table cellspacing=\"0\" border=\"1\" style=\"width: 99%;border-collapse: collapse;\">";
            strBody += "<tbody>";

            for (int i = 0; i < dsReqDetails.Tables[0].Rows.Count; i++)
            {
                strBody += "<tr>";
                strBody += "<td>";
                strBody += "<div style=\"border: 1px none Green;\">";
                strBody += "<table width=\"100%\">";
                strBody += "<tbody>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span style=\"color: Green;\">comment " + dsReqDetails.Tables[0].Rows[i]["CommentId"].ToString() + " posted by " + dsReqDetails.Tables[0].Rows[i]["PostedBy"].ToString() + " on " + DateTime.Parse(dsReqDetails.Tables[0].Rows[i]["PostedOn"].ToString()).ToString("dd MMM h:mm tt") + "</span>";
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\"> </td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span>" + dsReqDetails.Tables[0].Rows[i]["comment"].ToString() + "</span>";
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "</tbody>";
                strBody += "</table>";
                strBody += "</div>";
                strBody += "</td>";
                strBody += "</tr>";
            }
            strBody += "</tbody>";
            strBody += "</table>";
            strBody += "<br/><span style=\'font-size:11.0pt;font-family:Calibri,sans-serif;color:red\'>Please do not reply to this email as it does not take replies. Please use the Change Request System as this is the quickest way to process your requests and also keeps a record of communications and progress for you to review at any time.</span><br/>";
            strBody += "<br>";
            strBody += "<br/><br/>" + "Thanks & Regards," + "<br/>Support.";
        }
        return strBody;
    }

    private string generateAdminEmail(int RequestId)
    {
        DataSet dsRequests = DatabaseHelper.getClientRequest(RequestId);
        string strBody = "";
        if (dsRequests == null || dsRequests.Tables.Count <= 0 || dsRequests.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "Request not found.<br><br><a href='requests.aspx'>View requests</a>";
            divMessage.Visible = true;
        }
        else
        {
            strBody += "<b>" + "FAO  " + Session["username"].ToString() + " :" + "</b><br><span style=\"color:red\">";


            //strBody += task_action;
            strBody += dsRequests.Tables[0].Rows[0]["LastUpdatedBy"].ToString() + " has added Change Request " + "<span style=\"color:red\"> (number  </span>" + "<a style=\"color:red\" href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_request.aspx?reqid=" + RequestId + "'>" + RequestId + "</a>) </span> " + DateTime.Parse(dsRequests.Tables[0].Rows[0]["LastUpdatedOn"].ToString()).ToString("dd MMM h:mm tt") + "<br><br>";
            ////strBody += dsRequests.Tables[0].Rows[0]["LastUpdatedBy"].ToString() + " has added CR (<a style=\"color:red\" href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/edit_request.aspx?reqid=" + RequestId + "'>" + RequestId + "</a>) </span> " + DateTime.Parse(dsRequests.Tables[0].Rows[0]["LastUpdatedOn"].ToString()).ToString("dd MMM h:mm tt") + "<br><br>";
            strBody += "<span style=\'font-size:11.0pt;font-family:Calibri,sans-serif;color:red\'>Click on the following link to access or amend the change request:  " + " " + " <a href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_request.aspx?reqid=" + RequestId + "'>" + txtShortDescr.Text.Trim() + " . " + "</a>" + "  Do not start a new change request as this will slow the resolution." + "</span><br><br>";
            strBody += "Client: " + dsRequests.Tables[0].Rows[0]["UserName"].ToString() + "<br>";
            strBody += "Company name: " + companyname + "<br>";
            strBody += "Reported on: " + DateTime.Parse(dsRequests.Tables[0].Rows[0]["RequestDate"].ToString()).ToString("dd MMM h:mm tt") + "<br>";
            strBody += "Title or preferably URL: <a href='" + dsRequests.Tables[0].Rows[0]["WebsiteURL"].ToString() + "' target='_blank'>" + dsRequests.Tables[0].Rows[0]["WebsiteURL"].ToString() + "</a><br>";
            strBody += "Status:  " + dsRequests.Tables[0].Rows[0]["status"].ToString();
            strBody += "<br><br>";

            DataSet dsReqDetails = DatabaseHelper.getClientRequestDetails(RequestId);
           
            strBody += "<table cellspacing=\"0\" border=\"1\" style=\"width: 99%;border-collapse: collapse;\">";
            strBody += "<tbody>";

            for (int i = 0; i < dsReqDetails.Tables[0].Rows.Count; i++)
            {
                strBody += "<tr>";
                strBody += "<td>";
                strBody += "<div style=\"border: 1px none Green;\">";
                strBody += "<table width=\"100%\">";
                strBody += "<tbody>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span style=\"color: Green;\">comment " + dsReqDetails.Tables[0].Rows[i]["CommentId"].ToString() + " posted by " + dsReqDetails.Tables[0].Rows[i]["PostedBy"].ToString() + " on " + DateTime.Parse(dsReqDetails.Tables[0].Rows[i]["PostedOn"].ToString()).ToString("dd MMM h:mm tt") + "</span>";
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\"> </td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span>" + dsReqDetails.Tables[0].Rows[i]["comment"].ToString() + "</span>";
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "</tbody>";
                strBody += "</table>";
                strBody += "</div>";
                strBody += "</td>";
                strBody += "</tr>";
            }
            strBody += "</tbody>";
            strBody += "</table>";
            strBody += "<br/><span style=\'font-size:11.0pt;font-family:Calibri,sans-serif;color:red\'>Please do not reply to this email as it does not take replies. Please use the Change Request System as this is the quickest way to process your requests and also keeps a record of communications and progress for you to review at any time.</span><br/>";
            strBody += "<br>";
            strBody += "<br/><br/>" + "Thanks & Regards," + "<br/>Support.";
        }
        return strBody;
    }

    protected void lnkUnread_Click(object sender, EventArgs e)
    {

        Session["filterunreadCR"] = "CRNewComment";
        Session["Status"] = "";
        Session["Status"] = null;

        Response.Redirect("requests.aspx");
    }
    protected void lnkWORC_Click(object sender, EventArgs e)
    {
        Session["filterunreadCR"] = "";
        Session["Status"] = "awaiting client response- required";
        Response.Redirect("requests.aspx", false);

    }
}