<%@ Page Language="C#" AutoEventWireup="true" CodeFile="changerequest.aspx.cs" Inherits="changerequest"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="footer.ascx" TagName="footer" TagPrefix="uc2" %>
<%@ Register Src="Header.ascx" TagName="Header" TagPrefix="uc1" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <%= ConfigurationManager.AppSettings["CompanyName"].ToString()%></title>

    <script type="text/javascript" language="javascript">

        function hide() {


            //            var l = document.getElementById("lnk_h_s");
            //            var h = document.getElementById("d_h_s");
            //            var i = document.getElementById("img1");


            //            if (l.innerHTML == "Add Change request") {
            //                l.innerHTML = "Close Change request box"
            //                i.src = "images/Minus.png"
            //                h.style.display = "block";
            //            }
            //            else {
            //                l.innerHTML = "Add Change request"
            //                i.src = "images/plus.png"
            //                h.style.display = "none";
            //            }

            var l = document.getElementById("lnk_h_s");
            var h = document.getElementById("d_h_s");
            var i = document.getElementById("img1");


            if (l.innerHTML == "See comments") {
                l.innerHTML = "Close comments"
                i.src = "images/plus.png"
                h.style.display = "none";
            }
            else {
                l.innerHTML = "See comments"
                i.src = "images/Minus.png"
                h.style.display = "block"

            }

        }
        function defaultopen() {
            document.getElementById("d_h_s").style.display = "block"
            document.getElementById("img1").src = "images/Minus.png"
        } 
    </script>

    <link href="StyleSheet.css" rel="stylesheet" type="text/css" />
</head>
<body onload="defaultopen();">
    <form id="form1" runat="server">
    <div id="wrapper">
        <uc1:Header ID="Header1" runat="server" />
        <div id="Content" style="margin-top: 10px;">
            <div id="Content_98">
                <div>
                    <table width="100%">
                        <tr>
                            <td align="right">
                                <span style="color: red;" id="NewCR" runat="server">Unread messages<asp:Button ID="lnkUnread"
                                    runat="server" BackColor="transparent" ForeColor="blue" BorderStyle="none" Style="cursor: pointer;
                                    width: auto" CausesValidation="False" OnClick="lnkUnread_Click" /></span><span style="color: red;"
                                        id="SpanWOCR" runat="server">Message waiting for response<asp:Button ID="lnkWORC"
                                            runat="server" BackColor="transparent" ForeColor="blue" BorderStyle="none" Style="cursor: pointer;
                                            width: auto" CausesValidation="False" OnClick="lnkWORC_Click" /></span><span style="display: none"><a
                                                id="CompNotesIcon" runat="server" title="Click to view notes" style="border: none;
                                                font-size: 16px; color: Red; padding-left: 5px; text-decoration: none; cursor: pointer;"></a></span>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="divBorder" style="border: solid 1px #cdcdcd; padding: 10px;">
                    <table width="100%">
                        <tr>
                            <td colspan="3">
                                <p>
                                    The change request system is here for you to <span style="font-weight: bold">make requests
                                        for changes </span>or additions to a project or <span style="font-weight: bold">request
                                            support.</span> Please note these are only requests and may or may not be
                                    actioned dependent on the agreement(s)/subscription(s) in place (*).</p>
                                <p>
                                    Be as clear and concise as possible.<span style="color: red">The easier the request
                                        is to understand the quicker the change will be for us to complete.If you want the
                                        change request to be reviewed more quickly then it is very important to read the
                                        tips at the foot of this page.</span></p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <hr />
                                <span style="color: Red">*</span> Note: <span style="color: Green">If you need to upload
                                    any attachment please save the change request first then edit the change request.</span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td height="171" colspan="3">
                                <table>
                                    <tr>
                                        <td align="left" colspan="2" valign="top">
                                            Short description
                                        </td>
                                        <td align="left" colspan="2" valign="top">
                                            URL if relevant (Website address i.e. www.ourwebsite.com/contactus.html)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" colspan="2">
                                            <asp:TextBox ID="txtShortDescr" runat="server" Width="450px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtShortDescr"
                                                ErrorMessage="Please enter short description.">*</asp:RequiredFieldValidator><cc1:ValidatorCalloutExtender
                                                    ID="ValidatorCalloutExtender2" runat="server" TargetControlID="RequiredFieldValidator2">
                                                </cc1:ValidatorCalloutExtender>
                                        </td>
                                        <td align="left" valign="top" colspan="2">
                                            <asp:TextBox ID="txtWebsiteURL" runat="server" Width="550px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            <span></span>
                                        </td>
                                        <td align="left" valign="top">
                                        </td>
                                        <td align="left" valign="top">
                                        </td>
                                        <td align="left" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" colspan="2" style="height: 22px">
                                        </td>
                                        <td align="left" valign="top" style="height: 22px" colspan="2">
                                            Project name:
                                            <asp:DropDownList ID="drpProjects" runat="server" AutoPostBack="True" CssClass="filerDrpodown"
                                                Width="470px">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="drpProjects"
                                                ErrorMessage="Please select project name.">*</asp:RequiredFieldValidator>
                                            <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator4">
                                            </cc1:ValidatorCalloutExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="4" valign="top">
                                            <div style="height: 22px; background-color: rgb(40, 44, 95); padding-top: 5px; padding-left: 6px;">
                                                <asp:ImageButton ID="img1" runat="server" src="images/plus.png" Style="border-width: 0px;
                                                    margin-top: -2px;" OnClientClick="hide();return false;" />
                                                <asp:LinkButton ID="lnk_h_s" runat="server" OnClientClick="hide();return false;"
                                                    CssClass="whitetext2" Style="text-decoration: none; vertical-align: top;">See comments</asp:LinkButton>
                                            </div>
                                            <br />
                                            <div id="d_h_s" runat="server">
                                                <%--***************************************************************************************************--%>
                                                <%--<FTB:FreeTextBox ID="ftbComment" runat="server" Width="100%" Height="200px"></FTB:FreeTextBox>--%>
                                                <telerik:RadEditor ID="ftbComment" runat="server" Height="300px" Width="100%">
                                                    <Snippets>
                                                        <telerik:EditorSnippet Name="Product Template" Value="<table><tr><td><div style='width:300px;border:2px outset #D8D2BD;padding:34px;font-family:Arial, Verdana; font-size:11px;background- color:#F1EFE6;'>Dear ____________________,<br>Thank you for inquiring about ____________. Your request will be processed in 48 hours and shipped at the address you have provided.<b>Please, contact us if you have any problems.</b></div></td></tr></table>">            
                                                        </telerik:EditorSnippet>
                                                        <telerik:EditorSnippet Name="Signature" Value="<strong>John Smith</strong><br>Sales Manager<br>">
                                                        </telerik:EditorSnippet>
                                                    </Snippets>
                                                    <Links>
                                                        <telerik:EditorLink Name="Telerik" Href="http://www.telerik.com">
                                                            <telerik:EditorLink Name="Telerik Products" Href="http://www.telerik.com/products"
                                                                Target="_blank" />
                                                            <telerik:EditorLink Name="Telerik Support Center" Href="http://www.telerik.com/support/home.aspx"
                                                                Target="_blank" />
                                                            <telerik:EditorLink Name="Telerik Community" Href="http://www.telerik.com/community/home.aspx"
                                                                Target="_blank" ToolTip="Telerik Community" />
                                                        </telerik:EditorLink>
                                                    </Links>
                                                    <Tools>
                                                    </Tools>
                                                    <ImageManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                                    <MediaManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                                    <FlashManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                                    <TemplateManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                                    <DocumentManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                                    <Modules>
                                                        <telerik:EditorModule Name="RadEditorStatistics" Visible="False" Enabled="true" />
                                                        <telerik:EditorModule Name="RadEditorDomInspector" Visible="False" Enabled="true" />
                                                        <telerik:EditorModule Name="RadEditorNodeInspector" Visible="False" Enabled="true" />
                                                        <telerik:EditorModule Name="RadEditorHtmlInspector" Visible="False" Enabled="true" />
                                                    </Modules>
                                                    <Content>
                                                    </Content>
                                                </telerik:RadEditor>
                                                <%--****************************************************************************************************--%>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                        </td>
                                        <td align="left" valign="top">
                                        </td>
                                        <td align="left" valign="top">
                                        </td>
                                        <td align="left" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" colspan="3">
                                            <div id="divMessage" runat="server" style="color: Red" visible="false">
                                                <asp:HiddenField ID="hdnProjectCompany" runat="server" />
                                            </div>
                                        </td>
                                        <td align="right" valign="top">
                                            <asp:Button CssClass="blueBtns" ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                                            &nbsp;&nbsp;
                                            <asp:Button CssClass="blueBtns" ID="btnClear" runat="server" Text="Clear" CausesValidation="False"
                                                OnClick="btnClear_Click1" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <hr />
                                <p style="font-weight: bold">
                                    <span style="color: Red; font-size: 18px">What are the benefits to you?</span> Why
                                    use the Change Request system?</p>
                                <p>
                                </p>
                                <div style="padding-left: 15px">
                                    <li>The support team will receive the request<span style="color: Red"> immediately.</span></li>
                                    <li>You will also be able to login at any time and <span style="color: Red">check the
                                        status.</span></li>
                                    <li>The ongoing progress will be <span style="color: Red">logged in one place</span>
                                        rather than through a stream of unrelated email communications.</li>
                                    <li>If the support team require to speak directly with you they can initiate an <span
                                        style="color: Red">online chat</span> with you or <span style="color: Red">remote desktop
                                            login. </span></li>
                                </div>
                                <p style="font-weight: bold">
                                    <span style="color: Red; font-size: 18px">How to get your request looked at quickly?</span>
                                    Tips on ensuring that the change request is reviewed and/or processed in the least
                                    amount of time.</p>
                                <p>
                                </p>
                                <div style="padding-left: 15px">
                                    <li>Be as<span style="color: Red"> clear</span> and concise as possible</li>
                                    <li>When logging in ensure that you <span style="color: red">check the 'Save details'
                                        check box</span>. You will then never have to login again from that PC (if cookies
                                        are enabled).</li>
                                    <li><span style="color: red">Dont reply to the emails which are sent automatically </span>
                                        when any change request is updated. Login to your account and add any comments directly
                                        into the system</li>
                                    <li>If you have multiple changes for a certain page enter the changes in the same change
                                        request. <span style="color: Red">If you create more than one change request for one
                                            issue as it will slow the procedure.</span></li>
                                    <li>Ensure that any reference to a part of the site is referred to by page name or preferably
                                        URL (i.e. www.acmeco.com/contactus.htm) and the paragraph or element</li>
                                    <li>If you need to upload any attachment please save the change request first then edit
                                        the change request.</li>
                                    <li><span>Save this website as a bookmark so you can quickly access it.</span><span
                                        style="margin-top: 20px"><a class="addthis_button" href="http://www.addthis.com/bookmark.php?v=250&amp;username=mrscottjames">
                                            <img src="http://s7.addthis.com/static/btn/v2/lg-share-en.gif" width="125" height="16"
                                                alt="Bookmark and Share" style="border: 0" /></a>

                                        <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#username=mrscottjames"></script>

                                    </span></li>
                                </div>
                                <p>
                                    To most efficiently explain a text change please look at the following as example<br />
                                    In www.acmeco.com/aboutus.html, please change:<br />
                                    Acme Co is a great company in London which produces fantastic widgets for the discerning
                                    widget consumer.<br />
                                    to<br />
                                    Acme Co is a great company in the UK which produces fantastic widgets for the discerning
                                    widget consumer.
                                </p>
                                <div id="ShowSMP" runat="server">
                                    <p>
                                        <span style="color: Red; font-weight: bold; font-size: 18px">What support and/or maintenance
                                            packages are available?</span></p>
                                    <p>
                                        There are several packages available for premier support (immediate response) and
                                        to cut the costs of website maintenance. Please <a href="http://www.request.estatesolutions.eu/Support_and_maintenance_contracts.pdf"
                                            target="_blank" style="text-decoration: underline">download</a> the following
                                        PDF which outlines all the options including non related IT support.</p>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <uc2:footer ID="Footer1" runat="server" />
    </form>
</body>
</html>
