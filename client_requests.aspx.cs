using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
using Telerik.Web;

public partial class client_requests : System.Web.UI.Page
{
    string sql = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (Session["admin"] == null || Session["admin"].ToString() == "")
        {
            Session["returnUrl"] = "client_requests.aspx";
            Response.Redirect("login.aspx");
            return;
        }

        if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
        {
            Response.Redirect("tasks.aspx");
            return;
        }
        if (Session["boolclose"] == "true")
        {
            chkHideClosed.Checked = true;
            Session["boolclose"] = "false";
        }
        else { chkHideClosed.Checked = false; }

        if (!Page.IsPostBack)
        {
            Session["pagesize"] = 20;

            load_statuses();
            ///load_Priority();

            if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
            {
                LnkBtnToDeleteAll.Visible = false;
                LnkBtnToChngPR.Visible = false;
                DivMiltiselection.Visible = false;
            }
            else
            {
                LnkBtnToDeleteAll.Visible = true;
                LnkBtnToChngPR.Visible = true;
                DivMiltiselection.Visible = true;
            }
            LnkChngStatus.Enabled = false;
            LnkBtnToChngPR.Enabled = false;
            if (Session["filter"] == null || Session["filter"].ToString() == "")
            {

                lblOrderBy.Text = " ORDER BY LastUpdatedOn desc";
                BindWebsiteURL();
                BindPostedBy();
                getFilterSession();
                BindData();
            }
            else
            {
                if (Session["filter"].ToString() == "NewCR")
                {
                    drpStatus.SelectedValue = "new";
                    lblOrderBy.Text = " ORDER BY LastUpdatedOn desc";
                    BindWebsiteURL();
                    BindPostedBy();
                    getFilterSession();
                    BindData();
                }
                else if (Session["filter"].ToString() == "CRwocr")
                {
                    drpStatus.SelectedValue = "awaiting client response- required";
                    lblOrderBy.Text = " ORDER BY LastUpdatedOn desc";
                    BindWebsiteURL();
                    BindPostedBy();
                    getFilterSession();
                    BindData();

                }
                else if (Session["filter"].ToString() == "CRNewComment")
                {
                    lblOrderBy.Text = " ORDER BY LastUpdatedOn desc";
                    BindWebsiteURL();
                    BindPostedBy();
                    getFilterSession();
                    BindData2();

                }else if (Session["filter"].ToString() == "NewUnApprovedCR")
                {
                    drpStatus.SelectedValue = "new";
                    lblOrderBy.Text = " ORDER BY LastUpdatedOn desc";
                    BindWebsiteURL();
                    BindPostedBy();
                    getFilterSession();
                    BindData3("Select * from ClientRequest where ClientRequest.CRApprove='False' and ClientRequest.deleted <> 1  and ClientRequest.status <> 'closed'  and ClientRequest.Status = 'new'   ORDER BY LastUpdatedOn desc");
                }
                else if (Session["filter"].ToString() == "NewUnApprovedCRComment")
                {
                    //drpStatus.SelectedValue = "new";
                    lblOrderBy.Text = " ORDER BY LastUpdatedOn desc";
                    BindWebsiteURL();
                    BindPostedBy();
                    getFilterSession();
                    string sqlcmmt = @"select ClientRequest.* from ClientRequest where  ClientRequest.Status<>'closed' and ClientRequest.deleted <> 1 and RequestId in(select Distinct Requestid from ClientRequest_Details where ApprovUnApprov='Approve' and deleted<>1)";
                    BindData3(sqlcmmt);
                }
                else
                {
                    lblOrderBy.Text = " ORDER BY LastUpdatedOn desc";
                    BindWebsiteURL();
                    BindPostedBy();
                    getFilterSession();
                    BindData();
                }

            }
              
        }
    }

    protected int getHoursTakenSoFar(string task_id)
    {
        sql = @"select sum(datediff(minute,started_date,finished_date))
                from hours_reporting
                where finished_date is not null and task_id=" + task_id.ToString();

        int hrsTaken = 0;
        object objHrsTaken = DatabaseHelper.executeScalar(sql);
        if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
        {
            hrsTaken += (int)objHrsTaken;
        }
        else
        {
            hrsTaken = 0;
        }

        sql = @"select sum(datediff(minute,started_date,getdate()))
                from hours_reporting
                where finished_date is null and task_id=" + task_id.ToString();

        objHrsTaken = DatabaseHelper.executeScalar(sql);

        if (objHrsTaken != null && objHrsTaken.ToString() != "" && objHrsTaken.ToString() != "0")
        {
            hrsTaken += (int)objHrsTaken;
        }

        return hrsTaken;
    }

    void load_Priority()
    {
        sql = "select priority_name,priority_id from priorities order by sort_seq, priority_name;";
        DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);
        DrpMultipleTaskPR.DataSource = ds_dropdowns.Tables[0];
        DrpMultipleTaskPR.DataTextField = "priority_name";
        DrpMultipleTaskPR.DataValueField = "priority_id";
        DrpMultipleTaskPR.DataBind();
        DrpMultipleTaskPR.Items.Insert(0, new ListItem("[no filter]", "0"));

    }
    void load_statuses()
    {
        sql = "select status_name from statuses order by sort_seq, status_name;";
        DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);
        drpStatus.DataSource = ds_dropdowns.Tables[0];
        drpStatus.DataTextField = "status_name";
        drpStatus.DataValueField = "status_name";
        drpStatus.DataBind();
        drpStatus.Items.Insert(0, new ListItem("[no status]", ""));
        DrpMultipleTaskStatus.DataSource = ds_dropdowns.Tables[0];
        DrpMultipleTaskStatus.DataTextField = "status_name";
        DrpMultipleTaskStatus.DataValueField = "status_name";
        DrpMultipleTaskStatus.DataBind();
        DrpMultipleTaskStatus.Items.Insert(0, new ListItem("[no status]", "0"));
    }
    void getCRNewComment()
    {}
   
    private void BindData()
    {
        drpPerPage1.Visible = true;
        string strFilter = " where ";

        string topFilterLable = "";

        if (chkViewDeleted.Checked == true)
        {
            strFilter += " ClientRequest.deleted = 1 ";
        }
        else
        {
            strFilter += " ClientRequest.deleted <> 1 ";
        }

        if (chkHideClosed.Checked == true)
        {
            strFilter += " and ClientRequest.status = 'closed' ";
        }
        else
        {
            strFilter += " and ClientRequest.status <> 'closed' ";
        }


        if (drpChangeBy.SelectedIndex != 0)
        {
            char[] separator = new char[] { '+' };

            string[] strCid = drpChangeBy.SelectedItem.Value.Split(separator);

            strFilter += " and ClientRequest.ClientId = " + strCid[0] + " and ClientRequest.UserName='" + drpChangeBy.SelectedItem.Text + "' ";

            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Change added by: " + drpChangeBy.SelectedItem.Text;
            }
            else
            {
                topFilterLable += " / Change added by: " + drpChangeBy.SelectedItem.Text;
            }
        }

        if (drpStatus.SelectedIndex != 0)
        {
            strFilter += " and ClientRequest.Status = '" + drpStatus.SelectedItem.Value + "' ";

            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Status: " + drpStatus.SelectedItem.Text;
            }
            else
            {
                topFilterLable += " / Status: " + drpStatus.SelectedItem.Text;
            }
        }

        if (drpPriority.SelectedIndex != 0)
        {
            strFilter += " and ClientRequest.Priority = " + drpPriority.SelectedItem.Value + " ";

            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Priority: " + drpPriority.SelectedItem.Text;
            }
            else
            {
                topFilterLable += " / Priority: " + drpPriority.SelectedItem.Text;
            }
        }

        if (drpWebsiteUrl.SelectedIndex != 0)
        {
            strFilter += " and CompanyName = '" + drpWebsiteUrl.SelectedItem.Value + "' ";

            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Company Name: " + drpWebsiteUrl.SelectedItem.Text;
            }
            else
            {
                topFilterLable += " / Company Name: " + drpWebsiteUrl.SelectedItem.Text;
            }
        }

        if (chkViewDeleted.Checked == true)
        {
            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by View only deleted";
            }
            else
            {
                topFilterLable += " / View only deleted";
            }
        }

        if (chkHideClosed.Checked == true)
        {
            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by view closed";
            }
            else
            {
                topFilterLable += " / View closed";
            }
        }

        TopFilterLable.InnerHtml = topFilterLable;
        //*************************************************************************************************
        //sql = "Select * from ClientRequest" + strFilter + " " + lblOrderBy.Text;
        //sql = "Select top " + Convert.ToInt32(drpPerPage1.SelectedValue) + " * from ClientRequest" + strFilter + " " + lblOrderBy.Text;
        //DataSet ds = DatabaseHelper.getDataset(sql);
        DataSet ds = null;
        string filterAndOrderby = "";
        if (drpPerPage1.SelectedValue != "All")
        {
            filterAndOrderby = strFilter  + " " + lblOrderBy.Text;
            ds = DatabaseHelper.NewgetuspRequestDetail(filterAndOrderby, "");
        }
        else
        {
            filterAndOrderby = strFilter + " " + lblOrderBy.Text;
            ds = DatabaseHelper.NewgetuspRequestDetail(filterAndOrderby," Top "+ Convert.ToInt32(drpPerPage1.SelectedValue));
        }

        //*************************************************************************************************
        //ds = NewformatCrsDataSet(ds);

        
        try
        {
            if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0].Rows.Count <= 0)
            {
                divMessage.InnerHtml = "No request found.";
                divMessage.Visible = true;
                DataGrid1.DataSource = null;
                DataGrid1.DataBind();
                drpPerPage1.Visible = false;
            }
            else
            {

                //========================================================
                //if (ds.Tables[0].Rows.Count <= 100)
                if (ds.Tables[0].Rows.Count <= 25)
                //========================================================
                {
                    DataGrid1.PagerStyle.Visible = false;
                    DataGrid1.CurrentPageIndex = 0;
                }
                else
                {
                    DataGrid1.PagerStyle.Visible = true;
                }

                PagedDataSource objds = new PagedDataSource();
                objds.DataSource = ds.Tables[0].DefaultView;
                objds.AllowPaging = true;
                if (drpPerPage1.SelectedValue != "00")
                    objds.PageSize = Convert.ToInt16(Session["pagesize"]);
                else
                    objds.PageSize = ds.Tables[0].Rows.Count;

                LblRecordsno.Text = objds.DataSourceCount.ToString();
                

                divMessage.InnerHtml = "";
                divMessage.Visible = false;

                //for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                //{
                //    if (ds.Tables[0].Rows[i]["AddedBy"] != null && ds.Tables[0].Rows[i]["AddedBy"].ToString().Trim() != "")
                //    {
                //        ds.Tables[0].Rows[i]["AddedBy"] = ds.Tables[0].Rows[i]["AddedBy"].ToString();
                //    }
                //    else
                //    {
                //        ds.Tables[0].Rows[i]["AddedBy"] = ds.Tables[0].Rows[i]["UserName"].ToString();
                //    }
                //}


                if (drpPerPage1.SelectedValue != "00")
                    DataGrid1.PageSize = Convert.ToInt16(Session["pagesize"]);
                else
                    DataGrid1.PageSize = ds.Tables[0].Rows.Count;

                DataGrid1.DataSource = objds;
                //DataGrid1.DataSource = ds.Tables[0];
                DataGrid1.DataBind();

                if (DataGrid1.Items.Count == ds.Tables[0].Rows.Count)
                {
                    DataGrid1.PagerStyle.Visible = false;
                    DataGrid1.CurrentPageIndex = 0;
 
                }

            }
        }
        catch { }
    }

    private DataSet NewformatCrsDataSet(DataSet ds)
    {
        string strRequestid = "";
        try
        {

            ds.Tables[0].Columns.Add("ShowAllComment");
            ///ds.Tables[0].Columns.Add("");

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {



                    if (strRequestid == "")
                    {
                        strRequestid = ds.Tables[0].Rows[i]["RequestId"].ToString() + ",";
                    }
                    else
                    {
                        strRequestid = strRequestid + ds.Tables[0].Rows[i]["RequestId"].ToString() + ",";
                    }

                }
                catch { }
            }

            //******************************************************************************************************************
            strRequestid = strRequestid.TrimEnd(new char[] { ',' });
           //******************************************************************************************************************
        }
        catch
        {
            Server.ClearError();
        }
        return ds;
    }

    private void BindData2()
    {
        string strFilter = "";


        string topFilterLable = "";

        if (chkViewDeleted.Checked == true)
        {
            strFilter += " and ClientRequest_Details.deleted = 1 ";
        }
        else
        {
            strFilter += " and ClientRequest_Details.deleted <> 1 ";
        }

        if (chkHideClosed.Checked == true)
        {

        }
        else
        {
            strFilter += " and ClientRequest.status <> 'closed' ";
        }


        if (drpChangeBy.SelectedIndex != 0)
        {
            char[] separator = new char[] { '+' };

            string[] strCid = drpChangeBy.SelectedItem.Value.Split(separator);

            strFilter += " and ClientRequest.ClientId = " + strCid[0] + " and ClientRequest.UserName='" + drpChangeBy.SelectedItem.Text + "' ";

            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Change added by: " + drpChangeBy.SelectedItem.Text;
            }
            else
            {
                topFilterLable += " / Change added by: " + drpChangeBy.SelectedItem.Text;
            }
        }

        if (drpStatus.SelectedIndex != 0)
        {
            strFilter += " and ClientRequest.Status = '" + drpStatus.SelectedItem.Value + "' ";

            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Status: " + drpStatus.SelectedItem.Text;
            }
            else
            {
                topFilterLable += " / Status: " + drpStatus.SelectedItem.Text;
            }
        }
        else
        {
            // strFilter += " and ClientRequest.status <> 'closed' ";
        }

        if (drpPriority.SelectedIndex != 0)
        {
            strFilter += " and ClientRequest.Priority = " + drpPriority.SelectedItem.Value + " ";

            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Priority: " + drpPriority.SelectedItem.Text;
            }
            else
            {
                topFilterLable += " / Priority: " + drpPriority.SelectedItem.Text;
            }
        }

        if (drpWebsiteUrl.SelectedIndex != 0)
        {
            strFilter += " and CompanyName = '" + drpWebsiteUrl.SelectedItem.Value + "' ";

            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Company Name: " + drpWebsiteUrl.SelectedItem.Text;
            }
            else
            {
                topFilterLable += " / Company Name: " + drpWebsiteUrl.SelectedItem.Text;
            }
        }


        if (chkViewDeleted.Checked == true)
        {
            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by View only deleted";
            }
            else
            {
                topFilterLable += " / View only deleted";
            }
        }

        if (chkHideClosed.Checked == true)
        {
            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by view closed";
            }
            else
            {
                topFilterLable += " / View closed";
            }
        }

        TopFilterLable.InnerHtml = topFilterLable;



        /// sql = @"select * from ClientRequest where ClientRequest.[RequestId] in (select [RequestId] from ClientRequest_Details where [CommentId] not in (select  CommentId from read_CR_comments)) " + strFilter + " " + lblOrderBy.Text;
        sql = "select * from ClientRequest where RequestId in(select ClientRequest_Details.RequestId from ClientRequest_Details where [CommentId] not in (select  CommentId from read_CR_comments where read_CR_comments.username='" + Session["admin"].ToString() + "') " + strFilter + ")" + lblOrderBy.Text;

        DataSet ds = DatabaseHelper.getDataset(sql);
        ds = formatCrsDataSet(ds);

        try
        {

            if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0].Rows.Count <= 0)
            {
                divMessage.InnerHtml = "No request found.";
                divMessage.Visible = true;
                DataGrid1.DataSource = null;
                DataGrid1.DataBind();
                LblRecordsno.Text = "0";
            }
            else
            {



                if (ds.Tables[0].Rows.Count <= 25)
                {
                    DataGrid1.PagerStyle.Visible = false;
                    DataGrid1.CurrentPageIndex = 0;
                }
                else
                {
                    DataGrid1.PagerStyle.Visible = true;
                }

                PagedDataSource objds = new PagedDataSource();
                objds.DataSource = ds.Tables[0].DefaultView;

                //objds.PageSize = pagesize;
                objds.PageSize = Convert.ToInt16(Session["pagesize"]);

                LblRecordsno.Text = objds.DataSourceCount.ToString();
               

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (ds.Tables[0].Rows[i]["AddedBy"] != null && ds.Tables[0].Rows[i]["AddedBy"].ToString().Trim() != "")
                    {
                        ds.Tables[0].Rows[i]["AddedBy"] = ds.Tables[0].Rows[i]["AddedBy"].ToString();
                    }
                    else
                    {
                        ds.Tables[0].Rows[i]["AddedBy"] = ds.Tables[0].Rows[i]["UserName"].ToString();
                    }
                }

                divMessage.InnerHtml = "";
                divMessage.Visible = false;
               // DataGrid1.DataSource = ds.Tables[0];
                DataGrid1.DataSource = objds;
                DataGrid1.DataBind();


            }
        }
        catch { }
    }

    private void BindData3(string strsql)
    {

        //sql = "Select * from ClientRequest where ClientRequest.CRApprove='False' and ClientRequest.deleted <> 1  and ClientRequest.status <> 'closed'  and ClientRequest.Status = 'new'   ORDER BY LastUpdatedOn desc";
        DataSet ds = DatabaseHelper.getDataset(strsql);
        ds = formatCrsDataSet(ds);
        try
        {
            if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0].Rows.Count <= 0)
            {
                divMessage.InnerHtml = "No request found.";
                divMessage.Visible = true;
                DataGrid1.DataSource = null;
                DataGrid1.DataBind();
                drpPerPage1.Visible = false;
            }
            else
            {

                //========================================================
                //if (ds.Tables[0].Rows.Count <= 100)
                if (ds.Tables[0].Rows.Count <= 25)
                //========================================================
                {
                    DataGrid1.PagerStyle.Visible = false;
                    DataGrid1.CurrentPageIndex = 0;
                }
                else
                {
                    DataGrid1.PagerStyle.Visible = true;
                }

                PagedDataSource objds = new PagedDataSource();
                objds.DataSource = ds.Tables[0].DefaultView;
                objds.AllowPaging = true;
                if (drpPerPage1.SelectedValue != "00")
                    objds.PageSize = Convert.ToInt16(Session["pagesize"]);
                else
                    objds.PageSize = ds.Tables[0].Rows.Count;

                LblRecordsno.Text = objds.DataSourceCount.ToString();


                divMessage.InnerHtml = "";
                divMessage.Visible = false;

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (ds.Tables[0].Rows[i]["AddedBy"] != null && ds.Tables[0].Rows[i]["AddedBy"].ToString().Trim() != "")
                    {
                        ds.Tables[0].Rows[i]["AddedBy"] = ds.Tables[0].Rows[i]["AddedBy"].ToString();
                    }
                    else
                    {
                        ds.Tables[0].Rows[i]["AddedBy"] = ds.Tables[0].Rows[i]["UserName"].ToString();
                    }
                }


                if (drpPerPage1.SelectedValue != "00")
                    DataGrid1.PageSize = Convert.ToInt16(Session["pagesize"]);
                else
                    DataGrid1.PageSize = ds.Tables[0].Rows.Count;

                DataGrid1.DataSource = objds;
                //DataGrid1.DataSource = ds.Tables[0];
                DataGrid1.DataBind();

                if (DataGrid1.Items.Count == ds.Tables[0].Rows.Count)
                {
                    DataGrid1.PagerStyle.Visible = false;
                    DataGrid1.CurrentPageIndex = 0;

                }

            }
        }
        catch { }
    }

    







    private DataSet formatCrsDataSet(DataSet ds)
    {
        try
        {

            ds.Tables[0].Columns.Add("ShowAllComment");
            ///ds.Tables[0].Columns.Add("");

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {


                    if (ds.Tables[0].Rows[i]["ShortDescr"] != null && ds.Tables[0].Rows[i]["ShortDescr"].ToString().Trim() != "")
                    {

                        ds.Tables[0].Rows[i]["ShortDescr"] = "<a  href=\"edit_request.aspx?reqid=" + ds.Tables[0].Rows[i]["RequestId"].ToString() + "  \">" + ds.Tables[0].Rows[i]["ShortDescr"].ToString() + "</a>";

                    }
                    else
                    {

                        ds.Tables[0].Rows[i]["ShortDescr"] = "";
                    }



                    try
                    {

                        string LastComment = "";
                        /// Lastcomment2.Text = "";


                        sql = "Select top 1.* from ClientRequest_Details where deleted <> 1 and RequestId=" + ds.Tables[0].Rows[i]["RequestId"].ToString() + " order by CommentId desc";


                        DataSet dslastcomment = DatabaseHelper.getDataset(sql);

                        if (dslastcomment != null && dslastcomment.Tables[0].Rows.Count > 0)
                        {


                            for (int j = 0; j < dslastcomment.Tables[0].Rows.Count; j++)
                            {



                                LastComment += "<span style='color:Green'>" + "Comment " + dslastcomment.Tables[0].Rows[j]["CommentId"].ToString() + " posted by " + dslastcomment.Tables[0].Rows[j]["PostedBy"].ToString() + " on " + DateTime.Parse(dslastcomment.Tables[0].Rows[j]["PostedOn"].ToString()).ToString("dd MMM yyyy h:mm tt") + "</span>";

                                LastComment += "<br/><span>" + dslastcomment.Tables[0].Rows[j]["Comment"].ToString() + "</span>";

                                LastComment += "<span style=\"border-bottom: solid 1px green; margin-bottom: 2px; height:1px\"></span><br/><hr />";


                            }



                        }

                        ds.Tables[0].Rows[i]["ShowAllComment"] = LastComment.ToString();
                    }
                    catch
                    {

                    }





                }
                catch { }
            }
        }
        catch
        {
            Server.ClearError();
            ///Response.Redirect("fhs.aspx");
        }
        return ds;
    }
    protected void BindWebsiteURL()
    {
        // DataSet ds = DatabaseHelper.getWebsiteURL();
        DataSet ds = DatabaseHelper.getCompanyNameCR();

        if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0].Rows.Count <= 0)
        {
            ds = new DataSet();
            ds.Tables.Add();
            ds.Tables[0].Columns.Add("Company_Name");
        }
        DataRow row = ds.Tables[0].NewRow();
        row["Company_Name"] = "[no filter]";
        ds.Tables[0].Rows.InsertAt(row, 0);

        drpWebsiteUrl.DataSource = ds.Tables[0];
        drpWebsiteUrl.DataTextField = "Company_Name";
        drpWebsiteUrl.DataValueField = "Company_Name";
        drpWebsiteUrl.DataBind();
    }

    protected void BindPostedBy()
    {
        DataSet ds = DatabaseHelper.getRequestPostedBy();

        if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0].Rows.Count <= 0)
        {
            ds = new DataSet();
            ds.Tables.Add();
            ds.Tables[0].Columns.Add("Cid");
            ds.Tables[0].Columns.Add("UserName");
        }
        DataRow row = ds.Tables[0].NewRow();
        row["UserName"] = "[no filter]";
        row["Cid"] = "0";
        ds.Tables[0].Rows.InsertAt(row, 0);

        drpChangeBy.DataSource = ds.Tables[0];
        drpChangeBy.DataTextField = "UserName";
        drpChangeBy.DataValueField = "Cid";
        drpChangeBy.DataBind();
    }

    protected void DataGrid1_PageIndexChanged(object source,Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        DataGrid1.CurrentPageIndex = e.NewPageIndex;


        if (Session["filter"] == null || Session["admin"].ToString() == "")
        {


            BindData();
        }
        else
        {
            if (Session["filter"].ToString() == "NewCR")
            {

                BindData();
            }
            else if (Session["filter"].ToString() == "CRNewComment")
            {

                BindData2();

            }
            else
            {

                BindData();
            }
        }



        //// BindData();
    }

    protected void btnFilter_Click(object sender, EventArgs e)
    {
        //lblOrderBy.Text = " ORDER BY RequestId desc";
        if (Session["filter"] == null || Session["admin"].ToString() == "")
        {
            if (Session["filter"].ToString() == "NewCR")
            {

                BindData();
            }
            else if (Session["filter"].ToString() == "CRNewComment")
            {

                BindData2();

            }

            else
            {

                BindData();
            }

        }
        else
        {
            BindData();
        }


        /// BindData();
    }

    protected void DataGrid1_SortCommand(object source, GridSortCommandEventArgs e)
    {
        if (Session["reqColumn"] != null)
        {
            if (e.SortExpression.ToString() == Session["reqColumn"].ToString())
            {
                //Reverse the sort order
                if (Session["reqColumn"].ToString() == "CompanyName")
                {
                    if (Session["filter"] == null || Session["filter"].ToString() == "")
                    {

                        if (Session["reqOrder"].ToString() == "ASC")
                        {
                            lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " DESC";
                            Session["reqOrder"] = "DESC";
                        }
                        else
                        {
                            lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " ASC";
                            Session["reqOrder"] = "ASC";
                        }
                    }
                    else
                    {
                        if (Session["filter"].ToString() == "CRNewComment")
                        {

                            if (Session["reqOrder"].ToString() == "ASC")
                            {
                                lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " DESC";
                                Session["reqOrder"] = "DESC";
                            }
                            else
                            {
                                lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " ASC";
                                Session["reqOrder"] = "ASC";
                            }


                        }



                    }
                }
                else
                {
                    if (Session["reqOrder"] != null)
                    {
                        if (Session["reqOrder"].ToString() == "ASC")
                        {
                            lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " DESC";
                            Session["reqOrder"] = "DESC";
                        }
                        else
                        {
                            lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " ASC";
                            Session["reqOrder"] = "ASC";
                        }
                    }


                }
            }
            else
            {
                if (e.SortExpression.ToString() == "CompanyName")
                {
                    //Different column selected, so default to ascending order
                    if (Session["filter"] == null || Session["filter"].ToString() == "")
                    {
                        lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " ASC";
                        Session["reqOrder"] = "ASC";
                    }
                    else
                    {
                        if (Session["filter"].ToString() == "CRNewComment")
                        {
                            lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " ASC";
                            Session["reqOrder"] = "ASC";

                        }

                    }

                }
                else
                {
                    lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " ASC";
                    Session["reqOrder"] = "ASC";
                }
            }
        }

        Session["reqColumn"] = e.SortExpression.ToString();
        Session["reqOrderBy"] = lblOrderBy.Text;

        if (Session["filter"] == null || Session["filter"].ToString() == "")
        {
            BindData();
        }
        else
        {
            if (Session["filter"].ToString() == "NewCR")
            {

                BindData();
            }
            else if (Session["filter"].ToString() == "CRNewComment")
            {

                BindData2();

            }
            else
            {

                BindData();
            }
        }




    }

    protected void drpChangeBy_SelectedIndexChanged(object sender, EventArgs e)
    {
        //lblOrderBy.Text = " ORDER BY RequestId desc";
        setFileterSession();
        ///  BindData();
        ///  
        if (Session["filter"] == null || Session["filter"].ToString() == "")
        {
            BindData();
        }
        else
        {
            if (Session["filter"].ToString() == "NewCR")
            {

                BindData();
            }
            else if (Session["filter"].ToString() == "CRNewComment")
            {

                BindData2();

            }
            else
            {

                BindData();
            }
        }

    }

    protected void drpWebsiteUrl_SelectedIndexChanged(object sender, EventArgs e)
    {
        //lblOrderBy.Text = " ORDER BY RequestId desc";
        setFileterSession();
        /// BindData();
        if (Session["filter"] == null || Session["filter"].ToString() == "")
        {
            BindData();
        }
        else
        {
            if (Session["filter"].ToString() == "NewCR")
            {

                BindData();
            }
            else if (Session["filter"].ToString() == "CRNewComment")
            {

                BindData2();

            }
            else
            {

                BindData();
            }
        }

    }

    protected void drpStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        //lblOrderBy.Text = " ORDER BY RequestId desc";
        setFileterSession();
        /// BindData();
        /// 
        if (Session["filter"] == null || Session["filter"].ToString() == "")
        {
            BindData();
        }
        else
        {
            if (Session["filter"].ToString() == "NewCR")
            {

                BindData();
            }
            else if (Session["filter"].ToString() == "CRNewComment")
            {

                BindData2();

            }
            else
            {

                BindData();
            }
        }

    }

    protected void drpPriority_SelectedIndexChanged(object sender, EventArgs e)
    {
        //lblOrderBy.Text = " ORDER BY RequestId desc";
        setFileterSession();

        if (Session["filter"] == null || Session["filter"].ToString() == "")
        {
            BindData();
        }
        else
        {
            if (Session["filter"].ToString() == "NewCR")
            {

                BindData();
            }
            else if (Session["filter"].ToString() == "CRNewComment")
            {

                BindData2();

            }
            else
            {

                BindData();
            }
        }



    }

    protected void DataGrid1_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        //foreach (TableCell tc in e.Item.Cells)
        //{

        //    tc.Attributes["style"] = "border:1px solid #00A651;";


        //}
        //if (e.Item.ItemIndex != -1)
        if (e.Item is GridDataItem)
        {
            try
            {
                //**************************************************************************************
                GridDataItem dataItem = e.Item as GridDataItem;

                if (((Label)dataItem.FindControl("lblPriorityV")).Text == "0")
                {
                    ((Label)dataItem.FindControl("lblPriority")).Text = "0 - IMMEDIATE";
                    dataItem.BackColor = System.Drawing.Color.Pink;
                   //((Label)dataItem.FindControl("lblPriority")).BackColor = System.Drawing.Color.Pink;
                }
                else if (((Label)dataItem.FindControl("lblPriorityV")).Text == "1")
                {
                    ((Label)dataItem.FindControl("lblPriority")).Text = "1 - highest";
                }
                else if (((Label)dataItem.FindControl("lblPriorityV")).Text == "5")
                {
                    ((Label)dataItem.FindControl("lblPriority")).Text = "5 - lowest";
                }
                else if (((Label)dataItem.FindControl("lblPriorityV")).Text == "6")
                {
                    ((Label)dataItem.FindControl("lblPriority")).Text = "[no priority]";
                }
                else if (((Label)dataItem.FindControl("lblPriorityV")).Text == "8")
                {
                    ((Label)dataItem.FindControl("lblPriority")).Text = "1c - normal";
                    string htmlColor = "#ffeeee";
                    //((Label)dataItem.FindControl("lblPriority")).BackColor = System.Drawing.ColorTranslator.FromHtml(htmlColor);
                    dataItem.BackColor = System.Drawing.ColorTranslator.FromHtml(htmlColor);

                }
                else
                {
                    ((Label)dataItem.FindControl("lblPriority")).Text = ((Label)dataItem.FindControl("lblPriorityV")).Text;
                }
                //**************************************************************************************

                ((Label)e.Item.FindControl("lblChangeAddedOn")).Text = DateTime.Parse(((Label)e.Item.FindControl("lblChangeAddedOn")).Text).ToString("dd MMM yyyy h:mm tt").ToString();
                ((Label)e.Item.FindControl("lblLastUpdatedOn")).Text = DateTime.Parse(((Label)e.Item.FindControl("lblLastUpdatedOn")).Text).ToString("dd MMM yyyy h:mm tt").ToString();
                LinkButton readMarkButton = (LinkButton)e.Item.FindControl("lnkReadMark");
                LinkButton LinksupportType = (LinkButton)e.Item.FindControl("Supportype");

                //****************************************************************************
                //Label LblAllComment = (Label)e.Item.FindControl("lblshrtDesc");
                //LblAllComment.Attributes.Add("onmouseover", "get_(this);");
                //LblAllComment.Attributes.Add("onmouseout", "get_1(this);");
                //****************************************************************************
                int objIsRead;

                try
                {
                    //**************************************************************************************
                    //sql = "Select supportType,SupportUrl from NonesCRMusers where UserName in (select UserName from ClientRequest where RequestId = " + e.Item.Cells[0].Text + ")";
                    sql = "Select supportType,SupportUrl from NonesCRMusers where UserName in (select UserName from ClientRequest where RequestId = " + ((Label)dataItem.FindControl("lblRequestIdF")).Text + ")";
                    //**************************************************************************************
                    DataSet dsSupport = DatabaseHelper.getDataset(sql);

                    if (dsSupport != null && dsSupport.Tables[0].Rows.Count > 0)
                    {
                        if (dsSupport.Tables[0].Rows[0]["SupportUrl"].ToString() != "")
                        {
                            LinksupportType.Text = "<span style='color:red'>" + dsSupport.Tables[0].Rows[0]["supportType"].ToString() + "</span>";
                            if (dsSupport.Tables[0].Rows[0]["SupportUrl"].ToString().Contains("http://") == true)
                            {
                                LinksupportType.Attributes.Add("onclick", "window.open('" + dsSupport.Tables[0].Rows[0]["SupportUrl"].ToString() + "','',',,');");
                            }
                            else
                            {
                                LinksupportType.Attributes.Add("onclick", "window.open('" + "http://" + dsSupport.Tables[0].Rows[0]["SupportUrl"].ToString() + "','',',,');");
                            }
                            LinksupportType.Enabled = true;
                        }
                        else
                        {
                            LinksupportType.Text = "[<span style='color:Blue'>Not added</span>]";
                            LinksupportType.Enabled = false;
                            //LinksupportType.Attributes.Add("onclick", "window.open('" + dsSupport.Tables[0].Rows[0]["SupportUrl"].ToString() + "','',',,');");

                        }


                    }
                }
                catch { }

                try
                {
                    //************************************************************************************************************************************************************************************************************************************************************************************************
                    // sql = "select *  from ClientRequest_Details where [CommentId] not in (select CommentId from read_CR_comments where read_CR_comments.username='" + Session["admin"].ToString() + "') and ClientRequest_Details.deleted <> 1 and ClientRequest_Details.RequestId= " + e.Item.Cells[0].Text;
                    sql = "select *  from ClientRequest_Details where [CommentId] not in (select CommentId from read_CR_comments where read_CR_comments.username='" + Session["admin"].ToString() + "') and ClientRequest_Details.deleted <> 1 and ClientRequest_Details.RequestId= " + ((Label)dataItem.FindControl("lblRequestIdF")).Text;
                    //************************************************************************************************************************************************************************************************************************************************************************************************

                    // sql = "select * from read_comments where tc_id not in(select tc_id from read_comments where username ='" + Session["admin"] + "')and task_comments.deleted <> 1 and task_id =" + e.Item.Cells[0].Text;

                    DataSet ds = DatabaseHelper.getDataset(sql);

                    if (ds != null && ds.Tables[0].Rows.Count > 0)
                    {
                        //lblPosted.ForeColor = System.Drawing.Color.Red;

                        readMarkButton.Text = "[<span style='color:red'>Unread.</span>  Mark as read]";

                    }
                    else
                    {
                        /// lblPosted.ForeColor = System.Drawing.Color.Green;
                        readMarkButton.Text = "[<span style='color:green'>Read.</span> Mark as unread]";
                    }
                }
                catch
                {

                }
            }
            catch (Exception ex)
            { }
        }
    }

    protected void DataGrid1_ItemCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "ReadMark")
        {
            //******************************************************************************
            GridDataItem dataItem = e.Item as GridDataItem;
            //******************************************************************************
            LinkButton readMarkButton = (LinkButton)e.Item.FindControl("lnkReadMark");

            if (readMarkButton.Text == "[<span style='color:red'>Unread.</span>  Mark as read]")
            {
                //*************************************************************************************************************************************************************************************************************************************
                //sql = @"select *  from ClientRequest_Details where RequestId =" + e.Item.Cells[0].Text + " and  CommentId  not in (select  CommentId from read_CR_comments where read_CR_comments.UserName='" + Session["admin"].ToString() + "')";
                sql = @"select *  from ClientRequest_Details where RequestId =" + ((Label)dataItem.FindControl("lblRequestIdF")).Text + " and  CommentId  not in (select  CommentId from read_CR_comments where read_CR_comments.UserName='" + Session["admin"].ToString() + "')";
                
                //*****************************************************************************************************************************************************************************************************************************************
                /// sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

                DataSet ds = DatabaseHelper.getDataset(sql);

                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        try
                        {


                            string sqlinsert = "insert into read_CR_comments([CommentId],[UserName]) ";
                            sqlinsert += " values(" + ds.Tables[0].Rows[i]["CommentId"].ToString() + ",'" + Session["admin"].ToString() + "')";

                            int intResult = DatabaseHelper.executeNonQuery(sqlinsert);

                        }
                        catch
                        {
                        }
                    }

                }

                Response.Redirect("client_requests.aspx", false);
            }
            else
            {
               
                ////sql = @"delete from read_task where [task_id]=" + e.Item.Cells[0].Text + " and [username]='" + Session["admin"] + "'";
                ////DatabaseHelper.executeNonQuery(sql);

                //sql = @"select *  from ClientRequest_Details where RequestId =" + e.Item.Cells[0].Text + " and  CommentId  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "')";
                //**************************************************************************************************************************************************************************************************************************************
                //sql = @"select *  from ClientRequest_Details where RequestId =" + e.Item.Cells[0].Text + " and  CommentId  in (select  CommentId from read_CR_comments where read_CR_comments.UserName='" + Session["admin"].ToString() + "')";
                  sql = @"select *  from ClientRequest_Details where RequestId =" + ((Label)dataItem.FindControl("lblRequestIdF")).Text + " and  CommentId  in (select  CommentId from read_CR_comments where read_CR_comments.UserName='" + Session["admin"].ToString() + "')";
                //**************************************************************************************************************************************************************************************************************************************
                DataSet ds = DatabaseHelper.getDataset(sql);

                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        try
                        {

                            string sqlinsert = "Delete from read_CR_comments where CommentId =" + ds.Tables[0].Rows[i]["CommentId"].ToString() + " and UserName = '" + Session["admin"].ToString() + "'";
                            int intResult = DatabaseHelper.executeNonQuery(sqlinsert);

                        }
                        catch
                        {
                        }
                    }

                }

                Response.Redirect("client_requests.aspx", false);
            }
        }
    }

    private void setFileterSession()
    {
        if (Session["filter"] == null || Session["filter"].ToString() == "")
        {
            Session["reqHideClosed"] = chkHideClosed.Checked;
            Session["reqViewDeleted"] = chkViewDeleted.Checked;
            Session["reqChangedBy"] = drpChangeBy.SelectedValue;
            Session["reqStatus"] = drpStatus.SelectedValue;
            Session["reqPriority"] = drpPriority.SelectedValue;
            Session["reqWebsiteURL"] = drpWebsiteUrl.SelectedValue;
            Session["reqOrderBy"] = lblOrderBy.Text;
            Session.Remove("filter");
            ///BindData();
        }
        else
        {
            if (Session["filter"].ToString() == "CRNewComment")
            {

                Session["reqHideClosed"] = chkHideClosed.Checked;
                Session["reqViewDeleted"] = chkViewDeleted.Checked;
                Session["reqChangedBy"] = drpChangeBy.SelectedValue;
                Session["reqStatus"] = drpStatus.SelectedValue;
                Session["reqPriority"] = drpPriority.SelectedValue;
                Session["reqWebsiteURL"] = drpWebsiteUrl.SelectedValue;
                Session["reqOrderBy"] = lblOrderBy.Text;

            }
            if (Session["filter"].ToString() == "CRwocr")
            {

                Session["reqHideClosed"] = chkHideClosed.Checked;
                Session["reqViewDeleted"] = chkViewDeleted.Checked;
                Session["reqChangedBy"] = drpChangeBy.SelectedValue;
                Session["reqStatus"] = "awaiting client response- required";//drpStatus.SelectedValue;
                Session["reqPriority"] = drpPriority.SelectedValue;
                Session["reqWebsiteURL"] = drpWebsiteUrl.SelectedValue;
                Session["reqOrderBy"] = lblOrderBy.Text;

            }

        }


    }

    private void getFilterSession()
    {
        try
        {
            if (Session["filter"].ToString() == "CRwocr")
            {
                Session["reqStatus"] = "awaiting client response- required";
            }

            if (Session["reqHideClosed"] != null)
                chkHideClosed.Checked = (bool)Session["reqHideClosed"];

            if (Session["reqViewDeleted"] != null)
                chkViewDeleted.Checked = (bool)Session["reqViewDeleted"];

            if (Session["reqChangedBy"] != null)
                drpChangeBy.SelectedValue = Session["reqChangedBy"].ToString();

            if (Session["reqChangedBy"] != null)
                drpChangeBy.SelectedValue = Session["reqChangedBy"].ToString();


            if (Session["reqStatus"] != null)
                drpStatus.SelectedValue = Session["reqStatus"].ToString();

            if (Session["reqPriority"] != null)
                drpPriority.SelectedValue = Session["reqPriority"].ToString();

            if (Session["reqWebsiteURL"] != null)
                drpWebsiteUrl.SelectedValue = Session["reqWebsiteURL"].ToString();

            if (Session["reqOrderBy"] != null)
                lblOrderBy.Text = Session["reqOrderBy"].ToString();
        }
        catch
        {

        }
    }

    protected void btnClearFilters_Click(object sender, EventArgs e)
    {
        lblOrderBy.Text = " ORDER BY LastUpdatedOn desc";
        BindWebsiteURL();
        BindPostedBy();
        drpChangeBy.SelectedIndex = 0;
        drpStatus.SelectedIndex = 0;
        drpPriority.SelectedIndex = 0;
        drpWebsiteUrl.SelectedIndex = 0;
        setFileterSession();
        chkViewDeleted.Checked = false;
        chkHideClosed.Checked = false;
        Session.Remove("filter");

        BindData();
    }

    protected void chkViewDeleted_CheckedChanged(object sender, EventArgs e)
    {
        lblOrderBy.Text = " ORDER BY LastUpdatedOn desc";

        setFileterSession();
        if (Session["filter"] == null || Session["filter"].ToString() == "")
        {
            BindData();
        }
        else
        {
            if (Session["filter"].ToString() == "NewCR")
            {

                BindData();
            }
            else if (Session["filter"].ToString() == "CRNewComment")
            {

                BindData2();

            }
            else
            {

                BindData();
            }
        }
    }
    protected void chkHideClosed_CheckedChanged(object sender, EventArgs e)
    {
        setFileterSession();
        ///  BindData();
        ///  
        if (Session["filter"] == null || Session["filter"].ToString() == "")
        {
            BindData();
        }
        else
        {
            if (Session["filter"].ToString() == "NewCR")
            {

                BindData();
            }
            else if (Session["filter"].ToString() == "CRNewComment")
            {

                BindData2();

            }
            else
            {

                BindData();
            }
        }

    }
    protected void LnkMakecommentRead_Click(object sender, EventArgs e)
    {
        string sql = "select *  from ClientRequest_Details where [CommentId] not in (select  CommentId  from read_CR_comments where read_CR_comments.username='" + Session["admin"].ToString() + "') and ClientRequest_Details.deleted <> 1";
        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {
                    string sqlinsert = "insert into read_CR_comments([CommentId],[UserName]) ";
                    sqlinsert += " values(" + ds.Tables[0].Rows[i]["CommentId"].ToString() + ",'" + Session["admin"].ToString() + "')";

                    int intResult = DatabaseHelper.executeNonQuery(sqlinsert);
                }
                catch
                {
                }
            }


        }

        getCRNewComment();

    }

    protected void lnkUnansweredQuestions_Click(object sender, EventArgs e)
    {
        Session["filter"] = "Unanswered";
        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        Response.Redirect("tasks.aspx");
    }

    protected void lnkPr0Task_Click(object sender, EventArgs e)
    {

        Session["filter"] = "Immediate";
        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        Response.Redirect("tasks.aspx");
    }

    protected void lnkHighestTasks_Click(object sender, EventArgs e)
    {
        Session["filter"] = "HighestTasks";
        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        Response.Redirect("tasks.aspx");
    }

    protected void lnkHighTasks_Click(object sender, EventArgs e)
    {
        Session["filter"] = "HighTasks";
        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        Response.Redirect("tasks.aspx");
    }

   
    

   
  
    void getTskNewComment()
    {


       
    }

    protected void LnkTskNewcomment_Click(object sender, EventArgs e)
    {

        Session["filterunread"] = "TaskNewComment";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        Response.Redirect("tasks.aspx");


    }
    protected void LnkMakeTaskcommentRead_Click(object sender, EventArgs e)
    {
        sql = @"select *  from task_comments where task_id in (select task_id  from tasks where tasks.task_id = task_comments.task_id and  tasks.assigned_to_user = '" + Session["admin"].ToString() + "'  and tasks.deleted <> 1 and tasks.status <> 'closed') and task_comments.tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 ";
        /// sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {

                    //////string sqlgetcmntId = "select tc_id from task_comments where task_id ='" + ds.Tables[0].Rows[i]["task_id"].ToString() + "'";
                    //////object TskcmntID = DatabaseHelper.executeScalar(sqlgetcmntId);


                    //////if (TskcmntID!=null)
                    //////{
                    string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                    sqlinsert += " values(" + ds.Tables[0].Rows[i]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                    int intResult = DatabaseHelper.executeNonQuery(sqlinsert);
                    string sqlinsert2 = "insert into read_task([task_id],[username]) ";
                    sqlinsert2 += " values(" + ds.Tables[0].Rows[i]["task_id"].ToString() + ",'" + Session["admin"].ToString() + "')";
                    int intResult2 = DatabaseHelper.executeNonQuery(sqlinsert2);
                    //////}
                }
                catch
                {
                }
            }


        }

        getTskNewComment();
    }

 
    protected void LnkAllTsknewComment_Click(object sender, EventArgs e)
    {
        Session["filterunread"] = "AllTaskNewComment";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        Response.Redirect("tasks.aspx");
    }
    protected void LnkMakeAllTaskCmmntRead_Click(object sender, EventArgs e)
    {


        sql = @"select *  from task_comments where task_id in (select task_id  from tasks where tasks.task_id = task_comments.task_id) and task_comments.tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 ";
        /// sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {

                    string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                    sqlinsert += " values(" + ds.Tables[0].Rows[i]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                    int intResult = DatabaseHelper.executeNonQuery(sqlinsert);


                    string sqlinsert2 = "insert into read_task([task_id],[username]) ";
                    sqlinsert2 += " values(" + ds.Tables[0].Rows[i]["task_id"].ToString() + ",'" + Session["admin"].ToString() + "')";
                    int intResult2 = DatabaseHelper.executeNonQuery(sqlinsert2);

                    //////}
                }
                catch
                {
                }
            }


        }

        


    }
    
    protected void LnkBtnToDeleteAll_Click(object sender, EventArgs e)
    {

        try
        {

            int i = 0;
            foreach (GridDataItem row in DataGrid1.Items)
            {
                // Access the CheckBox 
                try
                {
                    CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                    if (cb != null && cb.Checked)
                    {
                        i = i + 1;

                        sql = "update ClientRequest set deleted=1 where [RequestId] =" + ((Label)row.FindControl("lblRequestIdF")).Text;

                        int intResult = DatabaseHelper.executeNonQuery(sql);

                        if (intResult != 0)
                        {


                            try
                            {

                                string sqlcomment = "select *  from ClientRequest_Details where [RequestId] = " + ((Label)row.FindControl("lblRequestIdF")).Text + " and [CommentId] not in (select  CommentId  from read_CR_comments where read_CR_comments.username='" + Session["admin"].ToString() + "')";
                                DataSet dscomment = DatabaseHelper.getDataset(sqlcomment);

                                if (dscomment != null && dscomment.Tables[0].Rows.Count > 0)
                                {
                                    for (int j = 0; j < dscomment.Tables[0].Rows.Count; j++)
                                    {
                                        try
                                        {
                                            string sqlinsert = "insert into read_CR_comments([CommentId],[UserName]) ";
                                            sqlinsert += " values(" + dscomment.Tables[0].Rows[j]["CommentId"].ToString() + ",'" + Session["admin"].ToString() + "')";

                                            int intCommentResult = DatabaseHelper.executeNonQuery(sqlinsert);
                                        }
                                        catch
                                        {
                                        }
                                    }


                                }
                            }
                            catch { }

                        }
                        
                    }
                }
                catch { }
            }
            if (i > 0)
            {
                Response.Redirect("client_requests.aspx", false);
            }
            else
            {
                Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('Please select CR to delete.');</script>");

                return;

            }
        }
        catch { Response.Redirect("client_requests.aspx", false); }


    }
    protected void LnkBtnToChngPR_Click(object sender, EventArgs e)
    {

        try
        {

            int i = 0;
            foreach (GridDataItem row in DataGrid1.Items)
            {
                // Access the CheckBox 
                try
                {
                    CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                    if (cb != null && cb.Checked)
                    {
                        i = i + 1;
                        sql = "update ClientRequest set Priority='" + DrpMultipleTaskPR.SelectedValue.ToString() + "' where [RequestId] =" + ((Label)row.FindControl("lblRequestIdF")).Text;

                        int intResult = DatabaseHelper.executeNonQuery(sql);

                    }
                }
                catch { }
            }
            if (i > 0)
            {
                Response.Redirect("client_requests.aspx", false);
            }
            else
            {
                Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('Please select CR to change priority.');</script>");

                return;

            }
        }
        catch { Response.Redirect("client_requests.aspx", false); }



    }
    protected void LnkChngStatus_Click(object sender, EventArgs e)
    {

        try
        {

            int i = 0;
            foreach (GridDataItem row in DataGrid1.Items)
            {
                // Access the CheckBox 
                try
                {
                    CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                    if (cb != null && cb.Checked)
                    {
                        i = i + 1;
                        sql = "update ClientRequest set Status = '" + DrpMultipleTaskStatus.SelectedValue.ToString() + "' where [RequestId] =" + ((Label)row.FindControl("lblRequestIdF")).Text;

                        int intResult = DatabaseHelper.executeNonQuery(sql);

                    }
                }
                catch { }
            }
            if (i > 0)
            {
                Response.Redirect("client_requests.aspx", false);
            }
            else
            {
                Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('Please select CR to change status.');</script>");

                return;

            }
        }
        catch { Response.Redirect("client_requests.aspx", false); }

    }
    protected void LnkBtnToMarkAsRead_Click(object sender, EventArgs e)
    {
        int i = 0;
        foreach (GridDataItem row in DataGrid1.Items)
        {
            try
            {
                CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                if (cb != null && cb.Checked)
                {
                    i = i + 1;

                    sql = @"select *  from ClientRequest_Details where RequestId =" + ((Label)row.FindControl("lblRequestIdF")).Text + " and  CommentId  not in (select  CommentId from read_CR_comments where read_CR_comments.UserName='" + Session["admin"].ToString() + "')";
                    /// sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

                    DataSet ds = DatabaseHelper.getDataset(sql);

                    if (ds != null && ds.Tables[0].Rows.Count > 0)
                    {
                        for (int j = 0; j < ds.Tables[0].Rows.Count; j++)
                        {
                            try
                            {


                                string sqlinsert = "insert into read_CR_comments([CommentId],[UserName]) ";
                                sqlinsert += " values(" + ds.Tables[0].Rows[j]["CommentId"].ToString() + ",'" + Session["admin"].ToString() + "')";

                                int intResult = DatabaseHelper.executeNonQuery(sqlinsert);

                            }
                            catch
                            {
                            }
                        }

                    }

                    /// Response.Redirect("client_requests.aspx", false);
                }


            }

            catch { }
        }
        if (i > 0)
        {
            Response.Redirect("client_requests.aspx", false);
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('Please select CR to mark as read.');</script>");

            return;

        }

    }
    protected void DrpMultipleTaskPR_SelectedIndexChanged(object sender, EventArgs e)
    {
        int i = 0;
        foreach (GridDataItem row in DataGrid1.Items)
        {
            try
            {
                CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                if (cb != null && cb.Checked)
                {
                    i = i + 1;

                    sql = @"update ClientRequest set priority='" + DrpMultipleTaskPR.SelectedItem.Value + "' where RequestId =" + ((Label)row.FindControl("lblRequestIdF")).Text;
                    DatabaseHelper.executeNonQuery(sql);
                    
                }


            }

            catch { }
        }
        if (i > 0)
        {
            Response.Redirect("client_requests.aspx", false);
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('Please select CR to mark as read.');</script>");

            return;

        }

    
    }

    protected void DrpMultipleTaskStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        int i = 0;
        foreach (GridDataItem row in DataGrid1.Items)
        {
            try
            {
                CheckBox cb = (CheckBox)row.FindControl("chkSelect");
                if (cb != null && cb.Checked)
                {
                    i = i + 1;


                    sql = @"update ClientRequest set status='" + DrpMultipleTaskStatus.SelectedItem.Text + "' where RequestId =" + ((Label)row.FindControl("lblRequestIdF")).Text;
                    DatabaseHelper.executeNonQuery(sql);

                }


            }

            catch { }
        }
        if (i > 0)
        {
            Response.Redirect("client_requests.aspx", false);
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "showmsg", "<script>  alert('Please select CR to mark as read.');</script>");

            return;

        }


    }
    protected void drpPerPage1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
           // pagesize = Convert.ToInt16(drpPerPage1.SelectedValue);
            Session["pagesize"] = drpPerPage1.SelectedValue;
            BindData();
        }
        catch (Exception ex)
        { }
    }
}
