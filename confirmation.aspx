<%@ Page Language="C#" AutoEventWireup="true" CodeFile="confirmation.aspx.cs" Inherits="confirmation" %>

<%@ Register Src="footer.ascx" TagName="footer" TagPrefix="uc2" %>

<%@ Register Src="Header.ascx" TagName="Header" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Logged out</title>
    <link href="StyleSheet.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <uc1:Header ID="Header1" runat="server" />
    <div style="text-align:center;">
    <div id="pageTitle" style="height:30px;text-align:left;font-weight:bold">Confirmation</div>
    <div id="divMessage" runat="server" style="height:300px;text-align:left;vertical-align:top"></div>    
    </div>
        
    </div>            <uc2:footer ID="Footer1" runat="server" />   
    </form>
</body>
</html>
