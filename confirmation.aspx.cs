using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class confirmation : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        divMessage.InnerHtml = "";
        if (Session["message"] != null)
        {
            if (Session["message"].ToString() != "")
            {
                divMessage.InnerHtml = Session["message"].ToString();
                Session.Remove("message");
                Session.Abandon();
            }
        }
    }
}
