﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="contact_us.aspx.cs" Inherits="contact_us" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Contact us</title>
<link href="css/default.css" rel="stylesheet" type="text/css" media="screen" />
<link href="Themes/330/1/styles.css" rel="stylesheet" type="text/css" media="screen" />
<style type="text/css">
<!--
body {
	background-color: #17561F;
}
.style1 {font-size:12px; color:#17561f; text-align:justify; font-family: tahoma;}
    .style2
    {
        padding: 0.5em;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 12px;
        color: #000000;
        text-align: justify;
        width: 302px;
    }
    .style3
    {
        width: 302px;
    }
-->
</style>
<script type="text/javascript">
<!--
function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
//-->
</script>
<script src="http://maps.google.com/maps?file=api&amp;v=2.x&amp;key=ABQIAAAAE6DGG2laj2PhDvaSmlO5DxRS31BufXR87GuZW0WL8UQ8hBRaAxR3EHcp-zV4BThMPSbrSCvKg-qXkQ" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript">
function showContactMap()
 {
    var map = new GMap(document.getElementById('map'));
    
	map.addControl(new GSmallMapControl()); 
	map.addControl(new GScaleControl()); 
	map.addControl(new GMapTypeControl()); 
		
	var point = new GLatLng(51.500152,-0.1234766); 
	var point2 = new GLatLng(51.500152,-0.126236); 
	
	map.centerAndZoom(point, 2);
	map.centerAndZoom(point2, 2);
	
	var marker = new GMarker(point); 
	var marker2 = new GMarker(point2); 
	map.addOverlay(marker);
	map.addOverlay(marker2);


}

</script>
</head>
<body onload="showContactMap();">
    <form id="form1" runat="server">
    <table width="909" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="908" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td rowspan="3" align="left" valign="bottom"><a href="fhs.aspx"><img src="images/logo.jpg" alt="Harmens estate agents Lettings and Estates. Letting property in Devon" width="450" height="109" class="logo logoContainer" title="Harmens estate agents Lettings and Estates. Letting property in Devon" /></a></td>
        <td height="58" align="right" valign="bottom"><img src="images/call.jpg" alt="Harmens estate agents" width="225" height="35"/></td>
      </tr>
      <tr>
        <td align="right" valign="top"><a href="mailto:lettings@scottbatesandcoles.co.uk"><img src="images/mail.jpg" alt="Harmens estate agents" width="184" height="35"/></a></td>
      </tr>
      <tr>
        <td align="right" valign="top" style="font-family:Tahoma, Verdana, Arial, Helvetica, sans-serif; font-size:13px; text-decoration:none; color:#FFFFFF; text-transform:none;"><img src="images/iconeproperty.png" width="182" height="20" border="0" usemap="#Map" />
            <map name="Map" id="Map">
              <area shape="rect" coords="66,-1,182,20" href="propertysearch.aspx" />
              <area shape="rect" coords="2,0,62,20" href="fhs.aspx" />
          </map></td>
      </tr>
      <tr>
        <td colspan="2" align="left" valign="top"><div>
            <script type="text/javascript">
function cntrlWebpages_MenuItem_MouseOver(o){var oparent = o.parentNode;
var ocurrentItemElements = o.getElementsByTagName("div");
var ocurrentItemMenu = null;
for ( var i = 0; i < ocurrentItemElements.length; i ++ ){
if ( ocurrentItemElements[i].id.indexOf("pnlItems") > 0 ){
ocurrentItemMenu = ocurrentItemElements[i];
break;
}
}
if ( ocurrentItemMenu ){
var ootherItemElements = oparent.getElementsByTagName("div");
for ( var i = 0; i < ootherItemElements.length; i ++ ){
if ( ootherItemElements[i].id.indexOf("pnlItems") > 0 ){
if ( ootherItemElements[i].id == ocurrentItemMenu.id ){
ocurrentItemMenu.style.display = ( ocurrentItemMenu.style.display == "block" ? "none" : "block" );
if ( typeof(cntrlWebpages_MenuItem_CheckFrame) != "undefined" ) { cntrlWebpages_MenuItem_CheckFrame( ocurrentItemMenu, 0 ); }
}else{
ootherItemElements[i].style.display = "none";
}
}
}
}
}
      </script>
            <div id="navbar">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td height="29" align="center" width="113px"><div align="center"><a href="our_company.html" class="nav_menu">OUR COMPANY</a></div></td>
                  <td width="1px">|</td>
                  <td width="113px"><div align="center"><a href="residential.html" class="nav_menu" >LETTINGS</a></div></td>
                  <td width="1px"> |</td>
                  <td width="112px"><div align="center"><a href="new_homes.html" class="nav_menu">SALES</a></div></td>
                  <td width="1px"> |</td>
                  <td width="113px"><div align="center"><a href="commerce.html" class="nav_menu">COMMERCIAL</a></div></td>
                  <td width="1px"> |</td>
                  <td width="113px"><div align="center"><a href="international.html" class="nav_menu">INTERNATIONAL</a></div></td>
                  <td width="1px"> |</td>
                  <td width="112px"><div align="center"><a href="mortgage.html" class="nav_menu">MORTGAGE</a></div></td>
                  <td width="1px"> |</td>
                  <td width="112px"><div align="center"><a href="careers.html" class="nav_menu">CAREERS</a></div></td>
                  <td width="1px"> |</td>
                  <td width="113px"><div align="center"><a href="contact_us.html" class="nav_menu">CONTACT US</a></div></td>
                </tr>
              </table>
            </div>
        </div></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="2px"></td>
  </tr>
  <tr>
    <td width="907px"><table width="908" border="0" align="left" cellpadding="0" cellspacing="0">
      <tr>
        <td height="35" valign="top" bgcolor="#FFFFFF" style="padding:0.5em; border-bottom:1px dotted #17561f; " class="sideheading"><strong>Contact us</strong></td>
      </tr>
      <tr>
        <td height="319" valign="top" bgcolor="#FFFFFF"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td align="center">
            	<div style="clear: both; height: 0px;"><img src="images/2.gif" width="1" height="1" />
	
                </div>
                <div id="map" style="width:690px; height:250px; border:1px solid #000000;"></div></div>
            <td width="19%" rowspan="3" valign="top" class="sideheading" bgcolor="#E9FFEC"><div align="center"><img src="images/smile copy.jpg" width="170" height="207" /></div></td>
            </tr>
          <tr>
            <td height="40" class="style1" style="padding-left:30px;"><strong> How much is your property worth? <br />
              Please include your details and we will get back to you as soon as possible.</strong></td>
            </tr>
          <tr>
            <td height="325" valign="top"><table width="668" border="0" align="center" cellpadding="0" cellspacing="0" class="texttable">
              <tr>
                <td width="92" class="ftext01"><div align="right">Title:</div></td>
                <td width="254" valign="bottom" class="ftext01">
                
                    <asp:DropDownList ID="ddlTitle" runat="server">
                        <asp:ListItem Value="0">Mr.</asp:ListItem>
                        <asp:ListItem Value="1">Mrs.</asp:ListItem>
                        <asp:ListItem Value="2">Ms</asp:ListItem>
                    </asp:DropDownList>
                              </td>
                <td width="10">&nbsp;</td>
                <td width="114" class="ftext01"><div align="right">Address: </div></td>
                <td rowspan="3" valign="top" class="style2"><label></label>
                  
                    <asp:TextBox ID="txtAddress" runat="server" style="width:225px;"></asp:TextBox>
                  </td>
              </tr>
              <tr>
                <td class="ftext01"><div align="right">First name:</div></td>
                <td class="ftext01">
                
                    <asp:TextBox ID="txtName" runat="server" style="width:225px;" Width="200px" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="txtName" ErrorMessage="Enter first name" 
                        ToolTip="Enter first name">*</asp:RequiredFieldValidator>
                </td>
                <td>&nbsp;</td>
                <td class="ftext01">&nbsp;</td>
              </tr>
              <tr>
                <td class="ftext01"><div align="right">Surname: </div></td>
                <td class="ftext01">
                
                    <asp:TextBox ID="txtSurname" runat="server" style="width:225px;" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        ControlToValidate="txtSurname" ErrorMessage="Enter surname name" 
                        ToolTip="Enter surname name">*</asp:RequiredFieldValidator>
                </td>
                <td>&nbsp;</td>
                <td class="ftext01">&nbsp;</td>
              </tr>
              <tr>
                <td class="ftext01"><div align="right">Telephone: </div></td>
                <td class="ftext01">
                
                    <asp:TextBox ID="txtTelephone" runat="server" style="width:225px;"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                        ControlToValidate="txtTelephone" ErrorMessage="Enter telephone number" 
                        ToolTip="Enter telephone number">*</asp:RequiredFieldValidator>
                </td>
                <td>&nbsp;</td>
                <td align="right" class="ftext01"><div align="right">Email:</div></td>
                <td class="style2">
      
                    <asp:TextBox ID="txtEmail" runat="server" style="width:225px;"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                        ControlToValidate="txtEmail" ErrorMessage="Enter email address" 
                        ToolTip="Enter email address">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                        ControlToValidate="txtEmail" ErrorMessage="Enter valid email address" 
                        ToolTip="Enter valid email address" 
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                </td>
              </tr>
              <tr>
                <td class="ftext01"><div align="right" style="width: 89px">Mobile number: </div></td>
                <td class="ftext01">
                
                    <asp:TextBox ID="txtMobile" runat="server" style="width:225px;"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
                <td class="ftext01">&nbsp;</td>
                <td align="right" class="style3">
                
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" BackColor="#339933" 
                        onclick="btnSubmit_Click" />
                    
                </td>
              </tr>
              <tr>
                <td colspan="2" class="ftext01"><strong>About your Property :</strong></td>
                <td>&nbsp;</td>
                <td colspan="2" align="right">
                    <asp:Label ID="lblMessage" runat="server" Font-Bold="True" Font-Size="10pt" 
                        ForeColor="#FF3300" Text="Label" Visible="False"></asp:Label>
                                                    </td>
              </tr>
              <tr>
                <td class="ftext01"><div align="right">Bedrooms:</div></td>
                <td class="ftext01">
                    <asp:DropDownList ID="ddlBed" runat="server" style="width:60px;"  >
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                        <asp:ListItem>9</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>&nbsp;</td>
                <td class="ftext01">&nbsp;</td>
                <td class="style2">&nbsp;</td>
              </tr>

            </table></td>
            </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="10px"></td>
  </tr>
  <tr>
    <td><div class="footerBelt" style="background:url(images/footer-bg.jpg) no-repeat center ; width:908px; height:39px;"></div></td>
  </tr>
  <tr>
    <td><div class="footer"><div class="copyRight" style="font-family:tahoma, Verdana, Arial, Helvetica, sans-serif; color:#FFFFFF; font-size:12px; text-decoration:none;">Harmens Estate Agents | Brixton Office - 0207 737 6000 | Streatham Office - 020 8769 477</div>		</div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
    </form>
</body>
</html>
