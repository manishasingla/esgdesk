﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;

using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;


public partial class contact_us : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string  RetMail1;
        string  RetMail2;
        string emailid;
        emailid = System.Configuration.ConfigurationManager.AppSettings["ToEmailId"].ToString();
        string strBody = "";
        strBody = GenerateHTML();
        RetMail1 = DatabaseHelper.SendEmailListnew(emailid, "Contact us information from: " + txtName.Text, strBody);
        RetMail2 = DatabaseHelper.SendEmailListnew(txtEmail.Text, "A copy of your contact us information " + ConfigurationManager.AppSettings["CompanyName"], strBody);
        if (RetMail1 == "true")
        {
            lblMessage.Text = "Thanks – your  information has been sent.";
            lblMessage.Visible = true;
            clearControls();
        }
        else
        {
            lblMessage.Text = "There was an error in sending mail. Please try again....";
            lblMessage.Visible = true;
        }
        
    }
    private string GenerateHTML()
    {
        ///string sb = "";
        string strHeader = "";
        string serverpath = ConfigurationManager.AppSettings["WebAddress"];
        //body of mail for the person
        strHeader += "<br>";
        strHeader += "Contact us details";
        strHeader += "<br><br>";
        strHeader += "Please keep this email for your records. Contact information is as follows:  ";
        strHeader += "<br><br>";

        if (txtName.Text.Trim() != "")
        {
            strHeader += "Name: ";
            strHeader += ddlTitle.SelectedItem.ToString()+" "+ txtName.Text +" "+ txtSurname.Text;
            strHeader += "<br>";
        }
        if (txtTelephone.Text.Trim() != "")
        {
            strHeader += "Telephone number: ";
            strHeader += txtTelephone.Text;
            strHeader += "<br>";
        }
        if (txtMobile.Text.Trim() != "")
        {
            strHeader += "Mobile number: ";
            strHeader += txtMobile.Text;
            strHeader += "<br>";
        }

        if (txtAddress.Text.Trim() != "")
        {
            strHeader += "Address: ";
            strHeader += txtAddress.Text;
            strHeader += "<br>";
        }

        if (txtEmail.Text.Trim() != "")
        {
            strHeader += "Email Address: ";
            strHeader += txtEmail.Text;
            strHeader += "<br>";
        }

       
            strHeader += "About his property: ";
            strHeader +=ddlBed.SelectedItem.ToString();
            strHeader += "<br>";
        
        
        strHeader += "------------------------------------";
        strHeader += "<br><br>";
        strHeader += ConfigurationManager.AppSettings["CompanyName"];
        strHeader += "<br>";
        strHeader += ConfigurationManager.AppSettings["WebAddress"];

        return strHeader;
    }

    private void clearControls()
    {
        lblMessage.Text = string.Empty;
        txtName.Text = string.Empty;
        txtSurname.Text = string.Empty;
        txtAddress.Text = string.Empty;
        txtEmail.Text = string.Empty;
        txtMobile.Text = string.Empty;
        ddlBed.SelectedIndex = 0;
        ddlTitle.SelectedIndex = 0;
       
    }
    //public static bool sendEmail(string strTo, string strSubject, string strBody)
    //{
    //    bool blndone = false;
    //    try
    //    {
    //        System.Web.Mail.MailMessage msg = new System.Web.Mail.MailMessage();
    //        msg.To = strTo;
    //        msg.From = ConfigurationManager.AppSettings["FromEmailId"];
    //        msg.Subject = strSubject;
    //        msg.Body = strBody;
    //        msg.Priority = System.Web.Mail.MailPriority.High;
    //        msg.BodyFormat = System.Web.Mail.MailFormat.Html;
    //        System.Web.Mail.SmtpMail.Send(msg);
    //        blndone = true;
    //    }
    //    catch { blndone = false; }
    //    return blndone;
    //}

}
