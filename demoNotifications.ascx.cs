﻿using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System;
using System.Data;

public partial class Admin_demoNotifications : System.Web.UI.UserControl
{

    int id;
    int reqid;
    string sql = "";


    protected static string TskID;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
            {
                firstpipe.Visible = false;
                secondpipe.Visible = false;
                allsign.Visible = false;
                CRsign.Visible = false;
            }
            else
            {
                firstpipe.Visible = true;
                secondpipe.Visible = true;
                allsign.Visible =true ;
                CRsign.Visible = true;
            }

            //LnkMakecommentRead.Attributes.Add("onclick", "return confirm('Sure you want to mark every comment in every CR as read by you?');");
            LnkMakeTaskcommentRead.Attributes.Add("onclick", "javascript:return confirm('Sure you want to mark every comment in each of your tasks as read by you (this means you must have read them all and acted on all that require action already)?');");
            LnkMakeAllTaskCmmntRead.Attributes.Add("onclick", "javascript:return confirm('Sure you want to mark every comment in each of tasks as read by you (this means you must have read them all and acted on all that require action already)?');");


            if (Session["admin"] == null)
            {
                return;
            }
            else
            {
                getUnasweredQuestions();
                getTobeAnswered();
                //=====Not Required=====================
                //getCRwaitingfrmClient();
                //======================================
                getPR0Tasks();
                getPR1aTasks();
                getPR1bTasks();
                getPR1cTasks();
                getPRLowTasks();
                getNewCR();
                getOverduetasks();
                getAllOverduetasks();
                getCRNewComment();
                getTskNewComment();
                getAllTskNewComment();

            }

        }
    }




    void getPR1bTasks()
    {
        sql = @"select tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '1b - high' ";

        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            sql += " and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + Session["admin"] + "' ";
        }
        else
        {
            sql += " and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + Session["admin"] + "' ";
        }

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            lnkHighTasks.Text = "(" + ds.Tables[0].Rows.Count + ")";
            highTasks.Visible = true;
        }
        else
        {
            lnkHighTasks.Text = "(0)";
            highTasks.Visible = false;
        }
    }
    protected void lnkHighTasks_Click(object sender, EventArgs e)
    {
        //Session["boolclose"] = "true";
        Session["filter"] = "HighTasks";
        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        //===============================
        Session["AllOverdueTask"] = "";
        //===============================
        Response.Redirect("tasks.aspx");
    }

    void getNewCR()
    {
        sql = @"select ClientRequest.* from ClientRequest 
                where ClientRequest.Status = 'new' and status <> 'closed' and deleted <> 1 ";


        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {

            if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
            {
                /// LnkNewCR.Text = "(" + ds.Tables[0].Rows.Count + ")";
                NewCR.Visible = false;
            }
            else
            {
                LnkNewCR.Text = "(" + ds.Tables[0].Rows.Count + ")";
                NewCR.Visible = true;
            }

        }
        else
        {


            if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
            {
                /// LnkNewCR.Text = "(" + ds.Tables[0].Rows.Count + ")";
                NewCR.Visible = false;
            }
            else
            {
                LnkNewCR.Text = "(0)";
                NewCR.Visible = false;
            }


        }
    }

    protected void LnkNewCR_Click(object sender, EventArgs e)
    {
        Session["boolclose"] = true;
        Session["boolcount"] = "false";
        Session["filter"] = "NewCR";
        //===============================
        Session["AllOverdueTask"] = "";
        //===============================
        Response.Redirect("client_requests.aspx", false);
        /// bindPR1bTasks();

    }
    void getUnasweredQuestions()
    {
        //////        sql = @"select task_comments.* from task_comments,tasks 
        //////                where tasks.task_id = task_comments.task_id and tasks.deleted <> 1 and tasks.status <> 'closed' and task_comments.deleted <> 1 and qflag=1 ";

        //////               sql += " and assigned_to_user = '" + Session["admin"] + "' ";

         //=============================================================================================
        //sql = @"select task_comments.* from task_comments,tasks 
                //where tasks.task_id = task_comments.task_id and tasks.deleted <> 1 and tasks.status <> 'closed' and task_comments.deleted <> 1 and qflag=1 ";
        //sql += " and task_comments.QuesTo = '" + Session["admin"] + "' ";

        
       
        sql = @"select distinct tasks.* from task_comments,tasks 
                where tasks.task_id = task_comments.task_id and tasks.deleted <> 1 and task_comments.deleted <> 1 and qflag=1 ";

        //if (!DatabaseHelper.can_Show_Closed_Tasks(Session["admin"].ToString()))
        //{
            sql += " and status <> 'closed' ";
       // }

        sql += " and task_comments.QuesTo = '" + Session["admin"] + "' ";
        //=============================================================================================
        
        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            lnkUnansweredQuestions.Text = "(" + ds.Tables[0].Rows.Count + ")";
            unansweredque.Visible = true;
        }
        else
        {
            lnkUnansweredQuestions.Text = "(0)";
            unansweredque.Visible = false;
        }
    }
    protected void lnkUnansweredQuestions_Click(object sender, EventArgs e)
    {
        //Session["boolclose"] = "true";
        Session["filter"] = "Unanswered";
        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        //===============================
        Session["AllOverdueTask"] = "";
        //===============================
        Response.Redirect("tasks.aspx");
    }

//    void getCRwaitingfrmClient()
//    {
//        sql = @"select ClientRequest.* from ClientRequest 
//                where ClientRequest.Status = 'awaiting client response- required' and status <> 'closed' and deleted <> 1 ";



//        DataSet ds = DatabaseHelper.getDataset(sql);

//        if (ds != null && ds.Tables[0].Rows.Count > 0)
//        {

//            if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
//            {

//                CRwocr.Visible = false;
//            }
//            else
//            {
//                LnkCRWocr.Text = "(" + ds.Tables[0].Rows.Count + ")";
//                CRwocr.Visible = true;
//            }

//        }
//        else
//        {


//            if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
//            {

//                CRwocr.Visible = false;
//            }
//            else
//            {
//                LnkCRWocr.Text = "(0)";
//                CRwocr.Visible = false;
//            }


//        }
//    }

    protected void LnkCRWocr_Click(object sender, EventArgs e)
    {
        // Session["boolclose"] = "true";
        Session["filter"] = "CRwocr";
        //===============================
        Session["AllOverdueTask"] = "";
        //===============================
        Response.Redirect("client_requests.aspx", false);


    }

    void getPR0Tasks()
    {
        sql = @"select tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '0 - IMMEDIATE' ";

        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            sql += " and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + Session["admin"] + "' ";
        }
        else
        {
           sql += " and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + Session["admin"] + "' ";
        }

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            lnkPr0Task.Text="("+ds.Tables[0].Rows.Count+")";
            immediatetask.Visible = true;
        }
        else
        {
            lnkPr0Task.Text = "(0)";
            immediatetask.Visible = false;
        }
    }

    protected void lnkPr0Task_Click(object sender, EventArgs e)
    {
        // Session["boolclose"] = "true";
        Session["filter"] = "Immediate";
        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        //===============================
        Session["AllOverdueTask"] = "";
        //===============================
        Response.Redirect("tasks.aspx");
    }

    void getPR1aTasks()
    {
        sql = @"select tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '1a - DO NOW' ";

        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            sql += " and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + Session["admin"] + "' ";
        }
        else
        {
            sql += " and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + Session["admin"] + "' ";
        }

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            lnkHighestTasks.Text = "(" + ds.Tables[0].Rows.Count + ")";
            highestTasks.Visible = true;
        }
        else
        {
            lnkHighestTasks.Text = "(0)";
            highestTasks.Visible = false;
        }
    }
    protected void lnkHighestTasks_Click(object sender, EventArgs e)
    {


        Session["fil"] = "1a - DO NOW";
        Session["filter"] = "1a - DO NOW";
        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        //===============================
        Session["AllOverdueTask"] = "";
        //===============================
        Response.Redirect("tasks.aspx");
    }
    void getPR1cTasks()
    {

        sql = @"select tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '1c - normal' ";
        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            sql += " and  status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + Session["admin"] + "' ";
        }
        else
        {
            sql += " and  status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + Session["admin"] + "' ";
        }

        DataSet ds = DatabaseHelper.getDataset(sql);
        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            Lnk1cTasks.Text = "(" + ds.Tables[0].Rows.Count + ")";
            PR1CTask.Visible = true;
        }
        else
        {
            Lnk1cTasks.Text = "(0)";
            PR1CTask.Visible = false;
        }
    }

    protected void Lnk1cTasks_Click(object sender, EventArgs e)
    {

        Session["filter"] = "1c - normal";
        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        //===============================
        Session["AllOverdueTask"] = "";
        //===============================
        Response.Redirect("tasks.aspx");
    }

    void getOverduetasks()
    {

        try
        {

            int GvnETC = 0;
            int j = 0;
            DateTime crrntdate = DateTime.Now.Date;
            DateTime duedate;
            sql = @"select tasks.* from tasks 
                        where tasks.deleted <> 1 and  status <> 'closed' and  status <> 'checked' and status <>'parked'";

            //if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
            //{
                sql += " and assigned_to_user = '" + Session["admin"] + "' ";
            //}

            DataSet ds = DatabaseHelper.getDataset(sql);

            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {




                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //========================================================================================
                    //string strcrrntdate = DateTime.Parse(DateTime.Now.Date.ToString()).ToString("dd/MM/yyyy");
                      string strcrrntdate = string.Format("{0:dd/MM/yyyy}", DateTime.Now.Date.ToString()).ToString();
                    //========================================================================================
                      string djgfjs = ds.Tables[0].Rows[i]["DueDate"].ToString();
                    if (ds.Tables[0].Rows[i]["DueDate"].ToString() != null && ds.Tables[0].Rows[i]["DueDate"].ToString() != "")
                    {
                        //=====================================================================================================
                        string strduedate = string.Format("{0:dd/MM/yyyy}", ds.Tables[0].Rows[i]["DueDate"].ToString()).ToString();
                        duedate = DateTime.Parse(strduedate.ToString());
                        crrntdate = DateTime.Parse(strcrrntdate.ToString());
                        //string strduedate = DateTime.Parse(ds.Tables[0].Rows[i]["DueDate"].ToString()).ToString("dd/MM/yyyy");
                        //duedate = Convert.ToDateTime(strduedate.ToString());
                        //crrntdate = Convert.ToDateTime(strcrrntdate.ToString());
                        //=====================================================================================================
                        
                        if (DateTime.Compare(duedate, crrntdate) < 0)
                        {

                            j++;

                        }


                    }

                }


                if (j > 0)
                {
                    BtnOverdue.Text = "(" + j.ToString() + ")";
                    OverDuetsk.Visible = true;
                }
                else
                {
                    BtnOverdue.Visible = false;
                    OverDuetsk.Visible = false;
                }


            }
            else
            {
                BtnOverdue.Visible = false;
                OverDuetsk.Visible = false;
            }

        }
        catch { }

    }

    protected void BtnOverdue_Click(object sender, EventArgs e)
    {

        Session["filter"] = " ";
        Session["filterunread"] = "";
        Session["OverdueTask"] = "True";
        //===============================
        Session["AllOverdueTask"] = "";
        //===============================
        Response.Redirect("tasks.aspx");


    }

    void getCRNewComment()
    {
        sql = "select ClientRequest.* from ClientRequest where RequestId in(select ClientRequest_Details.RequestId from ClientRequest_Details where [CommentId] not in (select  CommentId from read_CR_comments where read_CR_comments.username='" + Session["admin"].ToString() + "') and ClientRequest_Details.deleted <> 1)";
        DataSet ds = DatabaseHelper.getDataset(sql);
        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
            {
                /// LnkNewCR.Text = "(" + ds.Tables[0].Rows.Count + ")";
                CrNewComment.Visible = false;
                LnkMakecommentRead.Visible = false;
            }
            else
            {
                LnkCrNewComment.Text = "(" + ds.Tables[0].Rows.Count + ")";
                LnkMakecommentRead.Text = "(" + "-" + ")";
                CrNewComment.Visible = true;
                LnkMakecommentRead.Visible = true;
            }

        }
        else  
        { 

            if (!DatabaseHelper.can_Show_Client_Requests(Session["admin"].ToString()))
            {
                CrNewComment.Visible = false;
                LnkMakecommentRead.Visible = false;
            }
            else
            {
                LnkCrNewComment.Text = "(0)";
                CrNewComment.Visible = false;
                LnkMakecommentRead.Visible = false;
            }




        }
    }

    protected void LnkCrNewComment_Click(object sender, EventArgs e)
    {
        Session["boolclose"] = "true";
        Session["boolcount"] = "false";
        Session["filter"] = "CRNewComment";
        //===============================
        Session["AllOverdueTask"] = "";
        //===============================
        Response.Redirect("client_requests.aspx", false);
    }

    protected void LnkMakecommentRead_Click(object sender, EventArgs e)
    {
        Session["boolclose"] = "true";
        Session["boolcount"] = "false";
        string sql = "select *  from ClientRequest_Details where [CommentId] not in (select  CommentId  from read_CR_comments where read_CR_comments.username='" + Session["admin"].ToString() + "') and ClientRequest_Details.deleted <> 1";
        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {
                    string sqlinsert = "insert into read_CR_comments([CommentId],[UserName]) ";
                    sqlinsert += " values(" + ds.Tables[0].Rows[i]["CommentId"].ToString() + ",'" + Session["admin"].ToString() + "')";
                    int intResult = DatabaseHelper.executeNonQuery(sqlinsert);
                }
                catch
                {
                }
            }


        }

        getCRNewComment();

    }

    void getTskNewComment()
    {
        sql = "";
        LnkTskNewcomment.Text = "";
        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {

            sql = "select tasks.* from tasks where task_id in(select task_comments.task_id from task_comments where tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 )";

            sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

        }
        else
        {
            sql = "select tasks.* from tasks where task_id in(select task_comments.task_id from task_comments where tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 )";

            sql += "and tasks.deleted <> 1 and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";


        }
        DataSet dsNewTskCmnt = DatabaseHelper.getDataset(sql);

        if (dsNewTskCmnt != null && dsNewTskCmnt.Tables[0].Rows.Count > 0)
        {
            LnkTskNewcomment.Text = "(" + dsNewTskCmnt.Tables[0].Rows.Count + ")";
            LnkMakeTaskcommentRead.Text = "(" + "-" + ")";
            LnkMakeTaskcommentRead.Visible = true;
            TskNewComment.Visible = true;
        }
        else
        {
            LnkTskNewcomment.Text = "(0)";
            TskNewComment.Visible = false;
            LnkMakeTaskcommentRead.Visible = false;
        }
    }

    protected void LnkTskNewcomment_Click(object sender, EventArgs e)
    {
        Session["boolclose"] = "true";
        Session["filter"] = "";
        Session["filterunread"] = "TaskNewComment";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        //===============================
        Session["AllOverdueTask"] = "";
        //===============================
        Session["boolcount"] = "false";
        Response.Redirect("tasks.aspx");


    }
    protected void LnkMakeTaskcommentRead_Click(object sender, EventArgs e)
    {

        sql = @"select *  from task_comments where task_id in (select task_id  from tasks where tasks.task_id = task_comments.task_id and  tasks.assigned_to_user = '" + Session["admin"].ToString() + "'  and tasks.deleted <> 1 and tasks.status <> 'closed') and task_comments.tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 ";
        /// sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {


                    string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                    sqlinsert += " values(" + ds.Tables[0].Rows[i]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                    int intResult = DatabaseHelper.executeNonQuery(sqlinsert);

                    string sqlinsert2 = "insert into read_task([task_id],[username]) ";
                    sqlinsert2 += " values(" + ds.Tables[0].Rows[i]["task_id"].ToString() + ",'" + Session["admin"].ToString() + "')";
                    int intResult2 = DatabaseHelper.executeNonQuery(sqlinsert2);

                }
                catch
                {
                }
            }


        }

        sql = @"select *  from task_comments where task_id in (select task_id  from tasks where tasks.task_id = task_comments.task_id and  tasks.assigned_to_user = '" + Session["admin"].ToString() + "'  and tasks.deleted <> 1 and tasks.status = 'closed') and task_comments.tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 ";
        /// sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

        DataSet ds1 = DatabaseHelper.getDataset(sql);

        if (ds1 != null && ds1.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds1.Tables[0].Rows.Count; i++)
            {
                try
                {


                    string sqlinsert = "insert into read_comments([tc_id],[username]) ";
                    sqlinsert += " values(" + ds1.Tables[0].Rows[i]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

                    int intResult = DatabaseHelper.executeNonQuery(sqlinsert);

                    string sqlinsert2 = "insert into read_task([task_id],[username]) ";
                    sqlinsert2 += " values(" + ds1.Tables[0].Rows[i]["task_id"].ToString() + ",'" + Session["admin"].ToString() + "')";
                    int intResult2 = DatabaseHelper.executeNonQuery(sqlinsert2);

                }
                catch
                {
                }
            }


        }


        // getTskNewComment();
        Response.Redirect("tasks.aspx", false);
    }


    void getAllTskNewComment()
    {

        //sql = "select tasks.* from tasks where task_id in(select task_comments.task_id from task_comments where tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 )";
        sql = "Select t.*,tc.*,(select count(*) as commenttype from task_comments where tc_id not in";
        sql += " (select tc_id from read_comments where username ='" + Session["admin"].ToString() + "')and task_comments.deleted <> 1 and task_id =t.task_id)";
        sql += " from tasks t,task_comments tc where t.task_id in (select task_comments.task_id from task_comments where tc_id not in";
        sql += " (select tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "' )and task_comments.deleted <> 1) and t.task_id = tc.task_id and tc.post_date =";
        sql += " (select top 1 post_date from task_comments where tc.task_id = task_id order by post_date desc)";
        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables.Count >0 && ds.Tables[0].Rows.Count > 0)
        {
            if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
            {
                //********************************************
                LnkAllTsknewComment.Text = "(0)";
                SpnAllTaskNewComment.Visible = false;
                LnkMakeAllTaskCmmntRead.Visible = false;
                LnkAllTsknewComment.Visible = false;
                //********************************************
                

            }
            else
            {

                //********************************************

                LnkAllTsknewComment.Text = "(" + ds.Tables[0].Rows.Count + ")";
                LnkMakeAllTaskCmmntRead.Text = "(" + "-" + ")";
                LnkMakeAllTaskCmmntRead.Visible = true;
                SpnAllTaskNewComment.Visible = true;
                //********************************************
                
            }
        }
        else
        {
            //********************************************
            LnkAllTsknewComment.Text = "(0)";
            SpnAllTaskNewComment.Visible = false;
            LnkMakeAllTaskCmmntRead.Visible = false;
            LnkAllTsknewComment.Visible = false;
            //********************************************
            
        }


    }

    protected void LnkAllTsknewComment_Click(object sender, EventArgs e)
    {
        Session["boolclose"] = "true";
        Session["filter"] = "";
        Session["filterunread"] = "AllTaskNewComment";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        //===============================
        Session["AllOverdueTask"] = "";
        //===============================
        Session["boolcount"] = "true";
        Response.Redirect("tasks.aspx");
    }
    protected void LnkMakeAllTaskCmmntRead_Click(object sender, EventArgs e)
    {


    //    sql = @"select *  from task_comments where task_id in (select task_id  from tasks where tasks.task_id = task_comments.task_id ) and task_comments.tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 ";
    //    /// sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

    //    DataSet ds = DatabaseHelper.getDataset(sql);

    //    if (ds != null && ds.Tables[0].Rows.Count > 0)
    //    {
    //        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
    //        {
    //            try
    //            {

    //                string sqlinsert = "insert into read_comments([tc_id],[username]) ";
    //                sqlinsert += " values(" + ds.Tables[0].Rows[i]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

    //                int intResult = DatabaseHelper.executeNonQuery(sqlinsert);

    //                string sqlinsert2 = "insert into read_task([task_id],[username]) ";
    //                sqlinsert2 += " values(" + ds.Tables[0].Rows[i]["task_id"].ToString() + ",'" + Session["admin"].ToString() + "')";
    //                int intResult2 = DatabaseHelper.executeNonQuery(sqlinsert2);
    //                //////}
    //            }
    //            catch
    //            {
    //            }
    //        }


    //    }
    //    sql = @"select *  from task_comments where task_id in (select task_id  from tasks where tasks.task_id = task_comments.task_id and tasks.status='closed') and task_comments.tc_id  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "') and task_comments.deleted <> 1 ";
    //    /// sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

    //    DataSet ds1 = DatabaseHelper.getDataset(sql);

    //    if (ds1 != null && ds1.Tables[0].Rows.Count > 0)
    //    {
    //        for (int i = 0; i < ds1.Tables[0].Rows.Count; i++)
    //        {
    //            try
    //            {

    //                string sqlinsert = "insert into read_comments([tc_id],[username]) ";
    //                sqlinsert += " values(" + ds1.Tables[0].Rows[i]["tc_id"].ToString() + ",'" + Session["admin"].ToString() + "')";

    //                int intResult = DatabaseHelper.executeNonQuery(sqlinsert);

    //                string sqlinsert2 = "insert into read_task([task_id],[username]) ";
    //                sqlinsert2 += " values(" + ds1.Tables[0].Rows[i]["task_id"].ToString() + ",'" + Session["admin"].ToString() + "')";
    //                int intResult2 = DatabaseHelper.executeNonQuery(sqlinsert2);
    //                //////}
    //            }
    //            catch
    //            {
    //            }
    //        }


    //    }

    //    getAllTskNewComment();


    }

    void getAllOverduetasks()
    {

        try
        {

            int GvnETC = 0;
            int j = 0;
            DateTime crrntdate = DateTime.Now.Date;
            DateTime duedate;
            sql = @"select tasks.* from tasks 
                        where tasks.deleted <> 1 and  status <> 'closed' and  status <> 'checked' and status <>'parked'";

            //if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
            //{
            //sql += " and assigned_to_user = '" + Session["admin"] + "' ";
            //}

            DataSet ds = DatabaseHelper.getDataset(sql);

            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //=============================================================================================
                    //string strcrrntdate = DateTime.Parse(DateTime.Now.Date.ToString()).ToString("dd/MM/yyyy");
                    string strcrrntdate = string.Format("{0:dd/MM/yyyy}", DateTime.Now.Date.ToString()).ToString();
                    //=============================================================================================
                    string djgfjs = ds.Tables[0].Rows[i]["DueDate"].ToString();
                    if (ds.Tables[0].Rows[i]["DueDate"].ToString() != null && ds.Tables[0].Rows[i]["DueDate"].ToString() != "")
                    {
                        
                        //====================================================================================================
                        string strduedate = string.Format("{0:dd/MM/yyyy}", ds.Tables[0].Rows[i]["DueDate"].ToString()).ToString();
                        duedate = DateTime.Parse(strduedate.ToString());
                        crrntdate = DateTime.Parse(strcrrntdate.ToString());
                        //string strduedate = DateTime.Parse(ds.Tables[0].Rows[i]["DueDate"].ToString()).ToString("dd/MM/yyyy");
                        //duedate = Convert.ToDateTime(strduedate.ToString());
                        //crrntdate = Convert.ToDateTime(strcrrntdate.ToString());
                        //=====================================================================================================
                        if (DateTime.Compare(duedate, crrntdate) < 0)
                        {
                         j++;
                        }


                    }

                }


                if (j > 0)
                {
                    if (DatabaseHelper.isAdmin(Session["admin"].ToString()))
                    {
                        //************************************************
                        BtnallOverdue.Text = "(" + j.ToString() + ")";
                        AllOverDuetsk.Visible = true;
                        //************************************************
                        
                        
                        
                    }
                    else
                    {
                        AllOverDuetsk.Visible = false;
                    }
                }
                else
                {
                    //**************************************
                    BtnallOverdue.Visible = false;
                    AllOverDuetsk.Visible = false;
                    //**************************************
                    
                }


            }
            else
            {

                BtnallOverdue.Visible = false;
                AllOverDuetsk.Visible = true;
            }

        }
        catch { }
    }

    void getPRLowTasks()
    {
        sql = @"select tasks.* from tasks 
                where tasks.deleted <> 1 and tasks.priority = '2 - not urgent'";

        if (!DatabaseHelper.isAdmin(Session["admin"].ToString()))
        {
            sql += " and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + Session["admin"] + "' ";
        }
        else
        {
            sql += " and status <> 'closed' and  status <> 'checked' and status <>'parked' and assigned_to_user = '" + Session["admin"] + "' ";
        }

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            LinklowTasks.Text = "(" + ds.Tables[0].Rows.Count + ")";
            PRlowTask.Visible = true;
        }
        else
        {
            LinklowTasks.Text = "(0)";
            PRlowTask.Visible = false;
        }
    }
    protected void BtnallOverdue_Click(object sender, EventArgs e)
    {
        Session["filter"] = " ";
        Session["filterunread"] = "";
        Session["OverdueTask"] = "True";
        //===============================
        Session["AllOverdueTask"] = "True";
        //===============================
        Response.Redirect("tasks.aspx");

    }
    protected void LinklowTasks_Click(object sender, EventArgs e)
    {
        Session["filter"] = "2 - not urgent";
        Session["filterunread"] = "";
        //===============================
        Session["AllOverdueTask"] = "";
        //===============================
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        Response.Redirect("tasks.aspx");
    }
    void getTobeAnswered()
    {
        sql = @"select distinct tasks.* from task_comments,tasks 
              where tasks.task_id = task_comments.task_id and tasks.deleted <> 1 and task_comments.deleted <> 1 and qflag=1 ";
        sql += " and status <> 'closed' and task_comments.username = 'M'";
        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            if (DatabaseHelper.isAdmin(Session["admin"].ToString()))
            {
                lnktobeanswered.Text = "(" + ds.Tables[0].Rows.Count + ")";
                tobeanswered.Visible = true;
            }
            else 
            {
                
                tobeanswered.Visible = false;
            }
        }
        else
        {
            tobeanswered.Visible = false;
        }
    }
    protected void lnktobeanswered_Click(object sender, EventArgs e)
    {
        Session["filter"] = "ToBeAnswered";
        Session["filterunread"] = "";
        Session["OverdueTask"] = "";
        Session["OverdueTask"] = null;
        //===============================
        Session["AllOverdueTask"] = "";
        //===============================
        Response.Redirect("tasks.aspx");
    }
}
