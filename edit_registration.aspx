<%@ Page Language="C#" AutoEventWireup="true" CodeFile="edit_registration.aspx.cs" Inherits="edit_registration" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Src="Header.ascx" TagName="Header" TagPrefix="uc1" %>
<%@ Register Src="footer.ascx" TagName="footer" TagPrefix="uc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Edit registration</title>
    <link href="StyleSheet.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <uc1:Header ID="Header1" runat="server" />
    <div id="Content">
        
     <div>
        <table width="100%"><tr><td align="right">   <span style="color:red;" id="NewCR" runat="server">Unread messages<asp:Button id="lnkUnread" runat="server" BackColor="transparent" ForeColor="blue"  BorderStyle="none" style="cursor:pointer;width:auto" CausesValidation="False" OnClick="lnkUnread_Click" /></span><span style="color:red;" id="SpanWOCR" runat="server">Message waiting for response<asp:Button id="lnkWORC" runat="server" BackColor="transparent" ForeColor="blue"  BorderStyle="none" style="cursor:pointer;width:auto" CausesValidation="False" OnClick="lnkWORC_Click"  /></span><span style="display:none"><a id="CompNotesIcon" runat="server" title="Click to view notes" style="border:none; font-size:16px; color:Red; padding-left:5px; text-decoration:none; cursor:pointer;"></a></span></td></tr></table>
        
      </div>
        <table width="40%" cellpadding="5" cellspacing="2">
            
<tr>
  <td height="312" colspan="3" align="left" valign="top" style="width: 95px"><table width="420" cellpadding="0" cellspacing="0">
                  <tr>
                    <td colspan="2" align="left" valign="top"><img height="16" src="images/loginBox_top.png" width="420" /></td>
                  </tr>
                  <tr>
                    <td colspan="2" align="left" style="background: url(images/loginBox_mid.png) repeat-y left top;"><table width="507" cellpadding="5" cellspacing="2">
                      <tr>
                        <td width="105" align="left" valign="top" style="width: 95px"><span class="whitetext1">Username:</span><span style="color:Red">*</span></td>
                        <td width="150" align="left" valign="top"><asp:Label CssClass="whitetext1" ID="lblUserName" runat="server"></asp:Label></td>
                        <td width="212" style="width: 10px"></td>
                      </tr>
                      <tr>
                        <td><span class="whitetext1">First name:</span><span style="color:Red">*</span></td>
                        <td align="left"><asp:TextBox ID="txtFirstName" runat="server" Width="220px" MaxLength="50"></asp:TextBox></td>
                        <td><span style="width: 10px">
                          <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Enter first name." ControlToValidate="txtFirstName">*</asp:RequiredFieldValidator>
                        </span></td>
                      </tr>
                      <tr>
                        <td><span class="whitetext1">Surname:</span><span style="color:Red">*</span></td>
                        <td align="left"><asp:TextBox ID="txtSurname" runat="server" Width="220px" MaxLength="50"></asp:TextBox></td>
                        <td><span style="width: 10px">
                          <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Enter surname." ControlToValidate="txtSurname">*</asp:RequiredFieldValidator>
                        </span></td>
                      </tr>
                      <tr>
                        <td><span class="whitetext1">Company name:</span><span style="color:Red">*</span></td>
                        <td align="left"><asp:TextBox ID="txtCompanyName" runat="server" Width="220px" MaxLength="50"></asp:TextBox></td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td><span class="whitetext1">Phone:</span><span style="color:Red">*</span></td>
                        <td align="left"><asp:TextBox ID="txtPhone" MaxLength="16" runat="server" Width="220px"></asp:TextBox></td>
                        <td><span style="width: 10px">
                          <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Enter phone number." ControlToValidate="txtPhone">*</asp:RequiredFieldValidator>
                        </span></td>
                      </tr>
                      <tr>
                        <td><span class="whitetext1">Email address:</span><span style="color:Red">*</span></td>
                        <td align="left"><asp:TextBox ID="txtEmail" MaxLength="50" runat="server" Width="220px"></asp:TextBox></td>
                        <td><span style="width: 10px">
                          <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Enter email address." ControlToValidate="txtEmail" Display="Dynamic">*</asp:RequiredFieldValidator>
                          <asp:RegularExpressionValidator
                        ID="RegularExpressionValidator1" runat="server" Display="Dynamic" ErrorMessage="Enter valid email address" ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                        </span></td>
                      </tr>
                      <tr>
                        <td class="whitetext1">Website:</td>
                        <td align="left"><asp:TextBox ID="txtWebSite" MaxLength="50" runat="server" Width="220px"></asp:TextBox></td>
                        <td>&nbsp;</td>
                      </tr>
                      
                    </table></td>
                  </tr>
                  <tr>
                    <td colspan="2" align="left" style="width: 100px"><img height="16" src="images/loginBox_btm.png" width="420" /></td>
                  </tr>
                  
                  <tr>
                  <td width="418" height="10"><hr/></td>
                  <td width="89"></td>
               
            </tr>
                  <tr>
                  <td width="418" height="29" align="right"><span style="width: 10px">
           <asp:Button CssClass="blueBtns" ID="btnUpdate" runat="server" Text="Update" OnClick="btnUpdate_Click" />                 
               </span></td>
                  <td width="89" align="left" valign="middle"></td>
               
            </tr>
                  
              </table></td>
  </tr>
             <tr>
                <td width="438" align="left" valign="top" style="width: 70px">                </td>
             <td colspan="2" align="center" valign="top"></td>
          </tr>            
            <tr>
                <td align="left" colspan="2" valign="top">                </td>
                <td width="71" style="width: 10px">                </td>
          </tr>
        </table>            
     <div id="divMessage" runat="server" style="color:Red;font-weight:bold;"></div>
      </div>
    <uc2:footer ID="Footer1" runat="server" />
    </div>
        
        
    </form>
</body>
</html>
