using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.Drawing.Drawing2D;

public partial class edit_registration : System.Web.UI.Page
{
    string sql = "";
    string companyname;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["clientid"] == null || Session["username"] == null || Session["clientid"].ToString() == "" || Session["username"].ToString() == "")
        {
            Session["returnUrl"] = "edit_registration.aspx";
            Response.Redirect("login.aspx");
            return;
        }
        
        if (!Page.IsPostBack)
        {
            getUserDetails();
            getCRwaiting();
            getunreadmessage();

            try
            {
                string Clientcomp = "select [CompanyName] from [NonesCRMusers]  where UserName = '" + Session["username"].ToString() + "' and ClientId = " + Session["clientid"].ToString() + "";
                object objClientComp = DatabaseHelper.executeScalar(Clientcomp);
                if (objClientComp.ToString() != "")
                {

                    companyname = objClientComp.ToString();

                    string sqlCRnote = "select count(*) from [Company_notes]  where Company_Name = '" + objClientComp.ToString() + "' and Note_Type ='Client'  and Allow_Notes='True'";
                    int objCRNoteResult = int.Parse(DatabaseHelper.executeScalar(sqlCRnote).ToString());



                    if (objCRNoteResult > 0)
                    {


                        CompNotesIcon.InnerHtml = "<img src=\"images/notes.png\" style=\"cursor:pointer\" title=\"Notifications\" width=\"25\" height=\"25\" alt=\"Notifications\" />" + "Notification: for " + objClientComp.ToString();
                        Session["TaskComp"] = companyname;
                        CompNotesIcon.Attributes.Add("onclick", "window.open('CRNotes.aspx','','status=no,scrollbars=yes,position=center,resizable=yes,width=screen.width,height=screen.height');");


                    }
                    else
                    {

                        CompNotesIcon.InnerHtml = "";
                    }

                }

            }
            catch { }
        }
    }

    void getunreadmessage()
    {
        sql = "select ClientRequest.* from ClientRequest where RequestId in(select ClientRequest_Details.RequestId from ClientRequest_Details where [CommentId] not in (select  CommentId from read_CR_comments where read_CR_comments.username='" + Session["username"].ToString() + "') and ClientRequest_Details.deleted <> 1) and UserName = '" + Session["username"].ToString() + "' and ClientRequest.deleted <> 1";
        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {

            lnkUnread.Text = "(" + ds.Tables[0].Rows.Count + ")";

            NewCR.Visible = true;
        }
        else
        {



            lnkUnread.Visible = false;
            NewCR.Visible = false;
        }
    }

    void getCRwaiting()
    {
        sql = @"select ClientRequest.* from ClientRequest 
                where ClientRequest.Status = 'awaiting client response- required' and UserName ='" + Session["username"].ToString() + "' and status <> 'closed' and deleted <> 1 ";



        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {


            lnkWORC.Text = "(" + ds.Tables[0].Rows.Count + ")";
            SpanWOCR.Visible = true;


        }
        else
        {




            lnkWORC.Text = "(0)";
            SpanWOCR.Visible = false;



        }
    }


    private void getUserDetails()
    {
        DataSet ds = DatabaseHelper.getUserDetails(Session["clientid"].ToString(), Session["username"].ToString());

        if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0].Rows.Count <= 0)
        {
            
        }
        else
        {
            lblUserName.Text = ds.Tables[0].Rows[0]["UserName"].ToString().Replace("''","'");
            txtFirstName.Text = ds.Tables[0].Rows[0]["FirstName"].ToString().Replace("''", "'");
            txtSurname.Text = ds.Tables[0].Rows[0]["Surname"].ToString().Replace("''", "'");
            txtCompanyName.Text = ds.Tables[0].Rows[0]["CompanyName"].ToString().Replace("''", "'");
            txtPhone.Text = ds.Tables[0].Rows[0]["Phone"].ToString().Replace("''", "'");
            txtEmail.Text = ds.Tables[0].Rows[0]["Email"].ToString().Replace("''", "'");
            txtWebSite.Text = ds.Tables[0].Rows[0]["WebSite"].ToString().Replace("''", "'");
        }
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        object objResult = DatabaseHelper.updateRegisterUser(lblUserName.Text.Trim(), txtFirstName.Text.Trim(), txtSurname.Text.Trim(), txtCompanyName.Text.Trim(), txtPhone.Text.Trim(), txtEmail.Text.Trim(), "0", txtWebSite.Text.Trim());

        if (objResult.ToString() != "0")
        {
            string strBody = generateEmail();

            bool flag = DatabaseHelper.sendEmailChangeRequestNorply(ConfigurationManager.AppSettings["ToEmail1"].ToString(), "Registration details have been changed.", strBody);
            /////flag = DatabaseHelper.sendEmailChangeRequest(ConfigurationManager.AppSettings["ToEmail1"].ToString(), "Registration details", strBody);
            ////flag = DatabaseHelper.sendEmailChangeRequestNorply(ConfigurationManager.AppSettings["ToEmail2"].ToString(), "Registration details", strBody);
            flag = DatabaseHelper.sendEmailChangeRequestNorply(txtEmail.Text.Trim(), "Registration details have been changed.", strBody);

            divMessage.InnerHtml = "Your changes have been updated. An email has been sent to you confirming your details. Please keep it safe.";
            divMessage.Visible = true;
        }
        else
        {
            divMessage.InnerHtml = "There was problem in saving you details. Please try again.";
            divMessage.Visible = true;
        }
    }

    private string generateEmail()
    {
        DataSet ds = DatabaseHelper.getUserDetails(Session["clientid"].ToString(), Session["username"].ToString());
        
        string strBody = "";
        strBody += "<br>" + txtFirstName.Text.Trim() + " " + txtSurname.Text.Trim() + ",<br><br>";
        strBody += "Your registration details for the Change Request System have changed.<br> Your login details for any changes you might request are<br>";
        strBody += "Username: " + lblUserName.Text.Trim() + "<br>";
        strBody += "Password: " + ds.Tables[0].Rows[0]["Password"].ToString().Replace("''", "'") + "<br><br>";
        strBody += "You can change the password to something more memorable." + "<br><br>";
        strBody += "<span style=\'font-size:11.0pt;font-family:Calibri,sans-serif;color:red\'>Please do not reply to this email as it does not take replies. Please use the Change Request System as this is the quickest way to process your requests and also keeps a record of communications and progress for you to review at any time.</span><br/>";
        strBody += "<br>";
        strBody += "Your registered details follow. If any are incorrect please let us know:<br>";
        strBody += "First name: " + txtFirstName.Text.Trim() + "<br>";
        strBody += "Surname: " + txtSurname.Text.Trim() + "<br>";
        strBody += "Company name: " + txtCompanyName.Text.Trim() + "<br>";
        strBody += "Phone: " + txtPhone.Text.Trim() + "<br>";
        strBody += "Email address: " + txtEmail.Text.Trim() + "<br>";
        strBody += "WebSite: " + txtWebSite.Text.Trim() + "<br>";
        strBody += "To make any change requests please go to: " + ConfigurationManager.AppSettings["WebAddress2"].ToString()+"<br><br>";
        strBody += "Why use the Change Request system rather than email or call:" + "<br><br>";
        strBody += "<li style='margin-left:13px'>If the email recipient is not available to receive or action your email your request will still get actioned by the support team which monitor the Change Request system.</li><br>";
        strBody += "<li style='margin-left:13px'>The support team will receive the request immediately.</li><br>";
        strBody += "<li style='margin-left:13px'>You will also be able to login at any time and check the status.</li><br>";
        strBody += "<li style='margin-left:13px'>The ongoing progress will be logged in one place rather than through a stream of unrelated email communications.</li><br>";
        strBody += "<li style='margin-left:13px'>If the support team require to speak directly with you they can initiate an online chat with you or remote desktop login.</li><br><br>";
        strBody += "<br/><br/>" + "Regards," + "<br/>Support.";
        return strBody;



    }

    protected void lnkUnread_Click(object sender, EventArgs e)
    {

        Session["filterunreadCR"] = "CRNewComment";
        Session["Status"] = "";
        Session["Status"] = null;

        Response.Redirect("requests.aspx");
    }
    protected void lnkWORC_Click(object sender, EventArgs e)
    {
        Session["filterunreadCR"] = "";
        Session["Status"] = "awaiting client response- required";
        Response.Redirect("requests.aspx", false);

    }
}
