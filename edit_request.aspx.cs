using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.Drawing.Drawing2D;
using System.Net;
using System.Net.NetworkInformation;


public partial class edit_request : System.Web.UI.Page
{
    private static int RequestId;
    string companyname;
    string usercompanyname;

    string sql = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["clientid"] == null || Session["username"] == null || Session["clientid"].ToString() == "" || Session["username"].ToString() == "")
        {
            Session["returnUrl"] = (Request.QueryString["reqid"] == null || Request.QueryString["reqid"].ToString() == "" ? "requests.aspx" : "edit_request.aspx?reqid=" + Request.QueryString["reqid"].ToString());
            Response.Redirect("login.aspx");
            return;
        }
        
        divMessage.InnerHtml = "";
        if (Request.QueryString["reqid"] != null && Request.QueryString["reqid"] != "0")
        {
            if (!Page.IsPostBack)
            {
                try
                {
                    RequestId = Convert.ToInt16(Request.QueryString["reqid"].ToString());
                    lblRequestId.Text = RequestId.ToString();
                    lnkSendAttachment.HRef = "upload_attachment.aspx?reqid=" + RequestId.ToString();
                    lnkViewAttachment.HRef = "view_attachments.aspx?reqid=" + RequestId.ToString();

                    DataSet dsTotalAttachments = DatabaseHelper.getAttachments(RequestId, "");
                    lnkViewAttachment.InnerHtml = "View attachments (" + dsTotalAttachments.Tables[0].Rows.Count + ")";
                    BindData();

                    getCRwaiting();
                    getunreadmessage();
                   
                }
                catch
                {
                    divMessage.InnerHtml = "Request Id must be Integer.<br><br><a href='requests.aspx'>View requests</a>";
                    divRequest.Visible = false;
                    divRequestDetails.Visible = false;
                }



                try
                {
                    string Clientcomp = "select [CompanyName] from [NonesCRMusers]  where UserName = '" + Session["username"].ToString() + "' and ClientId = " + Session["clientid"].ToString() + "";
                    object objClientComp = DatabaseHelper.executeScalar(Clientcomp);
                    if (objClientComp.ToString() != "")
                    {

                        companyname = objClientComp.ToString();

                        string sqlCRnote = "select count(*) from [Company_notes]  where Company_Name = '" + objClientComp.ToString() + "' and Note_Type ='Client'  and Allow_Notes='True'";
                        int objCRNoteResult = int.Parse(DatabaseHelper.executeScalar(sqlCRnote).ToString());



                        if (objCRNoteResult > 0)
                        {
                            CRNotes.ForeColor = Color.Red;
                            CRNotes.Text = "Notes";
                            //LblCompany.ForeColor = Color.Red;
                            //LblCompany.Text = " for  " + objClientComp.ToString();

                            CompNotesIcon.InnerHtml = "<img src=\"images/notes.png\" style=\"cursor:pointer\" title=\"Notifications\" width=\"25\" height=\"25\" alt=\"Notifications\" />" + "Notification: for " + objClientComp.ToString();
                            Session["TaskComp"] = companyname;
                            CompNotesIcon.Attributes.Add("onclick", "window.open('CRNotes.aspx','','status=no,scrollbars=yes,position=center,resizable=yes,width=screen.width,height=screen.height');");


                        }
                        else
                        {
                            CRNotes.ForeColor = Color.Blue;
                            CRNotes.Text = "";
                           // LblCompany.Text = "";

                        }

                    }

                }
                catch { }

            }


            if (Session["message"] != null)
            {
                divMessage1.InnerHtml = Session["message"].ToString();
                divMessage1.Visible = true;
                Session.Remove("message");
            }
        }
        else
        {
            Response.Redirect("requests.aspx", false);
        }
    }


    void getunreadmessage()
    {
        sql = "select ClientRequest.* from ClientRequest where RequestId in(select ClientRequest_Details.RequestId from ClientRequest_Details where [CommentId] not in (select  CommentId from read_CR_comments where read_CR_comments.username='" + Session["username"].ToString() + "') and ClientRequest_Details.deleted <> 1) and UserName = '" + Session["username"].ToString() + "' and ClientRequest.deleted <> 1";
        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {

            lnkUnread.Text = "(" + ds.Tables[0].Rows.Count + ")";

            NewCR.Visible = true;
        }
        else
        {



            lnkUnread.Visible = false;
            NewCR.Visible = false;
        }
    }

    void getCRwaiting()
    {
        sql = @"select ClientRequest.* from ClientRequest 
                where ClientRequest.Status = 'awaiting client response- required' and UserName ='" + Session["username"].ToString() + "' and status <> 'closed' and deleted <> 1 ";



        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {


            lnkWORC.Text = "(" + ds.Tables[0].Rows.Count + ")";
            SpanWOCR.Visible = true;


        }
        else
        {




            lnkWORC.Text = "(0)";
            SpanWOCR.Visible = false;



        }
    }

    protected void lnkUnread_Click(object sender, EventArgs e)
    {

        Session["filterunreadCR"] = "CRNewComment";
        Session["Status"] = "";
        Session["Status"] = null;

        Response.Redirect("requests.aspx");
    }
    protected void lnkWORC_Click(object sender, EventArgs e)
    {
        Session["filterunreadCR"] = "";
        Session["Status"] = "awaiting client response- required";
        Response.Redirect("requests.aspx", false);

    }


    void Loadprogect_drp(string Company)
    {
        drpProjects.Items.Clear();

        try
        {
            string sql = @"select project_name
		from projects
		where active = 'Y' and CompanyName='" + Company.ToString() + "' order by project_name;";
            DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);
            if (ds_dropdowns.Tables[0].Rows.Count > 0)
            {
                drpProjects.DataSource = ds_dropdowns.Tables[0];
                drpProjects.DataTextField = "project_name";
                drpProjects.DataValueField = "project_name";
                drpProjects.DataBind();
                drpProjects.Items.Insert(0, new ListItem("[no project]", ""));
                ////drpProjects.Items.Insert(1, new ListItem("[Other]", "other"));
            }
            else
            {
                drpProjects.Items.Insert(0, new ListItem("[no project]", ""));
                /// drpProjects.Items.Insert(1, new ListItem("[Other]", "other"));
            }
        }
        catch { }

    }

    public void BindData()
    {
        int RequestId = Convert.ToInt16(lblRequestId.Text);

        sql = "Select * from ClientRequest where RequestId=" + RequestId.ToString() + " and UserName='" + Session["username"].ToString() + "'";
        
        
        DataSet dsRequests = DatabaseHelper.getDataset(sql);

        if (dsRequests == null || dsRequests.Tables.Count <= 0 || dsRequests.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "Request not found.<br><br><a href='requests.aspx'>back to change requests</a>";
            divRequest.Visible = false;
            divRequestDetails.Visible = false;
        }
        else
        {
            if (dsRequests.Tables[0].Rows[0]["deleted"].ToString() == "1")
            {
                btnUpdate.Enabled = false;
            }
            else
            {
                btnUpdate.Enabled = true;
            }


            lblRequestId.Text = dsRequests.Tables[0].Rows[0]["RequestId"].ToString();
            lblLastUpdated.Text = " Last changed by <b>" + dsRequests.Tables[0].Rows[0]["LastUpdatedBy"].ToString() + "</b> on <b>" + DateTime.Parse(dsRequests.Tables[0].Rows[0]["LastUpdatedOn"].ToString()).ToString("dd MMM h:mm tt") + "</b>";
            txtWebsiteURL.Text = dsRequests.Tables[0].Rows[0]["WebsiteURL"].ToString(); 
            txtShortDescr.Text = dsRequests.Tables[0].Rows[0]["ShortDescr"].ToString();
            prevWebsiteURL.Value = dsRequests.Tables[0].Rows[0]["WebsiteURL"].ToString();
            prevShortDescr.Value = dsRequests.Tables[0].Rows[0]["ShortDescr"].ToString();
            lblStatus.Text = dsRequests.Tables[0].Rows[0]["Status"].ToString();
            HdnClientCompany.Value = dsRequests.Tables[0].Rows[0]["CompanyName"].ToString();
            try
            {

                string sqlSEO = "select [SEO] from [projects]  where [project_name] = '" + dsRequests.Tables[0].Rows[0]["ProjectName"].ToString() + "'";
                object objResultSEO = DatabaseHelper.executeScalar(sqlSEO);

                if (objResultSEO.ToString() == "True")
                {
                    BtnAnalytics.ForeColor = Color.Red;
                }
                else
                {
                    BtnAnalytics.ForeColor = Color.Blue;
                    BtnAnalytics.Attributes.Add("onclick", "return alert('Please contact support as your subscription level does not allow to view analytics.');");
                }


            }
            catch { }

            try
            {
                string sqlCRnote = "select [ClientNotes] from [Company]  where Company_Name = '" + dsRequests.Tables[0].Rows[0]["CompanyName"].ToString() + "'";
                object objCRNoteResult = DatabaseHelper.executeScalar(sqlCRnote);

                if (objCRNoteResult.ToString() != "")
                {
                    CRNotes.ForeColor = Color.Red;

                }
                else
                {
                    CRNotes.ForeColor = Color.Blue;
                }

            }
            catch { }
            DataSet dsReqDetails = DatabaseHelper.getClientRequestDetails(RequestId);


            DataSet dsUserDetails = DatabaseHelper.getUserDetails(Session["clientid"].ToString(), Session["username"].ToString());

            ///bool flag = false;


            if (dsUserDetails == null || dsUserDetails.Tables.Count <= 0 || dsUserDetails.Tables[0].Rows.Count <= 0)
            {
                TDSubscription.InnerHtml = "ND";
            }
            else
            {

              
                if (dsUserDetails.Tables[0].Rows[0]["Subscription"].ToString() != "")
                {
                    TDSubscription.InnerHtml = dsUserDetails.Tables[0].Rows[0]["Subscription"].ToString();
                }
                else
                {
                    TDSubscription.InnerHtml = "ND";
                }

                usercompanyname = dsUserDetails.Tables[0].Rows[0]["CompanyName"].ToString();
               /// HdnClientCompany.Value = dsUserDetails.Tables[0].Rows[0]["CompanyName"].ToString();

            }
            string companynm = dsRequests.Tables[0].Rows[0]["CompanyName"].ToString();

            Loadprogect_drp(HdnClientCompany.Value);
            try
            {
                if (dsRequests.Tables[0].Rows[0]["ProjectName"].ToString() != "")
                {
                    drpProjects.SelectedValue = dsRequests.Tables[0].Rows[0]["ProjectName"].ToString();
                    CRProject.Value = dsRequests.Tables[0].Rows[0]["ProjectName"].ToString();

                }
            }
            catch { }


            DataGrid1.DataSource = dsReqDetails.Tables[0];
            DataGrid1.DataBind();
        }
    }

    protected void CRNotes_Click(object sender, EventArgs e)
    {
        Session["TaskComp"] = HdnClientCompany.Value;
      //  Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> window.open('CRNotes.aspx','','status=no,scrollbars=no,position=center,resizable=no,scrollbars=no,width=900,height=650'); </script>");
        Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> window.open('CRNotes.aspx','','status=no,scrollbars=yes,position=center,resizable=yes,width=screen.width,height=screen.height'); </script>");
    }
    public void InsertCRCommentintoTask(string vComment, string vIp, string vMacid, string vPostedby, object objRequestid)
    {
        string qryFindtask, qryClosed, qryICRcommt = "", qryReOpenTask;
        DataSet dscommt, dsTask, dsCount;
        qryClosed = "Select * from tasks where RequestId=" + Convert.ToInt16(objRequestid.ToString()) + ""; ;
        dsCount = DatabaseHelper.getDataset(qryClosed);
        if (dsCount.Tables[0].Rows.Count == 0)
        { }
        else if (dsCount.Tables[0].Rows.Count >= 1)
        {
            qryFindtask = "Select Max(task_id) as task_id from tasks where RequestId=" + Convert.ToInt16(objRequestid.ToString()) + "";
            dsTask = DatabaseHelper.getDataset(qryFindtask);
            if (dsTask != null && dsTask.Tables.Count > 0 && dsTask.Tables[0].Rows.Count > 0)
            {
                string chkclosed = "Select task_id from tasks where status='Closed' and task_id=" + Convert.ToInt16(dsTask.Tables[0].Rows[0]["task_id"]) + "";
                DataSet dschk;
                dschk = DatabaseHelper.getDataset(chkclosed);
                if (dschk != null && dschk.Tables.Count > 0 && dschk.Tables[0].Rows.Count > 0)
                {
                    string chkOpen = "Update tasks set status='re-opened' where task_id='" + dschk.Tables[0].Rows[0]["task_id"] + "'";
                    DatabaseHelper.executeNonQuery(chkOpen);
                }
                qryICRcommt = " insert into task_comments([task_id],[username],[post_date],[comment],[Ip],[Macid],[deleted],[qflag],[QuesTo]) ";
                qryICRcommt += " values(" + dsTask.Tables[0].Rows[0]["task_id"] + ",";
                qryICRcommt += "'" + vPostedby.ToString().Replace("'", "''") + "',";
                qryICRcommt += "getdate(),";
                qryICRcommt += "'" + vComment.Trim().Replace("'", "''") + "',";
                //************************************************
                qryICRcommt += "'" + vIp.ToString() + "',";
                qryICRcommt += "'" + vMacid.ToString() + "',";
                //************************************************
                qryICRcommt += "0,";
                qryICRcommt += "0,";
                qryICRcommt += "0);";
                DatabaseHelper.executeNonQuery(qryICRcommt);
            }
        }
    }




    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        //*****************************************************************
        
        string[] strArrayMacIDAndIP = new string[2];
        string strMacId = "";
        string strIpAddress = "";

        foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
        {
            if (nic.OperationalStatus == OperationalStatus.Up)
            {
                strMacId += nic.GetPhysicalAddress().ToString();
                break;
            }
        }

        strArrayMacIDAndIP[0] = strMacId;

        strIpAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (strIpAddress == null)

            strIpAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

        strArrayMacIDAndIP[1] = strIpAddress;
        
        //*****************************************************************
        
              
        int RequestId = Convert.ToInt16(lblRequestId.Text);
        //********************************************************************************************************************************
        //object objResult = DatabaseHelper.updateClientRequest(RequestId, Session["Clientid"].ToString(), Session["username"].ToString(), txtWebsiteURL.Text.Trim(), txtShortDescr.Text.Trim(), ftbComment.Text.Trim(), HdnClientCompany.Value, drpProjects.SelectedValue.ToString());
        object objResult = DatabaseHelper.updateClientRequest(RequestId, Session["Clientid"].ToString(), Session["username"].ToString(), txtWebsiteURL.Text.Trim(), txtShortDescr.Text.Trim(), ftbComment.Content.Trim(), HdnClientCompany.Value, drpProjects.SelectedValue.ToString(), strArrayMacIDAndIP[0], strArrayMacIDAndIP[1]);
        string AuthMigrate = "";
        DataSet dsMigrate;
        AuthMigrate = "select Migrate_CR_comments_Tasks from NonesCRMusers  where UserName='" + Session["username"] + "'";
        dsMigrate = DatabaseHelper.getDataset(AuthMigrate);
        if (dsMigrate.Tables[0].Rows[0]["Migrate_CR_comments_Tasks"].ToString() == "True")
        {
            InsertCRCommentintoTask(ftbComment.Content.Trim(), strArrayMacIDAndIP[1], strArrayMacIDAndIP[0], Session["username"].ToString(), RequestId.ToString());
        }
        //********************************************************************************************************************************
        if (objResult.ToString() != "0")
        {
            divMessage1.InnerHtml = "Comment was updated.";
            divMessage1.Visible = true;

            DataSet dsUserDetails = DatabaseHelper.getUserDetails(Session["clientid"].ToString(), Session["username"].ToString());

            bool flag = false;


            if (dsUserDetails == null || dsUserDetails.Tables.Count <= 0 || dsUserDetails.Tables[0].Rows.Count <= 0)
            {
            }
            else
            {

                companyname = dsUserDetails.Tables[0].Rows[0]["CompanyName"].ToString();

                flag = DatabaseHelper.sendEmailChangeRequestNorply(dsUserDetails.Tables[0].Rows[0]["Email"].ToString(), "Request ID:" + RequestId.ToString() + " was updated - Change request form  (Request ID:" + RequestId.ToString() + ")", generateEmail(RequestId));
            }

            flag = DatabaseHelper.sendEmailChangeRequestNorply(ConfigurationManager.AppSettings["ToEmail1"].ToString(), "Request ID:" + RequestId.ToString() + " was updated - Change request form  (Request ID:" + RequestId.ToString() + ")", generateAdminEmail(RequestId));
            ///flag = DatabaseHelper.sendEmailChangeRequestNorply(ConfigurationManager.AppSettings["ToEmail2"].ToString(), "Request ID:" + RequestId.ToString() + " was updated - Change request form  (Request ID:" + RequestId.ToString() + ")", generateAdminEmail(RequestId));
                            
            //////ftbComment.Text = "";
         ////BindData();

            Session["message"] = "CR was updated.";
            Response.Redirect("edit_request.aspx?reqid=" + RequestId.ToString());
        }
        else
        {
            divMessage1.InnerHtml = "Comment was not updated. Please try again.";
            divMessage1.Visible = true;
        }    
    }
    protected void DataGrid1_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemIndex != -1)
        {
            ////=================================================================
            //Label lblid = (Label)e.Item.FindControl("lblid");
            //Label lblmacid = (Label)e.Item.FindControl("lblmacid");
            //Label lblipAd = (Label)e.Item.FindControl("lblipAdd");
            //Label lblMacipAd = (Label)e.Item.FindControl("lblMacipAdd");

            //if (DatabaseHelper.isAdmin(Session["admin"].ToString()))
            //{
            //    if (lblid.Text == "")
            //    {
            //        lblid.Visible = false;
            //        lblipAd.Visible = false;
            //    }
            //    else
            //    {
            //        lblid.Visible = true;
            //        lblipAd.Visible = true;
            //    }
            //    if (lblmacid.Text == "")
            //    {
            //        lblmacid.Visible = false;
            //        lblMacipAd.Visible = false;
            //    }
            //    else
            //    {
            //        lblmacid.Visible = true;
            //        lblMacipAd.Visible = true;
            //    }
            //}
            //else
            //{
            //    lblid.Visible = false;
            //    lblmacid.Visible = false;
            //    lblipAd.Visible = false;
            //    lblMacipAd.Visible = false;
            //}
            //=================================================================
            Label lblPosted = (Label)e.Item.FindControl("lblPosted");
            LinkButton readMarkButton = (LinkButton)e.Item.FindControl("lnkReadMark");

            lblPosted.Text = "comment " + e.Item.Cells[1].Text + " posted by " + e.Item.Cells[2].Text + " on " + DateTime.Parse(e.Item.Cells[3].Text).ToString("dd MMM h:mm tt");

            sql = "select count(*) from read_CR_comments where CommentId=" + e.Item.Cells[1].Text + " and username='" + Session["username"] + "'";

            object objIsRead = DatabaseHelper.executeScalar(sql);
            if (objIsRead.ToString() == "0")
            {
                lblPosted.ForeColor = System.Drawing.Color.Red;
                readMarkButton.Text = "[<span style='color:red'>Unread.</span>  Mark as read]";
            }
            else
            {
                lblPosted.ForeColor = System.Drawing.Color.Green;
                readMarkButton.Text = "[<span style='color:green'>Read.</span> Mark as unread]";
            }
            readMarkButton.Visible = true;

            //string str1 = e.Item.Cells[1].Text;
            //str1 = e.Item.Cells[1].Text;
            //str1 = e.Item.Cells[1].Text;

            //((Label)e.Item.FindControl("lblPosted")).Text = "comment " + e.Item.Cells[1].Text + " posted by " + e.Item.Cells[2].Text + " on " + DateTime.Parse(e.Item.Cells[3].Text).ToString("dd MMM h:mm tt");
        }
    }

    private string record_changes_for_email()
    {
        string strSql = "";

        if (prevWebsiteURL.Value != txtWebsiteURL.Text.Trim())
        {
            strSql += "Relevant URL has been changed from  \""
                + prevWebsiteURL.Value.Replace("'", "''") + "\" to \""
               + txtWebsiteURL.Text.Trim().Replace("'", "''") + "\"<br>";

            //prevRelevantUrl.Value = txtRelevantURL.Text.Trim();
        }

        if (prevShortDescr.Value != txtShortDescr.Text.Trim())
        {
            strSql += "Short description has been changed from \""
                + prevShortDescr.Value.Replace("'", "''") + "\" to \""
                + txtShortDescr.Text.Trim().Replace("'", "''") + "\"<br>";

            //prevShortDescr.Value = txtShortDescr.Text.Trim();
        }

        //***********************************************************
        //if (ftbComment.Text.Trim() != "")
          if (ftbComment.Content.Trim() != "")
        //***********************************************************
        {
            strSql += "Comment has been added.<br>";
        }

        return strSql;
    }

    private string generateEmail(int RequestId)
    {
        DataSet dsRequests = DatabaseHelper.getClientRequest(RequestId);
        string strBody = "";
        if (dsRequests == null || dsRequests.Tables.Count <= 0 || dsRequests.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "Request not found.<br><br><a href='requests.aspx'>View requests</a>";
            divMessage.Visible = true;
        }
        else
        {
            strBody += "<b>" + "FAO  " + Session["username"].ToString() + " :"+"</b><br><span style=\"color:red\">";

            //strBody += task_action;

            strBody += dsRequests.Tables[0].Rows[0]["LastUpdatedBy"].ToString() + " has updated Change Request"+ "<span style=\"color:red\"> (number  </span>" +"<a style=\"color:red\" href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/edit_request.aspx?reqid=" + RequestId + "'>" + RequestId + "</a>) </span>  on" + DateTime.Parse(dsRequests.Tables[0].Rows[0]["LastUpdatedOn"].ToString()).ToString("dd MMM h:mm tt") + "<br><br>";
            
            string attribures = record_changes_for_email();

            if (attribures.ToString().Trim() != "")
            {
               // strBody += "The attributes below have been amended.<br>";
                strBody += "<span style=\"color:red\">" + attribures + "</span><br>";
            }
            strBody += "<span style=\'font-size:11.0pt;font-family:Calibri,sans-serif;color:red\'>Click on the following link to access or amend the change request: " + "  " + "<a href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/edit_request.aspx?reqid=" + RequestId + "'>" + txtShortDescr.Text.Trim() + " . " + "</a>" + "  Do not start a new change request as this will slow the resolution." + "</span><br><br>";
            ///strBody += "Short description:  <a href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/edit_request.aspx?reqid=" + RequestId + "'>" + txtShortDescr.Text.Trim() + "</a>&nbsp;&nbsp;&nbsp;(Status : " + dsRequests.Tables[0].Rows[0]["status"].ToString() + ")<br><br>";
            strBody += "Client: " + dsRequests.Tables[0].Rows[0]["UserName"].ToString() + "<br>";
            strBody += "Company name: " + companyname + "<br>";
               strBody += "Reported on: " + DateTime.Parse(dsRequests.Tables[0].Rows[0]["RequestDate"].ToString()).ToString("dd MMM h:mm tt") + "<br>";
            strBody += "Title or preferably URL: <a href='" + dsRequests.Tables[0].Rows[0]["WebsiteURL"].ToString() + "' target='_blank'>" + dsRequests.Tables[0].Rows[0]["WebsiteURL"].ToString() + "</a><br>";
            strBody += "Status:  " + dsRequests.Tables[0].Rows[0]["status"].ToString();
            strBody += "<br><br>";
          

            DataSet dsReqDetails = DatabaseHelper.getClientRequestDetails(RequestId);
           
            strBody += "<table cellspacing=\"0\" border=\"1\" style=\"width: 99%;border-collapse: collapse;\">";
            strBody += "<tbody>";

            for (int i = 0; i < dsReqDetails.Tables[0].Rows.Count; i++)
            {
                strBody += "<tr>";
                strBody += "<td>";
                strBody += "<div style=\"border: 1px none Green;\">";
                strBody += "<table width=\"100%\">";
                strBody += "<tbody>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span style=\"color: Green;\">comment " + dsReqDetails.Tables[0].Rows[i]["CommentId"].ToString() + " posted by " + dsReqDetails.Tables[0].Rows[i]["PostedBy"].ToString() + " on " + DateTime.Parse(dsReqDetails.Tables[0].Rows[i]["PostedOn"].ToString()).ToString("dd MMM h:mm tt") + "</span>";
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\"> </td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span>" + dsReqDetails.Tables[0].Rows[i]["comment"].ToString() + "</span>";
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "</tbody>";
                strBody += "</table>";
                strBody += "</div>";
                strBody += "</td>";
                strBody += "</tr>";
            }
            strBody += "</tbody>";
            strBody += "</table>";

            strBody += "<br/><span style=\'font-size:11.0pt;font-family:Calibri,sans-serif;color:red\'>Please do not reply to this email as it does not take replies. Please use the Change Request System as this is the quickest way to process your requests and also keeps a record of communications and progress for you to review at any time.</span><br/>";
            strBody += "<br>";
            strBody += "<br/><br/>" + "Thanks & Regards," + "<br/>Support.";
        }
        return strBody;
    }

    private string generateAdminEmail(int RequestId)
    {
        DataSet dsRequests = DatabaseHelper.getClientRequest(RequestId);
        string strBody = "";
        if (dsRequests == null || dsRequests.Tables.Count <= 0 || dsRequests.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "Request not found.<br><br><a href='requests.aspx'>View requests</a>";
            divMessage.Visible = true;
        }
        else
        {
            strBody += "<b>" + "FAO  " + Session["username"].ToString() +" :" +"</b><br><span style=\"color:red\">";

            //strBody += task_action;

            strBody += dsRequests.Tables[0].Rows[0]["LastUpdatedBy"].ToString() + " has updated Change Request "+"<span style=\"color:red\"> (number  </span>" +"<a style=\"color:red\" href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_request.aspx?reqid=" + RequestId + "'>" + RequestId + "</a>) </span> on " + DateTime.Parse(dsRequests.Tables[0].Rows[0]["LastUpdatedOn"].ToString()).ToString("dd MMM h:mm tt") + "<br><br>";

            string attribures = record_changes_for_email();

            if (attribures.ToString().Trim() != "")
            {
               /// strBody += "The attributes below have been amended.<br>";
                strBody += "<span style=\"color:red\">" + attribures + "</span><br>";
            }


            strBody += "<span style=\'font-size:11.0pt;font-family:Calibri,sans-serif;color:red\'>Click on the following link to access or amend the change request: " + "  " + "<a href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_request.aspx?reqid=" + RequestId + "'>" + txtShortDescr.Text.Trim() + " . " + "</a>" + "  Do not start a new change request as this will slow the resolution." + "</span><br><br>";
            ///strBody += "Short description:  <a href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/edit_request.aspx?reqid=" + RequestId + "'>" + txtShortDescr.Text.Trim() + "</a>&nbsp;&nbsp;&nbsp;(Status : " + dsRequests.Tables[0].Rows[0]["status"].ToString() + ")<br><br>";
            strBody += "Client: " + dsRequests.Tables[0].Rows[0]["UserName"].ToString() + "<br>";
            strBody += "Company name: " + companyname + "<br>";
            strBody += "Reported on: " + DateTime.Parse(dsRequests.Tables[0].Rows[0]["RequestDate"].ToString()).ToString("dd MMM h:mm tt") + "<br>";
            strBody += "Title or preferably URL: <a href='" + dsRequests.Tables[0].Rows[0]["WebsiteURL"].ToString() + "' target='_blank'>" + dsRequests.Tables[0].Rows[0]["WebsiteURL"].ToString() + "</a><br>";
            strBody += "Status:  " + dsRequests.Tables[0].Rows[0]["status"].ToString();
            strBody += "<br><br>";

            DataSet dsReqDetails = DatabaseHelper.getClientRequestDetails(RequestId);
           
            strBody += "<table cellspacing=\"0\" border=\"1\" style=\"width: 99%;border-collapse: collapse;\">";
            strBody += "<tbody>";

            for (int i = 0; i < dsReqDetails.Tables[0].Rows.Count; i++)
            {
                strBody += "<tr>";
                strBody += "<td>";
                strBody += "<div style=\"border: 1px none Green;\">";
                strBody += "<table width=\"100%\">";
                strBody += "<tbody>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span style=\"color: Green;\">comment " + dsReqDetails.Tables[0].Rows[i]["CommentId"].ToString() + " posted by " + dsReqDetails.Tables[0].Rows[i]["PostedBy"].ToString() + " on " + DateTime.Parse(dsReqDetails.Tables[0].Rows[i]["PostedOn"].ToString()).ToString("dd MMM h:mm tt") + "</span>";
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\"> </td>";
                strBody += "</tr>";
                strBody += "<tr>";
                strBody += "<td align=\"left\">";
                strBody += "<span>" + dsReqDetails.Tables[0].Rows[i]["comment"].ToString() + "</span>";
                strBody += "</td>";
                strBody += "</tr>";
                strBody += "</tbody>";
                strBody += "</table>";
                strBody += "</div>";
                strBody += "</td>";
                strBody += "</tr>";
            }
            strBody += "</tbody>";
            strBody += "</table>";
            strBody += "<br/><span style=\'font-size:11.0pt;font-family:Calibri,sans-serif;color:red\'>Please do not reply to this email as it does not take replies. Please use the Change Request System as this is the quickest way to process your requests and also keeps a record of communications and progress for you to review at any time.</span><br/>";
            strBody += "<br>";
            strBody += "<br/><br/>" + "Thanks & Regards," + "<br/>Support.";
        }
        return strBody;
    }
    protected void DataGrid1_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "ReadMark")
        {
            LinkButton readMarkButton = (LinkButton)e.Item.FindControl("lnkReadMark");

            if (readMarkButton.Text == "[<span style='color:red'>Unread.</span>  Mark as read]")
            {
                sql = @"insert into read_CR_comments([CommentId],[UserName]) 
                        values(" + e.Item.Cells[1].Text + ",'" + Session["username"] + "')";
                DatabaseHelper.executeNonQuery(sql);
                BindData();
            }
            else
            {
                sql = @"delete from read_CR_comments where [CommentId]=" + e.Item.Cells[1].Text + " and [UserName]='" + Session["username"] + "'";
                DatabaseHelper.executeNonQuery(sql);
                BindData();
            }
        }
    }
    protected void CheckAllasRead_CheckedChanged(object sender, EventArgs e)
    {

        if (CheckAllasRead.Checked)
        {

            string sql = "select *  from ClientRequest_Details where [RequestId] = " + lblRequestId.Text + " and [CommentId] not in (select  CommentId  from read_CR_comments where read_CR_comments.username='" + Session["username"].ToString() + "')";
            DataSet ds = DatabaseHelper.getDataset(sql);

            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    try
                    {
                        string sqlinsert = "insert into read_CR_comments([CommentId],[UserName]) ";
                        sqlinsert += " values(" + ds.Tables[0].Rows[i]["CommentId"].ToString() + ",'" + Session["username"].ToString() + "')";

                        int intResult = DatabaseHelper.executeNonQuery(sqlinsert);
                    }
                    catch
                    {
                    }
                }


            }
        }

        BindData();
    }

    protected void BtnAnalytics_Click(object sender, EventArgs e)
    {
      ///  Session["TaskComp"] = TaskProject.Value;
        sql = @"select *  from projects where project_name ='" + CRProject.Value + "'";

        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            if (ds.Tables[0].Rows[0]["SEO"].ToString() == "True")
            {

                Session["GoogleUser"] = ds.Tables[0].Rows[0]["GoogleUserId"].ToString();
                Session["GooglePwd"] = ds.Tables[0].Rows[0]["GooglePwd"].ToString();
                Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script> window.open('Analytics.aspx','','scrollbars=yes,width=470,height=500'); </script>");


            }
            else
            {
                ///Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "test", "<script>return alert('Please contact support as your subscription level does not allow to view analytics.'); </script>");
                BtnAnalytics.Attributes.Add("onclick", "return alert('Please contact support as your subscription level does not allow to view analytics.');");
            }



        }


    }

}
