<%@ Page Language="C#" AutoEventWireup="true" CodeFile="edit_request.aspx.cs" Inherits="edit_request"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="footer.ascx" TagName="footer" TagPrefix="uc2" %>
<%@ Register Src="Header.ascx" TagName="Header" TagPrefix="uc1" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        <%= ConfigurationManager.AppSettings["CompanyName"].ToString()%>
    </title>

    <script type="text/javascript" language="javascript">

        function hide() {
            //  alert("hide");

            var l = document.getElementById("lnk_h_s");
            var h = document.getElementById("d_h_s");
            var i = document.getElementById("img1");


            if (l.innerHTML == "Add new comment") {
                l.innerHTML = "Close comment box"
                i.src = "images/Minus.png"
                h.style.display = "block";
            }
            else {
                l.innerHTML = "Add new comment"
                i.src = "images/plus.png"
                h.style.display = "none";
            }



        }
        
    </script>

    <link href="StyleSheet.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div id="wrapper">
            <uc1:Header ID="Header1" runat="server" />
            <div id="Content">
                <div id="Content_98">
                    <div class="titleText" style="display: none;">
                    </div>
                    <div style="width: 100%; background: #ebebeb; padding: 5px 0; float: left;">
                        <div id="divMessage" runat="server" style="color: Red; font-weight: bold; text-align: left">
                        </div>
                        <div id="divRequest" runat="server" class="divBorder">
                            <table width="100%" border="0" cellspacing="0" cellpadding="5">
                                <tr>
                                    <td colspan="3" align="right" valign="top">
                                        <div>
                                            <table width="100%">
                                                <tr>
                                                    <td align="right" valign="top">
                                                        <span style="color: red;" id="NewCR" runat="server">Unread messages
                                                            <asp:Button ID="lnkUnread" runat="server" BackColor="transparent" ForeColor="blue"
                                                                BorderStyle="none" Style="cursor: pointer; width: auto" CausesValidation="False"
                                                                OnClick="lnkUnread_Click" /></span><span style="color: red;" id="SpanWOCR" runat="server">Message
                                                                    waiting for response<asp:Button ID="lnkWORC" runat="server" BackColor="transparent"
                                                                        ForeColor="blue" BorderStyle="none" Style="cursor: pointer; width: auto" CausesValidation="False"
                                                                        OnClick="lnkWORC_Click" /></span> <span style="display: none"><a id="CompNotesIcon"
                                                                            runat="server" title="Click to view notes" style="border: none; font-size: 16px;
                                                                            color: Red; padding-left: 5px; text-decoration: none; cursor: pointer;"></a></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="33%" align="left" valign="top" bgcolor="#282c5f" class="whitetext2 style2">
                                        <span class="whitetext2">
                                            <asp:Label ID="lblLastUpdated" runat="server" ForeColor="#FFFFFF"></asp:Label>
                                        </span>
                                    </td>
                                    <td width="1%" align="left" valign="top" bgcolor="#282c5f" class="whitetext2">
                                        &nbsp;
                                    </td>
                                    <td width="14%" align="right" valign="top" bgcolor="#282c5f" class="whitetext2">
                                        <span style="color: #E0BC14">Request Id:
                                            <asp:Label ID="lblRequestId" runat="server" Font-Bold="True"></asp:Label>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="left" valign="top">
                                        <table width="80%">
                                            <tr>
                                                <td style="width: 20%" colspan="0">
                                                    Status:
                                                    <asp:Label ID="lblStatus" runat="server" Font-Bold="True"></asp:Label>
                                                </td>
                                                <td>
                                                    &nbsp
                                                </td>
                                                <td colspan="2" align="left" valign="top">
                                                    Subscription: <span id="TDSubscription" runat="server" style="color: Red; font-weight: bold;">
                                                    </span>
                                                    <asp:LinkButton ID="CRNotes" runat="server" Text="Client notes" OnClick="CRNotes_Click"
                                                        CausesValidation="False"></asp:LinkButton>
                                                    &nbsp;&nbsp;
                                                    <asp:Button ID="BtnAnalytics" runat="server" Text="Analytics" BorderStyle="none"
                                                        BackColor="transparent" ForeColor="blue" Style="cursor: pointer" CausesValidation="False"
                                                        OnClick="BtnAnalytics_Click" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="46%" align="left" valign="top">
                                                    Short description
                                                </td>
                                                <td width="6%">
                                                    &nbsp;
                                                </td>
                                                <td width="48%" align="left" valign="top">
                                                    URL if relevant (Website address i.e. www.ourwebsite.com/contactus.html)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top">
                                                    <asp:TextBox ID="txtShortDescr" runat="server" Width="450px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtShortDescr"
                                                        ErrorMessage="Please enter short description.">*</asp:RequiredFieldValidator>
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td align="left" valign="top">
                                                    <asp:TextBox ID="txtWebsiteURL" runat="server" Width="450px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top">
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td align="left" valign="top">
                                                    Project Name:
                                                    <asp:DropDownList ID="drpProjects" runat="server" AutoPostBack="True" CssClass="filerDrpodown"
                                                        Width="368px">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="125" colspan="4" align="left" valign="top">
                                        <span></span>
                                        <table width="80%">
                                            <tr>
                                                <td align="left" colspan="4" valign="top">
                                                    <div style="height: 22px; background-color: rgb(40, 44, 95); padding-top: 5px; padding-left: 6px;">
                                                        <asp:ImageButton ID="img1" runat="server" src="images/plus.png" Style="border-width: 0px;
                                                            margin-top: -2px;" OnClientClick="hide();return false;" />
                                                        <asp:LinkButton ID="lnk_h_s" runat="server" OnClientClick="hide();return false;"
                                                            CssClass="whitetext2" Style="text-decoration: none; color: #FFFFFF !important;
                                                            vertical-align: top;">Add new comment</asp:LinkButton>
                                                    </div>
                                                    <div id="d_h_s" runat="server" style="display: none">
                                                        <br />
                                                        <%--**********************************************************************************************--%>
                                                        <%--<FTB:FreeTextBox ID="ftbComment" runat="server" Height="200px" Width="100%"></FTB:FreeTextBox>--%>
                                                        <telerik:RadEditor ID="ftbComment" runat="server" Height="300px" Width="100%">
                                                            <Snippets>
                                                                <telerik:EditorSnippet Name="Product Template" Value="<table><tr><td><div style='width:300px;border:2px outset #D8D2BD;padding:34px;font-family:Arial, Verdana; font-size:11px;background- color:#F1EFE6;'>Dear ____________________,<br>Thank you for inquiring about ____________. Your request will be processed in 48 hours and shipped at the address you have provided.<b>Please, contact us if you have any problems.</b></div></td></tr></table>">            
                                                                </telerik:EditorSnippet>
                                                                <telerik:EditorSnippet Name="Signature" Value="<strong>John Smith</strong><br>Sales Manager<br>">
                                                                </telerik:EditorSnippet>
                                                            </Snippets>
                                                            <Links>
                                                                <telerik:EditorLink Name="Telerik" Href="http://www.telerik.com">
                                                                    <telerik:EditorLink Name="Telerik Products" Href="http://www.telerik.com/products"
                                                                        Target="_blank" />
                                                                    <telerik:EditorLink Name="Telerik Support Center" Href="http://www.telerik.com/support/home.aspx"
                                                                        Target="_blank" />
                                                                    <telerik:EditorLink Name="Telerik Community" Href="http://www.telerik.com/community/home.aspx"
                                                                        Target="_blank" ToolTip="Telerik Community" />
                                                                </telerik:EditorLink>
                                                            </Links>
                                                            <Tools>
                                                            </Tools>
                                                            <ImageManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                                            <MediaManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                                            <FlashManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                                            <TemplateManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                                            <DocumentManager ViewPaths="~/Admin/Images" UploadPaths="~/Admin/Images" DeletePaths="~/Admin/Images,~/Admin/Images" />
                                                            <Modules>
                                                                <telerik:EditorModule Name="RadEditorStatistics" Visible="False" Enabled="true" />
                                                                <telerik:EditorModule Name="RadEditorDomInspector" Visible="False" Enabled="true" />
                                                                <telerik:EditorModule Name="RadEditorNodeInspector" Visible="False" Enabled="true" />
                                                                <telerik:EditorModule Name="RadEditorHtmlInspector" Visible="False" Enabled="true" />
                                                            </Modules>
                                                            <Content>
                                                            </Content>
                                                        </telerik:RadEditor>
                                                        <%--**********************************************************************************************--%>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="46%" align="left">
                                                    <a id="lnkSendAttachment" href="#" runat="server">Upload attachment</a> &nbsp;&nbsp
                                                    <a id="lnkViewAttachment" href="#" runat="server">View attachments</a>
                                                    <div id="divMessage1" runat="server" style="color: Red" visible="false">
                                                    </div>
                                                </td>
                                                <td width="6%">
                                                    &nbsp;
                                                </td>
                                                <td width="48%" align="right" valign="middle">
                                                    <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="blueBtns" OnClick="btnUpdate_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <br />
                        <div id="divRequestDetails" runat="server" style="width: 80%">
                            <asp:CheckBox ID="CheckAllasRead" runat="server" Text="Check all as read" Font-Bold="true"
                                OnCheckedChanged="CheckAllasRead_CheckedChanged" AutoPostBack="True" />
                            <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" Width="80%"
                                ShowHeader="False" OnItemDataBound="DataGrid1_ItemDataBound" OnItemCommand="DataGrid1_ItemCommand"
                                BorderStyle="solid" BorderColor="black" Style="margin: 5px 0 0 0;">
                                <Columns>
                                    <asp:BoundColumn DataField="RequestId" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="CommentId" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="PostedBy" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="PostedOn" Visible="False"></asp:BoundColumn>
                                    <asp:TemplateColumn>
                                        <ItemTemplate>
                                            <div style="border-top: 1px; border-left: 1px; border-bottom: 1px; border-right: 1px;
                                                border-color: black;">
                                                <table width="100%">
                                                    <tr>
                                                        <td align="left">
                                                            <asp:Label ID="lblPosted" runat="server" ForeColor="Green"></asp:Label>
                                                            &nbsp;
                                                            <asp:LinkButton ID="lnkReadMark" runat="server" CommandName="ReadMark">(Unread. Mark as read)</asp:LinkButton>
                                                        <%--************************************************************************************--%>
                                                            <%--<asp:Label ID="lblipAdd" runat="server" Text="IP:"></asp:Label>
                                                            <asp:Label ID="lblid" runat="server" Text='<%#bind("Ip") %>'></asp:Label>
                                                            <asp:Label ID="lblMacipAdd" runat="server" Text="MacId:"></asp:Label>
                                                            <asp:Label ID="lblmacid" runat="server" Text='<%#bind("Macid") %>'></asp:Label>--%>
                                                         <%--************************************************************************************--%>
                                                        
                                                       </td>
                                                    </tr>
                                                    <tr>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <asp:Label ID="lblComment" runat="server" Text='<%# bind("Comment") %>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <uc2:footer ID="Footer1" runat="server" />
        <asp:HiddenField ID="CRProject" runat="server" />
        <asp:HiddenField ID="prevWebsiteURL" runat="server" />
        <asp:HiddenField ID="prevShortDescr" runat="server" />
        <asp:HiddenField ID="HdnClientCompany" runat="server" />
    </form>
</body>
</html>
