﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="esActivityLog.aspx.cs" Inherits="esActivityLog" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>EstateCRM</title>
</head>
<body>
    <form id="form1" runat="server">
   <table  width="500" align="center">
        <tr>
            <td align="center" colspan="2">
                &nbsp;</td>
            <td align="center">
                &nbsp;</td>
            <td align="center">
                &nbsp;</td>
            <td align="center">
                &nbsp;</td>
            <td align="center">
                &nbsp;</td>
            <td align="center">
                &nbsp;</td>
            <td align="center">
                &nbsp;</td>
            <td align="center">
                &nbsp;</td>
            <td align="center">
                &nbsp;</td>
            <td align="center">
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Company name" Width="100px"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlCompanyname" runat="server" AutoPostBack="True" 
                    DataTextField="companyname" DataValueField="companyname" 
                    onselectedindexchanged="ddlCompanyname_SelectedIndexChanged" Width="250px">
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="Label9" runat="server" Text="Event type" Width="65px"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="ddlType_SelectedIndexChanged" Width="100px">
                    <asp:ListItem>All</asp:ListItem>
                    <asp:ListItem>New</asp:ListItem>
                    <asp:ListItem>Updated</asp:ListItem>
                    <asp:ListItem>Deleted</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="Label10" runat="server" Text="Date"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlDate" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="ddlDate_SelectedIndexChanged" Width="100px">
                    <asp:ListItem>All</asp:ListItem>
                    <asp:ListItem Value="0">Today</asp:ListItem>
                    <asp:ListItem Value="1">Yesterday</asp:ListItem>
                    <asp:ListItem Value="7">Last 7 days</asp:ListItem>
                    <asp:ListItem Value="30">Last 30 days</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="Label11" runat="server" Text="Number" Width="50px"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlNumber" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="ddlNumber_SelectedIndexChanged" Width="100px">
                    <asp:ListItem>All</asp:ListItem>
                    <asp:ListItem Value="10">Last 10</asp:ListItem>
                    <asp:ListItem Value="20">Last 20</asp:ListItem>
                    <asp:ListItem Value="30">Last 30</asp:ListItem>
                    <asp:ListItem Value="40">Last 40</asp:ListItem>
                    <asp:ListItem Value="50">Last 50</asp:ListItem>
                    <asp:ListItem Value="100">Last 100</asp:ListItem>
                    <asp:ListItem Value="500">Last 500</asp:ListItem>
                    <asp:ListItem Value="All in last 7 days">All in last 7 days</asp:ListItem>
                    <asp:ListItem Value="All in last 30 days">All in last 30 days</asp:ListItem>
                    <asp:ListItem Value="All in last 60 days">All in last 60 days</asp:ListItem>
                    <asp:ListItem Value ="All in last 360 days">All in last 360 days</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
               
    </table>
    
    <table align="center">
     <tr>
            <td colspan="3">
<asp:GridView ID="dgCompany" runat="server" AutoGenerateColumns="False" 
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" 
                    CellPadding="4" ForeColor="Black" GridLines="Vertical" AllowSorting="True" 
                    onrowcommand="dgCompany_RowCommand" onrowdeleting="dgCompany_RowDeleting1" 
                    onrowediting="dgCompany_RowEditing1">
                    <FooterStyle BackColor="#CCCC99" />
                    <RowStyle BackColor="#F7F7DE" HorizontalAlign="Left" />
                    <Columns>
                        <asp:TemplateField>
                        <HeaderTemplate>                        
                            <asp:LinkButton ID="LinkButton1" runat="server" Text="User Name" CommandName="sortusername" ForeColor="White" Font-Underline="False">User name</asp:LinkButton>
                        </HeaderTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("UserName") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("UserName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                        <HeaderTemplate>
                             <asp:LinkButton ID="LinkButton2" runat="server" Text="Client Id" CommandName="sortclientid" ForeColor="White" Font-Underline="False">Client Id</asp:LinkButton>
                        </HeaderTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("ClientId") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# Bind("ClientId") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                        <HeaderTemplate>
                        <asp:LinkButton ID="LinkButton3" runat="server" Text="Date time" CommandName="sortdatetime" ForeColor="White" Font-Underline="False">Date time</asp:LinkButton>
                        </HeaderTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("DateTime22") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# Bind("DateTime22") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField >
                        <HeaderTemplate>
                                        <asp:LinkButton ID="LinkButton4" runat="server" Text="Price" CommandName="sortprice" ForeColor="White" Font-Underline="False">Price</asp:LinkButton>
                        </HeaderTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("PriceP") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%# Bind("PriceP") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                         <HeaderTemplate>
                        <asp:LinkButton ID="LinkButton5" runat="server" Text="Type" CommandName="sorttype" ForeColor="White" Font-Underline="False">Type</asp:LinkButton>
                        </HeaderTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("DealType22") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label5" runat="server" Text='<%# Bind("DealType22") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                          
                                            
                        <asp:TemplateField >
                        <HeaderTemplate>
                       <asp:LinkButton ID="LinkButton6" runat="server" Text="Event type" CommandName="sorteventtype" ForeColor="White" Font-Underline="False">Event type</asp:LinkButton>
                        </HeaderTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("eventtype") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label6" runat="server" Text='<%# Bind("eventtype") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                         <HeaderTemplate>
                       <asp:LinkButton ID="LinkButton7" runat="server" Text="Description" CommandName="sortdescription" ForeColor="White" Font-Underline="False">Description</asp:LinkButton>
                        </HeaderTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox7" runat="server" Text='<%# Bind("Descreption") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label7" runat="server" Text='<%# Bind("Descreption") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                       <HeaderTemplate>
                        <asp:LinkButton ID="LinkButton8" runat="server" Text="Address" CommandName="sortaddress" ForeColor="White" Font-Underline="False">Address</asp:LinkButton>
                        </HeaderTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox8" runat="server" Text='<%# Bind("Address22") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label8" runat="server" Text='<%# Bind("Address22") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" 
                        HorizontalAlign="Left" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>

                <br />
                <asp:Label ID="lblDisplay" runat="server" ForeColor="Red" Text="Label" 
                    Visible="False"></asp:Label>
            </td>
        </tr>
    </table>
    </form>
   
</body>
</html>
