﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;


public partial class esActivityLog : System.Web.UI.Page
{
    string dealPrice;
    string dealType;
    DataSet cds = new DataSet();
    DataSet cdsRent = new DataSet();
    string strPricePer;
    DataSet dsDeleted = new DataSet();
    string strEventType;
    public static DataSet dsMain = new DataSet();
    string strPostCode;    
    bool flagClient = false;
    string strClient;
    string sort = string.Empty;
    public static DataView dv;
    protected void Page_Load(object sender, EventArgs e)
    {
       
            if (!(IsPostBack))
            {
                bindCompany();

            }
        
        
    }
    public void bindCompany()
    {
        DataSet ds = new DataSet();
        ds = DatabaseHelper.getCompany();
        try
        {
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlCompanyname.DataSource = ds;
                    ddlCompanyname.DataBind();
                    ddlCompanyname.Items.Insert(0, new ListItem("Select company", "Select company"));
                    int rcompany = ds.Tables[0].Rows.Count + 1;
                    ddlCompanyname.Items.Insert(rcompany, new ListItem("All", "All"));
                }
            }
        }
        catch { }
    }
    public void bindCompanyAll()
    {
        DataSet ds = new DataSet();
        ds = DatabaseHelper.getCompanyAll();
        try
        {
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    

                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            string[] Split;
                            string tick = dr["clientid"].ToString();
                            Split = tick.Split(',');
                            for (int j = 0; j < Split.Length; j++)                            
                            {

                                if (flagClient == false)
                                {
                                    strClient = "'" + dr["clientid"].ToString() + "',";
                                    flagClient = true;
                                }
                                else
                                {
                                    strClient = strClient + "'" + dr["clientid"].ToString() + "',";
                                }

                            }
                        }
                        if (strClient == "")
                        {
                            strClient = "No data";
                        }
                        if (strClient.EndsWith(","))
                        {
                            strClient = strClient.TrimEnd(new char[] { ',', ' ' });
                        }

                                      
                }
            }
        }
        catch { }
    }
    protected void ddlCompanyname_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCompanyname.SelectedValue != "Select company" && ddlCompanyname.SelectedValue != "All")
        {
            lblDisplay.Visible = false;
            sort = "";
            getClientid(ddlCompanyname.SelectedValue);
            bindGrid(strClient,"10");
            ddlType.SelectedIndex = 0;
            ddlDate.SelectedIndex = 0;
            ddlNumber.SelectedIndex = 0;
        }
        else if (ddlCompanyname.SelectedValue == "All")
        {
            lblDisplay.Visible = false;
            sort = "";
            bindCompanyAll();
            bindGrid(strClient,"10");
            ddlType.SelectedIndex = 0;
            ddlDate.SelectedIndex = 0;
            ddlNumber.SelectedIndex = 0;
        }
        
    }
    public void getClientid(string companyname)
    {

        DataSet ds = new DataSet();
        ds = DatabaseHelper.getCompanyClientid(companyname);
        try
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
               
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    string[] Split;
                    string tick = dr["clientid"].ToString();
                    Split = tick.Split(',');
                    for (int j = 0; j < Split.Length; j++)
                    {
                       
                            if (flagClient == false)
                            {
                                strClient = "'" + dr["clientid"].ToString() + "',";
                                flagClient = true;
                            }
                            else
                            {
                                strClient = strClient + "'" + dr["clientid"].ToString() + "',";
                            }
                        
                    }
                }
                if (strClient == "")
                {
                    strClient = "No data";
                }
                if (strClient.EndsWith(","))
                {
                    strClient = strClient.TrimEnd(new char[] { ',', ' ' });
                }

            }
            else
            {
                strClient = "No data";
                
            }
        }

        catch { }

    }
    public void bindGrid(string clientid,string strTop)
    {        
        dsMain = DatabaseHelper.bindCompanydetails(clientid,strTop);
        try
        {
            if (dsMain.Tables.Count > 0)
            {
                if (dsMain.Tables[0].Rows.Count > 0)
                {
                    dsMain = changedataset(dsMain);
                    if (sort=="")
                    {
                        OrderbyDate("DateTime desc");
                    }
                    dgCompany.DataSource = dv.ToTable();
                    dgCompany.DataBind();
                    lblDisplay.Visible = false;
                }
                else
                {
                    dgCompany.DataSource = dsMain;
                    dgCompany.DataBind();
                    lblDisplay.Visible = true;
                    lblDisplay.Text = "Record was not found";
                }
            }
        }
        catch { }
    }

    private DataSet changedataset(DataSet ds)
    {
        try
        {
            ds.Tables[0].Columns.Add("Dealtype22");
            ds.Tables[0].Columns.Add("PriceP");
            ds.Tables[0].Columns.Add("Address22");
            ds.Tables[0].Columns.Add("DateTime22"); 
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (ds.Tables[0].Rows[i]["PropertyId"].ToString() != "")
                {
                    if (ds.Tables[0].Rows[i]["eventtype"] != null && ds.Tables[0].Rows[i]["eventtype"].ToString().Trim() != "")
                    {
                        strEventType = ds.Tables[0].Rows[i]["eventtype"].ToString();
                    }
                    if (strEventType != "Deleted")
                    {
                        if (ds.Tables[0].Rows[i]["DealType"] != null && ds.Tables[0].Rows[i]["DealType"].ToString().Trim() != "")
                        {
                            string strDeal = ds.Tables[0].Rows[i]["DealType"].ToString();
                            if (strDeal == "1")
                            {
                                ds.Tables[0].Rows[i]["DealType22"] = "Buy";
                                bindSale(ds.Tables[0].Rows[i]["PropertyId"].ToString(), ds.Tables[0].Rows[i]["clientid"].ToString());
                                if (cds.Tables.Count > 0)
                                {
                                    if (cds.Tables[0].Rows.Count > 0)
                                    {
                                        dealPrice = cds.Tables[0].Rows[0]["bindSale"].ToString();
                                    }
                                    else
                                    {
                                        dealPrice = "";
                                    }
                                    ds.Tables[0].Rows[i]["PriceP"] = "£" + dealPrice;
                                }
                            }
                            else if (strDeal == "2")
                            {
                                ds.Tables[0].Rows[i]["DealType22"] = "Let";
                                bindRent(ds.Tables[0].Rows[i]["PropertyId"].ToString(), ds.Tables[0].Rows[i]["clientid"].ToString());
                                if (cdsRent.Tables.Count > 0)
                                {
                                    if (cdsRent.Tables[0].Rows.Count > 0)
                                    {
                                        dealPrice = cdsRent.Tables[0].Rows[0]["bindRent"].ToString();
                                        if (cdsRent.Tables[0].Rows[0]["PricePer"] != null && cdsRent.Tables[0].Rows[0]["PricePer"].ToString().Trim() != "")
                                        {
                                            string strPricep = statusPricePer(cdsRent.Tables[0].Rows[0]["PricePer"].ToString());
                                            dealPrice = dealPrice + " " + strPricep;
                                        }
                                    }
                                    else
                                    {
                                        dealPrice = "";
                                    }
                                    ds.Tables[0].Rows[i]["PriceP"] = "£" + dealPrice;
                                }
                            }
                            else if (strDeal == "4")
                            {

                                bindSale(ds.Tables[0].Rows[i]["PropertyId"].ToString(), ds.Tables[0].Rows[i]["clientid"].ToString());
                                if (cds.Tables.Count > 0)
                                {
                                    if (cds.Tables[0].Rows.Count > 0)
                                    {
                                        dealPrice = cds.Tables[0].Rows[0]["bindSale"].ToString();
                                        dealType = "Buy";
                                    }
                                    else
                                    {
                                        dealPrice = "";
                                        dealType = "";
                                    }
                                }
                                bindRent(ds.Tables[0].Rows[i]["PropertyId"].ToString(), ds.Tables[0].Rows[i]["clientid"].ToString());
                                if (cdsRent.Tables.Count > 0)
                                {
                                    if (cdsRent.Tables[0].Rows.Count > 0)
                                    {
                                        if (dealPrice != "")
                                        {
                                            dealPrice = dealPrice + " / " + cdsRent.Tables[0].Rows[0]["bindRent"].ToString();
                                        }
                                        else
                                        {
                                            dealPrice = cdsRent.Tables[0].Rows[0]["bindRent"].ToString();
                                        }
                                        if (cdsRent.Tables[0].Rows[0]["PricePer"] != null && cdsRent.Tables[0].Rows[0]["PricePer"].ToString().Trim() != "")
                                        {
                                            string priceper = statusPricePer(cdsRent.Tables[0].Rows[0]["PricePer"].ToString());

                                            dealPrice = dealPrice + " " + priceper;
                                        }
                                        if (dealType != "")
                                        {

                                            ds.Tables[0].Rows[i]["DealType22"] = dealType + "/" + "Let";
                                        }
                                        else
                                        {
                                            ds.Tables[0].Rows[i]["DealType22"] = "Let";
                                        }
                                    }
                                }
                                ds.Tables[0].Rows[i]["PriceP"] = "£" + dealPrice;
                            }
                        }
                        if (ds.Tables[0].Rows[i]["DateTime"] != null && ds.Tables[0].Rows[i]["DateTime"].ToString().Trim() != "")
                        {
                            DateTime dt = Convert.ToDateTime(ds.Tables[0].Rows[i]["DateTime"].ToString());
                            ds.Tables[0].Rows[i]["DateTime22"] = dt.ToShortDateString();
                        }
                        if (ds.Tables[0].Rows[i]["PostCode"] != null && ds.Tables[0].Rows[i]["PostCode"].ToString() != "")
                        {
                            strPostCode = ","+ ds.Tables[0].Rows[0]["PostCode"].ToString();
                        }
                            //ds.Tables[0].Rows[i]["Address22"] = ds.Tables[0].Rows[i]["Address"].ToString();
                            if (ds.Tables[0].Rows[i]["Address"] != null && ds.Tables[0].Rows[i]["Address"].ToString() != "")
                            {
                                string strAddress = ds.Tables[0].Rows[i]["Address"].ToString();
                                string strRegion = ds.Tables[0].Rows[i]["region"].ToString();
                                string strTown = ds.Tables[0].Rows[i]["town"].ToString();

                                if ((strAddress.ToString() != "") && (strRegion.ToString() != "") && (strTown.ToString() != ""))
                                {
                                    ds.Tables[0].Rows[i]["Address22"] = strAddress.ToString() + ", " + strRegion.ToString() + ", " + strTown.ToString()+ strPostCode;
                                }
                                else if ((strAddress.ToString() == "") && (strRegion.ToString() != "") && (strTown.ToString() != ""))
                                {
                                    ds.Tables[0].Rows[i]["Address22"] = strRegion.ToString() + ", " + strTown.ToString() + strPostCode;
                                }
                                else if ((strAddress.ToString() != "") && (strRegion.ToString() == "") && (strTown.ToString() != ""))
                                {
                                    ds.Tables[0].Rows[i]["Address22"] = strAddress.ToString() + ", " + strTown.ToString() + strPostCode;
                                }
                                else if ((strAddress.ToString() != "") && (strRegion.ToString() != "") && (strTown.ToString() == ""))
                                {
                                    ds.Tables[0].Rows[i]["Address22"] = strAddress.ToString() + ", " + strRegion.ToString() + strPostCode;
                                }
                                else if ((strAddress.ToString() != "") && (strRegion.ToString() == "") && (strTown.ToString() == ""))
                                {
                                    ds.Tables[0].Rows[i]["Address22"] = strAddress.ToString() + strPostCode;
                                }
                                else if ((strAddress.ToString() == "") && (strRegion.ToString() != "") && (strTown.ToString() == ""))
                                {
                                    ds.Tables[0].Rows[i]["Address22"] = strRegion.ToString() + strPostCode;
                                }
                                else if ((strAddress.ToString() == "") && (strRegion.ToString() == "") && (strTown.ToString() != ""))
                                {
                                    ds.Tables[0].Rows[i]["Address22"] = strTown.ToString() + strPostCode;
                                }
                                //ds.Tables[0].Rows[i]["Address"] = ds.Tables[0].Rows[i]["Address"].ToString().TrimEnd(new char[] { ',', ' ' }).TrimStart(new char[] { ',', ' ' });// +postcode2;



                            }
                            else
                            {
                                ds.Tables[0].Rows[i]["Address22"] = "";
                            }
                        
                    }
                    else
                    {
                        if (ds.Tables[0].Rows[i]["Price"] != null && ds.Tables[0].Rows[i]["Price"].ToString().Trim() != "")
                        {
                            dealPrice = ds.Tables[0].Rows[i]["Price"].ToString();
                        }
                        if (ds.Tables[0].Rows[i]["PricePer"] != null && ds.Tables[0].Rows[i]["PricePer"].ToString().Trim() != "")
                        {
                            dealPrice = dealPrice + " " + ds.Tables[0].Rows[i]["PricePer"].ToString();
                            ds.Tables[0].Rows[i]["DealType22"] = "Let";
                        }
                        else
                        {
                            ds.Tables[0].Rows[i]["DealType22"] = "Buy";
                        }
                        ds.Tables[0].Rows[i]["PriceP"] = "£" + dealPrice;

                        if (ds.Tables[0].Rows[i]["PropertyId"] != null && ds.Tables[0].Rows[i]["PropertyId"].ToString().Trim() != "")
                        {
                            ds.Tables[0].Rows[i]["Address22"] = ds.Tables[0].Rows[i]["PropertyId"].ToString();
                        }
                        if (ds.Tables[0].Rows[i]["DateTime"] != null && ds.Tables[0].Rows[i]["DateTime"].ToString().Trim() != "")
                        {
                            DateTime dt = Convert.ToDateTime(ds.Tables[0].Rows[i]["DateTime"].ToString());
                            ds.Tables[0].Rows[i]["DateTime22"] = dt.ToShortDateString();
                        }
                    }                    
                }



            }

            
        }
        catch { }
        return ds;
    }
    public DataSet bindSale(string propertyid,string clientid)
    {       
        
        cds = DatabaseHelper.bindSale(propertyid, clientid);
        cds.Tables[0].Columns.Add("bindSale");
        if (cds.Tables.Count > 0)
        {
            if (cds.Tables[0].Rows.Count > 0)
            {
                if (cds.Tables[0].Rows[0]["AskingPrice"] != null && cds.Tables[0].Rows[0]["AskingPrice"].ToString().Trim() != "") 
                {
                    cds.Tables[0].Rows[0]["bindSale"] = cds.Tables[0].Rows[0]["AskingPrice"].ToString();
                }
            }
        }
        return cds;
    }

    public DataSet bindRent(string propertyid, string clientid)
    {
        cdsRent = DatabaseHelper.bindRent(propertyid, clientid);
        cdsRent.Tables[0].Columns.Add("bindRent");
        if (cdsRent.Tables.Count > 0)
        {
            if (cdsRent.Tables[0].Rows.Count > 0)
            {
                if (cdsRent.Tables[0].Rows[0]["Price"] != null && cdsRent.Tables[0].Rows[0]["Price"].ToString().Trim() != "")
                {
                    cdsRent.Tables[0].Rows[0]["bindRent"] = cdsRent.Tables[0].Rows[0]["Price"].ToString();
                }
            }
        }
        return cds;
    }

    public string statusPricePer(string priceper)
    {
        switch (priceper)
        {
            case "1":
                strPricePer = "Month";
                break;
            case "2":
                strPricePer = "Quarter";
                break;
            case "3":
                strPricePer = "Year";
                break;
            default:
                strPricePer = "Week";
                break;
        }
        return strPricePer;
    }

    protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
    {
        sortDS(ddlType.SelectedValue);
        //ddlDate.SelectedIndex = 0;
    }

    public void OrderbyDate(string sort)
    {
        try
        {           
            if (dsMain.Tables.Count > 0)
            {
                if (dsMain.Tables[0].Rows.Count > 0)
                {
                    
                    dv = dsMain.Tables[0].DefaultView;
                    dv.Sort = sort;
                    //dv.Table.Clear();
                    //dsMain.Tables.Clear();
                    //dsMain.Tables.Add(dv.ToTable());
                    //dgCompany.DataSource = dsMain.Tables[0].DefaultView;
                    //dgCompany.DataBind();
                   // dsMain.AcceptChanges();

                    dgCompany.DataSource = dv.ToTable();
                    dgCompany.DataBind();
                }
            }            
            
        }
        catch { }
    }
    public void sortDS(string typeName)
    {
        try
        {
            if (typeName != "All")
            {
                if (dsMain.Tables.Count > 0)
                {
                    if (dsMain.Tables[0].Rows.Count > 0)
                    {

                        if (ddlDate.SelectedValue != "All")
                        {
                            bindByDateandEventtype();
                        }
                        else if (ddlType.SelectedValue != "All" && ddlNumber.SelectedValue!="All")
                        {                            
                             //bindNumberCombo();
                            
                                dsMain.Tables[0].DefaultView.RowFilter = "eventtype='" + typeName + "'";
                                dgCompany.DataSource = dsMain.Tables[0].DefaultView;
                                dgCompany.DataBind();
                                lblDisplay.Visible = false;                   
                        }                       
                        else
                        {                            
                            dsMain.Tables[0].DefaultView.RowFilter = "eventtype='" + typeName + "'";
                            if (dsMain.Tables[0].DefaultView.Count > 0)
                            {
                                dgCompany.DataSource = dsMain.Tables[0].DefaultView;

                                dgCompany.DataBind();
                                lblDisplay.Visible = false;
                            }
                            else
                            {
                                dgCompany.DataSource = dsMain.Tables[0].DefaultView;
                                dgCompany.DataBind();
                                lblDisplay.Visible = true;
                                lblDisplay.Text = "Record not found";
                            }                            
                          
                        }
                    }
                   
                }
            }
            else if ((typeName == "All") && (ddlDate.SelectedValue != "All"))
            {
                bindByDateandEventtype();
            }
            else if ((typeName == "All") && (ddlDate.SelectedValue == "All") && (ddlNumber.SelectedValue != "All"))
            {
                bindNumberCombo();
            }
            else
            {
                dgCompany.DataSource = dsMain;
                dgCompany.DataBind();
                lblDisplay.Visible = false;
            }
        }
        catch { }
    }


    protected void ddlDate_SelectedIndexChanged(object sender, EventArgs e)
    {
        //ddlNumber.SelectedIndex = 0;
        bindByDateandEventtype();
    }
    public void bindByDateandEventtype()
    {
        try
        {
            if (ddlDate.SelectedValue != "All")
            {
                if (dsMain.Tables.Count > 0)
                {
                    if (dsMain.Tables[0].Rows.Count > 0)
                    {
                        if (ddlType.SelectedValue != "All")
                        {
                            strEventType = ddlType.SelectedValue;
                        }
                        if (ddlDate.SelectedValue == "1")
                        {
                            DataTable dt;
                            if (ddlCompanyname.SelectedValue == "All" && ddlType.SelectedValue == "All" && ddlDate.SelectedValue != "All" && ddlNumber.SelectedValue == "All")
                            {
                                dt = dsMain.Tables[0];
                            }
                            else
                            {
                                dt = dv.ToTable();
                            }
                            DataView dv2 = new DataView(dt);
                            string EndDate, StartDate = "";
                            DateTime myDateTime1 = DateTime.Parse(System.DateTime.Now.ToShortDateString(), System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat);

                            StartDate = myDateTime1.ToString("MM/dd/yyyy");
                            DateTime myDateTime2 = DateTime.Parse(System.DateTime.Now.ToShortDateString(), System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat);
                            myDateTime2 = myDateTime2.AddDays(-1);

                            EndDate = myDateTime2.ToString("MM/dd/yyyy");
                            if (ddlType.SelectedValue != "All")
                            {
                                dv2.RowFilter = "eventtype='" + strEventType + "' and DateTime <=#" + StartDate + "# and DateTime>=#" + EndDate + "#";
                            }
                            else
                            {
                                dv2.RowFilter = "DateTime <=#" + StartDate + "# and DateTime>=#" + EndDate + "#";
                            }
                            if (dv2.Count >= 1)
                            {
                                lblDisplay.Visible = false;
                                dgCompany.DataSource = dv2.ToTable();
                                dgCompany.DataBind();
                            }
                            else
                            {
                                lblDisplay.Visible = true;
                                lblDisplay.Text = "Record was not found";
                                dgCompany.DataSource = dv2.ToTable();
                                dgCompany.DataBind();
                            }
                        }
                        else if (ddlDate.SelectedValue == "7" || ddlDate.SelectedValue == "30")
                        {
                            double ddays = Convert.ToDouble(ddlDate.SelectedValue.ToString());
                            DataTable dt;
                            if (ddlCompanyname.SelectedValue == "All" && ddlType.SelectedValue == "All" && ddlDate.SelectedValue != "All" && ddlNumber.SelectedValue == "All")
                            {
                                dt = dsMain.Tables[0];
                            }
                            else
                            {
                                dt = dv.ToTable();
                            }
                            DataView dv2 = new DataView(dt);
                            string EndDate, StartDate = "";
                            DateTime myDateTime1 = DateTime.Parse(System.DateTime.Now.ToShortDateString(), System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat);

                            StartDate = myDateTime1.ToString("MM/dd/yyyy");
                            DateTime myDateTime2 = DateTime.Parse(System.DateTime.Now.ToShortDateString(), System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat);
                            myDateTime2 = myDateTime2.AddDays(-ddays);

                            EndDate = myDateTime2.ToString("MM/dd/yyyy");
                            if (ddlType.SelectedValue != "All")
                            {
                                dv2.RowFilter = "eventtype='" + strEventType + "' and DateTime <=#" + StartDate + "# and DateTime>=#" + EndDate + "#";
                            }
                            else
                            {
                                dv2.RowFilter = "DateTime <=#" + StartDate + "# and DateTime>=#" + EndDate + "#";
                            }
                            if (dv2.Count >= 1)
                            {
                                lblDisplay.Visible = false;
                                dgCompany.DataSource = dv2.ToTable();
                                dgCompany.DataBind();
                            }
                            else
                            {
                                lblDisplay.Visible = true;
                                lblDisplay.Text = "Record was not found";
                                dgCompany.DataSource = dv2.ToTable();
                                dgCompany.DataBind();
                            }
                        }

                        else
                        {
                            DataTable dt;
                            if (ddlCompanyname.SelectedValue == "All" && ddlType.SelectedValue == "All" && ddlDate.SelectedValue != "All" && ddlNumber.SelectedValue == "All")
                            {
                                dt = dsMain.Tables[0];
                                
                            }
                            else
                            {
                                dt = dv.ToTable();
                            }
                            DataView dv2 = new DataView(dt);
                            string EndDate, StartDate = "";
                            DateTime myDateTime1 = DateTime.Parse(System.DateTime.Now.ToShortDateString(), System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat);
                            //  myDateTime1 = myDateTime1.AddDays(1);
                            StartDate = myDateTime1.ToString("MM/dd/yyyy");

                            DateTime myDateTime2 = DateTime.Parse(System.DateTime.Now.ToShortDateString(), System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat);
                            myDateTime2 = myDateTime2.AddDays(1);
                            EndDate = myDateTime2.ToString("MM/dd/yyyy");
                            if (ddlType.SelectedValue != "All")
                            {
                                dv2.RowFilter = "eventtype='" + strEventType + "' and DateTime >#" + StartDate + "# and DateTime<=#" + EndDate + "#";
                            }
                            else
                            {
                                dv2.RowFilter = "DateTime >#" + StartDate + "# and DateTime<#" + EndDate + "#";
                            }
                            if (dv2.Count >= 1)
                            {
                                lblDisplay.Visible = false;
                                dgCompany.DataSource = dv2.ToTable();
                                dgCompany.DataBind();
                            }
                            else
                            {
                                lblDisplay.Visible = true;
                                lblDisplay.Text = "Record was not found";
                                dgCompany.DataSource = dv2.ToTable();
                                dgCompany.DataBind();
                            }

                        }

                    }
                }
            }
            else if (ddlType.SelectedValue != "All" && (ddlNumber.SelectedValue == "All"))
            {
                sortDS(ddlType.SelectedValue);
            }
            else if ((ddlNumber.SelectedValue != "All" && ddlType.SelectedValue == "All") || (ddlNumber.SelectedValue != "All" && ddlType.SelectedValue != "All"))
            {
                bindNumberCombo();
            }
            else if (ddlNumber.SelectedValue == "All" && ddlType.SelectedValue == "All" && ddlDate.SelectedValue=="All" && ddlCompanyname.SelectedValue=="All")
            {
                dgCompany.DataSource = dv.ToTable();
                dgCompany.DataBind();
                lblDisplay.Visible = false;
            }  
            else
            {
                ddlType.SelectedIndex = 0;
                dgCompany.DataSource = dsMain;
                dgCompany.DataBind();
                lblDisplay.Visible = false;
            }

        }
        catch { }
    }


    public void bindNumberCombo()
    {
        if (ddlNumber.SelectedValue != "All")
        {
            if (ddlNumber.SelectedValue != "All in last 7 days" && ddlNumber.SelectedValue != "All in last 30 days" && ddlNumber.SelectedValue != "All in last 60 days" && ddlNumber.SelectedValue != "All in last 360 days")
            {
               // bindNumberCombowithselectedvalue(ddlNumber.SelectedValue);
                if (ddlCompanyname.SelectedValue != "All" && ddlCompanyname.SelectedValue != "Select company")
                {
                    getClientid(ddlCompanyname.SelectedValue);
                    bindGrid(strClient, ddlNumber.SelectedValue);
                    if(ddlType.SelectedValue!="All" && ddlDate.SelectedValue!="All")
                    {
                        sortDS(ddlType.SelectedValue);
                    }
                    else if (ddlType.SelectedValue != "All" && ddlDate.SelectedValue == "All")
                    {
                        sortDS(ddlType.SelectedValue);
                    }

                }
                else if (ddlCompanyname.SelectedValue != "All" && ddlCompanyname.SelectedValue != "Select company" && ddlType.SelectedValue=="All" && ddlNumber.SelectedValue=="All" && ddlDate.SelectedValue=="All")
                {
                    getClientid(ddlCompanyname.SelectedValue);
                    bindGrid(strClient, "10");
                    if (ddlType.SelectedValue != "All" && ddlDate.SelectedValue != "All")
                    {
                        sortDS(ddlType.SelectedValue);
                    }
                    else if (ddlType.SelectedValue != "All" && ddlDate.SelectedValue == "All")
                    {
                        sortDS(ddlType.SelectedValue);
                    }

                }
                else if (ddlCompanyname.SelectedValue == "All" && ddlCompanyname.SelectedValue != "Select company" && ddlType.SelectedValue != "All" && ddlNumber.SelectedValue != "All" && ddlDate.SelectedValue != "All")
                {
                    //getClientid(ddlCompanyname.SelectedValue);
                    //bindGrid(strClient, "10");
                    sortDS(ddlType.SelectedValue);


                }
                else if (ddlCompanyname.SelectedValue == "All" && ddlCompanyname.SelectedValue != "Select company" && ddlType.SelectedValue != "All" && ddlNumber.SelectedValue != "All" && ddlDate.SelectedValue == "All")
                {
                    //getClientid(ddlCompanyname.SelectedValue);
                    //bindGrid(strClient, "10");
                    sortDS(ddlType.SelectedValue);


                }
                else
                {
                    bindCompanyAll();
                    bindGrid(strClient, "10");
                }
            }
            else
            {
                
                bindCompanyAll();
                ddlCompanyname.SelectedValue = "All";
                bindGrid(strClient,"3");
                
                
                //ddlType.SelectedIndex = 0;
                ddlDate.SelectedIndex = 0;
                //ddlNumber.SelectedIndex = 0;
                if (ddlNumber.SelectedValue == "All in last 7 days")
                {
                    bindNumberCombowithselectedvalue("7");
                }
                else if (ddlNumber.SelectedValue == "All in last 30 days")
                {
                    bindNumberCombowithselectedvalue("30");
                }
                else if (ddlNumber.SelectedValue == "All in last 60 days")
                {
                    bindNumberCombowithselectedvalue("60");
                }
                else if (ddlNumber.SelectedValue == "All in last 360 days")
                {
                    bindNumberCombowithselectedvalue("360");
                }
            }
        }
        else if (ddlNumber.SelectedValue == "All" && ddlType.SelectedValue != "All")
        {
            sortDS(ddlType.SelectedValue);
        }
        else if (ddlNumber.SelectedValue == "All" && ddlType.SelectedValue == "All" && ddlCompanyname.SelectedValue != "All")
        {
            getClientid(ddlCompanyname.SelectedValue);
            bindGrid(strClient,"3");
        }
        else if ((ddlNumber.SelectedValue == "All" && ddlType.SelectedValue == "All" && ddlCompanyname.SelectedValue == "All") || (ddlNumber.SelectedValue == "All" && ddlType.SelectedValue != "All" && ddlCompanyname.SelectedValue == "All"))
        {

            lblDisplay.Visible = false;
            dgCompany.DataSource = dv.ToTable();
            dgCompany.DataBind();

        }
        else
        {
            lblDisplay.Visible = true;
            lblDisplay.Text = "Record was not found";
            dgCompany.DataSource = dsMain;
            dgCompany.DataBind();
        }
    }
    public void bindNumberCombowithselectedvalue(string strValue)
    {
        double ddays = Convert.ToDouble(strValue);
        DataTable dt = dv.ToTable();
        DataView dv2 = new DataView(dt);
        string EndDate, StartDate = "";
        DateTime myDateTime1 = DateTime.Parse(System.DateTime.Now.ToShortDateString(), System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat);
        myDateTime1 = myDateTime1.AddDays(1);
        StartDate = myDateTime1.ToString("MM/dd/yyyy");
        DateTime myDateTime2 = DateTime.Parse(System.DateTime.Now.ToShortDateString(), System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat);
        myDateTime2 = myDateTime2.AddDays(-ddays);

        EndDate = myDateTime2.ToString("MM/dd/yyyy");
        if (ddlType.SelectedValue != "All")
        {
            dv2.RowFilter = "eventtype='" + ddlType.SelectedValue + "' and DateTime <=#" + StartDate + "# and DateTime>=#" + EndDate + "#";
        }
        else
        {
            dv2.RowFilter = "DateTime <=#" + StartDate + "# and DateTime>=#" + EndDate + "#";
        }
        if (dv2.Count >= 1)
        {
            lblDisplay.Visible = false;
            dgCompany.DataSource = dv2.ToTable();
            dgCompany.DataBind();
        }
        else
        {
            lblDisplay.Visible = true;
            lblDisplay.Text = "Record was not found";
            dgCompany.DataSource = dv2.ToTable();
            dgCompany.DataBind();
        }
    }
    protected void dgCompany_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "sortclientid")
        {
            sort = "clientid asc";
            OrderbyDate(sort);
            
        }
        if (e.CommandName == "sortusername")
        {
            sort = "UserName asc";
            OrderbyDate(sort);
        }
        if (e.CommandName == "sortdatetime")
        {
            sort = "DateTime desc";
            OrderbyDate(sort);
        }
        if (e.CommandName == "sortprice")
        {
            sort = "Price desc";
            OrderbyDate(sort);
           
        }
        if (e.CommandName == "sorttype")
        {
            sort = "Dealtype22 asc";
            OrderbyDate(sort);
        }
        if (e.CommandName == "sorteventtype")
        {
            sort = "eventtype desc";
            OrderbyDate(sort);
        }
        if (e.CommandName == "sortdescription")
        {
            sort = "descreption asc";
            OrderbyDate(sort);
        }
        if (e.CommandName == "sortaddress")
        {
            sort = "Address asc";
            OrderbyDate(sort);
        }
        if ((ddlType.SelectedValue != "All" || ddlDate.SelectedValue != "All") && ddlCompanyname.SelectedValue != "All")
        {
            sortDS(ddlType.SelectedValue);
        }
        else if (ddlType.SelectedValue != "All" && ddlDate.SelectedValue == "All" && ddlCompanyname.SelectedValue == "All" && ddlNumber.SelectedValue=="All")
        {
            sortDS(ddlType.SelectedValue);
        }
        else if (ddlType.SelectedValue == "All" && ddlDate.SelectedValue != "All" && ddlCompanyname.SelectedValue == "All" && ddlNumber.SelectedValue == "All")
        {
            bindByDateandEventtype();
        }
        
        else if ((ddlNumber.SelectedValue != "All" && ddlType.SelectedValue == "All") || (ddlNumber.SelectedValue != "All" && ddlType.SelectedValue != "All"))
        {
            bindNumberCombo();
        }
        else if ((ddlNumber.SelectedValue == "All" && ddlType.SelectedValue == "All" && ddlDate.SelectedValue == "All" && ddlCompanyname.SelectedValue != "All"))
        {
            if (dv.Count > 0)
            {
                lblDisplay.Visible = false;
                dgCompany.DataSource = dv.ToTable();
                dgCompany.DataBind();
            }
            else
            {
                lblDisplay.Visible = false;
                dgCompany.DataSource = dsMain;
                dgCompany.DataBind();
            }
        }
        else if ((ddlNumber.SelectedValue == "All" && ddlType.SelectedValue == "All" && ddlDate.SelectedValue == "All" && ddlCompanyname.SelectedValue == "All"))
        {
            lblDisplay.Visible = false;
            dgCompany.DataSource = dv.ToTable();
            dgCompany.DataBind();
        }
        else
        {
            if (dsMain.Tables.Count > 0)
            {
                if (dsMain.Tables[0].Rows.Count > 0)
                {
                    lblDisplay.Visible = false;
                    dgCompany.DataSource = dsMain;
                    dgCompany.DataBind();
                }
            }
            else if (dsMain.Tables.Count > 0)
            {
                if (dsMain.Tables[0].Rows.Count > 0)
                {
                    lblDisplay.Visible = false;                    
                    dgCompany.DataSource = dsMain;
                    dgCompany.DataBind();
                }
            }
            else
            {
                lblDisplay.Visible = true;
                lblDisplay.Text = "Record was not found";
                dgCompany.DataSource = dsMain;
                dgCompany.DataBind();
            }
        }
    }
    protected void dgCompany_RowDeleting1(object sender, GridViewDeleteEventArgs e)
    {

    }
    protected void dgCompany_RowEditing1(object sender, GridViewEditEventArgs e)
    {

    }

    protected void ddlNumber_SelectedIndexChanged(object sender, EventArgs e)
    {
        //ddlDate.SelectedIndex=0;
        bindNumberCombo();
    }
}
