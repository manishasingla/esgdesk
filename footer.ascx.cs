using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class footer : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        dvFooterRight.InnerHtml = "Copyright � ESG 2005 - " + DateTime.Today.Year.ToString() + ". All Rights Reserved";

        if (Session["clientid"] == null && Session["username"] == null)
        {
            ShowSMPFlash.Visible = false;
        }
        else
        {


            object objUserId = DatabaseHelper.executeScalar("select ShowSMP from  NonesCRMusers where UserName = '" + Session["username"].ToString() + "'");
            if (objUserId != null)
            {
                if (objUserId.ToString() == "True")
                {
                    ShowSMPFlash.Visible = true;
                }
                else
                {
                    ShowSMPFlash.Visible = false;
                }
            }
            else
            {
                ShowSMPFlash.Visible = false;
            }


        }

    }
}
