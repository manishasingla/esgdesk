<%@ Page Language="C#" AutoEventWireup="true" CodeFile="forgot_password.aspx.cs" Inherits="forgot_password" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="Header.ascx" TagName="Header" TagPrefix="uc1" %>
<%@ Register Src="footer.ascx" TagName="footer" TagPrefix="uc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title><%= ConfigurationManager.AppSettings["CompanyName"].ToString()%></title>
     <link href="Admin/CSS/stylesheet1.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        function GetRadioButtonValue()
        {
            var radio = document.getElementById('rblEstateCRM');
            var options = radio.getElementsByTagName("input");
            
            if(document.getElementById('chkEmail').checked == true)
            {
                ValidatorEnable(document.getElementById('RegularExpressionValidator1'), true);
            }
            else
            {
                ValidatorEnable(document.getElementById('RegularExpressionValidator1'), false);
            }
                       
            if (options[1].checked)
            {
                document.getElementById('divNonEsCRMuser').style.display="Block";
                document.getElementById('divEsCRMuser').style.display="none";
                //document.getElementById('txtClientId').disabled=false;
                ValidatorEnable(document.getElementById('RequiredFieldValidator1'), true);
            }            
            else
            {
                document.getElementById('divEsCRMuser').style.display="Block";
                document.getElementById('divNonEsCRMuser').style.display="none";
                //document.getElementById('txtClientId').disabled=true;
                ValidatorEnable(document.getElementById('RequiredFieldValidator1'), false);
            }
            
            
        }                
    </script>
    
</head>
<body onload="GetRadioButtonValue();">
    <form id="form1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" EnablePageMethods="true" runat="server">
</asp:ScriptManager>
    <div id="wrapper_login">
  <div class="centralizer">
    <div class="authentication" >
      <div>
        <div class="content" style=" width:729px; height:275px;">
          <div class="logo_login"> 
            <!-- <a href="#" class="logo_text">estateCRM</a>--><img src="images/esg-desklogo.png" width="160" height="35" style="padding-top:10px;" />
            </div><div style="float:right; margin-top:-75px; font-size:13px;"><a href="login.aspx"> Client login</a>&nbsp;|&nbsp;<a href="Admin/login.aspx">Support login</a></div>
           
          <h4 class="ctx-clr"> Change request system </h4>
          <div class="message warning"> </div>
          <div class="form">
            <div class="field" style="width:560px;">Do you have a login already for estateCRM?
            <asp:RadioButtonList CssClass="whitetext1" ID="rblEstateCRM" runat="server" RepeatColumns="2" style="float:right; cellspacing=2">
                    <asp:ListItem>Yes &nbsp;</asp:ListItem>
                    <asp:ListItem Selected="True"> No</asp:ListItem>
                </asp:RadioButtonList>
            <asp:RequiredFieldValidator ControlToValidate="rblEstateCRM" CssClass="whitetext1" Display="None"
                    ErrorMessage="Please ensure Do you have a login already for estateCRM?" ID="RequiredFieldValidator4" runat="server">*</asp:RequiredFieldValidator>
                    
                    
              </div>
              
              
            <div class="field inline" style="margin-top:20px; width:95%">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="55%" valign="top"><div> <span style="font-size:20px;"><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtUserName"
                        Display="Dynamic" ErrorMessage="Please enter username or email.">*</asp:RequiredFieldValidator>Username or Email address  <br />
               <asp:TextBox ID="txtUserName" runat="server" Width="350px"></asp:TextBox>
                    
                    <asp:RegularExpressionValidator
                            ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtUserName"
                            ErrorMessage="Please enter valid email address." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic">*</asp:RegularExpressionValidator>
                    <cc1:ValidatorCalloutExtender
                            ID="ValidatorCalloutExtender2" runat="server" TargetControlID="RequiredFieldValidator2"> </cc1:ValidatorCalloutExtender>
                    <cc1:ValidatorCalloutExtender
                        ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RegularExpressionValidator1"> </cc1:ValidatorCalloutExtender></span>
                         </div></td>
                         
    <td width="45%" align="left" valign="top"><div class="buttons" style="margin-top:30px; margin-left:20px;">
                 <asp:Button CssClass="loginbutton" ID="btnForgotPassword" runat="server" Text="Send Password" OnClick="btnForgotPassword_Click" />        
           
         
            </div></td>
  </tr>
</table>
<div class="field" style="margin-top: 10px; width: 540px; font-size: 14px;" id="trClientId"> <asp:CheckBox CssClass="whitetext1" ID="chkEmail" runat="server" Text=" Tick here if using email address instead of username." /></div>
            
            	
              
                  
             
          
                        
            
            <div id="divMessage" runat="server" style="color: red; font-size:14px; padding-top:20px;" visible="true">
            
          </div>
        </div>
        
        <cc1:ValidatorCalloutExtender
                        ID="ValidatorCalloutExtender4" runat="server" TargetControlID="RequiredFieldValidator4">                    </cc1:ValidatorCalloutExtender>       
      </div>
    </div>
  </div>
</div>


</div>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    </div>  
   
    </form>
</body>
</html>