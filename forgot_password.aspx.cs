using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class forgot_password : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            rblEstateCRM.Attributes.Add("onclick", "GetRadioButtonValue();");
            chkEmail.Attributes.Add("onclick", "GetRadioButtonValue();");
        }
    }
    protected void btnForgotPassword_Click(object sender, EventArgs e)
   {

       string ClientId = "0";
            divMessage.InnerHtml = "";
            
            bool isEmail = chkEmail.Checked == true ? true : false;

            DataSet ds = DatabaseHelper.getUserNamePassword(ClientId,txtUserName.Text.Trim(),isEmail);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                bool flag = DatabaseHelper.sendEmailChangeRequestNorply(ds.Tables[0].Rows[0]["Email"].ToString(), "Login details from " + ConfigurationManager.AppSettings["CompanyName"].ToString(), generateEmail(ds));
                flag = DatabaseHelper.sendEmailChangeRequestNorply(ConfigurationManager.AppSettings["ToEmail1"].ToString(), "Login details from " + ConfigurationManager.AppSettings["CompanyName"].ToString(), generateEmail(ds));
               /// flag = DatabaseHelper.sendEmailChangeRequestNorply(ConfigurationManager.AppSettings["ToEmail1"].ToString(), "Login details from " + ConfigurationManager.AppSettings["CompanyName"].ToString(), generateEmail(ds));

                divMessage.InnerHtml = "Your login details has been sent to provided email address.";
                divMessage.Visible = true;

            }
            else
            {
                if (chkEmail.Checked == true)
                {
                    //divMessage.InnerHtml = "You must enter either your username or your email address to receive a password by email.";
                    divMessage.InnerHtml = "You must enter your email address to receive a password by email.";
                    //divMessage.InnerHtml = "The account with the email address you have provided does not exist. Please enter the email address you registered with.";
                    divMessage.Visible = true;
                }
                else
                {

                    divMessage.InnerHtml = "You must enter your username to receive a password by email.";
                    //divMessage.InnerHtml = "The account with the username you have provided does not exist. Please enter the username you registered with.";
                    divMessage.Visible = true;
                }
            }
        
    }

    private string generateEmail(DataSet ds)
    {

        ///string sb = "";
        string strHeader = "";
        string serverpath = ConfigurationManager.AppSettings["WebAddress"];
        //body of mail for the person
        strHeader += "<br>";
        strHeader += "<span style=\'font-size:11.0pt;font-family:Calibri,sans-serif;color:red\'>Please do not reply to this email as it does not take replies. Please use the Change Request System as this is the quickest way to process your requests and also keeps a record of communications and progress for you to review at any time.</span><br/>";
        strHeader += "<br>";
        strHeader += "You have requested the login details linked to email address: " + ds.Tables[0].Rows[0]["Email"].ToString() + ".<br> The login details are as follows: ";
        strHeader += "<br>";
        strHeader += "Username: "+ ds.Tables[0].Rows[0]["Username"].ToString() + "<br>";
        strHeader += "Password: " + ds.Tables[0].Rows[0]["Password"].ToString() + "<br><br>";
        strHeader += "<a href=\"" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/login.aspx\">Click here to login</a><br><br>";

        strHeader += "------------------";
        strHeader += "<br><br>";
        strHeader += ConfigurationManager.AppSettings["CompanyName"];
        strHeader += "<br>";
        strHeader += ConfigurationManager.AppSettings["WebAddress"];
        strHeader += "<br>";
        strHeader += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Do not start a new change request as this will slow the resolution.<br>";
        strHeader += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Forgot your username or password? Click <a href='http://www.esgdesk.com/SendConfirmation.aspx?username=" + ds.Tables[0].Rows[0]["Username"].ToString() + "' target='_blank'>here </a>to have it sent to your registered email address.";

        return strHeader;
    }

    
}
