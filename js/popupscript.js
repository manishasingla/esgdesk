﻿// JScript File

// ajax stuff

var xmlHttp
var ajax_url="getComment.aspx?id="
var current_element
var current_bug
var enable_popups=true;

function find_position(obj) {
	var curleft = curtop = 0;
	
	curleft = obj.offsetLeft
	curtop = obj.offsetTop
	
	
	
	if (obj.offsetParent) {
		while (obj = obj.offsetParent) {
			curleft += obj.offsetLeft
			curtop += obj.offsetTop
		}
	}
	return [curleft,curtop];
}

function get_bug_comment(bugid)
{
    xmlHttp=GetXmlHttpObject()
	if (xmlHttp==null)
	{
		return
	}

	var url = ajax_url + bugid
	xmlHttp.onreadystatechange=stateChanged
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
}

function stateChanged()
{
	if (current_element != null)
	{
		if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
		{

			var popup = document.getElementById("popup");
			if (xmlHttp.responseText != "")
			{
				popup.innerHTML = current_bug + ": " + xmlHttp.responseText
				var pos = find_position(current_element)

                popup.style.left = pos[0] + 30 + "px";
				popup.style.top = pos[1] + 30 + "px";
				
				popup.style.display = "block";

			}
		}
	}
}

function GetXmlHttpObject()
{
	var objXMLHttp=null
	if (window.XMLHttpRequest)
	{
		objXMLHttp=new XMLHttpRequest()
	}
	else if (window.ActiveXObject)
	{
		objXMLHttp=new ActiveXObject("Microsoft.XMLHTTP")
	}
	return objXMLHttp
}

function maybe_get_bug_comment(bug)
{
	// if they have already moved to another bug,
	// ignore where they HAD been hovering
	if (bug == current_bug)
	{
		get_bug_comment(current_bug)
	}
}

function on_mouse_over(el)
{

    
	if (enable_popups)
	{
	    current_element = el;
		pos = el.href.indexOf("=")
		pos++ // start with char after the =
		current_bug = el.href.substr(pos)
		// get comment if the user keeps hovering over this
		setTimeout('maybe_get_bug_comment(' + current_bug + ')', 400)
	}
}

function on_mouse_out()
{
	var popup = document.getElementById("popup");
	popup.style.display = "none";
	current_element = null
}
