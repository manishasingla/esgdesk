<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="login" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Src="footer.ascx" TagName="footer" TagPrefix="uc2" %>

<%@ Register Src="Header.ascx" TagName="Header" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <%--<title><%= ConfigurationManager.AppSettings["CompanyName"].ToString()%> - Login</title>--%>
    <title>Login</title>
  <!--  <link href="StyleSheet.css" rel="stylesheet" type="text/css" />-->
    <link href="Admin/CSS/stylesheet1.css" rel="stylesheet" type="text/css" />
    
    <script language="javascript" type="text/javascript">
        function trim (str)
        {
            return str.replace (/^\s+|\s+$/g, '');
        } 
      
        function Validate()
        {
            var msg ="";
            alert('Hi');
            if(document.getElementById("rbYes").checked==false && document.getElementById("rbNo").checked==false)
            {
                msg += " - Please ensure are you EstateCRM user?.\n";   
            }
            if(document.getElementById("rbYes").checked==true)
            {
                if(trim(document.getElementById("txtClientId").value) == "")
                {
                    msg += " - Enter client id.\n";   
                }
            }
            if(trim(document.getElementById("txtUserName").value) == "")
            {
                msg += " - Enter user name.\n";   
            }
            if(trim(document.getElementById("txtPassword").value) == "")
            {
                msg += " - Enter password.\n";   
            }                       
            
            if(msg!="")
            {
                alert(msg);
                return false;
            }            
            return true;
        }
        
        function GetRadioButtonValue() {

            //alert("Test");
            var radio = document.getElementById('rblEstateCRM');
            //alert(radio);
            var options = radio.getElementsByTagName("input");
            //alert(options);
           
            if (options[0].checked) {
               
                document.getElementById('trClientId').style.visibility="visible";
                document.getElementById('txtClientId').disabled=false;
                ValidatorEnable(document.getElementById('RequiredFieldValidator1'), true);
            }            
            else {
               
                document.getElementById('trClientId').style.visibility="hidden";
                document.getElementById('txtClientId').disabled=true;
                ValidatorEnable(document.getElementById('RequiredFieldValidator1'), false);
            }
        }        
    </script>
</head>
<body onload="GetRadioButtonValue();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" EnablePageMethods="true" runat="server">
</asp:ScriptManager>
    
    
    
    <div id="wrapper_login">
  <div class="centralizer">
    <div class="authentication" >
      <div>
        <div class="content" style=" width:729px; height:350px;">
          <div class="logo_login"> 
            <!-- <a href="#" class="logo_text">estateCRM</a>--><img src="images/esg-desklogo.png" width="160" height="35" style="padding-top:10px;" />
            </div><div style="float:right; margin-top:-75px; font-size:13px;"><a href="login.aspx"> Client login</a>&nbsp;|&nbsp;<a href="Admin/login.aspx">Support login</a></div>
           
          <h4 class="ctx-clr"> Change request system </h4>
          <div class="message warning"> </div>
          <div class="form">
            <div class="field"><span style="color: Red"> <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtUserName" ErrorMessage="Please enter username.">*</asp:RequiredFieldValidator> 
            </span>User name
              <asp:TextBox ID="txtUserName" runat="server" 
                                    Width="315px"></asp:TextBox>
                             
                        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" TargetControlID="RequiredFieldValidator2"> </cc1:ValidatorCalloutExtender>
              <span id="RequiredFieldValidator1" title="Enter user name" style="color:Red;display:none;"></span> </div>
              
            <div class="field" style="float:right"> <span style="color: Red"> <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtPassword"
                        ErrorMessage="Please enter password." CssClass="whitetext2">*</asp:RequiredFieldValidator>
            </span>Password
                <asp:TextBox ID="txtPassword" runat="server" 
                                    Width="315px" TextMode="Password"></asp:TextBox>
                               
              <span id="Span1" title="Enter password" style="color:Red;display:none;"></span> 
               &nbsp;&nbsp;&nbsp;
                              <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server" TargetControlID="RequiredFieldValidator3"> </cc1:ValidatorCalloutExtender>     </div>
              
            <div class="field inline" style="margin-top:10px; width:95%">
              <div> <span style="font-size:14px;">Do you have a login already for estateCRM? 
                <asp:RadioButtonList ID="rblEstateCRM" runat="server" RepeatColumns="2" 
                      Height="20px" Width="113px">
                            <asp:ListItem>Yes</asp:ListItem>
                            <asp:ListItem Selected="True">No</asp:ListItem>
                          </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="rblEstateCRM" ErrorMessage="Please ensure Do you have a login already for estateCRM?" Display="None">*</asp:RequiredFieldValidator>     </span>    </div>
                            <div style="font-size:14px; margin-top:10px;">If you have an estateCRM account you can login using the username, password & clientID. To find those details look in ther Admin module in estateCRM under Date Tables then Users. </div>
            </div>
            
            
            <div class="field" style="margin-top:10px;" id="trClientId"> <span style="color: Red">*</span> Client ID:
              <asp:TextBox ID="txtClientId" runat="server" Width="300px"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtClientId"
                    ErrorMessage="Please enter client id.">*</asp:RequiredFieldValidator>
      <cc1:ValidatorCalloutExtender
                        ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator1"> </cc1:ValidatorCalloutExtender>  </div>
            
           <%-- <div class="field"> <span style="color: Red">*</span> Client ID:
              <input name="txtClientID" id="txtClientID" onkeypress="return isNumberKey(event);" style="width: 315px;" type="text">
              <span id="Span2" title="Enter client ID" style="color:Red;display:none;"></span> </div>--%>
              
              
            <div class="buttons" style="float:left; margin-left:7px;">
             
                 <asp:Button ID="btnLogin" runat="server" OnClick="btnLogin_Click" Text="Client login" 
                                                        CssClass="loginbutton" TabIndex="3" />
              
                <div style="font-size:13px; margin-left:130px; margin-top:-25px;"><a href="forgot_password.aspx">Forgot your password?</a></div>
              
            </div>
            
            
            <!--<footer>
              <ul class="flags">
                <li><span id="lblMessage" style="color:Red;"></span> </li>
              </ul>
              <div id="ValidationSummary1" style="color:Red;width:300px;display:none;"> </div>
            </footer>-->
            
            <div id="divMessage" runat="server" style="color: red; float:left;" visible="true">
            
          </div>
        </div>
        
        
        <asp:CheckBox ID="chkSaveDetails" runat="server" Checked="false"  style="display:none;"/>   
      </div>
    </div>
  </div>
</div>


</div>
    
   </form>   
    
    
    
    
    
    
    
    
    
    
    
   <%-- <div id="wrapper" style="display:none;">
        <uc1:Header ID="Header1" runat="server" />
    <div id="Content">
    
   <!-- <div id="pageTitle">&nbsp;</div>-->
    <div style="text-align: left">
    <table width="450">
            
            <tr>
                <td width="30">&nbsp;                    </td>
                <td><table width="100%" border="0" cellspacing="2">
                  
                  <tr>
                    <td align="left" valign="top"><table width="420" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td align="left" valign="top"><img src="images/loginBox_top.png" width="420" 
                                height="16" /></td>
                      </tr>
                      <tr>
                        <td align="left" valign="top" style="background: url(images/loginBox_mid.png) repeat-y left top;">
                            <table width="90%" border="0" align="center" cellpadding="8" cellspacing="0">
                          <tr>
                            <td align="left" valign="top" class="whitetext1">Username:<span style="color:Red">*</span> </td>
                            
                            <td align="right" valign="top"><asp:TextBox ID="txtUserName" runat="server" 
                                    Width="220px"></asp:TextBox>
                              <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" TargetControlID="RequiredFieldValidator2"> </cc1:ValidatorCalloutExtender>
                              <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtUserName"
                        ErrorMessage="Please enter username." CssClass="whitetext2">*</asp:RequiredFieldValidator></td>
                          </tr>
                          <tr>
                            <td align="left" valign="top" class="whitetext1">Password:<span style="color:Red">*</span> </td>
                            
                            <td align="right" valign="top">
                                <asp:TextBox ID="txtPassword" runat="server" 
                                    Width="220px" TextMode="Password"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtPassword"
                        ErrorMessage="Please enter password." CssClass="whitetext2">*</asp:RequiredFieldValidator>                              </td>
                          </tr>
                          

                          <tr>
                            <td align="left" valign="top" class="whitetext1">&nbsp;</td>
                            
                            <td align="right" valign="top" class="whitetext2">
                                <a href="forgot_password.aspx">Forgot your password?</a>&nbsp;&nbsp;&nbsp;
                              <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server" TargetControlID="RequiredFieldValidator3"> </cc1:ValidatorCalloutExtender>                            </td>
                          </tr>
                        </table></td>
                      </tr>
                      <tr>
                        <td align="left" valign="top" style="background: url(images/loginBox_mid.png) repeat-y left top;">
                            <table align="center" width="90%">
                      <tr>
                        <td class="whitetext2">Do you have a login already for estateCRM? </td>
                        
                        <td class="whitetext2"><asp:RadioButtonList ID="rblEstateCRM" runat="server" RepeatColumns="2">
                            <asp:ListItem>Yes</asp:ListItem>
                            <asp:ListItem Selected="True">No</asp:ListItem>
                          </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="rblEstateCRM"
                    ErrorMessage="Please ensure Do you have a login already for estateCRM?" Display="None">*</asp:RequiredFieldValidator>                        </td>
                      </tr>
                    </table></td>
                      </tr>
                      <tr>
                        <td align="left" valign="top"><img src="images/loginBox_btm.png" width="420" height="13" /></td>
                      </tr>
                    </table></td>
                  </tr>
                  
                  <tr>
                    <td align="left" valign="top">If you have an estateCRM account you can login using the username, password &amp; clientID. To find those details look in ther Admin module in estateCRM under Date Tables then Users. </td>
                  </tr>
                  <tr id="trClientId">
                    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr id="tr1">
    <td align="left" valign="top"> Client ID<span style="color:Red">*</span></td>
    <td align="right" style="width: 5px" valign="top"></td>
    <td align="left" valign="top"><asp:TextBox ID="txtClientId" runat="server" Width="300px"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtClientId"
                    ErrorMessage="Please enter client id.">*</asp:RequiredFieldValidator>
      <cc1:ValidatorCalloutExtender
                        ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator1"> </cc1:ValidatorCalloutExtender>    </td>
  </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td align="left" valign="top"><hr /></td>
                  </tr>
                  <tr>
                    <td align="left" valign="top" style="height: 90px"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                       <td align="left" valign="top"><asp:CheckBox ID="chkSaveDetails" runat="server" Checked="True" />        </td>
        <td align="left" valign="top"> Save details (check this if you want to be able to go straight into the change request site without having to login. You will need to have cookies enabled in your browser.) </td>
                      </tr><tr>
                      <td colspan="2"><cc1:ValidatorCalloutExtender
                        ID="ValidatorCalloutExtender4" runat="server" TargetControlID="RequiredFieldValidator4"> </cc1:ValidatorCalloutExtender>    </td>
                    </tr>
                      
                    </table></td>
                  </tr>
              </table>                    </td>
            </tr>
            <tr>
                <td>&nbsp;                    </td>
                <td><table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td width="31%"></td>
        <td width="69%" align="right" valign="top"><asp:Button ID="btnLogin" runat="server" OnClick="btnLogin_Click" Text="Login" CssClass="blueBtns" /></td>
      </tr>
    </table></td>
          </tr>
            <tr>
              <td>&nbsp;</td>
              <td><div id="divMessage" style="color:Red" runat="server" visible="false"></div></td>
            </tr>
        </table>
    </div>
        
    </div>       
      <div id="footer-right">Copyright � estate solutions 2005. All Rights Reserved</div>
--%>  
   
</body>
</html>
