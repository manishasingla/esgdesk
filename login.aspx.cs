using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //Check if the browser support cookies

            rblEstateCRM.Attributes.Add("onclick", "GetRadioButtonValue();");

            if (Request.QueryString["Id"] != null)
            {
                if (Request.QueryString["Id"].ToString().Trim() == "1004ed92-6d40-4b7f-ad05-93c521ad7e31")
                {
                    Session["UserName"] = "Hardik Shah";
                    Session["ClientId"] = "0";
                    Session["ClientRegid"] = "251";
                    Response.Redirect("ClientRequests.aspx", false);
                    return;
                }
            }
            if (Request.Browser.Cookies)
            {
                //Check if the cookies with name PBLOGIN exist on user's machine

                if (Request.Cookies["ADMINLOGIN"] != null)
                {
                    Response.Redirect("admin/login.aspx");
                    return;
                }

                if (Request.Cookies["ESLOGIN"] != null)
                {
                    txtUserName.Text = Request.Cookies["ESLOGIN"]["USERNAME"].ToString();
                    txtClientId.Text = Request.Cookies["ESLOGIN"]["CLIENTID"].ToString();
                    string pwd = Request.Cookies["ESLOGIN"]["PASSWORD"].ToString();
                    this.txtPassword.Attributes.Add("value", pwd);
                    // txtPassword.Text
                    /// Response.Cookies["ESLOGIN"].Expires = DateTime.Now.AddYears(1);
                    /// 

                    if (Request.Cookies["ESLOGIN"]["EsCRMUser"].ToString() == "Yes")
                    {
                        rblEstateCRM.Items[0].Selected = true;
                        rblEstateCRM.Items[1].Selected = false;
                    }
                    else
                    {
                        rblEstateCRM.Items[0].Selected = false;
                        rblEstateCRM.Items[1].Selected = true;
                    }

                    //Pass the user name and password to the VerifyLogin method
                    if (Request.Cookies["ESLOGIN"]["ClientRegid"] != null)
                    {
                        VerifyLogin(Request.Cookies["ESLOGIN"]["CLIENTID"].ToString(), Request.Cookies["ESLOGIN"]["USERNAME"].ToString(), Request.Cookies["ESLOGIN"]["PASSWORD"].ToString(), Request.Cookies["ESLOGIN"]["ClientRegid"].ToString());
                    }
                    else
                    {
                        string regid = getclientid(Request.Cookies["ESLOGIN"]["CLIENTID"].ToString(), Request.Cookies["ESLOGIN"]["USERNAME"].ToString(), Request.Cookies["ESLOGIN"]["PASSWORD"].ToString());


                        VerifyLogin(Request.Cookies["ESLOGIN"]["CLIENTID"].ToString(), Request.Cookies["ESLOGIN"]["USERNAME"].ToString(), Request.Cookies["ESLOGIN"]["PASSWORD"].ToString(), regid);

                    }
                }
                //else
                //{
                //    btnLogin_Click(sender,e);
                //}
            }
        }
    }

    public static string getclientid(string ClientId, string UserName, string Password)
    {
        object objResult = "0";
       

            objResult = DatabaseHelper.getclientid(ClientId, UserName, Password);

        
        if (objResult != null)
        {
            return objResult.ToString();
        }
        else
        {
            objResult = "0";
            return objResult.ToString();
        }
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {

        string UserName = txtUserName.Text.Trim();
        string ClientId = txtClientId.Text.Trim();
        string Password = txtPassword.Text.Trim();

        if (rblEstateCRM.Items[0].Selected == true)
        {
            ClientId = txtClientId.Text.Trim();
        }
        else if (rblEstateCRM.Items[1].Selected == true)
        {
            ClientId = "0";
        }
        else
        {
            divMessage.InnerHtml = "Please ensure are you EstateCRM user.";
            divMessage.Visible = true;
            return;
        }
        string ClientRegid = getclientid(ClientId, UserName, Password);
        if (chkSaveDetails.Checked)
        {
            //Check if the browser support cookies

            if ((Request.Browser.Cookies))
            {
                //Check if the cookie with name PBLOGIN exist on user's machine

                if ((Request.Cookies["ESLOGIN"] == null))
                {
                    //Create a cookie with expiry of 30 days
                    //////HttpCookie cookie = new HttpCookie("ESLOGIN");
                    //////cookie.Expires = DateTime.Now.AddYears(1);

                    Response.Cookies["ESLOGIN"].Expires = DateTime.Now.AddYears(1);
                    //Write clientid to the cookie
                    Response.Cookies["ESLOGIN"]["CLIENTID"] = ClientId;
                    //Write username to the cookie
                    Response.Cookies["ESLOGIN"]["USERNAME"] = UserName;
                    //Write password to the cookie
                    Response.Cookies["ESLOGIN"]["PASSWORD"] = Password;
                    //Write Regid to the cookie
                    Response.Cookies["ESLOGIN"]["ClientRegid"] = ClientRegid;

                    Response.Cookies["ESLOGIN"]["EsCRMUser"] = rblEstateCRM.Items[0].Selected == true ? rblEstateCRM.Items[0].Text : rblEstateCRM.Items[1].Text;
                }
                //If the cookie already exist then wirte the user name and password on the cookie
                else
                {
                    Response.Cookies["ESLOGIN"].Expires = DateTime.Now.AddYears(1);
                    Response.Cookies["ESLOGIN"]["CLIENTID"] = ClientId;
                    Response.Cookies["ESLOGIN"]["USERNAME"] = UserName;
                    Response.Cookies["ESLOGIN"]["PASSWORD"] = Password;
                    Response.Cookies["ESLOGIN"]["ClientRegid"] = ClientRegid;
                    Response.Cookies["ESLOGIN"]["EsCRMUser"] = rblEstateCRM.Items[0].Selected == true ? rblEstateCRM.Items[0].Text : rblEstateCRM.Items[1].Text;
                }
            }
        }

        VerifyLogin(ClientId, UserName, Password, ClientRegid);
    }
    protected void VerifyLogin(string ClientId, string UserName, string Password, string ClientRegid)
    {
        object objResult = "0";

     //   try
      //  {
            if (rblEstateCRM.Items[0].Selected == true)
            {
                objResult = DatabaseHelper.authenticateEsSCRMuser(ClientId, UserName, Password);
            }
            else if (rblEstateCRM.Items[1].Selected == true)
            {
                objResult = DatabaseHelper.authenticateLogin(ClientId, UserName, Password);
            }

            if (objResult.ToString() == "0")
            {
                divMessage.InnerHtml = "Incorrect clientid and/or username and/or password. Please try again with caps off.";
                divMessage.Visible = true;
            }
            else
            {
                Session["UserName"] = UserName;
                Session["ClientId"] = ClientId;
                Session["ClientRegid"] = ClientRegid;

                if (rblEstateCRM.Items[0].Selected == true)
                {
                    Session["ESCRMuser"] = "true";
                }
                else
                {
                    Session["ESCRMuser"] = "false";
                }
                if (Session["returnUrl"] != null)
                {
                    Response.Redirect(Session["returnUrl"].ToString(), false);
                    Session.Remove("returnUrl");
                }
                else
                {
                    Response.Redirect("ClientRequests.aspx", false);
                }
            }
     //   }
      //  catch
      //  { }
    }
}
