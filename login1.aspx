<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="login" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Src="footer.ascx" TagName="footer" TagPrefix="uc2" %>

<%@ Register Src="Header.ascx" TagName="Header" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title><%= ConfigurationManager.AppSettings["CompanyName"].ToString()%></title>
    <link href="StyleSheet.css" rel="stylesheet" type="text/css" />
    
    <script language="javascript" type="text/javascript">
        function trim (str)
        {
            return str.replace (/^\s+|\s+$/g, '');
        } 
      
        function Validate()
        {
            var msg ="";
            alert('Hi');
            if(document.getElementById("rbYes").checked==false && document.getElementById("rbNo").checked==false)
            {
                msg += " - Please ensure are you EstateCRM user?.\n";   
            }
            if(document.getElementById("rbYes").checked==true)
            {
                if(trim(document.getElementById("txtClientId").value) == "")
                {
                    msg += " - Enter client id.\n";   
                }
            }
            if(trim(document.getElementById("txtUserName").value) == "")
            {
                msg += " - Enter user name.\n";   
            }
            if(trim(document.getElementById("txtPassword").value) == "")
            {
                msg += " - Enter password.\n";   
            }                       
            
            if(msg!="")
            {
                alert(msg);
                return false;
            }            
            return true;
        }
        
        function GetRadioButtonValue()
        {
            var radio = document.getElementById('rblEstateCRM');
            var options = radio.getElementsByTagName("input");
            
            if (options[0].checked)
            {
                document.getElementById('trClientId').style.visibility="visible";
                document.getElementById('txtClientId').disabled=false;
                ValidatorEnable(document.getElementById('RequiredFieldValidator1'), true);
            }            
            else
            {
                document.getElementById('trClientId').style.visibility="hidden";
                document.getElementById('txtClientId').disabled=true;
                ValidatorEnable(document.getElementById('RequiredFieldValidator1'), false);
            }
        }        
    </script>
</head>
<body onload="GetRadioButtonValue();">
    <form id="form1" runat="server">
    <div id="wrapper">
        <uc1:Header ID="Header1" runat="server" />
    <div id="Content">
    
   <!-- <div id="pageTitle">&nbsp;</div>-->
    <div style="text-align: left">
    <table width="450">
            
            <tr>
                <td width="30">&nbsp;                    </td>
                <td><table width="100%" border="0" cellspacing="2">
                  
                  <tr>
                    <td align="left" valign="top"><table width="420" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td align="left" valign="top"><img src="images/loginBox_top.png" width="420" 
                                height="16" /></td>
                      </tr>
                      <tr>
                        <td align="left" valign="top" style="background: url(images/loginBox_mid.png) repeat-y left top;">
                            <table width="90%" border="0" align="center" cellpadding="8" cellspacing="0">
                          <tr>
                            <td align="left" valign="top" class="whitetext1">Username:<span style="color:Red">*</span> </td>
                            
                            <td align="right" valign="top"><asp:TextBox ID="txtUserName" runat="server" 
                                    Width="220px"></asp:TextBox>
                              <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" TargetControlID="RequiredFieldValidator2"> </cc1:ValidatorCalloutExtender>
                              <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtUserName"
                        ErrorMessage="Please enter username." CssClass="whitetext2">*</asp:RequiredFieldValidator></td>
                          </tr>
                          <tr>
                            <td align="left" valign="top" class="whitetext1">Password:<span style="color:Red">*</span> </td>
                            
                            <td align="right" valign="top">
                                <asp:TextBox ID="txtPassword" runat="server" 
                                    Width="220px" TextMode="Password"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtPassword"
                        ErrorMessage="Please enter password." CssClass="whitetext2">*</asp:RequiredFieldValidator>                              </td>
                          </tr>
                          

                          <tr>
                            <td align="left" valign="top" class="whitetext1">&nbsp;</td>
                            
                            <td align="right" valign="top" class="whitetext2">
                                <a href="forgot_password.aspx">Forgot your password?</a>&nbsp;&nbsp;&nbsp;
                              <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server" TargetControlID="RequiredFieldValidator3"> </cc1:ValidatorCalloutExtender>                            </td>
                          </tr>
                        </table></td>
                      </tr>
                      <tr>
                        <td align="left" valign="top" style="background: url(images/loginBox_mid.png) repeat-y left top;">
                            <table align="center" width="90%">
                      <tr>
                        <td class="whitetext2">Do you have a login already for estateCRM? </td>
                        
                        <td class="whitetext2"><asp:RadioButtonList ID="rblEstateCRM" runat="server" RepeatColumns="2">
                            <asp:ListItem>Yes</asp:ListItem>
                            <asp:ListItem Selected="True">No</asp:ListItem>
                          </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="rblEstateCRM"
                    ErrorMessage="Please ensure Do you have a login already for estateCRM?" Display="None">*</asp:RequiredFieldValidator>                        </td>
                      </tr>
                    </table></td>
                      </tr>
                      <tr>
                        <td align="left" valign="top"><img src="images/loginBox_btm.png" width="420" height="13" /></td>
                      </tr>
                    </table></td>
                  </tr>
                  
                  <tr>
                    <td align="left" valign="top">If you have an estateCRM account you can login using the username, password &amp; clientID. To find those details look in ther Admin module in estateCRM under Date Tables then Users. </td>
                  </tr>
                  <tr id="trClientId">
                    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr id="tr1">
    <td align="left" valign="top"> Client ID<span style="color:Red">*</span></td>
    <td align="right" style="width: 5px" valign="top"></td>
    <td align="left" valign="top"><asp:TextBox ID="txtClientId" runat="server" Width="300px"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtClientId"
                    ErrorMessage="Please enter client id.">*</asp:RequiredFieldValidator>
      <cc1:ValidatorCalloutExtender
                        ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator1"> </cc1:ValidatorCalloutExtender>    </td>
  </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td align="left" valign="top"><hr /></td>
                  </tr>
                  <tr>
                    <td align="left" valign="top" style="height: 90px"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                       <td align="left" valign="top"><asp:CheckBox ID="chkSaveDetails" runat="server" Checked="True" />        </td>
        <td align="left" valign="top"> Save details (check this if you want to be able to go straight into the change request site without having to login. You will need to have cookies enabled in your browser.) </td>
                      </tr><tr>
                      <td colspan="2"><cc1:ValidatorCalloutExtender
                        ID="ValidatorCalloutExtender4" runat="server" TargetControlID="RequiredFieldValidator4"> </cc1:ValidatorCalloutExtender>    </td>
                    </tr>
                      
                    </table></td>
                  </tr>
              </table>                    </td>
            </tr>
            <tr>
                <td>&nbsp;                    </td>
                <td><table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td width="31%"></td>
        <td width="69%" align="right" valign="top"><asp:Button ID="btnLogin" runat="server" OnClick="btnLogin_Click" Text="Login" CssClass="blueBtns" /></td>
      </tr>
    </table></td>
          </tr>
            <tr>
              <td>&nbsp;</td>
              <td><div id="divMessage" style="color:Red" runat="server" visible="false"></div></td>
            </tr>
        </table>
    </div>
        
    </div>       
      <div id="footer-right">Copyright � estate solutions 2005. All Rights Reserved</div>
    </form>
</body>
</html>
