using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class loginparameter : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string ClientId = "0";
        string UserName = "";
        string Password = "";
        string ESlogin = "";

        if (Request.QueryString["username"] != null && Request.QueryString["username"] != "0")
        {

            try
            {

                UserName = Request.QueryString["username"];

                if (Request.QueryString["password"] != null && Request.QueryString["password"] != "0")
                {
                    Password = Request.QueryString["password"];
                }

                if (Request.QueryString["Clientid"] != null && Request.QueryString["Clientid"] != "0")
                {

                    ClientId = Request.QueryString["Clientid"].ToString();
                }
                if (Request.QueryString["EsLogin"] != null && Request.QueryString["EsLogin"] !="0")
                {
                    ESlogin = Request.QueryString["EsLogin"].ToString();
                   /// Request.Cookies["ESLOGIN"] = ESlogin;
                }
               

                if ((Request.Browser.Cookies))
                {
                    //Check if the cookie with name PBLOGIN exist on user's machine

                    if ((Request.Cookies["ESLOGIN"] == null))
                    {
                        //Create a cookie with expiry of 30 days
                        //////HttpCookie cookie = new HttpCookie("ESLOGIN");
                        //////cookie.Expires = DateTime.Now.AddYears(1);

                        Response.Cookies["ESLOGIN"].Expires = DateTime.Now.AddYears(1);
                        //Write clientid to the cookie
                        Response.Cookies["ESLOGIN"]["CLIENTID"] = ClientId;
                        //Write username to the cookie
                        Response.Cookies["ESLOGIN"]["USERNAME"] = UserName;
                        //Write password to the cookie
                        Response.Cookies["ESLOGIN"]["PASSWORD"] = Password;
                        Session["UserName"] = UserName;
                        Session["ClientId"] = ClientId;

                        Response.Cookies["ESLOGIN"]["EsCRMUser"] = ESlogin;///rblEstateCRM.Items[0].Selected == true ? rblEstateCRM.Items[0].Text : rblEstateCRM.Items[1].Text;
                    }
                    //If the cookie already exist then wirte the user name and password on the cookie
                    else
                    {
                        Response.Cookies["ESLOGIN"].Expires = DateTime.Now.AddYears(1);
                        Response.Cookies["ESLOGIN"]["CLIENTID"] = ClientId;
                        Response.Cookies["ESLOGIN"]["USERNAME"] = UserName;
                        Response.Cookies["ESLOGIN"]["PASSWORD"] = Password;
                        Session["UserName"] = UserName;
                        Session["ClientId"] = ClientId;
                        Response.Cookies["ESLOGIN"]["EsCRMUser"] = ESlogin; //rblEstateCRM.Items[0].Selected == true ? rblEstateCRM.Items[0].Text : rblEstateCRM.Items[1].Text;
                    }
                }
                Session["UserName"] = UserName;
                Session["ClientId"] = ClientId;

                Response.Redirect("ClientRequests.aspx", false);

            }
            catch { }
        }

    }
}
