<%@ Page Language="C#" AutoEventWireup="true" CodeFile="phone_call.aspx.cs" Inherits="phone_call" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title><%= ConfigurationManager.AppSettings["CompanyName"].ToString()%></title>   
    <link href="StyleSheet.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div id="Content">
    <div id="pageTitle" style="height:30px;font-weight:bold;text-align:left">Phone call or message recorde <span style="padding-left:865px;"></span> <a href="http://www.esgdesk.com//Download/ESG_Desk.application" style="color:white; font-size:15px; text-decoration:bold; cursor:pointer;" onmouseover="this.style.color='blue'" onmouseout="this.style.color='#ffffff'">Click here to download the desktop application</a></div>
            <table>
            <tr>
                <td align="left" valign="top">Name of caller <span style="color:Red">*</span> 
                </td>
                <td align="left" valign="top">
                    <asp:TextBox ID="txtUserName" runat="server" MaxLength="50" Width="300px"></asp:TextBox></td>
                <td>
                </td>
            </tr>
            <tr>
                <td align="left" valign="top">
                    Company of caller <span style="color:Red">*</span></td>
                <td align="left" valign="top">
                    <asp:TextBox ID="TextBox1" runat="server" MaxLength="50" Width="300px"></asp:TextBox></td>
                <td>
                </td>
            </tr>
            <tr>
                <td align="left" valign="top">
                    Date/time <span style="color:Red">*</span></td>
                <td align="left" valign="top">
                </td>
                <td>
                </td>
            </tr>
                <tr>
                    <td align="left" valign="top">
                        Message <span style="color:Red">*</span></td>
                    <td align="left" valign="top">
                        <asp:TextBox ID="TextBox2" runat="server" Width="300px" Rows="5" TextMode="MultiLine"></asp:TextBox></td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        Priority</td>
                    <td align="left" valign="top">
                        <asp:DropDownList ID="drpPriority" runat="server" Width="150px">
                            <asp:ListItem Value="6">[no priority]</asp:ListItem>
                            <asp:ListItem Value="0">0 - IMMEDIATE</asp:ListItem>
                            <asp:ListItem Value="1">1 - highest</asp:ListItem>
                            <asp:ListItem Value="2" Selected="True">2</asp:ListItem>
                            <asp:ListItem Value="3">3</asp:ListItem>
                            <asp:ListItem Value="4">4</asp:ListItem>
                            <asp:ListItem Value="5">5 - lowest</asp:ListItem>
                        </asp:DropDownList>
                        </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        Project
                    </td>
                    <td align="left" valign="top">
                        <asp:TextBox ID="TextBox3" runat="server" MaxLength="50" Width="300px"></asp:TextBox></td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                    </td>
                    <td align="left" valign="top">
                        <asp:Button ID="Button1" runat="server" Text="Button" /></td>
                    <td>
                    </td>
                </tr>
        </table>    
    </div>
    </form>
</body>
</html>
