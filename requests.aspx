<%@ Page Language="C#" AutoEventWireup="true" CodeFile="requests.aspx.cs" Inherits="requests"  %>

<%@ Register Src="Header.ascx" TagName="Header" TagPrefix="uc1" %>
<%@ Register Src="footer.ascx" TagName="footer" TagPrefix="uc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title><%= ConfigurationManager.AppSettings["CompanyName"].ToString()%></title>
    <link href="StyleSheet.css" rel="stylesheet" type="text/css" />
    
<script type="text/javascript">
function get_(div_)
{

div_=div_.id+"1";

document.getElementById(div_).style.display="block";
}
function get_1(div_)
{

div_=div_.id+"1";

document.getElementById(div_).style.display="none";
}
</script>

<link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="slider.css" />

</head>
<body style="background-color:InactiveBorder">
    <form id="form1" runat="server">
    <div style="background-color:InactiveBorder">
    <div id="wrapper">
        <uc1:Header ID="Header1" runat="server" />
        <div id="Content"><div id="Content_98">
        <div id="divMessage" runat="server" style="color:Red;font-weight:bold;"></div> 
            
        <div>
        <table width="100%"><tr><td align="left"><span id="TopFilterLable" runat="server" style="font-weight:bold;font-size:larger;color:Red"></span></td><td align="right">   <span style="color:red;" id="NewCR" runat="server">Unread messages<asp:Button id="lnkUnread" runat="server" BackColor="transparent" ForeColor="blue"  BorderStyle="none" style="cursor:pointer;width:auto" CausesValidation="False" OnClick="lnkUnread_Click" /></span><span style="color:red;" id="SpanWOCR" runat="server">Message waiting for response<asp:Button id="lnkWORC" runat="server" BackColor="transparent" ForeColor="blue"  BorderStyle="none" style="cursor:pointer;width:auto" CausesValidation="False" OnClick="lnkWORC_Click"  /></span> <span><a id="CompNotesIcon" runat="server" title="Click to view notes" style="border:none; font-size:16px; color:Red; padding-left:5px; text-decoration:none; cursor:pointer;"></a></span></td></tr></table>
        
        </div>
        <div style="text-align:left">       
               
        <div>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" onClick="return TABLE1_onclick()" id="TABLE1">
        <tr>
          <td align="right" valign="bottom"><img src="images/filter_corner_l.png" width="11" height="59" /></td>
          
              <td bgcolor="#282C5F" width="100%"><div class="slider">
    <div class="slidercontent" id="slider">
      <div id="section-1" class="section upper" style="margin-top:0px;">
      <table border="0" cellpadding="0" width="100%" cellspacing="0">
            <tr>
                <td width="4%" align="left" valign="bottom"><span class="style1"></span><b style="color:#e1ac10;">Filter:</b></td>
                <td width="27%" valign="top" class="whitetext2" style="color:#ffffff;">Company name<br />
                    <asp:DropDownList ID="drpWebsiteUrl" runat="server" CssClass="filerDrpodown" Width="300px" AutoPostBack="True" OnSelectedIndexChanged="drpWebsiteUrl_SelectedIndexChanged">                    
                    </asp:DropDownList></td>
                <td width="16%" align="left" valign="top" class="whitetext2"style="color:#ffffff;">Status<br />
                  <asp:DropDownList ID="drpStatus" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpStatus_SelectedIndexChanged" Width="170px" CssClass="filerDrpodown">    
                    </asp:DropDownList> </td>
                <td width="12%" align="left" valign="bottom" class="whitetext2">
                    <asp:Button ID="btnClearFilters" runat="server" CssClass="clearfilter" OnClick="btnClearFilters_Click"
                        Text="Clear filters" />  </td>                  
                <%--=======================================================================================================================--%>
                <%--<td width="11%" align="left" valign="bottom" class="whitetext2">
                     <asp:CheckBox ID="chkViewDeleted" runat="server" style="color:#ffffff;" AutoPostBack="True" OnCheckedChanged="chkViewDeleted_CheckedChanged" Text="View deleted" /></td>
                <td width="18%" align="left" valign="bottom" class="whitetext2">
                    <asp:CheckBox ID="chkHideClosed" runat="server" Checked="true" style="color:#ffffff;" AutoPostBack="True" OnCheckedChanged="chkHideClosed_CheckedChanged"
                        Text="Hide closed" /></td>--%>
                    <%--=======================================================================================================================--%>
                    
                        <td width="12%" align="left" valign="bottom" style="color:#e1ac10;"><b>Records: </b> <asp:Label CssClass="whitetext2" ID="LblRecordsno" runat="server" Text=""  ></asp:Label></td>
            </tr>
        </table>
      </div>
      
              </div></div></td>
              <td valign="bottom"><img src="images/filter_corner_r.png" width="11" height="59" /></td>
             </tr>
        </table>
        </div>
        </div>
        
        <div style="clear:both"></div>
        <div style="float:left; width:100%; background:#ebebeb; padding:5px 0px;"><a href="add_changeRequest.aspx"> Add change request</a></div>
        <div style="clear:both"></div>
                
         
        
          <div>
          <asp:DataGrid ID="DataGrid1" runat="server" Width="100%" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" CellPadding="5" OnPageIndexChanged="DataGrid1_PageIndexChanged" PageSize="100" OnSortCommand="DataGrid1_SortCommand"  OnItemCommand="DataGrid1_ItemCommand" OnItemDataBound="DataGrid1_ItemDataBound">
             <PagerStyle NextPageText="Next" PrevPageText="Prev" />
             <HeaderStyle Font-Bold="false" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                 Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Top" BackColor="#282C5F" CssClass="tblTitle1" />
          <Columns>
             
                 <asp:BoundColumn DataField="RequestId" HeaderText="Id" Visible="false" SortExpression="RequestId" ></asp:BoundColumn>
                 
                 
                    <asp:TemplateColumn HeaderText="Short Description" SortExpression="ShortDescr" HeaderStyle-CssClass="wordwrap">
                     <ItemTemplate>
                         <asp:Label Font-Size="13px"  ID="lblshrtDesc" runat="server" CssClass="wordwrap" Text='<%# bind("ShortDescr") %>'></asp:Label>
                         
                          
                         <div id="lblshrtDesc1" runat="server" style="display: none; position: absolute; background-color: #FEFFB3; width:730px;  border:solid 1px #333333; margin-top:5px; padding-left:3px">
                         
                          <asp:Label ID="LblLastcomment" runat="server"  Text='<%# bind("ShowAllComment") %>'></asp:Label>
                         </div>
                     </ItemTemplate>
                 </asp:TemplateColumn>
                 
                 
                   <asp:TemplateColumn HeaderText="Read/Unread" SortExpression="" HeaderStyle-CssClass="wordwrap">
                     <ItemTemplate>
                         <asp:LinkButton ID="lnkReadMark" runat="server" Width="150px" CommandName="ReadMark">(Unread. Mark as read)</asp:LinkButton>
                     </ItemTemplate>
                 </asp:TemplateColumn>
                  <asp:BoundColumn DataField="CompanyName" HeaderText="Company name" SortExpression="CompanyName"></asp:BoundColumn>
              
                
                 <asp:BoundColumn DataField="Status" HeaderText="Status" SortExpression="ClientRequest.Status"></asp:BoundColumn>
                     <asp:BoundColumn DataField="LastUpdatedBy" HeaderText="Last Updated By" SortExpression="ClientRequest.LastUpdatedBy"></asp:BoundColumn>
                      <asp:TemplateColumn HeaderText="Last updated on" SortExpression="LastUpdatedOn">
                     <ItemTemplate>
                         <asp:Label ID="lblLastUpdatedOn" runat="server" Text='<%# bind("LastUpdatedOn") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateColumn>
                 <asp:BoundColumn DataField="AddedBy" HeaderText="Change added by" SortExpression="ClientRequest.AddedBy">
                 </asp:BoundColumn>
                 <asp:TemplateColumn HeaderText="Change added on" SortExpression="ClientRequest.RequestDate">
                     <ItemTemplate>
                         <asp:Label ID="lblChangeAddedOn" runat="server" Text='<%# bind("RequestDate") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateColumn>
                            
                 <asp:TemplateColumn HeaderText="Type" Visible="false" SortExpression="">
                     <ItemTemplate>
                        <asp:LinkButton ID="Supportype" runat="server" Width="100px" CommandName="SupprtLink">Support type</asp:LinkButton>
                  </ItemTemplate>
                 </asp:TemplateColumn>
                     <asp:BoundColumn DataField="RequestId" HeaderText="Id" SortExpression="RequestId"></asp:BoundColumn>
                 <asp:HyperLinkColumn HeaderText="Edit" Text="edit" DataNavigateUrlField="RequestId" DataNavigateUrlFormatString="edit_request.aspx?reqid={0}" NavigateUrl="edit_request.aspx"></asp:HyperLinkColumn>
             </Columns>
               <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                 Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Top" />
        </asp:DataGrid>
        </div>
        </div></div> 
     
        <uc2:footer ID="Footer1" runat="server" />  
        </div></div>  
    <asp:Label id="lblOrderBy" runat="server" Visible="false" Text=""></asp:Label> 
    </form>
    
    </body>
    </html>
