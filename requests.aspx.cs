using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.Drawing.Drawing2D;

public partial class requests : System.Web.UI.Page
{
    string sql = "";
    string companyname;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["clientid"] == null || Session["username"] == null || Session["clientid"].ToString() == "" || Session["username"].ToString() == "" )
        {
            Session["returnUrl"] = "requests.aspx";
            Response.Redirect("login.aspx", false);
            return;
        }
        
        if (!Page.IsPostBack)
        {
            BindWebsiteURL();
            load_statuses();
            lblOrderBy.Text = " ORDER BY ClientRequest.LastUpdatedOn desc";
            getFilterSession();
            getCRwaiting();
            getunreadmessage();
            BindData();

            try
            {
                string Clientcomp = "select [CompanyName] from [NonesCRMusers]  where UserName = '" + Session["username"].ToString() + "' and ClientId = " + Session["clientid"].ToString() + "";
                object objClientComp = DatabaseHelper.executeScalar(Clientcomp);
                if (objClientComp.ToString() != "")
                {

                    companyname = objClientComp.ToString();

                    string sqlCRnote = "select count(*) from [Company_notes]  where Company_Name = '" + objClientComp.ToString() + "' and Note_Type ='Client'  and Allow_Notes='True'";
                    int objCRNoteResult = int.Parse(DatabaseHelper.executeScalar(sqlCRnote).ToString());



                    if (objCRNoteResult > 0)
                    {
                        

                        CompNotesIcon.InnerHtml = "<img src=\"images/notes.png\" style=\"cursor:pointer\" title=\"Notifications\" width=\"25\" height=\"25\" alt=\"Notifications\" />" + "Notifications for " + objClientComp.ToString();
                        Session["TaskComp"] = companyname;
                        CompNotesIcon.Attributes.Add("onclick", "window.open('CRNotes.aspx','','status=no,scrollbars=yes,position=center,resizable=yes,width=screen.width,height=screen.height');");


                    }
                    else
                    {

                        CompNotesIcon.InnerHtml = "";
                    }

                }

            }
            catch { }

    
        }        
    }


    void load_statuses()
    {
        sql = "select status_name from statuses order by sort_seq, status_name;";
        DataSet ds_dropdowns = DatabaseHelper.getDataset(sql);
       drpStatus.DataSource = ds_dropdowns.Tables[0];
        drpStatus.DataTextField = "status_name";
        drpStatus.DataValueField = "status_name";
        drpStatus.DataBind();
        drpStatus.Items.Insert(0, new ListItem("[no status]", ""));


    }   



    void getunreadmessage()
    {
        sql = "select ClientRequest.* from ClientRequest where RequestId in(select ClientRequest_Details.RequestId from ClientRequest_Details where [CommentId] not in (select  CommentId from read_CR_comments where read_CR_comments.username='" + Session["username"].ToString() + "') and ClientRequest_Details.deleted <> 1) and UserName = '" + Session["username"].ToString() + "' and ClientRequest.deleted <> 1";
        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {

            lnkUnread.Text = "(" + ds.Tables[0].Rows.Count + ")";
               
            NewCR.Visible = true;
        }
        else
        {

            
             
            lnkUnread.Visible = false;
            NewCR.Visible = false;
         }
    }

    void getCRwaiting()
    {
        sql = @"select ClientRequest.* from ClientRequest 
                where ClientRequest.Status = 'awaiting client response- required' and UserName ='" + Session["username"].ToString()  + "' and status <> 'closed' and deleted <> 1 ";



        DataSet ds = DatabaseHelper.getDataset(sql);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {

            
                lnkWORC.Text = "(" + ds.Tables[0].Rows.Count + ")";
                SpanWOCR.Visible = true;
           

        }
        else
        {




            lnkWORC.Text = "(0)";
            SpanWOCR.Visible = false;
           


        }
    }


    private void BindData()
    {
        string strFilter = "";
        string strFilter2 = "";

        string topFilterLable = "";
        //***************************************************
        //if (chkViewDeleted.Checked == true)
        //{
        //    strFilter += " and ClientRequest.deleted = 1 ";
        //}
        //else
        //{
        //    strFilter += " and ClientRequest.deleted <> 1 ";
        //}

        //if (chkHideClosed.Checked == true)
        //{
        //    strFilter += " and ClientRequest.status <> 'closed' ";
        //}
        //***************************************************
        if (drpStatus.SelectedIndex != 0)
        {
            strFilter += "and ClientRequest.Status = '" + drpStatus.SelectedItem.Value + "' ";

            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Status: " + drpStatus.SelectedItem.Text;
            }
            else
            {
                topFilterLable += " / Status: " + drpStatus.SelectedItem.Text;
            }
        }

        //////if (drpWebsiteUrl.SelectedIndex != 0)
        //////{
        //////    if (strFilter == "")
        //////    {
        //////        strFilter += "and WebsiteURL = '" + drpWebsiteUrl.SelectedItem.Value + "' ";
        //////    }
        //////    else
        //////    {
        //////        strFilter += " and WebsiteURL = '" + drpWebsiteUrl.SelectedItem.Value + "' ";
        //////    }

        //////    if (topFilterLable == "")
        //////    {
        //////        topFilterLable = "Filtered by WebsiteURL: " + drpWebsiteUrl.SelectedItem.Text;
        //////    }
        //////    else
        //////    {
        //////        topFilterLable += " / WebsiteURL: " + drpWebsiteUrl.SelectedItem.Text;
        //////    }
        //////}

        if (drpWebsiteUrl.SelectedIndex != 0)
        {
            if (strFilter == "")
            {
                strFilter += "and CompanyName = '" + drpWebsiteUrl.SelectedItem.Value + "' ";
            }
            else
            {
                strFilter += " and CompanyName  = '" + drpWebsiteUrl.SelectedItem.Value + "' ";
            }

            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by Company name: " + drpWebsiteUrl.SelectedItem.Text;
            }
            else
            {
                topFilterLable += " / Company name: " + drpWebsiteUrl.SelectedItem.Text;
            }
        }
        //*************************************************
        //if (chkViewDeleted.Checked == true)
        //{
        //    if (topFilterLable == "")
        //    {
        //        topFilterLable = "Filtered by View deleted";
        //    }
        //    else
        //    {
        //        topFilterLable += " / View deleted";
        //    }
        //}

        //if (chkHideClosed.Checked == true)
        //{
        //    if (topFilterLable == "")
        //    {
        //        topFilterLable = "Filtered by Hide closed";
        //    }
        //    else
        //    {
        //        topFilterLable += " / Hide closed";
        //    }
        //}
        //*************************************************
        if (Session["filterunreadCR"] == null || Session["filterunreadCR"].ToString() == "")
        {

        }
        else
        {
            if (topFilterLable == "")
            {
                topFilterLable = "Filtered by  Unread messages";
            }
            else
            {
                topFilterLable += " / Unread messages";
            }

        }
        
        TopFilterLable.InnerHtml = topFilterLable;
       
////            sql = @"Select *,(select CompanyName from [NonesCRMusers] where  NonesCRMusers.UserName = ClientRequest.UserName) as companyname from ClientRequest Inner Join NonesCRMusers ON (NonesCRMusers.UserName = ClientRequest.UserName) 
////              where ClientRequest.ClientId=" + Session["clientid"].ToString() + " and ClientRequest.UserName='" + Session["username"].ToString().Replace("'", "''") + "' " + strFilter + " " + lblOrderBy.Text;
        try
        {

            if (Session["filterunreadCR"] == null || Session["filterunreadCR"].ToString() == "")
            {
                sql = @"Select * from ClientRequest 
              where (CRApprove='True' or CRApprove Is NULL) and ClientRequest.ClientId=" + Session["clientid"].ToString() + "  and  ClientRequest.status <> 'closed' and ClientRequest.deleted <> 1 and ClientRequest.UserName='" + Session["username"].ToString().Replace("'", "''") + "' " + strFilter + " " + lblOrderBy.Text;

            }
            else
            {
                sql = @"Select * from ClientRequest 
              where (CRApprove='True' or CRApprove Is NULL) and ClientRequest.ClientId=" + Session["clientid"].ToString() + " and   ClientRequest.status <> 'closed' and ClientRequest.deleted <> 1 and ClientRequest.RequestId in(select ClientRequest_Details.RequestId from ClientRequest_Details where [CommentId] not in (select  CommentId from read_CR_comments where read_CR_comments.username='" + Session["username"].ToString() + "') and ClientRequest_Details.deleted <> 1) and ClientRequest.UserName='" + Session["username"].ToString().Replace("'", "''") + "' " + strFilter + " " + lblOrderBy.Text;


            }
        }
        catch 
        {

            sql = @"Select * from ClientRequest 
              where (CRApprove='True' or CRApprove Is NULL) and ClientRequest.ClientId=" + Session["clientid"].ToString() + " and   ClientRequest.status <> 'closed' and ClientRequest.deleted <> 1 and ClientRequest.UserName='" + Session["username"].ToString().Replace("'", "''") + "' " + strFilter + " " + lblOrderBy.Text;
        }
        
      
       

        DataSet ds = DatabaseHelper.getDataset(sql);

        ds = formatCrsDataSet(ds);

        if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "No request found.";
            divMessage.Visible = true;
            DataGrid1.DataSource = null;
            DataGrid1.DataBind();
        }
        else
        {
            if (ds.Tables[0].Rows.Count <= 20)
            {
                DataGrid1.PagerStyle.Visible = false;
                DataGrid1.CurrentPageIndex = 0;
            }
            else
            {
                DataGrid1.PagerStyle.Visible = false;
            }

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (ds.Tables[0].Rows[i]["AddedBy"] != null && ds.Tables[0].Rows[i]["AddedBy"].ToString().Trim() != "")
                {
                    ds.Tables[0].Rows[i]["AddedBy"] = ds.Tables[0].Rows[i]["AddedBy"].ToString();
                }
                else
                {
                    ds.Tables[0].Rows[i]["AddedBy"] = ds.Tables[0].Rows[i]["UserName"].ToString();
                }
            }

            divMessage.InnerHtml = "";
            DataGrid1.DataSource = ds.Tables[0];
            DataGrid1.DataBind();
        }
    }

    private DataSet formatCrsDataSet(DataSet ds)
    {
        try
        {

            ds.Tables[0].Columns.Add("ShowAllComment");
            ///ds.Tables[0].Columns.Add("");

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                try
                {


                    if (ds.Tables[0].Rows[i]["ShortDescr"] != null && ds.Tables[0].Rows[i]["ShortDescr"].ToString().Trim() != "")
                    {

                        ds.Tables[0].Rows[i]["ShortDescr"] = "<a  href=\"edit_request.aspx?reqid=" + ds.Tables[0].Rows[i]["RequestId"].ToString() + "  \">" + ds.Tables[0].Rows[i]["ShortDescr"].ToString() + "</a>";

                    }
                    else
                    {

                        ds.Tables[0].Rows[i]["ShortDescr"] = "";
                    }



                    try
                    {

                        string LastComment = "";
                        /// Lastcomment2.Text = "";


                        sql = "Select * from ClientRequest_Details where deleted <> 1 and RequestId=" + ds.Tables[0].Rows[i]["RequestId"].ToString() + " order by CommentId desc";


                        DataSet dslastcomment = DatabaseHelper.getDataset(sql);

                        if (dslastcomment != null && dslastcomment.Tables[0].Rows.Count > 0)
                        {


                            for (int j = 0; j < dslastcomment.Tables[0].Rows.Count; j++)
                            {



                                LastComment += "<span style='color:Green'>" + "Comment " + dslastcomment.Tables[0].Rows[j]["CommentId"].ToString() + " posted by " + dslastcomment.Tables[0].Rows[j]["PostedBy"].ToString() + " on " + DateTime.Parse(dslastcomment.Tables[0].Rows[j]["PostedOn"].ToString()).ToString("dd MMM h:mm tt") + "</span>";

                                LastComment += "<br/><span>" + dslastcomment.Tables[0].Rows[j]["Comment"].ToString() + "</span>";

                                LastComment += "<span style=\"border-bottom: solid 1px green; margin-bottom: 2px; height:1px\"></span><br/><hr />";


                            }



                        }

                        ds.Tables[0].Rows[i]["ShowAllComment"] = LastComment.ToString();
                    }
                    catch
                    {

                    }





                }
                catch { }
            }
        }
        catch
        {
            Server.ClearError();
            ///Response.Redirect("fhs.aspx");
        }
        return ds;
    }

    protected void BindWebsiteURL()
    {
        DataSet ds = DatabaseHelper.getCompanyName(Session["clientid"].ToString(), Session["username"].ToString());
        
        if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0].Rows.Count <= 0)
        {
            ds = new DataSet();
            ds.Tables.Add();
            ds.Tables[0].Columns.Add("CompanyName");
        }
        DataRow row = ds.Tables[0].NewRow();
        row["CompanyName"] = "[no filter]";
        ds.Tables[0].Rows.InsertAt(row, 0);

        drpWebsiteUrl.DataSource = ds.Tables[0];
        drpWebsiteUrl.DataTextField = "CompanyName";
        drpWebsiteUrl.DataValueField = "CompanyName";
        drpWebsiteUrl.DataBind();
    }

    protected void DataGrid1_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        DataGrid1.CurrentPageIndex = e.NewPageIndex;
        BindData();
    }
    protected void DataGrid1_SortCommand(object source, DataGridSortCommandEventArgs e)
    {

        if (e.SortExpression.ToString() == Session["Column"])
        {
            //Reverse the sort order
            if (Session["Order"] == "ASC")
            {
                lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " DESC";
                Session["Order"] = "DESC";
            }
            else
            {
                lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " ASC";
                Session["Order"] = "ASC";
            }
        }
        else
        {
            //Different column selected, so default to ascending order
            lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " ASC";
            Session["Order"] = "ASC";
        }

        Session["Column"] = e.SortExpression.ToString();
        Session["OrderBy"] = lblOrderBy.Text;
        BindData();
        
        //lblOrderBy.Text = " ORDER BY " + e.SortExpression;
        //BindData();
    }
    protected void drpChangeBy_SelectedIndexChanged(object sender, EventArgs e)
    {
        //lblOrderBy.Text = " ORDER BY RequestId desc";
        setFileterSession();
        BindData();
    }
    protected void drpWebsiteUrl_SelectedIndexChanged(object sender, EventArgs e)
    {
        //lblOrderBy.Text = " ORDER BY RequestId desc";
        setFileterSession();
        //Session["filterunreadCR"] = "";
        BindData();
    }
    protected void drpStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        //lblOrderBy.Text = " ORDER BY RequestId desc";
        setFileterSession();
        //Session["filterunreadCR"] = "";
        BindData();
    }

    private void setFileterSession()
    {
        //***************************************************
        //Session["reqHideClosed"] = chkHideClosed.Checked;
        //Session["ViewDeleted"] = chkViewDeleted.Checked;
        //***************************************************

        Session["Status"] = drpStatus.SelectedValue;
        Session["WebsiteURL"] = drpWebsiteUrl.SelectedValue;
        Session["OrderBy"] = lblOrderBy.Text;
       // Session["filterunreadCR"] = "";
    }

    private void getFilterSession()
    {
        try
        {
            //***********************************************************
            //if (Session["reqHideClosed"] != null)
            //    chkHideClosed.Checked = (bool)Session["reqHideClosed"];

            //if (Session["ViewDeleted"] != null)
            //    chkViewDeleted.Checked = (bool)Session["ViewDeleted"];
            //***********************************************************
            
            if (Session["Status"] != null)
                drpStatus.SelectedValue = Session["Status"].ToString();

           if (Session["WebsiteURL"] != null)
                drpWebsiteUrl.SelectedValue = Session["WebsiteURL"].ToString();

            if (Session["OrderBy"] != null)
                lblOrderBy.Text = Session["OrderBy"].ToString();
        }
        catch
        {

        }
    }
    protected void DataGrid1_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemIndex != -1)
        {
            ((Label)e.Item.FindControl("lblChangeAddedOn")).Text = DateTime.Parse(((Label)e.Item.FindControl("lblChangeAddedOn")).Text).ToString("dd MMM h:mm tt").ToString();
            ((Label)e.Item.FindControl("lblLastUpdatedOn")).Text = DateTime.Parse(((Label)e.Item.FindControl("lblLastUpdatedOn")).Text).ToString("dd MMM h:mm tt").ToString();
        }


        LinkButton readMarkButton = (LinkButton)e.Item.FindControl("lnkReadMark");
        LinkButton LinksupportType = (LinkButton)e.Item.FindControl("Supportype");
       /// Label LblAllComment = (Label)e.Item.FindControl("lblshrtDesc");
        try
        {
            Label LblAllcomments = (Label)e.Item.FindControl("lblshrtDesc");
            LblAllcomments.Attributes.Add("onmouseover", "get_(this);");
            LblAllcomments.Attributes.Add("onmouseout", "get_1(this);");
        }
        catch { }
        
        int objIsRead;

        try
        {
            sql = "Select supportType,SupportUrl from NonesCRMusers where UserName in (select UserName from ClientRequest where RequestId = " + e.Item.Cells[0].Text + ")";
            DataSet dsSupport = DatabaseHelper.getDataset(sql);

            if (dsSupport != null && dsSupport.Tables[0].Rows.Count > 0)
            {
                if (dsSupport.Tables[0].Rows[0]["SupportUrl"].ToString() != "")
                {
                    LinksupportType.Text = "<span style='color:red'>" + dsSupport.Tables[0].Rows[0]["supportType"].ToString() + "</span>";
                    if (dsSupport.Tables[0].Rows[0]["SupportUrl"].ToString().Contains("http://") == true)
                    {
                        LinksupportType.Attributes.Add("onclick", "window.open('" + dsSupport.Tables[0].Rows[0]["SupportUrl"].ToString() + "','',',,');");
                    }
                    else
                    {
                        LinksupportType.Attributes.Add("onclick", "window.open('" + "http://" + dsSupport.Tables[0].Rows[0]["SupportUrl"].ToString() + "','',',,');");
                    }
                    LinksupportType.Enabled = true;
                }
                else
                {
                    LinksupportType.Text = "[<span style='color:Blue'>Not added</span>]";
                    LinksupportType.Enabled = false;
                    //LinksupportType.Attributes.Add("onclick", "window.open('" + dsSupport.Tables[0].Rows[0]["SupportUrl"].ToString() + "','',',,');");

                }


            }
        }
        catch { }

        try
        {
            sql = "select *  from ClientRequest_Details where [CommentId] not in (select CommentId from read_CR_comments where read_CR_comments.username='" + Session["username"].ToString() + "') and ClientRequest_Details.deleted <> 1 and ClientRequest_Details.RequestId= " + e.Item.Cells[0].Text;

            // sql = "select * from read_comments where tc_id not in(select tc_id from read_comments where username ='" + Session["admin"] + "')and task_comments.deleted <> 1 and task_id =" + e.Item.Cells[0].Text;

            DataSet ds = DatabaseHelper.getDataset(sql);

            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                //lblPosted.ForeColor = System.Drawing.Color.Red;

                readMarkButton.Text = "[<span style='color:red'>Unread.</span>  Mark as read]";

            }
            else
            {
                /// lblPosted.ForeColor = System.Drawing.Color.Green;
                readMarkButton.Text = "[<span style='color:green'>Read.</span> Mark as unread]";
            }
        }
        catch
        {

        }
    }

    protected void DataGrid1_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "ReadMark")
        {
            LinkButton readMarkButton = (LinkButton)e.Item.FindControl("lnkReadMark");

            if (readMarkButton.Text == "[<span style='color:red'>Unread.</span>  Mark as read]")
            {

                sql = @"select *  from ClientRequest_Details where RequestId =" + e.Item.Cells[0].Text + " and  CommentId  not in (select  CommentId from read_CR_comments where read_CR_comments.UserName='" + Session["username"].ToString() + "')";
                /// sql += "and tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.assigned_to_user = '" + Session["admin"].ToString() + "' ";

                DataSet ds = DatabaseHelper.getDataset(sql);

                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        try
                        {


                            string sqlinsert = "insert into read_CR_comments([CommentId],[UserName]) ";
                            sqlinsert += " values(" + ds.Tables[0].Rows[i]["CommentId"].ToString() + ",'" + Session["username"].ToString() + "')";

                            int intResult = DatabaseHelper.executeNonQuery(sqlinsert);

                        }
                        catch
                        {
                        }
                    }

                }

                Response.Redirect("requests.aspx", false);
            }
            else
            {
                ////sql = @"delete from read_task where [task_id]=" + e.Item.Cells[0].Text + " and [username]='" + Session["admin"] + "'";
                ////DatabaseHelper.executeNonQuery(sql);

                //sql = @"select *  from ClientRequest_Details where RequestId =" + e.Item.Cells[0].Text + " and  CommentId  not in (select  tc_id from read_comments where read_comments.username='" + Session["admin"].ToString() + "')";

                sql = @"select *  from ClientRequest_Details where RequestId =" + e.Item.Cells[0].Text + " and  CommentId  in (select  CommentId from read_CR_comments where read_CR_comments.UserName='" + Session["username"].ToString() + "')";
                DataSet ds = DatabaseHelper.getDataset(sql);

                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        try
                        {

                            string sqlinsert = "Delete from read_CR_comments where CommentId =" + ds.Tables[0].Rows[i]["CommentId"].ToString() + " and UserName = '" + Session["username"].ToString() + "'";
                            int intResult = DatabaseHelper.executeNonQuery(sqlinsert);

                        }
                        catch
                        {
                        }
                    }

                }

                Response.Redirect("requests.aspx", false);
            }
        }
    }
    protected void btnClearFilters_Click(object sender, EventArgs e)
    {
        BindWebsiteURL();
        drpStatus.SelectedIndex = 0;
        lblOrderBy.Text = " ORDER BY LastUpdatedOn desc";
        //************************************
        //chkViewDeleted.Checked = false;
        //chkHideClosed.Checked = false;
        //************************************
        Session["filterunreadCR"] = "";
        setFileterSession();
        BindData();
    }
    //protected void chkViewDeleted_CheckedChanged(object sender, EventArgs e)
    //{
    //    lblOrderBy.Text = " ORDER BY LastUpdatedOn desc";
    //    BindWebsiteURL();
    //    drpStatus.SelectedIndex = 0;
    //    drpWebsiteUrl.SelectedIndex = 0;
    //    //Session["filterunreadCR"] = "";
    //    setFileterSession();
    //    BindData();
    //}
    //protected void chkHideClosed_CheckedChanged(object sender, EventArgs e)
    //{
    //    //Session["filterunreadCR"] = "";
    //    setFileterSession();
    //    BindData();
    //}
    protected void lnkUnread_Click(object sender, EventArgs e)
    {

        Session["filterunreadCR"] = "CRNewComment";
        Session["Status"] = "";
        Session["Status"] = null;
      
        Response.Redirect("requests.aspx");
    }
    protected void lnkWORC_Click(object sender, EventArgs e)
    {
        Session["filterunreadCR"] = "";
        Session["Status"] = "awaiting client response- required";
        Response.Redirect("requests.aspx",false);

    }
}
