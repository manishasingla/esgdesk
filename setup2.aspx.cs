using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class setup2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            string strRequest = Request.QueryString["file"];
            string Fullfilename = "";
            string originalfilename = "";
            DataSet dsAttachments = DatabaseHelper.getTaskAllfilename(strRequest);
            if (dsAttachments == null || dsAttachments.Tables.Count <= 0 || dsAttachments.Tables[0].Rows.Count <= 0)
            {


            }
            else
            {


                Fullfilename = dsAttachments.Tables[0].Rows[0]["FileName1"].ToString();

                originalfilename = dsAttachments.Tables[0].Rows[0]["FileName"].ToString().Replace(" ","_");
            }
             //-- if something was passed to the file querystring
            if (!string.IsNullOrEmpty(strRequest))
            {
                //get absolute path of the file
                string path = Server.MapPath(Request.ApplicationPath + "\\Attachment\\backup");

                path += "\\" + Fullfilename;
                //get file object as FileInfo
                System.IO.FileInfo file = new System.IO.FileInfo(path);
                //-- if the file exists on the server
                if (file.Exists)
                {
                    //set appropriate headers
                    Response.Clear();

                  //  string[] strFilename = file.Name.ToString().Split(new char[] { '_' });
                    ////string strOrginalFile = strFilename[1].ToString();
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + originalfilename);
                    Response.AddHeader("Content-Length", file.Length.ToString());
                    Response.ContentType = "application/octet-stream";
                  ///  string[] strFilename = file.FullName.ToString().Split(new char[] { '_' });
                   /// string strOrginalFile = strFilename[1].ToString();
                    Response.WriteFile(file.FullName);
                    //if file does not exist
                    Response.End();
                }
                else
                {
                    Response.Write("This file does not exist.");
                    //nothing in the URL as HTTP GET
                }
            }
            else
            {
                Response.Write("This file does not exist.");
            }
        }
        catch(Exception ex)
        {
        }
    }
}
