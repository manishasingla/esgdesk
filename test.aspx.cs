using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Web.Mail;

public partial class test : System.Web.UI.Page
{
    private static int RequestId;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["clientid"] == null || Session["username"] == null || Session["clientid"].ToString() == "" || Session["username"].ToString() == "")
        {
            Session["returnUrl"] = (Request.QueryString["reqid"] == null || Request.QueryString["reqid"].ToString() == "" ? "requests.aspx" : "test.aspx?reqid=" + Request.QueryString["reqid"].ToString());
            Response.Redirect("login.aspx", false);
            return;
        }

        divMessage.InnerHtml = "";
        if (Request.QueryString["reqid"] != null && Request.QueryString["reqid"] != "0")
        {
            if (!Page.IsPostBack)
            {
                try
                {
                    RequestId = Convert.ToInt16(Request.QueryString["reqid"].ToString());
                    divAttachment.Visible = true;
                    divBackLink.InnerHtml = "<a href=\"edit_request.aspx?reqid=" + RequestId.ToString() + "\">back to change request</a>";

                    string jscript = "function UploadComplete(){" + ClientScript.GetPostBackEventReference(btnSend, "") + "};";
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "FileCompleteUpload", jscript, true);
                    FlashUpload1.QueryParameters = string.Format("UserName={0}", Session["username"].ToString());

                }
                catch
                {
                    divMessage.InnerHtml = "Request Id must be Integer.<br><br><a href='requests.aspx'>View requests</a>";
                    divAttachment.Visible = false;
                }
            }
        }
        else
        {
            Response.Redirect("requests.aspx", false);
        }

    }

    public void BindData(int RequestId)
    {
        DataSet dsRequests = DatabaseHelper.getClientRequest(RequestId);

        if (dsRequests == null || dsRequests.Tables.Count <= 0 || dsRequests.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "Invalid requestId.<br><br><a href='requests.aspx'>back to change requests</a>";
            divAttachment.Visible = false;
        }
    }

    protected void btnSend_Click(object sender, EventArgs e)
    {

        bool flag = false;
        DataSet dsUserDetails = DatabaseHelper.getUserDetails(Session["clientid"].ToString(), Session["username"].ToString());

        if (dsUserDetails == null || dsUserDetails.Tables.Count <= 0 || dsUserDetails.Tables[0].Rows.Count <= 0)
        {
            flag = sendAttachment("", RequestId);
        }
        else
        {
            flag = sendAttachment(dsUserDetails.Tables[0].Rows[0]["Email"].ToString(), RequestId);
        }
        if (flag == true)
        {
            divMessage.InnerHtml = "Your attachment has been sent successfully.<br><br>Do you want to send attachment. Please <a href='test.aspx?reqid=" + RequestId.ToString() + "'>click here</a>";
            divMessage.Visible = true;
            divFlash.Visible = false;
            txtDescription.Text = "";
        }
        else
        {
            divMessage.InnerHtml = "There was problem in sending your attachment. Please <a href='test.aspx?reqid=" + RequestId.ToString() + "'>click here</a> to try again.";
            divMessage.Visible = true;
            divFlash.Visible = false;            
        }
    }
    
    private bool sendAttachment(string email, int RequestId)
    {
        bool flag = false;

        try
        {
            string strBody = "";


            MailMessage mail = new MailMessage();
            mail.From = ConfigurationManager.AppSettings["FromEmail"].ToString();
            mail.Subject = "Attachement(s) for change request " + RequestId.ToString();

            mail.BodyFormat = MailFormat.Html;

            //Change the path appropriately
            //string serverpath = Server.MapPath("Attachment") + "\\" + FileUpload1.FileName;
            //FileUpload1.SaveAs(serverpath);

           
            string uploadPath = Server.MapPath(Request.ApplicationPath + "\\Attachment");

            uploadPath = uploadPath + "\\" + Session["username"].ToString(); // your code goes here
            bool IsExists = System.IO.Directory.Exists(uploadPath);
            if (IsExists)
            {
                DirectoryInfo di = new DirectoryInfo(uploadPath);
                FileInfo[] rgFiles = di.GetFiles("*.*");
                foreach (FileInfo fi in rgFiles)
                {
                    mail.Attachments.Add(new MailAttachment(uploadPath + "\\" + fi.Name));
                }                  
            }
            else
            {
                return false;
            }

            if (email != "")
            {
                strBody = txtDescription.Text.Trim() + "<br><br><a href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/edit_request.aspx?reqid=" + RequestId.ToString() + "'>Click here</a> to view change request.<br><br>";
                strBody += "-------------------------------------------<br><br>";
                strBody += ConfigurationManager.AppSettings["CompanyName"].ToString() + "<br>";
                strBody += ConfigurationManager.AppSettings["WebAddress"].ToString() + "<br>";

                mail.To = email;
                mail.Body = strBody;

                try
                {
                    SmtpMail.Send(mail);
                }
                catch (Exception ex)
                {
                }
            }

            strBody = txtDescription.Text.Trim() + "<br><br><a href='" + ConfigurationManager.AppSettings["WebAddress"].ToString() + "/admin/edit_request.aspx?reqid=" + RequestId.ToString() + "'>Click here</a> to view change request.<br><br>";
            strBody += "-------------------------------------------<br><br>";
            strBody += ConfigurationManager.AppSettings["CompanyName"].ToString() + "<br>";
            strBody += ConfigurationManager.AppSettings["WebAddress"].ToString() + "<br>";

            mail.To = ConfigurationManager.AppSettings["ToEmail1"]; ///+ ";" + ConfigurationManager.AppSettings["ToEmail2"];
            mail.Body = strBody;

            try
            {
                SmtpMail.Send(mail);
            }
            catch (Exception ex)
            {
                flag = false;
            }

            // Uploaded file deleted after sending e-mail
            try
            {
                if (System.IO.Directory.Exists(uploadPath))
                {
                    DirectoryInfo di = new DirectoryInfo(uploadPath);
                    
                    FileInfo[] rgFiles = di.GetFiles("*.*");
                    foreach (FileInfo fi in rgFiles)
                    {
                        System.IO.File.Delete(uploadPath + "\\" + fi.Name);
                    }
                    di.Delete();
                }
            }
            catch (Exception ex)
            {
            }
            flag = true;
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
            flag = false;
        }
        return flag;
    }
}
