using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class test2 : System.Web.UI.Page
{
    static ArrayList arr1aAssignedUsers = new ArrayList();

    string sql = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        register1aTaskAllocated();
    }

     void register1aTaskAllocated()
    {
        arr1aAssignedUsers.Clear();
        
        sql = @"select distinct assigned_to_user from tasks 
                where tasks.deleted <> 1 and tasks.status <> 'closed' and tasks.priority = '1a - highest' ";

        DataSet ds = DatabaseHelper.getDataset(sql);

        foreach (DataRow row in ds.Tables[0].Rows)
        {
            try
            {
                if ((row["assigned_to_user"] != null && row["assigned_to_user"].ToString().Trim() != ""))
                {
                    arr1aAssignedUsers.Add(row["assigned_to_user"]);
                }
            }
            catch { }
        }
        
        ClientScriptManager cs = Page.ClientScript;

        string str1aAssignedUsers = "";

        if (arr1aAssignedUsers.Count > 0)
        {
            for (int j = 0; j < arr1aAssignedUsers.Count; j++)
            {
                try
                {
                    str1aAssignedUsers += "'" + arr1aAssignedUsers[j] + "',";
                }
                catch { }
            }
        }

        str1aAssignedUsers = str1aAssignedUsers.TrimEnd(new char[] { ',' });

        if (cs.IsClientScriptBlockRegistered("MyArr1aAssignedUsers") == false)
        {
            cs.RegisterArrayDeclaration("MyArr1aAssignedUsers", str1aAssignedUsers);
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {

    }
}
