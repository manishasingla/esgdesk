using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Mail;

public partial class test_attachment : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        FileUpload1.Attributes.Add("onchange", "checkFileSize(this);");
    }
    protected void btnSend_Click(object sender, EventArgs e)
    {
        bool flag=false;
        int RequestId = 12;
        try
        {
            MailMessage mail = new MailMessage();
            mail.From = "pratapk@123sitesolutions.com";
            mail.To = "pratapk@123sitesolutions.com";
            mail.Subject = "Attachement(s) for change request " + RequestId.ToString();
            mail.Body = "test only";
            mail.BodyFormat = MailFormat.Html;

            //Change the path appropriately
            string serverpath = Server.MapPath("Attachment") + "\\" + FileUpload1.FileName;
            FileUpload1.SaveAs(serverpath);
            mail.Attachments.Add(new MailAttachment(serverpath));
            try
            {
                SmtpMail.Send(mail);
            }
            catch (Exception ex)
            {
                flag = false;
            }

            // Uploaded file deleted after sending e-mail
            try
            {
                if (System.IO.File.Exists(serverpath))
                {
                    System.IO.File.Delete(serverpath);
                }
            }
            catch (Exception ex)
            {

            }

            flag = true;
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
            flag = false;
        }
        if (flag == true)
        {
            Response.Write("Sent successfully");
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {

    }
    protected void Button2_Click(object sender, EventArgs e)
    {

        string subPath = "Attachment/ImagesPath"; // your code goes here
        bool IsExists = System.IO.Directory.Exists(Server.MapPath(subPath));
        if (!IsExists)
            System.IO.Directory.CreateDirectory(Server.MapPath(subPath));
    }
}
