using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net.Mail;
using System.Net;

public partial class test_speed : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
		
		   if (Session["login"] == null || Session["login"].ToString() == "")
        {
            LinkButton1.Text = "Login";
            LinkButton2.Text = "Register";
        }
        else
        {
            LinkButton1.Text = "Logout";
            LinkButton2.Text = "My account";
        }
            
        }
        catch (Exception ex)
        {
            Session["Error"] = ex.ToString();
            string methdname = " sitemap.aspx page on Page_Load.";
            string StrErrorEmail = DatabaseHelper.SendErrorEmail(Session["Error"].ToString(), methdname);
            Response.Redirect("../fhs.aspx", false);


        }
    }
	
	protected void LinkButton1_Click(object sender, EventArgs e)
    {
        if (LinkButton1.Text == "Login")
        {
            Response.Redirect("../login.aspx", false);
        }
        else if (LinkButton1.Text == "Logout")
        {
            try
            {
                string strMsg = Session["login"].ToString() + "  " + "logged out from Spoof website at  " + hdndate.Value;
                MailMessage mMailMessage = new MailMessage();
                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                // Set the sender address of the mail message
                msg.From = new MailAddress("app@clubspoof.co.uk");

                // Set the recepient address of the mail message
                //msg.To.Add(new MailAddress("michaelr@123sitesolutions.com"));
                msg.To.Add(new MailAddress("michaelr@123sitesolutions.com"));
                msg.CC.Add(new MailAddress("jamesb@estatesolutions.co.uk"));

                // Set the subject
                msg.Subject = Session["login"].ToString() + "  " + "logged out from Spoof website";
                // Set the body of the mail message
                msg.Body = strMsg;

                // Set the format of the mail message body as HTML
                msg.IsBodyHtml = true;
                // Set the priority of the mail message to normal
                msg.Priority = MailPriority.Normal;

                // Instantiate a new instance of SmtpClient
                System.Net.Mail.SmtpClient mSmtpClient = new System.Net.Mail.SmtpClient("mail.estatesolutions.co.uk");
                /// System.Net.Mail.SmtpMail.SmtpServer = ConfigurationManager.AppSettings["SmtpServer"];

                mSmtpClient.UseDefaultCredentials = false;
                System.Net.NetworkCredential loginInfo = new System.Net.NetworkCredential("test@estatesolutions.co.uk", "testtest");
                mSmtpClient.Credentials = loginInfo;
                mSmtpClient.Send(msg);
            }
            catch { }

            Session.Remove("login");
            Session.Remove("UserFullname");
            Session.Remove("useraction");
            Response.Redirect("../fhs.aspx", false);
       }
        else
        {
            Response.Redirect("../login.aspx", false);
        }
    }
    protected void LinkButton2_Click(object sender, EventArgs e)
    {
        if (LinkButton2.Text == "Register")
        {
            Response.Redirect("../register.aspx",false);
        }
        else if (LinkButton2.Text == "My account")
        {
            if (Session["login"] == null || Session["login"].ToString() == "")
            {
                Response.Redirect("../login.aspx", false);
            }
            else
            {

                Response.Redirect("../MyAccount.aspx", false);
            }
        }
    }
    
}
