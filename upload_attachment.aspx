<%@ Page Language="C#" AutoEventWireup="true" CodeFile="upload_attachment.aspx.cs" Inherits="send_attachment" %>

<%@ Register Assembly="FlashUpload" Namespace="FlashUpload" TagPrefix="cc2" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Src="footer.ascx" TagName="footer" TagPrefix="uc2" %>

<%@ Register Src="Header.ascx" TagName="Header" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= ConfigurationManager.AppSettings["CompanyName"].ToString()%></title>
    <link href="StyleSheet.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        function disableSendButton()
        {            
            document.getElementById("btnSend1").disabled=true;
            document.getElementById("divMessage").innerHTML ="Please wait: your file is being sent...";
            document.getElementById("btnSend").click();
            return false;
        }
    </script>
    <script type="text/javascript">
        function GetFileName(val) {
            document.getElementById("divMessage").innerHTML = "";
        }
   </script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <uc1:Header ID="Header1" runat="server" />
    <div id="Content">
    
    <div id="divAttachment" runat="server" style="text-align:left">
        <table width="100%" cellpadding="0">
      <tr>
                <td align="left" colspan="1">
                <div id="divBackLink" runat="server" style="width:100%; background:#ebebeb; padding:5px 0;"></div>
                </td>
            </tr>
            <tr>
                <td align="left">
                <%--<div id="divMessage" runat="server" style="font-weight: bold; color: red; text-align: left"></div>--%>
                   <div id="divFlash" runat="server">
                    <table cellpadding="3">
                        <tr>
                            <td colspan="3">
                            <span style="color:Red;font-weight:bold;">
                            (Please  upload attachments only for files such as images rather than attachments which have instructions for us. Include instructions in the request itself.
                                            Please do not exceed the size of 20MB in total)
                            
                            <%--(Please include text in the request itself. Only upload attachments for such things as images and ensure they are no bigger than 20MB in total)--%></span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                Description: &nbsp;
                                <asp:TextBox ID="txtDescription" runat="server" Width="450px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3">
                            
                     <%--==========================================================================================--%>
                            <%--<cc2:FlashUpload ID="FlashUpload1" runat="server"
                     UploadPage="Upload.axd" OnUploadComplete="UploadComplete()" 
                     FileTypeDescription="Images" FileTypes="*.*" 
                     UploadFileSizeLimit="20971520" TotalUploadSizeLimit="20971520" >
                    </cc2:FlashUpload>--%>
                    <%--===========================================================================================--%>
                    <asp:FileUpload ID="FlashUpload1" runat="server" Onchange="GetFileName(this.value);" size="70px" />
                    <asp:Button ID="btnSend" runat="server" Text="  Upload   " OnClick="btnSend_Click"  />
                    <div id="divMessage" runat="server" style="font-weight: bold; color: red; text-align: left"></div>
                    
                    
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                
                            
                            </td>
                        </tr>
                    </table>
                   </div>
                </td>
            </tr>
        </table>
    </div>    
    </div>
        
    </div>       
        <uc2:footer ID="Footer1" runat="server" />
    </form>
</body>
</html>
