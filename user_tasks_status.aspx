<%@ Page Language="C#" AutoEventWireup="true" CodeFile="user_tasks_status.aspx.cs" Inherits="user_tasks" %>
<%@ OutputCache Duration="60" VaryByParam="*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="Admin_Header.ascx" TagName="Admin_Header" TagPrefix="uc2" %>
<%@ Register Src="Notifications.ascx" TagName="Notifications" TagPrefix="uc4" %>
<%@ Register Src="../footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        <%= ConfigurationManager.AppSettings["CompanyName"].ToString()%></title>
    <meta http-equiv="refresh" content="1200" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        function window.confirm(str) {
            execScript('n = msgbox("' + str + '","4132")', "vbscript");
            return (n == 6);
        }
    </script>

 

    <link rel="stylesheet" type="text/css" href="slider.css" />

    <script type="text/javascript" src="slider.js"></script>

    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
    <link href="SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <uc2:Admin_Header ID="Admin_Header1" runat="server" />
        <div id="Content">
            <asp:UpdateProgress ID="updateprogress1" runat="server">
                <ProgressTemplate>
                    <div id="processmessage" class="processMessage">
                        <img alt="loading" src="images/ajax-loader.gif" style="width: 80px; height: 80px" />
                        <span style="color: black; font-weight: bold; font-size: 18px; vertical-align: super;">
                            Please wait... </span>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div id="Content_98">
                        <%--<div id="pageTitle">
                Tasks<span style="padding-left: 885px;"></span> <a href="http://www.esgdesk.com//Download/ESG_Desk.application"
                    style="color: white; font-size: 15px; text-decoration: bold; cursor: pointer;"
                    onmouseover="this.style.color='blue'" onmouseout="this.style.color='#ffffff'">Click
                    here to download the desktop application</a></div>--%>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="left" valign="top" style="display: none;">
                                    <div style="height: 40px; vertical-align: middle; line-height: 40px; margin: 20px 0 0 0;">
                                        <div class="titleText" style="float: left;">
                                            Tasks</div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                
                        <div id="divMessage" runat="server" style="color: Red; font-weight: bold; text-align:center; font-size:14px; margin-top:5px;">
                        </div>
                        <div>
                           <asp:Label ID="Label1" runat="server" Text="List of employees who have 'CWO required' = true but which do not currently have any task which is 'CWO' OR 'CWO but away'"></asp:Label> 
                        
                         <%--   <asp:GridView ID="DataGrid1" runat="server" Width="26%" AutoGenerateColumns="False"
                            Style="margin-left: 0px; margin-top:50px; vertical-align: top" 
                                AllowPaging="True" AllowSorting="True" CellPadding="5" PageSize="14" 
                                  OnPageIndexChanging="DataGrid1_PageIndexChanging">--%>

                                <asp:GridView ID="DataGrid1" runat="server" Width="26%" AutoGenerateColumns="False"
                            Style="margin-left: 0px; margin-top:50px; vertical-align: top" 
                                AllowSorting="True" CellPadding="5" 
                                  >
                                <Columns>
                                     <asp:BoundField DataField="username" visible="false"  />
                                     <asp:BoundField DataField="firstname" HeaderText="First name" HeaderStyle-Font-Bold="true"   />
                                     <asp:BoundField DataField="lastname" HeaderText="Sur name" HeaderStyle-Font-Bold="true"   />
                                    
                                     <asp:TemplateField  HeaderText="Tasks" >
                                     <ItemTemplate>
                                        <asp:HyperLink id="lbltask" Text="Tasks" runat="server"  NavigateUrl='<%# "Priority_reports.aspx?Ename="+ Eval("username") %>' Target="_blank"></asp:HyperLink>
                                     </ItemTemplate>
                                     </asp:TemplateField>
                                     
                                     <asp:TemplateField  HeaderText="Employee details">
                                     <ItemTemplate>
                                      <asp:HyperLink id="lbltask" Text="Employee details" runat="server"  NavigateUrl='<%# "edit_user.aspx?id="+ Eval("user_id") %>' Target="_blank"></asp:HyperLink>
                                         <%--<asp:Label ID="lblEmpdet" runat="server" Text="Employee details"></asp:Label> --%>
                                     </ItemTemplate>
                                     </asp:TemplateField>
                                </Columns>
                                <RowStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Top" Font-Size="12px" />
                                <%--<ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Top" Font-Size="12px" />--%>
                            </asp:GridView>
                           
                        </div>
                       
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <uc1:footer ID="Footer1" runat="server" />
    <asp:Label ID="lblOrderBy" runat="server" Text="" Visible="false"></asp:Label>
    <div id="popup" class="popup" style="background-color: #e9e9e9; width: 650px; border: solid 1px #000000;
        border-bottom: solid 2px #000000; border-right: solid 2px #000000; padding: 10px 10px 10px 10px;
        position: absolute; display: none">
    </div>
    </form>
</body>
</html>
