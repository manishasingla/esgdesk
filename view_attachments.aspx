<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view_attachments.aspx.cs" Inherits="Admin_View_Attachments" %>

<%@ Register Src="footer.ascx" TagName="footer" TagPrefix="uc2" %>

<%@ Register Src="Header.ascx" TagName="Header" TagPrefix="uc1" %>

<%@ Register Assembly="FlashUpload" Namespace="FlashUpload" TagPrefix="cc2" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= ConfigurationManager.AppSettings["CompanyName"].ToString()%></title>
    <link href="StyleSheet.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        function disableSendButton()
        {            
            document.getElementById("btnSend1").disabled=true;
            document.getElementById("divMessage").innerHTML ="Please wait: your file is being sent...";
            document.getElementById("btnSend").click();
            return false;
        }
function pageTitle_onclick() {

}

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <uc1:Header ID="Header1" runat="server" />
    <div id="Content">
    <div id="pageTitle" style="height:30px;font-weight:bold;text-align:left" onclick="return pageTitle_onclick()">View attachment <span style="padding-left:875px;"></span> <a href="http://www.esgdesk.com//Download/ESG_Desk.application" style="color:white; font-size:15px; text-decoration:bold; cursor:pointer;" onmouseover="this.style.color='blue'" onmouseout="this.style.color='#ffffff'">Click here to download the desktop application</a></div>
    <div id="divBackLink" runat="server"></div>
    <div id="divMessage" runat="server" style="color:Red;font-weight:bold;"></div>   
    <div id="divAttachment" runat="server" style="text-align:left"><br />
        <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" CellPadding="5" Width="100%" AllowPaging="True" AllowSorting="True" OnSortCommand="DataGrid1_SortCommand" OnDeleteCommand="DataGrid1_DeleteCommand" OnPageIndexChanged="DataGrid1_PageIndexChanged" PageSize="100">
            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Top" />
            <HeaderStyle BackColor="LightGray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
            <Columns>
                <asp:BoundColumn DataField="Aid" HeaderText="Aid" Visible="False"></asp:BoundColumn>
                <asp:BoundColumn DataField="RequestId" HeaderText="RequestId" Visible="False"></asp:BoundColumn>
                <asp:BoundColumn DataField="FileName1" HeaderText="FileName1" Visible="False"></asp:BoundColumn>
                <asp:BoundColumn DataField="FileName" HeaderText="File name" SortExpression="FileName">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="Description" HeaderText="Description" SortExpression="Description">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="UploadedBy" HeaderText="Uploaded by" SortExpression="UploadedBy">
                </asp:BoundColumn>
                <%--===============================================================================================--%>
                   <%--<asp:BoundColumn DataField="UploadedOn" HeaderText="Uploaded on" SortExpression="UploadedOn">
                     </asp:BoundColumn>--%>
                
                 <%--==============================================================================================--%>
                        <asp:TemplateColumn HeaderText="UploadedOn" SortExpression="UploadedOn">
                        <ItemTemplate>
                        <asp:Label ID="lbldate" runat="server" Text='<%# Bind("UploadedOn","{0:dd/MM/yyyy HH:mm:ss }") %>' ></asp:Label>
                        </ItemTemplate> 
                        </asp:TemplateColumn>
                 <%--==============================================================================================--%>
                
                 <asp:HyperLinkColumn HeaderText="Download" Text="download" DataNavigateUrlField="Aid" DataNavigateUrlFormatString="setup.aspx?file={0}" NavigateUrl="setup.aspx" Target="_blank">
                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" HorizontalAlign="Center" />
                </asp:HyperLinkColumn>
                <asp:ButtonColumn CommandName="Delete" HeaderText="Delete" Text="delete" Visible="False">
                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" HorizontalAlign="Center" />
                </asp:ButtonColumn>
            </Columns>
            <PagerStyle NextPageText="Next" PrevPageText="Prev" />
        </asp:DataGrid>
    </div>
    </div>
    
    <asp:Label id="lblOrderBy" runat="server" Visible="false" Text=""></asp:Label>
    </div>        <uc2:footer ID="Footer2" runat="server" />
    </form>
</body>
</html>
