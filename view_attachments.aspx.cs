using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Admin_View_Attachments : System.Web.UI.Page
{
    private static int RequestId; 

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["clientid"] == null || Session["username"] == null || Session["clientid"].ToString() == "" || Session["username"].ToString() == "")
        {
            Session["returnUrl"] = (Request.QueryString["reqid"] == null || Request.QueryString["reqid"].ToString() == "" ? "requests.aspx" : "view_attachments.aspx?reqid=" + Request.QueryString["reqid"].ToString());
            Response.Redirect("login.aspx", false);
            return;
        }

        divMessage.InnerHtml = "";
        if (Request.QueryString["reqid"] != null && Request.QueryString["reqid"] != "0")
        {
            if (!Page.IsPostBack)
            {
                try
                {
                    RequestId = Convert.ToInt16(Request.QueryString["reqid"].ToString());
                    divAttachment.Visible = true;
                    divBackLink.InnerHtml = "<a href=\"edit_request.aspx?reqid=" + RequestId.ToString() + "\">back to change request</a>";
                    lblOrderBy.Text = " ORDER BY Aid desc";
                    BindData(RequestId);
                }
                catch
                {
                    divMessage.InnerHtml = "Request Id must be Integer.<br><br><a href='requests.aspx'>View requests</a>";
                    divAttachment.Visible = false;
                }
            }
        }
        else
        {
            Response.Redirect("requests.aspx", false);
        }
    }

    public void BindData(int RequestId)
    {
        DataSet dsAttachments = DatabaseHelper.getAttachments(RequestId, lblOrderBy.Text);

        if (dsAttachments == null || dsAttachments.Tables.Count <= 0 || dsAttachments.Tables[0].Rows.Count <= 0)
        {
            divMessage.InnerHtml = "<br>No attachment found.";
            divMessage.Visible = true;
            divAttachment.Visible = false;            
        }
        else
        {
            DataGrid1.DataSource = dsAttachments.Tables[0];
            DataGrid1.DataBind();
            ////Session["DBFilename"] = 
            if (dsAttachments.Tables[0].Rows.Count <= 100)
            {
                DataGrid1.PagerStyle.Visible = false;
            }
            else
            {
                DataGrid1.PagerStyle.Visible = false;
            }
        }
    }
    protected void DataGrid1_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        if (e.SortExpression.ToString() == Session["Column"])
        {
            //Reverse the sort order
            if (Session["Order"] == "ASC")
            {
                lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " DESC";
                Session["Order"] = "DESC";
            }
            else
            {
                lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " ASC";
                Session["Order"] = "ASC";
            }
        }
        else
        {
            //Different column selected, so default to ascending order
            lblOrderBy.Text = " ORDER BY " + e.SortExpression.ToString() + " ASC";
            Session["Order"] = "ASC";
        }

        Session["Column"] = e.SortExpression.ToString();
        BindData(RequestId);
    }
    protected void DataGrid1_DeleteCommand(object source, DataGridCommandEventArgs e)
    {
        if(e.CommandName =="Delete")
        {
            object objResult = DatabaseHelper.deleteAttachment(e.Item.Cells[0].Text);

            if (objResult.ToString() != "0")
            {
                string uploadPath1 = Server.MapPath(Request.ApplicationPath + "\\Attachment\\backup");

                uploadPath1 = uploadPath1 +"\\"+ e.Item.Cells[2].Text;

                try
                {
                    if (System.IO.File.Exists(uploadPath1))
                    {
                        System.IO.File.Delete(uploadPath1);
                    }
                }
                catch (Exception ex)
                {

                }                
                BindData(RequestId);
            }
            else
            {
                divMessage.InnerHtml = "There was problem in deleting attachment. Please try again.";
                divMessage.Visible = true;
                BindData(RequestId);
            }
        }
    }
    protected void DataGrid1_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        DataGrid1.CurrentPageIndex = e.NewPageIndex;
        BindData(RequestId);
    }
}
